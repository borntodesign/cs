#!/usr/bin/perl -w

##
## pf_download.pl - Gets a spreadsheet from Performics.
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use WWW::Mechanize;

my $rpt_dates  = '';
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
my $id = 'PF';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400) . '.xls';

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
        if (/^$id\..*\.xls$/) {unlink $RPT_DIR . $_;}
}

unless( $rpt_dates = aff_proc::rptDates('PF') ){ exit; }
$rpt_dates->{rpt_start_month} = sprintf("%02d", ++$rpt_dates->{rpt_start_month});
$rpt_dates->{rpt_end_month} = sprintf("%02d", ++$rpt_dates->{rpt_end_month});

my $mech = WWW::Mechanize->new();
$mech->get('http://www.connectcommerce.com/global/login.html');
$mech->submit_form(
		fields	=> {
				UserLogin	=> 'orders@dhs-club.com',
				UserPass	=> 'welcome'
			}
		);
$mech->get('http://www.connectcommerce.com/reports/performance_based.html?report_type=Transactions');
$mech->submit_form(
		form_name=> 'reportCriteria',
                fields  => {
			start_day      	=> $rpt_dates->{rpt_start_day},
			start_month	=> $rpt_dates->{rpt_start_month},
			start_year	=> $rpt_dates->{rpt_start_year},
                        end_day        	=> $rpt_dates->{rpt_end_day},
                        end_month	=> $rpt_dates->{rpt_end_month},
                        end_year	=> $rpt_dates->{rpt_end_year},
                        Period_Radio    => 2,
                        Client_Param    => 'AllClients',
                        output_type     => 'transaction',
                        Trans_Param     => 'AllTrans',
			view		=> 0
                        },
		button	=> 'download'
                );
$mech->save_content($rpt_file);

my $status = $mech->status;
my $log_msg = '[downloader]: Download of ' . 'PF' . " report: $status";
aff_proc::printLog($log_msg);
unless ($mech->success) {print $status;}
