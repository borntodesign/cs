#!/usr/bin/perl -w
## rpt_parse.pl - Basically this sends the configs to aff_proc::parseText, for 
## for parsing some text delimited files.
###### of course we end up doing some pre/post processing for the quirkiness of some affiliates

## originally written: 03/07	Shane Partain
## last modified: 06/22/07	Bill MacArthur

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;

my %reports = ();

# the cols used is roughly equivalent to the desired layout of the data in the output file
# the default first column is the affiliate code which we don't include in the cols_used array

# the contents within a 'sub{...}' will be eval'd, but will not create a column
# this code should end with 1; which is what eval will pickup. If the code doesn't eval properly (ie. return '1')
# an error will be generated

# $this_row{} are the hash keys which will hold the parsed data, the key assuming the name of the columns defined below
# our current row definition is:
# Affiliate Code | Advertiser Name | Advertiser ID | Trans Date | Sale Amount | Commission |
#	Member ID | Description | Sale Currency | Commission Currency

# output_cols defines the final column defs and this list will be used, if present, instead of cols_used
# the first column defaults to the affiliate code
# the 'BLANK' column should be used for a placeholder as necessary elsewhere

# this is what the output file parser currently expects
###### our input (.out) file columns
# aff_code adv-name adv-id trans-date sale-amt comm-amt 'sid' 'description' sale-currency commission-currency
#	0	1		2		3	4	5	   6		7		8		9

### 
### Config for CJ
###
# we include our sub{...} first so that the description can be built before we get to it since these will be processed in the order presented
$reports{CJ} = {
	rpt_type => 'txt',
	cols_used => [ q|sub{$this_row{'ID'} = "$this_row{'Action_Name'} - CJ ID: $this_row{'ID'}"; 1;}|,
		'Advertiser_Name', 'Advertiser_ID', 'Event_Date',
		'Sale_Amount', 'Commission', 'SID', 'ID', 'Action_Name'],
	output_cols => [ 'Advertiser_Name', 'Advertiser_ID', 'Event_Date',
		'Sale_Amount', 'Commission', 'SID', 'ID', 'BLANK', 'BLANK'],
	delimiter => '\t',
	head_row => 0};

###
### LS
###
$reports{LS} = {
	rpt_type => 'txt',
	cols_used => ['Merchant Name', 'Merchant ID', 'Transaction Date', 'Sales($)', 'Commissions($)', ' Member ID',
		'Order ID', 'Sale Currency', 'Commission Currency'],
	delimiter => '\t',
	head_row => 0,
	post_processing => sub {
		return Combine_orders( LS_Doctor_data($_[0]), 'Order ID', 'Sales($)', 'Commissions($)');
	}
};

###
### SS
###
$reports{SS}{rpt_type} = 'txt';
$reports{SS}{cols_used} = ['BLANK', 'merchantID', 'transdate', 'transamount', 'commission', 'affcomment'];
$reports{SS}{delimiter} = '\|';
$reports{SS}{head_row} = 0;

###
### TD
###
$reports{TD} = {
	rpt_type => 'txt',
	cols_used => ['Name', 'ID', 'of event', 'Order Value', 'Publisher', 'EPI 1', 'Order nr', 'BLANK', 'BLANK'],
	delimiter => ';',
	head_row => 1,
	pre_process_line => \&TD_PreProcess_Line,
	pre_process_col => \&TD_PreProcess_Col,	# restore our preprocessed/converted semicolons
	post_processing => sub {
		return Combine_orders( TD_Doctor_data($_[0]), 'Order nr', 'Order Value', 'Publisher');
	}
};

###
### ZA
###
$reports{ZA} = {
	rpt_type => 'txt',
	###### we will be creating columns from some existing ones, namely Advertiser, SaleCurr & CommCurr
	###### Zanox is the biggest pitb
	cols_used => [
		q|sub {
			# first off, we need to convert something like this: 82,97 EUR
			# into a currency column and a real value ie. 82.97
			# also we need to remove any . they use for commas.... did I say that Zanox is a pitb?
			foreach my $k ('Net amount (local currency)', 'Commission'){
				$this_row{$k} =~ m/^(.+?)\s(.{3})/;
				$this_row{ $k eq 'Commission' ? 'CommCurr' : 'SaleCurr' } = $2;
				$this_row{$k} = $1;
				$this_row{$k} =~ s/\.//g;
				$this_row{$k} =~ s/,/\./;
			}
			# now extract our advertiser from the Remarks
			($this_row{Advertiser} = $this_row{Remark}) =~ s/^(.+?):.*/$1/;

			# now reorganize the messed up german dd.mm.yyyy transaction date... and lose the timestamp
			# did I say that Zanox is a pitb?
			$this_row{'Dated on'} =~ s/^(\d{2})\.(\d{2})\.(\d{4}).*/$3-$2-$1/;
		}|,
		'Status', 'Net amount (local currency)', 'Dated on', 'Commission', 'Remark', 'SubAffiliateID'],
	output_cols => [ 'BLANK', 'Advertiser', 'Dated on', 'Net amount (local currency)',
		'Commission', 'SubAffiliateID', 'BLANK', 'SaleCurr', 'CommCurr'],
	delimiter => ';',
	head_row => 0,
	pre_process_line => \&ZA_PreProcess_Line
};

my @rpts = @ARGV;
@rpts = keys %reports unless @rpts;

foreach my $rptid (@rpts){
	my $rv = aff_proc::parseText( $rptid, $reports{$rptid}	);

	###### if we have a post_processing key, we should be expecting a dataset back
	###### we can operate on that and then write it out
	if ($reports{$rptid}{post_processing} && ref $rv eq 'ARRAY')
	{
		$rv = aff_proc::WriteOut(
			$rptid,
			($reports{$rptid}{output_cols} || $reports{$rptid}{cols_used}),
			&{$reports{$rptid}{post_processing}}($rv)
		);
	}

	if ($rv) { print "$rv\n"; }
	else {
		aff_proc::printLog("[parser]: $rptid successfully parsed.");
	}
}

exit;

sub Combine_orders($$$$)
{
	###### Linkshare & TradeDoubler spit out one line for every item a customer buys
	###### this is way too redundant for reporting sales, so we'll combine it all into one sale
	my $list = shift;		###### an array ref containing a list of hashrefs of data
	###### column name args
	my $col_order = shift;
	my $col_sale = shift;
	my $col_comm = shift;

	my (%rs) = ();
	my $x = 0;
	###### we'll combine on Order ID
	foreach my $item (@{$list}){
		###### TradeDoubler provides Order ID's *in most* cases
		###### if we don't receive one, we'll create our own using unique values ;)
		my $key = $item->{ $col_order } || $item;		# this should give something like 'HASH(0x15d5100)'

		if (exists $rs{ $key }){
			###### some fields have been known to come through empty
			###### I have unit'd var errors
			$rs{ $key }->{$col_sale} += $item->{ $col_sale } || 0;
			$rs{ $key }->{ $col_comm } += $item->{ $col_comm } || 0;
		} else {
			###### in order to preserve the merchant order to be identical with the input file
			###### we'll add a sorting value to this order ID/item
			$item->{sort_order} = $x++;
			$rs{ $key } = $item;
		}
	}
	return [sort {$a->{sort_order} <=> $b->{sort_order}} values %rs];
}

sub LS_Doctor_data
{
	###### Linkshare reports amounts with comma delimiters at the 1000's mark: "2,055.72"
	###### so we'll remove the commas

	my $list = shift;	###### an array ref containing a list of hashrefs of data

	for (my $x=0; $x < scalar @{$list}; $x++){
		$list->[$x]->{'Sales($)'} =~ s#,##g;
	}
	return $list;
}

sub TD_Doctor_data
{
	###### TradeDoubler reports dates as the bogus DD/MM/YY format with a timestamp to boot
	###### we'll reformat to a simple YYYY-MM-DD format
	###### yes we will assume the year is in the 21st century 20xx
	###### the guys taking care of this in the next century can worry about it then... lol

	###### they also report amounts with comma delimiters at the 1000's mark: "2,055.72"
	###### so we'll remove the commas

	###### lastly, the last line of their report is a numerical summary

	my $list = shift;	###### an array ref containing a list of hashrefs of data
	pop @{$list};		###### we'll remove that last line from our dataset
	
	for (my $x=0; $x < scalar @{$list}; $x++){
		$list->[$x]->{'of event'} =~ s#(\d{2})/(\d{2})/(\d{2}).*#20$3-$2-$1#;
		$list->[$x]->{'Order Value'} =~ s#,##g;
		$list->[$x]->{'Publisher'} =~ s#,##g;
	}
	return $list;
}

sub TD_PreProcess_Col
{
	###### we will work on refs instead of passing data back and forth
	my $col = shift;
	$$col =~ s/\x1F/;/g if $$col;
	return undef;
}

sub TD_PreProcess_Line
{
	# TradeDoubler sometimes throws in a quote encapsulated field because the column contains the delimiter ';'
	# so... we need to deal with it
	# first we will remove the quotes
	# then we will convert any semicolons between quotes into 'unit separator's \x1F
	# ... then, in the post processing of the column later,
	# we will convert the unit separators back to semicolons to restore the exact data
	###### we will work on refs instead of passing data back and forth
	my $line = shift;
	$$line =~ s/"(.+?)"/_my_inside($1)/eg if $$line;
	return undef;

	sub _my_inside { my $r = shift; $r =~ s#;#\x1F#g; return $r; }
}

sub ZA_PreProcess_Line
{
	# Zanox maintains realtime data even if looking at a backdated list
	# so.. we are only using 'Confirmed' entries as they are not subject to change
	###### we will work on refs instead of passing data back and forth
	my $line = shift;

	###### since the ladies were entering 'open' transactions
	###### we have to flag 'refused' ones and process them manually
	if ($$line =~ m/^Refused/){
		$$line =~ m/;(\d{2})\.(\d{2})\.(\d{4})/;
		###### reorganize the messed up german dd.mm.yyyy date so that is comparable lexically
		# we are not concerned after this date as the data was entered only after being 'Confirmed'
		if ("$3-$2-$1" lt '2007-06-26'){
			###### just print it out so the cron 'user' will get it
			print "Refused:\n$$line\n";
		}
	}
	$$line = undef unless $$line =~ m/^Confirmed/;
	return undef;
}

# 04/26/07 added the 'ID' column to the CJ list of fields used so we could include the transaction ID
###### 06/11/07 added the capability for post processing of data instead of an integrated read/process/write flow
###### 06/14/07 added the capability to perform multiple step post-processing
###### 06/22/07 added the TD_Pre* routines and changed the arg passing to aff_proc::parseText
# 01/19/09 added LS_Doctor_data and put it in to prefilter the data before doing the combine routine