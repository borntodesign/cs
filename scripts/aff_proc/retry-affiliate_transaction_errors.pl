#!/usr/bin/perl -w
###### retry-affiliate_transaction_errors.pl
###### gather previous affiliate transaction errors that have been corrected
###### deployed: 05/01/07	Bill MacArthur
###### last modified:

use strict;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;

###### our input (.out) file columns
# aff_code adv-name adv-id trans-date sale-amt comm-amt 'sid' 'description' sale-currency commission-currency
#	0	1		2		3	4	5	   6		7		8		9

(my $PATH = $0) =~ s#(.*)(/.*)#$1#;

my $INPUT_DIR = 'reports';		###### where we'll write to
my $TRANS_TBL = 'transactions';

my $SQL = 'SELECT pk, aff_code, adv_name, adv_id, trans_date, sale_amt, comm_amt,
		sid, description, sale_currency, commission_currency
	FROM affiliate_transaction_errors WHERE corrected=TRUE';

###### Notice that this order mirrors the order of our .out file
###### it also mirrors the DB column names
my @OUT_COLS = qw/aff_code adv_name adv_id trans_date sale_amt comm_amt sid description sale_currency commission_currency/;

my $db = DB_Connect::DB_Connect('generic') || die "Failed to create a DB connection\n";
my $list = $db->selectall_hashref($SQL, 'pk');
if (keys %{$list})
{
	my $file = "$PATH/$INPUT_DIR/" . time . '-retry-affiliate_transaction_errors.out';
	open (OUT, ">$file") || die "Failed to open output file: $file\n";
	print OUT join("\t", @OUT_COLS), "\n";
	foreach my $tmp(values %{$list}){
		print OUT join("\t", map {defined $tmp->{$_} ? $tmp->{$_} : ''} @OUT_COLS), "\n";
		###### now expunge the error row
		die "Failed to delete record with pk: $tmp->{pk}\n"
			unless $db->do("DELETE FROM affiliate_transaction_errors WHERE pk = $tmp->{pk}");
	}
	close OUT;
}
else { print "No error records received.\n"; }

$db->disconnect;
exit;
