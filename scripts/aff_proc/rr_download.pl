#!/usr/bin/perl -w

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use WWW::Mechanize;

my $rpt = '';
my $rpt_dates  = '';
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
my $id = 'RR';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400) . '.txt';
my $status = '';

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
        if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
}

unless( $rpt_dates = aff_proc::rptDates($id) ){ exit; }

my $mech = WWW::Mechanize->new();
$mech->get('https://affiliates.befree.com/');
$mech->submit_form(
		fields	=> {
				username	=> 'dhsclub',
				password	=> 'june1997'
			}
		);
$mech->follow_link(text_regex => qr/Transaction File/);
$mech->follow_link(text_regex => qr/Current File/);
$mech->save_content($rpt_file);
$status = $mech->status;

my $log_msg = '[downloader]: Download of ' . $id . " report: $status";
aff_proc::printLog($log_msg);
unless ($mech->success) {print "$status\n";}
