package vid1762;
use strict;

sub Process {
	my ($x, $trans, $row, $db, $return) = @_;
	my ($vip_date) = $db->selectrow_array("SELECT vip_date FROM members WHERE id= $trans->{id}");
	# abort unless we got a date
	return 'Failed to obtain vip_date' unless $vip_date;
	# we have to treat positive transactions differently than negative ones :-(
	if ($trans->{amount} > 0){
		my ($rv) = $db->selectrow_array("SELECT CASE WHEN '$vip_date'::DATE + 30 > ? THEN 1 ELSE 0 END AS test", undef, $trans->{trans_date});
		# if they didn't make the purchase within 30 days of their upgrade, process normally
		unless ($rv){
			# this means 'do not record' as an exception
			$return->{do_not_record} = 1;
			return 1;
		}
		my ($cnt) = $db->selectrow_array("SELECT COUNT(*) FROM $main::TRANS_TBL WHERE COALESCE(void,FALSE) != TRUE AND vendor_id=1762 AND id=$trans->{id} AND trans_date BETWEEN '$vip_date' AND '$vip_date'::DATE + 30");
		# if they already have a sale, then we simply record this one normally
		if ($cnt > 0){
			$return->{do_not_record} = 1;
			return 1;
		}
		# otherwise we will record this with no payout amount
		# it will be handled manually after 30 days with a direct rebate
		$trans->{discount_amt} = 0;
		# make sure we leave this one 'flagged' as needing further attention
		$return->{mark_processed} = 'FALSE';
		# send the email too
		$return->{email} = 1;
	} else {
		# are we within 60 days (30 days from upgrade and 30 day wait)
		my ($rv) = $db->selectrow_array("SELECT CASE WHEN '$vip_date'::DATE + 60 > ? THEN 1 ELSE 0 END AS test", undef, $trans->{trans_date});
		unless ($rv){
			$return->{do_not_record} = 1;
			return 1;
		}

		# get the earliest sale since it is the only one that could be an exception anyway
		$rv = $db->selectrow_hashref("SELECT amount, trans_date FROM $main::TRANS_TBL WHERE vendor_id=1762 AND id=$trans->{id} AND trans_date BETWEEN '$vip_date' AND '$vip_date'::DATE + 30 ORDER BY trans_date LIMIT 1");

		# if we didn't find a sale (like when support didn't enter it) then we will have to flag it
		# by skipping any further checks
		if ($rv){
			# now we'll check if our negative is within 30 days of the initial sale
			# if  not we're done (I'm using SQL rather than going through the gobbledy gook of figuring it in perl)
			my ($ck) = $db->selectrow_array("SELECT CASE WHEN '$trans->{trans_date}' > '$rv->{trans_date}'::DATE +30 THEN 1 ELSE 0 END AS test");

			# if our amounts don't correspond, then this return must not be on that sale
			if ($trans->{amount} != ($rv->{amount} * -1) || $ck){
				$return->{do_not_record} = 1;
					return 1;
			}
			# check to see if there are any other negative sales since then that match the transaction
			($rv) = $db->selectrow_array("SELECT COUNT(*) FROM $main::TRANS_TBL WHERE vendor_id=1762 AND id=$trans->{id} AND amount= ($rv->{amount} * -1) AND trans_date BETWEEN '$rv->{trans_date}' AND '$trans->{trans_date}'");

			# if we came up with a  matching negative, then this one must be against another sale
			if ($rv){
                     	   $return->{do_not_record} = 1;
	                        return 1;
			}
		}
		# otherwise we will record this with no payout amount
		$trans->{discount_amt} = 0;
		# make sure we leave this one 'flagged' as needing further attention
		$return->{mark_processed} = 'FALSE';
		# send the email too
		$return->{email} = 1;
	}
	# always end with a 1 so that we know we finished normally
	1;
}
1;

