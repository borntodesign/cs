package vid280;
###### special handling for Walmart sales
###### we cannot perform redistribution of commissions so we give the commissionable part of the transaction to DHS Club Kids
###### the sale goes to the original party
###### created: 06/14/07	Bill MacArthur

use strict;

sub Process {
	my ($x, $trans, $row, $db, $return) = @_;
	###### the member ID of the DHS Club Kids (AG) should be in the exception record
	###### create an soa transaction for DHS Club Kids
	my ($trans_id) = $db->selectrow_array("SELECT * FROM debit_trans_complete(?,1100,?,?,NULL,NULL)",
		undef, $x->{assign_to}, $trans->{receivable}, "Walmart commission proceeds from sale by member: $trans->{id}");

	$trans->{description} = "Commissions assigned to $x->{assign_to} (DHS Club Kids)\nsoa trans: $trans_id\n$trans->{description}";
	$trans->{discount_amt} = 0;
	$return->{do_not_record} = 1;
	#$return->{email} = 1;
	# always end with a 1 so that we know we finished normally
	1;
}
1;

