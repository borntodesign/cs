#!/usr/bin/perl -w 

##
## za_parse.pl - This is basically a modified version of aff_proc::parseText
## and is used for parsing reports from Zanox.
##
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc/');
use aff_proc;

my $aff_id = 'ZA';
my $file_type = 'txt';
my $cols_used = ['Remark', 'BLANK', 'Dated on', 'Net amount (local currency)', 'Commission', 'SubAffiliateID'];
my $delimiter = ';';
my $head_row = 0;
my $foot_start_row = '';

my @heads = ();
my %this_row = ();
my @this_row = ();
my $i = 0;
my $j = 0;
my $sale = '';
my $commission = '',

my $arch_dir = '/home/httpd/scripts/aff_proc/archive/';
my $rpt_dir = '/home/httpd/scripts/aff_proc/reports/';
my $rpt_file = $rpt_dir . $aff_id . '.*.' . $file_type;
my $end_date= time() - 86400;

$rpt_file = `/bin/ls $rpt_file`;
chomp $rpt_file;
if ($rpt_file =~ /^.*?\.(\d+)\..*$/) {$end_date = $1;}

unless (open(REPORT, $rpt_file)) {
	my $errstr = "[parser]: $aff_id report does not exist.";
	aff_proc::printLog($errstr);
	die $errstr;
}

my $outfile = ($rpt_file . '.' . time());
open(OUT, ">$outfile");

while(<REPORT>){
	chomp;
	s/\r$//;
	if ($foot_start_row && /^$foot_start_row/) {last;}
	unless($i > $head_row){
		if ($i < $head_row){
			$i++;
			next;
		}else{
			@heads = split /$delimiter/;
		}
	}
	@this_row = split /$delimiter/;
	foreach (@heads) {
		$this_row{$_} = shift @this_row;
	}
	foreach ( @{$cols_used} ){
		if ($_ eq 'BLANK') {print OUT "\t"; next;}
		unless(defined $this_row{$_} || $i != $head_row){
			my $errstr = "[parser]: Column: \"$_\" isn\'t in $rpt_file.";
			aff_proc::printLog($errstr);
			close OUT;
			close REPORT;
			print $errstr;
			exit;
		}
		if ($_ eq 'Remark'){
			$this_row{$_} =~ s/^(.*?)\:.*$/$1/;
		}
		## Month needs to be swapped with day for Postgres
		if ($_ eq 'Dated on') { $this_row{$_} =~ s/^(.*?)\.(.*?)\.(.*)$/$2\.$1\.$3/;}
		## European money figures use a comma as the decimal and vice-versa... WHY?
		if ($_ eq 'Net amount (local currency)') {
			$sale = $this_row{$_};
			$sale =~ s/^.+\s(.{3})$/$1/;
			$this_row{$_} =~ s/\s.{3}$//;
			$this_row{$_} =~ s/\.//;
			$this_row{$_} =~ s/,/\./;
			if ($this_row{'Status'} eq 'Refused') {$this_row{$_} = $this_row{$_} * -1;}
		}elsif ($_ eq 'Commission') {
			$commission = $this_row{$_};
			$commission =~ s/^.+\s(.{3})$/$1/;
			$this_row{$_} =~ s/\s.{3}$//;
                        $this_row{$_} =~ s/\.//;
			$this_row{$_} =~ s/,/\./;
			if ($this_row{'Status'} eq 'Refused') {$this_row{$_} = $this_row{$_} * -1;}
		}
		print OUT "$this_row{$_}\t";
	}
	%this_row = ();
	print OUT "$aff_id\t";
	if ($i == $head_row) {print OUT "Sale Currency\tComm. Currency\n"; $i++; next;}
	print OUT "$sale\t";
	print OUT "$commission\n";
	$i++;
}
close OUT;
close REPORT;
`/bin/mv $rpt_dir$aff_id.*.out $arch_dir`;
my $done_outfile = ($rpt_file . '.' . time() . '.' . 'out');
`/bin/mv $outfile $done_outfile`;
`/bin/echo "parsed\t$end_date" > $rpt_dir$aff_id`;
`/bin/rm $rpt_file`;
aff_proc::printLog('[parser]: ' . $aff_id . ' successfully parsed.');
