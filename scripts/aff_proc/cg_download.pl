#!/usr/bin/perl -w

##
## cg_download.pl - downloads the report from Clix Galore.
##
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use WWW::Mechanize;

my $rpt = '';
my $rpt_dates  = '';
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
my $id = 'CG';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400) . '.txt';
my $content = '';
my %trans_status = (	0	=> 'cancelled',
			1	=> 'confirmed',
			2	=> 'pending'
		);

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
	if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
} 

unless( $rpt_dates = aff_proc::rptDates($id) ){ exit; }

## This is for testing.
##########
#$rpt_dates->{rpt_start_month} = 0;
#$rpt_dates->{rpt_start_day} = 1;
##########

$rpt_dates->{rpt_start_month} = sprintf("%02d", ++$rpt_dates->{rpt_start_month});
$rpt_dates->{rpt_end_month} = sprintf("%02d", ++$rpt_dates->{rpt_end_month});

my $mech = WWW::Mechanize->new();
$mech->get('https://www.clixgalore.com/memberlogin.aspx');
$mech->submit_form(
		button	=> 'cmd_login',
		fields	=> {
				txt_UserName	=> 'orders@dhs-club.com',
				txt_Password	=> 'june1997'
			}
		);
$mech->get('http://www.clixgalore.com/AffiliateTransactionSentReport.aspx');
$mech->submit_form(
	fields  => {
		dd_period	=> 6
		}
	);

## The form only allows reports for one of "cancelled", "confirmed", or "pending".
## So, we need to submit it 3 times, and concatenate the results into one file.
for (my $i=0;$i<2;++$i) {
	$mech->submit_form(
			button  => 'cmd_report',
        	        fields  => {
					dd_status		=> $i,
					dd_period		=> 6,
					dd_specific_from_day	=> $rpt_dates->{rpt_start_day},
					dd_specific_from_month	=> $rpt_dates->{rpt_start_month},
					dd_specific_from_year	=> $rpt_dates->{rpt_start_year},
					dd_specific_to_day	=> $rpt_dates->{rpt_end_day},
					dd_specific_to_month	=> $rpt_dates->{rpt_end_month},
					dd_specific_to_year	=> $rpt_dates->{rpt_end_year}
	                        }
        	        );
	$mech->follow_link(text_regex => qr/Export Transaction/);
	$content = $mech->content;

	## Change the "id" attribute in the table tags so the xml parser
	## will see them as separate entities.
	$content =~ s/dg_Transactions/$trans_status{$i}/;

	## If we get a plain old web page (instead of just a table),
	## it means there are no results for the query, so it needs to
	## be skipped.
	unless ($content =~ /<html>.*<\/html>/s) {$rpt .= $content;}
	my $status = $mech->status;
	$mech->back;
	
	my $log_msg = '[downloader]: Download of ' . $id . " $trans_status{$i}" . " report: $status";
	aff_proc::printLog($log_msg);
	unless ($mech->success) {print "$status\n";}
	$content = '';
}

open (RPT, ">$rpt_file");
print RPT $rpt;
close RPT;

