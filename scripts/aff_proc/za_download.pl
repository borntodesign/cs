#!/usr/bin/perl -w
##
## za_download.pl - Zanox downloader
## originally written by: Shane Partain
## last modified:

use strict;
use WWW::Mechanize;
use lib '/home/httpd/scripts/aff_proc';
use aff_proc qw/$RPT_DIR $HOME_DIR/;

my %currency = (
		chf	=> 30,
		eur	=> 1,
		gbp	=> 52,
		jpy	=> 76,
		sek	=> 129,
		sgd	=> 130,
		usd	=> 3
		);
my $id = 'ZA';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400);
my $full_rpt = $rpt_file . '.txt';
my $rpt ='';
my $rpt_dates = '';
my $log_msg = '';
my $status = '';

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
        if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
}

unless ($rpt_dates = aff_proc::rptDates($id)) {exit;}
my $rpt_start_date = $rpt_dates->{rpt_start_date_str};
my $rpt_end_date = $rpt_dates->{rpt_end_date_str};
$rpt_start_date =~ s/^(\d{4})(\d{2})(\d{2})$/$3\.$2\.$1/;
$rpt_end_date =~ s/^(\d{4})(\d{2})(\d{2}$)/$3\.$2\.$1/;

$rpt_start_date='12.06.2007';
$rpt_end_date ='16.06.2007';

my $mech = WWW::Mechanize->new();

$mech->get('http://www.zanox.com/us/');
$mech->follow_link(tag => 'frame', url_regex => qr/goto\/home/);

$mech->submit_form(
	form_number => 2,
	button	=> 'b_next',
	fields      => {
		t_login_user_name => 'mallsales@dhs-club.com',
		t_login_password => 'june1997',
	}
);

$mech->follow_link(text_regex => qr/statistic/i);
$mech->follow_link(text_regex => qr/advanced/i);
exit unless CkOptions();

## They generate a separate report for each type of currency. So, we get all of them.
foreach (keys %currency)
{
#next unless $_ eq 'eur';
	$mech->submit_form(
		button	=> 'b_next',
         	fields => {
			d_accounting_currency => $currency{$_}
		}
	);
	$mech->follow_link(text_regex => qr/details/i, n => 2);
	$mech->untick('c_date_create', 'on');
	$mech->submit_form(
		button => 'b_save',
		fields => {
			t_date_from	=> $rpt_start_date,
			t_date_to	=> $rpt_end_date,
			c_date_edit	=> 'on',
			d_screen_file	=> 1
		}
	);

	$status = $mech->status;
	$log_msg = '[downloader]: Download of ' . $id . " $_ report: $status";
	aff_proc::printLog($log_msg);
	unless ($mech->success)
	{
		###### I don't think we want a partial download to look like a success
		die $log_msg;
#		next;
	}

#	print utf8::is_utf8($mech->content) ? "Flagged UTF8\n" : "NOT flagged utf8\n";
#	print utf8::valid($mech->content) ? "Valid UTF8\n" : "Invalid utf8\n";
	$rpt .= DoctorContent(); #$mech->content;
#	print utf8::is_utf8($rpt) ? "Flagged UTF8\n" : "NOT flagged utf8\n";
#	print utf8::valid($rpt) ? "Valid UTF8\n" : "Invalid utf8\n";

	for (my $i=0; $i<3; $i++) { $mech->back; }
}

# Not sure if it's a good idea to use echo, but it seems to be working.
#`/bin/echo \'$rpt\' > $rpt_file`;
open (DL_RPT, ">$rpt_file") or die "Failed to open $rpt_file for writing\n";
print DL_RPT $rpt;
close DL_RPT;

#open (DL_RPT, $rpt_file) or die "Failed to open $rpt_file for reading\n";
open (FULL_RPT, ">$full_rpt") or die "Failed to open $full_rpt for writing\n";
my $i = 0;

## Since this file is a concatenation of several reports, the
## extraneous garbage rows need to be removed.
#while (<DL_RPT>)
foreach (split /\n/, $rpt)
{
	++$i;
	next unless $_;
	if ( $i > 1 && ( /^Status/ ||
			/^Transaction/ ||
			/^User/ ||
			/^Advertising/ ||
			/^From/ ||
			/^until/ ||
			/^\n/ )) {
		next;
	}else{
		print FULL_RPT $_, "\n";
	}
}
#close DL_RPT;
close FULL_RPT;
#`/bin/rm $rpt_file`;

exit;

sub CkOptions
{
#open (MYTMP, ">$HOME_DIR" . 'test.out')||die "failed to open tmp file\n";
#print MYTMP $mech->content;
#return undef;
	my $err = '';
	###### as of 06/20/07, the link that was followed prior to calling this routine produced a page with the accounting currency options
	unless ( my ($w) = $mech->content =~ m#<select.+?name="d_accounting_currency".+?>(.+?)</select>#isg)
	{
		$err = 'Failed to obtain the d_accounting_currency control from the page';
		print $mech->save_content($HOME_DIR . 'test.out');
	} else {
		my @cnt = $w =~ m#<option.+?>#isg;
		if (scalar @cnt != scalar keys %currency){
			$err = "Apparently the number of currency options has changed.\n";
			$err .= "The script may need to be adjusted. Below are the options parsed:\n";
			$err .= "$_\n" foreach @cnt;
		} else {
			foreach my $opt (@cnt){
				my ($match) = $opt =~ m#value="(\d+?)"#i;
				unless (grep $_ eq $match, values %currency){
					$err .= "A currency option has changed. The script may need to be adjusted.\n";
					$err .= "The unrecognized option: $opt\n";
				}
			}
		}
	}

	if ($err){
		print "$err\n";
		aff_proc::printLog($err);
		return undef;
	}
	return 1;
}

sub DoctorContent
{
	#return $mech->content;
	my $tmp = $mech->content;
	###### they begin the report with an FEFF or is it FFFE multibyte character... arghh
	$tmp =~ s/^.{2}//;

	###### this outfit is a PITA
	###### it appears that their concept of utf8 is using multibyte characters
	###### regardless of whether necessary or not
	###### in other words, each character is followed by a NULL character
	$tmp =~ s/\x00//g;

	###### \r are included only on the actual data lines... not on the header or footer lines... go figure
	$tmp =~ s/\r\n/\n/g;
	return $tmp;
}

