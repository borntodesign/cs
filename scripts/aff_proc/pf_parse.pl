#!/usr/bin/perl

##
## pf_parse.pl - Parses the spreadsheet from Performics.
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc/');
use aff_proc;

use Spreadsheet::ParseExcel;

my $aff_id = 'PF';
my $file_type = 'xls';
my $row = '';
my $col = '';
my $cell = '';
my $head_row = 2;
my @cols_used = ('Advertiser Name', 'BLANK', 'Date', 'Amount', 'Publisher Comm.', 'Publisher Member ID');
my %col_nums = ();
my $excel = '';


my $arch_dir = '/home/httpd/scripts/aff_proc/archive/';
my $rpt_dir = '/home/httpd/scripts/aff_proc/reports/';
my $rpt_file = $rpt_dir . $aff_id . '.*.' . $file_type;
my $end_date= time() - 86400;

$rpt_file = `/bin/ls $rpt_file`;
chomp $rpt_file;
if ($rpt_file =~ /^.*?\.(\d+)\..*$/) {$end_date = $1;}

$excel = Spreadsheet::ParseExcel::Workbook->Parse($rpt_file);
unless ( defined @{$excel->{Worksheet}} ) {
        my $errstr = "[parser]: $aff_id report does not exist.";
        aff_proc::printLog($errstr);
        die $errstr;
}

my $outfile = ($rpt_file . '.' . time());
open(OUT, ">$outfile");

foreach my $sheet (@{$excel->{Worksheet}}) {
	foreach $row ($sheet->{MinRow} .. $sheet->{MaxRow}) {

		## Skip the rows before the header, and the last row is just totals.
		if ( ($row < $head_row) || ($row == $sheet->{MaxRow}) ) { next; }

		## We use the header row to make a hash of column numbers/column names.
		if ($row == $head_row) {
			foreach $col ($sheet->{MinCol} .. $sheet->{MaxCol}) {
				$cell = $sheet->{Cells}[$row][$col];
				foreach (@cols_used){
					if ($_ eq $cell->{Val}) {
						$col_nums{$_} = $col;
					}
				} 
			}

			## And then make sure all the columns we need are there.
			my @col_nums = keys %col_nums;
			unless ($#col_nums == ($#cols_used - 1)){ # 2nd col is blank so the sizes won't match
				aff_proc::printLog('[parser]: ' . $aff_id . ' report is missing a column.');
				exit;
			}
		}

		## This writes everything to the outfile.
		foreach (@cols_used){
			if ($_ eq 'BLANK') {print OUT "\t"; next;}
			$cell = $sheet->{Cells}[$row][$col_nums{$_}];
			print OUT "$cell->{Val}\t";
		}
		print OUT "$aff_id\t";
		print OUT "\n";
	}
}
close OUT;
`/bin/mv $rpt_dir$aff_id.*.out $arch_dir`;
my $done_outfile = ($rpt_file . '.' . time() . '.' . 'out');
`/bin/mv $outfile $done_outfile`;
`/bin/echo "parsed\t$end_date" > $rpt_dir$aff_id`;
`/bin/rm $rpt_file`;
aff_proc::printLog('[parser]: ' . $aff_id . ' report successfully parsed.');
