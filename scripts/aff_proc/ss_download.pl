#!/usr/bin/perl -w

##
## ss_download.pl - Sharasale report downloader.
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use WWW::Mechanize;

my $rpt = '';
my $rpt_dates  = '';
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
my $id = 'SS';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400) . '.txt';

# Get rid of old reports.
opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
        if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
}

unless( $rpt_dates = aff_proc::rptDates($id) ){ exit; }
$rpt_dates->{rpt_start_day} = sprintf("%02d", $rpt_dates->{rpt_start_day});
$rpt_dates->{rpt_start_month} = sprintf("%02d", ++$rpt_dates->{rpt_start_month});
$rpt_dates->{rpt_end_day} = sprintf("%02d", $rpt_dates->{rpt_end_day});
$rpt_dates->{rpt_end_month} = sprintf("%02d", ++$rpt_dates->{rpt_end_month});

## This one doesn't have the headers in the file, for some reason.
$rpt = "transID|userID|merchantID|transdate|transamount|commission|comment|voided|pendingdate|locked|affcomment|ubannerpage|dateofreversal|dateofclick|timeofclick\n";

my $mech = WWW::Mechanize->new();
$mech->get('http://www.shareasale.com/a-login.cfm');
$mech->submit_form(
		fields	=> {
				username	=> 'dhsclub',
				password	=> 'june1997'
			}
		);
$mech->get('http://www.shareasale.com/a-report-new.cfm');
$mech->submit_form(
                fields  => {
			firstday      	=> $rpt_dates->{rpt_start_day},
			firstmonth	=> $rpt_dates->{rpt_start_month},
			firstyear	=> $rpt_dates->{rpt_start_year},
                        secondday      	=> $rpt_dates->{rpt_end_day},
                        secondmonth	=> $rpt_dates->{rpt_end_month},
                        secondyear	=> $rpt_dates->{rpt_end_year},
                        merchant	=> 'All'
                        }
                );
$mech->follow_link(text_regex => qr/download/);
$rpt .= $mech->content;
open (RPT, ">$rpt_file");
print RPT $rpt;
close RPT;

my $status = $mech->status;
my $log_msg = '[downloader]: Download of ' . $id . " report: $status";
aff_proc::printLog($log_msg);
unless ($mech->success) {print "$status\n";}
