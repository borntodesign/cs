#!/usr/bin/perl -w
##
## ls_download.pl - Gets the report for Link Share.
## originally written by: Shane Partain
## last modified: 07/30/08	Bill MacArthur

use strict;
use WWW::Mechanize;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc qw($RPT_DIR $HOME_DIR);

my $id = 'LS';
my $rpt_file = $RPT_DIR . $id . '.' . (time() - 84600) . '.txt';
my $rpt_dates = '';
my $query_params = '';
#my $rpturl = 'http://www.linkshare.com/areport/u1field?';
my $rpturl = 'http://cli.linksynergy.com/cli/publisher/reports/downloadReport.php?';
my $mech = WWW::Mechanize->new(autocheck => 1);
unless ($rpt_dates = aff_proc::rptDates($id)) {exit;}

###### these dates take the form of yyymmdd ie. '20070609'
my %query_params = (
	bdate   => $rpt_dates->{rpt_start_date_str},
	edate   => $rpt_dates->{rpt_end_date_str},
	cuserid => 'dhsclub',
	cpi => 'dhsc0697',
	eid => 169689
# the above eid doesn't seem to be needed either, but they included it with the revised URL
#eid     => 'sDSKt0GP6Pc'	Shane had included this one, but it apparently is not needed
);

###### first we need to be sure they haven't expanded/changed their network options
###### as of 06/13/07 we have three using the parameter of 'nid'
# 1 = Network, 3 = UK, 5 = CA
# we'll call this routine and if we make it back, we can proceed normally
#exit unless CkOptions();
# as of 08/13/09 logging in is no longer needed -AND- neither is selecting the "network"

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
        if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
}

#my $rpt_start_date = '20070609'; #$rpt_dates->{rpt_start_date_str};
#my $rpt_end_date = $rpt_dates->{rpt_end_date_str};

foreach (keys %query_params) {
        $query_params .= $_ . '=' . $query_params{$_} . '&';
}
$rpturl .= $query_params;

# this works out to something like this:
#http://www.linkshare.com/areport/u1field?cuserid=dhsclub&cpi=dhsc0697&bdate=20090915&edate=20090915

###### now loop through our different reports and string 'em together into one
my $cnt = 0;
my $rpt = '';
#foreach my $nid (1,3,5){
#	$mech->get($rpturl . "nid=$nid");
	$mech->get($rpturl);
#	my $log_msg = "[downloader]: Download of $id report using nid=$nid: " . $mech->status;
	my $log_msg = "[downloader]: Download of $id report: " . $mech->status;
#	$log_msg .= "\n$rpturl" . "nid=$nid";
	aff_proc::printLog($log_msg);
	unless ($mech->success) {
		print "$log_msg\nAborting download file creation for $id.\n";
		exit;
	}

	###### get a reference back
	my $pg = AddCurrCodes();
	###### we want to remove the header line on all the other downloads besides the first
	$$pg =~ s#.*\n## if $cnt++ > 0;
	$rpt .= $$pg;
#}

if (open(OUT, ">$rpt_file")){
	print OUT $rpt;
	close OUT;
} else {
	my $err = "Failed to open $rpt_file for writing";
	aff_proc::printLog($err);
	print "$err\n";
}
exit;

sub AddCurrCodes
{
	my @rows = split /\n/, $mech->content;
	# the first line of our page should contain the header line
	# which should have the currency symbol embedded in the sale and commission columns
	# like:  Sales(�) 	 Quantity 	 Commissions(�)
	# or:  Sales($) 	 Quantity 	 Commissions($)
	###### at this point we will only look for GBP or USD, OK maybe Euro just in case
	$rows[0] =~ m#Sales\((\$|\�|\�)\).*Commissions\((\$|\�|\�)\)#;
	my %curr_map = ('$'=>'USD', '�'=>'GBP', '�'=>'EUR');
	my $sale_curr = $curr_map{$1};
	my $comm_curr = $curr_map{$2};
	unless ($sale_curr && $comm_curr){
		my $err = "Found an unanticipated currency code in a download.\nHere is the first line:\n$rows[0]\n";
		aff_proc::printLog($err);
		print "$err\n";
		print "\nHere is the whole section:\n" , $mech->content, "\n---\n";
		return undef;
	}
	my $rv = "$rows[0]\tSale Currency\tCommission Currency\n";
	for (my $x=1; $x < scalar @rows; $x++)
	{
		$rv .= "$rows[$x]\t$sale_curr\t$comm_curr\n";
	}
	return \$rv;
}

sub CkOptions
{
	my $err = '';
	$mech->get( "https://ssl.linksynergy.com/php-bin/common/login.shtml?cuserid=$query_params{cuserid}&cpi=$query_params{cpi}" );
	# 07/30/08 trying http since the secure version doesn't seem to work now - maybe it was a simple dns problem
	#$mech->get( "http://ssl.linksynergy.com/php-bin/common/login.shtml?cuserid=$query_params{cuserid}&cpi=$query_params{cpi}" );
	###### as of 06/13/07, the above login produces a simple page with a meta tag redirector
	unless ( $mech->follow_link( n => 1 ) && (my ($w) = $mech->content =~ m#<select.+?name="nid".+?>(.+?)</select>#isg) )
	{
		$err = 'Failed to obtain the Network control from the post-login page';
		print $mech->save_content($HOME_DIR . 'test.out');
	} else {
		my @cnt = $w =~ m#<option.+?>#isg;
		if (scalar @cnt != 4){
			$err = "Apparently the number of Network options has changed.\n";
			$err .= "The script may need to be adjusted. Below are the options parsed:\n";
			$err .= "$_\n" foreach @cnt;
		} else {
			foreach my $opt (@cnt){
				my ($match) = $opt =~ m#value="(\d+?)"#i;
				unless (grep $_ eq $match, (1,3,5,54)){
					$err .= "A Network option has changed. The script may need to be adjusted.\n";
					$err .= "The unrecognized option: $opt\n";
				}
			}
		}
	}

	if ($err){
		print "$err\n";
		aff_proc::printLog($err);
		return undef;
	}
	return 1;
}

# 10/19/07 had to change the number of expected options since they added one
# 04/15/08 had to change to http from https for the report download protocol: as of today, it seems there is no SSL for that domain (checked in browser as well)
# 07/30/08 trying a change from https to http for the ssl. URL
