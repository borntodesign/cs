#!/usr/bin/perl -w

##
## ko_parse.pl - Parses the Kolimbo CSV and saves the pertinent data to
## a tab file.
##

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
require Text::CSV_XS;

my $aff_id = 'KO';
my $file_type = 'txt';
my $cols_used = [' Merchant', 'BLANK', ' Sale Date', ' Sale Amount', ' Commission', ' SubID'];
my $head_row = 0;

my @heads = ();
my %this_row = ();
my @this_row = ();
my $i = 0;
my $j = 0;

my $arch_dir = '/home/httpd/scripts/aff_proc/archive/';
my $rpt_dir = '/home/httpd/scripts/aff_proc/reports/';
my $rpt_file = $rpt_dir . $aff_id . '.*.' . $file_type;
my $end_date= time() - 86400;
my $fixed_rpt = '';

$rpt_file = `/bin/ls $rpt_file`;
chomp $rpt_file;
if ($rpt_file =~ /^.*?\.(\d+)\..*$/) {$end_date = $1;}

unless (open(REPORT, $rpt_file)) {
	my $errstr = "[parser]: $aff_id report does not exist.";
	aff_proc::printLog($errstr);
	print $errstr;
	exit;
}

# This file has unusual line breaks (CR only), and has to be fixed first
while (<REPORT>){ 
	s/\r/\n/g;
	$fixed_rpt = $_;
	last;
}
close REPORT;
unlink $rpt_file;
open(REPORT, ">$rpt_file");
print REPORT $fixed_rpt;
close REPORT;

my $csv = Text::CSV_XS->new;
my $outfile = ($rpt_file . '.' . time());
open(OUT, ">$outfile");
open(REPORT, $rpt_file);

while (<REPORT>){
	$csv->parse($_);
	unless($i > $head_row){
		if ($i < $head_row){
			$i++;
			next;
		}else{
			@heads = $csv->fields;
		}
	}
	@this_row = $csv->fields;
	foreach (@heads) {
		$this_row{$_} = shift @this_row;
	}
	# Only use records for approved and pending sales.
	if ($i != $head_row){
		unless ($this_row{' Type'} eq 'Sale') {next;}
		if ($this_row{' Status'} eq 'Denied') {next;}
	}
	foreach ( @{$cols_used} ){
		if ($_ eq 'BLANK') {print OUT "\t"; next;}
		unless(defined $this_row{$_} || $i != $head_row){
			my $errstr = "[parser]: Column: \"$_\" isn\'t in $rpt_file.";
			aff_proc::printLog($errstr);
			close OUT;
			close REPORT;
			print "$errstr\n";
			exit;
		}
		# Skip rows where the first column is empty. This really isn't necessary
		# for this file.
		if ($_ eq $heads[0] && !$this_row{$_}) {last;} 
		$this_row{$_} .= "\t";
		print OUT $this_row{$_};
	}
	if (!$this_row{$heads[0]}) {next;}
	print OUT "$aff_id\n";
	%this_row = ();
	$i++;
}
close OUT;
close REPORT;
`/bin/mv $rpt_dir$aff_id.*.out $arch_dir`;
my $done_outfile = ($rpt_file . '.' . time() . '.' . 'out');
`/bin/mv $outfile $done_outfile`;
`/bin/echo "parsed\t$end_date" > $rpt_dir$aff_id`;
`/bin/rm $rpt_file`;

