#!/usr/bin/perl -w
## rpt_download.pl - This script uses the aff_proc::download sub for downloading
## linkshare affiliate reports that only require a simple login and then a download
## (two requests). It uses an LWP UA, and that works here, but WWW::Mechanize is better
## for more complex downloads.
## 
## originally written by:	Shane Partain
## last modified: 05/23/12	Bill MacArthur

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc qw($RPT_DIR);

my @rpts = ();
my %req_params = ();
my %query_params = ();
my $rpt_dates = '';
my $req_rv = '';


##############################
## Trade Doubler
##############################

$req_params{TD} = {
	id => 'TD',
	report_type => 'txt',
	login_params =>
#		['https://www.tradedoubler.com/pan/login', 'j_username=sales@dhs-club.com&j_password=dhsc0697']
		['https://reports.tradedoubler.com/pan/login', 'j_username=sales@dhs-club.com&j_password=dhsc0697']
};

## If the rptDates sub returns undef, then this one has already been downloaded.
##
unless( $rpt_dates = aff_proc::rptDates($req_params{TD}{id}) ){
	$req_params{TD}{skip} = 1;
}else{
	$rpt_dates->{rpt_end_date_str} =~ s/^\d{2}(\d{2})(\d{2})(\d{2})$/$3\/$2\/$1/;
	$rpt_dates->{rpt_start_date_str} =~ s/^\d{2}(\d{2})(\d{2})(\d{2})$/$3\/$2\/$1/;

	my $query_params =
		"startDate=$rpt_dates->{rpt_start_date_str}" .
		"&endDate=$rpt_dates->{rpt_end_date_str}" .
		'&reportName=aAffiliateEventBreakdownReport' .
		'&columns=programName' .				# Merchant Name
		'&columns=programId' .				# Merchant ID
		'&columns=timeOfEvent' .				# Transaction Date
		'&columns=epi1' .					# subid (member ID)
		'&columns=affiliateCommission' .			# our commission
		'&columns=link' .
		'&columns=orderNR' .					# the order number
		'&columns=orderValue' .				# the transaction amount
		'&columns=pendingStatus' .				# we don't use this, but we may need to at some point
		'&showAdvanced=false' .
		'&dateType=1' .
		'&reportTitleTextKey=REPORT3_SERVICE_REPORTS_AAFFILIATEEVENTBREAKDOWNREPORT_TITLE' .
		'&currencyId=USD' .
		'&format=CSV';

	$req_params{TD}{download_params} =
#		['https://www.tradedoubler.com/pan/aReport3Internal.action', $query_params];
		['https://reports.tradedoubler.com/pan/aReport3Internal.action', $query_params];
}

########################
## Commission Junction
########################

$req_params{CJ} = {
	'skip'=>1,
	id => 'CJ',
	report_type => 'txt',
	login_params =>
		['https://members.cj.com/member/foundation/memberlogin.do', 'uname=orders@dhs-club.com&pw=dhsc0697']
};

## If the rptDates sub returns undef, then this one has already been downloaded.
##
if ($req_params{CJ}{skip})
{
	# do nothing
}
elsif (! ($rpt_dates = aff_proc::rptDates($req_params{CJ}{id})) )
{
	$req_params{CJ}{skip} = 1;
}else{
	%query_params = (    period             => 'range',
                        endday          => $rpt_dates->{rpt_end_day},
                        endmonth        => $rpt_dates->{rpt_end_month},
                        endyear         => $rpt_dates->{rpt_end_year},
                        startday        => $rpt_dates->{rpt_start_day},
                        startmonth      => $rpt_dates->{rpt_start_month},
                        startyear       => $rpt_dates->{rpt_start_year},
                        filterby        => '-1',
                        what            => 'commDetail',
                        actionname      => '0',
                        dtType          => 'post',
                        download        => 'tab'
                        );

	my $query_params = '';
	foreach (keys %query_params) {
	        $query_params .= $_ . '=' . $query_params{$_} . '&';
	}
	chop $query_params;
	$req_params{CJ}{download_params} =
#		['https://members.cj.com/member/254314/publisher/report/transaction.do', $query_params];
###### this apparently changed on 01/29/09
		['https://members.cj.com/member/publisher/report/transaction.do', $query_params];
}

##
## From here down does the actual download(s).
##

@rpts = @ARGV;
@rpts = keys %req_params unless @rpts;
foreach my $rptid (@rpts) {
	unless (grep $_ eq $rptid, ('TD','CJ')){
		print "$rptid is not in my list of defined affiliates\n";
		next;
	}
	next if $req_params{$rptid}{skip};
	$req_rv = aff_proc::download(
		$req_params{$rptid}{id},
		$req_params{$rptid}{report_type},
		$req_params{$rptid}{download_params},
		$req_params{$rptid}{login_params}
	);
	my $log_message = "[downloader]: Download of $req_params{$rptid}{id} report: $req_rv";
	aff_proc::printLog( $log_message );
	if ($req_rv !~ /200/) {
	        print "$log_message\n";
	}
}

###### 06/12/07 basically cosmetic tweaks and making use of the imported vars
###### 01/29/09 tweaked the URL for the CJ download report
###### 05/23/12 changed the URL for the TD report

