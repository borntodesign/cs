#!/usr/bin/perl -w
#
# CJ-Download-Parse.pl
# an independent unit to handle the current API and dataset provided by CJ
# we will download and process the data into a .out file for final processing by parse-aff-out.pl

use strict;
use LWP::UserAgent;
use XML::LibXML;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;

my $TIME = time();
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports';
my $RPT_FILE = "/home/httpd/scripts/aff_proc/reports/CJ.$TIME.xml";
my $aff_id = 'CJ';

# the rptDates routine was originally written to take the last parsed date and add a day to use for the start date
# that is fine for other reports like currently TD, but we need to start on the last parsed date as that date is the end boundary for the data provided by CJ now
# the rptDates routine was originally written to use the current date - 1 day for the end date
# however, we need to use the current date for the end date to get everything up to today as the end date is now exclusive with CJ
my $rpt_dates = aff_proc::rptDates('CJ', {'start_seed'=>0, 'end_seed'=>86400}) || die "Failed to obtain report dates";
#debug();
$rpt_dates->{'rpt_start_date_str'} = $rpt_dates->{'rpt_start_year'} .'-'. $rpt_dates->{'rpt_start_month'} .'-'. $rpt_dates->{'rpt_start_day'};
$rpt_dates->{'rpt_end_date_str'} = $rpt_dates->{'rpt_end_year'} .'-'. $rpt_dates->{'rpt_end_month'} .'-'. $rpt_dates->{'rpt_end_day'};
my $ua = LWP::UserAgent->new;

# Create a request
my $url = "https://commission-detail.api.cj.com/v3/commissions?date-type=posting&start-date=$rpt_dates->{'rpt_start_date_str'}&end-date=$rpt_dates->{'rpt_end_date_str'}";
my $req = HTTP::Request->new('GET' => $url);
$req->header('Authorization', '0083b10e6207835f1b322cd92f1be2dd45ff8a2733fb399725f515ebafcc9de9357a5ebdb23bc0d6b2ef54bc37938bcdc5084812946d1a025d27595a4d52325113/1aed20dbba5d8a1cad6dae1a8bc11f2d28cabea94a3cc442e691ccd6ed277c7f3c5cd1389b9d3cd286621f7667589eb1dfb807997a8e33b5347efeaf9d409001');

# Pass request to the user agent and get a response back
my $res = $ua->request($req);

# Check the outcome of the response
if ($res->is_success)
{
	aff_proc::printLog("CJ Download successful: $url");
	Process();
}
else
{
	print $res->status_line, "\n";
	aff_proc::printLog("CJ Download error: $url\n" . $res->status_line);
}

exit;

sub debug
{
	print "$_ : $rpt_dates->{$_}\n" foreach keys %{$rpt_dates};
	exit;
}

sub Process
{
	open(XML, ">$RPT_FILE") || die "Failed to open $RPT_FILE for writing";	
	print XML $res->content;
	
	my @collist = qw/advertiser-name cid event-date sale-amount commission-amount sid description sale-currency commission-currency/;
	my @rows = ();
	my %row_template = (
		'sale-currency' 		=> 'USD',
		'commission-currency'	=> 'USD'
	);
	
	my $parser = XML::LibXML->new();
	my $xmldoc = $parser->parse_string($res->content);
	foreach my $c ($xmldoc->findnodes('/cj-api/commissions/commission'))
	{
		my %row = %row_template;
		foreach my $node_name (qw/advertiser-name cid event-date sale-amount commission-amount sid/)
		{
			my $node = $c->findnodes("./$node_name");
			$row{ $node_name } = $node ? ${($node)}[0]->textContent() : '';
		}
		
		# we will concatenate these two values for our description
		my $node = $c->findnodes("./action-tracker-name");
		$row{'description'} = $node ? (${($node)}[0]->textContent() . ' : ') : '';
		$node = $c->findnodes("./original-action-id");
		$row{'description'} .= $node ? ${($node)}[0]->textContent() : '';
		
		$row{'event-date'} = substr($row{'event-date'}, 0, 10); # the dates they provide are really some kind of timestamp "2013-05-08T14:43:27-0700"
		push @rows, \%row;
	}
#	use Data::Dumper; print Dumper(\@rows);
	aff_proc::WriteOut($aff_id, \@collist, \@rows, {'rpt_file'=>$RPT_FILE, 'aff_file_contents'=>("parsed\t$TIME")});
}

__END__

aff_proc::printLog($log_msg);

###### our input (.out) file columns
# aff_code adv-name adv-id trans-date sale-amt comm-amt 'sid' 'description' sale-currency commission-currency
#    0         1       2        3        4        5       6         7             8                9

The current important schema elements of affiliate_transaction_errors is this:

  adv_name character varying,
  adv_id character varying,
  trans_date character varying,
  sale_amt character varying,
  comm_amt character varying,
  sid character varying,
  aff_code character(2),
  record_insertion_date timestamp without time zone DEFAULT now(),
  sale_currency character varying(5),
  commission_currency character varying(5),
  description character varying,
  corrected boolean DEFAULT false