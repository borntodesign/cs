#!/usr/bin/perl -w

##
## ko_download.pl - downloads the CSV report from Kolimbo
##
##
##

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use WWW::Mechanize;

my $rpt = '';
my $rpt_dates  = '';
my $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
my $id = 'KO';
my $rpt_file = $RPT_DIR . $id . '.' . (time()-86400) . '.txt';

opendir (DIR, $RPT_DIR);
my @files = readdir(DIR);
closedir DIR;
foreach (@files) {
	if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
} 

unless( $rpt_dates = aff_proc::rptDates($id) ){ exit; }
$rpt_dates->{rpt_start_month} =~ ++$rpt_dates->{rpt_start_month};
$rpt_dates->{rpt_start_year} =~ s/^.*(\d{2})$/$1/;
$rpt_dates->{rpt_end_month} =~ ++$rpt_dates->{rpt_end_month};
$rpt_dates->{rpt_end_year} =~ s/^.*(\d{2})$/$1/;

my $mech = WWW::Mechanize->new();
$mech->get('http://www.kolimbo.com/');
$mech->submit_form(
		fields	=> {
				id	=> 16427,
				pass	=> 'june1997'
			}
		);
$mech->follow_link(text_regex => qr/Transaction History/);
$mech->submit_form(
		form_number => 2,
                fields  => {
				start_day	=> $rpt_dates->{rpt_start_day},
				start_month	=> $rpt_dates->{rpt_start_month},
				start_year	=> $rpt_dates->{rpt_start_year},
				end_day		=> $rpt_dates->{rpt_end_day},
				end_month	=> $rpt_dates->{rpt_end_month},
				end_year	=> $rpt_dates->{rpt_end_year}
                        }
                );
$mech->follow_link(text_regex => qr/Export to Excel/);
$mech->follow_link(text_regex => qr/Click Here/);
$mech->save_content($rpt_file);

my $status = $mech->status;
my $log_msg = '[downloader]: Download of ' . $id . " report: $status";
aff_proc::printLog($log_msg);
unless ($mech->success) {print "$status\n";}
