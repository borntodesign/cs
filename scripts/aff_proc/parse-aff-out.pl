#!/usr/bin/perl -w
###### parse-aff-out.pl
###### process pre-parsed affiliate downloads into member transactions
###### deployed: 05/01/07	Bill MacArthur
###### last modified: 12/23/15	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use Mail::Sendmail;
use SendErrorMail;

###### our input (.out) file columns
# aff_code adv-name adv-id trans-date sale-amt comm-amt 'sid' 'description' sale-currency commission-currency
#    0         1       2        3        4        5       6         7             8                9
my $EXPECTED_COL_CNT = 10;

###### we are taking 80% of the commission and paying it out in the discount_amt column of a transaction
my $PAYOUT_PCT = .8;

(my $PATH = $0) =~ s#(.*)(/.*)#$1#;

my $INPUT_DIR = 'reports';		###### where our input files will reside
my $ARCHIVE_DIR = 'archive';	###### where we will archive them when we are through
our $TRANS_TBL = 'transactions';
my $TRANS_TBL_SEQ = 'transactions_trans_id_seq';
my $EXCEPTIONS_TBL = 'affiliate_2vendor_exception_transactions';

my $BASE_SQL = "
	INSERT INTO $TRANS_TBL (
		trans_type,
		trans_id,
		description,
		vendor_id,
		affiliate_code,
		id,
		spid,
		membertype,
		trans_date,
		amount,
		discount_amt,
		cv_mult,
		reb_mult,
		comm_mult,
		receivable,
		currency_code,
		native_currency_amount
	) VALUES (2, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

###### Notice that this order mirrors the order of our BASE_SQL
###### we'll be working with a hash until we finally insert the data
###### we'll use a map() to create our list to feed into DBI
my @insert_order = qw(trans_id description vendor_id affiliate_code id spid membertype
	trans_date amount discount_amt cv_mult reb_mult comm_mult receivable
	currency_code native_currency_amount);

###### this could be the column order of an Exception record or an Error record
###### notice this mirrors the column order of the output files
my $Elist_order = 'aff_code, adv_name, adv_id, trans_date, sale_amt, comm_amt, sid, description, sale_currency, commission_currency';

###### a currency map for those affiliates that have chosen NOT to use standardized 3 character codes
my %CURRMAP = (
	'US' => 'USD',
	'NZ' => 'NZD',
	'AU' => 'AUD',
	'¥' => 'JPY',
	'€' => 'EUR',
	'£' => 'GBP'
);

###### if we need exchange rates, we'll look 'em up once and store them in here
my %XCHG = ();

my (@Cols, %hasx, %Advertisers, %VendorDetail) = ();
my $errors = '';

opendir (DIR, "$PATH/$INPUT_DIR") || die "Failed to open input directory: $PATH/$INPUT_DIR\n";
my $db = DB_Connect::DB_Connect('generic') || die "Failed to create a DB connection\n";
$db->{'AutoCommit'} = 0;
# instead we will rely on error reporting from cron
# as long as errors are rare we can fix up things as necessary
# if we have too many, then we'll go with the COMMIT at the end and just rerun the whole batch
###### we don't necessarily want to die on a transaction  error
$db->{'RaiseError'} = 0;

while (my $file = readdir DIR)
{
	next unless $file =~ /\.out$/;
	unless (open (INPUT, "$PATH/$INPUT_DIR/$file")){
#		print "Failed to open $PATH/$INPUT_DIR/$file\n";
		$errors .= "Failed to open $PATH/$INPUT_DIR/$file\n";
		next;
	}

	###### chop off our header line
	my $line = <INPUT>;
	chomp $line;
	@Cols = split /\t/, $line;
	###### at this time, 04/07, columns 9 & 10 (or 8 & 9 depending on how you look at it :> ) are optional
	if (scalar @Cols != $EXPECTED_COL_CNT){
		$errors .= "File problem: $PATH/$INPUT_DIR/$file\n";
		$errors .= "Column count: ${\scalar @Cols}\n";
		$errors .= "Incorrect column count based on header line:\n$line\n\n";
		next;
	}

	while ($line = <INPUT>)
	{
		chomp $line;
		###### ignore blank lines
		next unless $line;

	#	my @cols = split /\t/, $line;	###### this doesn't generate a 'column' for undef... ie: \t\t
		my @cols = ($line =~ /(.*?)\t|$/g);	###### this will give undef cols :-)
		###### 
		if (scalar @cols != $EXPECTED_COL_CNT)
		{
			$errors .= "data line column count error:\n$line\n" . scalar @cols . "\n";
			$errors .= "No record has been saved. To take care of this issue, use this data or refer to the file: $file\n";
		}
		else
		{
			###### take care of the unexpected leading and trailing whitespace
			@cols = map RmWs($_), @cols;

			###### take care of amounts that are not standard numerical values like 1,156.00
			###### if they are not standard values, then the DB insert will fail ;) (including the error record insert)
			$cols[5] =~ s/(,)(\d{2})$/.$2/; # this takes care of any European style numbers like 100,99
			$cols[5] =~ s/,//g;				# this gets rid of any remaining commas
			
			next if $cols[5] == 0;	# we don't enter -0- commission amount transactions
							# lest we forget,
							# this is because we have no way of knowing if they are real on not

			###### lookup our vendor
			my $vendor = Advertiser($cols[2], $cols[0], 'advertiser_id') ||	Advertiser($cols[1], $cols[0], 'vendor_name_map');
			
			###### if we didn't find our vendor we cannot proceed with this one
			unless ($vendor)
			{
				$errors .= "Failed to find vendor from affiliate data.\n";
				$errors .= 'Advertiser Name: ' . ($cols[1] || '') . "\n";
				$errors .= 'Advertiser ID: ' . ($cols[2] || '') . "\n";
				$errors .= "Creating affiliate_transaction_errors record\n";
				# I hate undef 'errors'
				$errors .= join(', ', map {defined $_ ? $_ : ''} @cols) . "\n";

				my $rv = Aff_trans_errors(@cols);
				$errors .= "Vendor error handler failure ($DBI::errstr):\n$line\n" unless $rv eq '1';
				next;
			}

			###### get some other stuff relating to our vendor
			$VendorDetail{$vendor} ||= $db->selectrow_hashref("
				SELECT cv_mult, reb_mult, comm_mult, status, vendor_name
				FROM vendors WHERE vendor_id = $vendor");
				
			###### if our vendor has a status reflecting they don't pay, we won't record the transaction
			###### theoretically, this should not happen, but go figure
			if ($VendorDetail{$vendor}->{'status'} == -1)
			{
				$errors .= "The vendor for this transaction has an 'NP' status.\n";
				$errors .= "The transaction has not been recorded.\nHere are the details.\n";
				$errors .= "Vendor ID: $vendor\n";
				$errors .= "Vendor Name: $VendorDetail{$vendor}->{vendor_name}\n\n";
				$errors .= 'Advertiser Name: ' . ($cols[1] || '') . "\n";
				$errors .= 'Advertiser ID: ' . ($cols[2] || '') . "\n";
				# I hate undef 'errors'
				$errors .= join(', ', map {defined $_ ? $_ : ''} @cols) . "\n";
				next;
			}

			###### make sure if we have a currency code it is a valid one
			foreach (9,8){ $cols[$_] = StandardizeCurrCode($cols[$_]) if $cols[$_]; }

			###### originally we just stuffed everything into an array for feeding the SQL
			###### but we may need to pass the data off as a ref into other modules
			###### we don't want to have to deal with array elements in that case :)
			my %vals = ('description' => ($cols[7] || ''), 'vendor_id' => $vendor, 'affiliate_code' => $cols[0]);

			###### now the person who gets credit
			($vals{'id'}, $vals{'spid'}, $vals{'membertype'}) = Lookup_Member($cols[6]);
			
			###### transaction date
			$vals{'trans_date'} = ($cols[3]);

			###### now our sale currency
			$vals{'native_currency_amount'} = $cols[4] || 0;
			$vals{'currency_code'} = $cols[8] || undef;

			###### our sale amount (currently converted to USD :> )
			$vals{'amount'} = Convert2USD(($cols[4] || 0), $cols[3], $cols[8]);

			###### convert our commission to USD for sure
			my $commission = Convert2USD($cols[5], $cols[3], $cols[9]);
			$vals{'discount_amt'} = ($commission * $PAYOUT_PCT);
			$vals{'receivable'} = $commission;

			###### we'll need the trans_id to attach to the exception record
			($vals{'trans_id'}) = $db->selectrow_array("SELECT NEXTVAL('$TRANS_TBL_SEQ')");

			###### handoff the data to our exception handler if necessary
			if (my $x = HasExceptions($vals{'vendor_id'})){

				###### we will pass the %vals as a ref and whatever routines get it
				###### can then modify the data indirectly and it will be available
				###### in the changed state for the SQL below
				HandleExceptions($x, \%vals, \@cols);
			}

			###### create our last minute vendor values that need to be reflected in the transaction record
#			$vals{$_} = $VendorDetail{$vendor}->{$_} foreach ('cv_mult', 'reb_mult', 'comm_mult');
			# pull in temporal values instead... as long as they are available
			my $multsOnDate = $db->selectrow_hashref("
				SELECT
					pp_mult AS cv_mult,
					cc_mult AS reb_mult,
					comm_mult
				FROM vendors_mults_on_date($vendor, ?::DATE)", undef, $vals{'trans_date'});
			if ($multsOnDate)
			{
				$vals{$_} = $multsOnDate->{$_} foreach ('cv_mult', 'reb_mult', 'comm_mult');
			}

			my $rv = $db->do($BASE_SQL, undef, (map $vals{$_}, @insert_order)) || $DBI::errstr || 'unknown failure';
			unless ($rv eq '1')
			{
				$errors .= "Transaction creation failure. Return code: $rv\n";
	#			print "DBI error: $DBI::errstr\n";
	#			print join(', ', @vals), "\n";
				$errors .= map "$vals{$_}, ", @insert_order;
				$db->rollback;
				$errors .= "Inserting raw data into affiliate_transaction_errors\n\n";
				$rv = Aff_trans_errors(@cols);
				$errors .= "Error handler failure ($DBI::errstr):\n$line\n" unless $rv eq '1';
			}
			else
			{
				$db->commit;
			}
			
		}
	}
	my $rv = qx#mv $PATH/$INPUT_DIR/$file $PATH/$ARCHIVE_DIR/$file#;
	$errors .= "file move results: $rv\n" if $rv;
}

#$db->commit;
$db->disconnect;
SendErrorMail::SendMail($errors) if $errors;

exit;

sub Advertiser
{
	my ($sv, $ac, $colname) = @_;
	###### let's retain our lookups for reuse
	$Advertisers{"$sv $ac $colname"} ||= $db->selectrow_array("
		SELECT vendor_id FROM linkshare_affiliates_2vendors
		WHERE $colname = ? AND affiliate_code = ?
	", undef, ($sv, $ac));
	return $Advertisers{"$sv $ac $colname"};
}

sub Aff_trans_errors
{
	while (scalar @_ < 9){ push @_, undef; }	###### pad it out so we will have the correct number of placeholders
	my $rv = $db->do("INSERT INTO affiliate_transaction_errors ($Elist_order) VALUES (?,?,?,?,?,?,?,?,?,?)", undef, @_);
	$rv eq '1' ? $db->commit : $db->rollback;
	return $rv;
}

sub Convert2USD
{
	my ($amt, $date, $currcode) = @_;
	###### the default (empty is USD)
	return $amt unless $amt && $currcode && $currcode eq 'USD';
	return sprintf '%8.2f', ($amt / GetXCHG($currcode, $date));
}

sub Email
{
	my ($x, $trans, $row) = @_;
	my $msg = "The data:\n";
	for (my $x=0; $x < scalar @Cols; $x++){
		$msg .= "$Cols[$x]: ";
		$msg .= (defined $row->[$x] ? $row->[$x] : 'NULL') . "\n";
	}
	$msg .= "\n";
	$msg .= "\n\nThe transaction created is $trans->{trans_id}\n";
	sendmail(
		'To' => $x->{'email_to'},
		'From' => '"www.clubshop.com parse-aff-out.pl"<root@clubshop.com>',
		'Subject' => 'Affiliate/Vendor data import exception',
		'Message' => $msg
	) || print "sendmail failed to send for trans_id: $trans->{trans_id}\n";
}

sub GetXCHG
{
	my ($code, $date) = @_;
	($XCHG{$code}{$date}) ||= $db->selectrow_array('
		SELECT rate FROM exchange_rates WHERE code= ? AND date <= ? ORDER BY date DESC LIMIT 1
	', undef, ($code, $date));
	return $XCHG{$code}{$date};
}

sub HandleExceptions
{
	my ($x, $trans, $row) = @_;
	###### we will perform some simple modifications
	###### or turn it over to an imported module for more complex calculations or data lookups
	if ($x->{'exception_type'} == 0)
	{
		###### a do nothing 'type'
	}
	elsif ($x->{'exception_type'} == 1)
	{
		###### if the email_to value is set, we will always do the email
		###### regardless of whether we do anything else or not
		Email(@_) if $x->{'email_to'};

		###### just utilize the simple values in the exception record
		$trans->{'discount_amt'} = $x->{'fix_payout'} if defined $x->{'fix_payout'};
		$trans->{'id'} = $x->{'assign_to'} if defined $x->{'assigned_to'};
		RecordException($trans, $row, {'processed' => $x->{'mark_processed'}});
	}
	elsif ($x->{'exception_type'} == 2)
	{
		###### look for a module name vidxxx where xxx = the vendor ID
		###### then we will execute the Process routine
		###### the Process routine should instantiate any 'global' vars
		###### as it may be called more than once per run of this script
		###### though perl will not load it more than once
		###### our module should return 1
		my $return = {};	# we will pass this in and refer to it for any info we need back
					# since we don't want to change our global hashes/hashrefs
		unless (eval "require '$PATH/exception_modules/vid$x->{'vendor_id'}.pm'")
		{
			$errors .= "eval require of module vid$x->{'vendor_id'} failed\n";
			$errors .= "This transaction with vendor_id: $trans->{'vendor_id'} and trans_id: $trans->{'trans_id'} has been processed normally\n";
		}
		else
		{
			my $rv = eval('vid' . "$x->{'vendor_id'}" . '::Process($x, $trans, $row, $db, $return)') || '';
			unless ($rv eq '1')
			{
				$errors .= "eval module vid$x->{'vendor_id'}::Process() failed with code: $rv\n";
				$errors .= "This transaction with vendor_id: $trans->{'vendor_id'} and trans_id: $trans->{'trans_id'} has been processed normally\n";
			}
			else
			{
				unless ($return->{'do_not_record'})
				{
					RecordException($trans, $row, {'processed' => $return->{'mark_processed'} || $x->{'mark_processed'}});
				}
				Email(@_) if $return->{'email'};
			}
		}
	}
	else
	{
		$errors .= "No exception handling provided for type: $x->{'exception_type'}\n";
		$errors .= "This transaction with vendor_id: $trans->{'vendor_id'} and trans_id: $trans->{'trans_id'} has been processed normally\n";
	}
}

sub HasExceptions
{
	###### we'll keep track of the vendor exceptions so we don't have to look them up more than once
	unless (exists $hasx{$_[0]})
	{
		$hasx{$_[0]} = $db->selectrow_hashref("SELECT * FROM affiliate_2vendor_exceptions WHERE vendor_id= $_[0]");
	}
	return $hasx{$_[0]};
}

sub Lookup_Member
{
	###### we will default to 01 for null ID or failed lookup
	my $id = shift;
	return (1,1,'v') unless $id;
	my @rv = $db->selectrow_array("
		SELECT id,spid,membertype FROM members
		WHERE ${\($id =~ /\D/ ? 'alias' : 'id')} = ?", undef, $id);
	@rv = (1,1,'v') unless @rv;
	return @rv;
}

sub RecordException
{
	my ($trans, $row, $args) = @_;
	
	my $rv = $db->do("
		INSERT INTO affiliate_2vendor_exception_transactions
		($Elist_order , processed, trans_id) VALUES (?,?,?,?,?,?,?,?,?,?,?, $trans->{'trans_id'})",
		undef, @{$row}, $args->{'processed'});
}

sub RmWs
{
	return $_[0] unless defined $_[0];
	$_[0]=~s/^\s+|\s+$//g;
	return $_[0]
}
sub StandardizeCurrCode
{
	return unless $_[0];
	return $_[0] if $_[0] =~ /[A-Z]{3}/;
	return $CURRMAP{$_[0]};
}

###### 05/04/07 added the gathering and insertion of the various vendor multipliers into the transaction record
# 06/06/07 revised the eval'd require to include the complete path due to the cron environment requirements
###### 06/20/07 revised the splitting mechanism on the data lines to use a regexp
###### 06/10/08 various little items like payout change, column name changes in transactions,
###### the inclusion of the native_currency_amount column
###### 12/12/08 added filtering of the amount to handle non-standard numbers
###### 04/22/09 added the error emailing routine
###### 12/23/15 the only fundamental change was the implementation of looking up multipliers based upon transaction date