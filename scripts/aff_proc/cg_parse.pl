#!/usr/bin/perl -w
# cg_parse.pl
# the commission junction download file parser
# written by Shane Partain 03/07
# last modified: 04/23/07 Bill MacArthur

use strict;
use lib ('/home/httpd/scripts/aff_proc');
use aff_proc;
use XML::Simple;
use Data::Dumper;

my $rpt = '';
my $file_type = 'txt';
my $i = 0;
my $j = 0;
my $status = '';
my $aff_id = 'CG';
my %col_nums = ();
my $col_val = '';
my $sale_currency = '';
my $comm_currency = '';
my $head_done = '';
my $blanks = '';

# Just skip the "pending" type
my %cols_used = (
	confirmed => ['Merchant Site', 'BLANK', 'Trans. Date (GMT)', 'Sale Value', 'Commission', 'Aff. Order ID'],
	cancelled => ['Merchant Site', 'BLANK', 'Trans. Date (GMT)', 'Sale Value', 'BLANK',  'Aff. Order ID']
		);

my $arch_dir = '/home/httpd/scripts/aff_proc/archive/';
my $rpt_dir = '/home/httpd/scripts/aff_proc/reports/';
my $rpt_file = $rpt_dir . $aff_id . '.*.' . $file_type;
my $end_date= time() - 86400;

$rpt_file = `/bin/ls $rpt_file`;
chomp $rpt_file;
if ($rpt_file =~ /^.*?\.(\d+)\..*$/) {$end_date = $1;}

open(RPT, $rpt_file);
$rpt = '<xml>';
while (<RPT>) {
	$rpt .= $_;
}
close RPT;
$rpt .= '</xml>';

## The xml parser chokes on the non-breaking spaces.
$rpt =~ s/\&nbsp\;//g;
$rpt = XMLin($rpt);

my $outfile = ($rpt_file . '.' . time());
open (OUT, ">$outfile");

## The whole data structure actually looks something like this,
## with "$status" being one of "confirmed", "cancelled", or "pending":
##  $rpt->{table}->{$status}->{tr}->[0]->{td}->[0]->{content}
## The above variable contains the content of the first column of the
## first row in the $status table.
##
## Loop through 0-3 possible tables.
foreach $status (keys %{$rpt->{table}}) {

	## Go through each row.
	for ($i=0;$i <= $#{$rpt->{table}->{$status}->{tr}};++$i) {

		## We use the header row to make a hash of column numbers/column names.
		if ($i==0 && !%col_nums) {
			foreach $col_val (@{$rpt->{table}->{$status}->{tr}->[$i]->{td}}) {
				foreach (@{$cols_used{$status}}){
					if ($_ eq $col_val->{content}){$col_nums{$_} = $j;}
				}
				$j++;
			}
			$j = 0;
			## And then make sure all the columns we need are there.
			my @col_nums = keys %col_nums;
			if ($status eq 'cancelled') {
				$blanks = 2;
			}else{ $blanks = 1;}
                        unless ($#col_nums == ($#{$cols_used{$status}} - $blanks)){
                                aff_proc::printLog('[parser]: ' . $aff_id . ' report is missing a column.');
                                exit;
                        }
			$col_val = '';
		}
		foreach (@{$cols_used{$status}}){
			## Skip additional head rows.
			if ($head_done && $i==0){last;}
			if ($_ eq 'BLANK') {print OUT "\t"; next;}
			$col_val = $rpt->{table}->{$status}->{tr}->[$i]->{td}->[$col_nums{$_}]->{content};
			if ($col_val) {
				if ($_ eq 'Sale Value' && $i) {
					$col_val =~ s/^(.*)\$(.*)$/$2/;
					$sale_currency = $1;

					## Cancelled values need to be made negative.
			                if ($status eq 'cancelled' && $i!=0) {
						$col_val *= -1;
					}
				}
				if ($_ eq 'Commission') {
					$col_val =~ s/^(.*)\$(.*)$/$2/;
					$comm_currency = $1;
				}
			}
			if ((!$head_done) || ($col_val && $i != 0)) {
				print OUT $col_val, "\t";
			}else{
				print OUT "\t";
			}
		}
		if ($head_done && $i==0){next;}
		unless($head_done){
			$head_done=1;
			print OUT "\n";
			next;
		}
		print OUT $aff_id, "\t";
		if ($sale_currency) {print OUT $sale_currency, "\t";}
		if ($comm_currency) {print OUT $comm_currency;}
		print OUT "\n";
		$sale_currency = '';
		$comm_currency = '';
	}
	%col_nums = ();

}
close OUT;
`/bin/mv $rpt_dir$aff_id.*.out $arch_dir`;
my $done_outfile = ($rpt_file . '.' . time() . '.' . 'out');
`/bin/mv $outfile $done_outfile`;
`/bin/echo "parsed\t$end_date" > $rpt_dir$aff_id`;
#`/bin/rm $rpt_file`;
`/bin/mv $rpt_file $arch_dir`;
aff_proc::printLog('[parser]: ' . $aff_id . ' successfully parsed.');

# print Dumper($rpt);
# print $rpt->{tr}->[0]->{td}->[0]->{content};
# print $rpt->{table}->{confirmed}->{tr}->[0]->{td}->[0]->{content}

exit;

# 04/23/07 revised to copy the download file to the archive directory as well
# this could be stopped after we are certain we *always* obtain the desired results
# and they don't change the report after the fact
