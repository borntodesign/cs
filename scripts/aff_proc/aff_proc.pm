package aff_proc;
## aff_proc.pm - Routines for use with downloading and parsing affiliate reports.
# originally written by Shane Partain 03/07
# last modified: 05/16/13	Bill MacArthur

use strict;
use base 'Exporter';

our @EXPORT_OK = qw($RPT_DIR $ARCHIVE_DIR $HOME_DIR);

our $HOME_DIR = '/home/httpd/scripts/aff_proc/';
our $RPT_DIR = '/home/httpd/scripts/aff_proc/reports/';
our $ARCHIVE_DIR = '/home/httpd/scripts/aff_proc/archive/';

our $DEBUG = 0;			# 1 enables logging of requests

my ($rpt_file) = ();	###### these will be assigned file path/names for use between routines
###### some other convenient globals
my (@heads, $end_date) = ();
#my $end_date = time() - 86400;

sub download
{
	use LWP;
	use HTTP::Cookies;

	my ($id, $rpt_type, $download_params, $login_params) = @_;

	my @req_rv = ();
	my @download = ();
	my $rpt_name = '';

	##
	## Put the time minus 24 hours (report end date) in the filename, so
	## the parser will know what period it's parsing.
	##
	my $filename =  $RPT_DIR . $id . '.' . (time() - 86400) . '.' . $rpt_type;
	push @{$download_params}, $filename;

	##
	## Get rid of any old report files to avoid duplicates.
	##
	opendir (DIR, $RPT_DIR);
	my @files = readdir(DIR);
	closedir DIR;
	foreach (@files)
	{
	        if (/^$id\..*\.txt$/) {unlink $RPT_DIR . $_;}
	}

	my $res = '';
	my $ua = LWP::UserAgent->new;
	$ua->cookie_jar( {} );

	if ($login_params)
	{
		@download = ($login_params, $download_params);
	}
	else { @download = ($download_params); }

	foreach(@download){

                my ($url, $query, $report_file) = @{$_};
				printLog("fetching: $url?$query") if $DEBUG;
                my $req = HTTP::Request->new(POST => $url);
                $req->content_type('application/x-www-form-urlencoded');
                $req->content($query);
                $res = $ua->request($req, $report_file);
        }
        return $res->status_line;
}

sub getDate($)
{
	my $time = shift || time();
	
	##
	## Returns an 8 element array, referenced from the
	## current time(), or the time passed in:
	## 
	## 0: yyyymmdd string for yesterday
	## 1: yyyymmdd string for today
	## 2: localtime() day for yesterday
	## 3: localtime() month for yesterday
	## 4: localtime() year + 1900 for yesterday 
	## 5: localtime() day for today
	## 6: localtime() month for today
	## 7: localtime() year + 1900 for today
	##

	# if we pass in a start_seed of -0-, then we will get exactly the day of $time
	my @yesterday =  localtime($time - 86400);
	my $yesterday_str = $yesterday[5] + 1900;
	$yesterday_str .= sprintf("%02d", $yesterday[4] + 1); # months are 0-11
	$yesterday_str .= sprintf("%02d", $yesterday[3]);

	# if we pass in an end_seed, then we can get something other than $time, like 1 day later like CJ wants if we set it to 86400
	my @today =  localtime($time);
        my $today_str = $today[5] + 1900;
        $today_str .= sprintf("%02d", $today[4] + 1);
        $today_str .= sprintf("%02d", $today[3]);

	return ($yesterday_str, $today_str, 
		sprintf("%02d", $yesterday[3]), sprintf("%02d", ($yesterday[4] + 1)), $yesterday[5] + 1900,
		sprintf("%02d", $today[3]), sprintf("%02d", ($today[4] + 1)), $today[5] +1900);

}

sub parseText($$)
{
	##
	## This one works for most simple straightforward text-delimited files.

	my $aff_id = shift;
	my $args = shift;	###### a hashref of other arguments
		# rpt_type : 'txt' or whatever
		# cols_used : an arrayref of columns used to create our output
		# output_cols : optional -  arrayref defining the actual output cols (if different than cols_used)
		# delimiter : self explanatory
		# head_row : which line in the file contains our column names (in array notation 0 = first row)
		# pre_process_line : a subroutine ref to be invoked before splitting each line
		# pre_process_col : a subroutine ref to be invoked on each 'column' before it's processed
		# post_processing : a subroutine ref not invoked within here, but whose presence causes the data not
		#			to be written but rather returned to the caller for post processing :) b4 writing

	my (@this_row, @rowlist) = ();
	my $i = 0;

	$rpt_file = $args->{'report_file'} || $RPT_DIR . $aff_id . '.*.' . $args->{'rpt_type'};

	$rpt_file = `/bin/ls $rpt_file`;
	chomp $rpt_file;
	if ($rpt_file =~ /^.*?\.(\d+)\..*$/) { $end_date = $1; }
		
	unless (open(REPORT, $rpt_file)) {
		my $errstr = "[parser]: $aff_id report does not exist.";
		printLog($errstr);
		return $errstr;
	}

	foreach my $line (<REPORT>)
	{
		chomp $line;
		###### skip blank lines
		next unless $line;
		$line =~ s/\r$//;
		if ($i < $args->{'head_row'}){
			$i++;
			next;
		}
		elsif ($i == $args->{'head_row'})
		{
			@heads = split /$args->{'delimiter'}/, $line;
		###### now loop through our expected/required columns and make sure we have them all
		###### ignore 'subs' and BLANKS
			foreach my $req (@{$args->{cols_used}})
			{
				next if $req eq 'BLANK' or $req =~ m#sub.*?\{(.*)\}#s;
				unless (grep $_ eq $req, @heads)
				{
					my $errstr = qq|[parser]: Column: "$req" isn't in $rpt_file.|;
					printLog($errstr);
				# rather than searching for where this error will end up and putting a print there,
				# right here is simpler
					print "$errstr\n";
					close REPORT;
					return $errstr;
				}
			}
			$i++;
			next;
		}
		###### doctor things up with this line if necessary
		&{$args->{'pre_process_line'}}( \$line ) if $args->{'pre_process_line'};
		next unless $line;	###### we may have 'deleted' it as a pre_process_event

		@this_row = split /$args->{delimiter}/, $line;
		my %this_row = ();
		foreach my $col (@heads)
		{
			$this_row{$col} = shift @this_row;
			###### doctor things up with this column if necessary
			&{$args->{'pre_process_col'}}( \$this_row{$col} ) if $args->{'pre_process_col'};
			###### remove leading/trailing whitespace
			$this_row{$col} =~ s/^\s*|\s*$//g;
		}

		## SS doesn't show voided trans as negative
		if ($aff_id eq 'SS' && $this_row{'voided'} eq '1') {
			$this_row{'transamount'} = $this_row{'transamount'} * -1;
			$this_row{'commission'} = $this_row{'commission'} * -1;
		}
		## Print out ACH payments to stdout so we have a record of what they've paid us,
		## then skip this row.
		if ($aff_id eq 'SS' && $this_row{'comment'} =~ /^Payment/)
		{
			print $this_row{'comment'};
			%this_row = ();
                	$i++;
			next;
		}
		## Save the date and drop the time.
		if ($aff_id eq 'SS') { $this_row{'transdate'} =~ s/^(.*?)\s.*$/$1/; }

		foreach (@{$args->{cols_used}}){
			if ( $_ =~ m#sub.*?\{(.*)\}#s )
			{
				###### execute this piece
				# we are eval'ing the code inside the sub{..}
				# because we cannot define the sub directly in the config as the vars don't exist there
				my $rv = (eval $1) || '';
				if ($rv ne '1'){ print STDERR "Error eval'ing: $1\neval returned: $rv\n"; }
			}
#			## Clear values that only have empty spaces. TD does this.
#			$this_row{$_} = '' if $this_row{$_} eq ' ';
		}
		push @rowlist, \%this_row;
	}
	close REPORT;
	###### either write out our data or pass back a reference for further processing by the caller
	###### the caller can then call the WriteOut procedure with the modified data
	return ($args->{'post_processing'} ? \@rowlist : WriteOut($aff_id, ($args->{'output_cols'} || $args->{'cols_used'}), \@rowlist));
}

sub printLog($)
{
        my $errstr = shift;
	
        my $date_time = `/bin/date +%Y%m%d-%R`;
        chomp $date_time;

        my $logfile = ($HOME_DIR . 'aff_proc.log');
        open(LOG, ">>$logfile");
        print LOG $date_time, " $errstr", "\n";
        close LOG;

}

sub rptDates($;$)
{
	my $id = shift;
	my $args = shift;
	my %rpt_dates = ();

	##
	## Report period ends yesterday if we do not provide an end_seed
	##
	my @date = aff_proc::getDate( time() + ($args->{'end_seed'} || 0));
	$rpt_dates{'rpt_end_day'} = $date[2];
	$rpt_dates{'rpt_end_month'} = $date[3];
	$rpt_dates{'rpt_end_year'} = $date[4];
	$rpt_dates{'rpt_end_date_str'} = $date[0];

	##
	## Report period starts the day after the last parsed date (or yesterday,
	## if one hasn't been recorded). "Last parsed" date is the period end
	## date for the report that was last successfully parsed.
	##
	my $last_parsed = `/bin/cat $RPT_DIR$id`;
	chomp $last_parsed;
	if ($last_parsed)
	{
		$last_parsed =~ /^parsed\t(\d*)$/;
		
		# if we have provided a start seed value we will use that over in getDate, like with CJ which uses -0-, otherwise we will add 86400 seconds for 1 day
		$last_parsed = $1 + ((defined $args->{'start_seed'}) ? $args->{'start_seed'} : 86400);
#		warn "last_parsed: $last_parsed";
	}
	else{ $last_parsed = time() - 86400;}

	my @rpt_start_date = aff_proc::getDate($last_parsed);
	$rpt_dates{'rpt_start_day'} = $rpt_start_date[5];
	$rpt_dates{'rpt_start_month'} = $rpt_start_date[6];
	$rpt_dates{'rpt_start_year'} = $rpt_start_date[7];
	$rpt_dates{'rpt_start_date_str'} = $rpt_start_date[1];

	if ($rpt_dates{'rpt_start_date_str'} > $rpt_dates{'rpt_end_date_str'})
	{
		aff_proc::printLog('[downloader]: Yesterday\'s ' . $id . ' report already downloaded.');
		warn "Start date: $rpt_dates{'rpt_start_date_str'} >= end date: $rpt_dates{'rpt_end_date_str'}";
		return undef;
	}
	else{ return \%rpt_dates;}
}

sub WriteOut
{
	my ($aff_id, $output_cols, $rowlist, $args) = @_;
	
	# with the new CJ downloader script, we do not set this value in one of the other functions, so we have to pass in a value
	$rpt_file ||= $args->{'rpt_file'};
	
	my $outfile = $rpt_file . '.' . time();
	open(OUT, ">$outfile") || die "Failed to open output file: $outfile\n";

	###### print our heading row
	print OUT $aff_id;

	###### skip our 'subs'
	foreach (grep { $_ !~ m#sub.*?\{(.*)\}#s } @{$output_cols})
	{
		print OUT "\t$_";
	}
	
	print OUT "\n";
	
	foreach my $row (@{$rowlist})
	{
		print OUT $aff_id;
		###### skip our 'subs'
		foreach (grep { $_ !~ m#sub.*?\{(.*)\}#s } @{$output_cols})
		{
			print OUT ($_ eq 'BLANK' ? "\t" : "\t$row->{$_}");
		}
		print OUT "\n";
	}
	close OUT;

	my $aff_file_contents = $args->{'aff_file_contents'} || "parsed\t$end_date";
	
	my $done_outfile = ($rpt_file . '.' . time() . '.' . 'out');
	`/bin/mv $outfile $done_outfile`;
	`/bin/echo "$aff_file_contents" > $RPT_DIR$aff_id`;
	`/bin/mv $rpt_file $ARCHIVE_DIR`;
	return 0;
}

1;

# 04/23/07 revised to save the downloaded file to the archive directory instead of deleting it
# once we are sure we always get the desired results and that the affiliate doesn't change the report after the fact
# we could resume deleting them
###### 04/27/07 heavy modification to parseText
###### 06/11/07 tweaked the looper in the row writer to use the cols_used list instead of counting
###### 06/13/07 ripped out the 'mv' of existing .out files,
###### they need to remain until they are parsed by the output file parser which will move them itself
###### 01/29/09 added the $DEBUG var and logging of requests if it was set
###### 05/16/13 added another