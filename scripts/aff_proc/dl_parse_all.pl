#!/usr/bin/perl -w

## dl_parse_all.pl [-d|-p] [affiliate_id]
##
## This is the top-level script for downloading/parsing linkshare affiliate
## reports. The optional -d or -p flags will download or parse only, respectively.
## Passing one or more affiliate IDs as arguments will perfom the selected action
## on only the named affiliate(s). The default action is to both downlad and parse all
## (except RR, since it's only available weekly).
##

use strict;
use Switch;

my @rpts = ();
my $action = '';

if (@ARGV) {
	if ($ARGV[0] =~ /-/) {$action = shift;}
}

unless (@ARGV) {
	push @rpts, 'CG';
	push @rpts, 'CJ';
	push @rpts, 'KO';
	push @rpts, 'LS';
	push @rpts, 'PF';
	push @rpts, 'SS';
	push @rpts, 'TD';
	push @rpts, 'ZA';
}else{
	@rpts = @ARGV;
}

if ($action =~ /d/ || !$action) {
	foreach (@rpts) {

		switch ($_) {

			case /CG/ { `/home/httpd/scripts/aff_proc/cg_download.pl`; }

			case /CJ/ { `/home/httpd/scripts/aff_proc/rpt_download.pl CJ`; }
			
			case /KO/ { `/home/httpd/scripts/aff_proc/ko_download.pl`; }
	
			case /LS/ { `/home/httpd/scripts/aff_proc/ls_download.pl`; }

			case /PF/ { `/home/httpd/scripts/aff_proc/pf_download.pl`; }
			
			case /RR/ { `/home/httpd/scripts/aff_proc/rr_download.pl`; }

			case /SS/ { `/home/httpd/scripts/aff_proc/ss_download.pl`; }
			
			case /TD/ { `/home/httpd/scripts/aff_proc/rpt_download.pl TD`; }

			case /ZA/ { `/home/httpd/scripts/aff_proc/za_download.pl`; }

		}

	}
}

if ($action =~ /p/ || !$action) {
        foreach (@rpts) {

                switch ($_) {

			case /CG/ { `/home/httpd/scripts/aff_proc/cg_parse.pl`; }
                        
			case /CJ/ { `/home/httpd/scripts/aff_proc/rpt_parse.pl CJ`; }
			
			case /KO/ { `/home/httpd/scripts/aff_proc/ko_parse.pl`; }

                        case /LS/ { `/home/httpd/scripts/aff_proc/rpt_parse.pl LS`; }
                        
			case /PF/ { `/home/httpd/scripts/aff_proc/pf_parse.pl`; }
			
			case /RR/ { `/home/httpd/scripts/aff_proc/rr_parse.pl`; }

                        case /SS/ { `/home/httpd/scripts/aff_proc/rpt_parse.pl SS`; }

                        case /TD/ { `/home/httpd/scripts/aff_proc/rpt_parse.pl TD`; }

                        case /ZA/ { `/home/httpd/scripts/aff_proc/za_parse.pl`; }

                }

        }
}
