<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>
<title>
<xsl:value-of select="//base/lang_blocks/request_title"/>
</title>
<link href="/css/soa.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
<xsl:comment>
function AccountCk(frm){
	if (frm.account_email.value.match('@') != '@' &amp;&amp; <xsl:value-of select="//params/payhow"/> != 9447){
		alert("<xsl:value-of select="//lang_blocks/payout_form/enter_account_email"/>");
		return false;
	}
	if ((! frm.account_email.value) || (frm.account_email.value.match('\D') &amp;&amp; <xsl:value-of select="//params/payhow"/> == 9447)){
		alert("<xsl:value-of select="//lang_blocks/payout_form/enter_account_number"/>");
		return false;
	}
	return true;
}
</xsl:comment>
</script>
</head>

<body onload="submitted = false;">

<div align="center">
<span class="head3">
<xsl:value-of select="//base/lang_blocks/request_title"/>
<xsl:text> - </xsl:text>
<xsl:value-of select="//base/data/user/firstname" />
<xsl:text> </xsl:text>
<xsl:value-of select="//base/data/user/lastname" />
</span>


<form action="%%ME%%" method="post">
<xsl:attribute name="onsubmit">return AccountCk(this)</xsl:attribute>
%%messages%%
<table border="0" cellpadding="2" cellspacing="0">

<tr><td align="center">
<table border="0">
<tr>
<td><xsl:value-of select="//lang_blocks/balance" />:</td>
<td class="amount"><xsl:text>$</xsl:text><xsl:value-of select="//base/data/balance" /></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/withdrawable_balance" /></td>
<td class="amount"><xsl:text>US$ </xsl:text><b><xsl:value-of select="//base/data/withdrawable_balance" /></b></td>
</tr>
</table>

</td></tr>

<tr><td colspan="2">
<hr width="100%"/>
</td></tr>

<tr><td colspan="2" align="center" class="note">
%%admin%%
</td></tr>

<tr><td colspan="2" align="left">

<table id="confirmation_payout_tbl">
<tr>
<td><xsl:value-of select="//base/lang_blocks/payout_form/amount_you_will_get"/>:</td>
<td class="amount">$<xsl:value-of select="format-number(//base/data/confirm/request_amount, '#.00')"/></td>
</tr>

<tr>
<td><xsl:value-of select="//base/lang_blocks/payout_form/fee_amount"/> ( <xsl:value-of select="//base/data/confirm/fee_label"/> ) :</td>
<td class="amount">$<xsl:value-of select="format-number(//base/data/confirm/fee, '#.00')"/></td>
</tr>

<tr>
<td><xsl:value-of select="//base/lang_blocks/payout_form/total_amount_to_be_deducted"/>:</td>
<td class="amount">$<xsl:value-of select="format-number((//base/data/confirm/request_amount + //base/data/confirm/fee), '#.00')"/></td>
</tr>

<tr>
<td><xsl:value-of select="//base/lang_blocks/payout_form/to_be_paid_by"/>:</td>
<td class="amount"><xsl:value-of select="//base/data/payment_by"/>
<xsl:if test="//base/params/payhow = 9320">: <xsl:value-of select="//base/params/pay_agent_selected_label"/></xsl:if>
</td>
</tr>

<xsl:if test="//base/data/confirm/need_account">
<tr>
<td>
<xsl:choose>
<xsl:when test="//params/payhow = 9447">
<xsl:value-of select="//base/lang_blocks/payout_form/enter_account_number"/>
</xsl:when>
<xsl:when test="//params/payhow = 9300">
<xsl:value-of select="//base/lang_blocks/payout_form/enter_paypal_account_email"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="//base/lang_blocks/payout_form/enter_account_email"/>
</xsl:otherwise>
</xsl:choose>
:</td>
<td><input type="text" name="account_email" size="30" /></td>
</tr>
</xsl:if>

</table> <!-- End of confirmation_payout_tbl -->

</td></tr>

<tr><td><br/></td></tr>

<tr><td colspan="2" align="center">

<input class="in" type="submit">
<xsl:attribute name="value"><xsl:value-of select="//base/lang_blocks/submit"/></xsl:attribute>
</input>

<input type="hidden" name="action" value="disburse"/>
</td></tr>
</table>

<input type="hidden" name="payhow">
<xsl:attribute name="value"><xsl:value-of select="//params/payhow"/></xsl:attribute>
</input>

<input type="hidden" name="amount">
<xsl:attribute name="value"><xsl:value-of select="//base/data/confirm/request_amount"/></xsl:attribute>
</input>

<xsl:if test="//base/params/pay_agent_selected != ''">
<input type="hidden" name="pay_agent_selected">
<xsl:attribute name="value"><xsl:value-of select="//base/params/pay_agent_selected"/></xsl:attribute>
</input>
</xsl:if>
</form>

<p class="footer"><img src="/images/copyright_.gif" width="173" height="19" alt="Copyright"/></p>
</div>

</body>
</html>
</xsl:template>

</xsl:stylesheet>