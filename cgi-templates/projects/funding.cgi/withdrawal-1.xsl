<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>
<title>
<xsl:value-of select="//base/lang_blocks/request_title"/>
</title>
<link href="/css/soa.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/check_req_x.js"></script>
<script type="text/javascript" src="/js/funding.cgi.js"></script>
<script type="text/javascript">
<xsl:comment>
var xml_cashout = "<xsl:value-of select="//base/lang_blocks/cashout" />";
var xml_empty_field = "<xsl:value-of select="//base/lang_blocks/messages/empty_field" />";
var xml_bad_amount = "<xsl:value-of select="//base/lang_blocks/messages/bad_amount" />";
var xml_payout_minimum = "<xsl:value-of select="//base/lang_blocks/payout_min" />" + ' ' + "<xsl:value-of select="//base/lang_blocks/messages/payout_req" />";
var xml_cashout_a = "<xsl:value-of select="//lang_blocks/cashout_a" />";

$( document ).ready(function() {
	$(".paradio").change(function () {
	
		if ($("#payhow9320").attr("checked")) {
	        $('#pay_agent_container').show();
	    }
	    else {
	        $('#pay_agent_container').hide();
	        // reset the selection, if any to option 0
	        $('#pay_agent_container :nth-child(1)').prop('selected', true);
	    }
	});
});
</xsl:comment>
</script>
</head>

<body onload="submitted = false;">

<div align="center">
<span class="head3">
<xsl:value-of select="//base/lang_blocks/request_title"/>
<xsl:text> - </xsl:text>
<xsl:value-of select="//base/data/user/firstname" />
<xsl:text> </xsl:text>
<xsl:value-of select="//base/data/user/lastname" />
</span>


<form action="%%ME%%" method="get">
<xsl:attribute name="onsubmit">return ckPayoutFormStep1(this);</xsl:attribute>

<table border="0" cellpadding="2" cellspacing="0">

%%messages%%

<tr><td align="center">
<table border="0">

<tr>
<td><xsl:value-of select="//lang_blocks/balance" />:</td>
<td class="amount"><xsl:text>$</xsl:text><xsl:value-of select="//base/data/balance" /></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/withdrawable_balance" /></td>
<td class="amount"><xsl:text>US$ </xsl:text><b><xsl:value-of select="//base/data/withdrawable_balance" /></b></td>
</tr>

<xsl:if test="//base/data/withdrawable_balance &lt; //base/data/payout_minimum_value">
	<tr>
	<td><xsl:value-of select="//lang_blocks/min_req_amt" /></td>
	<td class="amount"><b><xsl:value-of select="//lang_blocks/payout_min" /></b></td>
	</tr>
	
	<tr>
	<td><xsl:value-of select="//lang_blocks/maximum" />:</td>
	<td class="amount">-0-</td>
	</tr>
</xsl:if>

</table>
</td></tr>

<xsl:if test="//base/data/withdrawable_balance &gt;= //base/data/payout_minimum_value">

<tr><td colspan="2">
<hr width="100%"/>
</td></tr>
<tr><td colspan="2" align="center" class="note">
%%admin%%
</td></tr>

<tr><td colspan="2" align="left" class="note">

* <xsl:value-of select="//lang_blocks/cashout_a" />:

<ul id="payout_types">
<xsl:if test="//user/country = 'US' and //active_payment_methods/method = 9410">
<li>
<input type="radio" name="payhow" value="9410" class="paradio">
<xsl:if test="//params/payhow = 9310"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
</input>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pay_check" />
</li>
</xsl:if>

<!-- Alert Pay -->
<xsl:if test="//active_payment_methods/method = 9442">
<li>
<input type="radio" name="payhow" value="9442"/>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pay_ap" />:
</li>
</xsl:if>

<!-- EcoCard -->
<xsl:if test="//active_payment_methods/method = 9447">
<li>
<input type="radio" name="payhow" value="9447"/>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pay_ep" />:
</li>
</xsl:if>

<!-- PayPal -->
<xsl:if test="//active_payment_methods/method = 9300">
<li>
<input type="radio" name="payhow" value="9300" class="paradio">
<xsl:if test="//params/payhow = 9300"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
</input>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pay_pp" />
</li>
</xsl:if>

<!-- Pay Agent -->
<xsl:if test="//active_payment_methods/method = 9320 and //user/is_pay_agent = ''"><!-- pay agents themselves cannot get paid by a pay agent -->
<li style="line-height:200%; padding-left: 2em;"><a href="/pax/m/dr"><xsl:value-of select="//lang_blocks/pay_pay_agent" /></a></li>
</xsl:if>

</ul>

* <xsl:value-of select="//lang_blocks/cashout" />:

<input type="text" name="amount">
<xsl:if test="//params/amount">
<xsl:attribute name="value"><xsl:value-of select="format-number(//params/amount, '#.00')" />
</xsl:attribute>
</xsl:if>
</input>
<br/>

<p>
<xsl:value-of select="//lang_blocks/payout_form/confirm_notice" /></p>
<ul>
<li><xsl:value-of select="//lang_blocks/payout_form/prompt1" /></li>
<li><xsl:value-of select="//lang_blocks/payout_form/prompt2" /></li>
<li><xsl:value-of select="//lang_blocks/payout_form/prompt3" /></li>
</ul>

</td></tr>

<tr><td colspan="2" align="center">

<input class="in" type="submit">
<xsl:attribute name="value">
<xsl:value-of select="//base/lang_blocks/submit"/></xsl:attribute>
</input>

<input type="hidden" name="action" value="confirm"/>
</td></tr>

</xsl:if>

</table>
</form>

<p class="footer"><img src="/images/copyright_.gif" width="173" height="19" alt="Copyright"/></p>
</div>

</body>
</html>
</xsl:template>

</xsl:stylesheet>
