<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>
<title>
<xsl:value-of select="//base/lang_blocks/report_title"/>
</title>
<link href="/css/soa.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/reward.js"></script>
<script type="text/javascript" src="/js/yui/yahoo-min.js"></script>
<script type="text/javascript" src="/js/yui/event-min.js"></script>
<script type="text/javascript" src="/js/funding.cgi.js"></script>
<script type="text/javascript">
<xsl:comment>
// we can make this node translatable and use a value-of XSL tag to pull it in
var dateRangeError = "<xsl:value-of select="/base/lang_blocks/messages/selectmy" />";
</xsl:comment>
</script>
</head>

<body onload="init();">

<div align="center">
<span class="head3">
<xsl:value-of select="//base/lang_blocks/report_title"/>
<xsl:text> - </xsl:text>
<xsl:value-of select="//base/lang_blocks/welcome"/>
<xsl:text> </xsl:text>
<xsl:value-of select="//base/data/user/firstname" />
</span>
<br/>
<span class="head4">
<xsl:value-of select="//lang_blocks/balance" />: $<xsl:value-of select="//base/data/balance" />
</span>

<table border="0" cellpadding="2" cellspacing="0">
<tr><td colspan="5">
<hr width="100%"/>
<p class="note"><xsl:value-of select="//lang_blocks/ca_intro" /></p>
<ul><li class="note"><a href="/manual/business/clubaccount_help.xml"><xsl:value-of select="//lang_blocks/ca_howtohelp" /></a></li></ul>
</td></tr>

<tr><td colspan="5" align="left">

<!--Start Deposit section-->

<p class="head4" style="color:#339900"><b><u><xsl:value-of select="//lang_blocks/deposit_header" /></u></b></p>

<ul>
<li class="note"><a><xsl:attribute name="href"><xsl:value-of select="//base/params/script_name"/>?form=cc</xsl:attribute><xsl:value-of select="//lang_blocks/creditordebit" /></a></li>
<li class="note"><a href="https://www.clubshop.com/cgi/PP?x=startExpressCheckout;sfunc=funding">PayPal</a></li>
<!--li class="note"><a href="https://www.clubshop.com/cgi/Stripe.cgi?sfunc=funding;x=startStripeCheckout" target="_blank">Stripe</a></li-->
<!--li class="note"><a><xsl:attribute name="href"><xsl:value-of select="//base/params/script_name"/>?form=ep</xsl:attribute>ecoPayz</a><xsl:text> </xsl:text> <xsl:text> </xsl:text>[ <a href="/manual/business/fees.xml" target="_blank">more info</a> ]</li-->
<li class="note"><a href="/manual/business/fees.xml" target="_blank"><xsl:value-of select="//lang_blocks/paymethod_d" /></a></li>
<li class="note"><a href="/manual/business/fees.xml" target="_blank"><xsl:value-of select="//lang_blocks/wu_deposit_title" /></a></li>
</ul>

<hr/>
<!--start withdraw section-->
<p class="head4" style="color:#FF6600"><b><u><xsl:value-of select="//lang_blocks/withdraw_header" /></u></b></p>

<ul>
<li class="note">
<a onclick="ReLoad('reset')">
<xsl:attribute name="href"><xsl:value-of select="//base/params/script_name"/>?action=payout</xsl:attribute>
<xsl:value-of select="//lang_blocks/req_withdraw" />
</a>
</li>
<!--start vips only section-->

<xsl:if test="/base/data/user/membertype='v'">

	<li class="note"><a href="/cgi/pay4_others.cgi"><xsl:value-of select="//lang_blocks/payothers_header" /></a></li>
	<li class="note"><a href="/cgi/vip/TransferFunds.cgi"><xsl:value-of select="//lang_blocks/transfer_funds" /></a></li>

</xsl:if> <!-- end of Partner Only section -->

</ul>
<p class="note">
<xsl:value-of select="//lang_blocks/req_withdraw_rule" />
</p>
<!--xsl:value-of select="//lang_blocks/request_comm" /> (temporarily offline)-->

<!-- INSERT ANY MESSAGES IN AT THIS POINT... these will actually be preformatted HTML with XSL nodes :( -->
%%messages%%

<hr style="width:100%; clear:both;"/>
</td></tr>

<tr><td colspan="5" class="note">

<form action="%%ME%%" method="get" onsubmit="return checkDateRanges(this)">

<table border="0">

%%admin%%

<tr><td align="left" nowrap="nowrap">
<input type="radio" name="period" id="current" value="current" checked="checked"/>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/current" />
</td>
<td align="right" nowrap="nowrap">
<input type="radio" name="period" id="range" value="range" />
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/range" />
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/from" />
<xsl:text>: </xsl:text>
</td>
<td nowrap="nowrap">
	<xsl:call-template name="build_menu">
	<xsl:with-param name="fieldname" select="'start_mon'" />
	<xsl:with-param name="values" select="'months'" />

	<xsl:with-param name="onchange_event" select="'document.forms[0].period[1].checked=true'" />
	</xsl:call-template>
<xsl:text> </xsl:text>
	<xsl:call-template name="build_menu">
	<xsl:with-param name="fieldname" select="'start_year'" />
	<xsl:with-param name="values" select="'years'" />
	<xsl:with-param name="onchange_event" select="'document.forms[0].period[1].checked=true'" />
	</xsl:call-template>
</td>
<td align="center">
<input class="in" type="submit">
<xsl:attribute name="value">
<xsl:value-of select="//base/lang_blocks/submit"/></xsl:attribute>
</input>
</td></tr>
<tr>
<td align="left" nowrap="nowrap">
<input type="radio" name="period" id="previous" value="previous" />
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/previous" />
</td>
<td align="right" nowrap="nowrap">
<xsl:value-of select="//lang_blocks/thru" />
<xsl:text>: </xsl:text>
</td>
<td nowrap="nowrap">
	<xsl:call-template name="build_menu">
	<xsl:with-param name="fieldname" select="'stop_mon'" />
	<xsl:with-param name="values" select="'months'" />
	<xsl:with-param name="onchange_event" select="'document.forms[0].period[1].checked=true'" />
	</xsl:call-template>
<xsl:text> </xsl:text>
	<xsl:call-template name="build_menu">
	<xsl:with-param name="fieldname" select="'stop_year'" />
	<xsl:with-param name="values" select="'years'" />
	<xsl:with-param name="onchange_event" select="'document.forms[0].period[1].checked=true'" />
	</xsl:call-template>
</td>

<td align="center">
<input class="in" type="reset">
<xsl:attribute name="value">
<xsl:value-of select="//base/lang_blocks/reset"/></xsl:attribute>
</input>
<input type="hidden" name="action" value="report"/>
</td></tr>
</table>

</form>

</td></tr>

<tr><td colspan="5" align="center">
<hr width="50%"/>
</td></tr>

<tr>
<td colspan="5" class="head4">
<xsl:value-of select="//base/lang_blocks/trans_title" />
<xsl:text> </xsl:text>
<xsl:value-of select="//base/lang_blocks/from" />
<xsl:text> </xsl:text>
<xsl:text> </xsl:text>
<xsl:value-of select="//base/data/start" />
<xsl:text> </xsl:text>
<xsl:value-of select="//base/lang_blocks/thru" />
<xsl:text> </xsl:text>
<xsl:value-of select="//base/data/stop" />
<xsl:text>:</xsl:text>
</td>
</tr>

<xsl:choose>
<xsl:when test="//base/data/transactions != ''">
	<tr>
	<th><xsl:value-of select="//base/lang_blocks/date" /></th>
	<th><xsl:value-of select="//base/lang_blocks/transaction" /></th>
	<th><xsl:value-of select="//base/lang_blocks/description" /></th>
	<th><xsl:value-of select="//base/lang_blocks/amount" /></th>
	<th><xsl:value-of select="//base/lang_blocks/running_balance" /></th>
	</tr>

<tr class="beginning_balance">
<td><xsl:value-of select="//base/data/start" /></td>
<td></td>
<td>
<xsl:value-of select="//base/lang_blocks/beginning_balance" />
</td>
<td></td>
<td class="amount">
<xsl:text>$</xsl:text>
<xsl:value-of select="//base/data/beginning_balance" />
</td></tr>

	<xsl:for-each select = "//base/data/transactions/*">
	<!--xsl:sort select="./entered" /-->

	<tr><xsl:attribute name="class"><xsl:choose><xsl:when test="position() mod 2 = 1">odd</xsl:when><xsl:otherwise>even</xsl:otherwise></xsl:choose></xsl:attribute>
	<td class="trans_date" nowrap="nowrap"><xsl:value-of select="substring(./entered,1,10)" /></td>
	<td class="trans_id" align="center"><xsl:value-of select="./trans_id" /></td>

	<td class="data" align="left">

	<xsl:if test="./void = '1'">
		<b><xsl:value-of select="//base/lang_blocks/voided" /><xsl:text> - </xsl:text></b>
	</xsl:if>

	<xsl:if test="./frozen = '1'">
		<span class="frozen">(Frozen)</span><xsl:text> - </xsl:text>
	</xsl:if>

	<xsl:value-of disable-output-escaping="yes" select="./name" />
	<xsl:choose>
	<xsl:when test="./description != ''"> : <xsl:value-of disable-output-escaping="yes" select="./description" />
	</xsl:when>
	<xsl:otherwise>
	</xsl:otherwise>
	</xsl:choose>
	</td>

	<td class="amount">
	<xsl:if test="./void = '1'">
		<xsl:attribute name="style">
		text-decoration:line-through</xsl:attribute>
	</xsl:if>
	<xsl:value-of select="./amount" /></td>

	<td class="amount">
	<xsl:if test="./void != '1'">
		<xsl:value-of select="./running_balance" />
	</xsl:if>
	</td>

	</tr>
	</xsl:for-each>
</xsl:when>
<xsl:otherwise>
	<tr><td colspan="5">
	<xsl:value-of select="//base/lang_blocks/messages/no_transactions" />
	</td></tr>
</xsl:otherwise>
</xsl:choose>

<tr class="ending_balance">
<td><xsl:value-of select="//base/data/stop" /></td>
<td></td>
<td>
<xsl:value-of select="//base/lang_blocks/ending_balance" />
</td>
<td colspan="2" class="total">
<xsl:text>$</xsl:text>
<xsl:value-of select="//base/data/ending_balance" />

</td></tr>

<!--/table-->
<!--end transaction report-->


<tr><td colspan="5">
<hr/>

</td></tr>

</table>

<div class="footer"><img src="/images/copyright_.gif" width="173" height="19" alt="Copyright" /></div>
</div>

</body>
</html>
</xsl:template>

<!-- Build a drop-down menu. -->
<xsl:template name="build_menu">
<xsl:param name="fieldname" />
<xsl:param name="values" />
<xsl:param name="onchange_event" />
	<select>
		<xsl:attribute name="name">
		<xsl:value-of select="$fieldname"/></xsl:attribute>
		<!-- Get the onchange event if we have one. -->
		<xsl:attribute name="onchange">
		<xsl:value-of select="$onchange_event"/></xsl:attribute>
		<!-- Get the param value if we have one to restore the field. -->
		<xsl:variable name="selected" select="//base/params/*[name()=$fieldname]"/>
		<!-- create our 'first' option, an instruction -->
		<option value=""><xsl:value-of select="//lang_blocks/drop_down_inst/*[name()=$fieldname]" /></option>
		<!-- Load up all the options. -->
		<xsl:for-each select="//base/menus/*[name()=$values]/*">
			<option>
				<!-- If we have a selection, mark it selected. -->
				<xsl:if test="name()=$selected">
					<xsl:attribute name="selected">
						<xsl:value-of select="'true'"/></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="value">
					<xsl:value-of select="./@value"/></xsl:attribute>
				<xsl:value-of select="."/>
			</option>
		</xsl:for-each>
	</select>
</xsl:template>

</xsl:stylesheet>
