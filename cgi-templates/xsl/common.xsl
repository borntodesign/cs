<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Build a standard input label. -->
<xsl:template name="build_label">
<xsl:param name="fieldname" />
<xsl:param name="fieldtext" />
<xsl:param name="related" />
<xsl:param name="add_class" />
<xsl:param name="style" />
<xsl:param name="xcolon" />
	<xsl:attribute name="class">
		<xsl:if test="$add_class != ''"><xsl:value-of select="$add_class" /><xsl:text> </xsl:text></xsl:if>

		<xsl:call-template name="check_required">
		
		<xsl:with-param name="fieldname">
		<xsl:choose>
			<xsl:when test="$related != ''">
			<xsl:value-of select="$related" />
			</xsl:when>
		  
			<xsl:otherwise>
			<xsl:value-of select="$fieldname" />
			</xsl:otherwise>
		</xsl:choose>
		</xsl:with-param>
		
		</xsl:call-template>
	</xsl:attribute>

	<xsl:if test="$style != ''">
		<xsl:attribute name="style"><xsl:value-of select="$style" /></xsl:attribute>
	</xsl:if>
	<xsl:choose>
	<xsl:when test="$fieldtext"><xsl:value-of select="$fieldtext" /></xsl:when>
	<xsl:otherwise><xsl:value-of select="//lang_blocks/fields/*[name()=$fieldname]"/></xsl:otherwise>
	</xsl:choose>
<xsl:if test="not($xcolon)">:</xsl:if>
</xsl:template>


<!-- Build a standard input field. -->
<xsl:template name="build_input">
<xsl:param name="fieldname" />
<xsl:param name="default_value" />
<xsl:param name="class" />
<xsl:param name="onchange" />
<xsl:param name="onfocus" />
<xsl:param name="onkeypress" />
<xsl:param name="onkeydown" />
<xsl:param name="onkeyup" />
<xsl:param name="style" />
<xsl:param name="id" />
<xsl:param name="readonly" />
<xsl:param name="maxlength" />
<xsl:param name="disabled" />
	<input type="text">
		<xsl:attribute name="class">
		<xsl:choose><xsl:when test="$class !=''"><xsl:value-of select="$class"/></xsl:when>
				<xsl:otherwise>main</xsl:otherwise></xsl:choose></xsl:attribute>

<!-- normally there will be a name, but on the dynamic drop-down, there might not be -->
		<xsl:attribute name="name">
		<xsl:choose>
			<xsl:when test="$fieldname"><xsl:value-of select="$fieldname"/></xsl:when>
		  	<xsl:otherwise></xsl:otherwise>
		</xsl:choose></xsl:attribute>

<!-- concatenate any other events onto this one -->
		<xsl:attribute name="onfocus">this.select()<xsl:if test="$onfocus !=''">;<xsl:value-of select="$onfocus" />;</xsl:if></xsl:attribute>
		<xsl:attribute name="value">
			<xsl:choose>
				<xsl:when test="//params/*[name()=$fieldname] != ''">
					<xsl:value-of select="//params/*[name()=$fieldname]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$default_value" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>

		<xsl:if test="$disabled !=''">
			<xsl:attribute name="disabled"><xsl:value-of select="$disabled"/></xsl:attribute>
		</xsl:if>
		
		<xsl:if test="//fields/*[name()=$fieldname]/@max != ''">
		<xsl:attribute name="maxlength">
		<xsl:value-of select="//fields/*[name()=$fieldname]/@max"/></xsl:attribute>
		</xsl:if>
		
		<xsl:if test="$onchange !=''"><xsl:attribute name="onchange">
		<xsl:value-of select="$onchange"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$onkeypress !=''"><xsl:attribute name="onkeypress">
		<xsl:value-of select="$onkeypress"/></xsl:attribute>
		</xsl:if>
                <xsl:if test="$onkeydown !=''"><xsl:attribute name="onkeydown">
                <xsl:value-of select="$onkeydown"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="$onkeyup !=''"><xsl:attribute name="onkeyup">
                <xsl:value-of select="$onkeyup"/></xsl:attribute>
                </xsl:if>
		<xsl:if test="$style !=''"><xsl:attribute name="style">
		<xsl:value-of select="$style"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$id !=''"><xsl:attribute name="id">
		<xsl:value-of select="$id"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$readonly !=''"><xsl:attribute name="readonly">readonly</xsl:attribute>
		</xsl:if>
		<xsl:if test="$maxlength !=''">
			<xsl:attribute name="maxlength">
				<xsl:value-of select="$maxlength"/>
			</xsl:attribute>
		</xsl:if>
	</input>
</xsl:template>

<!-- Build a password input field. -->
<xsl:template name="build_password_input">
<xsl:param name="fieldname" />
<xsl:param name="default_value" />
<xsl:param name="class" />
<xsl:param name="onchange" />
<xsl:param name="onfocus" />
<xsl:param name="onkeypress" />
<xsl:param name="onkeydown" />
<xsl:param name="onkeyup" />
<xsl:param name="style" />
<xsl:param name="id" />
<xsl:param name="readonly" />
<xsl:param name="maxlength" />
	<input type="password">
		<xsl:attribute name="class">
		<xsl:choose><xsl:when test="$class !=''"><xsl:value-of select="$class"/></xsl:when>
				<xsl:otherwise>main</xsl:otherwise></xsl:choose></xsl:attribute>

<!-- normally there will be a name, but on the dynamic drop-down, there might not be -->
		<xsl:attribute name="name">
		<xsl:choose>
			<xsl:when test="$fieldname"><xsl:value-of select="$fieldname"/></xsl:when>
		  	<xsl:otherwise></xsl:otherwise>
		</xsl:choose></xsl:attribute>

<!-- concatenate any other events onto this one -->
		<xsl:attribute name="onfocus">this.select()<xsl:if test="$onfocus !=''">;<xsl:value-of select="$onfocus" />;</xsl:if></xsl:attribute>
		<xsl:attribute name="value">
			<xsl:choose>
				<xsl:when test="//params/*[name()=$fieldname] != ''">
					<xsl:value-of select="//params/*[name()=$fieldname]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$default_value" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>

		<xsl:if test="//fields/*[name()=$fieldname]/@max != ''">
		<xsl:attribute name="maxlength">
		<xsl:value-of select="//fields/*[name()=$fieldname]/@max"/></xsl:attribute>
		</xsl:if>
		
		<xsl:if test="$onchange !=''"><xsl:attribute name="onchange">
		<xsl:value-of select="$onchange"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$onkeypress !=''"><xsl:attribute name="onkeypress">
		<xsl:value-of select="$onkeypress"/></xsl:attribute>
		</xsl:if>
                <xsl:if test="$onkeydown !=''"><xsl:attribute name="onkeydown">
                <xsl:value-of select="$onkeydown"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="$onkeyup !=''"><xsl:attribute name="onkeyup">
                <xsl:value-of select="$onkeyup"/></xsl:attribute>
                </xsl:if>
		<xsl:if test="$style !=''"><xsl:attribute name="style">
		<xsl:value-of select="$style"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$id !=''"><xsl:attribute name="id">
		<xsl:value-of select="$id"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$readonly !=''"><xsl:attribute name="readonly">readonly</xsl:attribute>
		</xsl:if>
		<xsl:if test="$maxlength !=''">
			<xsl:attribute name="maxlength">
				<xsl:value-of select="$maxlength"/>
			</xsl:attribute>
		</xsl:if>
	</input>
</xsl:template>


<!-- Assigns the fieldname's class as required or not. -->
<xsl:template name="check_required">
<xsl:param name="fieldname" />
<xsl:variable name="app_type" select="//data/params/app_type" />
<xsl:variable name="app_step" select="//data/params/app_step" />
<xsl:variable name="required">


<xsl:if test = "(//data/errors/*[name()=$fieldname]) or
				(//*[name()=$app_type]/*[name()=$app_step]/required/*[name()=$fieldname]
					and //params/*[name()=$fieldname] = '')">1</xsl:if>

</xsl:variable>
<!-- fieldn=<xsl:value-of select="$fieldname" />req=<xsl:value-of select="$required" / -->
<xsl:choose>
	<xsl:when test="$required != ''">field_req</xsl:when>
	<xsl:otherwise>field</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- Choose the element to focus on -->
<!-- The idea is that this will go into an onload event -->
<xsl:template name="choose_focus">
	<xsl:param name="default" />

	<xsl:variable name="field">
	<xsl:choose>
		<xsl:when test="not(//data/errors/*)"><xsl:value-of select="$default" /></xsl:when>
		<!-- There can exist error nodes that are actual HTML messages.
		In that case the child of error will itself have descendents and we will not set the focus. -->
		<xsl:when test="//data/errors/*[1] and not(//data/errors/*[1]/*)">
			<xsl:value-of select="name(//data/errors/*[1])" />
		</xsl:when>
	  
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>

	</xsl:variable>
	<xsl:if test="$field != ''">ck_set_focus('<xsl:value-of select="$field" />')</xsl:if>
</xsl:template>

<!-- Replaces a single placeholder formatted like %%placeholder%%. -->
<xsl:template name="do-replace">
        <xsl:param name="str" />
        <xsl:param name="by" />
        <xsl:param name="skip" />
        <xsl:variable name="first" select="substring-before($str, '%%')" />
        <xsl:variable name="rest" select="substring-after($str, '%%')" />
        <!-- Unless marked to skip it, concatenate the $first part onto the field. -->
        <!-- Only the second call should be marked skip so the placeholder will be dropped. -->
        <xsl:if test="$skip='false'"><xsl:value-of select="concat($first, $by)" /></xsl:if>
        <xsl:choose>
                <xsl:when test="(substring-before($rest, '%%')) or (substring-after($rest, '%%'))">
                	<!-- This call will remove everything up to the second set of %%. -->
                	<xsl:call-template name="do-replace">
                    	<xsl:with-param name="str" select="$rest" />
                        <xsl:with-param name="by" select="''" />
                        <xsl:with-param name="skip" select="'true'" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:value-of select="$rest" />
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>


<xsl:template name="output_errors">
<xsl:for-each select="//data/errors/*">
	<div class="error">
	<xsl:variable name="fieldname" select="name()" />
	<xsl:variable name="fieldval" select="." />
	
<!-- let's see if our error corresponds with a field -->

	<xsl:if test="//lang_blocks/fields/*[name()=$fieldname]">
	  <xsl:value-of select="//lang_blocks/fields/*[name()=$fieldname]" /> - 
	</xsl:if>
	
	<!-- Find the error message based upon the error type returned from the script. -->

  	<xsl:choose>
    <xsl:when test="$fieldval = 'length_error'">
    <!-- Do a substitution of the maxlength field. -->
    <xsl:call-template name="do-replace">
    <xsl:with-param name="str" select="//lang_blocks/errors/*[name()=$fieldval]" />
    <xsl:with-param name="by" select="//fields/*[name()=$fieldname]/@max" />
    <xsl:with-param name="skip" select="'false'" />
    </xsl:call-template>
	</xsl:when>
    <xsl:otherwise>

    <xsl:choose>
      	<xsl:when test="//lang_blocks/errors/*[name()=$fieldval]">

      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/errors/*[name()=$fieldval]"/-->
<xsl:copy-of select="//lang_blocks/errors/*[name()=$fieldval]/text() | //lang_blocks/errors/*[name()=$fieldval]/*" />

      	</xsl:when>
      	<xsl:when test="//lang_blocks/errors/*[name()=$fieldname]">

      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/errors/*[name()=$fieldname]"/-->
<xsl:copy-of select="//lang_blocks/errors/*[name()=$fieldname]/text() | //lang_blocks/errors/*[name()=$fieldname]/*" />

      	</xsl:when>
      	<xsl:otherwise>
      	<!--xsl:value-of disable-output-escaping="yes" select="." /-->
			<xsl:choose>
				<xsl:when test="not(./*)"><xsl:value-of disable-output-escaping="yes" select="." /></xsl:when>
	  			<xsl:otherwise><xsl:copy-of select = "./*"/></xsl:otherwise>
			</xsl:choose>
      	</xsl:otherwise>
    </xsl:choose>

    </xsl:otherwise>
  	</xsl:choose>


	
	</div>
</xsl:for-each>

</xsl:template>










<xsl:template name="output_notices">
<xsl:for-each select="//data/notices/*">
	<div class="notices">
	<xsl:variable name="fieldname" select="name()" />
	<xsl:variable name="fieldval" select="." />
	
<!-- let's see if our error corresponds with a field -->

	<xsl:if test="//lang_blocks/fields/*[name()=$fieldname]">
	  <xsl:value-of select="//lang_blocks/fields/*[name()=$fieldname]" /> - 
	</xsl:if>
	
	<!-- Find the error message based upon the error type returned from the script. -->

  	<xsl:choose>
    <xsl:when test="$fieldval = 'length_error'">
    <!-- Do a substitution of the maxlength field. -->
    <xsl:call-template name="do-replace">
    <xsl:with-param name="str" select="//lang_blocks/notices/*[name()=$fieldval]" />
    <xsl:with-param name="by" select="//fields/*[name()=$fieldname]/@max" />
    <xsl:with-param name="skip" select="'false'" />
    </xsl:call-template>
	</xsl:when>
    <xsl:otherwise>

    <xsl:choose>
      	<xsl:when test="//lang_blocks/notices/*[name()=$fieldval]">
      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/notices/*[name()=$fieldval]"/-->
<xsl:copy-of select="//lang_blocks/notices/*[name()=$fieldval]/text() | //lang_blocks/notices/*[name()=$fieldval]/*" />

      	</xsl:when>
      	<xsl:when test="//lang_blocks/notices/*[name()=$fieldname]">
      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/notices/*[name()=$fieldname]"/-->
<xsl:copy-of select="//lang_blocks/notices/*[name()=$fieldname]/text() | //lang_blocks/notices/*[name()=$fieldname]/*" />

      	</xsl:when>
      	<xsl:otherwise>
      	<!--xsl:value-of disable-output-escaping="yes" select="." /-->
			<xsl:choose>
				<xsl:when test="not(./*)"><xsl:value-of disable-output-escaping="yes" select="." /></xsl:when>
	  			<xsl:otherwise><xsl:copy-of select = "./*"/></xsl:otherwise>
			</xsl:choose>
      	</xsl:otherwise>
    </xsl:choose>

    </xsl:otherwise>
  	</xsl:choose>


	
	</div>
</xsl:for-each>

</xsl:template>


<xsl:template name="output_warnings">

<xsl:for-each select="//data/warnings/*">
	<div class="warnings">
	<xsl:variable name="fieldname" select="name()" />
	<xsl:variable name="fieldval" select="." />
	
<!-- let's see if our error corresponds with a field -->

	<xsl:if test="//lang_blocks/fields/*[name()=$fieldname]">
	  <xsl:value-of select="//lang_blocks/fields/*[name()=$fieldname]" /> - 
	</xsl:if>
	
	<!-- Find the error message based upon the error type returned from the script. -->

  	<xsl:choose>
    <xsl:when test="$fieldval = 'length_error'">
    <!-- Do a substitution of the maxlength field. -->
    <xsl:call-template name="do-replace">
    <xsl:with-param name="str" select="//lang_blocks/warnings/*[name()=$fieldval]" />
    <xsl:with-param name="by" select="//fields/*[name()=$fieldname]/@max" />
    <xsl:with-param name="skip" select="'false'" />
    </xsl:call-template>
	</xsl:when>
    <xsl:otherwise>

    <xsl:choose>
      	<xsl:when test="//lang_blocks/warnings/*[name()=$fieldval]">
      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/warnings/*[name()=$fieldval]"/-->
<xsl:copy-of select="//lang_blocks/warnings/*[name()=$fieldval]/text() | //lang_blocks/warnings/*[name()=$fieldval]/*" />
      	</xsl:when>
      	<xsl:when test="//lang_blocks/warnings/*[name()=$fieldname]">
      	<!--xsl:value-of disable-output-escaping="yes" select="//lang_blocks/warnings/*[name()=$fieldname]"/-->
<xsl:copy-of select="//lang_blocks/warnings/*[name()=$fieldname]/text() | //lang_blocks/warnings/*[name()=$fieldname]/*" />
      	</xsl:when>
      	<xsl:otherwise>
      	<!--xsl:value-of disable-output-escaping="yes" select="." /-->
			<xsl:choose>
				<xsl:when test="not(./*)"><xsl:value-of disable-output-escaping="yes" select="." /></xsl:when>
	  			<xsl:otherwise><xsl:copy-of select = "./*"/></xsl:otherwise>
			</xsl:choose>
      	</xsl:otherwise>
    </xsl:choose>

    </xsl:otherwise>
  	</xsl:choose>


	
	</div>
</xsl:for-each>

</xsl:template>





<!-- Build a drop-down menu. -->
<xsl:template name="build_menu">
<xsl:param name="fieldname" />
<xsl:param name="default_value" />
<xsl:param name="class" />
<xsl:param name="onchange" />
<xsl:param name="onfocus" />
<xsl:param name="style" />
<xsl:param name="id" />
<xsl:param name="cc_exp_start_year" />
<xsl:param name="empty_option" />
<xsl:param name="use_nodeset" />
<xsl:param name="disabled" />
<!-- pass this one in to -NOT- sort lexically but to use the ordering as it appears in the XML -->
<xsl:param name="natural_sort" />
<!-- send in a numeric value to restrain the length of list items to the value... keeps country drop-downs from blowing out in Firefox -->
<xsl:param name="substr" />
	<select>
<!-- normally there will be a name, but on the dynamic drop-down, there might not be -->
		<xsl:attribute name="name">
		<xsl:choose>
			<xsl:when test="$fieldname != ''"><xsl:value-of select="$fieldname"/></xsl:when>
		  	<xsl:otherwise></xsl:otherwise>
		</xsl:choose></xsl:attribute>

		<xsl:attribute name="class">
		<xsl:choose><xsl:when test="$class != ''"><xsl:value-of select="$class"/></xsl:when>
				<xsl:otherwise>main</xsl:otherwise></xsl:choose></xsl:attribute>

		<!-- Get the onchange event from the messages node if we have one. -->
		<!--xsl:attribute name="onchange">
		<xsl:value-of select="//lang_blocks/messages/*[name()=$fieldname]"/></xsl:attribute -->

		<xsl:if test="$onchange != ''"><xsl:attribute name="onchange">
		<xsl:value-of select="$onchange"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$onfocus != ''"><xsl:attribute name="onfocus">
		<xsl:value-of select="$onfocus"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$style != ''"><xsl:attribute name="style">
		<xsl:value-of select="$style"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="$id != ''"><xsl:attribute name="id">
		<xsl:value-of select="$id"/></xsl:attribute>
		</xsl:if>
		
		<xsl:if test="$disabled != ''"><xsl:attribute name="disabled">
		<xsl:value-of select="$disabled"/></xsl:attribute>
		</xsl:if>
		
		<!-- Get the param value if we have one to restore the field. -->
		<!--xsl:variable name="selected" select="//data/params/*[name()=$fieldname]"/-->
		<xsl:variable name="selected">
			<xsl:choose>
				<xsl:when test="//params/*[name()=$fieldname] != ''">
					<xsl:value-of select="//params/*[name()=$fieldname]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$default_value" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>


		<xsl:choose>
	<!-- we will not be creating the data for these next two controls in our xml document -->
		<xsl:when test="$fieldname = 'cc_exp_month'">
			<xsl:call-template name="cc_exp_month" >
				<xsl:with-param name="selected"><xsl:value-of select="$selected"/></xsl:with-param>
				<xsl:with-param name="empty_option"><xsl:value-of select="$empty_option"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		
		<xsl:when test="$fieldname = 'cc_exp_year'">
			
			<xsl:call-template name="cc_exp_year" >
				<xsl:with-param name="selected"><xsl:value-of select="$selected"/></xsl:with-param>
				<xsl:with-param name="empty_option"><xsl:value-of select="$empty_option"/></xsl:with-param>
				<xsl:with-param name="cc_exp_start_year">
			<xsl:choose>
				<xsl:when test="$cc_exp_start_year != ''"><xsl:value-of select="$cc_exp_start_year"/>
				</xsl:when>
			  <!-- we can look for a specific node -->
				<xsl:when test="//timestamp != ''"><xsl:value-of select="substring(//timestamp,21,4)"/>
				</xsl:when>
			<!-- this is less than optimal, but at least we'll have something -->
				<xsl:otherwise>2005</xsl:otherwise>
			</xsl:choose>

				</xsl:with-param>
			</xsl:call-template>
		</xsl:when>
  
		<xsl:otherwise>

		<!-- create our 'first' option, an instruction -->
		<option value="">
		<!-- Since this document was originally created to work with appx,
			there is a predisposition to specific XML pieces which may not be available in other contexts.
			We can work with a parameter in that case, provide the necessary XML, or simply let the first option be empty. -->
			
			<xsl:choose>
				<xsl:when test="$empty_option != ''"><xsl:value-of select="$empty_option" />
				</xsl:when>
			  
				<xsl:otherwise>
					<xsl:value-of select="//lang_blocks/drop_down_inst/*[name()=$fieldname]" />
				</xsl:otherwise>
			</xsl:choose>
		</option>

<!-- allow a dropdown to use a nodeset not named after it -->
		<xsl:variable name="nodeset">
		<xsl:choose>
			<xsl:when test="$use_nodeset"><xsl:value-of select="$use_nodeset"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$fieldname"/></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>

		<!-- load our next option, a favored one, if applicable -->
		<xsl:choose>
		  <xsl:when test="$nodeset='country'">
	<!-- 05/06 Dick doesn't want US to be first on the list anymore -->
			<!--option value="US"><xsl:value-of select="//menus/country/us"/></option-->
		  </xsl:when>
		<!-- we can add other ones as necessary -->
		</xsl:choose>

		<!-- Load up all the options. -->
		<xsl:choose><xsl:when test="$natural_sort=''">
		<xsl:for-each select="//menus/*[name()=$nodeset]/*">
		<xsl:sort select="." data-type="text" order="ascending" />
			<xsl:call-template name="options">
			<!--xsl:with-param name="node" select="." /-->
			<xsl:with-param name="selected" select="$selected" />
			<xsl:with-param name="substr" select="$substr" />
			</xsl:call-template>
		</xsl:for-each></xsl:when>
		<xsl:otherwise>
		<xsl:for-each select="//menus/*[name()=$nodeset]/*">
			<xsl:call-template name="options">
			<!--xsl:with-param name="node" select="." /-->
			<xsl:with-param name="selected" select="$selected" />
			<xsl:with-param name="substr" select="$substr" />
			</xsl:call-template>
		</xsl:for-each></xsl:otherwise></xsl:choose>

			 
		</xsl:otherwise>
		</xsl:choose>
	</select>
</xsl:template>

<xsl:template name="options">
<!--xsl:param name="node" /-->
<xsl:param name="selected" />
<xsl:param name="substr" />
	<option>
		<!-- If we have a selection, mark it selected. -->
		<xsl:if test="name()=$selected or @val=$selected">
			<xsl:attribute name="selected">selected</xsl:attribute>
		</xsl:if>
		<xsl:if test="@disabled">
			<xsl:attribute name="disabled"><xsl:value-of select="@disabled" /></xsl:attribute>
		</xsl:if>	
		<xsl:if test="@class">
			<xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
		</xsl:if>		
		<xsl:attribute name="value">
			<xsl:choose>
			<xsl:when test="@val">
				<xsl:value-of select="@val" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="name()" />
			</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="$substr != ''">
				<xsl:value-of select="substring(.,0,$substr)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</option>
</xsl:template>

<xsl:template name="cc_exp_month">
<xsl:param name="selected" />
<xsl:param name="empty_option" />
<!-- Create our first option, an valueless one.
	In order to allow the label for this to come through as a parameter, a valuable option that would be better not hard-coded here,
	we will check for any value at all even a space which can be used to disable a 'label'. -->
	<option value="">
	
	<xsl:choose>
		<xsl:when test="$empty_option != ''"><xsl:value-of select="$empty_option" />
		</xsl:when>
	  
		<xsl:otherwise>Month</xsl:otherwise>
	</xsl:choose>
	</option>
	
	<xsl:call-template name = "cc_exp_month_options" >
		<xsl:with-param name="cm">1</xsl:with-param>
		<xsl:with-param name="selected"><xsl:value-of select="$selected" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="cc_exp_month_options">
	<xsl:param name="selected" />
	<xsl:param name="cm" />
	<xsl:variable name="ov">

<!-- Since we want to use 2 character values and our simple numeric value is usually only 1,
we will at the 0 in front and just use the raw numeric value when it is 2 characters. -->
		
		<xsl:choose>
			<xsl:when test="string-length($cm) = '1'">
				<xsl:value-of select="concat('0', $cm)" />
			</xsl:when>
		  
			<xsl:otherwise>
				<xsl:value-of select="$cm" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<option>
	        <xsl:attribute  name="value" ><xsl:value-of select="$ov"/></xsl:attribute>
	        
	        <xsl:if test="$selected = $ov"><xsl:attribute  name="selected" >selected</xsl:attribute>
	        </xsl:if>
	        <xsl:value-of select="$ov"/>
	</option>
	
	<xsl:if test = "$cm &lt; 12">
		<xsl:call-template name = "cc_exp_month_options" >
			<xsl:with-param name="cm"><xsl:value-of select="$cm + 1" /></xsl:with-param>
			<xsl:with-param name="selected"><xsl:value-of select="$selected" /></xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template name="cc_exp_year">
<xsl:param name="selected" />
<xsl:param name="empty_option" />
<xsl:param name="cc_exp_start_year" />
<!-- Create our first option, an valueless one.
	In order to allow the label for this to come through as a parameter, a valuable option that would be better not hard-coded here,
	we will check for any value at all even a space which can be used to disable a 'label'. -->
	<option value="">
	
	<xsl:choose>
		<xsl:when test="$empty_option != ''"><xsl:value-of select="$empty_option" />
		</xsl:when>
	  
		<xsl:otherwise>Year</xsl:otherwise>
	</xsl:choose>
	</option>
	
	<xsl:call-template name="cc_exp_year_options">
		<xsl:with-param name="cy"><xsl:value-of select="$cc_exp_start_year" /></xsl:with-param>
		<xsl:with-param name="cnt">1</xsl:with-param>
		<xsl:with-param name="selected"><xsl:value-of select="$selected" /></xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template name="cc_exp_year_options">
	<xsl:param name="selected"/>
	<xsl:param name="cy"/>
	<xsl:param name="cnt"/>
	<option>
	        <xsl:attribute name="value" ><xsl:value-of select="$cy"/></xsl:attribute>
	        
	        <xsl:if test="$selected = $cy"><xsl:attribute name="selected">selected</xsl:attribute>
	        </xsl:if>
	        <xsl:value-of select="$cy" />
	</option>
	
	<xsl:if test="$cnt &lt; '11'">
		<xsl:call-template name="cc_exp_year_options">
			<xsl:with-param name="cy"><xsl:value-of select="$cy + 1" /></xsl:with-param>
			<xsl:with-param name="selected"><xsl:value-of select="$selected" /></xsl:with-param>
			<xsl:with-param name="cnt"><xsl:value-of select="$cnt + 1" /></xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template name="choose_next_step">
<xsl:param name="app_type" />
<xsl:param name="app_step" />
<!-- we can allow the program to coerce the 'next step' or allow the XSLT to select it -->
	<input type="hidden" name="next_step"><xsl:attribute name="value">
	<!--xsl:value-of select="//params/next_step | //*[name()=$app_type]/*[name()=$app_step]/next_step" /-->
	<xsl:choose><xsl:when test="//params/next_step">
				<xsl:value-of select="//params/next_step" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//*[name()=$app_type]/*[name()=$app_step]/next_step" />
			</xsl:otherwise>
	</xsl:choose>
	</xsl:attribute></input>
</xsl:template>

<!-- build a control combination to make the dynamic state drop-down work -->
<xsl:template name="build_dynamic_state">
<xsl:param name="fieldname" />
<xsl:param name="default_value" />
<xsl:param name="class" />
<!-- the onchange and onfocus values will apply only to the text box -->
<xsl:param name="onchange" />
<xsl:param name="onfocus" />
<xsl:param name="style" />
<!-- send in a numeric value to restrain the length of list items to the value... keeps state drop-downs from blowing out in Firefox -->
<xsl:param name="substr" />
<!-- if we have data with which to build a 'state' dropdown,
then we will adjust our text control -->

<xsl:choose>
	<xsl:when test="//menus/state/*">
<!-- if we have nodes to complete a drop-down menu, then we'll proceed with this section -->
		<xsl:call-template name="build_menu" >
			<xsl:with-param name="fieldname"><xsl:value-of select="$fieldname" /></xsl:with-param>
			<xsl:with-param name="default_value"><xsl:value-of select="$default_value" /></xsl:with-param>

			<xsl:with-param name="style">display: inline</xsl:with-param>
			<xsl:with-param name="id">state_select</xsl:with-param>
			<xsl:with-param name="onfocus"><xsl:value-of select="$onfocus" /></xsl:with-param>
			<xsl:with-param name="substr"><xsl:value-of select="$substr" /></xsl:with-param>
			<!--xsl:if test="$class"-->
			<xsl:with-param name="class"><xsl:value-of select="$class" /></xsl:with-param>
			<!--/xsl:if-->
		</xsl:call-template>
		
		<xsl:call-template name="build_input" >
			<xsl:with-param name="fieldname"></xsl:with-param>
			<xsl:with-param name="style">display: none</xsl:with-param>
			<xsl:with-param name="id">state_text</xsl:with-param>
			<!--xsl:if test="$class"-->
			<!--xsl:with-param name="class"><xsl:value-of select="$class" /></xsl:with-param-->
			<!--/xsl:if-->
			<!--xsl:if test="$onchange"-->
			<xsl:with-param name="onchange"><xsl:value-of select="$onchange" /></xsl:with-param>
			<!--/xsl:if-->
			<!--xsl:if test="$onfocus"-->
			<xsl:with-param name="onfocus"><xsl:value-of select="$onfocus" /></xsl:with-param>
			<!--/xsl:if-->
		</xsl:call-template>
	</xsl:when>
  
	<xsl:otherwise>
<!-- if we don't have nodes to complete a drop-down menu, then we'll proceed with this section -->
		<xsl:call-template name="build_menu" >
			<xsl:with-param name="style">display: none</xsl:with-param>
			<xsl:with-param name="id">state_select</xsl:with-param>
			<!--xsl:if test="$class"-->
			<xsl:with-param name="class"><xsl:value-of select="$class" /></xsl:with-param>
			<!--/xsl:if-->
		</xsl:call-template>
		
		<xsl:call-template name="build_input" >
			<xsl:with-param name="fieldname"><xsl:value-of select="$fieldname" /></xsl:with-param>
			<xsl:with-param name="style">display: inline</xsl:with-param>
			<xsl:with-param name="id">state_text</xsl:with-param>
			<xsl:with-param name="class"><xsl:value-of select="$class" /></xsl:with-param>
			<xsl:with-param name="onchange"><xsl:value-of select="$onchange" /></xsl:with-param>
			<xsl:with-param name="onfocus"><xsl:value-of select="$onfocus" /></xsl:with-param>
		</xsl:call-template>
	
	</xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>
