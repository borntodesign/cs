<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
	<xsl:template match="/">	
	
	<xsl:choose>
	<!--response code -1 resp_reason_code -1 is an in-house response to a member name/CC name mismatch -->
		<xsl:when test="//resp_code = -1 and //resp_reason_code = -1">
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/name-mismatch-1" /></div>
		</xsl:when>
	<!-- response code 4 means authorized but awaiting Merchant review -->
		<xsl:when test="//resp_code = 4">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/awaiting_approval" /></div>
			<xsl:call-template name = "instructions" />
		</xsl:when>
		<xsl:when test="//resp_reason_code &gt;= 2 and //resp_reason_code &lt;= 4">
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/declined" /></div>
		</xsl:when>
		
		<xsl:when test="//resp_reason_code = 6 or //resp_reason_code = 37">
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/invalid_cc_number" /></div>
		</xsl:when>

		<xsl:when test="//resp_reason_code = 11">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/*[@code=//resp_reason_code]" /></div>
			<xsl:call-template name="instructions" />
		</xsl:when>
		
		<xsl:when test="//resp_reason_code &gt;= 19 and //resp_reason_code &lt;= 26">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/processing_error_try_again" /></div>
		</xsl:when>
		
		<xsl:when test="//resp_reason_code = 27">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/*[@code=//resp_reason_code]" /></div>
		<!-- although this is an avs error, Dick has chosen to deal with this one uniformly -->
			<!--xsl:call-template name="avs_error" /-->
		</xsl:when>
		
		<xsl:when test="//resp_reason_code = 44">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/card_code_invalid" /></div>
			<xsl:call-template name="instructions" />
		</xsl:when>
		
		<xsl:when test="//resp_reason_code = 45">
			<xsl:call-template name="avs_error" />
		</xsl:when>

		<xsl:when test="//resp_reason_code = 65">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/card_code_invalid" /></div>
			<xsl:call-template name="instructions" />
		</xsl:when>
		
		<xsl:when test="//resp_reason_code = 78">
			<xsl:call-template name = "error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/card_code_invalid" /></div>
			<xsl:call-template name="instructions" />
		</xsl:when>
		
	<!-- this is the case where we have an xml node with a matching code attribute -->
		<xsl:when test="//lang_blocks/errors/*[@code=//resp_reason_code]">
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/*[@code=//resp_reason_code]" /></div>
		</xsl:when>

		<xsl:otherwise>
			<xsl:call-template name="error_code" />
			<div class="auth_err_text"><xsl:value-of select="//lang_blocks/errors/generic" /></div>
			<xsl:call-template name="instructions" />
			<div class="auth_err_text"><xsl:value-of select="concat(//extra_info, ': ', //resp_reason_text)" /></div>
		</xsl:otherwise>
		
	</xsl:choose>
	</xsl:template>
	
	<xsl:template name="avs_error">
			<xsl:call-template name="error_code" />

		<div class="auth_avs_err"><xsl:value-of select="//lang_blocks/avs_codes/*[@code = //avs_result_code]" /></div>
		<xsl:if test="//lang_blocks/avs_codes/*[@code = //avs_result_code and @info = 1]">
			<div class="auth_error_instr"><xsl:value-of select="//lang_blocks/follow_instr" />:</div>
			<xsl:call-template name = "instructions" />
		</xsl:if>

	</xsl:template>
	
	<xsl:template name="error_code">
		<div class="auth_error_code"><xsl:value-of select="//lang_blocks/error_code" />:
		<xsl:value-of select="concat(//resp_code, ' - ', //resp_reason_code)" />
		<xsl:if test="//avs_result_code != ''"> - <xsl:value-of select="//avs_result_code" /></xsl:if>
		</div>
		<xsl:if test="//transaction_id != ''">
			<div class="auth_trans_id"><xsl:value-of select="//lang_blocks/trans_id" />:
			<xsl:value-of select="//transaction_id" /></div>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="instructions">
		<div class="auth_error_instr"><xsl:value-of select="//lang_blocks/instr1" /><xsl:text>
		</xsl:text><xsl:value-of select="//lang_blocks/instr2" /><br />
		<a href="mailto:acf1@dhs-club.com"><xsl:value-of select="//lang_blocks/contact_link"/></a></div>
	</xsl:template>
</xsl:stylesheet>