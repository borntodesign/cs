<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="https://www.clubshop.com/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes" media-type="text/html" />

<!-- This is for cgi/registration.cgi ... the merchant application -->

		<xsl:include href = "/home/httpd/cgi-templates/xsl/common.xsl" />

	<xsl:template match="/" >
<html>
<head>
<title><xsl:value-of select="//lang_blocks/title" /></title>
<link type="text/css" rel="stylesheet" href="/css/club-rewards-general.css" />
<link type="text/css" rel="stylesheet" href="/css/registration.css" />

<script type="text/javascript" src="/js/resource.js"></script>
<script type="text/javascript" src="/js/state_list.js"></script>
<script type="text/javascript" src="/js/getstates.js"></script>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-plugins/jquery.maxlength.js"></script>
<script type="text/javascript" src="/js/registration.js"></script>
<script type="text/javascript" src="/js/tooltip.js"></script>
  <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<style>
		.tip_trigger{cursor:help;}
		
		.tip {
			color: #FFF;
			background:#00376F;
			display:none; /*--Hides by default--*/
			padding:10px;
			position:absolute;    z-index:1000;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			width: 225px;
			text-align:left;
		}
   
 .Orange_Large_11{
      color: rgb(252, 139, 8);
      font-family: "Verdana", "Arial", "Helvetica", sans-serif;
      font-size: 11px;
      font-weight: 700;
      text-transform: uppercase;
     }

   </style>
	
</head>

<body background-image="none">
  <xsl:attribute name="onload">adjustInitialStateCtlFocus(<xsl:if test="//data/step = 2">'cc_state'</xsl:if>);setSelectPrompt('- Please select from this list -');</xsl:attribute>

              <div align="left"><span class="Orange_Large_11"><xsl:value-of select="//lang_blocks/heading" />           	</span>

              </div>
              <br />

              <!-- This is a bit of a hack, but we are probably going to have to redo this form some time in the near future any way. -->
<!--  Start checking for not_enough_reward_cards here -->

				<xsl:choose>
					<xsl:when test="//data/errors/not_enough_reward_cards">
					<br />
					<br />
					 <table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
					 	<tr>
					 	<td>
						<xsl:value-of select="//lang_blocks/more_club_reward_cards" />
						<br />
						<br />
						<xsl:value-of select="//lang_blocks/more_club_reward_cards_two" />
						<br />
						<br />
						<xsl:value-of select="//lang_blocks/contact_sponsor_for_club_reward_cards" />
						<br />
						<br />
						<xsl:value-of select="//lang_blocks/more_club_reward_cards_number_needed" />: <xsl:value-of select="//data/errors/not_enough_reward_cards" />
						<br />
						<xsl:value-of select="//lang_blocks/more_club_reward_cards_agent" />: <xsl:value-of select="//data/errors/not_enough_reward_cards_sponsors_name" />
						<xsl:text> </xsl:text>
						<a>
							<xsl:attribute name="href">mailto:<xsl:value-of select="//data/errors/not_enough_reward_cards_sponsors_email" /></xsl:attribute>
							<xsl:value-of select="//data/errors/not_enough_reward_cards_sponsors_email" />
						</a>
						
						</td>
						</tr>
					</table>

					</xsl:when>
					<xsl:otherwise>
<!-- Start checking for not_enough_reward_cards xsl:otherwise here -->

              <div align="center">

              	<span class="Text_Body_Content">
              		
           
                    <xsl:if test="//data/step= 1">
                	<xsl:value-of select="//lang_blocks/introduction" /><br /><xsl:value-of select="//lang_blocks/or" />
                    	<br />
                    </xsl:if>
                    	<xsl:value-of select="//lang_blocks/print_form_language" /><xsl:text> </xsl:text><a target="_blank" href="/print/clubshop_merchant_application.pdf" class="nav_blue_line"><xsl:value-of select="//lang_blocks/click_here" /></a>
                	<br />
                	
                </span>
              </div>
              
              <table width="70%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">
              
              
                <tbody>
                  <tr>
                    <td colspan="3" align="center" valign="top" bgcolor="#FCA641" class="Text_Body_Content" background="/images/bg/bg_header-orange-923x23.png">
                    	<span class="style13"><xsl:value-of select="//lang_blocks/form_heading" /></span>
                    	<br />
                    	
                    </td>
                    </tr>
                  <tr>
                    <td colspan="3" align="center" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content">
                    

              




<xsl:if test="//data/step != 3">
                    <span class="style5">
	                    <xsl:value-of select="//lang_blocks/required_fields" />
	                </span>
                    
                    <hr align="left" width="100%" size="2" color="#FCBF12" />
</xsl:if>                    
                    <xsl:if test="//data/errors/*">
                    <div align="left">
						<xsl:call-template name="output_errors" />
					</div>
						<hr align="left" width="100%" size="2" color="#FCBF12" />
					</xsl:if>
                    
                    
                    <form id="form1" name="form1" method="post" action="/cgi/registration.cgi">
                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    
                    
                    
						<xsl:choose>
							
								<xsl:when test="//data/step = 3">
								
								<xsl:choose>
									<xsl:when test="//params/payment_method = 'cc' or not(//params/payment_method)">
												<tr>
													<td colspan="2">
														<xsl:value-of select="//lang_blocks/step_3/cc_accepted" />
														<br /><br />
														<xsl:value-of select="//lang_blocks/step_3/username" />: <xsl:value-of select="//params/username" />
														<br />
														<xsl:value-of select="//lang_blocks/step_3/password" />: <xsl:value-of select="//params/password" />
														<br />
														<xsl:value-of select="//lang_blocks/step_3/merchant_number" />: <xsl:value-of select="//params/merchant_master_id" />
														<br /><br />
<a class="nav_blue_line" target="_parent">
<xsl:attribute name="href">/cgi/LoginMerchants.cgi?_action=login&amp;merchantid=<xsl:value-of select="//params/merchant_master_id" />&amp;masterbranch=maf&amp;destination=/maf/&amp;username=<xsl:value-of select="//params/username" />&amp;password=<xsl:value-of select="//params/password" /></xsl:attribute>
<xsl:value-of select="//lang_blocks/step_3/login" />
</a>
														<br /><br />
														<xsl:value-of select="//lang_blocks/complimentary_closing" />
														<br /><br />
														<xsl:value-of select="//lang_blocks/clubshop_signature" />
													</td>
												</tr>
									</xsl:when>
									<xsl:when test="//params/payment_method = 'ec'">
										<tr>
											<td colspan="2">
												<xsl:choose>
													<xsl:when test="//params/ec_number">
																												<xsl:value-of select="//lang_blocks/step_3/active_ec_pending_registration" />
														<br /><br />
														<xsl:value-of select="//lang_blocks/step_3/ec_add_funds" /><xsl:text> </xsl:text>
														<a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" class="nav_orange">ecoPayz</a>.
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="//lang_blocks/step_3/ec_pending" />
														<br /><br />
														<xsl:value-of select="//lang_blocks/step_3/ec_link_language" /><xsl:text> </xsl:text>
														<a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" class="nav_orange">ecoPayz</a>.
														
													</xsl:otherwise>
												</xsl:choose>
												<br /><br />
												<xsl:value-of select="//lang_blocks/complimentary_closing" />
												<br /><br />
												<xsl:value-of select="//lang_blocks/clubshop_signature" />
											</td>
										</tr>
									</xsl:when>
									<xsl:when test="//params/payment_method = 'if'">
										<tr>
											<td colspan="2" align="left">
												<!--  xsl:value-of select="//lang_blocks/step_3/if_pending" / -->
												
												<!--  br /><br / -->
												<xsl:choose>
												<xsl:when test="//params/merchant_type='offline_rewards'">
												<xsl:value-of select="//lang_blocks/step_3/if_pending_offline_rewards" />
												<br /><br />
												<xsl:value-of select="//lang_blocks/pending_payment_information_p2" />
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="//lang_blocks/payment_explanations/int_fund_transfer" />
												</xsl:otherwise>
												</xsl:choose>
												
												
												<br /><br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_bank_name" /></span>: <xsl:value-of select="//banks/bank_of_ireland/name" />
												<br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_bank_address" /></span>: <xsl:value-of select="//banks/bank_of_ireland/address" />
												<br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_sorting_code" /></span>: <xsl:value-of select="//banks/bank_of_ireland/sorting_code" />
												<br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_account_number" /></span>: <xsl:value-of select="//banks/bank_of_ireland/account_number" />
												<br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_iban" /></span>: <xsl:value-of select="//banks/bank_of_ireland/iban" />
												<br />
												<span class="style5 field"><xsl:value-of select="//lang_blocks/int_funding_pending_bic_number" /></span>: <xsl:value-of select="//banks/bank_of_ireland/bic" />
												
											</td>
										</tr>

									</xsl:when>
									<xsl:otherwise>
										<tr>
											<td colspan="2">

												<xsl:choose>
												<xsl:when test="//params/merchant_type='offline_rewards'">
												<xsl:value-of select="//lang_blocks/step_3/ca_pending_offline_rewards" />
												</xsl:when>
												<xsl:otherwise>
												<xsl:value-of select="//lang_blocks/step_3/ca_pending" />
												<br />
												<br />
												<xsl:value-of select="//lang_blocks/step_3/ca_pending_p2" />
												</xsl:otherwise>
												</xsl:choose>
											</td>
										</tr>
									</xsl:otherwise>
									
									
								</xsl:choose>
								
								
								
								
							</xsl:when>
							
						
							<xsl:when test="//data/step = 2">

						<tr>
							<td colspan="2" align="left">
								<span class="text_orange">
									<xsl:value-of select="//lang_blocks/choose_business_cats" />
								</span>
								<ul>
									<li>
										<xsl:value-of select="//lang_blocks/choose_business_cat_instructions1" />
									</li>
									<li>
										<xsl:value-of select="//lang_blocks/choose_business_cat_instructions2" />
									</li>
									<!--<li>
										<xsl:value-of select="//lang_blocks/choose_business_cat_instructions3" />
									</li>-->
								</ul>

								<iframe id="category_search" name="category_search" src="/cgi/biz_cat_search_merchant.cgi" scrolling="auto" width="100%" height="130" frameborder="0" allowtransparency="true"></iframe>
							</td>
						</tr>


						<tr>
							<td align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'business_type'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'biz_description'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
								</xsl:call-template>
								<input type="hidden" name="business_type" id="business_type">
									<xsl:attribute name="value">
										<xsl:value-of select="//params/business_type"/>
									</xsl:attribute>
								</input>
							</td>
						</tr>
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
<tr>
<td colspan="2">
					  <table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<th align="left"><xsl:value-of select="//lang_blocks/merchant_type" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/discount" /></th>
				<!--			<th align="left"><xsl:value-of select="//lang_blocks/bogo" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/ff" /></th> -->
							<th align="left"><xsl:value-of select="//lang_blocks/type_of_discount" /></th>							
							<th align="right"><xsl:value-of select="//lang_blocks/initial_fee" /></th>
				<!--			<th align="right"><xsl:value-of select="//lang_blocks/monthly_fee" /></th>  -->
							<th></th>
						</tr>
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/merchant_type_free" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/benefits_free" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/discount_free" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td> -->
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/free" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/free" />
							</td> -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_free" value="15" >
									<xsl:if test="//params/merchant_package=15">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
<!-- disabling this November 2015 as we are no longer setup to properly handle this
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline/free/merchant_type_free" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline/free/benefits_free" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/free/discount_free" /></td>


							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/tracked" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline/free" />
							</td>
						<td valign="top">
								<input type="radio" name="merchant_package" id="offline_free" value="0" >
									<xsl:if test="//params/merchant_package=0">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>

							</td>
						</tr>
-->
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/merchant_type_off_basic" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/benefits_off_basic" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/discount_basic" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td>  -->

							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/basic" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/basic" />
							</td>  -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_basic" value="14" >
									<xsl:if test="//params/merchant_package=14">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/merchant_type_off_bronze" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/benefits_off_bronze" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/discount_bronze" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td> -->
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/bronze" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/bronze" />
							</td> -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_bronze" value="10" >
									<xsl:if test="//params/merchant_package=10">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/merchant_type_off_silver" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/benefits_off_silver" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/discount_silver" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td> -->
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/silver" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/silver" />
							</td> -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_silver" value="11" >
									<xsl:if test="//params/merchant_package=11">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>

							</td>
						</tr>
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/merchant_type_off_gold" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/benefits_off_gold" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/discount_gold" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>  -->
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/gold" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/gold" />
							</td> -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_gold" value="12" >
									<xsl:if test="//params/merchant_package=12">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>

							</td>
						</tr>
						<tr>
							<td valign="top"><a href="#" class="tip_trigger"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/merchant_type_off_platinum" /><span class="tip"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/benefits_off_platinum" /></span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/discount_platinum" /></td>
						<!--	<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td> -->
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/platinum" />
							</td>
						<!--	<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/platinum" />
							</td> -->
							<td valign="top">
								<input type="radio" name="merchant_package" id="rewards_merchant_platinum" value="13" >
									<xsl:if test="//params/merchant_package=13">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>

							</td>
						</tr>
<!--
<tr>
<td colspan="8" class="small">
<xsl:value-of select="//lang_blocks/index/bogo" />
<br />
<xsl:value-of select="//lang_blocks/index/ff" />
</td>
</tr>
-->
						</table> 
				        

<hr align="left" color="#fcbf12" size="2" width="100%" id="package_options_divider" class="hide"/>        
<!-- Reward Non Tracking Merchant Discount Options -->
               <table width="80%" align="center" id="free_reward_merchant_benefits" class="hide">
				<tr>

					<td valign="top">
						<xsl:value-of select="//lang_blocks/instant_percentage_of_sale_off" />:
					</td>
                                      	<td valign="top">
                                      	<div align="right">
                                      		<xsl:call-template name="build_menu">
							<xsl:with-param name="fieldname" select="'percentage_off'" />
							<xsl:with-param name="id" select="'percentage_off'" />
							<xsl:with-param name="class" select="'registration_dropmenu'" />
							<xsl:with-param name="natural_sort" select="'1'" />
							<xsl:with-param name="disabled"><xsl:if test="not(//params/percentage_off)">disabled</xsl:if></xsl:with-param>
						</xsl:call-template>
<br />
<span class="small"><xsl:value-of select="//lang_blocks/buy_one_get_one_explanation" /></span>
                                    	  </div></td>
                         <td valign="top">
						<input type="radio" name="discount_subtype" id="offline_reward_discounts_percentage_off" value="3" >
							<xsl:if test="//params/percentage_off"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>

					</td>
				</tr>
				      <tr id="free_reward_merchant_benefits_flat_fee" class="hide">
                                        <td valign="top">

                                        	<xsl:value-of select="//lang_blocks/fields/flat_fee_off" />:
                                        </td>
                                        <td valign="top">
                                          <div align="right">
                          			<xsl:call-template name="build_input">
							<xsl:with-param name="fieldname" select="'flat_fee_off'" />
							<xsl:with-param name="id" select="'flat_fee_off'" />
							<xsl:with-param name="class" select="'registration_TextField'" />
							<xsl:with-param name="maxlength" select="'10'" />
							<xsl:with-param name="disabled"><xsl:if test="not(//params/flat_fee_off)">disabled</xsl:if></xsl:with-param>
						</xsl:call-template>
						<br />
<span class="small"><xsl:value-of select="//lang_blocks/flat_fee_off_explanation" /></span>
                                          </div></td>
                                        <td valign="top">
						<input type="radio" name="discount_subtype" id="offline_reward_discounts_flat_fee_off" value="1" >
							<xsl:if test="//params/flat_fee_off"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td valign="top">

                                        	<xsl:value-of select="//lang_blocks/special" />:
                                        </td>
<td>
 <div align="right">
                                      		<xsl:call-template name="build_menu">
												<xsl:with-param name="fieldname" select="'special'" />
												<xsl:with-param name="id" select="'special'" />
												<xsl:with-param name="class" select="'registration_dropmenu'" />
												<xsl:with-param name="natural_sort" select="'1'" />
							<xsl:with-param name="disabled"><xsl:if test="not(//params/special)">disabled</xsl:if></xsl:with-param>
											</xsl:call-template>
						<br />
<span class="small"><xsl:value-of select="//lang_blocks/buy_one_get_one_explanation" /></span>
</div>
</td>
                                        <td valign="top">
						<input type="radio" name="discount_subtype" id="offline_reward_special" value="2" >
							<xsl:if test="//params/special and not(//params/flat_fee_off) and not(//params/percentage_off)"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
                                        </td>

</tr>
                                      </table>
                                      
                                      
                                      
					<table width="80%" border="0" cellpadding="2" cellspacing="0" align="center" id="reward_merchant_benefits" class="hide">
						<tr>
							<td width="30%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'percentage_of_sale'" />
								</xsl:call-template>
								
							</td>
							<td width="70%" align="right">
								<xsl:call-template name="build_menu">
										<xsl:with-param name="fieldname" select="'percentage_of_sale'" />
										<xsl:with-param name="id" select="'percentage_of_sale'" />
										<xsl:with-param name="use_nodeset" select="'registration_percentage_of_sale'" />
										<xsl:with-param name="natural_sort" select="'1'" />
										<xsl:with-param name="class" select="'registration_dropmenu'" />
							</xsl:call-template>
							<br />
							<span class="small"><xsl:value-of select="//lang_blocks/registration_percentage_of_sale_explanation" /></span>
							</td>
						</tr>
                    </table>

</td>
</tr>

<tr>
<td colspan="2">

<hr align="left" width="100%" size="2" color="#FCBF12" />
<table width="80%" border="0" cellpadding="2" cellspacing="0" align="center">

			<tr>
				<td width="50%" align="left" valign="top">
<!-- img src="/images/icons/icon_info_15x15.png" border="0" /> <xsl:text > </xsl:text -->
					<span>
						<xsl:call-template name="build_label">
							<xsl:with-param name="fieldname" select="'discount_blurb'" />
						</xsl:call-template>
					</span>
				</td>
				<td width="50%" align="right">

					<textarea name="discount_blurb" id="discount_blurb" class="registration_TextField" rows="5">
                          		<xsl:if test="//params/discount_blurb = 0">
                          		     <xsl:attribute name="disabled">disabled</xsl:attribute>
                          		</xsl:if>
                          		<xsl:attribute name="maxlength"><xsl:value-of select="//discount_blurb/maximum_characters" /></xsl:attribute>
					<xsl:value-of select="//params/discount_blurb" />
					</textarea>

					<p><xsl:value-of select="//lang_blocks/blurb_characters_left" />: <span class="charsLeft"><xsl:value-of select="//discount_blurb/maximum_characters" /></span></p>

				</td>
			</tr>

</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0" align="center" id="payment_types">	

                       <tr>
                        <td colspan="2">
				<hr align="left" width="100%" size="2" color="#FCBF12" />
                        </td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'payment_method'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
							<xsl:call-template name="build_menu">
								<xsl:with-param name="fieldname" select="'payment_method'" />
								<xsl:with-param name="id" select="'payment_method'" />
								<xsl:with-param name="class" select="'registration_dropmenu'" />
								<xsl:with-param name="onchange" select="'paymentRequirements(this);'" />
							</xsl:call-template>

                          </td>
                        </tr>
                        <tr id="clubAccountNumberRow">
                          <td width="50%" align="left" id="clubAccountNumber">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'ca_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
									<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'ca'">'style5'</xsl:if>
									</xsl:with-param>
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
					<xsl:with-param name="fieldname" select="'ca_number'" />
					<xsl:with-param name="class" select="'registration_TextField'" />
					<xsl:with-param name="maxlength" select="'10'" />
				</xsl:call-template>

                          </td>
                        </tr>
                        
                        <tr id="clubAccountExplanationRow">
                          <td colspan="2" align="left">
                          	<xsl:value-of select="//payment_explanations/club_account" /><xsl:text> </xsl:text><a href="/cgi/converter.cgi" class="nav_orange">https://www.clubshop.com/cgi/converter.cgi 
						  </a> <xsl:text> </xsl:text><xsl:value-of select="//payment_explanations/club_account2" /></td>
                        </tr>
                        <tr id="clubAccountExplanationOfflineRewardsRow">
                          <td colspan="2" align="left">
                          	<xsl:value-of select="//payment_explanations/club_account_offline_rewards" />
                          </td>
                        </tr>                        
                        <tr id="ecoCardAccountNumberRow">
                          <td width="50%" align="left" id="ecoCardNumber">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'ec_number'" />
								<xsl:with-param name="add_class">
									<xsl:if test="//params/payment_method = 'ec'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'ec_number'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'16'" />
							</xsl:call-template>

                          </td>
                        </tr>
                        
                        <tr id="ecoCardExplanationRow">
                          <td colspan="2" align="left" id="ecoCardNumber">
                          	<xsl:value-of select="//payment_explanations/eco_card" />
                          	<br /><br />
                          	<xsl:value-of select="//payment_explanations/eco_card_get_one" /><xsl:text> </xsl:text>-<xsl:text> </xsl:text>
							<a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" class="nav_orange">ecoPayz</a>.
						  </td>
                        </tr>
                        <tr id="creditCardExplanationRow">
                          <td colspan="2" align="left" id="creditCardExplanation">
                          	<xsl:value-of select="//payment_explanations/credit_card" />
                          </td>
                        </tr>
                        <tr id="creditCardDividerRowOne">
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr id="creditCardTypeRow">
                          <td width="50%" align="left" id="creditCard">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_type'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
									</xsl:with-param>
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_menu">
								<xsl:with-param name="fieldname" select="'cc_type'" />
								<xsl:with-param name="id" select="'cc_type'" />
								<!-- xsl:with-param name="onchange" select="'getStates()'" / -->
								<xsl:with-param name="class" select="'registration_dropmenu'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr id="creditCardNumberRow" >
                          <td width="50%" align="left" id="creditCardNumber">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_number'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'cc_number'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'16'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        <tr id="creditCardCCVRow">
                          <td width="50%" align="left" id="ccvNumber">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'ccv_number'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'ccv_number'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'4'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        <tr id="creditCardExpirationDateRow">
                          <td width="50%" align="left" id="cardExpirationDate">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_expiration_date'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
							<xsl:call-template name="build_menu">
								<xsl:with-param name="fieldname" select="'cc_expiration_date'" />
								<xsl:with-param name="use_nodeset" select="'cc_exp'" />
								<xsl:with-param name="class" select="'registration_dropmenu'" />
								<xsl:with-param name="natural_sort" select="'yes'" />

							</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr id="creditCardNameRow">
                          <td width="50%" align="left" id="nameOnCard">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_name_on_card'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'cc_name_on_card'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'100'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        <tr id="creditCardDividerRow">
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr id="creditCardAddressRow">
                          <td width="50%" align="left" id="billingAddress">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_address'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'cc_address'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'100'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr id="creditCardCityRow">
                          <td width="50%" align="left" id="billingCity">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_city'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'cc_city'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'50'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        <tr id="creditCardCountryRow">
                          <td width="50%" align="left" id="billingCountry">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_country'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
                          	
                          </td>
                          <td width="50%" align="right">
							<xsl:call-template name="build_menu">
										<xsl:with-param name="fieldname" select="'cc_country'" />
										<xsl:with-param name="id" select="'country'" />
										<xsl:with-param name="onchange">getStates('cc_state')</xsl:with-param> 
										<xsl:with-param name="class" select="'registration_dropmenu'" />
										<xsl:with-param name="use_nodeset" select="'country'" />
							</xsl:call-template>
                          </td>
                        </tr>
						<tr id="creditCardStateRow">
							<td width="50%" align="left" id="billingState">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'cc_state'" />
									
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
									
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
                          	<xsl:call-template name="build_dynamic_state">
										<xsl:with-param name="fieldname" select="'cc_state'" />
										<xsl:with-param name="onfocus" select="'needs_country()'" />
										<xsl:with-param name="class" select="'registration_TextField'" />
								<!-- if country wasn't required, then we would do the following instead -->
								<!--xsl:with-param name="onchange" select="'getStates()'" /-->
							</xsl:call-template>
                          </td>
                        </tr>
                        <tr id="creditCardPostalCodeRow">
                          <td width="50%" align="left" id="billingZip">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_postalcode'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'cc'">'style5'</xsl:if>
								</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'cc_postalcode'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'20'" />
							</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr id="internationalFundsTransferNumberRow">
                          <td width="50%" align="left" id="internationalFundsTransferNumber">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'if_number'" />
								<xsl:with-param name="id" select="'internationalFundsTransferNumber'" />
								<xsl:with-param name="add_class">
										<xsl:if test="//params/payment_method = 'if'">'style5'</xsl:if>
								</xsl:with-param>
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'if_number'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
							</xsl:call-template>
							
                          </td>
                        </tr>
                        <tr id="internationalFundsTransferExplanationRow">
                          <td colspan="2" align="left" id="internationalFundsTransferExplanation">
                          	<xsl:value-of select="//payment_explanations/int_fund_transfer" />
			   </td>
                        </tr>
                        <tr id="internationalFundsTransferOfflineRewardsExplanationRow">
                          <td colspan="2" align="left" id="internationalFundsTransferOfflineRewardsExplanation">
                          	<xsl:value-of select="//payment_explanations/int_fund_transfer_offline_rewards" />
			   </td>
                        </tr>
                        
                        
                        <tr>
                          <td colspan="2" align="left"><script type="text/javascript"><xsl:comment>
paymentRequirements(document.getElementById("payment_method"));
//</xsl:comment></script>
                          	
                          </td>
                        </tr>
</table>
</td>
</tr>

<tr>
<td colspan="2">
<input name="member_id" type="hidden" id="member_id">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/member_id" />
	</xsl:attribute>
</input>


<input name="referral_id" type="hidden" id="referral_id">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/referral_id" />
	</xsl:attribute>
</input>


<input name="business_name" type="hidden" id="business_name">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/business_name" />
	</xsl:attribute>
</input>

<input name="tin" type="hidden" id="tin">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/tin" />
	</xsl:attribute>
</input>

<input name="firstname1" type="hidden" id="firstname1">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/firstname1" />
	</xsl:attribute>
</input>


<input name="lastname1" type="hidden" id="lastname1">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/lastname1" />
	</xsl:attribute>
</input>


<input name="address1" type="hidden" id="address1">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/address1" />
	</xsl:attribute>
</input>


<input name="city" type="hidden" id="city">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/city" />
	</xsl:attribute>
</input>


<input name="country" type="hidden" id="country">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/country" />
	</xsl:attribute>
</input>


<input name="state" type="hidden" id="state">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/state" />
	</xsl:attribute>
</input>


<input name="postalcode" type="hidden" id="postalcode">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/postalcode" />
	</xsl:attribute>
</input>


<input name="phone" type="hidden" id="phone">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/phone" />
	</xsl:attribute>
</input>


<input name="home_phone" type="hidden" id="home_phone">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/home_phone" />
	</xsl:attribute>
</input>


<input type="hidden" id="scanner_number" name="scanner_number">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/scanner_number" />
	</xsl:attribute>
</input>


<input name="fax" type="hidden" id="fax">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/fax" />
	</xsl:attribute>
</input>


<input name="language" type="hidden" id="language">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/language" />
	</xsl:attribute>
</input>


<input name="emailaddress1" type="hidden" id="emailaddress1">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/emailaddress1" />
	</xsl:attribute>
</input>


<input name="username" type="hidden" id="username" maxlength="20">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/username" />
	</xsl:attribute>
</input>


<input name="password" type="hidden" id="password">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/password" />
	</xsl:attribute>
</input>


<input name="url" type="hidden" id="url">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/url" />
	</xsl:attribute>
</input>


<input name="website_language" type="hidden" id="website_language">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/website_language" />
	</xsl:attribute>
</input>

<input name="percentage_of_sale" type="hidden" id="percentage_of_sale">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/percentage_of_sale" />
	</xsl:attribute>
</input>


<input name="exception_percentage_of_sale" type="hidden" id="exception_percentage_of_sale">
	<xsl:attribute name="value">
		<xsl:value-of select="//params/exception_percentage_of_sale" />
	</xsl:attribute>
</input>

					
					
					
					</td>
</tr>			
							</xsl:when>
							
							
							<xsl:otherwise>
							
						<tr>
						<td colspan="2" align="center">
                                                    <table width="70%" border="0" cellpadding="2" cellspacing="0">
                                                    <tr>
                                                    	<td colspan="2"><span class="style5"><xsl:value-of select="//lang_blocks/fields/please_select_one" /></span></td>
                                                    </tr>
                                                    <tr>
                                                    <td valign="top">
                                                      <input name="_membership" type="radio" id="_membership_member_id" value="member">
                                                        <xsl:if test="//params/member_id">
                                                        <xsl:attribute name="checked">checked</xsl:attribute>
                                                        </xsl:if>
                                                      </input>
                                                    </td>
                                                    <td width="40%" align="left">

                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'member_id'" />
							</xsl:call-template>
							<!-- br /><xsl:value-of select="//lang_blocks/fields/member_id_comment" / -->
						  </td>
                          <td width="50%" align="right" valign="top">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'member_id'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="id" select="'member_id'" />
								<xsl:with-param name="maxlength" select="'10'" />
								<xsl:with-param name="disabled"><xsl:if test="not(//params/member_id)">disabled</xsl:if></xsl:with-param>
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                                                    <td valign="top">
                                                      <input name="_membership" type="radio" id="_membership_referral_id" value="referred">
                                                        <xsl:if test="//params/referral_id">
                                                        <xsl:attribute name="checked">checked</xsl:attribute>
                                                        </xsl:if>
                                                      </input>
                                                    </td>

                          <td width="40%" align="left">

                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'referral_id'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'referral_id'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="id" select="'referral_id'" />
								<xsl:with-param name="maxlength" select="'10'" />
								<xsl:with-param name="disabled"><xsl:if test="not(//params/referral_id)">disabled</xsl:if></xsl:with-param>
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                                                    <td valign="top">
                                                      <input name="_membership" type="radio" id="_membership_new_member" value="new">
                                                        <xsl:if test="not(//params/referral_id) and not(//params/member_id)">
                                                        <xsl:attribute name="checked">checked</xsl:attribute>
                                                        </xsl:if>
                                                      </input>
                                                    </td>
                          <td colspan="2" align="left">
                          	<xsl:value-of select="//lang_blocks/fields/new_member" />
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
</table>
						<table width="60%" border="0" cellpadding="2" cellspacing="0">

                        
                        
                        
                        
                        
                        

                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'business_name'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'business_name'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'35'" />
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
					<xsl:with-param name="fieldname" select="'tax_id'" />
				</xsl:call-template>
							
			  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'tin'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'20'" />
							</xsl:call-template>
						 </td>
                        </tr>

                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'firstname1'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'firstname1'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'50'" />
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'lastname1'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'lastname1'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'50'" />
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr>
                          <td colspan="2" class="Text_Body_Content" valign="top" bgcolor="#FFFFFF" align="center"><xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'baddress1'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template></td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'address1'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'address1'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'100'" />
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'city'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
                          	<xsl:call-template name="build_input">
								<xsl:with-param name="fieldname" select="'city'" />
								<xsl:with-param name="class" select="'registration_TextField'" />
								<xsl:with-param name="maxlength" select="'50'" />
							</xsl:call-template>
						 </td>
                        </tr>
                        <tr>
                          <td width="50%" align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'country'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
                          	
                          </td>
                          <td width="50%" align="right">
							<xsl:call-template name="build_menu">
										<xsl:with-param name="fieldname" select="'country'" />
										<xsl:with-param name="id" select="'country'" />
										<xsl:with-param name="onchange" select="'getStates()'" />
										<xsl:with-param name="class" select="'registration_dropmenu'" />
							</xsl:call-template>
                          </td>
                        </tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'state'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
                          	<xsl:call-template name="build_dynamic_state">
										<xsl:with-param name="fieldname" select="'state'" />
										<xsl:with-param name="onfocus" select="'needs_country()'" />
										<xsl:with-param name="class" select="'registration_TextField'" />
								<!-- if country wasn't required, then we would do the following instead -->
								<!--xsl:with-param name="onchange" select="'getStates()'" /-->
							</xsl:call-template>
                          </td>
                        </tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'postalcode'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'postalcode'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'20'" />
								</xsl:call-template>
							</td>
						</tr>
						
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
 						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'phone'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'phone'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'25'" />
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'home_phone'" />
									
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'home_phone'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'25'" />
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'scanner_number'" />
								</xsl:call-template>
							</td>
							<td width="50%" align="right" valign="top">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'scanner_number'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'25'" />
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'fax'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'fax'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'25'" />
								</xsl:call-template>
							</td>
						</tr>
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'language'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_menu">
	                            	<xsl:with-param name="fieldname" select="'language'" />
	                            	<xsl:with-param name="id" select="'language'" />
	                          		<xsl:with-param name="class" select="'registration_dropmenu'" />
	                          	</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'emailaddress1'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'emailaddress1'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'100'" />
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'username'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'username'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'20'" />
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'password'" />
									<xsl:with-param name="add_class" select="'style5'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_password_input">
									<xsl:with-param name="fieldname" select="'password'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'20'" />
								</xsl:call-template>
							</td>
						</tr>
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'url'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_input">
									<xsl:with-param name="fieldname" select="'url'" />
									<xsl:with-param name="class" select="'registration_TextField'" />
									<xsl:with-param name="maxlength" select="'130'" />
								</xsl:call-template>
							</td>
						</tr>
                        <tr>
							<td width="50%" align="left">
								<xsl:call-template name="build_label">
									<xsl:with-param name="fieldname" select="'website_language'" />
								</xsl:call-template>
								
							</td>
							<td width="50%" align="right">
								<xsl:call-template name="build_menu">
	                            	<xsl:with-param name="fieldname" select="'website_language'" />
	                            	<xsl:with-param name="id" select="'website_language'" />
	                          		<xsl:with-param name="class" select="'registration_dropmenu'" />
	                          		<xsl:with-param name="use_nodeset" select="'language'" />
	                          	</xsl:call-template>
<br />
<br />
							</td>
						</tr>

                        </table>
                        </td>
                        </tr>










                </xsl:otherwise>
			</xsl:choose>
		
<xsl:if test="//data/step != 3">

                        <tr>
                          <td colspan="2" align="center" bgcolor="#FCA541"><input type="image" id="Submit" value="Submit" alt="Submit" src="/images/btn/btn_register.png" /></td>
                          </tr>
                        <tr>
                          <td colspan="2" align="center">
                          <a href="/print/merchant_terms_conditions.pdf" class="nav_orange" target="_blank">
                          	<xsl:value-of select="//lang_blocks/terms_and_conditions" />
                          </a>
                          <br />
                          <xsl:value-of select="//lang_blocks/agree" />
						 </td>
                        </tr>
</xsl:if>                        
                    </table>

			<input name="_step" type="hidden" id="_step">
				<xsl:attribute name="value">
					<xsl:value-of select="//data/step" />
				</xsl:attribute>
			</input>
				
		</form>
			
              
		
                   </td>
                    </tr>
                </tbody>
              </table>

</xsl:otherwise>

</xsl:choose>

</body>
</html>

	</xsl:template>

</xsl:stylesheet>
