<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>

<style type="text/css">
BODY {
	font-family : Arial, Helvetica, sans-serif;
 	background-color:white;
}
table {
 	border-color:#ffffff;
}
td {
 	background-color:#006699;
	alink : #ffffff;
}
.content {
	font-family : Verdana, Arial, sans-serif ;
	font-size : 0.8em;
	color: #004F69;
}
.cat_head{
 font-size:0.9em;
 font-family: Arial, Helvetica, sans-serif;
 color:#ff6600;
 text-align:center;
 font-weight:bold;
 background-color:#ffffff;
	border-bottom : thin White;
}
.nav_top{
 font-size:0.9em;
 font-family: Arial, Helvetica, sans-serif;
 color:white;
 font-weight:bold;
 background-color:#1177aa;
 padding:0 5 0 5;
}
 
 .nav_left:hover{
 	color:#ccffcc;
 	text-decoration:none;
 }
 
 .nav_left{
 	font-size:0.9em;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
 	background-color:#006699;
 	padding:10 10 10 10;
	border-bottom : thin White;
}

a {text-decoration: none;}
 
a:Hover {
	text-decoration: underline;
	color: #0066ff;
}
 
</style>
</head>

<body>

<table border="1" cellpadding="2" cellspacing="0">
  	<tr><td align="center">

		<form action="/cgi/search.cgi">
                <input type="hidden" name="start" value="0"/>
                <input type="hidden" name="qt" value="1"/>
                <input type="hidden" name="perPage" value="10"/>
                <input name="search" size="22" style="font-size:0.7em;"/>
                <xsl:text> </xsl:text><input TYPE="submit" value="Search" name="submit" style="font-size:0.7em;"/>
        </form>
    </td></tr>
  	<tr><td class="nav_left">&gt;&gt;&gt;
      	<a onclick="" class="nav_left" href="/mall/usstores.html" target="_blank">USA 
        Stores - Full List</a>
    </td></tr>
  	<tr><td class="nav_left">&gt;&gt;&gt;
		<a onclick="" class="nav_left" href="/mall/intlstores.html" target="_blank">International 
        Stores - Full List</a>
	</td></tr>
 	<tr><td class="cat_head">
      <div>USA Store Categories</div>
	</td></tr>

	<xsl:for-each select="//menuitems/*">
	<xsl:sort/>
  		<tr><td class="nav_left">&gt;&gt;&gt;
      	        <a onclick="" class="nav_left">
      	        <xsl:attribute name="href">
      	        	href="/mall/cata/<xsl:value-of select="name()"/>.html
      	        </xsl:attribute>
      	        <xsl:value-of select="."/>
      	        </a>
		</td></tr>
	</xsl:for-each>
</table>
</body>
</html>

</xsl:template>
</xsl:stylesheet>