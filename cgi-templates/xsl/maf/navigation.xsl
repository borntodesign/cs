<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template name="build_navigation">
		<xsl:param name="section" />
		<tr>
			<td><img src="/images/general/clubshop_merchant_interface_05.gif" width="172" height="44" alt="" /></td>
			<td>
				<a href="/maf/transaction/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image35','','/images/nav/mi/nav_transaction_2.gif',1)">
					<img alt="Transactions" name="Image35" width="110" height="44" border="0" id="Image35">
						<xsl:attribute name="src">
							<xsl:choose>
								<xsl:when test="$section = 'transaction'">/images/nav/mi/nav_transaction_3.gif</xsl:when>
								<xsl:otherwise>/images/nav/mi/nav_transaction_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
			<td>
				<a href="/cgi/maf/profile/update.cgi" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image36','','/images/nav/mi/nav_profile_2.gif',1)">
					<img alt="Update Profile" name="Image36" width="146" height="44" border="0" id="Image36">
						<xsl:attribute name="src">
							<xsl:choose>
								<xsl:when test="$section = 'profile'">/images/nav/mi/nav_profile_3.gif</xsl:when>
								<xsl:otherwise>/images/nav/mi/nav_profile_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
			<td>
				<a href="/cgi/maf/update_payment.cgi" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image37','','/images/nav/mi/nav_payment_2.gif',1)">
					<img alt="Update Payment" name="Image37" width="115" height="44" border="0" id="Image37">
						<xsl:attribute name="src">
							<xsl:choose>
								<xsl:when test="$section = 'payment'">/images/nav/mi/nav_payment_3.gif</xsl:when>
								<xsl:otherwise>/images/nav/mi/nav_payment_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
			<td>
				<a href="/cgi/maf/setup.cgi" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image38','','/images/nav/mi/nav_setup_2.gif',1)">
					<img alt="Set Up" name="Image38" width="134" height="44" border="0" id="Image38">
						<xsl:attribute name="src">
							<xsl:choose>
								<xsl:when test="$section = 'setup'">/images/nav/mi/nav_setup_3.gif</xsl:when>
								<xsl:otherwise>/images/nav/mi/nav_setup_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
			<td>
				<a href="/maf/faq/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image39','','/images/nav/mi/nav_faq_2.gif',1)">
					<img alt="FAQ" name="Image39" width="138" height="44" border="0" id="Image39">
						<xsl:attribute name="src">
							<xsl:choose>
									<xsl:when test="$section = 'faq'">/images/nav/mi/nav_faq_3.gif</xsl:when>
									<xsl:otherwise>/images/nav/mi/nav_faq_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
			<td>
				<a href="/maf/partner/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image40','','/images/nav/mi/nav_partner_2.gif',1)">
					<img src="/images/nav/mi/nav_partner_1.gif" alt="Partner With Us" name="Image40" width="117" height="44" border="0" id="Image40">
						<xsl:attribute name="src">
							<xsl:choose>
									<xsl:when test="$section = 'partner'">/images/nav/mi/nav_partner_3.gif</xsl:when>
									<xsl:otherwise>/images/nav/mi/nav_partner_1.gif</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</img>
				</a>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
