<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>

   <xsl:template match="/">
<html>
<head>
<title><xsl:value-of select="//lang_blocks/heading"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/pages.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="693" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923" align="center">
  <tr>
    <td valign="top" bgcolor="#FFBC23" class="Text_Content"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" bgcolor="#000000"><span class="header">

        </span></td>
      </tr>
      <tr>
        <td background="images/bg_header.jpg" bgcolor="#FFFFFF"><img src="images/movie_header.jpg" width="640" height="70" /></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">
          <tr>
            <td><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/heading"/></span><br />
                <br />
				<xsl:value-of select="//lang_blocks/p1"/>
				<br />
              	<br />
              <!--<xsl:value-of select="//lang_blocks/p2"/>-->
             </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="top" background="images/bg_2.gif" bgcolor="#FFFFFF"><img src="images/vip_contest_05.gif" width="667" height="39" /></td>
      </tr>
      <tr>
        <td align="left" background="images/vip_contest_06.gif" bgcolor="#FFFFFF">
        	<iframe id="ifrm" name="ifrm" src="vip_contest_data.xml" scrolling="Auto" width="100%" height="242" frameborder="0" allowtransparency="true" />
        </td>
      </tr>
      <tr>
        <td align="left" background="images/bg_1.gif" bgcolor="#FFFFFF"><img src="images/vip_contest_08.gif" width="667" height="25" /></td>
      </tr>
      
      
      
    </table>
    </td>
  </tr>
</table>
</body>
</html>
	</xsl:template>

</xsl:stylesheet>
