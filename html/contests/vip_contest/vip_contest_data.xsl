<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>

   <xsl:template match="/">
        <html>
            <head>
                <title><xsl:value-of select="//lang_blocks/heading"/></title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link href="css/page.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
                <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                    <xsl:for-each select="//contest_data/winners">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="location = 'IT, CT' or location = 'AN, CUR' or location = 'BE, OV' or location = 'SR, SR' or location = 'NL, DR'">
                                        <td width="25%" align="center"><b><xsl:value-of select="location"/></b></td>
                                        <td width="25%" align="center"><xsl:value-of select="location_count"/></td>
                                </xsl:when>
                                <xsl:otherwise>
        	                        <td width="25%" align="center"><xsl:value-of select="location"/></td>
                                        <td width="25%" align="center"><xsl:value-of select="location_count"/></td>
                                </xsl:otherwise>
                           </xsl:choose>
                            <td width="25%" align="center"><xsl:value-of select="vip"/></td>
                            <td width="25%" align="center"><xsl:value-of select="vip_count"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
                <!-- End ImageReady Slices -->
            </body>
        </html>
	</xsl:template>
</xsl:stylesheet>

