﻿<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>

	<!--#include virtual="../common/meta/meta.shtml" -->
	
   
</head>
<body class="blue">
<!-- Top Blue Header Container --><!--#include virtual="../common/body/header.shtml" --><!--End Top Blue Header Container --><!--Main Container -->
<div class="container white">
<div class="row">
<div class="twelve columns"><br />
<h4 class="topTitle">RAPPORTS DE GESTION DE VENTES</h4>
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
<h5 class="liteGrey">RAPPORTS DE COMMISSION</h5>
<br /> 
<ul class="disc">
<li value="0"><a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank">Rapport de revenu</a> - Affiche votre droit de revenu du mois à date.</li>
<li value="0"><a href="http://www.clubshop.com/cgi/net_calcs.cgi?rid=" target="_blank">Rapports de revenu archivés</a> - Affiche les précédents mois (avant Janvier 2012)- Calculs des rémunérations organisationnelles.</li>
<li value="0"><a href="http://www.clubshop.com/cgi/CommStmnt.cgi" target="_blank">Earnings Statement</a> - Affiche comment vos droits actuels sont calculés chaque mois.</li>
</ul>
<h5 class="liteGrey">RAPPORTS DE VENTES D'EQUIPE</h5>
<br /> 
<ul class="disc">
<li value="0"><a href="http://www.clubshop.com/cgi/vip/tree.cgi/team" target="_blank">Rapport d'équipe</a> - Affiche tous les Partners de votre équipe pour lesquels vous obtenez du crédit de Points Payants d'équipe, vers le bas jusqu'au Directeur Executif.</li>
<li value="0"><a href="http://www.clubshop.com/cgi/vip/tree.cgi/grp" target="_blank">Rapport du groupe</a> - Affiche tous les Partners dns votre groupe personnel pour lesquels vous obtenez des credits GRP. </li>
<li value="0"><strong>Listes Membership</strong> - Afficher en 2 différents formats - graphique et csv <br /><br /> 
<ul class="circle">
<li value="0"><a href="/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=m&amp;index=0">Membres</a></li>
<li value="0"><a href="/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=a&amp;index=0">Affiliates</a></li>
<li value="0"><a href="/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=mr&amp;index=0">Commerçants</a></li>
<li value="0"><a href="/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=ag&amp;index=0">Groupes d'Affinité</a></li>
<li value="0"><a href="/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=p&amp;index=0">Partners</a></li>
</ul>
</li>
<li value="0"><a href="http://www.glocalincome.com/cgi/activity_rpt" target="_blank">Rapport d'Activité</a> - Utilise le logiciel personnalisé ClubShop Rewards et est hébergé sur le serveur ClubShop Rewards. Ce rapport est disponible uniquement pour les abonnés PRO, PRO Plus, Premier and Premier Plus. Affiche diverses "activités" qui ont eu lieu au sein de votre équipe, du bas jusqu'à un directeur exécutif.</li>
<li value="0"><a href="http://www.glocalincome.com/cgi/vip/member-followup.cgi">Suivis Partner</a> - Utilise le logiciel personnalisé ClubShop Rewards et est hébergé sur le serveur ClubShop Rewards. Ce rapport est disponible uniquement pour les abonnés PRO, PRO Plus, Premier and Premier Plus. Affiche les informations de contact, d'activité et de suivi pour chaque type membre dans votre équipe.</li>
<!--
<li><b>Rapport de Partners "non payés"</b> - Ce rapport est en cours de révision et est temporairement indisponible. Passe en revue votre rapport généalogique d'équipe pour voir les Partners de votre équipe avec des frais impayés.</li>
--> 
</ul>
<h5 class="liteGrey">RAPPORTS DE VENTES DE CLIENT</h5>
<br /> 
<ul class="disc">
<li value="0"><a href="http://www.clubshop.com/cgi/TransDetails.cgi" target="_blank">Rapport de Transaction</a> - Affiche les transactions personnelles - incluant les frais, les achats coop, etc. -, les montants pour les transactions, les Points Payants et le ClubCash.</li>
<li value="0"><a href="https://www.clubshop.com/cgi/referral_cards.cgi" target="_blank">Cartes ClubShop Reward assignée</a> - Affiche les cartes que vous avez achetées et le numéro ID des cartes que vous avez activées.</li>
<!--
<li><b>Rpport de compensation de référencement de commerçant</b> - Ce rapport est en cours de révision et est temporairement indisponible. Affiche les ventes faites avec les commerçants qui ont un "suivi". </li>
<li><b>Rapport de nouveau consommateur</b> - Ce rapport est en cours de révision et est temporairement indisponible. Affiche la liste de tous les types de membres qui n'ont pas remplis l'enquête ou qui n'ont pas fait leur premier achat.</li>
<li><b>Rapport Personnel</b> - Ce rapport est en cours de révision et est temporairement indisponible. Affiche les Points Payants Personnels et les référencements Rewards.</li>
<li><b>Referral Compensation Report</b> - Ce rapport est en cours de révision et est temporairement indisponible. Ce rapport montre les ventes, les commissions et les points payants gagnés du fait de membres ClubShop Reward personnellement référés qui achètent au centre commercial ClubShop et chez les commerçants Rewards qui suivent leurs ventes.</li>
--> 
</ul>
</div>
</div>
</div>
<!-- container --><!-- container --><!--End Main Container --><!--Footer Container --><!--#include virtual="../common/footer/footer.shtml" --><!--End Footer Container -->
<script type="text/javascript">// <![CDATA[
		$(window).load(function() {
			$('#featured').orbit({
				bullets: true
			});
		});
// ]]></script>
</body>
</html>