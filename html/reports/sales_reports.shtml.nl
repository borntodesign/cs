<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>

	<!--#include virtual="../common/meta/meta.shtml" -->
	
   
</head>
<body class="blue">
<!-- Top Blue Header Container --> <!--#include virtual="../common/body/header.shtml" --> <!--End Top Blue Header Container --> <!--Main Container -->
<div class="container white">
<div class="row">
<div class="twelve columns"><br />




<h4 class="topTitle">VERKOOP MANAGEMENT RAPPORTEN</h4>
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">

<h5 class="liteGrey">COMMISSION RAPPORTEN</h5>
<br /> 
<ul class="disc">
<li><a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank">Inkomsten rapport</a> - Toont uw maand-to-date inkomsten.</li>
<li><a href="http://www.clubshop.com/cgi/net_calcs.cgi?rid=" target="_blank">Gearchiveerde Inkomsten Rapporten</a> - Toont voorgaande maanden (voor januari 2012) Organisatie Pay Calculaties.</li>
<li><a href="http://www.clubshop.com/cgi/CommStmnt.cgi" target="_blank">Earnings Statement</a> - Shows how your actual entitlement is calculated each month.</li>
</ul>


<h5 class="liteGrey">TEAM VERKOOPRAPPORTEN</h5>
<br /> 
<ul class="disc">
<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/team" target="_blank">Team Report</a> - Shows all of the Partners on your team that you get Team Pay Points credit for, down to an Executive Director.</li>
<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/grp" target="_blank">Group Report</a> - Shows all of the Partners in your personal group that you are getting GRP credits for.</li>



<li><strong>Membership Lijsten</strong> - Zichtbaar in twee verschillende indelingen - tabel en csv.<br /><br /> 
<ul class="circle">
<li><a href="/cgi/member_frontline2.cgi?csv_file=0&report_type=m&index=0">Members</a></li>
<li><a href="/cgi/member_frontline2.cgi?csv_file=0&report_type=a&index=0">Affiliates</a></li>
<li><a href="/cgi/member_frontline2.cgi?csv_file=0&report_type=mr&index=0">Winkels/ bedrijven</a></li>
<li><a href="/cgi/member_frontline2.cgi?csv_file=0&report_type=ag&index=0">Non-profit organisaties</a></li>
<li><a href="/cgi/member_frontline2.cgi?csv_file=0&report_type=p&index=0">Partners</a></li>
</ul>
</li>
<li><a href="http://www.glocalincome.com/cgi/activity_rpt" target="_blank">Activiteiten Rapport </a> - Maakt gebruik van maatwerk ClubShop Rewards software en wordt gehost op de ClubShop Rewards server. Dit rapport is alleen beschikbaar voor PRO, 
          PRO Plus, Premier and Premier Plus abonnees. Laat verschillende "activiteiten" zien die hebben plaatsgevonden binnen uw team, tot een Executive Director onderaan.</li>
<li><a href="http://www.glocalincome.com/cgi/vip/member-followup.cgi">Partner Opvolging </a> - Maakt gebruik van maatwerk ClubShop Rewards software en wordt gehost op de ClubShop Rewards server. Dit rapport is alleen beschikbaar voor PRO, 
          PRO Plus, Premier and Premier Plus abonnees. Laat contactinformatie, activiteiten en opvolging informatie zien voor alle member types in uw team.</li>
<!--
<li><strong>Onbetaalde Partners Rapport</strong> - Dit rapport wordt momenteel aangepast en is tijdelijk niet beschikbaar. Bekijk uw Team Genealogie Rapport om de Partners in uw team te zien die nog onbetaald staan.</li>
-->
</ul>
<h5 class="liteGrey">KLANTEN VERKOOPRAPPORTEN</h5>
<br /> 
<ul class="disc">
<li><a href="http://www.clubshop.com/cgi/TransDetails.cgi" target="_blank">Transactie Rapport</a> - Toont persoonlijke transacties, inclusief bijdragen, co-op aankopen, etc., bedragen voor transacties, Pay punten en ClubCash.</li>
<li><a href="https://www.clubshop.com/cgi/referral_cards.cgi" target="_blank">Toegewezen ClubShop Reward kaarten</a> - Toont de kaarten die u heeft gekocht en het ID nummer voor de kaarten die geactiveerd zijn.</li>
<!--<li><strong>Winkel Referral Compensatie Rapport</strong> - Dit rapport wordt momenteel aangepast en is tijdelijk niet beschikbaar. Toont verkopen via tracking Winkels/ bedrijven. </li>
<li><strong>Nieuwe Klanten Rapport</strong> - Dit rapport wordt momenteel aangepast en is tijdelijk niet beschikbaar. Toont lijsten van alle member types die de vragenlijst nog niet hebben ingevuld of nog geen eerste aankoop hebben gedaan.</li>
<li><strong>Persoonlijk Rapport</strong> - Dit rapport wordt momenteel aangepast en is tijdelijk niet beschikbaar. Toont persoonlijke Pay punten en Referral Rewards/ vergoedingen.</li>
<li><strong>Referral Compensatie Rapport</strong> - Dit rapport wordt momenteel aangepast en is tijdelijk niet beschikbaar. Dit rapport toont de verkopen, commissies en pay punten die worden verdient van persoonlijk verwezen ClubShop Reward Members die winkelen via het ClubShop winkelcentrum en bij ClubShop Rewards winkels/ bedrijven die hun verkopen tracken.</li>
-->
</ul>

</div>
</div>
</div>
<!-- container --> <!-- container --> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="../common/footer/footer.shtml" --> <!--End Footer Container -->
<script type="text/javascript">// <![CDATA[
		$(window).load(function() {
			$('#featured').orbit({
				bullets: true
			});
		});
// ]]></script>
</body>
</html>
