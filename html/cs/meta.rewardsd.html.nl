<meta name="keywords" content="ClubShop, Rewards Directory, Winkel, Rewards Handelaren, Lokaal winkelen"/>
<meta name="description" content="ClubShop, Rewards Directory, Winkel, Rewards Handelaren, Lokaal winkelen"/>
<meta name="abstract" content="digital online marketing"/>
<meta http-equiv="content-language" content="NL"/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop"/>
<meta name="publisher" content="clubshop"/>
<meta name="company" content="clubshop"/>
<meta name="revisit-after" content="15 days"/>
<meta name="copyright" content="copyright � clubshop.com, all rights reserved"/>
<meta name="page-topic" content="ecommerce"/>
<meta name="robots" content="follow,index"/>
<meta name="rating" content="all"/>
<meta name="audience" content="general"/>