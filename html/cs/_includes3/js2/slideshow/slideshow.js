
/***********************************************
* Ultimate Fade In Slideshow v2.0- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/


var mygallery=new fadeSlideShow({
	wrapperid: "fadeshow1", //ID of blank DIV on page to house Slideshow
	dimensions: [946, 174], //width/height of gallery in pixels. Should reflect dimensions of largest image
	imagearray: [
		["images/slides/slide_1.png", "index.shtml", "_parent", "Welcome to your GLOCAL INCOME."],
		["images/slides/slide_2.png", "index.shtml", "_parent", "Learn more about how to grow your GLOCAL INCOME business."],
		["images/slides/slide_3.png", "index.shtml", "_parent", "Grab the TOOLS you need to help grow your GLOCAL INCOME home business."],
		["images/slides/slide_4.png", "index.shtml", "_parent", "Check to see your GLOCAL INCOME points status."],
		["images/slides/slide_5.png", "index.shtml", "_parent", "Update your personal GLOCAL INCOME member PROFILE."],
		["images/slides/slide_6.png", "index.shtml", "_parent", "Check your personal GLOCAL INCOME CONTACT list."] //<--no trailing comma after very last image element!
	],
	displaymode: {type:'auto', pause:2500, cycles:0, wraparound:false},
	persist: false, //remember last viewed slide and recall within same session?
	fadeduration: 500, //transition duration (milliseconds)
	descreveal: "ondemand",
	togglerid: ""
})
