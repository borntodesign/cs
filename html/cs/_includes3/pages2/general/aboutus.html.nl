<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop: Over ons</title>
<link href="../../css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/cs/_includes/js/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="/cs/_includes/js/jquery-plugins/jquery.iframe.js"></script>


</head>

<body>
<table bgcolor="#001952" border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td height="185"><form enctype="application/x-www-form-urlencoded" id="form1" method="post" name="form1">
<table bgcolor="#ffffff" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody>
<tr>
<td width="100%">
<table border="0" cellpadding="1" cellspacing="0" width="100%">
<tbody>
<tr>
<td colspan="2" style="border-bottom: 2px dotted #CECECE;" width="100%"><span class="Blue_Bold">Over ons </span></td>
</tr>
<tr>
<td align="left" bgcolor="#e5e5e5" colspan="2" style="border-bottom: 2px dotted #CECECE;">
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" bgcolor="#ffffff" valign="top">ClubShop.com werd in juni 1997 opgericht in Englewood, Florida, USA. ClubShop.com is ontstaan uit het idee om de consumenten geld te laten besparen krachtens een unieke en internationale "kopersclub". Wanneer individuele consumenten de handen in elkaar slaan en een "koperclub" vormen, wordt echte "groepskoopkracht" gecreëerd en dit leidt tot doelmatig en gemakkelijk winkelen en geld besparen. <br />
<ul>
<img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com online winkels en bedrijven vindt men in meer dan 75 verschillende landen wereldwijd <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com heeft meer dan 4 miljoen mensen, non-profit organisaties en bedrijven zien inschrijven voor onze Gratis Lidmaatschap programma's <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com was in de VS te zien in de televisieshow “World’s Greatest” en werd bekeken door meer dan 100 miljoen potentiële gezinnen <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com is beschreven in lokale kranten en tijdschriften wereldwijd <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com heeft wereldwijd consumenten geholpen om geld te besparen op hun online en offline aankopen <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com heeft wereldwijd non-profit organisaties geholpen geld in te zamelen met onze fondsenwerving programma's <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com heeft wereldwijd handelaren en bedrijven geholpen om meer klanten te winnen en te behouden met ons getrouwheidsprogramma <br /><img height="9" src="http://www.clubshop.com/cs/images/icons/icon_bullet_orannge.png" width="39" />ClubShop.com handhaaft een juridisch onberispelijke staat van dienst en een reputatie voor eerlijk en integer zaken doen 
</ul>
<br /><br />
<div align="center"><span class="Movie_Header">
<script language="JavaScript1.2" type="text/javascript">// <![CDATA[
var message="HOE DE WERELD WINKELT EN BESPAART!"
var colorone="#CCCCCC"
var colortwo="#FC8B08"
var flashspeed=40  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('<font color="'+colorone+'">')
for (m=0;m<message.length;m++)
document.write('<span id="changecolor'+m+'">'+message.charAt(m)+'</span>')
document.write('</font>')
}
else
document.write(message)
function crossref(number){
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number)
return crossobj
}
function color(){

//Change all letters to base color
if (n==0){
for (m=0;m<message.length;m++)
//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo
if (n<message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",6000)
return
}
}
function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()
// ]]></script>
</span></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" colspan="2"></td>
</tr>
<tr>
<td align="left" colspan="2" height="30"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td></td>
</tr>
</tbody>
</table>
</form></td>
</tr>
</tbody>
</table>
</body>
</html>