<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>


<title><xsl:value-of select="//lang_blocks/title"/></title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />


</head>
<body>

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="12" valign="top">
<img src="/images/headers/charitable_orgs-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" /></td>
</tr>


<tr>
<td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>
<td colspan="8" bgcolor="#FFFFFF">

<div align="center">
<img src="/images/co/heart.gif" width="27" height="24" /><xsl:text> </xsl:text>
<a href="/cgi/co_donate.cgi"><xsl:value-of select="//lang_blocks/p8"/></a><xsl:text> </xsl:text>
</div>

<p class="a"><xsl:value-of select="//lang_blocks/p1"/></p>
<p class="a"><xsl:value-of select="//lang_blocks/p2"/></p>
<p class="a"><xsl:value-of select="//lang_blocks/p3"/></p>

<p class="a"><xsl:value-of select="//lang_blocks/p4"/><xsl:text> </xsl:text>
<a href="https://www.clubshop.com/cgi/appx.cgi//upg"><xsl:value-of select="//lang_blocks/p4a"/></a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p4b"/><xsl:text> </xsl:text>
<a href="http://www.glocalincome.com/home.html"><xsl:value-of select="//lang_blocks/p4c"/></a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p4d"/>
</p>

<p class="a"><xsl:value-of select="//lang_blocks/p5"/></p>
<p class="a"><xsl:value-of select="//lang_blocks/p6"/></p>
<p class="a"><xsl:value-of select="//lang_blocks/p7"/></p>

<p align="center">
<img src="/images/co/heart.gif" width="27" height="24" /><xsl:text> </xsl:text>
<a href="/cgi/co_donate.cgi"><xsl:value-of select="//lang_blocks/p8"/></a><xsl:text> </xsl:text>

</p>

<p class="a"><xsl:value-of select="//lang_blocks/p9"/></p>




      
</td>
 <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
 </tr>
 <tr> <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
 <tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>
 <tr>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 </tr></table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>