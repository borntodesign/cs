<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p30"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p6"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
            
            
            
            <p class="style24"><u><xsl:value-of select="//lang_blocks/p7"/></u></p>   

       
           <p><xsl:value-of select="//lang_blocks/p8"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p9"/></b></p>
            <p><xsl:value-of select="//lang_blocks/p10"/><xsl:text> </xsl:text>
           <a href="/manual/policies/solicitation.xml"><xsl:value-of select="//lang_blocks/p11"/></a></p>
            <p><xsl:value-of select="//lang_blocks/p12"/></p>
            
            <p class="style24"><u><xsl:value-of select="//lang_blocks/p13"/></u></p> 
            
       <p><xsl:value-of select="//lang_blocks/p14"/></p>
       <p><xsl:value-of select="//lang_blocks/p15"/></p>
        <ul>
        <li class="blue_bullet"><span class="medblue"><xsl:value-of select="//lang_blocks/p27"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27a"/></li><br/>
        <li class="blue_bullet"><span class="medblue"><xsl:value-of select="//lang_blocks/p28"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p28a"/></li><br/>
        <li class="blue_bullet"><span class="medblue"><xsl:value-of select="//lang_blocks/p29"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29a"/></li>       
       </ul>
       <p><xsl:value-of select="//lang_blocks/p31"/></p>
       
       	<ul>
       	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p32"/></li>
       	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p33"/></li>
       	</ul>
            
            <p class="style24"><u><xsl:value-of select="//lang_blocks/p16"/></u></p> 
            
            <p><xsl:value-of select="//lang_blocks/p17"/></p><br/>
       <!--<p><xsl:value-of select="//lang_blocks/p18"/></p>-->
            
            <p class="style24"><u><xsl:value-of select="//lang_blocks/p19"/></u></p>
            
            <p><xsl:value-of select="//lang_blocks/p20"/></p>
            <p><a href="/manual/forms/spousal_letter.html">http://www.clubshop.com/manual/forms/spousal_letter.html</a></p>
            <p><xsl:value-of select="//lang_blocks/p22"/></p> 
            <br/>
            
            
            
            <p class="style24"><u><xsl:value-of select="//lang_blocks/p35"/></u></p>
            <p><xsl:value-of select="//lang_blocks/p36"/></p>
            <p><xsl:value-of select="//lang_blocks/p37"/></p>
            <p><a href="http://www.clubshop.com//manual/forms/sale-transfer_partner-membership.html" target="_blank">http://www.clubshop.com//manual/forms/sale-transfer_partner-membership.html</a></p>
            <br/>
            
            
            
              <p class="style24"><u><xsl:value-of select="//lang_blocks/p23"/></u></p> 
            <p><xsl:value-of select="//lang_blocks/p24"/></p>
       <p><xsl:value-of select="//lang_blocks/p25"/></p>
       
       <br/>
       
       
       
            
                <hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="//www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2017
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
