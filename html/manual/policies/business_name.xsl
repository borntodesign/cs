<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="//www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p6"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p7"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
            
            
      
              

       
           <p><xsl:value-of select="//lang_blocks/p8"/></p>
            <br/><span class="sitemap"><xsl:value-of select="//lang_blocks/p9"/></span>
            <ol>
            <li><xsl:value-of select="//lang_blocks/p10"/></li>
            <li><xsl:value-of select="//lang_blocks/p11"/></li>
            <li><xsl:value-of select="//lang_blocks/p12"/></li>
            </ol>
            <p><xsl:value-of select="//lang_blocks/p13"/></p>
            <p><xsl:value-of select="//lang_blocks/p14"/></p>
            <hr/>
            <span class="sitemap"><xsl:value-of select="//lang_blocks/p15"/><xsl:text> </xsl:text><i><xsl:value-of select="//lang_blocks/p16"/></i><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17"/></span>
            
            <ol>
            <li><a href="/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;"  onFocus="this.blur()"><xsl:value-of select="//lang_blocks/p18"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19"/><xsl:text> </xsl:text><a href="/manual/forms/company_accept.html" target="_blank"><xsl:value-of select="//lang_blocks/p20"/></a> </li>
            <li><xsl:value-of select="//lang_blocks/p21"/></li>
            <li><xsl:value-of select="//lang_blocks/p22"/></li>
            
            
            <ul>
            <li><xsl:value-of select="//lang_blocks/p23"/></li>
            <li><xsl:value-of select="//lang_blocks/p24"/></li>
            <li><xsl:value-of select="//lang_blocks/p25"/></li>
            <li><xsl:value-of select="//lang_blocks/p42"/></li>
            </ul>
          
            <li><xsl:value-of select="//lang_blocks/p26"/></li>
            <li><a href="/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;"  onFocus="this.blur()"><xsl:value-of select="//lang_blocks/p27"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p28"/></li>
            <ul><li><xsl:value-of select="//lang_blocks/p29"/></li>
            <li><xsl:value-of select="//lang_blocks/p30"/><xsl:text> </xsl:text><a href="/manual/forms/company_accept.html" target="_blank"><xsl:value-of select="//lang_blocks/p31"/></a></li>
            
            </ul>
                     
            </ol>
            <p><xsl:value-of select="//lang_blocks/p32"/></p>
            
            <hr/>
           <p><span class="sitemap"><xsl:value-of select="//lang_blocks/p33"/></span></p>
              <p><xsl:value-of select="//lang_blocks/p34"/></p>
              
              <ol>
              <li><a href="/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;"  onFocus="this.blur()"><xsl:value-of select="//lang_blocks/p35"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36"/><xsl:text> </xsl:text> <a href="/manual/forms/company_transfer.html" target="_blank"><xsl:value-of select="//lang_blocks/p37"/></a></li>
             <li><a href="/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;"  onFocus="this.blur()"><xsl:value-of select="//lang_blocks/p35"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38"/><xsl:text> </xsl:text> <a href="/manual/forms/company_transfer.html" target="_blank"><xsl:value-of select="//lang_blocks/p37"/></a></li>
 
              </ol>
              <p><xsl:value-of select="//lang_blocks/p39"/></p>
              
               <p><xsl:value-of select="//lang_blocks/p40"/><xsl:text> </xsl:text><a href="mailto:support1@dhs-club.com?Subject=Business_Name_Policy_Question"><xsl:value-of select="//lang_blocks/p41"/></a></p>
              
              
              
              
              <hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="//www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>