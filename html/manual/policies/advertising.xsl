<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Business Manual</title>
            <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->




<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p6"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p7"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


       
           <p class="text_normal"><xsl:value-of select="//lang_blocks/p8"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p9"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p10"/></p>
            <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p11"/></b></span>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p12"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text>
            <a href="mailto:support1@dhs-club.com?Subject=Advertisment_Approval"><xsl:value-of select="//lang_blocks/p13a"/></a><xsl:text> </xsl:text>
            <xsl:value-of select="//lang_blocks/p13b"/>
            
            </p>
            
            <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p14"/></b></span>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p16"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p17"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p18"/><xsl:text> </xsl:text><a href="/manual/policies/spam.xml" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20"/></p>
            
            
             <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p21"/></b></span>
             <p class="text_normal"><xsl:value-of select="//lang_blocks/p22"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p24"/><xsl:text> </xsl:text><a href="mailto:support1@dhs-club.com?Subject=Media_Advertising"><xsl:value-of select="//lang_blocks/p25"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26"/></p>
            
            
            <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p27"/></b></span>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p28"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p29"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p30"/><br/>
            <a href="http://searchmarketing.yahoo.com/legal/trademarks.php" target="_blank"><xsl:value-of select="//lang_blocks/p31"/></a></p>
            
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p32"/></p>
            
         <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p33"/></b></span>
              <p class="text_normal"><xsl:value-of select="//lang_blocks/p34"/></p>       
            <p class="text_normal"><b><xsl:value-of select="//lang_blocks/p35"/></b></p>
            
            <hr/>
            
            
         </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2017
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>


