<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Business Manual</title>


<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

body {
   background-color: #245923;
} 

</style>

</head>
<body>
<div align="center"> 
<table width="950px" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      
    	
      
      
      <tr>
       <td background="/images/bg_header.jpg"><img src="http://www.clubshop.com/images/gi.jpg" width="640" height="70" align="left"/></td>

      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

          <tr>
            <td align="right">
<a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" 
class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="left">
            
            <span class="Header_Main"><xsl:value-of select="//lang_blocks/p6"/></span>
            
            
                
           <ul>
                 <li><a href="/manual/policies/abusefraud.xml" target="_blank"><xsl:value-of select="//lang_blocks/p29"/></a></li>
      
            <li><a href="/manual/policies/advertising.xml" target="_blank"><xsl:value-of select="//lang_blocks/p8"/></a></li>
            <li><a href="/manual/policies/advcoop.xml" taret="_blank"><xsl:value-of select="//lang_blocks/p28"/></a></li>
            <li><a href="/manual/policies/application.xml" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a></li>
            <li><a href="/manual/policies/business_name.xml" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a></li>
            
            <li><a href="/manual/policies/cancellation.xml" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a></li>
            <li><a href="/manual/policies/cb_activation.xml" target="_blank"><xsl:value-of select="//lang_blocks/p13"/></a></li>
            <li><a href="/manual/policies/communications.xml" target="_blank"><xsl:value-of select="//lang_blocks/p14"/></a></li>
           
            <li><a href="/manual/policies/intl_development.xml" target="_blank"><xsl:value-of select="//lang_blocks/p30"/></a></li>

           
           
            <li><a href="/manual/policies/membership.xml" target="_blank"><xsl:value-of select="//lang_blocks/p15"/></a></li>
            <li><a href="/manual/policies/mbr_processing.xml" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a></li>
            <li><a href="/manual/policies/merch_email_adv.xml" target="_blank"><xsl:value-of select="//lang_blocks/p17"/></a></li>
            <li><a href="/manual/policies/name_usage.xml" target="_blank"><xsl:value-of select="//lang_blocks/p18"/></a></li>
            <li><a href="/manual/policies/promotional_page.xml" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a></li>
            <li><a href="/manual/policies/privacy.xml" target="_blank"><xsl:value-of select="//lang_blocks/p20"/></a></li>
            <li><a href="/manual/policies/purchase_mbrs.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a></li>
            <li><a href="/manual/policies/quality_assurance.xml" target="_blank"><xsl:value-of select="//lang_blocks/p22"/></a></li>
            <li><a href="/manual/policies/return.xml" target="_blank"><xsl:value-of select="//lang_blocks/p23"/></a></li>
            <li><a href="/manual/policies/solicitation.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24"/></a></li>
            <li><a href="/manual/policies/spam.xml" target="_blank"><xsl:value-of select="//lang_blocks/p25"/></a></li>
            <li><a href="/manual/policies/spousal.xml" target="_blank"><xsl:value-of select="//lang_blocks/p26"/></a></li>
            <li><a href="/manual/policies/vipsponsor.xml" target="_blank"><xsl:value-of select="//lang_blocks/p27"/></a></li>
            
            
            
            
            
            </ul>
            </div>
</td>

        </tr>
        
      </table>
     
      </td></tr>


</table></td>
      </tr>
      

    </table>
    </td>
  </tr>
  
  <tr><td align="center"><span class="footer_copyright">| <a href="/manual/index.xml" target="_blank" class="nav_header">Home</a> | <a href="https://www.clubshop.com/p/index.php" target="_blank" class="nav_header">Partner Business Center</a> | <a href="/ppreport.html" target="_blank" class="nav_header">Affiliate Business Center</a> |</span></td></tr>
                <tr><td align="center"><div align="center"><img src="/images/icons/icon_copyright.gif" height="20" width="306" alt="copyright"/></div></td></tr>

</table>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

