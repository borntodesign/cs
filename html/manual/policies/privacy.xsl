<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="//www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p6"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p7"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
            
            
      
             
            <p><xsl:value-of select="//lang_blocks/p8"/></p>
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p10"/></p>
            <p><xsl:value-of select="//lang_blocks/p11"/></p>
            <ul>
            <li><xsl:value-of select="//lang_blocks/p12a"/></li>
            <li><xsl:value-of select="//lang_blocks/p12b"/></li>
            <li><xsl:value-of select="//lang_blocks/p12c"/></li>
            <li><xsl:value-of select="//lang_blocks/p12d"/></li>
            <li><xsl:value-of select="//lang_blocks/p12e"/></li>
            <li><xsl:value-of select="//lang_blocks/p12f"/></li>
            <li><xsl:value-of select="//lang_blocks/p12g"/></li>
            <li><xsl:value-of select="//lang_blocks/p12h"/></li>
            </ul>
            
            
            <p><xsl:value-of select="//lang_blocks/p13"/></p>
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p14"/></p>
            <p><xsl:value-of select="//lang_blocks/p15"/></p>
            
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p16"/></p>
            <p><xsl:value-of select="//lang_blocks/p17"/></p>
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p18"/></p>
            <p><xsl:value-of select="//lang_blocks/p19"/></p>
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p20"/></p>
            <p><xsl:value-of select="//lang_blocks/p21"/></p>
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p22"/></p>
            
            
            <p><xsl:value-of select="//lang_blocks/p23"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank"><xsl:value-of select="//lang_blocks/p24"/></a><xsl:text> </xsl:text>
            <xsl:value-of select="//lang_blocks/p25"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/other/questionnaire.php" target="_blank"><xsl:value-of select="//lang_blocks/p26"/></a>
            <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p27"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p28"/></b></p>
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p29"/></p>
            <p><xsl:value-of select="//lang_blocks/p30"/></p>
            
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p31"/></p>
            <p><xsl:value-of select="//lang_blocks/p32"/></p>
            
            
            
            <p class="medblue bold"><xsl:value-of select="//lang_blocks/p33"/></p>
            <p><xsl:value-of select="//lang_blocks/p34"/></p>
            <p><xsl:value-of select="//lang_blocks/p35"/><xsl:text> </xsl:text> <a href="mailto:legal@clubshop.com"><xsl:value-of select="//lang_blocks/p36"/></a></p>
            
            
            
            
            
            
            
            
          
          
         <hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="//www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>