<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="quality_assurance.xsl" ?>
<lang_blocks>
<p6>Gedragsregels</p6>
<p7>Kwaliteitsnormen verzekeren</p7>
<p8>Het ClubShop Rewards hoofdkwartier zou u kunnen kopieën vragen van de maandelijkse communicaties tussen u en uw organization Partners en leden.</p8>
<p9>Het naleven en onderhouden van deze ClubShop Rewards kwaliteitsnormen is een must om de ClubShop Rewards waarden en standaard te behouden. Alle vragen die ClubShop Rewards HQ u op dat gebied zou kunnen stellen, moeten beantwoord worden binnen 72 uur.</p9>
<p10>Nalatigheid op deze regel zal disciplinaire actie teweeg brengen, gaande van een schorsing of het opschorten van uitbetaling en/of tot het beëindigen van uw lidmaatschap.</p10>

</lang_blocks>