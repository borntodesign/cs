﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="cb_activation.xsl" ?>
<lang_blocks>
<p0>Glocal Income: Your vehicle to establish a Local business and a Global enterprise!</p0>
<p1>Hitro kazalo</p1><p2>Priročnik domov</p2>
<p3>VIP kontrolni center</p3><p4>Člansko info območje</p4>
<p5>Kazalo strani</p5><p6>Pravilniki</p6>
<p7>Pravila aktivacije ClubShop Rewards kartice</p7>
<p8>Smoter teh pravil se nanaša na raven članstva "člana".</p8>
<p9>Aktivacija kartice brez privoljenja imetnika kartice je prepovedana.</p9>
<p10>Bilokatera pritožba o aktivaciji kartice,ki bo prišla od lasnika kartice znotraj 72 ur "dovoljenja za aktiviranje", bo upoštevana. Zato mora vsak član shraniti dokaz o "dovoljenju za aktiviranje".</p10>
<p11>Kršitev teh pravil bodo podvržene takojšnji suspenziji vašega članstva. Ne glede na morebitno še neizplačano provizijo.</p11><p12/><p13/><p14/><p15/><p16/><p17/><p18/><p19/><p20/></lang_blocks>
