<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="//www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p6"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p7"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
            
     
        
           <p><xsl:value-of select="//lang_blocks/p8"/></p>
           <p><xsl:value-of select="//lang_blocks/p9"/></p>
           <p><xsl:value-of select="//lang_blocks/p10"/></p>
           <ol>
           <li><xsl:value-of select="//lang_blocks/p11"/><xsl:text> </xsl:text>[<a href="/manual/business/commission.xml" target="_blank">Link</a>]</li>
           <li><xsl:value-of select="//lang_blocks/p12"/><xsl:text> </xsl:text>[<a href="/manual/business/fees.xml" target="_blank">Link</a>]</li>
           <li><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/intl_pay_agent.html" target="_blank">http://www.clubshop.com/intl_pay_agent.html</a></li>
           <li><xsl:value-of select="//lang_blocks/p14"/><xsl:text> </xsl:text><a href="https://www.paypal.com/worldwide/" target="_blank">Click Here</a></li>
  
           </ol>
           
            <p><xsl:value-of select="//lang_blocks/p15"/></p>
            <p><span class="style24"><xsl:value-of select="//lang_blocks/p16"/></span></p>
            <p><span class="style24"><xsl:value-of select="//lang_blocks/p27"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p28"/></p>
            <ul>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p29"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p29a"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p29b"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p29c"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p29d"/></li>
            </ul>
            
            
            
            <p><span class="style24"><xsl:value-of select="//lang_blocks/p30"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></p>
            <p><xsl:value-of select="//lang_blocks/p31c"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/pax/pa" target="_blank"><xsl:value-of select="//lang_blocks/p31d"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31e"/></p>
            
            <ul>
            <li><img src="http://www.clubshop.com/images/bullet-orange-arrow-10x10.gif"/><xsl:text> </xsl:text><b>
            <a href="http://www.clubshop.com/pay_agent_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p31a"/></a></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31b"/></li>
            </ul>
            
            <ul>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p32"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p33"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p34"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p39a"/></li>
	    <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p42"/><br/> <br/>
	    
	    	<ul>
	    	<li class="half"><xsl:value-of select="//lang_blocks/p47"/></li>
	    	<li class="half"><xsl:value-of select="//lang_blocks/p48"/></li>
	    	<li class="half"><xsl:value-of select="//lang_blocks/p49"/><br/><br/>
	    
	    <ul>
	    	
	    	   	
	    	<li class="black_bullet"><xsl:value-of select="//lang_blocks/p44"/></li>
	    	<li class="black_bullet"><xsl:value-of select="//lang_blocks/p45"/></li>
	    	<li class="black_bullet"><xsl:value-of select="//lang_blocks/p46"/></li>
	    	
	    	</ul>
	    	</li></ul>
	    	
	    
	    
	    
	    </li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p43"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p40"/></li>
<!--<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p38"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41"/> </li>-->


            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p35"/></li>
            <li class="blue_bullet"><xsl:value-of select="//lang_blocks/p36"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/pax/pa" target="_blank"><xsl:value-of select="//lang_blocks/p36d"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36e"/><br/><br/>
            <xsl:value-of select="//lang_blocks/p36f"/><br/><br/>
            
            
            
            
            <xsl:value-of select="//lang_blocks/p36c"/><br/><br/> <xsl:value-of select="//lang_blocks/p36a"/></li>
            <!--<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p37"/><br/><br/><xsl:value-of select="//lang_blocks/p37a"/><br/><br/><xsl:value-of select="//lang_blocks/p37b"/><br/><br/><xsl:value-of select="//lang_blocks/p37c"/></li>-->
            
            </ul>
            
            
            
            
          
            <p><a href="http://www.clubshop.com/pay_agent_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/></p>
            
            
            
            
            
            
            <br/> <br/> <br/> <br/> <br/> 
            
            
           
<hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="//www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2017
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>