﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="cancellation.xsl" ?><lang_blocks><p6>REGLEMENTEN -gedragsregels-</p6><p7>Annuleringsbeleid</p7><p8>Het volgende is een uittreksel van onze annuleringsvoorwaarden van de ClubShop Rewards</p8><p9>Algemene voorwaarden</p9>



<p10>I understand that I may cancel my Global Partner System (GPS) Subscription and downgrade my position to a free Affiliate membership at any time, by completing the form at</p10>

<p11>http://www.clubshop.com/cgi/cancel.cgi</p11>
<p12>en door een e-mail te versturen naar</p12>
<p18>om terugbetaling van mijn betaalde bijdrage te vragen.</p18>

<p13>Wanneer ik er voor kies om mijn lidmaatschap te annuleren begrijp ik dat dit uiterlijk de laatste dag van de maand dient te gebeuren, anders wordt mij de bijdrage van de volgende maand aangerekend. (U bent verantwoordelijk voor het betalen van de Global Partner System (GPS) bijdrage voor de maand waarin u annuleert. Dus, als u opzegt op 1 augustus, bent u de bijdrage van augustus verschuldigd.)</p13><p14>Deze beëindiging zal zorgen dat ik onmiddellijk mijn positie in de organisatie verlies, de resulterende of mogelijke downline uit deze positie en eventuele kortingen of inkomsten verkregen of te worden ontleend, zal ik niet verder in aanmerking komen voor Partner-inkomen als gevolg van mijn opzegging.</p14><p15>Om uw lidmaatschap te beëindigen via onze website, ga naar :</p15><p16 new="1/10/12">Indien u niet over een internetaansluiting beschikt of niet online wilt annuleren, dan kunt u opzeggen per brief, en deze versturen naar het volgende postadres : ClubShop Rewards, 955 Morisson Ave, Englewood, FL 34223. Als u er voor kiest om uw annulering via de post te versturen, zal uw lidmaatschap geannuleerd worden op de datum dat wij uw annulering via de post ontvangen. </p16><p17 new="1/10/12">Annuleringen gaan onmiddellijk van kracht.</p17></lang_blocks>
