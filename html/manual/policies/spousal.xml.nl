<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="spousal.xsl" ?>
<lang_blocks>

<p6>Beleid</p6>
<p7>Echtgenoten</p7>
<p8>Het woord "lid" in dit beleid geldt voor ieder niveau van lidmaatschap in ClubShop Rewards. Dit hoofdstuk ging van kracht op 9 september 2000 en werd herzien op 1 oktober 2003.</p8>
<p9>Als beide echtgenoten lid willen worden van ClubShop Rewards, kunnen ze samen gesponsord worden in een lidmaatschap of kunnen ze elk een individueel lidmaatschap aangaan.</p9>
<p10>Als een lid wenst zijn of haar wederhelft tot het bestaande lidmaatschap toe te voegen, moeten de hieronder beschreven instructies gevolgd worden:</p10>
<p11>Om een echtgeno(o)t(e) tot uw lidmaatschap toe te voegen:</p11>
<p12>De houder van het lidmaatschap moet een</p12>
<p13>gewettigde brief</p13>
<p14>sturen naar ClubShop Rewards HQ, waarin hij/zij verklaart de wederhelft te willen toevoegen aan het lidmaatschap. De brief moet het ID nummer van het lid bevatten en de volgende informatie:</p14>
<p15>Volledige naam van het lid</p15>
<p16>Volledige naam van de echtgenoot(e)</p16>
<p17>Een verklaring door het lid dat hij/zij wenst de wederhelft toe te voegen tot het lidmaatschap.</p17>
<p18>Een verklaring door de wederhelft dat hij/zij akkoord gaat met de algemene voorwaarden van ClubShop Rewards (als Partner) en ermee akkoord gaat het lidmaatschap uit te oefenen met zijn/haar echtgeno(o)t(e).</p18>
<p19>Voorbeeldbrief</p19><p20>Deze gewettigde brief moet verzonden worden naar: ClubShop Rewards, 955 Morisson Avenue, Englewood, FL USA 34223.</p20>
</lang_blocks>