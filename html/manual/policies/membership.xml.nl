<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="membership.xsl" ?><lang_blocks>
  <p10>Voor meer informatie over dit gegeven, bekijk</p10>
  <p11>Wervingsbeleid</p11>
  <p12>In het geval waarbij 1 of meerdere Member-, Affiliate- of Partner lidmaatschappen worden gevonden, zal het laatste lidmaatschap worden beschouwd als de keuze van hun sponsoring en het eerste lidmaatschap zal worden beëindigd.</p12>
  <p13>Partners</p13>
  <p14>Partners bij ClubShop Rewards moeten de wettelijke leeftijd hebben in de staat (of voor internationale leden, land of provincie) waar zij verblijven.</p14>
  <p15>Vanaf het moment dat een Member een Partner is geworden, is het niet meer toegestaan om een ander lidmaatschap of sponsoring te nemen. Indien een Partner niet langer onder de hoede wil blijven van de persoon onder wie men is ingeschreven, dan moet men het lidmaatschap opzeggen en volgens onderstaande wachttijden, opnieuw een lidmaatschap aanvragen.</p15>
  <p19>Toevoeging van echtgenoot(e) aan bestaand lidmaatschap</p19>
  <p2>Home Handleiding</p2>
  <p20>Ik begrijp dat ik mijn echtgeno(o)t(e) kan toevoegen aan mijn lidmaatschap, dit door middel van een aanvraag gericht aan het ClubShop Rewards hoofdkantoor met vermelding van mijn bedoelingen.</p20>
  <p21>http://www.clubshop.com/manual/business/forms/spousal_letter.html</p21>
  <p22>Dit formulier dient te worden gewettigd en moet de namen bevatten van de Member en van diegene die aan het lidmaatschap toegevoegd moet worden en (alleen voor Amerika) moet het de social security nummers bevatten van alle betrokken partijen. Het moet ondertekend worden door alle betrokken partijen. Na ontvangst van de documenten en goedkeuring van ClubShop Rewards, zal het lidmaatschap toegevoegd worden.</p22>
  <p23>Adres verplichting</p23>
  <p24>Om het lidmaatschap actief te houden, zijn members verplicht om binnen een jaar na inschrijving als member, het ClubShop Rewards hoofdkantoor op de hoogte te stellen van de woonplaats, provincie, postcode en het land waarin zij wonen. Abonnees op het Global Partner System (GPS) zijn verplicht alle informatie te geven zoals gevraagd wordt op het Global Partner System (GPS) inschrijvingsformulier.</p24>
  <p25>Door de verplichting om de juiste woonplaats, provincie, land en postcode op te geven kan het ClubShop Rewards hoofdkantoor efficiënt communiceren met onze leden over bijvoorbeeld lokale bijeenkomsten, deelnemende ClubShop Rewards Kaart winkels en bedrijven en lokale aanbiedingen en specials. Daarnaast is deze informatie (en het adres) nodig voor de verzending van ClubShop Reward cheques of cadeaubonnen.</p25>
  <p26>Ieder member die niet de minimale informatie verschaft binnen een jaar na inschrijving, zal verwijderd worden uit het ledenbestand en behandeld worden als een &quot;inactief&quot; lid. Ieder inactief lid zal de kans krijgen tot een nieuw lidmaatschap, al dan niet bij dezelfde sponsor, als zij inschrijven met de hierboven besproken informatie.</p26>
  <p27>180 Dagen</p27>
  <p27a>- om te worden ingeschreven door een Partner die in dezelfde organisatie zit als één van de voormalige upline Partners.</p27a>
  <p28>90 Dagen</p28>
  <p28a>- om te worden ingeschreven door een Partner die NIET in dezelfde organisatie zit van één van de de voormalige upline Partners en indien de Partner recentelijk ten minste één andere Partner heeft ingeschreven toen het voormalige Partner lidmaatschap werd beëindigd.</p28a>
  <p29>30 Dagen</p29>
  <p29a>- om te worden ingeschreven door een Partner die NIET in dezelfde organisatie zit van één van de voormalige upline Partners en indien de Partner recentelijk geen andere Partners heeft aangesloten toen het voormalige Partner lidmaatschap werd beëindigd.</p29a>
  <p3>Partner Business Center</p3>
  <p30>Beleid</p30>
  <p31>ClubShop Rewards kan naar eigen goeddunken, afzien van de bovengenoemde wachttijden; eventuele partners die heeft verzocht om te annuleren of te downgraden hun lidmaatschap naar een andere sponsor, beter te waarborgen dat de Partner wordt behouden en voorzien van de juiste ondersteuning en hulp.</p31>
  <p4>Leden Informatie</p4>
  <p5>Site Index</p5>
  <p6>Lidmaatschap beleid</p6>
  <p7>Basisvoorwaarden</p7>
  <p8>Een ClubShop Rewards lidmaatschap is gratis en is beperkt tot één lidmaatschap per persoon, partnerschap of entiteit. Om onze Partners het recht te geven te kunnen kiezen met wie zij zaken willen doen, is het een Partner of ClubShop Rewards lid toegestaan een nieuw lidmaatschap of andere inschrijving te verkrijgen door het indienen van een nieuw lidmaatschapsaanvraag. Na onze ontvangst van de aanvraag, wordt het vorige lidmaatschap voor dit lid als nietig beschouwd.</p8>
  <p9>Echter, het aanzetten van ingeschreven leden tot stopzetting van hun lidmaatschap om een nieuwe inschrijving te verwerven, is ten strengste verboden.</p9>
</lang_blocks>
