﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="commission.xsl" ?><lang_blocks>
  <home_main>Home</home_main>
  <p10>Alle compensatie wordt onderworpen aan enige openstaande schuld voortvloeiend uit een ongedekte cheque van een Partner of een klant van een Partner. Het recht op compensatie is gebaseerd op de kwalificatie vereisten die gespecificeerd worden in het ClubShop Rewards Compensatieplan.</p10>
  <p11>Kwalificatie vereisten zijn gebaseerd op volgende criteria:</p11>
  <p12>Partner bijdragen moeten stipt iedere maand voldaan worden.</p12>
  <p13>Partners mogen hun lidmaatschap niet hebben beëindigd of stopgezet in de maand waarop compensatie wordt berekend of daarvoor.</p13>
  <p14>Het bereiken van een nieuw paylevel geeft geen garantie op dezelfde compensatie voor de volgende maanden mocht dezelfde vereiste niet worden bereikt. De compensatie die iedere maand wordt uitbetaald is enkel gebaseerd op de kwalificatie vereisten die werden bereikt voor die maand en kunnen groter of kleiner zijn dan de maand daarvoor.</p14>
  <p14a>Commission entitlement for prior months may also be adjusted for any changes made to the Partner Compensation Plan, prior to the commissions actually being disbursed.</p14a>
  <p15>Omdat vele punten meespelen bij de einde van de maand berekeningen, kunnen enige compensatie tegoeden pas de volgende maand weergegeven worden. Het is daarom aan te raden dat kwalificatie dient bereikt te worden voor de laatste week van de maand. Met uitzondering van stopzettingen binnen 30 dagen, fraudezaken en fouten, worden de boeken voor compensatie afgesloten en gepost om 12:00 middernacht (Pacific Time) op de laatste dag van iedere maand.</p15>
  <p16>Kies uw Betalingsopties:</p16>
  <p17>Iedere Partner heeft een ClubAccount ter beschikking om fondsen op te storten voor of fondsen te ontvangen van ClubShop Rewards. Alle commissies worden automatisch gestort op uw ClubAccount tot u ervoor kiest de fondsen te innen of tenzij u een specifieke betalingsmethode selecteert om uw commissies iedere maand automatisch te innen. The minimum disbursement allowed is $25.00 USD. Any commission entitlement less than $25.00 USD will be credited to your ClubAccount.
Er kan een kleine vergoeding gevraagd worden voor verwerk/verzendkosten om uw commissies te ontvangen, dit zal afhangen van de methode die u selecteert. Om te voorkomen dat u kleine bedragen ontvangt, is het aan te raden de commissies te laten overmaken naar uw ClubAccount en een groter bedrag te accumuleren.</p17>
  <p18>Betaalopties</p18>
  <p19>ClubAccount -</p19>
  <p20>- This is the default method for all Members. Commissions accumulate here and can be received at any time by requesting funds be sent to you. ClubAccount disbursements are combined and made monthly, not individually. Therefore, allow for as much as 30 days to receive your disbursement.To receive your commission entitlement in the most prompt manner, request a specific method for your commissions to be disbursed to you automatically each month.</p20>
  <p21>Opties voor internationale leden</p21>
  <p22>Payza (Alert Pay)</p22>
  <p23>- Hebt u een Payza (Alert Pay) account, dan kunt u ervoor kiezen om uw commissies te laten overmaken op uw Payza (Alert Pay) account. Er geldt een kleine transactiebijdrage, die zal afgetrokken worden van uw verdienste voor die wordt overgemaakt. Als u deze optie kiest, zullen uw commissies automatisch worden overgemaakt naar uw Payza (Alert Pay) account en wordt dit per e-mail bevestigd. We vragen een transactiebijdrage van 4% om via Payza (Alert Pay) uw ClubAccount aan te vullen.</p23>
  <p24>Meer informatie over Payza (Alert Pay) en om te zien of dit beschikbaar is waar u woont,</p24>
  <p25>Klik hier.</p25>
  <p26>Pay Agent -</p26>
  <p27>Internationale Pay Agents zijn Partners die de goedkeuring kregen om commissies en Club Rewards cashbacks over te maken aan Partnerss en leden die in hetzelfde land (of Europa) woonachtig zijn. De verdiensten van deelnemende leden en VIP's worden overgeschreven of elektronisch overgemaakt naar de Pay agent, die op zijn beurt de fondsen overmaakt aan de deelnemende leden, in hun lokale munteenheid.</p27>
  <p28>Meer informatie over de Pay Agent optie:</p28>
  <p29>http://www.clubshop.com/intl_pay_agent.html</p29>
  <p30>Opties voor U.S. inwoners</p30>
  <p31>Direct Deposit -</p31>
  <p32>U.S. inwoners kunnen ervoor kiezen hun commissies te ontvangen via Direct Deposit op een U.S. bankrekening. Direct deposits nemen 2 werkdagen in beslag.</p32>
  <p33>Cheque -</p33>
  <p34>U.S. inwoners kunnen er ook voor kiezen hun commissies te ontvangen via cheque.</p34>
  <p35>Om uw commissies op een andere manier te ontvangen, ga naar:</p35>
  <p36>https://www.clubshop.com/cgi/comm_prefs.cgi</p36>
  <p37>Persoonlijke documenten</p37>
  <p38>Het is voor iedere Partner belangrijk persoonlijke documenten over zakelijk inkomen, kosten en transport accuraat bij te houden. Velen kunnen gebruikt worden als inbreng van onkosten op uw belastingaangifte, zoals zakelijke etentjes, zakelijke reizen en promotieartikelen. Daarom raden wij aan dat iedere Partner een kopie neemt van de toepasselijke belastingformulieren (U.S. - IRS form Schedule C- Profit or Loss from a Business or Profession), alsook van de instructies op dit formulier, om zekerheid te creëren over uw belastingverminderingen en -verantwoordelijkheden.</p38>
  <p39>Bedrijfsdocumenten</p39>
  <p40>ClubShop Rewards is vereist het formulier 1099 te rapporteren aan de IRS (Amerikaanse belastingsdienst) en aan het individu, ieder uitbetaald inkomen van $600.00 of meer, in een kalenderjaar, aan een Partner die werkt of woont in de U.S. Alle U.S. Partners moeten ClubShop Rewards hun Social Security nummer meedelen om de IRS te kunnen rapporteren over $600.00 inkomen of meer. Inlichtingen aangaande uw compensatie kunt u verkrijgen bij acf1@dhs-club.com en moet uw naam en ID nummer bevatten en specifiek gericht zijn.</p40>
  <p41>Organisatie documenten</p41>
  <p42>Daar iedere Partner rechtstreeks handelt met ClubShop Rewards wat betreft orders en inkomen, worden zij niet vereist gegevens bij te houden van Partners in hun organisatie of inkomen te rapporteren van Partners in hun organisatie. U kunt verschillende transactie, compensatie en inkomen rapporten bekijken in de links &quot;Business rapporten&quot; op het Partner Business Center.</p42>
  <p43>Top</p43>
  <!--<p44>ecoPayz</p44>
  <p45>- Hebt u een ecoPayz account, dan kunt u ervoor kiezen om uw commissies te laten overmaken op uw ecoPayz account. Er geldt een kleine transactiebijdrage, die zal afgetrokken worden van uw verdienste voor die wordt overgemaakt. Als u deze optie kiest, zullen uw commissies automatisch worden overgemaakt naar uw ecoPayz account min een kleine EcoCard 1% bijdrage die automatisch van uw commissie wordt afgetrokken. De overmaking wordt u via e-mail bevestigd. We vragen geen transactiebijdrage om uw ClubAccount aan te vullen via ecoPayz.</p45>
  <p46>ecoPayz aanvragen</p46>-->
  <p50>Kies uw manier van Uitbetaling</p50>
  <p51>INCOME REPORT &amp; EARNINGS STATEMENT</p51>
  <p52>How your commissions are calculated is shown in your Income Report:</p52>
  <p53>How your actual earnings are calculated, after deductions are made, is shown in your Earnings Statement:</p53>
  <p54>PayPal -</p54>
  <p55>If you have a Pay Pal account, you can elect to have your commissions be deposited to your Pay Pal account. There is a small transaction fee that will be deducted from your earnings before the deposit is made. If you elect this option, your commission will automatically be sent to your account and you will receive an email notice when it has been sent.</p55>
  <p7>Commissie</p7>
  <p8>Compensatie</p8>
  <p9>Alle compensaties worden uitbetaald in U.S. dollar, tenzij deze in de lokale munteenheid worden omgezet door de processor of bank waar wij de fondsen aan overmaken. Om enige teruggaven of uitschrijvingen in commissie afrekeningen te kunnen incalculeren, wordt een 30 dagen wachttijd in acht genomen. Het uitbetalen van commissies gebeurt de eerste werkweek van iedere maand, gebaseerd op uw verdiensten en paylevel van de uit te keren maand. Bij voorbeeld, commissies verdiend in december, worden uitbetaald de eerste werkweek van februari.</p9>
</lang_blocks>
