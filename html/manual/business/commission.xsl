<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>


<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



</head>
<body>


	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>





	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p7"/></h4>

	<hr />
		
		<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
	
	
	
<p><span class="style28"><xsl:value-of select="//lang_blocks/p51"/></span></p>	
	
	<p><xsl:value-of select="//lang_blocks/p52"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank">https://www.clubshop.com/cgi/pireport.cgi</a></p>
	<p><xsl:value-of select="//lang_blocks/p53"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/CommStmnt.cgi" target="_blank">http://www.clubshop.com/cgi/CommStmnt.cgi</a></p>
	



<p class="style28"><xsl:value-of select="//lang_blocks/p8"/></p>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>

<span class="style24"><xsl:value-of select="//lang_blocks/p11"/></span>
<br/><br/>
<ol>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p81"/> <xsl:text> </xsl:text> <a href="http://www.clubshop.com/manual/policies/partnersponsor.xml" target="_blank"><xsl:value-of select="//lang_blocks/p82"/></a>.</li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p14"/> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14a"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>

 <hr align="left" width="100%" size="1" color="#A35912"/>



<!--<p><xsl:value-of select="//lang_blocks/p17"/></p>-->

<span class="style28"><xsl:value-of select="//lang_blocks/p18"/></span>
<br/><br/>
<ul>
<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p19"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p17"/><br/><br/>
<p><xsl:value-of select="//lang_blocks/p20"/></p>
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png"/><span class="bold"><xsl:value-of select="//lang_blocks/p35"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/comm_prefs.cgi" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p36"/></a></span></li>


<!--5/18/18 <li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p33"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34"/></li>-->


<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p62"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p63"/></li>
</ul>

<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png"/><b><xsl:value-of select="//lang_blocks/p56"/></b><xsl:text> </xsl:text><a href="https://www.paypal.com/worldwide/"><xsl:value-of select="//lang_blocks/p25"/></a></p>


<p><span class="style28"><xsl:value-of select="//lang_blocks/p64"/></span></p>
<p><xsl:value-of select="//lang_blocks/p83"/><xsl:text> </xsl:text> <a href="https://www.clubshop.com/pay_agent_list.php" target="_blank">https://www.clubshop.com/pay_agent_list.php</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p84"/></p>
<ul>

<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p19"/></span> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p87"/><br/></li>

<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p73"/></span><xsl:text> </xsl:text>  <span class="red"><xsl:value-of select="//lang_blocks/p57a"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p74"/><br/><br/>

<xsl:value-of select="//lang_blocks/p89"/>

<!--<p class="style24"><xsl:value-of select="//lang_blocks/p88"/></p>

<ul>
<li class="half"><xsl:value-of select="//lang_blocks/p75"/></li>
<li class="half"><xsl:value-of select="//lang_blocks/p76"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/manual/policies/feespayagent.xml" target="_blank">Fees Agent</a> .</li>
<li class="half"><xsl:value-of select="//lang_blocks/p77"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/manual/policies/feespayagent.xml" target="_blank">Fees Agent</a> .</li>
<li class="half"><xsl:value-of select="//lang_blocks/p79"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/manual/policies/feespayagent.xml" target="_blank">Fees Agent</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p80"/> .</li>


</ul>-->
</li>

<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p62"/></span> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p65"/><br/>

<ul>
<li class="half"><xsl:value-of select="//lang_blocks/p66"/></li>
<li class="half"><xsl:value-of select="//lang_blocks/p67"/><br/><br/><xsl:value-of select="//lang_blocks/p68"/></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p70"/></p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png"/><b><xsl:value-of select="//lang_blocks/p56"/></b><xsl:text> </xsl:text>
<a href="https://www.paypal.com/worldwide/"><xsl:value-of select="//lang_blocks/p25"/></a></p>

</li>




<p><xsl:value-of select="//lang_blocks/p85"/><xsl:text> </xsl:text> <a href="mailto:acfspvr@clubshop.com?Subject=Pay Agent Problem">acfspvr@clubshop.com</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p86"/></p>






<li class="blue_bullet"><span class="style24"><xsl:value-of select="//lang_blocks/p57"/></span><xsl:text> </xsl:text> <span class="red"><xsl:value-of select="//lang_blocks/p57a"/></span><br/>

<ul>
<li class="half"><xsl:value-of select="//lang_blocks/p58"/></li>
<li class="half"><xsl:value-of select="//lang_blocks/p59"/></li>
<!--<li class="half"><xsl:value-of select="//lang_blocks/p71"/></li>-->
<li class="half"><xsl:value-of select="//lang_blocks/p72"/></li>


</ul>
</li>

</ul>



<hr align="left" width="100%" size="1" color="#A35912"/>

<p><span class="style28"><xsl:value-of select="//lang_blocks/p37"/></span></p>

<p><xsl:value-of select="//lang_blocks/p38"/></p>

<p><span class="style28"><xsl:value-of select="//lang_blocks/p39"/></span></p>

<p><xsl:value-of select="//lang_blocks/p40"/></p>



<p><span class="style28"><xsl:value-of select="//lang_blocks/p41"/></span></p>

<p><xsl:value-of select="//lang_blocks/p42"/></p>


<br/>
<hr/>


<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>

			
	<br/><br/><br/>
</div></div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>

