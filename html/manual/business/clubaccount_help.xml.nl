﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="clubaccount_help.xsl" ?><lang_blocks><p1 new="1/4/12">Club Account</p1>
<p2>De Club Account is een zeer effectief en informatief hulpmiddel die al uw monetaire transacties volgt. Het doel van de Club Account is om een transparante weergave te bieden van alle transacties die voortvloeien uit uw harde en toegewijd werk in uw organisatie. Alle bedragen zijn te zien in US dollar.</p2><p3 new="12/10/11">Het Club Account rapport is te vinden op het Partner Business Center, onder Commissions en Fees. Het rapport laat u de activiteit in uw Club Account zien en met een eenvoudige klik vindt u de informatie over uw boekhoudkundige transacties.</p3>
<p4>Het rapport geeft het bedrag, het type, de datum en een omschrijving van de transactie weer. Onderaan het rapport vindt u twee totalen. Het eerste is het totaal voor de periode van het rapport. Deze periode beslaat de huidige maand, maar kan aangepast worden om verschillende tijdstippen weer te geven. Het tweede totaal is het voorlopige totaal of net balans van uw Club Account.</p4><p5>De verschillende transactietypes worden in detail hieronder omschreven:</p5><p6>Commissie tegoed</p6><p7>- Het commissie tegoed vertegenwoordigt het tegoed op de hangende commissie van de vorige maanden om de huidige lidmaatschapsbijdrage te dekken. Het blijft een tegoed en kan ingetrokken worden, hangt af van eventuele negatieven tegenover de vorige maand commissies bij het einde van de maand berekeningen.</p7><p8>Verdiende commissies</p8><p9>- Verdiende commissies vertegenwoordigen de verandering van tegoed aan commissie naar rechtmatige commissie voor de betaling van de vorige commissies.</p9><p10>Commissie afwikkeling</p10><p11>- De commissie afwikkeling bestaat uit de vorige twee maand commissie die in de wacht stond en is nu rechtmatig verdiend. Het bedrag wordt overgemaakt naar de Club Account en kan worden uitbetaald volgens uw uitbetalingsvoorkeur.  De fondsen kunnen geïnd worden of gebruikt worden om voor andere lidmaatschappen te betalen, voor co-op aankopen, pins of andere zaken. Dit is ook een uitstekende plek om uw appeltje voor de dorst te bewaren.</p11>
<p12>Uitbetaling</p12><p13>– Uitbetalingen vertegenwoordigen de beschikbare fondsen voor u. Deze transactiemethode is enkel voor verdiende commissies. Eender welke creditkaart betalingen kunnen niet uitbetaald worden.</p13>
<p13a>Uitbetalingskosten</p13a><p13b>– Uitbetalingskosten vertegenwoordigen de kosten die voortvloeien uit bankverrichtingen of andere monetaire transacties naar u toe.</p13b><p13c>KLIK HIER</p13c>

<p14>Global Partner Systeem maandelijkse abonnement</p14>
<p15>– Het Global Partner Systeem (GPS) maandelijks abonnement vertegenwoordigt de maandelijkse bijdrage. Dit bedrag is zichtbaar in USD ($).</p15>

<p16>Coach Fees</p16>
<p17>- Coach Fees worden in rekening gebracht voor het activiteitenrapport en het genealogie rapport, dit wordt u één keer per maand aangerekend. This is a fee that is charged once a month to Team Captains and higher.</p17>


<p18>Global Partner Systeem (GPS) jaarlijkse vernieuwingsbijdrage</p18>
<p19>– De Global Partner Systeem (GPS) jaarlijkse vernieuwingsbijdrage vertegenwoordigt de jaarlijkse bijdrage die aangerekend wordt in dezelfde maand dat uw lidmaatschap gestart is.</p19>

<p20>Global Partner Systeem (GPS) Partner Upgrade</p20>
<p21 new="12/10/11">– De Global Partner Systeem (GPS) Partner upgrade vertegenwoordigt een éénmalige bijdrage die het begin betekent van het leerproces naar een ervaren en vakkundige Partner.</p21>

<p22>Liefdadigheidsdonaties</p22>
<p23>– De liefdadigheidsdonaties vertegenwoordigen uw donaties aan een Goed Doel of liefdadigheidsorganisatie.</p23>

<p24>Boekhoudkundige actie</p24><p25>- Een boekhoudkundige actie is een actie die gecreëerd werd door de boekhoudafdeling voor een bepaald verzoek of kwestie die behandeld werd.</p25><p26>Andere termen</p26><p27 new="12/10/11">– Het kan zijn dat u nu en dan andere termen zult vinden op uw account en die al nader werden uitgelegd binnen ClubShop Rewards. De omschrijvingen zullen zichzelf verklaren.</p27><p28>Soort uitbetaling</p28><p29>Commissies per cheque</p29><p32>Commissies via Pay agent</p32><p35>Om dit bedrag te zien in een andere munteenheid dan USD -</p35><p36>Bedrag (in USD)</p36><comm>indien geselecteerd als Commissievoorkeur</comm><disburse>indien afgeschreven van ClubAccount</disburse><getstarted new="1/4/12">Aan de slag</getstarted>

<disburse_rules new="4/27/12">ClubAccount disbursements are combined and made monthly, not individually. Allow for as much as 30 days to receive your disbursement. </disburse_rules>


<p37 new="2/18/15">ClubAccount Rules</p37>
<p38 new="2/18/15">Withdrawals</p38>
<p39 new="2/18/15">- the only funds that can be withdrawn and paid to you are the funds that come from your commissions. The minimum amount that you are allowed to withdraw is $25.00 USD. </p39>
<p40 new="2/18/15">Transfers</p40>
<p41 new="2/18/15"> - the only funds that you can transfer to a Pay Agent, Partner in your upline or a Partner or Trial Partner in your downline are commissions you've earned and funding made to your account by credit card or PayPal. Note: Pay Agents are also able to make transfers or pay for fees to any Partners and Trial Partners that live in the same country that they are an approved Pay Agent for.
</p41>
<p42 new="2/18/15">Pay for GPS Subscription Fees &amp; ClubShop Purchases</p42>
<p43 new="2/18/15">- all funds in your ClubAccount are able to be used to pay for your personal GPS Fees and ClubShop purchases (CO-OP, ClubShop Reward Cards, ClubShop Outlet Center) and/or the GPS Fees for a downline Partner or Partner applicant. </p43>

<p44 new="2/18/15">FUNDS RECEIVED FROM</p44>
<p45 new="2/18/15">PAY FOR FEES &amp; CLUBSHOP PURCHASES</p45>
<p46 new="2/18/15">TRANSFERABLE</p46>
<p47 new="2/18/15">TABLE TO WITHDRAW</p47>
<p48 new="2/18/15">COMMISSIONS</p48>
<p49 new="2/18/15">CREDIT CARD OR PAYPAL</p49>
<p50 new="2/18/15">TRANSFERS</p50>
<p51 new="2/18/15">YES</p51>
<p52 new="2/18/15">NO</p52>



</lang_blocks>
