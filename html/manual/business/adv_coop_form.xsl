<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Business Manual</title>


<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

body {
   background-color: #245923;
} 

</style>

</head>
<body>
<div align="center"> 
<table width="950px" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      
    	
      
      
      <tr>
       <td background="/images/bg_header.jpg"><img src="http://www.clubshop.com/images/gi.jpg" width="640" height="70" align="left"/></td>

      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

          <tr>
            <td align="right">
<a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" 
class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="left">


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span>

<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text> <a href="/manual/policies/advcoop.xml" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></p>

<form action="/cgi/FormMail.cgi" method="post">
<INPUT name="recipient" type="hidden" value="supportspvr@dhs-club.com"/>

<INPUT name="email" type="hidden" value="apache@clubshop.com"/> 
<INPUT name="required" type="hidden" value="coord-name,coord-id,coord-email,advert-cost,participants"/> 
<INPUT name="sort" type="hidden" value="order:coord-name,coord-id,coord-email,advert-cost,participants"/> 
<INPUT name="redirect" type="hidden" value="/manual/business/adv_form_tyou.xml"/> 
<INPUT name="subject" type="hidden" value="Media Advertising Cooperative Update"/> 
<INPUT name="sort" type="hidden" value=""/> 

<INPUT name="env_report" type="hidden" value="HTTP_USER_AGENT"/>

		<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p5"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-name" size="25"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p6"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-id" size="25"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p7"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-email" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p8"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="advert-cost" size="25"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p9"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p10"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="search_engine" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p11"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="user_name" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p12"/></td>
<td bgcolor="#FFFFFF"><input type="text" name="password" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p13"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF" colspan="2" align="center"><textarea name="participants" cols="50" rows="10"></textarea></td>
</tr>

<tr>
<td bgcolor="#FFFFFF" colspan="2" align="center"> 
<input type="submit" name="Submit" value="Submit"/>
<xsl:text> </xsl:text><input type="reset" name="Reset" value="Reset"/>

</td>
</tr>


		</table>

</form>

<p><xsl:value-of select="//lang_blocks/p14"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/> </p>

<p><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="mailto:vipsales@dhs-club.com?Subject=Media Advertisment Approval">vipsales@dhs-club.com</a></p>


</div>


</td></tr></table>

</td>
      </tr>
    </table>

</td>
      </tr>
    </table>
    </td>
  </tr>
  
  <tr><td align="center"><span class="footer_copyright">| <a href="/manual/index.shtml" target="_blank" class="nav_header">Home</a> | <a href="https://www.clubshop.com/p" target="_blank" class="nav_header">Partner Business Center</a> | <a href="http://www.clubshop.com/cgi/apbc"_blank" class="nav_header">Affiliate Business Center</a> |</span></td></tr>
  <tr><td align="center"><div align="center"><img src="/images/icons/icon_copyright.gif" height="20" width="306" alt="copyright"/></div></td></tr>

</table>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>