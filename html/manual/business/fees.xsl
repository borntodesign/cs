﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>




   <xsl:template match="/">
    
         <html><head>
         
         
         
         
         
            <title>ClubShop Rewards Partner Manual</title>
            
            <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css" />


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/gibbsfees"/></h4>
	<br />
	<hr />
<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">










<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="//www.clubshop.com/fees/full.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a>.</b></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p80"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p81"/></p>



<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p90"/></b></span></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p91"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/welcomeback.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p92"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p93"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p94"/><xsl:text> </xsl:text><b><u><xsl:value-of select="//lang_blocks/p95"/></u></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p96"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p97"/><span class="style3"><xsl:value-of select="//lang_blocks/p98"/></span></p>

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p10"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p11"/></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p61"/><xsl:text> </xsl:text><a href="mailto:acf1@dhs-club.com?Subject=Credit_Card_Verification" class="nav_footer_brown">acf1@clubshop.com</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p62"/></p>


<!--<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p12"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p13"/></p>-->

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p14"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p16a"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16b"/></p>


<ul>
<li><span class="style24"><b><xsl:value-of select="//lang_blocks/p17"/></b></span><xsl:text> </xsl:text>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p18"/></span></li>


<!--<ul><li class="text_normal"><xsl:value-of select="//lang_blocks/p44"/><br/>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p45"/><xsl:text> </xsl:text> <a href="/manual/forms/transmission_agent.xml" target="_blank"><xsl:value-of select="//lang_blocks/p46"/></a>
<br/><br/></span>
</li>
</ul>-->

<li><span class="style24"><b><xsl:value-of select="//lang_blocks/p75"/></b></span><xsl:text> </xsl:text>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p76"/></span><br/><br/>

<ul>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p103"/><xsl:text> </xsl:text> <b>info@proprofit.co.uk</b></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p104"/></li>
</ul>




</li>


<!--<li><span class="style24"><b><xsl:value-of select="//lang_blocks/p19"/></b></span><xsl:text> </xsl:text>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p20"/></span></li>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p20a"/><xsl:text> </xsl:text><a href="http://www.Payza.com/" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a>.</p>

-->





<!--<hr align="left" width="100%" size="1" color="#A35912"/>

<li><span class="style24"><b><xsl:value-of select="//lang_blocks/p47"/></b></span><xsl:text> </xsl:text>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p48"/></p></li>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p49"/><xsl:text> </xsl:text><a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" target="_blank">https://www.ecocard.com/Registration.aspx?Referee=DHS</a>.</p>
<br/>

<li><span class="style24"><b><xsl:value-of select="//lang_blocks/p50"/></b></span><xsl:text> </xsl:text> - <xsl:value-of select="//lang_blocks/p51"/></li>
<p>
<xsl:value-of select="//lang_blocks/p52"/> = NL11FTSB0243058780 <br/>
<xsl:value-of select="//lang_blocks/p53"/> = ChargeStream  Ltd <br/>
<xsl:value-of select="//lang_blocks/p54"/>= 83 Victoria Street,<br/>
<xsl:value-of select="//lang_blocks/p55"/>= London SW1H OHW<br/> 
<xsl:value-of select="//lang_blocks/p56"/>= UK <br/>
<xsl:value-of select="//lang_blocks/p57"/>= Fortis Bank<br/>
<xsl:value-of select="//lang_blocks/p58"/>= FTSBNL2R<br/>
<xsl:value-of select="//lang_blocks/p59"/>= ecoPayz Account Number  <br/>                                       
                                          	


</p>-->




</ul>

<!--<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p63"/></b></span></p>

<p><xsl:value-of select="//lang_blocks/p64"/><br/><br/>
Lisa Young<br/>
<xsl:value-of select="//lang_blocks/p65"/><br/>
<xsl:value-of select="//lang_blocks/p66"/><br/>
<xsl:value-of select="//lang_blocks/p67"/></p>
<xsl:value-of select="//lang_blocks/p68"/><br/>
<xsl:value-of select="//lang_blocks/p69"/><br/>
<xsl:value-of select="//lang_blocks/p70"/>
<p><b><xsl:value-of select="//lang_blocks/p71"/></b></p>

<p><xsl:value-of select="//lang_blocks/p73"/><xsl:text> </xsl:text><a href="mailto:acf1@clubshop.com">acf1@clubshop.com</a><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p74"/></b></p>
<p><xsl:value-of select="//lang_blocks/p72"/></p>-->





<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p21"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p22"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<li class="blue_bullet"><span class="text_normal"><xsl:value-of select="//lang_blocks/p24"/></span></li>
<li class="blue_bullet"><span class="text_normal"><xsl:value-of select="//lang_blocks/p25"/></span></li>
<li class="blue_bullet"><span class="text_normal"><xsl:value-of select="//lang_blocks/p26"/></span></li>
<li class="blue_bullet"><span class="text_normal"><xsl:value-of select="//lang_blocks/p26a"/><xsl:text> </xsl:text><a href="http://www.youtube.com/watch?v=GwuDvYy8nWw&amp;list=UURvawE-f-HRfgnx3mgxWndA" target="_blank">You Tube Video</a></span></li>
</ul>


<p class="text_normal"><xsl:value-of select="//lang_blocks/p99"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p100"/></p>
<p class="text_normal"><a href="http://www.clubshop.com/pay_agent_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p101"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p102"/></p>


<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p60a"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p60"/></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p27"/></p>


<!--5/17/18<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p28"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p29"/></p>-->


<hr align="left" width="100%" size="1" color="#A35912"/>


<p class="text_normal"><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p32"/></b></span></p>

<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/fees/full.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a>.</b></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p33"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p34"/></p>




<p class="text_normal"><xsl:value-of select="//lang_blocks/p35"/></p>


<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p36"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p37"/></p>

<!--5/23/18 <p><span class="style24"><b><xsl:value-of select="//lang_blocks/p38"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p39"/></p>-->

<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p40"/></b><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p41"/></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42"/></p>


<hr align="left" width="100%" size="1" color="#A35912"/>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p43"/><xsl:text> </xsl:text><a href="mailto:acf1@clubshop.com" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p31"/></a></p>


<hr/>
   


<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>



</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
