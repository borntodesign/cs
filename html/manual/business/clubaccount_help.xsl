<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
       
         
         
         
         
         
            <title>ClubShop Rewards Partner Manual</title>
            
            <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <!--<script src="//www.clubshop.com/js/partner/app.js"></script>-->
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="//www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


<style type="text/css">
td.c {text-align: center;
}

</style>

</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p1"/></h4>
	<br />
	<hr />
	<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
	
	

<p class="text_normal"><xsl:value-of select="//lang_blocks/p2"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p3"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p4"/></p>



<p><span class="style24"><xsl:value-of select="//lang_blocks/p37"/></span></p>
<p><span class="style24"><xsl:value-of select="//lang_blocks/p38"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39"/></p>
<p><span class="style24"><xsl:value-of select="//lang_blocks/p40"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41"/><br/><br/>
<xsl:value-of select="//lang_blocks/p41a"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/pax/pa" target="_blank"><xsl:value-of select="//lang_blocks/p41b"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41c"/></p>

<p><span class="style24"><xsl:value-of select="//lang_blocks/p42"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p43"/></p>


<table width="100%" border="0" cellpadding="6" cellspacing="1">
<tr>
<td class="c"><b><xsl:value-of select="//lang_blocks/p44"/></b></td>
<td class="c"><b><xsl:value-of select="//lang_blocks/p45"/></b></td>
<td class="c"><b><xsl:value-of select="//lang_blocks/p46"/></b></td>
<td class="c"><b><xsl:value-of select="//lang_blocks/p47"/></b></td>
</tr>

<tr>
<td class="c"><b><xsl:value-of select="//lang_blocks/p48"/></b></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
</tr>

<tr>
<td class="c"><b><xsl:value-of select="//lang_blocks/p49"/></b></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p52"/></td>
</tr>


<tr>
<td class="c"><b><xsl:value-of select="//lang_blocks/p50"/></b></td>
<td class="c"><xsl:value-of select="//lang_blocks/p51"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p52"/></td>
<td class="c"><xsl:value-of select="//lang_blocks/p52"/></td>
</tr>

</table>





<p class="text_normal"><xsl:value-of select="//lang_blocks/p5"/></p>

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p24"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p25"/></span></p>

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p22"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p16"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p17"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p10"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p11"/></span></p>


<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p12"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/disburse_rules"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p13a"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p13b"/></span></p>

<table width="100%" border="0" cellpadding="6" cellspacing="1">

<tr>
<td><b><xsl:value-of select="//lang_blocks/p28"/></b></td>
<td><div align="center"><b>Paper Check (U.S. only)</b></div></td>
<td><div align="center"><b><xsl:value-of select="//lang_blocks/p32"/></b></div></td>

<td><div align="center"><b>PayPal</b></div></td>

</tr>

<tr>
<td><b><xsl:value-of select="//lang_blocks/p36"/></b></td>
<td><div align="center">$1.00</div></td>
<td><div align="center">$5.00</div></td>

<td><div align="center">2% of the amount requested to<br/> be disbursed from ClubAccount</div></td>

</tr>
</table>





<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p14"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p18"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p19"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p20"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p21"/></span></p>




<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p53"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p54"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p26"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p27"/></span></p>


<hr/>
<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>



</div>
</div>


            
  
              
          
    <!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>


