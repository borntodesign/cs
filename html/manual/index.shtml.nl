﻿<!DOCTYPE html>

 

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    

<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    

<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->

<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->

<!-- [if gt IE 8]>  --> 

 

<html xmlns="http://www.w3.org/1999/xhtml">

 

<!--<![endif]-->

 

<head>
 <title>ClubShop Rewards Manual</title>

<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css"/>
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css"/>
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css"/> 
    <link rel="stylesheet" type="text/css" href="/css/partner/orbit.css"/>   
    <link rel="stylesheet" type="text/css" href="/css/manual_2012.css"/>

<script src="/js/jquery.min.js"></script>    
    <script src="/js/partner/foundation.js"></script>    
    <script src="/js/partner/app.js"></script> 
    <script src="/js/partner/jquery.orbit-1.2.3.js"></script> 

    <script  language="javascript">

    $(document).ready(function() {
    var showText='( View More )';
    var hideText='Hide';
    var is_visible = false;

    $('.toggle').prev().append(' <a href="#" class="toggleLink">'+showText+'</a>');
    $('.toggle').hide();
    $('a.toggleLink').click(function() {

    is_visible = !is_visible;
    $(this).html( (!is_visible) ? showText : hideText);
    
     //toggle the display - uncomment the next line for a basic "accordion" style
    $('.toggle').hide();$('a.toggleLink').html(showText);
    $(this).parent().next('.toggle').toggle('slow');
    
    // return false so any link destination is not followed
    return false;
    
    });
    });

    </script>

<style>

.style25 a {

font-size: 12px;

}

.style25 a:link {

color: #2A5588;

text-decoration: none;

}

.style25 a:visited {

color: #2A5588;

}

.style25 a:hover {

color: #FFB404;

font-weight:bold;

height: 15px;

text-decoration: none;

}

.style25 a:active {

color: #2A5588;

text-decoration: none;

}

</style>

</head>

<body class="blue">
<!-- Top Blue Header Container --> <!--#include virtual="../p/common/body/header_manual.shtml" --> <!--End Top Blue Header Container --> <!--Main Container --> <!--lisas - start-->
<div class="container white">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle">ClubShop Rewards Partner Handleiding</h4>
<hr />
<ol class="darkblue bold">
<li>
<h5><span class="style28">Training Gids</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 1: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/getstarted/index.html" target="_blank">Van start gaat</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 2: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/referralmarketing/index.html" target="_blank">Referral Marketing</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 3: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/getsetup/index.html" target="_blank">Voorbereiding</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 4: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/sponsoring/index.html" target="_blank">Partner Sponsoring</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 5: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/coaching/index.html" target="_blank">Partner Coaching</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 6: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/mdirector/index.html" target="_blank">Marketing Director</a></span> <br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 7: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/execdir/index.html" target="_blank">Chief Director</a></span><br /><br /> 

<h5><span class="style28">Marketing</span></h5>

<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 1: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/localmarketing/" target="_blank">Locale Marketing</a></span><br /><br /> 
<img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 2: </span><span class="style25"><a class="style24" href="http://www.clubshop.com/vip/manual/training/guide/online_merchant/index.html" target="_blank">ONLINE MERCHANT MARKETING</a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Winkelen, kortingen en voordelen </span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="../cs/shop_save.shtml" target="_blank">Overzicht</a></span><br /><br /> <span class="style25"><a class="style24" href="../mall/" target="_blank">ClubShop winkelcentrum</a></span><br /><br /> <span class="style25"><a class="style24" href="../outletsp/bcenter.html" target="_blank">Business Center</a></span><br /><br /> <span class="style25"><a class="style24" href="../rewardsdirectory/" target="_blank">ClubShop Rewards Overzicht</a></span><br /><br /> <span class="style25"><a class="style24" href="../maps/rewardsdirectory.shtml" target="_blank">ClubShop Rewards Google Maps Directory</a></span><br /><br /> <span class="style25"><a class="style24" href="http://www.clubshop.com/cgi-bin/rd/14,3077,mall_id=1" target="_blank">ClubShop Kleding en Cadeau's </a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Marketing Plan</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="marketing/overview.xml" target="_blank">Overzicht</a></span><br /><br /> <span class="style25"><a class="style24" href="marketing/member.xml" target="_blank">Voordelen members verwijzen</a></span><br /><br /> <span class="style25"><a class="style24" href="marketing/merchant.xml" target="_blank">Voordelen winkels/ bedrijven verwijzen</a></span><br /><br /> <span class="style25"><a class="style24" href="marketing/affinity.xml" target="_blank">Voordelen non-profit organisaties verwijzen</a></span><br /><br /> <span class="style25"><a class="style24" href="marketing/affiliate.xml" target="_blank">Voordelen Affiliates verwijzen</a></span><br /><br /> <span class="style25"><a href="marketing/trialpartner.xml" target="_blank" class="style24">Trial Partner's Referring Benefits</a></span><br/><br/><span class="style25"><a class="style24" href="marketing/partner.xml" target="_blank">Voordelen Partners verwijzen</a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Compensatieplannen</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="compensation/partner.php" target="_blank">Partner Compensatieplan</a></span><br /><br /> <span class="style25"><a class="style24" href="compensation/affiliate.html" target="_blank">Affiliate Compensatieplan</a></span><br /><br /> <span class="style25"><a class="style24" href="compensation/affinity.html" target="_blank">Non-profit Compensatieplan</a></span><br /><br /> <span class="style25"><a class="style24" href="compensation/merchant.html" target="_blank">Winkel/ bedrijf Compensatieplan</a></span><br /><br /> 


<!--<span class="style25"><a class="style24" href="compensation/member.html" target="_blank">Member Compensatieplan</a></span><br /><br />-->

</div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Global Partner Systeem (GPS)</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="gps/overview.html" target="_blank"> Overzicht</a></span><br /><br /> 

<span class="style25"><a href="/fees/full.xml" target="_blank" class="style24">GPS Pricing and Pay Point Information</a></span><br/><br/>
<span class="style25"><a class="style24" href="business/fees.xml" target="_blank"> Kies de methode om uw abonnementbijdragen te betalen</a></span><br /><br /> 

<span class="style25"><a class="style24" href="https://www.clubshop.com/cgi/vip/gps_modification.cgi" target="_blank"> Modify Your Subscription Type</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Basic - Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_basic_preplus.png" width="115" /><a class="style24" href="https://www.clubshop.com/vip/coop.shtml" target="_blank"> Gezamenlijk Adverteren</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Basic - Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_basic_preplus.png" width="115" /><a class="style24" href="http://www.clubshop.com/cgi/vip/activity_rpt" target="_blank"> Activiteiten Rapport</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Basic - Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_basic_preplus.png" width="115" /><a class="style24" href="https://www.clubshop.com/cgi/vip/member-followup.cgi" target="_blank"> Follow Up Records</a></span><br /><br /> 



<span class="style25"><img src="http://www.clubshop.com/images/icons/icon_basic_preplus.png" height="12" width="115" alt="Basic - Premier Plus Subscriptions" align="bottom"> <a class="style24" href="http://www.clubshop.com/banners/select.xsp " target="_blank"> Banner Generator</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Pro/Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_pro_preplus.png" width="115" /><a class="style24" href="http://www.clubshop.com/cgi/vip/network_reports.cgi" target="_blank"> Lijst Generator</a> & <a class="style24" href="http://www.clubshop.com/cgi/vip/network_reports_status.cgi" target="_blank">Rapporten</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Pro through Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_pro_preplus.png" width="115" /> <a class="style24" href="http://www.clubshop.com/cgi/pwp-edit.cgi" target="_blank"> Persoonlijke Marketing Webpagina Creatie en Hosting</a></span> <br /><br /> 







<span class="style25"><img align="bottom" alt="Premier/Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_pre_preplus.png" width="115" /><a class="style24" href="http://www.clubshop.com/p/premier/auto_emailer.html" target="_blank"> Auto Emailer</a></span><br /><br /> 

<span class="style25"><img align="bottom" alt="Premier/Premier Plus Subscriptions" height="12" src="http://www.clubshop.com/images/icons/icon_pre_preplus.png" width="115" /><a class="style24" href="http://www.clubshop.com/p/premier/meeting_room_calendar.html" target="_blank"> MEETING ROOM</a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Bedrijfshandleiding</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<ul>
<li><span class="style24">CLUBACCOUNT</span><br /><br /> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="business/clubaccount_help.xml" target="_blank">Help</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">Statement</a></li>
</ul>
</li>
</ul>
</li>
<br />
<li><span class="style24">CLUBSHOP REWARDS KAARTEN</span><br /><br /> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/cb_card_transfer.cgi/layout_1" target="_blank">Kaarten activeren of toewijzen - Bestaande Member</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cb-activation.xml" target="_blank">Kaart activeren - Nieuwe Member</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/assign_cards.html" target="_blank">Kaarten overdragen</a></li>
</ul>
</li>
</ul>
</li>
<br />
<li><span class="style25"><a class="style24" href="business/commission.xml" target="_blank">COMMISSIES</a></span></li>
<li><span class="style24">FUNCTIES</span><br /><br /> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/vip/MemberModification.html" target="_blank">Member Functies Wijzigen</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cgi/vip/mv-members.cgi" target="_blank">Members verplaatsen</a></li>
</ul>
</li>
</ul>
</li>
<li><span class="style25"><a class="style24" href="business/fees.xml" target="_blank">GPS BIJDRAGEN</a></span></li>
<li><span class="style25"><a class="style24" href="business/legal.xml" target="_blank">WETTELIJKE VERKLARING</a></span></li>
<li><span class="style25"><a class="style24" href="business/mission.xml" target="_blank">MISSIE DOELSTELLINGEN</a></span></li>
<li><span class="style25"><a class="style24" href="http://www.clubshop.com/present/bizop.xml" target="_blank">Partner Income Opportunity Presentation</a></span></li>
<li><span class="style25"><a href="https://www.clubshop.com/pay_agent_list.php" target="_blank" class="style24">Pay Agent List</a></span></li>
<li><span class="style25"><a class="style24" href="http://clubshop.com/reports/sales_reports.shtml" target="_blank">Verkoop Management Rapporten</a></span></li>
<li><span class="style25"><a class="style24" href="https://www.clubshop.com/p/emails/index.html" target="_blank">Systeem Communicaties</a></span></li>
<li><span class="style25"><a class="style24" href="../terms.xml" target="_blank">Algemene Voorwaarden</a></span></li>
<li><span class="style25"><a class="style24" href="http://www.clubshop.com/mall/tools/tools.html" target="_blank">Hulpmiddelen</a></span></li>
<li><span class="style25"><a class="style24" href="https://www.clubshop.com/support" target="_blank">HELP</a></span></li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Beleid</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="policies/abusefraud.xml" target="_blank">Misbruik en Fraude</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/advertising.xml" target="_blank">Adverteren</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/advcoop.xml" target="_blank">Gezamenlijk Adverteren (Co-op)</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/application.xml" target="_blank">Aanmelding</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/business_name.xml" target="_blank">Bedrijfsnaam</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/cancellation.xml" target="_blank">Annuleren</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/cb_activation.xml" target="_blank">ClubShop Rewards Kaart Activatie Beleid</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/communications.xml" target="_blank">Communicaties</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/currency_conversion.xml" target="_blank">Valuta omrekenen</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/feespayagent.xml" target="_blank">Fees &amp; Pay Agent</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/membership.xml" target="_blank">Lidmaatschap</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/mbr_processing.xml" target="_blank">Member Behandeling en Procedure</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/name_usage.xml" target="_blank">Naamgebruik</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/promotional_page.xml" target="_blank">Promotiepagina</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/privacy.xml" target="_blank">Privacy</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/purchase_mbrs.xml" target="_blank">Aankopen Members</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/quality_assurance.xml" target="_blank">Kwaliteit betrouwbaarheid beleid</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/return.xml" target="_blank">Retourneerbeleid voor bedrukt Promotiemateriaal</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/solicitation.xml" target="_blank">Solicitation/ Uitlokking</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/spam.xml" target="_blank">Spam</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/spousal.xml" target="_blank">Echtelijk</a></span><br /><br /> <span class="style25"><a class="style24" href="policies/vipsponsor.xml" target="_blank">Partner Sponsor Verantwoordelijkheidsbeleid</a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Aanmelden en formulieren</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style24">Aanmelden</span> <br /> 
<ul>
<li> 
<ul>

<li class="style25 blue_bullet"><a href="http://www.clubshop.com/print/print_app.html" target="_blank" class="style24">OFFLINE MEMBERSHIP SIGNUP APPLICATION (html)</a></li>
<li class="style25 blue_bullet"><a href="http://www.clubshop.com/print/print_app.pdf" target="_blank" class="style24">OFFLINE MEMBERSHIP SIGNUP APPLICATION (pdf)</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/appinfo.xml" target="_blank">GPS aanmeld-informatie</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/appx.cgi/0/upg">PARTNER AAMELDEN</a></li>
<li class="style25 blue_bullet">AFFILIATE AANMELDING - https://www.clubshop.com/cgi/appx.cgi<span class="bold">REFERRALIDNUMBER</span>/af</li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cs/affinity_group_application.shtml" target="_blank">NON-PROFIT ORGANISATIE AANMELDEN</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cs/registration.shtml" target="_blank">OFFLINE WINKEL? BEDRIJF AANMELDEN</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cs/short-registration.shtml" target="_blank">FREE DIRECT DISCOUNT MERCHANT APPLICATION</a></li>
<li class="style25 blue_bullet"><!--<a class="style24" href="http://www.clubshop.com/cs/online-merchant-store.shtml " target="_blank">-->ONLINE MERCHANT APPLICATION - Currently Not Available</li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/appx.cgi" target="_blank">MEMBER AANMELDEN</a></li>
</ul>
</li>
</ul>
<span class="style24">WIJZIGINGEN Formulieren</span> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a href="https://www.clubshop.com/become_an_affiliate.html" target="_blank" class="style24">Change Membership from ClubShop Rewards Member to Affiliate</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cgi/cancel.cgi" target="_blank">Annuleren van Clubshop Rewards Lidmaatschap</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/cgi/cancel.cgi" target="_blank">Annuleren van ClubShop Rewards Partner Abonnement</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/vip/gps_modification.cgi" target="_blank">Modify Your GPS Subscription Type</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/spousal_letter.html" target="_blank">Echtgen(o)t(e) toevoegen aan Lidmaatschap</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/joint_removal.html" target="_blank">Gezamenlijke lidmaatschap verwijderen</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/company_accept.html" target="_blank">Plaats Lidmaatschap in Bedrijfsnaam Verklaring</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/company_transfer.html" target="_blank">Verplaats Lidmaatschap van Bedrijf naar Individu Verklaring</a></li>
</ul>
</li>
</ul>
<span class="style24">Commissie Formulieren</span> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="forms/directdeposit.html" target="_blank">Directe Storting: Begin, Wijzig of Beëindig</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/wirerequest.xml" target="_blank">Overschrijving: Begin, Wijzig of Beëindig</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/cgi/comm_prefs.cgi" target="_blank">Commissie Formulier</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/pay_agent_app.xml" target="_blank">Internationale Pay Agent - Aanvraag Aanmelding</a></li>
<!--<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/vip_pay_approval.xsp" target="_blank">Internationale Pay Agent - Goedkeuring Partner</a></li>-->
</ul>
</li>
</ul>
<span class="style24">Diverse Formulieren</span> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="forms/adv_coop_form.xml" target="_blank">Aanmeldformulier Gezamenlijk Adverteren</a></li>
<li class="style25 blue_bullet"><a class="style24" href="http://www.clubshop.com/mall/other/missing_order.shtml" target="_blank">ClubShop Winkelcentrum Formulier Vermiste Bestelling</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/login_registration.xml" target="_blank">Inloggen Registratieformulier</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/transmission_agent.xml" target="_blank">Transmission Agent</a></li>
<li class="style25 blue_bullet"><a class="style24" href="https://www.clubshop.com/merchants/setup_approval/index.xml" target="_blank">Formulier Transfer Fondsen naar Betaal Bijdragen voor Winkel/bedrijf</a></li>
</ul>
</li>
</ul>
<span class="style24">Formulieren Printen</span> 
<ul>
<li> 
<ul>
<li class="style25 blue_bullet"><a class="style24" href="forms/invoice.html" target="_blank">Internationale Factuur voor Initiële Bijdrage</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/invoice2.html" target="_blank">Internationale factuur voor Maandelijkse Bijdrage</a></li>
<li class="style25 blue_bullet"><a class="style24" href="forms/invoice_renewal.html" target="_blank">Internationale Factuur voor Jaarlijkse Bijdrage</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Technische Ondersteuning</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="style25"><a class="style24" href="technical/browserhelp.xml" target="_blank">Browser Help</a></span><br /><br /> <span class="style25"><a class="style24" href="technical/cookie_help.xml" target="_blank">Cookie Informatie</a></span><br /><br /> <span class="style25"><a class="style24" href="technical/cookiesetup.xml" target="_blank">Cookie SetUp</a></span><br /><br /> <span class="style25"><a class="style24" href="technical/internet_explorer.xml" target="_blank">Internet Explorer</a></span><br /><br /></div>
</div>
</div>
</div>
</li>
<li>
<h5><span class="style28">Webinars, Conferenties en conventies</span></h5>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><!--<span class="style25"><a href="http://www.clubshop.com/training_calendar.html" mce_href="http://www.clubshop.com/training_calendar.html" target="_blank" class="style24">Partner Income Opportunity Webinar Schedule</a></span><br /><br />-->  <span class="style25"><a class="style24" href="https://www.clubshop.com/vip/manual/training/interactive_online_schedule.html" target="_blank">Partner Webinar Overzicht</a></span><br /><br /> 
<!--<span class="style25"><a class="style24" href="training/conferences/index.html" target="_blank">Conferentie Index</a></span><br /><br /> <span class="style25"><a class="style24" href="training/conferences/schedule.html" target="_blank">Overzicht</a></span><br /><br /> <span class="style25"><a class="style24" href="training/conferences/conftestform.html" target="_blank">Verstuur een Testimonial</a></span>--><br /><br /></div>
</div>
</div>
</div>
</li>
</ol> <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
</div>
</div>
<!-- END LISA--> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="../p/common/footer/footer_manual.shtml" --> <!--End Footer Container -->
<script type="text/javascript">// <![CDATA[
		$(window).load(function() {

			$('#featured').orbit({

				bullets: true

			});

		});
// ]]></script>
</body>

</html>
