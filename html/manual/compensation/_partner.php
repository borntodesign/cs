﻿<?php
function lprintf($val)
{
    global $average_share_value;
    printf('%.2f', ($average_share_value * $val));
}
?>
<!DOCTYPE HTML >
<html>
<head>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<title><?=_('ClubShop Partner International Compensation Plan')?></title>

<script src="/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="partner.css" />
<script type="text/javascript" src="/js/partner/flash.js"></script>
<script type="text/javascript" src="/js/partner/foundation.js"></script>
<script type="text/javascript" src="/js/partner/app.js"></script>
<script type="text/javascript" src="/js/tipMap.js"></script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="includes/cplp.js"></script>

<script type="text/javascript">
var my_language_code = '<?=$pageLanguage?>';
var my_currency_code = '<?=$currency_code?>';
var my_country_code = '<?=$country?>';

	$(document).ready(function() {

		Transpose.transpose_currency();
    	var showText='MORE';
    
    	var hideText='LESS';
    
    	var is_visible = false; 

    	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');
    
    	$('.toggle').hide();
    
    	$('a.toggleLink').click(function() { 
        	is_visible = !is_visible;
        
        	$(this).html( (!is_visible) ? showText : hideText);
        	// toggle the display - uncomment the next line for a basic "accordion" style
        
        	//$('.toggle').hide();$('a.toggleLink').html(showText);
        
        	$(this).parent().next('.toggle').toggle('slow');
        
        	// return false so any link destination is not followed
        	return false;
    	});
	});

</script>
</head>

<body>

	<div class="container blue">

	<div class="row">

		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288" height="84" style="margin-top:5px;" /></a></div>

		<div class="six columns"></div>
	</div></div>

	<div class="container">

	<div class="row">

	<div class="twelve columns ">

	<br />

	<h4 class="topTitle"><?=_('ClubShop Partner International Compensation Plan')?></h4>

	</div></div>

	<div class="row">

		<div class="twelve columns">

<div id="google_translate_element" style="margin-top:0.5em;"></div>
                            <div align="left">
                              <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: '<?php echo $pageLanguage?>'
  }, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                            </div>

<?php foreach ($warnings AS $warning):?>
<div style="color: red"><?php echo $warning?></div>
<?php endforeach;?>

			<hr/>

      <p><?=_('The Compensation Plan shown below is a detailed presentation of how you can earn income as a ClubShop Rewards Partner.').PHP_EOL?>
        <?=_('Partner compensation is paid on a monthly basis, with the actual payment being made after a monthly waiting period in case any pay adjustments are required to be made.').PHP_EOL?>
        <?=_('For example, entitlement to Partner income for the month of January, is paid the beginning of March.').PHP_EOL?>
        <?php printf(_('You can see your potential Partner Income entitlement at your %sIncome Report%s.'), '<a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank">', '</a>')?>
         </p>
        
        
        <p class="style28"><?=_('BENEFITS OVERVIEW')?></p>
            <p>
            <?=_('The ClubShop Partner Compensation Plan enables you as a Partner to start a ClubShop Partner business in your spare time and join a team of Partners to share in earning Team Commissions.').PHP_EOL?>
            <?=_('Partners are able to benefit from their personal efforts and from the efforts made by all the Partners on their team.').PHP_EOL?> 

(<a href="compplan_overview.html" onClick="window.open(this.href,'help', 'width=700,height=675,scrollbars'); return false;"><?=_('Learn more')?></a>)</p>
				

<p class="style28"><?=_('ClubShop Rewards Card Sales')?></p>

<p><?=_('Purchase ClubShop Rewards Cards at <span class="medblue bold">"wholesale"</span> and sell them at retail, to earn a profit from each card sold.')?></p>
				

			<p><span class="style28"><?=_('PAY POINTS')?></span></p>
			<p><?=_('A Member needs to gain at least 10 Pay Points each month to qualify as a Partner, from their personal purchases and from purchases made by their personally referred Members and Member chains.')?></p>
			<p>
			<?=_('Pay Points are the commissionable value assigned to each type of purchase.').PHP_EOL?>
			<?=_('The value of 1 Pay Point = ')?><span class="transposeME">$1.00</span></p>	

				<div class="toggle" >

						<div class="row">

								<div class="eleven columns offset-by-one">

									<div class="panel"> 

										  <ul>
										      <li class="blue_bullet"><span class="larger_blue_bold"><?=_('ClubShop Rewards Mall stores and Offline &quot;Tracking&quot; Merchants').PHP_EOL?></span> - 
										      <?=_('Pay Points are shown in each store/merchant listing at <a href="http://www.clubshop.com" target="_blank">www.clubshop.com</a> and are equal to 1/2 the ClubCash value provided to the purchasing member.')?></li>
	<li class="blue_bullet"><span class="larger_blue_bold"><?=_('ClubShop Rewards Cards')?></span> - <?=_('One Pay Point per card purchased')?></li>

	<li class="blue_bullet"><span class="larger_blue_bold"><?=_('Offline Merchant Marketing Package')?></span> - 
	<?=_('Pay Points vary based on the package selected.').PHP_EOL?>


                <blockquote><table width="55%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">
                  <tr class="d"> 
                    <td class="style24 center"><?=_('Offline Merchant')?><br/><?=_('Marketing Packages')?></td>
                    <td class="style24 center"><?=_('Package Price')?></td>
                    <td class="style24 center"><?=_('Pay Points')?></td>
                  </tr>
				  <tr> 
                    <td class="bold center"><?=_('Free')?></td>
                    <td class="transposeME center">0.00</td>
                    <td class="center">0.00</td>
                  </tr>
                  <tr> 
                    <td class="bold center"><?=_('Basic')?></td>
                    <td class="transposeME center">19.99</td>
                    <td class="center">9.99</td>
                  </tr>
                  <tr> 
                    <td class="bold center"><?=_('Bronze')?></td>
                    <td class="transposeME center">39.99</td>
                    <td class="center">19.99</td>
                  </tr>
                  <tr> 
                    <td class="bold center"><?=_('Silver')?></td>
                    <td class="transposeME center">79.00</td>
                    <td class="center">39.50</td>
                  </tr>
                  <tr> 
                    <td class="bold center"><?=_('Gold')?></td>
                    <td class="transposeME center">149.00</td>
                    <td class="center">74.50</td>
                  </tr>
                  <tr> 
                    <td class="bold center"><?=_('Platinum')?></td>
                    <td class="transposeME center">299.00</td>
                    <td class="center">149.50</td>
                  </tr>
                </table></blockquote>
				<br/>
				</li>
				
				<li class="blue_bullet"><span class="larger_blue_bold"><?=_('Online ClubShop Rewards Mall Store Marketing Package')?></span>
				-
				<?=_('Pay Points vary based on the package selected.')?>

										 <br/> 				

                <blockquote><table width="55%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">

                  <tr class="d"> 
                    <td class="style24 center"><?=_('Mall Store')?><br/><?=_('Marketing Package')?></td>
                    <td class="style24 center"><?=_('Pricing')?></td>
                    <td class="style24 center"><?=_('Pay Points')?></td>
                  </tr>

                 <tr> 
                    <td class="bold center"><?=_('Basic')?></td>
                    <td class="transposeME center">19.99</td>
                    <td class="center">9.99</td>
                  </tr>

                  <tr> 
                    <td class="bold center"><?=_('Bronze')?></td>
                    <td class="transposeME center">39.99</td>
                    <td class="center">19.99</td>
                  </tr>

                  <tr> 
                    <td class="bold center"><?=_('Silver')?></td>
                    <td class="transposeME center">79.00</td>
                    <td class="center">39.50</td>
                  </tr>

                  <tr> 
                    <td class="bold center"><?=_('Gold')?></td>
                    <td class="transposeME center">149.00</td>
                    <td class="center">74.50</td>
                  </tr>

                  <tr> 
                    <td class="bold center"><?=_('Platinum')?></td>
                    <td class="transposeME center">299.00</td>
                    <td class="center">149.50</td>
                  </tr>
                </table>
				</blockquote>
				</li>
				<br/>
				
				
		        <li class="blue_bullet"><span class="larger_blue_bold"><a href="https://www.clubshop.com/manual/gps/overview.html" target="_blank"><?=_('GPS Subscriptions')?></a></span> - 
		        <?=_('Pay Points are based on the amount of the subscriptions, which can vary by type and from country to country.').PHP_EOL?>
		        <a href="http://www.clubshop.com/fees/full.xml" target="_blank"><?=_('List')?></a></li>
		        <li class="blue_bullet"><span class="larger_blue_bold"><a href="https://www.clubshop.com/vip/coop.shtml" target="_blank"><?=_('ClubShop Cooperative Advertising')?></a></span> - 
		        <?=_('Pay Points vary and are shown for each type of purchase made.')?></li>
		</ul>

									</div>

								</div>

						</div>

				 </div>
				 <p><?=_('Partners in the US, CA and EU that do not have at least 1 Personal Pay Point gained in a month from personal purchases or purchases made by personally referred Members, Affiliates, Affinity Groups or Merchants, other than GPS subscription purchases, will forfeit any entitlement to commissions for the month, other than the commissions sufficient enough to pay for their GPS subscription fees.')?></p>
		</div>	

	</div> 

	 <div class="row">

	<div class="twelve columns">
	<p><span class="style28"><?=_('GPS MINIMUM COMMISSIONS GUARANTEE')?></span> </p>
		<ul>
			
        <li class="blue_bullet"><?=_('Refer two or three GPS subscribers and you will are guaranteed to earn enough income to have have your GPS subscription for the following month deducted from your earnings:')?><br><br>
          <ul>
	<li class="half"><?=_('Refer two GPS subscribers and have your GPS BASIC subscription paid for!')?></li>
	<li class="half"><?=_('Refer three GPS subscribers and have your GPS PRO subscription paid for!')?></li>
	</ul>
	</li>

	</ul>

	
	</div>
	</div>

	 	 

	<div class="row">

	<div class="twelve columns">

     <p><span class="style28"><?=_('PERSONAL COMMISSIONS')?></span></p>	

	  <p><?=_('Personal Commissions equal 40% of your Personal Pay Points.')?></p>
 
	<p><img src="//www.clubshop.com/play.png" alt="play"> <a href="referral_desc.png" onClick="window.open(this.href,'help', 'width=520,height=520,scrollbars'); return false;"><?=_('SEE EXAMPLE')?></a></p>

	</div></div>

	  <div class="row">

	<div class="twelve columns">

	<p><span class="style28"><?=_('TEAM COMMISSIONS')?></span></p>

	   <p>
	   <?=_('All Partners are entitled to earn Team Commissions!').PHP_EOL?>
	   <?=_("The Pay Points generated by all ClubShop Partners are shared, distributed and paid out as Team Commissions, based on a revenue sharing formula that is designed to provide increasingly higher &quot;shares&quot; to the upline Partners in a team, based on each Partner's Pay Level qualification.").PHP_EOL?>
	   <?=_('See the table below to see the Team Commissions entitlement for each Pay Level. ')?></p>

<div class="row">
		<div class="twelve columns">

	  		<p><span class="style28"><?=_('PAY LEVEL QUALIFICATION')?></span></p>
	  		<p><?=_('Refer to the chart below to see what the Team Commissions entitlements are for each Pay Level and to determine how you can gain Pay Level promotions to increase your Team Commissions and Executive Director Bonus entitlements.')?></p>
			<p><?=_('You must meet three different qualifications to earn the applicable Pay Level promotion:')?></p>
	  		<ul>
        		<li class="blue_bullet"><span class="special4"><?=_('Personally Referred Partners and Pay Points').PHP_EOL?> (PRP/PP)</span> - 
        		<?=_('You can meet this qualifier by either having the applicable number of current Personally Referred Partners OR by your number of Personally Referred Pay Points.').PHP_EOL?>
        		<?=_('These Pay Points include your personal GPS Pay Points and the "Power Points" generated from your personal purchases and from the purchases made by your personally referred memberships and from the GPS Pay Points of your PRPs. ').PHP_EOL?> 
                <?=_('You can see this number and who they are in your Group Report and in your Income Report.')?></li>
                
				<li class="blue_bullet"><span class="special4"><?=_('Group Referred Partners and Pay Points').PHP_EOL?> (GRP/PP)</span> - 
				<?=_('You can meet this qualifier by either having the applicable number of current Partners OR by having the number of Pay Points generated by your personal Group.').PHP_EOL?>
				<?=_(' These Pay Points include your PRP/PP and the "Power Points" generated from the purchases made by the memberships and from the GPS Pay Points generated in your personal Group.').PHP_EOL?>
 <br/><br/>
 <?=_('To qualify for Director and Executive Director Pay Levels, you must also maintain "Side Volume" GRP/PP, which is based on the GRP/PP from other than the Partner Group with the highest number of GRP.').PHP_EOL?>
 <?=_('You can see these numbers and who they are in your Group Report.')?></li>
						<div class="toggle">
					<div class="row">
						<div class="six columns">
							<div class="panel">
							
						  			<p><span class="style28"><?=_('EXAMPLE')?></span></p>
						  			
									<p>
									<?=_('The Partner shown below has 60 GRP for their personal group, which meets the Diamond Director qualifier.').PHP_EOL?>
									<?=_('However, since they have a Partner Group that has 40 GRP, they only have 20 GRP Side Volume (60 - 40 = 20).').PHP_EOL?>
									<?=_('Therefore they do NOT meet GRP Side Volume requirement of 24 for the Diamond Director Pay Level.').PHP_EOL?>
									<?=_('Rather they meet the Emerald Director GRP Side Volume requirement of 20 GRP.')?></p>
								
									<div align="center"><img src="images/partner_group.png" height="397" width="161" alt="partner group" /></div>
							</div>
						</div>
						<div class="six columns">
							<div class="panel">
								<p><span class="style28"><?=_('EXAMPLE II')?></span></p>
								
									<p>
									<?=_('The Partner shown below has 70 GRP for their personal group, which meets the Diamond Director qualifier.').PHP_EOL?>
									<?=_('And since their biggest Partner Group has 42 GRP, they only have 28 GRP Side Volume (70 - 42 = 28).').PHP_EOL?>
									<?=_('Therefore they DO meet the GRP Side Volume requirement of 24 for the Diamond Director Pay Level.')?>
									</p>
									<div align="center"><img src="images/partner_groupII.png" height="397" width="161" alt="partner group" /></div>
						 
					</div>	
				</div>
				</div>	
				</div>
				
        		<li class="blue_bullet"><span class="special4"><?=_('Team Pay Points (TPP)')?></span> - 
        		<?php printf(_('this is the total number of GPS Pay Points and %sPower Points%s generated by yourself and your team.'), '<a href="powerpoints.html" onClick="window.open(this.href,\'help\',\'width=835,height=480,scrollbars\'); return false;">', '</a>')?><br />
       		  </li>
       		</ul>
	   </div>
	<div>
	   
	   
	   
			 <div class="row">
		<div class="twelve columns"> 
				<p><span class="style28"><?=_('HOW YOUR TEAM COMMISSIONS ARE CALCULATED')?></span></p>
                <p>
                <?=_('Your commission entitlement is based on two factors.').PHP_EOL?>
                <?=_('Each pay level shown below provides you with a "# of Shares" and a "Share Value".').PHP_EOL?>
                <?=_('These two factors are multiplied by each other to provide you with your Team Commission entitlement.')?></p>
                
              <p><b><?=_('# of Shares X Share Value = Team Commissions')?></b></p>

<p>
<?=_('The "Share Value" is based on what the average GPS Pay Points are for yourself and for the Partners in your Group.').PHP_EOL?>
<?=_(' This average is determined as follows:').PHP_EOL?>
<?=_('"Group GPS Pay Points" divided by your GRP plus 1 (for yourself) equals your average GPS Pay Points per Partner, which provides you with a matching Share Value.').PHP_EOL?>
<?=_('For example, if your Group GPS Pay Points totals 150 and you have 9 GRP (plus yourself to make 10), then your Share Value is 15 (150 / 10 = 15).')?>
<br /></p>

<p><?=_('You can click on the Share Value link shown in the table below to see how the average GPS Pay Points have been calculated to provide the Share Value shown.')?></p>
                
	  <br/><br/>
      <div align="center" id="tablesection"><h4 class="topTitle"><?=_('TEAM COMMISSIONS')?></h4></div>
      <p><br/>
        
        <?=_('The Commissions shown below are based on the average Share Value (GPS PP per Partner) for your country.').PHP_EOL?>
        <?=_('Your actual entitlement as a Partner will be based on the average GPS PP per Partner in your Group.').PHP_EOL?>
        <?=_('Use the drop down list shown below to see what the average Share Value and Commissions are for other countries.')?>
        </p>

      <form method="get" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) . '#tablesection'?>">
        <select name="country" onchange="document.forms[0].submit()"> 
            <option value=""><?=_('SELECT A COUNTRY')?></option>
            <?php foreach ($share_value_countries AS $record):?>
            <option value="<?=$record['country_code']?>"<?php if ($country == $record['country_code']) echo ' selected="selected"'?>><?=$record['country_name']?></option>
            <?php endforeach;?>
             </select>
      </form>

<?php if (array_key_exists('membertype', $Meminfo) && $Meminfo['membertype'] == 'v'):?>
<span class="style3">
    <?php printf(_('%sCLICK HERE%s to view your personal, month-to-date Team Commissions entitlement.'), '<a href="/p/r/PersonalTeamCommissions">', '</a>')?>
</span>
<?php endif;?>
      <br/><br/>
      <table width="100%">

            <tr> 
              <td class="center"><span class="special5"><?=_('PAY LEVEL')?></span></td>
              <td class="center quals"><a href="#" class="tip_trigger">PRP<br/>PP<span class="tip special5" style="display: none;"> PRP/PP - 
<?=_('You can meet this qualifier by either having the applicable number of current Personally Referred Partners OR by your number of Personally Referred Pay Points.').PHP_EOL?>
<?=_('These Pay Points include your personal GPS Pay Points and the "Power Points" generated from your personal purchases and from the purchases made by your personally referred memberships and from the GPS Pay Points of your PRPs.').PHP_EOL?>

<?=_('You can see this number and who they are in your Group Report and in your Income Report.')?></span></a></td>
              
              <td class="center quals"><a href="#" class="tip_trigger">GRP<br/>PP<span class="tip special5" style="display: none;">GRP/PP - 
              <?=_('This is the number of current Partners and the number of Pay Points you have in your personal Group.').PHP_EOL?>
              <?=_('These Pay Points include your PRP/PP and the "Power Points" generated from the purchases made by the memberships and from the GPS Pay Points generated in your personal Group.')?></span></a>
              </td>
              
              <td class="center quals"><a href="#" class="tip_trigger"> SV<br>GRP/PP<span class="tip special5" style="display: none;">
              <?=_('Directors and Executive Directors are required to maintain "Side Volume" GRP/PP, based on their GRP/PP from other than their Partner Group with the highest number of GRP.')?></span></a>
              </td>
              
              <td class="center quals"><a href="#" class="tip_trigger">TPP<span class="tip special5" style="display: none;">
              <?=_('This is the total number of GPS Pay Points and Power Points generated by yourself and your team.')?></span></a>
              </td>

              <td class="center commend"><span class="specialtitles"><b><?=_('# of Shares')?></b></span></td>
              <td class="center commend"><span class="specialtitles"><b><?=_('Share Value')?></b></span></td>
              <td class="center commend"><span class="specialtitles"><b><?=_('Commissions')?></b></span></td>
              </tr>
              
              
            <tr>
              <td valign="middle"><span class="larger_blue_bold navy"><?=_('Active Partner')?></span></td>
              <td class="center quals"><span class="specialtitles">0<br/>10</span></td>
              <td class="center quals"><span class="specialtitles">0<br/>10</span></td>
              <td class="center quals">&nbsp;</td>
              <td class="center quals"><span class="specialtitles">100</span></td>
              <td class="center commend"><?=$tnt_level_values['1']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['1']['shares'])?></td>
            </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/teamplayer.png" width="20" height="20" /> <span class="larger_blue_bold tp"><?=_('Team Player')?></span></td>
              <td class="center quals"><span class="specialtitles">1<br/>25</span></td>
              <td class="center quals"><span class="specialtitles">1<br/>25</span></td>
              <td class="center quals">&nbsp;</td>
              <td class="center quals"><span class="specialtitles">150</span></td>
              <td class="center commend"><?=$tnt_level_values['2']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['2']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/teamleader.gif" width="20" height="20" /> <span class="larger_blue_bold tl"><?=_('Team Leader')?></span></td>
              <td class="center quals"><span class="specialtitles">2<br/>50</span></td>
              <td class="center quals"><span class="specialtitles">3<br/>75</span></td>
              <td class="center quals">&nbsp;</td>
              <td class="center quals"><span class="specialtitles">200</span></td>
              <td class="center commend"><?=$tnt_level_values['3']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['3']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/teamcaptain.gif" width="20" height="20" /> <span class="larger_blue_bold tc"><?=_('Team Captain')?></span></td>
              <td class="center quals"><span class="specialtitles">3<br/>100</span></td>
              <td class="center quals"><span class="specialtitles">6<br/>150</span></td>
              <td class="center quals">&nbsp;</td>
              <td class="center quals"><span class="specialtitles">300</span></td>
              <td class="center commend"><?=$tnt_level_values['4']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['4']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/bronzemanager.jpg" width="20" height="20" /> <span class="larger_blue_bold bm"><?=_('Bronze Manager')?></span></td>
              <td class="center quals"><span class="specialtitles">4<br/>100</span></td>
              <td class="center quals"><span class="specialtitles">10<br/>250</span></td>
              <td class="center quals"><span class="specialtitles">5<br/>105</span></td>
              <td class="center quals"><span class="specialtitles">500</span></td>
              <td class="center commend"><?=$tnt_level_values['5']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['5']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/silvermanager.png" width="21" height="21" /> <span class="larger_blue_bold sm"><?=_('Silver Manager')?></span></td>
              <td class="center quals"><span class="specialtitles">5<br/>125</span></td>
              <td class="center quals"><span class="specialtitles">20<br/>500</span></td>
              <td class="center quals"><span class="specialtitles">10<br/>250</span></td>
              <td class="center quals"><span class="specialtitles">1,000</span></td>
              <td class="center commend"><?=$tnt_level_values['6']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['6']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/goldmanager.jpg" width="21" height="21" /> <span class="larger_blue_bold gm"><?=_('Gold Manager')?></span></td>
              <td class="center quals"><span class="specialtitles">6<br/>150</span></td>
              <td class="center quals"><span class="specialtitles">30<br/>750</span></td>
              <td class="center quals"><span class="specialtitles">15<br/>375</span></td>
              <td class="center quals"><span class="specialtitles">1,500</span></td>
              <td class="center commend"><?=$tnt_level_values['7']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['7']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/rubydirector.png" width="21" height="21" /> <span class="larger_blue_bold rd"><?=_('Ruby Director')?></span></td>
              <td class="center quals"><span class="specialtitles">7<br/>175</span></td>
              <td class="center quals"><span class="specialtitles">45<br/>1,125</span></td>
              <td class="center quals"><span class="specialtitles">23<br/>575</span></td>
              <td class="center quals"><span class="specialtitles">2,000</span></td>
              <td class="center commend"><?=$tnt_level_values['8']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['8']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/emeralddir_pin.jpg" width="22" height="22" /> <span class="larger_blue_bold ed"><?=_('Emerald Director')?></span></td>
              <td class="center quals"><span class="specialtitles">8<br/>200</span></td>
              <td class="center quals"><span class="specialtitles">60<br/>1,500</span></td>
              <td class="center quals"><span class="specialtitles">30<br/>750</span></td>
              <td class="center quals"><span class="specialtitles">3,000</span></td>
              <td class="center commend"><?=$tnt_level_values['9']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['9']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/diamond.png" width="21" height="21" /> <span class="larger_blue_bold dd"><?=_('Diamond Director')?></span></td>
              <td class="center quals"><span class="specialtitles">9<br/>225</span></td>
              <td class="center quals"><span class="specialtitles">80<br/>2,000 </span></td>
              <td class="center quals"><span class="specialtitles">40<br/>1,000</span></td>
              <td class="center quals"><span class="specialtitles">4,000</span></td>
              <td class="center commend"><?=$tnt_level_values['10']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['10']['shares'])?></td>
              </tr>
            <tr>
              <td valign="middle"><img src="/images/pins/exec00.gif" width="21" height="21" /> <span class="larger_blue_bold star"><?=_('Executive Director')?></span></td>
              <td class="center quals"><span class="specialtitles">10<br/>250</span></td>
              <td class="center quals"><span class="specialtitles">100<br/>2,500</span></td>
              <td class="center quals"><span class="specialtitles">50<br>1,250</span></td>
              <td class="center quals"><span class="specialtitles">5,000</span></td>
              <td class="center commend"><?=$tnt_level_values['11']['shares']?></td>
              <td class="center commend"><?=$average_share_value?></td>
              <td class="center commend transposeME"><?php lprintf($tnt_level_values['11']['shares'])?></td>
              </tr>

	      </table>
            </div>
	 </div>
	
	  <div class="row">
		<div class="twelve columns">
	  		<p><span class="style28"><?=_('EXECUTIVE DIRECTOR BONUSES')?></span></p>
				<p>
				<?=_('Executive Director Bonuses are paid in addition to Executive Director Team Commissions.').PHP_EOL?>
				<?=_('As an Executive Director, once you have one or more Executive Directors in your Group, you may be able to qualify for Executive Director Bonuses.').PHP_EOL?>
				<?=_('Each Executive Director in your Group can count as a "Star" credit to help you qualify as a 1 Star, 2 Star, etc. Executive Director!').PHP_EOL?>
				<?=_('You can see the number of Executive Directors that you may have in your Group and who they are, by viewing your Group Report.')?></p>
				
			<p><?=_('See the table below for the various Executive Director Star Pay Level qualifications required.')?></p>
			
            <div class="row">
		<div class="twelve columns"> 
            <table width="100%">
            <tr> 
              <td class="center"><span class="special5"><?=_('STARS')?></span></td>
              <td class="center quals"><span class="special5">PRP</span></td>
              <td class="center quals"><span class="special5">GRP</span></td>
              <td class="center qualsend"><span class="special5">SV GRP</span></td>
              <td class="center qualsend"><span class="special5"><?=_('Star Lines SV*')?></span></td>
              <td class="center qualsend"><span class="special5">TPP</span></td>
              <td class="center comm"><span class="special5"><?=_('# of Shares')?></span></td>
              <td class="center comm"><span class="special5"><?=_('Share Value')?></span></td>
              <td class="center comm"><span class="special5"><?=_('BONUS')?></span></td>
              </tr>

                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">1</span></td>    
				  <td class="center quals"><span class="specialtitles">11</span></td>
                  <td class="center quals"><span class="specialtitles">200</span></td>
                  <td class="center qualsend"><span class="specialtitles">100</span></td>
                  <td class="center qualsend"><span class="specialtitles">1</span></td>
                  <td class="center qualsend"><span class="specialtitles">6,000 </span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['12']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm transposeME"><?php lprintf($tnt_level_values['12']['xdir_bonus_shares'])?></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">2</span></td>    
				  <td class="center quals"><span class="specialtitles">12</span></td>
                  <td class="center quals"><span class="specialtitles">400 </span></td>
                  <td class="center qualsend"><span class="specialtitles">200</span></td>
                  <td class="center qualsend"><span class="specialtitles">1</span></td>
                  <td class="center qualsend"><span class="specialtitles">8,000 </span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['13']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm transposeME"><?php lprintf($tnt_level_values['13']['xdir_bonus_shares'])?></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">3</span></td>    
				  <td class="center quals"><span class="specialtitles">13</span></td>
                  <td class="center quals"><span class="specialtitles">600</span></td>
                  <td class="center qualsend"><span class="specialtitles">300</span></td>
                  <td class="center qualsend"><span class="specialtitles">1</span></td>
                  <td class="center qualsend"><span class="specialtitles">10,000</span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['14']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm transposeME"><?php lprintf($tnt_level_values['14']['xdir_bonus_shares'])?></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">4</span></td>    
				  <td class="center quals"><span class="specialtitles">14</span></td>
                  <td class="center quals"><span class="specialtitles">800 </span></td>
                  <td class="center qualsend"><span class="specialtitles">400</span></td>
                  <td class="center qualsend"><span class="specialtitles">1</span></td>
                  <td class="center qualsend"><span class="specialtitles">12,000</span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['15']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm transposeME"><?php lprintf($tnt_level_values['15']['xdir_bonus_shares'])?></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">5 <?=_("AMBASSADORS' CLUB")?></span></td>
                  <td class="center quals"><span class="specialtitles">15</span></td>
                  <td class="center quals"><span class="specialtitles">1000 </span></td>
                  <td class="center qualsend"><span class="specialtitles">500</span></td>
                  <td class="center qualsend"><span class="specialtitles">2</span></td>
                  <td class="center qualsend"><span class="specialtitles">15,000 </span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['16']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm transposeME"><?php lprintf($tnt_level_values['16']['xdir_bonus_shares'])?></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">6 - 9</span></td>    
				  <td class="center quals"><span class="specialtitles"><?=_('1 More per Star')?></span></td>
                  <td class="center quals"><span class="specialtitles"><?=_('300 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('100 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles">2</span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('3,000 More per Star')?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['17']['xdir_bonus_shares'])?> - 
                  <?=intval($tnt_level_values['20']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm"><span class="transposeME"><?php lprintf($tnt_level_values['17']['xdir_bonus_shares'])?></span> - 
                  <span class="transposeME"><?php lprintf($tnt_level_values['20']['xdir_bonus_shares'])?></span></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">10-14</span></td>    
				  <td class="center quals"><span class="specialtitles"><?=_('1 More per Star')?></span></td>
                  <td class="center quals"><span class="specialtitles"><?=_('300 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('100 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles">3</span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('3,000 More per Star')?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['21']['xdir_bonus_shares'])?> - 
                  <?=intval($tnt_level_values['25']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm"><span class="transposeME"><?php lprintf($tnt_level_values['21']['xdir_bonus_shares'])?></span> - 
                  <span class="transposeME"><?php lprintf($tnt_level_values['25']['xdir_bonus_shares'])?></span></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">15-19</span></td>    
				  <td class="center quals"><span class="specialtitles"><?=_('1 More per Star')?></span></td>
                  <td class="center quals"><span class="specialtitles"><?=_('300 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('100 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles">4</span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('3,000 More per Star')?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['26']['xdir_bonus_shares'])?> - 
                  <?=intval($tnt_level_values['30']['xdir_bonus_shares'])?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm"><span class="transposeME"><?php lprintf($tnt_level_values['26']['xdir_bonus_shares'])?></span> - 
                  <span class="transposeME"><?php lprintf($tnt_level_values['30']['xdir_bonus_shares'])?></span></td>
                </tr>
                
                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">20+</span></td>    
				  <td class="center quals"><span class="specialtitles"><?=_('1 More per Star')?></span></td>
                  <td class="center quals"><span class="specialtitles"><?=_('300 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('100 More per Star')?></span></td>
                  <td class="center qualsend"><span class="specialtitles">5</span></td>
                  <td class="center qualsend"><span class="specialtitles"><?=_('3,000 More per Star')?></span></td>
                  <td class="center comm"><span class="specialtitles"><?=intval($tnt_level_values['31']['xdir_bonus_shares'])?>+</span></td>
                  <td class="center comm"><span class="specialtitles"><?=$average_share_value?></span></td>
                  <td class="center comm"><span class="transposeME"><?php lprintf($tnt_level_values['31']['xdir_bonus_shares'])?></span>+</td>
                </tr>
            
          </table>
	 
	 <br/>
	
<p><span class="style28"><?=_("AMBASSADORS' CLUB")?></span></p>
<p><?=_("Qualify as a 5 Star + Executive Director and become a member of the Ambassadors' Club and earn an annual, expense paid 3 night cruise with your fellow Ambassadors' Club members!")?></p>

<div class="toggle">
					<div class="row">
						<div class="six columns">
							<div class="panel">
							
						  			<ul>
                                    <li class="blue_bullet"><?=_("Based on qualifying as an Ambassador's Club member in the month of December.")?></li>
<li class="blue_bullet"><?=_("Provides two tickets for a cruise to be taken sometime the next year, if a cruise embarkation port is available in the same country as residency for any of the following cruise lines: Carnival, NCL, RCL, MSC or Costa.")?></li>
<li class="blue_bullet"><?=_("If there are no embarkation ports available or if a cruise date cannot be used, then a $500 bonus to be paid by July 31st will be substituted for this benefit.")?></li>
                                    </ul>
                                    
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    
	<br/>
<p><span class="style28"><?=_('CROWN AMBASSADOR')?></span></p>
<p><?=_('Qualify as a 10 Star + Executive Director and become a Crown Ambassador and earn an annual, expense paid 7 night cruise with your fellow Crown Ambassadors!')?></p>

<div class="toggle">
					<div class="row">
						<div class="six columns">
							<div class="panel">
							
						  			<ul>
                                    <li class="blue_bullet"><?=_("Based on qualifying as a Crown Ambassador in the month of December.")?></li>
<li class="blue_bullet"><?=_("Provides two tickets for a cruise to be taken sometime the next year, if a cruise embarkation port is available in the same country as residency for any of the following cruise lines: Carnival, NCL, RCL, MSC or Costa.")?></li>
<li class="blue_bullet"><?=_("If there are no embarkation ports available or if a cruise date cannot be used, then a $1,000 bonus to be paid by July 31st will be substituted for this benefit.")?></li>
                                    </ul>
                                    
                                    </div>
                                    </div>
                                    </div>
                                    </div>

	 
	 </div>
	 </div>
            
	</div>
	</div>
            <br/><br/>
</div>

</div>
</div>
</div>
</div>

<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">

        <p><?php printf(_('Copyright © 1997-%s Proprofit Worldwide Ltd., All Rights Reserved.'), date("Y"))?></p>

    </div>
</div>
</div>
</div>
</body>
</html>


