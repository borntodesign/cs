﻿<!DOCTYPE HTML>
<html>
<head>
<title>ClubShop Rewards Affiliate Compensation Plan</title>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
<script src="http://www.clubshop.com/js/partner/flash.js"></script>

<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<script src="http://www.clubshop.com/js/partner/app.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<!--<script type="text/javascript" src="_includes/js/tip.js"></script>-->
<script type="text/javascript"src="http://www.glocalincome.com/js/tipMap.js"> </script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style>
.styleLarge {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	text-transform: uppercase;
}

td.center {text-align:center;}
</style>
<script language="javascript">
   
	$(document).ready(function() {
	var showText='Learn More';
	var hideText='Hide';
	var is_visible = false; 

	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');
	$('.toggle').hide();
	$('a.toggleLink').click(function() { 

	is_visible = !is_visible;
	$(this).html( (!is_visible) ? showText : hideText);
	 
	// toggle the display - uncomment the next line for a basic "accordion" style
	//$('.toggle').hide();$('a.toggleLink').html(showText);
	$(this).parent().next('.toggle').toggle('slow');
	 
	// return false so any link destination is not followed
	return false;
	 
	});
	});
 
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle">ClubShop Rewards Handleiding</h4>
<br />
<div id="google_translate_element"></div>
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<p>De Google Vertaler hierboven, is beschikbaar gesteld zodat al onze wereldwijde ClubShop Rewards Partners over een vertrouwd klinkende vertaling kunnen beschikken. Hoewel een door een computerprogramma vertaalde versie van een webpagina wellicht niet een vertaling is van hoge kwaliteit, toch wordt op deze manier onze Zakelijke Mogelijkheid van ClubShop Rewards wereldwijd toegankelijker.</p>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
<span class="ContactForm_TextField"><strong>ClubShop Rewards AFFILIATE Compensatieplan</strong></span> <br /> 
<hr />
<p>Het Compensatieplan hieronder, is een gedetaileerde presentatie hoe u een inkomen kunt verdienen als Affiliate. Affiliate compensatie wordt maandelijks betaald, waarbij de daadwerkelijke betaling na een maand wachttijd plaatsvind, voor het geval er aanpassingen in de betaling nodig zijn. Bijvoorbeeld, recht op Affiliate inkomen voor de maand januari, wordt begin maart uitbetaald.</p>
<p>De hoeveelheid ClubCash        die u verdient op elke aankoop wordt berekend uit het percentage dat naast de ClubShop Rewards online- en offline winkellijsten te zien is. All ClubCash betalingen kunnen worden ingewisseld voor contant geld terug in uw eigen valuta.</p>
<p><span class="style28">Pay Points &amp; Personal Commissions</span></p>
<p>Pay punten is de commissiewaarde die is toegekend aan elk type <a class="tip_trigger" href="#">gekwalificeerde aankopen, <span class="tip">ClubShop Rewards winkelcentrum aankopen en offline "Tracking"        winkel aankopen</span></a> die zijn gedaan door uzelf en de door u persoonlijk verwezen        ClubShop Rewards Members. Als een Affiliate verdient u Personal Commissies        die gelijk staan aan 1/2 van de Pay punten waarde die wordt geboden voor de aankopen die gedaan zijn door de Members die u verwijst of door de members die op hun beurt verwijzen.</p>
<p>De hoeveelheid Pay punten die verdient wordt per aankoop, kunt u zien op de Winkel- en bedrijven lijsten via <a href="http://www.clubshop.com" target="_blank">www.clubshop.com</a>. De waarde van 1 Pay punt =<span class="transposeME">$1.00</span></p>
<br />
<div class="row">
<div class="eleven columns centered">
<div class="five columns">
<div class="panel">
<div align="center">
<h5 class="style28">Compensatieplan, voorbeeld 1</h5>
</div>
<br />
<p>U (Affiliate) doet een <span class="transposeME">$100.00</span> aankoop bij een ClubShop Online winkelcentrum winkel die 10% ClubCash biedt en 4% Pay punten.<br /><br /> <br /><img src="http://www.clubshop.com/images/minions/affiliate_smaller.png" /><br /> <span class="styleLarge darkbrown">U (Affiliate) verdient <span class="transposeME">$10.00</span> ClubCash</span><br /> <span class="styleLarge darkbrown bold">EN</span><br /> <span class="styleLarge darkbrown">4 Pay punten (to earn 40% of these Pay Points as Personal Commissions and to help you gain the Qualifying Pay Points you need to become a Partner)</span><br /><br /><br /></p>
<hr />
<em>Elke maand dat een Affiliate ten minste 10 Pay punten verdient, zal deze kwalificeren als Partner en heeft deze het recht om Partner inkomen te verdienen.</em>
<p>Â</p>
</div>
</div>
<div class="seven columns">
<div class="panel">
<div align="center">
<h5 class="style28">Compensatieplan, voorbeeld 2</h5>
</div>
<br />
<p class="normal">Een ClubShop Rewards Member die <span style="text-decoration: underline;">u hebt verwezen</span> doet een aankoop van <span class="transposeME">$100.00</span> bij een ClubShop Online winkelcentrum winkel die 10% ClubCash biedt en 4% Pay punten.</p>
<p><img src="http://www.clubshop.com/images/minions/member_smaller.png" /><br /> <span class="styleLarge royal">De ClubShop Rewards Member verdient <span class="transposeME">$10.00</span> ClubCash</span><br /><br /> <img src="http://www.clubshop.com/images/minions/affiliate_smaller.png" /><br /> <span class="styleLarge darkbrown">U (Affiliate) verdient 4 Pay punten</span><br /> <span class="styleLarge darkbrown bold">EN</span><br /> <span class="styleLarge darkbrown">50% Personal Commissies = <span class="transposeME">$2.00</span> </span></p>
<hr />
            <em>Elke maand dat een Affiliate tenminste 10 Pay punten vertdien, 
            zal deze kwalificeren als Partner en heeft deze het recht om Partner 
            inkomen te verdienen.</em> 
            <p>Â</p>
</div>
</div>
</div>
</div>
<br /> 
<hr />
<br />
<p><span class="style28">ClubShop Rewards kaarten verkopen</span></p>
<p>Affiliates kunnen ook kaarten aankopen tegen groothandelprijs en deze weer verkopen met winst, om een winst te verdienen op elke verkochte kaart.</p>
<br />
<p><span class="style28">PARTNER TEAM COMMISSIONS</span></p>
<p>Once have become a Partner, you will be to benefit from <a href="http://www.clubshop.com/affiliate/2.0.html" target="_blank">TNT 2.0</a> to work with a team of Partners to build a global sales team and earn Team Commissions to develop a full time, residual income. (Note: The best and easiest way to qualify as a Partner is to <a href="http://www.clubshop.com/gps_new.html" target="_blank">subscribe to GPS</a>).
    </p>
<br/>
<p>Team Commissions are override commissions paid to you, based on the purchases and sales made by the Partners on your team. The amount of Team Commissions you earn is based on the Pay Levels you achieve as a Partner.</p>

<p>There are two different qualifications to earn Pay Level promotions and increased Team Commissions:<br/>
	<ol>
    <li>Personally Referred Partners (PRP) - This is the number of Partners that you personally refer and sponsor.</li>

    <li>Team Pay Points (TPP) - This is the total number of Pay Points generated each month by you and your Partner team.</li> 
	</ol>
</p>


























 
<br /><br />

<p class="styleLarge royal bold">Raadpleeg de tabel hieronder om het potentiÃ«le inkomen te zien, dat u kunt verdienen als partner.</p>
<br /><br /> 
    <table width="100%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">
      <tr class="d"> 
        <td width="22%" class="center"><div align="center"><span class="medblue bold">Personal 
            Referred Partners</span></div></td>
        <td width="27%" class="center"><div align="center"><span class="medblue bold">Team 
            Pay Points</span></div></td>
        <td width="24%"><div align="center"><span class="medblue bold">Pay 
            Title</span></div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">1</div></td>
        <td class="center"><div align="center">100</div></td>
        <td><div align="center">Team Player</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">2</div></td>
        <td class="center"><div align="center">200</div></td>
        <td><div align="center">Team Leader</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">3</div></td>
        <td class="center"><div align="center">400</div></td>
        <td><div align="center">Team Captain</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">5</div></td>
        <td class="center"><div align="center">800</div></td>
        <td><div align="center">Bronze Manager</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">6</div></td>
        <td class="center"><div align="center">1,200</div></td>
        <td><div align="center">Silver Manager</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">7</div></td>
        <td class="center"><div align="center">1,600</div></td>
        <td><div align="center">Gold Manager</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">10</div></td>
        <td class="center"><div align="center">2,100</div></td>
        <td><div align="center">Ruby Director</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">11</div></td>
        <td class="center"><div align="center">2,600</div></td>
        <td><div align="center">Emerald Director</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">12</div></td>
        <td class="center"><div align="center">3,100</div></td>
        <td><div align="center">Diamond Director</div></td>
      </tr>
      <tr class="b"> 
        <td height="29" class="center"><div align="center">15</div></td>
        <td class="center"><div align="center">3,600</div></td>
        <td><div align="center">Executive Director</div></td>
      </tr>
    </table>
		 <br/><br/>
    <table width="100%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">
      <tr class="d"> 
        <td class="center"><div align="center"><span class="medblue bold">Personal 
            Referred Partners</span></div></td>
        <td class="center"><div align="center"><span class="medblue bold">Team 
            Pay Points</span></div></td>
        <td class="center"><div align="center"><span class="medblue bold">Executive 
            Directors</span></div></td>
        <td><div align="center"><span class="medblue bold">Pay Title</span></div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">25</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">1</div></td>
        <td class="center"><div align="center">1 Star</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">30</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">2</div></td>
        <td class="center"><div align="center">2 Star</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">35</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">3</div></td>
        <td class="center"><div align="center">3 Star</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">40</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">4</div></td>
        <td class="center"><div align="center">4 Star</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">45</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">5</div></td>
        <td class="center"><div align="center">5 Star</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">50+</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">6</div></td>
        <td class="center"><div align="center">6 Star</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">50+</div></td>
        <td class="center"><div align="center">N/A</div></td>
        <td class="center"><div align="center">10</div></td>
        <td class="center"><div align="center">10 Star</div></td>
      </tr>
    </table>
<p><span class="style28">GPS MINIMUM COMMISSIE GARANTIE</span><br /></p>
<p>Abonneer op GPS BASIS of PRO om Partner te worden en kwalificeer voor de GPS MINIMUM COMMISSIE GARANTIE:</p>
<ul>
<li class="blue_bullet">GPS BASIS abonnees die tenminste 1 PRO of PREMIER abonnee hebben verwezen, hebben de garantie dat zij voldoende aan commissies verdienen om de maandelijkse bijdrage van hun BASIS abonnement van deze verdienste af te aten trekken.</li>
<li class="blue_bullet">GPS BASIS abonnees die ten minste twee BASIS of hogere abonnees hebben verwezen, hebben de garantie voldoende te verdienen aan commissie om hun maandelijkse BASIS abonnementsbijdrage van hun verdienste af te laten trekken. </li>
<li class="blue_bullet">GPS PRO abonnees die ten minste twee PRO of hogere abonnees hebben verwezen, hebben de garantie dat zij voldoende commissies verdienen om hun maandelijkse PRO abonnementsbijdrage van deze verdienste af te laten trekken.</li>
</ul>

</div>
</div>

<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" eight columns centered">          
                <div id=" footer ">
                    <p>Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();

                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->

</body>
</html>