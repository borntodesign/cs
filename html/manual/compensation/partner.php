<?php
require 'Clubshop/Session/Session.php';
require 'Clubshop/Models.php';
require 'Clubshop/NegotiateByLanguage.php';

$Session = new \Clubshop\Session\Session();
$Meminfo = $Session->meminfoFromCookie();
if (!$Meminfo)
    $Meminfo = array(); // the template page will be expecting this even if it is empty

$warnings = array();    # same concept
$Model = new \Clubshop\Models('generic');

// setup language negotiation stuff
$negotiator = new \Clubshop\NegotiateByLanguage();
$tmpl = $negotiator->bestCandidateFilename('_partner_.php');
$tmp = array();
// we will use that file name later on, but based upon that, we will make an assumption on what the page language will be
preg_match('/\.([a-z]+)$/', $tmpl, $tmp);
// that should give us the extension
$pageLanguage = $tmp[1];
$pageLocale = $negotiator->bestSupportedLocale($pageLanguage);
//error_log("page language: $pageLanguage - locale: $pageLocale");
putenv("LC_ALL=$pageLocale");
if (setlocale(LC_MESSAGES, $pageLocale) === false)
    error_log("setlocale failed for page language: $pageLanguage - locale: $pageLocale");
bindtextdomain("languagepack", "locale");
bind_textdomain_codeset('languagepack', 'UTF-8');
textdomain("languagepack");
// end of language setup stuff

$country = isset($_GET['country']) ? $_GET['country'] : '';    // we may have an incoming parameter

$country = $country ? $country : (isset($_COOKIE['country']) ? $_COOKIE['country'] : '');

if (! ($country && preg_match('/^[A-Z]{2}$/', $country))) {
    // maybe we can identify the country by the membership
    if ($Meminfo) {
        $country = $Model->dbc()->fetchSingleCol('SELECT country FROM members WHERE id= ?', array($Meminfo['id']));
        if ($country) {
            setcookie('country', $country, null, null, 'clubshop.com');
        }
    }
}
$country = $country ? $country : 'US';

$average_share_value = $Model->dbc()->fetchSingleCol('SELECT avg_pp FROM network.avg_pp_by_country WHERE country= ?', array($country));
if (! $average_share_value) {
    $warnings[] = sprintf(_('We do not have an average share value for %s.'), $country) . _('Using US instead');
    $average_share_value = $Model->dbc()->fetchSingleCol("SELECT avg_pp FROM network.avg_pp_by_country WHERE country= 'US'");
}

$currency_code = $Model->dbc()->fetchSingleCol("SELECT currency_code FROM currency_by_country WHERE country_code= ?", array($country));

$share_value_countries = $Model->dbc()->fetchList_assoc("
    WITH tcn AS (SELECT country_code, country_name FROM country_names_translated(?))
    SELECT cc.country_code, tcn.country_name
    FROM tbl_countrycodes cc
    JOIN tcn
        ON tcn.country_code=cc.country_code
    JOIN network.avg_pp_by_country apbc
        ON apbc.country=cc.country_code
    ORDER BY tcn.country_name
    ", array($pageLanguage));

$tnt_level_values = $Model->dbc()->fetchAll_assoc("
    SELECT pay_level, shares, xdir_bonus_shares
    FROM network.level_values_tnt", 'pay_level');

include "$tmpl";
?>