﻿<!DOCTYPE HTML>
<html>
<head>
<title>ClubShop Rewards Affiliate Compensation Plan</title>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
<script src="http://www.clubshop.com/js/partner/flash.js"></script>

<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<script src="http://www.clubshop.com/js/partner/app.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<!--<script type="text/javascript" src="_includes/js/tip.js"></script>-->
<script type="text/javascript"src="http://www.glocalincome.com/js/tipMap.js"> </script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style>
.styleLarge {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	text-transform: uppercase;
}

td.center {text-align:center;}
</style>
<script language="javascript">
   
	$(document).ready(function() {
	var showText='Learn More';
	var hideText='Hide';
	var is_visible = false; 

	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');
	$('.toggle').hide();
	$('a.toggleLink').click(function() { 

	is_visible = !is_visible;
	$(this).html( (!is_visible) ? showText : hideText);
	 
	// toggle the display - uncomment the next line for a basic "accordion" style
	//$('.toggle').hide();$('a.toggleLink').html(showText);
	$(this).parent().next('.toggle').toggle('slow');
	 
	// return false so any link destination is not followed
	return false;
	 
	});
	});
 
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle">ClubShop Rewards Manual</h4>
<br />
<div id="google_translate_element"></div>
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<p>De Google Vertaler hierboven, is beschikbaar gesteld zodat al onze wereldwijde ClubShop Rewards Partners over een vertrouwd klinkende vertaling kunnen beschikken. Hoewel een door een computerprogramma vertaalde versie van een webpagina wellicht niet een vertaling is van hoge kwaliteit, toch wordt op deze manier onze Zakelijke Mogelijkheid van ClubShop Rewards wereldwijd toegankelijker.</p>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
    <span class="ContactForm_TextField"><strong>ClubShop Rewards MERCHANT Compensation 
    Plan</strong></span> <br /> 
<hr />
<p>The Compensation Plan shown below is a detailed presentation of how you        can earn income as a Merchant. Merchant compensation is paid on a monthly        basis, with the actual payment being made after a monthly waiting period        in case any pay adjustments are required to be made. For example, entitlement        to Merchant income for the month of January, is paid the beginning of March.</p>
    <p>
      <!--<span class="style28">ClubCash</span><br />-->
      The amount of ClubCash that you earn on each purchase is calculated from 
      the percentage shown next to the ClubShop Rewards online and offline store 
      listings. All ClubCash redemptions can be redeemed for cash back in your 
      currency.</p>
<p><span class="style28">Pay Points & Referral Commissions</span></p>
<p>Pay Points are the commissionable value assigned to each type of <a class="tip_trigger" href="#">qualifying        purchases <span class="tip">ClubShop Rewards Mall purchase and Offline "Tracking"        Merchant purchase</span></a> made by yourself and by your personally referred        ClubShop Rewards Members. As a Merchant you earn Referral Commissions equal        to 1/2 of the Pay Point value provided for these purchases made by the Members        you refer, or by the Members they refer.</p>
<p>The amount of Pay Points earned per purchase is shown at the Store and Merchant listings at <a href="http://www.clubshop.com" target="_blank">www.clubshop.com</a>.  The value of 1 Pay Point =<span class="transposeME">$1.00</span></p>
<!--
<p><span class="style28">Referral Commissions</span><br /><br /> Affiliates earn Referral Commissions on  <a class="tip_trigger" href="#" mce_href="#">qualifying        purchases <span class="tip">ClubShop Rewards Mall purchase and Offline "Tracking"        Merchant purchase</span></a> made by the members an Affiliate refers. Referral Commissions are equal to 1/2 of the Pay Point value provided to Affiliates.</p>
<p><span class="style28">Pay Points</span><br><br> As an Affiliate you earn Pay Points on <a class="tip_trigger" href="#" mce_href="#">qualifying        purchases <span class="tip">ClubShop Rewards Mall purchase and Offline "Tracking"        Merchant purchase</span></a> made by yourself and by your personally referred ClubShop Rewards Members. Pay Points are the commissionable value assigned to each type of purchase and the value of 1 Pay Point = <span class="transposeME">$1.00</span>. The Pay Points value for participating ClubShop Rewards Mall stores and Offline "Tracking" Merchants are shown in each store/merchant listing at <a href="http//www.clubshop.com/" mce_href="http//www.clubshop.com/" target="_blank">www.clubshop.com</a>.</p>
--> 	 	 	 	<br />
<div class="row">
<div class="eleven columns centered">
<div class="five columns">
<div class="panel">
<div align="center">
<h5 class="style28">Compensation Plan Example 1</h5>
</div>
<br />
<p>You (Merchant) make a <span class="transposeME">$100.00</span> purchase at a ClubShop Online Mall store that offers 10% ClubCash                and 4% Pay Points.<br /> <br /> <br /> <img src="http://www.clubshop.com/images/minions/affiliate_smaller.png" /><br /> <span class="styleLarge darkbrown">You (Merchant) earn <span class="transposeME"> $10.00</span> ClubCash</span><br /> <span class="styleLarge darkbrown bold">AND</span><br /> <span class="styleLarge darkbrown">4 Pay Points (to help you gain                the Qualifying Pay Points you need to become a Partner)</span><br /> <br /> <br /></p>
<hr />
            <em>Any month that a Merchant earns at least 10 Pay Points, they will 
            qualify to be a Partner and be entitled to earn Partner Income.</em> 
            <p> </p>
</div>
</div>
<div class="seven columns">
<div class="panel">
<div align="center">
<h5 class="style28">Compensation Plan Example 2</h5>
</div>
<br />
<p class="normal">A ClubShop Rewards Member that <span style="text-decoration: underline;">you referred</span> makes a <span class="transposeME">$100.00</span> purchase at a ClubShop Online Mall store that offers 10% ClubCash and 4% Pay Points.</p>
<p><img src="http://www.clubshop.com/images/minions/member_smaller.png" /><br /> <span class="styleLarge royal">ClubShop Rewards Member earns <span class="transposeME">$10.00</span> ClubCash</span><br /> <br /> <img src="http://www.clubshop.com/images/minions/affiliate_smaller.png" /><br /> <span class="styleLarge darkbrown">You (Merchant) earn 4 Pay Points</span><br /> <span class="styleLarge darkbrown bold">AND</span><br /> <span class="styleLarge darkbrown">50% Referral Commissions = <span class="transposeME">$2.00</span> </span></p>
<hr />
            <em>Any month that a Merchant earns at least 10 Pay Points, they will 
            qualify to be a Partner and be entitled to earn Partner Income.</em> 
            <p> </p>
</div>
</div>
</div>
</div>
<br /> 
<hr />
<br />
<p><span class="style28">ClubShop Rewards Card Sales</span></p>
<p>Merchant are also able to purchase cards at "wholesale" and sell them at retail,        to earn a profit from each card sold.</p>
<br />
<p><span class="style28">PARTNER INCOME</span></p>
    <p>As a Merchant, once you have gained 10 Qualifying Pay Points from your personal 
      purchases and your referred Members' purchases, you will qualify as a Partner 
      and be able to build a global sales team and develop a full time, residual 
      income. (Note: The best and easiest way to qualify as a Partner is to subscribe 
      to the GPS).<br>
      <p><span class="style28">PARTNER TEAM COMMISSIONS</span></p>

    <p>Once you have become a Partner, you will be able to benefit from <a href="http://www.clubshop.com/affiliate/2.0.html" target="_blank">TNT 2.0</a> to work with a team of Partners to build a global sales team and earn Team Commissions to develop a full time, residual income. (Note: The best and easiest way to qualify as a Partner is to <a href="http://www.clubshop.com/gps_new.html" target="_blank">subscribe to GPS</a>).
    </p>

<p>Team Commissions are override commissions paid to you, based on the purchases and sales made by the Partners on your team. The amount of Team Commissions you earn is based on the Pay Levels you achieve as a Partner.</p>

<p>There are two different qualifications to earn Pay Level promotions and increased Team Commissions:<br/>
	<ol>
    <li>Personally Referred Partners (PRP) - This is the number of Partners that you personally refer and sponsor.</li>

    <li>Team Pay Points (TPP) - This is the total number of Pay Points generated each month by you and your Partner team.</li> 
	</ol>
</p>

<p class="styleLarge royal bold">Refer to the chart below to see the potential income you can earn as a Partner.</p>
<br />
    <table width="100%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">
      <tr class="d"> 
        <td width="22%" class="center"><div align="center"><span class="medblue bold">Personal 
            Referred Partners</span></div></td>
        <td width="27%" class="center"><div align="center"><span class="medblue bold">Team 
            Pay Points</span></div></td>
        <td width="24%"><div align="center" class="center"><span class="medblue bold">Pay 
            Title</span></div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">0</div></td>
        <td class="center"><div align="center">100</div></td>
        <td><div align="center">Active Partner</div></td>
      </tr>
      
      
      <tr class="b"> 
        <td class="center"><div align="center">1</div></td>
        <td class="center"><div align="center">100</div></td>
        <td><div align="center">Team Player</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">2</div></td>
        <td class="center"><div align="center">100</div></td>
        <td><div align="center">Team Leader</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">3</div></td>
        <td class="center"><div align="center">400</div></td>
        <td><div align="center">Team Captain</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">4</div></td>
        <td class="center"><div align="center">700</div></td>
        <td><div align="center">Bronze Manager</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">5</div></td>
        <td class="center"><div align="center">1,100</div></td>
        <td><div align="center">Silver Manager</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">6</div></td>
        <td class="center"><div align="center">1,600</div></td>
        <td><div align="center">Gold Manager</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">7</div></td>
        <td class="center"><div align="center">2,300</div></td>
        <td><div align="center">Ruby Director</div></td>
      </tr>
      <tr class="a"> 
        <td class="center"><div align="center">8</div></td>
        <td class="center"><div align="center">2,900</div></td>
        <td><div align="center">Emerald Director</div></td>
      </tr>
      <tr class="b"> 
        <td class="center"><div align="center">9</div></td>
        <td class="center"><div align="center">3,500</div></td>
        <td><div align="center">Diamond Director</div></td>
      </tr>
      <tr class="b"> 
        <td height="29" class="center"><div align="center">10</div></td>
        <td class="center"><div align="center">4,200</div></td>
        <td><div align="center">Executive Director</div></td>
      </tr>
    </table>
		 <br/><br/>
    <table width="100%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">
      <tr class="d"> 
      
      <td><div align="center" class="center"><span class="medblue bold">Pay 
            Title</span></div></td>
        <td class="center"><div align="center"><span class="medblue bold">Personal 
            Referred Partners</span></div></td>
        <td class="center"><div align="center"><span class="medblue bold">Team 
            Pay Points</span></div></td>
        <td class="center"><div align="center"><span class="medblue bold">Executive 
            Directors</span></div></td>
        
      </tr>
      <tr class="b"> 
      	<td class="center"><div align="center">1 Star</div></td>
        <td class="center"><div align="center">11</div></td>
        <td class="center"><div align="center">5,000</div></td>
        <td class="center"><div align="center">1</div></td>
        
      </tr>
      <tr class="a">
      	<td class="center"><div align="center">2 Star</div></td> 
        <td class="center"><div align="center">12</div></td>
        <td class="center"><div align="center">6,000</div></td>
        <td class="center"><div align="center">2</div></td>
        
      </tr>
      <tr class="b">
      	<td class="center"><div align="center">3 Star</div></td> 
        <td class="center"><div align="center">13</div></td>
        <td class="center"><div align="center">7,000</div></td>
        <td class="center"><div align="center">3</div></td>
        
      </tr>
      <tr class="a">
      	<td class="center"><div align="center">4 Star</div></td> 
        <td class="center"><div align="center">14</div></td>
        <td class="center"><div align="center">8,000</div></td>
        <td class="center"><div align="center">4</div></td>
        
      </tr>
      <tr class="b">
      	<td class="center"><div align="center">5 Star</div></td> 
        <td class="center"><div align="center">15</div></td>
        <td class="center"><div align="center">9,000</div></td>
        <td class="center"><div align="center">5</div></td>
        
      </tr>
      
      <tr class="a"> 
      	<td class="center"><div align="center">10 Star</div></td>
        <td class="center"><div align="center">20</div></td>
        <td class="center"><div align="center">14,000</div></td>
        <td class="center"><div align="center">10</div></td>
        
      </tr>
    </table>
<p><span class="medblue bold">** Average Monthly Income</span> - The figures 
      shown are based on the actual entitlement that some Partners have qualified 
      for at each pay level. No guarantee of earnings are made, however this provides 
      you with a genuine perspective of what your potential entitlement can be 
      for each pay level.</p>

    
	
	
	
	
	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br>	<br><br>	<br><br>	
	
	
    </div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd.,All Rights Reserved.
                    </p> 
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>