<!DOCTYPE HTML>
<html>
<head>
<title>ClubShop Rewards Affinity Group Compensation Plan</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
<script src="http://www.clubshop.com/js/partner/flash.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<script src="http://www.clubshop.com/js/partner/app.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<!--<script type="text/javascript" src="_includes/js/tip.js"></script>-->
<script type="text/javascript"src="http://www.glocalincome.com/js/tipMap.js"> </script>
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle">Manuel du ClubShop Rewards</h4>
</div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
<span class="ContactForm_TextField"><strong>Plan de Compensation ClubShop Rewards pour les Associations <br /></strong></span> 
<hr />
<p>Une association (organisation à but non lucratif) peut réunir des fonds en référençant des personnes pour qu'elles deviennent membres du ClubShop Rewards, ainsi qu'en vendant des cartes ClubShop Rewards. Les compensations pour les Associations sont payées tous les mois, pour le mois précédent le mois venant de se terminer, pour le cas où il y aurait des ajustements à faire dans le calcul du paiement. Par exemple, le paiement dû à l'association pour le mois de Janvier sera payé début Mars.</p>
<p><span class="style28">Dons de Bienfaisance</span><br /><br />Les groupes d'affinité sont en mesure de recevoir les dons de bienfaisance provenant des  <a class="tip_trigger" href="#">achats admissibles <span class="tip">du ClubShop Rewards, basés sur les  faits au centre commercial ClubShop Rewwards et chez les commerçants hors ligne effectuant un "Suivi"</span></a>, achats effectués par les membres référés qu'ils référencient. Les Dons de Bienfaisance sont égaux à 1/4 de la valeur ClubCash fourni au membre référé.</p>
<p><span class="style28">Ventes de cartes ClubShop Rewards </span></p>
<p>Les Associations peuvent également acheter des cartes au "prix de gros" et les revendre au détail, pour gagner un profit sur chaque carte vendue.</p>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, Tous droits réservés.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>