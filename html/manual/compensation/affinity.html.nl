﻿<!DOCTYPE HTML>
<html>
<head>
<title>ClubShop Rewards Affinity Group Compensation Plan</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
<script src="http://www.clubshop.com/js/partner/flash.js"></script>

<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<script src="http://www.clubshop.com/js/partner/app.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<!--<script type="text/javascript" src="_includes/js/tip.js"></script>-->
<script type="text/javascript"src="http://www.glocalincome.com/js/tipMap.js"> </script>
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle">ClubShop Rewards Handleiding</h4>
<br />
<div id="google_translate_element"></div>
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<p>De Google Vertaler hierboven, is beschikbaar gesteld zodat al onze wereldwijde ClubShop Rewards Partners over een vertrouwd klinkende vertaling kunnen beschikken. Hoewel een door een computerprogramma vertaalde versie van een webpagina wellicht niet een vertaling is van hoge kwaliteit, toch wordt op deze manier onze Zakelijke Mogelijkheid van ClubShop Rewards wereldwijd toegankelijker.</p>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
<span class="ContactForm_TextField"><strong>ClubShop Rewards Affinity Group Compensatieplan</strong></span> <br /> 
<hr />
<p>Een Affinity Group (non-profit organisatie) kan fondsen werven door het werven van mensen om ClubShop Reward Members te worden en door ClubShop Rewards kaarten te verkopen. Affinity Group compensatie wordt op maandelijkse basis betaald, waarbij de daadwerkelijke betaling na een maand wachttijd wordt overgemaakt, voor het geval er betalingsorrecties moeten plaatsvinden. Bijvoorbeeld, recht op Affinity Group inkomen voor de maand januari, wordt begin maart betaald.</p>
<p><span class="style28">Charitable Donations</span><br /><br /> Affinity Groups are able to receive Charitable Donations from ClubShop Rewards based on  <a class="tip_trigger" href="#">qualifying        purchases <span class="tip">ClubShop Rewards Mall purchase and Offline "Tracking"        Merchant purchase</span></a> made by the referred Members they refer. Charitable Donations are equal to 1/4 of the ClubCash value provided to the referred Member.</p>
<p><span class="style28">Verkopen van ClubShop Rewardskaarten</span><br /><br /> Non-profit organisaties kunnen tevens kaarten kopen tegen <span class="medblue bold">"groothandelsprijzen"</span> en deze met winst verkopen om geld te verdienen op elke kaart die verkocht wordt.</p>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>