<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="member.xsl" ?><lang_blocks><p1>Members Referring Voordelen</p1><p2>Verwijs- en Sponsorregels</p2><p3>MEMBER TYPE</p3><p4>U KAN VERWIJZEN</p4><p5>Partner</p5><p6>Member</p6><p7>Non-profit Organisatie</p7><p9>Ja</p9><p10>Nee</p10><p11>Winkel</p11><p12>Affiliate</p12><p13>Referral Regels</p13><pmember>Clubshop Rewards Members</pmember>

<pmember1>ClubCash - kunnen ClubCash verdienen op hun persoonlijke aankopen via het ClubShop winkelcentrum of offline bij deelnemende, tracking Winkels/ bedrijven.</pmember1>
<pmember2>Verwijs een Vriend Programma - Members kunnen deelnemen aan ons "Verwijs een Vriend" programma om mensen die zij kennen  te verwijzen om Members te worden. Zou een Member die zij hebben verwezen een prijs winnen, dan winnen zij een bijpassende prijs.</pmember2>
<!--<pmember3>Referral Rewards - Leden kunnen zich registreren om Referral Rewards te ontvangen, in plaats van in aanmerking te komen om bijpassende prijzen te winnen. Zou een Member die zij hebben verwezen een gekwalificeerde aankoop doen, dan verdienen zij Cash Referral Reward gelijk aan 1/4 van de ClubCash waarde die de verwezen Member krijgt.</pmember3>
<pmember4>Kaartverkoop - Members kunnen ClubShop Reward kaarten kopen voor "groothandelsprijs" om deze kaarten weer met winst te verkopen en het verschil te verdienen, zij krijgen dan de referral credit voor elke kaarthouder die diens lidmaatschap activeert via</pmember4>
-->
<pmember5>Members Compensatieplan:</pmember5>
<pmember6 new="4/1/12">Members komen niet in aanmerking om te worden beloond voor het verwijzen van winkels/ bedrijven, non-profit organisaties, Affiliates of Partners. Echter, zou een Member die zij hebben verwezen ervoor kiezen om 1 van de andere lidmaatschappen te nemen, dan blijft de verwijzende member tot het eind van de maand in aanmerking komen om diens lidmaatschap te veranderen in die van Affiliate en te kwalificeren als een Partner om aanmerking te blijven komen om te worden beloond voor hun doorverwijzing.</pmember6>

</lang_blocks>
