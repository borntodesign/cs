<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
   <xsl:template match="/">
   
   
   
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>ClubShop Rewards Partner Manual </title>
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <script src="http://www.clubshop.com/js/panel.js"></script>
    
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<html>
<body class="blue">


<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
<div class="six columns"></div>

</div></div>

<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"></h4>
<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<hr/>
	

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">

			<span class="style24"><xsl:value-of select="//lang_blocks/p2"/></span><br/>

		<br/>
			<img src="http://www.clubshop.com/images/minions/partner_smaller.png" height="46" width="25" class="img_heads" alt="partner"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/pparts"/></span>
			
			<div class="row">	
				<div class="eleven columns offset-by-one">
				<br/>
				<ul>
					<li class="half"><xsl:value-of select="//lang_blocks/pparts1"/></li>
					<li class="half"><xsl:value-of select="//lang_blocks/pparts2"/></li>
					<li class="half"><xsl:value-of select="//lang_blocks/pparts3"/></li>
					<li class="half"><xsl:value-of select="//lang_blocks/pparts4"/><xsl:text> </xsl:text><a href="../compensation/partner.php">http://www.clubshop.com/manual/compensation/partner.php</a></li>
					<!--<li class="half"><xsl:value-of select="//lang_blocks/pparts5"/></li>-->
				</ul>


				</div>
				</div>
				<br/><br/>
				<br/><br/><br/><br/><br/>
				<br/><br/><br/><br/><br/>
				<br/><br/><br/>
				<hr/>
				<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>




</div>
</div>
</div>




<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>