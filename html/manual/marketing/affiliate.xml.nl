<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="affiliate.xsl" ?><lang_blocks><p1>Affiliates Verwijs voordelen</p1><p2>Referral regels</p2><p3>MEMBER TYPE</p3><p4>U KUNT SPONSOREN</p4><p5>Partner</p5><p6>Member</p6><p7>Non-profit organisatie</p7><p9>Ja</p9><p10>Nee</p10><p11>Winkel</p11><p12>Affiliate</p12><p13>Verwijsregels</p13>
<paffs>Affiliates</paffs>
<!--<paffsa>Affiliates betalen een initiële en een jaarlijkse bijdrage om Affiliate te worden en te blijven.</paffsa>-->


<paffsb new="4/23/12">Affiliates are eligible to win a matching prize if a membership they've referred wins a prize in the ClubShop Rewards Giveaway Sweepstakes. </paffsb>

<paffsc new="4/1/12">Referral commissies - Zou een member die zij hebben verwezen een gekwalificeerde aankoop doen, dan verdienen Affiliates een Referral commissie die gelijk staat aan 50% van de Pay punten die worden toegewezen aan de aankoop.</paffsc><paffsd>Kaartverkoop - Winkels kunnen ClubShop Reward kaarten kopen voor "groothandelsprijs" om met winst te verkopen of als stimulans aan hun klanten of cliënten geven. Zij krijgen de referral credit voor elke kaarthouder die diens lidmaatschap activeert via</paffsd>
<paffse>Affiliate Compensatieplan</paffse>

<paffsf new="4/1/12">Affiliates komen niet in aanmerking om te worden beloond voor het verwijzen van andere Affiliates of Partners. Echter, zou een Member die zij hebben verwezen er voor kiezen om een Affiliate of Partner te worden, dan kan de verwijzende Affiliate tot het eind van de maand kwalificeren als Partner om in aanmerking te blijven komen om te worden beloond voor hun doorverwijzing. </paffsf>


<paffsg>Affiliates kunnen een gratis proefpositie krijgen in ons Partner verkooporganisatie</paffsg>
<paffsh>Affiliates kunnen kwalificeren om Partner te worden door te voldoen aan vereiste verkoopquota</paffsh>

</lang_blocks>


