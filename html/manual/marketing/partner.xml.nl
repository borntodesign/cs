<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="partner.xsl" ?><lang_blocks><p1>Partners Verwijsvoordelen</p1><p2>Verwijsregels</p2><p13 new="1/4/12">Verwijsregels</p13><pparts>Partners</pparts>

<pparts1 new="4/23/12">Partners need to create and maintain at least a sales quota of 12 Personal and/or Referral Pay Points to become and remain a Partner </pparts1>

<pparts2 new="1/4/12">Kunnen voldoen aan de vereiste Partner verkoopquota met de aankoop van een "Global Partner System (GPS)" abonnement.</pparts2>
<pparts3 new="1/4/12">Partners worden beloond voor en hebben de mogelijkheid om alle soorten member types te verwijzen, inclusief Affiliates en Partners.</pparts3>

<pparts4 new="1/4/12">Partner Compensatieplan</pparts4>

<pparts5 new="4/23/12">Partners are eligible to win a matching prize if a membership they've referred wins a prize in the ClubShop Rewards Giveaway Sweepstakes.</pparts5>
</lang_blocks>
