<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="merchant.xsl" ?>
<lang_blocks>
<p1>Winkels verwijsvoordelen</p1>
<p2>Verwijsregels</p2>
<p13>Verwijsregels</p13>

<pmerchants>Winkeliers</pmerchants>

<pmerchants1>Winkels- Winkels kunnen offline winkels zijn die zijn overeengekomen om onze ClubShop Rewards kaart te accepteren en een korting te geven aan ClubShop Rewards kaarthouders. Bij de implementatie van onze "ClubShop Rewards Marketplace"-programma, kunnen winkels/ bedrijven ook een online winkel hebben op de lijst van de ClubShop Rewards winkelcentrum.</pmerchants1>

<pmerchants2>Door het accepteren van de ClubShop Rewards kaart of op de lijst te staan als een online MarketPlace winkel/ bedrijf, kunnen winkels/ bedrijven een loyale klantenkring ontwikkelen die resulteert in herhalingsaankopen en hebben zij de mogelijkheid om direct ClubShop Rewards Members te krijgen als nieuwe klanten.</pmerchants2>

<pmerchants3>ClubCash - Winkels en bedrijven verdienen ClubCash op hun persoonlijke aankopen die ze online hebben gedaan via het ClubShop winkelcentrum of offline bij deelnemende, tracking winkels/ bedrijven.</pmerchants3>

<pmerchants4 new="4/23/12">Referral Commissions - Should a Member they refer make a qualifying purchase, Merchants will earn a Referral Commission equal to 50% of the Pay Points credited for the purchase.</pmerchants4>

<pmerchants5>Kaartverkoop - Winkels/ bedrijven kunnen ClubShop Reward kaarten kopen voor "groothandelsprijs" om met winst te verkopen of om als stimulatie weg te geven aan hun klanten of cliënten. Zij krijgen de referral credit voor elke kaarthouder die diens lidmaatschap activeert via</pmerchants5>

<pmerchants6>Winkels komen niet in aanmerking om te worden beloond voor het verwijzen van andere winkels, non-profit organisaties, Affiliates of Partners. Echter, zou een Member die zij hebben verwezen ervoor kiezen om 1 van deze andere lidmaatschappen te nemen, dan heeft de verwijzende winkel 72 uur de tijd om diens lidmaatschap te upgraden naar Affiliate of Partner om in aanmerking te blijven komen om te worden beloond voor hun verwijzing.</pmerchants6>

<pmerchants7>Winkeliers Compensatieplan</pmerchants7>

<pmerchants8 new="4/23/12">Merchants are eligible to win a matching prize if a membership they've referred wins a prize in the ClubShop Rewards Giveaway Sweepstakes. </pmerchants8>
</lang_blocks>