<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="affinity.xsl" ?>
<lang_blocks>
<p1>Non-profit Organisaties Verwijzingsvoordelen</p1>
<p2>Verwijsregels</p2>
<p13>Verwijsregels</p13>

<paffinity>Non-profit Organisatie</paffinity>

<paffinity1>Affinity Groups zijn non-profit organisaties of bedrijven die wensen deel uit te maken van ons ClubShop Rewards Programma om hun non-profit te helpen fondsen te werven of om hun werknemers een extra voordeel te geven. Non-profit organisaties kunnen ClubShop Rewards kaarten inkopen voor groothandelsprijs en deze vervolgens met winst te verkopen.</paffinity1>

<!--<paffinity2 new="4/23/12">Affinity Groups are eligible to win a matching prize (donation) if a membership they've referred wins a prize in the ClubShop Rewards Giveaway Sweepstakes. </paffinity2>-->
<paffinity3>Non-profit organisatie Compensatieplan</paffinity3>

<paffinity4 new="4/23/12">Referral Charitable Donations - Affinity Groups are able to receive a charitable donation from ClubShop Rewards, should a Member they refer make a qualifying purchase, equal to 1/4 of the ClubCash value provided to their referred Member.</paffinity4>
<paffinity5>Kaartverkoop - Non-profit organisaties kunnen ClubShop Reward kaarten voor "groothandelsprijs" kopen en met winst verkopen als een manier om fondsen te werven voor hun organisatie, zij krijgen ook de referral credit voor elke kaarthouder die diens lidmaatschap activeert via</paffinity5>

<paffinity6 new="4/23/12">Charitable Donations From Members - Non-profit Organisaties kunnen ook donaties ontvangen van ClubShop Rewards, wanneer een member ervoor kiest om een donatie te doen van de door hun ontvangen cash rewards of inkomsten.</paffinity6>

<paffinity7>Non-profit Organisaties komen niet in aanmerking om te worden beloond voor het verwijzen van andere Non-profit Organisaties, Winkels, affiliates of partners.</paffinity7>

</lang_blocks>