﻿<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
<head>
<title>GPS - Global Partner System</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/foundation/foundation.css" /> 
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/foundation/foundation2.css" /> 
<!-- <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/foundation/app.css" /> -->
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/foundation/general.css" /> 
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/cs/package_table/styles/main.css"/>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/cs/package_table/styles/style_3.css"/>
<link href="//www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<link href='//fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'> 


<style type="text/css">

.special{
font-family: 'Asul', sans-serif; 
font-size: 16px;}

.special2{
font-family: 'Asul', sans-serif; 
font-size: 14px;}

}

.r34 a {
font-size: 15px;

}
.r34 a:link {
color: #000;
font-weight:bold;
text-decoration: underline;
}
.r34 a:visited {
color: #3366CC;
}
.r34 a:hover {
color: #3366CC;
cursor:help;
font-weight:bold;
height: 15px;
text-decoration: none;
}
.r34 a:active {
color: #000;
text-decoration: none;
}
	
table tr {border-bottom: 1px solid #EFF5E7;}

ul.orgbullet li {
    line-height: 1.3em;
    list-style: disc outside url("http://www.glocalincome.com/images/icons/bullet_triangle_orange.png");
    margin-left: 35px;
    vertical-align: middle;
}
.navy {color: #000099;}
.tip_trigger{cursor:help;}
	
		.tip2 {
			color: #fff;
			background:#00376F;
			display:none; /*--Hides by default--*/
			padding:10px;
			position:absolute; 
			z-index:1000;
			margin-top: 100px;
			right:250px;
		}
		
div.p_table h2
	{
	font-family: 'YanoneKaffeesatzRegular', arial, sans-serif;
	font-size: 25px;
	}
div.p_table a.tooltip:before
	{
	content: attr(rel);
	display: block;
	width: 280px;
    position: absolute;
	z-index: 1000;
    bottom: 150%;
    left: -10px;
    padding: 5px 10px 8px 10px;
	text-align: left;
    color: #ffffff;
    display: none;
	/* background */
	background: #404040; /* CSS2 */
	background: -moz-linear-gradient(top, #4e4e4e 0%, #404040 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4e4e4e), color-stop(100%,#404040)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #4e4e4e 0%,#404040 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #4e4e4e 0%,#404040 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #4e4e4e 0%,#404040 100%); /* IE10+ */
	background: linear-gradient(top, #4e4e4e 0%,#404040 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4e4e4e', endColorstr='#404040',GradientType=0 ); /* IE6-9 */
	/* border-radius */
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	/* box-shadow */
	-webkit-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
	-moz-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.5);
	/* text-shadow */
	text-shadow: 0px 1px 0px #292929;
	}
.whitewash {font-family: Verdana, Arial, sans-serif;
font-size: 13px;
color: #FFFFFF;
padding-left: 4px;
}

div.p_table div.column_3
	{
	width: 120px;
	height: 100%;
	float: left;
	position: relative;
	}
.cap {font-family: 'YanoneKaffeesatzRegular', arial, sans-serif;
	font-size: 15px;
	color: #0065CA;
	text-transform: uppercase;
}
</style>

<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<!-- <script src="http://www.clubshop.com/js/partner/app.js"></script> -->
<!-- <script src="http://www.clubshop.com/js/partner/flash.js"></script> -->
<!-- <script src="http://www.glocalincome.com/js/foundation2/jquery.tooltips.js"></script> -->
<!-- <script src="http://www.clubshop.com/js/panel.js"></script> -->
<script type="text/javascript" src="http://www.clubshop.com/js/transpose.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/Currency.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/cgi/transposeMEvarsJS.cgi"></script> 
<script type="text/javascript" src="http://www.clubshop.com/_includes/js/tip.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 

<script>
		$(document).ready(function() {
			//Tooltips
			$(".tip_trigger").hover(function(){
				tip = $(this).find('.tip2');
				tip.show(); //Show tooltip
			}, function() {
				tip.hide(); //Hide tooltip
			}).mousemove(function(e) {
				var mousex = e.pageX + 20; //Get X coodrinates
				var mousey = e.pageY + 20; //Get Y coordinates
				var tipWidth = tip.width(); //Find width of tooltip
				var tipHeight = tip.height(); //Find height of tooltip

				//Distance of element from the right edge of viewport
				var tipVisX = $(window).width() - (mousex + tipWidth);
				//Distance of element from the bottom of viewport
				var tipVisY = $(window).height() - (mousey + tipHeight);

				if ( tipVisX < 20 ) { //If tooltip exceeds the X coordinate of viewport
					mousex = e.pageX - tipWidth - 20;
				} if ( tipVisY < 20 ) { //If tooltip exceeds the Y coordinate of viewport
					mousey = e.pageY - tipHeight - 800;
				}
				//Absolute position the tooltip according to mouse position
				tip.css({  top: mousey, left: mousex });
			});
		});	
	
	</script>
	
	
	
	
</head>

<body>
<!-- Top Blue Header Container -->
<div class=" container blue ">
<div class=" row ">
<div class=" six columns "><a href="#"><img height=" 84px " src="http://www.clubshop.com/images/partner/cs_logo.jpg" style=" margin:5px; " width=" 288px " /></a></div>
<div class=" six columns "><!--Extra Space Future Space --></div>
</div>
</div>
<!--End Top Blue Header Container -->
<p> </p>
<div align="center"><span style="font-size: x-large;"><span style="color: navy;"><strong>Het Global Partner Systeem</strong></span></span> <br /><br /> <em><span class="bold">Alles wat u nodig hebt om een succesvolle ClubShop Rewards zaak uit te bouwen!</span></em><br/><br/>


<p><span class="ContactForm_TextField"><strong><a href="http://www.clubshop.com/fees/full.xml" target="_blank">KLIK HIER</a></strong> for GPS pricing and Pay Point information.</span></p>

</div>
<!--
<div align="right"><a href="#" mce_href="#" class="style28" onclick="window.print();return false"><img src="/images/icon_print_page.gif" mce_src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGINA</a></div>
-->   				                     
<hr />
<div class="row">
<div class="twelve columns"><!--tag_1-open-->
<p class="style1 bold">Een wereldwijd Partner Systeem (GPS) abonnement geeft u krachtige hulpmiddelen en online training om het u mogelijk te maken om succes te bereiken als ClubShop Rewards Partner! Er zijn 5 verschillende GPS paketten en mogelijkheden, alle voldoen aan de Pay punten vereiste om kwalificeren als Partner:</p>
<!--tag_1-close--> <!--tag_2-open
<p class="style1 bold"; style="background-color:#eef;padding:0.5em;" mce_style="background-color:#eef;padding:0.5em;">
<span class="style28">Uw GPS huidige abonnement is ingesteld op</span> <span class="ContactForm_TextField bold">"[% xSubscription.label %]"</span>.<br/><br/>
U dient uw GPS abonnemnt te upgraden om toegang te krijgen tot de webpagina die u probeert te bezoeken. Lees alstublieft de informatie en bekijk onderstaande tabel, om vast te stellen naar welk type abonnement u wellicht wilt upgraden.</p>
tag_2-close-->  <!--tag_3-open
<p class="style1 bold"; style="background-color:#eef;padding:0.5em;" mce_style="background-color:#eef;padding:0.5em;">
<span class="style28">Uw GPS huidige abonnement is ingesteld op</span> <span class="ContactForm_TextField bold">"[% xSubscription.label %]"</span>.<br/><br/>
U dient een upgrade uit te voeren van uw GPS abonnement naar PREMIER of PREMIER PLUS om toegang te krijgen tot de webpagina die u probeert te bezoeken. Lees alstublieft de informatie en bekijk onderstaande tabel, om vast te stellen naar welk type abonnemnet u wellicht wilt upgraden.</p>
tag_3-close-->
<div class="row">
<div class="seven columns"><br />
<p><span class="style28 navy">BASIS</span><span class="special"> - Deze optie maakt het mogelijk voor een Affiliate om gemakkelijk te kwalificeren als Partner, met een zeer lage initiele en blijvende investering en het geeft toegang tot de basis Marketing Training Cursussen, Webinars en de ClubShop Rewards Advertising Cooperative - gezamenlijk adverteren (CO-OP).</span></p>
<p><span class="style28 navy">BASIS PLUS</span><span class="special"> - Deze optie heeft dezelfde voordelen als een BASIS abonnement, PLUS het stelt abonnees in staat om elke maand automatisch 5 nieuwe Members te krijgen uit onze co-op om uw persoonlijke advertenties aan te vullen.</span></p>
<p><span class="style28 navy">PRO</span><span class="special"> - Deze optie biedt dezelfde voordelen als een BASIS abonnement en bevat geavanceerde Marketing- en Management Training Cursussen, een groot scala aan waardevolle hulpmiddelen, rapporten en functies om u te helpen een verkoopteam op te zetten en te beheren.</span></p>
<p><span class="style28 navy">PRO PLUS</span><span class="special"> - Deze optie heeft dezelfde voordelen als een PRO abonnement, PLUS het stelt abonnees is staat om elke maand automatisch 10 nieuwe Members te krijgen uit onze co-op om uw persoonlijke advertenties aan te vullen.</span></p>
<p><span class="style28 navy">PREMIER</span><span class="special"> - Deze optie biedt dezelfde voordelen als die van het PRO pakket, samen met twee extra voordelen - een Auto Email programma en gebruik van een online Meeting Room (vergaderruimte.</span></p>
<p><span class="style28 navy">PREMIER PLUS</span><span class="special"> - Dit is ons "Auto Pilot" type optie welke u alle voordelen van het PREMIER pakket biedt, PLUS het stelt abonnees in staat om automatisch een totaal van 20 nieuwe Members per maand te krijgen van onze co-op.</span></p>
</div>
<div class="five columns"><br /><br /><br />
<div align="center"><img alt="gps" height="458" src="http://www.clubshop.com/manual/gps/gps-box-version5.png" width="378" /></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<p><span class="ContactForm_TextField"><strong><a href="http://www.clubshop.com/fees/full.xml" target="_blank">KLIK HIER</a></strong> for GPS pricing and Pay Point information.</span></p>
<hr />
</div>
</div>
<!--
<div class="row">
<div class="six columns">
<div class="six columns centered"><a href="#value" mce_href="#value" class="nice medium green button">VOORDELEN &amp; WAARDE 	&#187;</a></div>
<br /><br /></div>
<div class="six columns">
<div class="six columns centered"><a href="#reseller" mce_href="#reseller" class="nice medium green button" >RESELLER VOORDELEN &#187;</a></div>
<br /><br /></div>
</div>
-->
<div class="row">
<div class="four columns centered"><a class="nice large blue button White_Large_Bold" href="https://www.clubshop.com/cgi/vip/gps_modification.cgi">Wijzig abonnement 	»</a></div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
<br /><br /> <!-- pricing table -->
<div align="center">
<div class="p_table">
<div class="frame_border radius5">
<div align="center">The Initial Subscription amount pays for your use of the GPS BASIC Option you select, for the current month and for next month.<br/> De maandelijkse bijdrage hoeft niet eerder dan de volgende maand te worden voldaan, tenzij u kwalificeert voor de Minimum Commissie Garantie, in dit geval wordt uw maandelijkse abonnementsbijdrage automatisch van uw verdiensten afgetrokken. <br /><br /></div>
<!-- caption column -->
<div class="caption_column">
<ul>
<!-- column header -->
<li class="header_row_1 align_center"><span class="caption cap">Abonnementen</span></li>
<!--<li class="decor_line"></li>
<li class="header_row_2 align_center"><span class="caption cap">Initieel</span></li>
<li class="decor_line"></li>
<li class="header_row_2 align_center"><span class="caption cap">Prijzen</span></li>-->
<!-- data rows -->
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="Dit geeft u een enorme e-learning hulpbron waar u als GPS abonnee gratis toegang tot hebt en kunt gebruiken.">Training 
                Cursussen</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Met alle GPS paketten kwalificeert u als Partner.">Kwalificeert 
                een Affiliate als Partner</a></span></li>
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="Meertalige Training Webinars. Training Webinars worden gegeven door succesvolle Partners en bieden u live, interactieve training en tips om uw ClubShop Rewards zaak uit te bouwen.">Toegang 
                tot Training Webinars</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Maakt gebruik van 'Groepskoopkracht' om gericht advertenties te plaatsen, die gemaakt zijn door professionele marketeers en die leads omzetten in daadwerkelijke member accounts die u kunt kopen om uw klantenlijst en verkoopteam op te bouwen.">Toegang 
                tot Member Leads Programma</a></span></li>
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="Maakt gebruik van klanten GPS software en wordt gehost op de GPS server. Dit rapport is uitsluitend beschikbaar voor GPS abonnees. Het laat verschillende 'activiteiten' zien die hebben plaatsgevonden binnen uw team, tot aan een Executive Director.">Activiteiten 
                Rapport</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Laat de contactinformatie zien, net als de activiteiten- en opvolginformatie voor elk member type in uw team.">Opvolgsysteem</a></span></li>
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="SEO webpagina creator met vertaler, webpagina hosting, klanten prospect capture/landing webpagina's en zakelijke prospect capture/landing webpagina's">Marketing 
                Webpagina Creator</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Onze lijst generator is een flexibele lijst management programma dat u in staat stelt een op maat gemaakte lijst op te stellen ">E-mail 
                Lijst Generator</a></span></li>
              
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="De mogelijkheid om uw persoonlijk verwezen memers te verplaatsen">Transfer 
                Members Functie</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Professionele Banner Advertenties met uw ID nummer verwerkt in de link, welke u kunt gebruiken in uw advertenties, zodat u de credit krijgt voor de lidmaatschappen die u hebt ingeschreven via de advertenties.">Advertentie 
                Banners Generator</a></span></li>
				<li class="row_style_3"><span><a class="tooltip" href="#" rel="To qualify as a Coach, you need to have a PRO or PREMIER subscription.">Enables You To Be A Coach</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Basis Plus: 5 Members per maand Pro Plus: 10 Members per maand Premier Plus: 20 Members per maand ">Member 
                Leads Aangeboden</a></span></li>
              <li class="row_style_3"><span><a class="tooltip" href="#" rel="Maak en verstuur gepersonaliseerde e-mail door gebruik te maken van een menu van gepersonaliseerde 'merge fields' en kies uit een lijst van lidmaatschap types de mailing lijsten aan wie u automatisch deze e-mails wilt versturen vanaf de GPS mail servers!">Auto 
                e-mailer</a></span></li>
              <li class="row_style_1"><span><a class="tooltip" href="#" rel="Stelt een abonnee in staat om gebruik te maken van een online meeting room (vergaderruimte), met een capaciteit van 200 aanwezigen, om team bijeenkomsten en webinars te houden.">Toegang 
                Online Meeting Room - vergaderruimte </a></span></li>
<!-- column footer --> <!--
<li class="footer_row"></li>
--> 
</ul>
</div>
<!-- column style 2 -->
          <div class="column_3"> 
            <!-- ribbon (optional) -->
            <!--
<div class="column_ribbon ribbon_style1_new"></div>
-->
            <!-- /ribbon -->
            <ul>
              <!-- column header -->
              <!--
<li class="header_row_1 align_center">
<h2><! #include virtual="../translate/cs_loyalty/p17.shtml"</h2>
</li>
-->
              <li class="header_row_1 align_center radius5_topleft"> 
                <h2>Basis</h2>
              </li>
             <!--<li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">EU - €59.00</span><br />
                <br />
                <span class="whitewash">USA/CA - $79.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>
              <li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">EU - €25.00</span><br />
                <br />
                <span class="whitewash">USA/CA - $29.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>-->
              <!-- data rows -->
              <li class="row_style_2 align_center"><span>Basis Marketing Training</span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <!-- column footer -->
            </ul>
          </div>
<div class="column_1"><!-- ribbon (optional) --> <!--
<div class="column_ribbon ribbon_style1_new"></div>
--> 				<!-- /ribbon --> 
<ul>
<!-- column header --> <!--
<li class="header_row_1 align_center">
<h2><! #include virtual="../translate/cs_loyalty/p17.shtml"</h2>
</li>
-->
<li class="header_row_1 align_center">
<h2>Basic Plus</h2>
</li>
<!--<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €89.00</span><br /><br /><span class="whitewash">USA/CA-  $109.00</span><br /><br /><a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>
<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €41.00</span><br /><br /><span class="whitewash">USA/CA-  $44.00</span><br /><br /><a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>-->
<!-- data rows -->
<li class="row_style_2 align_center"><span>Basis Marketing Training</span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="no" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="no" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<!-- column footer --> <!--
<li class="footer_row"><a href="https://www.clubshop.com/cs/short-registration.shtml" mce_href="https://www.clubshop.com/cs/short-registration.shtml" class="button_3 radius5" target="_blank">sign up</a></li>
--> 
</ul>
</div>
<!-- column style 1 -->
          <div class="column_3"> 
            <!-- ribbon (optional) -->
            <!--
<div class="column_ribbon ribbon_style1_new"></div>
-->
            <!-- /ribbon -->
            <ul>
              <!-- column header -->
              <!--
<li class="header_row_1 align_center">
<h2><! #include virtual="../translate/cs_loyalty/p20.shtml"</h2>
</li>
-->
              <li class="header_row_1 align_center"> 
                <h2>PRO</h2>
              </li>
              <!--<li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">Europe - €79.00</span><br />
                <br />
                <span class="whitewash">USA/CA- $99.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>
              <li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">Europe - €35.00</span><br />
                <br />
                <span class="whitewash">USA/CA - $39.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>-->
              <!-- data rows -->
              <li class="row_style_2 align_center"><span>Management Training & 
                Winkel Marketing Training</span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="no" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <!-- column footer -->
            </ul>
          </div>
<!-- column style 3 -->
<div class="column_1"><!-- ribbon (optional) --> <!--
<div class="column_ribbon ribbon_style1_new"></div>
--> 				<!-- /ribbon --> 
<ul>
<!-- column header --> <!--
<li class="header_row_1 align_center">
<h2><! #include virtual="../translate/cs_loyalty/p24.shtml"</h2>
</li>
-->
<li class="header_row_1 align_center">
<h2>PRO Plus</h2>
</li>
<!--<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €140.00</span><br /><br /><span class="whitewash">USA/CA- $159.00</span><br /><br /> <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>
<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €66.00</span><br /><br /><span class="whitewash">USA/CA- $69.00</span><br /><br /><a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>-->
<!-- data rows -->
<li class="row_style_2 align_center"><span>Management Training & Winkel Marketing Training</span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
<!-- column footer --> 
</ul>
</div>
<!-- column style 1 -->
          <div class="column_3"> 
            <!-- ribbon (optional) -->
            <!--
<div class="column_ribbon ribbon_style1_new"></div>
-->
            <!-- /ribbon -->
            <ul>
              <!-- column header -->
              <!--
<li class="header_row_1 align_center">
<h2><! #include virtual="../translate/cs_loyalty/p28.shtml"</h2>
</li>
-->
              <li class="header_row_1 align_center"> 
                <h2>Premier</h2>
              </li>
              <!--<li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">Europe - €114.00</span><br />
                <br />
                <span class="whitewash">USA/CA- $149.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>
              <li class="decor_line"></li>
              <li class="header_row_2"><span class="whitewash">Europe - €53.00</span><br />
                <br />
                <span class="whitewash">USA/CA- $69.00</span><br />
                <br />
                <a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Anders</span></span></a></li>-->
              <!-- data rows -->
              <li class="row_style_2 align_center"><span>Management Training & 
                Winkel Marketing Training</span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="no" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="no" height="24" src="http://www.clubshop.com/cs/package_table/images/no.png" width="24" /></span></li>
              <li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
              <!-- column footer -->
              <!-- column footer -->
            </ul>
          </div>
<!-- column style 3 -->
<div class="column_1"><!-- ribbon (optional) -->
<div class="column_ribbon ribbon_style1_best"></div>
<!-- /ribbon --> 
<ul>
<li class="header_row_1 align_center radius5_topright">
<h2>Premier Plus</h2>
</li>
<!--<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €235.00</span><br /><br /><span class="whitewash">USA/CA-  $269.00</span><br /><br /><a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Other</span></span></a></li>
<li class="decor_line"></li>
<li class="header_row_2"><span class="whitewash">Europe -  €114.00</span><br /><br /><span class="whitewash">USA/CA-  $129.00</span><br /><br /><a class="whitewash" href="http://www.clubshop.com/fees/full.xml" target="_blank"><span class="whitewash"><span style="text-decoration: underline;">Other</span></span></a></li>-->
<!-- data rows -->
<li class="row_style_2 align_center"><span>Management Training &amp; Winkel Marketing Training</span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_2 align_center"><span><img alt="yes" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<li class="row_style_1 align_center"><span><img alt="No" height="24" src="http://www.clubshop.com/cs/package_table/images/yes.png" width="24" /></span></li>
<!-- column footer --> 
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns "><br /><br />
<p align="center"><span class="bold text_upper">Tel alles bij elkaar op en u kunt zien waarom duizenden Affiliates ervoor hebben gekozen om <br />zich te abonneren op GPS en ClubShop Rewards Partners zijn geworden! </span></p>
<br /><br />
<p align="center"><a class="nice large blue button White_Large_Bold" href="https://www.clubshop.com/cgi/vip/gps_modification.cgi ">Wijzig abonnement 	»</a></p>
<br /> <br /><br /><br /></div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div c<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" eight columns centered">          
                <div id=" footer ">
                    <p>Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();

                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->
</body>
</html>
