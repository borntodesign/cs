﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
            <title>Direct Deposit Request</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Verzoek tot Direct Deposit (ENKEL US)</span>
	<div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a></div>
<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


                            <p>Voor ClubShop Rewards kan overgaan met de aanvang, aanpassing of stopzetting van 
                              de Direct Deposit van uw Referral fees op uw bankrekening, 
                              moet onderstaand formulier in ons bezit zijn. De 
                              vermoedelijke datum van wanneer uw Direct Deposit 
                              in effect gaat, wordt u door onze boekhoudkundige 
                              afdeling via e-mail gemeld.</p>
<hr align="left" size="1" width="100%" />
                            <p>Verzoek tot Direct Deposit, update of stopzetten van de Direct Deposit van 
                              mijn verdiende ClubShop Rewards commissies:</p>
                            <p>Ik, (naam)______________________________________________, ID#________________________, 
                              geef hierbij toestemming aan ClubShop Rewards om:</p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#cccccc" colspan="2"><strong>Eén selecteren:</strong></td>
</tr>
<tr>
<td bgcolor="#ffffff">____________</td>
<td bgcolor="#ffffff">Om mijn Referral fees over te maken op volgende bankrekening.</td>
</tr>
<tr>
<td bgcolor="#ffffff">____________</td>
<td bgcolor="#ffffff">Om mijn bankgegevens te updaten.</td>
</tr>
<tr>
<td bgcolor="#ffffff">____________</td>
<td bgcolor="#ffffff">Om mijn Referral fees niet langer over te maken naar mijn bankrekening. Ik kies ervoor mijn Referral fees per cheque te ontvangen.</td>
</tr>
</tbody>
</table>
<p>Nota: Direct Deposits kunnen <em>enkel</em> geschieden tussen USA gebaseerde bank/spaarrekeningen en enkel op rekeningen die geregistreerd zijn op naam van het lid:</p>
<p align="center" class="style3">Print dit formulier. <span style="text-decoration: underline;">We moeten in het bezit zijn van</span> een kopie van of een nietige cheque, of een uittreksel van de rekening om uw verzoek te kunnen verwerken.</p>
<div align="center">
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="70%">
<tbody>
<tr>
<td bgcolor="#ffffff" height="100">
<div align="center"><strong>(plaats cheque hier)</strong></div>
</td>
</tr>
</tbody>
</table>
</div>
<p class="text_brown">Straatadres van bank:___________________________________________________________________</p>
<p class="text_brown">Stad, staat, Zip Code van bank: _____________________________________________________________</p>
<p class="text_brown">Gelieve te selecteren: Betaalrekening_____________ Spaarrekening_______________</p>
<p class="text_brown">Handtekening VIP:__________________________________________________Datum:________________</p>
<p class="text_brown">E-mail adres VIP:_________________________________________________________________________</p>
<br />
                            <p>Formulier versturen naar: Clubshop Rewards, 955 Morisson Ave, Englewood, 
                    FL 34223.</p>
<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright © 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>