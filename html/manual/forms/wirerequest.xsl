<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><xsl:value-of select="//lang_blocks/p1"/></title>

               <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  
</div>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">




   

<!-- start page here-->


<p><xsl:value-of select="//lang_blocks/p4"/></p>

<p><span class="style3"><xsl:value-of select="//lang_blocks/p5"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p6"/></p>
<p class="sitemap"><xsl:value-of select="//lang_blocks/p7"/></p>

		<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr><td colspan="2" bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p8"/></td></tr>
<tr><td colspan="2" bgcolor="#EAEAEA"><b><xsl:value-of select="//lang_blocks/check"/></b></td></tr>
<tr><td bgcolor="#FFFFFF">_____</td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p9"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF">_____</td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p10"/></td>
</tr>
<tr><td bgcolor="#FFFFFF" colspan="2" class="sitemap"><xsl:value-of select="//lang_blocks/p12"/></td></tr>
<tr><td bgcolor="#EAEAEA" colspan="2"><b><xsl:value-of select="//lang_blocks/bankinfo"/></b></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p13"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p14"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p15"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p16"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p17"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2"><xsl:value-of select="//lang_blocks/p18"/></td></tr>
<tr><td bgcolor="#FFFFFF" colspan="2">
<xsl:value-of select="//lang_blocks/p19"/><br/><br/>
<xsl:value-of select="//lang_blocks/p20"/><br/><br/>
<xsl:value-of select="//lang_blocks/p21"/>
</td></tr>
		</table>

<!-- end page text here-->


<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>