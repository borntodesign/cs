<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Transfer lidmaatschap onder bedrijfsnaam</span> 
     <div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  

<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<p><span style="font-family: Arial;">Brief ter bevestiging dat het lidmaatschap onder een bedrijfsnaam geplaatst wordt. Dit document dient te worden <a href="http://www.clubshop.com/manual/business/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()">gewettigd</a>.</span></p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff">
<p>ClubShop Rewards <br>
                              Attention: Membership Department <br>
                              2828 So. McCall Rd., Suite 13<br>
                              Englewood, FL 34224 </p>
							  <p><span style="font-family: Arial, Helvetica, sans-serif;"><span style="font-family: Arial;">Ter attentie van: Afdeling lidmaatschap</span></span></p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" colspan="2">_____________ Huidig leden ID nummer</td>
</tr>
<tr>
<td bgcolor="#ffffff"><br />Naam van lid</td>
<td bgcolor="#ffffff"><br />Naam voorgedragen bedrijf</td>
</tr>
<tr>
<td bgcolor="#ffffff"><br />Adres van lid</td>
<td bgcolor="#ffffff"><br />Adres voorgedragen bedrijf</td>
</tr>
<tr>
<td bgcolor="#ffffff"><br /></td>
<td bgcolor="#ffffff"><br />FEIN van bedrijf of persoonlijk Social Security nummer <span style="font-size: xx-small;">(enkel USA)</span></td>
</tr>
</tbody>
</table>
<p>Ik, (naam eigenaar/beheerder) __________________________________, ben eigenaar/beheerder                                      van bovengenoemd bedrijf en verklaar hierbij                                      dat ik heb gelezen en akkoord ga mij te zullen                                      houden aan de <a href="http://www.clubshop.com/terms.html" onclick="window.open(this.href,'help', 'width=700,height=450,scrollbars,menubar=yes,toolbar=yes,resizable=yes'); return false;" onfocus="this.blur()">Algemene                                      Voorwaarden</a> van dit ClubShop Rewards lidmaatschap                                      en ga akkoord mijn lidmaatschapsgelden te                                      voldoen zoals uitgelijnd in deze algemene                                      voorwaarden. Mijn handtekening hieronder indiceert                                      het accepteren van deze voorwaarden.</p>
<p>De betalingsmethode die ik verkies bij dit lidmaatschap is: (selecteer) ___ kredietkaart ___echeck</p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff"><span style="font-family: Arial, Helvetica, sans-serif;">
<p><span style="font-family: Arial;">Type kredietkaart: _____________ kk #:______________________ naam op kk:___________________</span></p>
<p><span style="font-family: Arial;">exp. datum_____________ CVV Code:___________(<a href="http://www.clubshop.com/help/cvv2.html" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;" onfocus="this.blur()">help</a>)</span></p>
</span></td>
</tr>
</tbody>
</table>
<br /><br /> 
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff">
<p>Naam van bank:_________________________________</p>
<p>Adres:_______________________________________________________</p>
<p>Acceptgirokaart #:______________________________</p>
<p>Bankrekening #:_________________________<a href="http://www.clubshop.com/check.html" onclick="window.open(this.href,'check', ',','width=600,height=400,scrollbars'); return false;" onfocus="this.blur()">(help)</a> Routing nummer: ______________</p>
</td>
</tr>
</tbody>
</table>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" valign="bottom">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>___________________________________<br />Handtekening bedrijfseigenaar/beheerder</p>
</td>
<td bgcolor="#ffffff">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>_____________________________________ <br />Datum</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p align="center"><span style="font-family: Arial;">NOTA: Dit document dient te worden<a href="/manual/business/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()"> gewettigd</a> door een notaris (of gelijkgestelde bevoegdheid buiten de USA).</span></p>
</div>
</td>
</tr>
</tbody>
</table>
<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>