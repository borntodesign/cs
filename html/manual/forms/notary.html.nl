<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Notary Public Information</title>
   <link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
</head>
<body bgcolor="#FFFFFF">
<p class="text_brown"><strong>Wettigen/legaliseren:</strong> Legaliseren of wettigen van een handtekening houdt in dat de gemeente of notaris verklaart dat de handtekening op een document ook werkelijk gezet is door de persoon die het document moest ondertekenen.</p>
<p class="Header_Main">Notaris en dienst burgerzaken</p>
<p>Notarissen en ambtenaren op de dienst burgerzaken zijn gemachtigd om een handtekening te wettigen of legaliseren. Het wettigen van een handtekening betekent dat een daartoe bevoegde overheid de echtheid van een handtekening schriftelijk bevestigt.</p>
<p>Volgens de Amerikaanse wetgeving wordt de notaris vereist om het te legaliseren document te voorzien van datum, handtekening en zijn zegel of stempel. Een bijkomende vereiste is dat de stempel duidelijk laat zien wie, waar aangesteld is en dat de stempel onder zijn handtekening geplaatst wordt. Dit wordt wettelijk vereist om de bekwaamheid van de notaris te verzekeren en, indien nodig, zijn identiteit te bevestigen.</p>
<p><a href="javascript:window.close();"><img alt="Close Window" border="0" height="16" src="../../images/icon_close_window.gif" width="88" /></a></p>
</body>
</html>