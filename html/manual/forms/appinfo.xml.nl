<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="appinfo.xsl" ?><lang_blocks>
  <p1>Global Partner Systeem (GPS) Lidmaatschap Informatie</p1>
  <p10>Op het volgende scherm wordt u gevraagd voor welk lidmaatschap u wilt betalen (Basis, Pro, Pro Plus, Premier, Premier Plus). Opmerking: Jaarlijkse bijdragen zullen de eerste van de maand worden geïnd en voordat de maandelijkse bijdrage kan worden betaald.</p10>
  <p11>Vervolgens wordt een e-mail verstuurd naar de Affiliate die wil upgraden om diens Partner lidmaatschap te bevestigen.</p11>
  <p12>Mocht een fout sluipen in de aankoop van de voucher zoals het selecteren van een incorrecte munteenheid of mocht een voucher niet opgehaald worden, dan kunt u de voucher transactie wissen, wat resulteert in de terugstorting van de fondsen op uw account. Wanneer u &quot;Voucher Transacties wissen&quot; selecteert, krijgt u alle vouchers te zien die niet werden opgehaald en kunt u selecteren welke gewist moet(en) worden.</p12>
  <p13>Aanmeldingbeleid</p13>
  <p14>Lijst internationale munteenheden</p14>
  <p15>STAP 1:</p15>
  <p16>STAP 2:</p16>
  <p17>STAP 3:</p17>
  <p18>Voucher transacties wissen</p18>
  <p2>U betaalt zelf uw initiële bijdrage</p2>
  <p3>- Als u zelf uw initiële bijdrage wilt voldoen, gelieve dan de instructies te volgen op</p3>
  <p4>Als iemand anders uw initiële bijdrage voldoet</p4>
  <p5>- U dient een autorisatie codenummer in te vullen op uw aanmeldingsformulier (dit wordt verstuurd via e-mail door de persoon die uw upgrade voucher aanschaft). Na ontvangst kunt u het autorisatienummer invullen en indienen op</p5>
  <p6>Als u betaalt voor de initiële bijdrage van iemand anders</p6>
  <p7>- Er dienen drie stappen gevolgd te worden wanneer u de initiële bijdrage betaalt voor een ander:</p7>
  <p8>Wanneer u uw ClubAccount gebruikt, moet deze over voldoende fondsen beschikken of u kunt deze aanvullen op</p8>
  <p9>Zodra er voldoende geld op uw ClubAcount staat, dient u het lidmaatschap te kiezen waarvoor u wenst te betalen (nieuw of maandelijks/ jaarlijks). Vul vervolgens het ID nummer in van de potentiële Partner.</p9>
</lang_blocks>
