<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>




<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  
</div>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">





<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text> <a href="/manual/policies/advcoop.xml" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></p>

<form action="/cgi/FormMail.cgi" method="post">
<INPUT name="recipient" type="hidden" value="l.young@dhs-club.com"/>

<INPUT name="email" type="hidden" value="apache@clubshop.com"/> 
<INPUT name="required" type="hidden" value="coord-name,coord-id,coord-email,nameofstore, urlofstore,merchantfee,iagree"/> 
<INPUT name="sort" type="hidden" value="order:coord-name,coord-id,coord-email,nameofstore, urlofstore,merchantfee,iagree"/> 
<INPUT name="redirect" type="hidden" value="http://www.clubshop.com/manual/forms/adv_form_tyou.xml"/> 
<INPUT name="subject" type="hidden" value="Media Advertising Cooperative Update"/> 
<INPUT name="sort" type="hidden" value=""/> 

<INPUT name="env_report" type="hidden" value="HTTP_USER_AGENT"/>

<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p5"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-name" size="25"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p6"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-id" size="25"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p7"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="coord-email" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p8"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="nameofstore" size="25"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p9"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="urlofstore" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><span class="bold"><xsl:value-of select="//lang_blocks/p10"/></span></td>
<td bgcolor="#FFFFFF"><input type="text" name="merchantfee" size="25"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF" colspan="2"><input type="checkbox" name="iagree" size="25" value="YES, By submitting this form, I acknowledge that I have read and agree to the Terms and Policies shown at: http://www.clubshop.com/manual/policies/advcoop.xml"/><xsl:value-of select="//lang_blocks/p17"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/policies/advcoop.xml" target="_blank">http://www.clubshop.com/manual/policies/advcoop.xml</a></td>
</tr>


<tr>
<td bgcolor="#FFFFFF" colspan="2" align="center"> 
<input type="submit" name="Submit" value="Submit"/>
<xsl:text> </xsl:text><input type="reset" name="Reset" value="Reset"/>
</td>
</tr>


		</table>

</form>

<p><xsl:value-of select="//lang_blocks/p14"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/> </p>

<p><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="mailto:support1@clubshop.com?Subject=Media Advertisment Approval">support1@clubshop.com</a></p>


<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>