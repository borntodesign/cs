<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Global Partner System (GPS) - Factuur Maandelijkse Bijdrage</span> 
     <div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  

<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
 <br /><br /> 
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" class="text_brown" width="25%"><strong>DATUM:</strong></td>
<td bgcolor="#ffffff" class="text_brown" width="33%"><strong>ID NUMMER:</strong></td>
<td bgcolor="#ffffff" class="text_brown" width="42%"><strong>NAAM:</strong></td>
</tr>
</tbody>
</table>
<p>Aan alle betrokkenen:</p>
<p>De hierboven vermelde persoon heeft een abonnement gekocht op het Global Partner System (GPS). Daar wij een bedrijf zijn dat gevestigd is in de Verenigde Staten, worden de abonnementsgelden van leden die gehuisvest zijn buiten de USA enkel geaccepteerd in de vorm van bankcheque of overschrijving, in US Dollar en te innen bij een USA Bank. Gelieve dit schrijven te willen accepteren als factuur ten bedrage van __________ voor de maandelijkse Glocal Income bijdrage.</p>
<p>Dank u, <br />Richard Burke <br />President-Directeur<br />ClubShop Rewards</p>
<p><a href="../../manual/forms/invoice.html">Klik hier voor de initiële bijdrage factuur.</a></p>
<p><a href="../../manual/forms/invoice_renewal.html">Klik hier voor de jaarlijkse vernieuwingsbijdrage factuur.</a></p>
<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright © 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>