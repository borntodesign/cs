<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Echtgeno(o)t(e) of partner toevoegen</span> 
     <div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  

<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<p>Brief met het verzoek om de wettelijke echtgeno(o)t(e) of partner toe te voegen aan het Partner lidmaatschap:</p>
<p>Verstuur naar: ClubShop Rewards <br /> Ter attentie van: Afdeling lidmaatschap <br />955 Morisson Avenue<br />Englewood, FL 34223</p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" class="style24" width="50%">
<div align="center">PARTNER INFORMATIE</div>
</td>
<td bgcolor="#faf9f8" class="style24" width="50%">
<div align="center">ECHTGENO(O)T(E)/PARTNER INFORMATIE</div>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold">ID nummer Partner:</td>
<td bgcolor="#faf9f8">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold">Naam Partner:</td>
<td bgcolor="#faf9f8" class="medblue bold">Naam echtgeno(o)t(e)/partner:</td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold"><br />Adres Partner:</td>
<td bgcolor="#faf9f8" class="medblue bold">Adres echtgeno(o)t(e)/partner:</td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold"><br />Social Security Number Partner (ENKEL USA):</td>
<td bgcolor="#faf9f8" class="medblue bold">Social Security Number echtgeno(o)t(e)/partner (ENKEL USA):</td>
</tr>
<tr>
<td bgcolor="#ffffff">
<p><em>Ik, (naam Partner) __________________________________ verzoek hierbij om 
                mijn wettelijke echtgeno(o)t(e) of partner, (naam wettelijke echtgeno(o)t(e) 
                of partner) _____________________________ toe te voegen aan mijn 
                ClubShop Rewards Partner lidmaatschap.</em></p>
</td>
<td bgcolor="#faf9f8"><em>Ik, (naam wettelijke echtgeno(o)t(e) of partner)____________________________,                                    heb gelezen en ga akkoord met de <a href="http://www.clubshop.com/terms.xml" onclick="window.open(this.href,'help', 'width=700,height=450,scrollbars,menubar=yes,toolbar=yes,resizable=yes'); return false;" onfocus="this.blur()">Algemene                                    Voorwaarden</a> van het ClubShop Rewards en bevestig hieronder met mijn handtekening                                    dat ik akkoord ga mij te zullen houden aan deze                                    voorwaarden en dit partnerschap met mijn echtgeno(o)t(e)/partner                                    accepteer.</em></td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold">Handtekening Partner:</td>
<td bgcolor="#faf9f8" class="medblue bold">Handtekening echtgeno(o)t(e)/partner:</td>
</tr>
<tr>
<td bgcolor="#ffffff" class="medblue bold">Datum:</td>
<td bgcolor="#faf9f8" class="medblue bold">Datum:</td>
</tr>
</tbody>
</table>

<p align="center">NOTA: Dit document dient te worden gewettigd door een <a href="http://www.clubshop.com/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()">notaris</a> (of de bevoegde instantie buiten USA).</p>
 <br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>