<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Lettre de transfert d’adhésion d’une société</span> 
     <div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  

<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns"> 
      <p>Exemple de lettre de la société envoyé au ClubShop Rewards pour demander 
        le placement d'une adhésion au nom d'une personne. Ce document doit être 
        <a href="http://www.clubshop.com/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()">notarié 
        </a>.</p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" colspan="2" valign="top">
<p>ClubShop Rewards<br /> 2828 S. McCall Road<br>Suite 13<br>
                Englewood, FL 34224</p>
<p>Attention: Département des adhésions</p>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" colspan="2"><br /> ________________________<br /> Numéro d'ID de la société membre</td>
</tr>
<tr>
<td bgcolor="#ffffff">________________________________________<br /> Nom de la société membre</td>
<td bgcolor="#ffffff">________________________________________<br /> Nom de la personne proposée</td>
</tr>
<tr>
<td bgcolor="#ffffff">________________________________________<br /> Adresse de la société</td>
<td bgcolor="#ffffff">________________________________________                                      <br /> Adresse de la personne proposée</td>
</tr>
<tr>
<td bgcolor="#ffffff"></td>
<td bgcolor="#ffffff">________________________________________                                      <br /> Numéro de sécurité social de la personne proposée <span style="font-size: xx-small;">(USA seulement)</span></td>
</tr>
<tr>
            <td bgcolor="#ffffff" colspan="2"> <p>Je, (Nom du propriétaire/agent) 
                __________________________________, suis un agent/propriétaire 
                de la société mentionnée ci-dessus et par la présente demande 
                de transfert de numéro de ClubShop Rewards membre ID# ________________ 
                à la personne mentionnée ci-dessus. Je comprends que, en mettant 
                cette adhésion au nom de cette personne, je perds par la présente 
                d’adhésion de la société, ainsi que potentielles ventes pyramidales 
                ou celles résultant de cette adhésion, et des éventuels réductions 
                ou les revenus dérivés ou qui devaient en découler.</p>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" valign="bottom">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>___________________________________<br /> Signature du propriétaire/agent de la société</p>
</td>
<td bgcolor="#ffffff">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>_____________________________________ <br /> Date</p>
</td>
</tr>
<p><span style="font-family: Arial;">PS: Ce document doit être<a href="http://www.clubshop.com/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()"> authentifié</a> par un notaire public (ou son équivalent pour les pays hors des USA).</span></p>
</tbody>
</table>




<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>