<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>


               <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<div align="right"><a href="#" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  
</div>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



<ol>
<li><span class="style24"><xsl:value-of select="//lang_blocks/p2"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/cgi/appx.cgi//upg" target="_blank">http://www.clubshop.com/cgi/appx.cgi//upg</a></li>
<br/>
<li><span class="style24"><xsl:value-of select="//lang_blocks/p4"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/cgi/appx.cgi//upg" target="_blank">http://www.clubshop.com/cgi/appx.cgi//upg</a></li>
<br/>
<li><span class="style24"><xsl:value-of select="//lang_blocks/p6"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/></li>

		<p><span class="style3"><xsl:value-of select="//lang_blocks/p15"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">https://www.clubshop.com/cgi/funding.cgi?action=report</a></p>
		<p><span class="style3"><xsl:value-of select="//lang_blocks/p16"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/pay4_others.cgi" target="_blank">https://www.clubshop.com/cgi/pay4_others.cgi</a></p>
        <p><span class="style3"><xsl:value-of select="//lang_blocks/p17"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11"/></p>
    
</ol>




<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>