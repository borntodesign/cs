<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><xsl:value-of select="//lang_blocks/p1"/></title>

               <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  
</div>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<br/><br/>

<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td  bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p2"/>_______________________<br/></td>
<td  bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p3"/>_______________________<br/></td>
</tr>
<tr>
<td  colspan="2" bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p4"/>_______________________<br/><xsl:value-of select="//lang_blocks/p5"/></td>
</tr>
<tr>
<td  bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p8"/>_______________________<br/><xsl:value-of select="//lang_blocks/p9"/></td>
<td bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p10"/>_______________________</td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p11"/> <xsl:text> </xsl:text> <a href="/manual/policies/abusefraud.xml" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a>
<xsl:value-of select="//lang_blocks/p13"/></p>



<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td  bgcolor="#FFFFFF"><br/></td>
<td  bgcolor="#FFFFFF"><br/></td>
</tr>
<tr>
<td  bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p14"/></td>
<td  bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p15"/></td>
</tr>
</table>


<br/>


<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p16"/></td>
<td bgcolor="#FFFFFF" class="text_brown"><xsl:value-of select="//lang_blocks/p17"/></td>
</tr></table>




<br/><br/><br/><br/><br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>