<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28">Transfer lidmaatschap onder persoonlijke naam</span> 
     <div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  

<hr/>
	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<p>Brief van bedrijf naar ClubShop Rewards met het verzoek                                om het lidmaatschap onder persoonlijke naam te plaatsen.                                Dit document dient te worden <a href="http://www.clubshop.com/manual/forms/notary.html" onclick="window.open(this.href,'help', 'width=510,height=350,scrollbars'); return false;" onfocus="this.blur()">gewettigd</a>.</p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" colspan="2" valign="top">
<p>ClubShop Rewards<br /> 2828 S. McCall Road<br/>Suite 13 <br /> Englewood, FL 34224</p>
<p>Ter attentie van: Afdeling lidmaatschap</p>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" colspan="2"><br /> ________________________<br /> ID nummer van bedrijf</td>
</tr>
<tr>
<td bgcolor="#ffffff">________________________________________<br /> Naam van bedrijf</td>
<td bgcolor="#ffffff">________________________________________<br /> Naam voorgedragen persoon</td>
</tr>
<tr>
<td bgcolor="#ffffff">________________________________________<br /> Adres van bedrijf</td>
<td bgcolor="#ffffff">________________________________________                                      <br /> Adres voorgedragen persoon</td>
</tr>
<tr>
<td bgcolor="#ffffff"></td>
<td bgcolor="#ffffff">________________________________________                                      <br /> Social Security Number van voorgedragen persoon                                      <span style="font-size: xx-small;">(enkel                                      USA)</span></td>
</tr>
<tr>
<td bgcolor="#ffffff" colspan="2">
<p>Ik, (naam eigenaar/beheerder) __________________________________, ben eigenaar/beheerder van hierboven genoemd bedrijf en verzoek mijn lidmaatschap met ID# ________________ te transfereren naar hierboven vermeld persoon. Ik begrijp dat                                        door het lidmaatschap te verplaatsen naar                                        deze persoon, ik het lidmaatschap van het                                        bedrijf heb verbeurd, alsook de resulterende                           of potentiële downline en alle kortingen of inkomen die hieruit voortvloeien of kunnen voortvloeien.</p>
</td>
</tr>
<tr>
<td bgcolor="#ffffff" valign="bottom">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>___________________________________<br /> Handtekening bedrijfseigenaar/beheerder</p>
</td>
<td bgcolor="#ffffff">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>_____________________________________ <br /> Datum</p>
</td>
</tr>
</tbody>
</table>
<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>