<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
       
         
         
         
         
         
            <title>ClubShop Rewards Business Manual</title>
            
            <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/getstarted"/></h4>
	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

	
	


<p class="text_normal"><xsl:value-of select="//lang_blocks/p2"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p3"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p4"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p5"/></p>




<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p10"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p11"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p6"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p7"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p8"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p9"/></span></p>

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p12"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p13"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p13a"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p13b"/></span></p>

<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr>
<td  bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p28"/></b></td>
<td  bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p29"/></b></td>
<td  bgcolor="#FFFFFF"><b>AlertPay/EcoCard</b></td>
<td  bgcolor="#FFFFFF"><b>AlertPay/EcoCard</b></td>

</tr>

<tr>
<td  bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p36"/></b></td>
<td  bgcolor="#FFFFFF">$1.00</td>
<td  bgcolor="#FFFFFF">$1.00 <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/comm"/></td>
<td  bgcolor="#FFFFFF">$3.00 <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/disburse"/></td>


</tr>
</table>

<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p35"/></b></span><xsl:text> </xsl:text><a href="https://www.clubshop.com/fees/full.xml" target="_blank"><xsl:value-of select="//lang_blocks/p13c"/></a></p>


<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p14"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></span></p>



<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p18"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p19"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p20"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p21"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p16"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p17"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p22"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p24"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p25"/></span></p>
<p><span class="style24"><b><xsl:value-of select="//lang_blocks/p26"/></b></span><xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p27"/></span></p>

<hr/>

<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>

</div>
</div>


            
  
              
          
    <!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>


