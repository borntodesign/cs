<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Business Manual</title>
			
			<xsl:comment> paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ </xsl:comment>
<xsl:comment><![CDATA[[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]]]></xsl:comment>
<xsl:comment><![CDATA[[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]]]></xsl:comment>
<xsl:comment><![CDATA[[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]]]></xsl:comment>
<xsl:comment><![CDATA[[if gt IE 8]><!]]></xsl:comment><xsl:comment><![CDATA[<![endif]]]></xsl:comment>
			
			
			


<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



</head>
<body>


	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>





	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/getstarted"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p50"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">




<span class="style24"><xsl:value-of select="//lang_blocks/p8"/></span><br/>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>

<span class="style24"><xsl:value-of select="//lang_blocks/p11"/></span>
<br/><br/>
<ol>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p14"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>

 <hr align="left" width="100%" size="1" color="#A35912"/>


<span class="style28"><xsl:value-of select="//lang_blocks/p16"/></span>
<p><xsl:value-of select="//lang_blocks/p17"/></p>

<span class="style28"><xsl:value-of select="//lang_blocks/p18"/></span>


<p><span class="style24"><xsl:value-of select="//lang_blocks/p19"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p20"/></p>


<span class="style28"><xsl:value-of select="//lang_blocks/p21"/></span>

<p><span class="style24"><xsl:value-of select="//lang_blocks/p22"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p23"/></p>

<p><b><xsl:value-of select="//lang_blocks/p24"/></b> <xsl:text> </xsl:text> <a href="http://www.alertpay.com/" target="_blank" class="nav_footer_brown" ><xsl:value-of select="//lang_blocks/p25"/></a>.</p>


<p><span class="style24"><xsl:value-of select="//lang_blocks/p44"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p45"/></p>
<p><b><xsl:value-of select="//lang_blocks/p46"/></b><xsl:text> </xsl:text><a href="https://www.ecocard.com/Registration.aspx?Referee=DHS">https://www.ecocard.com/Registration.aspx?Referee=DHS</a></p>









<p><span class="style24"><xsl:value-of select="//lang_blocks/p26"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></p>

<p><xsl:value-of select="//lang_blocks/p28"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/intl_pay_agent.html" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p29"/></a></p>
<br/>
<span class="style28"><xsl:value-of select="//lang_blocks/p30"/></span>

<p><span class="style24"><xsl:value-of select="//lang_blocks/p31"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p32"/></p>
<p><span class="style24"><xsl:value-of select="//lang_blocks/p33"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34"/></p>

<p><xsl:value-of select="//lang_blocks/p35"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/comm_prefs.cgi" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p36"/></a></p>

<hr align="left" width="100%" size="1" color="#A35912"/>

<p><span class="style28"><xsl:value-of select="//lang_blocks/p37"/></span></p>

<p><xsl:value-of select="//lang_blocks/p38"/></p>

<p><span class="style28"><xsl:value-of select="//lang_blocks/p39"/></span></p>

<p><xsl:value-of select="//lang_blocks/p40"/></p>



<p><span class="style28"><xsl:value-of select="//lang_blocks/p41"/></span></p>

<p><xsl:value-of select="//lang_blocks/p42"/></p>


<br/>



<p><a href="http://www.clubshop.com/manual/"><img src="http://www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>

			
	<br/><br/><br/>
</div></div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>

