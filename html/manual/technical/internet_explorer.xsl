<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
    <script src="/js/jquery.min.js"></script>
    <script src="/js/partner/foundation.js"></script>
    <script src="/js/partner/app.js"></script>
    <script src="/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p0"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p2"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
<li><xsl:value-of select="//lang_blocks/p8"/></li>
<li><xsl:value-of select="//lang_blocks/p9"/></li>
<li><xsl:value-of select="//lang_blocks/p10"/></li>
<li><xsl:value-of select="//lang_blocks/p11"/></li>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
</ul>
<p><xsl:value-of select="//lang_blocks/p13"/> <xsl:text> </xsl:text><a href="http://support.microsoft.com/default.aspx" target="_blank"><xsl:value-of select="//lang_blocks/p14"/></a></p>


<p><xsl:value-of select="//lang_blocks/p15"/> <xsl:text> </xsl:text><a href="http://support.microsoft.com/default.aspx" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a></p>

<p><xsl:value-of select="//lang_blocks/p17"/></p>
<ul>
<li><a href="/manual/technical/browserhelp.xml" target="_blank"><xsl:value-of select="//lang_blocks/p18"/></a></li>
<li><a href="/manual/technical/cookie_help.xml" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p20"/> <br/><a href="/manual/technical/cookiesetup.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p22"/></li>
<li><xsl:value-of select="//lang_blocks/p23"/></li>
<li><xsl:value-of select="//lang_blocks/p24"/></li>
<li><xsl:value-of select="//lang_blocks/p25"/></li>
<li><xsl:value-of select="//lang_blocks/p26"/></li>

</ol>

<p><xsl:value-of select="//lang_blocks/p27"/></p>

<span class="style24"><xsl:value-of select="//lang_blocks/p28"/></span>

<p><a href="/manual/technical/cookiesetup.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p29"/></li>
<li><xsl:value-of select="//lang_blocks/p30"/></li>
<li><xsl:value-of select="//lang_blocks/p31"/></li>
<li><xsl:value-of select="//lang_blocks/p32"/></li>
<li><xsl:value-of select="//lang_blocks/p33"/></li>
<li><xsl:value-of select="//lang_blocks/p34"/></li>
</ul>

<span class="style24"><xsl:value-of select="//lang_blocks/p35"/></span>

<ul>
<li><xsl:value-of select="//lang_blocks/p36"/></li>
<li><xsl:value-of select="//lang_blocks/p37"/></li>
<li><xsl:value-of select="//lang_blocks/p38"/></li>
<li><xsl:value-of select="//lang_blocks/p39"/></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p40"/></p>

<p><xsl:value-of select="//lang_blocks/p41"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42"/><xsl:text> </xsl:text><a href="mailto:support1@dhs-club.com?Subject=Javascript_Help">support1@dhs-club.com</a></p>

<hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
