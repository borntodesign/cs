<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
    <script src="/js/jquery.min.js"></script>
    <script src="/js/partner/foundation.js"></script>
    <script src="/js/partner/app.js"></script>
    <script src="/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p0"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span>

<p><xsl:value-of select="//lang_blocks/p2"/></p>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p3"/></span>
<p><xsl:value-of select="//lang_blocks/p4"/><br/>
<xsl:value-of select="//lang_blocks/p5"/></p>
<img src="/manual/images/1-ie4advanced-image.jpg" height="416" width="375" alt="Internet Explorer 4.x"/>


<br/><br/>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p3"/></span>
<p><xsl:value-of select="//lang_blocks/p6"/><br/>
<xsl:value-of select="//lang_blocks/p7"/></p>
<img src="/manual/images/1-iecustomlevels-image.jpg" height="407" width="355" alt="Internet Explorer 5 - Internet Options"/>

<br/><br/>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p8"/></span>
<p><xsl:value-of select="//lang_blocks/p9"/><br/>
<xsl:value-of select="//lang_blocks/p10"/></p>
<img src="/manual/images/mozilla_cookies.gif" height="407" width="355" alt="Mozilla-Cookies"/>
<br/><br/>

<hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
