<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
    <script src="/js/jquery.min.js"></script>
    <script src="/js/partner/foundation.js"></script>
    <script src="/js/partner/app.js"></script>
    <script src="/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p0"/></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>

<ol>

<li><xsl:value-of select="//lang_blocks/p4"/>
	<ul>
		<li><xsl:value-of select="//lang_blocks/p5"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/p/index.php" target="_blank">https://www.clubshop.com/p/index.php</a></li>
		<li><xsl:value-of select="//lang_blocks/p6a"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cs/login.shtml" target="_blank">https://www.clubshop.com/cs/login.shtml</a></li>
		<li><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cs/login.shtml" target="_blank">https://www.clubshop.com/cs/login.shtml</a></li>
	</ul>
<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="/manual/technical/browserhelp.xml" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a></p>
</li>

<li><xsl:value-of select="//lang_blocks/p10"/></li>
<br/>



<li><xsl:value-of select="//lang_blocks/p11"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/cookie_test.cgi" target="_blank">https://www.clubshop.com/cgi/cookie_test.cgi</a> 
<p><xsl:value-of select="//lang_blocks/p12"/><br/><xsl:value-of select="//lang_blocks/p13"/></p>
<p><xsl:value-of select="//lang_blocks/p14"/></p></li>

</ol>


<hr/>
  <p><a href="http://www.clubshop.com/manual/"><img src="/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
