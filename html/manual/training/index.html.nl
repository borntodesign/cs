﻿<!DOCTYPE HTML >

<html>

<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">

<title>ClubShop Rewards Partner Training Gids</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />

<link rel="stylesheet" type="text/css" href="/css/partner/app.css" />

<link rel="stylesheet" type="text/css" href="/css/partner/general.css" />

<script src="//www.clubshop.com/js/partner/flash.js"></script>

<script src="//www.clubshop.com/js/partner/foundation.js"></script>

<script src="//www.clubshop.com/js/partner/app.js"></script>

<!--<script src="//www.clubshop.com/js/panel.js"></script>-->

<script type="text/javascript" src="_includes/js/tip.js"></script>

<script type="text/javascript"src="//www.clubshop.com/js/tipMap.js"> </script>



<script type="text/javascript" src="/js/transpose.js"></script>

<script type="text/javascript" src="/js/Currency.js"></script>

<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 









<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.larger_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 14px;
	text-transform: uppercase;
	color: #135FB3;
	font-weight: bold;
}
td.center {text-align:center;}

.blue_bolderest {
color: #005BE9;}
</style>
</head>
<body>

	<div class="container blue">

	<div class="row">

		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>

		<div class="six columns"></div>



	</div></div>



	<div class="container">

	<div class="row">

	<div class="twelve columns ">

	<br />
				
      <h4 class="topTitle">ClubShop Rewards Partner Training Gids</h4>

				<hr />

	
			
			
			
					<h5><span class="style28">CURSUS</span></h5><br/><br/>
					
<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 1: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/getstarted/index.html" target="_blank">Van start gaat</a></span> <br /><br /> 
<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 2: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/referralmarketing/index.html" target="_blank">Referral Marketing</a></span> <br /><br /> 
<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 3: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/getsetup/index.html" target="_blank">Voorbereiding</a></span> <br /><br /> 
<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 4: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/sponsoring/index.html" target="_blank">Partner Sponsoring</a></span> <br /><br /> 
<img src="/images/icons/icon_pro_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 5: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/coaching/index.html" target="_blank">Partner Coaching</a></span> <br /><br /> 
<img src="/images/icons/icon_pro_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 6: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/mdirector/index.html" target="_blank">Marketing Director</a></span> <br /><br /> 
<img src="/images/icons/icon_pro_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 7: </span><span class="style25"><a class="style24" href="//www.clubshop.com/vip/manual/training/guide/execdir/index.html" target="_blank">Executive Director</a></span><br /><br /> 
<br/><br/>
<h5><span class="style28">Marketing Cursussen</span></h5><br/>

<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 1: </span><span class="style25"> <a class="style24" href="//www.clubshop.com/localmarketing/" target="_blank">Locale Marketing</a></span><br /><br /> 
<img src="/images/icons/icon_basic_preplus.png"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="Gold_Bold">Cursus 2: </span><span class="style25"> <a class="style24" href="//www.clubshop.com/vip/manual/training/guide/online_merchant/index.html" target="_blank">ONLINE MERCHANT MARKETING</a></span><br /><br />
					
					
					
				
			

			
			
		
			
				<br/><br/><br/><br/><br/>
				<hr/>
				<p><a href="//www.clubshop.com/manual/"><img src="//www.clubshop.com/images/icons/icon_home.png" height="64" width="64" alt="Manual Home Page"/></a></p>


			
			
			</div>


			
			</div>
	</div>
	
	

	
	
	<!-- END LISA-->
	
	<!--Footer Container -->



<div class="container blue"> 

  <div class="row "> 

    <div class="twelve columns"> 

      <div class="push"></div>

    </div>



  </div>

  <div class="row "> 

    <div class="twelve columns centered"> 

      <div id="footer"> 

        <p>Copyright © 1997- <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>

      </div>



    </div>

  </div>



<!--End Footer Container -->



</div>



<script type="text/javascript">

    $(document).ready(function() {

        Transpose.transpose_currency();

    });

</script> 
<script type="text/javascript">
		$(window).load(function() {
			$('#featured').orbit({
				bullets: true
			});
		});
	</script>



</body>

</html>
