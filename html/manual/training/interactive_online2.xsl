<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Partner Manual</title>

<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
    
    <link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style type="text/css">

.Header_Main {
	font-size: 14px;
	color: #0089B7;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.2em;
	font-family: verdana;
	text-transform: uppercase;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.Text_Content_heighlight {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #0089B7;
	text-transform: uppercase;
	font-weight:bold;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.ContactForm_TextField {
    
	color: #000066;
	font-family: verdana;
	font-size: 8pt;
	text-transform: uppercase;
	
}

.style1{font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
}


.style3 {
	font-size: 12px;
	font-family: verdana;
	color: #A91B07;
	font-weight:bold;
}

.style24 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-transform: uppercase;
	color: #2A5588;
}


.half{list-style-image: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);
list-style-positioning: inside;}


.topTitle{text-align:center; color: #5080B0; }



li.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(http://www.clubshop.com/images/minions/icon_blue_bulleted.png);
}	

	
li.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(http://www.clubshop.com/images/minions/icon_gold_bulleted.png);
}


li.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;

}




.img_heads {
	margin-bottom: -10px;
}
	
	
td.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	background-color: #FFFFFF;
}
	


</style>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
<div class="six columns"></div>

</div></div>

<div class="container">
<div class="row">
<div class="twelve columns ">
<br />
<h4 class="topTitle">CLUBSHOP REWARDS PARTNER MANUAL</h4>
<br />
<hr />

</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">	


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span>
<br/><br/>

<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/><xsl:text> </xsl:text><a href="mailto:partnersales@dhs-club.com?Subject=Meeting Room Question">partnersales@dhs-club.com</a></p>


  
  
  
  <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>
   <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>  <br/>
  
  
 </div></div>




<!--Footer Container -->

<div class="container blue"> 
  		<div class="row "> 
    	<div class="twelve columns"> 
      	<div class="push"></div>
    	</div>
  		</div>
  		
  		
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997- 
          <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>