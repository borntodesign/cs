<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	media-type="text/html"/>

<xsl:template match="/">
<b><content>
<h3 style="color: #036"><img src="/images/point.jpg" alt="" width="125" height="65" />
<xsl:value-of select="//lang_blocks/t" /></h3>
<p><xsl:value-of select="//lang_blocks/p7" /><xsl:text>
</xsl:text><a>
<xsl:attribute name="href">mailto:<xsl:choose>
<xsl:when test="//upline_contact/id = '1' or not(//upline_contact/id)">support1@dhs-club.com</xsl:when>
<xsl:otherwise><xsl:value-of select="//upline_contact/emailaddress" /></xsl:otherwise>
</xsl:choose>
<xsl:if test="//user/id">?Subject=Question%20from%20<xsl:value-of select="concat(//user/firstname, '%20', //user/lastname, '%20', //user/alias)" /></xsl:if>
</xsl:attribute>
<xsl:value-of select="//lang_blocks/a12" /></a><xsl:text>
</xsl:text><xsl:value-of select="//lang_blocks/p8" />.</p></content>
<xsl:copy-of select="*" />
</b>
</xsl:template>

</xsl:stylesheet>