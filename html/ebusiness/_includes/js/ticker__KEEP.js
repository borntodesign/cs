/*
Typing Scroller
Submitted by bengaliboy00@hotmail.com (hp: http://www.angelfire.com/nt/bengaliboy/)
With modifications by Dynamicdrive.com
For full source code, usage terms, and 100s more scripts, visit http://dynamicdrive.com
*/

//Secify scroller contents
var line=new Array()
line[1]="EDUCATION.. is the food of the soul."
line[2]="LEARNING.. is the food of the soul."
line[3]="THOUGHT.. is the food of the soul."
line[4]="WISDOM.. is the food of the soul."
line[5]="KNOWLEDGE.. is the food of the soul."

//Specify font size for scoller
var ts_fontsize="14px"

//--Don't edit below this line

var longestmessage=1
for (i=2;i<line.length;i++){
if (line[i].length>line[longestmessage].length)
longestmessage=i
}

//Auto set scroller width
var tscroller_width=line[longestmessage].length

lines=line.length-1 //--Number of lines

//if IE 4+ or NS6
if (document.all||document.getElementById){
document.write('<form name="bannerform">')
document.write('<input type="text" name="banner" size="'+tscroller_width+'"')
document.write('  style="background-color: transparent'+document.body.text+'; font-family: verdana; font-size 12px: '+ts_fontsize+'; font-weight:bold; color:#FEA400; border: medium none" onfocus="blur()">')
document.write('</form>')
}

temp=""
nextchar=-1;
nextline=1;
cursor="\\"
function animate(){
if (temp==line[nextline] & temp.length==line[nextline].length & nextline!=lines){
nextline++;
nextchar=-1;
document.bannerform.banner.value=temp;
temp="";
setTimeout("nextstep()",9000)}
else if (nextline==lines & temp==line[nextline] & temp.length==line[nextline].length){
nextline=1;
nextchar=-1;
document.bannerform.banner.value=temp;
temp="";
setTimeout("nextstep()",9000)}
else{
nextstep()}}

function nextstep(){

if (cursor=="\\"){
cursor="|"}
else if (cursor=="|"){
cursor="/"}
else if (cursor=="/"){
cursor="-"}
else if (cursor=="-"){
cursor="\\"}


nextchar++;
temp+=line[nextline].charAt(nextchar);
document.bannerform.banner.value=temp+cursor
setTimeout("animate()",50)}

//if IE 4+ or NS6
if (document.all||document.getElementById)
window.onload=animate
// -->