
/***********************************************
* Ultimate Fade In Slideshow v2.0- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/


var mygallery=new fadeSlideShow({
	wrapperid: "fadeshow1", //ID of blank DIV on page to house Slideshow
	dimensions: [470, 391], //width/height of gallery in pixels. Should reflect dimensions of largest image
	imagearray: [
	    ["images/slides/e_business_institute_1.png", "#", "_parent", "EDUCATION.. is the food of the soul."],
		["images/slides/e_business_institute_2.png", "#", "_parent", "LEARNING.. is the food of the soul."],
		["images/slides/e_business_institute_3.png", "#", "_parent", "THOUGHT.. is the food of the soul."],
		["images/slides/e_business_institute_4.png", "#", "_parent", "WISDOM.. is the food of the soul."],
		["images/slides/e_business_institute_5.png", "#", "_parent", "KNOWLEDGE.. is the food of the soul."]  //<--no trailing comma after very last image element!
		
		
		
		
		
		
		
	],
	displaymode: {type:'auto', pause:5000, cycles:0, wraparound:false},
	persist: false, //remember last viewed slide and recall within same session?
	fadeduration: 500, //transition duration (milliseconds)
	descreveal: "ondemand",
	togglerid: ""
})
