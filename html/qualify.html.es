﻿<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
<head>
<title>Califìcate como Partner</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation2.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/general.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<!-- <script src="http://www.clubshop.com/js/partner/app.js"></script>-->
<script src="http://www.clubshop.com/js/partner/flash.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'> 
<script type="text/javascript" src="http://www.clubshop.com/js/transpose.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/Currency.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/cgi/transposeMEvarsJS.cgi"></script> 

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 

<style>
.larger_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #003399;
	font-weight: bold;
}

.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}


.special{
font-family: 'Asul', sans-serif; 
font-size: 16px;}

.special2{
font-family: 'Asul', sans-serif; 
font-size: 14px;}

}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" style=" margin:5px; " height=" 84px " width=" 288px "></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns"><br />
<h4 class="topTitle"></h4>
<hr />
<div align="center"><span class="largehead_blue_bold">Califìcate como Partner</span></div>
<!--
<div align="right"><a href="#" mce_href="#" class="style28gi" onclick="window.print();return false"><img src="/images/icon_print_page.gif" mce_src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a></div>
-->   				               
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
<p>Una vez que tienes 12 Pay Points en un mes, te calificas automàticamente como Partner y tendràs el privilegio de referir Afiliados, Partners, Grupos de Afinidad, Mercantes y de ganar ingresos como Partner. Estas son las dos maneras de hacerlo:</p>
<ol>
<li><span class="larger_blue_bolder">CALIFÌCATE DESPUÈS</span> - realizando compras  personales suficientes y refiriendo miembros que a su vez compren y te proporcionen 12 Pay Points al mes a traves de esas compras.</li>
<li><span class="larger_blue_bolder">CALIFÌCATE AHORA</span> - suscribiendote al GPS para obtener todos los beneficios incluidos que te ayudaràn a construir un negocio exitoso como Partner del ClubShop Rewards. </li>
</ol>
<p>Si bien la suscripciòn al GPS para calificarse como Partner no es requerida , màs del 90% de nuestros Partners se suscribe para poder acceder a todos los beneficios que el GPS ofrece.</p>
<p> </p>
</div>
</div>
<div class="row">
	<div class="eight columns">
			<div class="eight columns centered">
			
			<img src="http://www.clubshop.com/manual/gps/gps-box-version5.png" height="" width="" alt="gps">
			
			</div></div>

	<div class="four columns">
			<div class="four columns push-one">
			<br><br><br><br><br>
			
			<p><a href="https://www.clubshop.com/cgi/appx.cgi/0/upg" target="_blank" class="nice large blue button">SUSCRIBIRSE a GPS »</a></p>
			
			</div></div>
	</div>
	
	<div class="row">
	<div class="twelve columns">
		<div class="twelve columns push-three">
		<br><br><p>Si quieres aprender màs sobre la suscripciòn al GPS</p>
<a href="gps_new.html" target="_blank" class="nice medium blue button">HAZ CLICK AQUÌ»</a></div></div>
	
</div>
<br /><br /><br /><br /><br /><br /><br /><br /></div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
        <p>Copyright © 1997-2014 
          <!-- Get Current Year -->
          ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>