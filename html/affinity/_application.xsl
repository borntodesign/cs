<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
<title>Affinity Group Application</title>

<meta name="keywords" content="ClubShop, Shop, Online, Rewards, Merchant, Stores, Local, Customers, Mall, Save, Members, Points, Discount, Card, Fundraising, Business, Online Mall, Shop Online, Customer Loyalty, Card, Non-Profit, Cash Back"/>
<meta name="description" content="ClubShop, Shop, Online, Rewards, Merchant, Stores, Local, Customers, Mall, Save, Members, Points, Discount, Card, Fundraising, Business, Online Mall, Shop Online, Customer Loyalty, Card, Non-Profit, Cash Back"/>
<meta http-equiv="content-language" content="en-us"/>
	
<meta name="audience" content="general"/>
<meta name="abstract" content="digital online marketing"/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop"/>
<meta name="publisher" content="clubshop"/>
<meta name="company" content="clubshop"/>
<meta name="revisit-after" content="15 days"/>
<meta name="copyright" content="copyright  clubshop.com, All Rights Reserved"/>
<meta name="page-topic" content="ecommerce"/>
<meta name="robots" content="follow,index"/>
<meta name="rating" content="all"/>



<link rel="stylesheet" type="text/css" href="/cs/_includes/css/main.css" />

<!--link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" /-->
<script type="text/javascript" src="/js/get_cookie_by_name.js"></script>
<script type="text/javascript"><xsl:comment>
function setReferral(){
	var refspid = get_cookie_by_name('refspid');
	if (refspid){
		document.forms[0].referral.value = refspid;
		//document.getElementById('referralRow').style.display = 'none';
	}
}
</xsl:comment></script>

<style type="text/css">
body {
	background-color: #fff;
	background-image:none;
}

table {
	background-color: #FFFFFF;
}

td {
	padding:0;
}


.style12 {line-height: 16px;}
</style>

</head>
<body onload="setReferral()">
<div align="left">
	<span class="Orange_Large_11" style="text-align:left;"><xsl:value-of select="//lang_blocks/p1"/></span>
</div>	
	<br/>
<div align="center">
	<span ><xsl:value-of select="//lang_blocks/p23"/><br/><xsl:value-of select="//lang_blocks/p24"/></span>
</div>

<form action="/cgi/FormMail.cgi" method="post">


<table style="border:1px solid #fcbf12; margin:1em auto;" cellpadding="0" cellspacing="0">

<tr><td style="background-color:#fca641; background-image:url(/cs/images/bg/bg_header-orange-923x23.png)" class="Text_Body_Content"><span class="style13">
<!--ClubShop Membership Application Registration Form--><img src="/images/blanklin.gif" height="20" width="1" alt="" /></span>
</td></tr>

<tr><td>


<div align="center" class="style5 field"><xsl:value-of select="//lang_blocks/required"/></div>

<hr style="border: none; height:1px; background-color: #fcbf12; color:#fcbf12; width:98%; margin-right:auto; margin-left:auto;" />

        <table style="border:0; margin:auto 5em;" cellspacing="0">
         <tr> 
            <td>
                <input name="recipient" value="support1@dhs-club.com" type="hidden"/>
				<input name="email" value="apache@clubshop.com" type="hidden"/>
                <input name="required" value="agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,agemailaddress,referral,password,agreement" type="hidden"/>
                <input name="sort" value="order:agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,cell,skype,fax,agemailaddress,url,referral,password,agreement" type="hidden"/>
                <input name="redirect" value="http://www.clubshop.com/affinity/thankyou.html" type="hidden"/>
                <input name="subject" value="Affinity Group Application" type="hidden"/>
                <input name="env_report" value="HTTP_USER_AGENT" type="hidden"/>
              
             <table border="0" cellspacing="4">
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p2"/></td>
                  <td><input class="registration_TextField" name="agname" id="agname" size="50" type="text"/></td>
                </tr>

                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p3"/></td>
                  <td><input class="registration_TextField" name="firstname" id="firstname" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p4"/></td>
                  <td><input class="registration_TextField" name="lastname" id="lastname" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p5"/></td>
                  <td><input class="registration_TextField" name="emailaddress" id="emailaddress" size="50" type="text"/></td>
                </tr>


                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p6"/></td>
                  <td><input class="registration_TextField" name="address" id="address" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p7"/></td>
                  <td><input class="registration_TextField" name="city" id="city" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p8"/></td>
                  <td><input class="registration_TextField" name="state" id="state" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p9"/></td>
                  <td><input class="registration_TextField" name="postal" id="postal" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p10"/></td>
                  <td><input class="registration_TextField" name="country" id="country" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p11"/></td>
                  <td><input class="registration_TextField" name="phone" id="phone" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p12"/></td>
                  <td><input class="registration_TextField" name="cell" id="cell" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p13"/></td>
                  <td><input class="registration_TextField" name="skype" id="skype" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p14"/></td>
                  <td><input class="registration_TextField" name="fax" id="fax" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p15"/></td>
                  <td><input class="registration_TextField" name="agemailaddress" id="agemailaddress" size="50" type="text"/></td>
                </tr>

                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p16"/></td>
                  <td><input class="registration_TextField" name="url" id="url" size="50" type="text"/></td>
                </tr>
                <tr id="referralRow"> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p17"/></td>
                  <td><input class="registration_TextField" name="referral" id="referral" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p18"/></td>
                  <td><input class="registration_TextField" name="password" id="password" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p19"/><br />
                  	<a href="/affinity/affinity_terms.shtml" target="_parent" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p20"/></a></td>
                  <td align="left" valign="top"><input name="agreement" value="Yes, I have read and agree to the Affinity Group Terms of Agreement" type="checkbox"/></td>

                </tr>
              </table>

                    </td></tr></table>

<hr style="border: none; height:1px; background-color: #fcbf12; color:#fcbf12; width:98%; margin-right:auto; margin-left:auto;" />

<div style="background-color: #fca541; margin: 5px; padding: 5px; text-align:center;">
<input type="submit" name="Submit" value="Submit"/>
<xsl:text> </xsl:text>
<input type="reset" name="Reset" value="Reset"/>
</div>

<br/>
<div align="center"><a href="http://www.clubshop.com/common/privacy.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a></div>

</td></tr></table>

</form>


 </body>
 </html>
</xsl:template>
</xsl:stylesheet>
