<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<!--xsl:include href = "affinity-nav.xsl"/-->
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Affinity Group</title>
<style type="text/css">
body {background-color: #FFFFFF; color: black;}
td {border-width: 0; padding: 0;}
td#banner {border-bottom: 2px solid #0033CC; height: 41px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	padding: 1em;
	font: smaller Verdana, Arial,san-serif;
	vertical-align:top;
}
td#main h2 {
	font: bold 100% Verdana;
	color:#0066cc;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #99FF33;
	vertical-align:top;
}
td#main p {margin: 1em 2.5em;}
td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #0033CC;
	width: 167px;
}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style>
<style type="text/css">
/* menu styles */
td#sidelinks a {
	display: block;
	margin: 0 3px 0 0;
	padding: 1px 10px 1px 5px;
	text-decoration: none;
	font: normal smaller Arial, Verdana, sans-serif;
	color: Black;
	background: transparent;
}
td#sidelinks a:visited {color: Green;
}
td#sidelinks h5 {background-color: #CEFF9C; color: #000000;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial,Verdana, sans-serif ;
   border-bottom: 1px solid #0033CC;
}
td#sidelinks h4 {background-color: #CCDAFF; color: #000000;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold  small Arial, Verdana, sans-serif ;
   border-bottom: 1px solid #0033CC;
   }
td#sidelinks a:hover {
	background-color: #F0F4FF;
	color: #330099;
	border-right: 7px solid #99FF33;
	padding-right: 7px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}

</style>
</head>
<body>
<table cellspacing="0">
  <tr> 
    <td colspan="2" id="banner"> 
     <img src="http://www.clubshop.com/affinity/images/affinity_hdr.gif" width="750" height="83" border="0" alt="Affinity Group Control Center"/>
<b>For:</b>AG-name <b>AG-id:</b>AG-number <b>Membership ID:</b> </td></tr>
<tr>
<td id="sidelinks">
<xsl:for-each select = "document('./affinity-nav.xml')//teams/leader">
	<h4><xsl:value-of select="@name" /></h4>

	<xsl:for-each select = "./*">
		<a><xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
			<xsl:value-of select="." /></a>
	</xsl:for-each>
</xsl:for-each>

</td>
    <td width="581" id="main">

<xsl:copy-of select = "*/*"/><!-- by doing */* instead of * we get rid of the 'article' tag -->
</td>
</tr>

</table>
<p style="text-align: right; margin-top: 3em;">
    <a href="http://validator.w3.org/check?uri=referer"><img style="border: 0"
        src="/images/valid-xhtml10.png"
        alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
</p>
</body>
</html>
</xsl:template>
</xsl:stylesheet>