<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="nav">
<xsl:for-each select = "document('./affinity-nav.xml')//teams/leader">
<h4><xsl:value-of select="@name" /></h4>

<xsl:for-each select = "./*">
<a><xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
<xsl:value-of select="." /></a>
</xsl:for-each>
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>