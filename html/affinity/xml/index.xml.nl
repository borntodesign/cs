<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="./affinity.xsl" ?>
<article>
<table><tr><td valign="top">
<img src="http://www.clubshop.com/images/hands.gif" height="121" width="115" alt="Affinity Group"/></td>
<td><h2>Non-Profit Organisatie Fondsenwerving</h2>
<br />Fundraisers organiseren en fondsen werven om een non-profit organisatie te steunen kan soms moeilijk en tijdrovend zijn. Met het ClubShop Affinity Group programma kan een vereniging zonder winstoogmerk gemakkelijk en op voordurende basis geld inzamelen.</td>
</tr>

</table>
<p><b>Hieronder de nieuwe manier van fondsenwerven</b></p>

<p>1. U post heel eenvoudig de ClubShop.com links en banners die wij u bieden (waar het ID nummer van uw organisatie is ingebed) op uw website, nieuwsbrieven, bulletins, brochures, enz. - dit is alles wat u hoeft te doen!</p>
<p>2. Wanneer iemand op de link of banner klikt, of uw ClubShop.com webpagina bezoekt en zich inschrijft voor een gratis lidmaatschap in onze kopersclub, dan wordt uw organisatie voor die verwijzingen beloond.</p>
<p>3. De leden die uw organisatie verwees, verdienen op al hun aankopen "Reward punten" (die verzilverbaar zijn voor Cash Backs of Gift Certificaten) EN wij geven een percentage van iedere aankoop terug aan uw organisatie in de vorm van een maandelijkse donatie. Het percentage van die donaties zullen variëren, maar schommelen normaliter rond 2% en kunnen oplopen tot 6%, dit hangt af van de commissie die we op die winkel ontvangen.</p>
<p>4. Informatie over aankopen, retours en punten is terug te vinden op een online rapport en punten worden geaccumuleerd op maandelijkse basis, om in de vorm van een donatie aan uw organisatie terugbetaald te worden, 30 dagen na de opgegeven maand om rekening te kunnen houden met eventuele terugzendingen.</p>
<p>5. Leden kunnen ook opteren om de cashwaarde van hun verzilverbare Reward punten te doneren aan uw organisatie. Deze donaties worden ook op maandelijkse basis doorgestuurd.</p>
<h2>Zo gaat u van start</h2>
<p>Klik op de Affinity Group Banner Links (link aan de linkerzijde) en post de link of banner advertentie (met uw embedded ID nummer) op uw website, nieuwsbrieven, bulletins, brochures, enz. Deze Affinity Group banner en link leidt de mensen rechtsreeks naar www.ClubShop.com en uw organisatie wordt beloond voor de verwijzingen.</p>
<p>Zeg het voort! Help mensen te <b>"Shop and Save the ClubShop Way"</b> om waardevolle ClubShop Reward punten te verdienen op hun online aankopen, en waarbij u hen tegelijk de mogelijkheid geeft om een percentage van al hun ClubShop Mall aankopen te doneren aan uw organisatie. Start nu door uw Affinity Group banner link op uw website en/of nieuwsbrief te plaatsen en beleef vanaf vandaag fondsenwerving op een nieuwe manier!</p>
<p><b>Het is eenvoudig en gemakkelijk om fondsen te werven met ons Affinity Group programma!</b></p>

</article>