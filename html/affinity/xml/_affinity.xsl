<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="//lang_blocks/title" /></title>
<style type="text/css">
body {background-color: #FFFFFF; color: black;}
td {border-width: 0; padding: 0;}
td#banner {border-bottom: 2px solid #0033CC;}
td#main {
	background-color: transparent;
	color: black;
	padding: 1em;
	font-family: Verdana, Arial, san-serif;
	font-size: 80%;
	vertical-align:top;
}
td#main p {margin: 1em 2.5em;}
td#main h2 {
	font: bold 100% Verdana,sans-serif;
	color:#0066cc;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #99FF33;
	vertical-align:top;
}
td#sidelinks
{
	vertical-align: top;
	border-right: 1px solid #0033CC;
	width: 200px;
}

td#sidelinks a
{
	display: block;
	margin: 0 3px 0 0;
	padding: 1px 10px 1px 5px;
	text-decoration: none;
	font-family:  Arial,Helvetica,sans-serif;
	font-size: 80%;
	color: #000;
	border-right: 3px solid white;
}

td#sidelinks a:visited
{
	color: Green;
	border-right: 3px solid white;
}

td#sidelinks h4
{
	background-color: #CCDAFF;
	color: #000000;
	margin: 0 3px 0 0;
	padding: 0.8em 0.2em 0 0.2em; 
	font-family:  Arial,Helvetica,sans-serif;
	border-bottom: 1px solid #0033CC;
	font-size: 80%
}
td#sidelinks a:hover {
	background-color: #F0F4FF;
	color: #330099;
	border-right: 3px solid #99FF33;
}
span.hd
{
	padding: 0 0.3em 0 1em;
	font-weight: bold;
}
</style>
</head>
<body>
<table cellspacing="0">
  <tr> 
    <td colspan="2" id="banner"> 
     <img src="/affinity/images/affinity_hdr.gif" width="750" height="83" border="0" alt="Affinity Group Control Center"/>
<div style="text-align:center;">
<span class="hd"><xsl:value-of select="//lang_blocks/for" />:</span><xsl:value-of select="//user/company_name" />
<span class="hd"><xsl:value-of select="//lang_blocks/agid" />:</span><xsl:value-of select="//user/alias" />
<span class="hd"><xsl:value-of select="//lang_blocks/mid" />:</span><xsl:value-of select="//user/id" />
</div>
</td></tr>
<tr>
<td id="sidelinks">
<h4><xsl:value-of select="//nav/leader1"/></h4>
<a href="/cgi-bin/wwg"><xsl:value-of select="//nav/a1"/></a>
<!-- notice the nowrap below - this is to force IE to make at least this text not wrap
since it doesn't seem to want to honor either a width style on the table cell or even a width
attribute-->
<a href="/cgi/reward_center.cgi" style="white-space:nowrap"><xsl:value-of select="//nav/a2"/></a>
<a>
<xsl:attribute name="href">mailto:<xsl:value-of select="//upline_contact/emailaddress" />?subject=<xsl:value-of select="concat(//lang_blocks/title, ' ', //user/alias)" />
</xsl:attribute>
<xsl:value-of select="//nav/a3"/></a>
<a href="https://www.clubshop.com/cgi/app.cgi?update=1" ><xsl:value-of select="//nav/a4"/></a>
<a href="/manual/business/forms/affinity_terms.html" ><xsl:value-of select="//nav/a5"/></a>


<xsl:choose>
	<xsl:when test="//user/id">
		<a href="https://www.clubshop.com/cgi-bin/Login.cgi?action=logout"><xsl:value-of select="//nav/logout"/></a>

	</xsl:when>
  
	<xsl:otherwise>
		<a href="https://www.clubshop.com/cgi-bin/Login.cgi?destination=" onclick="this.href=this.href + location.href"><xsl:value-of select="//nav/login"/></a>
	</xsl:otherwise>
</xsl:choose>
<h4><xsl:value-of select="//nav/leader2"/></h4>
<a href="https://www.clubshop.com/cgi/member_ppp_rpt.cgi"><xsl:value-of select="//nav/a7"/></a>
<a href="https://www.clubshop.com/cgi/member_trans_rpt.cgi"><xsl:value-of select="//nav/a8"/></a>

<h4><xsl:value-of select="//nav/leader3"/></h4>
<a href="https://www.clubshop.com/banners/select.xsp" ><xsl:value-of select="//nav/a9"/></a>
<h4><xsl:value-of select="//nav/leader5"/></h4>
<a href="http://www.clubshop.com/outletsp/bcenter.html" ><xsl:value-of select="//nav/acards"/></a>
<a href="http://www.clubshop.com/cgi/referral_cards.cgi"><xsl:value-of select="//nav/areferred"/></a>
</td>
    <td id="main">
<table><tr><td valign="top">
<img src="/images/hands.gif" height="121" width="115">
<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/title" /></xsl:attribute></img></td>
<td><h2><xsl:value-of select="//lang_blocks/h3" /></h2>
<br/><xsl:value-of select="//lang_blocks/raising" /></td>
</tr></table>

<h2><xsl:value-of select="//lang_blocks/h4" /></h2>
<p></p>
<ol>
<xsl:for-each select = "//lang_blocks/l1/*">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ol>

<h2><xsl:value-of select="//lang_blocks/h5" /></h2>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2-1" /><b> &quot;<xsl:value-of select="//lang_blocks/p2-2" />&quot; </b>
<xsl:value-of select="//lang_blocks/p2-3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p3" /></b></p>

</td>
</tr>

</table>

</body>
</html>
</xsl:template>
</xsl:stylesheet>