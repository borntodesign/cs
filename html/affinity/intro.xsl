<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

<xsl:include href = "common.xsl" />
<xsl:param name="id" />
<xsl:param name="dn" />

   <xsl:template match="/">
    
         <html>
         <head>
            <title><xsl:value-of select="//lang_blocks/title"/></title>
<meta name="description">
	<xsl:attribute name="content"><xsl:value-of select="//lang_blocks/meta_description" /></xsl:attribute>
</meta>
<meta name="keywords">
	<xsl:attribute name="content"><xsl:value-of select="//lang_blocks/meta_keywords" /></xsl:attribute>
</meta>

<link href="presentation.css" rel="stylesheet" type="text/css"/><link href="local.css" rel="stylesheet" type="text/css"/>
<xsl:if test="$id"><link rel="stylesheet" type="text/css">
<xsl:attribute name="href">/cgi/setck.cgi?id=<xsl:value-of select="$id" /></xsl:attribute>
</link></xsl:if>
</head>
<body>

<xsl:call-template name = "header" />
<table><tr>
<td id="sidelinks" valign="top">
<xsl:call-template name = "nav" />
</td>
    <td width="581" id="main" valign="top">

<h5 align="center"><xsl:value-of select="//lang_blocks/p0"/></h5>
<br/>

<p><xsl:value-of select="//lang_blocks/p1"/></p>

<h5><xsl:value-of select="//lang_blocks/p2"/></h5>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>

<p><xsl:value-of select="//lang_blocks/p5"/></p>


<p><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p7"/></b></p>

<p><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/"  target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p10"/></p>



<p><xsl:value-of select="//lang_blocks/p11"/></p>


<h5><xsl:value-of select="//lang_blocks/p12"/></h5>

<p><xsl:value-of select="//lang_blocks/p13"/></p>
<p><xsl:value-of select="//lang_blocks/p14"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p15"/></b></p>
<p><xsl:value-of select="//lang_blocks/p16"/></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>
<p><xsl:value-of select="//lang_blocks/p18"/></p>








</td></tr></table>


</body></html>
</xsl:template>
</xsl:stylesheet>