<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:param name="request.filename"/>

<xsl:template match="/">
<html>
<head>
<title>Affinity Group Banners</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<style type="text/css"> 
body{
	margin-top: 0;
	background-color: #fff;
	color: black;
}

td.desc {
	text-align: right;
	vertical-align: top;
	padding: 20px 2em 2em 2em;
	font-size: 80%;
	font-family: sans-serif;
	font-weight: bold;
	border-left: 1px solid green;
	border-bottom: 1px dotted green;
	background-color: #ffe;
}
td.bn {
	border-bottom: 1px dotted green;
}
a.bn img {
	padding: 3px;
	border: 2px solid white;
}

div.b{
/*	display: none; */
	font-size: 75%;
	background-color: #efe;
	padding: 3px 6px;
	border: 1px solid silver;
	margin-top: 2px;
}
table#mainTBL {
	border-collapse: collapse;
/*	border-spacing: 0 12px;  IE doesn't support this. Have to go with a spacer row ;-(*/
}
table#mainTBL td {
	padding: 0.5em;
}
td.spc { height: 12px }
p.a{font-family: Arial, san-serif;
font-size: smaller;
}
</style>
<!--script type="text/javascript" src="js.js"></script-->
</head>
<body>

<table style="width:750px; border: 1px solid #003366">
<tr> 
      <td style="color: #003399; font-size: 90% font-family: Georgia, Times New Roman, Times, serif"><img src="head_portal5.gif" width="442" height="69" alt="clubshop.com" /></td><td><img src="bannergen.gif" width="290" height="69" alt="agbanner generator"/>
          </td>
</tr>

</table>

<select style="margin-top:2px" onchange="location.search=this.options[this.selectedIndex].value;">
<xsl:for-each select="*/lang_blocks/menu-lang/*"><xsl:sort order="ascending" />
<option><xsl:attribute name="value"><xsl:value-of select="@value" /></xsl:attribute>
	<xsl:if test="@value = substring-after($request.filename, '/home/httpd/html/banners/')">
	<xsl:attribute name="selected">selected</xsl:attribute>
	</xsl:if>
<xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<!-- If we don't have an ID on them, the banners will not work -->
<xsl:choose><xsl:when test="//memberinfo/id">

<!--I added this--><p class="a"><xsl:value-of select="*/lang_blocks/s5" />:</p>
<div class="b">http://www.clubshop.com/cgi-bin/rd/10<xsl:choose>
	<xsl:when test="@type = 'consumer'">10</xsl:when>
</xsl:choose>,,refid=<xsl:value-of select="//memberinfo/id" /></div>
<br/>
<p class="a">
<b><xsl:value-of select="*/lang_blocks/s4" />:</b></p>
<p class="a"><xsl:value-of select="*/lang_blocks/s2" />.
<xsl:value-of select="*/lang_blocks/s3" />.</p></xsl:when>
<xsl:otherwise><p class="a" style="color:red"><xsl:value-of select="*/lang_blocks/noid" />.
<a href="/login.html"><xsl:value-of select="*/lang_blocks/login" /></a>.</p>
</xsl:otherwise>
</xsl:choose>

<table id="mainTBL">
<xsl:for-each select="//img">
<tr>
<td class="desc"><xsl:value-of select="concat(./@width, ' x ', ./@height)" /></td>
<!--td class="bn"><a class="bn" href="javascript:void(0)" onclick="flipDesc(this);"><img-->
<td class="bn"><a class="bn" href="javascript:void(0)"><img>

	<xsl:attribute name="width"><xsl:value-of select="./@width" /></xsl:attribute>
	<xsl:attribute name="alt"><xsl:value-of select="./@alt" /></xsl:attribute>
	<xsl:attribute name="height"><xsl:value-of select="./@height" /></xsl:attribute>
	<xsl:attribute name="src"><xsl:value-of select="./@src" /></xsl:attribute>
	<!--xsl:attribute name="onmouseover">this.style.borderColor='#00ff00'</xsl:attribute>
	<xsl:attribute name="onmouseout">this.style.borderColor='white'</xsl:attribute-->
</img></a>
<div class="b">&lt;a href=&quot;http://www.clubshop.com/cgi-bin/rd/<xsl:choose>
	<xsl:when test="@type = 'consumer'">10</xsl:when>
	<xsl:otherwise>5</xsl:otherwise>
</xsl:choose>,,refid=<xsl:value-of select="//memberinfo/id" />&quot;&gt;&lt;img<br /> src=&quot;http://www.clubshop.com/affinity/banners/<xsl:value-of select="@src" />&quot;<br />
height=&quot;<xsl:value-of select="@height" />&quot; width=&quot;<xsl:value-of select="@width" />&quot; alt=&quot;<xsl:value-of select="@alt" />&quot; /&gt;&lt;/a&gt;
</div></td>
</tr>
<tr><td colspan="2" class="spc"></td></tr>
</xsl:for-each>
</table>

<p style="margin-top: 3em; text-align: right;">
    <a href="http://validator.w3.org/check?uri=referer"><img style="border:none"
        src="/images/valid-xhtml10.png"
        alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
</p>

</body>
</html>

</xsl:template>
</xsl:stylesheet>


