<?xml version="1.0" ?>
<?xml-stylesheet href="NULL" type="application/x-xsp"?>
<?xml-stylesheet type="text/xsl" href="banners.xsl" ?>

<xsp:page
	xmlns:xsp="http://www.apache.org/1999/XSP/Core"
	xmlns:userinfo="http://localhost/xsp/userinfo/v1"
	xmlns:util="http://apache.org/xsp/util/v1"
>

<base>
<util:include-uri href="axkit:banners.xml"/>
<user><userinfo:userinfo/></user>

<!-- NO USER SERVICEABLE COMPONENTS FROM HERE UP -->
<!-- our predefined image 'types': consumer, bizop -->

<img src="banner_468_cy.gif" width="468" height="60" alt="clubshop.com" type="consumer" />
<img src="banner_234_cy.gif" width="234" height="60" alt="clubshop.com" type="consumer" />
<img src="banner_240_cy.gif" width="120" height="240" alt="clubshop.com" type="consumer" />
<img src="bannerbutton_cy.gif" width="120" height="90" alt="clubshop.com" type="consumer" />



<!-- NO USER SERVICEABLE COMPONENTS BELOW THIS LINE :) -->
</base>

<!-- The following line should only be used when testing. Comment it out otherwise. -->
<!--xsp:logic>AxKit::Apache->content_type('text/xml');</xsp:logic-->

</xsp:page>
