<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"/>

<xsl:template match="/">
<b>
<!-- pull these into the head of the layout doc -->
<head-items>
<style type="text/css">
table#report {border-collapse:collapse; margin-top: 0.5em;}
table#report td,th {padding: 3px 5px;}
table#report th {text-align: left; font-size: 90%;}
table#report tr.r1 {background-color: #ffe;}
table#report tr.r0 {background-color: #eee;}
table#report td.num {text-align:right;}
select#select_period {
	font-size: 85%;
	background-color:#ffe;
	margin-left: 1em;
	margin-right: 2px;
}
input#smt {
	font-size:85%;
	border:1px solid green;
	padding: 0;
}
input#smt:hover {background-color:#ffe;}
</style>
</head-items>

<xsl:copy-of select = "*"/>
<main_content>
<h2><xsl:value-of select="//lang_blocks/nav/a7" /></h2>
<xsl:if test="//select/*">
<form action="" style="text-align:right">
<xsl:value-of select="//lang_blocks/rpt/select-period" />
<xsl:copy-of select="//select[@id = 'select_period']" />
<input id="smt" type="submit">
<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/rpt/go" /></xsl:attribute>
</input>
</form></xsl:if>

<xsl:choose><xsl:when test="//reward_point_list/*">
<table id="report">
<tr>
<th><xsl:value-of select="//lang_blocks/rpt/name" /></th>
<th><xsl:value-of select="//lang_blocks/rpt/alias" /></th>
<th><xsl:value-of select="//lang_blocks/rpt/amount" /></th>
<th><xsl:value-of select="//lang_blocks/rpt/donation" /></th>
</tr>
<xsl:apply-templates select = "//reward_point_list/*" />
<tr><td colspan="2" align="right"><xsl:value-of select="//lang_blocks/rpt/totals" />:</td>
<td class="num"><xsl:value-of select="sum(//reward_point_list/item/amount)" /></td>
<td class="num"><xsl:value-of select="sum(//reward_point_list/item/commission)" /></td></tr>
</table>
</xsl:when><xsl:otherwise>
<p><xsl:value-of select="//lang_blocks/rpt/no-results" /></p>
</xsl:otherwise></xsl:choose>
</main_content></b>
</xsl:template>


<xsl:template match="//reward_point_list/*">
<tr><xsl:attribute name="class">r<xsl:value-of select="position() mod 2" /></xsl:attribute>
<td><xsl:value-of select="concat(firstname, ' ', lastname)" /></td>
<td><xsl:value-of select="alias" /></td>
<td class="num"><xsl:value-of select="amount" /></td>
<td class="num"><xsl:value-of select="commission" /></td>
</tr>
</xsl:template>
</xsl:stylesheet>