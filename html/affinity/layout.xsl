<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="/js/utils.js"></script>
<title><xsl:value-of select="//lang_blocks/title" /></title>
<link rel="stylesheet" type="text/css" href="local.css" />
<xsl:copy-of select="//head-items/*" />
</head>
<body>
<table cellspacing="0">
  <tr> 
    <td colspan="2" id="banner"> 
     <img src="//www.clubshop.com/images/headers/ag_cc-clubshop-1000x138.png"  width="1000" height="107" border="0" alt="Affinity Group Control Center"/>
<div style="text-align:center;">
<span class="hd"><xsl:value-of select="//lang_blocks/for" />:</span><xsl:value-of select="//memberinfo/company_name" />
<span class="hd"><xsl:value-of select="//lang_blocks/agid" />:</span><xsl:value-of select="//memberinfo/alias" />
<span class="hd"><xsl:value-of select="//lang_blocks/mid" />:</span><xsl:value-of select="//memberinfo/id" />
</div>
</td></tr>
<tr>
<td id="sidelinks">
<h4><xsl:value-of select="//nav/leader1"/></h4>
<a href="/cgi-bin/wwg" onclick="return openWin(this.href)"><xsl:value-of select="//nav/a1"/></a>

<!-- notice the nowrap below - this is to force IE to make at least this text not wrap
since it doesn't seem to want to honor either a width style on the table cell or even a width
attribute-->
<a href="/members/clubcash_details.shtml" onclick="return openWin(this.href)" style="white-space:nowrap"><xsl:value-of select="//nav/a2"/></a>

<a href="/cgi/funding.cgi" onclick="return openWin(this.href)">Clubaccount</a>

<a>
<xsl:attribute name="href">mailto:<xsl:value-of select="//upline_by_role/emailaddress" />
</xsl:attribute>
<xsl:attribute name="onclick">this.href += &quot;?subject=<xsl:value-of select="concat(//lang_blocks/title, ' ', //memberinfo/alias)" />&quot;</xsl:attribute><xsl:value-of select="//nav/a3"/></a>

<a href="https://www.clubshop.com/cgi/appx/update" onclick="return openWin(this.href)"><xsl:value-of select="//nav/a4"/></a>
<a href="/affinity/affinity_terms.xml" onclick="return openWin(this.href)"><xsl:value-of select="//nav/a5"/></a>


<xsl:choose>
	<xsl:when test="//memberinfo/id">
		<a href="https://www.clubshop.com/cgi/logout.cgi"><xsl:value-of select="//nav/logout"/></a>

	</xsl:when>
  
	<xsl:otherwise>
		<a href="https://www.clubshop.com/cs/login.shtml?destination=" onclick="this.href=this.href + location.href"><xsl:value-of select="//nav/login"/></a>
	</xsl:otherwise>
</xsl:choose>
<h4><xsl:value-of select="//nav/leader2"/></h4>
<a href="https://www.clubshop.com/cgi/member_frontline.cgi" onclick="return openWin(this.href)"><xsl:value-of select="//nav/a10"/></a>
<a href="https://www.clubshop.com/affinity/rpt1.php"><xsl:value-of select="//nav/a7"/></a>
<a href="https://www.clubshop.com/affinity/rpt2.php"><xsl:value-of select="//nav/a8"/></a>

<h4><xsl:value-of select="//nav/leader3"/></h4>
<a href="https://www.clubshop.com/affinity/banners/select.php" onclick="return openWin(this.href)"><xsl:value-of select="//nav/a9"/></a>

<h4><xsl:value-of select="//nav/leader5"/></h4>
<a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=1&amp;SearchDept.x=24&amp;SearchDept.y=10" onclick="return openWin(this.href)"><xsl:value-of select="//nav/acards"/></a>
<a href="https://www.clubshop.com/cgi/referral_cards.cgi" onclick="return openWin(this.href)"><xsl:value-of select="//nav/areferred"/></a>


</td>
    <td id="main">
<xsl:copy-of select="//main_content/*" />
</td>
</tr>

</table>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
