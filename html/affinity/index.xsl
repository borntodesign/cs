<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">
<b>
<xsl:copy-of select="*" />
<main_content>
<table><tr><td valign="top">
<img src="/images/hands.gif" height="121" width="115">
<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/title" /></xsl:attribute></img></td>
<td><h2><xsl:value-of select="//lang_blocks/h3" /></h2>
<br/><xsl:value-of select="//lang_blocks/raising" /></td>
</tr></table>

<h2><xsl:value-of select="//lang_blocks/h4"/></h2>
<p><xsl:value-of select="//lang_blocks/pjun10"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10a"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10b"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10c"/> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/pjun10d" /></b></p>

<p><xsl:value-of select="//lang_blocks/pjun10e" /> <xsl:text> </xsl:text><a href="http://www.clubshop.com/" target="_blank">www.clubshop.com</a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pjun10f" /> </p>

<p><xsl:value-of select="//lang_blocks/pjun10g" /></p>

<h2><xsl:value-of select="//lang_blocks/pjun10h" /></h2>

<p><xsl:value-of select="//lang_blocks/pjun10i" /></p>
<p><xsl:value-of select="//lang_blocks/pjun10j" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/pjun10k" /></b></p>

<p><xsl:value-of select="//lang_blocks/pjun10l" /></p>

<p><xsl:value-of select="//lang_blocks/pjun10m" /></p>

<p><xsl:value-of select="//lang_blocks/pjun10n" /></p>

<!--<ol>
<xsl:for-each select = "//lang_blocks/l1/*">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ol>-->

<!--<h2><xsl:value-of select="//lang_blocks/h5" /></h2>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2-1" /><b> &quot;<xsl:value-of select="//lang_blocks/p2-2" />&quot; </b>
<xsl:value-of select="//lang_blocks/p2-3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p3" /></b></p>-->
</main_content></b>
</xsl:template>
</xsl:stylesheet>