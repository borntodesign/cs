﻿<?xml version="1.0" encoding="utf-8"?>
<lang_blocks>
<title>Affinity Group</title>
<for>Voor</for>
<agid>AG ID</agid>
<mid>Membership ID</mid>
<h3>Non-Profit Organisatie Fondsenwerving</h3>
<raising>Fundraisers organiseren en fondsen werven om een non-profit organisatie te steunen kan soms moeilijk en tijdrovend zijn. Met het ClubShop Affinity Group programma kan een vereniging zonder winstoogmerk gemakkelijk en op voordurende basis geld inzamelen.</raising>

<h4>Hieronder de nieuwe manier van fondsenwerven</h4>
<l1>
<p1>U post heel eenvoudig de ClubShop.com links en banners die wij u bieden (waar het ID nummer van uw organisatie is ingebed) op uw website, nieuwsbrieven, bulletins, brochures, enz. - dit is alles wat u hoeft te doen!</p1>
<p2>Wanneer iemand op de link of banner klikt, of uw ClubShop.com webpagina bezoekt en zich inschrijft voor een gratis lidmaatschap in onze kopersclub, dan wordt uw organisatie voor die verwijzingen beloond.</p2>
<p3>De leden die uw organisatie verwees, verdienen op al hun aankopen "Reward punten" (die verzilverbaar zijn voor ClubCashs of Gift Certificaten) EN wij geven een percentage van iedere aankoop terug aan uw organisatie in de vorm van een maandelijkse donatie. Het percentage van die donaties zullen variëren, maar schommelen normaliter rond 2% en kunnen oplopen tot 6%, dit hangt af van de commissie die we op die winkel ontvangen.</p3>
<p4>Informatie over aankopen, retours en punten is terug te vinden op een online rapport en punten worden geaccumuleerd op maandelijkse basis, om in de vorm van een donatie aan uw organisatie terugbetaald te worden, 30 dagen na de opgegeven maand om rekening te kunnen houden met eventuele terugzendingen.</p4>
<p5>Leden kunnen ook opteren om de cashwaarde van hun verzilverbare Reward punten te doneren aan uw organisatie. Deze donaties worden ook op maandelijkse basis doorgestuurd.</p5>
</l1>

<h5>Zo gaat u van start</h5>
<p1>Klik op de Affinity Group Banner Links (link aan de linkerzijde) en post de link of banner advertentie (met uw embedded ID nummer) op uw website, nieuwsbrieven, bulletins, brochures, enz. Deze Affinity Group banner en link leidt de mensen rechtsreeks naar www.ClubShop.com en uw organisatie wordt beloond voor de verwijzingen.</p1>
<p2-1 description="The first part of sentence p2">Zeg het voort! Help mensen te </p2-1>
<p2-2 description="The second part of sentence p2. This section will be encapsulated in quotes and bolded.">Shop and Save the ClubShop Way</p2-2>
<p2-3 description="The last section of sentence p2">om waardevolle ClubShop Reward punten te verdienen op hun online aankopen, en waarbij u hen tegelijk de mogelijkheid geeft om een percentage van al hun ClubShop Mall aankopen te doneren aan uw organisatie. Start nu door uw Affinity Group banner link op uw website en/of nieuwsbrief te plaatsen en beleef vanaf vandaag fondsenwerving op een nieuwe manier!</p2-3>
<p3>Het is eenvoudig en gemakkelijk om fondsen te werven met ons Affinity Group programma!</p3>

<nav>
<leader1>Belangrijke Links</leader1>
<a1>ClubShop Mall</a1>
<a2>ClubCash centrum</a2>
<a3>Hulp vragen</a3>
<a4>Update profiel</a4>
<a5>Affinity Group Voorwaarden</a5>
<logout>Uitloggen</logout>
<login>Log In</login>
<leader2>Affinity Group rapporten</leader2>
<a10>Membership List</a10>
<a7>Leden Transaction Rapport</a7>
<a8>Persoonlijk Transaction Rapport</a8>

<leader3>Fondsenwerving</leader3>
<a9>Affinity Group Banner Links</a9>

<leader5>ClubShop Rewards Cards</leader5>
<acards>Purchase Cards</acards>
<areferred new="2/7/11">Referred Cards</areferred>
</nav>

<rpt description="these nodes are used in the 'transaction reports'">
<amount>Bedrag</amount>
<donation>Donation Credit</donation>
<alias>ID</alias>
<name>Naam</name>
<select-period>Maand Selecteer</select-period>
<go description="The label on the submit button next to the month range drop-down control in the 'member transaction report'.">Go</go>
<no-results>Sorry, there is no activity recorded.</no-results>
<vendor-name>Handelaren Naam</vendor-name>
<purchase-date>Datum aankoop</purchase-date>
<reward-points>ClubCash</reward-points>
<totals>Totaal</totals>
<ag-blurb1>Your Donation Credits on personal purchases made by your organization are credited as ClubCash</ag-blurb1>
</rpt>
</lang_blocks>