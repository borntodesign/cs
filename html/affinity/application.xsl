<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
<title>Affinity Group Application</title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/get_cookie_by_name.js"></script>
<script type="text/javascript"><xsl:comment>
function setReferral(){
	var refspid = get_cookie_by_name('refspid');
	if (refspid){
		document.forms[0].referral.value = refspid;
		//document.getElementById('referralRow').style.display = 'none';
	}
}
</xsl:comment></script>
<style type="text/css">
body {background-color: #003399;
}
table{background-color: #FFFFFF;
}
td{font-family: Verdana, Arial, sans-serif;
font-size: small;
}


</style>

</head>
<body onload="setReferral()">

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="12" valign="top">
<img src="/images/headers/affinityapp-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" /></td>
</tr>


<tr>
<td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>
<td colspan="8" bgcolor="#FFFFFF">
<div align="left"><span class="Orange_Large_11"><xsl:value-of select="//lang_blocks/p1"/></span></div>



<form action="/cgi/FormMail.cgi" method="post">

        <table width="90%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
         <tr> 
            <td>
                <input name="recipient" value="support1@dhs-club.com" type="hidden"/>
				<input name="email" value="apache@clubshop.com" type="hidden"/>
                <input name="required" value="agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,agemailaddress,referral,password,agreement" type="hidden"/>
                <input name="sort" value="order:agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,cell,skype,fax,agemailaddress,url,referral,password,agreement" type="hidden"/>
                <input name="redirect" value="http://www.clubshop.com/affinity/thankyou.html" type="hidden"/>
                <input name="subject" value="Affinity Group Application" type="hidden"/>

                <input name="env_report" value="HTTP_USER_AGENT" type="hidden"/>
              
              
              
             <table width="80%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p2"/></td>
                  <td bgcolor="#FFFFFF"> <input name="agname" id="agname" size="50" type="text"/> 
                  </td>
                </tr>

                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"> <p><xsl:value-of select="//lang_blocks/p3"/></p></td>
                  <td bgcolor="#FFFFFF"> <input name="firstname" id="firstname" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p4"/></td>
                  <td bgcolor="#FFFFFF"><input name="lastname" id="lastname" size="50" type="text"/> 
                  </td>

                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p5"/></td>
                  <td bgcolor="#FFFFFF"><input name="emailaddress" id="emailaddress" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" bgcolor="#FFFFFF" class="style12"><u><xsl:value-of select="//lang_blocks/p22"/></u></td>
                </tr>

                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p6"/></td>
                  <td bgcolor="#FFFFFF"> <input name="address" id="address" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p7"/></td>
                  <td bgcolor="#FFFFFF"> <input name="city" id="city" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p8"/></td>
                  <td bgcolor="#FFFFFF"> <input name="state" id="state" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p9"/></td>
                  <td bgcolor="#FFFFFF"> <input name="postal" id="postal" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p10"/></td>
                  <td bgcolor="#FFFFFF"><input name="country" id="country" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p11"/></td>
                  <td bgcolor="#FFFFFF"><input name="phone" id="phone" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p12"/></td>
                  <td bgcolor="#FFFFFF"><input name="cell" id="cell" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p13"/></td>
                  <td bgcolor="#FFFFFF"><input name="skype" id="skype" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p14"/></td>
                  <td bgcolor="#FFFFFF"><input name="fax" id="fax" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p15"/></td>
                  <td bgcolor="#FFFFFF"><input name="agemailaddress" id="agemailaddress" size="50" type="text"/></td>
                </tr>

                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p16"/></td>
                  <td bgcolor="#FFFFFF"><input name="url" id="url" size="50" type="text"/></td>
                </tr>
                <tr id="referralRow"> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p17"/></td>
                  <td bgcolor="#FFFFFF"><input name="referral" id="referral" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p18"/></td>
                  <td bgcolor="#FFFFFF"><input name="password" id="password" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td bgcolor="#FFFFFF" class="Text_Content_bold"><p><xsl:value-of select="//lang_blocks/p19"/><xsl:text> </xsl:text><a href="/affinity/affinity_terms.xml" target="_blank" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p20"/></a> </p>
                    <p>(<a href="http://www.clubshop.com/privacy.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a>)</p></td>
                  <td bgcolor="#FFFFFF"> <input name="agreement" value="Yes, I have read and agree to the Affinity Group Terms of Agreement" id="agreement" type="checkbox"/></td>

                </tr>
              </table>
                    <div align="center">
                      <input type="submit" name="Submit" value="Submit"/>
                      <input type="reset" name="Reset" value="Reset"/>
                    </div>
                    </td></tr></table>
            </form>





</td>
 <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
 </tr>
 <tr> <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
 <tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>
 <tr>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 </tr></table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>
