********* ********* ********* ********* ********* 
1.0 SAMPLE FILES	
********* ********* ********* ********* *********

Calendar.htm	Standard implementation of the calendar via IFrame
Theme.htm	Standard implementation plus theme selector
NoIFrame.htm	Implementation with IFrame

********* ********* ********* ********* ********* 
2.0 DOCUMENTATION
********* ********* ********* ********* *********

The ScriptCalendar installation documentation may be found at
http://www.scriptcalendar.com/dhtmlcal/cainst.asp

The ScriptCalendar online Frequently Asked Questions may be found at
http://www.scriptcalendar.com/dhtmlcal/cafaq.asp

The ScriptCalendar class property documentation may be found at
http://www.scriptcalendar.com/dhtmlcal/caprop.asp

********* ********* ********* ********* ********* 
3.0 NOTES
********* ********* ********* ********* *********

The FAQ is the most useful source of information.  Please visit it
for questions regarding the software.

********* ********* ********* ********* ********* 
4.0 EDITOR
********* ********* ********* ********* *********

The event editor can be found in the "sccomponents" folder.

