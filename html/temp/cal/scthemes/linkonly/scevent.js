// ********* ********* ********* ********* ********* ********* ********* ********* *********
// Define Events
// call the fscEvent function
// 
// #  PARMS		DATA TYPE	DESCRIPTION
// 1  m			number		2 digit month (1=jan, 2=feb, 3=mar,... 12=dec)
// 2  d			number		2 digit day
// 3  y			number		4 digit year
// 4  text		date		HTML event text
// 5  link		string		URL for popup window
// 6  style		string		CSS class for the event (in-line style is invalid)
// 8  tooltip		string		text for hover over tooltip
// 7  script		string		javascript to execute during onMouseDown
// ********* ********* ********* ********* ********* ********* ********* ********* *********

/*

all events are handled in the scspcevt.js file

*/
