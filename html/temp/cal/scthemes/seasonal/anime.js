var m_arrAnimeObjects = new Array();

// ********* ********* ********* ********* *********
function fntAnimeInitialize() {
	if (window.addEventListener) {
		window.addEventListener("load", fntAnimeStart, false);
	} else if (window.attachEvent) {
		window.attachEvent("onload", fntAnimeStart);
	}
}

// ********* ********* ********* ********* *********
function fntAnimeStart() {
	var objAnime;
	
	for (i=0; i<m_intAnimeNumberOfObjects; i++) {
		objAnime = new AnimeObject(i+1);
		objAnime.create();
		m_arrAnimeObjects[m_arrAnimeObjects.length] = objAnime;
	}
	
	setTimeout(fntAnimeMain, m_intAnimeTimerInterval);
}

// ********* ********* ********* ********* *********
function fntAnimeMain() {
	var objAnime;
	for (i=0; i<m_arrAnimeObjects.length; i++) {
		objAnime = m_arrAnimeObjects[i];
		objAnime.move();
	}

	setTimeout(fntAnimeMain, m_intAnimeTimerInterval);
}

// ********* ********* ********* ********* *********
// ANIME OBJECT & FUNCTIONS
// ********* ********* ********* ********* *********

// ********* ********* ********* ********* *********
function AnimeObject(intIndex)
{
	this.index = intIndex;
	this.id = "anime" + intIndex;
	this.name = "anime" + intIndex;

	this.imageNumber = 0;
	this.size = m_intAnimeImageSize;
	this.top = 0;
	this.left = 0;
	this.width = this.size;
	this.height = this.size;

	this.fall = (this.size == 1)?Math.round(2 + Math.random() * 2): Math.round(3 + Math.random() * 2);
	this.currStep = 0;
	this.step = (this.size == 1)?0.05 + Math.random() * 0.1 : 0.05 + Math.random() * 0.05;
	
	this.parentElement = m_objAnimeArea;
	//fntShowProperties(m_objAnimeArea);
	this.parentTop = m_objAnimeArea.offsetTop;
	this.parentLeft = m_objAnimeArea.offsetLeft;
	this.parentHeight = m_objAnimeArea.clientHeight;
	this.parentWidth = m_objAnimeArea.clientWidth;
}

// ********* ********* ********* ********* *********
AnimeObject.prototype.create = function()
{
	// create the actual DIV element
	var objElement = fntAppendNode(this.parentElement, "div", "");	
	this.element = objElement;
	fntAppendAttribute(objElement, "name", this.name);
	fntAppendAttribute(objElement, "id", this.name);

	this.element.style.position = "absolute";
	this.top = Math.round(Math.random() * this.parentHeight) + this.parentTop;
	this.left = Math.round(Math.random() * this.parentWidth) + this.parentLeft;

	var intRandom = (Math.round(1 + Math.random() * (m_arrAnimeImageUrl.length-1)) - 1);
	this.imageNumber = intRandom;
	this.image = m_arrAnimeImageUrl[this.imageNumber];
	
	var objImage = fntAppendNode(this.element, "img", "");	
	fntAppendAttribute(objImage, "src", this.image);
	this.imageElement = objImage;

	this.element.style.height = this.height + "px";
	this.element.style.width = this.width + "px";

	this.applyStyle();
}

// ********* ********* ********* ********* *********
AnimeObject.prototype.move = function()
{
	//document.getElementById("txtDebug").value = "this.step: " + this.step + " this.currStep: " + this.currStep + " Math.cos: " + Math.cos(this.currStep) + "\r\n";

	var dy = this.fall;
	var	dx = this.fall * Math.cos(this.currStep);

	this.top += dy;
	this.left += dx; 

	//document.getElementById("txtDebug").value = "this.top: " + this.top + " this.left: " + parseInt(this.left) + " dx:" + parseInt(dx) + " dy:" + parseInt(dy) + "\r\n";

	if (this.left >= this.parentWidth + this.parentLeft || this.top >= this.parentHeight + this.parentTop) {
		var intRandom = (Math.round(1 + Math.random() * (m_arrAnimeImageUrl.length-1)) - 1);
		this.imageNumber = intRandom;
		this.image = m_arrAnimeImageUrl[this.imageNumber];
		this.element.removeChild(this.imageElement);
		var objImage = fntAppendNode(this.element, "img", "");	
		fntAppendAttribute(objImage, "src", this.image);
		this.imageElement = objImage;

		//this.element.style.backgroundImage = "url('" + this.image + "')";
		this.element.innerHtml = "<img src='" + this.image + "'/>";

		this.top = -10 + this.parentTop;
		this.left = Math.round(Math.random() * this.parentWidth) + this.parentLeft;
		this.fall = (this.size == 1)?Math.round(2 + Math.random() * 2): Math.round(3 + Math.random() * 2);
		this.step = (this.size == 1)?0.05 + Math.random() * 0.1 : 0.05 + Math.random() * 0.05 ;
	}

	this.currStep += this.step;
	this.applyStyle();
}

// ********* ********* ********* ********* *********
AnimeObject.prototype.applyStyle = function() 
{
	var objStyle = this.element.style;
	objStyle.top = this.top + "px";
	objStyle.left = this.left + "px";
}

// ********* ********* ********* ********* *********
// NODE FUNCTIONS
// ********* ********* ********* ********* *********

// ********* ********* ********* ********* *********
function fntAppendNode(objParent, strName, strValue) 
{
	var objDoc = objParent.ownerDocument;
	if (!objDoc) objDoc = objParent;
	var objNode = objDoc.createElement(strName);
	if (strValue) {
		var objNodeText = objDoc.createTextNode(strValue);
		objNode.appendChild(objNodeText);
	};
	objParent.appendChild(objNode);
	return objNode;
};

// ********* ********* ********* ********* *********
function fntAppendAttribute(objParent, strName, strValue) 
{
	var objDoc = objParent.ownerDocument;
	if (!objDoc) objDoc = objParent;
	var objNode = objDoc.createAttribute(strName);
	if (strValue) objNode.nodeValue = strValue;
	objParent.attributes.setNamedItem(objNode);
};


// ********* ********* ********* ********* ********* ********* ********* ********* ********* *********
// * debug functions
// ********* ********* ********* ********* ********* ********* ********* ********* ********* *********

// ********* ********* ********* ********* *********
function fntOutput(text) {
	var obj; 
	if (document.createElement && (obj = document.createElement('textarea'))) {
		fntAppendAttribute(obj, "rows", "5");
		fntAppendAttribute(obj, "cols", "80");
		obj.appendChild(document.createTextNode(text));
		document.body.appendChild(obj);
	};
};

// ********* ********* ********* ********* *********
function fntShowProperties(obj) {
	var i;
	var strOutput;
	var strTerm;
	var strData;
	var strSpace;
	var lngCols;
	var lngMaxLen = 100;
	
	// initialize number of columns
	lngCols=3;
	if (document.layers) lngCols=3;

	// initialize an empty string
	strSpace = "                                        ";
	
	strOutput = obj.name + "\n\n";
	i=1;
	for (var prop in obj) {
		strTerm = "\n";
		if (i==lngCols) strTerm = "\n";

		i=i+1;
		if (i>lngCols) i=1;	

		strData = "." + prop + " = " + obj[prop];
		if (strData.length<lngMaxLen) strData += strSpace.substring(0, lngMaxLen-strData.length);
		if (strData.length>lngMaxLen) strData = strData.substring(0, lngMaxLen);

		strOutput += strData + strTerm;
	};	
	fntOutput(strOutput);
	return true;
};