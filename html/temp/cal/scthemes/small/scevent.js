// ********* ********* ********* ********* ********* ********* ********* ********* *********
// Define Events
// call the fscEvent function
// 
// #  PARMS		DATA TYPE	DESCRIPTION
// 1  m			number		2 digit month (1=jan, 2=feb, 3=mar,... 12=dec)
// 2  d			number		2 digit day
// 3  y			number		4 digit year
// 4  text		date		HTML event text
// 5  link		string		URL for popup window
// 6  style		string		CSS class for the event (in-line style is invalid)
// 8  tooltip		string		text for hover over tooltip
// 7  script		string		javascript to execute during onMouseDown
// ********* ********* ********* ********* ********* ********* ********* ********* *********


fscEvent( null, -1, null, " ", null, "scEventBlack", null, null );
fscEvent( null, 0, null, " ", null, "scEventBlack", null, null );
		
fscEvent( null, 1, null, " ", null, "scEventRed", null, null );
fscEvent( null, 2, null, " ", null, "scEventRed", null, null );

fscEvent( null, 4, null, " ", null, "scEventOrange", null, null );
fscEvent( null, 5, null, " ", null, "scEventOrange", null, null );

fscEvent( null, 7, null, " ", null, "scEventYellow", null, null );
fscEvent( null, 8, null, " ", null, "scEventYellow", null, null );

fscEvent( null, 10, null, " ", null, "scEventGreen", null, null );
fscEvent( null, 11, null, " ", null, "scEventGreen", null, null );

fscEvent( null, 13, null, " ", null, "scEventBlue", null, null );
fscEvent( null, 14, null, " ", null, "scEventBlue", null, null );

fscEvent( null, 16, null, " ", null, "scEventPurple", null, null );
fscEvent( null, 17, null, " ", null, "scEventPurple", null, null );			

fscEvent( null, 19, null, " ", null, "scEventRed", null, null );
fscEvent( null, 20, null, " ", null, "scEventRed", null, null );

fscEvent( null, 22, null, " ", null, "scEventOrange", null, null );
fscEvent( null, 23, null, " ", null, "scEventOrange", null, null );

fscEvent( null, 25, null, " ", null, "scEventYellow", null, null );
fscEvent( null, 26, null, " ", null, "scEventYellow", null, null );

fscEvent( null, 28, null, " ", null, "scEventGreen", null, null );
fscEvent( null, 29, null, " ", null, "scEventGreen", null, null );

