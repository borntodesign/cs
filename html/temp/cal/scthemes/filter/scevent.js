
// ********* ********* ********* ********* ********* ********* ********* ********* *********
// Define Events
// call the fscEvent function
// 
// #  PARMS		DATA TYPE	DESCRIPTION
// 1  m			number		2 digit month (1=jan, 2=feb, 3=mar,... 12=dec)
// 2  d			number		2 digit day
// 3  y			number		4 digit year
// 4  text		date		HTML event text
// 5  link		string		URL for popup window
// 6  style		string		CSS class for the event (in-line style is invalid)
// 7  tooltip		string		text for hover over tooltip
// 8  script		string		javascript to execute during onMouseDown
// 9  filter		string		keyword to allow users to filter events
// ********* ********* ********* ********* ********* ********* ********* ********* *********

			
fscEvent( null, 0, null, "Dead Date Cell", null, "scEventBlack", null, null, "early month");
		
fscEvent( null, 1, null, "<span style='font:bold 10pt verdana;'>First Day</span> of the Month", null, null, null, null, "early month");

fscEvent( null, 2, null, "<span style='font:bold 10pt verdana;'>Second Day</span> of the Month", null, null, null, null, "early month");
		
fscEvent( null, 4, null, "<center><img src='../../scimages/earth.gif' border='0'/><br/>Image (earth)</center>", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#section7", null, null, null, "early month");
		
fscEvent( null, 5, null, "<center><img src='../../scimages/monitor.gif' border='0'/><br/>Image (monitor)</center>", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#section7", null, null, null, "early month");
		
fscEvent( null, 7, null, "Pop-up window event example", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#Popup", null, null, null, "early month");
		
fscEvent( null, 8, null, "Pop-up window event example", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#Popup", null, null, null, "early month");
		
fscEvent( null, 10, null, "A license key is required to display events past the 10th", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#Demo", "eventYellow", null, null, "early month");
		
fscEvent( null, 11, null, "Each event uses Every Month feature", "http://www.scriptcalendar.com/dhtmlcal/cafaq.asp#NullMonth", null, null, null, "mid month");
		
fscEvent( null, 13, null, "Purchase the calendar", "http://www.scriptcalendar.com/dhtmlcal/capurch.asp", null, null, null, "mid month");
		
fscEvent( null, 14, null, "Purchase the calendar", "http://www.scriptcalendar.com/dhtmlcal/capurch.asp", null, null, null, "mid month");
		
fscEvent( null, 16, null, "<a href='http://www.scriptcalendar.com/dhtmlcal/cafaq.asp'>hyperlink</a> example", null, null, null, null, "mid month");
		
fscEvent( null, 17, null, "<a href='http://www.scriptcalendar.com/dhtmlcal/cafaq.asp'>hyperlink</a> example", null, null, null, null, "mid month");
		
fscEvent( null, 19, null, "Custom color event example", null, "scEventRed", null, null, "mid month");
		
fscEvent( null, 20, null, "Custom color event example", null, "scEventOrange", null, null, "mid month");
		
fscEvent( null, 22, null, "Tooltip example", null, null, "this is a tooltip", null, "late month");
		
fscEvent( null, 23, null, "Tooltip example", null, null, "this is a tooltip", null, "late month");
		
fscEvent( null, 25, null, "Script example", null, null, null, "alert('javascript alert')", "late month");
		
fscEvent( null, 26, null, "Script example", null, null, null, "alert('javascript alert')", "late month");
		
fscEvent( null, 28, null, "multiple events", null, "scEventRed", null, null, "late month");
		
fscEvent( null, 28, null, "multiple events", null, "scEventOrange", null, null, "late month");
		
fscEvent( null, 28, null, "multiple events", null, "scEventYellow", null, null, "late month");
		
fscEvent( null, 28, null, "multiple events", null, "scEventGreen", null, null, "late month");
		
fscEvent( null, 28, null, "multiple events", null, "scEventBlue", null, null, "late month");
		
fscEvent( null, 29, null, "multiple events", null, "scEventRed", null, null, "late month");;
		
fscEvent( null, 29, null, "multiple events", null, "scEventOrange", null, null, "late month");
		
fscEvent( null, 29, null, "multiple events", null, "scEventYellow", null, null, "late month");
		
fscEvent( null, 29, null, "multiple events", null, "scEventGreen", null, null, "late month");
		
fscEvent( null, 29, null, "multiple events", null, "scEventBlue", null, null, "late month");
	
