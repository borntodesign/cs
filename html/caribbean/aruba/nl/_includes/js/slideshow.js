
/***********************************************
* Ultimate Fade In Slideshow v2.0- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/


var mygallery=new fadeSlideShow({
	wrapperid: "fadeshow1", //ID of blank DIV on page to house Slideshow
	dimensions: [946, 174], //width/height of gallery in pixels. Should reflect dimensions of largest image
	imagearray: [
		["../common/images/slides/clubshop_rewards_aruba_1.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_2.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_3.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_4.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_5.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_6.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_7.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"],
		["../common/images/slides/clubshop_rewards_aruba_8.png", "", "_parent", "CLUBSHOP REWARDS ARUBA - OFFLINE & ONLINE BESPAREN, OVERAL TER WERELD!"] //<--no trailing comma after very last image element!
	],
	displaymode: {type:'auto', pause:5000, cycles:0, wraparound:false},
	persist: false, //remember last viewed slide and recall within same session?
	fadeduration: 500, //transition duration (milliseconds)
	descreveal: "ondemand",
	togglerid: ""
})
