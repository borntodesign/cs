<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>


   <xsl:template match="/">

         <html><head>
            <title>Pre-Registered Merchants</title>

<style type="text/css">

				h1 {
					font-family: Arial, Verdana, sans-serif;
					font-size: 24px;
					color: #336699;
					font-weight: bold;
				
				}
				
				h2 {
					font-family: Arial, Verdana, sans-serif;
					font-size: 18px;
					color: #336699;
				
				}
				td, th {
					font-family: Verdana, Arial, sans-serif;
					font-size: smaller;
					font-weight:bold;
					border:0;
				}
				
				p {
					font-weight:normal;
					border:0;
				}


th{
	text-align: left;
}

td.a {
	background-color:#FFAF37;
	text-align: center;
}
th {
	background-color:#FFF0D9;
}

td.b {
	background-color:#FFF0D9;
	text-align: center;
}
td.c {
	background-color:#E8EFF7;

}
td.d {
	background-color:#FFCF88;
}

tr.store_type {
	font-weight:bold;
	background-color:#336699;
	color:#FFFFFF;
}
tr.grey {
	font-weight:bold;
	background-color:#F0F0F0;
	/* text-align: center; */
}
tr.light_grey {
	font-weight:bold;
	background-color:#F9F9F9;
	/* text-align: center; */
}

pre {
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	font-weight: 100;
}

</style>

</head>
<body>

	<div align="center" name="heading">
		<h1>
			<xsl:value-of select="//lang_blocks/heading" />
		</h1>
		<img src="/clubrewards/images/cr_card.gif" height="148" width="226" alt="Club Rewards Card"/>
		<br /><br />
	</div>
	<table align="center" width="90%"><tbody><tr><td class="a"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr></tbody></table>
	<br />
	<br /><table align="center" border="0" width="90%"><tbody><tr><td class="basic">
		<p><xsl:value-of select="//lang_blocks/p1"/></p>
		<hr />
		
<p>

			
<pre wrap="true"><xsl:value-of select="//lang_blocks/message_salutation"/>,<xsl:text> 
			
</xsl:text>
			<xsl:value-of select="//lang_blocks/message_p1"/><xsl:text> 
			
</xsl:text>
			<xsl:value-of select="//lang_blocks/message_p2"/><xsl:text> 
			
</xsl:text>
			<xsl:value-of select="//lang_blocks/message_p3"/><xsl:text> 
			
</xsl:text>	
			<xsl:value-of select="//lang_blocks/complimentary_closing"/>,<xsl:text>
</xsl:text>
			<xsl:value-of select="//lang_blocks/author"/><xsl:text>
</xsl:text>
			<xsl:value-of select="//lang_blocks/title"/><xsl:text>
</xsl:text>
			<xsl:value-of select="//lang_blocks/email"/><xsl:text>			
</xsl:text>	
			</pre>
		</p>
	</td>
	</tr></tbody></table>
</body>
</html>


</xsl:template>
</xsl:stylesheet>