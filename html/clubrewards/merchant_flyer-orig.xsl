<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html" /> 






   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/title"/></title>
	
<style type="text/css">

	body{
		background-color: #FFFFFF;
	}
	
	body, p, li, h2, h1, td
	{
		font-family: Arial, Verdana, sans-serif;
		color: #000000;
		font-size: 14px;
	}
	
	h1{
		font-size: 28px;
	}

	h5{
		font-size: 10px;
	}
		
	table.main{
		color: #FFFFFF;
		width: 700px;
		vertical-align: top;
	}


	
	.bold, .question{
	font-weight:  bold;
}

</style>

</head>
<body>
<table class="main"><tr><td>
<div align="center">
	<img src="/clubrewards/images/clubrewards_card.gif" alt="Club Rewards Card" height="148" width="226" />
	<br />
	<br />

</div>
<div>
	<p>
		<xsl:value-of select="//lang_blocks/p1"/>
	</p>
	
	<p>
		<xsl:value-of select="//lang_blocks/p3"/>
	</p>
	
	<p>
		<xsl:value-of select="//lang_blocks/p4"/>
	</p>
	
	
	<form id="form1" name="form1" method="post" action="/cgi/potential_merchant_flyer.cgi" enctype="application/x-www-form-urlencoded">
	  <label>
	  <input type="text" name="location" id="location" />
	  </label>
	  <p><xsl:text> </xsl:text></p>
	  <p>
	    <label>
	    <input type="submit" name="button" id="button">
	    	<xsl:attribute name="value">
				<xsl:value-of select="//lang_blocks/submit_button" />
			</xsl:attribute>

		</input>
	    
	    
	    </label>
	  </p>
	</form>
</div>
<p><xsl:text> </xsl:text></p>
</td></tr></table>
</body>
</html>


</xsl:template>
</xsl:stylesheet>
