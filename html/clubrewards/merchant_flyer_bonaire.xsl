<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubRewards Merchant Flyer</title>

<style type="text/css">
background{color:#FFFFFF;}

table.main{
	color: #FFFFFF;
	width: 700px;
	vertical-align: top;

}
h2{font-family: Arial, Verdana, sans-serif;
font-size: 40px;
color:#000000;
}
h5{font-family: Arial, Verdana, sans-serif;
font-size: 20px;
color:#000000;
}

p {font-family: Arial, Verdana, sans-serif;
font-size: smaller;
color:#000000;
}

li{font-family: Arial, Verdana, sans-serif;
font-size: smaller;
color:#000000;
}

li.a{font-family: Arial, Verdana, sans-serif;
font-size: smaller;
color:#000000;
font-weight:  bold;
padding-bottom: 3px;
list-style-type: none;}

</style>

</head>
<body>
<table class="main">
<tr><td>

<div align="center"><img src="/clubrewards/images/clubrewards_card.gif" height="148" width="226" alt="ClubRewards Card"/></div>

<div align="center"><h2><xsl:value-of select="//lang_blocks/p1"/></h2></div>

<p><b><xsl:value-of select="//lang_blocks/p2"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p4"/></p>

<p><b><xsl:value-of select="//lang_blocks/p5"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><b><xsl:value-of select="//lang_blocks/p8"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p9"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p11"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<li><xsl:value-of select="//lang_blocks/p14"/></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
</ul>

<p><b><xsl:value-of select="//lang_blocks/p16"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p17"/></p>
<p><b><xsl:value-of select="//lang_blocks/p18"/></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p19"/></p>

<p><xsl:value-of select="//lang_blocks/p20"/></p>
<ul>
<li class="a"><xsl:value-of select="//lang_blocks/p21"/></li>
<li class="a"><xsl:value-of select="//lang_blocks/p22"/></li>
<li class="a"><xsl:value-of select="//lang_blocks/p23"/></li>
</ul>

<div align="center"><h5><xsl:value-of select="//lang_blocks/p24"/></h5></div>


</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>