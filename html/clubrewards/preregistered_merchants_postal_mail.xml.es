<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="preregistered_merchants_postal_mail.xsl" ?>
<lang_blocks>
	<heading>Modelo de Carta dirigida al Comerciante Pre-registrado</heading>
	<p1>La carta que te mostramos a continuación ha sido aprobado para que la puedas enviar mediante correo postal a posibles Comerciantes ClubRewards.  No envíes correspondencia a comerciantes que se encuentren fuera de la localidad para la cual has proyectado una campaña de marketing. Copia y pega el texto siguiente, adecua el mismo  antes de imprimirlo y enviarlo.  Incluye un panfleto impreso en tu correspondencia.</p1>
	<message_salutation>Estimado Propietario o Administrador (utiliza su nombre sí lo sabes)</message_salutation>
	<message_p1>¿Te gustaría obtener nuevos clientes, incrementar el promedio de tus ventas por cliente y lograr que clientes fieles efectúen reiteradas compras en tu negocio, sin tener que gastar dinero anticipadamente en publicidad?  Pronto podrás, gracias a la nueva Tarjeta del Programa de Incentivo al Cliente ClubRewards.</message_p1>
	<message_p2>Por favor revisa la información adjunta  y llámame al &lt;tu número de teléfono&gt;  para poder fijar una cita y así explicarte mejor como funciona el programa.</message_p2>
	<message_p3>¡Espero ayudarte a incrementar tus ventas!</message_p3>
	<complimentary_closing>Muy atentamente</complimentary_closing>
	<author>Tu nombre</author>
	<title>Consultor de Marketing del ClubRewards</title>
	<email>Tu dirección de correo electrónico</email>
</lang_blocks>

