<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="preregistered_merchants_cover_letter.xsl" ?>
<lang_blocks>
	<heading>Modelo de Carta de Presentación dirigida al Comerciante Pre-registrado</heading>
	<p1>La carta que te mostramos a continuación ha sido aprobado para que la puedas entregar a posibles Comerciantes ClubRewards LOCALES.   No distribuyas cartas a Comerciantes que se encuentren fuera de la localidad donde planeas efectuar tu campaña de marketing.  Copia y Pega el texto que sigue dentro de un documento, antes de imprimirlo y distribuirlo.  Incluye folletos impresos locales a esta carta de presentación.</p1>
	<message_salutation>Estimado Propietario o Administrador</message_salutation>
	<message_p1>¿Te gustaría obtener nuevos clientes, incrementar el promedio de tus ventas por cliente y lograr que clientes fieles efectúen reiteradas compras en tu negocio, sin tener que gastar dinero anticipadamente en publicidad?  Pronto podrás, gracias a la nueva Tarjeta del Programa de Incentivo al Cliente ClubRewards.</message_p1>
	<message_p2>Por favor revisa la información que te incluimos, y llámame al &lt;tu teléfono&gt; para poder fijar una cita y así explicarte mejor como funciona el programa.</message_p2>
	<message_p3>¡Espero poder ayudarte a incrementar tus ventas!</message_p3>
	<complimentary_closing>Muy atentamente</complimentary_closing>
	<author>Tu nombre</author>
	<title>Consultor de Marketing del ClubRewards</title>
	<email>Tu dirección de correo electrónico</email>
</lang_blocks>

