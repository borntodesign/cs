<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>


   <xsl:template match="/">

         <html><head>
            <title>Pre-Registered Merchants</title>

<style type="text/css">

h2 {
	font-family: Arial, Verdana, sans-serif;
	font-size: 18px;
	color: #336699;

}
p, td, th {
	font-family: Verdana, Arial, sans-serif;
	font-size: smaller;
}

td, th {
	font-weight:bold;
	border:0;
}

th{
	text-align: left;
}

td.a {
	background-color:#FFAF37;
	text-align: center;
}
th {
	background-color:#FFF0D9;
}

td.b {
	background-color:#FFF0D9;
	text-align: center;
}
td.c {
	background-color:#E8EFF7;

}
td.d {
	background-color:#FFCF88;
}

tr.store_type {
	font-weight:bold;
	background-color:#336699;
	color:#FFFFFF;
}
tr.grey {
	font-weight:bold;
	background-color:#F0F0F0;
	/* text-align: center; */
}
tr.light_grey {
	font-weight:bold;
	background-color:#F9F9F9;
	/* text-align: center; */
}


</style>

</head>
<body>

<h2><xsl:value-of select="//lang_blocks/p1"/></h2>

<div align="center"><img src="/clubrewards/images/cr_card.gif" height="148" width="226" alt="Club Rewards Card"/></div>

<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>

<p><xsl:value-of select="//lang_blocks/p4"/></p>

<div align="center"><h2><u><a href="/clubrewards/preregistered_merchants.xsp"><xsl:value-of select="//lang_blocks/p5"/></a></u></h2></div>

<table width="90%" align="center">
<tr>
<td class="a"><a href="/clubrewards/preregistered_merchants.xsp?country=AW"><xsl:value-of select="//lang_blocks/p6"/></a></td>
<td class="b"><a href="/clubrewards/preregistered_merchants.xsp?country=AN&amp;state=BON"><xsl:value-of select="//lang_blocks/p7"/></a></td>
<td class="a"><a href="/clubrewards/preregistered_merchants.xsp?country=AN&amp;state=CUR"><xsl:value-of select="//lang_blocks/p8"/></a></td>
<td class="b"><a href="/clubrewards/preregistered_merchants.xsp?country=SR"><xsl:value-of select="//lang_blocks/p9"/></a></td>
</tr>
</table>
<br />
<table width="90%" align="center" border="1">
<xsl:for-each select="//table/tr">
	<tr class="{@class}">    	
       	<xsl:for-each select="th">
        	<th><xsl:value-of select="."/></th>
    	</xsl:for-each>
    	<xsl:for-each select="td">
        	<td class="{@class}"><xsl:value-of select="."/></td>
    	</xsl:for-each>
	</tr>

</xsl:for-each>
</table>

</body></html>


</xsl:template>
</xsl:stylesheet>