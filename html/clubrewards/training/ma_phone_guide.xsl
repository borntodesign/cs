<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Merchant Pre-registration Training</title>

<style type="text/css">
body {background-color:#472684;}

p{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
}

li{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
padding-bottom: 5px;
}

li.alpha{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
padding-bottom: 5px;
list-style-type: upper-alpha;
border: 1px;
border-color: #663399;
}

table {background-color: white;
align:center;
valign: top;
border-style: outset;
border-color: #874BC2;
margin: 10px;
}

table.a{background-color: white;
align: center;
valign: top;
border-color: #874BC2;
}

tr.a{background-color: white;
align:center;
valign: top;
border-color: #874BC2;

}

p.h{font-family: Arial, Verdana, sans-serif;
font-size: 22px;
color: #582C85;
text-align: center;
font-weight: bold;
valign: top;
}
hr{height:7px;
color:#FFCC00;
}
tr.top{background-color: gold;
height: 5px;
}
tr.bottom{background-color: gold;
text-align: center;
}

tr.fill{background-color:#EBE1F4;}



</style>

</head>
<body>
<table><tr><td valign="top">

<p class="h"><xsl:value-of select="//lang_blocks/p1"/></p>
<div align="center"><img src="/clubrewards/images/phone_call.gif" height="170" width="170" alt="telephone call"/></div>
</td></tr>

<tr class="top"><td></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p2"/></p>


<table border="1">
<tr><td><xsl:value-of select="//lang_blocks/p3"/></td>  <td><xsl:value-of select="//lang_blocks/p4"/></td></tr>
<tr class="fill"><td><xsl:value-of select="//lang_blocks/p5"/></td>  <td><xsl:value-of select="//lang_blocks/p6"/></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p7"/></td>  <td><xsl:value-of select="//lang_blocks/p8"/></td></tr>
<tr class="fill"><td colspan="2"><xsl:value-of select="//lang_blocks/p9"/></td>  </tr>
<tr><td><xsl:value-of select="//lang_blocks/p10"/></td>  <td><xsl:value-of select="//lang_blocks/p11"/></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p12"/></td>  <td><xsl:value-of select="//lang_blocks/p13"/></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p14"/></td>  <td><xsl:value-of select="//lang_blocks/p15"/></td></tr>
<tr class="fill"><td colspan="2"><xsl:value-of select="//lang_blocks/p9"/></td>  </tr>
<tr><td><xsl:value-of select="//lang_blocks/p16"/></td>  <td><xsl:value-of select="//lang_blocks/p17"/></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p18"/></td>  <td><xsl:value-of select="//lang_blocks/p19"/></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p20"/></td>  <td><xsl:value-of select="//lang_blocks/p21"/></td></tr>
                             	
</table>                          	

<p><xsl:value-of select="//lang_blocks/p22"/></p>

<br/>
</td></tr>

<tr class="bottom"><td><img src="/clubrewards/images/copyright_.gif" height="19" width="173" alt="Copyright"/></td></tr>

</table>
</body></html>


</xsl:template>
</xsl:stylesheet>