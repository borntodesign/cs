<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Merchant Pre-registration Training</title>

<style type="text/css">
body {background-color:#472684;}

p{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
}

li{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
padding-bottom: 5px;
}

li.alpha{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
padding-bottom: 5px;
list-style-type: upper-alpha;
border: 1px;
border-color: #663399;
}

table {background-color: white;
align:center;
valign: top;
border-style: outset;
border-color: #874BC2;
margin: 10px;
}

p.h{font-family: Arial, Verdana, sans-serif;
font-size: 22px;
color: #582C85;
text-align: center;
font-weight: bold;
valign: top;
}
hr{height:7px;
color:#FFCC00;
}
tr.top{background-color: gold;
height: 5px;
}
tr.bottom{background-color: gold;
text-align: center;
}

.imgt_base{
	color: #000000;
	font-weight:400;
	font-size:17px;
	font-family:georgia,serif;
}
.imgt1{
	color: #000000;
}


</style>

</head>
<body>
<table><tr><td valign="top">

<p class="h"><xsl:value-of select="//lang_blocks/p1"/></p>
<div align="center"><img src="/clubrewards/images/meeting.gif" height="170" weight="170" alt="face to face"/></div></td></tr>
<tr class="top"><td></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><u><xsl:value-of select="//lang_blocks/p3"/></u></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/mall/other/apparel_gifts.shtml" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a></li>
<li><xsl:value-of select="//lang_blocks/p8"/></li>

<div style="background-image: url(/clubrewards/images/note_card.gif); background-repeat: no-repeat; width:700px; height:125px; align:center;">
<div style="text-align:left; position:relative; top:8px; height:150px; width:600px; left:8px; font-weight:400; font-size:17px; font-family:georgia,serif;">
<span class="imgt1"><xsl:value-of select="//lang_blocks/p9"/></span>
</div></div>

<p><xsl:value-of select="//lang_blocks/p10"/></p>
<p><xsl:value-of select="//lang_blocks/p10a"/></p>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<div style="background-image: url(/clubrewards/images/note_card.gif); background-repeat: no-repeat; width:700px; height:125px; align:center;">
<div style="text-align:left; position:relative; top:10px; height:150px; width:550px; left:8px; font-weight:500; font-size:17px; font-family:georgia,serif;">
<span class="imgt1"><xsl:value-of select="//lang_blocks/quote"/></span>
</div></div>
<p><xsl:value-of select="//lang_blocks/p13a"/></p>
<p><xsl:value-of select="//lang_blocks/p14"/></p>

<div style="background-image: url(/clubrewards/images/note_card_sm.gif); background-repeat: no-repeat; width:600px; height:91px; align:center;">
<div style="text-align:left; position:relative; top:10px; height:100px; width:575px; left:8px; font-weight:500; font-size:17px; font-family:georgia,serif;">
<span class="imgt1"><xsl:value-of select="//lang_blocks/p15"/></span>
</div></div>

<p><xsl:value-of select="//lang_blocks/p16"/></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>
<p><xsl:value-of select="//lang_blocks/p18"/><br/>
<b><xsl:value-of select="//lang_blocks/p19"/></b></p>

<p><xsl:value-of select="//lang_blocks/p20"/><br/>
<b><xsl:value-of select="//lang_blocks/p21"/></b></p>
<p><xsl:value-of select="//lang_blocks/p22"/></p>

<li><xsl:value-of select="//lang_blocks/p23"/></li>
<p><xsl:value-of select="//lang_blocks/p24"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<p><xsl:value-of select="//lang_blocks/p26"/></p>
<p><xsl:value-of select="//lang_blocks/p27"/></p>
<p><xsl:value-of select="//lang_blocks/p28"/></p>
<p align="center"><b><xsl:value-of select="//lang_blocks/p29"/></b></p>
</ol>
</td></tr>

<tr class="bottom"><td><img src="/clubrewards/images/copyright_.gif" height="19" width="173" alt="Copyright"/></td></tr>

</table>
</body></html>


</xsl:template>
</xsl:stylesheet>
