<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>
            
            <meta name="title" content="clubshop rewards"/>
<meta name="keywords" content="clubshop rewards, ClubCash"/>
<meta name="abstract" content="clubshop rewards. rewards points"/>
<meta http-equiv="content-language" content="en-us"/>
<meta http-equiv="content-language" content="english"/>
<meta http-equiv="content-language" content="lang_en"/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop rewards"/>
<meta name="design-development" content="carsten rieger"/>

<meta name="publisher" content="clubshop rewards"/>
<meta name="company" content="clubshop"/>
<meta name="copyright" content="copyright © 2009 clubshop rewards. all rights reserved"/>
<meta name="page-topic" content="ClubCash"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="general"/>
<meta name="revisit-after" content="7 days"/>


<link href="/clubrewards/scripts/css/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
a {
font-size: 12px;
}
a:link {
color: #224C9B;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
</style>





<script type="text/javascript">
<xsl:comment>

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i&lt;a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[i])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&amp;&amp;parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&amp;&amp;d.all) x=d.all[n]; for (i=0;!x&amp;&amp;i&lt;d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&amp;&amp;d.layers&amp;&amp;i&lt;d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x &amp;&amp; d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i&lt;(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//</xsl:comment>
</script>

            
            
    </head>        

<body>
<div>

<span class="Text_Body_Content"><span class="style12"><xsl:value-of select="//lang_blocks/p1"/></span><br />

              <br />
              <xsl:value-of select="//lang_blocks/p2"/><br />
<br />
 <xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text><span class="style25"><xsl:value-of select="//lang_blocks/p4"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p5"/> <xsl:text> </xsl:text><a href="http://www.clubshop.com" target="_blank" class="nav_blue">www.clubshop.com</a><xsl:value-of select="//lang_blocks/p6"/><br />

<br />
<img src="../images/general/dick_burke.jpg" width="94" height="156" align="left" class="image_right_padding" /><img src="../images/stills/clubshop_1.jpg" width="179" height="267" align="right" class="image_left_padding" /><xsl:value-of select="//lang_blocks/p7"/><br />
<br />
<xsl:value-of select="//lang_blocks/p8"/><br />

<br />
</span>

<span class="style5"><xsl:value-of select="//lang_blocks/p9"/></span>














</div>

</body></html>
</xsl:template>
</xsl:stylesheet>
