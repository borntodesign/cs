<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>


   <xsl:template match="/">

	<html>
		<head>
			<title><xsl:value-of select="//lang_blocks/title"/></title>

			<style type="text/css">
				h1 {
					font-family: Arial, Verdana, sans-serif;
					font-size: 24px;
					color: #336699;
					font-weight: bold;
				
				}
				
				h2 {
					font-family: Arial, Verdana, sans-serif;
					font-size: 18px;
					color: #336699;
				
				}
				td, th {
					font-family: Verdana, Arial, sans-serif;
					font-size: smaller;
					font-weight:bold;
					border:0;
				}
				
				p {
					font-weight:normal;
					border:0;
				}
				
				th{
					text-align: left;
				}
				
				td.a {
					background-color:#FFAF37;
					text-align: center;
				}
				th {
					background-color:#FFF0D9;
				}
				
				td.b {
					background-color:#FFF0D9;
					text-align: center;
				}
				td.c {
					background-color:#E8EFF7;
				
				}
				td.d {
					background-color:#FFCF88;
				}
				
				tr.store_type {
					font-weight:bold;
					background-color:#336699;
					color:#FFFFFF;
				}
				tr.grey {
					font-weight:bold;
					background-color:#F0F0F0;
					/* text-align: center; */
				}
				tr.light_grey {
					font-weight:bold;
					background-color:#F9F9F9;
					/* text-align: center; */
				}
			
			
			</style>

		</head>
		<body>
		
			<div align="center">
				<xsl:choose>
					<xsl:when test="//data/select">
						<h1><xsl:value-of select="//lang_blocks/p1"/></h1>
						<img src="/clubrewards/images/cr_card.gif" height="148" width="226" alt="Club Rewards Card" /> <br /><br />
					</xsl:when>
					<xsl:otherwise>
						<h1><xsl:value-of select="//lang_blocks/p5"/></h1>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			
			<table width="90%" align="center">
				<tr>
				<td class="a">
					<xsl:choose>
						<xsl:when test="//data/country_name">
						
							<a href="?">
								<xsl:value-of select="//lang_blocks/p1"/>
							</a>
							<xsl:text disable-output-escaping="yes"> > </xsl:text>
								<xsl:if test="//data/country_name">
									<a href="?country={//data/country}">
										<xsl:value-of select="//data/country_name"/>
									</a>
								</xsl:if>
								
								<xsl:if test="//data/state_name">
								 <xsl:text disable-output-escaping="yes"> > </xsl:text>
									<xsl:value-of select="//data/state_name"/>
								</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				</tr>
			</table>
			<br />
			
			
<xsl:choose>
	<xsl:when test="//data/select">
	<table width="90%" align="center" border="0">
		<tr>
			<td class="basic">
				
				<p><xsl:value-of select="//lang_blocks/p2"/></p>
				<p><xsl:value-of select="//lang_blocks/p3"/></p>
				<p><xsl:value-of select="//lang_blocks/p4"/></p>
				
				<p>
					<a href="/clubrewards/merchant_flyer.xml">
						<xsl:value-of select="//lang_blocks/link1"/>
					</a>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					<a href="/clubrewards/printcard.xml">
						<xsl:value-of select="//lang_blocks/link2"/>
					</a>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					<a href="/cgi/appx.cgi/pma">
						<xsl:value-of select="//lang_blocks/link3"/>
					</a>
					<br />
					<br />
					<a href="/clubrewards/preregistered_merchants_email.xml"><xsl:value-of select="//lang_blocks/link4"/></a>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					<a href="/clubrewards/preregistered_merchants_postal_mail.xml"><xsl:value-of select="//lang_blocks/link5"/></a>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					<a href="/clubrewards/preregistered_merchants_cover_letter.xml"><xsl:value-of select="//lang_blocks/link6"/></a>
					
					<br /><br />
					
				</p>
				
				<h2><xsl:value-of select="//lang_blocks/p5"/></h2>
		<form>
			
			<xsl:if test="//data/country">
				<input type="hidden" name="country" value="{//data/country}" />
			</xsl:if>
			
			<xsl:if test="//data/country_name">
				<xsl:value-of select="//data/country_name"/> <xsl:value-of select="//data/state_name"/>
			</xsl:if>

			
			
			
			
			<br /><br />
			
			<xsl:value-of select="//data/select_label"/>: 
			<select name="{//data/select/@name}"  onchange="{//data/select/@onchange}">
				<xsl:for-each select="//data/select/option">
					<option value="{@value}">
						<xsl:value-of select="."/>
					</option>
				</xsl:for-each>
			</select>
			
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<input type="submit" name=".submit" value="{//lang_blocks/submit_label}" />
			
		</form>
		</td></tr></table>
    </xsl:when>



    <xsl:otherwise>
		<table width="90%" align="center" border="1">
				<xsl:for-each select="//data/table/tr">
					<tr class="{@class}">    	
				       	<xsl:for-each select="th">
				        	<th><xsl:value-of select="."/></th>
				    	</xsl:for-each>
				    	<xsl:for-each select="td">
				        	<td class="{@class}"><xsl:value-of select="."/></td>
				    	</xsl:for-each>
					</tr>
				</xsl:for-each>
		</table>
	</xsl:otherwise>
</xsl:choose>
		
		</body>
	</html>


</xsl:template>
</xsl:stylesheet>
