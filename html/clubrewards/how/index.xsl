<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p1"/></title>
            
            <meta name="title" content="clubshop rewards"/>
<meta name="keywords" content="clubshop rewards, ClubCash"/>
<meta name="abstract" content="clubshop rewards. rewards points"/>
<meta http-equiv="content-language" content="en-us"/>
<meta http-equiv="content-language" content="english"/>
<meta http-equiv="content-language" content="lang_en"/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop rewards"/>
<meta name="design-development" content="carsten rieger"/>

<meta name="publisher" content="clubshop rewards"/>
<meta name="company" content="clubshop"/>
<meta name="copyright" content="copyright © 2009 clubshop rewards. all rights reserved"/>
<meta name="page-topic" content="ClubCash"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="general"/>
<meta name="revisit-after" content="7 days"/>


<link href="/clubrewards/scripts/css/main.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<xsl:comment>

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i&lt;a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[i])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&amp;&amp;parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&amp;&amp;d.all) x=d.all[n]; for (i=0;!x&amp;&amp;i&lt;d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&amp;&amp;d.layers&amp;&amp;i&lt;d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x &amp;&amp; d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i&lt;(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//</xsl:comment>
</script>
            
            
    </head>        

<body  onload="MM_preloadImages('../images/nav/nav_loyalty_2.gif','../images/nav/nav_merchant_2.gif','../images/nav/nav_partnership_2.gif','../images/nav/nav_about_2.gif','../images/nav/nav_advertising_2.gif')">

<ul id="tablist">
  <li><a href="" onClick="return expandcontent('sc1', this)" theme="#FFFFFF"><xsl:value-of select="//lang_blocks/p2"/></a></li>
  <li><a href="" onClick="return expandcontent('sc2', this)" theme="#FFFFFF"><xsl:value-of select="//lang_blocks/p3"/></a></li>
          </ul>


<DIV id="tabcontentcontainer">
  
  <div id="sc1" class="tabcontent">

 
 
<img src="../images/general/dick_burke.jpg" width="94" height="156" align="left" class="image_right_padding" />
<span class="style15"><xsl:value-of select="//lang_blocks/p4"/></span><br/>
  <br/>

<span class="Text_Body_Content">
<xsl:value-of select="//lang_blocks/p5"/><br/>
<xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/" target="_blank">http://www.clubshop.com/</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/><br/>

<xsl:value-of select="//lang_blocks/p8"/></span><span class="Text_Body_Content">.<br/>
<br/>

</span><span class="style15"><xsl:value-of select="//lang_blocks/p9"/></span>

<ul>
  <li class="Text_Body_Content"><xsl:value-of select="//lang_blocks/p10"/></li>

  <li class="Text_Body_Content"><xsl:value-of select="//lang_blocks/p11"/></li>
</ul>

<p class="Text_Body_Content"><xsl:value-of select="//lang_blocks/p12"/><br/><br/>

    <xsl:value-of select="//lang_blocks/p13"/>  <br/>
<br/>

<xsl:value-of select="//lang_blocks/p14"/><br/></p>
Dick Burke<br/>
Founder &amp; CEO<br/>
DHSC -<xsl:text> </xsl:text><span class="style25"><xsl:value-of select="//lang_blocks/p15"/></span>

</div>






<div id="sc2" class="tabcontent">

<script language="JavaScript">
<xsl:comment>
if(document.images) { 
currentslide=1; 
maxslides=21; 
slide = new Array(maxslides+1); 
for(var n=1;n&lt;=maxslides;n++) { 
slide[n]=new Image(602,454); 
slide[n].src='../images/slides/presentation/'+n+'.png'; 
} 
} 
function prevSlide() { 
if(document.images) { 
currentslide = currentslide -1; 
if(currentslide&lt;1) currentslide=maxslides; 
document.images['slide'].src=slide[currentslide].src; 
} 
} 
function nextSlide() { 
if(document.images) { 
currentslide++; 
if(currentslide &gt; maxslides) currentslide=1; 
document.images['slide'].src=slide[currentslide].src; 
} 
} 

//</xsl:comment>

</script>






<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" valign="top">

<table width="602" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
		<img name='slide' width="602" height="450" border="0" hspace="0" vspace="0" alt='slideshow' src='../images/slides/presentation/1.png'/>
		</td>
		</tr>
			
		<tr>
			<td align="center"><a href="javascript:prevSlide()"><img src="../images/nav/arrow_left_orange.png" alt="Previous Slide" width="35" height="35" hspace="10" vspace="10" border="0"/></a><a href="javascript:nextSlide()"><img src="../images/nav/arrow_right_orange.png" alt="Next Slide" width="35" height="35" hspace="10" vspace="10" border="0"/></a></td>
			</tr>
				<tr>
  				<td align="center"><img src="../images/icons/pdf_button_1.png" width="16" height="16" hspace="10" vspace="0" border="0" align="absmiddle"/><a href="../images/pdf/ClubShop_Rewards.pdf" target="_blank" class="nav_blue"><xsl:value-of select="//lang_blocks/p17"/></a></td>
				</tr>

</table>
			
				</td>
				</tr>	
				</table>










</div>










</DIV>

</body></html>
</xsl:template>
</xsl:stylesheet>