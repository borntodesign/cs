<html>
<head>
	<title>ClubShop Rewards News</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body bgcolor="#00376F" body leftmargin="1" topmargin="0" marginwidth="0" marginheight="0"> 

<table
width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#00376F">
<tbody>
<tr>
<td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="650">
<tbody>
<tr>
<td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif"><br>

Online bekijken in web <a href="http://www.clubshop.com/clubrewards/email/welcome_shopper/browserview.html" target="_blank" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline">browser</a></td>
</tr>
<tr>
<td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif">Gelieve <a href="mailto:Info@ClubShop.com" target="_blank" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline">Info@ClubShop.com</a> toe te voegen aan uw adresboek,<br />
                  zo mist u in de toekomst geen enkele e-mail in uw inbox.<br />
<br></td>
</tr>
<tr>
  <td align="center" valign="top" style="FONT-SIZE: 13px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold;"><table width="100%" border="0" cellpadding="10" cellspacing="5" bgcolor="#FCA640">
    <tr>
      <td valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
                          <td align="center"><span style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif"><strong>%%firstname%%, 
                            hieronder vindt u uw persoonlijk ID nummer en paswoord.</strong></span></td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><table width="220" border="0" cellspacing="0" cellpadding="0">
                <tr>
                                <td width="128" align="left" valign="top" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif">Uw 
                                  ID nummer:</td>
                  <td width="92" align="left" valign="top" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #00376F; FONT-FAMILY: Arial, Helvetica, sans-serif">%%alias%%</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center"><table width="220" border="0" cellspacing="0" cellpadding="0">
                <tr>
                                <td width="128" align="left" valign="top" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif">Uw 
                                  paswoord:</td>
                  <td width="92" align="left" valign="top" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #00376F; FONT-FAMILY: Arial, Helvetica, sans-serif">%%password%%</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
                          <td align="center"><span style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif">U 
                            kunt inloggen op uw <strong> portalpagina</strong> door
                            <a href="http://www.clubshop.com/ppreport.html" target="_blank" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline">hier te klikken</a> </span></td>
          </tr>
      </table></td>
    </tr>
  </table></td>
</tr>


<tr>
  <td align="center" valign="top"><table id="Table_01" width="650" height="942" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3"><img src="http://www.clubshop.com/clubrewards/email/images/header-newshopper_1.png" width="650" height="166" alt=""></td>
    </tr>
    <tr>
      <td><img src="http://www.clubshop.com/clubrewards/email/images/news_2.png" width="49" height="541" alt=""></td>
      <td width="552" height="541" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left"><span style="FONT-SIZE: 16px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold;">Nooit meer de volle pot betalen!</span></td>
        </tr>
        <tr>
          <td><span style="FONT-SIZE: 12px; COLOR: #160e0b; FONT-FAMILY: Arial, Helvetica, sans-serif"><br>
                <br>
                <img src="http://www.clubshop.com/images/cards/rewards_club.gif" width="200" height="129" align="right">Eerst 
                        en vooral willen wij u bedanken om in te schrijven voor 
                        een gratis <strong>ClubShop Rewards lidmaatschap</strong>. 
                        U kan uw lidmaatschap nu gebruiken om <strong>Reward punten</strong> 
                        te verdienen die kunnen omgezet worden in cash spaargeld, 
                        iedere keer u winkelt in onze <strong>ClubShop Mall</strong> 
                        of bij onze offline <strong>ClubShop Rewards kaart</strong> 
                        handelaren in uw buurt en overal ter wereld.<br />
                        <br />
                        Hierboven vindt u uw persoonlijk<strong> ID nummer</strong>en 
                        <strong>paswoord</strong> terug die u kunt gebruiken om 
                        in te loggen als <strong>Clubshop Rewards lid</strong>.<br />
                        <br />
                        Hieronder kunt u onze online <strong>ClubShop Mall </strong>bezoeken, 
                        een lijst bekijken met lokale handelaren die uw kaart 
                        offline accepteren en, uw eerste <strong>1,000 Bonus Reward 
                        punten</strong> binnenhalen.<br />
                        <br />
                        <br />
                        <img height="12" src="http://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /><a href="../../mall/US/" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier om de ClubShop Mall in uw land te bekijken</a><br />
                        <img height="12" src="http://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /><a href="../../rewardsdirectory/?country=US&amp;action=report_by_city&amp;state" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier om de ClubShop handelaren te zien die uw kaart lokaal 
                        zullen accepteren</a><br />
                        <img height="12" src="http://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /><a href="http://www.clubshop.com/mall/questionnaire.xsp" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier om een kleine vragenlijst in te vullen en verdien 
                        500 Bonus punten</a><br />
                        <img height="12" src="http://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /><a href="../../mall/US/" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Doe 
                        uw eerste aankoop en verdien 500 bijkomende Bonus punten</a><br />
                        <br />
                        <br />
                            We waarderen u als <strong>ClubShop Rewards lid</strong> 
                            e n werken daarom hard om u de beste kortingen online 
                            en offline te kunnen aanbieden, nu en in de toekomst.<br />
                        <br />
                        Dick Burke<br />
                        <strong>CEO - DHSC ClubShop Rewards</strong><br />
                        2560 Placida Road<br />
                        Englewood, FL USA 34224 - 5412</span></td>
                    </tr>
              
                </table></td>
              <td><img height="541" src="http://www.clubshop.com/clubrewards/email/images/news_4.png" width="49" /></td>
            </tr>
            <tr> 
              <td colspan="3"><img height="235" src="http://www.clubshop.com/clubrewards/email/images/footer-newshopper_1.png" width="650" /></td>
            </tr>

        </table></td>
    </tr>
    <tr> 
      <td height="10">&nbsp;</td>
    </tr>
    <tr> 
      <td align="left" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"><img border="0" height="37" src="http://www.clubshop.com/clubrewards/email/images/logos/facebook_logo.png" width="41" /></a><span style="font-size:8px; padding-left:10px"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Wordt fan op Facebook</a></span> <div style="font-size:8px; padding-left:10px"></div></td>
    </tr>
    <tr> 
      <td align="left" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="left" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top">Klik hier om <a href="http://www.clubshop.com/cgi/mailstatus.cgi?e=%%emailaddress%;o=%%oid%%" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">uit 
        te schrijven</a>.<br /> <br />� 2009 DHSC ClubShop Rewards. Alle rechten 
        voorbehouden.<br />
        Dit is een bevestigingse-mail van DHSC ClubShop Rewards.</td>
    </tr>
    <tr> 
      <td align="left" height="10">&nbsp;</td>
    </tr>
  </tbody>
</table>
<div align="center"></div></td>
</tr>
</tbody>
</table>
</body>
</html>
