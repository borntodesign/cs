<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Print Your ClubRewards Card</title>
<style type="text/css">
body {background-color: #FFFFFF;
}
td#main{background-color: #FFFFFF;
border: thin solid #003366;
font-family: Verdana, Arial, sans-serif;
font-size: small;
valign:top;
padding: 5px;

}

td#sidelinks{ }

hr{color: #006699;
}

h2 {font-family: Arial, Verdana, sans-serif;
font-size: 16px;
color: #003366;
text-align: center;
}

p {font-family: Verdana, Arial, sans-serif;
font-size: small;
}
table.a{cellpadding: 5px;
}

table.b{font-family: Verdana, Arial, sans-serif;
font-size: small;
align: center;
width: 90%;

}
p.p{color: #003366;
font-weight: bold;}
</style>

</head>
<body>
<table>
<tr>
<td id="main">
<img src="/clubrewards/images/print.gif" height="51" width="770" alt="Print Your ClubRewards Card"/>

<h2><xsl:value-of select="//lang_blocks/p1"/></h2>
<p><xsl:value-of select="//lang_blocks/p2"/></p>

<table class="b"><tr><td>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
<li><xsl:value-of select="//lang_blocks/p8"/></li>
</ol></td>

<td></td>
<td>
<div align="right"><img src="/clubrewards/images/cr_card.gif" height="148" width="226" alt="ClubRewards Card"/></div>
</td>
</tr></table>
<br/><br/>
<div align="center">
<p class="p"><xsl:value-of select="//lang_blocks/p9"/></p>
<a href="https://www.clubshop.com/cgi/cbmycards.cgi?action=new_order&amp;label=Quick%20Card&amp;selection=selected_only"><img src="/clubrewards/images/quickprint.gif" height="40" width="200" border="0" alt="QuickPrint Your Card NOW"/></a>
</div>



<p></p>



</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>