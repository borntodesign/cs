<ol>
<li>Vul de transactie in op een handelaren transactie inboekformulier</li>
<li>Op uw "Handelaren Interface" dient u bovenaan "Transacties" aan te klikken en op de transactiepagina selecteert u "Transactie Online Inboeken"</li>
<li>Op de "Transactie Online Inboeken" pagina, klikt u het lege "ClubShop Rewards ID Nummer" veld aan en scant u de "ClubShop Rewards Kaart" bar code. Door de "ClubShop Rewards Card" barcode te sncannen, verschijnt het ClubShop Rewards ID nummer automatisch in het veld </li>
<li>Klik in het veld “Bedrag aankoop” en vul het totale aankoopbedrag in (zonder BTW)</li>
<li>Klik in het veld “Datum transactie” en vul de datum in</li>
<li>Klik op de button “Toevoegen” om uw transactie in de “Cumulatieve Transactie Lijst” te zetten. Indien nodig, deze stappen herhalen om meerdere transacties in de "Cumulatieve Transactie Lijst" toe te voegen.</li>
<li>Om alle transacties in de “Cumulatieve Transactie Lijst” in te dienen, eerst nog uw munteenheid selecteren</li>
<li>Kijk even na of alle transacties correct zijn ingevuld en mocht u fouten vinden, dan kunnen deze in de “Cumulatieve Transactie Lijst” verbeteren</li>
<li>Klik éénmaal op de button “Submit lijst” en wacht tot uw Transactie rapport te voorschijn komt</li>
<li>U kunt uw transacties inkijken op uw “Transactie rapport”</li>
</ol>
