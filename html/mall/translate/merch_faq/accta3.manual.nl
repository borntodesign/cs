De "Internationale bankoverschrijving" betalingsmethode wordt momenteel enkel ter beschikking gesteld voor de lidstaten van de Europese Unie. Na ontvangst van fondsen op onze Bank of Ireland rekening en daarvan in kennis gesteld zijn (7 tot 10 werkdagen), zullen wij vervolgens de fondsen overmaken naar uw ClubAccount. Hier vindt u de gedetailleerde bankgegevens die u nodig hebt als u deze betalingsmethode wenst te gebruiken:<br /><br />
<strong>BANK NAAM:</strong> Bank of Ireland<br />
<strong>BANK ADRES:</strong> Northern Cross, 2-3 Burnell Green, Malahide Road, Dublin 17<br />
<strong>SORTING CODE:</strong> 904587<br />
<strong>REKENINGNUMMER:</strong> 95145818<br />
<strong>IBAN:</strong> IE06 BOFI 9045 8795 1458 18<br />
<strong>BIC CODE:</strong> BOFIIE2D 