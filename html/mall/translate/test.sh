#!/bin/bash
#
for file in *.shtml
do
    newname=`basename $file .shtml`
    /usr/bin/gtranslate en $1 `cat $file` | \
                 sed 's|\\u0026#39;|::|g' | \
                 sed "s/::/'/g" > $newname.$1
echo "$newname.$1"
done
