Er zijn plaatsen binnenin de ClubShop.com websites die u kunnen linken naar andere websites die niet het privacybeleid van ClubShop.com hanteren. Als u klikt naar deze websites, is het privacybeleid van ClubShop.com  niet langer van toepassing. Wij adviseren dan ook om de privacy verklaringen van deze derde partij websites aandachtig te lezen, om te begrijpen hoe en welke gegevens zij verzamelen, gebruiken, verwerken en uitbrengen. Deze derde partij websites behoren tot, maar beperken zich niet tot de ClubShop Mall, zijn dienstverleners en verkopers.



