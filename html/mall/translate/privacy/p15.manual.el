ν σημεία σε όλη την έκταση των websites του DHS Club που μπορεί να σας συνδέουν με άλλα websites που δέν λειτουργούν σύμφωνα με τις πρακτικές προστασίας δεδομένων του DHS Club. Όταν κάνετε click γιά να οδηγηθείτε στα sites αυτά, οι πρακτικές προστασίας δεδομένων του DHS Club δέν ισχύουν πλέον. Συνιστούμε να εξετάζετε τις δηλώσεις διαχείρισης στοιχείων γιά όλα τα web sites τρίτων μερών ώστε να κατανοήσετε τις διαδικασίες τους συλλογής, χρήσης και αποκάλυψης των πληροφοριακών σας στοιχείων. Τα websites αυτά των τρίτων μερών περιλαμβάνουν, αλλά δέν περιορίζονται στα Καταστήματα και τους Προμηθευτές Υπηρεσιών και Προϊόντων της Αγοράς ClubShop.











