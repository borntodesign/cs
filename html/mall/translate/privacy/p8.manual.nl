We begrijpen dat wanneer u online zaken doet, dat u de mensen waarmee u zaken doet moet kunnen vertrouwen. Wij nemen dit vertouwen zeer ernstig en maken het een prioriteit dat de beveiliging en vertrouwelijkheid van deze persoonlijke informatie ("PI") gewaarborgd blijft en veilig gesteld wordt. (PI omvat de informatie die gelinkt kan worden aan een specifiek individu, zoals de naam, adres, telefoonnummer of e-mail adres.)



