Om de vertrouwelijkheid van uw persoonlijke informatie te verzekeren, gebruikt ClubShop.com hiervoor de beste beveiligingstechnologieën en -procedures. Wanneer u ons van persoonlijke gegevens voorziet via ClubShop.com is uw data beschermd door Secure Socket Layer (SSL) technologie, voorzien van een digitaal certificaat van Thawte Server CA. Om dit certificaat nader te bekijken op elke beveiligde pagina (pagina die begint met "https"), klik op het plaatje van het gesloten slot of "solid key" in uw browser venster. Een klein pop-up venster met informatie over deze beveiliging zal verschijnen.



