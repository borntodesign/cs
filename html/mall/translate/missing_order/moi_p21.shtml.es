Cualquier pedido con un precio de compra de más de $ 150, no será acreditado hasta que la investigación con el comerciante se ha completado.
