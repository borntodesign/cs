Le pourcentage de ClubCash ou les offres spéciales instantanées
                        de votre achat figurent à côté de chaque magasin sur notre
                        annuaire des commerces du centre commercial en ligne ou
                        à côté de chaque activité d'Entreprise sur le répertoire
                        ClubShop Rewards. Pour chaque 1,00$ (USD) crédité sur
                        votre compte, vous gagnerez du ClubCash. Vous pouvez échanger
                        votre ClubCash contre des espèces ou contre une grande
                        variété de Bons cadeaux. Le ClubCsh se cumule, donc plus
                        vous achetez et gagnez du ClubCash, plus votre ClubCash
                        sera important! 
