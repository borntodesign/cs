<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="missing_form.xsl" ?>

<lang_blocks>

<p1>Pogrešano ClubShop spletno nakupovalno naročilo</p1>
<p2>PROSIMO PREBERITE</p2>
<p3 new="12/15/09">before submitting the missing order form below</p3>
<!--<p4>Posredovano neevidentirano naročilo še ne pomeni jamstva, da bo vaš zahtevek knjižen v vaše dobro, v vaše transakcijsko poročilo. Obvestili vas bomo tudi v primeru, ko ne bomo zmožni pridobiti verifikacije od trgovine ali prodajalca. Nobeno zamujanje ne odloča o tem ali je trgovec nezmožen potrditi dobropis v roku za vaše transakcijsko poročilo.</p4><p5>Če bo naša raziskava in poizvedba vašega zahtevka pozitivna, vam bomo vaš zahtevek knjižili v dobro v vaše transakcijsko poročilo ter vas o priznanju tudi obvestili. Obvestili vas bomo tudi v primeru, ko ne bomo sposobni pridobiti verifikacije od trgovine ali prodajalca. Nobeno zamujanje ne odloča o tem ali je trgovec nezmožen potrditi dobropis za vaše transakcijsko poročilo.</p5><p31>Ko bomo prejeli manjkajoče naročilo bomo preverili, če obstajajo informacije za sledenje in na ta način ugotovili, če je kupec tisti dan šel v trgovino iz ClubShop centra ali pa prej in ali je bilo naročilo oddano. Če bomo lahko s pomočjo sledilnih informacij preverili in če je bil nakup majši kot 250$, boste ustrezen kredit za nakup prejeli, še preden bomo mi začeli oziroma dokončali s preiskavo.</p31><p32>Prosimo počakajte 72 ur (96 ur če upoštevamo 3 dni vikenda), od trenutka ko nam pošljete obrazec za manjkajoče naročilo, da bo kredit iz nakupa na vašem transakcijskem poročilu.</p32><p33>Za vsako naročilo z nakupom več kot 250$, kredit ne bo nakazan pred koncem preiskave s trgovcem.</p33><p34>Ne pozabite, da dobite kredit za nakupe še preden mi dobimo bilokakšen kredit od trgovca. Če zaradi kakršnega koli razloga sumimo, da je bil ta privilegij zlorabljen, si pridržujemo pravico da kredit zadržimo tako dolgo, dokler preiskava s trgovcem ni zaključena.</p34><p35 new="09/11/08">Če s pomočjo naše preiskave, trgovec zanika nakup ali noče dati kredit, vas bomo kot kupca obvestili, da naročilo v tej trgovini ali pri tem trgovcu ne moremo potrditi (ki je prišlo iz trgovine ali od prodajalca z dostopom iz ClubShop centra), zato kredit na kupčevem računu ne bo zabeležen.</p35><p36 new="04/14/09">Nekatere trgovine ne bodo nudile kreditov, razen v primerih, če jih one samodejno ne priznavajo. V tem primeru se vloge za manjkajoča naročila ne bodo sprejemala. Kliknite na</p36><p37 new="04/14/09">gumb za seznam trgovin, da ugotovite če trgovina kjer nakupujete upošteva ta pravilnik.</p37>-->

<p6>Oddaja vloge in navodila</p6>
<!--<p7>Ne podajajte tega zahtevka prej ko poteče vsaj 10 dni od dneva ko ste dobili potrjeno naročilo ali pa vsaj 10 dni po tem, ko je bila vaša rezervacija uporabljena (storitev, potovanje).</p7>-->
<p8>V celoti izpolnite vsako polje preden oddate ta zahtevek.</p8>
<p9>Zahtevki oddani z nepopolnimi podatki /brez kopije uradnega računa/ ne bodo proučeni.</p9>
<p10>Vključiti morate datum iz vrha prejetega elektronskega sporočila, ki ste ga prejeli kot račun/potrdilo.</p10>
<p11>Če ne boste uporabili pravilnega postopka in oblike, vaše naročilo ne bo proučeno.</p11>
<p24>OPOMBA: Naročil starejših od 90 dni ne moremo preverjati.</p24>
<p12>POMEMBNO!</p12>
<!--<p13>če imate kakšna vprašanja v zvezi z vašimi nagradnimi točkami, morate vedno kontaktirati z vašim marketinškim direktorjem ali pa z vašim timskim vodjem. NE KONTAKTIRAJTE Z TRGOVCI ali z vašim pridruženim partnerjem o vaših nagradnih točkah. Oni nimajo informacij v zvezi z vašim programom.</p13>-->


<p14>* Zahtevano</p14>
<p15>* DHS Klub ID številka:</p15>
<p16>* Vaš elektronski naslov:</p16>
<p17>* Prodajna trgovina:</p17>
<p18>* Lokacija ClubShop nakupovalnega središča: (NL,IT,US itd)</p18>
<p19>* Datum nakupa:</p19>
<p20>* Številka računa:</p20>
<p21>* Skupaj (Brez prispevkov, davka, itd.):</p21>
<p22>* Kopiraj &amp; Prilepi elektronsko sporočilo z računom/potrdilom.</p22>
<p23>V glavi elektronskega sporočila mora biti viden datum:</p23>
<!--<p27>Način naročanja -</p27>
<p28>Zaradi vladnih uredb, ne morete zaslužiti točke za vsako vrsto načina nakupa (ov). Vi ne smete prijavljati manjkajočega naročila za kateregakoli nečlana mimo točk, in tega vrstnega reda.</p28><p29>Uporaba darilnih bonov za spletne nakupe -</p29><p30>Nekateri trgovci vam ne bodo dali kredita za nakup opravljen z darilnim bonom. Primer: Če boste naredili nakup za 75$ in plačali 25$ tega nakupa z darilnim bonom, boste vedno zaslužili nagradne točke le za preostalih 50$ nakupa.</p30>
-->
</lang_blocks>
