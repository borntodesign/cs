<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>

<xsl:param name="only-content" />
   <xsl:template match="/">

<xsl:choose>
	<xsl:when test="$only-content != ''">
<!-- we are going to simply push out a well formed XHTML fragment for an AJAX call -->	   
		<xsl:call-template name = "content" />
	</xsl:when>
  
	<xsl:otherwise>
      <html><head><title></title></head><body>
		<xsl:call-template name = "content" />
	</body></html>
	 
	</xsl:otherwise>
</xsl:choose>
</xsl:template>


<xsl:template name="content">
<div>
<h4><xsl:value-of select="//heading" /></h4>
<xsl:for-each select = "//paragraphs/*">
<p><xsl:value-of select="." /></p>
</xsl:for-each></div>
</xsl:template>

</xsl:stylesheet>
