<?xml version="1.0"?>
<!--DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href="/xml/head-foot.xsl"/>
<xsl:include href="vendorList.xsl"/>
<xsl:param name="alpha_range" />
<xsl:param name="catid" />
<xsl:param name="search_phrase" />

	<xsl:template match="/">
		<html><head>

<link rel="stylesheet" href="/css/global.css" type="text/css" />
<link rel="stylesheet" href="css/storeList.css" type="text/css" />
<link rel="stylesheet" href="css/catDrilling.css" type="text/css" />
<link href="css/xmall.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<xsl:comment>
var $MALL_ID = <xsl:value-of select="//mall/id"/>;
var $CATX = "<xsl:value-of select="//lang_blocks/catexpander-txt" />";
</xsl:comment>
</script>

<script type="text/javascript" src="mall.js"></script>
<script type="text/javascript" src="mall-vendor.js"></script>
<script type="text/javascript" src="/js/yui/yahoo-min.js"></script>
<script src="/js/yui/dom-min.js" type="text/javascript"></script>
<script src="/js/yui/event-min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/yui/connection-min.js"></script>
<script type="text/javascript" src="catDrilling.js"></script>
<script type="text/javascript" src="/js/miniLogin.js"></script>

<title><xsl:value-of select="concat('Clubshop ', ' - ', //title_append)" /></title>

		</head><body onload="init()">
<xsl:call-template name = "head" />
		<table class="xm_main">
		<thead><tr><td colspan="2" align="center"><xsl:call-template name="mall_list"/></td></tr></thead>
		<tfoot><tr><td colspan="2" align="center">
<xsl:call-template name="foot"/>

<!-- now hack in a Facebook link if we have a node to display -->
<xsl:if test="//lang_blocks/facebook !=''">
<p>
<img style="vertical-align:middle" src="/images/facebook_logo.png" height="41" width="37" alt="Facebook Logo" />
<a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" class="facebook_link">
<xsl:value-of select="//lang_blocks/facebook" /></a></p>
</xsl:if>

		<div class="xm_copyright">
		<img src="/images/copyright_.gif" width="173" height="19" alt="Copyright" />
		</div>
		</td></tr></tfoot>

		<tbody><tr><td id="nav_container">

		<xsl:if test="//root/user/top_picks = '1'">
			<div id="TopPicksTitle"><xsl:value-of select="//lang_blocks/top_picks_title" /></div>
			<a id="TopPicksLink">
				<xsl:attribute name="href">
					<xsl:text>top_picks.xsp?mall_id=</xsl:text>
					<xsl:value-of select="//mall/id"/>
				</xsl:attribute>
				<xsl:value-of select="//lang_blocks/top_picks_notice" />
			</a>
		<p></p>
		</xsl:if>


<xsl:choose><xsl:when test="count(//mall/alpha/section) = '1'">
<a id="alpha_search_link">
	<xsl:attribute name="href">alpha-results.xsp?alpha_range=<xsl:value-of select="//mall/alpha/section/@range" />;mall_id=<xsl:value-of select="//mall/id"/>
	</xsl:attribute>
	<xsl:value-of select="//lang_blocks/full_list" /></a>
</xsl:when>
<xsl:otherwise>
	<form action="alpha-results.xsp" method="get">
	<input type="hidden" name="mall_id">
		<xsl:attribute name="value"><xsl:value-of select="//mall/id"/></xsl:attribute></input>
	<select id="alpha_search_dropdown" name="alpha_range">
		<xsl:attribute name="onchange">if (this.selectedIndex != 0){ this.parentNode.submit()}</xsl:attribute>
	<option value=""><xsl:value-of select="//lang_blocks/alpha_dropdown_inst" /></option>
	<xsl:for-each select="//mall/alpha/section">
		<option><xsl:attribute name="value">
				<xsl:value-of select="@range" />
			</xsl:attribute>
			<xsl:if test="@range = $alpha_range">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="label" /></option>
	</xsl:for-each>
	</select>
	<noscript><input type="submit" value="Submit" /></noscript>
	</form>
</xsl:otherwise></xsl:choose>

<form id="KeywordSearch" action="cat-search.xsp" method="get">
        <input type="hidden" name="mall_id">
                <xsl:attribute name="value"><xsl:value-of select="//mall/id"/></xsl:attribute></input>

<div id="KeywordSearchLabel"><xsl:value-of select="//lang_blocks/enter-keyword" />
<a href="search-help.xml" onclick="return searchHelpClick()" style="margin-left:1em"><img width="15" height="15" src="icons/icon_more_info.gif" alt="" border="0" style="vertical-align:bottom" /></a></div>
<input type="text" name="search_phrase">
	<xsl:attribute name="value"><xsl:value-of select="$search_phrase" /></xsl:attribute>
</input><br />
<input type="submit">
        <xsl:attribute name="value"><xsl:value-of select="//lang_blocks/search-btn" /></xsl:attribute>
</input>
</form>

<div id="CategoryNavigation">
<a onclick="doCatList0(); return false;" href="#" id="Search-by-Category"><xsl:value-of select="//lang_blocks/online_cats" /> <img src="icons/tab-bumps.gif" width="14" height="9" alt="" border="0" hspace="10" /></a>
<div id="catlist0" class="catMenuBox">
<a class="MenuBoxCloser" onclick="return closeMenuBox(this)" href="#">X</a>
</div>
</div>
<noscript>
<!-- we will fill this in later with a plain old drop-down list and a Go button-->
<form method="get" action="cat-results.xsp">
<xsl:copy-of select="//mall/select" />
<input type="submit" />
<input type="hidden" name="mall_id"><xsl:attribute name="value"><xsl:value-of select="//mall/id"/></xsl:attribute></input>
</form>
</noscript>
			<p id="OfflineStoresLabel"><xsl:value-of select="//lang_blocks/offline" /></p>
		<!--form id="cb_merchant" action="/cgi/ma_real_rpt.cgi" target="_blank"-->
		<form id="cb_merchant" action="/rewardsdirectory/" target="_blank">
		<input type="hidden" name="country"><xsl:attribute name="value"><xsl:value-of select="//ma_states/country/@code" /></xsl:attribute>
			</input>
		<input type="hidden" name="action" value="report_by_city" />
		<select name="state" onchange="document.forms['cb_merchant'].submit()">
		<option value="">
			<xsl:choose><xsl:when test="//ma_states/country/state">
				<xsl:value-of select="//lang_blocks/select_state" />
			</xsl:when><xsl:otherwise>
				<xsl:value-of select="//lang_blocks/no_merchants" />
			</xsl:otherwise></xsl:choose>
		</option>
		<xsl:for-each select="//ma_states/country/*">
			<option><xsl:attribute name="value"><xsl:value-of select="./@code" /></xsl:attribute>
				<xsl:value-of select="." /></option>
		</xsl:for-each></select><br />
		<input type="submit" value="Go" style="margin-top:2px; font-size: 85%;" /></form>


		</td>
		<td id="main_content">
<xsl:copy-of select="//root/content/html/*" />
		</td>
		</tr></tbody>
</table>

		</body>
		</html>
	</xsl:template>


</xsl:stylesheet>
