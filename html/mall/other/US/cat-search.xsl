<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration="yes"
        media-type="text/html"/>


        <xsl:template match="/">
                <root>
			<!--xsl:copy-of select="document('common.xml')"/-->
                        <xsl:copy-of select="/*/*[name() != 'rpc']"/>
                        <content>
<!-- if we encountered any errors during the CGI generation of the cat list XML, they'll show up in an error node
also there probably won't be any rpc -->
                        <xsl:choose>
                                <xsl:when test="//error"><h4><xsl:value-of select="//error" /></h4></xsl:when>
				<xsl:when test="/*/rpc/count = '0'"><html><p>
					<!--xsl:value-of select="//lang_blocks/empty-search1"/>.<br /-->
					<xsl:value-of select="//lang_blocks/empty-search2"/>.
					<a href="search-help.xml" onclick="return searchHelpClick()" style="margin-left:1em"><img width="15" height="15" src="icons/icon_more_info.gif" alt="" border="0" style="vertical-align:bottom" /></a></p></html></xsl:when>
                                <xsl:otherwise>
                                        <html><div id="categoryList">
                                	<xsl:apply-templates select="/*/rpc/category" />
                                        </div></html>
                                </xsl:otherwise>
                        </xsl:choose>
                        </content>
                </root>
        </xsl:template>

        <xsl:template match="/*/rpc/category">
			
        <a>
        <xsl:attribute name="class">
                <xsl:choose>
                <xsl:when test="position() mod 2=0">lista</xsl:when>
                <xsl:otherwise>listb</xsl:otherwise>
                </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="href">cat-results.xsp?mall_id=<xsl:value-of select="/root/mall/id" />;catid=<xsl:value-of select="catid"/></xsl:attribute>
        <xsl:value-of select="label" />
		</a>
		</xsl:template>
		
</xsl:stylesheet>
