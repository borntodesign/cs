<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration="yes"
        media-type="text/html"/>


	<xsl:template match="/">
		<root>
			<xsl:copy-of select="/*/*[name() != 'root']"/>
			<xsl:copy-of select="top_picks_header"/>
			<content>
<!-- if we encountered any errors during the CGI generation of the vendors XML, they'll show up in an error node
also there probably won't be any vendors -->
			<xsl:choose>
				<xsl:when test="//error">
					<html>
						<h4>
							<xsl:choose>
								<xsl:when test="//error/not_logged_in">
									<xsl:value-of select="//lang_blocks/notices/login_notice" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="//lang_blocks/notices/no_results" />
								</xsl:otherwise>
							</xsl:choose>
						</h4>
					</html>
				</xsl:when>
				<xsl:when test="//none"><html><h4><xsl:value-of select="//notices/no_results" /></h4></html></xsl:when>
				<xsl:otherwise><xsl:apply-templates select="/*/*/vendors" /></xsl:otherwise>
			</xsl:choose>
			</content>
		</root>
	</xsl:template>



	<xsl:template match="/*/*/vendors">
	<html><table id="storeListTable">
		<caption>
			<xsl:value-of select="//user/firstname" />
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<xsl:value-of select="//lang_blocks/common/listing_header" />
		</caption>
		<tr>
			<td colspan="4" align="center">
				<a>
					<xsl:attribute name="href">
						<xsl:text>top_picks.xsp?mall_id=</xsl:text>
						<xsl:value-of select="/*/root/parameters/mall_id" />
						<xsl:text disable-output-escaping="yes">&amp;</xsl:text>
						<xsl:text>sort_by_date=</xsl:text>
						<xsl:value-of select="/*/root/parameters/sort_by" />
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="/*/root/parameters/sort_by = 1">
							<xsl:value-of select="//lang_blocks/common/sort_by_date" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//lang_blocks/common/sort_by_vendor" />
						</xsl:otherwise>
					</xsl:choose>
				</a>
			</td>
		</tr>
		<xsl:copy-of select="//vendors" />
	</table></html>
	</xsl:template>








</xsl:stylesheet>
