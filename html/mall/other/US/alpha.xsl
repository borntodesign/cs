<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration="yes"
        media-type="text/html"/>

	<xsl:param name="alpha_range" />

<xsl:include href = "vendorList.xsl"/>
	<xsl:template match="/">
		<root>
			<xsl:copy-of select="/*/*[name() != 'root']"/>
			<content><html>
<!-- if we encountered any errors during the CGI generation of the vendors XML, they'll show up in an error node
also there probably won't be any vendors -->
			<xsl:choose>
				<xsl:when test="//error"><h5><xsl:value-of select="//error" /></h5></xsl:when>
				<xsl:otherwise>
<!--					<div id="alpha_section_links">
					<xsl:for-each select="//mall/alpha/section[@range = $alpha_range]/slice/label">
					<xsl:choose>
					<xsl:when test="position() = 1"><xsl:value-of select="." /></xsl:when>
					<xsl:otherwise><a><xsl:attribute name="href">#section<xsl:value-of select="." /></xsl:attribute><xsl:value-of select="." /></a>
					</xsl:otherwise></xsl:choose>
					</xsl:for-each>
					</div>
-->
					<table id="storeListTable">
<xsl:call-template name = "vendor-list-header" />
					<xsl:for-each select = "//mall/alpha/section[@range = $alpha_range]/slice">
					
					<!--xsl:if test = "position() != 1"-->
						<tr class="section_header"><td colspan="4"><!--a><xsl:attribute name="name">section<xsl:value-of select="label" /></xsl:attribute><xsl:value-of select="label" /></a--></td></tr>
					<!--/xsl:if-->

					<xsl:for-each select = "c">
			
					<xsl:variable name = "c" select = "." />
						<xsl:for-each select="/*/root/vendors/v">
						<xsl:if test="@fl = $c">
							<xsl:call-template name = "vendors" />
						</xsl:if>
						</xsl:for-each>
					</xsl:for-each>					
					</xsl:for-each>
					</table>
				</xsl:otherwise>
			</xsl:choose>
			</html></content>
		</root>
	</xsl:template>
</xsl:stylesheet>
