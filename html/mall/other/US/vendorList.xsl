<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration="yes"
        media-type="text/html"/>

	
	<xsl:template match="/">
		<root>
			<xsl:copy-of select="/*/*[name() != 'root']"/>
			<xsl:copy-of select="/*/*/catname"/>
			<content>
<!-- if we encountered any errors during the CGI generation of the vendors XML, they'll show up in an error node
also there probably won't be any vendors -->
			<xsl:choose>
				<xsl:when test="//error"><h4><xsl:value-of select="//error" /></h4></xsl:when>
				<xsl:otherwise><xsl:apply-templates select="/*/*/vendors" /></xsl:otherwise>
			</xsl:choose>
			</content>
		</root>
	</xsl:template>
	
	<xsl:template match="/*/*/vendors">
	<html><table id="storeListTable">
		<caption><xsl:value-of select="/*/root/catname" /></caption>
		<xsl:call-template name = "vendor-list-header" />
		<xsl:apply-templates select="./v" />
	</table></html>
	</xsl:template>

	<xsl:template name="vendors" match="//vendors/v">
	<tr>
	<xsl:attribute name="class">
		<xsl:choose>
		<xsl:when test="position() mod 2=0">lista</xsl:when>
		<xsl:otherwise>listb</xsl:otherwise>
		</xsl:choose>
	</xsl:attribute>

	<td class="storeInfo"><a href="#">
		<xsl:attribute name="onclick">
		<xsl:text>return displayStoreInfo(</xsl:text>
		<xsl:value-of select="@vendor_id" />,<xsl:value-of select="/root/mall/id" />
		<xsl:text>);</xsl:text></xsl:attribute>
		
		<img src="icons/icon_more_info.gif" width="15" height="15">
			<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/store-info-icon" /></xsl:attribute>
		</img></a>
	</td>
	<td class="storeName">
		<a class="vendor"><!-- the first digits are the link type -->
		<xsl:attribute name="href"><xsl:text>/cgi-bin/rd/14,</xsl:text>
			<xsl:value-of select="@vendor_id" />,mall_id=<xsl:value-of select="/root/mall/id" />
		</xsl:attribute>
		<xsl:value-of select="vendor_name" /></a>

		<xsl:if test="primary_mallcat_text">
		<a class="pricat">
		<xsl:attribute name="href">cat-results.xsp?mall_id=<xsl:value-of select="/root/mall/id" />;catid=<xsl:value-of select="@primary_mallcat" />
		</xsl:attribute>
		<xsl:value-of select="primary_mallcat_text" /></a>
		</xsl:if>
	</td>
	
	<td class="rppp-col"><!-- our RP/PP section -->
		<!-- we won't display any numbers if there are ranges or free text -->
		<xsl:choose>
		<xsl:when test="@payout_details">
	<!-- Dick wants the icon to be centered below the text... but we want the text left justified...
		sorry, this is the easiest way to accomplish that ;-( -->
		<table><tr><td>
		<xsl:attribute name="align">center</xsl:attribute>
			<xsl:value-of select="//lang_blocks/rate-varies" />.<br />
			<!--xsl:value-of select="//lang_blocks/see-more-1" /-->
			<a href="#"><xsl:attribute name="onclick">
				<xsl:value-of select="concat('return displayStoreInfo(', @vendor_id, ',', /root/mall/id, ');')" />
				</xsl:attribute>
			<img class="see-more" src="icons/icon_more_info.gif" width="15" height="15">
				<xsl:attribute name="alt"><xsl:value-of select="concat(//lang_blocks/see-more-1, ' i ', //lang_blocks/see-more-2)" /></xsl:attribute>
				</img></a></td></tr></table>
			<!--xsl:value-of select="//lang_blocks/see-more-2" /-->
		</xsl:when>
		<xsl:otherwise>
				 	<xsl:call-template name = "RP-PP-percents" >
				 		<xsl:with-param name="v" select="." />
				 	</xsl:call-template>
		</xsl:otherwise>
		</xsl:choose>
	
	</td>
	
	<td class="store_icon_column">
	<!-- build out our icon table -->

	<table class="store_icons">
<!-- first digits are the vendor ID, next set is mall ID -->
<tbody>
       <xsl:attribute  name="id">tb<xsl:value-of select="@vendor_id" />_<xsl:value-of select="/root/mall/@id" />
       </xsl:attribute>
  <tr>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="@new">null_icon_new</xsl:when><xsl:otherwise>a</xsl:otherwise></xsl:choose></xsl:attribute></td>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="@reward_increase">null_icon_increase</xsl:when><xsl:otherwise>b</xsl:otherwise></xsl:choose></xsl:attribute></td>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="@code">icon_coupon</xsl:when><xsl:otherwise>c</xsl:otherwise></xsl:choose></xsl:attribute></td>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="@shipping_incentive">icon_ship</xsl:when><xsl:otherwise>d</xsl:otherwise></xsl:choose></xsl:attribute></td>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="@sale">icon_sale</xsl:when><xsl:otherwise>e</xsl:otherwise></xsl:choose></xsl:attribute></td>
    <td><xsl:attribute name="class"><xsl:choose><xsl:when test="gift_cert_url">null_icon_gift</xsl:when><xsl:otherwise>f</xsl:otherwise></xsl:choose></xsl:attribute>
    	<xsl:if test="gift_cert_url">
    	<a><xsl:attribute name="href"><xsl:text>/cgi-bin/rd/15,</xsl:text>
			<xsl:value-of select="@vendor_id" />,mall_id=<xsl:value-of select="/root/mall/id" /></xsl:attribute>
    	<img src="icons/icon_giftc.gif" height="15" width="15">
    		<xsl:attribute name="title"><xsl:value-of select="//lang_blocks/gift-certs" /></xsl:attribute>
    		<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/gift-certs" /></xsl:attribute>
    	</img>
    	</a>
    </xsl:if></td>
    <td>
    	<xsl:attribute name="class"><xsl:choose><xsl:when test="languages != ''">static_icon_language</xsl:when><xsl:otherwise>g</xsl:otherwise></xsl:choose></xsl:attribute>
		<xsl:if test="languages != ''"><div class="icon_language_rollover"><xsl:value-of select="languages" /></div>
    	</xsl:if>
    </td>
  </tr>

</tbody>
</table>
	<!-- end of our icon table -->
	
	</td></tr>

<!-- end of template matching vendors/v -->
	</xsl:template>
	
	<xsl:template name="RP-PP-percents">
	<xsl:param name="v" />
        <xsl:text>CC: </xsl:text>

		<xsl:choose>
		<xsl:when test="$v/@vrp_range_lower and $v/@vrp_range_upper">
			<xsl:value-of select="concat($v/@vrp_range_lower, ' - ', $v/@vrp_range_upper, '%')" />
		</xsl:when>
	  
		<xsl:otherwise>
		<!-- since we know we have either one or the other, we can safely run 'em together with impunity -->
		 	<xsl:value-of select="concat($v/@vrp_range_lower, $v/@vrp_range_upper, '%')" />
		</xsl:otherwise>
		</xsl:choose>
		
		<xsl:if test="//user/membertype = 'v' or //user/membertype = 'm'">
		<br />
	<xsl:text>PP: </xsl:text>
		<xsl:choose>
		<xsl:when test="$v/@pp_range_lower and $v/@pp_range_upper">
			<xsl:value-of select="concat($v/@pp_range_lower, ' - ', $v/@pp_range_upper, '%')" />
		</xsl:when>
	  
		<xsl:otherwise>
		<!-- since we know we have either one or the other, we can safely run 'em together with impunity -->
		 	<xsl:value-of select="concat($v/@pp_range_lower, $v/@pp_range_upper, '%')" />
		</xsl:otherwise>
		</xsl:choose>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template name="vendor-list-header">
	<thead><tr class="vendor-list-header">
	<th><img src="icons/icon_more_info.gif" width="15" height="15">
		<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/info" /></xsl:attribute>
		<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/info" /></xsl:attribute>
		</img></th>
	<th><xsl:value-of select="//lang_blocks/storename" /></th>
	<th><xsl:value-of select="//lang_blocks/points" /></th>

	<th class="store_icon_column">
		<table class="store_icons"><tr>
			<td class="a"><img src="icons/icon_new.gif" height="16" width="15">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/new" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/new" /></xsl:attribute>
				</img></td>
			<td class="b"><img src="icons/icon_increase.gif" height="14" width="8">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/increase" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/increase" /></xsl:attribute>
				</img></td>
			<td class="c"><img src="icons/icon_coupon.gif" height="14" width="19">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/coupon" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/coupon" /></xsl:attribute>
				</img></td>
			<td class="d"><img src="icons/icon_shipping.gif" height="14" width="20">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/shipping" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/shipping" /></xsl:attribute>
				</img></td>
			<td class="e"><img src="icons/icon_sale.gif" height="14" width="19">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/sale" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/sale" /></xsl:attribute>
				</img></td>
			<td class="f"><img src="icons/icon_giftc.gif" height="15" width="15">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/gc" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/gc" /></xsl:attribute>
				</img></td>
			<td class="g"><img src="icons/icon_language.gif" height="14" width="18">
				<xsl:attribute name="alt"><xsl:value-of select="//icon-descriptions/language" /></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="//icon-descriptions/language" /></xsl:attribute>
				</img></td>
		</tr></table>
	</th>
	</tr></thead>
	</xsl:template>

</xsl:stylesheet>
