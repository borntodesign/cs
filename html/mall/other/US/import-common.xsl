<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
<!--
Theoretically, AxKit should be able to pull these things in using include statements in the .xsp doc.
However, in reality, we end up with random errors when the file cannot be found.
So far, the XSLT processor doesn't seem to experience these issues.
-->
	<xsl:copy-of select = "/root/*"/>
	<ma_states><xsl:copy-of select="document('ma_state_list.xml')"/></ma_states>
	<xsl:copy-of select="document('mall.xml')"/>
	<xsl:copy-of select="document('common.xml')"/></root>
	</xsl:template>
</xsl:stylesheet>
