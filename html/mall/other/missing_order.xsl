<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>


<title><xsl:value-of select="//lang_blocks/p0"/></title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />


</head>
<body style="background:#ffffff;">

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:860px;">
<tr>
<td valign="top">
<table id="Table_01" width="860px" border="0" cellpadding="0" cellspacing="0">
<tr>
<td></td>
</tr>


<tr>

<td colspan="8" bgcolor="#FFFFFF">


<p class="style12"><xsl:value-of select="//lang_blocks/p0"/></p>
<p class="Text_Body_Content"><xsl:value-of select="//lang_blocks/p1a"/></p>
<p><xsl:value-of select="//lang_blocks/p24"/><xsl:text> </xsl:text><a href="/shoppinginstructions.xml" target="_blank" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p25"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26"/></p>

<p class="style14"><u><xsl:value-of select="//lang_blocks/p13"/></u></p>
<ul>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p15"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p1"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p17"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p4"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p18"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/contact.xml" target="_blank">https://www.clubshop.com/contact.xml</a><br/><xsl:value-of select="//lang_blocks/p18a"/></span></li><br/>

</ul>



<p class="style14"><u><xsl:value-of select="//lang_blocks/p14"/></u></p>


<ul>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p2"/><xsl:text> </xsl:text> <img src="/mall/US/icons/icon_more_info.gif" height="15" width="15" alt="Information button"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text> <img src="/mall/US/icons/icon_more_info.gif" height="15" width="15" alt="Information button"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/><xsl:text> </xsl:text> <img src="/mall/US/icons/icon_giftc.gif" height="15" width="15" alt="Gift Certificate"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p5"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p8"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p9"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p10"/></span></li><br/>
</ul>

<p class="style14"><u><xsl:value-of select="//lang_blocks/p22"/></u></p>


<ul>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p21"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p12"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p16"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p20"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p19"/></span></li><br/>
<li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p27"/></span></li><br/>
<li><span class="Text_Content"><a href="/mall/other/missing_form.xml"  class="nav_blue_line"><xsl:value-of select="//lang_blocks/p23"/></a></span></li><br/>
</ul>


<p class="text_orange"><xsl:value-of select="//lang_blocks/p11"/></p>

</td> 
 </tr>
 
 </table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>
