<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="missing_form.xsl" ?>
<lang_blocks><p1>Formulario para Ordenes Extraviadas de ClubShop En-Línea</p1>
<p2>POR FAVOR LEER</p2>
<p3>Si ha creado una orden de la cual no ha recibido crédito aún, debe esperar 10 días desde el momento en que</p3>


<!--<p4>usted *recibió* o *utilizó* su orden, o *completó* su viaje</p4><p5>, antes de enviar el formulario de órdenes extraviadas que aparece abajo, como la mayoría de las compras pueden tomar hasta 10 días hábiles para ser reportadas luego de que la mercancía haya sido enviada, o el servicio o viaje haya sido utilizado y en algunos casos (Hoteles, Boletos de Avión, Cruceros, Alquiler de Autos, etc.) las órdenes pueden tomar aún más tiempo en ser verificadas y agregadas a nuestro Reporte de Transacciones.</p5><p31>Una vez que recibimos un formulario de Órdenes Extraviadas, verificaremos si la información de rastreo se encuentra disponible, para determinar si el comprador ingresó a la tienda desde el Centro Comercial de ClubShop en la fecha y antes de la hora en que la orden fue creada. Si podemos verificar la información de rastreo y el monto de la compra es $250 o menos, el crédito apropiado será otorgado por la compra, antes de contactar a la tienda y antes de iniciar y completar nuestra investigación.</p31><p32>Por favor esperar 72 horas (96 horas si fue enviado en un fin de semana feriado de tres días) desde el momento en que envió el formulario de Orden Extraviada, para que acreditemos su compra en su Reporte de Transacciones.</p32><p33>Cualquier orden con un monto de compra de más de $250, no será acreditada hasta que la investigación con el comerciante haya sido completada.</p33><p34>Por favor recuerde que nosotros estamos dando el crédito por las compras antes de recibir cualquier crédito por parte del comerciante. Si sospechamos por cualquier motivo, que éste privilegio está siendo abusado, nos reservamos el derecho de retener el crédito hasta que la investigación con el comerciante sea completada.</p34><p35 new="09/11/08">Si, durante nuestra investigación, el comerciante deniega o rechaza el crédito por una compra, notificaremos al comprador que no es posible confirmar una orden con la tienda o vendedor (que vino desde la tienda o vendedor siendo accesada desde el Centro Comercial de ClubShop) y el crédito será negado de la cuenta del comprador.</p35><p36 new="04/14/09">Algunas tiendas no otorgan créditos, a menos que sean acreditados automáticamente y no sea posible enviar Ordenes Extraviadas. Presione en el</p36><p37 new="04/14/09">botón en el listado de tiendas, para determinar si la tienda en la que estás comprando tiene esta política.</p37>-->

<p6>Reglas y Guías para Enviar</p6>
<!--<p7>No archivar este formulario a menos que hayan transcurrido 10 días desde que la orden fue recibida (entregada a su puerta) o al menos 10 días después de que su reservación fue utilizada (para servicios y viajes).</p7>-->

<p8>Debe llenar por completo cada sección abajo antes de enviar este formulario.</p8>
<p9>Formularios enviados con información incompleta y/o sin copia de la factura/recibo original no serán investigados.</p9>
<p10>Debe incluír los encabezados con fecha de su email de factura/recibo original.</p10>
<p11>Si usted no utiliza el procedimiento y formulario adecuados, su orden no será investigada.</p11>
<p24>NOTA: No nos es posible investigar órdenes de más de 90 días de antigüedad.</p24>
<p12>IMPORTANTE!</p12>
<!--<p13>Si tiene preguntas relacionadas con los Puntos de Recompensa o la Redención de Puntos de Recompensa, debe SIEMPRE contactar a su Director de Mercadeo o Entrenador de Equipo. NO CONTACTAR AL COMERCIANTE ni a nuestros socios afiliados sobre Puntos de Recompensa. Ellos no tienen información sobre nuestro programa.</p13>-->

<p14>* Requerido</p14>
<p15>* DHS Club Número de ID:</p15>
<p16>* Su Dirección de Email:</p16>
<p17>* TIENDA O COMERCIO:</p17>
<p18>* Cuál Centro Comercial de ClubShop: (NL,IT,US etc)</p18>
<p19>* Fecha de la Compra:</p19>
<p20>* Número de Orden:</p20>
<p21>* Subtotal (Sin gastos de envío, impuestos, etc.):</p21>
<p22>* Copiar &amp; Pegar el Email de confirmación de la orden.</p22>
<p23>Los encabezados del Email con Fecha deben ser visibles:</p23>

<!--<p27>Órdenes de Medicamentos con Receta -</p27>

<p28>Debido a regulaciones del gobierno, no es posible ganar Puntos con ningún tipo de compra(s) de Medicamentos con Receta. Deberá archivar una orden extraviada por cada artículo(s) sin Receta, en esa orden.</p28><p29>Utilizando Certificados de Regalo para Compras En-línea -</p29><p30>Algunos comerciantes no dan crédito por la porción de la compra que haya hecho utilizando un Certificado de Regalo. Ejemplo: Si realiza una compra de $75.00 y paga $25 de esa compra con un Certificado de Regalo, sólo ganará Puntos de Recompensa por los restantes $50.00 de la compra.</p30>
-->

</lang_blocks>
