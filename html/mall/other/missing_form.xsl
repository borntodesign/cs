<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>


<title>ClubShop Missing Order Form</title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="application_files/get_cookie_by_name.js"></script>

<!--<style type="text/css">
body {background-color: #003399;
}
table{background-color: #FFFFFF;
}
td{font-family: Verdana, Arial, sans-serif;
font-size: small;
}


</style>-->

</head>
<body style="background:#ffffff;">

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:860px;">
<tr>
<td valign="top">
<table id="Table_01" width="860px" border="0" cellpadding="0" cellspacing="0">
<tr>

</tr>


<tr>

<td colspan="8" bgcolor="#FFFFFF">
<div align="center"><span class="style12"><xsl:value-of select="//lang_blocks/p1"/></span></div>

<p><font color="#FF0000"><b><xsl:value-of select="//lang_blocks/p12"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p2"/></b><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/missing_order.xml" class="nav_blue_line">http://www.clubshop.com/mall/missing_order.xml</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></font> </p>


<p><font color="#FF0000"><b><xsl:value-of select="//lang_blocks/p27"/></b></font><xsl:value-of select="//lang_blocks/p28"/></p>
<p><font color="#FF0000"><b><xsl:value-of select="//lang_blocks/p29"/></b></font><xsl:value-of select="//lang_blocks/p30"/></p>


<hr/>
<div align="center"><p><b><xsl:value-of select="//lang_blocks/p6"/></b></p></div>

<ol>

<li><xsl:value-of select="//lang_blocks/p8"/></li>

<li><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text><font color="#FF0000"><b><xsl:value-of select="//lang_blocks/p10"/></b></font></li>

<li><xsl:value-of select="//lang_blocks/p11"/></li></ol>



<form action="/cgi/FormMail.cgi" method="post">
  <div align="left">

    <input name="recipient" type="hidden" value="orders@dhs-club.com"/>
    <input name="email" type="hidden" value="apache@clubshop.com"/>
    <input name="required" type="hidden" value="id,email,store,mall,dop,order,subtotal,items"/>
    <input name="sort" type="hidden" value="order:id,email,store,mall,dop,order,subtotal,items"/>
    <input name="redirect" type="hidden" value="http://www.clubshop.com/mall/thankyou.html"/>
    <input name="subject" type="hidden" value="Missing_Order"/>
    <input name="env_report" type="hidden" value="HTTP_USER_AGENT"/>
  </div>
  <table>

    <tr> 
      <td colspan="2"><b><xsl:value-of select="//lang_blocks/p14"/></b></td>
   
    </tr>
    <tr> 
      <td></td>
      <td></td>
    </tr>
    <tr> 
      <td><div align="left"><xsl:value-of select="//lang_blocks/p15"/></div></td>

      <td><input type="text" name="id" size="25"/></td>
    </tr>
	 <tr> 
      <td><div align="left"><xsl:value-of select="//lang_blocks/p16"/></div></td>
      <td><input type="text" name="email" size="25"/> </td>
    </tr>

    <tr> 
      <td> <div align="left"><xsl:value-of select="//lang_blocks/p17"/></div></td>
      <td> <input type="text" name="store" size="25"/> </td>
    </tr>
    <tr> 
	  
      <td> <div align="left"><xsl:value-of select="//lang_blocks/p18"/></div></td>

      <td> <input type="text" name="mall" size="25"/> </td>
    </tr>
    <tr> 
      <td><xsl:value-of select="//lang_blocks/p19"/></td>
      <td> <input type="text" name="dop" size="25"/> </td>
    </tr>
    <tr> 
      <td><xsl:value-of select="//lang_blocks/p20"/></td>

      <td> <input type="text" name="order" size="25"/> </td>
    </tr>
    <tr> 
      <td><xsl:value-of select="//lang_blocks/p21"/></td>
      <td valign="top"> <input type="text" name="subtotal" size="25"/> </td>
    </tr>
    <tr> 
      <td valign="top"><xsl:value-of select="//lang_blocks/p22"/>
        <b><xsl:value-of select="//lang_blocks/p23"/></b>
        </td>

      <td><textarea name="items" cols="60" rows="12"></textarea></td>
    </tr>
    
  </table>
                          
  <p align="center"> 
    <input type="submit" name="Submit2" value="Submit"/>
    <input type="reset" name="Reset" value="Reset"/>
  </p><br/>
</form>


  </td>

 </tr>
 <tr> </tr>
 <tr></tr>
 <tr></tr>
 </table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>