<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration="yes"
        media-type="text/html"/>
        
<xsl:template match="/">

<root>

	<title_append><xsl:value-of select="//lang_blocks/common/survey_title" /></title_append>
	<user>
		<country><xsl:value-of select="//user/country"/></country>
		<firstname><xsl:value-of select="//user/firstname"/></firstname>
		<top_picks><xsl:value-of select="//user/top_picks"/></top_picks>
		<lastname><xsl:value-of select="//user/lastname"/></lastname>
		<reward_points><xsl:value-of select="//user/reward_points"/></reward_points>
		<membertype><xsl:value-of select="//user/membertype"/></membertype>
		<spid><xsl:value-of select="//user/spid"/></spid>
		<id><xsl:value-of select="//user/id"/></id>
		<alias><xsl:value-of select="//user/alias"/></alias>
		<emailaddress><xsl:value-of select="//user/emailaddress"/></emailaddress>
	</user>


<content>

			<xsl:choose>
				<xsl:when test="//step/thank_you='yes'">
					<p>
						<xsl:choose>
							<xsl:when test="//step/points='0'">
								<xsl:value-of select="//lang_blocks/completed/message_no_points"/><br />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="//lang_blocks/completed/message"/>
								<xsl:text> </xsl:text><a href="http://www.clubshop.com/members/clubcash_details.shtml"><xsl:value-of select="//lang_blocks/completed/uppercase_click_here"/></a><xsl:text> </xsl:text>
								<xsl:value-of select="//lang_blocks/completed/message1"/><br />
							</xsl:otherwise>
						</xsl:choose>
					</p>
				</xsl:when>
				<xsl:when test="//step/login='yes'">
<p>
					 <div class="alert">
					 	<xsl:value-of select="//user/error"/>
					 </div>
					<xsl:value-of select="//lang_blocks/incorrect_mtype_msg"/><br />
					<a href="https://www.clubshop.com/cgi/LoginMembers.cgi?destination=https://www.clubshop.com/mall/other/questionnaire.php"><xsl:value-of select="//lang_blocks/login_here"/></a><br />

</p>

				</xsl:when>
				<xsl:otherwise>

<table style="width: 500px;">

  <tbody>
  <xsl:choose>
	<xsl:when test="//admin/submit = 'NO'">

	 <tr class="b">
		 <td colspan="2">
			Rewards Points: <xsl:value-of select="//admin/rewards_points"/><br />
			Date Completed: <xsl:value-of select="//admin/completed"/><br />
			Date Last Updated: <xsl:value-of select="//admin/updated"/><br />
			Date First Took Questionnaire: <xsl:value-of select="//admin/created"/>
		</td>
	</tr>
	</xsl:when>
	<xsl:otherwise>
      <tr>
      <td colspan="2"><strong><xsl:value-of select="//lang_blocks/common/title"/></strong></td>
    </tr>
        <tr class="b">
      <td colspan="2">

<xsl:choose>
	<xsl:when test="//display_no_points_message = 'yes'">
		<xsl:value-of select="//lang_blocks/common/privacy_disclamer_no_points"/>
	</xsl:when>
	<xsl:otherwise>
<xsl:value-of select="//lang_blocks/common/privacy_disclamer"/>
      	<xsl:text> </xsl:text>
      	<a href="#" onclick="window.open('/privacy.xml');return false;">
      		<xsl:value-of select="//lang_blocks/common/privacy_disclamer_link"/>.
      	</a>

</xsl:otherwise>
</xsl:choose>
      	</td>
    </tr>
	</xsl:otherwise>
  </xsl:choose>


    <xsl:if test="//user_notices/incomplete='yes'">
        <tr>

      <td colspan="2" class="notanswered"><xsl:value-of select="//lang_blocks/notices/incomplete"/></td>

    </tr>
	</xsl:if>
        <tr>

      <td colspan="2"><br /></td>

    </tr>
    <tr class="a">

      <td colspan="2"><strong><xsl:value-of select="//lang_blocks/common/personal_information"/></strong></td>

    </tr>

    <tr class="b">

      <td colspan="2"><xsl:value-of select="//lang_blocks/common/update_personal_info"/><xsl:text> </xsl:text><a href="#" onclick="window.open('https://www.clubshop.com/members/update_profile.shtml');return false;"><xsl:value-of select="//lang_blocks/common/clicking_here"/></a>.</td>

    </tr>

    <tr>

      <td colspan="2"></td>

    </tr>





    <tr class="b">

      <td><xsl:value-of select="//lang_blocks/common/name"/>:</td>

      <td><xsl:value-of select="//user/firstname"/><xsl:text> </xsl:text><xsl:value-of select="//user/lastname"/></td>

    </tr>

    <tr class="b">

      <td valign="top"><xsl:value-of select="//lang_blocks/common/address"/>:</td>
	<td>
      	<xsl:value-of select="//user/address1"/><br />

			<xsl:choose>
				<xsl:when test="//user/address2=''">
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="//user/address2"/><br />
				</xsl:otherwise>
			</xsl:choose>

			<xsl:value-of select="//user/city"/>,<xsl:text> </xsl:text>
			<xsl:value-of select="//user/state"/><xsl:text> </xsl:text>
			<xsl:value-of select="//user/postalcode"/>

	</td>
    </tr>

    <tr class="b">

      <td><xsl:value-of select="//lang_blocks/common/email"/>:</td>

      <td><xsl:value-of select="//user/emailaddress"/></td>

    </tr>

  </tbody>
</table>

<br />
<form action="/cgi/marketing_questionnaires_updated.cgi" method="post">
	<table>
	      <tr class="a">

        <td colspan="2"><b><xsl:value-of select="//lang_blocks/common/survey_title"/></b>
        </td>

      </tr>
	<xsl:for-each select="//lang_blocks/survey/*">
	<tr class="questions">
	<xsl:choose>
				<xsl:when test="question/@answered='no'">
			        <td valign="top" colspan="2" class="notanswered"><xsl:value-of select="question"/></td>
    			</xsl:when>
    			<xsl:otherwise>
    				<td valign="top" colspan="2"><xsl:value-of select="question"/></td>
    			</xsl:otherwise>
    </xsl:choose>



    </tr>
    <tr class="b">
    <td width="20"></td>
        <td>
        	<xsl:for-each select="answers/*">
	       		<xsl:call-template name="input"/>
	       		<br />
	       	</xsl:for-each>
        </td>
    </tr>
	</xsl:for-each>


	      <tr>
        <td colspan="2" align="center" height="20">
        	<xsl:for-each select="//lang_blocks/hidden_survey_info/*">
        		<xsl:call-template name="input"/>
        	</xsl:for-each>
        </td>

      </tr>


	      <tr>
        <td colspan="2" align="center">
        <xsl:choose>
                <xsl:when test="//admin/submit = 'NO'">
                </xsl:when>
                <xsl:otherwise>
        		<input name="step" value="submit" type="submit" />
                </xsl:otherwise>
        </xsl:choose>
        </td>

      </tr>
	</table>
</form>

				</xsl:otherwise>
			</xsl:choose>

</content>
</root>




</xsl:template>

	<xsl:template name="input">

		<xsl:element name="input">

			<xsl:choose>
				<xsl:when test="@type='radio'">
					<xsl:attribute name="type">radio</xsl:attribute>
				</xsl:when>
				<xsl:when test="@type='text'">
					<xsl:attribute name="type">text</xsl:attribute>
				</xsl:when>
				<xsl:when test="@type='password'">
					<xsl:attribute name="type">password</xsl:attribute>
				</xsl:when>
				<xsl:when test="@type='hidden'">
					<xsl:attribute name="type">hidden</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="type">checkbox</xsl:attribute>
				</xsl:otherwise>

			</xsl:choose>

			<xsl:attribute name="name">
				<xsl:value-of select="@question_id" />
			</xsl:attribute>

			<xsl:if test="@element_id!=''">
				<xsl:attribute name="id">
					<xsl:value-of select="@element_id" />
				</xsl:attribute>
			</xsl:if>

			<xsl:attribute name="value">
				<xsl:value-of select="@key" />
			</xsl:attribute>

			<xsl:if test="@default='yes'">
				<xsl:attribute name="checked">checked</xsl:attribute>
			</xsl:if>

			<xsl:if test="@js != ''">
				<xsl:attribute name="onclick">
					<xsl:value-of select="@js" />
				</xsl:attribute>
			</xsl:if>

		</xsl:element>
		<xsl:value-of select="current()" />
	</xsl:template>


</xsl:stylesheet>
