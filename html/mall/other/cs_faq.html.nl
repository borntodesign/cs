﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop: FAQ</title>
<link href="../../css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/cs/_includes/js/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="/cs/_includes/js/jquery-plugins/jquery.iframe.js"></script>


</head>

<body>
<table bgcolor="#001952" border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td height="185"><form enctype="application/x-www-form-urlencoded" id="form1" method="post" name="form1"> 
<table bgcolor="#ffffff" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody>
<tr>
<td width="100%">
<table border="0" cellpadding="1" cellspacing="0" width="100%">
<tbody>
<tr>
<td colspan="2" style="border-bottom: 2px dotted #CECECE;" width="100%"><span class="Blue_Bold">ClubShop FAQ</span></td>
</tr>
<tr>
<td align="left" bgcolor="#e5e5e5" colspan="2" style="border-bottom: 2px dotted #CECECE;">
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" bgcolor="#ffffff" valign="top"><a class="style14" href="#1">CLUBSHOP FAQ</a><br /><a class="style14" href="#2">CLUBSHOP SHOPPING FAQ</a><br /><a class="style14" href="#3">CLUBSHOP REWARDS ONDERNEMER FAQ</a><br /><a class="style14" href="#4">CLUBSHOP NON-PROFIT ORGANISATIE FAQ</a><br /> 
<hr align="left" size="2" />
<a id="12" name="1"><span class="text_orange">CLUBSHOP FAQ</span></a><br /><br /><span class="Blue_Large_Bold">Waar kan ik mijn ClubShop ID nummer en paswoord terugvinden?</span><br /><a class="nav_blue_line" href="https://www.clubshop.com/idlookup.html" target="_blank">https://www.clubshop.com/idlookup.html</a><br /> 
<hr align="left" size="2" width="100%" />
<a id="12" name="2"><span class="text_orange">CLUBSHOP ONLINE WINKELEN FAQ</span></a><br /><br />

<span class="Blue_Large_Bold">Wat is ClubCash?</span><br/>    
Als ClubShop Rewards lid verdient u ClubCash, iedere keer u een geldige online aankoop maakt in de ClubShop Mall, of lokaal winkelt bij een deelnemend ClubShop Rewards handelaar. 

<span class="Blue_Large_Bold">Wat gebeurt er met mijn ClubShop Reward punten?</span><br/>
U blijft die punten behouden en kunt uw totaal inkijken op uw ClubCash Account. ClubCash is enkel de nieuwe term voor ‘Reward punten’.
<br/><br/>

<span class="Blue_Large_Bold">Hoeveel ClubCash heb ik?</span><br/>
Bekijk uw totaalbalans op uw ClubCash Account.<br/><br/>


<span class="Blue_Large_Bold">Ik wil mijn ClubCash verzilveren, hoe ontvang ik ze?</span><br/>
Om uw ClubCash te kunnen ontvangen, biedt ClubShop Rewards de volgende methodes aan:
<ol>
<li>PayPal</li>
<li>Pay Agent (countries without Pay Pal)g</li>
<li>Cheque (enkel US)</li>

</ol>
U selecteert uw voorkeur van uitbetaling wanneer u uw ClubCash wil verzilveren in cashback. 
<br/><br/>







<span class="Blue_Large_Bold">Geven alle ClubShop Rewards handelaren en Mall winkels Reward punten? </span><br />U verdient Reward punten als u online winkelt via de ClubShop Mall. Niet alle offline ClubShop Rewards handelaren bieden Reward punten. Er zullen winkelier Reward punten geven, terwijl u bij andere offline ClubShop Rewards winkeliers kunt genieten van Directe Kortingen op uw aankopen.<br /><br /><span class="Blue_Large_Bold">Hoe worden de Rewards punten berekend?</span><br />Het aantal Reward punten dat u verdient op iedere aankoop, wordt berekend aan de hand van uw totale aankoopbedrag en het percentage dat genoteerd is naast de winkel in de ClubShop Mall winkel index. En voor ieder punt berekend uit het Reward puntenpercentage, verdient u 100 Reward punten, m.a.w. totaal aankoopbedrag, maal x%, maal 100.<br /><br /><span class="Blue_Large_Bold">Ik heb mijn ClubShop online mall order ontvangen of benut, maar de Reward punten zie ik niet verschijnen op mijn Transactie rapport. Wat moet ik doen?</span><br />Gelieve de richtlijnen te volgen op <a class="nav_blue_line" href="https://www.clubshop.com/mall/other/missing_order.shtml" target="_blank">https://www.clubshop.com/mall/other/missing_order.shtml</a><br /><br /><span class="Blue_Large_Bold">Kan ik offline winkelen bij lokale winkels die in de ClubShop Mall te vinden zijn?</span><br />Nee. Als u offline wenst te winkelen, dan kunt u uw keuze maken in de lijst met deelnemende offline <a class="nav_blue_line" href="http://www.clubshop.com/rewardsdirectory/" target="_blank">ClubShop Rewards ondernemers</a>. <br /><br /><span class="Blue_Large_Bold">Hoe verdien ik Reward punten?</span><br />Log in op de <a class="nav_blue_line" href="http://www.clubshop.com/mall/" target="_blank">ClubShop Mall</a> site en doe een online aankoop bij een deelnemende winkel of, laat bij afrekening uw ClubShop Rewards kaart zien aan de deelnemende offline <a class="nav_blue_line" href="http://www.clubshop.com/rewardsdirectory/" target="_blank">ClubShop Rewards winkelier</a>.<br /><br /><span class="Blue_Large_Bold">Wanneer mag ik de Reward punten over mijn aankoop op mijn account verwachten?</span><br />De meeste transacties zullen binnen 5 dagen op uw Transactie rapport verschijnen. In andere gevallen, moet uw order verscheept of benut zijn, voor wij de aankoop kunnen verifiëren. Dit betekent dat voor sommige orders de verificatie en toevoeging tot uw rapport, iets meer tijd vraagt. <br /><br /><span class="Blue_Large_Bold">Wat zijn de instructies of richtlijnen voor het online winkelen in de ClubShop Mall? </span><br />1. U dient in te loggen met uw ClubShop ID nummer. Hebt u nog geen ID nummer, dan wordt u gevraagd een gratis ClubShop lidmaatschap te nemen voor u terug verwezen wordt naar de online winkel. <br />2. U moet cookies toelaten. Alleen zo kunnen wij uw order volgen en de bijbehorende punten toekennen. Bij de meeste computers zijn de cookies reeds ingeschakeld, tenzij u die persoonlijk hebt uitgeschakeld.<br />3. Om online te winkelen en Reward punten te verdienen, gebruik enkel de winkellinks die beschikbaar zijn in de ClubShop Mall. Blijf in die winkel tot na de afrekening en zie erop toe dat u niet wordt doorverwezen naar een andere site. Bij voorbeeld, een website van een bepaalde winkel kan de mogelijkheid geven om het land, taalvoorkeur of munteenheid te kiezen. Hoewel dit een handige faciliteit is, wordt u hierdoor op sommige website doorverwezen naar een andere website. Als dit gebeurt, kunnen wij een correcte tracking en dus de toewijzing van punten niet garanderen.<br /><br /><span class="Orange_Large_11">NOTA: In de meeste ClubShop Mall winkels wordt uw aankoop automatisch getraceerd en aan ons gerapporteerd. Mocht in een bepaalde winkel de automatische optie niet beschikbaar zijn, dan zult u gedetailleerde instructies krijgen na het aanklikken van de winkellink.</span> <br /><br /><span class="Blue_Large_Bold">Ik heb het Missing Order formulier ingevuld en ingediend, maar heb nog steeds de Reward punten op die aankoop niet ontvangen op mijn Transactie rapport account. Waarom niet?</span><br />Mocht het onwaarschijnlijke zich toch voordoen, dan volgen hier enkele redenen waarom een order niet op uw Transactie rapport verschijnt:<br /><br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 1. U hebt onze Online Shopping Instructies niet stap-voor-stap opgevolgd <br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 2. U logde in met een verkeerd ID nummer<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 3. Uw computer bevat software die de manier waarop wij uw order traceren blokkeert <br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 4. Uw login naar onze site werd 'onderbroken' tijdens de sessie<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 5. U had artikelen in uw winkelkarretje, maar handelde de transactie niet volledig af. U keerde in een andere sessie terug om uw order af te handelen<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 6. U bezocht een andere website tijdens uw Clubshop Mall sessie en de andere website verving uw "cookie" door het hunne<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 7. U bestelde artikelen via een promotionele e-mail die u van een handelaar kreeg toegestuurd<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 8. Als u gebruik maakt van Google Check Out, PayPal of 'Bill Me Later' opties, houdt voor de ondernemer meestal daar de tracking op en crediteert ons dus niet voor die aankoop<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 9. Als u rechtsreeks een winkel contacteert en uw order plaatst, wordt uw order niet getraceerd en kunnen er dus geen punten ontvangen worden<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 10. U koos ervoor uw order zelf op te halen in plaats van de gewone verzendoptie<br /><img height="9" src="images/icons/icon_bullet_orannge.png" width="39" /> 11. U voerde een promotie code in die niet door de ClubShop Mall of op de website van de handelaar werd voorzien. In sommige gevallen worden promotionele codes die u werden toegestuurd door een handelaar, niet als een ClubShop aankoop beschouwd.<br /><br /> 
<hr align="left" size="2" />
<a id="12" name="3"><span class="text_orange">CLUBSHOP REWARDS HANDELAAR FAQ</span></a><br /><br /><span class="Blue_Large_Bold">Kan ik als ClubShop Rewards winkelier Reward punten verdienen?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Kan ik als ClubShop Rewards winkelier Referral Commissie verdienen?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Kan ik mijn bedrijfslogo op uw ClubShop Rewards kaart toevoegen?</span><br />Ja, voor een kleine, extra bijbetaling.<br /><br /><span class="Blue_Large_Bold">Kan ik de ClubShop Rewards kaart gebruiken als een klantengetrouwheid-kortingskaart?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Mag ik de ClubShop Rewards kaarten met winst verkopen?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Wat is het laagste kortingspercentage die ik als ClubShop Rewards handelaar kan aanbieden?</span><br />Als u het Platina handelaren pakket opteert, kunt u het laagste kortingspercentage, namelijk 1% korting, aanbieden.<br /><br /><span class="Blue_Large_Bold">Ik heb een online winkel. Kan ik mijn winkel in de winkellijst van de ClubShop Online Mall beschikbaar stellen?</span><br />Deze optie wordt beschikbaar in de toekomst. Doch indien u het Zilver Handelaren Pakket selecteert, krijgt u een directe link naar uw bedrijfswebsite vanuit uw Rewards Ondernemers listing.<br /><br /><span class="Blue_Large_Bold">Hoe kan mijn eerste aankoop bonus verdienen?</span><br />Wanneer u een online aankoop heeft gedaan online bij een aangesloten ClubShop.com site, ontvangt u 500 ClubShop Reward punten op die aankoop. Dit is een eenmalige bonus en zal niet opnieuw worden wanneer u uw aankoop retour stuurt. Deze bonus is niet geldig voor offline bestellingen die gedaan zijn via de ClubShop Rewards Directory. Klik hier voor het inzien van uw <a href="https://www.clubshop.com/cgi/reward_points.cgi" target="_blank">ClubShop Rewards punten rapport</a>.<br /><br /> 
<hr align="left" size="2" />
<a id="12" name="4"><span class="text_orange">CLUBSHOP AFFINITY GROUP FAQ</span></a><br /><br /><span class="Blue_Large_Bold">Kan ik mijn non-profit organisatie gratis laten deelnemen als ClubShop Affinity Groep?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Kan ik als ClubShop Affinity Groep Reward punten verdienen?</span><br />Ja!<br /><br /><span class="Blue_Large_Bold">Kan ik ons non-profit logo laten toevoegen op uw ClubShop Rewards kaart?</span><br />Ja! (Een kleine vergoeding is van toepassing)<br /><br />
<div align="center"><span class="Movie_Header">
<script language="JavaScript1.2" type="text/javascript">// <![CDATA[
var message="HOE DE WERELD WINKELT EN BESPAART!"
var colorone="#CCCCCC"
var colortwo="#FC8B08"
var flashspeed=40  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('<font color="'+colorone+'">')
for (m=0;m<message.length;m++)
document.write('<span id="changecolor'+m+'">'+message.charAt(m)+'</span>')
document.write('</font>')
}
else
document.write(message)
function crossref(number){
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number)
return crossobj
}
function color(){

//Change all letters to base color
if (n==0){
for (m=0;m<message.length;m++)
//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo
if (n<message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",6000)
return
}
}
function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()
// ]]></script>
</span></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" colspan="2"></td>
</tr>
<tr>
<td align="left" colspan="2" height="30"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td></td>
</tr>
</tbody>
</table>
</form></td>
</tr>
</tbody>
</table>
</body>
</html>