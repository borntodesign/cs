<?xml version="1.0"?>
<!--DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<!-- xsl:include href="/xml/head-foot.xsl"/-->


<xsl:template match="/">	
	<html>
		<head>

			<link rel="stylesheet" href="/css/global.css" type="text/css" />
			<link rel="stylesheet" href="/css/storeList.css" type="text/css" />
			<link rel="stylesheet" href="/css/catDrilling.css" type="text/css" />
			<link type="text/css" rel="stylesheet" href="/css/questionnaires.css" />
			<link href="/css/xmall.css" rel="stylesheet" type="text/css" />
			
			<script type="text/javascript">
			<xsl:comment>
			var $MALL_ID = 0;
			var $CATX = "<xsl:value-of select="//lang_blocks/catexpander-txt" />";
			</xsl:comment>
			</script>
			
			<script type="text/javascript" src="/js/mall.js"></script>
			<script type="text/javascript" src="/js/mall-vendor.js"></script>
			<script type="text/javascript" src="/js/yui/yahoo-min.js"></script> 
			<script src="/js/yui/dom-min.js" type="text/javascript"></script>
			<script src="/js/yui/event-min.js" type="text/javascript"></script>
			<script type="text/javascript" src="/js/yui/connection-min.js"></script>
			<script type="text/javascript" src="/js/catDrilling.js"></script>
			<script type="text/javascript" src="/js/miniLogin.js"></script>
			
			<script type="text/javascript" src="/js/questionnaires.js"></script>
			
			<title><xsl:value-of select="concat('Clubshop ', ' - ', //lang_blocks/title_append)" /></title>
			
		</head>
		<body onload="init()">
			<table class="xm_main">
				<thead>
					<tr>
						<td colspan="2" align="center">
						</td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="2" align="center">
						</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td id="main_content">
							<xsl:copy-of select="//root/content/*" />
						</td>
					</tr>
				</tbody>
			</table>
		</body>	
	</html>
</xsl:template>
	
	
</xsl:stylesheet>
