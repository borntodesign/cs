#!/bin/bash
#
for ii in $( ls -d [ABCDEFGHIJKLMNOPQRSTUVWXYZ]* ); do
   if [ $ii != "US" ]; then
      cd /home/httpd/html/mall
      export kk=0
      for jj in $( cat $ii/lang ); do
         if [ $jj = "en" ]; then
             export kk=1
         fi
         echo  Into $ii using language $jj
# makecountry will regenerate all necessary shtml java and css files for the
# country and language passed in
         ./makecountry.sh $ii $jj
         ./updatelocallanguage.sh $ii $jj
      done
      if [ $kk = 0 ]; then
         ./makecountry $ii en
      fi
   fi
done
