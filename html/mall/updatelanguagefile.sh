#!/bin/bash  -v
#
if [ $1 == "en" ];then
   echo "No Language update"
   exit
fi
#
#
echo "Translating from en to " $1 " " `cat translate/$2`
cd translate
    newname=`basename $2 .shtml`
    newbase=`basename $newname .shtml`
    if [ -e $newbase.manual.$1 ]; then
       ln -s $newbase.manual.$1 $newname.$1
       echo "$newname.$1 manual"
    else
       /usr/bin/gtranslate en $1 `cat $2` | \
       sed 's|\\u0026#39;|::|g' | \
       sed 's|\\u0026quot;|::|g' | \
       sed "s/::/'/g" | \
       sed 's|\\u0026amp;|::|g' | \
       sed "s|::|\&amp;|g" > $newname.$1
       echo "$newname.$1"
    fi
    if [ ! -e $newname.en ]; then
       ln -s $2 $newname.en
    fi
cd ..
