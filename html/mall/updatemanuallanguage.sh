#!/bin/bash 
#
#if [ $1 == "en" ];then
#   echo "No Language update"
#   exit
#fi
#
#
cd translate
for file in *
do
    if [ -d $file ]; then
       cd $file
       for file2 in *.shtml
       do
           newname=`basename $file2 .shtml`
           newbase=`basename $newname .shtml`
           if [ -e $newbase.manual.it ]; then
              if [ -e $newname.it ]; then
                 rm $newname.it
              fi
              ln -s $newbase.manual.it $newname.it
           fi
           if [ -e $newbase.manual.fr ]; then
              if [ -e $newname.fr ]; then
                 rm $newname.fr
              fi
              ln -s $newbase.manual.fr $newname.fr
           fi
           if [ -e $newbase.manual.nl ]; then
              if [ -e $newname.nl ]; then
                 rm $newname.nl
              fi
              ln -s $newbase.manual.nl $newname.nl
           fi
           if [ -e $newbase.manual.af ]; then
              if [ -e $newname.af ]; then
                 rm $newname.af
              fi
              ln -s $newbase.manual.af $newname.af
           fi
           if [ -e $newbase.manual.de ]; then
              if [ -e $newname.de ]; then
                 rm $newname.de
              fi
              ln -s $newbase.manual.de $newname.de
           fi
           if [ -e $newbase.manual.el ]; then
              if [ -e $newname.el ]; then
                 rm $newname.el
              fi
              ln -s $newbase.manual.el $newname.el
           fi
           if [ -e $newbase.manual.es ]; then
              if [ -e $newname.es ]; then
                 rm $newname.es
              fi
              ln -s $newbase.manual.es $newname.es
           fi
           if [ -e $newbase.manual.fi ]; then
              if [ -e $newname.fi ]; then
                 rm $newname.fi
              fi
              ln -s $newbase.manual.fi $newname.fi
           fi
           if [ -e $newbase.manual.hr ]; then
              if [ -e $newname.hr ]; then
                 rm $newname.hr
              fi
              ln -s $newbase.manual.hr $newname.hr
           fi
           if [ -e $newbase.manual.pt ]; then
              if [ -e $newname.pt ]; then
                 rm $newname.pt
              fi
              ln -s $newbase.manual.pt $newname.pt
           fi
           if [ -e $newbase.manual.ro ]; then
              if [ -e $newname.ro ]; then
                 rm $newname.ro
              fi
              ln -s $newbase.manual.ro $newname.ro
           fi
           if [ -e $newbase.manual.sl ]; then
              if [ -e $newname.sl ]; then
                 rm $newname.sl
              fi
              ln -s $newbase.manual.sl $newname.sl
           fi
           if [ -e $newbase.manual.sq ]; then
              if [ -e $newname.sq ]; then
                 rm $newname.sq
              fi
              ln -s $newbase.manual.sq $newname.sq
           fi
           if [ -e $newbase.manual.sv ]; then
              if [ -e $newname.sv ]; then
                 rm $newname.sv
              fi
              ln -s $newbase.manual.sv $newname.sv
           fi
           if [ ! -e $newname.en ]; then
              ln -s $file2 $newname.en
           fi
       done
       cd ..
    else
       newname=`basename $file .shtml`
       newbase=`basename $newname .shtml`
       if [ $newname != $file ]; then
          if [ -e $newbase.manual.it ]; then
             if [ -e $newname.it ]; then
                rm $newname.it
             fi
             ln -s $newbase.manual.it $newname.it
             echo "$newname.it manual"
          fi
          if [ -e $newbase.manual.fr ]; then
             if [ -e $newname.fr ]; then
                rm $newname.fr
             fi
             ln -s $newbase.manual.fr $newname.fr
             echo "$newname.fr manual"
          fi
          if [ -e $newbase.manual.nl ]; then
             if [ -e $newname.nl ]; then
                rm $newname.nl
             fi
             ln -s $newbase.manual.nl $newname.nl
             echo "$newname.nl manual"
          fi
          if [ ! -e $newname.en ]; then
             ln -s $file $newname.en
          fi
       fi
    fi
done
