$(document).ready(function()
		{
			$('input.keyword_submit').click(function()
					{
						$('form.keyword_form').submit();
						return false;
					}
			);
			
			$("img.info_icon").each(function()
					{
						$(this).click(function()
							{
								$("p#extra_information" + this.id).toggle('slow');
							}
						);
					}
			);
			
			$("div.select_a_country").click(function()
					{
						$("div.country_scroll").toggle();
						$("div.province_scroll").hide();
						$("div.city_scroll").hide();
					}
			);
			$("div.select_a_country2").click(function()
					{
						$("div.country_scroll").toggle();
					}
			);
			
			
			$("div.select_a_province").click(function()
					{
						$("div.province_scroll").toggle();
						$("div.country_scroll").hide();
						$("div.city_scroll").hide();
					}
			);
			
			$("div.select_a_city").click(function()
					{
						$("div.city_scroll").toggle();
						$("div.country_scroll").hide();
						$("div.province_scroll").hide();
						
					}
			);
			
			$('input.print_button').click(function()
				{
					print();
					return false;
				}
			);
		}
);
