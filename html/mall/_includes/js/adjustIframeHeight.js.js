function getElement(aID) { 
         return(document.getElementById) ? document.getElementById(aID) : document.all[aID];
}
function getIFrameDoc(aID) {
        var rv = null; 
        var frame=getElement(aID);
        // if contentDocument exists, W3C compliant (e.g. Mozilla) 
        if (frame.contentDocument)
            rv = frame.contentDocument;
        else // bad Internet Explorer  ;)
            rv = document.frames[aID].document;
        return rv;
    }
function adjustIFrameHeight() {
         var height = null;
         var frame = getElement("dataframe");
         var frameDoc = getIFrameDoc("dataframe");
//alert(frameDoc.body.offsetHeight);
         if (frameDoc.body.offsetHeight < 150) {
            height = 200;
         }
         else {
            height = frameDoc.body.offsetHeight;
         }
         frame.height = height + 25;
}
