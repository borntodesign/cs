//
// New function for string data
//
String.prototype.capwords = function() {
    return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
};
//
// Global variables
//
    var markers = [];
    var sidehtml = "<ul class=\"side_list\">";
    var infoopen = -1;
//
// If link in right list is picked, trigger a click on the appropriate marker
//
function myclick(num) {
   google.maps.event.trigger(markers[num], 'click');
}
function getmarker (catnum) {
//
// change the marker type for each catagory
//
     var mcolor = "/maps/markers/general_pin.png";
			
     if (catnum == 5000) {
        mcolor = "/maps/markers/activities.png";
     }
     else if (catnum == 5001) {
        mcolor = "/maps/markers/restaurant.png";
     }
     else if (catnum == 5002) {
        mcolor = "/maps/markers/health.png";
     }
     else if (catnum == 5003) {
        mcolor = "/maps/markers/service.png";
     }
     else if (catnum == 5004) {
        mcolor = "/maps/markers/retail.png";
     }
     else if (catnum == 5005) {
        mcolor = "/maps/markers/travel.png";
     }
     else if (catnum == 5006) {
        mcolor = "/maps/markers/school.png";
     }
     return mcolor;					
}
//
// create and open the info window for the appropriate marker, also close the previously opend info window
//
function loadinfowindow(stuff, marker, map) {
     var infowindow = new google.maps.InfoWindow({content: stuff, disableAutoPan: false});
     google.maps.event.addListener(marker, 'click', function() {
                                   if (infoopen != -1) {
                                       infoopen.setMap(null);
                                   }
                                   infowindow.open(map, marker);
                                   infoopen = infowindow;
                                   });
}
//
// Search the keyword list for typed in key word
//
function Checkeyword() {
        var srchval = document.getElementById("search").value;
        sidehtml = "<ul class=\"side_list\">";
        var cookies = cook.split("|");
        var locat = cookies[0].split("^");
        infoopen = -1;
        markers = [];
        var latlng = new google.maps.LatLng(locat[2],locat[1]);
        var myOptions = {
                              zoom: 13,
                              center: latlng,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                         };
        var map2 = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        var marker = new google.maps.Marker({position:latlng, map: map2, title:locat[0], icon:"/maps/markers/smiley_happy.png", shadow:"/maps/markers/shadow.png"});
        for (jj=3; jj<cookies.length; jj++) {
            locat = cookies[jj].split("^");
            for (kk=15;kk<locat.length;kk++) {
                if (locat[kk].toLowerCase() == srchval.toLowerCase() ||
                    locat[0].toLowerCase() == srchval.toLowerCase()) {
                   if (locat[0].toLowerCase() == srchval.toLowerCase()) {
                      kk = locat.length;
                   } 
                   latlng = new google.maps.LatLng(locat[2],locat[1]);
                   var mcolor = getmarker(locat[13]);
                   var capname = locat[0].capwords();
                   marker = new google.maps.Marker({position:latlng, map: map2, title:capname, icon:mcolor});
                   markers.push(marker);
                   var len = markers.length;        
                   var capname2 = capname;       
                   var discount_type = locat[12] * 1;
                   if (discount_type >= 10 && discount_type != 15) {
                       capname = '<a href="http://www.clubshop.com/cgi/setaddressurl.cgi?location_id='+locat[11]+'" target="_blank">'+locat[0].capwords()+'</a>';
                   }
                   var cntg = '<b>'+ capname + '</b><br/>' + locat[5] + '<br/>' + locat[6] + '<br/><br/><b>' + locat[7] + '</b><br/>' + locat[8] + '<br/><br/>' + locat[9] + '<br/>' + locat[10];
                   loadinfowindow(cntg, marker, map2);
                   sidehtml += '<li class="merc_list"><a href="javascript:myclick(' + (len-1) + ')">' + capname2 +'</a></li>';
                }
            }
        }
        sidehtml += "</ul>";
        document.getElementById("side_html").innerHTML = sidehtml;
//
//Check number of merchants found
//
        if (markers.length == 0) {
            var tmpsrch = srchval.split(" ");
            var tmpsrch2 = new Array();
            if (tmpsrch.length > 1) {
                for (ii=0; ii<tmpsrch.length; ii++) {
                    var tmp = tmpsrch[ii].split(" ");
                    if (tmp.length > 1) {
                        for (jj=0; jj<tmp.length; jj++) {
                            tmpsrch2.push(tmp[jj]);
                        }
                    }
                    else {
                        tmpsrch2.push(tmpsrch[ii]);
                    }
                }
            }
            else {
               tmpsrch2.push(tmpsrch[0]);
            }
//
// Search through each merchant and check their keywords against what the user put in.
//
            for (ii=3; ii<cookies.length; ii++) {
                locat = cookies[ii].split("^");
                var doit = 0;
                for (kk=13;kk<locat.length;kk++) {
                    for (jj=0; jj<tmpsrch2.length; jj++) {
                        if (locat[kk].toLowerCase() == tmpsrch2[jj].toLowerCase()) {
                            doit = 1;
                            kk = locat.length;
                            break;
                       }
                    }
                    if (doit == 1) {
                       latlng = new google.maps.LatLng(locat[2],locat[1]);
                       var mcolor = "/maps/markers/general_pin.png";
//
// change the marker type for each catagory
//
            var mcolor = "/maps/markers/general_pin.png";
			
            if (locat[12] == 5000) {
                mcolor = "/maps/markers/activities.png";
            }
	    else if (locat[12] == 5001) {
                mcolor = "/maps/markers/restaurant2.png";
            }
	    else if (locat[12] == 5002) {
                mcolor = "/maps/markers/health.png";
            }
	    else if (locat[12] == 5003) {
                mcolor = "/maps/markers/service.png";
            }
	    else if (locat[12] == 5004) {
                mcolor = "/maps/markers/retail.png";
            }
	    else if (locat[12] == 5005) {
                mcolor = "/maps/markers/travel.png";
            }
	    else if (locat[12] == 5006) {
                mcolor = "/maps/markers/school.png";
            }
					
                       var capname = locat[0].capwords();
                       marker = new google.maps.Marker({position:latlng, map: map2, title:capname, icon:mcolor});
                       markers.push(marker);
                       var len = markers.length;        
                       var capname2 = capname;       
                       var discount_type = locat[12] * 1;
                       if (discount_type >= 10 && discount_type != 15) {
                           capname = '<a href="http://www.clubshop.com/cgi/setaddressurl.cgi?location_id='+locat[11]+'" target="_blank">'+locat[0].capwords()+'</a>';
                       }
                       var cntg = '<b>'+ capname + '</b><br/>' + locat[5] + '<br/>' + locat[6] + '<br/><br/><b>' + locat[7] + '</b><br/>' + locat[8] + '<br/><br/>' + locat[9] + '<br/>' + locat[10];
                       loadinfowindow(cntg, marker, map2);
                       sidehtml += '<li class="merc_list"><a href="javascript:myclick(' + (len-1) + ')">' + capname2 +'</a></li>';
                    }
                }
            }
            sidehtml += "</ul>";
            document.getElementById("side_html").innerHTML = sidehtml;
        }
    }
//
// Display the markers for a given category
//
function Display_Cat_Map( cat ) {
        var ii = cat.selectedIndex;
        var category = cat.children[ii].value - 0;
        sidehtml = "<ul class=\"side_list\">";
        var cookies = cook.split("|");
        var locat = cookies[0].split("^");
        infoopen = -1;
        var subcat1 = new Array();
        var subcat2 = new Array();
        markers = [];
        var latlng = new google.maps.LatLng(locat[2],locat[1]);
        var myOptions = {
                              zoom: 13,
                              center: latlng,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                         };
        var map2 = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        var marker = new google.maps.Marker({position:latlng, map: map2, title:locat[0], icon:"/maps/markers/smiley_happy.png", shadow:"/maps/markers/shadow.png"});
        var sublen=0;
        for (jj=3; jj<cookies.length; jj++) {
            locat = cookies[jj].split("^");
            var catcheck = locat[13] - 0;
            if (catcheck == category) {
               latlng = new google.maps.LatLng(locat[2],locat[1]);
               var capname = locat[0].capwords();
               mcolor = getmarker(locat[13]);
               marker = new google.maps.Marker({position:latlng, map: map2, title:capname, icon:mcolor});
               markers.push(marker);
               var len = markers.length;
               var found = 0;
               var numcat = locat[3] - 0;
               for (kk=0; kk<subcat1.length; kk++) {
                  if (subcat1[kk] == numcat) {
                      found = 1;
                      break;
                  }
               }
               if (! found) {
                   subcat1.push(numcat);
                   subcat2.push(locat[4]);
//                   subcat1[sublen++] = new subcatInfo(locat[3]-0,locat[4]);
               }
               var capname2 = capname;       
               var discount_type = locat[12] * 1;
               if (discount_type >= 10 && discount_type != 15) {
                   capname = '<a href="http://www.clubshop.com/cgi/setaddressurl.cgi?location_id='+locat[11]+'" target="_blank">'+locat[0].capwords()+'</a>';
               }
               var cntg = '<b>'+ capname + '</b><br/>' + locat[5] + '<br/>' + locat[6] + '<br/><br/><b>' + locat[7] + '</b><br/>' + locat[8] + '<br/><br/>' + locat[9] + '<br/>' + locat[10];
               loadinfowindow(cntg, marker, map2);
               sidehtml += '<li class="merc_list"><a href="javascript:myclick(' + (len-1) + ')">' + capname2 + '</a></li>';
			   
            }
        }
        sidehtml += "</ul>";
        document.getElementById("side_html").innerHTML = sidehtml;
        var subcathtml = "<select name=\"Category2\" id=\"category2\" onChange=\"Display_Cat2_Map(this)\">\n";
        subcathtml += "<option value=\"none\" selected=\"yes\">SubCategory List</option>\n";
        for (kk=0; kk<subcat1.length; kk++) {
            subcathtml += "<option value=\""+subcat1[kk]+"\"> "+subcat2[kk]+" </option>\n";
        }
        subcathtml += "</select>\n";
        document.getElementById("category2").innerHTML = subcathtml;
    }
//
// Display the markers for a given category
//
function Display_Cat2_Map( cat ) {
        var ii = cat.selectedIndex;
        var category = cat.children[ii].value - 0;
        sidehtml = "<ul class=\"side_list\">";
        var cookies = cook.split("|");
        var locat = cookies[0].split("^");
        infoopen = -1;
        markers = [];
        var latlng = new google.maps.LatLng(locat[2],locat[1]);
        var myOptions = {
                              zoom: 13,
                              center: latlng,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                         };
        var map2 = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        var marker = new google.maps.Marker({position:latlng, map: map2, title:locat[0], icon:"/maps/markers/smiley_happy.png", shadow:"/maps/markers/shadow.png"});
        for (jj=3; jj<cookies.length; jj++) {
            locat = cookies[jj].split("^");
            var catcheck = locat[3] - 0;
            if (catcheck == category) {
               latlng = new google.maps.LatLng(locat[2],locat[1]);
               mcolor = getmarker(locat[13]);
               var capname = locat[0].capwords();
               marker = new google.maps.Marker({position:latlng, map: map2, title:capname, icon:mcolor});
               markers.push(marker);
               var len = markers.length;        
               var capname2 = capname;       
               var discount_type = locat[12] * 1;
               if (discount_type >= 10 && discount_type != 15) {
                   capname = '<a href="http://www.clubshop.com/cgi/setaddressurl.cgi?location_id='+locat[11]+'" target="_blank">'+locat[0].capwords()+'</a>';
               }
               var cntg = '<b>'+ capname + '</b><br/>' + locat[5] + '<br/>' + locat[6] + '<br/><br/><b>' + locat[7] + '</b><br/>' + locat[8] + '<br/><br/>' + locat[9] + '<br/>' + locat[10];
               loadinfowindow(cntg, marker, map2);
               sidehtml += '<li class="merc_list"><a href="javascript:myclick(' + (len-1) + ')">' + capname2 + '</a></li>';
			   
            }
        }
        sidehtml += "</ul>";
        document.getElementById("side_html").innerHTML = sidehtml;
    }
//
// initialize everything for the main map
//
    function initialize() {
        var bounds = new google.maps.LatLngBounds();
        var cookies = cook.split("|");
        var locat = cookies[0].split("^");
        var latlng = new google.maps.LatLng(locat[2],locat[1]);
        sidehtml = "<ul class=\"side_list\">";
	var myOptions = {
			      zoom: 13,
			      center: latlng,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
	                 };
        var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        var marker = new google.maps.Marker({position:latlng, title:locat[0], icon:"/maps/markers/smiley_happy.png"});
        marker.setMap(map);
        for (ii=3; ii<cookies.length; ii++) {
	    locat = cookies[ii].split("^");
            latlng = new google.maps.LatLng(locat[2],locat[1]);
            var capname = locat[0].capwords();
            var capname2 = capname;       
            var discount_type = locat[12] * 1;
            if (discount_type >= 10 && discount_type != 15) {
                capname = '<a href="http://www.clubshop.com/cgi/setaddressurl.cgi?location_id='+locat[11]+'" target="_blank">'+locat[0].capwords()+'</a>';
            }
            var mshadow = "";
            var newlocat = "<b>"+capname+"</b>" + "<br/>" + locat[5] + "<br/>" + locat[6] + "<br/><br/><b>"+ locat[7]+"</b><br/>"+locat[8]+"<br/><br/>"+locat[9]+"<br/>"+locat[10];
            mcolor = getmarker(locat[13]);
            marker = new google.maps.Marker({position:latlng, map:map, title:capname2, icon:mcolor, shadow:"/maps/markers/shadow.png"});
            loadinfowindow(newlocat, marker, map);
            markers.push(marker);
            var len = markers.length;               
            sidehtml += '<li class="merc_list"><a href="javascript:myclick(' + (len-1) + ')">' + capname2 + '</a></li>';
			
       	}
    locat = cookies[1].split("^");
    latlng = new google.maps.LatLng(locat[2],locat[1]);
    bounds.extend(latlng);
    locat = cookies[2].split("^");
    latlng = new google.maps.LatLng(locat[2],locat[1]);
    sidehtml += "</ul>";
    document.getElementById("side_html").innerHTML = sidehtml;
    bounds.extend(latlng);
    map.fitBounds(bounds);
//
// If the cookies length is 4 then we don't have any vendors
// Create a new division for the pop up box. In that division, add the text entity for the first part,
// the link for going to the merchant signup and the remainder of the text
//
    if (cookies.length == 4) {
        locat = cookies[0].split("^");
        var new_div=document.createElement("div");
        new_div.id="alert_div";
        document.getElementById("main").appendChild(new_div);
        var new_par=document.createElement('p');
        new_par.id = "text";
        document.getElementById("alert_div").appendChild(new_par); 
        var new_txt=document.createTextNode(locat[3]);
        document.getElementById("text").appendChild(new_txt); 
        var new_href=document.createElement('a');
        newtext = document.createTextNode(locat[4]);
        new_href.appendChild(newtext);
        new_href.setAttribute('href',"http://www.clubshop.com/cs/merchant_affiliate_application.shtml");
        new_href.id = "button1";
        new_href.setAttribute('target',"_parent");
        document.getElementById("alert_div").appendChild(new_href);
        var new_par2=document.createElement('p');
        new_par2.id = "text2";
        document.getElementById("alert_div").appendChild(new_par2); 
        var new_txt2=document.createTextNode(locat[5]);
        document.getElementById("text2").appendChild(new_txt2); 
    }
}
