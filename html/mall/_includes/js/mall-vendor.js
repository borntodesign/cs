/*
mall-vendor.js
functions related to displaying various details about the mall stores
ie. store info, blurbs
*/

// some globals
var StoreInfoInitStateLeft = '-2000px';	// where we will move 'hidden' boxes
var StoreInfoRevealRight = '10px';		// how far off the right margin we will place the store info
var storeList;						// will hold a reference to the store list HTML container in the page
var StoreInfoRevealedStateTop = 10;	// how far off the top of the visible list will we set the box
var urlStoreInfo = '/cgi/storeInfoFeed2/';
var urlStoreSpec = '/cgi/storeInfoFeed3/';
var urlStoreGift = '/cgi/storeInfoFeed4/';
var urlStoreCoup = '/cgi/storeInfoFeed5/';
var urlStoreShip = '/cgi/storeInfoFeed6/';
var urlIconFeed = '/cgi-bin/iconFeed/';

function createStoreInfoBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeInfoBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	$E.addListener(infoCloser, 'click', hideStoreInfoBox);
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}
function createStoreSpecBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeSpecBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	$E.addListener(infoCloser, 'click', hideStoreInfoBox);
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}
function createStoreGiftBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeGiftBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	$E.addListener(infoCloser, 'click', hideStoreInfoBox);
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}
function createStoreCoupBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeCoupBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	$E.addListener(infoCloser, 'click', hideStoreInfoBox);
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}
function createStoreShipBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeShipBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	$E.addListener(infoCloser, 'click', hideStoreInfoBox);
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}

function displayStoreInfo(idn){
// since the icons that call this routine are expecting a return value, ie. false,
// so the link doesn't really work, we always need to return false :)

	var idnStr = idn2String(idn);
// idnStr is basically a vendor ID here
	var boxID = 'storeInfoBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
		}
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){

			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreInfoBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreInfo + idn + ',' + $MALL_ID, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function displayStoreGift(idn){
// since the icons that call this routine are expecting a return value, ie. false,
// so the link doesn't really work, we always need to return false :)

	var idnStr = idn2String(idn);
// idnStr is basically a vendor ID here
	var boxID = 'storeGiftBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
//			break;
		}
//		else if (otherBoxes[x].className == 'storeInfoBox'){
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){

			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreGiftBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreGift + idn + ',' + $MALL_ID, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function displayStoreCoup(idn){
// since the icons that call this routine are expecting a return value, ie. false,
// so the link doesn't really work, we always need to return false :)

	var idnStr = idn2String(idn);
// idnStr is basically a vendor ID here
	var boxID = 'storeCoupBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
//			break;
		}
//		else if (otherBoxes[x].className == 'storeInfoBox'){
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){

			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreCoupBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreCoup + idn + ',' + $MALL_ID, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function displayStoreShip(idn){
// since the icons that call this routine are expecting a return value, ie. false,
// so the link doesn't really work, we always need to return false :)

	var idnStr = idn2String(idn);
// idnStr is basically a vendor ID here
	var boxID = 'storeShipBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
//			break;
		}
//		else if (otherBoxes[x].className == 'storeInfoBox'){
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){

			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreShipBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreShip + idn + ',' + $MALL_ID, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function displayStoreSpec(idn){
// since the icons that call this routine are expecting a return value, ie. false,
// so the link doesn't really work, we always need to return false :)

	var idnStr = idn2String(idn);
// idnStr is basically a vendor ID here
	var boxID = 'storeSpecBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
//			break;
		}
//		else if (otherBoxes[x].className == 'storeInfoBox'){
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){

			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreSpecBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreSpec + idn + ',' + $MALL_ID, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function displayStoreX(idn, exttype)
{
	var idnStr = idn2String(idn);

	var boxID = 'storeSpecBox' + idnStr;
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(boxID) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
		}
                else if (otherBoxes[x].className.match(/^storeInfoBox/)){
			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return false; }
	var newBox = createStoreSpecBox(idnStr);
	storeList.appendChild( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", '/cgi/storeInfoFeedX/' + idn + ',' + $MALL_ID + ',' + exttype, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {

	}
	return false;
}

function GeneralResponseFailure(o) {
	alert("There has an unexpected error:\n" + o.responseText);
}

function getScrollingPosition() {
// returns just the scrolling coordinates

  var position = [0, 0];

  if (typeof window.pageYOffset != 'undefined')
  {
    position = [
        window.pageXOffset,
        window.pageYOffset
    ];
  }

  else if (typeof document.documentElement.scrollTop != 'undefined'
      && (document.documentElement.scrollTop > 0 ||
      document.documentElement.scrollLeft > 0))
  {
    position = [
        document.documentElement.scrollLeft,
        document.documentElement.scrollTop
    ];
  }

  else if (typeof document.body.scrollTop != 'undefined')
  {
    position = [
        document.body.scrollLeft,
        document.body.scrollTop
    ];
  }

  return position;
}

function hideStoreInfoBox(e){
// we can receive either an event or a DOM object
	var elm;
	if (! e.type){	// this is the when we are receiving a DOM object
		elm = e;
	} else {
		$E.preventDefault(e);
		elm = $E.getTarget(e).parentNode;
	}
	elm.style.left = StoreInfoInitStateLeft;
	elm.style.right = 'auto';
}

function iconMouseOut(e){
	var elm = $E.getTarget(e);
//console.log('mout: ' +elm.className+' related target: '+$E.getRelatedTarget(e).className);
// if we end up moving to the box, we will be have a mouseout event from that box
	while (elm.nodeName.toUpperCase() != 'TD'){
		elm = elm.parentNode;
	}

	$E.preventDefault(e);
	if (elm.timerMouseOut){ return; }	// we must have been this way before

	window.clearTimeout(elm.timerMouseOver);
	elm.timerMouseOver = undefined;

	var newfunc = function(){ iconMouseOutAction(elm); }
	elm.timerMouseOut = window.setTimeout(newfunc, 500);
}

function iconMouseOutAction(elm){
/*
with mouse events firing crazily,
this item can fire without having the object initialized
*/
	if (elm.iconRoObj){ elm.iconRoObj.hide(); }
}

function iconMouseOver(e){
	var elm = $E.getTarget(e);
//console.log('mover: ' +elm.className);
// if we end up moving to the box, we will be have a mouseover event from that box
	while (elm.nodeName.toUpperCase() != 'TD'){
		elm = elm.parentNode;
	}

	$E.preventDefault(e);
	if (elm.timerMouseOver){ return; }	// we must have been this way before
	window.clearTimeout(elm.timerMouseOut);
	elm.timerMouseOut = undefined;
	
	var newfunc = function(){ iconMouseOverAction(elm, e); }
	elm.timerMouseOver = window.setTimeout(newfunc, 1000);
}

function iconMouseOverAction(elm,evt){
// create the box only if it does not yet exist
	if (! elm.iconRoObj){
		elm.iconRoObj = new IconRolloverObj(elm,evt);
	// normally we will be loading dynamic content, but with the language icon
	// we are using hidden embedded content within the page
		if (elm.className.match(/^icon_/)){
			elm.appendChild(elm.iconRoObj.createBox());
		} else {
			elm.iconRoObj.attachToBox();
			elm.iconRoObj.reveal();
		}

/*
at this point we have a potentially empty container
waiting for the XMLHttpRequest to finish
*/
	}
// otherwise, just reveal it again
	else { elm.iconRoObj.reveal(); }
}

function IconRolloverObj(elm,evt){
	this.evt = evt;
	this.elm = elm;
// calculate these only once
	this.elm.cVals = {X : $D.getX(elm), Y : $D.getY(elm)};
	this.box = undefined;
	this.Xoffset = 10;	// how far horizontally we will offset our rollover box
	this.Yoffset = 10;	// how far vertically we will offset our rollover box
	this.MaxBoxWidth = 200;	// how big do we want to allow this box to be
							// if we define it larger in CSS, then we need to adjust this value

	this.adjustSize = function(){
		if (this.box.offsetWidth > this.MaxBoxWidth){
			this.box.style.width = this.MaxBoxWidth + 'px';
		}
	};

	this.attachToBox = function(){
	// at the moment this is used only by the language icon box
	// the div is created as a child of the td, but is absolute in position and hidden left
		this.box = elm.getElementsByTagName('div')[0];
	};
	
	this.createBox = function(){
		if (! this.box){
			this.box = document.createElement('div');
			this.box.className = this.elm.className + '_rollover';
			var callbackRollover = 
			{
				success: populateRolloverBox,
				failure: GeneralResponseFailure,
				argument: this
			}
			var hash = new Object;
			hash['icon_sale'] = '3';
			hash['icon_coupon'] = '4';
			hash['icon_ship'] = '5';
			
			// the following is the real mccoy
			var idn = idnPartialFromTbody(elm.parentNode.parentNode.id);
			idn += ',' + $MALL_ID + ',' + hash[elm.className];
			var rv = YAHOO.util.Connect.asyncRequest("GET", urlIconFeed + idn, callbackRollover);
//			var rv = YAHOO.util.Connect.asyncRequest("GET", '/cgi-bin/iconFeed/6,1,3', callbackRollover);
			if (rv == null){ alert('XMLHttpRequest failure'); }
			else {

			}
		}
		return this.box;
	};

	this.hide = function(){this.box.style.visibility = 'hidden'};
	this.reveal = function(){
		var Top = this.elm.cVals.Y;
// icon_left is the top row, so we will try placing the box above
// otherwise we will place it below 
		if (this.elm.parentNode.className == 'icon_left'){
	// if the box will appear partly above the top of the screen, 
			if((Top - this.box.offsetHeight) < 0){ Top += this.elm.offsetHeight; }
			else { Top -= this.box.offsetHeight; }
		} else {
			if ((Top + this.box.offsetHeight - 15) > $D.getViewportHeight()){
	// if the box would extend beyond the current bottom of the screen
	// place it instead above the icons
				Top -= this.box.offsetHeight + 20;	// 20 is a buffer so as to not totally obscure the top row
			} else {
	// the 10 is an extra buffer to allow for the cursor hanging		
				Top += this.elm.offsetHeight + 10;
			}
		}
		this.box.style.top = Top + 'px';
// if the box would extend beyond the viewing area on the right, we'll shift it left
// the 25 is a buffer for the right scrollbar
		var scrollXY = getScrollingPosition();
		var setBack = $D.getViewportWidth() - 25 - this.elm.cVals.X - this.box.offsetWidth + scrollXY[0];
		if (setBack > 0){ setBack = 0; }
		this.box.style.left = this.elm.cVals.X + setBack + 'px';
		this.box.style.visibility = 'visible';
	};
}

function idnPartialFromTbody(id){
// our icon tables have a tbody tag with an id like tb123_456
// where 123=vendor_id and 456=mall_id
	id = id.toString();
	return id.match(/\d+/g).join(',');
}

function idn2String(idn){
	idn = idn.toString();
	return idn.replace(/,/g, '');
}

function init_store_icons(){
	var tbls = storeList.getElementsByTagName('table');
	for(var x=0; x < tbls.length; x++){
		var tds = tbls[x].getElementsByTagName('td');
		for (var td=0; td < tds.length; td++){
			if ( tds[td].className.match(/^(icon_|static_icon)/) ){
				//create mouse events
				$E.addListener(tds[td], 'mouseover', iconMouseOver);
				$E.addListener(tds[td], 'mouseout', iconMouseOut);
			}
		}
	}
}

function mall_vendor_init(){
// init procedures specifically related to the needs in these routines
// it is expected that this routine will be called with a more global init routine
// after the page is fully loaded and all the HTML elements are available
	storeList = document.getElementById('storeList') || document.getElementsByTagName('body')[0];
}


function populateRolloverBox(o){
	if (o.status == 200){
		o.argument.box.innerHTML = o.responseText;
	} else {
		o.argument.box.innerHTML = '<p>There has been an error:</p>' + o.responseText;
	}
	init_links(o.argument.box);
	o.argument.adjustSize();
	o.argument.reveal();
}

function populateStoreInfoBox(o){
	if (o.responseXML == null){
		alert("Unanticipated error:\n" + o.responseText);
	} else {
		o.argument[1].innerHTML = o.responseText;
		init_links(o.argument[1]);
		revealStoreInfoBox(o.argument[0]);
	}
}

function revealStoreInfoBox(box){
	var position = getScrollingPosition();
	box.style.top = StoreInfoRevealedStateTop + position[1] + 'px';
	box.style.right = StoreInfoRevealRight;
	box.style.left = 'auto';
}
