/* not sure if this is always used alongside the script delivered by cgi/makeminidee.cgi...
 * so we will check for the existence of the vars that that "script" produces and if they are not present, then we will try based upon the cookie
 */

if (typeof curtag !== 'undefined') {
//   var splittag = curtag.split("|");    splittag is defined if curtag is
   var convrt = splittag[3];
   $("#minidee1").html($languagePack['hello'] + ' ' + convrt + '<br />' + $languagePack['clubcash'] + ' <a class="anchor_login" href="http://www.clubshop.com/members/clubcash_redemptions.shtml">' + splittag[6] + '</a>');
}
else {
	var curtag = Get_Cookie('minidee');
	if (curtag) {
	   var splittag = curtag.split("|");
	   var convrt = Utf8.decode(splittag[3]);	// needed since the cookie will contain multi-bytes if utf-8
	   $("#minidee1").html($languagePack['hello'] + ' ' + convrt + '<br />' + $languagePack['clubcash'] + ' <a class="anchor_login" href="http://www.clubshop.com/members/clubcash_redemptions.shtml">' + splittag[6] + '</a>');
	}
}
	