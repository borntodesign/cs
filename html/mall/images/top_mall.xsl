<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>ClubShop Mall Header</title>

<style type="text/css">

td.a{font-family: Arial, sans-serif, Verdana;
font-size: smaller;
font-weight: bold;
color: #FFFFFF;
background-color: #003366;
}
td.b{
	font-family: Arial, sans-serif;
	font-size: smaller;
	font-weight: bold;
	color: #FFFFFF;
	background-color: #CC3300;
	text-align: center;


}
td.c{
	font-family: Arial, sans-serif;
	font-size: smaller;
	font-weight: bold;
	color: #FFFFFF;
	background-color: #33CC00;
	text-align: center;


}
A {
	text-decoration: underline;
	color: white;
}

A:Hover {
	text-decoration: underline;
	color: #33ff66;
}
</style>
</head>

<body>

<table width="100%"><tr>
<td class="a"><img src="http://www.clubshop.com/mall/images/head_246x46.gif" width="246" height="46" alt="ClubShop Mall" />
</td>
<td class="a"><a href="http://www.clubshop.com/"><xsl:value-of select="//lang_blocks/link1" /></a><br/>
<a href="http://www.clubshop.com/test/mall/mall_rewards.xml/"><xsl:value-of select="//lang_blocks/link2" /></a><br/>
<xsl:value-of select="//lang_blocks/link3" /></td>
<td class="c">Log In Here</td>
</tr>
<tr><td class="b" colspan="3"><img src="http://www.clubshop.com/mall/images/tchest.gif" width="32" height="32" alt="attention!"/>  <xsl:value-of select="//lang_blocks/p1" />
</td></tr>
</table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>