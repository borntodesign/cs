#!/bin/bash -v
#
if [ $1 == "en" ];then
   echo "No Language update"
   exit
fi
#
#
cd $1/translate_local
for file in *.manual.en
do
    newname=`basename $file .en`
    newbase=`basename $newname .manual`
    if [ ! -e $newbase.shtml.shtml ]; then
       cp $file $newbase.shtml.shtml
    fi
done
for file in *.shtml
do
    newname=`basename $file .shtml`
    newbase=`basename $newname .shtml`
    if [ -e $newbase.manual.$2 ]; then
       if [ -e $newname.$2 ]; then
          rm $newname.$2
       fi
       ln -s $newbase.manual.$2 $newname.$2
       echo "$newname.$2 manual"
    else
       if [ -s $file ]; then
          /usr/bin/gtranslate en $2 `cat $file` | \
          sed 's|\\u0026#39;|::|g' | \
          sed "s/::/'/g" | \
          sed 's|\\u0026amp;|::|g' | \
          sed "s|::|\&amp;|g" > $newname.$2
          echo "$newname.$2"
       else
          ln -s $file $newname.$2
       fi
    fi
    if [ ! -e $newname.en ]; then
       ln -s $file $newname.en
    fi
done
cd ../..
