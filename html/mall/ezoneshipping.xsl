<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>e-Zone Shipping</title>

<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

<style type="text/css">
body {background-color: #003399;
}
td{font-family: Verdana, Arial, sans-serif;
font-size: small;
}


</style>

</head>
<body>

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="12" valign="top">
<img src="/images/headers/header_mall_ezone.png" width="1000" height="138" alt="Clubshop header" /></td>
</tr>


<tr>
<td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>
<td colspan="8" bgcolor="#FFFFFF">
<div align="center">
<img src="http://www.clubshop.com/shippingbanner.gif" height="150" width="600" alt="Shipping banner"/>
<br/>
</div>

<p align="left"><img src="/mall/images/ezone.gif" height="96" width="193" alt="e-Zone"/></p>


<p>
<xsl:value-of select="//lang_blocks/p1"/></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p2"/> <xsl:text> </xsl:text> <a href="http://www.clubshop.com/cgi/appx.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></li>
<li><xsl:value-of select="//lang_blocks/p5"/><xsl:text> </xsl:text> <a href="http://www.getezone.com/Rates.aspx" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a>.</li>


<li><xsl:value-of select="//lang_blocks/p8"/></li>
<p><b><xsl:value-of select="//lang_blocks/p9"/></b><br/>
<xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/><br/></p>

<li><xsl:value-of select="//lang_blocks/p12"/></li>
<p><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/ezonefaq.xml" target="_blank"> <xsl:value-of select="//lang_blocks/p14"/></a></p>
</ol>






</td>
 <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
 </tr>
 <tr> <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
 <tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>
 <tr>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 </tr></table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>