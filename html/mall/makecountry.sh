#!/bin/bash
# to run this script ./makecountry.sh country language
#
if [ $1 == "US" ]; then
   echo "Cannot do myself"
   exit
fi
#
# lowercase the country so the flag gets put in correctly
export lcnt=`echo $1 | tr '[A-Z]' '[a-z]'`
#
# Create the directory structure for this country
if [ ! -d /home/httpd/html/mall/$1 ]; then
   mkdir -p /home/httpd/html/mall/$1
   grep $1 country_list > .c
   if [ ! -s .c ]; then
      y=`echo $1 | tr '[:upper:]' '[:lower:]'`
      echo "<$y>$1</$y>" >> country_list
   fi
fi
cd /home/httpd/html/mall/$1
#
# Check to see if this language is already in this country
if [ ! -e lang ]; then
   echo $2 > lang
else
   export inthere=0
   for jj in $( cat lang ); do
      if [ $jj == $2 ]; then
         export inthere=1
      fi
   done
   if [ $inthere != 1 ]; then
      echo $2 >> lang
   fi
fi
#
# Create the links if they don't already exist
if [ ! -e icons ]; then
   ln -s ../../mall/images/icons .
fi
if [ ! -e linksnscripts.shtml.shtml ]; then
   ln -s ../linksnscripts.shtml.shtml .
fi
if [ -e _index.shtml.shtml ]; then
   if [ -L _index.shtml.shtml ]; then
      rm _index.shtml.shtml
   fi
fi
if [ ! -e _index.shtml.shtml ]; then
   ln -s ../US/_index.shtml.shtml .
   if [ ! -e _index.shtml.$2 ]; then
      ln -s _index.shtml.shtml _index.shtml.$2
   else
      ln -s _index.shtml.en _index.shtml.shtml
   fi
fi
if [ ! -e _index.shtml.$2 ]; then
   ln -s _index.shtml.shtml _index.shtml.$2 .
fi
if [ ! -d _includes ]; then
   mkdir _includes
fi
if [ ! -e translate ]; then
   ln -s ../translate .
fi
if [ ! -e translate_local ]; then
   mkdir translate_local
fi
#
# Change the country index page for ther desired language The index.shtml.* should all be links to
# index.shtml.shtml which is itself a link to that country
#
if [ ! -e index.shtml.shtml ]; then
   ln -s ../US/index.shtml.shtml .
   if [ ! -L index.shtml.$2 ]; then
      ln -s index.shtml.shtml index.shtml.$2
   fi
fi
#sed 's/us_flag/'$lcnt'_flag/g' ../US/index.shtml.shtml |\
#sed 's|/mall/US|/mall/'$1'|g' > index.shtml.$2
if [ ! -e index.shtml.shtml ]; then
   ln -s index.shtml.shtml index.shtml.$2
fi
if [ ! -e index.shtml.en ]; then
   ln -s index.shtml.shtml index.shtml.en
else
   if [ ! -L index.shtml.en ]; then
      rm index.shtml.en
      ln -s index.shtml.shtml index.shtml.en
   fi
fi
if [ ! -e index.shtml.it ]; then
   ln -s index.shtml.shtml index.shtml.it
else
   if [ ! -L index.shtml.it ]; then
      rm index.shtml.it
      ln -s index.shtml.shtml index.shtml.it
   fi
fi
if [ ! -e index.shtml.fr ]; then
   ln -s index.shtml.shtml index.shtml.fr
else
   if [ ! -L index.shtml.fr ]; then
      rm index.shtml.fr
      ln -s index.shtml.shtml index.shtml.fr
   fi
fi
if [ ! -e index.shtml.nl ]; then
   ln -s index.shtml.shtml index.shtml.nl
else
   if [ ! -L index.shtml.nl ]; then
      rm index.shtml.nl
      ln -s index.shtml.shtml index.shtml.nl
   fi
fi
#
#
cd _includes
if [ ! -e css ]; then
   ln -s ../../_includes/css .
fi
if [ ! -e cube ]; then
   ln -s ../../../cs/_includes/cube .
fi
if [ ! -e gallery ]; then
   ln -s ../../../cs/_includes/gallery .
fi
if [ ! -e greybox_gallery ]; then
   ln -s ../../_includes/greybox_gallery .
fi
if [ ! -e images ]; then
   ln -s ../../_includes/images .
fi
if [ ! -e js ]; then
   ln -s ../../_includes/js .
fi
if [ ! -e SpryAssets ]; then
   ln -s ../../_includes/SpryAssets .
fi
if [ ! -d pages ]; then
   mkdir pages
fi
#
#
cd pages
#
# Create category menus for the mall this will also create the toppicks files
/home/admin/tools/mall/create-mall-menus.pl
#
if [ ! -e toppicks.shtml.shtml ]; then
    ln -s ../US/toppicks.shtml.shtml .
    ln -s toppicks.shtml.shtml toppicks.shtml.$2
fi
if [ ! -e translate ]; then
   ln -s ../../../translate .
fi
if [ ! -e member_header.shtml.shtml ]; then
   ln -s ../../../_includes/pages/member_header.shtml.shtml .
fi
if [ ! -e member_header.shtml.$2 ]; then
   ln -s ../../../_includes/pages/member_header.shtml.$2 .
fi
if [ ! -e clubshop_copyright.shtml.shtml ]; then
   ln -s ../../../_includes/pages/clubshop_copyright.shtml.shtml .
fi
if [ ! -e clubshop_copyright.shtml.$2 ]; then
   ln -s ../../../_includes/pages/clubshop_copyright.shtml.$2 .
fi
if [ ! -e clubshop_footers.shtml.shtml ]; then
   ln -s ../../../_includes/pages/clubshop_footers.shtml.shtml .
fi
if [ ! -e clubshop_footers.shtml.$2 ]; then
   ln -s ../../../_includes/pages/clubshop_footers.shtml.$2 .
fi
if [ ! -e member_info.shtml.shtml ]; then
   ln -s ../../../_includes/pages/member_info.shtml.shtml .
fi
if [ ! -e member_info.shtml.$2 ]; then
   ln -s ../../../_includes/pages/member_info.shtml.$2 .
fi
if [ ! -e general ]; then
   ln -s /home/httpd/html/cs/_includes/pages/general .
fi
if [ ! -e special_tabs.shtml.shtml ]; then
   ln -s /home/httpd/html/mall/US/_includes/pages/special_tabs.shtml.shtml .
fi
if [ ! -e special_tabs.shtml.$2 ]; then
   ln -s /home/httpd/html/mall/US/_includes/pages/special_tabs.shtml.$2 .
fi
if [ ! -e special_tabs.shtml.en ]; then
   ln -s /home/httpd/html/mall/US/_includes/pages/special_tabs.shtml.en .
fi
if [ ! -d nav ]; then
   mkdir nav
fi
#
#
cd nav
if [ ! -e translate ]; then
   ln -s ../../../../translate .
fi
if [ ! -e login_dropmenu.shtml.shtml ]; then
   ln -s ../../../../_includes/pages/nav/login_dropmenu.shtml.shtml .
fi
if [ ! -e login_dropmenu.shtml.$2 ]; then
   ln -s ../../../../_includes/pages/nav/login_dropmenu.shtml.$2 .
fi
if [ ! -e nologin_dropmenu.shtml.shtml ]; then
   ln -s ../../../../_includes/pages/nav/nologin_dropmenu.shtml.shtml .
fi
if [ ! -e nologin_dropmenu.shtml.$2 ]; then
   ln -s ../../../../_includes/pages/nav/nologin_dropmenu.shtml.$2 .
fi
#
# Create the main index country select pulldowns
#/home/admin/tools/mall/create-mall-countries.pl
#
# Go back to Country menu and link up the directory and category menus
cd ../../..
if [ ! -e mall-submenus.shtml.shtml ]; then
    ln -s ../US/mall-submenus.shtml.shtml .
    ln -s mall-submenus.shtml.shtml mall-submenus.shtml.$2
fi
