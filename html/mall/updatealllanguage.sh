#!/bin/bash 
#
if [ $1 == "en" ];then
   echo "No Language update"
   exit
fi
#
#
cd translate
for file in *.shtml
do
    newname=`basename $file .shtml`
    newbase=`basename $newname .shtml`
    if [ -e $newbase.manual.$1 ]; then
       if [ -e $newname.$1 ]; then
          rm $newname.$1
       fi
       ln -s $newbase.manual.$1 $newname.$1
       echo "$newname.$1 manual"
    else
       /usr/bin/gtranslate en $1 `cat $file` | \
       sed 's|\\u0026#39;|::|g' | \
       sed "s/::/'/g" | \
       sed 's|\\u0026amp;|::|g' | \
       sed "s|::|\&amp;|g" > $newname.$1
       echo "$newname.$1"
       sleep 5
    fi
    if [ ! -e $newname.en ]; then
       ln -s $file $newname.en
    fi
done
cd ..

#
#
# Translate the Javascript
#
cd _includes/js
export hi=`/usr/bin/gtranslate en $1 Hello`
export bal=`/usr/bin/gtranslate en $1 ClubCash Balance`
if [ $1 == "fr" ]; then
   export bal="Solde du ClubCash"
else
   if [ $1 == "it" ]; then
      export bal="Saldo ClubCash"
   fi
fi
/bin/sed 's/Hello/'"$hi"'/g' loadminidee.js.js | \
/bin/sed 's/ClubCash Balance/'"$bal"'/g' > loadminidee.js.$1
#
# 
