#!/bin/bash
#
for ii in $( ls -d [ABCDEFGHIJKLMNOPQRSTUVWXYZ]* ); do
   if [ $ii != "US" ]; then
      chmod 775 $ii
   fi
done
