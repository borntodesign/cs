<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title><!--#include virtual="translate/mall-index-meta.shtml" --></title>
	
<!--#include virtual="linksnscripts.shtml" -->

<style type="text/css">
.desc-icons{height:30px; margin: auto 0px; }
.cat{height:28px; display: block; text-align:center;}
.title{color:#0359B2; font-weight:bold; margin: auto 0px; height: 27px; font-size: 1.0em; padding: 0px 10px;}
img{padding: 0px 4px 2px 4px; vertical-align: -30%;}
</style>
  <meta http-equiv="Expires" CONTENT="-1">
  <meta http-equiv="Cache-Control" CONTENT="no-cache">
  <meta http-equiv="Pragma" CONTENT="no-cache">
  
  <style>

	 
	.container{width:900px; height:145px; display:block; margin:0px auto; position: relative; left:3px;}
	.label{width:160px; height:120px; margin: 10px 0px 0px 10px;float:left;}
	.box1{width:294px; height:145px; float:left; margin: 0px 2px; background:url(/mall/images/special.png) no-repeat;}
	.box2{width:294px; height:145px; float:left; margin: 0px 2px; background:url(/mall/images/survey.png) no-repeat;}
	.box3{width:294px; height:145px; float:left; margin: 0px 2px; background:url(/mall/images/purchase.png) no-repeat;}
	
	.h2text{color: #F68A07; margin: 2px 0px; text-align:center; font-weight:900;font-family:Arial,Helvetica,sans-serif;}
	.ptext {color: #0B5EB1; margin: 2px 0px; text-align:center;font-weight:bold; font-family:Arial,Helvetica,sans-serif;}
	
	a.external:link {color:#F68A07;}      /* unvisited link */
	a.external:visited {color:#F68A07;}  /* visited link */
	a.external:hover {color:#0B5EB1;}  /* mouse over link */
	a.external:active {color:#F68A07;}  /* selected link */
	
</style>
  
  
  
  
</head>

<body>

	<div id="page-wrap">
		
		<div id="header">
<!--#include virtual="_includes/pages/member_header.shtml" -->
<!--#include virtual="_includes/pages/member_info.shtml" -->
<div id="not_logged_in">
<!--#include virtual="_includes/pages/nav/nologin_dropmenu.shtml" -->
</div>
<div id="logged_in">
<!--#include virtual="_includes/pages/nav/login_dropmenu.shtml" -->
</div>

		</div>		
		
		<div id="headbar">
		
<!--#include virtual="mall-submenus.shtml" -->
<a href="/mall/MX"><img id="flag2" alt="" src="/imall/images/flags_ver2/mx_flag.png"></a>
			
		
		</div>
		
		
	
		<div id="main">
			<div id="frameheader" class="main_title">
			   <table width="900px" border="0">
				  <tr>
					 <td width="50" class="Orange_Bold">&nbsp;</td>
					 <td width="250" class="Orange_Bold"><!--#include virtual="translate/onlinestore.shtml" --></td>
					 <td width="110" class="Orange_Bold"><!--#include virtual="translate/rewards.shtml" --></td>
					 <td width="140" class="Orange_Bold"><!--#include virtual="translate/deals.shtml" --></td>
					 <td width="420" class="Orange_Bold"><!--#include virtual="translate/storedetails.shtml" --></td>
				  </tr>
			   </table>
			</div>

			<iframe id="dataframe" name="dataframe" scrolling="auto" src="_index.shtml" align="center" frameBorder="0" style="width:900px; height:405px; border:0; margin: 1em auto;"></iframe>




			<!--#include virtual="/mall/_includes/pages/special_tabs.shtml" -->


			<div id="frametrailer" class="main_title">
			   <div class="center">
				  <div class="desc-icons">
					 <div class="cat">
						 <p class="title"><img src="/mall/icons/icon_directory_new.png" alt="New Store" title="New Store"/>
							<strong><!--#include virtual="translate/new.shtml" --></strong>
							<span style="font-size:80%;"><!--#include virtual="translate/store.shtml" --> | </span>
							<img src="/mall/icons/icon_directory_sale.png" alt="Sale Promotion"  title="Sale Promotion" />
							<strong><!--#include virtual="translate/onlinestore.shtml" --></strong>
							<span style="font-size:80%;"> <!--#include virtual="translate/promotion.shtml" --> | </span>
							<img src="/mall/icons/icon_directory_points.png" alt="ClubCash Increase" title="ClubCash Increase"/>
							<strong><!--#include virtual="translate/clubcash.shtml" --></strong>
							<span style="font-size:80%;"> <!--#include virtual="translate/increase.shtml" --> |</span>
							<img src="/mall/icons/icon_directory_coupon.png" alt="Coupon Code" title="Coupon Code"/>
							<strong><!--#include virtual="translate/coupon.shtml" --></strong> 
							<span style="font-size:80%;"><!--#include virtual="translate/code.shtml" --></span>
						 </p>
					</div>
				</div>
				<div class="desc-icons">
				   <div class="cat">
					  <p class="title">
						 <img src="/mall/icons/icon_directory_gift.png" alt="Gift Card ClubCash Eligible" title="Gift Card ClubCash Eligible" />
						 <strong><!--#include virtual="translate/giftcard.shtml" --></strong>
						 <span style="font-size:80%;"> <!--#include virtual="translate/clubcasheligible.shtml" --> |</span>
						 <img src="/mall/icons/icon_directory_shipping.png" alt="Shipping Special" title="Shipping Special"/>
						 <strong><!--#include virtual="translate/shipping.shtml" --> </strong>
						 <span style="font-size:80%;"><!--#include virtual="translate/special.shtml" --> |</span>
						 <img src="/mall/icons/icon_directory_language.png" alt="Language Options" title="Language Options" />
						 <strong><!--#include virtual="translate/language.shtml" --></strong> 
						 <span style="font-size:80%;"><!--#include virtual="translate/multipleoptions.shtml" --></span>
					  </p>
				   </div>
			   </div>
			</div>
			</div>


		</div>
		
		<div id="bottom_content"></div>
		
<!--#include virtual="_includes/pages/clubshop_footers.shtml" -->
<!--#include virtual="_includes/pages/clubshop_copyright.shtml" -->

	</div>
	
	<!-- Put Google Analytics Code Here -->
<script type="text/javascript" src="_includes/js/loadminidee.js"></script>
<script type="text/javascript" src="_includes/js/loginLink.js"></script>
<script type="text/javascript">
function handleResponse(msg) {
        if (msg == "opensaysme") {
           document.getElementById('frameheader').style.display = 'inline';
           document.getElementById('frametrailer').style.display = 'inline';
        }
        else {
           document.getElementById('frameheader').style.display = 'none';
           document.getElementById('frametrailer').style.display = 'none';
        }
}
</script>
</body>

</html>
