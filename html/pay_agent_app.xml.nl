﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="pay_agent_app.xsl" ?><lang_blocks><p1>Aanmelding Internationale Pay Agent</p1><p2>Naam:</p2><p3>ID nummer:</p3><p4>Pay Level:</p4><p5>(Team Captain of hoger)</p5><p6>Rol:</p6><p7>(Builder of hoger)</p7><p8>Land:</p8><p10>Munteenheid type:</p10><p11>Select your methods of payments you are able to use to send funds to the DHS Club:</p11><p12>Overschrijving ($25.00 kost)</p12><p13>Aanvraag overschrijving</p13><p14>Alert Pay</p14><p15>Account e-mail adres:</p15><dis>Ik heb de bepalingen in het Internationale Pay Agent programma gelezen en verklaar mij akkoord. Ik begrijp dat van mij vereist wordt om, na ontvangst van de fondsen mij overgemaakt door ClubShop Rewards, alle betalingen door te voeren en dit binnen 72 uur. Ik begrijp dat indien ik de bepalingen in het Pay Agent programma niet naleef, dat ik mijn Pay Agent titel en het privilege kan verliezen en indien ik faal in het uitbetalen van de totale verdiensten naar de rechthebbenden toe binnen de 15 dagen, dat ik deze verdiensten zal afgehouden zien van mijn commissies en namens mij zullen overgemaakt worden door ClubShop Rewards. Ik begrijp ook dat indien ik faal in het uitbetalen van de toegewezen en rechthebbende leden, dit kan resulteren in de beëindiging van mijn lidmaatschap.</dis><p17>Handtekening</p17><p18>Datum</p18><p19>EcoCard</p19><p20>Account nummer:</p20>

<p21 new="2/17/15">Transfer Fee Percentage (The percentage of the fee you want to charge to offset your costs to send us funds)</p21>
<p22 new="2/17/15">Credit/Debit Card:</p22>
<p23 new="2/17/15">PayPal:</p23>
<p24 new="2/17/15">Western Union:</p24>

</lang_blocks>
