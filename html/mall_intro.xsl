<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>ClubShop Mall Rewards</title>

<style type="text/css">
body{background:#FFFFFF;}

div.a{font-family: Arial, sans-serif, Verdana;
font-size: smaller;
font-weight: bold;
color: #FFFFFF;

}
p{font-family: Arial, sans-serif, Verdana;
font-size: smaller;

}
td.b{
	font-family:  Arial, sans-serif,Verdana;
	font-size: smaller;
	font-weight: bold;
	color: #FFFFFF;
	background-color: #CC3300;
	text-align: center;

}
A {
	text-decoration: underline;
	color: white;
}

A:Hover {
	text-decoration: underline;
	color: #33ff66;
}

h1{font-family: Georgia, Times New Roman, Times, sans-serif;
color:#FF3300;
font-size: medium; 
}

h2{font-family: Georgia, Times New Roman, Times, sans-serif;
color:#009933;
font-size: medium; 
}

ul{font-family:  Arial,sans-serif,Verdana;
	font-size: smaller;
	color: #000000;
}

</style>
</head>

<body>
<table width="700" cellpadding="10"><tr><td>
<img src="http://www.clubshop.com/mall/images/hunter.gif" width="204" height="185" alt="Bargain Hunter" /><h1><xsl:value-of select="//lang_blocks/p1" /></h1>
</td>
</tr>

<tr><td><h2><xsl:value-of select="//lang_blocks/p2" /></h2>

<p><b><xsl:value-of select="//lang_blocks/p3" /></b> <xsl:value-of select="//lang_blocks/p4" /></p>
<ul><li><xsl:value-of select="//lang_blocks/p5" /><b><xsl:value-of select="//lang_blocks/p6" /></b></li>
<li><xsl:value-of select="//lang_blocks/p7" />(s) ,<b><xsl:value-of select="//lang_blocks/p8" /></b><xsl:value-of select="//lang_blocks/p9" /></li>
<li><xsl:value-of select="//lang_blocks/p10" />(s), <xsl:value-of select="//lang_blocks/p11" /><b><xsl:value-of select="//lang_blocks/p12" /></b></li>
</ul>
</td>
</tr>

</table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet> 