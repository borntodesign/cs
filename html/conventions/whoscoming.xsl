<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "./stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>USA Convention 2006 - Atlanta, Georgia</title>
<link rel="stylesheet" href="./conv2006.css" type="text/css" />
<style type="text/css">
table.wc {border-collapse: collapse; margin-top: 2em;}
td.wc {border-bottom: 1px solid #C7DFDE; padding: 3px 0.5em;}
td.l {border-left: 1px solid #C7DFDE; font-size: 120%; color: white; background-color: #80B4D0;}
td.name {border-left: 1px solid #C7DFDE; font-weight: bold;}
td.role {}
td.loc {}
td.blank {height: 1.5em;}
</style>

</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" /></td></tr><tr>
<td  class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->
<table class="wc">
<xsl:for-each select="//leaders/*">
<xsl:sort select="name"/>
<xsl:variable name = "leader" select = "name()" />
<tr><td class="wc l" colspan="3"><xsl:value-of select="name" /> Team</td></tr>
	
	<xsl:for-each select="//members/*[leader = $leader]">
	<xsl:sort data-type="number" select="level" order="descending" />
	<xsl:sort select="name" />
	<tr><td class="wc name"><xsl:value-of select="name" /></td>
	<td class="wc role"><xsl:value-of select="./role" /></td>
	<td class="wc loc"><xsl:value-of select="concat(state, ', ', country)" /></td></tr>

	</xsl:for-each>
<tr><td colspan="3" class="blank"></td></tr>
</xsl:for-each>
</table>




<!-- ######## variable content stops here ######### -->

<img src="images/breakfastofchamps.jpg" alt="Breakfast of Champs"/>
</td></tr></table></body></html>
</xsl:template>
</xsl:stylesheet>