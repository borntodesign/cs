<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>

<xsl:include href = "./stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>Convention 2006</title>
<link rel="stylesheet" href="./conv2006.css" type="text/css" />


</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" /></td></tr><tr>
<td  class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->

<p><xsl:value-of select="//lang_blocks/p1" /></p>
<table border="1" cellpadding="5">
<tr>
<td>Bessie El Hadrami </td>
<td>bessiehdr@yahoo.com</td>
</tr>
<tr>
<td>Gwen Trullinger </td>
<td>gwent48@yuhknow.com</td>
</tr>
<tr>
<td>Darrell Ervin</td>
<td>blue_ladyndwe@yahoo.com</td>
</tr>

<tr>
<td>Roland Dufault</td>
<td>Rolandd@cox.net</td>

<!--notes-->


</tr></table>


<!-- ######## variable content stops here ######### -->
</td></tr></table></body></html>
</xsl:template>
</xsl:stylesheet>