<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>US Convention 2006 - Atlanta, Georgia</title>
<link rel="stylesheet" href="conv2006.css" type="text/css" />
<style type="text/css">
p{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:smaller;}


 </style>
</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head"/></td></tr><tr>
<td class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->

<p><xsl:value-of select="//lang_blocks/p1" /></p>
<table cellpadding="5"><tr>
<td valign="top"><img src="http://www.clubshop.com/conventions/2006/images/hotel.gif" width="266" height="179" alt="hotel"/></td>
<td valign="top">
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p></td></tr></table>
<p><xsl:value-of select="//lang_blocks/p4" />
<!--<p><xsl:value-of select="//lang_blocks/p5" /></p>-->
<xsl:value-of select="//lang_blocks/p6" /><b><xsl:value-of select="//lang_blocks/p7" /></b> <xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>


<!-- ######## variable content stops here ######### -->
</td></tr></table></body></html>
</xsl:template>
</xsl:stylesheet>