<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>European Convention 2006 - Brussels, Belgium</title>
<link rel="stylesheet" href="conv2006.css" type="text/css" />
<style type="text/css">
p{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:small;}
li{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:small;}

 </style>
</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" 

/></td></tr><tr>
<td class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->

<h3><font color="purple"><xsl:value-of select="//lang_blocks/p1" /></font></h3>
<p><xsl:value-of select="//lang_blocks/p2" /><br/>
<xsl:value-of select="//lang_blocks/p3" />
</p>


<h3><font color="purple"><xsl:value-of select="//lang_blocks/p4" /></font></h3>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p>

<xsl:value-of select="//lang_blocks/p6" /><br/><br/>
<xsl:value-of select="//lang_blocks/p7" /><br/><br/>
<xsl:value-of select="//lang_blocks/p8" /><br/><br/>
<xsl:value-of select="//lang_blocks/p9" /><br/><br/>
<xsl:value-of select="//lang_blocks/p10" /><br/><br/>
<xsl:value-of select="//lang_blocks/p11" /><br/><br/>
<xsl:value-of select="//lang_blocks/p12" /><br/><br/>
<xsl:value-of select="//lang_blocks/p13" /><br/><br/>
<xsl:value-of select="//lang_blocks/p14" /><br/><br/>
<xsl:value-of select="//lang_blocks/p15" /><br/><br/>
<xsl:value-of select="//lang_blocks/p16" /><br/><br/>
<xsl:value-of select="//lang_blocks/p17" /><br/><br/>
<xsl:value-of select="//lang_blocks/p18" /><br/><br/>
<xsl:value-of select="//lang_blocks/p19" /><br/><br/>

</p>

<h3><b><font color="purple"><xsl:value-of select="//lang_blocks/p20" /></font></b></h3>

<p><xsl:value-of select="//lang_blocks/p20a" /><br/><br/>
<xsl:value-of select="//lang_blocks/p21" /><br/><br/>
<xsl:value-of select="//lang_blocks/p22" /><br/><br/>
<xsl:value-of select="//lang_blocks/p23" /><br/><br/>
<xsl:value-of select="//lang_blocks/p24" /><br/><br/>
<xsl:value-of select="//lang_blocks/p25" /><br/><br/>
<xsl:value-of select="//lang_blocks/p26" /><br/><br/>
<xsl:value-of select="//lang_blocks/p27" /><br/><br/>
<xsl:value-of select="//lang_blocks/p28" /><br/><br/>
<xsl:value-of select="//lang_blocks/p29" /><br/><br/>
<xsl:value-of select="//lang_blocks/p30" /><br/><br/>
<xsl:value-of select="//lang_blocks/p31" /><br/><br/>
<xsl:value-of select="//lang_blocks/p32" /><br/><br/>
<xsl:value-of select="//lang_blocks/p33" /><br/><br/>
<xsl:value-of select="//lang_blocks/p34" /><br/><br/>
</p>

<h3><b><font color="purple"><xsl:value-of select="//lang_blocks/p35" /></font></b></h3>
<p><xsl:value-of select="//lang_blocks/p36" /><br/>
<xsl:value-of select="//lang_blocks/p37" /><br/>
</p>




<p><b><xsl:value-of select="//lang_blocks/p47" /></b></p>


<li><xsl:value-of select="//lang_blocks/p48" /></li>
<li><xsl:value-of select="//lang_blocks/p49" /></li>
<li><xsl:value-of select="//lang_blocks/p50" /></li>
<li><xsl:value-of select="//lang_blocks/p51" /></li>
<br/>
<br/>
<hr/>
<br/>

<!-- ######## variable content stops here ######### -->
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

