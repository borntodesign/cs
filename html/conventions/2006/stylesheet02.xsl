<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="head">

<table width="100%" cellpadding="0">
  <tr> 
    <td width="22%" height="130" valign="top" bgcolor="#5CA2A0" id="mytd"><br/><br/>DHS Club USA<br/> Convention<br/> October 5-8,2006</td>
    <td rowspan="2" valign="top" style="width: 54%; background-color: #80B4D0" align="center">
 <img src="/conventions/2006/images/toplobby.gif" width="400" height="172" alt="hotel lobby" />
</td>
    <td width="24%" valign="top" bgcolor="#909DC0" align="center"><br/><img src="/conventions/2006/images/rock.gif" width="120" height="120" alt="rock the world"/></td>
  </tr>
  <tr> 
    <td height="19" valign="top" bgcolor="#909DC0"></td>
    <td valign="top" bgcolor="#5CA2A0"></td>
  </tr>
</table>


</xsl:template>

<xsl:template name="side">

<!--<h4><a href="/conventions/2006/index.xml"><xsl:value-of select="//lang_blocks/lh1"/></a></h4>
<h4><a href="/conventions/2006/hotel.xml"><xsl:value-of select="//lang_blocks/lh2"/></a></h4> 
<h4><a href="/conventions/2006/schedule.xml"><xsl:value-of select="//lang_blocks/lh3"/></a></h4>
<h4><a href="/conventions/2006/whoscoming.xml"><xsl:value-of select="//lang_blocks/lh4"/></a></h4> 
<h4><a href="/conventions/2006/ticketorder_1.xml"><xsl:value-of select="//lang_blocks/lh5"/></a></h4>
<h4><a href="/conventions/2006/roomshare.xml"><xsl:value-of select="//lang_blocks/lh6"/></a></h4>
--><h4><a href="/conventions/2006/testimonials.xml"><xsl:value-of select="//lang_blocks/lh7"/></a></h4>

</xsl:template>

<xsl:template name="caption">
 <!--h2><xsl:value-of select="//lang_blocks/mh1"/></h2-->

</xsl:template>

</xsl:stylesheet>