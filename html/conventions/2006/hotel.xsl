<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:include href="./stylesheet02.xsl" />

   <xsl:template match="/">
      <html>
         <head>
            <title>US Convention 2006 Atlanta, Georgia</title>

            <link rel="stylesheet" href="./conv2006.css" type="text/css" />
          

 
         </head>

         <body>
            <table>
               <tr>
                  <td class="shell" colspan="2">
                     <xsl:call-template name="head" />
                  </td>
               </tr>

               <tr>
                  <td class="shell">
                     <xsl:call-template name="side" />
                  </td>

                  <td class="main">
                     <xsl:call-template name="caption" />

<!-- ######## variable content starts here ######### -->
<table>
<tr>

<td><p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><b><xsl:value-of select="//lang_blocks/p2" />:</b></p>
<p><xsl:value-of select="//lang_blocks/p3" /><b> <a href="http://www.starwoodmeeting.com/StarGroupsWeb/res?id=0606020156&amp;key=69095">
Click Here</a></b><br/><br/>
Or Copy and paste link: <br/>http://www.starwoodmeeting.com/StarGroupsWeb/res?id=0606020156&amp;key=69095
</p>
 

<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
</td>

<td><img src="http://www.clubshop.com/conventions/2006/images/sh_sleep" width="258" height="174" alt="sleeping room"/></td></tr></table>
 
<hr/>


<p><b><xsl:value-of select="//lang_blocks/p13" /></b><br/>
<xsl:value-of select="//lang_blocks/p14" /><br/>
<xsl:value-of select="//lang_blocks/p15" /><br/>
<xsl:value-of select="//lang_blocks/p16" />
</p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<table align="center"><tr><td>
<img src="http://www.clubshop.com/conventions/2006/images/pool.gif" width="253" height="174" alt="pool"/></td>
<td></td>
<td><img src="http://www.clubshop.com/conventions/2006/images/cyber.gif" width="254" height="173" alt="cybercenter"/></td>
</tr></table>




<!-- ######## variable content stops here ######### -->
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

