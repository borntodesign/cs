<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>USA Convention 2006 - Atlanta, Georgia</title>
<link rel="stylesheet" href="conv2006.css" type="text/css" />
<style type="text/css">
p{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:small;}
li{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:small;}

 </style>
</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" 

/></td></tr><tr>
<td class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->

<h3><b><font color="purple"><xsl:value-of select="//lang_blocks/p1" /></font></b></h3>
<p><a href="http://www.clubshop.com/manual/training/conferences/conftestform.html"><xsl:value-of select="//lang_blocks/test" /></a></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /><br/><xsl:value-of select="//lang_blocks/p16" /></p>
<p><xsl:value-of select="//lang_blocks/p17" /></p>

<hr/>

<p><xsl:value-of select="//lang_blocks/p18" /></p>
<p><xsl:value-of select="//lang_blocks/p19" /></p>
<p><xsl:value-of select="//lang_blocks/p20" /> <xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p28" /></p>
<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p32" /></p>
<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p><xsl:value-of select="//lang_blocks/p35" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p36" /></p>
<p><xsl:value-of select="//lang_blocks/p37" /></p>
<p><xsl:value-of select="//lang_blocks/p38" /></p>
<p><xsl:value-of select="//lang_blocks/p39" /></p>
<p><xsl:value-of select="//lang_blocks/p40" /></p>
<p><xsl:value-of select="//lang_blocks/p41" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p42" /></p>
<p><xsl:value-of select="//lang_blocks/p43" /></p>
<p><xsl:value-of select="//lang_blocks/p44" /></p>
<p><xsl:value-of select="//lang_blocks/p45" /></p>
<p><xsl:value-of select="//lang_blocks/p46" /></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>
<hr/>


<p><xsl:value-of select="//lang_blocks/p48" /></p>
<p><xsl:value-of select="//lang_blocks/p49" /></p>
<p><xsl:value-of select="//lang_blocks/p50" /></p>
<p><xsl:value-of select="//lang_blocks/p51" /></p>
<p><xsl:value-of select="//lang_blocks/p52" /></p>
<p><xsl:value-of select="//lang_blocks/p53" /></p>
<p><xsl:value-of select="//lang_blocks/p54" /></p>
<p><xsl:value-of select="//lang_blocks/p55" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p><xsl:value-of select="//lang_blocks/p57" /></p>
<p><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
<p><xsl:value-of select="//lang_blocks/p60" /></p>
<p><xsl:value-of select="//lang_blocks/p61" /></p>
<p><xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p63" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p64" /></p>
<p><xsl:value-of select="//lang_blocks/p65" /></p>
<p><xsl:value-of select="//lang_blocks/p66" /></p>
<p><xsl:value-of select="//lang_blocks/p67" /></p>
<p><xsl:value-of select="//lang_blocks/p68" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p69" /></p>
<p><xsl:value-of select="//lang_blocks/p70" /></p>
<p><xsl:value-of select="//lang_blocks/p71" /></p>
<p><xsl:value-of select="//lang_blocks/p72" /></p>
<p><xsl:value-of select="//lang_blocks/p73" /></p>
<p><xsl:value-of select="//lang_blocks/p74" /></p>
<p><xsl:value-of select="//lang_blocks/p75" /></p>
<p><xsl:value-of select="//lang_blocks/p76" /></p>
<p><xsl:value-of select="//lang_blocks/p77" /></p>
<hr/>


<p><xsl:value-of select="//lang_blocks/p78" /></p>
<p><xsl:value-of select="//lang_blocks/p79" /></p>
<p><xsl:value-of select="//lang_blocks/p80" /></p>
<p><xsl:value-of select="//lang_blocks/p81" /></p>
<p><xsl:value-of select="//lang_blocks/p82" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p83" /></p>
<p><xsl:value-of select="//lang_blocks/p84" /></p>
<p><xsl:value-of select="//lang_blocks/p85" /></p>
<p><xsl:value-of select="//lang_blocks/p86" /></p>
<p><xsl:value-of select="//lang_blocks/p87" /></p>
<p><xsl:value-of select="//lang_blocks/p88" /></p>
<p><xsl:value-of select="//lang_blocks/p89" /></p>
<p><xsl:value-of select="//lang_blocks/p90" /></p>
<p><xsl:value-of select="//lang_blocks/p91" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p92" /></p>
<p><xsl:value-of select="//lang_blocks/p93" /></p>
<p><xsl:value-of select="//lang_blocks/p94" /></p>
<p><xsl:value-of select="//lang_blocks/p95" /></p>
<p><xsl:value-of select="//lang_blocks/p96" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p97" /></p>
<p><xsl:value-of select="//lang_blocks/p98" /></p>
<p><xsl:value-of select="//lang_blocks/p99" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p100" /></p>
<p><xsl:value-of select="//lang_blocks/p101" /></p>
<p><xsl:value-of select="//lang_blocks/p102" /></p>
<p><xsl:value-of select="//lang_blocks/p103" /></p>
<p><xsl:value-of select="//lang_blocks/p104" /></p>
<p><xsl:value-of select="//lang_blocks/p105" /></p>
<p><xsl:value-of select="//lang_blocks/p106" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p107" /></p>
<p><xsl:value-of select="//lang_blocks/p108" /></p>
<p><xsl:value-of select="//lang_blocks/p109" /></p>
<p><xsl:value-of select="//lang_blocks/p110" /></p>
<p><xsl:value-of select="//lang_blocks/p111" /></p>
<p><xsl:value-of select="//lang_blocks/p112" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p113" /></p>
<p><xsl:value-of select="//lang_blocks/p114" /></p>
<p><xsl:value-of select="//lang_blocks/p115" /></p>
<p><xsl:value-of select="//lang_blocks/p116" /></p>
<p><xsl:value-of select="//lang_blocks/p117" /></p>
<p><xsl:value-of select="//lang_blocks/p118" /></p>
<p><xsl:value-of select="//lang_blocks/p119" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p120" /></p>
<p><xsl:value-of select="//lang_blocks/p121" /></p>
<p><xsl:value-of select="//lang_blocks/p122" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p123" /></p>
<p><xsl:value-of select="//lang_blocks/p124" /></p>
<p><xsl:value-of select="//lang_blocks/p125" /></p>
<p><xsl:value-of select="//lang_blocks/p126" /></p>
<p><xsl:value-of select="//lang_blocks/p127" /></p>
<p><xsl:value-of select="//lang_blocks/p128" /></p>
<p><xsl:value-of select="//lang_blocks/p129" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p130" /></p>
<p><xsl:value-of select="//lang_blocks/p131" /></p>
<p><xsl:value-of select="//lang_blocks/p132" /></p>
<p><xsl:value-of select="//lang_blocks/p134" /></p>

<hr/>

<p><xsl:value-of select="//lang_blocks/p135" /></p>
<p><xsl:value-of select="//lang_blocks/p136" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p137" /></p>
<p><xsl:value-of select="//lang_blocks/p138" /></p>
<p><xsl:value-of select="//lang_blocks/p139" /></p>
<p><xsl:value-of select="//lang_blocks/p140" /></p>
<p><xsl:value-of select="//lang_blocks/p141" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p142" /></p>
<p><xsl:value-of select="//lang_blocks/p143" /></p>
<p><xsl:value-of select="//lang_blocks/p144" /></p>
<p><xsl:value-of select="//lang_blocks/p145" /></p>
<p><xsl:value-of select="//lang_blocks/p146" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p147" /></p>
<p><xsl:value-of select="//lang_blocks/p148" /></p>
<p><xsl:value-of select="//lang_blocks/p149" /></p>
<p><xsl:value-of select="//lang_blocks/p150" /></p>
<p><xsl:value-of select="//lang_blocks/p151" /></p>
<p><xsl:value-of select="//lang_blocks/p152" /></p>
<p><xsl:value-of select="//lang_blocks/p153" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p154" /></p>
<p><xsl:value-of select="//lang_blocks/p155" /></p>
<p><xsl:value-of select="//lang_blocks/p156" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p157" /></p>
<p><xsl:value-of select="//lang_blocks/p158" /></p>
<p><xsl:value-of select="//lang_blocks/p159" /></p>
<p><xsl:value-of select="//lang_blocks/p160" /></p>


<!-- ######## variable content stops here ######### -->
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

