<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>

<xsl:include href = "./stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>US Convention 2006 - Atlanta, Georgia</title>
<link rel="stylesheet" href="./conv2006.css" type="text/css" />
<style>
h2{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 border-bottom: 0.1 em solid #F33;}

td.lbl {font-weight: bold; text-align: left;}
tr.required td.lbl {background: #D8DCE9;
	border-left: 0.5em solid #4D8885;
}
td.lbl {background: #D8DCE9; border-left: 0.5em solid #4D8885;}
div#submitArea {text-align: center; margin-top: 1em; padding-top: 1em;}
input:focus {background: yellow;}

 </style>
</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" /></td></tr><tr>
<td class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->
<form action="/cgi/FormMail.cgi" method="post">
            
                <input name="recipient" type="hidden" value="sales@dhs-club.com;acf1@dhs-club.com" />
                <input name="email" type="hidden" value="apache@clubshop.com" />
                
  <input name="required" type="hidden" value="name,id,email,exec,amt,credit_card_type,cardnumber,expiredate,cvv2,cardholder,cardholdersstaddr,cardholderscity,cardholdersst,cardholderszip,cardholderscountry" />
                <input name="sort" type="hidden" value="order:name,id,email,exec,guest,amt,credit_card_type,cardnumber,expiredate,cvv2,cardholder,cardholdersstaddr,cardholderscity,cardholdersst,cardholderszip,cardholderscountry,bankname,accountname,accaddress,acccity,accstate,acczip,routenumber,accountnumber,Comm_deduction" />
                <input name="redirect" type="hidden" value="http://www.clubshop.com/conventions/2006/thankyou.xml" />
                
  <input name="subject" type="hidden" value="US Convention Ticket Order - Atlanta 2006" />
                <input name="sort" type="hidden" />
                <input name="env_report" type="hidden" value="HTTP_USER_AGENT" />



<p><b><xsl:value-of select="//lang_blocks/hmain" /></b></p>


<p><a href="http://www.clubshop.com/conventions/2006/conv2006-by-prepay.html"><xsl:value-of select="//lang_blocks/p1" /></a></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<table cellspacing="0">

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label1" /></td>
<td><input type="text" name="name" size="50" maxlength="50"/></td>
</tr>

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label2" /></td>
<td><input type="text" name="id" size="50" maxlength="50"/></td>
</tr>

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label3" /></td>
<td><input type="text" name="email" size="50" maxlength="50"/></td>
</tr>

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label5" /></td>
<td><input type="text" name="exec" size="50" maxlength="50"/></td>
</tr>

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label6" /></td>
<td>
<input type="text" name="guest" size="50" maxlength="50"/>
</td>
</tr>

<tr class="required">
<td class="lbl"><xsl:value-of select="//lang_blocks/label7" /></td>
<td>
<input type="text" name="amt" size="50" maxlength="50"/>
</td>
</tr>

<tr><td colspan="2"><hr />
</td></tr>

<tr>
      <td class="lbl"><b><xsl:value-of select="//lang_blocks/cch" /></b></td><td></td>
    </tr>
<tr>

<td class="lbl"><xsl:value-of select="//lang_blocks/ccc" /></td><td></td></tr>
<tr>
<td colspan="2">
<input type="radio" name="credit_card_type" value="MasterCard" /><xsl:value-of select="//lang_blocks/cmc" />
<input type="radio" name="credit_card_type" value="Visa" /><xsl:value-of select="//lang_blocks/cvi" />
<input type="radio" name="credit_card_type" value="AmEx" /><xsl:value-of select="//lang_blocks/cax" />
<input type="radio" name="credit_card_type" value="Discover" /><xsl:value-of select="//lang_blocks/cdc" />
</td>
</tr>

<tr> 
<td class="lbl"><xsl:value-of select="//lang_blocks/clabel1" /></td>
<td class="inp"> <input type="text" name="cardnumber" /></td>
</tr>
<tr> 
<td class="lbl" ><xsl:value-of select="//lang_blocks/clabel2" /></td>
<td> 
<input type="text" name="expiredate" />
</td>
</tr>
<tr>
                             
<td class="lbl"><xsl:value-of select="//lang_blocks/clabel3" />
<a href="https://www.clubshop.com/help/cvv2.html" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;">(help)</a>
</td>

<td > 
<input type="text" name="cvv2" />
</td>
               
</tr>
<tr> 
<td class="lbl" ><xsl:value-of select="//lang_blocks/clabel4" /></td>
<td><input type="text" name="cardholder" />
</td>
</tr>

<tr> 
<td  class="lbl"><xsl:value-of select="//lang_blocks/clabel5" /></td>
<td> <input type="text" name="cardholdersstaddr" /> </td>
</tr>
                        
<tr> 
<td class="lbl"><xsl:value-of select="//lang_blocks/clabel6" /></td>
<td> <input type="text" name="cardholderscity" /> </td>
</tr>
<tr> 
<td class="lbl" ><xsl:value-of select="//lang_blocks/clabel7" /></td>
<td> <input type="text" name="cardholdersst" /> </td>
</tr>
<tr> 
<td class="lbl"><xsl:value-of select="//lang_blocks/clabel8" /></td>
<td> <input type="text" name="cardholderszip" /> </td>
</tr>
<tr> 
<td class="lbl"><xsl:value-of select="//lang_blocks/clabel9" /></td>
<td> <input type="text" name="cardholderscountry" /> </td>
</tr>
<tr><td colspan="2"><hr/>
</td></tr>

<tr><td> </td></tr>
<tr><td colspan="2"><xsl:value-of select="//lang_blocks/bh" /><br />

<b><xsl:value-of select="//lang_blocks/bha" /></b></td></tr>



</table>
  <div id="submitArea"><input name="submit" type="submit" value="Purchase Convention Tickets!"/>
<br />
</div></form>


<!-- ######## variable content stops here ######### -->
</td></tr></table></body></html>
</xsl:template>
</xsl:stylesheet>