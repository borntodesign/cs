﻿<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
<head>
<title>KWALIFICEER ALS PARTNER</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation2.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/general.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<!-- <script src="http://www.clubshop.com/js/partner/app.js"></script>-->
<script src="http://www.clubshop.com/js/partner/flash.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'> 
<script type="text/javascript" src="http://www.clubshop.com/js/transpose.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/Currency.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/cgi/transposeMEvarsJS.cgi"></script> 

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 

<style>
.larger_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 14px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #003399;
	font-weight: bold;
}

.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}

.special{
font-family: 'Asul', sans-serif; 
font-size: 16px;}

.special2{
font-family: 'Asul', sans-serif; 
font-size: 14px;}

}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" style=" margin:5px; " height=" 84px " width=" 288px "></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns"><br />
<h4 class="topTitle"></h4>
<hr />
<div align="center"><span class="largehead_blue_bold">KWALIFICEER ALS PARTNER</span></div>
				     
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
      <p>Zodra u in een maand 12 Pay punten hebt, kwalificeert u automatisch als 
        Partner en hebt u het privilege om Affiliates, Partners, Non-profit organisaties 
        en Winkels/ bedrijven te verwijzen en kunt u Partner Inkomen verdienen. 
        Er zijn twee manieren waarop u kunt dit doen:</p>
<ol>
        <li><span class="larger_blue_bolder">KWALIFICEER LATER</span> - door het 
          doen van genoeg persoonlijke aankopen en het verwijzen van voldoende 
          Members die aankopen doen, zodat u 12 Pay punten per maand verdient 
          van deze aankopen.</li>
        <li><span class="larger_blue_bolder">KWALIFICEER NU</span> - door een GPS abonnement 
          te kopen en alle voordelen te krijgen waarin onder andere ondersteuning 
          is opgenomen om u te helpen een succesvolle Partner ClubShop Rewards 
          zaak uit te bouwen. </li>
</ol>
      <p>Hoewel het abonneren op GPS niet vereist is om te kwalificeren als Partner, 
        abonneren meer dan 90% van onze Partners op GPS om toegang te hebben tot 
        de vele voordelen die het biedt.</p>
</div>
</div>
<div class="row">
	<div class="eight columns">
			<div class="eight columns centered">
<br/>
			<img src="http://www.clubshop.com/manual/gps/gps-box-version5.png" height="" width="" alt="gps">

		</div></div>

	<div class="four columns">
			<div class="four columns push-one">
			<br><br><br><br><br>
			
			
        <p><a href="https://www.clubshop.com/cgi/appx.cgi/0/upg" target="_blank" class="nice large blue button">ABONNEREN 
          op GPS »</a></p>
			
			</div></div>
	</div>
	
	<div class="row">
	<div class="twelve columns">
		<div class="twelve columns push-three">
		<br><br><p>Voor meer informatie over een abonnement op GPS</p>
<br /> <a class="nice medium green button" href="gps_new.html" target="_blank">KLIK HIER »</a></div>
</div>
</div>
<br /><br /><br /><br /><br /><br /><br /></div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2014           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>