<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:template match="/">
<html>
<head>
   <title><xsl:value-of select="//lang_blocks/title" /></title>
</head>
<body text="#000000" bgcolor="#FFFFFF" style="font-family: Arial, Helvetica, sans-serif;">
 
<div align="center"><img src="/images/mast3.gif" alt="" width="780" height="60" /></div>

<p style="text-align:left; border:1px double gray; padding:1em; width:700px; margin:auto; margin-top:1em;">
<xsl:value-of select="//lang_blocks/line1" />.<br />
<xsl:value-of select="//lang_blocks/line2" />.<br />
<xsl:value-of select="//lang_blocks/line3" />.
</p>

</body>
</html>
</xsl:template>
</xsl:stylesheet>