<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p2" /></title>
<!--<script type="text/javascript" src="/js/gi/blurb.js"></script>
<script type="text/javascript" src="/js/gi/banner.js"></script>-->

<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->

<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container blue">
	<div class="row">
		<div class="six columns">

<script type="text/javascript">
<xsl:comment>
DoIt();
//</xsl:comment>
</script>


<a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /> <xsl:text> </xsl:text>
   <!--<xsl:value-of select="//lang_blocks/p5" /> <xsl:text> </xsl:text>
   <xsl:value-of select="//lang_blocks/p6" /> <xsl:text> </xsl:text>-->
   <xsl:value-of select="//lang_blocks/p7" /> <xsl:text> </xsl:text>
   <a href="https://www.clubshop.com/p/index.shtml
">https://www.clubshop.com/p/index.shtml
</a> </p>
   <!--<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9" /> </p>-->
   
<p class="a"><xsl:value-of select="//lang_blocks/p10" /></p>  
<br/>  <br/><br/><br/><br/>  

<hr/>
    <br/>  <br/><br/><br/><br/>  <br/>  <br/><br/><br/><br/>  <br/>  <br/><br/><br/><br/>  <br/>  <br/>     
     
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>





