<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" 
 encoding="utf-8" 	
 doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" 	
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 	
 omit-xml-declaration="yes" 	media-type="text/html"/> 
 <xsl:param name="alias" />	  
 <xsl:template match="/">       
          
<html><head>
            <title>Partner Upgrade Pending - Receipt of Funds</title>

<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
    
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"></h4>

	<hr />
		<span class="style28"><xsl:value-of select="//lang_blocks/p1a"/></span>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



 <p><xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/pay2-1" /> $79.95 (USD) <xsl:value-of select="//lang_blocks/pay2-2" />: 
 <blockquote> <xsl:for-each select="//lang_blocks/p2/*"><xsl:value-of select="." /><br /> </xsl:for-each></blockquote>
   <p><xsl:value-of select="//lang_blocks/p8" /></p>
   <b><xsl:value-of select="//lang_blocks/p3" />
<!-- we have no parameters to use
: <xsl:value-of select="$alias" />
-->
</b></p>

<p><b><xsl:value-of select="//lang_blocks/p4" />: <xsl:value-of select="//lang_blocks/p5" /></b></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p class="a"><xsl:value-of select="//lang_blocks/p7" /></p>   

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>



     
<hr/>
       

     
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>







