<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="congrats1.xsl" ?>
<lang_blocks>
<p1>Van harte gefeliciteerd dat u Partner bent geworden!</p1>

<p2 new="4/1/12">Door het aankopen van een GPS abonnement, bent u nu gekalificeerd als een ClubShop Rewards Partner en heeft u de eerste stap gezet naar het creëren van een succesvolle ClubShop Rewards zaak</p2>
<p3>Behalve dat u vanuit het ClubShop Rewards hoofdkantoor van ons een e-mail zult ontvangen, zal er ook contact met u worden opgenomen door één of meerdere van uw Partner team coaches om u te verwelkomen en u te helpen van start te gaan.</p3>

<p4 new="4/1/12">Tot het zover is, kunt u beginnen om vertrouwd te raken met uw Partner Business Center en alle voordelen die u worden geboden in het GPS.</p4>
<!--<p5>Neem even de tijd en klik op de verschillende links om vertrouwt te raken met dit rapport.</p5>
<p8 description="This phrase is hyperlinked and reads in a sentence like this: CLICK HERE to go to your organization report">KLIK HIER</p8>
<p9>om naar uw Organisatie Rapport te gaan.</p9>-->
<p10>Van harte gefeliciteerd dat u een ClubShop Rewards Partner bent geworden!</p10>
</lang_blocks>



