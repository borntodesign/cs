<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<base>
		
		<user><xsl:copy-of select = "//minidee/*"/></user>
		<xsl:copy-of select = "//html"/>
		<xsl:copy-of select = "document('/xml/head-foot.xml')"/>
	</base>
	</xsl:template>
</xsl:stylesheet>
