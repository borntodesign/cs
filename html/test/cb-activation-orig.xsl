<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
	<xsl:template match="/">
	
<html><head>
<base href="http://www.clubshop.com/" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="/lang_blocks/title" /></title>


<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
      <tr>
        <td valign="top"><table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="12" valign="top"><img src="/images/headers/plain-clubshop-1000x138.png" width="1000" height="138" /></td>
          </tr>
          
          <tr>
            <td colspan="2" background="/images/bg/bg_left.png"></td>

            <td colspan="8" bgcolor="#FFFFFF">
            <p><xsl:value-of select="/lang_blocks/p1" /></p>
			<p><xsl:value-of select="/lang_blocks/p2" /></p>
			
            <form action="http://www.clubshop.com/cgi-bin/activate" method="post" onsubmit="return submitCheck()" style="text-align:center;">

                            <input name="cbid" value="" class="registration_TextField" size="20" type="text" />
                            <input type="submit" name="submit" style="margin-left:0.5em; font-size:12px;">
                            <xsl:attribute  name = "value" ><xsl:value-of select="/lang_blocks/submit-activate" /></xsl:attribute>
                            </input>
   

            </form>
              
</td>
            <td colspan="2" background="/images/bg/bg_right.png"></td>
          </tr>
          <tr>
            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td>
          </tr>
          <tr>

            <td height="134" colspan="12" background="/images/general/footerbar.png"></td>
          </tr>
          <tr>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
          </tr>
        </table></td>
      </tr>

      
    </table>
    
</body>
</html>

	</xsl:template>

</xsl:stylesheet>