<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">

<html>
<head>
<title>Glocal Income VIP Welcome Email</title>
<link href="http://www.glocalincome.com/css/gi_pages.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
body{background-color: #173A16;}

a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
</style>
</head>



<body bgcolor="#245923">
<table width="67%" border="0" align="center" cellpadding="3" cellspacing="10" bgcolor="#245923">
  <tr>

    <td height="479" valign="top" bgcolor="#FFBC23" class="Text_Content"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
      
      
      
      
      
<tr>
<td background="http://www.glocalincome.com/images/bg_header.jpg"><img src="http://www.glocalincome.com/images/gi.jpg" width="640" height="70" /></td>
</tr>
      
<tr>
<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">



<tr>
                <td valign="top"><xsl:value-of select="//lang_blocks/header"/><br/><hr/> </td>
              </tr>

<tr>
                <td valign="top" align="left">
                
                <p><xsl:value-of select="//lang_blocks/greeting"/> [[firstname]],</p>
                
                
                
                <p><xsl:value-of select="//lang_blocks/p1"/></p>
                
                
                <ul>
                <li><xsl:copy-of select="//lang_blocks/p2/ *| //lang_blocks/p2/text()"/></li><br/><br/>
                <li><xsl:value-of select="//lang_blocks/p3"/></li>
                <p><xsl:value-of select="//lang_blocks/p4"/></p>
                <p><xsl:value-of select="//lang_blocks/p5"/></p>
                </ul>
                
                <p><xsl:value-of select="//lang_blocks/p6"/></p>
                   <p><xsl:value-of select="//lang_blocks/p7"/></p>
                
                <p><xsl:value-of select="//lang_blocks/p8"/><br/><br/>
           <xsl:value-of select="//lang_blocks/p9"/><br/>
           <xsl:value-of select="//lang_blocks/p10"/></p>
                
                
                <p><xsl:value-of select="//lang_blocks/p11"/></p>
                
                <p><xsl:copy-of select="//lang_blocks/p12/* | //lang_blocks/p12/text()"/></p>
                
	<br/>	<br/>



</td> 
</tr>




</table>
        
        
        </td>

      </tr>
    </table>
    
    
    
    
    

</td></tr></table>

</body></html>






   </xsl:template>
</xsl:stylesheet>