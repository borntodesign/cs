<?php
if (function_exists('mb_internal_encoding'))
    echo 'mb_internal_encoding: ', mb_internal_encoding(), PHP_EOL;
echo 'mb_http_input: ', mb_http_input(), PHP_EOL;
echo 'mb_detect_order: ', mb_detect_order(), PHP_EOL;
echo 'mb_regex_encoding: ', mb_regex_encoding(), PHP_EOL;