<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDB.php';

$operator = htmlspecialchars($_COOKIE['operator']);
if (preg_match('/[^a-zA-Z0-9\.]/', $operator)) {
    die("The username looks bad: $operator");
}
$pwd = htmlspecialchars($_COOKIE['pwd']);

try {
    $dbi = new \Clubshop\AdminDB($operator, $pwd);
} catch (\Exception $e) {
    die("DB connect failed: " . $e->getMessage());
}

$Stypes = $dbi->fetchAll_assoc("
    SELECT subscription_type, pkg_pp_differential
    FROM subscription_types
    WHERE sub_class IN ('VM','VA')", 'subscription_type');
    
$errors = array();
$messages = array();
$map = array(
    '2' => 'annual_initial',
    '3' => 'pro',
    '4' => 'annual_renewal',
    '40' => 'basic_monthly_initial',
    '41' => 'basic_monthly',
    '43' => 'pro_plus',
    '45' => 'premier',
    '47' => 'premier_plus',
    '48' => 'basic_plus_monthly_initial',
    '49' => 'basic_plus_monthly'
);

$vals = array(
    'currency' => paramVal('currency'),
    'currency_code' => paramVal('currency_code'),
    'exchange_rate' => paramVal('exchange_rate'),
    'allowable_xr_change' => isset($_GET['allowable_xr_change']) ? $_GET['allowable_xr_change'] : '0.05',
    'price_annual_initial' => paramVal('price_annual_initial'),   //2
    'pp_annual_initial' => paramVal('pp_annual_initial'),
    'annual_initial_ppperc' => paramVal('annual_initial_ppperc'),
    'price_basic_monthly' => paramVal('price_basic_monthly'),       //41
    'pp_basic_monthly' => paramVal('pp_basic_monthly'),
    'basic_monthly_ppperc' => paramVal('basic_monthly_ppperc'),
    'price_basic_monthly_initial' => paramVal('price_basic_monthly_initial'),       //40
    'pp_basic_monthly_initial' => paramVal('pp_basic_monthly_initial'),
    'basic_monthly_initial_ppperc' => paramVal('basic_monthly_initial_ppperc'),
    'price_basic_plus_total_target' => '',
    'pp_basic_plus_total_target' => '',
    'price_basic_plus_monthly' => paramVal('price_basic_plus_monthly'),      //49
    'pp_basic_plus_monthly' => paramVal('pp_basic_plus_monthly'),
    'basic_plus_monthly_ppperc' => paramVal('basic_plus_monthly_ppperc'),
    'price_basic_plus_monthly_initial' => paramVal('price_basic_plus_monthly_initial'),      //48
    'pp_basic_plus_monthly_initial' => paramVal('pp_basic_plus_monthly_initial'),
    'basic_plus_monthly_initial_ppperc' => paramVal('basic_plus_monthly_initial_ppperc'),
    'price_pro' => paramVal('price_pro'),      //3     - these are actually renewal subscriptions
    'pp_pro' => paramVal('pp_pro'),
    'pro_ppperc' => paramVal('pro_ppperc'),
    'price_pro_plus' => paramVal('price_pro_plus'),
    'pp_pro_plus' => paramVal('pp_pro_plus'),    //43    - these are actually renewal subscriptions
    'pro_plus_ppperc' => paramVal('pro_plus_ppperc'),
    'price_premier' => paramVal('price_premier'),
    'pp_premier' => paramVal('pp_premier'),     //45    - these are actually renewal subscriptions
    'premier_ppperc' => paramVal('premier_ppperc'),
    'price_premier_plus' => paramVal('price_premier_plus'),
    'pp_premier_plus' => paramVal('pp_premier_plus'),    //47    - these are actually renewal subscriptions
    'premier_plus_ppperc' => paramVal('premier_plus_ppperc'),
    'price_annual_renewal' => paramVal('price_annual_renewal'),
    'pp_annual_renewal' => paramVal('pp_annual_renewal'),
    'annual_renewal_ppperc' => paramVal('annual_renewal_ppperc'),
    'price_basic_total_target' => paramVal('price_basic_total_target'),
    'pp_basic_total_target' => paramVal('pp_basic_total_target'),
    'effective_date' => effectiveDate()
);
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : '';
switch ($action) {
    case 'save':
 //       var_dump($_GET);
        savePending();
        $_GET['frum'] = 'temp';
        LoadDataByCC();
        break;
    case 'lookup_currency':
        LoadDataByCC();
        break;
    default:
        //;
}
//var_dump($vals);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript">
function calcUSD(bN){
	var price = $('#price_' + bN).val();
	var xr = $('#exchange_rate').val();
	if (! (price && xr)) return;
	$('#td_' + bN + '_usd').val( (price / xr).toFixed(2) );

	calcPerc(bN);
}
function calcPerc(bN){
	var price = Number( $('#td_' + bN + '_usd').val() );
	var pp = $('#pp_' + bN).val();
	if (! (price && pp)) return;
	var perc = (1 + Number( $('#allowable_xr_change').val() )) * (pp/price);
	$('#' + bN + '_ppperc').val( perc.toFixed(4) );
}
function calcAllPercs(){
	calcPerc('annual_initial');
	calcPerc('basic_monthly');
	calcPerc('basic_initial');
	calcPerc('pro');
	calcPerc('pro_plus');
	calcPerc('premier');
	calcPerc('premier_plus');
	calcPerc('annual_renewal');
}
function calcTotals(){
	calcNativeTotal('basic');
	calcUSDTotal('basic');
	
	calcNativeTotal('basic_plus');
	calcUSDTotal('basic_plus');
	calcPPPTotals();
}
function calcPPPTotals(){
	calcPPTotal('basic');
	calcPPTotal('basic_plus');
}
function calcNativeTotal(bN){
	var subs = Number($('#price_annual_initial').val()) + Number($('#price_' + bN + '_monthly').val()) + Number($('#price_' + bN + '_monthly_initial').val());
	$('#td_' + bN + '_calculated_total').html(subs);
	var ttl = Number($('#price_' + bN + '_total_target').val()) - subs;
	var dest = $('#td_' + bN + '_adjustment_needed');
    if (ttl < 0){
        dest.css('color','red');
    } else {
    	dest.css('color','black');
    }
	dest.html(ttl);
}
function calcUSDTotal(bN){
	var subs = Number($('#td_annual_initial_usd').val()) + Number($('#td_' + bN + '_monthly_usd').val()) + Number($('#td_' + bN + '_monthly_initial_usd').val());
	$('#td_' + bN + '_calculated_usd').html(subs.toFixed(2));
}
function calcPPTotal(bN){
	var subs = Number($('#pp_annual_initial').val()) + Number($('#pp_' + bN + '_monthly').val()) + Number($('#pp_' + bN + '_monthly_initial').val());
	$('#pp_' + bN + '_calculated').html(subs);
	var ttl = Number($('#pp_' + bN + '_total_target').val()) - subs;
	var dest = $('#pp_' + bN + '_adjustment_needed');
    if (ttl < 0){
        dest.css('color','red');
    } else {
    	dest.css('color','black');
    }
	dest.html(ttl);
}

$( document ).ready(function() {
	calcUSD('basic_total_target');
	calcUSD('basic_plus_total_target');
    calcUSD('basic_monthly');
    calcUSD('basic_monthly_initial');
    calcUSD('basic_plus_monthly');
    calcUSD('basic_plus_monthly_initial');
    calcUSD('pro');
    calcUSD('pro_plus');
    calcUSD('premier');
    calcUSD('premier_plus');
    calcUSD('annual_renewal');
    calcUSD('annual_initial');
    calcTotals();
    
    $('form [type=text]').on('click', function(){
        $(this).select();
    });

    $('#price_basic_total_target').on('change', function(){
    	calcUSD('basic_total_target');
    	calcNativeTotal('basic');
    	calcUSDTotal('basic');
    });

    $('#price_basic_monthly').on('change', function(){
    	calcUSD('basic_monthly');
    	calcNativeTotal('basic');
    	calcUSDTotal('basic');
    });

    $('#price_basic_monthly_initial').on('change', function(){
    	calcUSD('basic_monthly_initial');
    	calcNativeTotal('basic');
    	calcUSDTotal('basic');
    });

    $('#price_basic_plus_total_target').on('change', function(){
    	calcUSD('basic_plus_total_target');
    	calcNativeTotal('basic_plus');
    	calcUSDTotal('basic_plus');
    });

    $('#price_basic_plus_monthly').on('change', function(){
    	calcUSD('basic_plus_monthly');
    	calcNativeTotal('basic_plus');
    	calcUSDTotal('basic_plus');
    });

    $('#price_basic_plus_monthly_initial').on('change', function(){
    	calcUSD('basic_plus_monthly_initial');
    	calcNativeTotal('basic_plus');
    	calcUSDTotal('basic_plus');
    });

});
</script>
<style type="text/css">
body {
	font-size: 80%;
}
table {
	border-collapse: collapse;
}
td,th {
	border: 1px solid grey;
}
th {
	padding: 1em 0.3em 0.2em 0.3em;
	font-family: sans-serif;
}
input.w3 {
	width: 3em;
	border-style: solid;
}
input.w5 {
	width: 5em;
	border-style: solid;
}
input.w6 {
	width: 6em;
	border-style: solid;
}
input.w7 {
	width: 7em;
	border-style: solid;
}
input.w10 {
	width: 10em;
	border-style: solid;
}
.vatop {
	vertical-align:top;
}
.fp {
	font-size: 75%;
}
.al {
	text-align:left;
}
.ar {
	text-align: right;
}
.ac {
	text-align: center;
}
.nb {
	border: none;
}
.bgusd {
	background-color: #eec;
	text-align: right;
}
input.bgusd {
	padding-right: 0.5em;
	border: none;
}
.bgltg {
	background-color: #cfc;
}
input.bgltg {
	border: none;
}
.bgltb {
	background-color: #ccf;
	text-align: right;
}
input.bgltb {
	padding-right: 0.5em;
	border: none;
}
.error {
	color: red;
}
.message {
	color: blue;
}
.bfp {
	font-family: arial,helvetica,sans-serif;
	color: blue;
	font-size: 80%;
}
#selectccode {
    display: none;
    position: absolute;
    top: -1px;
    left: -1px;
    padding: 1em;
    border: 1px solid green;
    background-color: white;
}
option.even {
	background-color: #ddffdd;
}
</style>
</head>
<body>
<form action="" method="get" id="frm_currency">
    <input type="hidden" id="currency" name="currency" value="" />
    <input type="hidden" id="frum" name="frum" value="" />
    <input type="hidden" name="action" value="lookup_currency" />
</form>
<!-- keep us from submitting on hitting the return key on a field -->
<form method="get" id="frm_main" action="" onsubmit="if (this.elements['action'].value == '') return false;">
<input id="frm_main_action" type="hidden" name="action" value="" />
<table>
<!-- ROW 1 -->
<tr>
<th style="position: relative" class="al">Currency:
    <div id="selectccode">
        <select onchange="$('#currency_code').val(this.options[this.selectedIndex].value); $('#selectccode').hide();" style="background-color:#ffffee">
            <option value="">Choose a currency</option>
            <?php $x = 0;
                foreach ($dbi->fetchList_assoc('
                SELECT DISTINCT cbc.currency_code, cc.description
                FROM currency_by_country cbc
                JOIN currency_codes cc
                    ON cc.code=cbc.currency_code
                ORDER BY cbc.currency_code') AS $c): ?>
                
                <option <?php if ($x % 2) echo 'class="even "'; ?>value="<?=$c['currency_code']?>"><?="{$c['currency_code']} {$c['description']}"?></option>
            <?php $x++; endforeach;?>
        </select>
    </div>
</th>
<td><span class="fp">Click for currency</span><br />
<input onclick="$('#selectccode').toggle()" type="text" name="currency_code" id="currency_code" value="<?=$vals['currency'] ? $vals['currency'] : $vals['currency_code']?>" class="w3 bgltg" /></td>
<td colspan="4" class="nb">
    <script type="text/javascript">
    function submitFrmCurrency(frum){
        if (! $('#currency_code').val()){
        	$('#currency_code').css('border-color', 'red');
            alert('Set the currency code first'+$('#currency_code').val());
        } else {
            $('#currency').val( $('#currency_code').val() );
            $('#frum').val(frum);
            $('#frm_currency').submit();
        }
    }
    </script>
    <input style="margin-left: 1em;" type="button" class="btn" value="Load from Live" onclick="submitFrmCurrency('live')" />
    <input style="margin-left: 1em;" type="button" class="btn" value="Load from Pending" onclick="submitFrmCurrency('temp')" />
</td>
<td colspan="5" rowspan="6" class="nb" style="padding-left: 1em; vertical-align: top;">
<ul class="bfp"><li>The total upgrade price (Target Total) consists of Annual Initial + Initial Monthly + Monthly Renewal<br />
The PP for the Annual Initial are distributed in the upgrade month</li>
<li>If you select "Load from Live" and end up empty handed, it is because there is no explicit configuration for that currency yet.
Instead a default is used.
Just key in and save a new configuration.</li>
<li>The exchange rate % value is not stored anywhere. Rather it is applied to the PP% immediately.</li>
</ul>
<?php
foreach ($errors as $item) {
    echo '<div class="error">' . $item . '</div>';
}
foreach ($messages as $item) {
    echo '<div class="message">' . $item . '</div>';
}
?>
</td>
</tr>
<!-- ROW 2 -->
<tr>
<th class="al">Exchange Rate:</th>
<td>
<input type="text" name="exchange_rate" value="<?=$vals['exchange_rate']?>" id="exchange_rate" readonly="readonly" class="bgltg w7" />
</td>
<td colspan="4" style="padding-left:5em;">Allowable % exchange rate devaluation <span class="fp">(ie. 5% = 0.05)</span>:
<input type="text" name="allowable_xr_change" id="allowable_xr_change" value="<?=$vals['allowable_xr_change']?>" class="w3 bgltg" onchange="calcAllPercs();" />
<br /><span class="fp">Devaluations greater than this value will end up depreciating PP value</span>
</td>
</tr>
<!-- ROW 3 -->
<tr>
<td class="nb" colspan="2"></td>
<th>Target</th>
<th>USD value</th>
<td colspan="2" class="nb"></td>
</tr>

<!-- ROW 4 -->
<tr>
<th rowspan="3" class="vatop al">Annual Initial</th>
<td>Price</td>
<td>
<input name="price_annual_initial" id="price_annual_initial" type="text" value="<?=$vals['price_annual_initial']?>" class="w10 bgltg" onchange="calcUSD('annual_initial');calcTotals();" />
</td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_annual_initial_usd" class="bgusd w5" readonly="readonly" /></td>
<td colspan="2" rowspan="3" class="nb"></td>
</tr>

<!-- ROW 5 -->
<tr>
<!-- rowspan on row 4 fills this slot -->
<td>PP value</td>
<td>
    <input name="pp_annual_initial" id="pp_annual_initial" type="text" value="<?=$vals['pp_annual_initial']?>" class="w3 bgltg" onchange="calcPerc('annual_initial');calcPPPTotals();" />
</td>
<td class="nb"></td>
</tr>

<!-- ROW 6 -->
<tr>
<!-- rowspan on row 4 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb">
<input id="annual_initial_ppperc" name="annual_initial_ppperc" readonly="readonly" value="<?=$vals['annual_initial_ppperc']?>" class="w5 bgltb" />
</td>
</tr>

<!-- ROW 7 -->
<tr>
<td colspan="2" class="nb"></td>
<th>Target Total</th><th class="bgusd">USD value</th><th>Monthly Renewal</th><th class="bgusd">USD value</th><th>Initial Monthly</th>
<th class="bgusd">USD value</th><th>Calculated</th><th class="bgusd">Total USD value</th><th>Adj needed</th>
</tr>

<!-- ROW 8 -->
<tr>
<th rowspan="3" class="vatop al">BASIC</th>
<td>Price</td>
<td>
<input type="text" name="price_basic_total_target" id="price_basic_total_target" value="<?=$vals['price_basic_total_target']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_total_target_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td>
<input type="text" name="price_basic_monthly" id="price_basic_monthly" value="<?=$vals['price_basic_monthly']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_monthly_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td>
<input type="text" name="price_basic_monthly_initial" id="price_basic_monthly_initial" value="<?=$vals['price_basic_monthly_initial']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_monthly_initial_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td id="td_basic_calculated_total" class="ac"></td>
<td id="td_basic_calculated_usd" class="bgusd"></td>
<td id="td_basic_adjustment_needed" class="ac"></td>
</tr>

<!-- ROW 9 -->
<tr>
<!-- rowspan from 8 here -->
<td>PP value</td>
<td>
<input type="text" name="pp_basic_total_target" id="pp_basic_total_target" value="<?=$vals['pp_basic_total_target']?>" class="w3 bgltg" onchange="calcPPTotal('basic')" />
</td>
<td></td>
<td>
<input type="text" name="pp_basic_monthly" id="pp_basic_monthly" value="<?=$vals['pp_basic_monthly']?>" class="w3 bgltg" onchange="calcPerc('basic_monthly');calcPPTotal('basic');" />
</td>
<td></td>
<td>
<input type="text" name="pp_basic_monthly_initial" id="pp_basic_monthly_initial" value="<?=$vals['pp_basic_monthly_initial']?>" class="w3 bgltg" onchange="calcPerc('basic_monthly_initial');calcPPTotal('basic');" />
<span class="bfp"><?php if ($Stypes['40']['pkg_pp_differential']) echo "+Co-Op PP: {$Stypes['40']['pkg_pp_differential']}"; ?></span>
</td>
<td></td>
<td id="pp_basic_calculated" class="bgltg ac"></td>
<td></td>
<td id="pp_basic_adjustment_needed" class="ac"></td>
</tr>

<!-- ROW 10 -->
<tr>
<!-- rowspan from 8 here -->
<td colspan="4" class="ar">PP %</td>
<td class="bgltb">
<input id="basic_monthly_ppperc" name="basic_monthly_ppperc" value="<?=$vals['basic_monthly_ppperc']?>" readonly="readonly" class="w5 bgltb" type="text" />
</td>
<td class="ar">PP %</td>
<td class="bgltb">
<input id="basic_monthly_initial_ppperc" name="basic_monthly_initial_ppperc" value="<?=$vals['basic_monthly_initial_ppperc']?>" readonly="readonly" type="text" class="w5 bgltb" />
</td>
<td colspan="3"></td>
</tr>

<!-- ROW 11 -->
<tr>
<th rowspan="3" class="vatop al">BASIC PLUS</th>
<td>Price</td>
<td>
<input type="text" name="price_basic_plus_total_target" id="price_basic_plus_total_target" value="<?=$vals['price_basic_plus_total_target']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_plus_total_target_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td>
<input type="text" name="price_basic_plus_monthly" id="price_basic_plus_monthly" value="<?=$vals['price_basic_plus_monthly']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_plus_monthly_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td>
<input type="text" name="price_basic_plus_monthly_initial" id="price_basic_plus_monthly_initial" value="<?=$vals['price_basic_plus_monthly_initial']?>" class="w10 bgltg" />
</td>
<td class="bgusd"><input id="td_basic_plus_monthly_initial_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td id="td_basic_plus_calculated_total" class="ac"></td>
<td id="td_basic_plus_calculated_usd" class="bgusd"></td>
<td id="td_basic_plus_adjustment_needed" class="ac"></td>
</tr>

<!-- ROW 12 -->
<tr>
<!-- rowspan from 11 here -->
<td>PP value</td>
<td>
<input type="text" name="pp_basic_plus_total_target" id="pp_basic_plus_total_target" value="<?=$vals['pp_basic_plus_total_target']?>" class="w3 bgltg" onchange="calcPPTotal('basic_plus')" />
</td>
<td></td>
<td>
<input type="text" name="pp_basic_plus_monthly" id="pp_basic_plus_monthly" value="<?=$vals['pp_basic_plus_monthly']?>" class="w3 bgltg" onchange="calcPerc('basic_plus_monthly'); calcPPTotal('basic_plus');" />
<span class="bfp"><?php if ($Stypes['48']['pkg_pp_differential']) echo "+Co-Op PP: {$Stypes['48']['pkg_pp_differential']}"; ?></span>
</td>
<td></td>
<td>
<input type="text" name="pp_basic_plus_monthly_initial" id="pp_basic_plus_monthly_initial" value="<?=$vals['pp_basic_plus_monthly_initial']?>" class="w3 bgltg" onchange="calcPerc('basic_plus_monthly_initial');calcPPTotal('basic_plus');" />
<span class="bfp"><?php if ($Stypes['48']['pkg_pp_differential']) echo "+Co-Op PP: {$Stypes['48']['pkg_pp_differential']}"; ?></span>
</td>
<td></td>
<td id="pp_basic_plus_calculated" class="bgltg ac"></td>
<td>
<?php
    $xpp = 0;
    if ($Stypes['48']['pkg_pp_differential']) $xpp += $Stypes['48']['pkg_pp_differential'];
    if ($Stypes['49']['pkg_pp_differential']) $xpp += $Stypes['49']['pkg_pp_differential'];
?>
<span class="bfp">+Co-Op PP: <?=$xpp?></span>
</td>
<td id="pp_basic_plus_adjustment_needed" class="ac"></td>
</tr>

<!-- ROW 13 -->
<tr>
<!-- rowspan from 11 here -->
<td colspan="4" class="ar">PP %</td>
<td class="bgltb">
<input id="basic_plus_monthly_ppperc" name="basic_plus_monthly_ppperc" value="<?=$vals['basic_plus_monthly_ppperc']?>" readonly="readonly" class="w5 bgltb" type="text" />
</td>
<td class="ar">PP %</td>
<td class="bgltb">
<input id="basic_plus_monthly_initial_ppperc" name="basic_plus_monthly_initial_ppperc" value="<?=$vals['basic_plus_monthly_initial_ppperc']?>" readonly="readonly" type="text" class="w5 bgltb" />
</td>
<td colspan="3" class="nb"></td>
</tr>

<!-- ROW 14 -->
<tr>
<th rowspan="3" class="vatop al">PRO</th>
<td>Price</td>
<td><input name="price_pro" id="price_pro" type="text" value="<?=$vals['price_pro']?>" class="w10 bgltg" onchange="calcUSD('pro')" /></td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_pro_usd" class="w5 bgusd" type="text" readonly="readonly"></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 15 -->
<tr>
<!-- rowspan on row 14 fills this slot -->
<td>PP value</td>
<td><input name="pp_pro" id="pp_pro" type="text" value="<?=$vals['pp_pro']?>" class="w3 bgltg" onchange="calcPerc('pro')" /></td>
<td colspan="8" class="nb"></td>
</tr>

<!-- ROW 16 -->
<tr>
<!-- rowspan on row 14 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb"><input id="pro_ppperc" name="pro_ppperc" class="w5 bgltb" readonly="readonly" value="<?=$vals['pro_ppperc']?>" type="text" />
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 17 -->
<tr>
<th rowspan="3" class="vatop al">PRO PLUS</th>
<td>Price</td>
<td><input name="price_pro_plus" id="price_pro_plus" type="text" value="<?=$vals['price_pro_plus']?>" class="w10 bgltg" onchange="calcUSD('pro_plus')" /></td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_pro_plus_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 18 -->
<tr>
<!-- rowspan on row 17 fills this slot -->
<td>PP value</td>
<td>
<input name="pp_pro_plus" id="pp_pro_plus" type="text" value="<?=$vals['pp_pro_plus']?>" class="w3 bgltg" onchange="calcPerc('pro_plus')" />
<span class="bfp"><?php if ($Stypes['43']['pkg_pp_differential']) echo "+Co-Op PP: {$Stypes['43']['pkg_pp_differential']}"; ?></span>
</td>
<td colspan="8" class="nb"></td>
</tr>

<!-- ROW 19 -->
<tr>
<!-- rowspan on row 17 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb"><input id="pro_plus_ppperc" name="pro_plus_ppperc" type="text" readonly="readonly" value="<?=$vals['pro_plus_ppperc']?>" class="w5 bgltb" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 20 -->
<tr>
<th rowspan="3" class="vatop al">PREMIER</th>
<td>Price</td>
<td><input name="price_premier" id="price_premier" type="text" value="<?=$vals['price_premier']?>" class="w10 bgltg" onchange="calcUSD('premier')" /></td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_premier_usd" class="w5 bgusd" type="text" readonly="readonly" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 21 -->
<tr>
<!-- rowspan on row 20 fills this slot -->
<td>PP value</td>
<td><input name="pp_premier" id="pp_premier" type="text" value="<?=$vals['pp_premier']?>" class="w3 bgltg" onchange="calcPerc('premier')" /></td>
<td colspan="8" class="nb"></td>
</tr>

<!-- ROW 22 -->
<tr>
<!-- rowspan on row 20 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb"><input id="premier_ppperc" name="premier_ppperc" type="text" readonly="readonly" value="<?=$vals['premier_ppperc']?>" class="w5 bgltb" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 23 -->
<tr>
<th rowspan="3" class="vatop al">PREMIER PLUS</th>
<td>Price</td>
<td><input name="price_premier_plus" id="price_premier_plus" type="text" value="<?=$vals['price_premier_plus']?>" class="w10 bgltg" onchange="calcUSD('premier_plus')" /></td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_premier_plus_usd" class="bgusd w5" readonly="readonly" type="text" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 24 -->
<tr>
<!-- rowspan on row 23 fills this slot -->
<td>PP value</td>
<td>
<input name="pp_premier_plus" id="pp_premier_plus" type="text" value="<?=$vals['pp_premier_plus']?>" class="w3 bgltg" onchange="calcPerc('premier_plus')" />
<span class="bfp"><?php if ($Stypes['47']['pkg_pp_differential']) echo "+Co-Op PP: {$Stypes['47']['pkg_pp_differential']}"; ?></span>
</td>
<td colspan="8" class="nb"></td>
</tr>

<!-- ROW 25 -->
<tr>
<!-- rowspan on row 23 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb"><input id="premier_plus_ppperc" name="premier_plus_ppperc" type="text" readonly="readonly" value="<?=$vals['premier_plus_ppperc']?>" class="w5 bgltb" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 26 -->
<tr>
<th rowspan="3" class="vatop al">Annual Renewal</th>
<td>Price</td>
<td>
<input name="price_annual_renewal" id="price_annual_renewal" type="text" value="<?=$vals['price_annual_renewal']?>" class="w10 bgltg" onchange="calcUSD('annual_renewal')" />
</td>
<!-- THE USD VALUE WILL BE CALCULATED AND PLACED IN HERE -->
<td class="bgusd"><input id="td_annual_renewal_usd" readonly="readonly" class="w5 bgusd" type="text" /></td>
<td colspan="7" class="nb"></td>
</tr>

<!-- ROW 27 -->
<tr>
<!-- rowspan on row 26 fills this slot -->
<td>PP value</td>
<td><input name="pp_annual_renewal" id="pp_annual_renewal" type="text" value="<?=$vals['pp_annual_renewal']?>" class="w3 bgltg" onchange="calcPerc('annual_renewal')" /></td>
<td colspan="8" class="nb"></td>
</tr>

<!-- ROW 28 -->
<tr>
<!-- rowspan on row 26 fills this slot -->
<td colspan="2" class="ar">PP %</td>
<!-- the % will be calculated and placed in here -->
<td class="bgltb">
<input id="annual_renewal_ppperc" name="annual_renewal_ppperc" type="text" readonly="readonly" value="<?=$vals['annual_renewal_ppperc']?>" class="w5 bgltb" />
</td>
<td colspan="7" class="nb">
<input style="margin-left: 1em;" type="button" value="Save Changes to Pending" class="btn" onclick="submitMainFrm()" />
Pending records will become effective:
<input type="text" name="effective_date" id="effective_date" value="<?=$vals['effective_date']?>" readonly="readonly" class="w6" />
    <script type="text/javascript">
        function submitMainFrm(){
        	$('#frm_main_action').val('save');
            $('#frm_main').submit();
        }
    </script>
</td>
</tr>
</table>
</form>
</body>
</html>
<?php
function buildTotalTargets() {
    global $vals;
    $vals['price_basic_total_target'] = $vals['price_annual_initial'] + $vals['price_basic_monthly'] + $vals['price_basic_monthly_initial'];
    $vals['pp_basic_total_target'] = $vals['pp_annual_initial'] + $vals['pp_basic_monthly'] + $vals['pp_basic_monthly_initial'];
    $vals['price_basic_plus_total_target'] = $vals['price_annual_initial'] + $vals['price_basic_plus_monthly'] + $vals['price_basic_plus_monthly_initial'];
    $vals['pp_basic_plus_total_target'] = $vals['pp_annual_initial'] + $vals['pp_basic_plus_monthly'] + $vals['pp_basic_plus_monthly_initial'];
}

function qryLiveData($cc) {
    exchangeRate($cc);
    global $map, $vals;
    foreach ($map as $stype => $label){
        $data = getLiveTblData($cc, $stype);
        if (isset($data)) {
            $vals["price_$label"] = $data['amount'];
            $vals["pp_$label"] = $data['pp'];
            $vals[$label . '_ppperc'] = $data['percentage'];
        }
    }
    buildTotalTargets();
}

function qryTempData($cc) {
    exchangeRate($cc);
    global $map, $vals;
    foreach ($map as $stype => $label){
        $data = getWorkTblData($cc, $stype);
        if (isset($data)) {
            $vals["price_$label"] = $data['amount'];
            $vals["pp_$label"] = $data['pp_cap'];
            $vals[$label . '_ppperc'] = $data['percentage'];
        }
    }
    buildTotalTargets();
}

function getLiveTblData($cc, $stype) {
    global $dbi, $errors;
    try {
        $rv = $dbi->selectrow_array("
            SELECT
                sp.amount,
                sp.pp,
                spp.percentage
            FROM subscription_pricing_current sp
            LEFT JOIN subscription_point_percentages spp
                ON spp.subscription_type=sp.subscription_type
                AND spp.currency_code=sp.currency_code
            WHERE sp.subscription_type=?
            AND sp.currency_code=?
            ",array($stype, $cc));
    } catch (Exception $e) {
        $errors[] = $e;
    }
    return $rv;
}

function getWorkTblData($cc, $stype) {
    global $dbi, $errors, $messages;
    try {
        $rv = $dbi->selectrow_array("
            SELECT
                sp.amount,
                spp.pp_cap,
                spp.percentage
            FROM work.subscription_pricing sp
            JOIN work.subscription_point_percentages spp
                ON spp.subscription_type=sp.subscription_type
                AND spp.currency_code=sp.currency_code
            WHERE sp.subscription_type=?
            AND sp.currency_code=?
            ",array($stype, $cc));
        if (! $rv) {
            $messages[] = "query on work tables returned no result for code: $cc - stype: $stype";
        } else {
            //error_log(serialize($rv));
            //var_dump($rv);
        }
    } catch (Exception $e) {
        $errors[] = $e;
    }
    return $rv;
}

function exchangeRate($cc) {
    global $vals, $dbi, $errors;
    try {
        $rv = $dbi->selectrow_list('SELECT "rate" FROM exchange_rates_now WHERE "code"= ?', array($cc));
        $vals['exchange_rate'] = $rv[0];
    } catch (\Exception $e) {
        $errors[] = $e;
    }
}

function LoadDataByCC() {
    global $vals, $errors;
    $cc = $vals['currency'] ? $vals['currency'] : $vals['currency_code'];
    if (! $cc) {
        error_log('Could not determine the currency code at line# ' . __LINE__);
        $errors[] = 'The data could not be refreshed from the DB';
        return ;
    }

    switch ($_GET['frum']) {
        case 'live':
            qryLiveData($cc);
            break;
        case 'temp':
            qryTempData($cc);
            break;
        default:
            qryLiveData($cc);
    }
}

function paramVal($key) {
    $rv = htmlspecialchars(isset($_GET[$key]) ? $_GET[$key] : '');
    return $rv;
}

function effectiveDate() {
    global $dbi;
    $rv = $dbi->selectrow_list('SELECT last_of_month() +1');
    return $rv[0];
}

function savePending() {
    global $map;
    $pam = array_flip($map);
    foreach ($pam as $label => $stype) {
        savePendingPP($stype, $label);
        savePendingPrice($stype, $label);
    }
}

function savePendingPP($stype, $label) {
    global $vals, $dbi, $errors;
    
    if (! $vals[$label . '_ppperc'])    // nothing to do with no value
        return;
    
    $existing = $dbi->selectrow_array('
        SELECT subscription_type FROM work.subscription_point_percentages WHERE currency_code=? AND subscription_type=?', array($vals['currency_code'], $stype));

    if ($existing) {
        try {
            $sth = $dbi->prepareXSth("
                UPDATE work.subscription_point_percentages SET notes= NOW()::DATE ||?, percentage=?, pp_cap=?
                WHERE currency_code= ? AND subscription_type= ?", array(
                    " - exchg rate: {$vals['exchange_rate']} - effective: {$vals['effective_date']}",
                    $vals[$label . '_ppperc'],
                    $vals['pp_' . $label],
                    $vals['currency_code'],
                    $stype
                ));
            if (! $sth) {
                throw new Exception("subscription_point_percentages not updated for subscription_type: $stype");
            }
        } catch (Exception $e) {
            $errors[] = $e;
        }
        
    } else {
        try {
            $sth = $dbi->db()->prepare("
                INSERT INTO work.subscription_point_percentages (notes, percentage, pp_cap, currency_code, subscription_type)
                VALUES ( NOW()::DATE ||?, ?, ?, ?, ?)");
            if (! $sth) {
                throw new Exception("Statement prepare failure: subscription_point_percentages record not created for subscription_type: $stype");
            }
            $rv = $sth->execute(array(
                    " - exchg rate: {$vals['exchange_rate']} - effective: {$vals['effective_date']}",
                    $vals[$label . '_ppperc'],
                    $vals['pp_' . $label],
                    $vals['currency_code'],
                    $stype
                ));
            if (! $rv) {
                throw new Exception("Statement execution failure: subscription_point_percentages record not created for subscription_type: $stype");
            }
        } catch (Exception $e) {
            $errors[] = $e;
        }
    }
}

function savePendingPrice($stype, $label) {
    global $vals, $dbi;
    
    if (! $vals['price_' . $label])     // nothing to do without a price
        return;
    
    $existing = $dbi->selectrow_array('
        SELECT subscription_type FROM work.subscription_pricing WHERE currency_code=? AND subscription_type=?', array($vals['currency_code'], $stype));
    
    if ($existing) {
        try {
            $sth = $dbi->prepareXSth("
                UPDATE work.subscription_pricing SET notes= NOW()::DATE ||?, amount=?
                WHERE currency_code= ? AND subscription_type= ?", array(
                        " - exchg rate: {$vals['exchange_rate']} - effective: {$vals['effective_date']}",
                        $vals['price_' . $label],
                        $vals['currency_code'],
                        $stype
                    ));
            if (! $sth) {
                throw new Exception("subscription_pricing not updated for subscription_type: $stype");
            }
        } catch (Exception $e) {
            $errors[] = $e;
        }
    
    } else {
        try {
            $sth = $dbi->db()->prepare("
                INSERT INTO work.subscription_pricing (notes, amount, effective_date, currency_code, subscription_type)
                VALUES ( NOW()::DATE ||?, ?, ?, ?, ?)");
            if (! $sth) {
                throw new Exception("Statement prepare failure: subscription_pricing record not created for subscription_type: $stype");
            }
            $rv = $sth->execute(array(
                " - exchg rate: {$vals['exchange_rate']} - effective: {$vals['effective_date']}",
                $vals['price_' . $label],
                $vals['effective_date'],
                $vals['currency_code'],
                $stype
            ));
            if (! $rv) {
                throw new Exception("Statement execution failure: subscription_pricing record not created for subscription_type: $stype");
            }
        } catch (Exception $e) {
            $errors[] = $e;
        }
    }
}
?>