package rpccgi::reward_points_summary;
sub Describe {
	return "For a given ID, returns various reward point values. These are currently:
reward_points: their current RP total (does not include pending point additions)
redeemable_points: the actual points that can be redeemed out of the total
pending_points: the tentative points that will be added on the next commission run
ytd_points: the total of points (type 1 and type 4) rewarded so far this year (not including pending points)
ytd_year: the year we are referring to
ytd_redemptions: this currently refers to type 2 and type 5 (donations) redemptions";
}
sub new { return bless{} }
sub Params {
	return {id=>{value=>undef,req=>1,ck=>['=~m/\D/']}};
}
sub Process {
	my $self = shift;
	my($db, $params) = @_;
	my $rv = {ytd_year => ((localtime)[5] + 1900)};
	# with Pg.pm ver. 1.43, selectrow_array doesn't seem to work when returning multiple cols
	# so we'll do it old school
	my $sth = $db->prepare("
		SELECT COALESCE(SUM(ytd_points),0) AS ytd_points,
			COALESCE(SUM(ytd_redemptions),0) AS ytd_redemptions FROM
		(	SELECT CASE WHEN \"reward_type\" IN (1,4) THEN SUM(reward_points) END AS ytd_points,
				CASE WHEN \"reward_type\" IN (2,5) THEN SUM(reward_points) END AS ytd_redemptions
			FROM reward_transactions WHERE id=? AND \"reward_type\" IN (1,2,4,5)
			AND date_reported BETWEEN '$rv->{ytd_year}-01-01' AND NOW()
			GROUP BY \"reward_type\") AS tmp");
	$sth->execute($params->{id});
	($rv->{ytd_points}, $rv->{ytd_redemptions}) = $sth->fetchrow_array;
	$sth = $db->prepare("
                SELECT COALESCE((SUM(reward_points*cur_xchg_rate - total_pending_rewards*cur_xchg_rate))::NUMERIC(10,2),0) as reward_points, 
                       COALESCE((SUM(total_pending_rewards*cur_xchg_rate))::numeric(10,2),0) as pending_points 
                FROM reward_transactions 
                WHERE id = ? and 
                      date_reported >= (select  extract(year from now()) || '-' || lpad((extract(month from now()))::text,2,'0') || '-01')::date");
	$sth->execute($params->{id});
	($rv->{redeemable_points}, $rv->{pending_points}) = $sth->fetchrow_array;
	$rv->{reward_points} = $rv->{redeemable_points} + $rv->{pending_points};
	$rv->{pending_points} ||= 0;
	return $rv;
}
1;
