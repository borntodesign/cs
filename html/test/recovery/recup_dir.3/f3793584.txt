ossible, let them know that you will help them take care of it when  you come back to get them set up.</p226>
<p227 new="6/25/09">CLICK HERE</p227>
<p228 new="6/25/09">for</p228>
<p229 new="6/25/09">Merchant Application</p229>
<p230 new="6/25/09">Printable Merchant Application and Terms of Agreement</p230>
<p231 new="6/25/09">Then confirm their Business Name and start to complete the application</p231>
<p232 new="6/25/09">When you get to the Referral Fee %, ask them what percentage they feel they would like to start at (reminding them that they can change it whenever they want). If they say they need to think about that, have them start at 5% (or if an exception, the lowest we allow) and if and when they want to change it, they can.</p232>
<p233 new="6/25/09">If they are "unable" to provide a credit card, offer them the option of making a deposit to set up a ClubAccount to deduct their fees from.</p233>
<p233a new="7/1/09">If they choose to set up a ClubAccount and prepay the deposit by giving you cash or a check (and pay for an upgrade package), provide them with a receipt (for cash) and deposit their payment into your bank account, then fund your ClubAccount (if necessary) with the same amount of money they have paid. Upon submitting the application, you will receive an email with directions on how to transfer the necessary funds from your ClubAccount to set up their ClubAccount.</p233a>

<p234 new="6/25/09">Go over what they will get with the</p234>
<p235 new="6/25/09">FREE Basic Option</p235>
<p236 new="6/25/09">, and show them how the various upgrade options can benefit them. Compare these one-year investments to one or two weeks of newspaper advertising.</p236>
<p237 new="6/25/09">Have them sign the application, then provide them with a copy of the Terms of Agreement and set an appointment as soon as possible, to come back and get them "set up". Once the application is signed, give them their card and write that card number down on their application. Note: Activate their card before submitting the application.</p237>
<p238 new="6/25/09">Overcoming Objections</p238>
<p239 new="6/25/09">- Member Application</p239>
<p240 new="6/25/09">Despite your best efforts, some may say "no" or more likely, put you off.</p240>
<p241 new="6/25/09">I need to think about it" or "I have to talk it over with _____</p241>
<p242 new="6/25/09">When this happens, ask them what specifically is it that they have to "think about" - the percentage they want to give OR how to have us debit their account? Do your best to answer the objection (as previously shown) but do not push them. Help them fill out a cardholder application, so they can see how it works and then find out when you should get back with them.</p242>
<p243 new="6/25/09">IT IS IMPORTANT TO AT LEAST LEAVE WITH THEM BEING A MEMBER</p243>
<p244 new="6/25/09">Note: Initially, put them in as a Clubshop Rewards Member, unless they have expressed an interest in referring others.</p244>
<p245 new="6/25/09">Tip:</p245>
<p246 new="6/25/09">Do NOT talk with them about referring others until AFTER the application is signed.</p246>
<p247 new="6/25/09">Send an email to them after their sign up to confirm the "Set Up" date and time and to provide them with a link to the Merchant Partner information, for them to consider doing.</p247>
<p248 new="6/25/09">Follow Up and Follow Through</p248>
<p249 new="6/25/09">For those that have not said "</p249>
<p250 new="6/25/09">no</p250>
<p251 new="6/25/09">", but have put off the decision, follow up with them after you have some "</p251>
<p252 new="6/25/09">news</p252>
<p253 new="6/25/09">" about new Merchants and/or card distribution.</p253>
<p254 new="6/25/09">More importantly, go out and work with someone else. Don't let meeting your goals for sign-ups be based solely on follow ups. Contact new Merchants!</p254>

<p256 new="7/8/09">ClubShop Rewards Merchant Set Up</p256>
<p257 new="7/8/09">After you have signed up a Merchant, you will need to arrange a time to get them "Set Up" to receive the CLUBSHOP REWARDS Card and train them on how to transmit the transaction information to HQ, as well as orient them on becoming a Merchant. You may also need some time to reacquaint them with the benefits of upgrading their Marketing Package, and becoming a Partner, by distributing cards.</p257>
<p258 new="7/8/09">As this "Set Up" time can take up to an hour (and because you may need more time to order a scanner or arrange to transfer funds to their ClubAccount), it is usually necessary to make another appointment to meet with them, after the sign up has been done. If the Merchant elects to pay their fees by credit card and submit the transaction information manually, and if you have access to an online computer, you may be able to get them "set up" directly following their "sign up".</p258>
<p259 new="7/8/09">Merchant Set Up Preparations</p259>
<p260 new="7/8/09">1. Determine their Method of Payment to pay for their future Referral Fees and their upgrade fees, if applicable.</p260>
<p261 new="7/8/09">Credit Card or Debit Card</p261>
<p262 new="7/8/09">- If their credit/debit card has been  approved, you will receive a notice of its approval, along with the  Merchant's ID number. Print out a copy of this notice for future  reference. If their credit card is not approved, only the Merchant will  receive a notice, which explains the reason for non-approval. If this  happens, you will need to follow up with the Merchant to use a  different credit card or to help them set up and fund their ClubAccount to pay for their fees.</p262>
<p263 new="7/8/09">ClubAccount</p263>
<p264 new="7/8/09"> - If the Merchant elects not to use a credit card or  debit card, then you will need to help them fund their ClubAccount. The  easiest way to do this is to have the Merchant pay you  directly,providing them with a receipt for the money they gave you.  They will need to provide you with enough funds to pay for their  deposit and to pay for any upgrade package, if selected. You will then  need to deposit their payment into your bank account and fund your ClubAccount if necessary, then submit a</p264>
<p265 new="7/8/09">Transfer  Authorization Form</p265>
<p266 new="7/8/09">to authorize our Accounting Dept. to  transfer the funds from your ClubAccount to the Merchant's ClubAccount.</p266>
<p267 new="7/8/09">2. Determine their method of transmitting the transaction information to DHSC.</p267>
<p268 new="7/8/09">MANUAL</p268>
<p269 new="7/8/09">- This is the best recommended method for most Merchants to begin with. If they elect to use this method by entering the  transaction information on a </p269>
<p270 new="7/8/09">Merchant  Transaction Entry Form</p270>
<p271 new="7/8/09">, determine if they have Internet access to  be able to input the information at their Merchant Interface, or if  they will need to Fax the form to you or if you will need to pick up  the form and input the transactions yourself. If a Manual method is  selected, the minimum acceptable time to transmit the transactions is  weekly, although the more frequently done the better. </p271>
<p272 new="7/8/09">CELLPHONE</p272>
<p273 new="7/8/09"> - A cellphone can also be used to manually submit text  messages with the required transaction information. This option will  require that a cellphone always be available and depending on the phone  plan the Merchant has, can be somewhat costly if sending individual  text messages for each transaction. For more technically savvy people, a cell phone can be used to record the text messages, then downloaded to a computer with Internet access, for the transactions to be "copied and pasted" into the Merchant's Transaction Online Entry form to be submitted.</p273>
<p274 new="7/8/09">SCANNER AND COMPUTER</p274>
<p275 new="7/8/09"> - In order to use this method, the Merchant will need to have access to a computer that can be online, at the point of sale. The type of scanner that we recommend that they use or purchase should be an inexpensive USB Barcode Scanner. You should research the most affordable scanners that are available where you live, so that you can refer the Merchant to purchase one, or so that  you can purchase one for your Merchant and have them pay you for the scanner when you get them "set up". If ordering a scanner online (preferably from a ClubShop Mall store), allow for the time it will  take to receive the scanner, in making your "set up" appointment.</p275>
<p276 new="7/8/09">SCANNER AND CELLPHONE</p276>
<p277 new="7/8/09"> - In order to use this method, the Merchant will need to have a cell phone (and text message plan) that can work with the required wireless scanner (LaserMagic Professional).  In addition, they will probably need for this cell phone to be used exclusively for this purpose during business hours. To view a list of acceptable cell phone platforms that will work with the LaserMagic Professional Scanner, and to order a scanner, go to the Serialio Store at the ClubShop Mall.</p277>
<p278 new="7/8/09">If the Merchant's cellphone is not listed, they may consider purchasing a new cellphone to use exclusively  for this purpose. Therefore, you should also research the most affordable cellphones and text messaging plans in your area, that will work with the scanner.</p278>
<p279 new="7/8/09">Set Up Procedures</p279>
<p280 new="7/8/09">1. Make sure that your "Set Up" appointment is made during a time that the Merchant will not be distracted or interrupted. Bring everything necessary to get the Merchant properly "set up", including:</p280>
<p281 new="7/8/09">Merchant ID Number</p281>
<p282 new="7/8/09">Internet accessible computer (theirs or yours)</p282>
<p283 new="7/8/09">Transaction Manual Entry Forms (at least ten)</p283>
<p284 new="7/8/09">Merchant Door Sticker</p284>
<p285 new="7/8/09">Merchant Receipt Stamp</p285>
<p286 new="7/8/09">Copy of their Merchant Application (if previously completed  printed application)</p286>
<p287 new="7/8/09">If applicable, Scanner or Cell Phone (if purchased by you for them)</p287>
<p288 new="7/8/09">If applicable, ClubShop Rewards Cards for them to purchase and distribute (at least 100) and any other Partner materials (Brochures, List of Merchants, Cover Letters, Application Drop Box)</p288>
<p289 new="7/8/09">2. Have them open their Merchant Interface page and explain to them how to navigate the site to find the information they may need. Using the directions at the Merchant Set Up page at their Merchant Interface, train the Merchant on how to record and submit transactions, depending on the method(s) they elect to use. ALWAYS show them how to use the Manual Method as a back up method, in case they experience a problem with their Internet access, cell phone or scanner use.</p289>
<p290 new="7/8/09">3. Explain how to initial or stamp receipts. Go through how they will  process any returns, to ensure that they will not pay a Referral Fee on  any returned items.</p290>
<p291 new="7/8/09">4. Show them their Merchant Transaction Report and explain how they will get an email on the first of each month, confirming the transactions and the amount they will be paying the company on the 5th f each month as Referral Fees. </p291>
<p292 new="7/8/09">5. Review their Payment Method and if they are using their ClubAccount,  train them on how to open their ClubAccount page and how to fund their ClubAccount for payment of future Referral Fees.</p292>
<p293 new="7/8/09">IMPORTANT - THIS IS REQUIRED FOR THE MERCHANT TO BECOME "ACTIVE" AND FOR THEIR LISTING TO APPEAR AT CLUBSHOP.COM</p293>
<p294 new="7/8/09">. Return to the Merchant Set Up page and have the Merchant read the statement at the bottom of the page and click on the Activate button. Then show them their listing at </p294>
<p295 new="7/8/09">7. Answer any questions they may have and explain when the Card Distribution Phase for your campaign will take place. Leave them with your business card and ask them to contact you once they have their first ClubShop Rewards customer, so you can make sure that the everything went okay. Apply their Merchant Sticker to the inside of a glass front door or the inside of a window next to the front door (next to any credit card stickers), on your way out.</p295>
































<p71>Coming soon.</p71>
<p72>Your Merchant Presentation Includes</p72>
<p73>Pre Registration Tools</p73>
<p74>Print Merchant fliers</p74>
<p75>Print a Merchant pre-registration application form</p75>
<p76>Online Pre-Registered Merchant Application</p76>
<p77>Pre-registered Merchant list</p77>
<p78>Pre-registered Merchant phone contact guide</p78>
<p79>Pre-registered Merchant cover letter</p79>
<p80>Pre-registered Merchant personal contact guide</p80>
<p81>Pre-registered Merchant prospecting email</p81>
<p82>Pre-registered Merchant prospecting postal letter</p82>
<p83>New Pre-registered Merchant welcome email sent by DHSC</p83>
<p84>Pre-registered Merchant update email sent by DHSC</p84>

<p85 new="6/25/09">Online Merchant Application</p85>
<p86 new="6/25/09">Printable Merchant Application &amp; Terms of Agreement</p86>
<p87 new="7/1/09">Merchant Transaction Entry Form</p87>
<p300 new="7/27/09">Print Quality Images</p300>




</lang_blocks>

