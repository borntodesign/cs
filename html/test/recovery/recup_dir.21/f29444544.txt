archive all list messages it sends. Archived messages are archived with the list message, its subject and the date it was sent. No header information is saved. Mojo Mail also allows you to view these archives online, and provides a many features to make this quite a pleasant experience.

=back 

=head2 How does Mojo Mail save its information?

Mojo Mail saves its information in many different files, in two formats, plain text and DB_File. 

The list subscription list is saved in a plain text file, with one email per line. The file is called 'list_name.list' where 'list_name' is the name of your list, with underscores instead of spaces. Blacklists are stored in a similar way, but with a 'blacklist' extension. 

DB_Files are used to saved list information and to archive messages. Mojo Mail is flexible in what DB_File package you use, be it DB_File, GDBM, ODBM, or SDBM

list information is stored with one preference per key, archived messages are saved with the date sent as the key, looking like: 20010308151009 (YYYYMMDDHHMMSS) with the value being the subject, the message and then the message format, delimited by double colons '::'


=head2 Does Mojo Mail support the saving of more than the email address? 

No. At the present time, only the email address is saved. Fields such as First Name, Last Name, Street Address, Zip Code, etc aren't. There are tentitive plans to create this, but it would mean quite a major overhaul of the program. 

=head2 Who develops Mojo Mail 

Justin Simoni is the lead designer of Mojo Mail. Many other people have helped out along the way, but most everything is the work of his grubby little hands. 

=head2 How is Mojo Mail developed? Can I help? Is there a CSV server I can check in/out of? 

Mojo Mail is developed internally. If you want to make a change, or submit a bug fix/patch, just email Justin at justin@skazat.com. There is plans on opening up a CSV server for Mojo Mail, but we're lazy and not too bright on how to use a CVS server.  







  
  
  
  
