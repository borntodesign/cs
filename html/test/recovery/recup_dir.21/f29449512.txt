=pod

=over

=item 2.6.4

The outside config file scheme has been changed!

Please refer to the Config.pm docs to see how to use the outside configuration file

=back

=cut
