# cgi.ini

# these are generic parameters which will be used to logon to the DB in the event a process doesn't supply them
username:cgi
password:automate

# database flag will disable/enable all user functions
database:on
dbserver:192.168.17.2

# these take the form of "off" or "on"
appx:on
newviplist:on
# a GI VIP script
member-followup.cgi:on

# a generic entry for general use - currently the UserInfo Axkit taglib is the only one
# If other parts use it, be sure they are global in nature and use simple queries
generic:on

# the redirection/tracking script in cgi-bin
# this handles/will handle all the shopping links out and some specific incoming traffic
rd:on

# the pixel tag script
tr:on

# the admin resource redirector
admin-rd:on

# mail tracking
mt:on

# the sales tracking done with image tags - don't turn off!
img_tag_tracking:on

# app.cgi is the upgrade/info update application
app.cgi:on

benefits_redirect.cgi:on

# online cancellation application
cancellation.cgi:on

# Cash Card application
cashcard_app.cgi:off

# ClubPerks application
clubperks_app.cgi:on

cancel_db.cgi:on

# Member Portal Page
mpp:on
mpp.cgi:on

# personal postlaunch reports
ppreport:on

# Request a report on a member's downline VIP network.
network_reports.cgi:on

# Request a report on a member's downline VIP network.
network_reports_generator.pl:on

# Display a members reports from the request table.
network_reports_status.cgi:on

# Historical month-end network calcs reports.
net_calcs.cgi:on
org_calcs.cgi:on

# our administrative main database interface
db.cgi:on

# Do Not Solicit registration script.
do_not_solicit.cgi:on

# Charitable Org. nomination interface.
co_nomination.cgi:on

# Charitable Org. admin interface.
co_admin.cgi:on

# Non-VIP PrePay refund process.
prepay_refunds.cgi:on

# Charitable Org. donation payout process.
co_payout.cgi:on

# Charitable Org. donation selection interface.
co_donate.cgi:on

# Charitable Org. donation report.
co_trans_rpt.cgi:on

# Consumable Goods interface.
consume.cgi:on

# Merchant specials for SRW's admin interface.
srwspec_admin.cgi:on

# Merchant specials listing for the general public.
specials.cgi:on

# Vendor specials admin for adding and modifying specials.
specials_admin.cgi:on

# our marketing goal tracking report.
goal_tracking.cgi:on

# our e-business course status interface.
ebiz_sessions.cgi:on

# our e-business course admin interface.
ebiz_course_admin.cgi:on

# our Marketing Director research submission form.
md_research.cgi:on

# our auto_pilot sign-up form.
auto_pilot.cgi:on

# our auto_pilot co-op order insertion program.
auto_pilot_fill.pl:on

# our co-op member order interface for VIPs.
coop_order.cgi:on

# our co-op member replacement interface for VIPs.
coop_replace.cgi:on

# our co-op member order backend processor.
coop_order_fill.pl:on

# our co-op member admin interface for staff.
coop_order_admin.cgi:on

# our co-op member transfer admin interface.
coop_transfer.cgi:on

# our co-op member import admin interface.
coops_import_admin.cgi:on

# our remote co-op member import. 
remote_coop_signup.cgi:on

# our translation queue admin interface.
tq_admin.cgi:on

# Member commission status admin interface.
member_comm_status.cgi:on

# Records member actions and sends an email to upline in certain cases.
member_actions.cgi:on

# our self replicating websites
members:on

# Currency conversion tool.
converter.cgi:on

# Reward Points redemption center
reward_center.cgi:on

# Reward Points admin
rewards_admin.cgi:on

# Reward Points report
reward_points.cgi:on

# VIP vertical line reports
vlinerpt:on

# Member language preference selection form.
language_pref.cgi:on

# Member line reports
line_rpt.cgi:on

# administrative member record interface
memberinfo.cgi:on

member_trans_rpt.cgi:on

# Merchant Affiliate record search interface
merchant_survey61103.cgi:on

# Merchant Affiliate coupon report
ma_coupons.cgi:on

# Merchant Affiliate coupon editing interface
ma_coupons_edit.cgi:on

# Merchant Affiliate discount editing interface
ma_discount_edit.cgi:on

# Merchant Affiliate email editing interface
ma_mailing_admin.cgi:on

# Merchant Affiliate record search interface
maf_info.cgi:on

# Merchant Affiliate transaction entry interface
maf_transactions.cgi:on

# Merchant Affiliate transaction reporting
ma_trans_rpt.cgi:on

#Merchant Affiliate funding redirect
maf_financials:on

#Merchant Affiliate Registration
merchant_applications:on
# the process that does member ID lookups
findme:on

# Allows an admin to deposit funds into a VIP's pre-pay account.
funding_admin.cgi:on

# Allows a  member to fund their pre-pay account.
funding.cgi:on

# Allows us to receive pre-pay account funds via moneybookers.com.
mb_funding.cgi:on

# Allows a  member to pay for something via their pre-pay account.
pay4_others.cgi:on

# Allows a  member to void a transaction in their pre-pay account.
void_trans.cgi:on

mall_redirect.cgi:on

# VIP genealogy reports
genealogy:on

# Genealogy trees
tree.cgi:on

# Business category search script.
biz_cat_search:on

# Purged members mailing form.
mp_mail_form.cgi:on

# online members_purge removal system
mp_remove.cgi:on

# online removal system
mailstatus:on

# in-house bulk mailing interface
mail_job.cgi:on

# GLOBAL login will replace the other two once scripts are converted
GLOBAL_LOGIN:on
VIP_login:on
m_login:on

# the member transfer interface for VIPs
mv-members:on

test.cgi:on

# administrative terminations interface
process_terminations.cgi:on

# administrative member record searching interface
admin_search.cgi:on

# adminstrative reinstatement interface
reinstate.cgi:on

# adminstrative mail_refused_domains mgt.
domains_blocked:on

# the Method Of Payment Inerface
mopi.cgi:on

spam2.cgi:on

# cert.cgi is the certificate producer for ebiz test congrats
cert.cgi:on

# ebiz will control the entire suite of scripts
ebiz:on

# listcomm is the script that serves up the commission reports
listcomm:on

# Allows the VIPs to change their commission hold and payment preferences.
comm_prefs.cgi:on

# the clubdepot ID preprocessor
validate_id.cgi:on

# the script that displays the list of domains that block our mail
domain_blocks:on

# the script that allows VIPs to purchase convention tickets with prepay acct.
pp_convention.cgi:on

# the script that allows VIPs to change their method of payment (MOP)
ppmop:on

# the script that generates banners for clubdepot users
banners:on

# HTMLcontent is a  script that presents a list of cgi_objects to edit.
HTMLcontent.cgi:on

# cgi_Object_Editor.cgi is an Editor to alter the characteristics of a template object
cgi_Object_Editor.cgi:on

# A simple script to preview a template in a browser.
template_preview.cgi:on

# cgi_Object_Recovery.cgi produces a list of recoverable cgi objects
cgi_Object_Recovery.cgi:on

# cgi_Object_Viewer.cgi verification and recovery of a cgi object
cgi_Object_Viewer.cgi:on

# Help modules for the Template management system
OEhelp4.cgi:on
OEhelp3.cgi:on
OEhelp2.cgi:on
OEhelp1.cgi:on

# Month end switch off utility
MonthEndSwitch.cgi:on

# VIP upline script 
show_upline.pl:on

# VIP upline Script CGI version
show_vip_upline:on

# VIP Accountability system.
vipacc.cgi:on

# XML re-utilization script.
xml_compare.cgi:on

# the scripts for ebiz testing stuff
test-suite:on

# pt_view.cgi Public Template Viewer
pt_view:on

# mdi.cgi  Email verification CGI allows members to re-affirm that their email address is now good!
mdi.cgi:on

# Potential Members search
pmembers.cgi:on

# Upgrade shoppers to members.
NetShopUpgrade.cgi:on

# View the freedomsarmy.org log file
falog.cgi:on

# Create Affinity Group or Affinity Group Unit
cag.cgi:on

# Email Deletion Schedule
edelschedule.cgi:on

# Message Sub System
msg_sub_sys.cgi:on
msg_sub_sys:on

administrative_suspensions.cgi:on

# Print Do NOT email list
address_removal.cgi:on

# The utility for adding to the do NOT email list.
do_not_email.cgi:on

# Display the current status of the outstanding mail jobs
mail_jobs_status.cgi:on

# the monthend upline checker
upline_ck.cgi:on

# The statement of Accounts
SOAreport.cgi:on

# the WebLink manager
pwl-mgr.cgi:on

# the Online Merchant Affiliate report by description
oma_rpt.cgi:on

# the combination Member/Merchant Affiliate pre-registration application
mbr-ma-app:on

# the existing Merchant Affiliate pre-registration application
x-ma-app.cgi:on

# Merchant Affiliate (the real deal)
ma-master-app.cgi:on

# Merchant Affiliate Location Application
ma-location-app.cgi:on

# Admin Merchant Affiliate Location/Vendor management
ma_location_admin.cgi:on

# the Real/Active Merchant Affiliate by location report
ma_real_rpt.cgi:on

# the Real/Active Merchant Affiliate admin form
ma_real_admin.cgi:on

#Merchant Stuff
merchants:on

#Merchant Affiliate (internal) Administration Interfaces
merchants_admin:on

#Merchant Affiliate Site
merchant_affiliates:on

# the Merchant Affiliate by location report
mafrpt.cgi:on

# clubbucks scripts will be here.

# The ClubBucks soft-card display script.
cbcards.cgi:on

# The ClubBucks soft-card order script.
cbneworder.cgi:on

# The ClubBucks personal card center script.
cbmycards.cgi:on

# The ClubBucks report of cards referred to by a member ID.
referral_cards.cgi:on

# The ClubBucks card admin script.
cbid_info.cgi:on

# The ClubBucks hard-card order script.
cbid_orders.cgi:on

# The ClubBucks card order admin search script.
cbid_order_info.cgi:on

# The ClubBucks card ownership transfer script.
cb_card_transfer.cgi:on

# the activation frontend for ClubBucks Cards
activate:on

# The state lookup script for member app's.
get_states.cgi:on

# CBID batch reporting
cbid_batch_rpt.cgi:on

# CBID batch management
cbid_batch_mgt.cgi:on

# admin Merchant mtd transaction stats
merch-trans_stats.cgi:on

# SOA Billing reconcilation module
soa_billing_recon.cgi:on

# Vendor Management System key
index.cgi:on

# Transaction Management System key
trans-index.cgi:on

# opt in/out interface for the automated member transfer system directed towards new VIP upgrades
opt2003:on

# the member selection/deselection interface for the above and the ARS system
opt2003-xl:on

# activity reports membership
activity_rpt:on

# admin activity report
activity-report.pl:on

# Balanced Budget Adjustment Report
bba_report:on

# auto-recycle-stack mgt. interface
arsmgt.cgi:on

# auto-recycle-stacking participation election
ars_election:on

# VIP email/membership processing tool
#bulk_bem:on	retired in place of the next incarnation
MemberModification:on

# an SSI script in the httpd/scripts directory
fill_it_in.pl:on

member_frontline.cgi:on
JumpStart:on
Unpaid_VIPs:on

member_detail.cgi:on
dream_team_approvals:on

# Network Building (Dream) Team Qualifications report.
nbteam_quals.cgi:on

# the member 'control panel'
mem_cpl:on

# admin application for managing translation templates
translation_templates:on

translation_editor:on

# Vested Income Report
virpt.cgi:on

# our background static file parser
Insert:on

# a simple login geared toward easy shopping
miniLogin:on

# the aggregate sales report for VIPs
sales-rpt.cgi:on

# the Ongoing Role Requirements report
orr.cgi:on

state-list-js.cgi:on

# the Potential Income report
pireport.cgi:on

# the Upgrade Marketing page
um:on

srw_specials:on

# functionality for the 'Team Pages'
team_pages:on

# the 'Team Leader' redirecter
tl-redir:on

# Profile Questions Admin Interface
profile_questions_admin.cgi:on

# rbldnsd zone file generator
zone.pl:on

# Linkshare Affiliate/Vendor Name mapping editor
aff2vendor.cgi:on

tracking_search.cgi:on

EventLogin:on

# the special coop interface managed by Chiefs (11-08-07)
spec-coop:on

auction.cgi:on

# the Personal Web Page system
pwp:on

# our mall icon mouseover feeding script
iconFeed:on

# our mall store info box feeding script
storeInfoFeed:on
# the mall store listing feed
vendorList:on
# our mall category lists
getCatLists:on

#Label for questionnairs
questionnairs:on

# a 'temporary' report for VIPs to see what memberships haven't filled out the survey or made the initial purchase
spec1:on

30-day-cancel-report:on

# the import script for working with lead providers
coop-import:on

auction-mgt.cgi:on

# Browser View script
bv:on

merchant_comm_rpt.cgi:on
merch-frontline-rpt.cgi:on
vip-merch-referral-comp.cgi:on

#Used for the cron job that builds the rewards card cache.
build_reward_cards_cache:on

# a GI script
scrd.cgi:on

# Glocal Income
# Locator Map
glocal_income_locator_map:on

# the new GI cancellation interface
cancel:on

joinpool.cgi:on

# a GI script for partner pool reports
pprpt:on

# New Clubcash functions
clubcash_monthly_detail.cgi:on
clubcash_redemption.cgi:on
clubcash_redemption_list.cgi:on


# a GI activity tracker/redirector
GI_inbound_activity_tracking.cgi:on

# a CS report primarily for card activations based upon ranges of cards in a cbid_orders record
card-order-reporting:on

# maps information (replaces rewards directory)
ipaddress.cgi:on
setaddress.cgi:on

# the GI member/partner map
1p.cgi:on

# odp.pm is the class used by the One Downline Pool "presentation"
odp.pm:on

# member id lookup
idemailmap.cgi:on

# transaction reports related to commissions from the referral compensation plan
transrpt:on

gps_modification:on

# a merchant script
maf/setup.cgi:on

# the Partner Subscription Access scripts
PSA:on

merchant_update_payment:on

CommStmnt.cgi:on
PP.cgi:on
activity_report_instructions.cgi:on
gainTP.cgi:on
TrialPartner:on
MeetCoaches:on
