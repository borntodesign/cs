e updateMerchantLocationInformation method.
	
	2009-11-04
	Keith
	Added the "retrieveAllMerchantLocationsByMerchantID" method.
	
	2009-12-12
	Keith
	Added the "updateVendorBannerByMerchantID" method, and modified 
	the "updateMerchantMasterInformation" method so merchants can 
	add, and delete their banner to the merchant listing.
	
	2010-01-07
	Keith
	Added the "createMerchantSubscription" method, modified 
	the "createMerchant" method, and modified the 
	"createMerchantMasterInformation" so they utilize 
	the "createMerchantSubscription".

	2010-01-07
	Keith
	Modified the "createMerchant" method so the club account
	transaction entries include the Merchant ID.

	2010-01-13
	Keith
	Modified the following methods to truncate the Business Name field,
	if it is more than 30 characters long.
	
	createMerchant
	createMerchantLocationInformation
	createMerchantMasterInformation
	insertPendingMerchantInformation
	updateMerchantLocationInformation
	updateMerchantMasterInformation
	updatePendingMerchantInformation
	
	2010-02-22
	Keith
	I added the createMerchantDiscount Method,
	and modified the updateMerchantDiscount Method
	so when the Discount Information is updated a new
	discount record is created, and the previous discount
	records end_date field is set.
	
	2010-03-02
	Keith
	I added some error trapping to the createMerchantDiscount Method,
	for the discount parameter.

	2010 Mar 10	
	Keith
	Modified the createMerchant method, so if a merchant has applied
	and they supply a member ID that is an Associate Member, we use the
	associates SPID for the Merchant Affiliate memberships SPID

	2010 Jun 29
	Keith
	I added the "getPackageUpgradeValues" method to help determin if a 
	merchant is upgrading, downgrading, or staying at the same level.
	
	2011 Feb 9
	Bill
	revised various SQL modifiers to employ db->quote instead of directly inserting the string into the SQL

	2011 Feb 25
	Bill
	added 'amp' to the list of valid merchant sponsors
	
       2011 Sept 22
       George
       getMerchantLocationCoordinates  added to get lat/long coordinates from google geocoder

=cut
