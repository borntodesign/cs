 is available for your local area to make presentations with. It is recommended that you print out the presentation in full color (see the PDF link below the first page of the presentation) and put each page in a plastic sleeve in a 3 ring binder to create a flip chart presentation.</p202>

<p203 new="6/25/09">Curacao, Netherlands Antilles (Dutch)</p203>
<p203a new="6/25/09">Curacao, Netherlands Antilles (English)</p203a>
<p204 new="6/25/09">Netherlands</p204>
<p205 new="6/25/09">Italy</p205>
<p205a new="8/6/09">USA</p205a>
<p205b new="8/6/09">Belgium</p205b>
<p205c new="8/21/09">Canada</p205c>
<p205d new="8/25/09">Suriname</p205d>
<p205e new="8/25/09">Aruba (English)</p205e>
<p205f new="8/25/09">Belgium (French)</p205f>
<p205g new="8/27/09">Australia</p205g>
<p205h new="8/27/09">Great Britain</p205h>
<p205i new="9/14/09">Tunisia</p205i>
<p205j new="10/12/09">Aruba (Dutch)</p205j>

<p206 new="6/25/09">Presentation Overview and Stages</p206>
<p207 new="6/25/09">What is an assumptive close?</p207>
<p208 new="6/25/09">You are able to assume that your prospect will sign up due to overcoming potential objections during the presentation process.</p208>
<p209 new="6/25/09">Why can you make the assumption that they will sign up when you are done with the presentation?</p209>
<p210 new="6/25/09">Because you will have qualified them and addressed the 3 major objections they may have, during the interview and presentation process.</p210>
<p211 new="6/25/09">The interview</p211>
<p212 new="6/25/09">Develop empathy and find out what they have done or are doing to advertise, promote or have a customer loyalty program.</p212>
<p213 new="6/25/09">Ask them how their POS (Point of Sale) system works (this will help you in the presentation) and if they have Internet access.</p213>
<p214 new="6/25/09">Qualify them</p214>
<p215 new="6/25/09">The KEY point to determine before proceeding with the presentation</p215>
<p216 new="6/25/09">Are they willing to advertise, promote, discount, etc. to get more business?</p216>
<p217 new="6/25/09">If they answer "no" to this question, do not continue with the presentation - give them a card and sign them up as a free Member. (This may get them asking questions and make them reconsider their willingness to participate as a Merchant).</p217>
<p218 new="6/25/09">Use the flip chart to do the presentation</p218>
<p219 new="6/25/09">Three main talking points to stop and "tie them down":</p219>
<p220 new="6/25/09">The Transaction Fee percentage. After going over the fee and marketing capability information, ability to change percentages and canceling, ask them, "Do you feel that you could do at least 5%?" (Unless you are visiting an exception type of business, then quote the applicable percentage 