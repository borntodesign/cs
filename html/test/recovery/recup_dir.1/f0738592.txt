1) Get server running
2) Load desired Server Software
   a) 6.0 Centos
   b) Apache 2.2 or Apache 2.3
   c) Cocoon
   d) Latest Perl
   e) PHP
3) Transfer clubshop software over to new server
   a) rewrite Authorization modules
   b) consolidate liked named modules and includes
   c) convert all scripts/pages using axkit (depending on axkit rewrite) **
   d) remove unused scripts/pages/files (a little over 90,000 files currently)
      56 direcdtories in cgi
       2 directories in cgi-bin
       1 directory in cgi-data
      13 directories in cgi-lib
      15 directories in cgi-templates (if they can still be used from c above)
    3043 directories in html
       9 directories in icon
       8 directories in scripts
