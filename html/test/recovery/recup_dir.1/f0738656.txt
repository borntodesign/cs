Goal

1) Get all services off the old server onto the new
2) Clean up all files that are duplicate 
3) Get Rid of files not curreently being used.

1) Get server running		Done
2) Load desired Server Software
   a) 6.0 Centos		Done
   b) Apache 2.2 or Apache 2.3	Done
   c) Cocoon			Does not work for our needs
   d) Latest Perl		Done
   e) PHP			Done
   f) rewrite Axkit		(Being done by original author)
3) Transfer clubshop software over to new server
   a) rewrite Authorization modules				Done
   b) consolidate liked named modules and includes		This will happen as more files and scripts are migrated.
   c) convert/rewrite all scripts/pages using axkit (depending on axkit rewrite) **
   d) remove unused scripts/pages/files (a little over 90,000 files Clubshop, 10,700 GlocalIncome currently, 2 directories that I hope to be able to delete have over 28,000 files in them)
      ClubShop
      56 directories in cgi
       2 directories in cgi-bin
       1 directory in cgi-data
      13 directories in cgi-lib
      15 directories in cgi-templates (if they can still be used from c above)
    3043 directories in html (members, maps, mall is currently done,  cs and merchants are partually done, )
       9 directories in icon
       8 directories in scripts
    4687 xml files
    1755 xsl files
      GlocalIncome
      10 directories in cgi
       1 directory in cgi-bin
       4 directories in cgi-lib
       5 directories in cgi-templates (if they can still be used from c above)
     926 directories in html
      25 directories in test
     935 xml files
     157 xsl files

My plan is to go through each page in the html directories and test them as they are being copied over. Getting those files copied over and running should also bring the majority of the cgi directories over as they are mostly accessed by the pages in the html. 
4) Set up load balancing
5) Centralized Staging/Deployment area for content including translations interface 
6) Rsync solutions to make each server in the set a mirror of the "master" 


** Bill has contacted the original author of Axkit and has been told that he is working on porting the package to the lastest version of Cent and Apache.
