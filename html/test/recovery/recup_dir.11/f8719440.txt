<base>
<last_updated>Wed Jul 11 09:35:41 2007</last_updated>
<upline><leader id="3078815">Luigi De Siato</leader>
<leader id="159991">Fabrizio Perotti</leader>
<leader id="3721659">Viola Pieternella</leader>
<leader id="3473613">Stefania Ghiani</leader>
<leader id="170520">Dindo Barriga</leader>
<leader id="3726857">Oswin Pieternella</leader>
<leader id="3462963">Jean Preda</leader>
<leader id="3473302">Jan Braham</leader>
<leader id="3227891">Rens Van Houte</leader>
<leader id="3431833">Parolini Giovanni</leader>
<leader id="2758344">Gino Descamps</leader>
<leader id="2801365">Giuseppe Francavilla</leader>
<leader id="11860">Robbie Bracco</leader>
<leader id="22034">Roland Dufault</leader>
<leader id="3256568">Giacinto Magliocco</leader>
<leader id="3634135">Charles Haras</leader>
<leader id="3732538">Hensley Basilio</leader>
<leader id="3222069">Benedict Landerwyn</leader>
<leader id="3533741">Giel Verdaasdonk</leader>
<leader id="3276350">Wim Groot Zwaaftink</leader>
<leader id="3192129">Sandra Van den Bergh</leader>
<leader id="3741912">Davide Bottoni</leader>
<leader id="3541881">Loraine Brouwn</leader>
<leader id="3077498">Scott Cunningham</leader>
</upline><graduates><vip id="3634135" trainer="3541881" chief="3227891" role="140"><name>Charles Haras</name><country>AN</country><pwl>http://www.thuisbusiness.cc/english.htm</pwl></vip>
<vip id="3705748" trainer="3533741" chief="3276350" role="120"><name>Johan Mol</name><country>NL</country><pwl>http://www.geocities.com/johanmolholland/OverMij.html </pwl></vip>
<vip id="3726857" trainer="3721659" chief="3634135" role="120"><name>Oswin Pieternella</name><country>AN</country><pwl>http://www.freewebs.com/sandriver/home.htm</pwl></vip>
<vip id="3652981" trainer="3473613" chief="2801365" role="115"><name>Roberto Spanicciati</name><country>IT</country><pwl>http://www.ilbusinessonline.com/robertospanicciati.htm</pwl></vip>
<vip id="3727586" trainer="3726857" chief="3634135" role="115"><name>Sorine Deira</name><country>AN</country><pwl>http://www.freewebs.com/marielendclub/mypage.htm</pwl></vip>
<vip id="3843547" trainer="3462963" chief="3222069" role="110"><name>Sidi Zidani</name><country>CH</country><pwl>http://marketing.puissanceweb.info/aboutme.html</pwl></vip>
<vip id="3800082" trainer="3473613" chief="2801365" role="100"><name>Roberto Calledda</name><country>IT</country><pwl>http://www.businessclub.it/robcalledda.html</pwl></vip>
<vip id="3862663" trainer="3732538" chief="3634135" role="100"><name>Renny Albertus</name><country>AN</country><pwl>http://www.geocities.com/reobal/Renny.html</pwl></vip>
<vip id="3854413" trainer="3533741" chief="3276350" role="100"><name>Tim Boudewijns</name><country>NL</country><pwl>http://www.extrainkomsten.eu</pwl></vip>
<vip id="3835376" trainer="3192129" chief="2758344" role="100"><name>Bran Tiberiu</name><country>RO</country><pwl>http://market-group.tripod.com</pwl></vip>
<vip id="3828538" trainer="3462963" chief="3222069" role="100"><name>Alain Fraise</name><country>FR</country><pwl>http://reseaux.puissanceweb.info/aboutme.html</pwl></vip>
<vip id="3794322" trainer="3741912" chief="2801365" role="100"><name>Francesco Palma</name><country>IT</country><pwl>http://www.beepworld.it/members/francescopalma</pwl></vip>
<vip id="3898030" trainer="3078815" chief="159991" role="100"><name>Stefano Massaccesi Loreti</name><country>IT</country><pwl>http://it.geocities.com/stefano_massaccesi60/miapagina.html</pwl></vip>
<vip id="3684964" trainer="3741912" chief="2801365" role="100"><name>Pierluigi Furl