body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: transparent;
}
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #999999;
}
.Text_Content {	margin-bottom: 15px;
    line-height: 18px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #333333;
}
.nav_main:link {
	color: #FFFFFF;
	text-decoration: none;
	padding-left: 17px;
}
.nav_main:visited {
	color: #FFFFFF;
	text-decoration: none;
	padding-left: 17px;
}
.nav_main:hover {   
	color: #FFBC22;
	text-decoration: none;
	padding-left: 17px;
}
.nav_main:active {
	color: #FFFFFF;	
	text-decoration: none;
	padding-left: 17px; 
}
.footer_copyright {	
	font-size: 9px;
	color: #666666;
}
.nav_footer:link {
	color: #999999;
	font-size: 10px;
	text-decoration: underline;
	font-weight: normal;
}
.nav_footer:visited {
	color: #999999;
	font-size: 10px;
	text-decoration: underline;
	font-weight: normal;
}
.nav_footer:hover {
	color: #FFFFFF;
	font-size: 10px;
	text-decoration: none;
	font-weight: normal;
}
.nav_footer:active {
	color: #999999;
	font-size: 10px;
	text-decoration: underline;
	font-weight: normal;
}