<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
<xsl:comment>[if IE]&gt;
	&lt;link rel="stylesheet" type="text/css" href="/css/gi-IE.css" /&gt;
&lt;![endif]</xsl:comment>

            <title><xsl:value-of select="//lang_blocks/p0"/></title>

<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

body {
   background-color: #245923;
} 
li {padding-bottom: 20px;}
li li{list-style: uppercase;}

</style>

</head>
<body>
<div align="center"> 



<table width="950px" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
		<tr>
		<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">      
		<tr>
		<td background="/images/header/bg_header.jpg"><img src="/images/gi.jpg" width="640" height="70" align="left"/></td>
	    </tr>
      
      	<tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">
          
          
          <tr>
            <td align="right">
<a href="javascript:window.close();"><img src="/images/icons/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" 
class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>


      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="left">

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p0"/></span>

<ol>

<li class="a"><b><xsl:value-of select="//lang_blocks/p2"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p4"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p6"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p8"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9"/></li>

<xsl:value-of select="//lang_blocks/p11"/><br/><br/>


<li class="a"><b><xsl:value-of select="//lang_blocks/p12"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>
<li><b><xsl:value-of select="//lang_blocks/p14"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/fees/full.xml" target="blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p17"/></a></li>

    <ol class="sub">
A: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19"/><br/><br/>
B: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p21"/><br/><br/>
C: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/><br/><br/>
D: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23"/><br/><br/>
E: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24"/><br/><br/>
F: <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p25"/><br/><br/>
    </ol>






<li class="a"><b><xsl:value-of select="//lang_blocks/p26"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></li>
<!--10/24/10
<li class="a"><b><xsl:value-of select="//lang_blocks/p28"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29"/></li>
-->

<li class="a"><b><xsl:value-of select="//lang_blocks/p30"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p32"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33"/></li>

<!--10/24/10 
<li class="a"><b><xsl:value-of select="//lang_blocks/p34"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p35"/><br/><br/>
-->

<li class="a"><b><xsl:value-of select="//lang_blocks/p75a"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p75"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p36"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p37"/></li>

<!--10/24/10
<li class="a"><b><xsl:value-of select="//lang_blocks/p38"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39"/><xsl:text> </xsl:text><a href="/manual/policies/vipsponsor.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p40"/></a>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p42"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p43"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p44"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45"/></li>
-->


<li class="a"><b><xsl:value-of select="//lang_blocks/p46"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47"/></li>
<li><b><xsl:value-of select="//lang_blocks/p48"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p49"/></li>
<xsl:value-of select="//lang_blocks/p50"/><br/><br/>

<li class="a"><b><xsl:value-of select="//lang_blocks/p51"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p52"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p53"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p54"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p55"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p56"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p57"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p58"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p59"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p60"/></li>

<li><b><xsl:value-of select="//lang_blocks/p61"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p62"/><br/><a href="http://www.glocalincome.com/cancel/" target="_blank" class="nav_footer_brown">http://www.glocalincome.com/cancel/</a><br/><!--<xsl:value-of select="//lang_blocks/p63"/><xsl:text> </xsl:text><a href="mailto:acf1@dhs-club.com?Subject=30 Day Refund">acf1@dhs-club.com</a> <xsl:text> </xsl:text>--><xsl:value-of select="//lang_blocks/p64"/></li>

<!--<xsl:value-of select="//lang_blocks/p65"/><br/>-->
<xsl:value-of select="//lang_blocks/p66"/><br/>

<br/><b><xsl:value-of select="//lang_blocks/p67"/></b><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cancel/" target="blank" class="nav_footer_brown">http://www.glocalincome.com/cancel/</a>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p68"/><br/>
<br/>

<li class="a"><b><xsl:value-of select="//lang_blocks/p69"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p70"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p71"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p72"/></li>
</ol>

</div>

</td></tr></table>


  
    </td>
  </tr></table>
  



</td></tr></table>

<tr><td align="center"><span class="footer_copyright">| <a href="/manual/index.xml" target="_blank" class="nav_header">Home</a> | <a href="https://www.clubshop.com/vip/control/index.shtml" target="_blank" class="nav_header">VIP Control Center</a> | <a href="http://www.clubshop.com/ppreport.html" target="_blank" class="nav_header">Member Portal Page</a> |</span></td></tr>
<tr><td align="center"><div align="center"><img src="/images/icons/icon_copyright.gif" height="20" width="306" alt="copyright"/></div></td>
</tr>


</td></tr></table>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
