/**
 * 
 */
var plabels = {
	    1 : 'Active Partner',
	    2 : 'Team Player',
	    3 : 'Team Leader',
		4 : 'Team Captain',
		5 : 'Bronze Manager',
		6 : 'Silver Manager',
		7 : 'Gold Manager',
		8 : 'Ruby Director',
		9 : 'Emerald Director',
		10 : 'Diamond Director',
		11 : 'Executive Director',
		16 : "Ambassadors' Club"
};