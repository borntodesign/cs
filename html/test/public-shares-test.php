<?php
include '/home/clubshop.com/php-includes/Clubshop/DB_Connect.php';
$args = array('enable_lockout'=>1);
try {
    $db = new Clubshop\DB_Connect('test', null, $args);
} catch (Exception $e) {
    header('text/plain');
    echo $e->getMessage();
    exit();
}

if (! $db->db())
{
    header('text/plain');
    echo 'No DB connection' . PHP_EOL;
    var_dump( \PDO::getAvailableDrivers());
    exit();
}

$country = isset($_GET['country']) ? $_GET['country'] : 'US';
if (! preg_match('/^[A-Z]{2}$/', $country))
    $country = 'US';

$results = $db->fetchList_assoc("
    SELECT
        spl.pay_level
        ,lvl.lname
        ,pbc.avg_pp
        ,spl.shares
        ,spl.stars_shares
        ,(spl.shares * pbc.avg_pp)::NUMERIC(7,2) AS my25
        ,(spl.stars_shares * pbc.avg_pp)::NUMERIC(7,2) AS exec_bonus
    FROM network.avg_pp_by_country pbc, network.shares_by_pay_level spl, level_values lvl
    WHERE lvl.lvalue=spl.pay_level
    AND pbc.country= ?
    ORDER BY spl.pay_level", array($country));

$countries = $db->fetchList_assoc("
    SELECT tc.country_code, tc.country_name
    FROM tbl_countrycodes tc
    JOIN network.avg_pp_by_country pbc ON pbc.country=tc.country_code
    ORDER BY tc.country_name");
error_log(print_r($countries,1));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>public shares test</title>
<style type="text/css">
p {display:inline-block; border:1px solid red; margin:0 0.2em;}
div{padding:1em;border:1px solid blue;}
td,th {
	border: 1px solid silver;
	padding: 0.5em;
}
.alr {
	text-align:right;
}
.even {
	background-color: #f3f3fe;
}
.odd {
	background-color: #fafaf3;
}
.xs {
	color: grey;
}
</style>
</head>
<body>
<form method="get" action="<?php echo htmlentities($_SERVER['PHP_SELF'])?>">
<select name="country" onchange="document.forms[0].submit()">
<?php $position=0; foreach ($countries AS $row):?>
<option value="<?php echo $row['country_code']?>"<?php if ($country == $row['country_code']) echo ' selected="selected"'?>><?php echo $row['country_name']?></option>
<?php endforeach;?>
</select>
</form>
<table style="margin-top: 1em; border-collapse: collapse;">
<tr>
    <th>Pay Level</th>
    <th>Avg PP</th>
    <th>Shares</th>
    <th>Team Comm.</th>
    <th>Star<br />Shares/Bonus</th>
    <th>Total</th>
</tr>
<?php $position=0; foreach ($results AS $row):?>
<tr class="<?php echo ($position & 1) ? 'odd' : 'even'?>">
    <td><?php echo $row['lname']?></td>
    <td><?php echo $row['avg_pp']?></td>
    <td><?php echo $row['shares']?></td>
    <td class="alr"><?php echo $row['my25']?></td>
    <td class="alr"><span class="xs"><?php echo $row['stars_shares']?></span> / <?php echo $row['exec_bonus']?></td>
    <td class="alr"><?php printf('%.2f', $row['my25'] + $row['exec_bonus'])?></td>
</tr>
<?php $position++; endforeach;?>
</table>
</body>
</html>