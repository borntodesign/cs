﻿<!DOCTYPE HTML >
<?php
include '/home/clubshop.com/php-includes/Clubshop/DB_Connect.php';
$args = array('enable_lockout'=>1);
try {
    $db = new Clubshop\DB_Connect('test', null, $args);
} catch (Exception $e) {
    header('text/plain');
    echo $e->getMessage();
    exit();
}

if (! $db->db())
{
    header('text/plain');
    echo 'No DB connection' . PHP_EOL;
    var_dump( \PDO::getAvailableDrivers());
    exit();
}
?>
<html>

<head>
<script type="text/javascript" src="cplp.js"></script>
<title>ClubShop Rewards Partner Compensation Plan</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<script src="/js/partner/flash.js"></script>
<script src="/js/partner/foundation.js"></script>
<script src="/js/partner/app.js"></script>

<script type="text/javascript" src="/js/tipMap.js"></script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script>


<style type="text/css">
.larger_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 12px;
	text-transform: uppercase;
	color: #135FB3;
	font-weight: bold;
}
td.center {text-align:center;
vertical-align: middle;}

td.quals {background-color: #F7F9CC;}
td.qualsend {background-color: #F7F9CC;
border-bottom-width: thin;
	border-top-style: none;
	border-right-style: solid;
	border-right-color: #CCCCCC;
	border-left-style: none;
	border-bottom-style: none;}


td.comm {background-color: #B9E3FF;}
td.commend {background-color: #B9E3FF;
border-bottom-width: thin;
	border-top-style: none;
	border-right-style: none;
	border-left-style: solid;
	border-left-color: #CCCCCC;
	border-bottom-style: none;}


.small {font-size: 10px;}

td {
	background-color: #FFFFFF;
	}
	
tr {border-bottom-width: thin;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-bottom-color: #CCCCCC;}
.blue_bolderest {
color: #005BE9;
	
}

.special{
font-family: Arial, Verdana, sans-serif;
font-size: 18px;
color: #000000;}

.special2{
	font-family: Arial, Verdana, sans-serif;
	font-size: 12px;
 	color: #2D57B0;
	font-weight: bold;
}
.special4{
	font-family: Arial, Verdana, sans-serif;
	font-size: 14px;
 	color: #2D57B0;
	font-weight: bold;
}

.special5{
	font-family: Arial, Verdana, sans-serif;
	font-size: 12px;
 	color: #333333;
	font-weight: bold;
}




.special3{font-family: Arial, Verdana, serif;
font-size: 14px;

}


.specialtitles{font-family: Arial, Verdana, sans-serif;
font-size: 15px;
}
.specialtitles2{font-family: Arial, Verdana, sans-serif;
size: 13px;
}
.tp{color: #666666;}
.tl{color: #F40C29;}
.tc{color: #00008b;}
.bm{color: #A8463D;}
.sm{color: #757575;}
.gm{color: #F4C01B;}
.rd{color: #A8081C;}
.ed{color: #257518;}
.dd{color: #660099;}
.star{color:#bdb150;}
.stars{color:#A4993E;}

.bl {color: navy;}
.grey {color: gray;}
.navy {color: #0066CC;}

.tip {
    background: none repeat scroll 0 0 #00376F;
    border-radius: 3px 3px 3px 3px;
    color: #FFFFFF;
    display: none;
    padding: 5px;
    width: 25%;
    text-align:left;
    position: absolute;
    z-index: 200;
}

a.tip_trigger {
	text-decoration: underline;
}
span.tip
{
	font-family: arial,helvetica,sans-serif;
	font-size: x-small;
	font-weight: normal;
}







</style>
<script type="text/javascript">

	$(document).ready(function() {

	var showText='MORE';

	var hideText='LESS';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

</script>

</head>
<body>

	<div class="container blue">

	<div class="row">

		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288" 

height="84" style="margin-top:5px;" /></a></div>

		<div class="six columns"></div>

	</div></div>
	<div class="container">

			 <div class="row">
		<div class="twelve columns"> 
				
            
      <div align="center"><h4 class="topTitle">TEAM COMMISSIONS</h4></div><br/>
	      <table width="100%">
            <tr> 
              <td rowspan="2" class="center"><span class="special5">PAY LEVEL</span></td>
              <td rowspan="2" class="center quals"><a href="#" class="tip_trigger">PRP<br/>PP<span 

class="tip special5" style="display: none;"> PRP/PP - This is the number of current Personally Referred 

Partners and the number of Personally Referred Pay Points you have. These Pay Points include your personal 

GPS Pay Points and the "Power Points" generated from your personal purchases and from the purchases made 

by your personally referred memberships and from the GPS Pay Points of your PRPs. </span></a></td>
              <td rowspan="2" class="center quals"><a href="#" class="tip_trigger">GRP<br/>PP<span 

class="tip special5" style="display: none;">GRP/PP - This is the number of current Partners and the number 

of Pay Points you have in your personal Group. These Pay Points include your PRP/PP and the "Power Points" 

generated from the purchases made by the memberships and from the GPS Pay Points generated in your 

personal Group.</span></a></td>
              <td rowspan="2" class="center quals"><a href="#" class="tip_trigger"> SV<br>GRP/PP<span 

class="tip special5" style="display: none;">Directors and Executive Directors are required to maintain 

"Side Volume" GRP/PP, based on their GRP/PP from other than their Partner Group with the highest number of 

GRP.
</span></a></td>
              <td colspan="11" class="center commend"><a href="#" class="tip_trigger">TEAM PAY POINTS 

(TPP)<span class="tip special5" style="display: none;">This is the total number of GPS Pay Points and 

Power Points generated by yourself and your team.</span></a></td>

              </tr>

            <tr>
              <td class="center commend"><span class="specialtitles"><b>100</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>200</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>400</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>700</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>1,100</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>1,600</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>2,300</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>2,900</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>3,500</b></span></td>
              <td class="center commend"><span class="specialtitles"><b>4,200</b></span></td>
              <td class="center commend"><span class="specialtitles">each add'l 1000</span></td>
            </tr>

<?php

function xShareValue($val)
{
    if (! $val) {
        return '-';
    }
    else {
        return '$' . sprintf('%01.2f', ($val * 0.1));
    }
}

function dashNotNull($val)
{
    if (! $val) return '-';
    return $val;
}

function fillRow($row)
{
    echo '<td class="center quals"><span class="specialtitles">';
    echo dashNotNull($row['prp']) . '<br/>' . dashNotNull($row['prp_pp']) . '</span></td>';
    
    echo '<td class="center quals"><span class="specialtitles">';
    echo dashNotNull($row['grp']) . '<br/>' . dashNotNull($row['grp_pp']) . '</span></td>';
    
    echo '<td class="center quals">';
    if (! $row['sv_grp']) {
        echo '-';
    }
    else {
        echo '<span class="specialtitles">' . $row['sv_grp'] . '<br />' . $row['sv_grp_pp'] . '</span>';
    }
    echo '</td>';
    
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col1'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col2'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col3'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col4'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col5'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col6'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col7'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col8'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col9'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['col10'])  . '</span></td>';
    echo '<td class="center commend"><span class="transposeME specialtitles">' . xShareValue($row['addl_shares'])  . '</span></td>';
}

if ($db->db()) {
$qry = <<<'QRY'
WITH xt AS (SELECT * FROM CROSSTAB(
'WITH tppl AS (SELECT DISTINCT tcs.tpp, tcs2.pay_level FROM test.tc_share_tiers tcs, test.tc_share_tiers tcs2 ORDER BY 2,1)
SELECT tppl.pay_level,
     tppl.tpp, tcs.shares
FROM tppl
LEFT JOIN test.tc_share_tiers tcs ON tppl.tpp=tcs.tpp AND tppl.pay_level=tcs.pay_level
ORDER BY tppl.pay_level,2'
)
AS (pay_level smallint, "100" smallint, "200" smallint, "400" smallint, "700" smallint, "1100" smallint, "1600" smallint, "2300" smallint, "2900" smallint,
         "3500" smallint, "4200" smallint)
)
SELECT lvl.lname,
     tnt.pay_level,
     tnt.prp,
     tnt.prp_pp,
     tnt.grp,
     tnt.grp_pp,
     tnt.sv_grp,
     tnt.sv_grp_pp,
     xt."100" AS col1, xt."200" AS col2, xt."400" AS col3, xt."700" AS col4, xt."1100" AS col5, xt."1600" AS col6, xt."2300" AS col7,
	    xt."2900" AS col8, xt."3500" AS col9, xt."4200" AS col10,
     tx.addl_shares,
     tx.addl_tpp
FROM xt
JOIN level_values lvl ON lvl.lvalue=xt.pay_level
JOIN test.level_values_tnt tnt ON tnt.pay_level=xt.pay_level
JOIN test.tc_share_tier_extensions tx ON tx.pay_level=xt.pay_level
QRY;

$mysth = $db->prepareXSth($qry);

}
else {
    echo 'DB connection problem';
    exit();
}
?>
<!-- ##### START OF MATRIX -->
            <tr>
              <td valign="middle"><span class="larger_blue_bold navy"><script type="text/javascript">document.write(plabels[1])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
            </tr>
            
            <tr> 
              <td valign="middle"><img src="/images/pins/teamplayer.png" width="20" height="20"> 
                <br> <span class="larger_blue_bold tp"><script type="text/javascript">document.write(plabels[2])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/teamleader.gif" width="20" height="20"> 
                <br> <span class="larger_blue_bold tl"><script type="text/javascript">document.write(plabels[3])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/teamcaptain.gif" width="20" height="20" /><br> 
                <span class="larger_blue_bold tc"><script type="text/javascript">document.write(plabels[4])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/bronzemanager.jpg" width="20" height="20" /><br> 
                <span class="larger_blue_bold bm"><script type="text/javascript">document.write(plabels[5])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
            <tr> 
              <td valign="middle"><img src="/images/pins/silvermanager.png" width="21" height="21" /><br> 
                <span class="larger_blue_bold sm"><script type="text/javascript">document.write(plabels[6])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/goldmanager.jpg" width="21" height="21" /><br> 
                <span class="larger_blue_bold gm"><script type="text/javascript">document.write(plabels[7])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/rubydirector.png" width="21" height="21" /><br> 
                <span class="larger_blue_bold rd"><script type="text/javascript">document.write(plabels[8])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/emeralddir_pin.jpg" width="22" height="22" /><br> 
                <span class="larger_blue_bold ed"><script type="text/javascript">document.write(plabels[9])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/diamond.png" width="21" height="21" /><br> 
                <span class="larger_blue_bold dd"><script type="text/javascript">document.write(plabels[10])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>
              
            <tr> 
              <td valign="middle"><img src="/images/pins/exec00.gif" width="21" height="21" /><br> 
                <span class="larger_blue_bold star"><script type="text/javascript">document.write(plabels[11])</script></span></td>
<?php
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    fillRow($rz);
?>
              </tr>

<!-- ##### END OF MATRIX -->

	      </table>
            </div>
	 </div>

	  
	  <div class="row">
		<div class="twelve columns">
	  		<p><span class="style28">EXECUTIVE DIRECTOR BONUSES</span></p>
				<p>Executive Director Bonuses are paid in addition to Executive Director Team Commissions. As an Executive Director, once you have one or more Executive Directors in your Group, you may be able to qualify for Executive Director Bonuses. Each Executive Director in your Group can count as a "Star" credit to help you qualify as a 1 Star, 2 Star, etc. Executive Director! You can see the number of Executive Directors that you may have in your Group and who they are, by viewing your Group Report.</p>

			
			<p>See the table below for the various Executive Director Star Pay Level qualifications required. </p>

<?php
function starsRow($row)
{
    echo <<<ROW
              <tr><td class="center"><span class="center larger_blue_bold stars">{$row['execs']}
              <script type="text/javascript">document.write(plabels[{$row['pay_level']}] || '')</script>
              </span></td>
              <td class="center quals"><span class="specialtitles">{$row['prp']}</span></td>
              <td class="center quals"><span class="specialtitles">{$row['grp']}</span></td>
              <td class="center qualsend"><span class="specialtitles">{$row['sv_grp']}</span></td>
              <td class="center qualsend"><span class="specialtitles">{$row['tmpp']}</span></td>
              <td class="center comm"><span class="transposeME specialtitles">
ROW;
    echo xShareValue($row['xdir_bonus_shares']) . '</span></td></tr>';
}

$mysth = $db->prepareXSth('
    SELECT pay_level, prp, grp, tmpp, execs, xdir_bonus_shares, sv_grp
    FROM test.level_values_tnt
    WHERE pay_level BETWEEN 12 AND 18');
?>
            
            <div class="row">
		<div class="twelve columns"> 
            <table width="100%">
            <tr> 
              <td class="center"><span class="special5">STARS</span></td>
              <td class="center quals"><span class="special5">PRP</span></td>
              <td class="center quals"><span class="special5">GRP</span></td>
              <td class="center qualsend"><span class="special5">SV GRP</span></td>
              <td class="center qualsend"><span class="special5">TPP</span></td>
              <td class="center comm"><span class="special5">BONUS</span></td>
              </tr>

<!-- ##### START OF STARSMATRIX -->
              
<?php
for ($x=1; $x < 6; $x++)
{
    $rz = $mysth->fetch(\PDO::FETCH_ASSOC);
    starsRow($rz);
}
?>

<!-- ##### END OF STARSMATRIX -->

                <tr> 
                  <td class="center"><span class="larger_blue_bold stars">6+</span></td>    
				  <td class="center quals"><span class="specialtitles">1 more per Star</span></td>
                  <td class="center quals"><span class="specialtitles">100 More per Star</span></td>
                  <td class="center qualsend"><span class="specialtitles">100</span></td>
                  <td class="center qualsend"><span class="specialtitles">1,000 More per Star</span></td>
                  <td class="center comm"><span class="transposeME specialtitles">$500 More per star</span></td>
                </tr>
          </table>

	 
	 
	 </div>
	 </div>
            
            
			
	</div>
	</div>
	
            <br/><br/>

</div>

<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">

        <p>Copyright © 1997-2014
          <!-- Get Current Year -->
          ClubShop Rewards, All Rights Reserved. </p>

    </div>
</div>
</div>
</div>


<script type="text/javascript">

    $(document).ready(function() {

        Transpose.transpose_currency();

    });

</script> 



</body>

</html>


