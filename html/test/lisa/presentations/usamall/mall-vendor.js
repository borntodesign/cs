/*
mall-vendor.js
functions related to displaying various details about the mall stores
ie. store info, blurbs
*/

// some globals
var StoreInfoInitStateLeft = '-2000px';	// where we will move 'hidden' boxes
//var StoreInfoInitStateWidth = '1em';
var StoreInfoRevealRight = '10px';		// how far off the right margin we will place the store info
var storeList;						// will hold a reference to the store list HTML container in the page
var StoreInfoRevealedStateTop = 10;	// how far off the top of the visible list will we set the box
var urlStoreInfo = '/cgi/tmp/store-info-frag.cgi?idn=';

function createStoreInfoBox(idnStr){
	var outerDiv = document.createElement('div');
	outerDiv.id = 'storeInfoBox' + idnStr;
	outerDiv.className = 'storeInfoBox';

	var infoCloser = document.createElement('a');
	infoCloser.href = '#';
	infoCloser.className = 'storeInfoClose';
	infoCloser.onclick = hideStoreInfoBox;
	infoCloser.appendChild( document.createTextNode('X') );
	outerDiv.appendChild( infoCloser );
	
	var innerDiv = document.createElement('div');
	innerDiv.id = 'storeInfoContent' + idnStr;
	innerDiv.className = 'storeInfoContent';

	outerDiv.appendChild( innerDiv );
	return [outerDiv, innerDiv];
}

function displayStoreInfo(idn){
	var idnStr = idn2String(idn);
	var otherBoxes = storeList.getElementsByTagName('div');
	var Xists;
	for (var x=0; x < otherBoxes.length; x++){
		if (otherBoxes[x].id.match(idnStr) != null){
			revealStoreInfoBox(otherBoxes[x]);
			Xists = true;
		}
		else if (otherBoxes[x].className == 'storeInfoBox'){
			hideStoreInfoBox(otherBoxes[x]);
		}
	}
	if (Xists){ return true; }
	var newBox = createStoreInfoBox(idnStr);
	storeList.appendChild( newBox[0] );
//	newBox[1].innerHTML = '<table border="1"><tr><td>stuff</td></tr><tr><td>more stuff</td></tr></table>';
//	revealStoreInfoBox( newBox[0] );
	var callbackStoreInfo = 
	{
		success: populateStoreInfoBox,
		failure: GeneralResponseFailure,
		argument: newBox
	}
	var rv = YAHOO.util.Connect.asyncRequest("GET", urlStoreInfo + idn, callbackStoreInfo);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
/*		var newspan = document.createElement('span');
//		newspan.className = 'please_wait';
	//	var newtxt = document.createTextNode('Please wait');
		newspan.appendChild( document.createTextNode('Please wait') );
		ACTIVE_SPAN = newspan;
		frm.appendChild(newspan);
		ACTIVE_FRM = frm;*/
	}
}

function GeneralResponseFailure(o) {
	alert("There has an unexpected error:\n" + o.responseText);
}

function getScrollingPosition() {
// returns just the scrolling coordinates

  var position = [0, 0];

  if (typeof window.pageYOffset != 'undefined')
  {
    position = [
        window.pageXOffset,
        window.pageYOffset
    ];
  }

  else if (typeof document.documentElement.scrollTop != 'undefined'
      && (document.documentElement.scrollTop > 0 ||
      document.documentElement.scrollLeft > 0))
  {
    position = [
        document.documentElement.scrollLeft,
        document.documentElement.scrollTop
    ];
  }

  else if (typeof document.body.scrollTop != 'undefined')
  {
    position = [
        document.body.scrollLeft,
        document.body.scrollTop
    ];
  }

  return position;
}

function hideStoreInfoBox(e){
//alert('hiding');
	e = window.event ? window.event.srcElement : e ? e.target : null;
	e.parentNode.style.left = StoreInfoInitStateLeft;
	e.parentNode.style.right = 'auto';
//	e.parentNode.style.width = StoreInfoInitStateWidth;
}
function idn2String(idn){ return idn.replace(/,/g, ''); }

function mall_vendor_init(){
// init procedures specifically related to the needs in these routines
// it is expected that this routine will be called with a more global init routine
// after the page is fully loaded and all the HTML elements are available
	storeList = document.getElementById('storeList');
}

function populateStoreInfoBox(o){
	if (o.responseXML == null){
		alert("Unanticipated error:\n" + o.responseText);
	} else {
//		var mybox = document.getElementById('storeInfoBox'+ o.argument[0]);
		o.argument[1].innerHTML = o.responseText;
		revealStoreInfoBox(o.argument[0]);
	}
}

function revealStoreInfoBox(box){
	var position = getScrollingPosition();
//	alert('x='+position[0]+' y='+position[1]);
	box.style.top = StoreInfoRevealedStateTop + position[1] + 'px';
	box.style.right = StoreInfoRevealRight;
//	box.style.width = 'auto';
	box.style.left = 'auto';
}