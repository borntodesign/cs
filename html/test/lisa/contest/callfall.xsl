<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
            <title>Call This Fall Sales Contest</title>

<style type="text/css">

body { background-image: url("/vip/contests/fallcall.gif"); 
}
div#main {margin: 0 0 0 10em;}

h1 {font-family: Verdana, Arial, sans-serif,Helvetica;
color: #FFCC00;

}

p {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

ol.rules,ol.prizes {font-family: Verdana, Arial, sans-serif,Helvetica;
font-size: smaller;}



</style>

</head>
<body>
<div id="main">


<h1><xsl:value-of select="//lang_blocks/p0"/><br/>
<div align="center"><img src="makecall.gif" height="154" width="139" alt="make the call"/></div></h1>

<h4><xsl:value-of select="//lang_blocks/p1"/></h4>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><b><xsl:value-of select="//lang_blocks/p3"/></b></p>

<ol class="rules">
<li><xsl:value-of select="//lang_blocks/rule1"/></li>
<li><xsl:value-of select="//lang_blocks/rule2"/></li>
<li><xsl:value-of select="//lang_blocks/rule3"/></li>
<li><xsl:value-of select="//lang_blocks/rule4"/></li>
</ol>

<p><b><xsl:value-of select="//lang_blocks/p4"/></b></p>

<ol class="prizes">
<li><xsl:value-of select="//lang_blocks/prize1"/></li>
<li><xsl:value-of select="//lang_blocks/prize2"/></li>
<li><xsl:value-of select="//lang_blocks/prize3"/></li>
</ol>



<div class="list_builders">
<!--Builders-->
<h5><xsl:value-of select="//lang_blocks/p5"/></h5>
</div>

<!--endBuilders-->





<div class="list_trainers">
<!--Trainers-->
<h5><xsl:value-of select="//lang_blocks/p6"/></h5>
</div>
<!--end Trainers-->





<div class="list_chiefs">
<!--Chiefs-->
<h5><xsl:value-of select="//lang_blocks/p7"/></h5>
<!--end Chiefs-->
</div>


</div>

</body></html>


</xsl:template>
</xsl:stylesheet>