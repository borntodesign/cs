<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Currency List</title>
            <script type="text/javascript" src="/js/gi/blurb.js"></script>
<script type="text/javascript" src="/js/gi/banner.js"></script>

<link rel="stylesheet" type="text/css" href="/css/gi_container.css" />
<link rel="stylesheet" type="text/css" href="/css/gi_pages.css" />

<style type="text/css">
table.a {border: thin solid #999966;
}
table.b {text-align: center;}

td {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: .65em;
border: 1px solid #999966;
}
tr.top{
	background-color: #FFFFCC;

}
tr.a {
	background-color: #FFFFFF;
}
tr.b {
	background-color: #F7F7F2;
}
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

</style>

</head>
<body>



<div id="outer_shell">
<div id="scrolling_banner">
<span class="header">
<script type="text/javascript">
<xsl:comment>
if (document.all || document.getElementById){
	document.write( _a() );
	for (var m=0;m &lt;  message.length;m++){
		document.write( _b() );
		document.write( _c() );
	}
}
else {
	document.write(message);
}
begincolor();
</xsl:comment>
</script>
</span>
</div>

<div id="gi_header"><img src="/images/gi.jpg" width="640" height="70" alt="" /></div>

<div id="inner_shell">
<div align="right"><a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle"/>PRINT PAGE</a> <xsl:text> </xsl:text> <xsl:text> </xsl:text><a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></div>














<table>
<div align="center"><h4><xsl:value-of select="//lang_blocks/p0"/></h4></div>
<p><b><xsl:value-of select="//lang_blocks/p1"/></b></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<tr class="b"><td colspan="2"></td>
<td colspan="3"><div align="center"><xsl:value-of select="//lang_blocks/subs_fee"/></div></td>
<td colspan="2" align="center"><xsl:value-of select="//lang_blocks/point_values"/></td>
</tr>


  <tr class="top"> 
    <td><xsl:value-of select="//lang_blocks/country"/></td>
    <td><xsl:value-of select="//lang_blocks/ccode"/></td>
    <td><xsl:value-of select="//lang_blocks/fee1"/></td>
    <td><xsl:value-of select="//lang_blocks/fee2"/></td>
    <td><xsl:value-of select="//lang_blocks/fee3"/></td>
<td><xsl:value-of select="//lang_blocks/fee1"/></td>
<td><xsl:value-of select="//lang_blocks/fee2"/></td>
  </tr>


<xsl:for-each select="document('pricing.xml')/pricing/item">
<tr>
<xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">a</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="currency_code" /> -
<xsl:value-of select="currency_name" /></td>
<td><xsl:value-of select="initial_fee" /></td>
<td><xsl:value-of select="monthly_fee" /></td>
<td><xsl:value-of select="annual_renewal_fee" /></td>
<td><xsl:value-of select="initial_pp" /></td>
<td><xsl:value-of select="monthly_pp" /></td>
</tr>
</xsl:for-each>
</table>

</div></div>
</body></html>


</xsl:template>

<xsl:template match="/pricing/item">
<tr><xsl:copy-of select="*" /></tr>
</xsl:template>
</xsl:stylesheet>
