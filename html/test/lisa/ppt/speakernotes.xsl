<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>VIP Signup</title>
<link href="presentation.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
h3{font-family: Arial, Verdana, sans-serif;
size: +1;
}
p.a{font-family: Verdana, Arial, sans-serif;
font-size: 14px;
}
p.subtitle {font-family: Verdana, Arial, sans-serif;
font-size: 14px;
font-weight: bold;

}
li{font-family: Verdana, Arial, sans-serif;
font-size: 14px;
}
p.point{font-family: Verdana, Arial, sans-serif;
font-size: 14px;
font-weight: bold;
text-indent: 50px;}

td.list{border-bottom: 2px black solid;}

</style>

</head>
<body>
<div align="center">
<h3><xsl:value-of select="//lang_blocks/heading"/></h3></div>
<table>
<tr>
<td>

<table><tr><td class="list">

<p class="subtitle"><xsl:value-of select="//lang_blocks/slide1"/></p>

<ul><li><xsl:value-of select="//lang_blocks/s1"/></li></ul></td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide2"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s2"/></li>
<li><xsl:value-of select="//lang_blocks/s2a"/></li>
<li><xsl:value-of select="//lang_blocks/s2b"/></li>
</ul>

<p class="point"><xsl:value-of select="//lang_blocks/s2h"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s2c"/></li>
<li><xsl:value-of select="//lang_blocks/s2d"/></li>
<li><xsl:value-of select="//lang_blocks/s2e"/></li>
</ul>

<p class="point"><xsl:value-of select="//lang_blocks/s2hb"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s2f"/></li>
<li><xsl:value-of select="//lang_blocks/s2g"/></li>
<li><xsl:value-of select="//lang_blocks/s2i"/></li>
<li><xsl:value-of select="//lang_blocks/s2j"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide3"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s3"/></li></ul>

</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide4"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s4"/></li></ul>

</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide5"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s5"/></li></ul>

</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide6"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s6"/></li>
<li><xsl:value-of select="//lang_blocks/s6a"/></li>
</ul>

</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide7"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s7"/></li>
</ul>

</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide8"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/s8"/></li>
<li><xsl:value-of select="//lang_blocks/s8a"/></li>
<li><xsl:value-of select="//lang_blocks/s8b"/></li>
<li><xsl:value-of select="//lang_blocks/s8c"/></li>
</ul>

</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide9"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s9"/></li>
<li><xsl:value-of select="//lang_blocks/s9a"/></li>
<li><xsl:value-of select="//lang_blocks/s9b"/></li>
<li><xsl:value-of select="//lang_blocks/s9c"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide9"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s9"/></li>
<li><xsl:value-of select="//lang_blocks/s9a"/></li>
<li><xsl:value-of select="//lang_blocks/s9b"/></li>
<li><xsl:value-of select="//lang_blocks/s9c"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide10"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s10"/></li>
<li><xsl:value-of select="//lang_blocks/s10a"/></li>
<li><xsl:value-of select="//lang_blocks/s10b"/></li>
<li><xsl:value-of select="//lang_blocks/s10c"/></li>
</ul>
</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide11"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s11"/></li>
<li><xsl:value-of select="//lang_blocks/s11a"/></li>
<li><xsl:value-of select="//lang_blocks/s11b"/></li>
<li><xsl:value-of select="//lang_blocks/s11c"/></li>
<li><xsl:value-of select="//lang_blocks/s11d"/></li>
</ul>
</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide12"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s12"/></li>
<li><xsl:value-of select="//lang_blocks/s12a"/></li>
</ul>
</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide13"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s13"/></li>
<li><xsl:value-of select="//lang_blocks/s13a"/></li>
</ul>
</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide14"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s14"/></li>
<li><xsl:value-of select="//lang_blocks/s14a"/></li>
<li><xsl:value-of select="//lang_blocks/s14b"/></li>
<li><xsl:value-of select="//lang_blocks/s14c"/></li>
<li><xsl:value-of select="//lang_blocks/s14d"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide15"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s15"/></li>
<li><xsl:value-of select="//lang_blocks/s15a"/></li>
</ul>
</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide16"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s16"/></li>
<li><xsl:value-of select="//lang_blocks/s16a"/></li>
<li><xsl:value-of select="//lang_blocks/s16b"/></li>
<li><xsl:value-of select="//lang_blocks/s16c"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide17"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s17"/></li>
<li><xsl:value-of select="//lang_blocks/s17a"/></li>
</ul>
</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide18"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s18"/></li>
<li><xsl:value-of select="//lang_blocks/s18a"/></li>
</ul>
</td></tr>

<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide19"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s19"/></li>
<li><xsl:value-of select="//lang_blocks/s19a"/></li>
<li><xsl:value-of select="//lang_blocks/s19b"/></li>
</ul>
</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide20"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s20"/></li>
</ul>
</td></tr>



<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide21"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s21"/></li>
</ul>
</td></tr>


<tr><td class="list">
<p class="subtitle"><xsl:value-of select="//lang_blocks/slide22"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/s22"/></li>
<li><xsl:value-of select="//lang_blocks/s22a"/></li>
<li><xsl:value-of select="//lang_blocks/s22b"/></li>
<li><xsl:value-of select="//lang_blocks/s22c"/></li>
<li><xsl:value-of select="//lang_blocks/s22d"/></li>
</ul>
</td></tr>





</table>








</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>