<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_a.css"/>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_side.css"/>

<title>Paris, France Conference May 2, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created Nov 2007 LY-->


</head>

<body>
<img src="paris.gif" width="768" height="99" alt="2008 Conference"/> 
<table>
  <tr> 
   <td id="sidelinks"> 
     <h3><font color="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p0"/></b></font></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="lodging.xml"><xsl:value-of select="//lang_blocks/lodging"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>    
     
   </td>
	  
	  
    <td width="613" id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
        <br/>
        <h3><xsl:value-of select="//lang_blocks/p1"/></h3>
            <ul>
            <p><xsl:value-of select="//lang_blocks/p2"/></p>
            <li><xsl:value-of select="//lang_blocks/p3"/></li>
            <li><xsl:value-of select="//lang_blocks/p4"/></li>
            <li><xsl:value-of select="//lang_blocks/p5"/></li>
            <li><xsl:value-of select="//lang_blocks/p6"/></li>
            <li><xsl:value-of select="//lang_blocks/p7"/></li>
            <li><xsl:value-of select="//lang_blocks/p8"/></li>
            <li><xsl:value-of select="//lang_blocks/p9"/></li>
            <li><xsl:value-of select="//lang_blocks/p10"/></li>
            <li><xsl:value-of select="//lang_blocks/p11"/></li>
            <li><xsl:value-of select="//lang_blocks/p12"/></li>
            <li><xsl:value-of select="//lang_blocks/p13"/></li>
            <li><xsl:value-of select="//lang_blocks/p14"/></li>
         <li><xsl:value-of select="//lang_blocks/p17"/></li>
            <li><xsl:value-of select="//lang_blocks/p18"/></li>
            <li><xsl:value-of select="//lang_blocks/p19"/></li>
            </ul>
            <h3><xsl:value-of select="//lang_blocks/p15"/></h3>
            <ul><p><xsl:value-of select="//lang_blocks/p16"/></p>
            <li><xsl:value-of select="//lang_blocks/p20"/></li>
            <li><xsl:value-of select="//lang_blocks/p21"/></li>
            <li><xsl:value-of select="//lang_blocks/p22"/></li>
            <li><xsl:value-of select="//lang_blocks/p23"/></li>
            <li><xsl:value-of select="//lang_blocks/p24"/></li>
            <li><xsl:value-of select="//lang_blocks/p25"/></li>
            <li><xsl:value-of select="//lang_blocks/p26"/></li>
            <li><xsl:value-of select="//lang_blocks/p27"/></li>
            <li><xsl:value-of select="//lang_blocks/p28"/></li>
            <li><xsl:value-of select="//lang_blocks/p29"/></li>
            <li><xsl:value-of select="//lang_blocks/p30"/></li>
            <li><xsl:value-of select="//lang_blocks/p31"/></li>
            <li><xsl:value-of select="//lang_blocks/p32"/></li>
            <li><xsl:value-of select="//lang_blocks/p33"/></li>
            </ul>
            
            
            <br/>
            <div align="center"><img src="meetingroom.gif" height="145" width="224" alt="meeting room"/></div>
            
            
                
            
            
            
            
            </td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>



