<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Standing Order</title>
            
<link href="http://12.32.98.246/localmarketing/styles/css/pages.css" rel="stylesheet" type="text/css"/>
       
            
            
<!--<style type="text/css">
p{font-family: Verdana, Arial, sans-serif;
font-size: smaller;
}

li {font-family: Verdana, Arial, sans-serif;
font-size: smaller;
padding-bottom: 4px;
}

h5{font-family: Verdana, Arial, sans-serif;
font-size: medium;
}
</style>-->


</head>
<body>
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
  <tr>
    <td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
      
      <tr>
        <td align="center" bgcolor="#000000"><span class="header">     
<script language="JavaScript1.2">
<xsl:comment>
var message="<xsl:value-of select="//lang_blocks/p15"/>"
var colorone="#CCCCCC"
var colortwo="#CC6633"
var flashspeed=100  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('&lt;font color="'+colorone+'"&gt;')
for (m=0;m &lt;message.length;m++)
document.write('&lt;span id="changecolor'+m+'"&gt;'+message.charAt(m)+'&lt;/span&gt;')
document.write('&lt;/font&gt;')
}
else
document.write(message)

function crossref(number){
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number)
return crossobj
}

function color(){

//Change all letters to base color
if (n==0){
for (m=0;m &lt; message.length;m++)

//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo

if (n&lt;message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",12000)
return
}
}

function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()
      //</xsl:comment></script></span></td></tr>
      
      
      
<tr>
<td bgcolor="#245923"><img src="http://12.32.98.246/localmarketing/images/main/movie_header.jpg" width="640" height="70" /></td>
</tr>
      
<tr>
<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

<tr>
<td align="right"><a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="http://12.32.98.246/localmarketing/images/icons/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle"/><xsl:value-of select="//lang_blocks/p16"/></a> <xsl:text> </xsl:text> <xsl:text> </xsl:text><a href="javascript:window.close();"><img src="http://12.32.98.246/localmarketing/images/icons/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" class="nav_footer_brown"></a><span class="Header_Main"> </span>
<hr align="left" width="100%" size="1" color="#A35912"/></td> </tr>

<tr><td valign="top">
<p><font color="#996633"><b><xsl:value-of select="//lang_blocks/p0"/></b></font></p>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/> </span>
 
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<p><b><xsl:value-of select="//lang_blocks/p6"/></b></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
<li><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="/cgi/vip/coop_replace.cgi" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a>. </li>
<li><xsl:value-of select="//lang_blocks/p10"/></li>
<li><xsl:value-of select="//lang_blocks/p11"/></li>

<li><xsl:value-of select="//lang_blocks/p13"/></li>
</ol>

<p align="center"><a href="/cgi/vip/coop_standing_order.cgi"><xsl:value-of select="//lang_blocks/p14"/></a></p>

</td></tr>

<tr>
<td align="right">
<hr align="left" width="100%" size="1" color="#A35912"/>
<a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="http://12.32.98.246/localmarketing/images/icons/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle"/><xsl:value-of select="//lang_blocks/p16"/></a> <xsl:text> </xsl:text> <xsl:text> </xsl:text><a href="javascript:window.close();"><img src="http://12.32.98.246/localmarketing/images/icons/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absbottom"/></a><a href="javascript:window.close();" class="nav_footer_brown"></a></td> 
</tr>




</table>
        
        
        </td>
      </tr>
    </table>
    
    
    
    
    

</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>