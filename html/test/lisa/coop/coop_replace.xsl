<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
<xsl:template match="/">	

<html>
<head>
<title><xsl:value-of select="//lang_blocks/titles/main" /></title>
<link href="/css/coop.css" rel="stylesheet" type="text/css" />
<link href="http://12.32.98.246/localmarketing/styles/css/pages.css" rel="stylesheet" type="text/css"/>	

<script type="text/javascript" src="/js/check_req_x.js"></script>
</head>

<body bgcolor="#ffffff" onload="document.forms[0].id.focus()">

<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
  <tr>
    <td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
        
          <tr>
<td bgcolor="#245923"><img src="http://12.32.98.246/localmarketing/images/main/movie_header.jpg" width="640" height="70" /></td>
</tr>

<!--<span class="head4"><xsl:value-of select="//lang_blocks/titles/main" /></span>-->


      
<tr>
<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">



<tr><td valign="top">

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p0"/> </span>









<xsl:if test="/*/errors/* or /*/messages/*"><hr width="30%" />

<xsl:if test="/*/errors/*">
<xsl:for-each select="/*/errors/*">
<xsl:variable name="fieldname" select="." />
<div class="bad">
<xsl:choose><xsl:when test="//lang_blocks/messages/*[name() = $fieldname]">
<xsl:value-of select="//lang_blocks/messages/*[name() = $fieldname]" /></xsl:when>
<xsl:otherwise><xsl:value-of select="." /></xsl:otherwise></xsl:choose>
</div>
</xsl:for-each>
</xsl:if>

<xsl:if test="/*/messages/*">
</xsl:if>

<hr width="30%" /></xsl:if>

<form method="get"><xsl:attribute name="action">
<xsl:value-of select="/*/script_name" /></xsl:attribute>
<table border="0">

<tr><td>
<b><xsl:value-of select="//lang_blocks/p1" /></b>
<div align="left">
<ul>
<li><xsl:value-of select="//lang_blocks/instruction1" /></li>
<li><xsl:value-of select="//lang_blocks/instruction2" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/maxdays" /><xsl:text>.</xsl:text></li>
<li><xsl:value-of select="//lang_blocks/instruction3" /></li>
</ul>
</div>
</td></tr>

<tr><td align="center">
<xsl:value-of select="//lang_blocks/enter" />
<xsl:text> </xsl:text>
<input size="20" type="text" name="replace_id" value=""/>	
</td></tr>
</table>

<div align="center">
<input type="submit" class="button">
<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/replace_button" /></xsl:attribute>
<!-- xsl:attribute name="onClick">return CheckRequired('0','id','<xsl:value-of select="//lang_blocks/replace/string" />','<xsl:value-of select="//lang_blocks/messages/miss_field" />')</xsl:attribute -->
<xsl:attribute name="onclick">return CheckRequired('0','replace_id','<xsl:value-of select="//lang_blocks/replace/string" />','<xsl:value-of select="//lang_blocks/messages/miss_field" />')</xsl:attribute>
</input>
</div>

<input type="hidden" name="action" value="replace"/>
<input type="hidden" name="id">
<xsl:attribute name="value"><xsl:value-of select="//params/id" /></xsl:attribute>
</input>
<xsl:if test="//params/admin != ''">
	<input type="hidden" name="admin">
	<xsl:attribute name="value"><xsl:value-of select="//params/admin" /></xsl:attribute>
	</input>
</xsl:if>

</form>  


</td></tr>

<tr>
<td align="right">
<hr align="left" width="100%" size="1" color="#A35912"/>
<a href="javascript:window.close();"><img src="http://12.32.98.246/localmarketing/images/icons/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absbottom"/></a><a href="javascript:window.close();" class="nav_footer_brown"></a></td> 
</tr>




</table>
        
        
        </td>
      </tr>
    </table>
    
    
    
    
    

</td></tr></table>

</body>
</html>
</xsl:template>

</xsl:stylesheet>