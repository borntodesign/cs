<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

 <xsl:template match="/">
    <html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="//lang_blocks/p8"/></title>
<meta name="title" content="Glocal Income"/>
<meta name="Keywords" content="Glocal Income, offline marketing, e-business, training, learning, be successful, Entrupenure"/>
<meta name="abstract" content="Learn everything you need to know to create your own business."/>
<meta http-equiv="content-language" content="en-us"/>
<meta http-equiv="content-language" content="english"/>
<meta http-equiv="content-language" content="lang_en"/>
<meta name="coverage" content="Worldwide"/>
<meta name="distribution" content="Global"/>
<meta name="author" content="Glocal Income"/>
<meta name="design-development" content="Carsten Rieger"/>
<meta name="publisher" content="Glocal Income"/>
<meta name="company" content="Glocal Income"/>
<meta name="copyright" content="Copyright © 2009 Glocal Income"/>
<meta name="page-topic" content="Glocal Income"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="General"/>
<meta name="revisit-after" content="7 days"/>

<link href="http://12.32.98.246/localmarketing/styles/css/pages.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="20" cellspacing="0">
  <tr>
    <td valign="top" class="Text_Content"><span class="header">
	
	
<script type="text/javascript" language="JavaScript1.2">
<xsl:comment>
var message="Local Marketing: The foundation for building YOUR Glocal Income business"
var colorone="#CCCCCC"
var colortwo="#914304"
var flashspeed=100  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('&lt;font color="'+colorone+'"&gt;')
for (m=0;m &lt;  message.length;m++)
document.write('&lt;span id="changecolor'+m+'"&gt;'+message.charAt(m)+'&lt;/span&gt;')
document.write('&lt;/font&gt;')
}
else
document.write(message)

function crossref(number){
//alert("changecolor"+number);
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number);
return crossobj
}

function color(){

//Change all letters to base color
if (n==0){
for (m=0;m &lt;  message.length;m++)

//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo

if (n &lt; message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",12000)
return
}
}

function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()

//</xsl:comment>
</script>

    </span>

<ul>
<img src="http://12.32.98.246/localmarketing/images/content/content_main.jpg" width="202" height="231" hspace="5" align="right" alt="The Sky is your limit"/>
<li><span class="text_green"><xsl:value-of select="//lang_blocks/p1"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p2"/><br/><br/></li>
<li><span class="text_green"><xsl:value-of select="//lang_blocks/p3"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p4"/><br/><br/></li>
<li><span class="text_green"><xsl:value-of select="//lang_blocks/p5"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p6"/><br/><br/></li>

</ul>

<img src="http://12.32.98.246/localmarketing/images/content/rewards_club.gif" alt="ClubShop Rewards Card" width="200" height="129" hspace="80"/></td>

  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>
