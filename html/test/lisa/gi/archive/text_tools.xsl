<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
        media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="//lang_blocks/p0"/></title>
<meta name="title" content="Glocal Income"/>
<meta name="Keywords" content="Glocal Income, offline marketing, e-business, training, learning, be successful, Entrupenure"/>
<meta name="abstract" content="Learn everything you need to know to create your own business."/>
<meta http-equiv="content-language" content="en-us"/>
<meta http-equiv="content-language" content="english"/>
<meta http-equiv="content-language" content="lang_en"/>
<meta name="coverage" content="Worldwide"/>
<meta name="distribution" content="Global"/>
<meta name="author" content="Glocal Income"/>
<meta name="design-development" content="Carsten Rieger"/>
<meta name="publisher" content="Glocal Income"/>
<meta name="company" content="Glocal Income"/>
<meta name="copyright" content="Copyright © 2009 Glocal Income"/>

<meta name="page-topic" content="Glocal Income"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="General"/>
<meta name="revisit-after" content="7 days"/>

          

<link href="../styles/css/pages.css" rel="stylesheet" type="text/css"/>

</head>

<body>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">

      <tr>
        <td valign="top" bgcolor="#021A36"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          
          <tr>
            <td align="center" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="10" cellspacing="0">
              <tr>
                <td width="75%" align="left" valign="top">
                <span class="text_bold"><span class="Text_Content"><span class="header">
                  
<script type="text/javascript" language="JavaScript1.2">
<xsl:comment>
var message="<xsl:value-of select="//lang_blocks/p0"/> "
var colorone="#CCCCCC"
var colortwo="#914304"
var flashspeed=100  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('&lt;font color="'+colorone+'"&gt;')
for (m=0;m &lt;  message.length;m++)
document.write('&lt;span id="changecolor'+m+'"&gt;'+message.charAt(m)+'&lt;/span&gt;')
document.write('&lt;/font&gt;')
}
else
document.write(message)

function crossref(number){
//alert("changecolor"+number);
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number);
return crossobj
}

function color(){

//Change all letters to base color
if (n==0){
for (m=0;m &lt;  message.length;m++)

//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo

if (n &lt; message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",12000)
return
}
}

function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()

//</xsl:comment>
</script></span>
             

<script type="text/javascript">
 <xsl:comment> 

/***********************************************
* Tab Content script- © Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

//Set tab to intially be selected when page loads:
//[which tab (1=first tab), ID of tab content to display]:
var initialtab=[1, "sc1"]

////////Stop editting////////////////

function cascadedstyle(el, cssproperty, csspropertyNS){
if (el.currentStyle)
return el.currentStyle[cssproperty]
else if (window.getComputedStyle){
var elstyle=window.getComputedStyle(el, "")
return elstyle.getPropertyValue(csspropertyNS)
}
}

var previoustab=""

function expandcontent(cid, aobject){
if (document.getElementById){
highlighttab(aobject)
detectSourceindex(aobject)
if (previoustab!="")
document.getElementById(previoustab).style.display="none"
document.getElementById(cid).style.display="block"
previoustab=cid
if (aobject.blur)
aobject.blur()
return false
}
else
return true
}

function highlighttab(aobject){
if (typeof tabobjlinks=="undefined")
collecttablinks()
for (i=0; i &lt; tabobjlinks.length; i++)
tabobjlinks[i].style.backgroundColor=initTabcolor
var themecolor=aobject.getAttribute("theme")? aobject.getAttribute("theme") : initTabpostcolor
aobject.style.backgroundColor=document.getElementById("tabcontentcontainer").style.backgroundColor=themecolor
}

function collecttablinks(){
var tabobj=document.getElementById("tablist")
tabobjlinks=tabobj.getElementsByTagName("A")
}

function detectSourceindex(aobject){
for (i=0; i&lt;tabobjlinks.length; i++){
if (aobject==tabobjlinks[i]){
tabsourceindex=i //source index of tab bar relative to other tabs
break
}
}
}

function do_onload(){
var cookiename=(typeof persisttype!="undefined" &amp;&amp; persisttype=="sitewide")? "tabcontent" : window.location.pathname
var cookiecheck=window.get_cookie &amp;&amp; get_cookie(cookiename).indexOf("|")!=-1
collecttablinks()
initTabcolor=cascadedstyle(tabobjlinks[1], "backgroundColor", "background-color")
initTabpostcolor=cascadedstyle(tabobjlinks[0], "backgroundColor", "background-color")
if (typeof enablepersistence!="undefined" &amp;&amp; enablepersistence &amp;&amp; cookiecheck){
var cookieparse=get_cookie(cookiename).split("|")
var whichtab=cookieparse[0]
var tabcontentid=cookieparse[1]
expandcontent(tabcontentid, tabobjlinks[whichtab])
}
else
{
		var query_string = location.search.replace(/^\?/, '');
		var whichtab = query_string?query_string:initialtab[1];
		var which_tabobjlink = whichtab.replace(/^sc/, '');
		
		//expandcontent(initialtab[1], tabobjlinks[initialtab[0]-1]);
		
		expandcontent(whichtab, tabobjlinks[which_tabobjlink - 1]);

}
}

if (window.addEventListener)
window.addEventListener("load", do_onload, false)
else if (window.attachEvent)
window.attachEvent("onload", do_onload)
else if (document.getElementById)
window.onload=do_onload
</xsl:comment> 
</script>


				
				

************Pre-registration********************


			<ul id="tablist">
  			<li><a href="" class="current" onClick="return expandcontent('sc1', this)"><xsl:value-of select="//lang_blocks/p1"/></a></li>
  				<li><a href="" onClick="return expandcontent('sc2', this)" theme="#DFF4B9"><xsl:value-of select="//lang_blocks/p2"/></a></li>
  					<li><a href="" onClick="return expandcontent('sc3', this)" theme="#DFF4B9"><xsl:value-of select="//lang_blocks/p3"/></a></li>
  					<li><a href="" onClick="return expandcontent('sc5', this)" theme="#DFF4B9"><xsl:value-of select="//lang_blocks/p255"/></a></li>
  						<li><a href="" onClick="return expandcontent('sc4', this)" theme="#DFF4B9"><xsl:value-of select="//lang_blocks/p4"/></a></li>
  						
          					</ul>
  
<div id="tabcontentcontainer">
   
  <div id="sc1" class="tabcontent">
  
		<span class="Header_Main"><img src="../images/content/rewards_club.gif" width="200" height="129" align="right" class="image_left_padding" alt="" /><xsl:value-of select="//lang_blocks/p5"/></span>
		<br />

		<p><span class="Text_Content"><xsl:value-of select="//lang_blocks/p6"/> </span><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p7"/></span><xsl:text> </xsl:text><span class="Text_Content"> <xsl:value-of select="//lang_blocks/p8"/><br /><br />
		 <xsl:value-of select="//lang_blocks/p9"/><br/><br/>
		 <xsl:value-of select="//lang_blocks/p10"/><br/><br/>
  		 <xsl:value-of select="//lang_blocks/p11"/><br/><br/></span>
  		  		<span class="text_green"><xsl:value-of select="//lang_blocks/p12"/></span><xsl:text> </xsl:text>
  		<span class="Text_Content"><xsl:value-of select="//lang_blocks/p13"/><br />
  		</span></p>
  
	<ul>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p14"/><br /> </li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p15"/><br /> </li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p16"/> <br /> </li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p17"/></li>

	</ul>



	<p class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p18"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11"  alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_print_merchant_fliers.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p21"/><br /><br />
  	<span class="text_green"><xsl:value-of select="//lang_blocks/p22"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p23"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="../images/pdf/ClubShop_Rewards_Pre-registered_Merchant_Approval_Form.pdf" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p24"/><br /><br />
  	<span class="text_green"><xsl:value-of select="//lang_blocks/p25"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p26"/><br />
	</p>
	
	
		<ul>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p27"/><br /> </li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p27a"/><br /> </li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p28"/><xsl:text> </xsl:text>  <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_list.xsp" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p29"/></a>.</li>
  		</ul>
  
  
		<span class="text_green"><xsl:value-of select="//lang_blocks/p30"/></span>
		<span class="Text_Content"><br /><br />
		</span>



		<ul>
  			<li class="Text_Content"> <span class="text_green"><xsl:value-of select="//lang_blocks/p31"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p32"/><br /> <br /><xsl:value-of select="//lang_blocks/p33"/><br /> <br /><xsl:value-of select="//lang_blocks/p34"/><xsl:text> </xsl:text> <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <a href="text_toolbox_pre_registered_merchant_personal_contact_guide.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p35"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36"/> <xsl:text> </xsl:text> <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_cover_letter.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p37"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38"/><br /><br /><xsl:value-of select="//lang_blocks/p39"/> <br /> </li>
  			<li class="Text_Content"> <span class="text_green"><xsl:value-of select="//lang_blocks/p40"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_phone_contact_guide.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42"/> <span class="text_green"><xsl:value-of select="//lang_blocks/p43"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p36"/> <br /></li>
  			<li class="Text_Content"> <span class="text_green"><xsl:value-of select="//lang_blocks/p45"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p46"/> <br /><br /><xsl:value-of select="//lang_blocks/p47"/><br /> <br /><xsl:value-of select="//lang_blocks/p48"/> <xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt="" /> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_prospecting_email.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p49"/><br /></li>
  			<li class="Text_Content"> <span class="text_green"><xsl:value-of select="//lang_blocks/p50"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt="" /> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_prospecting_postal.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p52"/> </li>
		</ul>
		
		
<span class="text_green"><xsl:value-of select="//lang_blocks/p53"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p54"/><br /><br /></span><span class="Header_Main"><xsl:value-of select="//lang_blocks/p55"/></span> <xsl:text> </xsl:text><span class="Text_Content"><br /><br /><xsl:value-of select="//lang_blocks/p56"/><br /><br /><xsl:value-of select="//lang_blocks/p57"/><br /><br /></span>
<span class="text_green"><xsl:value-of select="//lang_blocks/p58"/></span><xsl:text> </xsl:text><span class="Text_Content"> <xsl:value-of select="//lang_blocks/p59"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="http://glocalincome.com/cgi/appx.cgi/pma" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p60"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p61"/><br /></span>
<span class="text_green"><xsl:value-of select="//lang_blocks/p62"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p63"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11"  alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_new_pre_registered_merchant_welcome_email_dhsc.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p64"/></a> <br /><br /><xsl:value-of select="//lang_blocks/p65"/> <xsl:text> </xsl:text> <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_update_email_dhsc.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p66"/><br /><br /><xsl:value-of select="//lang_blocks/p67"/><xsl:text> </xsl:text><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" alt=""/> <xsl:text> </xsl:text> <a href="text_toolbox_pre_registered_merchant_list.xsp" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p20"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p68"/><br /><br /><xsl:value-of select="//lang_blocks/p69"/></span>
  
  

</div>

*********************Prospecting*******
  
  	<div id="sc2" class="tabcontent">

<img src="../images/content/small/content_4.jpg" width="178" height="129" hspace="5" align="right"/><span class="Header_Main"><xsl:value-of select="//lang_blocks/p70"/></span><br />
<br />
<span class="Header_green_11"><span class="Header_green_14"><xsl:value-of select="//lang_blocks/p90"/></span><br />
  <xsl:value-of select="//lang_blocks/p91"/><br />
  <br />
</span>
  <span class="sitemap"><xsl:value-of select="//lang_blocks/p92"/></span>
  
     <ul>
    <li> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p93"/></span> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p94"/></span><br />
    <br/>
    </li>
    <li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p95"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p94"/><xsl:text> </xsl:text> </span><a href="http://www.clubshop.com/mall/apparel_gifts.html" target="_blank" class="nav_footer_brown">http://www.clubshop.com/mall/apparel_gifts.html</a><br />
    <br/>
    </li>
    <li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p97"/></span> <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p98"/></span></li>
    </ul>
    
<p><img src="../images/icons/icon_checkmark.png" width="12" height="12" alt="checkmark" class="image_right_padding" /><xsl:text> </xsl:text>    <span class="Text_Content_heighlight"> <xsl:value-of select="//lang_blocks/p99"/></span><br />
    </p>
       <ul>
  <li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p100"/></span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p101"/></span></li>
  <li class="footer_divider"><xsl:value-of select="//lang_blocks/p102"/></li>
  <li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p103"/></span> <xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p104"/></span></li>
      </ul>
    
    
    <p><img src="../images/icons/icon_smile_2.png" width="24" height="24" alt="Smile!"/><xsl:text> </xsl:text> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p105"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p106"/></span><br /> 
      <br />
      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p107"/></span> <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p108"/></span><br /> 
      <br />

      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p109"/></span> <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p110"/></span><br /> 
      <br />
      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p111"/></span> <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p112"/><br /> 
      </span><br />
      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p113"/></span> <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p114"/></span><br />

      <br />
      <span class="text_brown"><xsl:value-of select="//lang_blocks/p115"/></span> - "<span class="footer_divider"><xsl:value-of select="//lang_blocks/p116"/></span>?"<br />
      <br />
      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p117"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p118"/><br />
      </span><br />

      <span class="footer_divider"><xsl:value-of select="//lang_blocks/p119"/></span> - <span class="text_brown"><xsl:value-of select="//lang_blocks/p120"/></span><br />
      <br />
      <img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /><xsl:text> </xsl:text> <span class="Text_Content_heighlight"> <xsl:value-of select="//lang_blocks/p99"/></span><br />
    </p>
    
    
      <ol>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p121"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p122"/></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p123"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p124"/></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p125"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p126"/></li>
  <li class="Text_Content"> <span class="text_green"><xsl:value-of select="//lang_blocks/p127"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p128"/></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p129"/><br />
   
    A. </span><span class="sitemap"><xsl:value-of select="//lang_blocks/p130"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p131"/><span class="text_green"><br />
    B. </span><span class="sitemap"><xsl:value-of select="//lang_blocks/p132"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p132"/><span class="text_green"><br />
    C. </span><span class="sitemap"><xsl:value-of select="//lang_blocks/p134"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p135"/><span class="text_green"><br />
    D. </span><span class="sitemap"><xsl:value-of select="//lang_blocks/p136"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p137"/><span class="text_green"><br />
    </span></li>
      </ol>
    
    
    <span class="Text_Content"><xsl:value-of select="//lang_blocks/p138"/></span><xsl:text> </xsl:text><span class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p139"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p140"/></span><br />

    <br />
    <span class="footer_divider"><xsl:value-of select="//lang_blocks/p141"/></span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p142"/><br />
    </span><br />
    <img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" alt="checkmark"/><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p143"/></span><br />
    		
    		
    		<ul>
  			<li> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p144"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p145"/></span></li>
			<li><span class="footer_divider"> <xsl:value-of select="//lang_blocks/p146"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p147"/></span></li>
  			<li><span class="footer_divider"> <xsl:value-of select="//lang_blocks/p148"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p149"/> </span></li>
			</ul>
			
			
    <img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" alt="checkmark"/><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p150"/></span><br />
    		<ul>
		  <li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p151"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p152"/></span><br />
    		<br />
  			</li>
  			<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p153"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p154"/></span><br />
  			<br />
  			</li>
  			<li> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p155"/></span> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p156"/><br />
      		<br />
    		</span></li>
  			<li><span class="text_brown"><xsl:value-of select="//lang_blocks/p157"/></span><span class="Text_Content"> "</span><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p158"/></span>"<br />       
    		<br />
  			</li>
  			<li class="text_brown"><xsl:value-of select="//lang_blocks/p159"/></li>
			</ul>
    <ol>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p160"/></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p161"/><xsl:text> </xsl:text><span class="text_brown"><xsl:value-of select="//lang_blocks/p162"/></span>"</li>

	</ol>

  

  </div>

    		
*******************Presentation*********		
  
  		<div id="sc3" class="tabcontent">
  <img src="../images/content/small/content_2.jpg" width="178" height="129" hspace="5" align="right" alt="icon"/><span class="Header_Main"><xsl:value-of select="//lang_blocks/p72"/></span><br />
  <br />
    <span class="Header_green_11"><span class="Header_green_14"><xsl:value-of select="//lang_blocks/p200"/></span><br />
      <xsl:value-of select="//lang_blocks/p201"/><br />

</span>
<p class="Text_Content"><xsl:value-of select="//lang_blocks/p202"/></p>
  <span class="Header_green_11"><br />
  </span><img src="../images/icons/icon_arrow_box.gif" width="11" height="11" class="image_right_padding" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/pr/annl.curacao/how" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p203"/></a><br />
  <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" class="image_right_padding" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/pr/nlnl/how" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p204"/></a><br />
  <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" class="image_right_padding" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/pr/itit/how" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p205"/></a><br />
  <br />

  <img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p206"/></span>
			<ul>
  			<li><span class="text_brown"><xsl:value-of select="//lang_blocks/p207"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p208"/></span> <br />
      		<br /></li>
  			<li><span class="text_brown"><xsl:value-of select="//lang_blocks/p209"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p210"/> </li>
			</ul>


			<ol>
  			<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p211"/></span><br /><span class="footer_divider">A.</span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p212"/></span><br /> 
    		<span class="footer_divider">B.</span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p213"/></span><br />
			<br />
			</li>
  			<li> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p214"/></span><br /><span class="text_brown"><xsl:value-of select="//lang_blocks/p215"/></span><xsl:text> </xsl:text>- <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p216"/></span><br />
			<br />
			<span class="Text_Content"><xsl:value-of select="//lang_blocks/p217"/></span><br />
			<br />
			</li>
  			<li> <span class="footer_divider"><xsl:value-of select="//lang_blocks/p218"/></span><br /><xsl:value-of select="//lang_blocks/p219"/><br />

        		<br />
    		<span class="footer_divider">A.</span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p220"/></span><br />
			<br />
			<span class="footer_divider">B.</span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p221"/><xsl:text> </xsl:text> (<a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" class="nav_footer_brown" target="_blank"><xsl:value-of select="//lang_blocks/p221a"/></a>)<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p221b"/><br />
			</span><br />
			<span class="Text_Content"><xsl:value-of select="//lang_blocks/p222"/><br />
			<br />
			<xsl:value-of select="//lang_blocks/p223"/><br />
			</span><br />
			<span class="footer_divider">C.</span><xsl:text> </xsl:text> <span class="Text_Content"><xsl:value-of select="//lang_blocks/p224"/><br/> <a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank" class="nav_footer_brown">https://www.clubshop.com/cgi/funding.cgi?action=report</a>
				<br/><ul>
					<li><xsl:value-of select="//lang_blocks/p224a"/></li>
					<li><xsl:value-of select="//lang_blocks/p224b"/></li>
					<li><xsl:value-of select="//lang_blocks/p224c"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/converter.cgi" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p224d"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p224e"/> </li>
					</ul>
			
			
			 </span></li>
			</ol>
			
			
 <p><span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" alt="checkmark"/></span><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p225"/></span></p>

   <span class="Text_Content"><xsl:value-of select="//lang_blocks/p226"/><br />
   <br />
   <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" class="image_right_padding" alt="arrow_icon"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/registration.cgi" target="_blank" class="nav_footer_brown"><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p227"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p228"/> </span><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p229"/></span><span class="Text_Content"><br />
   <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" class="image_right_padding" alt="arrow_icon"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/print/ClubShop_Print_Registration_and_Terms.pdf" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p227"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p228"/> </span><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p230"/></span><br />

   <br />
   <span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /></span><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p231"/></span><br /> 
   <span class="Text_Content"><xsl:value-of select="//lang_blocks/p232"/><br />
   <br />
   <xsl:value-of select="//lang_blocks/p233"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p233a"/><br />
   <br />
<xsl:value-of select="//lang_blocks/p234"/></span><xsl:text> </xsl:text><span class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p235"/></span><span class="Text_Content"><xsl:value-of select="//lang_blocks/p236"/><br />

<br />
<xsl:value-of select="//lang_blocks/p237"/><br />
</span><br />
<span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /></span><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p238"/></span><xsl:text> </xsl:text><span class="text_brown"><xsl:value-of select="//lang_blocks/p239"/></span><br />
<span class="Text_Content"> <xsl:value-of select="//lang_blocks/p240"/></span><xsl:text> </xsl:text>"<span class="text_brown"><xsl:value-of select="//lang_blocks/p241"/></span>".<br /> 
<br />
<span class="Text_Content"><xsl:value-of select="//lang_blocks/p242"/><br />
</span><br />

<span class="text_brown"><xsl:value-of select="//lang_blocks/p243"/></span>. <xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p244"/><br />
</span><br />
<span class="footer_divider"><xsl:value-of select="//lang_blocks/p245"/></span><xsl:text> </xsl:text><span class="Text_Content_heighlight"> <xsl:value-of select="//lang_blocks/p246"/></span><br /> 
<br />
<span class="Text_Content"><xsl:value-of select="//lang_blocks/p247"/><br />   
</span><br />
<span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" alt="checkmark"/></span><xsl:text> </xsl:text><span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p248"/></span><br />
<span class="Text_Content"><xsl:value-of select="//lang_blocks/p249"/></span><xsl:text> </xsl:text><span class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p250"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p251"/></span><span class="Text_Content_bold"><xsl:value-of select="//lang_blocks/p252"/></span><xsl:text> </xsl:text><span class="Text_Content"><xsl:value-of select="//lang_blocks/p253"/><br /> 

<br />
</span><span class="footer_divider"><xsl:value-of select="//lang_blocks/p254"/></span>


  </div>
  
  
***********************Set UP************
  
  

   <div id="sc5" class="tabcontent">

<img src="../images/content/small/content_8.jpg" width="178" height="129" hspace="5" align="right"/><xsl:text> </xsl:text><span class="Header_Main"><xsl:value-of select="//lang_blocks/p256"/></span><br />
  <br />
    <span class="Text_Content"><xsl:value-of select="//lang_blocks/p257"/><br />

    <br />
    <xsl:value-of select="//lang_blocks/p258"/><br />
    <br />

        </span><span class="Text_Content_heighlight"><span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p259"/></span><span class="Text_Content"><br />
    <br />
    <xsl:value-of select="//lang_blocks/p260"/><br />
  
<ul>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p261"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p262"/> <br /></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p263"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p264"/> <xsl:text> </xsl:text> <a href="https://www.clubshop.com/merchants/setup_approval/index.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p265"/></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p266"/><br /></li>
</ul>
<span class="Text_Content"><xsl:value-of select="//lang_blocks/p267"/><br /></span>
<ul>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p268"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p269"/><xsl:text> </xsl:text> <a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" class="nav_footer_brown"><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p270"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p271"/><br /></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p272"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p273"/><br/></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p274"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p275"/><br/></li>
  <li class="Text_Content"><span class="text_green"><xsl:value-of select="//lang_blocks/p276"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p277"/><br/>
    <xsl:value-of select="//lang_blocks/p278"/><br /></li>
</ul>


<p><span class="Text_Content_heighlight"><span class="Text_Content"><img src="../images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p279"/></span><span class="Text_Content"><br /><br /><xsl:value-of select="//lang_blocks/p280"/> <br /></span></p>
<ul>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p281"/><br /></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p282"/></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p283"/></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p284"/></li>

  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p285"/><br /></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p286"/></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p287"/></li>
  <li class="Text_Content"><xsl:value-of select="//lang_blocks/p288"/><br /></li>
</ul>


<p><span class="Text_Content"><xsl:value-of select="//lang_blocks/p289"/><br /><br />
  <xsl:value-of select="//lang_blocks/p290"/><br />
  <br />
    <xsl:value-of select="//lang_blocks/p291"/><br />
  <br />
 <xsl:value-of select="//lang_blocks/p292"/><br />
  <br />

  6. </span><span class="text_green"><xsl:value-of select="//lang_blocks/p293"/></span><span class="Text_Content"><xsl:value-of select="//lang_blocks/p294"/><xsl:text> </xsl:text><a href="http://www.clubshop.com" target="_blank" class="nav_footer_brown">www.clubshop.com</a><br />

  <br /><xsl:value-of select="//lang_blocks/p295"/></span><br />


 </p></span>

  </div>

  
  
  
  
  
  
  
  
 ***************Tools Section**************
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div id="sc4" class="tabcontent">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td bgcolor="#FFBC23" class="sub_header_white_ndent"><xsl:value-of select="//lang_blocks/p73"/></td>
  </tr>

  <tr>
    <td bgcolor="#FFBC23">
    <table width="100%" border="0" cellpadding="15" cellspacing="1" bgcolor="#CCCCCC">
      <tr>
        <td align="left" valign="top" bgcolor="#FFECBF">
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon" /> <span class="footer_bold">1. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_print_merchant_fliers.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p74"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">2.  </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/images/pdf/ClubShop_Rewards_Pre-registered_Merchant_Approval_Form.pdf" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p75"/></a><br />

          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">3. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/cgi/appx.cgi/pma" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p76"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon" /> <span class="footer_bold">4. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_list.xsp" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p77"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon" /> <span class="footer_bold">5. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_phone_contact_guide.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p78"/></a><br />

          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">6. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_cover_letter.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p79"/></a> <br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">7. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_personal_contact_guide.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p80"/></a> <br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">8. </span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_prospecting_email.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p81"/></a><br />

          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon" /> <span class="footer_bold">9.</span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_prospecting_postal.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p82"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">10.</span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_new_pre_registered_merchant_welcome_email_dhsc.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p83"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle"  alt="arrow_icon"/> <span class="footer_bold">11.</span><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/localmarketing/pages/text_toolbox_pre_registered_merchant_update_email_dhsc.xml" class="nav_footer_brown"> <xsl:value-of select="//lang_blocks/p84"/></a><br/>

		  <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon"/><span class="footer_bold">12.  </span><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/registration.cgi" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p85"/></a><br />
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon"/> <span class="footer_bold">13. </span><xsl:text> </xsl:text><a href="https://www.clubshop.com/print/ClubShop_Print_Registration_and_Terms.pdf" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p86"/></a><br/>
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon"/> <span class="footer_bold">14. </span><xsl:text> </xsl:text><a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p87"/></a><br/>
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon"/> <span class="footer_bold">15. </span><xsl:text> </xsl:text><a href="https://www.clubshop.com/merchants/setup_approval/index.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p265"/></a><br/>
          <img src="../images/icons/icon_arrow_box.gif" width="11" height="11" align="absmiddle" alt="arrow_icon"/> <span class="footer_bold">16. </span><xsl:text> </xsl:text><a href="http://www.glocalincome.com/localmarketing/pages/text_download.html" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p300"/></a>

</td>
    
      </tr>
    </table></td>
  </tr>
</table>


  </div>
 

  </div>	</span></span>	</td>
                </tr>
              
              
            </table></td>

          </tr>
          
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
	

<!--Start of Google Analytics Stat Code 
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-6467634-1");
pageTracker._trackPageview();
</script>

Start of Google Analytics Stat Code -->




</body>
</html>



</xsl:template>
</xsl:stylesheet>