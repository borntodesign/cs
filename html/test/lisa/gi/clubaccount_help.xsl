<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

  <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p1"/></title>



<link href="/test/lisa/gi/pages.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      
    	<tr>
        <td align="center" bgcolor="#000000"><span class="header">

<script type="text/javascript" language="JavaScript1.2">
<xsl:comment>
var message="<xsl:value-of select="//lang_blocks/p0"/> "
var colorone="#CCCCCC"
var colortwo="#914304"
var flashspeed=100  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('&lt;font color="'+colorone+'"&gt;')
for (m=0;m &lt;  message.length;m++)
document.write('&lt;span id="changecolor'+m+'"&gt;'+message.charAt(m)+'&lt;/span&gt;')
document.write('&lt;/font&gt;')
}
else
document.write(message)

function crossref(number){
//alert("changecolor"+number);
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number);
return crossobj
}

function color(){

//Change all letters to base color
if (n==0){
for (m=0;m &lt;  message.length;m++)

//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo

if (n &lt; message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",12000)
return
}
}

function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()

//</xsl:comment>
</script>


</span></td>
      </tr>
      
      
      <tr>
        <td bgcolor="#245923"><img src="gi.gif" width="640" height="70" /></td>
      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

          <tr>
            <td align="right"><a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle"/>PRINT PAGE</a> <xsl:text> </xsl:text> <xsl:text> </xsl:text><a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span>


<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>




<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p10"/></span></p>
<p><xsl:value-of select="//lang_blocks/p11"/></p>

<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p6"/></span></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p8"/></span></p>
<p><xsl:value-of select="//lang_blocks/p9"/></p>


<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p10"/></span></p>
<p><xsl:value-of select="//lang_blocks/p11"/></p>

<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p12"/></span></p>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p13a"/></span></p>
<p><xsl:value-of select="//lang_blocks/p13b"/></p>

<table border="1px">
<tr align="center">
<td class="text_brown"><b><xsl:value-of select="//lang_blocks/p28"/></b></td>
<td><xsl:value-of select="//lang_blocks/p29"/></td>
<td><xsl:value-of select="//lang_blocks/p30"/></td>
<td><xsl:value-of select="//lang_blocks/p31"/></td>
<td><xsl:value-of select="//lang_blocks/p32"/></td>
<td><xsl:value-of select="//lang_blocks/p33"/></td>
<td><xsl:value-of select="//lang_blocks/p34"/></td>
</tr>

<tr align="center">
<td class="text_brown" align="center"><b><xsl:value-of select="//lang_blocks/p36"/></b></td>
<td>$1.00</td>
<td>$1.00</td>
<td>$1.00</td>
<td>$3.00</td>
<td>$25.00</td>
<td>$3.00</td>

</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p35"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/converter.cgi" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p13c"/></a></p>


<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p14"/></span></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>



<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p18"/></span></p>
<p><xsl:value-of select="//lang_blocks/p19"/></p>
<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p20"/></span></p>
<p><xsl:value-of select="//lang_blocks/p21"/></p>
<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p16"/></span></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>
<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p22"/></span></p>
<p><xsl:value-of select="//lang_blocks/p23"/></p>
<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p24"/></span></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<p><span class="Header_Main"><xsl:value-of select="//lang_blocks/p26"/></span></p>
<p><xsl:value-of select="//lang_blocks/p27"/></p>


</td></tr>


</table></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
