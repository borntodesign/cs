<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<title>The DHS Club - Discount Home Shoppers' Club</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link href="http://www.clubshop.com/test/lisa/gi/main.css" rel="stylesheet" type="text/css"/>



</head>

<body onload="MM_preloadImages('../images/nav/nav_signup_2.jpg','../images/nav/nav_home_2.jpg','../images/nav/nav_about_2.jpg','../images/nav/nav_opportunity_2.jpg','../images/nav/nav_packages_2.jpg','../images/nav/nav_contact_2.jpg','../images/nav/nav_income_2.jpg','../images/nav/nav_launch_2.jpg','../images/nav/nav_tools_2.jpg','../images/nav/nav_card_2.jpg','../images/nav/nav_merchant_2.jpg','../images/nav/nav_campaign_2.jpg','../images/nav/nav_servicing_2.jpg','../images/nav/nav_business_2.jpg')"/>
<table width="100%" hight="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="976" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table id="Table_01" width="976" height="760" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="5"><a href="http://www.clubshop.com/test/lisa/gi/abusefraud.xml"><img src="../images/main/local_marketing_header_logo.jpg" alt="" width="975" height="136" border="0" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="136" alt="" /></td>

          </tr>
          <tr>
            <td rowspan="18"><img src="../images/main/glocal_income_left_bar.jpg" width="81" height="579" alt="" /></td>
            <td rowspan="2"><img src="../images/main/glocal_income_1.jpg" width="169" height="105" alt="" /></td>
            <td rowspan="2"><img src="../images/main/index_04.jpg" width="26" height="105" alt="" /></td>
            <td><img src="../images/slides/slide_small/slide_small__main_3.jpg" width="618" height="93" alt="" /></td>
            <td rowspan="17"><img src="../images/main/glocal_income_right_bar.jpg" width="81" height="553" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="93" alt="" /></td>
          </tr>

          <tr>
            <td rowspan="2"><img src="../images/main/glocal_income_8.jpg" width="618" height="25" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="12" alt="" /></td>
          </tr>
          <tr>
            <td rowspan="2"><a href="../index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image44','../images/nav/nav_home_2.jpg',0,0,0.4,1)"><img src="../images/nav/nav_home_1.jpg" alt="Home" name="Image44" width="169" height="25" border="0" id="Image44" /></a></td>
            <td rowspan="16"><img src="../images/main/glocal_income_3.jpg" width="26" height="474" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="13" alt="" /></td>
          </tr>

          <tr>
            <td rowspan="2"><img src="../images/header/income.gif" width="618" height="30" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="12" alt="" /></td>
          </tr>
          <tr>
            <td rowspan="2"><img src="../images/nav/nav_income_3.jpg" alt="Income Potential" name="Image45" width="169" height="25" border="0" id="Image45" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="18" alt="" /></td>
          </tr>
          <tr>

            <td height="405" rowspan="12" align="center" valign="top" bgcolor="#FFFFFF"><iframe id="ifrm" name="ifrm" src="../pages/text_income.html" scrolling="Auto" width="618" height="405" frameborder="0" allowtransparency="true"></iframe></td>
            <td><img src="../images/main/spacer.gif" width="1" height="7" alt="" /></td>
          </tr>
          <tr>
            <td><a href="../campaign/index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image46','../images/nav/nav_campaign_2.jpg',0,0,0.4,1)"><img src="../images/nav/nav_campaign_1.jpg" alt="Income Phases" name="Image46" width="169" height="25" border="0" id="Image46" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="25" alt="" /></td>
          </tr>
          <tr>

            <td><a href="../tools/index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image47','../images/nav/nav_tools_2.jpg',0,0,1,1)"><img src="../images/nav/nav_tools_1.jpg" alt="Tools &amp; Training" name="Image47" width="169" height="25" border="0" id="Image47" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="25" alt="" /></td>
          </tr>
          <tr>
            <td><a href="../card/index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image48','../images/nav/nav_card_2.jpg',0,0,0.4,1)"><img src="../images/nav/nav_card_1.jpg" alt="Card Distribution" name="Image48" width="169" height="25" border="0" id="Image48" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="25" alt="" /></td>
          </tr>
          <tr>
            <td><a href="../servicing/index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image49','../images/nav/nav_servicing_2.jpg',0,0,0.4,1)"><img src="../images/nav/nav_servicing_1.jpg" alt="Servicing Merchants" name="Image49" width="169" height="23" border="0" id="Image49" /></a></td>

            <td><img src="../images/main/spacer.gif" width="1" height="23" alt="" /></td>
          </tr>
          <tr>
            <td><a href="../business/index.html" onmouseout="flvFSTI2()" onmouseover="flvFSTI1('Image50','../images/nav/nav_business_2.jpg',0,0,0.4,1)"><img src="../images/nav/nav_business_1.jpg" alt="Business Expansion" name="Image50" width="169" height="25" border="0" id="Image50" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="25" alt="" /></td>
          </tr>
          <tr>
            <td><img src="../images/main/local_marketing_thumb_1.jpg" width="169" height="12" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="12" alt="" /></td>

          </tr>
          <tr>
            <td><a href="javascript:popupcenter('../pages/text_presentation.html', 666, 606)"><img src="../images/thumb/thumb_local_marketing_1.jpg" alt="Watch our marketing presentation!" width="169" height="89" border="0" /></a></td>
            <td><img src="../images/main/spacer.gif" width="1" height="89" alt="" /></td>
          </tr>
          <tr>
            <td><img src="../images/main/local_marketing_thumb_2.jpg" width="169" height="11" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="11" alt="" /></td>
          </tr>

          <tr>
            <td><img src="../images/thumb/thumb_local_marketing_2.jpg" width="169" height="89" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="89" alt="" /></td>
          </tr>
          <tr>
            <td><img src="../images/main/local_marketing_thumb_2a.jpg" width="169" height="11" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="11" alt="" /></td>
          </tr>
          <tr>

            <td rowspan="2"><img src="../images/thumb/thumb_local_marketing_3.jpg" width="169" height="89" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="63" alt="" /></td>
          </tr>
          <tr>
            <td colspan="2"><img src="../images/main/glocal_income_footer.jpg" width="699" height="26" alt="" /></td>
            <td><img src="../images/main/spacer.gif" width="1" height="26" alt="" /></td>
          </tr>
          <tr>
            <td colspan="5"><img src="../images/main/index_25.jpg" width="975" height="45" alt="" /></td>

            <td><img src="../images/main/spacer.gif" width="1" height="45" alt="" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="top"><iframe id="ifrm2" name="ifrm" src="../_includes/footer.html" scrolling="Auto" width="975" height="75" frameborder="0" allowtransparency="true"></iframe></td>
      </tr>
    </table></td>

  </tr>
</table>
</html>


</xsl:template>
</xsl:stylesheet>


