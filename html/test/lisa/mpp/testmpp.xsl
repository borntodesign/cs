<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">

<html>

<head>
<title>Affiliate Partner Account</title>
<link href="/css/member_portal.css" rel="stylesheet" type="text/css" /></head>

<body>
<table width="100%" cellspacing="0">

<tr>
<td colspan="3">
<img src="/images/mpp/portal_head.gif" width="758" height="93" alt="Affiliate Partner Account"/>
<div id="minfo">
<xsl:value-of select="//lang_blocks/name"/>: %%name%%<br />
<xsl:value-of select="//lang_blocks/idnum"/>: %%alias%%<br />
<xsl:value-of select="//lang_blocks/membertype"/>: %%membertype%%<br />
<xsl:value-of select="//lang_blocks/reward_bal"/>: %%reward_total%%<br />
<xsl:value-of select="//lang_blocks/pending_points"/>: %%pending_points%%</div>
</td>
</tr>

<tr>
<td id="sidelinks">

<h4><img src="/images/mpp/asseenontv.gif" width="63" height="47" alt="As Seen On TV"/>
<xsl:value-of select="//lang_blocks/tv"/></h4>
<a href="http://www.clubshop.com/worlds_greatest.html">
<xsl:value-of select="//lang_blocks/schedule"/></a>

<h4><img src="/images/mpp/bizopp.gif" width="39" height="50" alt="Business Opportunity"/>
<xsl:value-of select="//lang_blocks/lsideh3"/></h4>
<xsl:if test="//report/report_available = 1">
	<a href="/cgi-bin/ppreport.cgi?a=%%alias%%&amp;activity_report=1">
	<xsl:value-of select="//lang_blocks/lsidesh3d"/></a>
</xsl:if>
<a href="/cgi/appx.cgi//upg?id=%%alias%%">
<xsl:value-of select="//lang_blocks/lsidesh3c"/></a>
<a href="/cgi-bin/um?id=%%alias%%">
<xsl:value-of select="//lang_blocks/lsidesh3b"/></a>
<a href="/mbr/presentation/intro.xml?id=%%alias%%">
<xsl:value-of select="//lang_blocks/lsidesh3a"/></a>


<h4><img src="/images/mpp/cart.gif" width="43" height="50" alt="mall"/>
<xsl:value-of select="//lang_blocks/lsideh1"/></h4>
<a href="/cgi-bin/wwg">
<!--a href="http://www.clubshop.com/mall/"-->
<xsl:value-of select="//lang_blocks/lsidesh1a"/></a>
<a href="/rewardsdirectory/">
<xsl:value-of select="//lang_blocks/lsidesh1d"/></a>


<h4><img src="/images/mpp/tag_small.gif" width="43" height="24" alt="Rewards Points"/>
<xsl:value-of select="//lang_blocks/lsideh2"/></h4>
<a href="/cgi/reward_center.cgi">
<xsl:value-of select="//lang_blocks/lsidesh2a"/></a>
<a href="/co_explain.xml">
<xsl:value-of select="//lang_blocks/cos"/></a>


</td>

<td id="main"  valign="top">

<!--middle postings-->
<xsl:choose>
	<!-- If this is an activity report -->
	<xsl:when test="//report/show_report = 1">
		<xsl:value-of select="//lang_blocks/mid3" />
		<hr />
		<xsl:for-each select="//report/activities/*">
			<span class="date"><xsl:value-of select="date"/></span>
			<ul class="activity">
				<xsl:for-each select="activities/*">
					<li class="activity">
						<xsl:choose>
							<xsl:when test=". = 'new'">
								<xsl:value-of select="//lang_blocks/activity_new_org"/>
							</xsl:when>
							<xsl:when test=". = 1">
								<xsl:value-of select="//lang_blocks/activity_one"/>
							</xsl:when>
							<xsl:when test=". = 4">
								<xsl:value-of select="//lang_blocks/activity_four"/>
							</xsl:when>
							<xsl:when test=". = 81">
								<xsl:value-of select="//lang_blocks/activity_81"/>
							</xsl:when>
							<xsl:when test=". = 67">
								<xsl:value-of select="//lang_blocks/activity_82"/>
							</xsl:when>
							<xsl:when test=". = 'expires'">
								<xsl:value-of select="//lang_blocks/activity_org_expires_soon"/>
							</xsl:when>
						</xsl:choose>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
		<p>
			<a href="/cgi-bin/um?id=%%alias%%">
				<xsl:value-of select="//lang_blocks/click_here"/>
			</a>
			<xsl:text> </xsl:text>
			<xsl:value-of select="//lang_blocks/means_to_you"/>
		</p>

	</xsl:when>
	<!-- If this is the portal home page -->
	<xsl:otherwise>

	<xsl:value-of select="//lang_blocks/mid1"/>
	<xsl:text></xsl:text> %%firstname%%<xsl:text>!</xsl:text>

<hr/>

<xsl:if test="//report_available = 1">
	<xsl:value-of select="//lang_blocks/mid2" />
	<xsl:text> </xsl:text>
	<a href="/cgi-bin/ppreport.cgi?a=%%alias%%&amp;activity_report=1">
	<xsl:value-of select="//lang_blocks/mid3"/></a>
</xsl:if>

<!--ENGLISH--><p><b><xsl:value-of select="//lang_blocks/mid4"/></b><br/>
 <xsl:value-of select="//lang_blocks/mid5"/><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/US"><xsl:value-of select="//lang_blocks/mid6"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/GB"><xsl:value-of select="//lang_blocks/mid6a"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/CA"><xsl:value-of select="//lang_blocks/mid6b"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/AU"><xsl:value-of select="//lang_blocks/mid6c"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/AW"><xsl:value-of select="//lang_blocks/aruba"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/AN"><xsl:value-of select="//lang_blocks/nethan"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/ZA"><xsl:value-of select="//lang_blocks/sa"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/IE"><xsl:value-of select="//lang_blocks/ie"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/MY"><xsl:value-of select="//lang_blocks/my"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/PH"><xsl:value-of select="//lang_blocks/ph"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/HK"><xsl:value-of select="//lang_blocks/hk"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/IN"><xsl:value-of select="//lang_blocks/in"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/IL"><xsl:value-of select="//lang_blocks/il"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/GY"><xsl:value-of select="//lang_blocks/gy"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/NZ"><xsl:value-of select="//lang_blocks/nz"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/CZ"><xsl:value-of select="//lang_blocks/cz"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/JP"><xsl:value-of select="//lang_blocks/jp"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/GR"><xsl:value-of select="//lang_blocks/gr"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/CN"><xsl:value-of select="//lang_blocks/cn"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/SG"><xsl:value-of select="//lang_blocks/sg"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/DK"><xsl:value-of select="//lang_blocks/dk"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/SK"><xsl:value-of select="//lang_blocks/sk"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/BS"><xsl:value-of select="//lang_blocks/bs"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/TT"><xsl:value-of select="//lang_blocks/tt"/></a><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/BM"><xsl:value-of select="//lang_blocks/bm"/></a><xsl:text> </xsl:text>

<xsl:value-of select="//lang_blocks/conj"/><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/BB"><xsl:value-of select="//lang_blocks/bb"/></a><xsl:text> </xsl:text>


</p>
<p><xsl:value-of select="//lang_blocks/rp500"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/rp5001"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/rp500a"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/marketing_questionnaires.cgi"><xsl:value-of select="//lang_blocks/rp500b"/></a></p>

<p></p>
<!--end ENGLISH-->


<!--ITALIAN-->
<p><b><xsl:value-of select="//lang_blocks/it1"/></b><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/it2"/> <xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/IT" target="_blank"><xsl:value-of select="//lang_blocks/it3"/></a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/it4"/></p>
<!--END Italian-->

<!--FRENCH-->
<p><b><xsl:value-of select="//lang_blocks/frmall"/></b><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/FR" target="_blank"><xsl:value-of select="//lang_blocks/frmall1"/></a>
<xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/DZ" target="_blank"><xsl:value-of select="//lang_blocks/dz"/></a><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/TN" target="_blank"><xsl:value-of select="//lang_blocks/tn"/></a>
<xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/MQ" target="_blank"><xsl:value-of select="//lang_blocks/mq"/></a><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/GP" target="_blank"><xsl:value-of select="//lang_blocks/gp"/></a>
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/frconj"/><xsl:text> </xsl:text>


<a href="http://www.clubshop.com/mall/CA.fr" target="_blank"><xsl:value-of select="//lang_blocks/cafr"/></a>
</p>


<p><b><xsl:value-of select="//lang_blocks/fr1"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/fr2"/> <xsl:text> </xsl:text><a href="mailto:support.fr@dhs-club.com"><xsl:value-of select="//lang_blocks/fr3"/></a></p>

<!-- Dutch-->
<p><b><xsl:value-of select="//lang_blocks/dumall"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/dumall2"/><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/mall/NL" target="_blank"><xsl:value-of select="//lang_blocks/du1"/></a><xsl:text> </xsl:text>  <a href="/mall/BE" target="_blank"><xsl:value-of select="//lang_blocks/du2"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/cont"/> <xsl:text> </xsl:text> <a href="/mall/SR" target="_blank"><xsl:value-of select="//lang_blocks/du3"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/du4"/></p>


<br/>
<div align="center">
<a href="http://www.clubshop.com/cgi/marketing_questionnaires.cgi" target="_blank"><img src="/images/rp500.gif" height="125" width="125" alt="Earn 500 ClubCash"/></a></div>
<br/>
<div align="center">
<a href="http://www.clubshop.com/weeklyspecial.html" target="_blank"><img src="http://www.clubshop.com/images/mpp/weeklyspecial.gif" height="150" width="160" border="1" alt="weekly special"/></a></div>
<!--end middle postings-->




<hr />
<br />



	</xsl:otherwise>
</xsl:choose>

<!-- end middle postings-->









</td>

<td id="rsidelinks" valign="top">
<!--
messaging cannot be used in here until the messaging system
is revised to be able to do a global message to VIPs only
-->
<!-- % %MESSAGING% % -->

<h4><xsl:value-of select="//lang_blocks/rsideh0"/></h4>
<h5><xsl:value-of select="//lang_blocks/rsideh1"/></h5>
%%upline_contact_list%%
<!--h5/-->
<a href="/contact.xml">
<xsl:value-of select="//lang_blocks/lsidesh4a"/></a>
<h4><img src="/images/mpp/info.gif" width="43" height="50" alt=""/>
<xsl:value-of select="//lang_blocks/rsideh2"/></h4>
<a href="http://www.clubshop.com/memberorient.html">
<xsl:value-of select="//lang_blocks/rsidesh2a"/></a>
<a href="/manual/index.xml">
<xsl:value-of select="//lang_blocks/rsidesh2c"/></a>
<a href="http://www.clubshop.com/legal.xml">
<xsl:value-of select="//lang_blocks/rsidesh2b"/></a>

<h4><img src="/images/mpp/personal.gif" width="43" height="50" alt=""/>
<xsl:value-of select="//lang_blocks/rsideh4"/></h4>

<a href="https://www.clubshop.com/cgi/member_trans_rpt.cgi">
<xsl:value-of select="//lang_blocks/lsidesh5a"/></a>
<!-- income report -->
<a href="https://www.clubshop.com/cgi/member_ppp_rpt.cgi">
<xsl:value-of select="//lang_blocks/rsidesh5c"/></a>

<a href="https://www.clubshop.com/cgi/appx.cgi/update">
<xsl:value-of select="//lang_blocks/rsidesh4a"/></a>
<a href="/remove-me.xml">
<xsl:value-of select="//lang_blocks/rsidesh4b"/></a>

<a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">
<xsl:value-of select="//lang_blocks/ca"/></a>
<br/>
<!-- this would be so much better if the nodes were available rather than old style placeholders -->
<xsl:if test="'%%membertype%%' = 'Clubshop Rewards Member'">
<a href="/cgi/NetShopUpgrade.cgi?alias=%%alias%%">
<xsl:value-of select="//lang_blocks/rsidesh5b"/></a>
</xsl:if>

<h4>
<xsl:value-of select="//lang_blocks/rsideh6"/></h4>
<a href="http://www.clubshop.com/cb-activation.xml">
<xsl:value-of select="//lang_blocks/rsideh6a"/></a>
<a href="http://www.clubshop.com/getcards.html">
<xsl:value-of select="//lang_blocks/rsideh6b"/></a>
<a href="http://www.clubshop.com/assign_cards.html">
<xsl:value-of select="//lang_blocks/rsideh6c"/></a>
<a href="https://www.clubshop.com/cgi/referral_cards.cgi">
<xsl:value-of select="//lang_blocks/rsideh6d"/></a>
<a href="http://www.clubshop.com/rewardsdirectory">
<xsl:value-of select="//lang_blocks/rsideh6e"/></a>


<h4><img src="/images/mpp/marketing.gif" width="37" height="37" alt="Member Marketing"/><xsl:value-of select="//lang_blocks/rsideh7"/></h4>
<a href="/member_mktmanual.html"><xsl:value-of select="//lang_blocks/rsideh7a"/></a>
<a href="/member_referral.html"><xsl:value-of select="//lang_blocks/rsideh7b"/></a>
<a href="/banners/select.xsp"><xsl:value-of select="//lang_blocks/rsideh7e"/></a>
<a href="/cgi/appx.cgi/%%alias%%"><xsl:value-of select="//lang_blocks/rsideh7c"/></a>
<a href="/ceochat/index.html"><xsl:value-of select="//lang_blocks/rsideh7d"/></a>

</td>
</tr>

<tr>
<td colspan="3" id="footer"></td>
</tr>
</table>

<!--conditional-langpref-open-->
<!-- THIS IS THE LANGUAGE SELECTION POP-UP -->
<script type="text/javascript" src="/cgi/language_pref.cgi">
</script>
<!-- END OF THE LANGUAGE SELECTION POP-UP -->
<!--conditional-langpref-close-->

</body>
</html>

</xsl:template>
</xsl:stylesheet>
