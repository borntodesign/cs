<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">

<html>

<xsl:comment>
myodp_me_deadline = %%myodp_me_deadline%%
myodp_me_counts_member_count = %%myodp_me_counts_member_count%%
myodp_me_counts_partner_count = %%myodp_me_counts_partner_count%%
</xsl:comment>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />


<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />
<title>Affiliate Business Center</title>
<!-- Included CSS Files -->
<link rel="stylesheet" type="text/css" href="http://www.glocalincome.com/css/foundation/foundation.css"/>
<link rel="stylesheet" type="text/css" href="http://www.glocalincome.com/css/foundation/app.css"/>
<link rel="stylesheet" type="text/css" href="http://www.glocalincome.com/css/foundation/general.css"/>
<link rel="stylesheet" type="text/css" href="http://www.glocalincome.com/css/foundation/manual_2012.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<!--[if lt IE 9]>
		<link rel="stylesheet" href="http://www.clubshop.com/css/partner/ie.css"/>
	<![endif]-->
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!-- Included JS Files -->
<script src="http://www.glocalincome.com/js/foundation/foundation.js"></script>
<script src="www.glocalincome.com/js/foundation/app.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/tipmap.js"></script>

<style type="text/css">
.text_blue {
	font-size: 12px;
	color: #00529E;
	font-weight: bold;
}

.text_smaller {font-size: 11px;}

.tip_trigger{cursor:help;}
	
.tip {
			color: #fff;
			background:#00376F;
			display:none; /*--Hides by default--*/
			padding:10px;
			position:absolute; 
			z-index:1000;
			margin-top: 100px;
			right:300px;
		}

</style>

<script type="text/javascript">
<xsl:comment>
//alert(deadline.toString());
$(document).ready(function() {

//	$("#Member_Name").text(member_name);

			/*
				Tips:
				
				event.target is DOM Element
				this is DOM element
				$(this) is jQuery Element
				timer is interval for countdown
				
				If a countdown should end early you could do:
				
				clearInterval( timer );
				$(this).trigger('complete');
			*/	
	$("#time").countdown({date: deadline, //Counting TO a date
		offset: -7,
		//htmlTemplate: "%{h} &lt;span class=\"cd-time\"&gt;hours&lt;/span&gt; %{m} &lt;span class=\"cd-time\"&gt;mins&lt;/span&gt; %{s} &lt;span class=\"cd-time\"&gt;sec&lt;/span&gt;",
		//date: "july 1, 2011 19:24", //Counting TO a date
		onChange: function( event, timer ){
		


		},
		onComplete: function( event ){
		
			$(this).html("Time Expired");
		},
		leadingZero: true,
		direction: "down"
	});
	
	$("#MemberNumberCount").text(member_count);
	$("#PartnerNumberCount").text(partner_count);
	
	

	// setup scrolling
	// first set the text in the divs
	//$("#MemberTicker").text(members);
	$("#PartnerTicker").text(partners);

	//create scroller for each element with "horizontal_scroller" class...
	$('.horizontal_scroller').SetScroller({	velocity: 	 80,
											direction: 	 'horizontal',
											startfrom: 	 'right',
											loop:		 'infinite',
											movetype: 	 'linear',
											onmouseover: 'play',
											onmouseout:  'play',
											onstartup: 	 'play',
											cursor: 	 'pointer'
										});
	/*
		All possible values for options...
		
		velocity: 		from 1 to 99 								[default is 50]						
		direction: 		'horizontal' or 'vertical' 					[default is 'horizontal']
		startfrom: 		'left' or 'right' for horizontal direction 	[default is 'right']
						'top' or 'bottom' for vertical direction	[default is 'bottom']
		loop:			from 1 to n+, or set 'infinite'				[default is 'infinite']
		movetype:		'linear' or 'pingpong'						[default is 'linear']
		onmouseover:	'play' or 'pause'							[default is 'pause']
		onmouseout:		'play' or 'pause'							[default is 'play']
		onstartup: 		'play' or 'pause'							[default is 'play']
		cursor: 		'pointer' or any other CSS style			[default is 'pointer']
	*/
});
</xsl:comment></script> 


<script type="text/javascript"><xsl:comment>
            var windowSizeArray = [ "width=450,height=300" ];
 
            $(document).ready(function(){
                $('.newWindow').click(function (event){
 
                    var url = $(this).attr("href");
                    var windowName = "popUp";//$(this).attr("name");
                    var windowSize = windowSizeArray[$(this).attr("rel")];
 
                    window.open(url, windowName, windowSize);
 
                    event.preventDefault();
 
                });
            });
        </xsl:comment></script>



		
</head>

<body class="blue">

<!-- Top Blue Header Container -->
	<div class="container blue">
		
		<div class="row">		
			<div class="six columns">				
				<a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin:5px;"/></a>
			</div>
			
			<div class="six columns">				
				<!--Extra Space Future Space -->
				
				<div style="margin-top: 8px; color:#fff; ">
					<strong><xsl:value-of select="//lang_blocks/name"/>:</strong> %%name%%<br />
					<strong><xsl:value-of select="//lang_blocks/idnum"/>:</strong> %%alias%%<br />
					<strong><xsl:value-of select="//lang_blocks/membertype"/>:</strong> %%membertype%%<br />
					<strong><xsl:value-of select="//lang_blocks/reward_bal"/>:</strong> %%reward_total%% %%cc_currency%%<br />
					
				</div>

				
			</div>			
		</div>		
	</div>	
<!--End Top Blue Header Container -->


<!--Content Container -->			
	<div class="container " style="background:#FFF; margin-top: 10px;">	
		<div class="row "><div class="twelve columns"><div class="push"></div></div></div>	
		<div class="row ">	
			<div class="twelve columns"> <!--Main Content -->	
<!-- CONTENT STARTS -->

					
				<div class="three columns"> <!--Left Side Content -->					
					
<!--NEW NEW NEW NEW -->					
					
					<div class="panel">
						<p class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/aff_title"/></p><hr />
						<ul class="">
							<li><a href="http://www.clubshop.com/affiliate/getstarted.xml" target="_blank"><xsl:value-of select="//lang_blocks/aff_link1"/></a></li>
							<li><a href="http://www.clubshop.com/manual/compensation/affiliate.html" target="_blank"><xsl:value-of select="//lang_blocks/aff_link2"/></a></li>
							<li><a href="https://www.glocalincome.com/cgi/member_ppp_rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/aff_link3"/></a></li>
                            <li><a href="http://clubshop.com/cgi/member_frontline2.cgi" target="_blank"><xsl:value-of select="//lang_blocks/aff_link4"/></a></li>
							<li><a href="http://www.clubshop.com/affiliate/affiliatefaq.xml" target="_blank"><xsl:value-of select="//lang_blocks/aff_link5"/></a></li>
						</ul>
					</div>

					<div class="panel">
                        <p class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/pi_title"/></p><hr />
                        <ul class="">
			    <li><a href="http://www.clubshop.com/present/bizop.xml" target="_blank"><xsl:value-of select="//lang_blocks/pi_bizop"/></a></li>
                            <li><a href="http://www.glocalincome.com/cgi/my_pool.cgi" target="_blank"><xsl:value-of select="//lang_blocks/pi_link1"/></a></li>
                            <li><a href="http://www.glocalincome.com/cgi/joinpool.cgi" target="_blank"><xsl:value-of select="//lang_blocks/pi_link2"/></a></li>
                            <li><a href="http://www.glocalincome.com/qualify.html" target="_blank"><xsl:value-of select="//lang_blocks/pi_link3"/></a></li>
							<li><a href="http://www.clubshop.com/training_calendar.html" target="_blank"><xsl:value-of select="//lang_blocks/pi_link4"/></a></li>
							<li><a href="http://www.clubshop.com/affiliate/becomingapartner.xml" target="_blank"><xsl:value-of select="//lang_blocks/pi_link5"/></a></li>
						</ul>			
					</div>

					<div class="panel">				
						<p class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/s_title"/></p><hr />
						<ul class="">
							<li><a href="http://www.clubshop.com/cgi-bin/wwg" target="_blank"><xsl:value-of select="//lang_blocks/s_link1"/></a></li>
							<li><a href="http://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank"><xsl:value-of select="//lang_blocks/s_link2"/></a></li>
							<li><a href="http://www.clubshop.com/members/clubcash_redemptions.shtml" target="_blank"><xsl:value-of select="//lang_blocks/s_link3"/></a></li>
							<li><a href="http://www.glocalincome.com/cgi/member_trans_rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/s_link4"/></a></li>
						</ul>
					</div>

					<div class="panel">
						<p class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/p_title"/></p><hr />
						<ul class="">
							<li><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p_link1"/></a></li>
							<li><a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p_link2"/></a></li>
							<li><a href="http://www.clubshop.com/remove-me.shtml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p_link3"/></a></li>
							<li><a href="http://www.clubshop.com/cgi/mytcex.cgi " target="_blank"><xsl:value-of select="//lang_blocks/p_link4"/></a></li>					
						</ul>
					</div>		
						
				</div> <!-- END OF Left Side Content -->
				


				<div class="nine columns"> <!--Center Content -->		
				
							<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<!--<p class="text_smaller"><xsl:value-of select="//lang_blocks/google-disclaimer"/></p>-->
<br/><br/>
					
					<div align="center"><span class="style28"><xsl:value-of select="//lang_blocks/content_title"/></span></div>
					<br />
					<p><xsl:value-of select="//lang_blocks/content_p1"/></p>
					
					<p><xsl:value-of select="//lang_blocks/content_p2"/></p>
					
					<p><xsl:value-of select="//lang_blocks/content_p3"/><br/><br/><a href="http://www.glocalincome.com/cgi/my_pool.cgi"><strong><xsl:value-of select="//lang_blocks/content_p3a"/></strong></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/content_p3b"/></p>
<br /><br />





[% IF myodp.me.counts.partner_count > 0 %] 
					<p class="style29" align="center"><xsl:value-of select="//lang_blocks/map_status"/></p>
					
					<table width="100%" border="1px" bgcolor="#CCCCCC">
        <tr> 
          <td style="border: 1px solid #CCC"><div align="center"><a class="tip_trigger" href="#"><span class="style28"><xsl:value-of select="//lang_blocks/youhaveaa"/> &quot; %%myodp_me_counts_member_count%% &quot; <xsl:value-of select="//lang_blocks/youhavebb"/> </span><span class="tip"><xsl:value-of select="//lang_blocks/youhavea"/><strong>&quot;%%myodp_me_counts_member_count%% &quot;</strong> <xsl:value-of select="//lang_blocks/youhaveb"/></span></a></div></td>
		  
          <td style="border: 1px solid #CCC"><div align="center"><a class="tip_trigger" href="#"><span class="style28"><xsl:value-of select="//lang_blocks/youhaveaa"/> <strong> &quot; %%myodp_me_counts_partner_count%% &quot;</strong> <xsl:value-of select="//lang_blocks/youhavec"/></span> <span class="tip"><xsl:value-of select="//lang_blocks/bullet22"/> <strong>&quot;%%myodp_me_counts_partner_count%% &quot;</strong> <xsl:value-of select="//lang_blocks/bullet2"/></span></a></div></td>
		  
          <td style="border: 1px solid #CCC"><div align="center"><a class="tip_trigger" href="https://www.glocalincome.com/cgi/appx.cgi/upg?alias=[% myodp.me.alias %]" target="_blank"><span class="style28"><xsl:value-of select="//lang_blocks/ddate1"/> <strong> &quot; %%myodp_me_deadline%% &quot;</strong> 
		  </span><span class="tip"><xsl:value-of select="//lang_blocks/bullet3a"/> <strong> &quot;[% myodp.me.days_left %]&quot; <xsl:value-of select="//lang_blocks/bullet3b"/></strong> <xsl:value-of select="//lang_blocks/bullet3c"/> <u><xsl:value-of select="//lang_blocks/bullet3d"/></u> <xsl:value-of select="//lang_blocks/bullet3e"/> <xsl:value-of select="//lang_blocks/bullet3fa"/> <strong>&quot; %%myodp_me_deadline%% &quot;</strong>, <xsl:value-of select="//lang_blocks/bullet3f"/></span></a></div></td>
        </tr>
      </table>
      [% END %] 
      
      <br/><br/>




















					
<!--IFRAME IFRAME IFRAME -->					
					<iframe src="http://www.glocalincome.com/flash/banner/gibbs-ad.html" width="650" height="300" frameborder="0" scrolling="no"><p><xsl:value-of select="//lang_blocks/fl_warning"/></p></iframe>
				
					<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
					
					
					
					<p><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133 " target="_blank"><img src="/images/icons/icon_face_book.png"/></a><xsl:text> </xsl:text> <span class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/face_book"/></span></p>

					
					
					
					
				</div><!-- END of Center Content -->
				
<!-- CONTENT ENDS -->

			</div>
		</div>
	</div>
	
	
<!-- FOOTER STARTS -->	
	<div class="container blue">		
		<div class="row "><div class="twelve columns"><div class="push"></div></div></div>	
		<div class="row ">
			<div class="twelve columns centered">			
			
				<div id="footer">
					<p>Copyright &#xA9; 1997- 2012 ClubShop Rewards, All Rights Reserved.</p>
				</div>				
			</div>
		</div>
	
	</div>	<!-- container -->
<!--End Footer Container -->	


<!--conditional-langpref-open-->
<!-- THIS IS THE LANGUAGE SELECTION POP-UP -->
<script type="text/javascript" src="/cgi/language_pref.cgi"></script>
<!-- END OF THE LANGUAGE SELECTION POP-UP -->
<!--conditional-langpref-close-->


</body>
</html>

</xsl:template>
</xsl:stylesheet>