<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:include href="/xml/head-foot.xsl"/>

<xsl:template match="/">	

<html>
<head>
<title><!--xsl:value-of select="//lang_blocks/common/t" /--></title>

<link rel="stylesheet" href="/css/global.css" type="text/css" />
<script src="/js/miniLogin.js" type="text/javascript"></script>
<style type="text/css">
tr.a {
	background-color: #EAF3FF;
}
tr.b{
	background-color: #FFFFFF;
}
table#mymain td {
	font-size: 90%;
	padding: 5px;
	padding-right: 1em;
}
table#mymain td.l {
	text-align: center;
	padding-right: 5px;
}
table#mymain a {
	font-size: 80%;
	text-decoration: none;
	color: blue;
}
table#mymain a:hover {
	text-decoration: underline;
}
hr {
	height: 1px;
	background-color:navy;
	color:navy;
	border:0;
	margin-left: 0;
	margin-right: auto;
	width:95%;
}
</style>
</head>
<body>
<xsl:call-template name="head" />
<xsl:call-template name="mall_list" />
<hr />
<div>
<xsl:choose>
<!--xsl:when test="//user/membertype = 's' and //unique_number != ''"-->
<xsl:when test="//user/membertype = 's'">

<img src="/images/memberinfo.gif" width="751" height="45" alt="" />
<table id="mymain">
<!--tr class="a"><td class="l" colspan="4" style="text-align:left"><xsl:value-of select="//lang_blocks/p7" /><xsl:text>
	</xsl:text><b><xsl:value-of select="concat(//user/firstname, ' ', //user/lastname)" /></b></td>
</tr-->
<tr class="b">
	<td><xsl:value-of select="//lang_blocks/p8" /></td>
	<td><xsl:value-of select="concat(//user/firstname, ' ', //user/lastname)" /></td>
	<td colspan="2"><xsl:choose>
		<xsl:when test="//loginck">
			<a onclick="window.open(this.href);return false;"><xsl:attribute name="href">https://www.clubshop.com/cgi/app.cgi?update=1;alias=<xsl:value-of select="//user/alias" />;o=<xsl:value-of select="//rpc/unique_number" />;password=</xsl:attribute><xsl:value-of select="//lang_blocks/a7" /></a>
		</xsl:when><xsl:otherwise>
			<a onclick="window.open(this.href);return false;" href="https://www.clubshop.com/m-update.html"><xsl:value-of select="//lang_blocks/a7" /></a>
		</xsl:otherwise></xsl:choose>
	</td>
</tr>
<tr class="a">
	<td><xsl:value-of select="//lang_blocks/p9" /></td>
	<td colspan="3"><xsl:value-of select="//user/alias" /></td>
</tr>
<tr class="b">
	<td><xsl:value-of select="//lang_blocks/p10" /></td>
	<td><xsl:value-of select="//cards/card" /></td>
	<td colspan="2"><a href="http://www.clubbucks.com/getcards.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/a8" /></a></td>
</tr>

<tr class="a">
	<!--td><xsl:value-of select="concat(//lang_blocks/p12, ' ', //rpc/ytd_year)" /></td-->
	<td><xsl:value-of select="//lang_blocks/p12" /></td>
	<td><xsl:value-of select="//rpc/ytd_points" /></td>
	<td colspan="2"></td>
</tr>

<tr class="b">
	<td><xsl:value-of select="//lang_blocks/px1" /></td>
	<td><xsl:value-of select="//rpc/redeemable_points" /></td>
	<td colspan="2"><a href="https://www.clubshop.com/cgi/reward_center.cgi" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/a9" /></a></td>
</tr>

<tr class="b">
	<td><xsl:value-of select="//lang_blocks/px3" /></td>
	<td><xsl:value-of select="//rpc/reward_points - //rpc/redeemable_points" /></td>
	<td colspan="2"><a href="/cgi/reward_points.cgi" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/a10a" /></a></td>
</tr>

<tr class="b">
	<td><xsl:value-of select="//lang_blocks/px4" /></td>
	<td><xsl:value-of select="//rpc/reward_points" /></td>
	<td colspan="2"></td>
</tr>

<tr class="a">
	<td><xsl:value-of select="//lang_blocks/px2" /></td>
	<td><xsl:value-of select="//rpc/pending_points" /></td>
	<td colspan="2"><a href="https://www.clubshop.com/cgi/member_trans_rpt.cgi" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/a10" /></a></td>
</tr>

<tr class="b">
	<td><xsl:value-of select="//lang_blocks/p13" /></td>
	<td>Clubshop Rewards Member</td>
	<td><a><xsl:attribute name="href">https://www.clubshop.com/cgi/NetShopUpgrade.cgi?alias=<xsl:value-of select="//user/alias" />;o=<xsl:if test="//loginck"><xsl:value-of select="//rpc/unique_number" /></xsl:if></xsl:attribute><xsl:value-of select="//lang_blocks/a11" /></a></td>
	<td><a><xsl:attribute name="href">
		<xsl:choose><xsl:when test="//loginck">https://www.clubshop.com/cgi/mailstatus.cgi?e=<xsl:value-of select="//user/emailaddress" />;o=<xsl:value-of select="//rpc/unique_number" /></xsl:when>
		<xsl:otherwise>/remove-me.html</xsl:otherwise></xsl:choose></xsl:attribute><xsl:value-of select="//lang_blocks/a12" />
	</a></td>
</tr>
<!--NEW-->
<tr class="a">
<td><xsl:value-of select="//lang_blocks/p14" /></td>
<td></td>
<td colspan="2"><a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//lang_blocks/ca" /></a></td>
</tr>


</table>
</xsl:when><xsl:otherwise>
	<xsl:value-of select="//lang_blocks/incorrect_mtype_msg" /><br />
	<a href="/cgi-bin/Login.cgi?destination=/sportal.xsp"><xsl:value-of select="//lang_blocks/login_here" /></a>
</xsl:otherwise></xsl:choose>
<hr />
<xsl:call-template name = "foot" />
</div>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
