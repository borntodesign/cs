<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

  <xsl:template match="/">
    
<html><head>
<title>Compensation Plan</title>

<style type="text/css">
p {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
p.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-indent: 20px;}
li{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	list-style-type: disc;
	}
li.a{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	list-style-type: upper-alpha;
	font-style: normal;
	white-space: normal;
}

h3{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #666633;
}
tr {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	line-height: normal;
	height: 7px;
	left: 3px;
}
tr.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	background-color: #F3F3DE;
}
tr.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F3F3DE;
	}

tr.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F3F3DE;
	
}
tr.d {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #FFFFFF;
	text-align: center;
}
tr.e {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #FFFFFF;
	
}
tr.execa {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #FFFFFF;
	font-weight: bold;
	
}
tr.execb {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F3F3DE;
	font-weight: bold;
	
}


td.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	background-color: #F3F3DE;
}


td.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	text-align: center;
}


td.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: center;
	background-color: #F1E4C9;
	

		
}
td.d {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: center;
	background-color: #ECFFDD;

		
}

hr{color: #999900;}

table.a { border: 1px;
cellpadding: 3px;
cellspacing: 3px;
border-color: #999900;

}

</style>

</head>
<body>

<!--start-page-->
<img src="http://www.clubshop.com/images/vipcompplan.jpg" height="93" width="758" alt="comp plan"/>
<p><xsl:value-of select="//lang_blocks/p1"/></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p2"/></p>

<h3><xsl:value-of select="//lang_blocks/p3"/></h3>
<p><xsl:value-of select="//lang_blocks/p4"/></p>

<h3><a name="pi" id="pi"><xsl:value-of select="//lang_blocks/p5"/></a></h3>
<p><xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/><a href="dp_entitlements.html" onclick="" target="_blank"><xsl:value-of select="//lang_blocks/p8"/></a></p>

<h3><a name="refcomm"><xsl:value-of select="//lang_blocks/p9"/></a></h3>
<p><xsl:value-of select="//lang_blocks/p10"/></p>


<h3><a name="npp"><xsl:value-of select="//lang_blocks/p11"/></a></h3>
<p><xsl:value-of select="//lang_blocks/p12"/></p>

<p><xsl:value-of select="//lang_blocks/p13"/></p>

<ol><li><xsl:value-of select="//lang_blocks/p14"/></li></ol>
<ol><li><xsl:value-of select="//lang_blocks/p15"/></li></ol>
<ol><li><xsl:value-of select="//lang_blocks/p16"/></li></ol>
<ol><li><xsl:value-of select="//lang_blocks/p17"/></li></ol>

<hr/>
<h3><a name="netcomm"><xsl:value-of select="//lang_blocks/p18"/></a></h3>

<p><xsl:value-of select="//lang_blocks/p19"/></p>

<p><b><a name="ppp"><xsl:value-of select="//lang_blocks/p20"/></a></b><xsl:value-of select="//lang_blocks/p21"/></p> 

<p><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:value-of select="//lang_blocks/p23"/></p>

<p><b><a name="dpp"><xsl:value-of select="//lang_blocks/p24"/></a></b><xsl:value-of select="//lang_blocks/p25"/></p>
<div align="center">
<table>
<tr><td><div align="center"><h3><xsl:value-of select="//lang_blocks/p26"/></h3></div></td>
<td rowspan="2"><img src="http://www.clubshop.com/images/thin_line.gif" alt="thin div line" width="5" height="200"/></td>
<td><div align="center"><h3><xsl:value-of select="//lang_blocks/p27"/></h3></div></td></tr>

<tr>
<td><div align="center"><img src="http://www.clubshop.com/images/dp_nwi-no.gif" width="125" height="100" alt="no network income"/></div></td>
<td><div align="right"><img src="http://www.clubshop.com/images/dp_nwi_yes.gif" width="450" height="200" alt="network income"/></div></td>
</tr>
</table>
</div>

<p><b><xsl:value-of select="//lang_blocks/p28"/></b><xsl:value-of select="//lang_blocks/p29"/></p>
<p><a name="role"><b><xsl:value-of select="//lang_blocks/p30"/></b></a><br/><xsl:value-of select="//lang_blocks/p31"/></p>

<p><xsl:value-of select="//lang_blocks/p32"/> <a href="https://www.clubshop.com/cgi/orr.cgi"><xsl:value-of select="//lang_blocks/p33"/></a><xsl:value-of select="//lang_blocks/p34"/></p>

<p><xsl:value-of select="//lang_blocks/p35"/></p>

<hr/>

<h3><xsl:value-of select="//lang_blocks/p36"/></h3>

<p><b><xsl:value-of select="//lang_blocks/p37"/></b></p>
<p><b><xsl:value-of select="//lang_blocks/p38"/></b><xsl:value-of select="//lang_blocks/p39"/></p>


<table class="a" border="1" >
<tr class="a">
<td><xsl:value-of select="//lang_blocks/p40"/></td>
<td>75</td>
<td>150</td>
<td>250</td>
<td>400</td>
<td>600</td>
<td>1,000</td>
<td>1,500</td>
<td>2,250</td>
<td>3,250</td>
<td>4,500</td>
<td>6,000</td>
<td>6,000 <font size="1">Executive Director</font></td>
</tr>
<tr class="e">
<td><a name="pay_percentage"><b><xsl:value-of select="//lang_blocks/p41"/></b></a></td>
<td class="b">34%</td>
<td class="b">36%</td>
<td class="b">39%</td>
<td class="b">42%</td>
<td class="b">46%</td>
<td class="b">49%</td>
<td class="b">52%</td>
<td class="b">56%</td>
<td class="b">59%</td>
<td class="b">61%</td>
<td class="b">65%</td>
<td class="b">70%</td>
</tr>

<tr class="a">
<td><a name="potcomm"><xsl:value-of select="//lang_blocks/p42"/></a></td>
<td class="b">$25 to $51</td>
<td class="b">$55 to $92</td>
<td class="b">$100 to $160</td>
<td class="b">$172 to $258</td>
<td class="b">$264 to $460</td>
<td class="b">$490 to $735</td>
<td class="b">$780 <br/>to<br/> $1,299</td>
<td class="b">$1,375 to $1,924</td>
<td class="b">$2,030 to $2,610</td>
<td class="b">$2,700 to $3,600</td>
<td class="b">$3,900+</td>
<td class="b">$4,200+</td>
</tr>

</table>


<p><font color="#006600"><xsl:value-of select="//lang_blocks/p43"/></font></p>

<p><b><xsl:value-of select="//lang_blocks/p44"/></b><xsl:value-of select="//lang_blocks/p45"/></p>


<blockquote><p class="a"><b><xsl:value-of select="//lang_blocks/p46"/></b><xsl:value-of select="//lang_blocks/p47"/></p>

<p><font color="#006600"><xsl:value-of select="//lang_blocks/p48"/></font> </p>

<p class="a"><b><xsl:value-of select="//lang_blocks/p50"/></b><xsl:value-of select="//lang_blocks/p49"/></p>
<p><font color="#006600"><xsl:value-of select="//lang_blocks/p51"/></font><br/> 
<b><xsl:value-of select="//lang_blocks/p52"/></b></p>



<p class="a"><b><xsl:value-of select="//lang_blocks/p53"/></b><xsl:value-of select="//lang_blocks/p54"/><a name="mgtmult"><xsl:value-of select="//lang_blocks/p55"/></a> 
<xsl:value-of select="//lang_blocks/p56"/></p>

<p><b><xsl:value-of select="//lang_blocks/p57"/></b></p>
</blockquote>

<p><xsl:value-of select="//lang_blocks/p58"/></p>


<table>
<tr>
<td class="c"><b><xsl:value-of select="//lang_blocks/p59"/></b>
<p></p>
<table border="1">
<tr class="d">
<td><b>$154.00</b></td>
<td><xsl:value-of select="//lang_blocks/p61"/> </td></tr>

<tr class="d">
<td><b><u>x___.55</u></b></td>
<td><xsl:value-of select="//lang_blocks/p62"/> </td></tr>

<tr class="d">
<td><b>   $84.70</b></td>
<td><xsl:value-of select="//lang_blocks/p63"/> </td></tr>


</table>



</td>
<td class="d"><b><xsl:value-of select="//lang_blocks/p60"/></b>
<p></p>
<table border="1">
<tr class="d">
<td><b>$154.00</b></td>
<td><xsl:value-of select="//lang_blocks/p61"/> </td></tr>

<tr class="d">
<td><b><u>x__1.00</u></b></td>
<td><xsl:value-of select="//lang_blocks/p64"/></td></tr>

<tr class="d">
<td><b>$154.00</b></td>
<td><xsl:value-of select="//lang_blocks/p63"/> </td></tr>


</table>



</td>

</tr>

</table>

<p><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:value-of select="//lang_blocks/p65"/></p>

<p></p>

<table width="100%" border="1" cellpadding="2" cellspacing="2">
        <tr class="a"> 
          <td>Organizational<br/>Pay Points</td>
          <td>75</td>
          <td>150</td>
          <td>250</td>
          <td>400</td>
          <td>600</td>
          <td>1,000</td>
          <td>1,500</td>
          <td>2,250</td>
          <td>3,250</td>
          <td>4,500</td>
          <td>6,000</td>
        </tr>
        <!--tr> 
          <td class="a">Side Volume<sup>1</sup></td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>200 PP</td>
          <td>300 PP</td>
          <td>450 PP</td>
          <td>650 PP</td>
          <td>900 PP</td>
          <td>0</td>
        </tr-->
        <tr> 
          <td class="a"><a name="pay_title">Pay Title</a><sup>1</sup></td>
          <td>Producer</td>
          <td>Team Player</td>
          <td>Team Leader</td>
          <td>Team Captain</td>
          <td>Bronze Manager</td>
          <td>Silver Manager</td>
          <td>Gold Manager</td>
          <td>Ruby Director</td>
          <td>Emerald Director</td>
          <td>Diamond Director</td>
          <td>Executive Director</td>
        </tr>
        <tr> 
          <td class="a">AP<sup>2</sup></td>
          <td>.90</td>
          <td>.70</td>
          <td>.50</td>
          <td>.40</td>
          <td>.30</td>
          <td>.25</td>
          <td>.20</td>
          <td>.15</td>
          <td>.10</td>
          <td>.05</td>
          <td>.00</td>
        </tr>
        <tr> 
          <td class="a">Passive<sup>3</sup></td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
          <td>$25</td>
        </tr>
        <tr> 
          <td class="a">Trainee I</td>
          <td>.70</td>
          <td>.60</td>
          <td>.40</td>
          <td>.25</td>
          <td>.20</td>
          <td>.15</td>
          <td>.10</td>
          <td>.05</td>
          <td>.00</td>
          <td>.00</td>
          <td>.00</td>
        </tr>
        <tr> 
          <td class="a">Trainee II</td>
          <td>.80</td>
          <td>.70</td>
          <td>.50</td>
          <td>.35</td>
          <td>.25</td>
          <td>.20</td>
          <td>.15</td>
          <td>.10</td>
          <td>.05</td>
          <td>.00</td>
          <td>.00</td>
        </tr>
        <tr> 
          <td class="a">Trainee III</td>
          <td>.90</td>
          <td>.80</td>
          <td>.60</td>
          <td>.45</td>
          <td>.30</td>
          <td>.25</td>
          <td>.20</td>
          <td>.15</td>
          <td>.10</td>
          <td>.05</td>
          <td>.00</td>
        </tr>
        <tr> 
          <td class="a">Builder Intern</td>
          <td>1.00</td>
          <td>.90</td>
          <td>.70</td>
          <td>.55</td>
          <td>.35</td>
          <td>.30</td>
          <td>.25</td>
          <td>.20</td>
          <td>.15</td>
          <td>.10</td>
          <td>.05</td>
        </tr>
        <tr> 
          <td class="a">Builder</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>.90</td>
          <td>.80</td>
          <td>.70</td>
          <td>.60</td>
          <td>.50</td>
          <td>.40</td>
        </tr>
        <tr> 
          <td class="a">Strategist</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>.85</td>
          <td>.75</td>
          <td>.65</td>
          <td>.55</td>
          <td>.45</td>
        </tr>
        <tr> 
          <td class="a">Trainer</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>.95</td>
          <td>.85</td>
          <td>.80</td>
          <td>.75</td>
        </tr>
        <tr> 
          <td class="a">Marketing Director</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>.90</td>
          <td>.85</td>
          <td>.80</td>
        </tr>
        <tr> 
          <td class="a">Promotions Director</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>1.00</td>
          <td>1.00</td>
          <td>.95</td>
          <td>.90</td>
        </tr>
        <tr> 
          <td class="a">Chief Director</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>1.00</td>
          <td>1.00</td>
        </tr>
      </table>

<p><b><sup><xsl:value-of select="//lang_blocks/p69"/></sup><xsl:value-of select="//lang_blocks/p70"/></b><xsl:value-of select="//lang_blocks/p71"/></p>
<p><b><sup><xsl:value-of select="//lang_blocks/p72"/></sup><xsl:value-of select="//lang_blocks/p73"/></b><xsl:value-of select="//lang_blocks/p74"/></p>
<p><b><sup><xsl:value-of select="//lang_blocks/p75"/></sup><xsl:value-of select="//lang_blocks/p76"/></b><xsl:value-of select="//lang_blocks/p77"/></p>


<p><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:value-of select="//lang_blocks/p78"/></p>


<h3><xsl:value-of select="//lang_blocks/p79"/></h3>
<p><xsl:value-of select="//lang_blocks/p80"/></p>
<p></p>

<table width="75%" border="1">
        <tr class="b"> 
          <td> <strong>Stars</strong></td>
          <td> <strong>Level 1</strong></td>
          <td> <strong>Level 2</strong></td>
          <td> <strong>Level 3</strong></td>
          <td> <strong>Level 4</strong></td>
        </tr>
        <tr> 
          <td> 1 Star Executive Director</td>
          <td> 7.0 %</td>
          <td> 3.0 %</td>
          <td> 1.25 %</td>
          <td> .5 %</td>
        </tr>
        <tr class="b"> 
          <td> 2 Star Executive Director</td>
          <td> 7.5 %</td>
          <td> 3.5 %</td>
          <td> 1.5 %</td>
          <td> .6 %</td>
        </tr>
        <tr> 
          <td> 3 Star Executive Director</td>
          <td> 8.0 %</td>
          <td> 4.0 %</td>
          <td> 1.75 %</td>
          <td> .7 %</td>
        </tr>
        <tr class="b"> 
          <td> 4 Star Executive Director</td>
          <td> 8.5 %</td>
          <td> 4.5 %</td>
          <td> 2.0 %</td>
          <td> .8 %</td>
        </tr>
        <tr> 
          <td> 5 Star Executive Director</td>
          <td> 9.0 %</td>
          <td> 5.0 %</td>
          <td> 2.25 %</td>
          <td> .9 %</td>
        </tr>
        <tr class="b"> 
          <td> 6 Star Ambassador's Club</td>
          <td> 9.5 %</td>
          <td> 5.5 %</td>
          <td> 2.5 %</td>
          <td> 1.0 %</td>
        </tr>
      </table>
      
      
      <h3><xsl:value-of select="//lang_blocks/p97"/></h3>
      <ul><li><xsl:value-of select="//lang_blocks/p98"/></li></ul>
      <ul><li><xsl:value-of select="//lang_blocks/p99"/></li></ul>
      <div align="center"><table border="1">
      <tr>
      <td>2 Star</td>
      <td>1,200 Organizational Pay Points</td>
      </tr>
      <tr class="b">
      <td>3 Star</td>
      <td>900 Organizational Pay Points</td>
      </tr>
      
       <tr>
      <td>4 Star</td>
      <td>600 Organizational Pay Points</td>
      </tr>
      
       <tr class="b">
      <td>5 Star</td>
      <td>300 Organizational Pay Points</td>
      </tr>
       <tr class="b">
      <td colspan="4">There are no "Side Volume" requirements for 6 Star or higher, Ambassadors' Club Members.</td>
      </tr>
       
      
      </table></div>
      <p><xsl:value-of select="//lang_blocks/p100"/></p>

<h3><xsl:value-of select="//lang_blocks/p81"/></h3>

<p><xsl:value-of select="//lang_blocks/p82"/></p>
<p><xsl:value-of select="//lang_blocks/p83"/></p>

<table width="92%" border="1" cellpadding="5">
        <tr class="a"> 
          <td>Side Volume</td>
          <td>PPM</td>
          <td>Exec Bonus Paid to 1 Star Exec</td>
          <td>Exec Bonus Rolled Up to 1st Exec Chief</td>
        </tr>
        <tr> 
          <td>1,500PP</td>
          <td>.25</td>
          <td>$112.50</td>
          <td>$337.50</td>
        </tr>
        <tr class="b"> 
          <td>2,000PP</td>
          <td>.33</td>
          <td>$150.00</td>
          <td>$300.00</td>
        </tr>
        <tr> 
          <td>3,000PP</td>
          <td>.50</td>
          <td>$225.00</td>
          <td>$225.00</td>
        </tr>
        <tr class="b"> 
          <td>4,500PP</td>
          <td>.75</td>
          <td>$337.50</td>
          <td>$112.50</td>
        </tr>
      </table>

    <p></p>
<p><b><xsl:value-of select="//lang_blocks/p84"/></b><xsl:value-of select="//lang_blocks/p85"/></p>


<h3><xsl:value-of select="//lang_blocks/p86"/></h3>
<p><xsl:value-of select="//lang_blocks/p87"/></p>
<ul><li><xsl:value-of select="//lang_blocks/p88"/></li></ul>
<ul><li><xsl:value-of select="//lang_blocks/p89"/></li></ul>
<ul><li><xsl:value-of select="//lang_blocks/p90"/></li></ul>


<p><b><xsl:value-of select="//lang_blocks/p91"/></b></p>


<ul><li><xsl:value-of select="//lang_blocks/p92"/></li></ul>
<ul><li><xsl:value-of select="//lang_blocks/p93"/></li></ul>
<ul><li><xsl:value-of select="//lang_blocks/p94"/></li></ul>
<p><xsl:value-of select="//lang_blocks/p95"/></p>

<!--end page-->
<hr/>
<div align="center">
<p><img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/></p>

<p><xsl:value-of select="//lang_blocks/p96"/></p></div>

<p align="left"><font color="#CCCCCC" size="1" face="Verdana, Arial, Helvetica, sans-serif">created 
        LY 2/8/07</font></p>
        <p align="right"><img src="/images/valid-xhtml10-blue.png" alt="validated"/></p>



</body></html>


</xsl:template>
</xsl:stylesheet>