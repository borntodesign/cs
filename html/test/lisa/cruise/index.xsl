<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<title>The DHS Club - 10th Anniversary Director's Cruise</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created June 2007 LY-->


<style type="text/css">

body {background-color: #82AAFF; color: black;}
td {border-width: 0; padding: 5px;
font: small Verdana, Arial,san-serif;
weight:bold;
}

table.ports{ width: 500px;
             margin: 20px;
             weight: bold;
             cell-padding: 5px;
}

tr.cruise {background-color: #82AAFF;
}

tr.cruise1{background-color: #EEFFD5;
}

h5 {text-indent: 20px;
    color: #003399;
}

td#banner {border-bottom: 2px solid #FFFFFF; height: 30px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	padding: 1em;
	padding-bottom: 0;
	font: small Verdana, Arial,san-serif;
	vertical-align:top;
	
}
td#main h2 {
	font: bold 105% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #000099;
	vertical-align:top;
	text-align: center;
	color: #50260D;
}
td#main p {margin: 1em 2.5em;}
td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #003399;
	background-color: #FFFFFF;
}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style>
<style type="text/css">

/* menu styles */
td#sidelinks a {
	display: block;
	margin: 10px 2px 0 0;
	padding: 0px 0px 0px 4px;
	text-decoration: none;
	font: normal 12px Verdana, Arial, sans-serif;
	color: Black;
	
}
td#sidelinks a:visited {color: #4B3E35;
}
td#sidelinks h5 {background-color: #FFFFFF; color: #996600;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial, Verdana, sans-serif;
   border-bottom: 1px solid #6F7B33;
}
td#sidelinks h4 {
	background-color: #003366;
	color: #FFFFFF;
	margin: 0 0 0 0;
	padding: 0 0  0 8px;
	font: bold  small Arial, Verdana, sans-serif;
	border-bottom: 1px solid #999999;
	background-position: left;
   }
td#sidelinks a:hover {
	background-color: #DCE7EF;
	color: #000000;
	border-right: 3px solid #000066;
	padding-right: 3px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}

</style>
</head>

<body>
<img src="/test/lisa/cruise/images/carnival.gif" width="750" height="98" alt="carnival Cruise Lines"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <div align="center"><p><b>Carnival Sensation</b></p></div>
       
      <p><img src="/test/lisa/cruise/images/group.gif" height="109" width="150" alt="group"/></p>
      <p><img src="/test/lisa/cruise/images/michelangelos.gif" height="109" width="150" alt="michelangelos"/></p>
      <p><img src="/test/lisa/cruise/images/diningroom.gif" height="109" width="150" alt="dining"/></p>
      <p><img src="/test/lisa/cruise/images/ovroom.gif" height="109" width="150" alt="ocean view room"/></p>
      <p><img src="/test/lisa/cruise/images/inroom.gif" height="109" width="150" alt="inroom"/></p>
      <p><img src="/test/lisa/cruise/images/atrium.gif" height="109" width="150" alt="atrium"/></p>
      <p><img src="/test/lisa/cruise/images/casino.gif" height="109" width="150" alt="casino"/></p>
      <p><img src="/test/lisa/cruise/images/mirage.gif" height="109" width="150" alt="mirage"/></p>
      <p><img src="/test/lisa/cruise/images/pool.gif" height="109" width="150" alt="pool"/></p>
      <p><img src="/test/lisa/cruise/images/golf.gif" height="109" width="150" alt="golf"/></p>
      <p><img src="/test/lisa/cruise/images/spa.gif" height="109" width="150" alt="spa"/></p>
      <p><img src="/test/lisa/cruise/images/beach.gif" height="109" width="150" alt="beach"/></p>
      <p><img src="/test/lisa/cruise/images/boat.gif" height="109" width="150" alt="ship"/></p>

   </td>
    <td width="613" id="main">
          <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="center"><h3><xsl:value-of select="//lang_blocks/p0"/></h3></div>
            <p><xsl:value-of select="//lang_blocks/p1"/></p>
            <p><xsl:value-of select="//lang_blocks/p2"/></p>
            
            <h5><xsl:value-of select="//lang_blocks/p3"/></h5>
            <table class="ports">
            <tr class="cruise1"><td><b><xsl:value-of select="//lang_blocks/day"/></b></td><td><b><xsl:value-of select="//lang_blocks/port"/></b></td><td><b><xsl:value-of select="//lang_blocks/arrive"/></b></td><td><b><xsl:value-of select="//lang_blocks/depart"/></b></td></tr>
            <tr class="cruise"><td><xsl:value-of select="//lang_blocks/p12"/></td><td><xsl:value-of select="//lang_blocks/p17"/></td><td>-</td><td><xsl:value-of select="//lang_blocks/p18"/></td></tr>
            <tr class="cruise1"><td><xsl:value-of select="//lang_blocks/p13"/></td><td><xsl:value-of select="//lang_blocks/p19"/></td><td><xsl:value-of select="//lang_blocks/p20"/></td><td><xsl:value-of select="//lang_blocks/p21"/></td></tr>
            <tr class="cruise"><td><xsl:value-of select="//lang_blocks/p14"/></td><td><xsl:value-of select="//lang_blocks/p22"/></td><td><xsl:value-of select="//lang_blocks/p23"/></td><td>-</td></tr>
            <tr class="cruise1"><td><xsl:value-of select="//lang_blocks/p15"/></td><td><xsl:value-of select="//lang_blocks/p24"/></td><td>-</td><td><xsl:value-of select="//lang_blocks/p25"/></td></tr>
            <tr class="cruise"><td><xsl:value-of select="//lang_blocks/p16"/></td><td><xsl:value-of select="//lang_blocks/p17"/></td><td><xsl:value-of select="//lang_blocks/p23"/></td><td>-</td></tr>
            </table>
            
            
            <h5><xsl:value-of select="//lang_blocks/p4"/></h5>
            
            <table class="ports">
            <tr class="cruise1"><td><b>Cabin Type</b></td><td><b>Cost</b></td></tr>
            <tr class="cruise"><td>Inside Cabin</td><td>$315.91 Person/Double Occupancy </td></tr>
            <tr class="cruise1"><td>Ocean View</td><td>$345.91 Person/Double Occupancy</td></tr>
            <tr class="cruise"><td>Suite</td><td>$655.91 Person/Double Occupancy </td></tr>
            </table>
            
            
             <h5><xsl:value-of select="//lang_blocks/p5"/></h5>
            
            <p><xsl:value-of select="//lang_blocks/p6"/> <b>6S4N22</b></p>
          
            <h5><img src="/test/lisa/cruise/images/deposit.gif" height="52" width="71" alt="deposit"/><xsl:value-of select="//lang_blocks/p7a"/></h5>
            <p><xsl:value-of select="//lang_blocks/p7"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p9"/></b><xsl:value-of select="//lang_blocks/p9a"/> <b>6S4N22</b></p>
            
            <h5><img src="/test/lisa/cruise/images/motorcoach.gif" height="63" width="112" alt="motorcoach"/><xsl:value-of select="//lang_blocks/p8a"/></h5>
            <p><xsl:value-of select="//lang_blocks/p8"/></p>
            
            
            
            <h5><img src="/test/lisa/cruise/images/passport.gif" height="75" width="56" alt="passport"/> <xsl:value-of select="//lang_blocks/p10a"/></h5>
            <p><xsl:value-of select="//lang_blocks/p10"/></p>
            <p><xsl:value-of select="//lang_blocks/p10b"/></p>
            <p><xsl:value-of select="//lang_blocks/p10c"/></p>
            
            
            <p><img src="/test/lisa/cruise/images/cr_sm.gif" height="37" width="197" alt="clubrewards"/><br/><xsl:value-of select="//lang_blocks/p11"/></p>
                      
            
            <h5><u><xsl:value-of select="//lang_blocks/p27"/></u></h5>
            
            <p><xsl:value-of select="//lang_blocks/p28"/></p> 
            <p><xsl:value-of select="//lang_blocks/p29"/></p> 
            <p><xsl:value-of select="//lang_blocks/p30"/></p> 
            
            
            
            <div align="right"><img src="http://www.clubshop.com/images/valid-xhtml10-blue.png" alt="passed validation"/></div>

<br/>
            <br/>
            <br/>
</td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>


