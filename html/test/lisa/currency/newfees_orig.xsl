<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Currency List</title>

<style type="text/css">
table.a {border: thin solid #999966;}
td {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: .65em;
border: 1px solid #999966;
}
tr.top{
	background-color: #FFFFCC;

}
tr.a {
	background-color: #FFFFFF;
}
tr.b {
	background-color: #F7F7F2;
}
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

</style>

</head>
<body>



<table>
<xsl:for-each select="document('pricing.xml')/pricing/item">
<tr>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="currency_code" /> -
<xsl:value-of select="currency_name" /></td>
<td><xsl:value-of select="initial_fee" /></td>
<td><xsl:value-of select="monthly_fee" /></td>
<td><xsl:value-of select="annual_renewal_fee" /></td>
</tr>
</xsl:for-each>
</table>
</body></html>


</xsl:template>

<xsl:template match="/pricing/item">
<tr><xsl:copy-of select="*" /></tr>
</xsl:template>
</xsl:stylesheet>
