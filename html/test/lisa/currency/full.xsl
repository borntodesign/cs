<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>Currency List</title>

<style type="text/css">
table.a {border: thin solid #999966;
}
table.b {text-align: center;}

td {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: .65em;
border: 1px solid #999966;
}
tr.top{
	background-color: #FFFFCC;

}
tr.a {
	background-color: #FFFFFF;
}
tr.b {
	background-color: #F7F7F2;
}
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

</style>

</head>
<body>
<table>
<div align="center"><h4><xsl:value-of select="//lang_blocks/p0"/></h4></div>
<p><b><xsl:value-of select="//lang_blocks/p1"/></b></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<tr class="b"><td colspan="2"></td>
<td colspan="3"><div align="center"><xsl:value-of select="//lang_blocks/subs_fee"/></div></td>
<td colspan="2" align="center"><xsl:value-of select="//lang_blocks/point_values"/></td>
</tr>


  <tr class="top"> 
    <td><xsl:value-of select="//lang_blocks/country"/></td>
    <td><xsl:value-of select="//lang_blocks/ccode"/></td>
    <td><xsl:value-of select="//lang_blocks/fee1"/></td>
    <td><xsl:value-of select="//lang_blocks/fee2"/></td>
    <td><xsl:value-of select="//lang_blocks/fee3"/></td>
<td>Initial</td>
<td>Monthly</td>
  </tr>


<xsl:for-each select="document('pricing.xml')/pricing/item">
<tr>
<xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">a</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>
<td><xsl:value-of select="country" /></td>
<td><xsl:value-of select="currency_code" /> -
<xsl:value-of select="currency_name" /></td>
<td><xsl:value-of select="initial_fee" /></td>
<td><xsl:value-of select="monthly_fee" /></td>
<td><xsl:value-of select="annual_renewal_fee" /></td>
<td><xsl:value-of select="initial_pp" /></td>
<td><xsl:value-of select="monthly_pp" /></td>
</tr>
</xsl:for-each>
</table>
</body></html>


</xsl:template>

<xsl:template match="/pricing/item">
<tr><xsl:copy-of select="*" /></tr>
</xsl:template>
</xsl:stylesheet>
