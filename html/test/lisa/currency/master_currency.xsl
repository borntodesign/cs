<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Currency List</title>

<style type="text/css">
table.a {border: thin solid #999966;}
td {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: .65em;
border: 1px solid #999966;
}
tr.top{
	background-color: #FFFFCC;

}
tr.a {
	background-color: #FFFFFF;
}
tr.b {
	background-color: #F7F7F2;
}
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

</style>

</head>
<body>



<table>
  <tr class="b"> 
    <td colspan="2"></td>
    <td colspan="4"><div align="center">Subscription Fees</div></td>
  </tr>
  
  
  <tr class="top"> 
    <td>Country</td>
    <td>Currency</td>
    <td>Initial</td>
    <td>Monthly</td>
    <td>Renewal</td>
    <td>Reports</td>
  </tr>
  <tr class="a"> 
    <td>US</td>
    <td>USD - United States Dollar</td>
    <td>49.95</td>
    <td>25.00</td>
    <td>24.95</td>
    <td>5.00</td>
  </tr>
  <tr class="b"> 
    <td>AD</td>
    <td>EUR - EURO</td>
    <td>32.32</td>
    <td>16.77</td>
    <td>16.14</td>
    <td>3.24</td>
  </tr>
  <tr class="a"> 
    <td>AG</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>132.37</td>
    <td>66.25</td>
    <td>66.12</td>
    <td>13.25</td>
  </tr>
  <tr class="b"> 
    <td>AI</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>132.37</td>
    <td>66.25</td>
    <td>66.12</td>
    <td>13.25</td>
  </tr>
  <tr class="a"> 
    <td>AQ</td>
    <td>NOK - Norway Kroner</td>
    <td>258.23</td>
    <td>129.25</td>
    <td>128.95</td>
    <td>25.85</td>
  </tr>
  <tr class="b"> 
    <td>BJ</td>
    <td>XOF - Communaute Financiere Africaine Francs BECAO</td>
    <td>21022.98</td>
    <td>10522.01</td>
    <td>10500.97</td>
    <td>2104.40</td>
  </tr>
</table>
  
<br/><br/>


<table>
  <tr class="b"> 
    <td colspan="2"></td>
    <td colspan="4"><div align="center">Subscription Fees</div></td>
  </tr>
  
  
  <tr class="top"> 
    <td>Country</td>
    <td>Currency</td>
    <td>Initial</td>
    <td>Monthly</td>
    
  </tr>
  <tr class="a"> 
    <td>US</td>
    <td>USD - United States Dollar</td>
    <td>49.95</td>
    <td>25.00</td>
    
  </tr>
  <tr class="b"> 
    <td>AD</td>
    <td>EUR - EURO</td>
    <td>32.32</td>
    <td>16.77</td>
    
  </tr>
  <tr class="a"> 
    <td>AG</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>132.37</td>
    <td>66.25</td>
   
  </tr>
  <tr class="b"> 
    <td>AI</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>132.37</td>
    <td>66.25</td>
    
  </tr>
  <tr class="a"> 
    <td>AQ</td>
    <td>NOK - Norway Kroner</td>
    <td>258.23</td>
    <td>129.25</td>
    
  </tr>
  <tr class="b"> 
    <td>BJ</td>
    <td>XOF - Communaute Financiere Africaine Francs BECAO</td>
    <td>21022.98</td>
    <td>10522.01</td>
   
  </tr>
</table>
  
  <br/><br/>
  
  <table>
  <tr class="b"> 
    <td colspan="2"></td>
    <td colspan="4"><div align="center">Subscription Fees</div></td>
  </tr>
  
  
  <tr class="top"> 
    <td>Country</td>
    <td>Currency</td>
    <td>Renewal</td>
    
  </tr>
  <tr class="a"> 
    <td>US</td>
    <td>USD - United States Dollar</td>
    <td>24.95</td>
    
  </tr>
  <tr class="b"> 
    <td>AD</td>
    <td>EUR - EURO</td>
    <td>16.14</td>
   
  </tr>
  <tr class="a"> 
    <td>AG</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>66.12</td>
    
  </tr>
  <tr class="b"> 
    <td>AI</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>66.12</td>
    
  </tr>
  <tr class="a"> 
    <td>AQ</td>
    <td>NOK - Norway Kroner</td>
    <td>128.95</td>
   
  </tr>
  <tr class="b"> 
    <td>BJ</td>
    <td>XOF - Communaute Financiere Africaine Francs BECAO</td>
    <td>10500.97</td>
    
  </tr>
</table>


<br/><br/>


<table>
  <tr class="b"> 
    <td colspan="2"></td>
    <td colspan="4"><div align="center">Subscription Fees</div></td>
  </tr>
  
  
  <tr class="top"> 
    <td>Country</td>
    <td>Currency</td>
    <td>Reports</td>
  </tr>
  <tr class="a"> 
    <td>US</td>
    <td>USD - United States Dollar</td>
    <td>5.00</td>
  </tr>
  <tr class="b"> 
    <td>AD</td>
    <td>EUR - EURO</td>
    <td>3.24</td>
  </tr>
  <tr class="a"> 
    <td>AG</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>13.25</td>
  </tr>
  <tr class="b"> 
    <td>AI</td>
    <td>XCD - Eastern Caribbean Dollar</td>
    <td>13.25</td>
  </tr>
  <tr class="a"> 
    <td>AQ</td>
    <td>NOK - Norway Kroner</td>
    <td>25.85</td>
  </tr>
  <tr class="b"> 
    <td>BJ</td>
    <td>XOF - Communaute Financiere Africaine Francs BECAO</td>
    <td>2104.40</td>
  </tr>
</table>
  

  




</body></html>


</xsl:template>
</xsl:stylesheet>