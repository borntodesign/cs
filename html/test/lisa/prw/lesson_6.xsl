<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="725" margin="25px">
<tr>
<td class="top">
<img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" />
</td>
</tr>
</table>
<table width="700">
<tr>
<td>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" /><br/>

<sub><xsl:value-of select="//lang_blocks/p0" /></sub>

<p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt="" /></p>                  <br />

<sub><xsl:value-of select="//lang_blocks/head" /></sub>


<p><xsl:value-of select="//lang_blocks/p1" /></p>

<sub><xsl:value-of select="//lang_blocks/p17" /></sub>
<p><xsl:value-of select="//lang_blocks/p18" /></p>

<sub><xsl:value-of select="//lang_blocks/p19" /></sub>

<p><xsl:value-of select="//lang_blocks/p20" /><a href="http://www.clubshop.com/manual/policies/spam.xml" target="_blank">http://www.clubshop.com/manual/policies/spam.xml </a> </p>

<sub><xsl:value-of select="//lang_blocks/p21" /></sub>
<p><xsl:value-of select="//lang_blocks/p22" /><a href="/vip/control/index.shtml" target="_blank"><xsl:value-of select="//lang_blocks/p23" /></a></p>





<!--p><xsl:value-of select="//lang_blocks/p2" /> (<xsl:value-of select="//lang_blocks/p3" />)
<xsl:value-of select="//lang_blocks/p4" /><a href="http://www.clubshop.com/manual/policies/spam.html">http://www.clubshop.com/manual/policies/spam.html</a></p-->



<!--p><xsl:value-of select="//lang_blocks/p5" /> " <xsl:value-of select="//lang_blocks/p6" /> " <xsl:value-of select="//lang_blocks/p7" /></p-->

<!--p><xsl:value-of select="//lang_blocks/p8" /> " <xsl:value-of select="//lang_blocks/p9" />" <xsl:value-of select="//lang_blocks/p10" />
" <xsl:value-of select="//lang_blocks/p11" />" <xsl:value-of select="//lang_blocks/p12" />
<a href="https://www.clubshop.com/vip/control/index.shtml"><xsl:value-of select="//lang_blocks/p13" /></a></p-->


<table width="500" border="1" border-color="#3F4164" align="center" cellpadding="5">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
<tr><td><b><xsl:value-of select="//lang_blocks/p14" /></b> <a href="http://www.clubshop.com/vip/ebusiness/prw.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/prw.xml</a>
<p><xsl:value-of select="//lang_blocks/p24" />
</p>
</td></tr>
</table>











</td>
</tr>
</table>

<hr />

<div align="center">
<p>
<a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" /></a>

               | 
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_7.xml">
                  <xsl:value-of select="//lang_blocks/lesson" />
               </a>
               </p>
            </div>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>