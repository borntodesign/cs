<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:include href="common.xsl" />

   <xsl:param name="id" />

   <xsl:param name="dn" />

   <xsl:template match="/">
      <html>
         <head>
            <title>Fundraising Program</title>

            <link href="presentation.css" rel="stylesheet" type="text/css" />

            <link href="local.css" rel="stylesheet" type="text/css" />
         </head>

         <body>
<xsl:call-template name="header" />

<table>
<tr>
<td id="sidelinks" valign="top">
<xsl:call-template name="nav" /></td>

<td width="581" id="main">
<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>

<table class="benefits" border="1">
<tr class="e"><td><xsl:value-of select="//lang_blocks/p5" /></td><td><xsl:value-of select="//lang_blocks/p6" /></td></tr>                  
<tr class="c"><td>75</td><td>34%</td></tr>   
<tr class="c"><td>150</td><td>36%</td></tr> 
<tr class="c"><td>250</td><td>39%</td></tr> 
<tr class="c"><td>400</td><td>42%</td></tr> 
<tr class="c"><td>600</td><td>46%</td></tr> 
<tr class="c"><td>1,000</td><td>49%</td></tr> 
<tr class="c"><td>1,500</td><td>52%</td></tr> 
<tr class="c"><td>2,250</td><td>56%</td></tr> 
<tr class="c"><td>3,250</td><td>59%</td></tr> 
<tr class="c"><td>4,500</td><td>61%</td></tr> 
<tr class="c"><td>6,000</td><td>65%</td></tr>                
</table>
                     
                     
<p><xsl:value-of select="//lang_blocks/p7" /></p> 
<p><b><xsl:value-of select="//lang_blocks/p8" /></b><xsl:value-of select="//lang_blocks/p9" /></p>                
<p align="center"><img src="http://www.clubshop.com/affinity/images/7500pp.gif" height="100" width="300" alt="7500 pp"/></p>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     

                     <p>
                        <xsl:value-of select="//lang_blocks/p1" />
                     </p>

                     <p>
                        <b>
                           <xsl:value-of select="//lang_blocks/p2" />
                        </b>

                        <xsl:value-of select="//lang_blocks/p3" />
                     </p>

                     <br />

                     <table class="y">
                        <tr>
                           <td>
                              <b>
                                 <xsl:value-of select="//lang_blocks/p5" />
                              </b>

                              <xsl:value-of select="//lang_blocks/p6" />
                           </td>
                        </tr>

                        <tr>
                           <td>
                              <img src="http://www.clubshop.com/images/av_referralcom.gif" alt="referral commission" width="450" height="100" />
                           </td>
                        </tr>
                     </table>

                     <p>
                        <xsl:value-of select="//lang_blocks/p9" />
                     </p>

                     <p>
                        <b>
                        <xsl:value-of select="//lang_blocks/p11" />

                        :</b>

                        <xsl:value-of select="//lang_blocks/p12" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p13" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p15" />
                     </p>

                     <p>
                     <b>
                        <xsl:value-of select="//lang_blocks/p16" />
                     </b>

                     - 
                     <xsl:value-of select="//lang_blocks/p17" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p18" />
                     </p>

                     <table cellpadding="5" border="1" align="center">
                        <tr class="c">
                           <td>
                              <b>
                                 <xsl:value-of select="//lang_blocks/chart1" />
                              </b>
                           </td>

                           <td>
                              <b>
                                 <xsl:value-of select="//lang_blocks/chart1a" />
                              </b>
                           </td>

                           
                        </tr>

                        <tr class="c">
                           <td>75</td>

                           <td>34%</td>
                        </tr>

                        <tr class="c">
                           <td>150</td>

                           <td>37%</td>
                        </tr>

                        <tr class="c">
                           <td>250</td>

                           <td>40%</td>
                        </tr>

                        <tr class="c">
                           <td>400</td>

                           <td>43%</td>
                        </tr>

                        <tr class="c">
                           <td>600</td>

                           <td>46%</td>
                        </tr>

                        <tr class="c">
                           <td>1,000</td>

                           <td>49%</td>
                        </tr>

                        <tr class="c">
                           <td>1,500</td>

                           <td>52%</td>
                        </tr>

                        <tr class="c">
                           <td>2,250</td>

                           <td>55%</td>
                        </tr>

                        <tr class="c">
                           <td>3,250</td>

                           <td>58%</td>
                        </tr>

                        <tr class="c">
                           <td>4,500</td>

                           <td>60%</td>
                        </tr>

                        <tr class="c">
                           <td>6,000</td>

                           <td>70%</td>
                        </tr>
                     </table>

                     <p>
                        <xsl:value-of select="//lang_blocks/p22" />
                     </p>

                     <p>
                        <b>
                           <xsl:value-of select="//lang_blocks/p26" />
                        </b>

                        <xsl:value-of select="//lang_blocks/p26a" />
                     </p>

                     <p>
                        <div align="center">
                           <img src="http://www.clubshop.com/images/6000npp.gif" />
                        </div>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p27" />
                     </p>

                     <p>
                        <b>
                           <i>
                              <xsl:value-of select="//lang_blocks/p28" />
                           </i>
                        </b>
                     </p>

                     <h1 align="center">
                        <xsl:value-of select="//lang_blocks/p83" />
                     </h1>

                     <p align="center">
                        <a>
                           <xsl:attribute name="href">
                           <xsl:value-of select="document('nav.xml')//teams/a/@href" />

                           ?flg=dv&amp;sponsorID=
                           <xsl:choose>
                              <xsl:when test="$id">
                                 <xsl:value-of select="$id" />
                              </xsl:when>

                              <xsl:otherwise>1</xsl:otherwise>
                           </xsl:choose>
                           </xsl:attribute>

                           <img src="http://www.clubshop.com/ca/ca_logo_250x37.gif" />
                        </a>
                     </p>

                     
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

