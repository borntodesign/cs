<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Contact Information</title>

<link href="club-rewards-general.css" rel="stylesheet" type="text/css" />

<style type="text/css">
<!--body {background-color: #003399;
}
table{background-color: #FFFFFF;
}
td{font-family: Verdana, Arial, sans-serif;
font-size: small;
}
hr{color: #003399;}
ul {display: inline-block;}
-->
li {list-style: url(http://www.clubshop.com/images/icons/icon_checkmark.png);}

</style>

</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0"><tr>
          <td colspan="12" valign="top"> <!--<img src="/images/headers/products-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" />-->

            <div style="background-image: url(/images/headers/header_clubshop_contact.png); background-repeat: no-repeat; width: 1000px; height:138px; align:left;"> 
            </div></td>
        </tr>
<tr><td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>

<td colspan="8" bgcolor="#FFFFFF">
<div align="center" class="style12">

<table width="600" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">
              
                <tr>
				<td colspan="2" align="center" valign="top" style="background-image:url(/images/bg/bg_header-orange-923x23.png)" bgcolor="#FCA641" class="Text_Body_Content"><span class="style13"><xsl:value-of select="//lang_blocks/p0"/></span><br />                    </td>
				</tr>
                <tr>
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content">
            <ul>
                
 <table width="100%" border="0" cellspacing="0" cellpadding="2">
   <tr>   <td colspan="2"><li><span class="style14"><xsl:value-of select="//lang_blocks/p6"/></span>
                <xsl:text> </xsl:text><br/><xsl:text> </xsl:text><span class="style2"> ClubShop.com <br/>
                2560 Placida Road <br/> Englewood, FL 34224-5412 USA</span></li></td>
          </tr>
          <tr>
     <td colspan="2"><li><span class="style14"><xsl:value-of select="//lang_blocks/p7"/></span> <xsl:text> </xsl:text>
            <span class="style2"><xsl:value-of select="//lang_blocks/p8"/></span></li></td>
          </tr>
          <tr>
         <td colspan="2"><li><span class="style14"><xsl:value-of select="//lang_blocks/p9"/> 
                </span> <xsl:text> </xsl:text><span class="style2"> 941-475-3435</span></li></td>
          </tr>
          <tr>
       <td colspan="2"><li><span class="style14"><xsl:value-of select="//lang_blocks/p10"/> 
                </span> <xsl:text> </xsl:text><span class="style2"> 941-475-4081</span></li></td>
          </tr>
          
          <tr> 
            <td colspan="2" align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
           
          </tr>
          
          
          
          <tr> 
            <td colspan="2" align="center" valign="top" style="background-image:url(/images/bg/bg_header-orange-923x23.png)" bgcolor="#FCA641" class="Text_Body_Content"><span class="style13"><xsl:value-of select="//lang_blocks/p11"/></span></td>
          </tr>
          
          <tr> 
            <td colspan="2"><li><span class="Text_Content"><a href="/mall/" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p12"/></a></span></li></td>
          </tr>
          
          <tr> 
            <td colspan="2"><li><span class="Text_Content"><a href="/rewardsdirectory/" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p13"/></a></span></li></td>
          </tr>
          <tr> 
            <td colspan="2"><li><span class="Text_Content"><a href="/idlookup.html" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p14"/></a></span></li></td>
          </tr>
          <tr> 
            <td colspan="2"><li><span class="Text_Content"><a href="/remove-me.xml" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p15"/></a></span></li></td>
          </tr>
          <tr> 
            <td colspan="2"><li><span class="Text_Content"><a href="/mall/missing_order.xml" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p17"/></a></span></li></td>
          </tr>
          <tr> 
            <td colspan="2"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
           
          </tr>
          
          
          <tr> 
            <td colspan="2" align="center" valign="top" style="background-image:url(/images/bg/bg_header-orange-923x23.png)" bgcolor="#FCA641" class="Text_Body_Content"><span class="style13"><xsl:value-of select="//lang_blocks/p18"/></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p1"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:mallsales@clubshop.com?Subject=Mall_Question" class="nav_blue_line">mallsales@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p2"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:support1@clubshop.com?Subject=Mall_Question" class="nav_blue_line">support1@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p3"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:support.it@clubshop.com?Subject=Mall_Question" class="nav_blue_line">support.it@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p4"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:support.fr@clubshop.com?Subject=Mall_Question" class="nav_blue_line">support.fr@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p5"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:support.nl@clubshop.com?Subject=Mall_Question" class="nav_blue_line">support.nl@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td><li><span class="Text_Content"><xsl:value-of select="//lang_blocks/p16"/></span></li></td>
            <td><span class="Text_Content"><a href="mailto:acf1@clubshop.com?Subject=Mall_Question" class="nav_blue_line">acf1@clubshop.com</a></span></td>
          </tr>
          
          <tr> 
            <td colspan="2"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
          </tr>
 
          
        </table>
        </ul>
      
        
        </td>
                </tr>
             
            </table>





            </div>
</td>
<td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td></tr>
<tr><td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
<tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>

<tr><td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
<td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
</tr></table>

</td></tr></table>

</body>
</html>
</xsl:template>
</xsl:stylesheet>