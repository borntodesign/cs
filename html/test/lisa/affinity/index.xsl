<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>


<title><xsl:value-of select="//lang_blocks/p0"/></title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />


</head>
<body>

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="12" valign="top">
<img src="http://www.clubshop.com/images/headers/nonprofit-clubshop-1000x138.png" width="1000" height="138" alt="Affinity Group" /></td>
</tr>


<tr>
<td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>
<td colspan="8" bgcolor="#FFFFFF">



<main_content>
<table><tr><td valign="top">
<img src="/images/hands.gif" height="121" width="115">
<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/title" /></xsl:attribute></img></td>
<td><h2><xsl:value-of select="//lang_blocks/h3" /></h2>
<br/><xsl:value-of select="//lang_blocks/raising" /></td>
</tr></table>













<h2><xsl:value-of select="//lang_blocks/h4"/></h2>
<p><xsl:value-of select="//lang_blocks/pjun10"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10a"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10b"/></p>
<p><xsl:value-of select="//lang_blocks/pjun10c"/> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/pjun10d" /></b></p>

<p><xsl:value-of select="//lang_blocks/pjun10e" /> <xsl:text> </xsl:text><a href="http://www.clubshop.com/" target="_blank">www.clubshop.com</a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/pjun10f" /> </p>

<p><xsl:value-of select="//lang_blocks/pjun10g" /></p>

<h2><xsl:value-of select="//lang_blocks/pjun10h" /></h2>

<p><xsl:value-of select="//lang_blocks/pjun10i" /></p>
<p><xsl:value-of select="//lang_blocks/pjun10j" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/pjun10k" /></b></p>

<p><xsl:value-of select="//lang_blocks/pjun10l" /></p>

<p><xsl:value-of select="//lang_blocks/pjun10m" /></p>

<p><xsl:value-of select="//lang_blocks/pjun10n" /></p>

<!--<ol>
<xsl:for-each select = "//lang_blocks/l1/*">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ol>-->

<!--<h2><xsl:value-of select="//lang_blocks/h5" /></h2>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2-1" /><b> &quot;<xsl:value-of select="//lang_blocks/p2-2" />&quot; </b>
<xsl:value-of select="//lang_blocks/p2-3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p3" /></b></p>-->
</main_content>



</td>
 <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
 </tr>
 <tr> <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
 <tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>
 <tr>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 </tr></table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>