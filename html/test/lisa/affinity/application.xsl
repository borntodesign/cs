<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
   doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
   omit-xml-declaration="yes"
   media-type="text/html"/>






  <xsl:template match="/">
<html><head>
           <title>Affinity Group Application</title>
<script type="text/javascript" src="application_files/get_cookie_by_name.js"></script>

<style type="text/css">
body {background-color: #003399;
}
table{background-color: #FFFFFF;
}
td{font-family: Verdana, Arial, sans-serif;
font-size: small;
}


</style>

</head>
<body onload="document.forms[0].referral.value=get_cookie_by_name('refspid')">

<table width="100%" cellpadding="5">
  <tr> 
    <td width="59%"><img src="/affinity/images/head_portal5.gif" alt="ClubShop Affinity Group Application" width="442" height="69"/></td>
    <td width="41%"><h3><font face="Arial, Helvetica, sans-serif"><u><xsl:value-of select="//lang_blocks/p1"/></u></font></h3></td>
  </tr>

<tr>
<td colspan="2"><form action="/cgi/FormMail.cgi" method="post">

        <table width="98%" border="0" cellpadding="5">
         <tr> 
            <td height="383">
                <input name="recipient" value="support1@dhs-club.com" type="hidden"/>
				<input name="email" value="apache@clubshop.com" type="hidden"/>
                <input name="required" value="agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,agemailaddress,referral,password,agreement" type="hidden"/>
                <input name="sort" value="order:agname,firstname,lastname,emailaddress,address,city,state,postal,country,phone,cell,skype,fax,agemailaddress,url,referral,password,agreement" type="hidden"/>
                <input name="redirect" value="http://www.clubshop.com/affinity/thankyou.html" type="hidden"/>
                <input name="subject" value="Affinity Group Application" type="hidden"/>

                <input name="env_report" value="HTTP_USER_AGENT" type="hidden"/>
              
              
              
              <table width="100%" border="1" cellpadding="5">
                <tr> 
                  <td width="40%"><xsl:value-of select="//lang_blocks/p2"/></td>
                  <td width="60%"> <input name="agname" id="agname" size="50" type="text"/> 
                  </td>
                </tr>

                <tr> 
                  <td height="34"> <p><xsl:value-of select="//lang_blocks/p3"/></p></td>
                  <td> <input name="firstname" id="firstname" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p4"/></td>
                  <td><input name="lastname" id="lastname" size="50" type="text"/> 
                  </td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p5"/></td>
                  <td><input name="emailaddress" id="emailaddress" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td colspan="2"><div align="center"><b><xsl:value-of select="//lang_blocks/p22"/></b></div></td>
                </tr>

                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p6"/></td>
                  <td> <input name="address" id="address" size="50" type="text"/> 
                  </td>
                </tr>
                <tr> 
                  <td height="24"><xsl:value-of select="//lang_blocks/p7"/></td>
                  <td> <input name="city" id="city" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p8"/></td>
                  <td> <input name="state" id="state" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p9"/></td>
                  <td> <input name="postal" id="postal" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p10"/></td>
                  <td><input name="country" id="country" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p11"/></td>
                  <td><input name="phone" id="phone" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p12"/></td>
                  <td><input name="cell" id="cell" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p13"/></td>
                  <td><input name="skype" id="skype" size="50" type="text"/></td>

                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p14"/></td>
                  <td><input name="fax" id="fax" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p15"/></td>
                  <td><input name="agemailaddress" id="agemailaddress" size="50" type="text"/></td>
                </tr>

                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p16"/></td>
                  <td><input name="url" id="url" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p17"/></td>
                  <td><input name="referral" id="referral" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p18"/></td>
                  <td><input name="password" id="password" size="50" type="text"/></td>
                </tr>
                <tr> 
                  <td><p><xsl:value-of select="//lang_blocks/p19"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/business/forms/affinity_terms.html" target="_blank" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p20"/></a> </p>
                    <p>(<a href="http://www.clubshop.com/privacy.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a>)</p></td>
                  <td> <input name="agreement" value="Yes, I have read and agree to the Affinity Group Terms of Agreement" id="agreement" type="checkbox"/></td>

                </tr>
              </table>
                    <div align="center">
                      <input type="submit" name="Submit" value="Submit"/>
                      <input type="reset" name="Reset" value="Reset"/>
                    </div>
                    </td></tr></table>
            </form>

</td></tr></table><!--end Main Section-->



</body></html>


</xsl:template>
</xsl:stylesheet>