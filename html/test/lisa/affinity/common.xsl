<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="header">
<div style="background-image: url(/images/autoviphead.gif); background-repeat:no-repeat; width:733px; height:86px">
<xsl:if test="$dn">
<img src="/blanklin.gif" height="19" width="1" alt="" />
<div style="font-style:italic; text-align:right; font-family:serif; font-size: 90%; color: #339; padding-right: 0.5em;">
<span style="font-family: Arial, Helvetica, sans-serif; font-size: 90%;">
<xsl:value-of select="document('nav.xml')//teams/brought" /></span>
<xsl:text> </xsl:text><xsl:value-of select="$dn" />
</div>
</xsl:if>
</div>


</xsl:template>


<xsl:template name="nav">
<xsl:for-each select = "document('nav.xml')//teams/leader/*">
	<a><xsl:attribute name="href"><xsl:value-of select="@href" />?id=<xsl:call-template name="getid" /><xsl:call-template name="getdn" />
	</xsl:attribute>
<xsl:value-of select="." /></a><br />
</xsl:for-each>

<a>
   <xsl:attribute name="href"><xsl:value-of select="document('nav.xml')//teams/a/@href" />?flg=dv&amp;sponsorID=<xsl:call-template name="getid" />
   </xsl:attribute>
   <xsl:value-of select="document('nav.xml')//teams/a" /></a>
</xsl:template>

<xsl:template name="getid">
	<xsl:choose>
	<xsl:when test="$id"><xsl:value-of select="$id" /></xsl:when>
	<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="getdn">
	<xsl:if test="$dn">&amp;dn=<xsl:value-of select="$dn" /></xsl:if>
</xsl:template>

</xsl:stylesheet>
