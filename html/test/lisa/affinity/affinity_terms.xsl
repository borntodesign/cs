<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
   doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
   omit-xml-declaration="yes"
   media-type="text/html"/>

  <xsl:template match="/">
           <html><head>
           <title>Affinity Group Terms of Agreement</title>

<style type="text/css">
body {background-color: #FFFFFF;
}
table{border-color: #336699;
}

p {font-family: Verdana, Arial, sans-serif;
font-size: small;
}
h3{color: #336699;
text-align: center;}

</style>

</head>
<body>
<table><tr><td>

<img src="/manual/images/head_portal5.gif" height="69" width="442" alt="ClubShop"/>

<h3><xsl:value-of select="//lang_blocks/p1"/></h3>
<p><b><xsl:value-of select="//lang_blocks/p2"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></p>

<p><b><xsl:value-of select="//lang_blocks/p4"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5"/></p>
<p><b><xsl:value-of select="//lang_blocks/p6"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/></p>
<p><b><xsl:value-of select="//lang_blocks/p8"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9"/></p>
<p><b><xsl:value-of select="//lang_blocks/p10"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11"/></p>

<p><b><xsl:value-of select="//lang_blocks/p12"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></p>

<p><b><xsl:value-of select="//lang_blocks/p14"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/></p>

<p><b><xsl:value-of select="//lang_blocks/p16"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17"/></p>
<p><b><xsl:value-of select="//lang_blocks/p18"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19"/></p>
<p><b><xsl:value-of select="//lang_blocks/p20"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p21"/></p>
<p><b><xsl:value-of select="//lang_blocks/p22"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23"/></p>
<p><b><xsl:value-of select="//lang_blocks/p24"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p25"/></p>

<p><b><xsl:value-of select="//lang_blocks/p26"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></p>
<p><xsl:value-of select="//lang_blocks/p28"/></p>

</td></tr></table>
<br/>
<br/>
<div align="center">
<a href="/affinity/application.xml" target="_blank"><xsl:value-of select="//lang_blocks/p29"/></a>


</div>


</body></html>


</xsl:template>
</xsl:stylesheet> 