<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Spotlight</title>

<style type="text/css">
table.1{
	width:100%;
	border:1px;
	cellpadding: 5px;
	bordercolor:#666666;
	bgcolor:#6699CC;
	padding: 5px;
}
td{
	font-family: Verdana, Arial, sans-serif;
	font-size: 12px;
	background-color: #FFFFFF;
	width: 25%;
	padding-top: 5px;
}
 a{
	text-decoration: none;
}

a:visited {color: Green;
}
hr{ color:#FFFFFF;}

</style>

</head>
<body background="/test/lisa/spotlight/images/blpinstripes.gif">

<table width="100%" cellpadding="5">
  <tr> 
    <td><h1 align="center"><xsl:value-of select="//lang_blocks/lead_head"/></h1></td>
    <td><div align="center"><img src="/test/lisa/spotlight/images/tibaldi.gif" width="151" height="124" alt="Franco Tibaldi"/></div></td>
    <td><h2 align="center"><br/><xsl:value-of select="//lang_blocks/spot_name"/><br/>
    <xsl:value-of select="//lang_blocks/spot_location"/>
        </h2><br/><div align="center"><a href="http://www.clubshop.com/spotlight/tibaldi.xml" target="_blank">Read</a></div></td>
  </tr>
</table>
<hr/>
<table width="100%" class="1">

<tr>
<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_descamps.gif" width="92" height="85" alt="Gino Descamps"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/descamps.xml" target="_blank"><xsl:value-of select="//lang_blocks/descamps"/></a> <br/>
          <xsl:value-of select="//lang_blocks/descamps_a"/></div></td>
          
          
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_landerwyn.gif" width="92" height="85" alt="Benedict Landerwyn"/><br/>
         <a class="b" href="http://www.clubshop.com/spotlight/landerwyn.xml" target="_blank"><xsl:value-of select="//lang_blocks/landerwyn"/></a> <br/>
         <xsl:value-of select="//lang_blocks/landerwyn_a"/></div></td>

<td> <div align="center"> <img src="/test/lisa/spotlight/images/slide_verdickt.gif" width="92" height="85" alt="Manuela Verdickt"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/verdickt.xml" target="_blank"><xsl:value-of select="//lang_blocks/verdickt"/></a> <br/>
          <xsl:value-of select="//lang_blocks/verdickt_a"/></div></td>

         
         
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_layton.gif" width="92" height="85" alt="Mary-Ann Layton"/><br/>
         <a class="b" href="http://www.clubshop.com/spotlight/layton.xml" target="_blank"><xsl:value-of select="//lang_blocks/layton"/></a> <br/>
         <xsl:value-of select="//lang_blocks/layton_a"/></div></td>
</tr>
<tr>
          
    <td><div align="center"> <img src="/test/lisa/spotlight/images/slide_vassilikos.gif" width="92" height="85" alt="Margaritis Vassilikos"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/vassilikos.xml" target="_blank"><xsl:value-of select="//lang_blocks/vassilikos"/></a> <br/>
          <xsl:value-of select="//lang_blocks/vassilikos_a"/></div></td>

          
<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_francavilla.gif" width="92" height="85" alt="Giuseppe Francavilla"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/francavilla.xml" target="_blank"><xsl:value-of select="//lang_blocks/francavilla"/></a> <br/>
          <xsl:value-of select="//lang_blocks/francavilla_a"/></div></td>
          
          
<td><div align="center"> 
          <img src="/test/lisa/spotlight/images/slide_perotti.gif" width="92" height="85" alt="Fabrizio Perotti"/><br/>
          <a class="b" href="http://www.clubshop.com/test/lisa/spotlight/new-perotti_.xml" target="_blank"><xsl:value-of select="//lang_blocks/perotti"/></a> <br/>
          <xsl:value-of select="//lang_blocks/perotti_a"/></div></td>

          

<td> <div align="center"> <img src="/test/lisa/spotlight/images/slide_grootzwaaftink.gif" width="92" height="85" alt="Wim Groot-Zwaaftink"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/groot_zwaaftink.xml" target="_blank"><xsl:value-of select="//lang_blocks/wim"/></a> <br/>
          <xsl:value-of select="//lang_blocks/wim_a"/></div></td>  
</tr> 
<tr>       
<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_vanhoute.gif" width="92" height="85" alt="Rens van Houte"/><br/>
         <a class="b" href="http://www.clubshop.com/spotlight/van_houte.xml" target="_blank"><xsl:value-of select="//lang_blocks/rens"/></a> <br/>
        <xsl:value-of select="//lang_blocks/rens_a"/></div></td>  
       
   
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_wolff.gif" width="92" height="85" alt="Gerrit Wolff"/><br/>
         <a class="b" href="http://www.clubshop.com/spotlight/wolff.xml" target="_blank"><xsl:value-of select="//lang_blocks/wolff"/></a> <br/>
         <xsl:value-of select="//lang_blocks/wolff_a"/></div></td>
   
<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_barriga.gif" width="92" height="85" alt="Dindo Barriga"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/barriga.xml" target="_blank"><xsl:value-of select="//lang_blocks/barriga"/></a> <br/>
          <xsl:value-of select="//lang_blocks/barriga_a"/></div></td> 
   
   
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_taglinao.gif" width="92" height="85" alt="Wilmar Taglinao"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/taglinao.xml" target="_blank"><xsl:value-of select="//lang_blocks/taglinao"/></a> <br/>
        <xsl:value-of select="//lang_blocks/taglinao_a"/></div></td>
   
</tr>   
<tr>


<td><div align="center"><img src="/test/lisa/spotlight/images/slide_gustafsson.gif" width="92" height="85" alt="Peter Gustafsson"/><br/>
         <a class="b" href="http://www.clubshop.com/spotlight/gustafsson.xml" target="_blank"><xsl:value-of select="//lang_blocks/gustafsson"/></a> <br/>
         <xsl:value-of select="//lang_blocks/gustafsson_a"/></div></td> 
         
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_finch.gif" width="92" height="85" alt="Frank Finch"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/finch.xml" target="_blank"><xsl:value-of select="//lang_blocks/finch"/></a> <br/>
          <xsl:value-of select="//lang_blocks/finch_a"/></div></td> 
          
          
          
                  
          
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_dufault.gif" width="92" height="85" alt="Roland Dufault"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/dufault.xml" target="_blank"><xsl:value-of select="//lang_blocks/dufault"/></a> <br/>
          <xsl:value-of select="//lang_blocks/dufault_a"/></div></td>
          
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_cunningham.gif" width="92" height="85" alt="Scott Cunningham"/><br/>
           <a class="b" href="http://www.clubshop.com/spotlight/cunningham.xml" target="_blank">  
          <xsl:value-of select="//lang_blocks/cunningham"/></a> <br/>
          <xsl:value-of select="//lang_blocks/cunningham_a"/></div></td>
   
</tr>  

<tr>
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_kennedy.gif" width="92" height="85" alt="Beverly Kennedy"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/kennedy.xml" target="_blank"><xsl:value-of select="//lang_blocks/kennedy"/> </a><br/>
          <xsl:value-of select="//lang_blocks/kennedy_a"/></div></td>   

          
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_marrow.gif" width="92" height="85" alt="Lee Marrow"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/marrow.xml" target="_blank"><xsl:value-of select="//lang_blocks/marrow"/></a> <br/>
          <xsl:value-of select="//lang_blocks/marrow_a"/></div></td>
    
<td> <div align="center"> <img src="/test/lisa/spotlight/images/slide_secord.gif" width="92" height="85" alt="Scott Secord"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/secord.xml" target="_blank"><xsl:value-of select="//lang_blocks/secord"/></a> <br/>
          <xsl:value-of select="//lang_blocks/secord_a"/></div></td>
          
          

<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_griffin.gif" width="92" height="85" alt="Paula Griffin"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/griffin.xml" target="_blank"><xsl:value-of select="//lang_blocks/griffin"/></a> <br/>
          <xsl:value-of select="//lang_blocks/griffin_a"/></div></td>


</tr>
<tr>

<td><div align="center"> 
          <img src="/test/lisa/spotlight/images/slide_braccos.gif" alt="Robbie &amp; Phil Bracco" width="92" height="85" border="0"/> <br/>
          <a class="b" href="http://www.clubshop.com/spotlight/bracco.xml" target="_blank"><xsl:value-of select="//lang_blocks/braccos"/></a> <br/>
          <xsl:value-of select="//lang_blocks/braccos_a"/></div></td>
          
<td> <div align="center"> <img src="/test/lisa/spotlight/images/slide_fagaldes.gif" width="92" height="85" alt="Kate and John Fagalde"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/fagalde.xml" target="_blank"><xsl:value-of select="//lang_blocks/fagaldes"/></a> <br/>
          <xsl:value-of select="//lang_blocks/fagaldes_a"/></div></td> 
          
<td><div align="center"> <img src="/test/lisa/spotlight/images/slide_south.gif" width="92" height="85" alt="Mark South"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/south.xml" target="_blank"><xsl:value-of select="//lang_blocks/south"/></a> <br/>
          <xsl:value-of select="//lang_blocks/south_a"/></div></td>                
          

<td><div align="center"><img src="/test/lisa/spotlight/images/slide_barbour.gif" width="92" height="85" alt="Cynthia Barbour"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/barbour.xml" target="_blank"><xsl:value-of select="//lang_blocks/barbour"/></a> <br/>
          <xsl:value-of select="//lang_blocks/barbour_a"/></div></td>        


</tr>

<tr>  

<td><div align="center"><img src="/test/lisa/spotlight/images/slide_trullinger.gif" width="92" height="85" alt="Gwenlynn Trullinger"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/trullinger.xml" target="_blank"><xsl:value-of select="//lang_blocks/trullinger"/></a> <br/>
        <xsl:value-of select="//lang_blocks/trullinger_a"/></div></td> 
        
        
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_lwalker.gif" width="92" height="85" alt="Linda Walker"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/l_walker.xml" target="_blank"><xsl:value-of select="//lang_blocks/walker"/></a> <br/>
          <xsl:value-of select="//lang_blocks/walker_a"/></div></td>
          
        
  
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_royaards.gif" width="92" height="85" alt="The Royaards"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/royaards.xml" target="_blank"><xsl:value-of select="//lang_blocks/royaards"/></a> <br/>
          <xsl:value-of select="//lang_blocks/royaards_a"/></div></td>   
    


<td> <div align="center"> <img src="/test/lisa/spotlight/images/slide_drawdy.gif" width="92" height="85" alt="Joe Drawdy"/><br/>
          <a class="b" href="http://www.clubshop.com/spotlight/drawdy.xml" target="_blank"><xsl:value-of select="//lang_blocks/drawdy"/></a> <br/>
          <xsl:value-of select="//lang_blocks/drawdy_a"/></div></td>
          
          
  </tr>
  <tr>

<td><div align="center"><img src="/test/lisa/spotlight/images/slide_hh.gif" width="92" height="85" alt="Harold Hill"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/hill.xml" target="_blank"><xsl:value-of select="//lang_blocks/hill"/></a> <br/>
          <xsl:value-of select="//lang_blocks/hill_a"/></div></td>
          
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_horner.gif" width="92" height="85" alt="Sharon Horner"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/horner.xml" target="_blank"><xsl:value-of select="//lang_blocks/horner"/></a> <br/>
        <xsl:value-of select="//lang_blocks/horner_a"/></div></td>
          
    
<td><div align="center"><img src="/test/lisa/spotlight/images/slide_wildman.gif" width="92" height="85" alt="John Wildman"/><br/>
        <a class="b" href="http://www.clubshop.com/spotlight/wildman.xml" target="_blank"><xsl:value-of select="//lang_blocks/wildman"/></a> <br/>
         <xsl:value-of select="//lang_blocks/wildman_a"/></div></td>
    

    
   <td></td>
    
    
    
  </tr>
</table>

<img src="http://www.clubshop.com/images/valid-xhtml10-blue.png " alt="passed validation"/>

</body></html>


</xsl:template>
</xsl:stylesheet>