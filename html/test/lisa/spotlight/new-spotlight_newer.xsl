<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Member Spotlight</title>
<style type="text/css">
body {background-color: #FFFFFF; color: black;}
p{font: smaller Verdana, Arial,san-serif;}
td {border-width: 0; padding: 0;}
td#banner {border-bottom: 2px solid #0033CC; height: 41px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	padding: 1em;
	font: smaller Verdana, Arial,san-serif;
	vertical-align:top;
}
h2 {
	font: bold 125% sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #0033cc;
	vertical-align:top;
}
td#main p {margin: 1em 2.5em;}
td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #0033CC;
	width: 167px;
}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style>
</head>
<body>

<xsl:copy-of select = "*/*"/><!-- by doing */* instead of * we get rid of the 'article' tag -->
</body>
</html>
</xsl:template>
</xsl:stylesheet>