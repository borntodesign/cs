﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Glocal Income Compensation Plan</title>


<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

</style>



<style type="text/css">
p {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
p.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-indent: 20px;}
table {border: 1px;
border-color: #003399;
}
li{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	list-style-type: disc;
	padding-right: 5px 5px 5px 5px ;

	}
li.a{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	list-style-type: upper-alpha;
	font-style: normal;
	white-space: normal;
	
}
li.b{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	padding-bottom: 7px;
}


h3{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #003399;
}
tr {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	line-height: normal;
	height: 7px;
	left: 3px;
}
tr.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
	background-color: #F3F3DE;
}
tr.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F3F3DE;
}
tr.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F3F3DE;
	text-align: center;
}
tr.d {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #FFFFFF;
	text-align: center;
}
tr.e {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	background-color: #FFFFFF;
	
}

td.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	background-color: #F3F3DE;	
}

</style>
</head>
<body>
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      

      
      
      <tr>
       <td background="/images/bg_header.jpg"><img src="http://www.clubshop.com/images/headers/header_compplan.png" width="640" height="70" alt="Compensation Plan"/></td>

      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

          <tr>
            <td align="right">
<a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle"/></a><a href="javascript:window.close();" 
class="nav_footer_brown"></a><span class="Header_Main"> </span>
              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>

<p><xsl:value-of select="//lang_blocks/p1"/> <xsl:text> </xsl:text> <a href="http://www.clubshop.com/present/bizop.xml" target="_blank"><xsl:value-of select="//lang_blocks/p2"/></a></p>

<p new="10/30/2008"><xsl:value-of select="//lang_blocks/p3"/> <xsl:text> </xsl:text><a href="/fees/full.xml" target="_blank"><xsl:value-of select="//lang_blocks/p2"/></a></p>
 <hr align="left" width="100%" size="1" color="#A35912"/>
<p><xsl:value-of select="//lang_blocks/p4"/></p>


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p5"/></span>
<p><xsl:value-of select="//lang_blocks/p6"/></p>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p7"/></span>
<p><xsl:value-of select="//lang_blocks/p8"/></p>

<p><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text> <a href="/fees/full.xml" target="_blank"><xsl:value-of select="//lang_blocks/p2"/></a></p>


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p10"/></span>
<p><xsl:value-of select="//lang_blocks/p11"/></p>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p12"/></span>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<p><xsl:value-of select="//lang_blocks/p14"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>
<li><xsl:value-of select="//lang_blocks/p17"/></li>
<li><xsl:value-of select="//lang_blocks/p18"/></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p19"/></p>
 <hr align="left" width="100%" size="1" color="#A35912"/>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p20"/></span>
<p><xsl:value-of select="//lang_blocks/p21"/></p>

<p><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23"/></p>
<p><b><xsl:value-of select="//lang_blocks/p24"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p25"/></p>
<p><b><xsl:value-of select="//lang_blocks/p26"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></p>

<div><table width="95%" border="0" cellpadding="5">
        <tr> 
          <td><b><xsl:value-of select="//lang_blocks/p28"/></b></td>
          <td rowspan="2"><img src="/images/thin_line.gif" alt="thin div line" width="2" height="228"/></td>
          <td><b><xsl:value-of select="//lang_blocks/p29"/></b></td>
        </tr>
        <tr> 
          <td valign="top"><img src="/images/dp_nwi-no.gif" alt="no network income" width="125" height="100"/></td>
          <td valign="top"><img src="/images/dp_nwi_yes.gif" alt="network income" width="450" height="200"/></td>
        </tr>
      </table>
      </div>

<p><b><xsl:value-of select="//lang_blocks/p30"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></p>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p32"/></span>

<p><xsl:value-of select="//lang_blocks/p33"/></p>
<p><xsl:value-of select="//lang_blocks/p34"/></p>
<p><xsl:value-of select="//lang_blocks/p35"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p36"/></li>
<li><xsl:value-of select="//lang_blocks/p37"/></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p38"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/orr.cgi"><xsl:value-of select="//lang_blocks/p39"/></a></p>


 <hr align="left" width="100%" size="1" color="#A35912"/>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p40"/></span>
<p><b><xsl:value-of select="//lang_blocks/p41"/></b></p>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p42"/></span><br/>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p43"/></span>
<p><xsl:value-of select="//lang_blocks/p44"/></p>


<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p45"/></td>
<td bgcolor="#FFFFFF">75</td>
<td bgcolor="#FFFFFF">75</td>
<td bgcolor="#FFFFFF">150</td>
<td bgcolor="#FFFFFF">250</td>
<td bgcolor="#FFFFFF">400</td>
<td bgcolor="#FFFFFF">600</td>
<td bgcolor="#FFFFFF">1,000</td>
<td bgcolor="#FFFFFF">1,500</td>
<td bgcolor="#FFFFFF">2,250</td>
<td bgcolor="#FFFFFF">3,250</td>
<td bgcolor="#FFFFFF">4,500</td>
<td bgcolor="#FFFFFF">6,000</td>
<td bgcolor="#FFFFFF">6,000</td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p46"/></td>
<td bgcolor="#FFFFFF">25</td>
<td bgcolor="#FFFFFF">25</td>
<td bgcolor="#FFFFFF">50</td>
<td bgcolor="#FFFFFF">100</td>
<td bgcolor="#FFFFFF">150</td>
<td bgcolor="#FFFFFF">200</td>
<td bgcolor="#FFFFFF">300</td>
<td bgcolor="#FFFFFF">500</td>
<td bgcolor="#FFFFFF">750</td>
<td bgcolor="#FFFFFF">1,000</td>
<td bgcolor="#FFFFFF">1,500</td>
<td bgcolor="#FFFFFF">6,000</td>
<td bgcolor="#FFFFFF">2,000</td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p47"/></td>
<td bgcolor="#FFFFFF">Non-<br/>Builder</td>
<td bgcolor="#FFFFFF">Builder</td>
<td bgcolor="#FFFFFF">Builder</td>
<td bgcolor="#FFFFFF">Builder</td>
<td bgcolor="#FFFFFF">Strategist</td>
<td bgcolor="#FFFFFF">Strategist</td>
<td bgcolor="#FFFFFF">Trainer</td>
<td bgcolor="#FFFFFF">Trainer</td>
<td bgcolor="#FFFFFF">Marketing Director</td>
<td bgcolor="#FFFFFF">Promotions Director</td>
<td bgcolor="#FFFFFF">Chief<br/>Director</td>
<td bgcolor="#FFFFFF">Non Chief</td>
<td bgcolor="#FFFFFF">Chief<br/>Director</td>

</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p48"/></td>
<td bgcolor="#FFFFFF">0%</td>
<td bgcolor="#FFFFFF">34%</td>
<td bgcolor="#FFFFFF">36%</td>
<td bgcolor="#FFFFFF">39%</td>
<td bgcolor="#FFFFFF">42%</td>
<td bgcolor="#FFFFFF">46%</td>
<td bgcolor="#FFFFFF">49%</td>
<td bgcolor="#FFFFFF">52%</td>
<td bgcolor="#FFFFFF">56%</td>
<td bgcolor="#FFFFFF">59%</td>
<td bgcolor="#FFFFFF">62%</td>
<td bgcolor="#FFFFFF">65%</td>
<td bgcolor="#FFFFFF">67%</td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p49"/></td>
<td bgcolor="#FFFFFF">n/a</td>
<td bgcolor="#FFFFFF">Producer</td>
<td bgcolor="#FFFFFF">Team Player</td>
<td bgcolor="#FFFFFF">Team Leader</td>
<td bgcolor="#FFFFFF">Team Captain</td>
<td bgcolor="#FFFFFF">Bronze Manager</td>
<td bgcolor="#FFFFFF">Silver Manager</td>
<td bgcolor="#FFFFFF">Gold Manager</td>
<td bgcolor="#FFFFFF">Ruby<br/>Director</td>
<td bgcolor="#FFFFFF">Emerald<br/>Director</td>
<td bgcolor="#FFFFFF">Diamond<br/>Director</td>
<td bgcolor="#FFFFFF">n/a</td>
<td bgcolor="#FFFFFF">Executive Director</td>
</tr>
</table>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p50"/></span>

<p><xsl:value-of select="//lang_blocks/p51"/></p>

<ol>
<li class="a"><xsl:value-of select="//lang_blocks/p52"/></li>
<p><b><xsl:value-of select="//lang_blocks/p53"/></b></p>
<li class="a"><xsl:value-of select="//lang_blocks/p54"/></li>
<p><xsl:value-of select="//lang_blocks/p55"/></p>
<li class="a"><xsl:value-of select="//lang_blocks/p56"/></li>
<p><xsl:value-of select="//lang_blocks/p57"/></p>
<p><xsl:value-of select="//lang_blocks/p58"/></p>
</ol>
 

<hr align="left" width="100%" size="1" color="#A35912"/>
 
 
 <span class="Header_Main"><xsl:value-of select="//lang_blocks/p120"/></span>
 
 <br/><br/>

<span class="text_brown"><b><xsl:value-of select="//lang_blocks/p59"/></b></span>
	<ul>
	<li><xsl:value-of select="//lang_blocks/p60"/></li>
	<li><xsl:value-of select="//lang_blocks/p61"/></li>
	</ul>

 
<span class="text_brown"><b><xsl:value-of select="//lang_blocks/p121"/></b></span>
	<ul><li><xsl:value-of select="//lang_blocks/p122"/></li></ul>

 
<span class="text_brown"><b><xsl:value-of select="//lang_blocks/p123"/></b></span>
 	 <ul><li><xsl:value-of select="//lang_blocks/p124"/></li></ul>
 
 
<table width="40%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr>
    <td bgcolor="#FFFFFF" align="center"><span class="text_brown"><xsl:value-of select="//lang_blocks/p125"/></span></td>
    <td bgcolor="#FFFFFF" align="center"><span class="text_brown"><xsl:value-of select="//lang_blocks/p126"/></span></td>
    <td bgcolor="#FFFFFF" align="center"><span class="text_brown"><xsl:value-of select="//lang_blocks/p127"/></span></td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p128"/></td>
    <td bgcolor="#FFFFFF">2,000</td>
    <td bgcolor="#FFFFFF">$1,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">1 Star</td>
    <td bgcolor="#FFFFFF">6,000</td>
    <td bgcolor="#FFFFFF">$1,500</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">2 Star</td>
    <td bgcolor="#FFFFFF">2,000</td>
    <td bgcolor="#FFFFFF">$2,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">2 Star</td>
    <td bgcolor="#FFFFFF">6,000</td>
    <td bgcolor="#FFFFFF">$2,500</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">3 Star</td>
    <td bgcolor="#FFFFFF">2,000</td>
    <td bgcolor="#FFFFFF">$3,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">3 Star</td>
    <td bgcolor="#FFFFFF">6,000</td>
    <td bgcolor="#FFFFFF">$3,500</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">4 Star</td>
    <td bgcolor="#FFFFFF">2,000</td>
    <td bgcolor="#FFFFFF">$4,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">4 Star</td>
    <td bgcolor="#FFFFFF">6,000</td>
    <td bgcolor="#FFFFFF">$4,500</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">5 Star</td>
    <td bgcolor="#FFFFFF">2,000</td>
    <td bgcolor="#FFFFFF">$5,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">5 Star</td>
    <td bgcolor="#FFFFFF">6,000</td>
    <td bgcolor="#FFFFFF">$5,500</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">6 Star (Ambassador's Club - AC)</td>
    <td bgcolor="#FFFFFF">0</td>
    <td bgcolor="#FFFFFF">$6,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 1 AC</td>
    <td bgcolor="#FFFFFF">$7,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 2 AC</td>
    <td bgcolor="#FFFFFF">$8,000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 3 AC</td>
    <td bgcolor="#FFFFFF">$9.000</td>

  </tr>
  <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 4 AC</td>
    <td bgcolor="#FFFFFF">$10,000</td>

  </tr>
  
    <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 5 AC</td>
    <td bgcolor="#FFFFFF">$11,000</td>

  </tr>
  
    <tr>
    <td bgcolor="#FFFFFF">AC</td>
    <td bgcolor="#FFFFFF">with 6 AC</td>
    <td bgcolor="#FFFFFF">$12,000</td>

  </tr>
  
  
  </table>
 
 
 <br/>
 
 
  <hr align="left" width="100%" size="1" color="#A35912"/>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p62"/></span>
<p><xsl:value-of select="//lang_blocks/p63"/></p>



<table width="50%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p64"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p65"/></td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p66"/></td>
<td bgcolor="#FFFFFF">7.5%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p67"/></td>
<td bgcolor="#FFFFFF">8.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p68"/></td>
<td bgcolor="#FFFFFF">8.5%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p69"/></td>
<td bgcolor="#FFFFFF">9.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p70"/></td>
<td bgcolor="#FFFFFF">9.5%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p71"/></td>
<td bgcolor="#FFFFFF">10.0%</td></tr>

</table>

<p><xsl:value-of select="//lang_blocks/p72"/></p>


<table width="50%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p64"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p73"/></td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p67"/></td>
<td bgcolor="#FFFFFF">2.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p68"/></td>
<td bgcolor="#FFFFFF">4.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p69"/></td>
<td bgcolor="#FFFFFF">6.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p70"/></td>
<td bgcolor="#FFFFFF">8.0%</td></tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p71"/></td>
<td bgcolor="#FFFFFF">10.0%</td></tr>

</table>
<p><xsl:value-of select="//lang_blocks/p74"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p75"/></li><br/>
<li><xsl:value-of select="//lang_blocks/p76"/></li><br/>
<li><xsl:value-of select="//lang_blocks/p77"/></li>
</ul>


 <hr align="left" width="100%" size="1" color="#A35912"/>


<!--removed 6/1/10"<span class="Header_Main"><xsl:value-of select="//lang_blocks/p83"/></span>

<p><xsl:value-of select="//lang_blocks/p84"/></p>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p85"/></span>

<p><b><xsl:value-of select="//lang_blocks/p86"/></b></p>

<ul>
<li class="b"><xsl:value-of select="//lang_blocks/p87"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p88"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p89"/></li>
</ul>-->

<!-- removed 6/1/10<p><b><xsl:value-of select="//lang_blocks/p90"/></b></p>

<ul>
<li class="b"><xsl:value-of select="//lang_blocks/p91"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p92"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p93"/></li>
</ul>
-->



<span class="Header_Main"><xsl:value-of select="//lang_blocks/p102"/></span>
<p><xsl:value-of select="//lang_blocks/p103"/></p>
<ul>
<li class="b"><xsl:value-of select="//lang_blocks/p104"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p105"/></li>
<li class="b"><xsl:value-of select="//lang_blocks/p106"/></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p107"/></p>
<p><xsl:value-of select="//lang_blocks/p108"/></p>
<p><xsl:value-of select="//lang_blocks/p109"/></p>
<p><xsl:value-of select="//lang_blocks/p110"/></p>

 <hr align="left" width="100%" size="1" color="#A35912"/>




</td></tr>


</table></td>
      </tr>
    </table>
    </td>
  </tr>
  
  <tr><td align="center"><span class="footer_copyright">| <a href="/manual/index.xml" target="_blank" class="nav_header">Home</a> | <a href="https://www.clubshop.com/vip/control/index.shtml" target="_blank" class="nav_header">VIP Control Center</a> | <a href="/ppreport.html" target="_blank" class="nav_header">Member Portal Page</a> | <a href="/manual/siteindex.html" target="_blank" class="nav_header">Site Index</a></span></td></tr>
                <tr><td align="center"><span class="footer_copyright"><br />Copyright © 2010 Glocal Income™. All rights reserved.</span></td></tr>

</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
