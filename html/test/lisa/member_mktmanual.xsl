<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
   doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
   omit-xml-declaration="yes"
   media-type="text/html"/>






  <xsl:template match="/">
           <html><head>
           <title>Member Market Manual</title>

<style type="text/css">
p {font-family: Verdana, Arial, sans-serif;
font-size: small;
}
p.a{font-family: Verdana, Arial, sans-serif;
color: #336699;
font-weight: bold;
}
h3 {font-family: Verdana, Arial, sans-serif;
color: #336699;
}
hr {color: #336699;}
td {font-family: Verdana, Arial, sans-serif;
font-size: small;
}

</style>

</head>
<body>

<table border="1"><tr><td valign="top">
<div align="center">
<img src="http://www.clubshop.com/images/topblue.gif" width="700" height="25"/><br/><img src="http://www.clubshop.com/images/mem_mkthdr.gif" width="700" height="50"/>
</div></td></tr>

<tr><td>
<p><xsl:value-of select="//lang_blocks/p1"/></p>

<p><b><xsl:value-of select="//lang_blocks/p2"/></b></p>

<p><xsl:value-of select="//lang_blocks/p3"/></p>

<p class="a"><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>

<p class="a"><xsl:value-of select="//lang_blocks/p6"/></p>

<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>

<p class="a"><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>

<p><xsl:value-of select="//lang_blocks/p11"/><xsl:text> </xsl:text><a href="/fees/full.xml" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a>.</p>

<div align="center">

<p><a href="https://www.clubshop.com/cgi/appx.cgi//upg"><img src="images/subscribe_ca.gif" height="30" width="350" alt="Upgrade to VIP"/></a></p>
<p><a href="/manual/policies/terms.xml"><xsl:value-of select="//lang_blocks/p13"/></a></p>

</div>

<hr/>
<h3><xsl:value-of select="//lang_blocks/p14"/></h3>
<table width="75%">

<tr>
<td><a href="/manual/policies/advertising.xml" target="_blank"><xsl:value-of select="//lang_blocks/p15"/></a></td>
<td><a href="/manual/policies/privacy.xml" target="_blank"><xsl:value-of select="//lang_blocks/p20"/></a></td>
</tr>

<tr>
<td><a href="/manual/policies/cb_activation.xml" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a></td>
<td><a href="/manual/policies/purchase_mbrs.xml" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a></td>
</tr>

<tr>
<td><a href="/manual/policies/membership.xml" target="_blank"><xsl:value-of select="//lang_blocks/p17"/></a></td>
<td><a href="/manual/policies/return.xml" target="_blank"><xsl:value-of select="//lang_blocks/p22"/></a></td>
</tr>

<tr>
<td><a href="/manual/policies/name_usage.xml" target="_blank"><xsl:value-of select="//lang_blocks/p18"/></a></td>
<td><a href="/manual/policies/solicitation.xml" target="_blank"><xsl:value-of select="//lang_blocks/p23"/></a></td>
</tr>

<tr>
<td><a href="/manual/policies/promotional_page.xml" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a></td>
<td><a href="/manual/policies/spam.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24"/></a></td>
</tr>

</table>

<br/>
<br/>
<p align="center"><a href="http://www.clubshop.com/ppreport.html"><xsl:value-of select="//lang_blocks/p25"/></a></p>


</td></tr></table>


</body></html>

</xsl:template>
</xsl:stylesheet> 