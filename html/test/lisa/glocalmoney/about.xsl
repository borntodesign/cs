<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Glocal Money Limited</title>

<style type="text/css">

body {background-color: #758C4A;
}
h2 {font-family: Trebuchet MS,Verdana, Arial,sans-serif;
font-size: 25px;
color: #006600;
}
h4 {font-family: Arial, Verdana, sans-serif;
font-size: 15px;
color: #006600;
}

td#main{background-color: #FAFBF7;
padding: 5px;
border: thin solid #006600;
vertical-align: top;
}

h5 {font-family: Arial, Verdana, sans-serif;
font-size: 12px;
color: #006600;

}
p {font-family: Verdana, Arial, sans-serif;
font-size: smaller;
}


ul{list-style: url(/test/lisa/glocalmoney/images/bullet.gif);
}

li {font-family: Verdana, Arial, sans-serif;
font-size: smaller;
}
hr{color: #CCCCCC;}

table.foot{font-family: Verdana, Arial, sans-serif;
font-size: x-small;
width: 100%;
color: #CCCCCC;
}
a {color: #006633;
text-decoration: none;}
a.hover{color: #006633;
text-decoration: underline;}
</style>

</head>
<body>
<table>
<tr><td valign="top">
<img src="/test/lisa/glocalmoney/images/gm_header.gif" height="60" width="770" alt="Glocal Money Limited"/>
</td></tr>
<tr><td id="main">

<div align="center">

<h2><xsl:value-of select="//lang_blocks/p2"/></h2></div>




<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<p><xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>


<hr/>

<p><i><xsl:value-of select="//lang_blocks/p20"/></i></p>

<table class="foot">
<tr>
<td><a href="index.xml" target="blank"><xsl:value-of select="//lang_blocks/p21"/></a></td>
<td><a href="faq.xml" target="_blank"><xsl:value-of select="//lang_blocks/p22"/></a></td>
<td><xsl:value-of select="//lang_blocks/p23"/></td>
<td><a href="about.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24"/></a></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p25"/></td>
<td><xsl:value-of select="//lang_blocks/p26"/></td>
<td><xsl:value-of select="//lang_blocks/p27"/></td>
<td><xsl:value-of select="//lang_blocks/p28"/></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p29"/></td>
<td><xsl:value-of select="//lang_blocks/p30"/></td>
<td><xsl:value-of select="//lang_blocks/p31"/></td>
<td><xsl:value-of select="//lang_blocks/p32"/></td>
</tr>

</table>




</td></tr></table>
</body></html>


</xsl:template>
</xsl:stylesheet>