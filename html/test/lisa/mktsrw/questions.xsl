
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>DHS Club International Business Opportunity</title>

<style type="text/css">
p {font-family: Verdana, Arial, sans-serif;
font-size: small;
text-align: left;
}
td.odd{background-color: #F2F2F2;
font-family: Verdana, Arial, sans-serif;
font-size: smaller;
vertical-align: top;}

td.even{background-color: #E6F2FF;
font-family:Verdana, Arial, sans-serif;
font-size: smaller;
vertical-align: top;}

h3{
	font-family:Verdana, Arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	}
	
td.meta{
	font-family:Verdana, Arial, sans-serif;
	font-size: 9px;
	}
td.headinga{
	font-family:Verdana, Arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	vertical-align: top;
	background-color :  #CCCCCC;
}

td.headingb{
	font-family:Verdana, Arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	vertical-align: top;
	background-color :  #00A9E1;
}






 
</style>
</head>

<body>
<table width="769" cellpadding="5" bgcolor="#003366">
  <tr> 
    <td width="753" bgcolor="#FFFFFF"> 
      <table width="98%" cellpadding="2">
        
        <tr> 
          <td  bgcolor="#FFFFFF" cellpadding="5">
          <p><xsl:value-of select="//lang_blocks/p1"/></p>
          <p><xsl:value-of select="//lang_blocks/p2"/></p>
          <p><xsl:value-of select="//lang_blocks/p3"/></p>
          <p><xsl:value-of select="//lang_blocks/p3a"/></p>
           
            <hr/></td>
        </tr>
        <tr> 
          <td bgcolor="#FFFFFF"> 
</td>
        </tr>
      </table>
      <table width="100%" cellpadding="2">
        <tr> 
          <td colspan="2"> <h3><xsl:value-of select="//lang_blocks/p4"/></h3></td>
        </tr>
        <tr> 
          <td colspan="2" class="headinga"><h5><xsl:value-of select="//lang_blocks/p5"/> </h5></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu1"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu2"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu3"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu4"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu5"/></td>
          <td class="odd"><form name="form6" method="post" action="">
              <select name="selected">
                <option selected="select">SELECT</option>
                <option>Under 18</option>
                <option>19-25</option>
                <option>26-35</option>
                <option>36-45</option>
                <option>46-55</option>
                <option>56-65</option>
                <option>Over 65</option>
              </select>
            </form></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu6"/></td>
          <td class="odd"> 
            <form name="form2" method="post" action="">
              <select name="select2">
                <option selected="select">SELECT</option>
                <option>Married</option>
                <option>Single</option>
              </select>
            </form>
            <label></label></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu7"/></td>
          <td class="odd"><form name="form3" method="post" action="">
              <select name="select3">
                <option selected="select">SELECT</option>
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5 or more</option>
              </select>
            </form></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu8"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu9"/></td>
          <td class="odd"></td>
        </tr>
        <tr> 
          <td class="odd"><xsl:value-of select="//lang_blocks/qu10"/></td>
          <td class="odd"><form name="form1" method="post" action="">
              <input type="text" name="textfield"/>
              <input type="text" name="textfield2"/>
              <input type="text" name="textfield3"/>
            </form></td>
        </tr>
        <tr> 
          <td colspan="2" class="headingb"><h5><xsl:value-of select="//lang_blocks/p6"/></h5></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu11"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu12"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu13"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu14"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu15"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu16"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu17"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu18"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu19"/><br/>
            <xsl:value-of select="//lang_blocks/qu20"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu21"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu22"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td class="even"><xsl:value-of select="//lang_blocks/qu23"/></td>
          <td class="even"></td>
        </tr>
        <tr> 
          <td colspan="2" class="headinga"><h5><xsl:value-of select="//lang_blocks/p7"/></h5></td>
        </tr>
        <tr bgcolor="#FFFF00"> 
          <td colspan="2" valign="top" class="odd"> 
            <p> <xsl:value-of select="//lang_blocks/p8"/></p>
            <p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p9"/> </font><xsl:value-of select="//lang_blocks/p10"/></p>
            <p><font color="#009900"><xsl:value-of select="//lang_blocks/p11"/></font> <xsl:value-of select="//lang_blocks/p12"/></p>
            <p><xsl:value-of select="//lang_blocks/p13"/></p>
            <p><xsl:value-of select="//lang_blocks/p14"/></p>
            <div align="center"><img src="/test/lisa/mktsrw/images/search.gif" width="301" height="98" alt="search engines"/> 
            </div>
           
            <p><xsl:value-of select="//lang_blocks/p17"/></p>
            <hr/>
            <p><xsl:value-of select="//lang_blocks/bio1"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/bio2"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/bio3"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/bio4"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/bio5"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/bio6"/></p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table>
            <p></p>
            <p><xsl:value-of select="//lang_blocks/p18"/> </p>
            <table width="80%" border="1" cellpadding="5">
              <tr> 
                <td bgcolor="#FFFFFF"></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td colspan="2" class="headingb"><h5><xsl:value-of select="//lang_blocks/p19"/></h5></td>
        </tr>
        <tr > 
          <td colspan="2" class="even"><p><xsl:value-of select="//lang_blocks/p20"/></p>
          <p><xsl:value-of select="//lang_blocks/p20a"/></p>
            <p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p9"/></font><xsl:value-of select="//lang_blocks/p21"/></p>
            <p><font color="#009900"><xsl:value-of select="//lang_blocks/p11"/></font><xsl:value-of select="//lang_blocks/p22"/> </p>
            <p><xsl:value-of select="//lang_blocks/p23"/></p>
            <table width="80%" border="1" cellpadding="5" bgcolor="#FFFFFF">
              <tr> 
                <td class="meta"><xsl:value-of select="//lang_blocks/mt1"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt2"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt3"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt4"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt5"/></td>
              </tr>
              <tr> 
                <td class="meta"><xsl:value-of select="//lang_blocks/mt6"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt7"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt8"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt9"/></td>
                <td class="meta"><xsl:value-of select="//lang_blocks/mt10"/></td>
              </tr>
            </table>
            <p></p></td>
        </tr>
        <tr> 
          <td colspan="2" class="headinga"> 
            <h5><xsl:value-of select="//lang_blocks/p24"/></h5></td>
        </tr>
        <tr> 
          <td height="103" colspan="2" class="odd"> <p> <xsl:value-of select="//lang_blocks/p25"/></p>
            <p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p9"/></font> <xsl:value-of select="//lang_blocks/p26"/></p>
            <p><font color="#009900"><xsl:value-of select="//lang_blocks/p11"/></font> <xsl:value-of select="//lang_blocks/p27"/></p>
            <p><xsl:value-of select="//lang_blocks/p28"/></p>
            <p><font color="#000000"><xsl:value-of select="//lang_blocks/p29"/></font><br/>
              <font color="#006600"><xsl:value-of select="//lang_blocks/p31"/></font><br/>
              <font color="#FF0000"><xsl:value-of select="//lang_blocks/p32"/></font></p>
            <table width="80%" border="1" cellpadding="5" bgcolor="#FFFFFF">
              <tr> 
                <td></td>
              </tr>
            </table>
            <p></p></td>
        </tr>
      </table> 
      
      <form name="form23" method="post" action="">
        <div align="center">
          <input type="submit" name="Submit" value="Submit"/>
        </div>
      </form>
      <p></p>
      </td>
</tr> 
</table>
</body>
</html>


</xsl:template>
</xsl:stylesheet>