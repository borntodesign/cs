<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>DHS Club International Business Opportunity</title>

<style type="text/css">
body {background-color: #999999; color: black;}
td {border-width: 0; padding: 5px;
font: smaller Verdana, Arial,san-serif;
}
td#banner {border-bottom: 2px solid #FFFFFF; height: 30px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	padding: 0em;
	font: smaller Verdana, Arial,san-serif;
	vertical-align:top;
	
}
td#main h2 {
	font: bold 105% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #000099;
	vertical-align:top;
	text-align: center;
	color: #50260D;
}

td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #0093C4;
	background-color: #FFFFFF;
	
}
td#sidelinks p {
font-family: Verdana, Arial, sans-serif;
	font-size: x-small;}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}

<!--middle section-->

</style>
<style type="text/css">

/* menu styles */
td#sidelinks a {
	display: block;
	margin: 10px 2px 0 0;
	padding: 0px 0px 0px 4px;
	text-decoration: none;
	font: normal 11px Verdana, Arial, sans-serif;
	color: Black;
	
}
td#sidelinks a:visited {color: #4B3E35;
}
td#sidelinks h5 {background-color: #00AEE8; color: #996600;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial, Verdana, sans-serif;
   border-bottom: 1px solid #6F7B33;
}
td#sidelinks h4 {
	background-color: #00AEE8;
	color: #000000;
	margin: 0 0 0 0;
	padding: 0 0  0 10px;
	font: bold  12px Arial, Verdana, sans-serif;
	border-bottom: 1px solid #006699;
	background-position: left;
   }
td#sidelinks a:hover {
	background-color: #CCCCCC;
	color: #000000;
	border-right: 3px solid #00AEE8;
	padding-right: 3px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}





td#sidlinks p{font-family: Verdana, Arial, sans-serif;
size: x-small;
}
</style>

</head>
<body>

<table width="100%" border="0" cellpadding="0" cellspacing="1" >
  <tr bgcolor="#FFFFFF" valign="top"> 
    <td valign="top"> 
      <img src="/test/lisa/mktsrw/images/glocal.gif" width="768" height="86"/></td>
  </tr>
  <tr bgcolor="#000000" valign="top"> 
    <td height="22" valign="top"> </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td id="sidelinks"> 

<h4><xsl:value-of select="//lang_blocks/p1"/></h4>
      <p align="center"><img src="/test/lisa/mktsrw/images/meme.gif" width="100" height="100"/></p>
        <p>firstname lastname<br/>
        city, state/region,country<br/>
          Club Member Since:</p>
          
<h4><xsl:value-of select="//lang_blocks/p2"/></h4>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<p><xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>
<p><xsl:value-of select="//lang_blocks/p9"/></p>


<h4><xsl:value-of select="//lang_blocks/p10"/></h4>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<p><xsl:value-of select="//lang_blocks/p12"/></p>
<p><xsl:value-of select="//lang_blocks/p13"/></p>
<p><xsl:value-of select="//lang_blocks/p14"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>
<p><xsl:value-of select="//lang_blocks/p16"/></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>
<p><xsl:value-of select="//lang_blocks/p18"/></p>
<p><xsl:value-of select="//lang_blocks/p19"/></p>
<p><xsl:value-of select="//lang_blocks/p20"/></p>
<p><xsl:value-of select="//lang_blocks/p21"/></p>

<h4><xsl:value-of select="//lang_blocks/p22"/></h4>
<p><a href="http://www.clubshop.com/cgi/appx.cgi/[[alias]]"><xsl:value-of select="//lang_blocks/p23"/></a></p>
<p><a href="https://www.clubshop.com/cgi/appx.cgi//upg?flg=dv&amp;sponsorID=[[id]]"><xsl:value-of select="//lang_blocks/p25"/></a></p>
<p><a href="http://www.clubshop.com/nonprofit.xml"><xsl:value-of select="//lang_blocks/p27"/><br/><xsl:value-of select="//lang_blocks/p27a"/></a></p>


<h4><xsl:value-of select="//lang_blocks/p28"/></h4>
<p><a href="https://www.clubshop.com/test/lisa/mktsrw/contact.html"><xsl:value-of select="//lang_blocks/p29"/></a></p>
<p><img src="/test/lisa/mktsrw/images/skype.gif" width="41" height="18"/><xsl:value-of select="//lang_blocks/p30"/></p>








</td>
	  
	  
    <td valign="top" id="main">
<table height="1150">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
          
     <h5><xsl:value-of select="//lang_blocks/p31"/></h5>
  
          
          
          
          
          
          
          </td>
          
          </tr>
      </table>
      
      
    </td>
  </tr>
</table>

	
<div align="center"> 
  <table width="100%" border="1" cellpadding="5">
    <tr>
      <td height="28" bgcolor="#FFFFFF"> 
        <div align="center">ClubShop Mall Image</div></td>
    </tr>
  </table>
</div>



</body></html>


</xsl:template>
</xsl:stylesheet>