/*Example message arrays for the two demo scrollers*/

var pausecontent=new Array()                                                           
pausecontent[0]='<a href="#" class="Nav_Scroller_Green" >FREDDY BROWN</a><br />Athens: Glyfada, <a href="#" class="Nav_Red">GREECE</a>'
pausecontent[1]='<a href="#" class="Nav_Scroller_Green" >JENNY SMITH</a><br />Curacao: <a href="#" class="Nav_Red">NETHERLANDS ANTILLES</a>'
pausecontent[2]='<a href="#" class="Nav_Scroller_Green" >TRACY BLING</a><br />Miami: Florida. <a href="#" class="Nav_Red">UNITED STATES</a>'
pausecontent[3]='<a href="#" class="Nav_Scroller_Green" >TOM LIVINGSTONE</a><br />Paris: Malakoff, <a href="#" class="Nav_Red">FRANCE</a>'
pausecontent[4]='<a href="#" class="Nav_Scroller_Green" >BOBBY SCHILLING</a><br /> Roma, Region Lazio, <a href="#" class="Nav_Red">ITALY</a>'
pausecontent[5]='<a href="#" class="Nav_Scroller_Green" >VERONICA PAPER</a><br />Venice: Cipriani, <a href="#" class="Nav_Red">ITALY</a>'
pausecontent[6]='<a href="#" class="Nav_Scroller_Green" >ARNOLD TREEHOUSE</a><br />Montego Bay, <a href="#" class="Nav_Red">JAMAICA</a>'
pausecontent[7]='<a href="#" class="Nav_Scroller_Green" >JAMES SMILER</a><br />Oranjestadt: Salsa, <a href="#" class="Nav_Red">ARUBA</a>'
pausecontent[8]='<a href="#" class="Nav_Scroller_Green" >BEN ROUNDFOOT</a><br />Frankfurt: Hesse, <a href="#" class="Nav_Red">GERMANY</a>'
pausecontent[9]='<a href="#" class="Nav_Scroller_Green" >ERICA HILL</a><br />Amsterdam: Noord-Holland, <a href="#" class="Nav_Red">NETHERLANDS</a>'
pausecontent[10]='<a href="#" class="Nav_Scroller_Green" >BETSY TURTLENECK</a><br />Los Angelese, <a href="#" class="Nav_Red">UNITED STATES</a>'
pausecontent[11]='<a href="#" class="Nav_Scroller_Green" >CHER BRIGHTHOUSE</a><br /> Tuscany (Toscana), <a href="#" class="Nav_Red">ITALY</a>'
pausecontent[12]='<a href="#" class="Nav_Scroller_Green" >TONY JONES</a><br />Istambul Marmara Region, <a href="#" class="Nav_Red">TURKEY</a>'
pausecontent[13]='<a href="#" class="Nav_Scroller_Green" >HARRY LIVESTRONG</a><br />Tangiers La Tangerina, <a href="#" class="Nav_Red">MOROCCO</a>'

/***********************************************
* Pausing up-down scroller- � Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/

function pausescroller(content, divId, divClass, delay){
this.content=content //message array content
this.tickerid=divId //ID of ticker div to display information
this.delay=delay //Delay between msg change, in miliseconds.
this.mouseoverBol=0 //Boolean to indicate whether mouse is currently over scroller (and pause it if it is)
this.hiddendivpointer=1 //index of message array for hidden div
document.write('<div id="'+divId+'" class="'+divClass+'" style="position: relative; overflow: hidden"><div class="innerDiv" style="position: absolute; width: 100%" id="'+divId+'1">'+content[0]+'</div><div class="innerDiv" style="position: absolute; width: 100%; visibility: hidden" id="'+divId+'2">'+content[1]+'</div></div>')
var scrollerinstance=this
if (window.addEventListener) //run onload in DOM2 browsers
window.addEventListener("load", function(){scrollerinstance.initialize()}, false)
else if (window.attachEvent) //run onload in IE5.5+
window.attachEvent("onload", function(){scrollerinstance.initialize()})
else if (document.getElementById) //if legacy DOM browsers, just start scroller after 0.5 sec
setTimeout(function(){scrollerinstance.initialize()}, 500)
}

// -------------------------------------------------------------------
// initialize()- Initialize scroller method.
// -Get div objects, set initial positions, start up down animation
// -------------------------------------------------------------------

pausescroller.prototype.initialize=function(){
this.tickerdiv=document.getElementById(this.tickerid)
this.visiblediv=document.getElementById(this.tickerid+"1")
this.hiddendiv=document.getElementById(this.tickerid+"2")
this.visibledivtop=parseInt(pausescroller.getCSSpadding(this.tickerdiv))
//set width of inner DIVs to outer DIV's width minus padding (padding assumed to be top padding x 2)
this.visiblediv.style.width=this.hiddendiv.style.width=this.tickerdiv.offsetWidth-(this.visibledivtop*2)+"px"
this.getinline(this.visiblediv, this.hiddendiv)
this.hiddendiv.style.visibility="visible"
var scrollerinstance=this
document.getElementById(this.tickerid).onmouseover=function(){scrollerinstance.mouseoverBol=1}
document.getElementById(this.tickerid).onmouseout=function(){scrollerinstance.mouseoverBol=0}
if (window.attachEvent) //Clean up loose references in IE
window.attachEvent("onunload", function(){scrollerinstance.tickerdiv.onmouseover=scrollerinstance.tickerdiv.onmouseout=null})
setTimeout(function(){scrollerinstance.animateup()}, this.delay)
}


// -------------------------------------------------------------------
// animateup()- Move the two inner divs of the scroller up and in sync
// -------------------------------------------------------------------

pausescroller.prototype.animateup=function(){
var scrollerinstance=this
if (parseInt(this.hiddendiv.style.top)>(this.visibledivtop+5)){
this.visiblediv.style.top=parseInt(this.visiblediv.style.top)-5+"px"
this.hiddendiv.style.top=parseInt(this.hiddendiv.style.top)-5+"px"
setTimeout(function(){scrollerinstance.animateup()}, 50)
}
else{
this.getinline(this.hiddendiv, this.visiblediv)
this.swapdivs()
setTimeout(function(){scrollerinstance.setmessage()}, this.delay)
}
}

// -------------------------------------------------------------------
// swapdivs()- Swap between which is the visible and which is the hidden div
// -------------------------------------------------------------------

pausescroller.prototype.swapdivs=function(){
var tempcontainer=this.visiblediv
this.visiblediv=this.hiddendiv
this.hiddendiv=tempcontainer
}

pausescroller.prototype.getinline=function(div1, div2){
div1.style.top=this.visibledivtop+"px"
div2.style.top=Math.max(div1.parentNode.offsetHeight, div1.offsetHeight)+"px"
}

// -------------------------------------------------------------------
// setmessage()- Populate the hidden div with the next message before it's visible
// -------------------------------------------------------------------

pausescroller.prototype.setmessage=function(){
var scrollerinstance=this
if (this.mouseoverBol==1) //if mouse is currently over scoller, do nothing (pause it)
setTimeout(function(){scrollerinstance.setmessage()}, 100)
else{
var i=this.hiddendivpointer
var ceiling=this.content.length
this.hiddendivpointer=(i+1>ceiling-1)? 0 : i+1
this.hiddendiv.innerHTML=this.content[this.hiddendivpointer]
this.animateup()
}
}

pausescroller.getCSSpadding=function(tickerobj){ //get CSS padding value, if any
if (tickerobj.currentStyle)
return tickerobj.currentStyle["paddingTop"]
else if (window.getComputedStyle) //if DOM2
return window.getComputedStyle(tickerobj, "").getPropertyValue("padding-top")
else
return 0
}










//Adjust this number to speedup or to slow down the scroller ~ Carsten  (name_of_message_array, CSS_ID, CSS_classname, pause_in_miliseconds)

new pausescroller(pausecontent, "pscroller1", "someclass", 1000)
document.write("<br />")