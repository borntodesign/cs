<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_a.css"/>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_side.css"/>

<title>Paris, France Convention May 22 &amp; 23, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created Nov 2007 LY-->


</head>

<body>
<img src="paris.gif" width="1024" height="100" alt="2009 Convention"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <h3><font color="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p0"/></b></font></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="location.xml"><xsl:value-of select="//lang_blocks/location"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>    
     
   </td>
	  
	  
    <td id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
            
            <div><img style="float:right" src="hotel.gif" height="200" width="200" alt="Hotel"/><xsl:value-of select="//lang_blocks/p1"/></div>
            <br/>
            <div><b><xsl:value-of select="//lang_blocks/p2"/><br/>
            <xsl:value-of select="//lang_blocks/p3"/><br/>
          <xsl:value-of select="//lang_blocks/p4"/><br/>
          <xsl:value-of select="//lang_blocks/p5"/></b></div>
          
          <p><xsl:value-of select="//lang_blocks/dis"/></p>
          <br/><br/><br/><br/><br/>
          <h3><xsl:value-of select="//lang_blocks/loc"/></h3>
          
          <p><img style="float: left" src="map.gif" height="256" width="313" alt="paris hotel map" right-padding="5px"/><br/>
           <xsl:value-of select="//lang_blocks/drive"/></p>
           <p><xsl:value-of select="//lang_blocks/shuttle"/><br/><xsl:value-of select="//lang_blocks/shuttle1"/></p>
            
            </td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>



