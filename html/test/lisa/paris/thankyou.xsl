<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_a.css"/>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/paris/paris_side.css"/>

<title>Paris, France Convention May 22 &amp; 23, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created Nov 2007 LY-->


</head>

<body>
<img src="paris.gif" width="1024" height="100" alt="2009 Convention"/> 
<table>
  <tr> 
   <td id="sidelinks"> 
     <h3><font color="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p0"/></b></font></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="location.xml"><xsl:value-of select="//lang_blocks/location"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>    
     
   </td>
	  
	  
    <td id="main" height="500">
      <table width="100%" cellpadding="2">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
                   
            
                       
            <p><xsl:value-of select="//lang_blocks/p1"/></p>
            
            
            
            
            
            <br/><br/><br/><br/><br/><br/><br/><br/>
            
                      </td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>