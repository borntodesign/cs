<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:include href="/xml/head-foot.xsl"/>
<!--xsl:include href="http://www.clubshop.com/xml/head-foot.xsl"/-->

<xsl:template match="/">	

<html>
<head>
<title><xsl:value-of select="//lang_blocks/common/t" /></title>
<meta name="description">
	<xsl:attribute name="content"><xsl:value-of select="//meta_description" /></xsl:attribute>
</meta>
<meta name="keywords">
	<xsl:attribute name="content"><xsl:value-of select="//meta_keywords" /></xsl:attribute>
</meta>

<link rel="stylesheet" href="/css/global.css" type="text/css" />
<script src="/js/miniLogin.js" type="text/javascript"></script>

<style type="text/css">
table.content{background-color: #EEF3F7;}

.x1 {font-size: 110%; font-weight: bold;}
.x2 {font-size: 100%; font-weight: bold;}

table#dsp td{text-align: left; padding: 0.5em; vertical-align: top; background-color: #FFFFFF;}

div#malllist {background-color: #BED0EF;
text-align:center;
}

p.rewards{font-family: Trebuchet MS, Arial, Verdana, Helvetica,sans-serif;
font-size: 15px;
color: #000066;}

h4 {
	background-color: #000066;
	color: #FFFFFF;
	margin: 0 0 0 0;
	padding: 0 5px  0 5px;
	font: bold  12px Arial, Verdana, sans-serif;
	border-bottom: 1px solid #999999;
	background-position: left;
}

h5 {font-family: Trebuchet MS, Arial, Verdana, Helvetica,sans-serif;
font-size: 15px;
color: #000066;
}

div#mallstores{text-align:center;}
hr{ color:  #BED0EF; }

</style>



</head>
<body>
<div style="width:765px; margin-right:auto;margin-left:auto; border:1px solid navy; padding: 2px;">
<xsl:call-template name="head" />


<div id="malllist"><xsl:call-template name="mall_list" /></div>

           
<table width="100%" border="0" cellspacing="5" id="dsp">
              <tr><td>
              
              <table>
              <tr>
                <td style="width:360px"><h4><xsl:value-of select="//lang_blocks/rw" /></h4>
<div style="border:2px solid #BED0EF; padding: 10px;"> 
                <img src="score!.png" height="108" width="108" alt="" />
<img src="clubrewards.png" style="margin-bottom: 50px;" width="223" height="61" alt="ClubShop ClubCash"/>
             <p><xsl:value-of select="//lang_blocks/cr" /></p>
<p style="text-align:right; font-size: 85%; font-weight:bold;"><a href="#" style="color:#b20000; text-decoration:none">Sign Up For Free
<img src="wt-arrow-red-dot.png" style="bottom: -5px; position:relative; border:0;" width="22" height="19" alt="" /></a>
</p></div>
                </td>
                <td><h4><xsl:value-of select="//lang_blocks/about" /></h4>
                <p><xsl:value-of select="//lang_blocks/aboutus" /></p>
                <p><xsl:value-of select="//lang_blocks/gbp" /></p></td>
                </tr>
                  
                  
                <tr>
                <td style="text-align:center"><h4><xsl:value-of select="//lang_blocks/merchants" /></h4>
                <img src="stores.gif" height="110" width="360" alt="ClubShop Mall Stores"/></td>
                
                <td><h4><xsl:value-of select="//lang_blocks/worlds" /></h4>
                <p><img src="tv.gif" height="20" width="20" alt="World's Greatest"/><xsl:value-of select="//lang_blocks/greatest" />
                <br/><a href="http://www.clubshop.com/worlds_greatest.html" target="_blank">
                <xsl:value-of select="//lang_blocks/video" /></a></p>
                
                <hr/>
                <h5><xsl:value-of select="//lang_blocks/specials" /></h5></td>
               
                </tr></table>

              
            
            </td></tr></table>
            
           <!-- <xsl:if test="not(//user/firstname)">
            <h2 style="margin-bottom: 0; font-family: serif;"><a href="/cgi/appx.cgi" onclick="window.open(this.href); return false;" class="a">
            <xsl:value-of select="//lang_blocks/sign_me_up" /></a></h2>
            </xsl:if>-->
  
<div align="center"><xsl:call-template name = "foot" /></div>
</div>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
