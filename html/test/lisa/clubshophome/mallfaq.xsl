<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	media-type="text/html"/>

<xsl:template match="/">
<b><content>
<!-- The HTML content for the right main part of the page goes here -->
<a name="top"/>

		<h3>
			<xsl:value-of select="//lang_blocks/p6"/>
		</h3>

<form action=""><select style="width:100%; font-size:85%"><xsl:attribute name="onchange">
document.location= '#'+this.options[this.selectedIndex].value;this.selectedIndex=0;
</xsl:attribute>
<option value=""><xsl:value-of select="//lang_blocks/select_option" /></option>
<xsl:for-each select = "//lang_blocks/*[@q != '']">
<option><xsl:attribute name="value"><xsl:value-of select="name()" /></xsl:attribute>
<xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">a</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>
<xsl:value-of select="." /></option>
</xsl:for-each>
</select></form>

<a name="p7"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p7"/></span><br />
	<xsl:value-of select="//lang_blocks/p8-a"/><xsl:text>
	</xsl:text><a class="a" href="shoppinginstructions.xml" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p8-b"/></a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p8-c"/></p>

<a name="p10"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p10"/></span><br />
	<xsl:value-of select="//lang_blocks/p11"/></p>

<p><xsl:value-of select="//lang_blocks/p12-a"/><xsl:text>
	</xsl:text><img src="/images/icon_mp.gif" width="20" height="20">
	<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/mpicon" /></xsl:attribute></img>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12-b"/><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p13"/><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p14-a"/><xsl:text>
	</xsl:text><a class="a" href="mailto:orders@dhs-club.com">orders@dhs-club.com</a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p14-b"/></p>

<p><xsl:value-of select="//lang_blocks/p15-a"/><xsl:text>
	</xsl:text><img src="/images/icon_mp.gif" width="20" height="20">
	<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/mpicon" /></xsl:attribute></img>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15-b"/></p>
<xsl:call-template name="top" />

<a name="p17"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p17"/></span><br />
	<xsl:value-of select="//lang_blocks/p18-a"/><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p18_2-a" /><xsl:text>
	</xsl:text><a class="a" href="/cgi/reward_center.cgi" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p18_2-b"/></a>
	<xsl:if test="//lang_blocks/p18_2-c != ''"><xsl:text>
		</xsl:text><xsl:value-of select="//lang_blocks/p18_2-c"/>
	</xsl:if>.</p>
<xsl:call-template name="top" />

<a name="p19"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p19"/></span><br />
	<xsl:value-of select="//lang_blocks/p19a"/></p>
<xsl:call-template name="top" />

<a name="p20"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p20"/></span><br />
	<xsl:value-of select="//lang_blocks/p21-a"/><xsl:text>
	</xsl:text><a class="a" href="/mall/missing_form.xml" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p21-b"/></a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p21-c"/></p>
<xsl:call-template name="top" />

<a name="p23"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p23"/></span><br />
	<xsl:value-of select="//lang_blocks/p24"/></p>
<ol>
	<li><xsl:value-of select="//lang_blocks/li-1-a"/><xsl:text>
		</xsl:text><a class="a" href="shoppinginstructions.xml" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/li-1-b"/></a><xsl:text>
		</xsl:text><xsl:value-of select="//lang_blocks/li-1-c"/>
	</li>
	<li><xsl:value-of select="//lang_blocks/li-2"/>
		(<a class="a" href="/manual/technical/cookie_help.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a17"/></a>)
	</li>
	<li><xsl:value-of select="//lang_blocks/li-3"/></li>
	<li><xsl:value-of select="//lang_blocks/li-4"/></li>
</ol>
<xsl:call-template name="top" />

<a name="p31"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p31"/></span><br />
	<xsl:value-of select="//lang_blocks/p32"/></p>
<xsl:call-template name="top" />

<a name="p33"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p33"/></span><br />
	<xsl:value-of select="//lang_blocks/p34"/></p>
<xsl:call-template name="top" />

<a name="p35"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p35"/></span><br />
	<xsl:value-of select="//lang_blocks/p36"/><xsl:text>
	</xsl:text><a class="a" href="https://www.clubshop.com/m-update.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a18"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p37"/></p>
<xsl:call-template name="top" />

<a name="p38"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p38"/></span><br />
	<xsl:value-of select="//lang_blocks/p39"/><xsl:text>
	</xsl:text><a class="a" href="/mall/testimonials.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a19"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40"/></p>
<xsl:call-template name="top" />

<a name="p41"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p41"/></span><br />
	<xsl:value-of select="//lang_blocks/p42"/></p>
<xsl:call-template name="top" />

<a name="p43"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p43"/></span><br />
	<xsl:value-of select="//lang_blocks/p44"/><xsl:text>
	</xsl:text><a class="a" href="/cgi/appx.cgi" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a20"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45"/></p>
<xsl:call-template name="top" />
	
<a name="p46"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p46"/></span><br />
	<xsl:value-of select="//lang_blocks/p47"/><xsl:text> </xsl:text>
	<a class="a" href="/mall/mall1.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a21"/></a>.</p>
<xsl:call-template name="top" />

<!--a name="p49"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p49"/></span><br />
	<xsl:value-of select="//lang_blocks/please_review"/><xsl:text> </xsl:text>
	<a class="a" href="/Direct_Percentage.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a_compplan"/></a><xsl:text> </xsl:text>
	<xsl:value-of select="//lang_blocks/learn_pp"/></p>
<xsl:call-template name="top" /-->

<!--a name="p52"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p52"/></span><br />
	<xsl:value-of select="//lang_blocks/p53"/><xsl:text> </xsl:text>
	<xsl:value-of select="//lang_blocks/please_review"/><xsl:text> </xsl:text>
	<a class="a" href="/Direct_Percentage.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a_compplan"/></a><xsl:text> </xsl:text>
	<xsl:value-of select="//lang_blocks/learn_refer"/></p>
<xsl:call-template name="top" /-->

<a name="p55"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p55"/></span><br />
	<xsl:value-of select="//lang_blocks/p56"/></p>
<xsl:call-template name="top" />

<a name="p57"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p57"/></span><br />
	<xsl:value-of select="//lang_blocks/p58"/><xsl:text> </xsl:text>
	<a class="a" href="/idlookup.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a24"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p59"/></p>
<xsl:call-template name="top" />

<a name="p60"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p60"/></span><br />
	<xsl:value-of select="//lang_blocks/p61"/></p>
<xsl:call-template name="top" />

<a name="p62"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p62"/></span><br />
	<a class="a" href="/mall_listing.xml">
		<xsl:value-of select="//lang_blocks/a25"/></a></p>

<!-- END of the content -->
</content>
<xsl:copy-of select="*" />
</b>
</xsl:template>


<xsl:template name="top">
<div class="gt"><a href="#top"><xsl:value-of select="//lang_blocks/top" /></a></div>
</xsl:template>
</xsl:stylesheet>