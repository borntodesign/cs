<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:include href="/xml/head-foot.xsl"/>

   <xsl:template match="/">

<html><head>
<title><xsl:value-of select="//lang_blocks/title" /></title>
<meta name="description">
	<xsl:attribute name="content"><xsl:value-of select="//lang_blocks/meta_description" /></xsl:attribute>
</meta>
<meta name="keywords">
	<xsl:attribute name="content"><xsl:value-of select="//lang_blocks/meta_keywords" /></xsl:attribute>
</meta>

<link href="/css/global.css" rel="stylesheet" type="text/css" />
<script src="/js/miniLogin.js" type="text/javascript"></script>

<style type="text/css">
body { background-color: white; }
h3 {
	color:#003366;
	font-size:medium;
	font-family: Trebuchet MS,Arial, Helvetica, sans-serif;
	border-bottom: 1px solid #003366;
	text-align: center;
}
p {
	font-family: Verdana,Geneva,Arial,Helvetica,sans-serif;
	font-size: small;
}
hr { color: Navy; }

a.a {
	text-decoration: underline;
	color: #0033CC;
}

a.a:hover {
	text-decoration: underline;
	color: #CC3300;}
	a.b {
	text-decoration: underline;
	color: #0033CC;
}

a.b:hover {
	text-decoration: underline;
	color: #CC3300;
}
ul,ol {
/*	margin-left: 3px;
	padding-left: 1em;*/
}

li {
	padding-bottom: 0.7em;
	font-family: Verdana,Geneva,Arial,Helvetica,sans-serif;
	font-size: small;
	font-weight: bold;
}
hr{color: #333399;}

div#malllist {background-color: #BED0EF;
text-align:center;}

</style>
</head>
<body>
<xsl:call-template name="head" />
<div id="malllist"><xsl:call-template name="mall_list" /></div>
	
<div><p>
			<h3><xsl:value-of select="//lang_blocks/p13"/></h3>
		</p>
		<p>	<img src="DickB_Pic.gif" alt="Dick Burke, CEO" width="82" height="115" style="float:left; margin-right: 2em; margin-bottom: 0.5em;" />
</p>
		<p>
			<xsl:value-of select="//lang_blocks/p3"/>
		</p>
		<p>
			<xsl:value-of select="//lang_blocks/p11"/>
		</p>
	</div>
		<p style="clear:both">
			<xsl:value-of select="//lang_blocks/p4"/>
		</p>


	<table width="96%" cellpadding="2">
	  <tr>
	    <td width="54%" height="29">
		<ul>
			<li><xsl:value-of select="//lang_blocks/p5"/></li>

			<li><xsl:value-of select="//lang_blocks/p6"/></li>

			<li><xsl:value-of select="//lang_blocks/p7"/></li>

			<li><xsl:value-of select="//lang_blocks/p8"/></li>

			<li><xsl:value-of select="//lang_blocks/p9"/></li>
		</ul>
	    </td>
	    <td width="46%">
			<img src="dhscheadquartersx.jpg" alt="DHS Club  HQ" width="263" height="197" />
	    </td>
	  </tr>	</table>
<p>
			<xsl:value-of select="//lang_blocks/p12"/>
		</p>
		<p>
			<xsl:value-of select="//lang_blocks/p10"/>
		</p>

	
<div align="center"><xsl:call-template name = "foot" /></div>
 </body></html>
 </xsl:template> </xsl:stylesheet> 