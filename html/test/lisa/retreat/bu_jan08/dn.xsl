<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<title>The DHS Club - Discount Home Shoppers' Club</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created May 2007 LY-->


<style type="text/css">

body {background-color: #0066600; color: black;}
td {border-width: 0; padding: 0;
font: small Verdana, Arial,san-serif;
}
li {12px Verdana, Arial, sans-serif; 
margin: 1em 2.5em;
}

ul{list-style: url(http://www.clubshop.com/retreat/orangebullet.gif);}


td#banner {border-bottom: 2px solid #FFFFFF; height: 30px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: #FFFFFF;
	color: black;
	padding: 1em;
	font: small Verdana, Arial,san-serif;
	vertical-align:top;
}
td#main h2 {
	font: bold 100% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #99CC33;
	vertical-align:top;
	text-align: center;
	color: #50260D;
}
td#main p {margin: 1em 2.5em;}
td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #99CC33;
	background-color: #006600;
}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style>
<style type="text/css">

/* menu styles */
td#sidelinks a {
	display: block;
	margin: 10px 2px 0 0;
	padding: 0px 0px 0px 4px;
	text-decoration: none;
	font: normal 12px Verdana, Arial, sans-serif;
	color: White;
	
}
td#sidelinks a:visited {color: #99CC33;
}
td#sidelinks h5 {background-color: #FFFFFF; color: #996600;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial, Verdana, sans-serif;
   border-bottom: 1px solid #6F7B33;
}
td#sidelinks h4 {
	background-color: #003366;
	color: #FFFFFF;
	margin: 0 0 0 0;
	padding: 0 0  0 8px;
	font: bold  small Arial, Verdana, sans-serif;
	border-bottom: 1px solid #999999;
	background-position: left;
   }
td#sidelinks a:hover {
	background-color: #FFFFCC;
	color: #000000;
	border-right: 3px solid #FFD942;
	padding-right: 3px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}

</style>
</head>

<body>
<img src="mast.gif" width="768" height="100" alt="2008 Builder Workshop"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <h3><font color="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p0"/></b></font></h3>
      <p><a href="dn_sched.xml" target="blank"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="dn_lodging.xml" target="_blank"><xsl:value-of select="//lang_blocks/lodging"/></a></p>
      <p><a href="dn_tickets.xml" target="_blank"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml" target="_blank"><xsl:value-of select="//lang_blocks/whos"/></a></p>

      <p><img src="divider.gif" width="150" height="2" alt="divider"/></p>
      
     
   </td>
	  
	  
    <td width="613" id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
            <p><b><xsl:value-of select="//lang_blocks/p1"/></b></p> 
            <img src="meeting_room.gif" height="176" width="265" alt="The Retreat at Duck's Nest Lake"/>
                     
           <p><xsl:value-of select="//lang_blocks/p2"/></p>
           <p><xsl:value-of select="//lang_blocks/p3"/></p>
           <p><xsl:value-of select="//lang_blocks/p4"/>
           <a href="http://www.copperhillcabins.com/" target="_blank"><xsl:value-of select="//lang_blocks/p6"/></a><xsl:value-of select="//lang_blocks/p7"/></p>
     
            
            
            
            
            
            
            <h2><xsl:value-of select="//lang_blocks/p8"/></h2>
            
            <p><b><u><xsl:value-of select="//lang_blocks/p9"/></u></b></p>
            <p><b><xsl:value-of select="//lang_blocks/p10"/></b><xsl:value-of select="//lang_blocks/p10a"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p11"/></b><xsl:value-of select="//lang_blocks/p11a"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p12"/></b><xsl:value-of select="//lang_blocks/p12a"/></p>
            
            
            <p><b><u><xsl:value-of select="//lang_blocks/p13"/></u></b></p>
            <p><b><xsl:value-of select="//lang_blocks/p14"/></b><xsl:value-of select="//lang_blocks/p15"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p16"/></b><xsl:value-of select="//lang_blocks/p17"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p18"/></b><xsl:value-of select="//lang_blocks/p19"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p20"/></b><xsl:value-of select="//lang_blocks/p21"/></p>
            
            
            <p><b><u><xsl:value-of select="//lang_blocks/p22"/></u></b></p>
            <p><b><xsl:value-of select="//lang_blocks/p23"/></b><xsl:value-of select="//lang_blocks/p24"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p25"/></b><xsl:value-of select="//lang_blocks/p26"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p27"/></b><xsl:value-of select="//lang_blocks/p28"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p29"/></b><xsl:value-of select="//lang_blocks/p30"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p31"/></b><xsl:value-of select="//lang_blocks/p32"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p33"/></b><xsl:value-of select="//lang_blocks/p34"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p35"/></b><xsl:value-of select="//lang_blocks/p36"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p37"/></b><xsl:value-of select="//lang_blocks/p38"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p39"/></b><xsl:value-of select="//lang_blocks/p40"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p41"/></b><xsl:value-of select="//lang_blocks/p42"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p43"/></b><xsl:value-of select="//lang_blocks/p44"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p45"/></b><xsl:value-of select="//lang_blocks/p46"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p47"/></b><xsl:value-of select="//lang_blocks/p48"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p49"/></b><xsl:value-of select="//lang_blocks/p50"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p51"/></b><xsl:value-of select="//lang_blocks/p52"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p53"/></b><xsl:value-of select="//lang_blocks/p54"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p55"/></b><xsl:value-of select="//lang_blocks/p56"/></p>
            
            
            
            
            
            
            
            <p><b><u><xsl:value-of select="//lang_blocks/p57"/></u></b></p>
            <p><b><xsl:value-of select="//lang_blocks/p58"/></b><xsl:value-of select="//lang_blocks/p59"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p60"/></b><xsl:value-of select="//lang_blocks/p61"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p62"/></b><xsl:value-of select="//lang_blocks/p63"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p64"/></b><xsl:value-of select="//lang_blocks/p65"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p66"/></b><xsl:value-of select="//lang_blocks/p67"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p68"/></b><xsl:value-of select="//lang_blocks/p69"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p70"/></b><xsl:value-of select="//lang_blocks/p71"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p72"/></b><xsl:value-of select="//lang_blocks/p73"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p74"/></b><xsl:value-of select="//lang_blocks/p75"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p76"/></b><xsl:value-of select="//lang_blocks/p77"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p78"/></b><xsl:value-of select="//lang_blocks/p79"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p80"/></b><xsl:value-of select="//lang_blocks/p81"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p82"/></b><xsl:value-of select="//lang_blocks/p83"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p84"/></b><xsl:value-of select="//lang_blocks/p85"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p86"/></b><xsl:value-of select="//lang_blocks/p87"/></p>
            
            
            
            
            
            
            <p><b><u><xsl:value-of select="//lang_blocks/p88"/></u></b></p>
            <p><xsl:value-of select="//lang_blocks/p89"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p90"/></b><xsl:value-of select="//lang_blocks/p91"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p92"/></b><xsl:value-of select="//lang_blocks/p93"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p94"/></b><xsl:value-of select="//lang_blocks/p95"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p96"/></b><xsl:value-of select="//lang_blocks/p97"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p98"/></b><xsl:value-of select="//lang_blocks/p99"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p100"/></b><xsl:value-of select="//lang_blocks/p101"/></p>
            <p><b><xsl:value-of select="//lang_blocks/p102"/></b><xsl:value-of select="//lang_blocks/p103"/></p>


            
            
            
            
            
            
            
            <div align="right"><img src="http://www.clubshop.com/images/valid-xhtml10-blue.png" alt="passed validation"/></div>
</td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>


