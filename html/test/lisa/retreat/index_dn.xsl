<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<title>The DHS Club - Discount Home Shoppers' Club</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created May 2007 LY-->


<style type="text/css">

body {background-color: #D4D400; color: black;}
td {border-width: 0; padding: 0;
font: small Verdana, Arial,san-serif;
}
li {12px Verdana, Arial, sans-serif; 
margin: 1em 2.5em;
}

ul{list-style: url(http://www.clubshop.com/retreat/orangebullet.gif);}


td#banner {border-bottom: 2px solid #FFFFFF; height: 30px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	
	font: small Verdana, Arial,san-serif;
	vertical-align:top;
}
td#main h2 {
	font: bold 105% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #FF9900;
	vertical-align:top;
	text-align: center;
	color: #50260D;
}
td#main p {margin: 1em 2.5em;}
td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #FF9900;
	background-color: #FFFFFF;
}
td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style>
<style type="text/css">

/* menu styles */
td#sidelinks a {
	display: block;
	margin: 10px 2px 0 0;
	padding: 0px 0px 0px 4px;
	text-decoration: none;
	font: normal 12px Verdana, Arial, sans-serif;
	color: Black;
	
}
td#sidelinks a:visited {color: #4B3E35;
}
td#sidelinks h5 {background-color: #FFFFFF; color: #996600;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial, Verdana, sans-serif;
   border-bottom: 1px solid #6F7B33;
}
td#sidelinks h4 {
	background-color: #003366;
	color: #FFFFFF;
	margin: 0 0 0 0;
	padding: 0 0  0 8px;
	font: bold  small Arial, Verdana, sans-serif;
	border-bottom: 1px solid #999999;
	background-position: left;
   }
td#sidelinks a:hover {
	background-color: #FFFFCC;
	color: #000000;
	border-right: 3px solid #FFD942;
	padding-right: 3px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}

</style>
</head>

<body>
<img src="mast.gif" width="768" height="60" alt="2007 Leadership Retreats"/> 
<table>
  <tr> 
    <td height="100%" id="sidelinks"> 
     <p><a href="/retreat/dn/index_dn.xml" target="_blank"><xsl:value-of select="//lang_blocks/p0"/></a></p>
      <p><a href="/retreat/dn/sched_dn.xml" target="_blank"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="/retreat/dn/lodging_dn.xml" target="_blank"><xsl:value-of select="//lang_blocks/lodging"/></a></p>
      <p><a href="/retreat/dn/tickets_dn.xml" target="_blank"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="/retreat/dn/whoscoming_dn.xml" target="_blank"><xsl:value-of select="//lang_blocks/whos"/></a></p>
      <p><img src="/retreat/dn/orangeline.gif" width="150" height="2" alt="divider"/></p>
      
     
   </td>
	  
	  
    <td width="613" id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
            <table><tr><td><img src="/retreat/dn/dn.gif" height="151" width="234" alt="The Retreat at Ducks' Nest Lake"/></td>
            <td><xsl:value-of select="//lang_blocks/p1"/></td>
            </tr></table> 
            <p><xsl:value-of select="//lang_blocks/p2"/></p>         
      
           <ul>
           
            <li><xsl:value-of select="//lang_blocks/p4"/></li>
            <li><xsl:value-of select="//lang_blocks/p5"/></li>
            <li><xsl:value-of select="//lang_blocks/p6"/></li>
            <li><xsl:value-of select="//lang_blocks/p10"/></li> 
            </ul>
     
     <table><tr><td><img src="/retreat/dn/conf_back.gif" height="176" width="265" alt="Conference Center"/></td><td><xsl:value-of select="//lang_blocks/p7"/><br/>
    <div align="center"> <a href="/test/lisa/retreat/bonus_dn.xml" onclick="window.open(this.href); return false;">
<img src="/retreat/bonus.gif" height="78" width="78" border="0" alt="Special Bonus"/></a></div></td></tr></table>
         <p><xsl:value-of select="//lang_blocks/p8"/></p> 
         <p><xsl:value-of select="//lang_blocks/p9"/></p>  
         <div align="center"><img src="/retreat/dn/redneck.gif" height="156" width="209" alt="redneck horseshoes"/></div>
            <p><xsl:value-of select="//lang_blocks/p11"/></p> 
            
            
            <br/><br/><br/>
            
            
            <div align="right"><img src="http://www.clubshop.com/images/valid-xhtml10-blue.png" alt="passed validation"/></div>
</td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>