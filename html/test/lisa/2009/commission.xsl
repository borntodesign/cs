<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>




   <xsl:template match="/">
    
         <html><head>
            <title>DHS Club Manual - Business</title>
<style type="text/css">

body {background-color: #EEf3F7; color: black;}
td {border-width: 0; padding: 0;
font: small Verdana, Arial,san-serif;
}

ul.sub{list-style: url(http://www.clubshop.com/test/lisa/manual/bluebullet.gif);}

ul{list-style: disc:}

li.sub{12px Verdana, Arial, sans-serif; 
margin: 1em 1em;
}

li{12px Verdana, Arial, sans-serif; 
margin: 1em 2em;
}

td#banner {border-bottom: 2px solid #FFFFFF; height: 30px; }
td#banner h1 {color:#0033CC;
   margin: 0; padding: 0.25em 0 0.125em 0;
   font: bold 125% Arial,san-serif; 
   }
td#main {
	background-color: transparent;
	color: black;
	padding: 1em;
	font: small Verdana, Arial,san-serif;
	vertical-align:top;
}
td#main h2 {
	font: bold 105% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	border-bottom: 1px solid #000099;
	vertical-align:top;
	text-align: center;
	color: #50260D;
}
td#main p {margin: 1em 2.5em;}


td#main h4 {
	font: bold 100% Verdana, Arial, Helvetica, sans-serif;
	margin: 0.5em 1em;
	padding: 0;
	vertical-align:top;
	color: #003399;
}




td#sidelinks {
	vertical-align: top;
	border-right: 1px solid #4794C4;
	background-color: #FFFFFF;
}

td#footer {background-color: #0033CC;
   border-top: 1px ;
   text-align: right; font-size: 85% ;
   padding-top: 0.03em; font-style: italic bold; color:#FFFFFF;}
</style><style type="text/css">

/* menu styles */
td#sidelinks a {
	display: block;
	margin: 10px 2px 0 0;
	padding: 0px 0px 0px 4px;
	text-decoration: none;
	font: normal 12px Verdana, Arial, sans-serif;
	color: Black;
	
}
td#sidelinks a:visited {color: #4B3E35;
}
td#sidelinks h5 {background-color: #FFFFFF; color: #996600;
   margin: 0 3px 0 0; padding: 1em 0 0; 
   font: bold small Arial, Verdana, sans-serif;
   border-bottom: 1px solid #6F7B33;
}
td#sidelinks h4 {
	background-color: #003366;
	color: #FFFFFF;
	margin: 0 0 0 0;
	padding: 0 0  0 8px;
	font: bold  small Arial, Verdana, sans-serif;
	border-bottom: 1px solid #999999;
	background-position: left;
   }
td#sidelinks a:hover {
	background-color: #DCE7EF;
	color: #000000;
	border-right: 3px solid #000066;
	padding-right: 3px;
	margin-right: 0;
	height : auto;
}
td#sidelinks a#comment {
   background-color: rgb(100%,92%,90%); color: black;
   border: 1px solid rgb(60%,50%,40%);
   border-right-width: 4px; padding-right: 7px;
   margin-right: 3px;}

</style>
</head><body>

<img src="/images/mast3.gif" width="768" height="60" alt="The DHS Club" />


<table><tr><td height="100%" id="sidelinks">
<p><xsl:value-of select="//lang_blocks/p1"/><br/><b><xsl:value-of select="//lang_blocks/p1a"/></b></p>
<p><b><xsl:value-of select="//lang_blocks/p2"/></b></p>
<p><img src="/manual/images/ltblueline.gif" width="150" height="2" alt="divider" /></p>

<p><a href="/manual/index.html" target="_blank"><xsl:value-of select="//lang_blocks/p3"/></a></p>
<p><a href="/vip/control/index.shtml" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></p>
<p><a href="/ppreport.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></p>
<p><a href="/manual/siteindex.html" target="_blank"><xsl:value-of select="//lang_blocks/p6"/></a></p>


</td>

<td id="main">

<h2><xsl:value-of select="//lang_blocks/p7"/></h2>
<h4><xsl:value-of select="//lang_blocks/p8"/></h4>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>

<p><b><xsl:value-of select="//lang_blocks/p11"/></b></p>

<ol>

<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>

</ol>

<p><xsl:value-of select="//lang_blocks/p14"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>

<hr/>


<p><b><xsl:value-of select="//lang_blocks/p16"/></b></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>

<p><b><xsl:value-of select="//lang_blocks/p18"/></b></p>


<p><b><xsl:value-of select="//lang_blocks/p19"/></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p20"/></p>


<p><b><xsl:value-of select="//lang_blocks/p21"/></b></p>

<p><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p23"/></p>

<p><b><xsl:value-of select="//lang_blocks/p24"/></b> <xsl:text> </xsl:text> <a href="https://www.moneybookers.com/app/?rid=175175" target="_blank"><xsl:value-of select="//lang_blocks/p25"/></a>.</p>



<p><b><xsl:value-of select="//lang_blocks/p26"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></p>

<p><xsl:value-of select="//lang_blocks/p28"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/intl_pay_agent.html"><xsl:value-of select="//lang_blocks/p29"/></a></p>

<p><b><xsl:value-of select="//lang_blocks/p30"/></b></p>

<p><b><xsl:value-of select="//lang_blocks/p31"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p32"/></p>
<p><b><xsl:value-of select="//lang_blocks/p33"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34"/></p>

<p><xsl:value-of select="//lang_blocks/p35"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/comm_prefs.cgi"><xsl:value-of select="//lang_blocks/p36"/></a></p>

<hr/>


<p><b><xsl:value-of select="//lang_blocks/p37"/></b></p>

<p><xsl:value-of select="//lang_blocks/p38"/></p>

<p><b><xsl:value-of select="//lang_blocks/p39"/></b></p>

<p><xsl:value-of select="//lang_blocks/p40"/></p>



<p><b><xsl:value-of select="//lang_blocks/p41"/></b></p>

<p><xsl:value-of select="//lang_blocks/p42"/></p>


<br/>

<div align="center">[<a href="#top"><xsl:value-of select="//lang_blocks/p43"/>]</a></div>




</td>











</tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>
