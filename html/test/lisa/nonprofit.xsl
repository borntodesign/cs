<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>


<title><xsl:value-of select="//lang_blocks/p0a"/></title>
<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />


</head>
<body>

<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
<tr>
<td valign="top">
<table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="12" valign="top">
<img src="/mall/images/headers/nonprofit-clubshop-1000x138.png" width="1000" height="138" alt="Non-Profit Fundraising" /></td>
</tr>


<tr>
<td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>
<td colspan="8" bgcolor="#FFFFFF">

<p class="style12"><xsl:value-of select="//lang_blocks/p0"/></p>



<p class="style14"><u><xsl:value-of select="//lang_blocks/p1"/></u></p>


<p><xsl:value-of select="//lang_blocks/p2"/></p>


<ol>
<li class="style14"><xsl:value-of select="//lang_blocks/p3"/></li>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<p><xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p7a"/></b></p>

<li class="style14"><xsl:value-of select="//lang_blocks/p8"/></li>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<p><xsl:value-of select="//lang_blocks/p12"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p13"/></b></p>
<p><xsl:value-of select="//lang_blocks/p14"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/></p>

</ol>
<p class="text_orange"><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/affinity/application.xml" class="nav_blue_line"><xsl:value-of select="//lang_blocks/p17"/></a></p>
<br/><br/>









</td>
 <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
 </tr>
 <tr> <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td></tr>
 <tr><td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td></tr>
 <tr>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
 <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
 </tr></table>
 
 </td></tr>
 </table>
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>