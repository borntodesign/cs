<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
         
         <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
         
<xsl:comment>[if IE]&gt;
	&lt;link rel="stylesheet" type="text/css" href="/css/gi-IE.css" /&gt;
&lt;![endif]</xsl:comment>

            <title><xsl:value-of select="//lang_blocks/p0"/></title>


<style type="text/css">

 
li {padding-bottom: 20px;
font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 


li li{list-style: uppercase;}



.ContactForm_TextField {
    border-color: #977339;
	background-color: #FFFFEA;
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}
.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif;
	  font-size: 13px;}

.topTitle{text-align:center; color: #5080B0; }
</style>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
<div class="six columns"></div>

</div></div>

<div class="container">
<div class="row">
<div class="twelve columns ">
<br />
<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>
<br />
<hr />

</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



</div>


<ol>

<li class="a"><span class="a"><b><xsl:value-of select="//lang_blocks/p2"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3"/></li>
<li class="a"><span class="a"><b><xsl:value-of select="//lang_blocks/p4"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5"/></li>
<li class="a"><span class="a"><b><xsl:value-of select="//lang_blocks/p6"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/></li>
<li class="a"><span class="a"><b><xsl:value-of select="//lang_blocks/p8"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9"/><br/>

<xsl:value-of select="//lang_blocks/p11"/></li>


<li class="a"><b><xsl:value-of select="//lang_blocks/p12"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>
<li><b><xsl:value-of select="//lang_blocks/p14"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/fees/full.xml" target="blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p17"/></a></li>

    <ol>
<span class="a"><b>A:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p19"/></span><br/><br/>
<span class="a"><b>B:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p21"/></span><br/><br/>
<span class="a"><b>C:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p22"/></span><br/><br/>
<span class="a"><b>D:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></span><br/><br/>
<span class="a"><b>E:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p24"/></span><br/><br/>
<span class="a"><b>F:</b></span> <xsl:text> </xsl:text><span class="text_normal"><xsl:value-of select="//lang_blocks/p25"/></span><br/><br/>
    </ol>






<li class="a"><b><xsl:value-of select="//lang_blocks/p26"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/></li>
<!--10/24/10
<li class="a"><b><xsl:value-of select="//lang_blocks/p28"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29"/></li>
-->

<li class="a"><b><xsl:value-of select="//lang_blocks/p30"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p32"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33"/></li>

<!--10/24/10 
<li class="a"><b><xsl:value-of select="//lang_blocks/p34"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p35"/><br/><br/>
-->

<li class="a"><b><xsl:value-of select="//lang_blocks/p75a"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p75"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p36"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p37"/></li>

<!--10/24/10
<li class="a"><b><xsl:value-of select="//lang_blocks/p38"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39"/><xsl:text> </xsl:text><a href="/manual/policies/vipsponsor.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p40"/></a>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p42"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p43"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p44"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45"/></li>
-->


<li class="a"><b><xsl:value-of select="//lang_blocks/p46"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47"/></li>
<li><b><xsl:value-of select="//lang_blocks/p48"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p49"/><br/>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p50"/></span></li>

<li class="a"><b><xsl:value-of select="//lang_blocks/p51"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p52"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p53"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p54"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p55"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p56"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p57"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p58"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p59"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p60"/></li>

<li><b><xsl:value-of select="//lang_blocks/p61"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p62"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cancel/" target="_blank" class="nav_footer_brown">http://www.glocalincome.com/cancel/</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p64"/><br/>

<br/><span class="text_normal"><xsl:value-of select="//lang_blocks/p66"/></span><br/><br/>

<span class="text_normal"><b><xsl:value-of select="//lang_blocks/p67"/></b><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cancel/" target="blank" class="nav_footer_brown">http://www.glocalincome.com/cancel/</a>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p68"/></span>
</li>

<li class="a"><b><xsl:value-of select="//lang_blocks/p69"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p70"/></li>
<li class="a"><b><xsl:value-of select="//lang_blocks/p71"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p72"/></li>
</ol>


</div>
















<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
