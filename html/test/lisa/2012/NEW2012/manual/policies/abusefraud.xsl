﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Rewards Business Manual</title>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle">CLUBSHOP REWARDS MANUAL</h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
     
            <span class="style24 liteGrey"><xsl:value-of select="//lang_blocks/p6"/></span> <br/><br/>
            
            <span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p7"/></span> <br/><br/>
            
            
            
            <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p16"/></b></span>  
           <p class="text_normal"><xsl:value-of select="//lang_blocks/p8"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text><b><a href="/manual/forms/login_registration.xml" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p12"/></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text><b><a href="/manual/forms/transmission_agent.xml" target="_blank"><xsl:value-of select="//lang_blocks/p14"/></a></b></p>
            <p class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></p>
  
  <span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p17"/></b></span>  
  <p class="text_normal"><xsl:value-of select="//lang_blocks/p18"/></p>  
  <p class="text_normal"><b><xsl:value-of select="//lang_blocks/p19"/></b></p>  
  
  
  
  
  
  </div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>