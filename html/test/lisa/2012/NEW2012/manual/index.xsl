<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html>
         
         
         
         
         <head>
            <title><xsl:value-of select="//lang_blocks/p1"/></title>


<!-- Included CSS Files -->
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css"/>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css"/>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css"/>
	<![endif]-->
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!-- Included JS Files -->
<script src="http://www.clubshop.com/js/partner/foundation.js"></script>
<script src="http://www.clubshop.com/js/partner/app.js"></script>
<script src="http://www.clubshop.com/js/partner/flash.js"></script>
<script src="http://www.clubshop.com/js/panel.js"></script>

<link href="/css/manual_2012.css" rel="stylesheet" type="text/css" />


</head>

      		
      		
      		
      		<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle">CLUBSHOP REWARDS MANUAL</h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

      		
      		
      		
      		
      		
      		
      		
      		
      		
      		 <h5 class="liteGrey"><xsl:value-of select="//lang_blocks/toc"/></h5>
      		 
      		 <br/>
      		 
        				<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p2"/></h5>
        				<div class="eleven columns offset-by-one">
        					<ul>
								<li class="half"><a href="/manual/membership/types.xml" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></li>
								<li class="half"><a href="/manual/membership/mission.xml" target="_blank"><xsl:value-of select="//lang_blocks/p34"/></a></li>
								<li class="half"><a href="/manual/membership/mlmtruth.xml" target="_blank"><xsl:value-of select="//lang_blocks/p35"/></a></li>
							</ul>
							</div>
        		   			
        			
        	
        	
        	
        
        	
        
        	
        	
        	
        	
         
        	
        	  			<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p7"/></h5>
        	  			<div class="eleven columns offset-by-one">
							<ul>
								<li class="half"><a href="../terms.xml" target="_blank"><xsl:value-of select="//lang_blocks/p8"/></a></li>
								<li class="a"><a href="http://www.glocalincome.com/CompensationPlan.xml" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a></li>
								<li class="half"><a href="business/appinfo.xml" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a></li>
								<li class="half"><a href="business/commission.xml" target="_blank"><xsl:value-of select="//lang_blocks/p11"/></a></li>
								<li class="half"><a href="business/fees.xml" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a></li>
								<li class="half"><a href="business/clubaccount_help.xml" target="_blank"><xsl:value-of select="//lang_blocks/p40"/></a></li>
								<li class="half"><a href="business/changes.xml" target="_blank"><xsl:value-of select="//lang_blocks/p13"/></a></li>
							</ul>
						</div>
		
		
		

						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p15"/></h5>
								<div class="eleven columns offset-by-one">
							<ul>
								<li class="half"><a href="http://www.clubshop.com/mall/" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a></li>
								<li class="half"><a href="http://www.clubshop.com/outletsp/bcenter.html" target="_blank"><xsl:value-of select="//lang_blocks/p18"/></a></li>
								<li class="half"><a href="http://www.clubshop.com/cs/rewardsdirectory/" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a></li>
								<li class="half"><a href="http://www.clubshop.com/cgi-bin/rd/14,3077,mall_id=1" target="_blank"><xsl:value-of select="//lang_blocks/p38"/></a></li>
							</ul>
							
							</div>







						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p20"/></h5>
							
							<div class="eleven columns offset-by-one">
							<ul>
								<li class="half"><a href="/manual/policies/abusefraud.xml" target="_blank">Abuse &amp; Fraud</a></li>
								<li class="half"><a href="/manual/policies/advertising.xml" target="_blank">Advertising</a></li>
								<li class="half"><a href="/manual/policies/advcoop.xml" taret="_blank">Advertising Cooperative (Co-op)</a></li>
								<li class="half"><a href="/manual/policies/application.xml" target="_blank">Application</a></li>
								<li><a href="/manual/policies/business_name.xml" target="_blank">Business Name</a></li>
								<li><a href="/manual/policies/cancellation.xml" target="_blank">Cancellation</a></li>
								<li><a href="/manual/policies/cb_activation.xml" target="_blank">ClubShop Rewards Card Activation Policy</a></li>
								<li><a href="/manual/policies/communications.xml" target="_blank">Communications Policy</a></li>
								<li><a href="/manual/policies/currency_conversion.xml" target="_blank">Currency Conversion</a></li>
								<li><a href="/manual/policies/intl_development.xml" target="_blank">International Development</a></li>
								<li><a href="/manual/policies/membership.xml" target="_blank">Membership</a></li>
								<li><a href="/manual/policies/mbr_processing.xml" target="_blank">Member Processing and Procedure</a></li>
								<li><a href="/manual/policies/name_usage.xml" target="_blank">Name Usage</a></li>
								<li><a href="/manual/policies/promotional_page.xml" target="_blank">Promotional Page</a></li>
								<li><a href="/manual/policies/privacy.xml" target="_blank">Privacy</a></li>
								<li><a href="/manual/policies/purchase_mbrs.xml" target="_blank">Purchasing Members</a></li>
								<li><a href="/manual/policies/quality_assurance.xml" target="_blank">Quality Assurance Policy</a></li>
								<li><a href="/manual/policies/return.xml" target="_blank">Return Policy for Promotional Printed Materials</a></li>
								<li><a href="/manual/policies/solicitation.xml" target="_blank">Solicitation</a></li>
							</ul>
							
							</div>
							

						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p21"/></h5>
						<div class="eleven columns offset-by-one">
						
							<ul>
								<li><a href="/manual/technical/aol_msn_browser.xml" target="_blank"><xsl:value-of select="//lang_blocks/p22"/></a></li>
								<li><a href="/manual/technical/browserhelp.xml" target="_blank"><xsl:value-of select="//lang_blocks/p23"/></a></li>
								<li><a href="/manual/technical/cookie_help.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24"/></a></li>
								<li><a href="/manual/technical/cookiesetup.xml" target="_blank"><xsl:value-of select="//lang_blocks/p37"/></a></li>
							</ul>
							</div>

	
						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p25"/></h5>
							
							<div class="eleven columns offset-by-one">
								<ul>
									<li><a href="http://www.clubshop.com/vip/ebusiness/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p26"/></a></li>
									<li><a href="http://www.glocalincome.com/help/partnerpool_training.xml" target="_blank"><xsl:value-of select="//lang_blocks/p41"/></a></li>
								</ul>
							</div>



						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p30"/></h5>
							<div class="eleven columns offset-by-one">
							<ul>
								<li><a href="/manual/training/interactive_online2.xml" target="_blank"><xsl:value-of select="//lang_blocks/p31"/></a></li>
								<li><a href="/manual/training/interactive_online_schedule.html" target="_blank"><xsl:value-of select="//lang_blocks/p32"/></a></li>
								<li><a href="/manual/training/conferences/index.html" target="_blank"><xsl:value-of select="//lang_blocks/p36"/></a></li>
								<li><a href="http://www.clubshop.com/training_schedule.html" target="_blank"><xsl:value-of select="//lang_blocks/p32a"/></a></li>

							</ul>
							</div>


						<h5 class="darkblue"><xsl:value-of select="//lang_blocks/p14"/></h5>
							<div class="eleven columns offset-by-one">
								<ul>
									<li><a href="/manual/forms/apps_forms.xml" target="_blank"><xsl:value-of select="//lang_blocks/p33"/></a></li>
								</ul>
							<br/>
							</div>
							
			</div>		</div>	
		

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

   
</div>


</body>
</html>
</xsl:template>
</xsl:stylesheet>