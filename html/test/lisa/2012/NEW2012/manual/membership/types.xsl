<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
   <xsl:template match="/">
   
   
   
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>CLUBSHOP REWARDS MANUAL</title>
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <script src="http://www.clubshop.com/js/panel.js"></script>
    

<style type="text/css">


.Header_Main {
	font-size: 14px;
	color: #0089B7;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.2em;
	font-family: verdana;
	text-transform: uppercase;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.darkblue {color: #011A52;}

.medblue {color:#004A95;}

.bold{font-weight:bold;}

.smaller{font-size: 13px;}

.blue_bullet{list-style-image: url(http://www.clubshop.com/images/partner/bullets/bluedot.png);
list-style-positioning: inside;

}

.half{list-style-image: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);
list-style-positioning: inside;}


.topTitle{text-align:center; color: #5080B0; }

.liteGrey{color:#b3b3b3;}

.centerbold {text-align: center;
			 font-weight: bold;
}





li.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style-image: url(http://www.clubshop.com/images/minions/icon_blue_bulleted.png);
}





li.b{font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 13px;
list-style-image: url(http://www.clubshop.com/images/minions/icon_gold_bulleted.png);
}


.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif;
	  font-size: 13px;}
	


.toggleLink {
font-style: italic;
font-size: 10px;
}



.style3 {
	font-size: 12px;
	font-family: verdana;
	color: #A91B07;
	font-weight:bold;
}
.style24 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-transform: uppercase;
	color: #2A5588;
}

.Text_Content_heighlight {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #0089B7;
	text-transform: uppercase;
	font-weight:bold;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.ContactForm_TextField {
    border-color: #977339;
	background-color: #FFFFEA;
	color: #000066;
	font-family: verdana;
	font-size: 8pt;
	text-transform: uppercase;
	
}



</style>

</head>
<html>
<body>


<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
<div class="six columns"></div>

</div></div>

<div class="container">
<div class="row">
<div class="twelve columns ">
<br />
<h4 class="topTitle">CLUBSHOP REWARDS MANUAL</h4>
<br />
<hr />

</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


			<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span><br/><br/>
			<span class="style24"><xsl:value-of select="//lang_blocks/p2"/></span>
			<br/><br/>

			<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

				<tr align="center">
    				<td bgcolor="#FFFFFF" class="style3" colspan="1" rowspan="2"><xsl:value-of select="//lang_blocks/p3"/><br/><img src="http://www.glocalincome.com/images/icons/black_arrow_down.gif" height="37" width="9" alt="black arrow down"/></td>
    				<td bgcolor="#FFFFFF" class="style3" colspan="6"><xsl:value-of select="//lang_blocks/p4"/></td>
   				</tr>
  
  				<tr align="center">
     				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p5"/></b></td>
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p12"/></b></td>
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p6"/></b></td>
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p7"/></b></td>
        			<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p11"/></b></td>
  				</tr>
  
  
  				<tr align="center">
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p5"/></b></td>
       				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
       				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
       				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
   					<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
  				</tr>
  
  
  
    			<tr align="center">
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p12"/></b></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
   					<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
   				    <td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
  				</tr>
  
  
  			   <tr align="center">
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p6"/></b></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
      				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
   				    <td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
  			  </tr>
    
 
  				<tr align="center">
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p7"/></b></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
   				    <td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
  				</tr>
  
  
    			<tr align="center">
    				<td class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p11"/></b></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
   					<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p9"/></td>
    				<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
   		   			<td class="a"><xsl:value-of select="//lang_blocks/p10"/></td>
  				</tr>
  

  			</table>
					<br/><br/>

<div class="row">
	
	<div class="twelve columns">
			<span class="Text_Content_heighlight"><xsl:value-of select="//lang_blocks/p13"/></span><br/><br/>

</div>
</div>



			<img src="http://www.clubshop.com/images/minions/member_smaller.png" height="46" width="25" class="img_heads" alt="member"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/pmember"/></span>
				
				
				<div class="row">
	
	<div class="eleven columns offset-by-one">
				
				
				<ul>
					<li class="a"><xsl:value-of select="//lang_blocks/pmember1"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/pmember2"/></li>
						<ul>
						<li class="b"><xsl:value-of select="//lang_blocks/pmember3"/></li>
						<li class="b"><xsl:value-of select="//lang_blocks/pmember4"/></li>
						</ul>
				</ul>


</div>
</div>



			<img src="http://www.clubshop.com/images/minions/affinity_small.png" height="38" width="40" class="img_heads" alt="affinity group"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/paffinity"/></span>

				<div class="row">
				<div class="eleven columns offset-by-one">



				<ul>
					<li class="a"><xsl:value-of select="//lang_blocks/paffinity1"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/paffinity2"/></li>
				</ul>
				
				</div>
				</div>
				
				
				
				
				
				
				
				
				
				
				
		<br/>

			<img src="http://www.clubshop.com/images/minions/merchant_small.png" height="43" width="45"  class="img_heads" alt="merchant"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/pmerchants"/></span>
				
				
				<div class="row">	
				<div class="eleven columns offset-by-one">
				
				
				
				<ul>
					<li class="a"><xsl:value-of select="//lang_blocks/pmerchants1"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/pmerchants2"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/pmerchants3"/></li>
						
					
						
						<ul>
						<li class="b"><xsl:value-of select="//lang_blocks/pmerchants4"/></li>
						<li class="b"><xsl:value-of select="//lang_blocks/pmerchants5"/></li>
						<li class="b"><xsl:value-of select="//lang_blocks/pmerchants6"/></li>
						</ul>
						
				</ul>
				
				
				</div>
				</div>
		<br/>
			<img src="http://www.clubshop.com/images/minions/affiliate_smaller.png" height="46" width="25" class="img_heads" alt="affiliate"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/paffs"/></span>
				
				
				<div class="row">	
				<div class="eleven columns offset-by-one">
				
				
				<ul>
					<li class="a"><xsl:value-of select="//lang_blocks/paffsa"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/paffsb"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/paffsc"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/paffsd"/></li>
				</ul>
				
				
				</div>
				</div>

		<br/>
			<img src="http://www.clubshop.com/images/minions/partner_smaller.png" height="46" width="25" class="img_heads" alt="partner"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/pparts"/></span>
			
			<div class="row">	
				<div class="eleven columns offset-by-one">
				<ul>
					<li class="a"><xsl:value-of select="//lang_blocks/pparts1"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/pparts2"/></li>
					<li class="a"><xsl:value-of select="//lang_blocks/pparts3"/></li>
				</ul>


				</div>
				</div>



</div>
</div>





<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>