<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            
         
         
         
         
            <title>ClubShop Rewards Business Manual</title>
            
            <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<style type="text/css">
.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.darkblue {color: #011A52;} .medblue {color:#004A95;} .bold{font-weight:bold;} 

.smaller{font-size: 13px;} 

.blue_bullet{list-style: url(http://www.clubshop.com/images/partner/bullets/bluedot.png);} 

.half{list-style: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);} 

.topTitle{text-align:center; color: #5080B0; } .liteGrey{color:#b3b3b3;} 

.centerbold {text-align: center; font-weight: bold; } 

.toggleLink { font-style: italic; font-size: 10px; } 

.Header_Main { font-size: 14px; color: #0089B7; font-weight:bold; line-height:20px; letter-spacing:0.2em; font-family: verdana; text-transform: uppercase; } 

.Text_Content_heighlight { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: #0089B7; text-transform: uppercase; font-weight:bold; } 

.ContactForm_TextField {
    border-color: #977339;
	background-color: #FFFFEA;
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}
.style3 { font-size: 12px; font-family: verdana; color: #A91B07; font-weight:bold; } 

.styleorange { font-size: 12px; font-family: verdana; color: #FF9C04; font-weight:bold; } 

.style24 { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; text-transform: uppercase; color: #2A5588; } 

li.a {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; list-style: url(http://www.clubshop.com/images/minions/icon_blue_bulleted.png);} 

li.b { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; list-style: url(http://www.clubshop.com/images/minions/icon_gold_bulleted.png);} 

td.a {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; background-color: #FFFFFF;} 

.img_heads {margin-bottom: -10px;}

</style> 


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle">CLUBSHOP REWARDS MANUAL</h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p2"/></span><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p1"/></b></span>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p3"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p4"/></p>
<span class="Header_Main"><xsl:value-of select="//lang_blocks/p5"/></span><br/><br/>
<ul>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank"><xsl:value-of select="//lang_blocks/p6"/></a></li>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a></li>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank"><xsl:value-of select="//lang_blocks/p8"/></a></li>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a></li>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/mopi.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a></li>
<li class="text_normal"><a href="https://www.clubshop.com/cgi/mopi.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p11"/></a></li>
<li class="text_normal"><a href="/manual/forms/spousal_letter.html" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a></li>
<li class="text_normal"><a href="/manual/forms/joint_removal.html" target="_blank"><xsl:value-of select="//lang_blocks/p19"/></a></li>
<li class="text_normal"><a href="http://www.glocalincome.com/cancel/" target="_blank"><xsl:value-of select="//lang_blocks/p13"/></a></li>
</ul>


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p14"/></span><br/><br/>
<ul>
<li class="text_normal"><a href="http://www.clubshop.com/m-update.html" target="_blank"><xsl:value-of select="//lang_blocks/p15"/></a></li>
<li class="text_normal"><a href="http://www.clubshop.com/remove-me.xml" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a></li>
<li class="text_normal"><a href="http://www.clubshop.com/m-update.html" target="_blank"><xsl:value-of select="//lang_blocks/p17"/></a></li>
<li class="text_normal"><a href="http://www.clubshop.com/cgi/NetShopUpgrade.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p18"/></a></li>

</ul>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>





