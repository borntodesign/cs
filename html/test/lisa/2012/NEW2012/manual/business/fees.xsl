﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>




   <xsl:template match="/">
    
         <html><head>
         
         
         
         
         
            <title>ClubShop Rewards Business Manual</title>
            
            <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<style type="text/css">

.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.darkblue {color: #011A52;} .medblue {color:#004A95;} .bold{font-weight:bold;} 

.smaller{font-size: 13px;} 

.blue_bullet{list-style: url(http://www.clubshop.com/images/partner/bullets/bluedot.png);} 

.half{list-style: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);} 

.topTitle{text-align:center; color: #5080B0; } .liteGrey{color:#b3b3b3;} 

.centerbold {text-align: center; font-weight: bold; } 

.toggleLink { font-style: italic; font-size: 10px; } 

.Header_Main { font-size: 14px; color: #0089B7; font-weight:bold; line-height:20px; letter-spacing:0.2em; font-family: verdana; text-transform: uppercase; } 

.Text_Content_heighlight { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: #0089B7; text-transform: uppercase; font-weight:bold; } 

.ContactForm_TextField {
    border-color: #977339;
	background-color: #FFFFEA;
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}
.style3 { font-size: 12px; font-family: verdana; color: #A91B07; font-weight:bold; } 

.styleorange { font-size: 12px; font-family: verdana; color: #FF9C04; font-weight:bold; } 

.style24 { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; text-transform: uppercase; color: #2A5588; } 

li.a {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; list-style: url(http://www.clubshop.com/images/minions/icon_blue_bulleted.png);} 

li.b { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; list-style: url(http://www.clubshop.com/images/minions/icon_gold_bulleted.png);} 

td.a {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; background-color: #FFFFFF;} 

.img_heads {margin-bottom: -10px;}

</style> 


</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle">CLUBSHOP REWARDS MANUAL</h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p7"/></span>








<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/fees/full.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a>.</b></p>

<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p10"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p11"/></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p61"/><xsl:text> </xsl:text><a href="mailto:acf1@dhs-club.com?Subject=Credit_Card_Verification" class="nav_footer_brown">acf1@dhs-club.com</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p62"/></p>


<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p12"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p13"/></p>

<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p14"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p15"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p16a"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16b"/></p>


<ul>
<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p17"/></b></span><xsl:text> </xsl:text>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p18"/></span></li><br/>


<ul><li><xsl:value-of select="//lang_blocks/p44"/><br/>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p45"/><xsl:text> </xsl:text> <a href="/manual/forms/transmission_agent.xml" target="_blank"><xsl:value-of select="//lang_blocks/p46"/></a>
<br/><br/></span>
</li>
</ul>



<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p19"/></b></span><xsl:text> </xsl:text>
<span class="text_normal"><xsl:value-of select="//lang_blocks/p20"/></span></li>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p20a"/><xsl:text> </xsl:text><a href="http://www.alertpay.com/" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a>.</p>
<hr align="left" width="100%" size="1" color="#A35912"/>

<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p47"/></b></span><xsl:text> </xsl:text>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p48"/></p></li>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p49"/><xsl:text> </xsl:text><a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" target="_blank">https://www.ecocard.com/Registration.aspx?Referee=DHS</a>.</p>
<br/>

<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p50"/></b></span><xsl:text> </xsl:text> - <xsl:value-of select="//lang_blocks/p51"/></li>
<p >
<xsl:value-of select="//lang_blocks/p52"/> = NL11FTSB0243058780 <br/>
<xsl:value-of select="//lang_blocks/p53"/> = ChargeStream  Ltd <br/>
<xsl:value-of select="//lang_blocks/p54"/>= 83 Victoria Street,<br/>
<xsl:value-of select="//lang_blocks/p55"/>= London SW1H OHW<br/> 
<xsl:value-of select="//lang_blocks/p56"/>= UK <br/>
<xsl:value-of select="//lang_blocks/p57"/>= Fortis Bank<br/>
<xsl:value-of select="//lang_blocks/p58"/>= FTSBNL2R<br/>
<xsl:value-of select="//lang_blocks/p59"/>= Eco Card Account Number  <br/>                                       
                                          	


</p>




</ul>


<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p21"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p22"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<li class="text_normal"><xsl:value-of select="//lang_blocks/p24"/></li>
<li class="text_normal"><xsl:value-of select="//lang_blocks/p25"/></li>
<li class="text_normal"><xsl:value-of select="//lang_blocks/p26"/></li>
</ul>

<p class="text_normal"><xsl:copy-of select="//lang_blocks/p60/*|//lang_blocks/p60/text()"/></p>

<p class="text_normal"><xsl:value-of select="//lang_blocks/p27"/></p>


<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p28"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p29"/></p>


<hr align="left" width="100%" size="1" color="#A35912"/>


<p class="text_normal"><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p32"/></b></span></p>

<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/fees/full.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a>.</b></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p33"/></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p34"/></p>




<p class="text_normal"><xsl:value-of select="//lang_blocks/p35"/></p>


<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p36"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p37"/></p>

<p><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p38"/></b></span></p>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p39"/></p>

<p class="text_normal"><b><xsl:value-of select="//lang_blocks/p40"/></b><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p41"/></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42"/></p>


<hr align="left" width="100%" size="1" color="#A35912"/>
<p class="text_normal"><xsl:value-of select="//lang_blocks/p43"/><xsl:text> </xsl:text><a href="mailto:acf1@glocalincome.com" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p31"/></a></p>

</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
