Hello,
Thank you for submitting your Missing Order.  We have researched this order and have found that on the date of purchase, you were not logged into the ClubShop Mall.

Since our link was not used for this order Pixmania will not be crediting us for your sale. And therefore, we are unable to credit your sale.

In the future, please make sure you are logged into the ClubShop Mall before making your purchase in order to receive proper credit.

Best Regards,
Lisa Young
Sales Support Department
ClubShop.com