Hello,
Thank you for submitting the missing order form for your recent purchase at Dmail
The credit for your purchase has been posted to your account.  You can view it now by going to your Transaction Report.  Reward and Pay Points will appear after the next update. 

Thank you for shopping at The ClubShop Mall.  We appreciate your business.

Best Regards,
Lisa Young
Missing Orders Department
The ClubShop Mall