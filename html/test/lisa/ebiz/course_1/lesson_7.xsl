<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000">
<tr>
<td class="back">
<img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" />
</td>
</tr>
</table>
<table width="1000">
<tr>
<td>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<hr/>

<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>

<p><xsl:value-of select="//lang_blocks/p_1" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /></p>

		<ol>
		<li><xsl:value-of select="//lang_blocks/p6" /></li>
<li><xsl:value-of select="//lang_blocks/p_7" /></li>
<li><xsl:value-of select="//lang_blocks/p_11" /></li>
		</ol>
		<hr/>
		
<p><xsl:value-of select="//lang_blocks/p_12" /></p>
	
		<p><xsl:value-of select="//lang_blocks/p12" /></p>
		<p><sub><xsl:value-of select="//lang_blocks/p13" /></sub></p>
		<p><xsl:value-of select="//lang_blocks/p14" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16" /><xsl:text> </xsl:text>
		<xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text>
		<xsl:value-of select="//lang_blocks/p18" /><xsl:text> </xsl:text>
		<xsl:value-of select="//lang_blocks/p19" /></p>
		<p><sub><xsl:value-of select="//lang_blocks/p20" /></sub></p>

		<p><xsl:value-of select="//lang_blocks/p21" /></p>
		<p><xsl:value-of select="//lang_blocks/p22" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p23" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24" /></p>


		<p><xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p26" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27" /></p>

		<p><sub><xsl:value-of select="//lang_blocks/p28" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p29" /><xsl:text> </xsl:text> "!</sub></p>

		<p><xsl:value-of select="//lang_blocks/p30" /><xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p31" />" <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p32" /></p>
		<p><xsl:value-of select="//lang_blocks/p33" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p34" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p35" />
		"<xsl:value-of select="//lang_blocks/p36" /><xsl:text> </xsl:text>" <xsl:value-of select="//lang_blocks/p37" /> "<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38" />".</p>

		<p><b><xsl:value-of select="//lang_blocks/p39" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40" /></p>
		
<p><sub><xsl:value-of select="//lang_blocks/p_41" /></sub></p>
		
<p><xsl:value-of select="//lang_blocks/p_42" /></p>

		<p><xsl:value-of select="//lang_blocks/p43" /></p>

		<table border="1" border-color="#3F4164" align="center" cellpadding="5">
		<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
		<tr><td>
		<ol>
		<li><xsl:value-of select="//lang_blocks/p44" /><xsl:text> </xsl:text>
<b><xsl:value-of select="//lang_blocks/p_45" /></b>
		
		
		<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p46" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p47" />)</li>
		<li><xsl:value-of select="//lang_blocks/p48" /> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p49" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p50" />. <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51" /><xsl:text> </xsl:text>" <xsl:value-of select="//lang_blocks/p52" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p53" /> <xsl:text> </xsl:text>
		
<xsl:value-of select="//lang_blocks/p_54" /></li>
		<br/>
		<br/>
		<ol>
<xsl:value-of select="//lang_blocks/p_55" />
		</ol>




		<br/>
		<ol>
<xsl:value-of select="//lang_blocks/p_63" />
</ol>	
		
		</ol>

</td></tr>

</table>
<hr/>
<p><sub><xsl:value-of select="//lang_blocks/p67" /></sub></p>

<p><xsl:value-of select="//lang_blocks/p68" /></p>

<p><xsl:value-of select="//lang_blocks/p69" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p70" />".</p>

<p><a href="http://www.clubshop.com/vip/ebusiness/movies/course2_movie1.swf"><img src="http://www.clubshop.com/vip/ebusiness/images/chalkboard_staticsmall.jpg"/></a><xsl:text> </xsl:text>

<xsl:value-of select="//lang_blocks/p_71" />
		</p>

</td></tr></table>




            <hr />

            <div align="center">
               <p>
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
                  <xsl:value-of select="//lang_blocks/tr1index" />
               </a>

               | 
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_8.xml">
                  <xsl:value-of select="//lang_blocks/lesson" />
               </a>
               </p>
            </div>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>