<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
	<xsl:template match="/">
	
<html><head>
<base href="http://www.clubshop.com/" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="/lang_blocks/title" /></title>


<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
      <tr>
        <td valign="top"><table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="12" valign="top"><img src="/images/headers/plain-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" /></td>
          </tr>
          
          <tr>
            <td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>

            <td colspan="8" bgcolor="#FFFFFF">
           <span class="style12"><xsl:value-of select="/lang_blocks/title" /></span>
             
             <form action="/cgi/FormMail.cgi" method="post">
      
                <input name="recipient" type="hidden" value="l.young@dhs-club.com"/>
                <input name="email" type="hidden" value="apache@clubshop.com"/>
                <input name="required" type="hidden" value="VIP SPONSOR,Merchant ID,setup"/>
                <input name="sort" type="hidden" value="order:VIP SPONSOR,Merchant,setup"/>
                <input name="redirect" type="hidden" value="http://www.clubshop.com/test/lisa/merchant_setup_submitted.xml"/>
                <input name="subject" type="hidden" value="Merchant Set-Up ClubAccount"/>
                <input name="sort" type="hidden"/>
                <input name="env_report" type="hidden" value="HTTP_USER_AGENT"/>
             
                
                <table border="1" cellpadding="5">
                <tr> 
                  <td><div align="left"><b><xsl:value-of select="//lang_blocks/p1"/></b></div></td>
                  <td><input type="text" name="VIP SPONSOR" size="25"/></td>
                </tr>
                
                 <tr> 
                  <td><div align="left"><b><xsl:value-of select="//lang_blocks/p2"/></b></div></td>
                  <td><input type="text" name="Merchant ID" size="25"/></td>
                </tr>
                </table>
        <br/>
                <table cellpadding="5" border="1">
                <tr>
           
                <td><div align="left"><xsl:value-of select="//lang_blocks/p3"/><br/>(<xsl:value-of select="//lang_blocks/p4"/>)</div></td></tr>
                
                                
               <tr><td><input type="radio" name="setup" value="Authorize to deduct $50.00 from my Club Account for setup + Basic Package"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5"/></td></tr>
               <tr><td><input type="radio" name="setup" value="Authorize to deduct $129.00 from my Club Account for setup + Bronze Package"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p6"/></td></tr>
               <tr><td><input type="radio" name="setup" value="Authorize to deduct $199.00 from my Club Account for setup + Silver Package"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7"/></td></tr>
               <tr><td><input type="radio" name="setup" value="Authorize to deduct $249.00 from my Club Account for setup + Gold Package"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8"/></td></tr>
                 
                
               
                
                
                </table>
                <br/>
                
                <div align="center">
                      <input type="submit" name="Submit" value="Submit"/><xsl:text> </xsl:text><xsl:text> </xsl:text>
                      <input type="reset" name="Reset" value="Reset"/>
                    </div>
               
                </form>
                <br/>
                <p><b><xsl:value-of select="//lang_blocks/p9"/></b></p>
                
       
			
 
   

              
</td>
            <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
          </tr>
          <tr>
            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td>
          </tr>
          <tr>

            <td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td>
          </tr>
          <tr>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
          </tr>
        </table></td>
      </tr>

      
    </table>
    
</body>
</html>

	</xsl:template>

</xsl:stylesheet>