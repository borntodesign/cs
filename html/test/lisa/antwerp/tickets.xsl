<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>

<link rel="stylesheet" type="text/css" href="ams_a.css"/>
<link rel="stylesheet" type="text/css" href="ams_side.css"/>

<title>Antwerp Convention - May 15 &amp; 16, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created FEB 08 LY-->


</head>

<body>
<img src="antwerp.gif" width="1024" height="100" alt="Antwerp 2009"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <h3><b><xsl:value-of select="//lang_blocks/p0"/></b></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="location.xml"><xsl:value-of select="//lang_blocks/location"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>

      
      
     
   </td>
	  
	  
    <td id="main">
      <table cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
                   
            
            
            <h2><xsl:value-of select="//lang_blocks/forms"/></h2>          <br/>
                       
            <p><xsl:value-of select="//lang_blocks/ticket"/><br/><b><xsl:value-of select="//lang_blocks/ticket1"/><br/><xsl:value-of select="//lang_blocks/ticket1a"/></b><br/><xsl:value-of select="//lang_blocks/ticket2"/></p>
            
            <p><xsl:value-of select="//lang_blocks/dis"/></p>
            <p><xsl:value-of select="//lang_blocks/welcome"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/welcome2"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/welcome3"/></p>
      
            
            <hr/>
            
            
            
            <form action="/cgi/FormMail.cgi" method="post">
      
                <input name="recipient" type="hidden" value="acfspvr@dhs-club.com"/>
                <input name="email" type="hidden" value="apache@clubshop.com"/>
                <input name="required" type="hidden" value="name,id,email,role,exec,tickets"/>
                <input name="sort" type="hidden" value="order:name,id,email,role,exec,guest,tickets,credit_card_type,cardnumber,expiredate,cvv2,cardholder,cardholdersstaddr,cardholderscity,cardholdersst,country,cardholderszip,moneybookers,clubaccount"/>
                <input name="redirect" type="hidden" value="http://www.clubshop.com/conferences/antwerp/thankyou.xml"/>
                <input name="subject" type="hidden" value="Antwerp Convention"/>
                <input name="sort" type="hidden"/>
                <input name="env_report" type="hidden" value="HTTP_USER_AGENT"/>
               
                
                
                <table  border="1" cellpadding="0">
                <tr> 
                  <td><div align="left"><xsl:value-of select="//lang_blocks/p5"/></div></td>
                  <td><input type="text" name="name" size="25"/></td>
                </tr>
                <tr> 
                  <td><div align="left"><xsl:value-of select="//lang_blocks/p6"/></div></td>
                  <td><input type="text" name="id" size="25"/>
                    </td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p7"/></td>
                  <td><input type="text" name="email" size="25"/>
                    </td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p8"/></td>
                  <td><input type="text" name="role" size="25"/></td>
                </tr>
                <tr> 
                  <td><div align="left"><xsl:value-of select="//lang_blocks/p9"/></div></td>
                  <td> <input type="text" name="exec" size="25"/>
                    </td>
                </tr>
                <tr> 
                  <td><xsl:value-of select="//lang_blocks/p10"/></td>
                  <td><input type="text" name="guest" size="25"/></td>
                </tr>
                <tr> 
                <td><xsl:value-of select="//lang_blocks/p11"/></td>
                  <td><input type="text" name="tickets" size="25"/> 
                    </td>
                </tr>
              </table>
                    <table>
                      <tr>
                        <td><xsl:value-of select="//lang_blocks/p12"/><br/>
                    <br/>
                  <div align="center"> 
                      <table>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p13"/></td>
                          <td><input type="radio" name="credit_card_type" value="Mastercard"/>
                           <img src="http://www.clubshop.com/conferences/images/mastercard.gif" width="52" height="29" alt="Master Card" align="middle" border="0"/> 
                                  </td>
                          <td><input type="radio" name="credit_card_type" value="Visa"/>
                            <img src="http://www.clubshop.com/conferences/images/visa.gif" width="47" height="30" alt="visa" align="middle" border="0"/> 
                            </td>
                          <td>
                            <input type="radio" name="credit_card_type" value="Amex"/>
                                  <img src="http://www.clubshop.com/conferences/images/amex.gif" width="48" height="31" align="middle" alt="amex"/> 
                                  </td>
                          <td>
                            <input type="radio" name="credit_card_type" value="Discover Card"/>
                                  <img src="http://www.clubshop.com/conferences/images/discover.gif" width="48" height="31" align="middle" alt="discover"/> 
                                  </td>
                        </tr>
                      </table>
                    </div>
                    <center>
                      <table width="73%" align="center" >
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p14"/></td>
                          <td><input name="cardnumber"/></td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p15"/></td>
                          <td><input name="expiredate"/></td>
                        </tr>
                        <tr>
                             
                          <td><xsl:value-of select="//lang_blocks/p16"/>
                            <a href="https://www.clubshop.com/help/cvv2.html" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;">(help)</a></td>
                          <td><input name="cvv2"/></td>
               
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p17"/></td>
                          <td><input name="cardholder"/>
                            </td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p18"/></td>
                          <td> <input name="cardholdersstaddr"/> </td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p19"/></td>
                          <td> <input name="cardholderscity"/></td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p20"/></td>
                          <td><input name="cardholdersst"/></td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p27"/></td>
                          <td> <input name="country"/></td>
                        </tr>
                        <tr> 
                          <td><xsl:value-of select="//lang_blocks/p21"/></td>
                          <td> <input name="cardholderszip"/></td>
                        </tr>
                      </table>
                    </center>
                    <hr/>
                          <table width="100%" cellpadding="1">
                            <!--tr> 
                              <td><input type="checkbox" name="moneybookers" value="Moneybookers"/>
                                <xsl:value-of select="//lang_blocks/p22"/><br/>
                                <xsl:value-of select="//lang_blocks/p23"/></td>
                            </tr>
                            <tr><td><hr/></td></tr>
                            <tr>
                              <td><input type="checkbox" name="paypal" value="PayPal"/>
                                <xsl:value-of select="//lang_blocks/p24"/><br/>
                                <xsl:value-of select="//lang_blocks/p23"/></td>
                            </tr>
                            <tr><td><hr/></td></tr-->
                            <tr>
                              <td><input type="checkbox" name="clubaccount" value="I want to use my ClubAccount"/>
                                <xsl:value-of select="//lang_blocks/p25"/><br/>
                                <xsl:value-of select="//lang_blocks/p26"/></td>
                            </tr>
                            
                            
                          </table>
                          <p></p>
                    <hr/>
                          <center>
                          </center>
                    </td>
                </tr>
              </table>
                    <div align="center">
                      <input type="submit" name="Submit" value="Submit"/>
                      <input type="reset" name="Reset" value="Reset"/>
                    </div>
            </form>
            
            
            
            
            
            
            
       
            
            
            
            
            
            
            
            
            
            
            
            
            
            
</td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>





