<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    
        omit-xml-declaration="yes"
            media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/test/lisa/fl_retreat/flor_a.css"/>

<title>Advisory Board and New European Executive Director's Retreat</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created FEB 2008 LY-->


</head>

<body>
<img src="florence2.gif" width="768" height="100" alt="2008 Conference"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     
      <p><img src="sidepics.gif" height="1400" width="100" alt="villa pictures"/></p>
      
    
       
   </td>
    
	  
	  
    <td width="613" id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="center"><h1><xsl:value-of select="//lang_blocks/adv"/></h1></div>
            
            <h2><xsl:value-of select="//lang_blocks/dreams"/></h2>
            <br/>
            
            <div align="center"><b>Schedule of Events</b></div>
            <br/>
            
            <h3><xsl:value-of select="//lang_blocks/may6"/></h3>
            <p><xsl:value-of select="//lang_blocks/may6a"/> <b>Benedict Landerwyn</b> (Belgium), <b>Giel Verdaasdonk</b> (Holland),
            <b>Charles Ramos</b> (France) , <b>Sidi Zidani</b> (Switzerland) and <b>Franco Manieri</b> (Italy) join the Burke's, and Advisory Board Members <b>Harold Hill, Gino Descamps ,Fabrizio Perotti</b> and <b>Rens van Houte</b> for a welcome reception and a pizza party at the villa</p>
            
            
            <h3>May 7th (Wednesday)</h3>
            <ul><li><xsl:value-of select="//lang_blocks/p3"/></li>
            <li><xsl:value-of select="//lang_blocks/p4"/></li>
            <li><xsl:value-of select="//lang_blocks/p5"/></li>
            <li><xsl:value-of select="//lang_blocks/p6"/></li>
            </ul>
            
            
            
            <h3>May 8th (Thursday)</h3>
            
             <ul><li><xsl:value-of select="//lang_blocks/pth1"/></li>
            <li><xsl:value-of select="//lang_blocks/pth2"/></li>
            <li><xsl:value-of select="//lang_blocks/pth3"/></li>
            <li><xsl:value-of select="//lang_blocks/pth4"/></li>
            <li><xsl:value-of select="//lang_blocks/pth5"/></li>
            </ul>
            
            
                <h3>May 9th (Friday)</h3>
            
             <ul><li><xsl:value-of select="//lang_blocks/pf1"/></li>
            <li><xsl:value-of select="//lang_blocks/pf2"/></li>
            <li><xsl:value-of select="//lang_blocks/pf3"/></li>
            <li><xsl:value-of select="//lang_blocks/pf3a"/></li>
            <li><xsl:value-of select="//lang_blocks/pf4"/></li>
           
            </ul>
            
            
              <h3>May 10th (Saturday)</h3>
            
             <ul><li><xsl:value-of select="//lang_blocks/ps1"/></li>
            <li><xsl:value-of select="//lang_blocks/ps2"/></li>
            <li><xsl:value-of select="//lang_blocks/ps3"/></li>
            <li><xsl:value-of select="//lang_blocks/ps4"/></li>
           <li><xsl:value-of select="//lang_blocks/ps5"/></li>
           <li><xsl:value-of select="//lang_blocks/ps6"/></li>
           <li><xsl:value-of select="//lang_blocks/ps7"/></li>
           <li><xsl:value-of select="//lang_blocks/ps8"/></li>
            </ul>
            
                 <h3>May 11th (Sunday)</h3>
            
             <ul><li><xsl:value-of select="//lang_blocks/psu1"/></li>
            <li><xsl:value-of select="//lang_blocks/psu2"/></li>
            
            </ul>
            
            
            <h3>May 12th (Monday)</h3>
            
             <ul><li><xsl:value-of select="//lang_blocks/pm1"/></li>
            
            
            </ul>
            
             <h3>May 13th (Tuesday)</h3>
            
             <ul>
            <li><xsl:value-of select="//lang_blocks/ptu1"/></li>
            
            </ul>
            
            <p><xsl:value-of select="//lang_blocks/p7"/></p>
            
            
            </td>
        </tr>
      </table>
      <hr/>
      
      <table><tr><td>
      <tr><td><b>GROUP 1 ARRIVALS</b></td><td><b></b></td><td><b>May 6th</b></td></tr>
      <tr><td>Giel Verdaasdonk (2)</td><td>...</td><td>12:00</td></tr>
      <tr><td>Harold Hill</td><td>...</td><td>14:00</td></tr>
      <tr><td>Gino Descamps</td><td>...</td><td>14:10</td></tr>
      <tr><td>Benedict Landerwyn</td><td>...</td><td>14:10</td></tr>
      <tr><td>Rens van Houte (2)</td><td>...</td><td>Hotel</td></tr>
      <tr><td>Fabrizio Perotti</td><td>...</td><td>Drive</td></tr>
      <tr><td>Franco Manieri</td><td>...</td><td>Drive</td></tr>
      <tr><td>Sidi Zidani</td><td>...</td><td>Drive</td></tr>
      <tr><td>Charles Ramos</td><td>...</td><td>Train</td></tr>
            </td></tr></table>
            
            <p></p>
            
            
    <h3>Villa Information</h3>
    <p>We will be staying at the Agriturismo Petrognano in S.Ellero - Reggello (Florence - Italy)</p>
    <p>Agriturismo Petrognano is immersed in the heart of the Tuscan countryside amongst vineyards, olive groves and orchards. The complex is a recently restored and comfortably furnished antique farmhouse, composed of five apartments and surrounded by a splendid garden. Guests can enjoy a striking panorama, typical of the Tuscan hills, including a view of Vallombrosa and its antique abbey. A swimming pool and tennis court are at the guests disposal.
Petrognano is an ideal base for visting the Tuscan art cities: Florence (only 20 minutes away by car), Arezzo, Siena and the Chianti region...</p>
    
      <p><a href="http://www.agriturismopetrognano.com/" target="_blank">http://www.agriturismopetrognano.com</a></p>
      
      <p>Arrival time at the villa is 17:00 for those who will be driving in.  Directions can be found <a href="http://www.agriturismopetrognano.com/mappa.asp" target="_blank">here.</a></p> 
      <p>There is a train station at S. Ellero that is only 1.5km from Agriturismo Petrognano.</p>
      
     <table><tr><td>
      <tr><td><b><u>GROUP 1 DEPARTURES</u></b></td><td><b></b></td><td><b>May 8th</b></td></tr>
      <tr><td>Giel Verdaasdonk (2)</td><td>...</td><td>Florence Hotel</td></tr>
      <tr><td>Benedict Landerwyn</td><td>...</td><td>13:20</td></tr>
      <tr><td>Franco Manieri (2)</td><td>...</td><td>Drive 11:00</td></tr>
      <tr><td>Sidi Zidani</td><td>...</td><td>Drive 11:00</td></tr>
      <tr><td>Charles Ramos</td><td>...</td><td>Train 11:00</td></tr>
            </td></tr></table>
      
      
      <hr/>
      
      
      
      <table><tr><td>
      <tr><td><b><u>GROUP 2 ARRIVALS</u></b></td><td><b></b></td><td><b>May 8th</b></td></tr>
      <tr><td>Giuseppe Francavilla (3)</td><td>...</td><td>Drive</td></tr>
      <tr><td>Salvo Arena (2)</td><td>...</td><td>Train 15:46</td></tr>
    
      
            </td></tr></table>
            <hr/>
              <table><tr><td>
      <tr><td><b><u>GROUP 3 ARRIVALS</u></b></td><td><b></b></td><td><b>May 9th</b></td></tr>
      <tr><td>Giorgio Turri</td><td>...</td><td>Drive 16:00</td></tr>
      <tr><td>Amedeo Stasi</td><td>...</td><td>Drive 16:00</td></tr>
    
            </td></tr></table>
            
           
      <h3>Friday Night Dinner - May 9th </h3>
      <p>For those Executive Directors joining us for dinner, please plan to meet us at Agriturismo Petrognano at 18:30 for drinks and a meet and greet.</p>
  <p>Dinner will be at: 19:15<br/>Ristorante Pizzeria I' Tacca Via Montecristo,<br/> 2 S. Ellero Reggello (FI) <br/>50066 <br/>Telefono 055 860146.</p>
    <p>Driving from Florence: direction Pontassieve and then for Incisa,Arezzo until S.Ellero</p>
    
     <h3>Saturday, May 10th Conference in Florence</h3>
     
     
     <p>Departures on Saturday after the Conference: Giorgio Turri...Drive</p>
     
     <h3>Sunday, May 11th</h3>
    <table><tr><td>
      <tr><td><b>SUNDAY DEPARTURES</b></td><td><b></b></td><td><b>May 11th</b></td></tr>
      <tr><td>Giuseppe Francavilla (3)</td><td>...</td><td>Drive</td></tr>
      <tr><td>Salvo Arena (2)</td><td>...</td><td>Train 10:13</td></tr>
    <tr><td>Amedeo Stasi </td><td>...</td><td>Drive</td></tr>
    <tr><td>Will &amp; Carina Burke</td><td>...</td><td>7:15 am</td></tr>
            </td></tr></table>
            
            
      <h3>Monday, May 12th</h3>
    <table><tr><td>
      <tr><td><b>MONDAY DEPARTURES</b></td><td><b></b></td><td><b>May 12th</b></td></tr>
      <tr><td>Harold Hill</td><td>...</td><td>Train</td></tr>
      <tr><td>Rens van Houte (2)</td><td>...</td><td>16:30</td></tr>
      
      
      </td>
  </tr>
</table>


      <h3>Tuesday, May 13th</h3>
    <table><tr><td>
      <tr><td><b>TUESDAY DEPARTURES</b></td><td><b></b></td><td><b>May 13th</b></td></tr>
      <tr><td>Gino Descamps</td><td>...</td><td>6:50</td></tr>
      <tr><td>Fabrizio Perotti</td><td>...</td><td>Drive</td></tr>
      
      
      </td>
  </tr>
</table>
<h3>Apartments </h3>

<table border="1" cellpadding="5" cellspacing="5" bordercolor="#333333">
  <tr bgcolor="#FFFFCC"> 
    <td bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif">.</font></td>
    <td bgcolor="#FFFFCC"><font size="2" face="Arial, Helvetica, sans-serif"><b>BDRM</b></font></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Tuesday,<br/>6th </font></b></td>

    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Wednesday,<br/>7th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Thursday, <br/>8th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Friday,<br/>9th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Saturday,<br/>10th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Sunday,<br/>11th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Monday,<br/>12th</font></b></td>
    <td><b><font size="2" face="Arial, Helvetica, sans-serif">Tuesday,<br/>13th</font></b></td>
  </tr>
  <tr bgcolor="#66CC00"> 
    <td rowspan="2"><font size="2" face="Arial, Helvetica, sans-serif">Apt 1</font></td><td>1</td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Dick/Pat</font></td>
  </tr>
  <tr bgcolor="#009900"> 
    <td bgcolor="#66CC00">2</td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Will/Carina</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Will/Carina</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Will/Carina</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Will/Carina</font></td>
    <td bgcolor="#66CC00"><font size="2" face="Arial, Helvetica, sans-serif">Will/Carina</font></td>
    <td bgcolor="#66CC00">x</td>
    <td bgcolor="#66CC00">x</td>
    <td bgcolor="#66CC00">x</td>
  </tr>
  <tr bgcolor="#B9D5FF"> 
    <td rowspan="2"><font size="2" face="Arial, Helvetica, sans-serif">Apt 2</font></td>

    <td>1</td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
    <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Fab</font></td>
  </tr>
  <tr bgcolor="#009900"> 
    <td bgcolor="#B9D5FF">2</td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino &amp; Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino &amp; Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino&amp;Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino &amp; Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino &amp; Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino &amp; Harold</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino</font></td>
    <td bgcolor="#B9D5FF"><font size="2" face="Arial, Helvetica, sans-serif">Gino</font></td>
  </tr>
  <tr bgcolor="#FFCC00"> 
    <td rowspan="2"><font size="2" face="Arial, Helvetica, sans-serif">Apt 3</font></td>
    <td>1</td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td><font size="2" face="Arial, Helvetica, sans-serif">Rens (2)</font></td>
    <td>x</td>
  </tr>

  <tr bgcolor="#CC9900"> 
    <td bgcolor="#FFCC00">2</td>
    <td bgcolor="#FFCC00"><font size="2" face="Arial, Helvetica, sans-serif">Giel (2) </font></td>
    <td bgcolor="#FFCC00"><font size="2" face="Arial, Helvetica, sans-serif">Giel (2) </font></td>
    <td bgcolor="#FFCC00"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Giuseppe(3)</font></td>
    <td bgcolor="#FFCC00"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Giuseppe(3)</font></td>
    <td bgcolor="#FFCC00"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Giuseppe(3)</font></td>
    <td bgcolor="#FFCC00">x</td>
    <td bgcolor="#FFCC00">x</td>
    <td bgcolor="#FFCC00">x</td>
  </tr>
  <tr bgcolor="#CC9900"> 
    <td rowspan="2" bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Apt 4</font></td>
    <td bgcolor="#9999FF">1</td>
    <td bgcolor="#9999FF"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Franco (2) </font></td>
    <td bgcolor="#9999FF"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Franco (2)</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Salvo (2)</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Salvo (2)</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Salvo (2)</font></td>
    <td bgcolor="#9999FF">x</td>
    <td bgcolor="#9999FF">x</td>
    <td bgcolor="#9999FF">x</td>

  </tr>
  <tr bgcolor="#CC9900"> 
    <td bgcolor="#9999FF">2</td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Charles/Sidi/Benedict</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Charles/Sidi/Benedict</font></td>

    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">x</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Amedeo/Giorgio</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">Amedeo/Giorgio</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">x</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">x</font></td>
    <td bgcolor="#9999FF"><font size="2" face="Arial, Helvetica, sans-serif">x</font></td>

  </tr>
</table>





















      </td>
  </tr>
</table>


</body></html>


</xsl:template>
</xsl:stylesheet>


