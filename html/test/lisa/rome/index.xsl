<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>

<link rel="stylesheet" type="text/css" href="flor_a.css"/>
<link rel="stylesheet" type="text/css" href="flor_side.css"/>

<title>Rome Convention - May 29 &amp; 30, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created FEB 08 LY-->


</head>

<body>
<img src="rome.gif" width="1024" height="100" alt="Rome 2009"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <h3><b><xsl:value-of select="//lang_blocks/p0"/></b></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="location.xml"><xsl:value-of select="//lang_blocks/location"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>

      
      
     
   </td>
	  
	  
    <td id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
            <div><xsl:value-of select="//lang_blocks/p1"/></div>
            <div><xsl:value-of select="//lang_blocks/p2"/><img style="float: right" src="rome_night.gif" height="194" width="275" alt="rome at night"/></div> 
           <br/><div><xsl:value-of select="//lang_blocks/p3"/><br/><br/>
<xsl:value-of select="//lang_blocks/p4"/><br/><br/>
</div>
           
           <div><xsl:value-of select="//lang_blocks/p5"/></div>
           
           
           
          <ul >
          <li><xsl:value-of select="//lang_blocks/p6"/></li>
          <li><xsl:value-of select="//lang_blocks/p7"/></li>
          <li><xsl:value-of select="//lang_blocks/p8"/></li>
          <li><xsl:value-of select="//lang_blocks/p9"/></li>
          <li><xsl:value-of select="//lang_blocks/p10"/></li>
          
          </ul> 
            
             <p><xsl:value-of select="//lang_blocks/p11"/></p>       
            
            
            
            <div align="center"><img src="HI_hotel.gif" height="170" width="277" alt="Hotel"/>
            <br/><xsl:value-of select="//lang_blocks/p12"/></div>
            
            </td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>


