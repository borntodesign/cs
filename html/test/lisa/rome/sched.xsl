
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
<html><head>

<link rel="stylesheet" type="text/css" href="flor_a.css"/>
<link rel="stylesheet" type="text/css" href="flor_side.css"/>

<title>Rome Convention - May 29 &amp; 30, 2009</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- created FEB 08 LY-->


</head>

<body>
<img src="rome.gif" width="1024" height="100" alt="Rome 2009"/> 
<table>
  <tr> 
    <td id="sidelinks"> 
     <h3><b><xsl:value-of select="//lang_blocks/p0"/></b></h3>
      <p><a href="index.xml"><xsl:value-of select="//lang_blocks/pa"/></a></p>
      <p><a href="sched.xml"><xsl:value-of select="//lang_blocks/sched"/></a></p>
      <p><a href="location.xml"><xsl:value-of select="//lang_blocks/location"/></a></p>
      <p><a href="tickets.xml"><xsl:value-of select="//lang_blocks/tickets"/></a></p>
      <p><a href="whos.xml"><xsl:value-of select="//lang_blocks/whos"/></a></p>

      
      
     
   </td>
	  
	  
    <td id="main">
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            
            <h2><xsl:value-of select="//lang_blocks/leader"/></h2>
            <br/>
       
                
           <ul>
            <p><b><u><xsl:value-of select="//lang_blocks/p2"/></u></b></p>
            <li><xsl:value-of select="//lang_blocks/p3"/></li>
            <li><xsl:value-of select="//lang_blocks/p4"/></li>
            <li><xsl:value-of select="//lang_blocks/p5"/></li>
            <li><xsl:value-of select="//lang_blocks/p6"/></li>
            <li><xsl:value-of select="//lang_blocks/p7"/></li>
            <li><xsl:value-of select="//lang_blocks/p8"/></li>
            <li><xsl:value-of select="//lang_blocks/p9"/></li>
            <li><xsl:value-of select="//lang_blocks/p10"/></li>
            <li><xsl:value-of select="//lang_blocks/p11"/></li>
            <li><xsl:value-of select="//lang_blocks/p12"/></li>
            <li><xsl:value-of select="//lang_blocks/p13"/></li>
            <li><xsl:value-of select="//lang_blocks/p14"/></li>
         <li><xsl:value-of select="//lang_blocks/p17"/></li>
            <li><xsl:value-of select="//lang_blocks/p18"/></li>
            <li><xsl:value-of select="//lang_blocks/p19"/></li>
            </ul>
          
            <ul>
            <p><b><u><xsl:value-of select="//lang_blocks/p16"/></u></b></p>
            <li><xsl:value-of select="//lang_blocks/p20"/></li>
            <li><xsl:value-of select="//lang_blocks/p21"/></li>
            <li><xsl:value-of select="//lang_blocks/p22"/></li>
            <li><xsl:value-of select="//lang_blocks/p23"/></li>
            <li><xsl:value-of select="//lang_blocks/p24"/></li>
            <li><xsl:value-of select="//lang_blocks/p25"/></li>
            <li><xsl:value-of select="//lang_blocks/p26"/></li>
            <li><xsl:value-of select="//lang_blocks/p27"/></li>
            <li><xsl:value-of select="//lang_blocks/p28"/></li>
            <li><xsl:value-of select="//lang_blocks/p29"/></li>
            <li><xsl:value-of select="//lang_blocks/p30"/></li>
            <li><xsl:value-of select="//lang_blocks/p31"/></li>
            <li><xsl:value-of select="//lang_blocks/p32"/></li>
            <li><xsl:value-of select="//lang_blocks/p33"/></li>
            </ul>
            
            
            <br/>
            <div align="center"><img src="meeting_room.jpg" height="163" width="291" alt="meeting room"/></div>
            
            
            
            </td>
        </tr>
      </table>
      </td>
  </tr>
</table>




</body></html>


</xsl:template>
</xsl:stylesheet>



