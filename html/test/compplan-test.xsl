<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method = "xml" encoding="utf-8"
	doctype-public=""
standalone="yes"
	omit-xml-declaration="yes"
		media-type="text/html"/>
	
	<xsl:template match="/">	
		<html>
			<title></title>
			<body>
			<h4>my 50 list</h4>
			<table><tr><th>ID</th><th>FPP</th><th>PP</th></tr>
			<xsl:for-each select = "//m50/*">
			<tr><td><xsl:value-of select="@id" /></td><td><xsl:value-of select="@fpp" /></td><td><xsl:value-of select="@ppp" /></td></tr>
			</xsl:for-each>
			</table>
			
			<h4>my 25 list</h4>
			<table><tr><th>ID</th><th>FPP</th><th>Actual Pay</th></tr>
			<xsl:for-each select = "//m25/*">
			<tr><td><xsl:value-of select="@id" /></td><td><xsl:value-of select="@fpp" /></td><td><xsl:value-of select="@actual_pay" /></td></tr>
			</xsl:for-each>
			</table>
			
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>