<!DOCTYPE HTML >
<?php
//require_once 'Clubshop/G_S.php';
require 'Clubshop/NegotiateByLanguage.php';
require 'Clubshop/Session/Session.php';

//$gs = new \Clubshop\G_S();

$negotiator = new \Clubshop\NegotiateByLanguage();
$accept_language = $negotiator->accept_language_header; //$gs->setDefault($_SERVER['HTTP_ACCEPT_LANGUAGE'], 'en');

echo 'accept-language: ' . $accept_language . '<br />';

echo 'lang prefs: ' .var_dump($negotiator->language_prefs()) . '<br />';

echo 'best option: ' . $negotiator->bestLanguage() . '<br/>';

echo 'best option short: ' . $negotiator->bestLanguageShort() . '<br/>';

$fn = $negotiator->bestCandidateFilename('_test.xml');

echo 'best filename: ' . ($fn ? $fn : 'none found') . '<br />';

echo 'Locale by locale_accept_from_http: ' . locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']) . '<br />';

echo 'Best supported locale based upon lang prefs: ' . $negotiator->bestSupportedLocale() . '<br />';

echo 'Best supported locale for "it": ' . $negotiator->bestSupportedLocale('it') . '<br />';

echo 'Best supported locale based upon lang prefs with locales specified "it-it,fr_fr,ar_ar, en-us": ' . $negotiator->bestSupportedLocale(null, array('it-it','fr-ch','ar-ar', 'en-us')) . '<br />';

$cs_session = new Clubshop\Session\Session();
$_SESSION['memberinfo'] = $cs_session->meminfoFromCookie();
