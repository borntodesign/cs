<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />





<title>Print Out ClubShop Rewards Card</title>



<link href="http://www.clubshop.com/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto; width: 1000px;">

<tbody>

<tr>

<td valign="top">

<table border="0" cellpadding="0" cellspacing="0" id="Table_01" width="1000">

<!--DWLayoutTable-->

<tbody>

<tr>

<td colspan="12" valign="top"><img height="138" src="../../images/headers/getcards-clubshop-1000x138.png" width="1000" /></td>

</tr>

<tr>

<td colspan="2" style="background-image:url(http://www.clubshop.com/images/bg/bg_left.png)"></td>

<td bgcolor="#ffffff" colspan="8" valign="top">

<div align="center"><span class="style12">PRINT OPTIE 3 INSTRUCTIES</span></div>

<hr />

<p>Na deze instructies gelezen en afgedrukt te hebben, moet u wel terug naar de ClubShop Rewards kaarten print pagina en de link "Print kaarten" aanklikken. U hebt gekozen voor de optie "Print kaarten om te verdelen". U kunt nu tien kaarten uitprinten om te verdelen aan prospectieve leden.</p>

<p>Daar deze kaarten gelinkt zijn aan uw ID nummer, wordt u de sponsor van de nieuwe kaarthouders. Gebruik deze kaarten niet voor uzelf. Deel ze allemaal uit. ClubShop Rewards kaarten kunnen niet her-linkt of geherprogrammeerd worden met een ander ID nummer oen kunnen dus niet aan een ander lid toegekend worden of door een ander lid verdeeld worden.</p>

<p>U bent de sponsor van ALLE ontvangers van deze ClubShop Rewards kaarten.</p>

<p>Nadat u bent terug gekeerd naar de "ClubShop Rewards kaarten print pagina" en de link "PRINT KAARTEN" hebt aangeklikt, krijt u dit te zien:</p>

<p>U zult zien: 10 ClubShop Rewards kaarten om te verdelen.</p>

<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="18%">

<tbody>

<tr>

<td bgcolor="#ffffff" width="55%"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff" width="45%"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

</tbody>

</table>

<p><span class="style14">Wat hebt u nodig om te printen</span></p>

<p>Om ClubShop Rewards kaarten te printen stellen wij het volgende voor:</p>

<ul>

<li value="0">Avery business kaarten (wit)</li>

<li value="0">8371 voor Inkjet Printers of</li>

<li value="0">5371 voor Laser Printers of</li>

<li value="0">65# wit papier</li>

</ul>

<p><span class="style14">Print Set Up</span></p>

<ol start="1">

<li value="0">Op de Pagina-instelling van uw browser (Bij de meeste browsers: klik Bestand - Pagina-instelling.</li>

<li value="0">Tekst passend maken - uitschakelen, als uw browser deze optie biedt.</li>

<li value="0">Alle kop- en voetteksten uitschakelen/leegmaken.</li>

<li value="0">Marges - zo klein mogelijk - minimaliseren.</li>

</ol>

<p>We hebben een <a class="nav_blue_line" href="http://www.clubshop.com/cgi/cbcards.cgi/c68365098d7dc0155ed80b7c8f7bcf16/3,3,3,3,3,3,3,3,3,3">TEST PRINT PAGINA</a> om te oefenen.</p>

<p><img alt="arrow" height="28" src="images/arrow29.jpg" width="34" />&nbsp; <span class="style24">Besparingstip:</span> Gebruik gewoon wit papier en enkel zwarte inkt voor een proefdruk.</p>

<p>Lijn de kaart linksboven goed uit: *Dit is de belangrijkste&nbsp;richtlijn om de rest van de kaarten in nette lijnen en mooi onder elkaar te zetten.</p>

<ol start="1">

<li value="0">Maak gebruik van het 'Afdrukvoorbeeld' (Bestand - Afdrukvoorbeeld), indien beschikbaar, om de kaart linksboven te bekijken en eventueel aan te passen.</li>

<li value="0">Indien geen "Afdrukvoorbeeld" beschikbaar, print een proefdruk/test pagina&nbsp;en gebruik, indien beschikbaar, de "klad" mode (Snel/besparend afdrukken) in uw printer Eigenschappen om het testen sneller en besparend te laten verlopen.</li>

<li value="0">Bij gebruik van Avery kaartvellen: als de kaart linksboven niet is uitgelijnd in de sectie linksboven van de Avery template, ga terug naar de print pagina en klik "Control Panel" en gebruik de controls om de kaart linksboven in de juiste positie te zetten.</li>

<li value="0">Op het Print Control Panel, past&nbsp;u de 'Top' en 'Left' marges aan totdat de kaart linksboven, zich in het midden van het veld linksboven bevindt, daarna gaat u terug naar stap 1 hierboven.</li>

</ol>

<p><span class="style24">NOTA bij aanpassingen: 9 (pixels) in het print "Control Panel" pop-up venster evenaart 1/8 inch, hetzij 3 mm.</span></p>

<p><span class="style14">De rest van de kaarten uitlijnen:</span></p>

<ol start="1">

<li value="0">Klik op "Afdrukvoorbeeld" om de stand van de andere kaarten te bekijken.</li>

<li value="0">Staan&nbsp;de kaarten&nbsp;bijna in het midden van hun sectie of bij gebrek van de optie afdrukvoorbeeld, print dan eerst nog een proefafdruk door de snelle en besparende "klad" mode van uw printer te gebruiken.</li>

<li value="0">Staat de rest van de kaarten niet in het midden van hun individuele sectie op uw Avery template, ga dan via deze link terug naar het print '<a class="nav_blue_line" href="http://www.clubbucks.com/printcontrol.html">Control Panel</a>' venster.</li>

<li value="0">Pas de 'Center' en 'Cell Spacing' marges zodanig aan om de andere kaarten in het midden van hun sectie te laten passen en ga dan terug naar stap 1 hierboven.</li>

</ol>

<p>Eenmaal de setting voor de achterkant van de kaart goed is gezet, dient u het niet nogmaal te doen voor de voorkant, daar "cookies" uw setting onthouden, dus als u achteraf nogmaals de ClubShop Rewards Print Pagina wenst te gebruiken, staan uw settings nog op punt. Mocht u zo nu en dan om welke reden dan ook uw cookies verwijderen, dan kunt u beter iedere setting noteren voor later gebruik want zonder cookies dient u bij iedere ClubShop Rewards kaart printing de settings&nbsp;opnieuw manueel in te voeren.</p>

<p>Wanneer u tevreden bent&nbsp;over uw proefafdruk, klik dan <a class="nav_blue_line" href="http://www.clubshop.com/getcards.html">HIER</a> om terug te gaan naar de ClubShop Rewards Card Print Pagina. Vergewis u dat uw printer voldoende gevuld is met visitekaartjes of het nodige papier bevat en vervang eventueel uw inktpatronen.</p>

<p>Nadat u de rugzijde van de kaart hebt geprint, dient de voorzijde nog geprint te worden. U dient dus de geprinte papieren omgedraaid terug in de papierlade te plaatsen. U dient weliswaar de richtlijnen van uw printer te volgen en desnoods, VOOR u de kaarten daadwerkelijk print, een voor+achterzijde-proefdruk te doen.</p>

<p><span class="style14">Eerst achterzijde printen&nbsp;- Papier omdraaien - Daarna voorzijde printen</span></p>

<table cellpadding="5">

<tbody>

<tr>

<td>

<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="18%">

<tbody>

<tr>

<td bgcolor="#ffffff" width="55%"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff" width="45%"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

</tbody>

</table>

</td>

<td>

<table align="right" bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="18%">

<tbody>

<tr>

<td bgcolor="#ffffff" width="55%"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff" width="45%"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

<tr>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

<td bgcolor="#ffffff"><img height="25" src="../../images/box.png" width="48" /></td>

</tr>

</tbody>

</table>

</td>

</tr>

<tr>

<td colspan="2"><img alt="red-arrow" height="54" src="images/arrowa.jpg" width="324" /></td>

</tr>

</tbody>

</table>

<p>Nadat u de ClubShop Rewards kaarten hebt afgedrukt, hoeft u enkel de vellen Avery kaartjes langs de tanding los te scheuren. Koos u voor het zwaardere papier, gebruik dan&nbsp;een schaar of misschien investeert u liever in een papiersnijmachine.</p>

<p align="center"><a class="nav_blue_line" href="http://www.clubshop.com/getcards.html">[Terug naar Print UW ClubShop Rewards kaarten]</a></p>

</td>

<td colspan="2" style="background-image:url(http://www.clubshop.com/images/bg/bg_right.png)"></td>

</tr>

<tr>

<td colspan="12" height="62" valign="top"><img height="62" src="http://www.clubshop.com/images/general/clubshop_rewards_merchant_page_16.png" width="1000" /></td>

</tr>

<tr>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="34" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="14" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="146" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="142" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="119" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="90" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="38" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="128" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="133" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="109" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="13" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="34" /></td>

</tr>

</tbody>

</table>

</td>

</tr>

</tbody>

</table>

</body>

</html>