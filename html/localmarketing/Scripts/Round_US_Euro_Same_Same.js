// JavaScript Document
	
	var spans_to_convert = Array();
	var spans_original_value = Array();
	var spans_number_of_place_to_round_to = Array();
	var spans = Array();
	
	var non_digits = /[^\.\d]/g;
	
	function findCurrencyConversions()
	{
		spans = document.getElementsByTagName('span');
		
		
        for (var counter=0; counter < spans.length; counter++)
		{
			var number_of_places = spans[counter].getAttribute("convert", 2);
			
			if (number_of_places)
			{
				spans_to_convert.push(spans[counter]);
				
				var places = number_of_places != 'convert' ? number_of_places : 10;
				
				spans_number_of_place_to_round_to.push(places);
				spans_original_value.push(spans[counter].innerHTML.replace(non_digits, ''));
				
			}
        }
		spans = Array();
		
		document.getElementById("currency").selectedIndex = 0;
		
	}
	
	function roundUp(data, amount)
	{
		amount = amount.replace(non_digits, '');
		
		var flag = 0;
		
		/*
		*	conversion_info[0] holds the currency code
		*	conversion_info[1] holds the conversion rate
		*/
		var conversion_info = data.split('|');
		conversion_info[0] = conversion_info[0].replace(/\s/g, '');

		var converted_value = 0;
			
		switch(conversion_info[0])
		{
			case 'USD':
				converted_value = amount;
				break;
				
/*			case 'EUR':
				converted_value = amount;
				break;
*/				
			default :
				converted_value = amount * conversion_info[1];
				
				var orig_converted_value = converted_value;
				
				//var places_to_round_to = spans_number_of_place_to_round_to[span];
				
				converted_value = Math.ceil(converted_value*100) / 100
		}
		
		return comma(converted_value) + ' ' + conversion_info[0];	
		
	}
	
	
	function convert(data)
	{
		var flag = 0;
		
		/*
		*	conversion_info[0] holds the currency code
		*	conversion_info[1] holds the conversion rate
		*/
		var conversion_info = data.split('|');
		conversion_info[0] = conversion_info[0].replace(/\s/g, '');

		for (var span in spans_to_convert)
		{
			var converted_value = 0;
			
			switch(conversion_info[0])
			{
				case 'USD':
					converted_value = spans_original_value[span];
					break;
					
/*				case 'EUR':
					converted_value = spans_original_value[span];
					break;
*/					
			default :
				converted_value = spans_original_value[span] * conversion_info[1];
				
				var orig_converted_value = converted_value;
				
				
				var places_to_round_to = spans_number_of_place_to_round_to[span];
				
				if (converted_value > 1000)
				{
					places_to_round_to = 1000;
				}
				
				converted_value = roundToHalf(converted_value, places_to_round_to);
			}
			
			spans_to_convert[span].innerHTML = comma(converted_value) + ' ' + conversion_info[0];
			
		}
	}
	
	function roundToHalf(value, places)
	{
		var converted = parseFloat(value); // Make sure we have a number
		//converted = Math.ceil(converted);
		converted = converted /places;
		var decimal = (converted - parseInt(converted, 10));
		decimal = Math.round(decimal * 10);
		
		if (decimal == 5 || (decimal < 5 && decimal > 0)) 
		{ 
			return ((parseInt(converted, 10)+0.5) * places);
		}
		
		if (decimal > 5)
		{
			
			return Math.ceil(converted) * places;
		}
		
		return parseInt(converted * places, 10);
	}
	
	
	
	function commaNull(number) 
	{
		
		var number_array = Array();
		
		
		number = '' + number;
		
		
		number_array = number.split('.');
		
		number = number_array[0];
		
		decimal = isNaN(number_array[1]) ? '': '.' + number_array[1];
		
		
		
		if (number.length > 3)
		{
			var mod = number.length % 3;
			var output = (mod > 0 ? (number.substring(0,mod)) : '');
			for (i=0 ; i < Math.floor(number.length / 3); i++)
			{
				if ((mod == 0) && (i == 0))
					output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
				else
					output+= ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
			}
			return output + '' + decimal;
		}
		else return number + '' + decimal;
	}


	function comma(number) 
	{
		
		var number_array = Array();
		
		number = '' + number;
		
		var decimal = '.00';
		
		
		number_array = number.split('.');
		
		number = number_array[0];
		

		if(number_array[1])
		{
			if(number_array[1] < 10 && number_array[1] > 0)
			{
				decimal = '.' + number_array[1] + '' + '0';
			}
			else if(number_array[1] >= 10 && number_array[1] < 100)
			{	
				decimal = '.' + number_array[1];	
			} else {
				decimal = '.' + number_array[1].substring(0,2);				
			}
		}
		
		
		
		if (number.length > 3)
		{
			var mod = number.length % 3;
			var output = (mod > 0 ? (number.substring(0,mod)) : '');
			for (i=0 ; i < Math.floor(number.length / 3); i++)
			{
				if ((mod == 0) && (i == 0))
					output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
				else
					output+= ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
			}
			
			return output + '' + decimal;
		}
		else
		{
			return number + '' + decimal;
		}
	}
