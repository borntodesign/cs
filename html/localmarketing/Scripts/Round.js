// JavaScript Document
	
	var spans_to_convert = Array();
	var spans_original_value = Array();
	var spans_number_of_place_to_round_to = Array();
	var spans = Array();
	
	var non_digits = /[^\.\d]/g;
	
	function findCurrencyConversions()
	{
		spans = document.getElementsByTagName('span');
		
		
        for (var counter=0; counter < spans.length; counter++)
		{
			var number_of_places = spans[counter].getAttribute("convert", 2);
			
			if (number_of_places)
			{
				spans_to_convert.push(spans[counter]);
				
				var places = number_of_places != 'convert' ? number_of_places : 10;
				
				spans_number_of_place_to_round_to.push(places);
				spans_original_value.push(spans[counter].innerHTML.replace(non_digits, ''));
				
			}
        }
		spans = Array();
		
		document.getElementById("currency").selectedIndex = 0;
		
	}
	
	
	function convert(data)
	{
		var flag = 0;
		
		/*
		*	conversion_info[0] holds the currency code
		*	conversion_info[1] holds the conversion rate
		*/
		var conversion_info = data.split('|');
		conversion_info[0] = conversion_info[0].replace(/\s/g, '');

		for (var span in spans_to_convert)
		{
			var converted_value = 0;
			
			if (conversion_info[0] == 'USD')
			{
				converted_value = spans_original_value[span];
			} else {
				converted_value = spans_original_value[span] * conversion_info[1];
				
				var orig_converted_value = converted_value;
				
				converted_value = roundToHalf(converted_value, spans_number_of_place_to_round_to[span]);
			}
			spans_to_convert[span].innerHTML = comma(converted_value) + ' ' + conversion_info[0];
			
		}
	}
	
	function roundToHalf(value, places)
	{
		var converted = parseFloat(value); // Make sure we have a number
		//converted = Math.ceil(converted);
		converted = converted /places;
		var decimal = (converted - parseInt(converted, 10));
		decimal = Math.round(decimal * 10);
		
		if (decimal == 5 || (decimal < 5 && decimal > 0)) 
		{ 
			return ((parseInt(converted, 10)+0.5) * places);
		}
		
		if (decimal > 5)
		{
			return Math.ceil(converted) * places;
		}
		
		return parseInt(converted * places, 10);
	}
	
	
	
	function comma(number) 
	{
		
		var number_array = Array();
		
		
		number = '' + number;
		
		
		number_array = number.split('.');
		
		number = number_array[0];
		
		decimal = isNaN(number_array[1]) ? '': '.' + number_array[1];
		
		
		if (number.length > 3)
		{
			var mod = number.length % 3;
			var output = (mod > 0 ? (number.substring(0,mod)) : '');
			for (i=0 ; i < Math.floor(number.length / 3); i++)
			{
				if ((mod == 0) && (i == 0))
					output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
				else
					output+= ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
			}
			return output + '' + decimal;
		}
		else return number + '' + decimal;
	}


