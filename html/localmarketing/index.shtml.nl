?<!DOCTYPE html>
 

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>
		
		<!--#include virtual="common/meta/meta.shtml" -->
		
		<title>Local Marketing Income Potential</title>	
   
	<style>
		.money{font-weight:bold;}
		
		dl.nice.contained.tabs dd a:hover {
			color:#00A6FC;
			padding: 7px 18px 9px;
		}

	
	</style>
	
	
	
   
	</head>
<body>
<!-- Top Blue Header Container --> <!--#include virtual="common/body/top_bar/top_logo.shtml" --> <!--End Top Blue Header Container --> <!--Header--> <!--#include virtual="common/body/header/header.shtml" --> <!-- / Header --> <!-- Main Container -->
<div class="container white">
<div class="row">
<div class="three columns">
<div id="mainMenu"><!-- Menu --> <dl class="nice vertical tabs hide-on-print"> <dd><a class="active" href="index.shtml">Potentieel Inkomen</a></dd> <dd><a href="affinity.shtml">Non-profit organisaties</a></dd> <dd><a href="merchant.shtml">Winkels/ bedrijven</a></dd> <dd><a href="tools.shtml">Hulpmiddelen</a></dd> </dl> <!-- / Menu --></div>
<div class="panel hide-on-print">
<div><img border="0" height="31" src="images/thumb/merchant_welcome1.png" width="169" /></div>
<div id="newest_merchant"></div>
<div id="newest_merchants_sponsor"></div>
</div>
</div>
<div class="nine columns">
<div class="row"><!-- Content Place Holder --> <!-- Content Place Holder --> <!-- Content Place Holder --> <dl class="nice contained tabs"> <dd><a class="active" href="#nice1">Overzicht</a></dd> <dd><a href="#nice2">Direct inkomen</a></dd> <dd><a href="#nice3">Maandelijks inkomen</a></dd> </dl> 
<ul class="nice tabs-content contained">
<li class="active" id="nice1Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Potentieel Inkomen - Overzicht</h6>
<div class="row">
<div class="twelve columns">
<p>Bent u op zoek naar het vestigen en lokaal uitbreiden van uw zaak? Zo ja, zoek dan niet verder want deze lokale marketing cursus zal u laten zien hoe u een hoog inkomen creert door het inschrijven van winkels en ondernemers, non-profit organisaties en mensen die op zoek zijn naar manieren om geld te besparen op hun dagelijkse aankopen. Zodra u secces heeft met uw eigen lokale regio, kunt u wereldwijd dupliceren wat u hebt gedaan in andere locaties!</p>
<h5>VIER STAPPEN OM EEN SUCCESVOLLE LOKALE MARKETING CAMPAGNE TE <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">CREËREN</span></span></h5>
<br /> <img align="right" alt="Coffe Shop" height="35%" hspace="5" src="images/coffee_shop.jpg" style="margin-left:8px;" width="35%" /> <ol>
<li>Neem contact op met de bekendste en populairste lokale non-profit organisaties, waarvan de mensen in uw  gemeente bereid zijn deze te ondersteunen, om non-profit organisatie te worden bij ClubShop Rewards.</li>
<li>Indien dit is toegestaan bij de wet, vraag dan de non-profit organisaties die u aanmeld om kaarten te verkopen als fondsenwerver. Indien het in uw land wettelijk niet is toegestaan om als non-profit organisatie kaarten te verkopen, dan kunt u aanbieden hen een donatie te geven voor elke kaart die verkocht wordt in hun gemeente, door u of door de ondernemers of Partners die u heeft geworven en die de kaarten verkopen.</li>
<li>Neem contact op met ondernemers om de kaart te accepteren om de non-profit organisaties te ondersteunen die u hebt aangemeld.</li>
<li>Zorg dat <span class="short_text" id="result_box" lang="nl"><span class="hps">kaarten</span> <span class="hps">gedistribueerd en verkocht worden</span></span>, zodra u ten minste tien lokale winkels/ bedrijven hebt die de kaart accepteren.</li>
</ol></div>
</div>
</div>
</div>
</div>
</li>
<li id="nice2Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Potentieel Inkomen - Direct inkomen</h6>
<div class="row">
<div class="twelve columns"><img align="right" alt="content5" height="129" hspace="5" src="images/content/small/content_5.jpg" width="178" />
<p>Er zijn twee manieren om direct geld te gaan verdienen zodra u een lokale markt hebt <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">gecreëerd</span></span> van ClubShop Ondernemer Zaken!</p>
<h5 class="subheader">1. Kaarten kopen en verkopen</h5>
<ul>
<li>Koop zelf persoonlijk ClubShop Reward kaarten tegen groothandelprijzen, u verdient hiermee Pay punten en het verkopen van deze kaarten tegen detailhandel prijzen kan u een hoge winst opleveren. Aanbevolen verkoopprijs is <span class="transposeME money">$10.00</span>.</li>
</ul>
<table bgcolor="#A35912" border="0" cellpadding="4" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="left" bgcolor="#CFDBEC" valign="top">VOORBEELD - Verkoop persoonlijke 6.000 kaarten en verdien maar liefst <span class="transposeME money">$50.000</span> in totaal aan Retail en Pay punten inkomen!!<br /></td>
</tr>
</tbody>
</table>
<p><img height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.clubshop.com/outletsp/bcrewards.html" target="_blank">KLIK HIER</a> om te zien wat de kaarten tegen groothandelsprijs kosten.</p>
<hr />
<h5 class="subheader">2. Ondernemer marketingpakketten verkopen</h5>
<ul>
<li>Verdien 50% op de verkoopprijs van elk marketingpakket in Pay punten.</li>
</ul>
<table bgcolor="#A35912" border="0" cellpadding="4" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="left" bgcolor="#CFDBEC" valign="top">VOORBEELD - Verkoop 10 Zilver ondernemer marketing pakketten en verdien <span style="font-weight:bold;">400</span> Pay punten!<br /></td>
</tr>
</tbody>
</table>
<p><img height="11px" src="images/icons/icon_arrow_box.gif" width="11px" /> <a class="nav_footer_brown" href="http://www.clubshop.com/cs/customer_loyalty_program.shtml" target="_blank">KLIK HIER</a> om de kosten en voordelen te zien voor elk van de lokale Ondernemer marketingpakketten.</p>
<input alt="Print" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /></div>
</div>
</div>
</div>
</div>
</li>
<li id="nice3Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Potentieel inkomen - Maandelijks inkomen</h6>
<div class="row">
<div class="twelve columns">
<p><img align="right" alt="content6" height="129" hspace="5" src="images/content/small/content_6.jpg" style="margin-left:8px;" width="178" />Hoeveel geld kunt u maandelijks verdienen <span id="result_box" lang="nl"><span class="hps">door het uitvoeren van</span> <span class="hps">succesvolle lokale</span> <span class="hps">marketingcampagnes</span></span>? De mogelijke verdiensten zijn onbeperkt, maar veel is afhankelijk van hoeveel kaarten u distribueerd en hoeveel tracking winkels en bedrijven u aanmeld in een gebied.</p>
<p>Hier is <span id="result_box" lang="nl"><span class="hps alt-edited">een geschat</span> <span class="hps">maandelijks inkomen, ge</span></span>basseerd op het distribueren van 10.000 kaarten en het voltooien van een lokale marketing campagne met gemiddelde resultaten.</p>
<p>Laten we er van uitgaan dat slechts 3.000 van de 10.000 kaarthouders een aankoop doen bij de lokale "tracking" winkels en bedrijven die u hebt aangesloten. Wanneer ieder van deze 3.000 kaarthouders gemiddeld 3 aankopen per maand doet, dan staat dit gelijk aan 9.000 aankopen per maand. Wanneer het gemiddelde aankoopbedrag <span class="money transposeME">$25 was</span>, dan maakt dit een totaal van <span class="money transposeME">$225,000</span> in aankopen per maand. Wanneer de gemiddelde ondernemer transactie bijdrage 10% was, dan zou er <span class="money transposeME">$22,500 </span>aan ondernemer transactie bijdragen zijn.</p>
<p>Als de sponsor van al deze kaarthouders die aankopen doen, verdient u 20% van de winkel transactie fee aan Referral Pay punten (4,500 RPP) en als de sponsor van deze "Tracking" winkels/ bedrijven waar deze aankopen werden gedaan door uw kaarthouders, verdient u 10% van de winkel transactie fee aan Referral Pay punten (2,250 RPP). Hier is het inkomen dat u zou verdienen uit deze Referral Pay punten.</p>
<ul class="disc">
<li><strong>Referral Pay punten </strong>- 50% van uw Referral Pay punten wordt uitbetaald in Referral Commissies - 50% van 6,750 RPP = <strong><span class="money transposeME">$3,375</span> Referral Commissies per maand</strong></li>
</ul>
<p>Naast het verdienen van commissies en Pay punten van de verkoop van kaarten en het offline winkelen door de kaarthouders, is het tevens mogelijk dat een aantal van deze Members online gaat winkelen via het ClubShop winkelcentrum, waardoor u een extra maandelijks inkomen verdient.</p>
<input alt="Print" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /></div>
</div>
</div>
</div>
</div>
</li>
</ul>
<!--/ Content Place Holder --> <!--/ Content Place Holder --> <!--/ Content Place Holder --></div>
</div>
<!-- / nine columns --></div>
<!-- / Main container Row --></div>
<!--  / container --> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="common/footer/footer.shtml" --> <!--End Footer Container -->
</body>
</html>