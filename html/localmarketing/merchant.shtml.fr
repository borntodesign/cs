<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>
		
		<!--#include virtual="common/meta/meta.shtml" -->
		
		<title>Potentiel revenu du Marketing Local</title>	
		
		<style>
		.money{font-weight:bold;}
		
		dl.nice.contained.tabs dd a:hover {
			color:#00A6FC;
			padding: 7px 18px 9px;
		}

	
	</style>
   
	</head>

	
	<body class="blue">
<!-- Top Blue Header Container --> <!--#include virtual="common/body/top_bar/top_logo.shtml" --> <!--End Top Blue Header Container --> <!--Header--> <!--#include virtual="common/body/header/header.shtml" --> <!-- / Header --> <!-- Main Container -->
<div class="container white">
<div class="row">
<div class="three columns hide-on-print"><!-- Menu --> <dl class="nice vertical tabs hide-on-print"> <dd><a href="index.shtml">Revenu Potentiel</a></dd> <dd><a href="affinity.shtml">Groupes d'Affinité</a></dd> <dd><a class="active" href="merchant.shtml">Commerçants</a></dd> <dd><a href="tools.shtml">Outils</a></dd> </dl> <!-- / Menu -->
<div class="panel">
<div><img border="0" height="31" src="images/thumb/merchant_welcome1.png" width="169" /></div>
<div id="newest_merchant"></div>
<div id="newest_merchants_sponsor"></div>
</div>
</div>
<div class="nine columns">
<div class="row"><dl class="nice contained tabs"> <dd><a class="active" href="#nice1">Vue générale</a></dd> <dd><a href="#nice2">Remise directe</a></dd> <dd><a href="#nice3">Suivi</a></dd> </dl> 
<ul class="nice tabs-content contained">
<!-- TAB 1 -->
<li class="active" id="nice1Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Commerçant - Vue générale</h6>
<p>Avant de commencer à débuter la communication avec des commerçants par Email ou par rendez-vous, fixez vous un objectif sur le nombre de commerçants avec qui vous allez entrer en contact. Concentrez-vous sur une zone à la fois et surtout visitez des commerçants que vous savez faire de la publicité.</p>
<p>Surtout assurez-vous que vous avez une stratégie pour avoir beaucoup de cartes vendues dans la région/zone (par les groupes d'affinité, les commerçants, les Affiliates/Partners, ou par des ventes personnelles) lors de la signature des commerçants.</p>
</div>
</div>
</div>
</li>
<!-- TAB 2 -->
<li id="nice2Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Commerçant - Remise directe</h6>
<h5 class="subheader">1- Email de parrainage</h5>
<p>Parrainer par Email un commerçant qui fait des remises directes est très simple à faire et peut ne pas vous obliger à rencontrer personnellement un commerçant. La "clé" pour mener une campagne de parrainage de commerçant par Email est d'inscrire un ou plusieurs groupes d'affinité, qui peuvent être soutenus par la vente de cartes ou qui vendront également des cartes en tant que collecteur de fonds.</p>
<p><span class="green round label">Étape 1</span> - Aller sur le <a href="http://www.clubshop.com/mall" target="_blank">centre commercial ClubShop</a> télécharger la version gratuite de Infacta GroupMail.</p>
<p><span class="green round label">Étape 2</span> - Recueillir les adresses Emails des commerçants et importer ces adresses Email sur un tableur (feuille Excel) et dans GroupMail. Il y a toutes sortes d'endroits en ligne où vous pouvez récupérer les adresses Emails de commerçants, tels que les moteurs de recherche, facebook, google maps, les chambres de commerce et les répertoires d'entreprises locales.</p>
<p><span class="green round label">Étape 3</span> - Envoyer par Email cette <a href="javascript:void(0);" onclick="PopupCenter('pages/text_merchant_email.xml', 'myPop1',700,800);">lettre</a> à la liste d'adresse Email de commerçant que vous avez collecté à l'aide de GroupMail.</p>
<p><span class="green round label">Étape 4</span> - Tout commerçant qui répond à votre Email et qui veut participer doit être inscrit directement par vous en utilisant notre simple et rapide <a href="https://www.clubshop.com/cs/short-registration.shtml" target="_blank">candidature commerçante</a> à remise directe. . Vous DEVEZ conserver les Emails d'approbation des commerçants afin de pouvoir utiliser cette candidature commerçante.</p>
<p><span class="green round label">Étape 5</span> - Envoyer à nouveau la même lettre-Email (une fois) quelques jours après votre premier Email GroupMail et cela devrait vous permettre d'obtenir quelques commerçants de plus qui veulent participer.</p>
<h5 class="subheader">2- Conseils pour l'Email de parrainage</h5>
<p><span class="white round label">1. Restez organisé</span> - Assurez-vous de supprimer tous les commerçants qui vous demandent de retirer leur adresse Email de votre liste de contacts. Assurez-vous que votre liste de contacts de votre feuille Excel correspond à votre liste de contacts GroupMail.</p>
<p><span class="white round label">2. Adresse Email</span> - Assurez-vous que le Nom indiqué en tant qu'Expéditeur/De dans vos Emails inclut ClubShop Rewards (Exemple: John Doe - Carte ClubShop Rewards).</p>
<p><span class="white round label">3. Email de Bienvenue</span> - Avant d'inscrire le commerçant, envoyez lui un rapide Email pour lui expliquer qu'il recevra un Email de Bienvenue de la part du ClubShop Rewards. Pour vos gros commerçants ou les plus connus qui reçoivent beaucoup de clients, vous devriez aller vous présenter personnellement et les accueillir en tant qur commerçant participant. C'est toujours une bonne idée d'obtenir leur permission de placer un autocollant fenêtre pour eux et de leur vendre une carte lors de votre visite.</p>
<p><span class="white round label">4. Groupes GroupMail</span> - Créez des groupes de contact multiples dans votre GroupMail puisque vous ne pouvez envoyer d'Emails à des groupes de plus de 100. Mettez chaque groupe dans différents groupes alphabétiques (Exemple: Rome Italie A à D, Rome, Italie E à H, etc)</p>
<p><span class="white round label">5. Moyenne d'Email</span> - En moyenne, pour chaque tranche de 100 bonnes adresses Emails que vous collectez, environ 10% répondront vouloir participer, ou ils auront plus de questions ou ils peuvent vouloir un appel téléphonique de votre part afin de confirmer leur participation. Vous devriez appeler les commerçants qui demandent à être rappelés parce qu'ils sont d'excellents candidats pour y participer. En recueillant quelques centaines adresses Email, vous pouvez générer 30 ou plus nouveaux commerçants participants.</p>
<p><span class="white round label">6. Simple</span> - La lettre-Email que nous vous fournissons est très simple et est au point, elle a été testée pour fonctionner. Ajouter de plus amples détails dans la lettre-Email vous donne moins de commerçants et moins de résultats. Gardez la simple et n'ajoutez pas plus de détails à la lettre-Email!</p>
</div>
</div>
</div>
</li>
<!-- TAB 3 -->
<li id="nice3Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Commerçants - Suivi</h6>
<p>Inscrire des commerçants qui sont prêts à déclarer les ventes que nous pouvons suivre et fournir du ClubCash et donner du crédit pour les commissions, vous permettra de développer un revenu récurrent sur achats effectués chez les commerçants participants. Cependant, cela va aussi vous demander de rencontrer personnellement les commerçants afin de leur présenter comment le programme fonctionne. Si vous n'êtes pas prêts, pas capables et si vous ne voulez pas rencontrer les commerçants, vous devez simplement utiliser la méthode de parrainage par Email pour inscrire des commerçants qui font des remises directes.</p>
<h5 class="subheader">1- Rendez-vous de parrainage</h5>
<p>Soyez enthousiaste et "Habillé pour le Succès" si vous avez planifié de sortir pour voir personnellement les commerçants face à face. Vous n'obtiendrez pas une deuxième chance de faire une bonne première impression, alors laissez votre tenue vestimentaire refléter le fait que vous êtes un professionnel. Le port d'un beau <a href="http://www.clubshop.com/mall/other/apparel_gifts.shtml" target="_blank">Polo ClubShop Rewards</a> va automatiquement vous faire obtenir la reconnaissance positive de votre position.</p>
Le but de votre prise de contact N'est PAS de faire une présentation, mais de prendre un rendez-vous pour faire une présentation. Vous devrez peut-être aller dans plusieurs entreprises pour obtenir un rendez-vous. Ne laissez pas le "non" vous empêcher d'obtenir les "oui" dont vous avez besoin.
<p> </p>
<h5 class="subheader">2- Points clés du rendez-vous de parrainage</h5>
<div class="toggle"><ol>
<li>Ne les laisser voir aucun renseignement, ni votre carte de visite</li>
<li>Avoir votre calendrier / livre de visite visible</li>
<li>Rappeler pourquoi vous êtes ici - <strong>POUR OBTENIR UN RENDEZ-VOUS!</strong></li>
</ol> 
<hr />
</div>
<h5 class="subheader">3- Intro sur le rendez-vous de parrainage</h5>
<div class="toggle">
<p>Présentez-vous et présentez la carte tout en leur demandant: "Avez-vous déjà entendu parler de la carte ClubShop Rewards?"</p>
<p>NON ou PAS SUR = Demandez-leur, "Etes-vous le propriétaire ou le gérant?" S'ils ne sont ni l'un, ni l'autre, demandez-leur si le propriétaire/gérant est disponible pour seulement une minute. Si ce ne l'est pas, trouvez le meilleur moment pour prendre contact avec eux.</p>
<p>DE QUOI S'AGIT-IL? = Donnez-leur une explication introductive brève (voir ci-dessous), puis demandez-leur, "Etes-vous le propriétaire ou le gérant?" S'ils ne sont ni l'un, ni l'autre, demandez-leur si le propriétaire/gérant est disponible pour seulement une minute. Si ce ne l'est pas, trouvez le meilleur moment pour prendre contact avec eux.</p>
<p>OUI = "Merveilleux! S'ils ne sont ni l'un, ni l'autre, demandez-leur si le propriétaire/gérant est disponible pour seulement une minute. Si ce ne l'est pas, trouvez le meilleur moment pour prendre contact avec eux.</p>
<p>Si vous avez pris contact avec le propriétaire, alors commencez par demander au propriétaire une question simple pour les qualifier - "Etes-vous intéressé pour augmenter vos ventes?"</p>
<p>NON = "Etes-vous intéressé pour faire économiser de l'argent à votre entreprise et à vous-même?" Essayez et donnez-leur une carte et remplissez la demande avec eux. Plusieurs fois, cela va les amener à vous poser quelques questions, afin de pouvoir transformer leur NON en un Peut-être ou un OUI. Ou cela peut leur ouvrir la porte pour devenir un Affiliate ou un Partner.</p>
<p>PEUT-ÊTRE ou OUI = Donnez-leur une explication introductive brève et aller sur ces points clés rapides.</p>
<ol>
<li>Programme de collecte de fond - La carte de remise est conçue pour aider les organisations locales à but non lucratif à collecter des fonds en vendant la carte. Dites-leur les noms (ou montrer leur une liste) de quelques-uns des groupes d'affinité qui ont signé pour participer.</li>
<li>Attirer et garder les clients - Publicité, Marketing et programme de fidélisation des clients</li>
<li>Sans risque - PAS de coût à verser pour vous pour participer. Le seul coût pour vous pourrez avoir sera après la réalisation d'une vente</li>
<li>Pourcentage - Gratuit, le programme de base commence aussi bas que 5% d'une transaction. D'autres options peuvent être aussi bas que 1% de la transaction.</li>
</ol>
<p>"Maintenant, le programme est offert ici aux entreprises locales à qu'elles participent, et la raison pour laquelle je suis SPÉCIFIQUEMENT venu aujourd'hui, est pour fixer un rendez-vous avec vous afin de m'arrêter et de passer en revue comment le programme fonctionne et de voir si cela serait un bon moyen pour vous"</p>
<hr />
</div>
<h5 class="subheader">4- Rendez-vous</h5>
<div class="toggle">
<p>Alors que vous ouvrez votre agenda, demandez: "Quel serait le meilleur moment pour nous rencontrer, avant votre ouverture ou parfois pendant un moment calme de la journée?"</p>
<p><strong>Réponses possibles</strong></p>
<ul class="disc">
<li>PRISE DE RENDEZ-VOUS - Voir ci-dessous.</li>
<li>PAS INTERRESSÉ - "Etes-vous intéressé à économiser de l'argent pour votre entreprise et pour vous-même?" Essayez et donnez-leur une carte et remplissez la demande avec eux.</li>
<li>OCCUPÉ MAINTENANT - Trouvez quel est le meilleur moment pour revenir vers eux. Ensuite faites le suivi.</li>
</ul>
<p><strong>Questions possibles</strong></p>
<ul class="disc">
<li>Combien y a t-il d'autres entreprises qui y participent? "Actuellement, nous commençons tout juste à communiquer avec les entreprises ici à _______. Parce qu'il s'agit d'un moyen sans risque pour la publicité, nous nous attendons à ce que de nombreuses entreprises y participent." Puis demandez-leur à nouveau: "Quand est-ce le bon moment pour se réunir?"</li>
<li>Combien de personnes sont membres dans la région? "Je ne sais pas exactement les chiffres actuels, mais nous prévoyons une campagne de distribution massive de cartes pour le mois prochain, une fois que nous aurons une bonne liste de commerçant à fournir aux membres potentiels. (Si vous avez inscrit quelques groupes d'affinité dans votre région, faites-leur savoir quels sont ceux qui ont signé pour y participer). Entre ça et notre plan de marketing de référencement, nous nous attendons à avoir pratiquement tout le monde dans _______ qui sera un membre au cours de l'année prochaine. Puis demandez-leur: "Alors, quand est-ce le bon moment pour se réunir pendant environ 30 minutes?"</li>
<li>Peu importe ce qu'est leur réponse, répondez lui avec une réponse brève, mais positive, suivie par, "Quand est-ce le bon moment pour se réunir?"</li>
<li>Une fois que vous avez défini un jour et l'heure pour un rendez-vous, assurez-vous que tout le monde qui est impliqué dans une décision sera  ensemble au rendez-vous. Aussi, assurez-vous qu'ils ont soit écrit la date du rendez-vous ou notez la sur le dos de votre carte de visite et dites: "Si pour une raison quelconque, vous ne pouvez pas me rencontrer à ce moment, pourriez vous, s'il vous plaît, m'appeler pour le reprogrammer?"</li>
</ul>
<p><strong>Présentation commerçante</strong></p>
<p>Utilisez la présentation en diaporama qui est disponible pour votre région afin de faire des présentations. Il est recommandé d'imprimer la présentation en couleur (voir le lien PDF en-dessous de la première page de présentation) et de mettre chaque page dans un manchon en plastique dans un classeur à 3 anneaux pour créer une présentation en conférencier.</p>
<div class="row">
<div class="twelve columns">
<div class="six columns">
<div class="panel">
<ul>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/awen/" target="_blank">Aruba (Anglais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/awnl/" target="_blank">Aruba (Néerlandais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/auen/" target="_blank">Australie (Anglais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/benl/" target="_blank">Belgique (Néerlandais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/befr/" target="_blank">Belgique (Français)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/caen/" target="_blank">Canada (Anglais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/coes/" target="_blank">Colombie (Espagnol)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/annl.curacao/" target="_blank">Curacao, Antilles néerlandaises (Néerlandais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/anen.curacao/" target="_blank">Curacao, Antilles néerlandaises (Anglais)</a></li>
</ul>
</div>
</div>
<div class="five columns ">
<div class="panel">
<ul>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/frfr/" target="_blank">France (Français)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/gben/" target="_blank">Grande-Bretagne  (Anglais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/inen/" target="_blank">Inde (Anglais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/itit/" target="_blank">Italie (Italien)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/mxes/" target="_blank">Mexique (Espagnol)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/nlnl/" target="_blank">Pays Bas (Néerlandais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/srnl/" target="_blank">Suriname (Néerlandais)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/tnfr/" target="_blank">Tunisie (Français)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/usen/" target="_blank">USA (Anglais)</a></li>
</ul>
</div>
</div>
</div>
</div>
<p><strong>Aperçu de la présentation et Étapes</strong></p>
<ul>
<li>Qu'est-ce qu'une proche présomption? Vous êtes en mesure d'assumer que votre prospect s'inscrira du fait d'avoir surmonté les objections possibles au cours du processus de présentation.</li>
<li>Pourquoi pouvez-vous faire l'hypothèse qu'ils vont signer lorsque vous avez terminé la présentation? Parce que vous aurez les qualifications et aurez abordé les 3 principales objections qu'ils peuvent avoir, au cours du processus d'entrevue et durant la présentation.</li>
<li>Développer l'empathie et trouvez ce qu'ils ont fait ou font en publicité, en promotion ou s'ils ont un programme de fidélisation de la clientèle.</li>
<li>Le point CLÉ à déterminer avant de procéder à la présentation, c'est de voir qu'ils sont prêts de faire de la publicité, DE la promotion, à offrir des remises, etc pour obtenir plus d'affaires. Si la réponse est "non", ne continuer pas avec la présentation et donnez leur une carte ClubShop Rewards afin qu'ils puissent s'inscrire en tant que membre gratuit. En faisant cela, cela pourrait les amener à poser plus de questions et ils pourraient reconsidérer leur volonté de participer en tant que commerçant.</li>
<li>Utilisez le conférencier pour faire la présentation et créer deux principaux points de discussion pour les arrêter et les retenir/harnacher. </li>
</ul>
<p><strong>A.</strong></p>
<p>Pourcentage de Frais de transaction: Après avoir passé en revue les frais et les informations sur la capacité marketing, la capacité de changer les pourcentages et sur l'annulation, demandez-leur: "Pensez vous que vous pourriez faire au moins une remise de 10%, ou une offre "un acheté, un gratuit", ou seriez-vous plus à l'aise avec une remise de 5% ou moins?"</p>
<ul>
<li>Remise de 10% - Poursuivre la présentation, mais passer par-dessus les diapositives d'information sur le "suivi"</li>
<li>Offre "un acheté, un gratuit" - Poursuivre la présentation, mais passer par-dessus les diapositives d'information sur le "suivi"</li>
<li>Remise de 5% - Expliquer l'option de la méthode de "suivi" libre et comment fonctionne le processus d'enregistrement de base et de transmission des informations. Puis demandez-leur, "Pensez-vous avoir un problème avec cela?"<br /><br /> 
<ul>
<li>Problème - Déterminer si les méthodes de suivi sont acceptables (scanner ou un téléphone cellulaire) ou s'ils préfèrent une option de Remise directe. Poursuivez en vous basant sur leur réponse.</li>
<li>Pas de Problème - Montrez-leur le formulaire de "suivi" manuel de transaction, puis recommandez leur qu'ils commencent avec un processus manuel avec lequel ils peuvent faire les entrées, ou qu'ils peuvent vous faxer ou que vous pouvez leur procurer <a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">(Imprimer le formulaire).</a></li>
</ul>
</li>
<li>Remise de moins de 5% - Continuer avec la présentation, mais passer par-dessus les diapositives d'information de suivi et passer en revue les packs marketing quand vous arrivez à cette diapositive.</li>
<p>Surtout, n'oubliez pas de les "harnacher" quelle que soit la méthode qui est acceptable pour eux, avant de poursuivre la présentation. Si aucune n'est acceptable, cesser la présentation et inscrivez-les en tant que membre gratuit ClubShop Rewards ou vendez leur une carte ClubShop Rewards.</p>
</ul>
<p><strong>B.</strong></p>
<p>Remise Directe: Si les commerçants indiquent qu'ils sont à l'aise avec l'option gratuite d'une remise de 10% de remise ou le BOGO, alors passez la diapositive qui se réfère au paiement des frais mensuels ClubShop. S'ils ont choisi l'une des autres options, expliquez la procédure du prélèvement automatique de leur carte de crédit ou carte de débit ou le pré-financement d'un compte Club.</p>
<ul>
<li>Expliquez-leur que la société ClubShop Rewards traite avec les commerçants de partout dans le monde et que la société ne peut certainement pas attendre que les chèques arrivent par la poste. Puis dites: "C'est logique, n'est-ce pas?"</li>
<li>Faites-leur savoir que nous leur fournir un rapport en ligne et nous leur enverrons un Email le premier de chaque mois pour leur notifier du montant total de la transaction.</li>
<li>Demandez-leur s'ils préfèrent utiliser une carte de crédit ou carte de débit, ou mettre en place un compte Club.</li>
</ul>
<hr />
</div>
<h5 class="subheader">5- Répondez aux questions &amp; terminez la demande de candidature</h5>
<div class="toggle">
<p>Après avoir terminé la présentation, demandez-leur s'ils ont des questions, et tout en sortant sur le formulaire de demande de candidature et répondez à leurs questions de manière concise.</p>
<ul>
<li><a href="https://www.clubshop.com/print/clubshop_merchant_application.pdf" target="_blank">CLQUEZ ICI</a> pour la demande de candidature commerçante imprimable</li>
</ul>
<p>Confirmez le Nom de leur entreprise et commencez à remplir la demande. Lorsque vous arrivez au % de Frais de transaction, demandez-leur quel est le pourcentage avec lequel ils aimeraient commencer (en leur rappelant qu'ils peuvent changer quand ils le veulent). S'ils disent qu'ils ont besoin d'y penser, suggérez leur de commencer avec l'option gratuite de 5% de frais de transaction, mais ils peuvent choisir d'avoir un % frais de transaction plus élevé.</p>
<p>S'ils sont "incapables" de fournir une carte de crédit, offrez-leur la possibilité de mettre en place un dépôt de 50 $ USD à partir duquel leurs frais de transaction seront déduits. S'ils choisissent de donner un dépôt en vous donnant de l'argent ou un chèque, fournissez-leur un accusé de réception. Déposez leur paiement dans votre compte bancaire, puis financer votre compte Club (si nécessaire) avec la même quantité d'argent qu'ils ont payé. Dès la soumission de la candidature, vous, en tant que parrain, recevrez un Email avec des instructions sur la façon de transférer les fonds nécessaires à partir de votre compte Club afin de mettre en place leur Compte Club.</p>
<p>Passez en revue les avantages des commerçants effectuant un suivi gratuit.</p>
<p>Demandez-leur de signer la demande de candidature, puis fournissez leur une copie des conditions de l'accord. Dès qu'ils ont choisi l'option de suivi gratuit, fixez un rendez-vous le plus tôt possible, afin de revenir et de faire leur "mise en place".</p>
<hr />
</div>
<h5 class="subheader">6- Surmonter les objections - Demande d'adhésion de membre</h5>
<div class="toggle">
<p>Malgré tous vos efforts, certains peuvent dire "non" ou, plus vraisemblablement, vous renvoyer. "J'ai besoin d'y penser" ou "Je dois en parler avec _____".</p>
<p>Lorsque cela se produit, demandez-leur ce à quoi ils doivent expressément "penser" - le pourcentage qu'ils veulent donner OU comment nous faire débiter son compte? Faites de votre mieux pour répondre à l'objection, mais ne les pousser pas. Aidez-les à remplir une demande du titulaire de carte, afin qu'ils puissent voir comment cela fonctionne et puis prenez connaissance de quand vous devez revenir vers eux..</p>
<p><span class="green round label">Astuce:</span> NE PAS parler avec eux du référencement d'autres personnes jusqu'à ce que vous reveniez pour les mettre en place.</p>
<p>Envoyez leur un Email après leur inscription pour confirmer la date et l'heure de la "mise en place".</p>
<hr />
</div>
<h5 class="subheader">7- Suivre et Accompagner</h5>
<div class="toggle">
<p>Pour ceux qui n'ont pas dit "non", mais ont repoussez la décision, faites un suivi avec eux après que vous ayez des "nouvelles" au sujet de nouveaux commerçants et/ou de  la distribution de carte.</p>
<p>Plus important encore, sortez et travaillez avec quelqu'un d'autre. Ne laissez pas vos objectifs d'inscription se fonder uniquement sur des suivis. Contactez les nouveaux commerçants!</p>
<hr />
</div>
<h5 class="subheader">8- Mise en place d'un commerçant effectuant un Suivi</h5>
<div class="toggle">
<p>Après avoir inscrit un commerçant qui a choisi l'option de suivi, vous aurez besoin d'organiser un temps pour les "mettre en place" afin qu'ils reçoivent la carte ClubShop Rewards et pour les former sur la façon de soumettre l'information de transaction, ainsi que de les orienter à devenir un commerçant. Vous pouvez également avoir besoin de temps pour les informer à nouveau sur les avantages de la vente de cartes.</p>
<p>Puisque le temps de cette "mise en place" peut prendre jusqu'à une heure, il est généralement nécessaire de prendre un autre rendez-vous avec eux, après que l'inscription ait été fait. Si les commerçants choisissent de payer leurs frais de transaction par carte de crédit, qu'ils veulent soumettre la transaction manuellement et qu'ils ont accès à un ordinateur, alors vous pouvez être en mesure de les "mettre en place" directement après leur "inscription".</p>
<hr />
</div>
<h5 class="subheader">9- Préparations de la mise en place commerçante</h5>
<div class="toggle"><ol>
<li>
<p>Déterminer leur méthode de paiement pour payer leurs frais de transaction à venir.</p>
<ul class="disc">
<li>Carte de crédit ou carte de débit - Si leur carte de  crédit ou de débit a été approuvée, vous recevrez un avis de leur approbation, avec le numéro d'identification ID du commerçant. Si leur carte de crédit n'est pas approuvée, seul le commerçant recevra un avis, qui leur expliquera la raison de la non-approbation. Si cela se produit, vous devrez faire un suivi auprès du commerçant afin d'utiliser une carte de crédit différente ou pour les aider à mettre en place un compte Club pour  payer pour leurs frais de transaction.</li>
<li>Compte Club - Si le commerçant choisit de ne pas utiliser de carte de crédit ou de débit, alors vous aurez besoin pour les aider à financer leur Compte Club. La meilleure façon de faire cela est de faire en sorte que le commerçant vous paie directement, en leur fournissant un reçu pour l'argent qu'ils vous ont donné. Ils auront besoin de vous fournir suffisamment de fonds pour payer leur dépôt (50 $ USD). Vous devrez ensuite déposer leur paiement sur votre compte bancaire et financer votre Compte Club si nécessaire, puis de soumettre un formulaire d'autorisation de transfert en ligne (Ce formulaire peut être trouvé à partir de votre Centre de Contrôle de Gestion) afin d'autoriser notre société à transférer les fonds à partir de votre Compte Club sur le Compte Club du commerçant.</li>
</ul>
</li>
<li>
<p>Déterminer leur méthode de transmission des frais de transaction.</p>
<ul class="disc">
<li>MANUEL - C'est la meilleure méthode recommandée pour la plupart des commerçants pour commencer. S'ils choisissent d'utiliser cette méthode en entrant l'information de transaction sur un <a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">Formulaire d'Inscription de Transaction Commerçante</a> , déterminez si ils ont accès à Internet pour être en mesure de saisir l'information à leur compte commerçant, ou si ils devront vous faxer ce formulaire ou si vous aurez besoin de venir chercher le formulaire et de faire l'entrée de transactions vous-même. Si une méthode manuelle est sélectionnée, le temps minimum acceptable pour transmettre les transactions est hebdomadaire, bien que le plus souvent c'est fait, le mieux c'est.</li>
<li>SCANNER ET ORDINATEUR - Pour utiliser cette méthode, le commerçant devra avoir accès à un ordinateur qui peut être en ligne sur le point de vente. Le type de scanner que nous leur recommandons d'utiliser ou d'acheter devrait être un Scanner de code barre USB peu coûteux. Vous devriez rechercher les scanners les plus abordables qui sont disponibles là où vous vivez, de sorte que vous puissiez donner l'information au commerçant pour qu'il en achète un, ou de sorte que vous puissiez en acheter un pour votre commmerçant et pour lequel vous vous ferez rembourser lors de la "mise en place" / configuration du Scanner. Si vous commandez un scanner en ligne (de préférence à partir d'un magasin du centre commercial ClubShop), prévoyez le temps qu'il faudra pour recevoir le scanner, en prenant votre rendez-vous pour la "mis en place".</li>
</ul>
</li>
</ol> 
<hr />
</div>
<h5 class="subheader">10-Procédures de configuration/mise en place</h5>
<div class="toggle"><ol>
<li>
<p>Assurez-vous que votre rendez-vous pour la "mis en place" est pris pendant un temps où le commerçant ne sera pas distrait ou interrompu. Apportez tout ce qu'il faut pour obtenir le commerçant soit bien "configuré", y compris:</p>
<ul class="circle">
<li>le Numéro ID du commerçant</li>
<li>un Accès Internet</li>
<li>des formulaires d'entrée manuelle de transaction (au moins dix)</li>
<li>des autocollants décalcomanies pour la fenêtre du commerçant</li>
<li>la copie de leur candidature commerçante (si c'est une candidature imprimée déjà remplie)</li>
<li>Si applicable, un timbre de reçu commerçant</li>
<li>Si applicable, un Scanner</li>
<li>Si applicable, des cartes ClubShop Rewards pour qu'ils les achètent et les distribuent (au moins 50) et tous les autres matériaux Partners (Brochures, liste des commerçants, lettres de motivation, boîte de dépôt pou les candidatures)</li>
</ul>
</li>
<li>
<p>Demandez-leur de se connecter sur leur compte commerçant (https://www.clubshop.com/maf). Un exemple de compte commerçant peut être vu en se connectant avec les informations suivantes: <br /><br /> Nom d'utilisateur: bill | Mot de passe: mac | ID: 41 | Sélectionner le magasin principal <br /><br /> Expliquez-leur comment naviguer sur le site pour trouver l'information dont ils ont besoin. Activez le commerçant sur la page de mise en place/configuration commerçante dans leur compte commerçant, puis formez le commerçant sur la façon d'enregistrer et de soumettre des transactions.</p>
</li>
<li>
<p>Expliquez-leur ce qu'est les reçus initiaux et les timbres de reçu. Passez en revue la façon dont ils vont traiter les retours, afin de s'assurer qu'ils ne payeront pas de frais de transaction sur les articles retournés.</p>
</li>
<li>
<p>Montrez-leur leur rapport de transaction commerçant et expliquez-leur comment ils vont recevoir un Email le premier de chaque mois, pour leur confirmer les transactions et le montant qu'ils auront à payer à la société au 5 de chaque mois, basé sur leur Transaction qu'ils entrent.</p>
</li>
<li>
<p>Revoyez le mode de paiement et s'ils se servent de leur Compte Club, formez-les sur la façon d'ouvrir leur page Compte Club et sur comment financer leur Compte Club pour le paiement des frais de transaction à venir.</p>
</li>
<li>
<p>Montrez-leur leur inscription commerçante sur l' <a href="https://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank">Annuaire Google Maps</a></p>
</li>
<li>
<p>Répondez à toutes les questions qu'ils peuvent avoir et expliquez-leur quand la phase de distribution de carte de votre campagne aura lieu. Laissez-leur votre carte de visite et demandez-leur de vous contacter une fois qu'ils auront leur premier client ClubShop Rewards, afin que vous puissiez vous assurer que tout s'est bien passé. Appliquez leur autocollant décalcomanie sur la fenêtre du commerçant à l'intérieur d'une porte avant en verre ou à l'intérieur d'une fenêtre à côté de la porte d'entrée.</p>
</li>
</ol></div>
</div>
</div>
</div>
</li>
<p><input alt="Imprimer" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /></p>
<!-- / TAB 3 --> 
</ul>
<!--/ Content Place Holder --> <!--/ Content Place Holder --> <!--/ Content Place Holder --></div>
</div>
<!-- / nine columns --></div>
<!-- / Main container Row --></div>
<!--  / container --> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="common/footer/footer.shtml" --> <!--End Footer Container -->
</body>
</html>