<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>
		
		<!--#include virtual="common/meta/meta.shtml" -->
		
		<title>Local Marketing Income Potential</title>	
		
		<style>
		.money{font-weight:bold;}
		
		dl.nice.contained.tabs dd a:hover {
			color:#00A6FC;
			padding: 7px 18px 9px;
		}

	
	</style>
		
   
	</head>

	
	<body class="blue">
<!-- Top Blue Header Container --> <!--#include virtual="common/body/top_bar/top_logo.shtml" --> <!--End Top Blue Header Container --> <!--Header--> <!--#include virtual="common/body/header/header.shtml" --> <!-- / Header --> <!-- Main Container -->
<div class="container white">
<div class="row">
<div class="three columns"><!-- Menu --> <dl class="nice vertical tabs hide-on-print"> <dd><a href="index.shtml">Potentieel Inkomen</a></dd> <dd><a class="active" href="affinity.shtml">Non-profit organisaties</a></dd> <dd><a href="merchant.shtml">Winkels/ bedrijven</a></dd> <dd><a href="tools.shtml">Hulpmiddelen</a></dd> </dl> <!-- / Menu -->
<div class="panel">
<div><img border="0" height="31" src="images/thumb/merchant_welcome1.png" width="169" /></div>
<div id="newest_merchant"></div>
<div id="newest_merchants_sponsor"></div>
</div>
</div>
<div class="nine columns">
<div class="row"><dl class="nice contained tabs"> <dd><a class="active" href="#nice1">Overzicht</a></dd> <dd><a href="#nice2">Sponsoring</a></dd> <dd><a href="#nice3">Fondsenwerving</a></dd> </dl> 
<ul class="nice tabs-content contained">
<li class="active" id="nice1Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Non-profit organisatie - Overzicht</h6>
<div class="container">
<div class="row">
<div class="twelve columns">
<p>De SLEUTEL om de deur te openene naar het opzetten van een succesvolle lokale marketing campagne is eerst non-profit organisatie in te schrijven.</p>
<h6 class="subheader"><strong>Hier is waarom:</strong></h6>
<ul class="disc">
<li>Zodra u 1 of meerdere lokale non-profit organisaties hebt ingeschreven in een vastgesteld gebied, zal het gemakkelijker zijn om lokale ondernemers in te schrijven in datzelfde gebied, omdat veel ondernemers <span class="short_text" id="result_box" lang="nl"><span class="hps">bereid zullen zijn</span> <span class="hps">om deel te nemen om de non-profit organisaties te ondersteunen met het verkopen van kaarten om fondsen te werven</span></span>.</li>
<li>Consumenten voelen een grotere stimulans om de kaart te kopen wanneer zij weten dat een deel van de verkoop van de kaart naar een non-profit organisatie gaat.</li>
<li>Non-profit organisaties kunne ook een GEWELDIGE bron zijn voor het verwijzen van lokale winkels en bedrijven die u kunt aanmelden. Laat de ondernemers weten welke non-profit organisaties u naar hen hebben verwezen, dit kan de ondernemer een extra stimulans geven om deel te nemen en/ of een hogere korting te geven. Dit kan zeer behulpzaam zijn voor u in het "lanceren" van de ondernemer aanmeld fase van uw campagne.</li>
<li>U kunt uw lijst van de non-profit organisaties gebruiken die zich hebben aangemeld, om u te helpen winkels en bedrijven aan te melden, omdat het ondersteunen van lokale non-profit organisaties sommige ondernemers een extra stimulans geeft om deel te nemen en een korting te geven.</li>
</ul>
<p>Wanneer u dan bent begonnen met het aanmelden van winkels en bedrijven, dan kunt u uw lijst met winkels en bedrijven gebruiken zodat u, uw lokale Affiliates/ Partners, Ondernemers en non-profit organisaties succesvol kaarten kunnen gaan kopen en doorverkopen. Zodra u tenminste 10 of meer lokale ondernemers hebt aangemeld, gebruikt u deze geregistreerde winkels en bedrijven lijst om uw non-profit organisaties kaarten te laten verkopen aan consumenten in de plaatselijke bevolking, als een fondsenwerving programma, indien het hen is toegestaan om te doen.</p>
<img align="right" height="140px" hspace="5" src="images/content/small/content_14.gif" width="145px" />
<h6 class="subheader"><strong>Voordelen non-profit organisatie<br /></strong></h6>
<ul class="disc">
<li>Non-profit organisaies kunnen de ClubShop Rewards kaarten "voorverkopen", voordat zij de feitelijke aankoop van een x aantal kaarten doen. Hierdoor kan de non-profit organisatie beginnen met fondsen werven zonder van te voren kosten te hoeven maken of te moeten investeren.</li>
<li>Non-profit organisaties hebben een goed potentieel om een GROTE <span class="short_text" id="result_box" lang="nl"><span class="hps">hoeveelheid</span> <span class="hps">geld in te zamelen</span></span> door het inkopen van kaarten voor groothandelsprijs om ze vervolgens <span class="short_text" id="result_box" lang="nl"><span class="hps">te verkopen</span> voor<span class="hps"> detailhandel</span> <span class="hps">prijzen.</span></span> Voorbeeld: 1.000 kaarten verkopen of voorverkopen kan zo'n <span class="transposeME">$7,800.00</span> opbrengen voor een non-profit organisatie!</li>
<li>Non-profit organisaties kunnen inkomsten blijven verdienen wanneer de kaarthouders die zij hebben verwezen, winkelen bij de ClubShop Rewards winkels en bedrijven die hun verkopen tracken en ClubCash geven en wanneer zij online winkelen via het ClubShop winkelcentrum, terwijl zij gebruik maken van hun lidmaatschap. <span class="short_text" id="result_box" lang="nl"><span class="hps">Zo</span> <span class="hps">is het een</span> <span class="hps">fondsenwerver die geld kan blijven verdienen voor een non-profit organisatie</span></span>!</li>
<li>Hoewel de ClubShop Rewards kaart dezelfde prijs kan hebben als andere kortingkaarten die gebruikt worden om fondsen te werven, biedt geen andere kaart de unieke en blijvende waarde die onze ClubShop Rewards kaart de consument biedt. Dit geeft de non-profit organisatie een "kortingkaart" van  meer waarde dan welke andere kortingkaart ook, welke zij kunnen verkopen en fondsen mee kunnen werven. </li>
<li>Wij bieden meer potentiele plaatsen om de kaart te gebruiken dan op de achterkant van een kaart geprint kan worden. Een normale kortingkaart is beperkt tot 10 - 20 lokale ondernemers waar korting kan worden verkregen. </li>
<li>In tegenstelling tot andere kortingkaarten, is onze kaart wereldwijd te gebruiken. Daardoor is het mogelijk dat wij kaarthouders kortingen bieden wanneer zij reizen, verhuizen, of zich buiten hun lokale woongebied bevinden. geen andere kortingkaart biedt dit wereldwijde gebruik.</li>
<li>Wij bieden de kaarthouders een manier om kortingen en geld terug te krijgen wanneer zij online winkelen. Wij bieden de enige kaart, waar we op dit moment van weten, die kaarthouders zowel offline winkels biedt die kortingen of geld terug geven als online winkels die kortingen geven.</li>
</ul>
<p>Alles bij elkaar biedt ClubShop Rewards aan non-profit organisaties 1 van de meest fantastische fondsenwervingsprogramma's ter wereld!</p>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li id="nice2Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Non-profit organisaties - Sponsoring</h6>
<ol>
<li><span class="sitemap">Maak een contactlijst</span> 
<ul class="disc">
<li>Gebruik het internet en uw "Gouden Gids" Telefoonboek om contactinformatie op te zoeken van de lokale georganiseerde, de nationale of internationale non-profit organisaties en goede doelen.</li>
<li>Neem contact op met alle lokale scholen voor contactinformatie van hun schoolhoofd, sportcoaches, clubs en organisaties.</li>
<li>Neem contact op met alle lokale jeugd sportorganisaties voor contactinformatie van hun voorzitters en/ of fondsenwervers.</li>
<li>Neem contact op met uw lokale kerken, verenigingen, clubs en vakbonden.</li>
</ul>
</li>
<li><span class="sitemap">Contact opnemen</span> 
<ul class="disc">
<li>Bel hen om de e-mail adressen te krijgen van uw contactpersonen om hen wat geweldige fondsenwerving informatie te sturen.</li>
<li>Stuur een e-mail <a href="javascript:void(0);" onclick="PopupCenter('pages/text_ag_email.xml', 'myPop1',700,800);">(E-mail voorbeeld)</a> aan uw contactpersoon met een link naar uw non-profit marketing website.</li>
<li>Voer een opvolg telefoongesprek met uw contactpersoon om na te gaan of zij uw e-mail hebben ontvangen of naar de website hebben gekeken. Moedig hen aan om gratis aan te melden en probeer een afspraak met hen te maken om hen te ontmoeten en uit te leggen hoe het ClubShop Rewards Fondsenwervingprogramma werkt.</li>
<li class="">If you receive an email back from a potential AG or if they submit the form to get more information about our ClubShop Rewards Card Fundraising Program, respond with an email <a href="javascript:void(0);" onclick="PopupCenter('pages/text_ag_info.xml', 'myPop1',700,800);">(Email Example)</a> to encourage them to sign up as an AG, before you make an appointment to meet with them. Once they are signed up as an AG, you can discuss their options with them and decide if they will purchase cards, once you have signed up at least 10 Merchants in their local area to accept the card.</li>
</ul>
</li>
<li class="a"><span class="sitemap">Ontmoet uw contactpersoon</span> 
<ul class="disc">
<li class="a">Indien nodig, om een gratis aanmelding te krijgen, ontmoet u uw contactpersoon om uit te leggen hoe het ClubShop Rewards Fondsenwervingprogramma werkt. </li>
</ul>
</li>
</ol></div>
</div>
</div>
</li>
<li id="nice3Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Non-profit organisatie - Fondsenwerven</h6>
<ol>
<li><span class="sitemap">Aanmelden</span> - Wanneer de non-profit organisatie zich niet kan of wil aanmelden via het online formulier, <a class="nav_footer_brown" href="http://www.clubshop.com/print/ag_application.pdf" target="_blank">KLIK HIER</a> om een aanmeldformulier voor hen te printen en mee aan te melden. Vraag de contactinformatie van de perso(o)n(en) die verantwioordelijk is/ zijn voor de website en/ of communicatie en maak een afspraak om de <span class="short_text" id="result_box" lang="nl"><span class="hps atn">"</span><span>beslisser</span><span class="hps">(s)"</span></span> van de non-profit organisatie te ontmoeten (vaak de Raad van Bestuur) binnen 30 - 60 dagen na aanmelding, om uitleg te geven hoe zij fondsen kunnen werven door het verkopen van de ClubShop Rewards kaart. Geef u zelf voldoende tijd om een mooi aantal winkels en bedrijven aan te melden, voordat u de non-profit organisaties ontmoet om hen kaarten te laten verkopen.</li>
<li><span class="short_text" id="result_box" lang="nl"><span class="hps">Coördinatie </span></span>- Maak een afspraak met de persoon die verantwoordelijk is voor de website en/ of de communicatie van de non-profit organisaties. Laat hen hun non-profit interface website zien en help hen met het uploaden van een banner op hun website en stuur een e-mail naar hun mailing lijst en/ of plaats een uitnodiging in hun volgende nieuwsbrief.</li>
<li><span class="sitemap">Kaartverkoop bijeenkomst</span> - Zodra u ten minste 10 lokale winkels en bedrijven hebt aangemeld, maakt u een afspraak met de beslissers(s) van de non-profit organisaties om hen uitleg te geven hoe hun non-profit organisatie fondsen kan werven door het verkopen van ClubShop Rewards kaarten. Neem een aantal Ondernemer aanmelformulieren mee, voor het geval er deelnemers aanwezig zijn bij de bijeenkomst die ook ondernemer zijn.</li>
</ol> <img align="right" height="179" hspace="5" src="images/content/small/content_16.jpg" style="margin-left:10px;" width="267" />
<h6>Voorverkoop kaarten</h6>
<p>Met deze mogelijkheid kan de non-profit organisatie bestellingen voor kaarten en alvast geld binnen krijgen, voorafgaand aan het plaatsen van hun bestelling. Deze mogelijkheid werkt goed voor scholen en jeugd sportorganisaties. Maak gebruik van een non-profit organisatie Voorverkoop ClubShop Rewards Flyer om uit te leggen hoe de kaart en het lidmaatschap werkt en een lokale winkels en bedrijven lijst om te laten zien waar en hoe de kaart gebruikt kan worden, mensen kunnen een bestelformulier invullen en hun kaart vooruit betalen. Zodra de non-profit organisatie genoeg geld in kas heeft van de kaarten die zij hebben verkocht in de voorverkoop, kunnen zij de bestelling plaatsen en betalen door gebruik te maken van het formulier dat hiervoor beschikbaar is in hun non-profit interface site. De non-profit organisatie kan het resterende bedrag dat is ingezameld met het verkopen van de kaarten houden als inkomsten, net als bij alle kaarten die zij in de toekomst zullen gaan verkopen.</p>
<ul class="disc">
<span style="line-height:20px"><img src="images/icons/icon_green_checkmark.png" /> <a class="nav_footer_brown" href="pages/text_ag_print.html" target="_blank">KLIK HIER</a> - Non-profit voorverkoop Flyer en voorverkoop bestelformulier.</span><br /> <span style="line-height:20px"><img src="images/icons/icon_green_checkmark.png" /> <a class="nav_footer_brown" href="https://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank">KLIK HIER</a> - Maak een ljist van lokale deelnemende winkels en bedrijven.</span> 
</ul>
<h6>Vooruit betalen</h6>
<p>Door deze mogelijkheid kan de non-profit organisatie hun eigen kaarten, vooraf, bestellen en volledig betalen, door gebruik te maken van het formulier dat hiervoor beschikbaar is in hun non-profit interface site. Wanneer een non-profit organisatie hun eigen kaarten bestellen en betalen, krijgt de partner die hen sponsorde automatisch de van toepassing zijnde referral commissie voor hun bestelling.</p>
<h6>Partner Bestelling</h6>
<p>Er kunnen zich situaties voordoen waarbij een <span id="result_box" lang="nl"><span class="hps">partner kan</span> <span class="hps">willen of moeten</span> <span class="hps">toestaan ​​dat een</span> non-profit organisatie <span class="hps">een storting doet</span></span>, of de non-profit organisatie toestemming krijgt om kaarten te bestellen zonder eerst een storting te doen. In deze gevallen, zou de storting moeten verzamelen en moeten zoregn voor een ontvangstbewijs aan de non-profit organisatie voor het bedrag van de storting. De Partner neemt dan de verantwoordelijkheid voor het plaatsen van de bestelling en het vooraf betalen van de betaling van de bestelling en het verzamelen van de resterende balans van de non-profit organisatie.</p>
</div>
</div>
</div>
</li>
<p><input alt="Print" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /></p>
</ul>
</div>
</div>
<!-- / nine columns --></div>
<!-- / Main container Row --></div>
<!--  / container --> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="common/footer/footer.shtml" --> <!--End Footer Container -->
</body>
</html>