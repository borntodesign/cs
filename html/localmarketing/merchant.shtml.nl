﻿<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>
		
		<!--#include virtual="common/meta/meta.shtml" -->
		
		<title>Local Marketing Income Potential</title>	
		
		<style>
		.money{font-weight:bold;}
		
		dl.nice.contained.tabs dd a:hover {
			color:#00A6FC;
			padding: 7px 18px 9px;
		}

	
	</style>
   
	</head>
<body>
<!-- Top Blue Header Container --> <!--#include virtual="common/body/top_bar/top_logo.shtml" --> <!--End Top Blue Header Container --> <!--Header--> <!--#include virtual="common/body/header/header.shtml" --> <!-- / Header --> <!-- Main Container -->
<div class="container white">
<div class="row">
<div class="three columns hide-on-print"><!-- Menu --> <dl class="nice vertical tabs hide-on-print"> <dd><a href="index.shtml">Potentieel Inkomen</a></dd> <dd><a href="affinity.shtml">Non-profit organisaties</a></dd> <dd><a class="active" href="merchant.shtml">Winkels/ bedrijven</a></dd> <dd><a href="tools.shtml">Hulpmiddelen</a></dd> </dl> <!-- / Menu -->
<div class="panel">
<div><img border="0" height="31" src="images/thumb/merchant_welcome1.png" width="169" /></div>
<div id="newest_merchant"></div>
<div id="newest_merchants_sponsor"></div>
</div>
</div>
<div class="nine columns">
<div class="row"><dl class="nice contained tabs"> <dd><a href="#nice2">Overzicht</a></dd><dd><a href="#nice2">Directe korting</a></dd> <dd><a href="#nice3">Tracking</a></dd> </dl> 
<ul class="nice tabs-content contained">
<!-- TAB 1 -->
<li class="active" id="nice1Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Winkels/ Bedrijven - Overzicht</h6>
<p>Voor u van start gaat met contact op te nemen met winkels/ bedrijven via e-mail of via afspraken, stel een doel voor het aantal winkels/ bedrijven waar u contact mee wilt opnemen. Focus op 1 gebied per keer en bezoek in het bijzonder ondernemers waarvan u weet dat zij adverteren.</p>
<p>Het belangrijkste is dat u er zeker van bent dat u een strategie heeft om veel kaarten te verkopen in het gebied (door non-profit organisaties, winkels/ bedrijven, Affiliates/Partners, of van persoonlijke verkopen) tijdens het inschrijven van winkels en bedrijven.</p>
</div>
</div>
</div>
</li>
<!-- TAB 2 -->
<li id="nice2Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Winkels/ bedrijven- Directe korting</h6>
<h5 class="subheader">1- E-mail Sponsoring</h5>
<p>Sponsoren van Directe Korting winkels/ bedrijven via e-mail is erg eenvoudig om te doen en het is niet nodig dat u persoonlijk contact opneemt met een ondernemer. De "sleutel" voor het voeren van een ondernemer e-mail Sponsoring Campagne is het inschrijven van 1 of meerdere non-profit organisaties, <span class="short_text" id="result_box" lang="nl"><span class="hps">die</span> <span class="hps">worden ondersteund door</span> <span class="hps">de verkoop</span> <span class="hps">van kaarten of die tevens kaarten willen verkopen als een fondsenwerver</span></span>.</p>
<p><span class="green round label">Stap 1</span> - Ga naar het <a href="http://www.clubshop.com/mall" target="_blank">ClubShop winkelcentrum</a> en download de gratis versie van Infacta GroupMail.</p>
<p><span class="green round label">Stap 2</span> - Verzamel e-mail adressen van winkels/ bedrijven en importeer deze e-mail adressen in een spreadsheet en GroupMail. Er zijn verschillende manieren om online e-mail adressen te verzamelen van ondernemers, zoals zoekmachines, facebook, google maps, <span class="short_text" id="result_box" lang="nl"><span class="hps">Kamer van Koophandel en</span></span> locale <span class="short_text" id="result_box" lang="nl"><span class="hps">bedrijvengidsen</span></span>.</p>
<p><span class="green round label">Stap 3</span> - Stuur <a href="javascript:void(0);" onclick="PopupCenter('pages/text_merchant_email.xml', 'myPop1',700,800);">deze e-mail</a> aan de lijst van winkels/ bedrijven waarvan u e-mail adressen heeft verzameld door GroupMail te gebruiken.</p>
<p><span class="green round label">Stap 4</span> - Elke winkel/ ondernemer die op uw e-mail reageert en wil deelnemen moet direct door u worden aangemeld door gebruik te maken van ons eenvoudige en directe korting <a href="https://www.clubshop.com/cs/short-registration.shtml" target="_blank">ondernemer aanmeldformulier</a>. U MOET de goedkeuring e-mails goed bewaren om gebruik te kunnen maken van deze ondernemer aanmelding.</p>
<p><span class="green round label">Stap 5</span> - Stuur een paar dagen na de eerste GroupMail e-mail, dezelfde e-mail nog eens, deze zou u nog een paar meer winkels/ bedrijven moeten opleveren die willen deelnemen.</p>
<h5 class="subheader">2- E-mail Sponsoring Tips</h5>
<p><span class="white round label">1. Blijf georganiseerd</span> - Verwijder winkels/ bedrijven uit uw lijst wanneer zij hier om vragen. Zorg ervoor dat uw spreadsheet contactlijst exact overeenkomt met uw GroupMail contactlijst.</p>
<p><span class="white round label">2. E-mail adres</span> - Zorg ervoor dat de naam die zichtbaar is als de afzender van uw e-mails ClubShop Rewards bevat (Voorbeeld: Jan de Vries - ClubShop Rewards Kaart).</p>
<p><span class="white round label">3. Welkom e-mail</span> - Voordat u de ondernemer inschrijft, stuurt u hen een korte e-mail met uitleg dat zij een welkom e-mail zullen krijgen van ClubShop Rewards. Voor uw grotere of bekende winkels en bedrijven die veel klanten krijgen, zou u zich persoonlijk moeten voorstellen en hen verwelkomen als deelnemende ondernemer. Het is altijd een goed idee om hun toestemming te krijgen om een raamsticker voor hen te plaatsen en hen een kaart te verkopen tijdens uw bezoek.</p>
<p><span class="white round label">4. GroupMail Groepen</span> - Maak meerdere contactgroepen aan in uw GroupMail, omdat u slechts 100 e-mails per groep kunt versturen. Zet elke groep in verschillende alfabetische groepen (Voorbeeld: Amsterdam Nederland A tot D, Amsterdam Nederland E tot H, etc.)</p>
<p><span class="white round label">5. E-mail Gemiddelde</span> - Gemiddeld, voor elke 100 goede e-mail adressen die u verzamelt, zal ongeveer 10% reageren dat zij willen deelnemen, dat zij meer vragen hebben of dat zij willen dat u hen belt om hun deelname te bevestigen. U moet de winkels/ bedrijven bellen die hierom vragen omdat zij goede kandidaten zijn voor deelname. Door het verzamelen van een paar honderd e-mail adressen, kunt u 30 of meer nieuwe deelnemende winkels/ bedrijven genereren.</p>
<p><span class="white round label">6. Eenvoudig</span> - De e-mail die wij u bieden, is erg eenvoudig en to the point en is getest dat deze werkt. Het toevoegen van meer details aan de e-mail zorgt voor minder winkels/ bedrijven en minder goede resultaten. Hou het eenvoudig en voeg niet meer details toe aan de e-mail!</p>
</div>
</div>
</div>
</li>
<!-- TAB 3 -->
<li id="nice3Tab">
<div class="container">
<div class="row">
<div class="twelve columns">
<h6 class="subheader">Winkels/ bedrijven - Tracking</h6>
<p>Het aanmelden van winkels/ bedrijven die verkopen willen rapporteren die we kunnen traceren en om ClubCash en Commissies credit voor te geven, stelt u in staat om een terugkerend inkomen te ontwikkelen van aankopen die worden gedaan bij deelnemende winkels/ bedrijven. Echter, dit vereist tevens dat u persoonlijk de winkels/ bedrijven moet ontmoeten om uitleg te geven hoe het programma werkt. Bent u hier niet klaar voor, kunt of wilt u geen eigenaars/ bedrijfsleiders van winkels en bedrijven ontmoeten, dan gebruikt u alleen de e-mail sponsoring methode om Directe Korting ondernemers aan te melden.</p>
<h5 class="subheader">1- Sponsoring afspraak</h5>
<p>Wees enthousiast en "kleed u voor succes" wanneer u van plan bent om erop uit gaan om persoonlijk winkeliers/ bedrijfsleiders te ontmoeten. U heeft geen tweede kans om een goede eerste indruk te maken, dus laat uw kleding weerspiegelen dat u een professional bent. Het dragen van een leuk <a href="http://www.clubshop.com/mall/other/apparel_gifts.shtml" target="_blank">ClubShop Rewards polo shirt</a> zal automatisch een positieve herkenning geven van uw positie.</p>
<p>De bedoeling van uw making contact is NIET om een presentatie te geven, maar om een afspraak te maken voor een presentatie. Het kan nodig zijn dat u meerdere winkels/ bedrijven moet bezoeken om 1 afspraak te krijgen. Laat de "nee's" u niet weerhouden om de "ja's" te krijgen die u nodig hebt.</p>
<h5 class="subheader">2- Belangrijke punten sponsoring afspraak</h5>
<div class="toggle"><ol>
<li>Laat hen geen enkele informatie of zelfs uw visitekaartje zien</li>
<li>Hou uw agenda/ afsprakenboek in het zicht</li>
<li>Vergeet niet waarom u hier bent - <strong>OM EEN AFSPRAAK TE MAKEN!</strong></li>
</ol> 
<hr />
</div>
<h5 class="subheader">3- Intro sponsoring afspraak</h5>
<div class="toggle">
<p>Stel uzelf voor en geef de kaart terwijl u hen vraagt, "Heeft u al gehoord over de ClubShop Rewards kaart?"</p>
<p>NEE of NIET ZEKER = Vraag hen, "Bent u de eigenaar of de manager?" Indien niet, vraag hen of de eigenaar/manager een minuutje beschikbaar is. Zo niet, zoek uit wat het beste moment is om contact met hem/haar op te nemen.</p>
<p>WAAR GAAT DIT OVER? = Geef hen de korte introductie uitleg (zie hieronder) en vraag hen dan, "Bent u de eigenaar of de manager?" Indien niet, vraag hen of de eigenaar/manager een minuutje beschikbaar is. Zo niet, zoek uit wat het beste moment is om contact met hem/haar op te nemen.</p>
<p>JA = "Geweldig! Bent u de eigenaar of manager?" Indien niet, vraag hen of de eigenaar/manager een minuutje beschikbaar is. Zo niet, zoek uit wat het beste moment is om contact met hem/haar op te nemen.</p>
<p>Als contact is gemaat met de eigenaar, begin dan de eigenaar een eenvoudige vraag te stellen om deze te kwalificeren - "Heeft u interesse in toename van uw verkopen?"</p>
<p>NEE = "Heeft u interesse in geld besparen voor uw bedrijf en uzelf?" Probeer hen een kaart te geven en vul het aanmeldformulier met hen in. Vaak zorgt dit ervoor dat zij vragen gaan stellen, zodat u hun NEE kan veranderen in een MISSCHIEN of een JA. Het kan ook de deur voor hen openen om een Affiliate of Partner te worden.</p>
<p>MISSCHIEN OF JA = Geef een korte introductie uitleg en neem met hen deze korte belangrijke punten door.</p>
<ol>
<li>Fondsenwerving Programma - Kortingkaart ontwikkeld om de lokale non-profit organisaties te helpen om geld in te zamelen door de verkoop van de kaart. Vertel hen de namen (of laat hen een lijst zien) van een aantal non-profit organisaties die zich hebben aangemeld voor deelname.</li>
<li>Aantrekken &amp; behouden van klanten - Adverteren, Marketing en trouwe klanten programma</li>
<li>Risicoloos - GEEN kosten voor u vereist voor deelname. De enige kosten voor u zijn nadat de verkoop gedaan is</li>
<li>Percentage - Gratis, basisprogramma start al vanaf 5% per transactie. Andere mogelijkheden kunnen starten vanaf 1% per transactie.</li>
</ol>
<p>"Het programma wordt hier nu aangeboden aan lokale bedrijven om deel te nemen en dit is dan ook de reden dat ik vandaag langs kom om een afspraak te maken met u voor een moment dat ik terug kan komen om door te nemen hoe het programma werkt en te bekijken of het geschikt is voor u."</p>
<hr />
</div>
<h5 class="subheader">4- Afspraak</h5>
<div class="toggle">
<p>Terwijl u uw agenda/ afsprakenboek open doet, vraagt u "Wanneer is voor u een geschikte tijd voor ons om af te spreken, voordat u open gaat of ergens op een dag dat het rustig is?"</p>
<p><strong>Possible responses</strong></p>
<ul class="disc">
<li>AFSPRAAK MAKEN - Zie hieronder.</li>
<li>GEEN INTERESSE - "Heeft u interesse in geld besparen voor uw zaak en uzelf?" Probeer hen een kaart te geven en vul een aanmeldformulier met hen in.</li>
<li>NU DRUK - Zoek uit wanneer het goed uit komt om terug te komen. Volg hen op.</li>
</ul>
<p><strong>Mogelijke vragen</strong></p>
<ul class="disc">
<li>Hoeveel andere winkels/ bedrijven doen mee? "Momenteel zijn we net van start gegaan contact op te nemen met de winkels/ bedrijven hier in _______. Omdat dit zo'n risicoloze manier van adverteren is, verwachten we dat veel ondernemers zullen deelnemen." Vraag hen dan nogmaals, "Wat is een geschikt moment om af te spreken?"</li>
<li>Hoeveel mensen zijn lid (members) in deze regio? "Ik weet niet exact wat het huidige aantal is, maar wij plannen voor volgende maand een massale kaart distributie campagne, zodra we een goede deelnemerslijst hebben van winkels en bedrijven om te kunnen laten zien aan mogeleijke leden (members). (Indien u een aantal non-profit organisaties hebt ingeschreven in uw regio, laat hen dan weten welke dit zijn). <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">Tussen dat</span> <span class="hps">en onze</span> <span class="hps">Referral</span> <span class="hps">Marketing Plan</span></span>, <span class="short_text" id="result_box" lang="nl"><span class="hps">verwachten wij dat</span> <span class="hps">vrijwel iedereen</span> <span class="hps">in</span><span class="hps"> </span></span> _______ lid (member) zal zijn binnen het komende jaar." Vraag hen dan, "Dus, wat is een goed moment om af te spreken voor ongeveer 30 minuten?"</li>
<li>Het maakt niet uit wat hun reactie is, antwoord met een korte, maar positieve reactie, gevolgd door, "Dus, wat is een goed moment om af te spreken?"</li>
<li>Zodra u een dag en tijd heeft afgesproken, zorg er dan voor dat iedereen die betrokken is bij het maken van beslissingen, aanwezig is op de afspraak. Zorg er tevens voor dat zij de afspraak opschrijven of zet het achter op uw visitekaartje en zeg, "Indien u om wat voor reden dan ook de afspraak niet kunt nakomen, wilt u mij dan alstublieft bellen om een nieuwe afspraak te maken?"</li>
</ul>
<p><strong>Winkel/ bedrijf Presentatie</strong></p>
<p>Gebruik de dia presentatie die beschikbaar is voor uw locale gebied om presentaties mee te geven. Het is aanbevolen dat u de presentatie uitprint in kleur (zie de PDF link onderaan de eerste pagina van de presentatie) en doe elke pagina in een plastic insteekhoesje in een 4 ringband om een flipover presentatiemap te maken.</p>
<div class="row">
<div class="twelve columns">
<div class="six columns">
<div class="panel">
<ul>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/awen/" target="_blank">Aruba (English)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/awnl/" target="_blank">Aruba (Dutch)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/auen/" target="_blank">Australia (English)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/benl/" target="_blank">Belgie (Nederlands)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/befr/" target="_blank">Belgium (French)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/caen/" target="_blank">Canada (English)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/coes/" target="_blank">Colombia (Spanish)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/annl.curacao/" target="_blank">Curacao, Netherlands Antilles (Nederlands)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/anen.curacao/" target="_blank">Curacao, Netherlands Antilles (English)</a></li>
</ul>
</div>
</div>
<div class="five columns ">
<div class="panel">
<ul>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/frfr/" target="_blank">France (French)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/gben/" target="_blank">Great Britain (English)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/inen/" target="_blank">India (English)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/itit/" target="_blank">Italy (Italian)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/mxes/" target="_blank">Mexico (Spanish)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/nlnl/" target="_blank">Nederland (Nederlands)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/srnl/" target="_blank">Suriname (Nederlands)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/tnfr/" target="_blank">Tunisia (French)</a></li>
<li><img class="image_right_padding" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <a class="nav_footer_brown" href="http://www.glocalincome.com/presentations/pr1/usen/" target="_blank">USA (English)</a></li>
</ul>
</div>
</div>
</div>
</div>
<p><strong>Presentatie Overzicht en Fasen</strong></p>
<ul>
<li>Wat is een veronderstelde deal? U kunt er van uitgaan dat uw prospect zal aanmelden <span id="result_box" lang="nl"><span class="hps alt-edited">als gevolg van</span> <span class="hps">het overwinnen van</span> <span class="hps">potentiële bezwaren gedurende de presentatie</span></span>.</li>
<li>Waarom kunt u veronderstellen dat zij zich zullen aanmelden wanneer u klaar bent met de presentatie? Omdat u hen heeft kunnen kwalificeren en heeft kunnen ingaan op de 3 grote bezwaren die zij kunnen hebben, gedurende het gesprek en de presentatie.</li>
<li>Ontwikkel empathie en zoek uit wat zij hebben gedaan of nu doen aan adverteren en promotie en of zij een trouwe klanten programma hebben.</li>
<li>Het BELANGRIJKSTE punt om vast te stellen voordat u verder gaat met de presentatie is na te gaan of zij bereid zijn om te adverteren, promoten, korting te geven, etc. om <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">meer klandizie</span> <span class="hps">te krijgen</span></span>. Is het antwoord "nee", ga dan niet verder met de presentatie en geef hen een ClubShop Rewards kaart zodat zij zich kunnen aanmelden als een gratis Member. Door dit te doen, is het mogelijk dat zij meer vragen gaan stellen en dat zij misschien hun bereidheid om deel te nemen als ondernemer zullen heroverwegen.</li>
<li>Gebruik de flipover-map om de presentatie te doen en maak twee hoofd gesprekspunten om te stoppen en ze vast te leggen.</li>
</ul>
<p><strong>A.</strong></p>
<p>Transactie Bijdrage Percentage: Na het doornemen van de bijdrage en marketing mogelijkheden informatie, de mogelijkheid om percentages te wijzigen en te stoppen, vraag hen, "Hebt u het gevoel dat u ten minste 10% korting kunt geven, of een "2 voor de prijs van 1" aanbieding, of voelt u zich beter bij een korting van 5% of minder?"</p>
<ul>
<li>10% Korting - Ga verder met de presentatie, maar sla de informatie dia's over tracking over</li>
<li>2 voor de prijs van 1 aanbieding - Ga verder met de presentatie, maar sla de informatie dia's over tracking over</li>
<li>5% Korting - Geef uitleg over de gratis Tracking Methode mogelijkheid en hoe de basis opname en het proces van <span id="result_box" lang="nl"><span class="hps">overdracht van informatie</span><span class="hps"> werkt</span></span>. Vraag hen dan, "Denkt u dat u hier problemen mee zult hebben?"<br /><br /> 
<ul>
<li>Probleem - Stel vast of er aanvaardbare tracking methoden zijn (scanner of mobiele telefoon) of dat zij voorkeur hebben voor de Directe korting optie. Ga verder op basis van hun reactie.</li>
<li>Geen Probleem - Laat hen het handmatige transactie tracking formulier zien, vervolgens aanraden dat zij van start gaan met het handmatige  proces dat zij zelf kunnen invoeren, of dat zij naar u kunnen faxen of u haalt het op bij hen <a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">(Print Formulier).</a></li>
</ul>
</li>
<li>Minder dan 5% korting - ga verder met de presentatie, maar sla de dia's met informatie over tracking over en neem de Marketing Paketten met hen door wanneer u bij deze dia aankomt.</li>
<p>Erg belangrijk, zorg ervoor dat u hen "vastlegt" met een methode die acceptabel is voor hen, voordat u verder gaat met de presentatie. Indien geen van de mogelijkhede acceptabel is, ga dan niet verder met de presentatie en schrijf hen in als een gratis ClubShop Rewards Member of verkoop heb een  ClubShop Rewards kaart.</p>
</ul>
<p><strong>B.</strong></p>
<p>Directe Korting: Indien de ondernemer aangeeft dat deze tevreden is met de gratis, 10% korting of BOGO (2 voor de prijs van 1) mogelijkheid, sla dan de dia over die verwijst naar de maandelijkse betaling aan ClubShop Rewards. Als zij hebben gekozen voor 1 van de andere mogelijkheden, leg dan de automatische afschrijving van hun credit kaart of debit kaart uit of de <span class="short_text" id="result_box" lang="nl"><span class="hps">pre</span> <span class="hps">financiering van een</span></span> ClubAccount.</p>
<ul>
<li>Leg hen uit dat het ClubShop Rewards bedrijf te maken heeft met winkels en bedrijven van overal ter wereld en dat het bedrijf uiteraard niet kan wachten op <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">cheques </span></span>via de post. Zeg dan, "<span class="short_text" id="result_box" lang="nl"><span class="hps">Dat klinkt logisch</span><span>,</span> <span class="hps">nietwaar</span></span>?"</li>
<li>Laat hen weten dat we hen hun rapport online aanbieden en we hen elke eerste van de maand een e-mail sturen om hen op de hoogte te stellen van het totale transactie bedrag.</li>
<li>Vraag hen of zij voorkeur hebben voor het gebruik van een credit kaart of debit kaart of het instellen van een ClubAccount.</li>
</ul>
<hr />
</div>
<h5 class="subheader">5- Beantwoord vragen &amp; het invullen van het aanmeldformulier</h5>
<div class="toggle">
<p>Na afloop van de presentation, vraagt u hen of zij nog vragen hebben, terwijl u het aanmeldformulier pakt en beantwoord u kort hun vragen.</p>
<ul>
<li><a href="https://www.clubshop.com/print/clubshop_merchant_application.pdf" target="_blank">KLIK HIER</a> voor het te printen ondernemers aanmeldformulier</li>
</ul>
<p>Bevestig hun bedrijfsnaam en begin met het invullen van het aanmeldformulier. Wanneer u bij de transactie bijdrage % komt, vraag hen dan met welk percentage zij willen starten (herinner hen eraan dat zij dit elk moment weer kunnen veranderen). Wanneer zij zeggen dat zij erover moeten nadenken, stel dan voor dat zij starten met de gratis 5% transactie bijdrage mogelijkheid, maar zij kunnen ervoor kiezen een hogere transactie bijdrage % te geven.</p>
<p>Wanneer het voor hen "onmogelijk" is om een credit kaart te gebruiken, bied hen dan de mogelijkheid aan om een storting te doen van $50 USD waarvan hun transactie bijdrage dan wordt afgetrokken. Wanneer zij ervoor kiezen een storting te doen door u geld of een cheque te geven, geef hen dan een ontvangstbewijs. Zet hun betaling op uw bankrekening, stort dan hetzelfde bedrag op uw ClubAccount (indien nodig) dat zij u hebben betaald. <span class="short_text" id="result_box" lang="nl"><span class="hps">Bij het</span> <span class="hps">indienen van het aanmeldformulier</span></span>, ontvangt u als de sponsor een e-mail met aanwijzingen over hoe u de nodige gelden van uw ClubAccount overschrijft om hun ClubAccount op te zetten.</p>
<p>Neem de voordelen door die de gratis Tracking winkels/ bedrijven hebben.</p>
<p>Laat hen het aanmeldformulier ondertekenen, geef hen dan een kopie van de Algemene Voorwaarden. Omdat ze gekozen hebben voor de gratis Tracking mogelijkheid, maakt u zo snel mogelijk een nieuwe afspraak, om terug te komen en hen te "installeren".</p>
<hr />
</div>
<h5 class="subheader">6- Bezwaren overwinnen - Member Aanmeldformulier</h5>
<div class="toggle">
<p>Ondanks al uw inspanningen, zullen sommigen "nee" zeggen of meer waarschijnlijk, afhouden. "Ik moet hier over nadenken" of "Ik moet dit bespreken met  _____".</p>
<p>Wanneer dit gebeurt, vraag hen wat het in het bijzonder is waar zij over moeten "denken" - het percentage dat zij willen geven OF hoe wij hun account debiteren? Doe uw best om het bezwaar te beantwoorden, maar druk niet door. Help hen met het invullen van een kaarthouder aanmeldformulier, zodat zij kunnen zien  hoe het werkt en zoek dan uit wanneer u bij hen terug kunt komen.</p>
<p><span class="green round label">Tip:</span> Spreek NIET met hen over het werven van anderen totdat u terug komt om hen te installeren.</p>
<p>Stuur hen een e-mail nadat u hen hebt aangemeld en om hun "Installatie" datum en tijd te bevestigen.</p>
<hr />
</div>
<h5 class="subheader">7- Opvolgen en Doorpakken</h5>
<div class="toggle">
<p>Voor hen die geen "nee" hebben gezegd, maar de beslissing hebben afgehouden; volg hen op nadat u wat "nieuws" heeft over nieuwe winkels en/ of kaart  distributie.</p>
<p>Nog belangrijker, ga er op uit en werk met iemand anders. Laat het behalen van uw doelen voor aanmeldingen niet alleen afhangen van het opvolgen. Neem contact op met nieuwe winkels en bedrijven!</p>
<hr />
</div>
<h5 class="subheader">8- Tracking Winkels/ bedrijven installeren</h5>
<div class="toggle">
<p>Nadat u een ondernemer hebt aangemeld die de tracking mogelijkheid heeft gekozen, moet u tijd met hen afspreken om hen te "Installeren" om de ClubShop Rewards kaart te ontvangen en hen te trainen hoe zij de transactie informatie insturen, alsmede hen <span class="short_text" id="result_box" lang="nl"><span class="hps">oriënteren</span> </span>op het zijn van een aangesloten ondernemer. U moet tevens wat tijd besteden om hen opnieuw kennis te laten maken met de voordelen van het verkopen van kaarten.</p>
<p>Omdat deze "Installatie"tijd tot een uur kan duren, is het gewoonlijk noodzakelijk om een nieuwe afspraak met hen te maken, nadat de aanmelding is gedaan. Wanneer de ondernemer ervoor kiest om diens transactie bijdragen te betalen met een credit kaart, dan willen zij de transactie handmatig invoeren en hebben zij toegang tot een computer, het kan dan mogelijk zijn om hen direct te "installeren" na hun "aanmelding".</p>
<hr />
</div>
<h5 class="subheader">9- Winkel/ bedrijf voorbereiding op installatie</h5>
<div class="toggle"><ol>
<li>
<p>Stel vast wat hun methode van betalen is om hun toekomstige transactie bijdragen te betalen.</p>
<ul class="disc">
<li>Credit kaart of Debit kaart - Wanneer hun credit- of debit kaart is goedgekeurd, ontvangt u een bericht van goedkeuring, samen met het IDnummer van de ondernemer. Wanneer hun credit kaart niet is goedgekeurd, krijgt alleen de ondernemer hiervan bericht met de uitleg van de reden waarom. Indien dit gebeurt, moet u opvolgen met de ondernemer om een andere credit kaart te gebruiken of om hen te helpen een ClubAccount op te zetten om hun transactie bijdragen te betalen.</li>
<li>ClubAccount - Wanneer de winkel/ het bedrijf ervoor kiest om geen gebruik te maken van een credit kaart of debit kaart, dan moet u hen helpen om geld op hun ClubAccount te zetten. De gemakkelijkste manier om dit te doen is door de ondernemer direct aan u te laten betalen, waarbij u hen een ontvangstbewijs van betaling geeft van het geld dat zij u hebben gegeven. Zij dienen u voldoende geld te geven om te betalen voor hun deposit ($50 USD). U moet vervolgens hun storting op uw bankrekening zetten en indien nodig overmaken naar uw ClubAccount, dan stuur u een online Transfer Authorisatie formulier in (Dit formulier kunt u vinden in uw Business Control Center) om ons bedrijf te autoriseren om de gelden over te maken van uw ClubAccount naar de ClubAccount van de ondernemer.</li>
</ul>
</li>
<li>
<p>Stel vast wat hun methode voor het verzenden van de transactiekosten is.</p>
<ul class="disc">
<li>MANUEEL - Dit is de best aanbevolen methode voor de meeste ondernemers om mee te beginnen. Wanneer zij ervoor kiezen deze methode te gebruiken door het invullen van de transactie informatie op een <a href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">Ondernemers Transactie invulformulier</a> , stel vast of zij toegang hebben tot het internet om de informatie in te kunnen vullen via hun Ondernemer Account, of dat zij het formulier naar u moeten faxen of door u op moeten laten halen zodat u de transacties zelf in kunt voeren. Indien gekozen is voor de manuele methode, dan is de minimale tijd die is toegestaan om de transacties in te voeren, wekelijks, maar hoe vaker dit gedaan wordt des te beter. </li>
<li>SCANNER EN COMPUTER - Om deze methode te kunnen gebruiken, heeft de ondernemer toegang nodig tot een computer die online is op het verkooppunt. Het scannertype dat wij adviseren om hen te laten gebruiken of aankopen dient een goedkope USB Barcode Scanner te zijn. U zou moeten uitzoeken wat de meest betaalbare scanners zijn die beschikbaar zijn waar u woont, zodat u de ondernemer kan verwijzen waar zij een scanner kunnen kopen, of zodat u een scanner kunt kopen voor uw ondernemer en hen u kunt laten betalen voor de scanner wanneer u hen help"installeren". Wanneer u de scanner online besteld (bij voorkeur bij een ClubShop Winkelcentrum winkel), hou dan rekening met de levertijd van de scanner bij het maken van een vervolg afspraak voor het "installeren".</li>
</ul>
</li>
</ol> 
<hr />
</div>
<h5 class="subheader">10-Procedures voor installeren</h5>
<div class="toggle"><ol>
<li>
<p>Zorg ervoor dat uw afspraak voor het "Installeren" plaats kan vinden op een tijdstip dat de ondernemer niet zal worden afgeleid of gestoord. Neem alle benodigde zaken mee om de ondernemer op de juiste manier te "installeren", inclusief:</p>
<ul class="circle">
<li>Ondernemer ID Nummer</li>
<li>Internettoegang</li>
<li>Manuele transactie invulformulieren (minimaal 10)</li>
<li>Ondernemer raamsticker</li>
<li>Kopie van hun Ondernemer aanmeldformulier (<span id="result_box" lang="nl"><span class="hps">indien</span> <span class="hps">vooraf ingevuld en</span> <span class="hps">geprint aanmeldformulier</span></span>)</li>
<li><span id="result_box" lang="nl"><span class="hps">Indien van toepassing</span> <span class="hps">een ondernemer o</span><span class="hps">ntvangst</span> <span class="hps">stempel</span></span></li>
<li>Indien van toepassing een scanner</li>
<li>Indien van toepassing, ClubShop Rewards kaarten voor hen om te kopen en distribueren (ten minste 50) en alle andere Partner materialen (Brochures, Lijst van winkels en bedrijven, Cover Letters, Verzamelbus voor aanmeldingen)</li>
</ul>
</li>
<li>
<p>Laat hen inloggen op hun Ondernemer Account (https://www.clubshop.com/maf). Een voorbeeld Ondernemer Account kunt u bekijken door in te loggen met de volgende informatie: <br /><br /> Gebruikersnaam: bill | Wachtwoord: mac | ID: 41 | Kies Main Store <br /><br /> Leg hen uit hoe door de site te navigeren om de informatie te vinden die zij nodig kunnen hebben. Activeer het Ondernemer formulier, de ondernemer instellen pagina in hun Ondernemer account en leer dan de ondernemer hoe deze de transacties moet invoeren en indienen.</p>
</li>
<li>
<p>Leg uit hoe zij de verkoopbonnen tekenen of stempelen. Neem met hen de procedure door wanneer klanten artikelen retourneren, om er zeker van te zijn dat zij geen transactie bijdragen betalen voor artikelen die zijn teruggebracht.</p>
</li>
<li>
<p>Laat hen hun eigen ondernemer transactierapport zien en leg uit hoe zij een e-mail zullen ontvangen op de eerste dag van elke maand, als bevestiging van de transacties en het bedrag die zij het bedrijf moeten betalen op de 5de van eke maand van de transacties die zij invulden.</p>
</li>
<li>
<p>Neem de betaalmethode met hen door en wanneer zij hun ClubAccount gebruiken, leer hen dan hoe zij hun ClubAccount pagina openen en hoe zij er geld op zetten voor toekomstige transactie bijdragen betalingen.</p>
</li>
<li>
<p>Laat hen de opname van hun winkel/ bedrijf zien via <a href="https://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank">Google Maps Directory</a></p>
</li>
<li>
<p>Beantwoord alle vragen die zij hebben en leg uit wanneer de fase van kaart distributie voor uw campagne zal plaatsvinden. Geef uw visitekaartje en vraag om contact met u op te nemen zodra zij hun eerste ClubShop Rewards klant hebben gehad, zodat u ervoor kunt zorgen dat alles goed is gegaan. Plaats de Ondernemer raamsticker aan de binnekant op een glazen voordeur of aan de binnenkant van een raam naast de voordeur.</p>
</li>
</ol></div>
</div>
</div>
</div>
</li>
<p><input alt="Print" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /></p>
<!-- / TAB 3 --> 
</ul>
<!--/ Content Place Holder --> <!--/ Content Place Holder --> <!--/ Content Place Holder --></div>
</div>
<!-- / nine columns --></div>
<!-- / Main container Row --></div>
<!--  / container --> <!--End Main Container --> <!--Footer Container --> <!--#include virtual="common/footer/footer.shtml" --> <!--End Footer Container -->
</body>
</html>