// use the top copy if you want the popup to be flush top left
function popup(URL, width, height) {
	var winl = 0;
	var wint = 0;
	day = new Date();
	id = day.getTime();
	window.open(URL, id, 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=' + width + ',height=' + height + ',left=' + winl + ',top=' + wint);
}

// use the second copy if you want the popup to be center page
function popupcenter(URL, width, height) {
	var winl = (screen.width - width) / 2;
	var wint = (screen.height - height) / 2;
	day = new Date();
	id = day.getTime();
	window.open(URL, id, 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=' + width + ',height=' + height + ',left=' + winl + ',top=' + wint);
}

// use the second copy if you want the popup to be center page
function popupcenter_scroll(URL, width, height) {
	var winl = (screen.width - width) / 2;
	var wint = (screen.height - height) / 2;
	day = new Date();
	id = day.getTime();
	window.open(URL, id, 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=' + width + ',height=' + height + ',left=' + winl + ',top=' + wint);
}

// Remember that you you want to use the center option type javascriptcenter in the link to in the design window