<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 



   <xsl:template match="/">

    

<html><head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Local Marketing: Merchants</title>

<meta name="title" content="Glocal Income"/>

<meta name="Keywords" content="Glocal Income, offline marketing, e-business, training, learning, be successful, Entrupenure"/>

<meta name="abstract" content="Learn everything you need to know to create your own business."/>

<meta http-equiv="content-language" content="en-us"/>

<meta http-equiv="content-language" content="english"/>

<meta http-equiv="content-language" content="lang_en"/>

<meta name="coverage" content="Worldwide"/>

<meta name="distribution" content="Global"/>

<meta name="author" content="Glocal Income"/>
<meta name="publisher" content="Glocal Income"/>
<meta name="company" content="Glocal Income"/>
<meta name="copyright" content="Copyright © 2009 Glocal Income"/>
<meta name="page-topic" content="Glocal Income"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="General"/>
<meta name="revisit-after" content="7 days"/>



<link href="../styles/css/pages.css" rel="stylesheet" type="text/css" />


<style type="text/css">


.blue {

color: #0066CC;
font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.free {
	color: #339900;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.basic {
	color: #0066CC;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.bronze { color: #DE850C;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.gold { color: #B79300;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.silver {	color: #999999;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.platinum {	color: #0099FF;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.style3 {

	font-size: 12px;

	font-family: verdana;

	color: #A91B07;

	font-weight:bold;

}
</style>





</head>

<body>

<table width="100%" border="0" cellspacing="0" cellpadding="10">

  <tr>

    <td align="right" bgcolor="#FFFFFF"><span class="Header_Main"> </span>



    <hr align="left" width="100%" size="1" color="#A35912"/></td>

  </tr>

  <tr>

    <td valign="top" bgcolor="#FFFFFF"><span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span><span class="Text_Content"><br/>

    </span><br />
    <p><xsl:copy-of select="//p2/* | //p2 /text()"/></p>

    <p><xsl:copy-of select="//p3/* | //p3 /text()"/></p>

<p><xsl:copy-of select="//p4/* | //p4 /text()"/></p>
<p><xsl:copy-of select="//p5/* | //p5 /text()"/></p>
<p><xsl:copy-of select="//p6/* | //p6 /text()"/></p>
<p><xsl:copy-of select="//p7/* | //p7 /text()"/></p>


<p><span class="style3"><xsl:value-of select="//lang_blocks/p8"/></span><br/>
<xsl:value-of select="//lang_blocks/p9"/><br/>
<xsl:value-of select="//lang_blocks/p10"/></p>

<p><xsl:value-of select="//lang_blocks/p11"/>
</p>


    <br/>
    <br/>
   
    
</td>



  </tr>

  <tr>

    <td align="right" bgcolor="#FFFFFF"><hr align="left" width="100%" size="1" color="#A35912"/>

        <a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="../images/icons/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle"/>PRINT PAGE</a><a href="javascript:window.close();" class="nav_footer_brown"></a></td>

  </tr>

</table>

</body>

</html>









































</xsl:template>

</xsl:stylesheet>

