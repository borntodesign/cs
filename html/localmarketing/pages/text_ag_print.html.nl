<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Affinity Groep Folders en Formulieren</title>

<!-- Set the viewport width to device width for mobile -->
		<meta name="viewport" content="width=device-width" />
		
<META NAME="title" CONTENT="Glocal Income">
<META NAME="Keywords" content="Glocal Income, offline marketing, e-business, training, learning, be successful, Entrupenure">
<META NAME="abstract" CONTENT="Learn everything you need to know to create your own business.">
<META HTTP-EQUIV="content-language" CONTENT="en-us">
<META HTTP-EQUIV="content-language" CONTENT="english">
<META HTTP-EQUIV="content-language" CONTENT="lang_en">
<META NAME="coverage" CONTENT="Worldwide">
<META NAME="distribution" CONTENT="Global">
<META NAME="author" CONTENT="Glocal Income">
<META NAME="design-development" CONTENT="">
<META NAME="publisher" CONTENT="Glocal Income">
<META NAME="company" CONTENT="Glocal Income">
<META NAME="copyright" CONTENT="Copyright ? 2009 Glocal Income">
<META NAME="page-topic" CONTENT="Glocal Income"> 
<META NAME="robots" CONTENT="index,follow">
<META NAME="rating" CONTENT="all">
<META NAME="audience" CONTENT="General">
<META NAME="revisit-after" CONTENT="7 days">


<!-- Included CSS Files -->
		
		<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css"/>
		<!-- <link rel="stylesheet" type="text/css" href="/css/foundation/app.css"/> -->
		<link rel="stylesheet" type="text/css" href="/css/foundation/local_marketing.css"/>    
		<link rel="stylesheet" type="text/css" href="/css/foundation/general.css"/> 
		 
		<!-- [if lt IE 9] >
			<link rel="stylesheet" href="/css/foundation/ie.css">
		<![endif]>
	 
		 
		<!-- IE Fix for HTML5 Tags -->
		<!-- [if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif] -->
	 
	 
		<!-- Included JS Files -->

		<script src="/js/jquery-1.7.1.min.js"></script>    
		<script src="/js/foundation/foundation.js"></script>  
		<script src="/js/foundation/modernizr.foundation.js"></script>
		<script src="/js/foundation/app.js"></script>   
<style>
		
		.text-centered{text-align:center;}
		.nav-bar > li {padding: 0px 10px;}
		
		.nav-bar > li > a {
			display: block;
			font-size: 1.5rem;
			padding: 0 0px;
			position: relative;
			text-decoration: none;
		}
		
</style>
		

</head>

<body>

	<div class="container">
	<div class="row">
	<div class=" twelve columns ">             
	<div class="panel">
	
		<div class="row blue">     
            <div class=" six columns ">             
                <a href="#"><img src="../images/cs_logo.jpg" style=" margin:5px; " height=" 84px " width=" 288px "></a>
            </div>

             
            <div class=" six columns ">             
                <!--Extra Space Future Space -->                
            </div>          
        </div>  
		
	
		<div class="row">
			<div class="twelve columns"> 			   
				<br/>
				<h3>Afdrukbare folders en formulieren voor verenigingen (AG's)</h3>
				
				<p>We nodigen u uit de volgende promotionele hulpmiddelen te downloaden als hulp bij uw AG marketing campagne.</p>
				
				<p>De hulpmiddelen zijn in PDF opgesteld om sneller te kunnen downloaden.</p>
				
				<p>Nadat u het document hebt geopend, klik dan aan de rechterbovenkant van de PDF viewer op “Velden markeren”. U zult gekleurde velden zien tevoorschijn komen. Deze velden dienen om de naam van de AG’s in te vermelden. Het pre-order formuliertje heeft een extra veld waar kan ingevuld worden naar wie de cheques dienen verstuurd te worden. Van zodra u de velden hebt voorzien van de nodige gegevens, bent u in staat onbeperkt formulieren en folders af te drukken.</p>
				
			</div>		
			<hr/>
			
		</div>		
	
		<div class="row">
			<div class=" twelve columns "> 	
				<h5 >FOLDERS</h5>
			</div>
		</div>
		
		
		<div class="row">
			<div class=" six columns "> 		
				<div class="panel">
				
					<div class="row">
						<div class=" three columns "> 
							<img src="../images/thumbs_download/ag_flyer_en.png" width="108" height="143" align="left" >
						</div>
						<div class=" nine columns "> 
						
							<h5	class="text-centered">Affinity Groep Marketing Folder</h5> 
							
							<p></p>
							
							<p class="text-centered"><img src="../images/icons/icon_checkmark.png" width="12" height="12" align="absmiddle" > Kleur <img src="../images/icons/icon_pdf.png" alt="pdf file" width="37" height="39"></p>
							
							<p class="text-centered">3 velden in te vullen</p>							
							
						</div>
					</div>
					
					<span>Folder downloaden</span>
					
					<ul class="nav-bar" style="margin-top:0px;" >
						<li><a href="http://www.clubshop.com/print/ag_flyer_en.pdf" >ENGELS</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_nl.pdf" >NEDERLANDSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_fr.pdf" >FRANSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_it.pdf" >ITALIAANS</a></li>
					</ul>					
					
				</div>
			</div>
			
			
		
			<div class=" six columns "> 		
				<div class="panel">
				
					<div class="row">
						<div class=" three columns "> 
							<img src="../images/thumbs_download/ag_flyer_bw_en.png" width="108" height="143" align="left" >
						</div>
						<div class=" nine columns "> 
						
							<h5	class="text-centered">Affinity Groep Marketing Folder</h5> 
							
							<p></p>
							
							<p class="text-centered"><img src="../images/icons/icon_checkmark.png" width="12" height="12" align="absmiddle" > Zwart-Wit <img src="../images/icons/icon_pdf.png" alt="pdf file" width="37" height="39"></p>
							
							<p class="text-centered">3 velden in te vullen</p>						
							
						</div>
					</div>
					
					<span>Folder downloaden</span>
					
					<ul class="nav-bar" style="margin-top:0px;" >
						<li><a href="http://www.clubshop.com/print/ag_flyer_bw_en.pdf" >ENGELS</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_bw_nl.pdf" >NEDERLANDSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_bw_fr.pdf" >FRANSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_flyer_bw_it.pdf" >ITALIAANS</a></li>
					</ul>				
					
				</div>
			</div>			
			
		</div>

		<div class="row">
			<div class=" six columns "> 		
				<div class="panel">
				
					<div class="row">
						<div class=" three columns "> 
							<img src="../images/thumbs_download/ag_preorder_form_en.png" width="103" height="181" align="left" >
						</div>
						<div class=" nine columns "> 
						
							<h5	class="text-centered">Affinity Groep Kaart Pre-Order Formulier</h5> 
							
							<p></p>
							
							<p class="text-centered"><img src="../images/icons/icon_checkmark.png" width="12" height="12" align="absmiddle" > PDF Form <img src="../images/icons/icon_pdf.png" alt="pdf file" width="37" height="39"></p>
							
							<p class="text-centered">3 velden in te vullen</p>							
							
						</div>
					</div>
					
					<span>Folder downloaden</span>
					
					<ul class="nav-bar" style="margin-top:0px;" >
						<li><a href="http://www.clubshop.com/print/ag_order_form_en.pdf" target="_blank" >ENGELS</a></li>
						<li><a href="http://www.clubshop.com/print/ag_order_form_nl.pdf" target="_blank" >NEDERLANDSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_order_form_fr.pdf" target="_blank" >FRANSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_order_form_it.pdf" target="_blank" >ITALIAANS</a></li>
					</ul>					
					
				</div>
			</div>
				
			<div class=" six columns "> 		
				<div class="panel">
				
					<div class="row">
						<div class=" three columns "> 
							<img src="../images/thumbs_download/ag_preorder_form_en.png" width="108" height="143" align="left" >
						</div>
						<div class=" nine columns "> 
						
							<h5	class="text-centered">Affinity Groep 'Kaart Activeren' Formulier</h5> 
							
							<p></p>
							
							<p class="text-centered"><img src="../images/icons/icon_checkmark.png" width="12" height="12" align="absmiddle" > PDF Form <img src="../images/icons/icon_pdf.png" alt="pdf file" width="37" height="39"></p>
							
							<p class="text-centered">3 velden in te vullen</p>							
							
						</div>
					</div>
					
					<span>Folder downloaden</span>
					
					<ul class="nav-bar" style="margin-top:0px;" >
						<li><a href="http://www.clubshop.com/print/ag_activate_form_en.pdf" target="_blank" >ENGELS</a></li>
						<li><a href="http://www.clubshop.com/print/ag_activate_form_nl.pdf" target="_blank" >NEDERLANDSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_activate_form_fr.pdf" target="_blank" >FRANSE</a></li>
						<li><a href="http://www.clubshop.com/print/ag_activate_form_it.pdf" target="_blank" >ITALIAANS</a></li>
					</ul>				
					
					
				</div>
			</div>			
			
		</div>		  
	  
    </div>  
	</div>
	</div>     
	</div>              
</body>

</html>

