﻿<?xml version="1.0" ?>

<?xml-stylesheet type="text/xsl" href="text_ag_email.xsl" ?>

<lang_blocks>
<p1>EAG - 4 - Local AG Prospecting Email</p1>
<p2 literal="1">Onderwerp: <span style="color: red;">(NAAM VAN DE ORGANISATIE)</span> - Nieuwe Fundraiser</p2>
<p3 literal="1">Beste <span style="color: red;">VOORNAAM</span>,</p3>
<p4 literal="1">Laat me beginnen met mezelf even voor te stellen. Mijn naam is<span style="color: red;">(UW VOOR- EN ACHTERNAAM)</span> en ik ben een lokale Affinity Groep consulent voor ClubShop Rewards.</p4>
<p5 literal="1">Het is algemeen geweten dat fondsen werven om een non-profit of charitatieve organisatie te steunen moeilijk en tijdrovend kan zijn. Dit is de reden waarom ik oprecht enthousiast ben over wat wij <span style="color: red;">(NAAM VAN HUN ORGANISATIE)</span>te bieden hebben.</p5>
<p6 literal="1">We bieden <span style="color: red;">(NAAM VAN HUN ORGANISATIE)</span> twee geweldige manieren van fondsenwerving aan, beide zijn snel en op permanente basis. Om meer te weten over het ClubShop Rewards Affinity Groep programma, ga naar:  http://www.clubshop.com/cgi-bin/rd/10/0/refid%3D<span style="color: red;">UWIDNUMMER</span>/cs/non_profit.shtml</p6>

<p7 literal="1">Op de site kunt u  <span style="color: red;">(NAAM VAN HUN ORGANISATIE)</span> geheel gratis en zonder verplichting registreren als een ClubShop Rewards Affinity Groep. Na registratie neem ik contact met u op om een afspraak te regelen en alle fundraiser opties te doorlopen.</p7>
<p8 literal="1">If after reviewing the information at our website, should you have any questions, please don't hesitate to contact me. You can email me or call me at <span style="color: red;">(YOUR PHONE NUMBER)</span>. </p8>
<p9 literal="1">Mochten u na het bekijken van de informatie op onze website nog enige vragen hebben, aarzel dan niet mij elektronisch of telefonisch te contacteren.

Ik verheug me op een gesprek en zie er naar uit om <span style="color: red;">(NAAM VAN HUN ORGANISATIE)</span> en u te helpen bij uw fondsenwerving.</p9>
<p10>Met oprechte groet,</p10>
<p11>UW NAAM</p11>
<p12>UW E-MAILADRES</p12>
<p13>P.S. - ClubShop Rewards hanteert een strikt spambeleid. Als u niet wenst gecontacteerd te worden per e-mail door mezelf of een andere ClubShop Rewards consulent, gelieve deze e-mail te negeren en (http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress=<span style="color: red;">THEIRIDNUMBER</span>) om uw e-mailadres automatisch toe te voegen aan onze “Do Not Email” lijst. </p13>
<p14 literal="1"><span style="color: red; font-weight:bold;">UW POSTADRES MOET HIER TE ZIEN ZIJN </span></p14>




</lang_blocks>