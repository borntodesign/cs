﻿<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Affinity Groep Presentaties</title>
<META NAME="title" CONTENT="Glocal Income">
<META NAME="Keywords" content="Glocal Income, offline marketing, e-business, training, learning, be successful, Entrupenure">
<META NAME="abstract" CONTENT="Learn everything you need to know to create your own business.">
<META HTTP-EQUIV="content-language" CONTENT="en-us">
<META HTTP-EQUIV="content-language" CONTENT="english">
<META HTTP-EQUIV="content-language" CONTENT="lang_en">
<META NAME="coverage" CONTENT="Worldwide">
<META NAME="distribution" CONTENT="Global">
<META NAME="author" CONTENT="Glocal Income">
<META NAME="design-development" CONTENT="">
<META NAME="publisher" CONTENT="Glocal Income">
<META NAME="company" CONTENT="Glocal Income">
<META NAME="copyright" CONTENT="Copyright ? 2009 Glocal Income">
<META NAME="page-topic" CONTENT="Glocal Income">
<META NAME="robots" CONTENT="index,follow">
<META NAME="rating" CONTENT="all">
<META NAME="audience" CONTENT="General">
<META NAME="revisit-after" CONTENT="7 days">


<!-- Included CSS Files -->
		
		<link rel="stylesheet" type="text/css" href="../../../css/foundation/foundation.css"/>
		<link rel="stylesheet" type="text/css" href="../../../css/foundation/app.css"/>
		<link rel="stylesheet" type="text/css" href="../../../css/foundation/local_marketing.css"/>    
		 
		 
		<!-- [if lt IE 9] >
			<link rel="stylesheet" href="../../../css/foundation/ie.css">
		<![endif]>
	 
		 
		<!-- IE Fix for HTML5 Tags -->
		<!-- [if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif] -->
	 
	 
		<!-- Included JS Files -->

		<script src="../../../js/jquery-1.7.1.min.js"></script>    
		<script src="../../../js/foundation/foundation.js"></script>    
		 
		<style type="text/css">
		
		.text-centered{text-align:center;}		
		
		.nice.tabs.vertical dd a:link {
			background: none repeat scroll 0 0 #FFFFFF;						
			height: auto;
			margin: 0;
			position: static;
			top: 0;
		}
		
		.nice.tabs.vertical dd a:visited {
			background: none repeat scroll 0 0 #FFFFFF;						
			height: auto;
			margin: 0;
			position: static;
			top: 0;
		}
		
		.nice.tabs.vertical dd a:hover {
			background: none repeat scroll 0 0 #00A6FC;			
			color: #FFFFFF;
			height: auto;
			margin: 0;
			position: static;
			top: 0;
		}
		
		.nice.tabs.vertical dd a:active {
			background: none repeat scroll 0 0 #FFFFFF;					
			height: auto;
			margin: 0;
			position: static;
			top: 0;
		}
		
		
	</style>
</head>



<body>

	<div class="container">
	<div class="row">
	<div class=" twelve columns ">  
	<div class="panel">
		<div class="row blue">     
            <div class=" six columns ">             
                <a href="#"><img src="../images/cs_logo.jpg" style=" margin:5px; " height=" 84px " width=" 288px "></a>
            </div>

             
            <div class=" six columns ">             
                <!--Extra Space Future Space -->                
            </div>          
        </div>  
		<p></p>
		<h3>Affinity Groep Presentaties</h3>
       
		<h5>We nodigen u uit deze promotionele marketing tools te bekijken, te downloaden en te gebruiken als visuele hulp bij uw Affinity Groep marketing campagnes.</h5>
		
		<hr/>
		
		<div class="row">
		<div class=" ten columns centered"> 
		
		
			<h4>Affinity Groep Marketing Presentatie</h4>
			<h6>Gebruik deze presentatie om ons Affinity Groep programma uit te leggen aan een contactpersoon of groep (met behulp van een videoprojector).</h6>
			<p></p>
			<div class="row">
				<div class=" four columns "> 
					<h6 style="text-align:center; margin-bottom:5px;"> <img src="../images/icons/icon_checkmark.png" width="12" height="12" > Bekijk presentatie</h6>
				<div class="panel">					
					<dl class="nice tabs vertical tabs">					
						<dd class="text-centered"></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/usen/" target="_blank">ENGELS - USD</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/nlnl/" target="_blank">NEDERLANDS - EURO</a></dd>				
						<dd><a href="http://www.clubshop.com/present/ag1/frfr/" target="_blank">FRANSE - EURO</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/itit/" target="_blank">ITALIAANS - EURO</a></dd>										
					</dl>			
				</div>
				</div>			
				
				<div class=" four columns "> 
				<div class="panel">			
					  <img src="../images/thumbs_download/ag_1_usen.png" width="108" height="77" align="center" style="margin-bottom:12px;" >				
				</div>		
				</div>		
				
				<div class=" four columns ">
					<h6 style="text-align:center; margin-bottom:5px;"><img src="../images/icons/icon_checkmark.png" width="12" height="12" > Download PDF presentatie</h6>
				<div class="panel">
					<dl class="nice tabs vertical tabs">					
						<dd class="text-centered"></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/usen/ag_marketing.pdf" target="_blank">ENGELS - USD</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/nlnl/ag_marketing.pdf" target="_blank">NEDERLANDS - EURO</a></dd>				
						<dd><a href="http://www.clubshop.com/present/ag1/frfr/ag_marketing.pdf" target="_blank">FRANSE - EURO</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag1/itit/ag_marketing.pdf" target="_blank">ITALIAANS - EURO</a></dd>										
					</dl>
				
				</div>
				</div>		
			</div>
			
			
		</div>
		</div>
		
		<hr/>
		
		<div class="row">
		<div class=" ten columns centered"> 
		
		
			<h4>Affinity Group Kaart Fundraising Presentatie</h4>
			<h6>Gebruik deze presentatie om uit te leggen hoe de Affinity Groep een fundraiser kan creëren door de ClubShop Rewards kaart te verkopen.</h6>
			<p></p>
			<div class="row">
				<div class=" four columns "> 
					<h6 style="text-align:center; margin-bottom:5px;"><img src="../images/icons/icon_checkmark.png" width="12" height="12" > Bekijk presentatie</h6>
				<div class="panel">						
					<dl class="nice tabs vertical tabs">						
						<dd><a href="http://www.clubshop.com/present/ag2/usen/" target="_blank">ENGELS - USD</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag2/nlnl/" target="_blank">NEDERLANDS - EURO</a></dd>				
						<dd><a href="http://www.clubshop.com/present/ag2/frfr/" target="_blank">FRANSE - EURO</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag2/itit/" target="_blank">ITALIAANS - EURO</a></dd>										
					</dl>
					
				
				</div>
				</div>			
				
				<div class=" four columns "> 
				<div class="panel">
				
					  <img src="../images/thumbs_download/ag_2_usen.png" width="108" height="77" align="center" style="margin-bottom:12px;" >
				
				</div>		
				</div>		
				
				<div class=" four columns "> 
				<h6 style="text-align:center; margin-bottom:5px;"><img src="../images/icons/icon_checkmark.png" width="12" height="12" > Download PDF presentatie</h6>
				<div class="panel">					
					<dl class="nice tabs vertical tabs">						
						<dd><a href="http://www.clubshop.com/present/ag2/usen/ag_cardfundraiser.pdf" target="_blank">ENGELS - USD</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag2/nlnl/ag_cardfundraiser.pdf" target="_blank">NEDERLANDS - EURO</a></dd>				
						<dd><a href="http://www.clubshop.com/present/ag2/frfr/ag_cardfundraiser.pdf" target="_blank">FRANSE - EURO</a></dd>
						<dd><a href="http://www.clubshop.com/present/ag2/itit/ag_cardfundraiser.pdf" target="_blank">ITALIAANS - EURO</a></dd>											
					</dl>
				
				</div>
				</div>		
			</div>
			
				
		</div>
		</div>
	</div>	
	</div>
	</div>
	</div>

</body>

</html>

