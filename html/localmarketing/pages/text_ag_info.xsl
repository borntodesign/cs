<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    
doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 



   
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Local Marketing: Affinity Groups</title>
<meta name="title" content="ClubShop Rewards"/>
<meta name="Keywords" content="ClubShop Rewards, offline marketing, e-business, training, learning, be successful, Entrupenure"/>
<meta name="abstract" content="Learn everything you need to know to create your own business."/>
<meta http-equiv="content-language" content="en-us"/>
<meta http-equiv="content-language" content="english"/>
<meta http-equiv="content-language" content="lang_en"/>
<meta name="coverage" content="Worldwide"/>
<meta name="distribution" content="Global"/>
<meta name="author" content="Glocal Income"/>
<meta name="publisher" content="Glocal Income"/>
<meta name="company" content="Glocal Income"/>
<meta name="copyright" content="Copyright © 2013 ClubShop Rewards"/>
<meta name="page-topic" content="ClubShop Rewards"/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="General"/>
<meta name="revisit-after" content="7 days"/>
<link href="../styles/css/pages.css" rel="stylesheet" type="text/css" />
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css" />


<style type="text/css">
.blue {

color: #0066CC;
font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.free {
	color: #339900;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
	.basic {
	color: #0066CC;
	font-size: 12px;
	font-weight: bold;
	background-color: #FFFFFF;
}
.bronze { color: #DE850C;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.
	gold { color: #B79300;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.
	silver {	color: #999999;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}

.
	platinum {	color: #0099FF;
font-size: 12px;
font-weight: bold;
background-color: #FFFFFF;
}
</style>
</head>
	
	
	<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
		<tr>
			<td align="right" bgcolor="#FFFFFF">
			
			<hr align="left" width="100%" size="1" color="#A35912"/>
			</td> 
		</tr>
	
	  <tr>
	      <td valign="top" bgcolor="#FFFFFF"><span class="style24"><xsl:value-of select="//lang_blocks/p1"/></span>
	      
	      
	      <br/><br />   
	      
	      
	      <p><xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p2"/></span></p>  
	      
	      
	      <p><xsl:value-of select="//lang_blocks/p4"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p5"/></span></p>
	      
	      
	      <p><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/localmarketing/pages/text_ag_presentations.html" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a></p>
	      
	      
	      <p><xsl:value-of select="//lang_blocks/p8"/></p>
	      
	      <p><xsl:value-of select="//lang_blocks/p10"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p5"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11"/></p>
	      
	      <p><a href="http://www.clubshop.com/cs/affinity_group_application.shtml" target="_blank">http://www.clubshop.com/cs/affinity_group_application.shtml</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12"/></p>
	      
	      <p><xsl:value-of select="//lang_blocks/p13"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p5"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14"/></p>
	      <p><xsl:value-of select="//lang_blocks/p15"/></p>
	      <p><xsl:value-of select="//lang_blocks/p16"/></p>
	      <p><xsl:value-of select="//lang_blocks/p17"/><br/><br/>
	      <span class="style3"><xsl:value-of select="//lang_blocks/p18"/></span><br/><xsl:value-of select="//lang_blocks/p19"/></p>
	      
	      
	      
	      </td>
	   </tr>
	 </table>
	        
	
	
</body></html>
</xsl:template>
</xsl:stylesheet>