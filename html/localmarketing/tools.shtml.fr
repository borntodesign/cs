﻿<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<!--<![endif]-->
	 
	<head>
		
		<!--#include virtual="common/meta/meta.shtml" -->
		
		<title>Potentiel Revenu du Marketing Local</title>	
		
		<style>
		.money{font-weight:bold;}
		
		dl.nice.contained.tabs dd a:hover {
			color:#00A6FC;
			padding: 7px 18px 9px;
		}

	
	</style>
   
	</head>

	
	<body class="blue">
<!-- Top Blue Header Container --><!--#include virtual="common/body/top_bar/top_logo.shtml" --><!--End Top Blue Header Container --><!--Header--><!--#include virtual="common/body/header/header.shtml" --><!-- / Header --><!-- Main Container -->
<div class="container white">
<div class="row">
<div class="three columns"><!-- Menu --><dl class="nice vertical tabs hide-on-print"><dd><a href="index.shtml">Potentiel Revenu</a></dd><dd><a href="affinity.shtml">Groupe d'Affinité</a></dd><dd><a href="merchant.shtml">Commerçants</a></dd><dd><a class="active" href="tools.shtml">Outils</a></dd></dl><!-- / Menu -->
<div class="panel">
<div><img border="0" height="31" src="images/thumb/merchant_welcome1.png" width="169" /></div>
<div id="newest_merchant"></div>
<div id="newest_merchants_sponsor"></div>
</div>
</div>
<div class="nine columns">
<div class="row"><!-- Content Place Holder --><!-- Content Place Holder --><!-- Content Place Holder --> 
<table border="0" cellpadding="15" cellspacing="1" width="100%">
<tbody>
<tr>
<td><span class="ContactForm_TextField">Liens pour le Groupe d'Affinité</span></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">1.</span> <a class="nav_footer_brown" href="#" onclick="window.open('pages/text_ag_email.xml', '', 'width=750, height=800, left=300, top=50, menubar=0, toolbar=0, scrollbars=0, addressbar=0, location=0, directories=0, resizable=1, status=0');return false" target="_blank">Exemple d'Email pour le Groupe d'Affinité</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">2.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/print/ag_application.pdf" target="_blank">Candidature imprimable pour le Groupe d'Affinité</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">3.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/cs/affinity_group_application.shtml" target="_blank">Candidature en ligne pour le Groupe d'Affinité</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">4.</span> <a class="nav_footer_brown" href="pages/text_ag_presentations.html" target="_blank">Présentation Marketing du Groupe d'Affinité</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">5.</span> <a class="nav_footer_brown" href="pages/text_ag_print.html" target="_blank">Brochures et formulaires imprimables pour les Groupes d'Affinité</a></td>
</tr>
</tbody>
</table>
<table border="0" cellpadding="15" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="left" valign="top"><span class="ContactForm_TextField">Liens pour les commerçants</span></td>
</tr>
<!--<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">1.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/cs/short-registration.shtml" target="_blank">Candidature en ligne pour le commerçant à remise directe</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">1.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/cs/merchant_affiliate_application.shtml" target="_blank">Candidature en ligne pour le commerçant effectuant un suivi</a></td>
</tr>-->
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">1.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/print/clubshop_merchant_application.pdf" target="_blank">Candidature imprimable pour le commerçant</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">2.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/print/merchant_terms_conditions.pdf" target="_blank">Termes et Conditions imprimables</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">3.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">Formulaire d'entrée de transaction commerçante</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">4.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/merchants/setup_approval/index.xml" target="_blank">Formulaire d'autorisation de transfert</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">5.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/cs/customer_loyalty_program.shtml" target="_blank">Avantages du Pack commerçant</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">6.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/maf" target="_blank">Compte commerçant</a></td>
</tr>
</tbody>
</table>
<table border="0" cellpadding="15" cellspacing="1" width="100%">
<tbody>
<tr>
<td><span class="ContactForm_TextField">Liens pour les Bonus</span></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">1.</span> <a class="nav_footer_brown" href="pages/text_print.html" target="_blank">Brochures imprimables pour vendre les Cartes ClubShop Rewards</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">2.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/mall" target="_blank">Centre Commercial ClubShop en ligne</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">3.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/mall/other/apparel_gifts.shtml" target="_blank">Vêtements ClubShop Rewards</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">4.</span> <a class="nav_footer_brown" href="https://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank">Annuaire commerçant sur Google Maps</a></td>
</tr>
<tr>
<td><img alt="arrow_icon" height="11" src="images/icons/icon_arrow_box.gif" width="11" /> <span class="footer_bold">5.</span> <a class="nav_footer_brown" href="http://www.clubshop.com/outletsp/bcenterbucks.html" target="_blank">Cartes, Brochures, Affiches de porte, Adhésifs Vinyles commerçants et Plus</a></td>
</tr>
</tbody>
</table>
<br /><input alt="Imprimer" id="print_button" onclick="print(); return false;" src="../images/btn/btn_print-88x22.png" type="image" /> <!--/ Content Place Holder --><!--/ Content Place Holder --><!--/ Content Place Holder --></div>
</div>
<!-- / nine columns --></div>
<!-- / Main container Row --></div>
<!--  / container --><!--End Main Container --><!--Footer Container --><!--#include virtual="common/footer/footer.shtml" --><!--End Footer Container -->
</body>
</html>