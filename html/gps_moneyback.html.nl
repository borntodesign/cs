﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">



<html>



<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    

<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    

<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->

<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->

<!-- [if gt IE 8]> <!-- --> 

 

<html xmlns="http://www.w3.org/1999/xhtml">

 

<!--<![endif]-->

<head>



            <title>ClubShop Rewards Partner Handleiding</title>











<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />

    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />

    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

    <script src="//www.clubshop.com/js/partner/foundation.js"></script>

    <script src="//www.clubshop.com/js/partner/app.js"></script>

    <script src="//www.clubshop.com/js/partner/flash.js"></script>

    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->







<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



</head>

<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">GPS GELD TERUG GARANTIE </span> 
<hr />
</div>
</div>
</div>
<div class="row">
<div class="twelve columns"><br /> <span class="style24">GPS GELD TERUG GARANTIE</span>
<p>If you are a first time subscriber to GPS, you have 30 days to check out the benefits that GPS provides you and experience being a Partner to see how the Team Pools, TNT and Training program works. If you are not completely satisfied with your GPS subscription, you can cancel your subscription and get a refund of your Initial GPS Subscription Fee.</p>
<p>The following conditions apply for you to take advantage of this guarantee:</p>
<ul>
<li class="blue_bullet">You must attend at least one Webinar</li>
<li class="blue_bullet">You must complete the Customer Interest Survey</li>
<li class="blue_bullet">You must have viewed your Activity Report at least 3 times</li>
</ul>
<p> </p>
<p>If you meet all three of these conditions and request your cancellation within 30 days of signing up, you will be entitled to receive a refund, less a $5.00 transaction and processing fee.</p>
</div>
</div>
<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" eight columns centered">          
                <div id=" footer ">
                    <p>Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();

                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         ClubShop Rewards, All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->

</body>

</html>