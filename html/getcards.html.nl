<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />





<title>Print Out ClubShop Rewards Card</title>



<link href="http://www.clubshop.com/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto; width: 1000px;">

<tbody>

<tr>

<td valign="top">

<table border="0" cellpadding="0" cellspacing="0" id="Table_01" width="1000">

<tbody>

<tr>

<td colspan="12" valign="top"><img height="138" src="../../images/headers/getcards-clubshop-1000x138.png" width="1000" /></td>

</tr>

<tr>

<td colspan="2" style="background-image:url(http://www.clubshop.com/images/bg/bg_left.png)"></td>

<td bgcolor="#ffffff" colspan="8" valign="top">

<div align="center"><span class="style12">Print ClubShop Rewards kaarten </span></div>

<hr />

<table>

<tbody>

<tr>

<td>Neem gratis deel en print uw eigen ClubShop Rewards kaart uit met uw computer en printer. U hebt keuze uit drie opties om een papieren ClubShop Rewards kaart uit te printen: <ol>

<li>U kunt de link aanklikken voor uw eigen persoonlijke ClubShop Rewards kaart en die ClubShop Rewards kaart is onmiddellijk geactiveerd&nbsp;bij het printen.</li>

<li>U kunt uw eigen ClubShop Rewards kaart(en) uitprinten, samen met enkele extra kaarten om te verdelen.</li>

<li>U kunt ook enkel ClubShop Rewards kaarten uitprinten om te verdelen. </li>

</ol></td>

<td><img alt="ClubShop Rewards Card" height="129" src="../../images/cards/rewards_club.gif" width="200" /></td>

</tr>

</tbody>

</table>

<hr />

<p>Hieronder vindt u "Print instructies", u vindt die ook op de "Print kaarten" pagina. Lees deze aandachtig en print deze instructies eventueel uit voor u uw kaarten print. **Voor u begint te printen, wees zeker dat u bent ingelogd. Leden dienen hun paswoord of uniek ID nummer te gebruiken wanneer daarom gevraagd wordt. <br />Wij adviseren onderstaande te gebruiken:</p>

<ul>

<li value="0">Avery business kaarten (wit)</li>

<li value="0">8371 voor Inkjet Printers of</li>

<li value="0">5371 voor Laser Printers of</li>

<li value="0">65# wit papier</li>

<li value="0">Een kleuren inktpatroon voor uw printer.</li>

</ul>

<br />Als u &eacute;&eacute;n of meerdere van bovenstaande artikelen nog dient aan te schaffen, neem dan zeker een kijkje in de <a class="nav_blue_line" href="../../mall/">ClubShop Mall</a> en maak gebruik van de grote waaier aan kortingen! <br /><br />

<div align="center"><span class="style14">Maak uw keuze uit deze 3 Print Opties en print uw ClubShop Rewards kaart:</span></div>

<br /> 

<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">

<tbody>

<tr>

<td bgcolor="#ffffff" valign="top"><span class="style14">OPTIE 1:</span><br /><strong>Print enkel 1 ClubShop Rewards kaart voor uw peroonlijk gebruik.</strong><br /><br /><span class="text_orange">Wanneer u gebruik maakt van de <a class="nav_blue_line" href="https://www.clubshop.com/cgi/cbmycards.cgi?action=new_order&label=Quick%20Card&selection=selected_only">Quick Print Optie</a> wordt uw ClubShop Rewards kaart bij het printen onmiddellijk geactiveerd. </span><br />

<p>De ClubShop Rewards kaart wordt zij aan zij geprint, bijkomende printer setting is niet nodig.</p>

<p>Enkel printen, plooien en&nbsp;uw kaart is gebruiksklaar!</p>

</td>

<td bgcolor="#ffffff" valign="top"><span class="style14">OPTIE 2:</span><br /><strong>Print uw eigen ClubShop Rewards kaarten PLUS enkele extra kaarten om te verdelen</strong><br /><br /><br /><span class="text_orange">NOTA: Gelieve eerst de instructies te lezen VOOR u begint uw kaarten te printen. U kunt gemakshalve de instructies afdrukken.</span> <br /><br /><br /><br />

<div></div>

<div align="center"><a href="http://www.clubshop.com/print_cb_1.html" target="_blank" class="nav_blue_line">Instructies</a> | <a href="https://www.clubshop.com/cgi/cbmycards.cgi" class="nav_blue_line">Print kaarten</a></div>

</td>

<td bgcolor="#ffffff" valign="top"><span class="style14">OPTIE 3: </span><br /><strong>Print ClubShop Rewards kaarten om te verdelen </strong><br /><br /><br /><br /><span class="text_orange">NOTA: Gelieve eerst de instructies te lezen VOOR u begint uw kaarten te printen. U kunt gemakshalve de instructies afdrukken.</span> <br /><br /><br /><br />

<div></div>

<div align="center"><a href="http://www.clubshop.com/print_cb_2.html" target="_blank" class="nav_blue_line">Instructies</a> | <a href="https://www.clubshop.com/cgi/cbneworder.cgi/full_page" target="_blank" class="nav_blue_line">Print kaarten</a></div>

</td>

</tr>

</tbody>

</table>

</td>

<td colspan="2" style="background-image:url(http://www.clubshop.com/images/bg/bg_right.png)"></td>

</tr>

<tr>

<td colspan="12"><img height="62" src="http://www.clubshop.com/images/general/clubshop_rewards_merchant_page_16.png" width="1000" /></td>

</tr>

<tr>

<td colspan="12" height="134" style="background-image:url(http://www.clubshop.com/images/general/footerbar.png)"></td>

</tr>

<tr>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="34" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="14" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="146" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="142" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="119" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="90" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="38" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="128" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="133" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="109" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="13" /></td>

<td><img height="1" src="http://www.clubshop.com/images/general/spacer.gif" width="34" /></td>

</tr>

</tbody>

</table>

</td>

</tr>

</tbody>

</table>

</body>

</html>