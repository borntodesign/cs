﻿<html>
<head>
<title>Trial Partner Monthly Email</title>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
</head>
<body bgcolor="#FFFFFF" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tbody>
<tr>
<td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="650">
<tbody>

<!--<tr>
<td height="20" align="center" valign="top" >
[% IF ! BrowserViewFlag %]
<div id="browserview" style="font:10px normal arial,helvetica,sans-serif; color: rgb(102,102,102);">[% lang_nodes.view_in_browser %]</a></div>
[% ELSE %]
 
[% END %]
</td>
</tr>-->

<tr>
<td align="center" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td bgcolor="#FCBF12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" bgcolor="#FFFFFF">
<div style="position:relative"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_header.png" alt="[% shell_variable_1 %]" width="650" height="122"> 
                              <div style="position:absolute; right:32px; top:78px; font:bold 90% arial,helvetica,sans-serif; color:#ffbb00;">[firstname] 
                                - we hebben geweldig nieuws...</div>
                            </div>
</td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><table width="580" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" >

<tr>
<td>

<div style="font-family:arial,helvetica,sans-serif">
Hallo [firstname],
<p>
Wij willen u graag op de hoogte houden van fantastisch nieuws. Uw potentiële verkooporganisatie groeit nog steeds!
</p>
<p>
U hebt momenteel [] Affiliates en [] Partners in uw potentiële verkooporganisatie.
</p>
<p>
Klik hierom uw Proefpartner rapport te zien.
</p>
<p>
Mocht u besluiten om voor het eind van deze maand te kwalificeren om Partner te worden, dan kun u alle Affiliates en Partners behouden die vermeld staan op
Uw Proefpartner rapport. Dit zal uw proces versnellen naar het verdienen van inkomen in onze fantastisch nieuwe marketing plan!  
</p>
<p>
Klik hier om meer te lezen over hoe u vandaag nog Partner kunt worden!
</p>
<p>
ClubShop Rewards Partner Sales
</p>

<br><br>
<br><br>
<div style="text-align:right; font-size:11px;">email: 10940</div>
</div>


<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px dotted #000099;">
                                          <tr> 
                                            <td align="left"><span style="FONT-SIZE: 10px; COLOR: #000099; FONT-FAMILY: Arial, Helvetica, sans-serif">VOLG ONS OP UW FAVORIETE SOCIAAL NETWERK</span><br>
  <br>

<a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"> <img src="http://www.glocalincome.com/images/icons/icon_face_book.png" alt="Facebook" width="28" height="28" border="0" align="absmiddle"></a>    

</td>
</tr>
</table>

</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_footer.png" width="650" height="74"></td>

</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">&nbsp;</td>
</tr>

<tr>
              <td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">
                © 2012 ClubShop Rewards.<br>
 ClubShop Rewards, 2828 S. McCall Road, Englewood, FL 34224 <br>Attn: Marketing Preferences</td>
</tr>
<tr>
<td height="10" align="left">&nbsp;</td>
</tr>

</tbody>
</table></td>
</tr>
</tbody>
</table>

</body>
</html>
