<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>


     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
	
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script> -->
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
   <!--  <script src="http://www.clubshop.com/js/partner/app.js"></script> -->
   <!--  <script src="http://www.clubshop.com/js/partner/flash.js"></script> -->	
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>





</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>

	<hr />
	
	<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
		


<br/>
<br/>
<br/>
<span class="text_normal"><xsl:value-of select="//lang_blocks/pa"/></span>
<br/>


<br/>
<span class="style24"><b><xsl:value-of select="//lang_blocks/p22"/></b></span><br/><br/>
<p><span class="Green_Large_Bolder"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="cs_welcome.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p23"/></a></span></p>
<p><span class="Green_Large_Bolder"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="10877.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p54"/></a></span></p>
<p><span class="Green_Large_Bolder"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="10807.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p24"/></a></span></p>
<!--<p><span class="Green_Large_Bolder"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="affiliate_welcome.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p25"/></a></span></p>-->
<p><span class="Green_Large_Bolder"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="partner_welcome.html" target="_blank" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p26"/></a></span></p>




<br/>
<span class="style24"><b><xsl:value-of select="//lang_blocks/p17"/></b></span><br/><br/>

<p><span class="Green_Bold"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="10953.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p57"/></a></span></p>
<!--<p><span class="Green_Bold"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="NewTrialPartnerEmail.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p18"/></a></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19"/></p>-->
<!--<p><span class="Green_Bold"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="10843.html.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p9"/></a></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14"/></p>-->
<p><span class="Green_Bold"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="monthly_trial_partners.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p20"/></a></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p21"/></p>
<p><span class="Green_Bold"><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><a href="eom_trial_partners.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p55"/></a></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p56"/></p>


<br/><br/>



<span class="style24"><b><xsl:value-of select="//lang_blocks/p58"/></b></span><br/><br/>
<p><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p59"/><xsl:text> </xsl:text><a href="1107.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p60"/></a></p>
<p><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p61"/><xsl:text> </xsl:text><a href="1101.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p60"/></a></p>
<p><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p63"/><xsl:text> </xsl:text><a href="1103.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p60"/></a></p>
<p><img src="orange_arrow_new.png"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p65"/><xsl:text> </xsl:text><a href="1105.html" target="_blank" class="Nav_Green_Plain" onclick="window.open(this.href,'help', 'width=750,height=800,scrollbars'); return false;"><xsl:value-of select="//lang_blocks/p60"/></a></p>





<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>



</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2014
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>