<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>[[FIRSTNAME]]- Uw ClubShop Rewards details</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body marginwidth="0" marginheight="0" bgcolor="#00376f" topmargin="0" leftmargin="1" body="">
<table height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#00376f" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table cellspacing="0" cellpadding="0" border="0" width="650">
          <tbody>
            <tr> 
              <td align="center" valign="top"><table id="Table_01" width="650" border="0" cellpadding="0" cellspacing="0" height="964">
                  <tbody>
                    <tr> 
                      <td colspan="3"><img src="https://www.clubshop.com/clubrewards/email/images/header-newshopper_1.png" alt="" width="650" height="166"></td>
                    </tr>
                    <tr> 
                      <td valign="top"><img src="https://www.clubshop.com/clubrewards/email/images/news_2.png" alt="" width="49" height="629"></td>
                      <td valign="top" width="552" align="left" bgcolor="#ffffff" height="563"> 
                        <table width="100%" height="561" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr> 
                              <td height="19" align="left"><span style="font-size: 16px; color: rgb(252, 111, 63); font-family: Arial,Helvetica,sans-serif; font-weight: bold;"> 
                                Welkom!</span></td>
                            </tr>
                            <tr> 
                              <td height="542" valign="top"><span style="font-size: 12px; color: rgb(22, 14, 11); font-family: Arial,Helvetica,sans-serif;"> 
                                <br />
                                <img src="https://www.clubshop.com/clubrewards/email/welcome_shopper/shopper_1/english/images/clubshop_rewards_card.png" width="215" align="right" height="135"> 
                                Bedankt voor uw gratis inschrijving voor een <strong>ClubShop 
                                Rewards</strong> lidmaatschap. Hieronder vindt 
                                u uw lidmaatschap ID en wachtwoord waarmee u kunt 
                                <a href="https://www.clubshop.com/cgi-bin/wwg2" style="font-weight: bold; font-size: 12px; color: rgb(252, 111, 63); font-family: Arial,Helvetica,sans-serif; text-decoration: underline;"> 
                                loggen op uw account</a>.<br/>
                                <br/>
                                <br/>
                                Uw lidmaatschap ID: <b>IDNUMBER</b><br/>
                                Uw paswoord: <b>PASSWORD</b><br/>
                                <br/>
                                <p><span style="font-size: 12px; color: rgb(22, 14, 11); font-family: Arial,Helvetica,sans-serif;">U 
                                  kunt uw gratis lidmaatschap gebruiken voor::</span><br/>
                                  <br/>
                                  <span style="font-size: 14px; color: rgb(10, 50, 150); font-family: Arial,Helvetica,sans-serif; font-weight: bold;"><u>ONLINE 
                                  WINKELEN</u></span><br/>
                                  <img src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" align="absmiddle" height="12" /> 
                                  Via de <a href="http://www.clubshop.com/cgi/cm/" style="font-weight: bold; font-size: 12px; color: rgb(252, 111, 63); font-family: Arial,Helvetica,sans-serif; text-decoration: underline;">ClubShop 
                                  Online Mall</a> verdient u ClubCash in al uw 
                                  favoriete online winkels.<br/>
                                  <br/>
                                  <span style="font-size: 14px; color: rgb(10, 50, 150); font-family: Arial,Helvetica,sans-serif; font-weight: bold;"><u>LOKAAL 
                                  WINKELEN</u></span><br/>
                                  <img src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" align="absmiddle" height="12" /> 
                                  Op vertoon van uw ClubShop Rewards kaart verdient 
                                  u ClubCash of Direct Kortingen bij deelnemende 
                                  <a href="http://www.clubshop.com/rewardsdirectory/?country=&amp;city=&amp;state=" style="font-weight: bold; font-size: 12px; color: rgb(252, 111, 63); font-family: Arial,Helvetica,sans-serif; text-decoration: underline;"> 
                                  ClubShop Rewards handelaren</a>.<br/>
                                  <br/>
                                  <span style="font-size: 14px; color: rgb(10, 50, 150); font-family: Arial,Helvetica,sans-serif; font-weight: bold;"><u>GELD 
                                  BESPAREN</u></span><br/>
                                  <img src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" align="absmiddle" height="12" /> 
                                  ClubCash kunt u inwisselen voor contant geld. 
                                  <br/>
                                  <img src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" align="absmiddle" height="12" /> 
                                  Geniet van de "Members Only" kortingen bij uw 
                                  favoriete ClubShop Rewards handelaren en online 
                                  winkels.<br/>
                                  <br/>
                                  <span style="font-size: 14px; color: #0a3296; font-family: Arial,Helvetica,sans-serif; font-weight: bold;"><span style="text-decoration: underline;">GELD VERDIENEN</span></span><br /> <img height="12" src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /> Verdien eenvoudig een inkomen door anderen te verwijzen om ClubShop Rewards Members te worden.<br />
                                  <img height="12" src="https://www.clubshop.com/clubrewards/email/images/icons/icon_checkmark_long.png" width="42" /> 
                                  Ontdek hoe uw Partner Inkomsten Mogelijkheid 
                                  werkt - <a href="https://www.clubshop.com/cgi/r/tp.cgi" style="font-weight: bold; font-size: 12px; color: #fc6f3f; font-family: Arial,Helvetica,sans-serif; text-decoration: underline;">KLIK 
                                  HIER</a> om een gratis Proefpartner positie 
                                  te krijgen in ons wereldwijde verkooporganisatie.<br />
                                   <br />
                                <p align="center"><span style="font-size: 14px; color: rgb(10, 50, 150); font-family: Arial,Helvetica,sans-serif; font-weight: bold;">ClubShop 
                                  Rewards - Hoe de wereld winkelt en bespaart!</span></p>
                                </span> 
                                <p>&nbsp;</p>
                                <p><span style="font-size: 12px; color: rgb(22, 14, 11); font-family: Arial,Helvetica,sans-serif;">ClubShop 
                                  Rewards<br/>
                                  2828 S.McCall Road<br/>
                                  Englewood, FL USA 34224 - 5412</span> </p></td>
                            </tr>
                          </tbody>
                        </table></td>
                      <td valign="top"><img src="https://www.clubshop.com/clubrewards/email/images/news_4.png" alt="" width="49" height="629" /></td>
                    </tr>
                    <tr> 
                      <td colspan="3"><img src="https://www.clubshop.com/clubrewards/email/images/footer-newshopper_1.png" alt="" width="650" height="235"></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr> 
              <td style="font-size:20px">&nbsp;</td>
            </tr>
            <tr> 
              <td align="left" valign="top" style="font-size: 11px; color: rgb(255, 255, 255); font-family: Arial,Helvetica,sans-serif;"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"><img src="https://www.clubshop.com/clubrewards/email/images/logos/facebook_logo.png" width="41" align="absmiddle" border="0" height="37"></a> 
                <span style="font-size: 8px; padding-left: 10px;"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank" style="font-weight: bold; font-size: 11px; color: rgb(254, 188, 34); font-family: Arial,Helvetica,sans-serif; text-decoration: underline;">Become 
                a fan on Facebook</a></span> <fb:fan profile_id="75003391133" stream="" connections="" width="300"></fb:fan></td>
            </tr>
            <tr> 
              <td style="font-size:15px">&nbsp;</td>
            </tr>
            <tr> 
              <td align="left" valign="top" style="font-size: 11px; color: rgb(255, 255, 255); font-family: Arial,Helvetica,sans-serif;">&copy; 
                2015 ClubShop Rewards. Alle rechten voorbehouden <br/>
                Deze e-mail is een bevestiging van: ClubShop Rewards, 955 Morisson Avenue, Englewood, FL 34223 Attn: Marketing Preferences</td>
            </tr>
            <tr> 
              <td style="font-size:20px">&nbsp;</td>
            </tr>
          </tbody>
        </table></td></tr></tbody></table></body></html>