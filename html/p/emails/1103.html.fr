<html>
<head>
<title>Series Emails</title>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
</head>
<body bgcolor="#FFFFFF" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tbody>
<tr>
<td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="650">
<tbody>

<!--<tr>
<td height="20" align="center" valign="top" >
[% IF ! BrowserViewFlag %]
<div id="browserview" style="font:10px normal arial,helvetica,sans-serif; color: rgb(102,102,102);">[% lang_nodes.view_in_browser %]</a></div>
[% ELSE %]
 
[% END %]
</td>
</tr>-->

<tr>
<td align="center" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td bgcolor="#FCBF12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" bgcolor="#FFFFFF">
<div style="position:relative"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_header.png" alt="" width="650" height="122"> 
                              <div style="position:absolute; right:32px; top:78px; font:bold 90% arial,helvetica,sans-serif; color:#ffbb00;">[[firstname]] 
                                - Qu'estce qui rend nos Partners si excités?</div>
                            </div>
</td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><table width="580" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" >

<tr>
<td>

<div style="font-size: 12px; color: #000000; font-family: Verdana,Helvetica,sans-serif;">


                                
Bonjour  [firstname],

<p>En tant qu'Affiliate, vous vous demandez peut-être pourquoi nos Partners sont si enthousiastes à l'idée de leur entreprise ClubShop Rewards.</p>

    <p>Une des choses à propos de quoi nos Partners sont le plus excités est notre plan de rémunération révolutionnaire qui offre aux Partners la capacité unique de partager les revenus créés par notre équipe mondiale de Partners, sous la forme de dividendes. <a href="http://www.clubshop.com/manual/compensation/affiliate.html">Cliquez ici</a> pour voir notre plan de rémunération.</p>

    <p>En utilisant la puissance d'Internet et notre énorme plan marketing de référencement pour aider les gens à économiser de l'argent, cela permet aux gens de presque partout dans le monde de lancer, gérer et faire croître leur entreprise localement et mondialement. Nos Partners se sentent à l'aise de référencer des consommateurs pour un produit gratuit qui peut réellement les aider à économiser de l'argent, en ligne et localement.</p>

    <p>OL'un des moyens les plus rapides et les plus faciles pour se qualifient en tant que Partners et pour continuer à être considéré comme un Partner chaque mois, est de souscrire au Global Partner System (GPS).<a href="http://www.clubshop.com/gps.html">Cliquez ici</a> pour en savoir plus sur ce qui rend notre système GPS si efficace.</p>

    <p>Achetez un abonnement GPS aujourd'hui et commencez à gagner pendant que vous apprenez!</p>

    <p>Sincèrement,</p>

    <p>Département des ventes Partner ClubShop Rewards</p>

    <!--<p>P.S. - Vous êtes invités à assister à un webinaire éducatif pour en apprendre davantage au sujet de notre Opportunité de Revenu Partner. <a href="http://www.clubshop.com/training_calendar.html">Cliquez ici</a> pour voir notre calendrier de webinaires.</p>-->

<br><br>
<br><br>
<div style="text-align:right; font-size:11px;">email: 1103</div>
</div>




<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px dotted #000099;">
                                          <tr> 
                                            <td align="left"><span style="FONT-SIZE: 10px; COLOR: #000099; FONT-FAMILY: Arial, Helvetica, sans-serif">SUIVEZ NOUS SUR VOTRE RESEAU SOCIAL PREFERE</span><br>
  <br>

<a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"> <img src="http://www.glocalincome.com/images/icons/icon_face_book.png" alt="Facebook" width="28" height="28" border="0" align="absmiddle"></a>    

</td>
</tr>
</table>

</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_footer.png" width="650" height="74"></td>

</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">&nbsp;</td>
</tr>

<tr>
              <td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">
                © 2012 ClubShop Rewards.<br>
 ClubShop Rewards, 2828 S. McCall Road, Englewood, FL 34224 <br>Attn: Marketing Preferences</td>
</tr>
<tr>
<td height="10" align="left">&nbsp;</td>
</tr>

</tbody>
</table></td>
</tr>
</tbody>
</table>

</body>
</html>

