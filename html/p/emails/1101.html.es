<html>
<head>
<title>Series Emails -1</title>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
</head>
<body bgcolor="#FFFFFF" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tbody>
<tr>
<td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="650">
<tbody>

<!--<tr>
<td height="20" align="center" valign="top" >
[% IF ! BrowserViewFlag %]
<div id="browserview" style="font:10px normal arial,helvetica,sans-serif; color: rgb(102,102,102);">[% lang_nodes.view_in_browser %]</a></div>
[% ELSE %]
 
[% END %]
</td>
</tr>-->

<tr>
<td align="center" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td bgcolor="#FCBF12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" bgcolor="#FFFFFF">
<div style="position:relative"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_header.png" alt="" width="650" height="122"> 
                              <div style="position:absolute; right:32px; top:78px; font:bold 90% arial,helvetica,sans-serif; color:#ffbb00;">[[firstname]] 
                                - ¿Estàs listo para entrar en nuestro Equipo de 
                                Partners?</div>
                            </div>
</td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><table width="580" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" >

<tr>
<td>

<div style="font-size: 12px; color: #000000; font-family: Verdana,Helvetica,sans-serif;">


                                
Hola [firstname],

    <p>Felicitaciones por entrar en tu Informe de Posiciòn de Prueba.</p>

    <p>Seguramente te estàs preguntando como te puede beneficiar convertirte en un Partner del ClubShop Rewards.</p>

    <p>Una de las mejor maneras que tenemos para educarte sobre nuestro negocio, productos y enseñarte porquè cada vez màs personas se convierten en Partners del ClubShop Rewards es ver la presentaciòn con diapositivas sobre la oportunidad de ingresos del ClubShop Rewards.<br>
<a href="http://www.clubshop.com/present/bizop.xml">Haz click aquì</a> para acceder a las diapositivas.</p>

    <p>Como Partner del ClubShop Rewards, tienes la posibilidad de empezar un negocio divertido, sencillo, ùtil y rentable que ayuda no solo a los consumidores sino tambièn a las organizaciones sin fin de lucro y mercantes en todo el mundo. Mientras continuaràs a aprender màs sobre nuestro sistema y con la ayuda de tu entrenador, tu ingreso a tiempo parcial se convertirà en ¡ingreso a tiempo lleno!
<br>
<a href="http://www.glocalincome.com/cgi/apbc?alias=[ID]">Haz click aquì</a> para acceder a tu Affiliate Business Center</p>

    <p>Somos orgullosos de decir que nuestra compañia es libre de deudas y tiene una ¡Valutaciòn Crediticia de A+ con la Better Business Bureau! Desde el 1997 ayudamos a las personas exactamente como estamos haciendo contigo a obtener ingresos a tiempo parcial o lleno en su tiempo libre. Por lo tanto te invitamos a convertirte en un partner hoy para ganar mientras aprendes.</p>

    <p>Atentamente,</p>

    <p>Secciòn de Ventas ClubShop Rewards</p>

    

<br><br>
<br><br></div>
<div style="text-align:right; font-size:small;">email: 1100</div>




<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px dotted #000099;">
                                          <tr> 
                                            <td align="left"><span style="FONT-SIZE: 10px; COLOR: #000099; FONT-FAMILY: Arial, Helvetica, sans-serif">SÌGUENOS EN TU SOCIAL NETWORK PREFERIDO</span><br>
  <br>

<a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"> <img src="http://www.glocalincome.com/images/icons/icon_face_book.png" alt="Facebook" width="28" height="28" border="0" align="absmiddle"></a>    

</td>
</tr>
</table>

</td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_footer.png" width="650" height="74"></td>

</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">&nbsp;</td>
</tr>

<tr>
              <td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">
                © 2012 ClubShop Rewards.<br>
 ClubShop Rewards, 2828 S. McCall Road, Englewood, FL 34224 <br>Attn: Marketing Preferences</td>
</tr>
<tr>
<td height="10" align="left">&nbsp;</td>
</tr>

</tbody>
</table></td>
</tr>
</tbody>
</table>

</body>
</html>
