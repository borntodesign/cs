<html>
<head>
<title>Congratulations on becoming a ClubShop Rewards Affiliate!</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
</head>
<body bgcolor="#FFFFFF" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tbody>
<tr>
<td align="center" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="650">
<tbody>

<tr>

<td height="20" align="center" valign="top" >&nbsp;



</td>
</tr>

<tr>
<td align="center" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td bgcolor="#FCBF12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" bgcolor="#FFFFFF">
<div style="position:relative"><img src="https://www.clubshop.com/cs/mail/en/images/cs_email_generic_header.png" alt="ClubShop" width="650" height="122"><div style="position:absolute; right:32px; top:78px; font:bold 90% arial,helvetica,sans-serif; color:#003366;">Gefeliciteerd dat u ClubShop Rewards Affilialte bent geworden!</div></div>
</td>
</tr>

<tr>
<td align="center" bgcolor="#FFFFFF"><table width="580" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" >

<tr>
<td>

<!--div style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif"-->
<div style="font:normal 12px arial,helvetica,sans-serif;">
<p>Beste [% member.firstname %],</p>
<p>Gefeliciteerd dat u een ClubShop Rewards Affiliate bent geworden!</p>
<p>Hier is wat belangrijke informatie voor u om goed van start te gaan met uw ClubShop Rewards zaak:</p>


<ul>
<img src="http://www.glocalincome.com/email/template/newsletter/vip/en/images/icon_bullet.png" border="0" width="15" height="9"> Ga naar <a href="http://www.glocalincome.com/">www.glocalincome.com</a> en voeg deze toe aan uw favorieten.
Log vervolgens in met uw ID nummer  <b>[% member.alias %]</b> en uw paswoord <b>[% member.password %]</b>.
 Dit is uw belangrijkste interface pagina voor uw ClubShop Rewards Affiliate zaak. Als een Affiliate, kunt u hier al het laatste ClubShop Rewards nieuws lezen. U heeft hier tevens toegang tot de Locale Marketing Training, uw zakelijke Rapporten, het ClubShop Rewards winkelcentrum en alles wat u nodig heeft om u te helpen succesvol te worden met uw ClubShop Rewards Affiliate zakelijke mogelijkheid!<br><br>

<img src="http://www.glocalincome.com/email/template/newsletter/vip/en/images/icon_bullet.png" border="0" width="15" height="9"> Indien u interesse heeft in het hebben van een volledig Partner ClubShop Rewards zaak en een wereldwijde verkooporganisatie, neem dan zeker de Zakelijke informatie door die u hierover kunt vinden op uw Business Center en bekijk de voordelen van het verkrijgen van een gratis proefpositie in onze wereldwijde Partner Organisatie, om u te helpen van start te gaan met een ClubShop Rewards Partner zaak.
<br>

</ul>

<p><img src="http://www.glocalincome.com/email/template/newsletter/vip/en/images/girl_happy.png" align="right" width="178" height="129">Wij weten dat u staat te popelen om te beginnen, maar het is belangrijk om op de juiste mannier van start te gaan door alles te begrijpen wat nodig is, om te leren hoe een effectieve Locale Marketing Campagne te lanceren. Bestudeer de Locale Marketing Cursus, die voor u ter beschikking staat in uw Business Center dus zorgvuldig.</p>

<p>Namens onze medewerkers van het hoofdkantoor en uw Partner Team Coaches, wil ik u welkom heten in ons ClubShop Rewards Affiliate marketing team, met mijn beste wensen voor uw succes.</p>

<p>Met vriendelijke groeten,
<br /><br />Dick Burke<br />President</p>

<p>P.S. – Wij weten dat u waarschijnlijk staat te popelen om met al uw vrienden en familie te delen hoe zij geld kunnen besparen als ClubShop Rewards Member en geld kunnen verdienen als Affiliate. ClubShop Rewards staat niet toe dat u e-mails verstuurd die verwijzen naar alle ClubShop.com URLs, tenzij u toestemming heeft van degene aan wie u de e-mail verstuurd.
</p>

<p>VOORDAT u e-mails verstuurd om ClubShop winkels, producten of diensten te promoten of om de ClubShop Rewards mogelijkheid 

te promoten, moet u echt het ClubShop Rewards' E-mail (Spam) beleid lezen, deze kunt u vinden op<br> <a href="http://www.clubshop.com/manual/policies/spam.xml" style:"text-decoration: underline">http://www.clubshop.com/manual/policies/spam.xml</a> - dank u!</p>
</div>


<!--/div-->
<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px dotted #003366;">
<tr>
<td align="left"><span style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif"><span style="FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #003366; FONT-FAMILY: Arial, Helvetica, sans-serif">VOLG ONS OP UW FAVORIETE SOCIAAL NETWERK</span><br>
  <br>

<a href="http://www.facebook.com/GlocalIncome" target="_blank"> <img src="https://www.glocalincome.com/images/icons/icon_face_book.png" alt="Become a Fan of Glocal Income" width="28" height="28" border="0" align="absmiddle"></a>&nbsp;&nbsp;&nbsp;&nbsp;

<a href="http://www.youtube.com/watch?v=HCLvtEoFqNA" target="_blank"> <img src="https://www.glocalincome.com/images/icons/icon_you_tube.png" alt="Become a fan of ClubShop Rewards on Facebook" width="28" height="28" border="0" align="absmiddle"></a>
<a target="_blank" href="http://twitter.com/Glocal_Income"> <img height="28" width="28" border="0" align="absmiddle" alt="Follow Glocal Income on Twitter" src="https://www.glocalincome.com/images/icons/icon_twitter.png"></a></span></td>

</tr>
</table>

<span style="FONT-SIZE: 12px; COLOR: #160e0b; FONT-FAMILY: Arial, Helvetica, sans-serif"><strong>
</strong></span></td>
</tr>

</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="center" bgcolor="#FFFFFF"><img src="https://www.clubshop.com/cs/mail/en/images/cs_email_generic_footer.png" width="650" height="74"></td>

</tr>
</table></td>
</tr>

</table></td>
</tr>
<tr>
<td align="left" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">&nbsp;</td>
</tr>

<tr>
<td align="center" valign="top" style="FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Arial, Helvetica, sans-serif">

Please add both <a href="mailto:Info@GlocalIncome.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #41A543; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline">Info@GlocalIncome.com</a> and <a href="mailto:Info@ClubShop.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #41A543; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline">Info@ClubShop.com</a> to your address book now to ensure all future emails reach your inbox.

<br>

<br>
&copy; 2012 ClubShop Rewards. All Rights Reserved.<br>
This email is a confirmation from: ClubShop Rewards, 2828 S.McCall 
Road Suite 13, Englewood, FL 34224 Attn: Marketing Preferences</td>
</tr>
<tr>
<td height="10" align="left">&nbsp;</td>
</tr>

</tbody>
</table></td>
</tr>
</tbody>
</table>

</body>
</html>
