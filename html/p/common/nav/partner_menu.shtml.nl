<div class="container  white">
		
		<div class="row">  <!-- MENU ROW -->
			<div class="twelve columns ">
			
			
				<ul class="nav-bar">
					<!-- <li><a href="" target="_blank" >Home</a></li> -->
					<li><a href="http://www.clubshop.com/cgi-bin/wwg2" target="_blank" >Winkelen & Rewards</a></li>
					<li><a href="http://www.clubshop.com/manual/index.shtml"  target="_blank" >Handleiding</a></li>
					<li class="has-flyout"><a href="" target="_blank">Member Zoeken/Lijsten</a>
					<div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="https://www.clubshop.com/cgi/vip/member-followup.cgi" target="_blank"><b>Member Zoeken</b></a></li>
												<li><b><span class="gibbs">Member Lijsten</span></b></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=m&index=0" target="_blank">Members</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=a&index=0" target="_blank">Affiliates</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=tp&index=0" target="_blank">Trial Partners</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=mr&index=0" target="_blank">Merchants</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=ag&index=0" target="_blank">Affinity Groups</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=p&index=0" target="_blank">Partners</a></li>
                                                
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					</li>
					
					
					
					
					<li class="has-flyout"><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank" >G.P.S</a>
						<div class="flyout ">
							<div class="row">
				         		<div class="twelve columns">
				         			<h5><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank" ><span class="gibbs">G</span>lobal <span class="gibbs">P</span>artner <span class="gibbs">S</span>ystem</a></h5>

				     				<div class="row hide-on-phones">

				   				  		<div class="eight columns">
											<p> </p>
				   				  			<p><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank" >KLIK HIER - om alle voordelen van een abonnement en kosten op GPS zien</a></p>
												
				     					</div>

				     					<div class="four columns ">

				     						<a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank" ><img src="https://www.clubshop.com/p/common/images/gibbs.png"/></a>

				     					</div>

				     				</div>
				  				</div>
				  			</div>
				  		</div>
					</li>

					<li class="has-flyout"><a href="" target="_blank" >Webinars</a>
						<div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="http://www.clubshop.com/affiliate/webinars.html" target="_blank" >Business Opportunity Presentations</a></li>
												<li><a href="/vip/manual/training/interactive_online_schedule.html" target="_blank" >Partner Training Webinars</a></li>												
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					
					</li>
					
					<li><a href="https://www.clubshop.com/vip/coop.shtml" target="_blank" >Co-Op</a></li>
					
					<li class="has-flyout"><a href="">Persoonlijk</a>
						<div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="https://www.clubshop.com/support/">Help</a></li>
												<li><a href="http://www.clubshop.com/cgi/funding.cgi?action=report">ClubAccount</a></li>
												<li><a href="http://www.clubshop.com/cgi/CommStmnt.cgi">Earning Statement</a></li>
												<li><a href="http://www.clubshop.com/cgi/comm_prefs.cgi">Commissie Uitbetalingmethode</a></li>
												<li><a href="http://www.clubshop.com/cgi/mopi.cgi">Update Uitbetalinginformatie</a></li>
												<li><a href="http://www.clubshop.com/cgi/appx.cgi/update">Update Profiel</a></li>
												<li><a href="http://www.clubshop.com/cgi/cancel.cgi">Wijzig Lidmaatschap</a></li>
												<li><a href="https://www.clubshop.com/cgi/logout.cgi/redirect">Uitloggen</a></li>
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					</li>
					
					<li class="has-flyout"><a href="">Nieuws</a>
						<div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="http://www.clubshop.com/p/blog/dblog.xml">Burke Blog</a></li>
												<li><a href="http://www.clubshop.com/p/news/topview.xml">View From The Top</a></li>
												
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					</li>
					
					
					
					
					<li class="has-flyout show-on-phones"><a href="">Tools</a>					
						<div class="flyout small right">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
											<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/team">Team Rapport</a></li>
											<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/grp">Group Rapport</a></li>
												<li><a href="http://www.clubshop.com/cgi/vip/activity_rpt">Activiteiten Rapport</a></li>
												
												<li><a href="http://www.clubshop.com/cgi/pireport.cgi">Inkomsten Rapport</a></li>
												
												<li><a href="http://www.clubshop.com/vip/manual/training/guide/getstarted/index.html">Training Guide</a></li>
												<li><a href="http://www.clubshop.com/cgi/tl-redir">Team Pagina</a></li>
											
																																				
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>					
					</li>
					
				</ul>				
			</div> 
		</div>

		<script>
			$(function() {
				$("a[data-popup]").live('click', function(e) {
					window.open($(this)[0].href);
					// Prevent the link from actually being followed
					e.preventDefault();
				});
			});
		</script>
		
</div>	
