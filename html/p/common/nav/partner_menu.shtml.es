﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Untitled document</title>
</head>
<body>
<div class="container  white">
<div class="row"><!-- MENU ROW -->
<div class="twelve columns ">
<ul class="nav-bar">

<li><a href="http://www.clubshop.com/cgi-bin/wwg2" target="_blank">Compras y Recompensas</a> </li>
<li><a href="http://www.clubshop.com/manual/index.shtml" target="_blank">Manual</a> </li>
<li class="has-flyout"><a href="" target="_blank">Buscar Member/Lista</a> 
<div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="https://www.clubshop.com/cgi/vip/member-followup.cgi" target="_blank"><b>Miembro Buscar</b></a></li>
												<li><b><span class="gibbs">Miembro Lista</span></b></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=m&index=0" target="_blank">Members</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=a&index=0" target="_blank">Affiliates</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=tp&index=0" target="_blank">Trial Partners</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=mr&index=0" target="_blank">Merchants</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=ag&index=0" target="_blank">Affinity Groups</a></li>
												<li><a href="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&report_type=p&index=0" target="_blank">Partners</a></li>
                                                
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					</li>







<li class="has-flyout"><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank">G.P.S.</a>
<div class="flyout ">
<div class="row">
<div class="twelve columns">
<h5><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank"><span class="gibbs">G</span>lobal <span class="gibbs">P</span>artner <span class="gibbs">S</span>ystem</a></h5>
<div class="row hide-on-phones">
<div class="eight columns">
<p> </p>
<p><a href="https://www.clubshop.com/manual/gps/overview.html" target="_blank">Haz click aquí - para ver todas las ventajas que ofrece y costos la supscripción al G.P.S.</a></p>
</div>
<div class="four columns "><a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank"><img src="common/images/gibbs.png" /></a></div>
</div>
</div>
</div>
</div>
</li>
<li class="has-flyout"><a target="_blank">Seminarios</a>
<div class="flyout small">
<div class="row">
<div class="twelve columns">
<div class="row">
<div class="twelve columns">
<ul>
<li><a href="http://www.clubshop.com/affiliate/webinars.html" target="_blank" >Business Opportunity Presentations</a></li>
<li><a href="/vip/manual/training/interactive_online_schedule.html" target="_blank" >Partner Training Webinars</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</li>
<li><a href="https://www.clubshop.com/vip/coop.shtml" target="_blank" >Co-Op</a></li>

<li class="has-flyout"><a>Personal</a>
<div class="flyout small">
<div class="row">
<div class="twelve columns">
<div class="row">
<div class="twelve columns">
<ul>
<li><a href="https://www.clubshop.com/support/">Ayuda</a> </li>
<li><a href="http://www.clubshop.com/cgi/funding.cgi?action=report">Cuenta del Club</a> </li>
<li><a href="http://www.clubshop.com/cgi/comm_prefs.cgi">Método de preferencia para recibir las comisiones</a> </li>
<li><a href="http://www.clubshop.com/cgi/CommStmnt.cgi">Earnings Statement</a></li>
<li><a href="http://www.clubshop.com/cgi/mopi.cgi">Actualiza la información de pago</a> </li>
<li><a href="http://www.clubshop.com/cgi/appx.cgi/update">Actualiza el perfil</a> </li>
<li><a href="http://www.clubshop.com/cgi/cancel.cgi">Cambia el tipo de membresía</a> </li>
<li><a href="http://www.clubshop.com/cgi/logout.cgi/redirect">Salir</a> </li>
</ul>
</div>
</div>
</div>
</div>
</div>
</li>
        <li class="has-flyout"><a href="">Avisos</a> 
          <div class="flyout small">						
							<div class="row">
				        		<div class="twelve columns">				        			
				    				<div class="row">
				           				<div class="twelve columns">
				            				<ul class="">
												<li><a href="http://www.clubshop.com/p/blog/dblog.xml">El Blog de Burke</a></li>
												<li><a href="http://www.clubshop.com/p/news/topview.xml">View From The Top </a></li>
												
											</ul>
				            			</div>									
									</div>
				  				</div>
				  			</div>						
						</div>
					</li>




<li class="has-flyout show-on-phones"><a>Herramientas</a>
<div class="flyout small right">
<div class="row">
<div class="twelve columns">
<div class="row">
<div class="twelve columns">
<ul>
<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/team">Genealogy Report</a></li>
<li><a href="http://www.clubshop.com/cgi/vip/tree.cgi/grp">Group Report</a></li>
<li><a href="http://www.clubshop.com/cgi/vip/activity_rpt">Activity Report</a></li>
<li><a href="http://www.clubshop.com/cgi/pireport.cgi">Informe de Ingresos</a> </li>
<li><a href="http://www.clubshop.com/vip/manual/training/guide/getstarted/index.html">Training Guide</a></li>
<li><a href="http://www.clubshop.com/cgi/tl-redir">Página del Equipo</a> </li>



</ul>
</div>
</div>
</div>
</div>
</div>
</li>


</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
			$(function() {
				$("a[data-popup]").live('click', function(e) {
					window.open($(this)[0].href);
					// Prevent the link from actually being followed
					e.preventDefault();
				});
			});
// ]]></script>
</div>
</body>
</html>
