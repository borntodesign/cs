<!DOCTYPE html>
<?php
require 'Clubshop/Session/Session.php';
require 'Clubshop/Models.php';
require 'Clubshop/NegotiateByLanguage.php';
require 'Clubshop/Translation/PayLevelDescriptions.php';
require 'Clubshop/Models/TpTransferRequest.php';

if(! $_SERVER['HTTPS']) { header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); exit(); }

$messages = array();    # something we can stuff messages into and show at the top of the page content if necessary
$errors = array();      # same concept
$warnings = array();    # same concept

// setup language negotiation stuff
$negotiator = new \Clubshop\NegotiateByLanguage();
$newsFN = $negotiator->bestCandidateFilename('news.html');
$tmp = array();
// we will use that filname later on, but based upon that, we will make an assumption on what the page language will be
preg_match('/\.([a-z]+)$/', $newsFN, $tmp);
// that should give us the extension
$pageLanguage = $tmp[1];
$pageLocale = $negotiator->bestSupportedLocale($pageLanguage);
//error_log("page language: $pageLanguage - locale: $pageLocale");
putenv("LC_ALL=$pageLocale");
if (setlocale(LC_ALL, $pageLocale) === false)
    error_log("setlocale failed for page language: $pageLanguage - locale: $pageLocale");
bindtextdomain("messages", "pbc-includes/locale");
bind_textdomain_codeset('messages', 'UTF-8');
textdomain("messages");

// load Promotion Analysis stuff from session cache if available -or- regenerate it and save it
$Session = new \Clubshop\Session\Session();
$Models = new \Clubshop\Models('partner_business_center');

// we need to be logged in to get to here, so we will just rely on the cookie
// this resource is protected by Apache mod-perl, so the cookie should be available
$Meminfo = $Session->meminfoFromCookie();
$Members = $Session->getKey('MEMBERS');
if (! isset($Members)){

    $Members = $Models->dbc()->selectrow_array('SELECT country FROM members WHERE id= ?', array((int) $Meminfo['id']));

    $Session->setKey('MEMBERS', $Members);
}

$isPayAgent = $Session->getKey('isPayAgent');
if (! isset($isPayAgent)) {
    $isPayAgent = $Models->dbc()->fetchSingleCol('SELECT id FROM pay_agent.agents WHERE id= ?', array((int) $Meminfo['id']));
    // if there is no matching record, this method returns false
    $Session->setKey('isPayAgent', $isPayAgent);
}

// set this up as it will be used for some currency formatting below
$Models->setCurrencyNumberFormattingLocale($Members['country'], $negotiator->bestLanguageShort());
$Models->setID($Meminfo['id']);

$refcomp = $Session->getKey('REFCOMP');
if (! isset($refcomp)){
    // that currently returns these columns: * FROM network.promotion_analysis -or- a similarly named function, plus a flag (nopay) to indicate current subscription status
    $refcomp = $Models->getPromotionAnalysis($Meminfo['id']);
    if ($refcomp) {
        $refcomp['next_mcg_formatted'] = $Models->format_currency_by_locale(
             sprintf('%u', $Models->exchange_usd_for_cbc($refcomp['next_mcg'], $Members['country'])) );
    }
    // we could come up empty handed, so we want to at least initialize the var to an empty array
    $Session->setKey('REFCOMP', ($refcomp ? $refcomp : array()));
}

$tnt = $Session->getKey('TNT_LEVEL_VALUES');
if (! isset($tnt)){
    // this gives us the highest pay level row that we have sufficient TPP for
    $sv = $Models->dbc()->fetchSingleCol('SELECT network.some_share_value_by_id(?)', array($Meminfo['id']));
    $tnt = $Models->dbc()->selectrow_array('SELECT * FROM network.level_values_tnt WHERE tmpp <= ? ORDER BY pay_level DESC LIMIT 1', array((int) $refcomp['tpp']));
    $tnt['mcg_formatted'] = $Models->format_currency_by_locale( sprintf('%u', $Models->exchange_usd_for_cbc((($tnt['shares'] + $tnt['xdir_bonus_shares']) * $sv), $Members['country'])) );
    
    $Session->setKey('TNT_LEVEL_VALUES', $tnt);
}

$ps_counts = $Session->getKey('PSCOUNTS');
if (! isset($ps_counts)){
//    $ps_counts = $Models->psCounts();
    $ps_counts = $Models->dbc()->fetchSingleCol('SELECT COUNT(*) FROM members m JOIN od.pool p ON m.id=p.id WHERE m.spid= ?', array((int) $Meminfo['id']));
    $Session->setKey('PSCOUNTS', $ps_counts);
}

$trialPartnerInfo = $Session->getKey('TP_INFO');
if (! isset($trialPartnerInfo)){
    // this gives us the highest pay level row that we have sufficient TPP for
    $trialPartnerInfo = $Models->getTrialPartnerData();
    
    // there simply may not be any data, so we will create an empty key so as to not keep checking
    if (! $trialPartnerInfo)
        $trialPartnerInfo = array();
    $Session->setKey('TP_INFO', $trialPartnerInfo);
}

$payLevelDescriptions = new \Clubshop\Translation\PayLevelDescriptions($pageLanguage, $pageLocale);
$TpXr = new \Clubshop\Models\TpTransferRequest($Models->dbc(), $Meminfo['id']);

if (isset($_GET['action']) && $_GET['action'] == 'requestXfer') {
    if ($d = $TpXr->checkRequest($Members['country'])) {
        $warnings[] = sprintf(gettext('A request for a Trial Partner transfer was sent on %s.'), $d['stamp_formatted']);
    } elseif ($TpXr->makeRequest($Meminfo['membertype'])) {
        $messages[] = gettext('Your request for a Trial Partner transfer has been sent.');
    } else {
        $errors[] = gettext('Your request for a Trial Partner transfer was unsuccessful.');
    }
}
/*
    our business logic used for the Trial Partner Group Report link
    returns 1 if we're to render the link
*/
function showTPGroupNav()
{
    global $ps_counts;
    if ($ps_counts) {
        return 1;
    }
    return 0;
}
?>
<html lang="<?php echo $pageLanguage; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />

<title><?=gettext('Partner Business Center'); ?></title>

<script type="text/javascript" src="/cgi/PSA.cgi/reset"><!-- this simply resets the PartnerSubscription cookie --></script>

<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />

<link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
<link rel="stylesheet" type="text/css" href="/css/partner/orbit.css" />
<link rel="stylesheet" type="text/css" href="/css/manual_2012.css" />

<!-- [if lt IE 9] >
    <link rel="stylesheet" href="/css/partner/ie.css">
<![endif]-->

<!-- [if lt IE 9]>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/partner/foundation.js" type="text/javascript"></script>
<script src="/js/partner/jquery.orbit-1.2.3.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi?id=<?=$refcomp['id'];?>"></script>
<script type="text/javascript">
var showText="<?=gettext('View More'); ?>";
var hideText="<?=gettext('Hide'); ?>";
var is_visible = false;

$(document).ready(function() {
	Transpose.transpose_currency('transposeME');
    Transpose.format_currency('formatME');
    
    $('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');
    $('.toggle').hide();
    $('a.toggleLink').click(function() {
    	
        
        is_visible = !is_visible;
        $(this).html( (!is_visible) ? showText : hideText);
        
        // toggle the display - uncomment the next line for a basic "accordion" style
        //$('.toggle').hide();$('a.toggleLink').html(showText);
        $(this).parent().next('.toggle').toggle('slow');
        
        // return false so any link destination is not followed
        return false;
    });
});

function ckPRTP()
{
	if (<?php echo showTPGroupNav(); ?>) {
//		alert('true ?');
		return true;					// if they already have a personally referred TP or P, then give 'em the link
	}
	$('#tpgroup_blurb').toggle();
	return false;
}
</script>

<link rel="stylesheet" type="text/css" href="pbc-includes/styles.css" />

</head>

<body class="blue">

	<!-- Top Blue Header Container -->

	<!-- php include $negotiator->bestCandidateFilename("common/body/header.shtml"); -->
	<div class="container blue">
		<div class="row">
			<div class="four columns">
				<a href="http://www.clubshop.com/p/"><img
					src="/images/partner/cs_logo.jpg" height="84" width="288"
					alt="ClubShop Rewards" style="margin: 12px;" /></a>
			</div>

			<div class="eight columns">
				<!--Extra Space Future Space -->
				<h5 style="color: #FFA22F; margin-top: 10px;"><?php printf(gettext('%s - Welcome to your'), $Meminfo['firstname']);?></h5>
				<h3 style="color: #FFF;"><?=gettext('Partner Business Center'); ?></h3>

				<div style="float: right; background-color: white; padding: 6px; margin-bottom: 18px;">
				    <?=gettext('Have questions?') . ' ' . gettext('Need help?'); ?><br />
					<?php printf(gettext('Go to our %sSupport Center%s or %sContact your Partner Coach%s'),
					    '<a href="http://www.clubshop.com/support" target="_blank">',
					    '</a>',
					    '<a href="http://www.clubshop.com/cgi/mytcex.cgi" target="_blank">',
					    '</a>');?>
				</div>


			</div>
		</div>
	</div>


	<!--End Top Blue Header Container -->

	<!--Main Container -->

	<!-- START OF MENU ROW -->

<div class="container white">
		<div class="row">
			<!-- MENU ROW -->
			<div class="twelve columns">

				<ul class="nav-bar" id="nav-bar-ul">

					<li><a href="http://www.clubshop.com/cgi-bin/wwg2"><?=gettext('Shopping &amp; Rewards'); ?></a></li>

					<li><a href="http://www.clubshop.com/manual/index.shtml"><?=gettext('Manual'); ?></a></li>
					
					<li class="has-flyout"><a href="#"><?=gettext('Member Search/Lists'); ?></a>
						<div class="flyout small">
							<div class="row">
								<div class="twelve columns">
									<div class="row">
										<div class="twelve columns">
											<ul class="">
												<li><a href="https://www.clubshop.com/cgi/vip/member-followup.cgi" target="_blank"><b><?=gettext('Member Search'); ?></b></a></li>
												<li><b><span class="gibbs"><?=gettext('Membership Lists'); ?></span></b></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=m&amp;index=0"><?=gettext('Members'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=a&amp;index=0"><?=gettext('Affiliates'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=tp&amp;index=0"><?=gettext('Trial Partners'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=mr&amp;index=0"><?=gettext('Merchants'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=ag&amp;index=0"><?=gettext('Affinity Groups'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0&amp;report_type=p&amp;index=0"><?=gettext('Partners'); ?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>

					<li class="has-flyout"><a href=""><?=gettext('Reports'); ?></a>
						<div class="flyout small right">
							<div class="row">
								<div class="twelve columns">
									<div class="row">
										<div class="twelve columns">
											<ul class="">
												<li><a href="https://www.clubshop.com/cgi/vip/activity_rpt"><?=gettext('Activity Report'); ?></a></li>
												
										        <li><a class="askipme" href="https://www.clubshop.com/cgi/vip/tree.cgi/tp?id=<?=$Meminfo['id']?>" onclick="return ckPRTP()"><?=gettext('Trial Partner Group Report')?></a></li>
										    <!-- a Trial Partner -or- a new Partner will have a key in the Trial Partner line -->
												<?php if (array_key_exists('id', $trialPartnerInfo)): ?>
    											<li><a href="https://www.clubshop.com/cgi/TrialPartner?id=<?=$trialPartnerInfo['id']?>"><?=gettext('Trial Partner Team Report')?></a></li>
												<?php endif;?>
												<li><a href="https://www.clubshop.com/cgi/vip/tree.cgi/grp"><?=gettext('Partner Group Report')?></a></li>
												<li><a href="https://www.clubshop.com/cgi/pireport.cgi"><?=gettext('Income Reports')?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div></li>

					<li class="has-flyout"><a href="" target="_blank"><?=gettext('Training'); ?></a>
						<div class="flyout small">
							<div class="row">
								<div class="twelve columns">
									<div class="row">
										<div class="twelve columns">
											<ul class="">
												<li><a href="http://www.clubshop.com/manual/training/index.html"><?=gettext('Training Guide'); ?></a></li>
												<li><a href="http://www.clubshop.com/affiliate/webinars.html"><?=gettext('Business Opportunity Presentations'); ?></a></li>
												<li><a href="/vip/manual/training/interactive_online_schedule.html"><?=gettext('Partner Training Webinars'); ?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>

					<li><a href="https://www.clubshop.com/vip/coop.shtml"
						target="_blank"><?=gettext('Co-Op'); ?></a></li>

					<li class="has-flyout"><a href=""><?=gettext('Personal'); ?></a>
						<div class="flyout small">
							<div class="row">
								<div class="twelve columns">
									<div class="row">
										<div class="twelve columns">
											<ul class="">
												<li><a href="http://www.clubshop.com/support"><?=gettext('Help'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/funding.cgi?action=report"><?=gettext('ClubAccount'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/comm_prefs.cgi"><?=gettext('Commission Payment Method'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/CommStmnt.cgi"><?=gettext('Earnings Statement'); ?></a></li>
                                                <?php if ($isPayAgent):?>
												<li><a href="https://www.clubshop.com/pax/pa"><?=gettext('Pay Agent Transfer Center'); ?></a></li>
												<?php endif;?>
												<li><a href="https://www.clubshop.com/cgi/mopi.cgi"><?=gettext('Update Payment Information'); ?></a></li>
												<li><a href="https://www.clubshop.com/cgi/appx.cgi/update"><?=gettext('Update Profile'); ?></a></li>
												<li><a href="http://www.clubshop.com/cgi/cancel.cgi"><?=gettext('Change Membership'); ?></a></li>
												<li><a class="askipme" href="http://www.clubshop.com/cgi/logout.cgi/redirect"><?=gettext('Logout'); ?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>

					<!--<li class="has-flyout"><a href=""><?=gettext('Communications'); ?></a>
						<div class="flyout small">
							<div class="row">
								<div class="twelve columns">
									<div class="row">
										<div class="twelve columns">
											<ul class="">
												<li><a href="http://www.clubshop.com/p/blog/dblog.xml"><?=gettext('Burke Blog'); ?></a></li>
												<li><a href="http://www.clubshop.com/p/news/topview.xml"><?=gettext('View From The Top'); ?></a></li>
												<li><a href="http://www.clubshop.com/cgi/tl-redir"><?=gettext('Team Page');?></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>-->
				</ul>

			</div>
		</div>
<script type="text/javascript">
	$(function() {
		$("#nav-bar-ul").on('click', 'a:not(.askipme)', function(e) {
			window.open($(this)[0].href);
			// Prevent the link from actually being followed
			e.preventDefault();
		});
	});
</script>

</div>

	<!-- END OF MENU ROW -->

	<div class="container white">

<!-- Messages/Warnings/Errors that pertain to the operation of this script go right at the top if they exist -->
	<?php if ($messages || $warnings || $errors):?>
	   <div class="row">
	   <?php foreach ($messages as $msg):?>
	       <div class="alert-box success"><?=$msg?></div>
	   <?php endforeach;?>
	   <?php foreach ($warnings as $msg):?>
	       <div class="alert-box warning"><?=$msg?></div>
	   <?php endforeach;?>
	   <?php foreach ($errors as $msg):?>
	       <div class="alert-box error"><?=$msg?></div>
	   <?php endforeach;?>
	   </div>
	<?php endif;?>

	<div id="tpgroup_blurb" class="row" style="display:none">
	   <div class="panelpbc">
        <h4 class="darkblue bold" align="center"><?=gettext('TRIAL PARTNER GROUP REPORT UNAVAILABLE');?></h4>
        <br />
        <?php
            echo gettext('You do not yet have any personally referred Trial Partners/Partners to have a personal Trial Partner Group.'), PHP_EOL;
            $d = $TpXr->checkRequest($Members['country']);
            if ($d) {
                echo '<br />';
                printf(gettext('A request for a Trial Partner transfer was sent on %s.'), $d['stamp_formatted']);
            } else {
                printf(gettext('If you are willing to be trained and work with your Sponsor and/or Coach to learn how to use TNT and your GPS to build a line of Trial Partners to gain more PRPs and GRPs, then <a href="%s">CLICK HERE</a> to request that your Partner Sponsor or Coach transfer a Trial Partner to you, to help you start and communicate with a personal Trial Partner Group.'),
                '?action=requestXfer');
            }
        ?>
        <p style="text-align: right; font-weight: bold; margin-bottom:0;"><a class="Grey" href="#" onclick="$('#tpgroup_blurb').hide(); return false;">X</a></p>
        </div>
    </div>

<?php
include 'pbc-includes/partner-team-update.php';

if (isset($refcomp['id']) && $refcomp['tpp'] >= 100)
    include 'pbc-includes/promotion-analysis.php';
?>
	

		<div class="row">

			<div class="eight columns">

				<div id="google_translate_element"></div>
<script type="text/javascript">

function googleTranslateElementInit() {

  new google.translate.TranslateElement({

	    pageLanguage: '<?php echo $pageLanguage; ?>'

  }, 'google_translate_element');

}

</script>

<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

<p><?php
echo gettext("The Google Translator above is provided at this website, so all of our worldwide ClubShop Rewards Members may have access to a familiar translation for this page, other than the version shown below. "); 
echo gettext("Although a computer program translated version for a webpage may not provide one with a high quality translation, it does enable better global access to the ClubShop Rewards website.");
?>
</p>

				<h4><?=gettext('Latest News and Updates'); ?></h4>

				<br />

<?php include $newsFN; ?>
				
				<hr />

			</div>
			<!-- END OF EIGHT COLUMN -->

			<div class="four columns">

				<!--<div class="panel">
					<img src="images/promo.png" alt="" style="margin: 0px auto;" /> <span class="date"><?php
					   $promo_stamp = filemtime('./promotions/promotions.html');
					   echo gettext(date('F', $promo_stamp)) . PHP_EOL . gettext(date('Y', $promo_stamp)); ?></span>
					<p><?=gettext('To view Pay Level promotions for this month:') ?>
                        <a href="http://www.clubshop.com/p/promotions/promotions.html" target="_blank"><?=gettext('Click Here') ?></a>
					</p>

				</div>-->




					<!--<div class="panel">
					<img src="images/foodforthought.png" alt="" style="margin: 0px auto;" /> <span class="date"><?php
					   $promo_stamp = filemtime('./foodforthought.html');
					   echo gettext(date('F', $promo_stamp)) . PHP_EOL . gettext(date('Y', $promo_stamp)); ?></span>
					<p><?=gettext('From DHS Club Founder and CEO Dick Burke:') ?>
                        <a href="http://www.clubshop.com/p/foodforthought.html" target="_blank"><?=gettext('Click Here') ?></a>
					</p>

				</div>-->






				<br />

<?php include $negotiator->bestCandidateFilename('pbc-includes/mall-panel.php'); ?>	

	</div>

			<hr />

		</div>

	</div>

	<!--End Main Container -->


	<!--Footer Container -->

<?php include $negotiator->bestCandidateFilename("common/footer/footer.shtml"); ?>

<!--End Footer Container -->

</body>

</html>
