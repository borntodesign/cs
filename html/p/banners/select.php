<?php

include 'Get_local_url.php';

$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
if (! preg_match('/^\w{2}$/', $lang))
{
	$lang = 'en';
}
$xml = '<root>';
$xsp = GetRelativeURI('select.xsp.' . $lang);
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);
$xsp = GetRelativeURI('banners.xml');
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);
$xml .= GetURI("/cgi/rpc.cgi?proc=memberinfo_by_ck;Cookie={$_COOKIE['AuthCustom_Generic']}");
$xml .= '</root>';
//error_log($xml);
$xsl = GetRelativeURI('banners.xsl');

$xslDD = new DOMDocument();
$xslDD->loadXML($xsl);
$processor = new XsltProcessor();
$processor->importStyleSheet($xslDD);

$xmlDD = new DOMDocument();
$xmlDD->loadXML($xml);

$xml = $processor->transformToDOC($xmlDD);
echo $xml->saveHTML();
?>