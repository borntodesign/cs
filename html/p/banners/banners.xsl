<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:param name="request.filename"/>

<xsl:template match="/">
<html>
<head>
<script type="text/javascript" src="/cgi/PSA/script/17"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>
<title>Banner Generator</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/general.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="https://www.clubshop.com/js/partner/foundation.js"></script>
<script src="https://www.clubshop.com/js/partner/app.js"></script>
<script src="https://www.clubshop.com/js/partner/flash.js"></script>
<script src="https://www.clubshop.com/js/panel.js"></script>
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>
</head>

<body class="blue">
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
<div class="six columns"></div>
</div></div>
<div class="container white">
<div class="row">
<div class="twelve columns ">
<br />



<p><xsl:value-of select="//lang_blocks/p1"/></p>



<select style="margin-top:2px" onchange="location=this.options[this.selectedIndex].value">
<xsl:for-each select="*/lang_blocks/menu-lang/*"><xsl:sort order="ascending" />
<option><xsl:attribute name="value"><xsl:value-of select="@value" /></xsl:attribute>
	<xsl:if test="@value = substring-after($request.filename, '/home/httpd/html/p/banners/')">
	<xsl:attribute name="selected">selected</xsl:attribute>
	</xsl:if>
<xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<!-- If we don't have an ID on them, the banners will not work -->
<xsl:choose><xsl:when test="//memberinfo/id">
<p class="a">
<xsl:value-of select="*/lang_blocks/s2" />.
<xsl:value-of select="*/lang_blocks/s3" />.</p></xsl:when>
<xsl:otherwise><p class="a" style="color:red"><xsl:value-of select="*/lang_blocks/noid" />.
<a href="https://www.clubshop.com/cs/login.shtml?destination=/p/banners/select.php"><xsl:value-of select="*/lang_blocks/login" /></a>.</p>
</xsl:otherwise>
</xsl:choose>

<table id="mainTBL">
<xsl:for-each select="*/img">
<tr>
<td class="desc"><xsl:value-of select="concat(./@width, ' x ', ./@height)" /></td>
<!--td class="bn"><a class="bn" href="javascript:void(0)" onclick="flipDesc(this);"><img-->
<td class="bn"><a class="bn" href="javascript:void(0)"><img>

	<xsl:attribute name="width"><xsl:value-of select="./@width" /></xsl:attribute>
	<xsl:attribute name="alt"><xsl:value-of select="./@alt" /></xsl:attribute>
	<xsl:attribute name="height"><xsl:value-of select="./@height" /></xsl:attribute>
	<xsl:attribute name="src">/banners/<xsl:value-of select="./@src" /></xsl:attribute>
	<!--xsl:attribute name="onmouseover">this.style.borderColor='#00ff00'</xsl:attribute>
	<xsl:attribute name="onmouseout">this.style.borderColor='white'</xsl:attribute-->
</img></a>
<div class="b">
<!--xsl:choose>
	<xsl:when test="@type = 'consumer'">&lt;a href=&quot;http://www.clubshop.com/cgi-bin/rd/4,,refid=</xsl:when>
	<xsl:otherwise>&lt;a href=&quot;http://www.clubshop.com/cgi-bin/rd/16,,refid=</xsl:otherwise>
</xsl:choose-->
&lt;a href=&quot;http://www.clubshop.com/cgi-bin/rd/16/0/refid%3D<xsl:value-of select="//memberinfo/id" />&quot;&gt;&lt;img<br /> src=&quot;http://www.clubshop.com/banners/<xsl:value-of select="@src" />&quot;<br />
height=&quot;<xsl:value-of select="@height" />&quot; width=&quot;<xsl:value-of select="@width" />&quot; alt=&quot;<xsl:value-of select="@alt" />&quot; /&gt;&lt;/a&gt;
</div></td>
</tr>
<tr><td colspan="2" class="spc"></td></tr>
</xsl:for-each>
</table>



</div>
<br/><br/><br/>





</div>
</div>
<!--Footer Container -->
<div class="container blue"> 
<div class="row">
<div class="twelve columns">
<div class="push"></div>
</div>

  </div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
 </div>
    </div>
  </div>
<!--End Footer Container -->
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
