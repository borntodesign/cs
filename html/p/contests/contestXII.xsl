
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title><xsl:value-of select="//lang_blocks/pp"/></title>


     <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/general.css" />
	<link rel="stylesheet" type="text/css" href="/css/manual_2012.css"/>
	<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'/>
	
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="https://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="https://www.clubshop.com/js/partner/app.js"></script>
    <script src="https://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->







<style>


.prize{
	font-family: 'Yanone Kaffeesatz', sans-serif;
	text-align:center;
}

.color_box {
    border: 1px solid #F9F9F9;
    height: 30px;
    width: 100%;
}

.tc { background: #499bea;
background: -moz-linear-gradient(top,  #499bea 1%, #3e62c4 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#499bea), color-stop(100%,#3e62c4));
background: -webkit-linear-gradient(top,  #499bea 1%,#3e62c4 100%);
background: -o-linear-gradient(top,  #499bea 1%,#3e62c4 100%);
background: -ms-linear-gradient(top,  #499bea 1%,#3e62c4 100%);
background: linear-gradient(top,  #499bea 1%,#3e62c4 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#499bea', endColorstr='#3e62c4',GradientType=0 );
}

.bronze{background: #ab6a29;
background: -moz-linear-gradient(top,  #ab6a29 0%, #bf6e4e 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ab6a29), color-stop(100%,#bf6e4e));
background: -webkit-linear-gradient(top,  #ab6a29 0%,#bf6e4e 100%);
background: -o-linear-gradient(top,  #ab6a29 0%,#bf6e4e 100%);
background: -ms-linear-gradient(top,  #ab6a29 0%,#bf6e4e 100%);
background: linear-gradient(top,  #ab6a29 0%,#bf6e4e 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ab6a29', endColorstr='#bf6e4e',GradientType=0 );
}

.silver{background: #d1d1d1;
background: -moz-linear-gradient(top,  #d1d1d1 0%, #bfbfbf 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d1d1d1), color-stop(100%,#bfbfbf));
background: -webkit-linear-gradient(top,  #d1d1d1 0%,#bfbfbf 100%);
background: -o-linear-gradient(top,  #d1d1d1 0%,#bfbfbf 100%);
background: -ms-linear-gradient(top,  #d1d1d1 0%,#bfbfbf 100%);
background: linear-gradient(top,  #d1d1d1 0%,#bfbfbf 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d1d1d1', endColorstr='#bfbfbf',GradientType=0 );
}

.gold {background: #cbb101;
background: -moz-linear-gradient(top,  #cbb101 0%, #c4a701 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#cbb101), color-stop(100%,#c4a701));
background: -webkit-linear-gradient(top,  #cbb101 0%,#c4a701 100%);
background: -o-linear-gradient(top,  #cbb101 0%,#c4a701 100%);
background: -ms-linear-gradient(top,  #cbb101 0%,#c4a701 100%);
background: linear-gradient(top,  #cbb101 0%,#c4a701 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cbb101', endColorstr='#c4a701',GradientType=0 );
}

.ruby {background: #aa0025;
background: -moz-linear-gradient(top,  #aa0025 1%, #82001c 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#aa0025), color-stop(100%,#82001c));
background: -webkit-linear-gradient(top,  #aa0025 1%,#82001c 100%);
background: -o-linear-gradient(top,  #aa0025 1%,#82001c 100%);
background: -ms-linear-gradient(top,  #aa0025 1%,#82001c 100%);
background: linear-gradient(top,  #aa0025 1%,#82001c 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aa0025', endColorstr='#82001c',GradientType=0 );
}

.emerald {background: #006400;
background: -moz-linear-gradient(top,  #006400 1%, #008e09 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#006400), color-stop(100%,#008e09));
background: -webkit-linear-gradient(top,  #006400 1%,#008e09 100%);
background: -o-linear-gradient(top,  #006400 1%,#008e09 100%);
background: -ms-linear-gradient(top,  #006400 1%,#008e09 100%);
background: linear-gradient(top,  #006400 1%,#008e09 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#006400', endColorstr='#008e09',GradientType=0 );
}

.diamond {background: #d1d1e7;
background: -moz-linear-gradient(top,  #d1d1e7 1%, #e5e8f9 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#d1d1e7), color-stop(100%,#e5e8f9));
background: -webkit-linear-gradient(top,  #d1d1e7 1%,#e5e8f9 100%);
background: -o-linear-gradient(top,  #d1d1e7 1%,#e5e8f9 100%);
background: -ms-linear-gradient(top,  #d1d1e7 1%,#e5e8f9 100%);
background: linear-gradient(top,  #d1d1e7 1%,#e5e8f9 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d1d1e7', endColorstr='#e5e8f9',GradientType=0 );
}

.exec {background: #001966;
background: -moz-linear-gradient(top,  #001966 1%, #2100a8 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#001966), color-stop(100%,#2100a8));
background: -webkit-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: -o-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: -ms-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: linear-gradient(top,  #001966 1%,#2100a8 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001966', endColorstr='#2100a8',GradientType=0 );
}

.exec1 {background: #001966;
background: -moz-linear-gradient(top,  #001966 1%, #2100a8 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#001966), color-stop(100%,#2100a8));
background: -webkit-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: -o-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: -ms-linear-gradient(top,  #001966 1%,#2100a8 100%);
background: linear-gradient(top,  #001966 1%,#2100a8 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001966', endColorstr='#2100a8',GradientType=0 );
}

.effect
{
    position:relative;
    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
.effect:before, .effect:after
{
    content:"";
    position:absolute;
    z-index:-1;
    -webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
    -moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
    box-shadow:0 0 20px rgba(0,0,0,0.8);
    top:0;
    bottom:0;
    left:10px;
    right:10px;
    -moz-border-radius:100px / 10px;
    border-radius:100px / 10px;
}
.effect:after
{
    right:10px;
    left:auto;
    -webkit-transform:skew(8deg) rotate(3deg);
       -moz-transform:skew(8deg) rotate(3deg);
        -ms-transform:skew(8deg) rotate(3deg);
         -o-transform:skew(8deg) rotate(3deg);
            transform:skew(8deg) rotate(3deg);
}

h5.new {color: navy;}


</style>



</head>

<body class="blue">

	<div class="container blue">
		<div class="row">
			<div class="six columns"><a href="#"><img src="../../images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
			<div class="six columns"></div>

		</div>
	</div>  <!-- / logo container -->
	
	

	<div class="container white">
		<div class="row">
			<div class="twelve columns ">
			<br />
			<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>
			<hr />
			</div>
		</div>
			

      
	<div class="row">
		<div class="twelve columns">
			<div align="right"><a href="contest.xml" target="_blank"><xsl:value-of select="//lang_blocks/jan"/></a></div>
			<div align="right"><a href="contestII.xml" target="_blank"><xsl:value-of select="//lang_blocks/feb"/></a></div>

			<p class="style28"><xsl:value-of select="//lang_blocks/p1"/></p>

			<ol>
				<li><xsl:value-of select="//lang_blocks/p2"/></li>
				<li><xsl:value-of select="//lang_blocks/p3"/></li>
				<li><xsl:value-of select="//lang_blocks/p4"/></li>
			

			
			</ol>
				
			
			<hr/>
		</div>
	</div>
	
	
		<div class="row">
		<div class="twelve colums" style="text-align:left;">
			<span class="liteGrey">Updated: 12/31/12 </span> 
			
		</div>
	</div>
		
	<div class="row">
		<div class="twelve colums" style="text-align:center;">
			<span class="style28" ><xsl:value-of select="//lang_blocks/p16"/></span>
			<hr/>
		</div>
	</div>
	
	
	


	<div class="row">
		<div class="twelve columns">
		
			<div class="row hide-on-phones">	<!-- winner row -->
					<div class="two columns"><h3 class="prize " style="color:#2A85E8;">LEGEND</h3></div>
					
					<div class="one columns"><div class="color_box effect tc"></div><h6 class="prize">CAPTAIN</h6><br/><div align="center">$25</div></div>					
					<div class="one columns"><div class="color_box effect bronze"></div><h6 class="prize">BRONZE</h6><br/><div align="center">$50</div></div>
					<div class="one columns"><div class="color_box effect silver"></div><h6 class="prize">SILVER</h6><br/><div align="center">$75</div></div>
					<div class="one columns"><div class="color_box effect gold"></div><h6 class="prize">GOLD</h6><br/><div align="center">$100</div></div>
					<div class="one columns"><div class="color_box effect ruby"></div><h6 class="prize">RUBY</h6><br/><div align="center">$200</div></div>
					<div class="one columns"><div class="color_box effect emerald"></div><h6 class="prize">EMERALD</h6><br/><div align="center">$300</div></div>
					<div class="one columns"><div class="color_box effect diamond"></div><h6 class="prize">DIAMOND</h6><br/><div align="center">$400</div></div>
					<div class="two columns"><div class="color_box effect exec"></div><h6 class="prize">EXEC OR Add'l Star</h6><br/><div align="center">$500</div></div>
					<div class="one columns"><div class="color_box effect"></div><h6 class="prize">Total</h6></div>
					<hr/>
			</div> <!-- / of winner row -->	
			
			
			
			
			
			


<!--EMERALD DIRECTOR -->


			<div class="row">	
					<div class="three columns"><h5>Daniela Bellu</h5></div>
					
					<div class="one columns hide-on-phones"></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"><div class="color_box effect emerald"></div><h6 class="prize">$300.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns "><h6 class="prize">$300.00</h6></div>
					<hr/>
			</div>
		

				<div class="row">	
					<div class="three columns"><h5>Paula Griffin</h5></div>
					
					<div class="one columns hide-on-phones"></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"><div class="color_box effect emerald"></div><h6 class="prize">$300.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns "><h6 class="prize">$300.00</h6></div>
					<hr/>
			</div>
			
			
			
			<div class="row">	
					<div class="three columns"><h5>Scott Secord</h5></div>
					
					<div class="one columns hide-on-phones"></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"><div class="color_box effect ruby"></div><h6 class="prize">$200.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns "><h6 class="prize">$200.00</h6></div>
					<hr/>
			</div>
			
			<div class="row">	
					<div class="three columns"><h5>Laurent Fritsch</h5></div>
					
					<div class="one columns hide-on-phones"></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"><div class="color_box effect gold"></div><h6 class="prize">$100.00</h6></div>
					<div class="one columns hide-on-phones"><div class="color_box effect ruby"></div><h6 class="prize">$200.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns "><h6 class="prize">$300.00</h6></div>
					<hr/>
			</div>
			
			
			
			
			
	
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Vito Chinellato</h5></div>
					
					<div class="one columns hide-on-phones"></div>					
					<div class="one columns hide-on-phones"><div class="color_box effect bronze"></div><h6 class="prize">$50.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$50.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			

			

<!--TEAM CAPTAIN -->			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Amir Chaabane</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"><div class="color_box effect bronze"></div><h6 class="prize">$50.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$75.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Anna Valerio</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"><div class="color_box effect bronze"></div><h6 class="prize">$50.00</h6></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$75.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Marco Pelizzari</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Anthony Woutaz</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Willy Elia</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>John Priestley</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Christian Randazzo</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
			
			<div class="row">	<!-- winner row -->
					<div class="two columns"><h5>Frederick Combs</h5></div>
					
					<div class="one columns hide-on-phones"><div class="color_box effect tc"></div><h6 class="prize">$25.00</h6></div>					
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="one columns hide-on-phones"></div>
					<div class="two columns hide-on-phones"></div>
					<div class="one columns"><h6 class="prize">$25.00</h6></div>
					<hr/>
			</div> <!-- / of winner row -->
			
		
			
			
		</div>
	</div>



	</div> <!-- / container white -->





<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>