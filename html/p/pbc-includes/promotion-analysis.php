<div class="row hide-on-phones">
	<script type="text/javascript" src="/js/tipMap.js"></script>
	<script type="text/javascript">
	function showA() {
		$('#promo_show_link').hide();
		$('#promoanalysis').show();
	}
	function hideA() {
		$('#promo_show_link').show();
		$('#promoanalysis').hide();
	}
	</script>
	<div id="promo_show_link"><a href="#" onclick="showA(); return false;"><?php echo gettext('CLICK HERE to see your personal Business Analysis'); ?></a></div>
	
	<div class="panelpbc" id="promoanalysis" style="display:none">
		<h4 align="center" class="darkblue bold"><?php echo gettext('YOUR PERSONAL BUSINESS ANALYSIS'); ?></h4>
<?php if ($refcomp['nopay']):?>
    <p style="background-color:#ffc; padding: 0.5em;">
    <?=gettext("To be able to share in and earn Team Commissions, you have to first meet the 10 Personal Pay Point requirement.")?>
    
    <?=gettext("Since your GPS fees are unpaid at this time, you have not yet met this requirement.")?>
    
    <?php printf(gettext('You need to fund your %s with enough money to pay your fees in order to earn team Commissions this month and retain your Partner position on your Team and to retain the Partners and Trial Partners that are in your Group.'),
        '<a href="https://www.clubshop.com/cgi/funding.cgi?action=report">ClubAccount</a>')?>
    </p>
<?php else:?>
		<br />
		<br />
<?php endif;?>

		<ul>
			<li class="blue_bullet"><span class="royal"><?php
echo gettext('VESTED INCOME POTENTIAL');
echo ' - ';
printf(gettext('Your current Team Pay Points are <b>%s</b> which satisfies the Team Pay Points Qualifications for you to become a <span class="style24">%s</span> and earn <span style="font-weight:bold" class="transposeME">%s</span> per month from your business.'),
    $refcomp['tpp'],
    $payLevelDescriptions->descriptionByPayLevel($tnt['pay_level']),
    $tnt['mcg_formatted']);
?>
    </span></li>

			<li class="blue_bullet"><span class="royal"><?php
echo gettext('CURRENT INCOME POTENTIAL');
echo ' - ';
printf(gettext('You are currently qualifying for the <span class="style24">%s</span> pay level, according to the <a href="%s" target="_blank">Partner Compensation Plan</a>.'),
    $payLevelDescriptions->descriptionByPayLevel($refcomp['pay_level']),
    '/p/r/PersonalTeamCommissions');
echo PHP_EOL;
printf(gettext('Here is what you need to do to gain a promotion to the <span class="style24">%s</span> pay level and earn <span class="transposeME">%s</span> in Team Commissions:'),
    $payLevelDescriptions->descriptionByPayLevel($refcomp['next_pay_level']),
    $refcomp['next_mcg_formatted']);
?>
    </span></li>
		</ul>

		<table style="margin-left: auto; margin-right: auto;">
			<tr>
				<td class="style24 center"><?php echo gettext('QUALIFIER'); ?></td>
				<td class="style24 center"><?php echo gettext('AMOUNT NEEDED'); ?></td>
				<td class="style24 center"><?php echo gettext('AMOUNT YOU HAVE'); ?></td>
				<td class="style24 center"><?php echo gettext('INCREASE NEEDED'); ?></td>
			</tr>

<!-- ################ PRP ################ -->
<?php
if ($refcomp['next_prp'] && $refcomp['next_prp_pp']) {
    
    echo '<tr><td class="style24 center"><a href="#" class="tip_trigger">PRP/PP<span class="tip" style="display: none; text-transform:none;"> PRP/PP - ';
    echo gettext('This is the number of current Personally Referred Partners and the number of Personally Referred Pay Points you have.');
    echo PHP_EOL;
    echo gettext('These Pay Points include your personal GPS Pay Points and the "Power Points" generated from your personal purchases and from the purchases made by your personally referred memberships and from the GPS Pay Points of your PRPs.');
    echo <<<MARK1
    </span></a></td>
	<td class="center">{$refcomp['next_prp']} / {$refcomp['next_prp_pp']}</td>
	<td class="center">{$refcomp['prp']} / {$refcomp['prp_pp']}</td>
	<td class="center">
MARK1;
    
    if ($refcomp['diff_prp'] > 0 && $refcomp['diff_prp_pp'] > 0) {
        echo "{$refcomp['diff_prp']} / {$refcomp['diff_prp_pp']}";
    } else {
        echo gettext('NONE');
    }
} else {
    echo <<<MARK2
    <tr><td class="style24 center">PRP</td>
	<td class="center">{$refcomp['next_prp']}</td>
	<td class="center">{$refcomp['prp']}</td>
	<td class="center">
MARK2;
    
    if ($refcomp['diff_prp'] > 0) {
        echo $refcomp['diff_prp'];
    } else {
        echo gettext('NONE');
    }
}
?>
</td></tr>

<!-- ################# GRP ################ -->
<?php
if ($refcomp['next_grp'] && $refcomp['next_grp_pp']) {
    
    echo '<tr><td class="style24 center"><a href="#" class="tip_trigger">GRP/PP<span class="tip" style="display: none; text-transform:none;"> GRP/PP - ';
    echo gettext('This is the number of current Partners and the number of Pay Points you have in your personal Group.');
    echo PHP_EOL;
    echo gettext('These Pay Points include your PRP/PP and the "Power Points" generated from the purchases made by the memberships and from the GPS Pay Points generated in your personal Group.');
    echo <<<MARK1
    </span></a></td>
	<td class="center">{$refcomp['next_grp']} / {$refcomp['next_grp_pp']}</td>
	<td class="center">{$refcomp['grp']} / {$refcomp['grp_pp']}</td>
	<td class="center">
MARK1;
    
    if ($refcomp['diff_grp'] > 0 && $refcomp['diff_grp_pp'] > 0) {
        echo "{$refcomp['diff_grp']} / {$refcomp['diff_grp_pp']}";
    } else {
        echo gettext('NONE');
    }
} else {
    $diff = $refcomp['next_grp'] - $refcomp['grp'];
    echo <<<MARK2
    <tr><td class="style24 center">GRP</td>
	<td class="center">{$refcomp['next_grp']}</td>
	<td class="center">{$refcomp['grp']}</td>
	<td class="center">
MARK2;
    
    if ($refcomp['diff_grp'] > 0) {
        echo $refcomp['diff_grp'];
    } else {
        echo gettext('NONE');
    }
}
?>
</td></tr>

<!-- ################# SV ################ -->
<?php
if ($refcomp['next_sv_grp'] && $refcomp['next_sv_grp_pp']) {

    echo '<tr><td class="style24 center"><a href="#" class="tip_trigger">SV GRP/PP*<span class="tip" style="display: none; text-transform:none;">SV GRP/PP - ';
    echo gettext('Directors and Executive Directors are required to maintain "Side Volume" GRP/PP, based on their GRP/PP from other than their Partner Group with the highest number of GRP.');
    echo <<<MARK1
    </span></a></td>
	<td class="center">{$refcomp['next_sv_grp']} / {$refcomp['next_sv_grp_pp']}</td>
	<td class="center">{$refcomp['sv_grp']} / {$refcomp['sv_grp_pp']}</td>
	<td class="center">
MARK1;

    if ($refcomp['diff_sv_grp'] > 0 && $refcomp['diff_sv_grp_pp'] > 0) {
        echo "{$refcomp['diff_sv_grp']} / {$refcomp['diff_sv_grp_pp']}";
    } else {
        echo gettext('NONE');
    }
} elseif ($refcomp['next_sv_grp']) {

    echo <<<MARK2
    <tr><td class="style24 center">SV GRP</td>
	<td class="center">{$refcomp['next_sv_grp']}</td>
	<td class="center">{$refcomp['sv_grp']}</td>
	<td class="center">
MARK2;

    if ($refcomp['diff_sv_grp'] > 0) {
        echo $refcomp['diff_sv_grp'];
    } else {
        echo gettext('NONE');
    }
}
?>
</td></tr>

<!-- ################# TPP ################ -->
<?php

echo '<tr><td class="style24 center"><a href="#" class="tip_trigger">TPP<span class="tip" style="display: none; text-transform: none;"> TPP - ';
echo gettext('This is the total number of GPS Pay Points and Power Points generated by yourself and your team.');
echo <<<MARK1
</span></a></td>
<td class="center">{$refcomp['next_tpp']}</td>
<td class="center">{$refcomp['tpp']}</td>
<td class="center">
MARK1;
if ($refcomp['diff_tpp'] > 0){
    printf('%.2f', $refcomp['diff_tpp']);
} else {
    echo gettext('NONE');
}
?>
</td></tr>

<!-- ################# Execs ################ -->
<?php 
if ($refcomp['next_execs']){

    echo <<<MARK1
    <tr><td class="style24 center">EXECS</td>
    <td class="center">{$refcomp['next_execs']}</td>
    <td class="center">{$refcomp['execs']}</td>
    <td class="center">
MARK1;
    if ($refcomp['diff_execs'] > 0){
        echo $refcomp['diff_execs'];
    } else {
        echo gettext('NONE');
    }
    echo '</td></tr>' . PHP_EOL;
}
?>

<!-- ################# Star Lines SV ################ -->
<?php 
if ($refcomp['next_star_lines_sv']){
    
    echo <<<MARK1
    <tr><td class="style24 center">STAR LINES SV</td>
    <td class="center">{$refcomp['next_star_lines_sv']}</td>
    <td class="center">{$refcomp['star_lines_sv']}</td>
    <td class="center">
MARK1;
    if ($refcomp['diff_star_lines_sv'] > 0){
        echo $refcomp['diff_star_lines_sv'];
    } else {
        echo gettext('NONE');
    }
    echo '</td></tr>' . PHP_EOL;
}
?>

</table>

		<ul>
			<li class="blue_bullet"><span class="royal"><?php
printf(gettext('Go to your <a href="%s" target="_blank">GPS ACTIVITY REPORT</a> to see the activity that has occurred in your business and use the report to help you to meet the qualifications shown above to gain a Pay Level promotion.'), '/cgi/vip/activity_rpt');
?>
    </span></li>
		</ul>
<p style="text-align:right"><a href="#" onclick="hideA(); return false;"><?php echo gettext('Hide Business Analysis'); ?></a></p>
	</div>

	<hr />
</div>
