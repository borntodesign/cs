<?php
require 'Clubshop/Models/LastLineActivity.php';

$LLA = new \Clubshop\Models\LastLineActivity(array('db'=>$Models->dbc(), 'id'=>$Meminfo['id']));
?>
<div id="partnerteamupdate" class="row hide-on-phones"><br />
<h4 class="darkblue bold" align="center"><?=gettext('YOUR PARTNER TEAM UPDATE')?></h4>
<br />
<ul>
<?
$LineCounts = $Models->LineCounts($Meminfo['id']);
$LLU = $LLA->LLU();
if ($LineCounts['v']):?>

<li class="blue_bullet">
    <?php if ($LLU['country_code'])
            printf(gettext('The newest Partner to join your team is <b>%s. %s</b> from <b>%s</b>.'), $LLU['first_initial'], $LLU['lastname'], $LLU['country_code']); ?>

    <?php printf(gettext('You now have <b>%s</b> Partner(s) in your team.'), $LineCounts['v']);?>
</li>

<?php endif;

$LLP = $LLA->LLP();
$rc = $LLA->Refcomp();

if ($LLP['country_code'] || $rc['tpp']):?>

<li class="blue_bullet">
<?    
    if ($LLP['country_code']) {
        printf(gettext('Another member in your team - <b>%s. %s</b> from <b>%s</b> - just made a purchase which has increased your Team Pay Points.'),
            $LLP['first_initial'],
            $LLP['lastname'],
            $LLP['country_code']);
        echo PHP_EOL;
    }

    if ($rc['tpp'])
        printf(gettext('You now have accumulated <b>%s</b> in Team Pay Points that you can earn Team Commissions from.'), $rc['tpp']);
?>
</li>

<?php endif;?>

</ul><hr /></div>