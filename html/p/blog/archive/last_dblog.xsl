<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


  <xsl:template match="/">
    
         <html><head>
            <title>Dick Burke's Weblog</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <script src="//www.clubshop.com/js/panel.js"></script>



<link href="//www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>


<style>
.center {text-align: center;}
ul ul ul {list-style-type: square;}


</style>

</head>



<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" 

height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<div align="center"><h4><span class="topTitle"><xsl:value-of select="//lang_blocks/bb"/></span></h4>
	<p><span class="medblue"><xsl:value-of select="//lang_blocks/bb1"/></span></p></div>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="six columns">





                        <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment></script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div>
<div class="six columns"><a href="archive/dblog.xml" target="_blank">ARCHIVE</a></div>
</div>
<div class="row">
	<div class="twelve columns">

<p class="style28">Monday, July 18, 2016</p>
<p><b>There is nothing more important for the growth and stability of building your organization and maintaining and increasing your income, than keeping your taproots growing with at least one new Partner each month!</b></p>
<p>When the taproot stops growing, so will the Team and Group. Taproot grows = Team and Group grows. Taproot stops growing = Team and Group declines.</p>
<p>To better ensure that this crucial development continues to occur in each taproot, the following change has been made to the <a href="http://www.clubshop.com/vip/manual/training/guide/mdirector/index.html" target="_blank">Marketing Director Training Course:</a></p>
<p>"You will be responsible for ensuring that new Partners are being added to the taproot (bottom of each line) for each Partner that you have personally sponsored.</p>
<p>Two functions need to occur on a regular basis to make the taproot go deeper - new TPs need to be transferred to the taproot and good communications need to be made with each Trial Partner. You may be able to delegate these responsibilities to a Sales Manager in the line, if you see that they are being successful in using TNT to make at least one of their other lines grow. </p>
<p>However, if you see that at least one new Partner has not been added to the taproot each month, then you need to retrain and work with the Sales Manager to help them make the taproot grow or resume being responsible for the taproot growth, until you can see a Sales Manager that has proven they are capable of assuming the role or until you can gain a Marketing Director in the line whom you can delegate the role to."
</p>
<p>I urge each Marketing Director to look at when the last Partner has been added to each of your taproots which you have not been responsible for, to determine if and how you should work with the Sales Manager in the line to retrain them or if you should assume the responsibility for the taproot growth for the line. Communicate, Coordinate and Collaborate! </p>

<!--

<p class="style28">Monday, July 11, 2016</p>
<p>I will taking this week off to travel to visit my wife's sister in Virginia and making some stops along the way, so I won't be responding to any emails or begin working with Coaches until Monday, July 18th. Every once in awhile we have to stop and smell the roses! <img src="face_smile.gif"/>
</p>-->


<!--
<p class="style28">Tuesday, June 14, 2016</p>
<p>We want all Partners to have 100% confidence that they will be able to receive any commissions that they have earned. In order to ensure that this will occur, we have had to modify the Fees Agent Program for Partners in Pay Agent countries, to require all Partners to send either their Pay Agent or the company, any fees which they have collected from the members in their Group. Any funds that are sent to Pay Agents are also required to be sent to the company by the Pay Agent.</p>

<p>This is the only way that we can guarantee that all commissions are paid to each Partner in Pay Agent countries. Why? Because we can not and will not be able to pay commissions on sales made, when we have not received the sales proceeds from which the commissions are to be paid from.</p>

<p>In order to implement this revised policy, we will be disallowing the direct payment of commissions to Partners that have paid for fees from funds in their ClubAccount. Consequently, we will be paying (beginning in August) the same percentage of commission entitlement as the fess we have received directly from Partners or Pay Agents. For example, if we receive 20% of the fees from which commissions have been earned, then we will pay 20% of the commission entitlement via PayPal or via Pay Agent (by sending the Pay Agent these funds to be able to pay commissions in the local currency). If we receive 100% of the fees from which commissions are earned, then we will pay 100% of the entitlement.</p>

<p>In addition, to help Partners in Pay Agent countries to receive past commissions that have created balances in their ClubAccounts, we will also begin in August to allow Partners (in Pay Agent countries only) to receive an additional 10% of their monthly commission entitlement, based on the company receiving the funds from which the commissions are to be paid. For example, if the company receives 100% of the fees that have been paid by a Partner and the members in their Group and the commissions earned equal $100, then the company will pay this Partner $100 via PayPal or via their Pay Agent, plus an additional 10% (or $10) which will be deducted from their ClubAccount balance.</p>

<p>So the harder a Partner works and the more they follow the rules (of having all fees sent to the company), the more they can make and receive directly in their own currency. </p>

<hr/>



<p class="style28">Wednesday, June 1, 2016</p>

<p>Over the last year we have been blessed with having some very reliable people act as Pay Agents on behalf of the Partners in their country. This has enabled us to open up markets in countries where credit cards and PayPal are not yet generally available to the country's residents. This has especially benefited many of our Partners in the African continent.</p>

<p>However, we have also seen many Pay Agents that have collected and kept fees (without sending the funds to the company), not pay commissions to the Partners in their Group. And many of these Pay Agents collected and kept thousands of dollars, then quit before paying out commissions. And in most of these cases, these quitters blamed the company for not paying commissions to defend themselves by falsely discrediting the company.</p>

<p>Fortunately, one of these Pay Agents from Algeria has been found guilty of fraud by an Algerian court as they also acted fraudulently in other matters as well. Nevertheless, the company has been left with a discredited reputation in countries where Pay Agents failed to honorably perform their responsibilities. And I have learned that it takes time and MANY positives to overcome a negative.</p>

<p>Consequently, we have decided to return to our initial Pay Agent policies and rules to require Pay Agents to send the company all of the fees they collect (or pay for), so that the company will regain the responsibility to pay all the commissions due to the Pay Agents and the Partners in Pay Agent countries. This does create a greater expense to transmit the funds internationally, which is the biggest "downside" for re-implementing this policy.</p>

<p>In addition, we will be allowing Pay Agents that have more than $100 from past commission entitlements in their ClubAccounts, to receive an additional 10% of their commissions (but not more than their ClubAccount balance) beginning in August. In turn, we will deduct any additional commissions paid from their ClubAccounts in order to allow Pay Agents to eventually gain all of the commissions credits that have accumulated in their ClubAccounts.</p>

<p>We are implementing these changes effective June 1st for all fees collected and/or paid for by Pay Agents, from which the company will pay the commission disbursements beginning in August.</p>

<p>By making this change, we are providing all Partners and potential Partners in Pay Agent countries with a renewed confidence that they will always be able to receive all of the commissions they have earned!</p>


<hr/>-->








<!--<p class="style28">Monday, February 15, 2016</p>

<p>One of the most important skills that successful salespeople and marketers become proficient at is the ability and tenacity to FOLLOWUP with prospects and customers. </p>
<p>With this in mind, we have been working for the past month on improvements to your Activity Reports and Member Followup Records (MFURs) to provide enhanced tools, information and directions to help all Sponsors, Coaches and Executive Directors, by making the following improvements to the GPS Followup Processes:</p>
<ul>
<li class="blue_bullet">The numbered row at the top of the Activity Report table, now has "mouse over" explanations for the Activity represented for each column shown.</li>
<li class="blue_bullet">When you scroll down the table, this row will continue to appear at the top, so you can readily see what activities have been performed.</li>
<li class="blue_bullet">The entire recommended Followup Process is now shown for most activities for each Sponsor, Coach and Executive Director to be able to more professionally and proficiently perform their communications role, if needed.</li>
<li class="blue_bullet">The emails and links to information or training courses shown for each Step in a process are customized for the Partner that is viewing the MFURs, so they can each perform their role as a Sponsor, Coach and/or Executive Director.</li>
</ul>

<p>Although we are still making some tweaks and fixes to some things that aren't working as designed, based on feedback we have gained from some Partners that have BETA tested the new versions of the report and records, these new versions with improvements made to them are now "live", with some exceptions: </p>
<ul>
<li class="blue_bullet">Existing activities will no longer have what they previously had for followup features. In other words, previously there was followup required for a new Partner, but it was a one item deal. That one item deal no longer exists and without the sub-activities being in place for prior activity, there are no followup items. </li>
<li class="blue_bullet">In some cases, some of these Followup Steps showing are unwarranted. So you can check these off if the followup has already been done for these steps. </li>
<li class="blue_bullet">Followup Steps have been created for the new Trial Partners, as of Thursday, the 11th. </li>
<li class="blue_bullet">Starting today, all new activities coming into the system will trigger the corresponding Followup Processes to be shown, for those activities we have created them for.  </li>
</ul>

<p>We plan to make further improvements to the Activity Reports and MFURs in February, to include:</p>
<ul>
<li class="blue_bullet">Creating the remaining Followup Processes for all the activities we report.</li>
<li class="blue_bullet">Providing Tutorials for both the Activity Reports and the MFURs.</li>
<li class="blue_bullet">Providing new A/V presentations and tutorials for the training lessons we provide links to in the Followup Process Steps. </li>

</ul>

<p>Does the ClubShop Partner Business Opportunity and our GPS keep getting better and better? You can bet on it! <img src="face_wink"/></p>


<hr/>
<p class="style28">Wednesday, January 27, 2016</p>

<p>Today I am a happy ClubShop Rewards shopper!</p>
<p>We recently had our kitchen wall clock stop working, so we had a need to replace it. So we began looking for a large, oak wall clock, without much luck.</p>
<p>This morning in my email inbox, I received my monthly ClubShop Rewards special reminder email, which I clicked on to see the special offers being provided by stores in our U.S. ClubShop Mall which match the items of interest I selected in my <a href="http://www.clubshop.com/mall/other/questionnaire.php" target="_blank">Customer Preference Survey</a>. One of the stores (Bellacor) was offering a 15% discount, so I clicked on the listing to discover that they had a genuine oak, 16 inch wall clock that I thought my wife Pat would really like. So I showed her the clock and she smiled in agreement told me to order it.</p>
<p>It was already affordably priced and the shipping cost was about the same as the 15% discount and we had no need to pay 7% FL sales tax. And as a ClubShop Rewards member (yes, I am a member too!) I will also receive 4% in ClubCash and 2% in personal commissions. When you add it all up, it is a heck of a deal and I didn't have to trounce around town or the not so local shopping mall to find something similar to it!</p>
<p>And my wife is happy too! </p>
<p>Happy Wife = Happy Life! <img src="face_smile.gif"/></p>



<hr/>

<p class="style28">Friday, January 8, 2016</p>


<p>I just received an email from Patrick Mudenyo (caretaker of the Neema Children's Home in Kenya, Africa) asking for help in 

providing the school fees for a number of high school age orphans that need to be paid in order that they can attend school. 

Here is an edited, excerpt from his email, <br/><br/>

<i>"Dear Richard and Pat, new year greetings from Neema . I write to thank you very much for your love and support that 

DHSClubKids has shown for our children at Neema. I remember when you were able to support the children with school fees and 

many have been able to complete high school as a result of your support. We are all doing well here but the only thing that is 

disturbing is school fees for the children here. This is the first week of school here and the children are not in school 

because of the school fees. We are praying that the Lord may open doors for you this year 2016. Please pray for us. God bless 

you, we love you. Pastor Patrick" </i></p>


<p>Certainly, DHSClubKids helping to provide food and shelter for these orphans has been a blessing to them. But they also 

need to gain an education to be able to gain a high school degree so that they can make a successful transition from living in 

the orphanage to be able to work and live on their own as adults. </p>
<p>In order to attend public high school in Kenya, school fees are required to be paid by/for each student in high school. So 

I am hoping that DHSClubKids can help (as we have in the past) provide for this need for the teenagers that have grown up in 

Neema.</p>
<p>All funds that are collected by DHSClubKids go directly to the orphanages we support and the DHS Club matches all funds 

that are sent to DHSClubKids and we are only able to accept and match donations made by credit card or Pay Pal <b>(not by 

using funds in your ClubAccount)</b>. </p>
<p>Jesus said and I believe that if we "Give, and it will be given to you. A good measure, pressed down, shaken together and 

running over, will be poured into your lap. For with the measure you use, it will be measured to you."! </p>
<p>To learn more about DHSClubKids and how you can help provide support to these orphans so we can help them pay for their 

school fees, go to <a href="http://www.dhsclubkids.org/" target="_blank">http://www.dhsclubkids.org/</a></p>
-->






    
            
            
            
            
            
            
            
    
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
           	<br/><br/><br/>	<br/><br/><br/>	<br/>
           <hr/>
           
           
            
            
            
            
                  
                  
            

 </div>        			<br/><br/><br/>
</div>
</div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row"> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2016
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>
  

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
