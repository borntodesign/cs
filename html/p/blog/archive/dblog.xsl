<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


  <xsl:template match="/">
    
         <html><head>
            <title>Dick Burke's Weblog</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <script src="//www.clubshop.com/js/panel.js"></script>



<link href="//www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>


<style>
.center {text-align: center;}
ul ul ul {list-style-type: square;}


</style>

</head>



<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" 

height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<div align="center"><h4><span class="topTitle"><xsl:value-of select="//lang_blocks/bb"/></span></h4>
	<p><span class="medblue"><xsl:value-of select="//lang_blocks/bb1"/></span></p></div>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">





                        <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment></script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>






<p class="style28">Monday, February 15, 2016</p>

<p>One of the most important skills that successful salespeople and marketers become proficient at is the ability and tenacity to FOLLOWUP with prospects and customers. </p>
<p>With this in mind, we have been working for the past month on improvements to your Activity Reports and Member Followup Records (MFURs) to provide enhanced tools, information and directions to help all Sponsors, Coaches and Executive Directors, by making the following improvements to the GPS Followup Processes:</p>
<ul>
<li class="blue_bullet">The numbered row at the top of the Activity Report table, now has "mouse over" explanations for the Activity represented for each column shown.</li>
<li class="blue_bullet">When you scroll down the table, this row will continue to appear at the top, so you can readily see what activities have been performed.</li>
<li class="blue_bullet">The entire recommended Followup Process is now shown for most activities for each Sponsor, Coach and Executive Director to be able to more professionally and proficiently perform their communications role, if needed.</li>
<li class="blue_bullet">The emails and links to information or training courses shown for each Step in a process are customized for the Partner that is viewing the MFURs, so they can each perform their role as a Sponsor, Coach and/or Executive Director.</li>
</ul>

<p>Although we are still making some tweaks and fixes to some things that aren't working as designed, based on feedback we have gained from some Partners that have BETA tested the new versions of the report and records, these new versions with improvements made to them are now "live", with some exceptions: </p>
<ul>
<li class="blue_bullet">Existing activities will no longer have what they previously had for followup features. In other words, previously there was followup required for a new Partner, but it was a one item deal. That one item deal no longer exists and without the sub-activities being in place for prior activity, there are no followup items. </li>
<li class="blue_bullet">In some cases, some of these Followup Steps showing are unwarranted. So you can check these off if the followup has already been done for these steps. </li>
<li class="blue_bullet">Followup Steps have been created for the new Trial Partners, as of Thursday, the 11th. </li>
<li class="blue_bullet">Starting today, all new activities coming into the system will trigger the corresponding Followup Processes to be shown, for those activities we have created them for.  </li>
</ul>

<p>We plan to make further improvements to the Activity Reports and MFURs in February, to include:</p>
<ul>
<li class="blue_bullet">Creating the remaining Followup Processes for all the activities we report.</li>
<li class="blue_bullet">Providing Tutorials for both the Activity Reports and the MFURs.</li>
<li class="blue_bullet">Providing new A/V presentations and tutorials for the training lessons we provide links to in the Followup Process Steps. </li>

</ul>

<p>Does the ClubShop Partner Business Opportunity and our GPS keep getting better and better? You can bet on it! <img src="face_wink"/></p>


<hr/>
<p class="style28">Wednesday, January 27, 2016</p>

<p>Today I am a happy ClubShop Rewards shopper!</p>
<p>We recently had our kitchen wall clock stop working, so we had a need to replace it. So we began looking for a large, oak wall clock, without much luck.</p>
<p>This morning in my email inbox, I received my monthly ClubShop Rewards special reminder email, which I clicked on to see the special offers being provided by stores in our U.S. ClubShop Mall which match the items of interest I selected in my <a href="http://www.clubshop.com/mall/other/questionnaire.php" target="_blank">Customer Preference Survey</a>. One of the stores (Bellacor) was offering a 15% discount, so I clicked on the listing to discover that they had a genuine oak, 16 inch wall clock that I thought my wife Pat would really like. So I showed her the clock and she smiled in agreement told me to order it.</p>
<p>It was already affordably priced and the shipping cost was about the same as the 15% discount and we had no need to pay 7% FL sales tax. And as a ClubShop Rewards member (yes, I am a member too!) I will also receive 4% in ClubCash and 2% in personal commissions. When you add it all up, it is a heck of a deal and I didn't have to trounce around town or the not so local shopping mall to find something similar to it!</p>
<p>And my wife is happy too! </p>
<p>Happy Wife = Happy Life! <img src="face_smile.gif"/></p>



<hr/>





<hr/>
<p class="style28">Friday, January 8, 2016</p>


<p>I just received an email from Patrick Mudenyo (caretaker of the Neema Children's Home in Kenya, Africa) asking for help in 

providing the school fees for a number of high school age orphans that need to be paid in order that they can attend school. 

Here is an edited, excerpt from his email, <br/><br/>

<i>"Dear Richard and Pat, new year greetings from Neema . I write to thank you very much for your love and support that 

DHSClubKids has shown for our children at Neema. I remember when you were able to support the children with school fees and 

many have been able to complete high school as a result of your support. We are all doing well here but the only thing that is 

disturbing is school fees for the children here. This is the first week of school here and the children are not in school 

because of the school fees. We are praying that the Lord may open doors for you this year 2016. Please pray for us. God bless 

you, we love you. Pastor Patrick" </i></p>


<p>Certainly, DHSClubKids helping to provide food and shelter for these orphans has been a blessing to them. But they also 

need to gain an education to be able to gain a high school degree so that they can make a successful transition from living in 

the orphanage to be able to work and live on their own as adults. </p>
<p>In order to attend public high school in Kenya, school fees are required to be paid by/for each student in high school. So 

I am hoping that DHSClubKids can help (as we have in the past) provide for this need for the teenagers that have grown up in 

Neema.</p>
<p>All funds that are collected by DHSClubKids go directly to the orphanages we support and the DHS Club matches all funds 

that are sent to DHSClubKids and we are only able to accept and match donations made by credit card or Pay Pal <b>(not by 

using funds in your ClubAccount)</b>. </p>
<p>Jesus said and I believe that if we "Give, and it will be given to you. A good measure, pressed down, shaken together and 

running over, will be poured into your lap. For with the measure you use, it will be measured to you."! </p>
<p>To learn more about DHSClubKids and how you can help provide support to these orphans so we can help them pay for their 

school fees, go to <a href="http://www.dhsclubkids.org/" target="_blank">http://www.dhsclubkids.org/</a></p>



<!--
<p class="style28">Monday, November 23, 2015</p>


<p>Many years ago, right after I was married to my wife Pat, we used to go to "auctions" on weekends to try and find bargains 

tp purchase various things for our new household. At one of these auctions I paid $1.00 for a box of old books, because the 

box contained some books (Letters of our first Presidents) that had been printed by the Library of Congress. One of the books 

from the set was missing, so the the value for having a complete set was diminished.</p>
<p>The first book in the set was composed of the various letters that our first president George Washington sent to Congress, 

ambassadors or other public servants. In was in this book that I discovered the truth to what many things in our Constitution 

represent, like why the federal government was/is only authorized to mint coins and not print "fiat" money and what represents 

a "treaty" between our country and other countries (hint on the latter - foreign trade agreements like NAFTA and GATT and the 

current one being negotiated should all be considered treaties which need the Senate's "advice and consent" by a 2/3 vote to 

be ratified).  </p>
<p>Another truth that I discovered as I read through these letters, was why and how our Thanksgiving holiday was first 

created. I learned in school that we were replicating the first feast shared between the local, native Americans and the 

Pilgrims (first colonists that settled in the new world from England), although that meal was most likely shared earlier than 

late November.  </p>
<p>So how did we get a national holiday on the 4th Thursday of November? Well I discovered in this book that our newly formed 

Congress asked President Washington to declare a national day of public thanksgiving and prayer, which was how our national 

Thanksgiving holiday was really created. Here is a copy of this declaration:</p>
<p><i>Whereas it is the duty of all nations to acknowledge the providence of Almighty God, to obey His will, to be grateful 

for His benefits, and humbly to implore His protection and favor; and Whereas both Houses of Congress have, by their joint 

committee, requested me "to recommend to the people of the United States a day of public thanksgiving and prayer, to be 

observed by acknowledging with grateful hearts the many and signal favors of Almighty God, especially by affording them an 

opportunity peaceably to establish a form of government for their safety and happiness:"</i></p>
<p><i>Now, therefore, I do recommend and assign Thursday, the 26th day of November next, to be devoted by the people of these 

States to the service of that great and glorious Being who is the beneficent author of all the good that was, that is, or that 

will be; that we may then all unite in rendering unto Him our sincere and humble thanks for His kind care and protection of 

the people of this country previous to their becoming a nation; for the signal and manifold mercies and the favorable 

interpositions of His providence in the course and conclusion of this late war; for the great degree of tranquility, union, 

and plenty which we have since enjoyed; for the peaceable and rational manner in which we have been enabled to establish 

constitutions of government for our safety and happiness, and particularly the national one now lately instituted; for the 

civil and religious liberty with which we are blessed, and the means we have of acquiring and diffusing useful knowledge; and, 

in general, for all the great and various favors which He has been pleased to confer upon us.</i></p>
<p><i>And also that we may then unite in most humbly offering our prayers and supplications to the great Lord and Ruler of 

Nations, and beseech Him to pardon our national and other transgressions; to enable us all, whether in public or private 

stations, to perform our several and relative duties properly and punctually; to render our National Government a blessing to 

all people by constantly being a Government of wise, just , and constitutional laws, discreetly and faithfully executed and 

obeyed; to protect and guide all sovereigns and nations (especially such as have shown kindness to us), and to bless them with 

good governments, peace, and concord; to promote the knowledge and practice of true religion and virtue, and the increase of 

science among them and us; and, generally, to grant unto all mankind such a degree of temporal prosperity as He alone knows to 

be best.</i></p>
<p><i>Given under my hand, at the city of New York, the 3d day of October, A.D. 1789.</i></p>
<p><i>George Washington</i></p>
<p>And as the late Paul Harvey used to say, "Now you know the rest of the story". :)</p>
<p>Happy Thanksgiving!</p>


<hr/>
<p class="style28">Thursday, October 1, 2015</p>
<p>Except for overcoming the Spam Complaints problems we suffered back in 2001, expanding our ClubShop consumer and business 

opportunities into many northern African and Middle Eastern countries has been the biggest business challenge I have ever 

faced. And it continues to provide a challenge to me, our staff and to entrepreneurial Partners that are seeking to gain the 

benefits of owning their own glocal (global and local) business and accessing the free enterprise system.
</p>
<p>Certainly, there are many issues to deal with and problems to overcome when we try to do business in countries other than 

our own, with currency exchange and movement being the biggest hurdle we all face. In countries where their governments allow 

for a freer trade and exchange of currencies (like the United States and European Union countries as an example), this can be 

a problem too. However many/most Americans and Europeans have either credit cards or debit cards that can be used to do 

business on the Internet. And with the advancement of the Internet, other financial intermediary companies like PayPal allow 

for the free exchange of currency.
</p>
<p>But in countries like Tunisia, Morocco, Algeria, Nigeria, India, Pakistan and Cuba (to name a few), PayPal is either not 

allowed by the respective country's governments to do business or PayPal has decided that the fraud or AML (Anti Money 

Laundering) risks are too great to allow them to do business in certain countries. For example, PayPal once did business in 

Nigeria, but after having to deal with so many fraud issues, they decided to stop doing business in Nigeria. </p>
<p>Consequently and unfortunately, the many good, hard working and honest Nigerian people (like our Nigerian ClubShop 

Partners) are hurt by the minority of bad, dishonest and greedy Nigerians, as the country continues to struggle with 

implementing, policing and enforcing laws to prevent such criminal behavior. </p>
<p>Incidentally, the DHS Club suffered an individual financial loss of $14,000 many years ago, when a check was recreated on a 

computer by a Nigerian, then used to make a large purchase from an Asian business, before finding its way back to our bank 

account. At that time, we had the ability to check our bank account on a daily basis for any unregistered transactions, so we 

could deny them, but our Accounting Manager failed to look at this on the day this check cleared our bank. Needless to say, he 

was fired for the next day! And our insurance company would not honor our claim, as the theft took place outside of the U.S. 

Needless to say, we also changed insurance companies!  :)</p>
<p>So in order to support our good Nigerian Partners and to continue on our journey, we chose to continue to do business in 

Nigeria while making some modifications to gain greater bank account security and to stop sending paper checks 

internationally. And we began to put together a plan to create a "Pay Agent" program to enable entrepreneurs in countries like 

Nigeria to be able to become ClubShop Partners and build their own glocal business. </p>
<p>As I have said many times in my 18 year career with this business, it is not easy to be a trailblazer! There is no path to 

follow - only one to create as we change or reverse our course many times, or as we need to build bridges or tunnels to 

overcome the obstacles we will face. </p>
<p>So in addition to our Pay Agent program (which is still evolving), we have been working to be able to offer members in 

countries where PayPal is not available, the ability to gain a physical, pre-paid MasterCard which could be used to disburse 

commissions to, pay for fees and orders and even used to shop online and offline and to get cash from a local bank or ATM. And 

over the past several months we have been negotiating with a company in the U.S. named Payoneer to be able to issue and fund 

their Payoneer MasterCard. </p>
<p>Last month we completed our negotiations (which included gaining a picture ID for each international cardholder), agreed to 

terms and a contract with Payoneer, which was signed by its CEO Scott Galit and myself to "seal" the deal. We were given 

credentials to log in to our new portal page that we were going to test out last week before publishing it at our website this 

week.</p>
<p>And then it happened - I received a call from our account manager wherein she told me that Payoneer's banking partners 

decided that their risk of violating AML (Anti Money Laundering) laws was too great to allow our company to issue and fund 

cards on their behalf, due to the clientele we were planning on making the cards available to. Although they weren't specific 

in identifying which clientele they were referring to, I think that it is obvious that they were referring to our members that 

live in countries where terrorism is occurring or may be supported from. </p>
<p>Since we do have a signed contract, I could seek an injunction from the courts to enforce our agreement, as we did nothing 

to violate the agreement. But gaining a resolution using the legal system would take years, be very expensive and even if the 

court decided in our favor, Payoneer or their banking Partners would undoubtedly find another way not to do business with us 

and our international members. And after being in business for 18 years, our company has never been involved with a lawsuit, 

either as a defendant or as a complainant and I would like to keep it that way.</p>
<p>Although many Partners that live in Pay Agent countries may see this as a setback, I see this as just another obstacle to 

overcome in our journey to become the largest consumer organization (buyer's club) in the world. So we will seek other means 

and methods to enable our international Partners to be able to succeed in their own ClubShop business. </p>
<p>Success in business and in life doesn't come to the pessimistic thinker, or the fainthearted, or to quitters, or to the 

fearful. It is only earned by the optimistic, ambitious, determined, courageous and persistent! </p>
<p>And so our journey continues! </p>






<hr/>-->


<!--<p class="style28">Wednesday, September 23, 2015</p>

<p>Benefit 7 of our International Partner Compensation Plan is that the "Income Reports and Team Commissions Reports provide 

'live' reporting of current (month-to-date) entitlements."</p>
<p>What makes this such an important benefit? Because Partners need to be able to see exactly how the compensation plan 

entitlements translate into their own, personal entitlement and our corresponding Income Reports and Team Commissions Reports 

show exactly what the current (month-to-date) entitlements are, based on the current qualifications being met and the current 

Share Values that are being earned. This enables Partners to see what their current entitlement is without having to do 

calculations.</p>

<p>In addition to reporting what the current entitlements are, the Income Reports also provide a "Promotion Analysis" which 

show exactly what qualifications need to achieve the next pay level from what the current pay level met. Consequently, 

Partners are able to see exactly what they are entitled to and exactly what you need to do to get a promotion and increase in 

your income. </p>
<p>Just as I stated in my previous blog entry, "...when people don't know what they need to do, they usually do nothing", the 

opposite of this is also a true -  when people know what they need to do, they are empowered to create goals and more 

motivated to reach their goals. In other words, knowledge = power.  By having a "Promotion Analysis" simplistically shown in 

such clarity is an important benefit, because it "builds in" the motivation to gain a promotion! </p>

<p>There are millions of people in the world that are looking for a way to make more money and have their own business and the 

ClubShop Partner Business Opportunity provides such an opportunity for about the same cost of a good pair of shoes or dinner 

at a nice restaurant. Your success as a Sponsor and Coach will be based on your knowledge of and enthusiasm for our 

tremendously beneficial International Partner Compensation Plan. </p>

<p>The more you know, the more you'll grow! </p>


<hr/>


<p class="style28">Friday, September 18, 2015</p>

<p>In my two previous blog posts, I shared my thoughts with you on the 10th and 9th benefits for our International Partner 

Compensation Plan which are shown at <a href="http://www.clubshop.com/manual/compensation/compplan_overview.html" 

target="_blank">http://www.clubshop.com/manual/compensation/compplan_overview.html</a>
</p>
<p>Benefit # 8 states, "It is simple to see and understand how much money you can make and commission entitlements are shown 

in each Partner's currency."
</p>
<p>Although this would seem like a benefit that all network marketing compensation plans should provide, most of them do not. 

Actually, one of the most difficult things to understand in network marketing compensation plans is how much money you will 

make based on the pay levels you may achieve! Usually there are a number of calculations that have to be done to determine 

this and rarely does the pay level you may achieve, actually determine your resulting income. </p>
<p>I have learned and observed over the years that when people don't know what they need to do, they usually do nothing!  

Therefore, having a simple and understandable plan to see how much money you can make is a very important benefit that our 

compensation plan delivers. Furthermore, it is also very important for people to see what their income will be in their own 

currency, which our International Partner Compensation Plan provides as well.</p>
<p>Creating this capability in our plan was the biggest driver (motivation) for me as we have developed our plan over the 

years. And this has been the most difficult benefit to provide, especially considering the different sales values that are 

gained in different countries. Yet we persisted in modifying our plan with this number one goal in mind - to make it simple to 

see and understand how much money you can make and commission entitlements are shown in each Partner's currency.</p>

<p>Now after 18 years of research, testing and development, you can be proud to offer our business opportunity and 

compensation plan to any person in the world that is looking to own their own home based business, because they will also be 

able to see how much they can make in their own currency as they build their business.
</p>

<hr/>

<p class="style28">Wednesday, September 9, 2015</p>

<p>Here is "More Information" about benefit number nine for our new International Partner Compensation Plan - "Plan adjusts 

automatically to pay Partners based on the amount of sales made."</p>
<p>Having a compensation plan that can be used in all countries creates a real challenge for the company that provides the 

plan, due to:</p>
<ul>
<li class="blue_bullet">Having varying costs for the products and services sold.</li>
<li class="blue_bullet">Different ranges of average incomes for different countries.</li>
<li class="blue_bullet">Continuous monthly adjustments for different currencies. </li>
<li class="blue_bullet">Varying economic and political situations in different countries.</li>
</ul>

<p>Yet our International Partner Compensation Plan has a "built in" feature which allows our plan to make automatic 

adjustments in the commissions earned for sales generated from different countries, which allow for all of the above factors 

to be considered. </p>
<p>This creates a stable plan which accounts for differing sales values to enable all Partners to be free to build an 

international sales organization! And this creates a truly unique and potentially tremendous opportunity for many Partners 

that may live in economically depressed and/or politically repressed countries. </p>
<p>In addition, this feature also allows Partners in each country to determine what the best costs and corresponding 

commission values may be best suited for their country. </p>
<p>This month we will start our first Partner Participation Survey to enable Partners in Pay Agent countries to vote to 

determine if they want the GPS subscription fees and corresponding commission values to be increased, remain the same or 

decreased. </p>
<p>The reason we are starting to allow voting to determine this in Pay Agent countries first, is because many/most Pay Agents 

have had to deal with the repercussions of our previous plan not having this built in feature, as commissions were based on 

the number of Partners (PRP and GRP qualifiers) and not on the Pay Points or the amount of the sales made. Consequently, 

despite our trying to control the over payment of commissions, the commissions earned by Partners in Pay Agent countries were 

greater than the fees/revenue collected by the company and Pay Agents.</p>
<p>Initially, this created a kind of bonanza (windfall, treasure trove) for Partners (and especially Executive Directors) in 

Pay Agent countries, however since the responsibilities of collection of fees and payment of commissions have been delegated 

to Pay Agents and not been performed by the company, many Pay Agents found in time that they have had a burden placed on them 

due to having to pay out more in commissions than funds they received. </p>
<p>Since the company always has and always will ensure that all commissions earned are able to be received, each Pay Agent has 

received "commissions credits" in their ClubAccounts which they can use to pay for fees they collect and keep, to eventually 

receive the commissions they have been credited with, despite these commissions being greater than they should have been. </p>
<p>As a result, the company may not be able to gain much net revenue from Pay Agent countries sales, until these Pay Agent's 

commission credits have been paid down. Yet since our new plan now has this built in feature, we have provided a solution for 

this problem (which will take a little time to work itself out) and which will also become a permanent solution. </p>
<p>Like everything else I have encountered in my life and in this business,  when we seek to be trail blazers we will always 

have new obstacles and problems to overcome to continue our journey and to create the best trail to the top of the mountain we 

are climbing. But if we persist, we will find the solutions and create not just an established and proven trail for others to 

follow, but a super highway for Partners to use. And as we continue to improve our GPS, we also improve our vehicle to drive 

up the mountain on the super highway we have created!</p>
<p>Here are some of my favorite quotes on the subject of "Persistence" that I have used and applied to motivate me to never, 

never, never give up! </p>
<p>"Quitters never win and winners never quit!"</p>
<p>"Nothing in this world can take the place of persistence. Talent will not: nothing is more common than unsuccessful men 

with talent. Genius will not; unrewarded genius is almost a proverb. Education will not: the world is full of educated 

derelicts. Persistence and determination alone are omnipotent." - Calvin Coolidge</p>
<p>"Success is almost totally dependent upon drive and persistence. The extra energy required to make another effort or try 

another approach is the secret of winning." - Denis Waitley</p>-->




<hr/>
<p class="style28">Friday, September, 4 2015</p>
<p>In my last blog entry (shown below), I created a "Top Ten List" of benefits that our new International Partner Compensation Plan provides. We have now included this list in our Compensation Plan Overview page so that it can be seen by Trial Partners when they study how our Compensation Plan works - <br/><a href="http://www.clubshop.com/manual/compensation/compplan_overview.html" target="_blank">http://www.clubshop.com/manual/compensation/compplan_overview.html</a></p>


<p>
In addition to providing this Top Ten List of benefits, I will be adding a (More Information) script for each benefit. Here is the (More Information) script for benefit number ten:</p>
<p>10. The qualifications to reach the various pay levels will remain in effect for the future with no monthly changes in qualifiers:</p>

<p>It is very important for the qualifications required to earn income for the various pay levels in a Compensation Plan, have two features "built in" to the plan:</p>

<ol>
<li>Qualifications to receive a promotion need to gradual, incremental increases to gain promotions to new pay levels.<br/><br/>

The Partners that are willing to work hard to make sales and act as a good Coach for the Partners in their Group, must not have to work for many months before being able to gain a promotion. Ideally, new promotions are able to be gained each month for some Partners, which is how our Compensation Plan works.<br/><br/>
This enables our most successful Partners to be able to qualify as Executive Directors in less than a year and as Ambassador's Club members in a second year.</li>

    <li>Qualifications should not need to be increased from month to month.<br/><br/>

    When qualifications change, it feels like the goal posts are moved further back. Although there may be factors which contribute towards the need to change qualifications (inflation, price increases, currency devaluations), a plan needs to be able to make adjustments for the commissions to be paid, not to the qualifications.
<br/><br/>

    Therefore, the qualifications needed to gain a pay level promotion do not need to be adjusted from month. Rather an annual review is performed to ensure that the qualifications set forth will continue to enable Partners to earn regular promotions.</li>
</ol>

<p>This feature of our International Partner Compensation Plan enables Partners to set goals with the confidence that they will be rewarded when they achieve their goal. And this provides Partners with a great deal of security and stability for what the future can hold.</p>


<hr/>

<p>As a Partner that is recruiting members that have requested to be Trial Partners, you can feel proud and confident that you are offering your prospects access to the best business opportunity in the world, which has a time test and most rewarding Compensation Plan! So do so with enthusiasm and belief! (Note that the last four letters in enthusiasm are I-A-S-M, which is really an acronym for "I Am Sold Myself")</p>
<p>The more you know, the more you'll grow. The greater your belief, the greater your enthusiasm. </p>
<p>And ENTHUSIASM is the magnet that attracts and draws people to be involved and active in any enterprise or organization!</p>

<p><img src="knowledgeispower_en.png"/></p>

<!--

<hr/>

<p class="style28">Thursday, August 27, 2015</p>
<p class="style24">My "Top Ten List" of benefits for our International Partner Compensation Plan:</p>
<ul>
<li>10. Plan will remain in effect for the future with no monthly changes in qualifiers! </li>
<li>9. Plan adjusts automatically to pay Partners based on the amount of sales made!</li>
<li>8. Simple to understand, "When I do this, I earn this" and commission entitlements are shown in each Partner's currency!</li>
<li>7. Income Reports and new Team Commissions Reports provide "live" reporting of current (month-to-date) entitlements!</li>
<li>6. All Partners are able to share in Team Commissions!</li>
<li>5. Minimum Commission Guarantee -  sponsor two and your fees are through!</li>
<li>4. New pay level promotions can be earned monthly, enabling a Partner to reach Executive Director in ten months or less!</li>
<li>3. There are no "break aways of sales volume" - Executive Directors have the ability to earn override commissions (Star Bonuses) on an unlimited number of Executive Directors in depth!</li>
<li>2. Partners are able to earn 85% in commissions of the revenue the company has received, which is the highest and best payout percentage in the network marketing and direct sales industries!</li>
<li>1. This is a time tested plan, built to last and endure for years to come!</li>

</ul>


<hr/>
<p class="style28">Wednesday, August 26, 2015</p>
<p>If you have looked at the preview version of our new, International Partner Compensation Plan, you are able to see the commissions that can be earned for each pay level (beginning in September) are based on the average GPS Pay Points for Partners in each country and are therefore different for each country. And we are providing what these commissions are in each country's currency. This is why we are calling this final version of our compensation plan, our "international" plan. </p>
<p>Each Partner's actual entitlement will not be based on which country they are from, but rather will be determined by the average GPS PP per Partner in each Partner's Group. If all the Partners in your Group are from the same country, then your commission entitlement will be very close to the commission entitlement shown in the plan for your country. However, if you have Partners from different countries (which have different GPS costs and Pay Point values) then your actual entitlement will be based on the average GPS PP per Partner in YOUR Group. </p>
<p>This new feature for our plan is the missing piece of the puzzle that we have needed to have in place to safely and securely grow our business internationally by enabling the company to keep our commission entitlements within the 85% target we have set and budgeted ourselves for. In our current plan, a Partner that had an average of $15.00 per GPS fee paid by the Partners in their Group, has made as much as a Partner that had an average of $30.00 per GPS fee paid by the Partners in their Group. Certainly, basing commission entitlement of the number of sales made versus the amount of sales made, is neither fair or sustainable.</p>
<p>Our new, international plan will now automatically adjust commission entitlement for each pay level, based on the amounts of the fees paid by the Partners in each Group, while keeping the various qualifications for each pay level (PRP, GRP, TPP) the same from month to month. Groups that have more new subscribers in them will have higher average GPS PP values (and commissions), based on the higher amount paid for initial fees. And Groups that have Partners that pay higher amounts in fees (like the US and EU), will also have higher GPS PP values (and commissions) than Groups that have Partners that may pay lower amounts in fees.</p>
<p>So some Partners may see an increase in their commissions and other Partners may see a decrease in their commissions. </p>
<p>And we won't know what anyone's commissions will be for September, until the sales are made in September which will provide the average GPS PP values which will determine your commissions. However, we will be providing you with a new, "live" Team Commissions report next week so that you can see what your average GPS PP are for YOUR Group, which will determine what your commissions will be. This new report will be able to be seen in conjunction with your Income Reports so that you can track your progress throughout each month and so you can see what your potential commissions may be should you achieve a higher pay level than the current pay level.</p>
<p>Although our work to create and develop the "best compensation plan ever" is completed, our work to communicate how and why this plan is so great, still needs to be done. Starting tomorrow, I plan to write a series of "in depth" articles as blog entries, to explain the many benefits of our unique, hybrid plan, which we will keep posted as links from the Compensation Plan for future viewing by Trial Partners and Partners. </p>
<p>Then by October, we plan to release new Partners Business Opportunity Presentations which will explain how our fantastic plan works and how TNT works, to help Trial Partners see the complete picture, which will encourage and motivate a higher percentage of them to become Partners. </p>
<p>FUNTASTIC! </p>

<hr/>
<p class="style28">Friday, August 21, 2015</p>

<p>Yesterday was a good day for me - a very, good day!</p>
<p>After 18 years of development, testing, trials and errors, I was able to complete the final testing and "number crunching" for our fantastic, new and improved ClubShop Partner International Compensation Plan. And for the first time in 18 years, I am confident that this will be our plan for many years to come. </p>
<p>Although it resembles our current plan in many ways, it will now have built in to it automatic adjustments for commission values based on the amount of the sales made for each Partner Group. If you were able to "look under the hood" to see how this plan works and performs its calculations, you would be confused for sure, as all of the underpinnings are very complex and have been thoroughly tested. </p>
<p>This is what allows us to provide a very simple plan to understand. In fact, our plan represents a break through in the industry, because there is no other network marketing plan like it in the world! And no other plan pays up to 85% in commissions from the revenue generated! </p>
<p>You will now have (without a doubt) the absolute best compensation plan in the industry and in the world, that you can rely on to create and maintain a long term, residual income. </p>
<p>The journey to arrive at this final destination has not been an easy one for me, or for the company or for the Partners that have joined us in our journey. More than a few times, different models that we created were found to have weaknesses or defects that more than offset the strengths and benefits. As we have discovered these, we have had to make changes to the plan. And most people really dislike changes! </p>
<p>I have always been aware that we could have settled for a plan using an existing model, like the tried and proven "stair step, break away" model that companies like Amway have used for the past 50 years. Or we could have given in to the temptation to use a binary or trinary plan which can provide an initial jolt of excitement to be followed within a few years by most companies going out of business. We could have used a uni-level plan too. </p>
<p>And I have used elements of all of these types of plan, combined with some creative ingenuity to finally develop the plan we will be releasing in September.
</p>
<p>Next week, I plan to write an article each day to highlight one of the five aspects that makes this plan a truly unique hybrid plan and what makes it so special. Then in September, I want to shine a spotlight on our new plan to help us retain and attract new Partners and new leaders as we continue to forge our global path forward.
</p>
<p>Today I want to use this opportunity to say thank you to my staff and all of the Partners that have joined me on our journey together and especially to the Partners that have kept the faith in me and in our opportunity, even while we dealt with the problems we have had to face to overcome a weakness in our plan and while we were forced to modify or change our plan. THANK YOU! We couldn't have arrived here without your support and enduring faith and belief. </p>
-->


<!--

<p class="style28">Tuesday, August 4, 2015</p>

<p>I will be writing a new "View From The Top" article this week to "kick off" a major communications campaign which I will be leading this month, to share my perspective on a number of issues related to our international expansion of ClubShop Rewards. </p>
<p>Over the past year we have found ourselves pioneering (again!) to expand ClubShop into many countries that have poorer economies, trade and travel restrictions and limited freedoms. And what we have learned (largely by trial and error) is that we cannot allow the same Compensation Plan, policies and procedures to exist for all Partners in all countries. "One size fits all" is definitely not the case when it comes to doing business internationally!
</p>
<p>Different languages, currencies, laws, cultures and religions require us to creatively reshape and refashion just about everything we do, to enable all Partners to able to build a successful ClubShop business. As a result, as pioneers we have many obstacles which need to be overcome and many decisions which need to be made to correct mistakes and errors in judgment, in order to create the best path forward. </p>
<p>One of the programs we have put in place in order to enable people in many countries to become Partners is the Pay Agent Program. This program started out with a simple need to be filled and with a simple premise. First, we needed to be able to delegate the ability to receive and pay money in countries that do not have PayPal. Second, once we had a Partner that had the willingness and the ability to send and receive funds from outside their country, we enabled them to act as a "Pay Agent". As a Pay Agent, they have been able to collect fees and sales proceeds and send these funds to us and also be able to receive funds from us, so they could receive and pay commissions to Partners in their country.</p>
<p>This program as originally designed worked well in a few countries, but as we started to expand into countries without Pay Agents, we have had the need to modify the Pay Agent program to enable more Partners to become Pay Agents to be able to allow for greater and freer expansion of each Partner's international ClubShop business. </p>
<p>In order to administer this program, we have had to create various rules and policies to protect the company and all Partners from Pay Agent fraud, abuse and failure to perform their Pay Agent role in a prompt and professional manner. Although acting as a Pay Agent provides a privilege and benefit, it also adds more responsibilities to act more like an actual business owner than non Pay Agents have to be. </p>
<p>One of these responsibilities is the payment of commissions by Pay Agents to the Partners in their Group (and in some cases to Partners that are not in their Group). In most cases, the Pay Agents or the Partners (acting as Fees Agents) have collected fees and not sent the funds they have collected to the company. Therefore the company never received the fees or sales proceeds from which the commissions are to be paid. So who needs to be responsible to ensure the commissions are paid? This responsibility has been delegated to the Pay Agents.</p>
<p>And this responsibility has been increased dramatically due to commission payouts in Pay Agent countries being greater for some Pay Agents than the fees that may have been collected and/or when the Pay Agent may not have actually collected the fees from which the commissions are to be paid. This latter situation can occur when Partners have acted as Fees Agents and collected and kept fees or when another Pay Agent (many times from another country) has collected the fees.</p>
<p>Although our final "internationalized" version of our (wonderful and simple to understand) compensation plan - which we will be releasing in September, will finally resolve the commission percentage payout problem in Pay Agent countries, we still have to attend to the issue and problem that has been created by Pay Agents having to pay commissions to Partners whom have acted (or may act) as Fees Agents to collect and kept the fees from which they earning commissions. </p>
<p>Our most recent change in our Commissions Policy is designed to help correct this problem. Is it a perfect solution? No it is not. Will it help reduce the burden for some Pay Agents? Yes it will. </p>
<p>Also, we hope to be able to complete our Pay Agent Improvement Project this week, so it will be released in full, which will also help Pay Agents to perform their roles more efficiently and effectively. I expect that we will have been able to make any adjustments needed by the end of the month, so that we have a finalized and seamless process in place for Partners (in Pay Agent countries) and Pay Agents to use going forward. </p>
<p>And I will be sharing more information over the next few weeks about other resolutions we are making to problems we have had in Pay Agent countries, so that we can start in September on a fresh, new course to advance ClubShop internationally and in Pay Agent countries.</p>
<p>When I talk about being "pioneers" I am reminded of sign that hangs on an old pioneer, covered wagon that was pulled by horses all the way to Tucson, AZ during the westward expansion of the United States in the 1800's. It states, "The cowards never got started. The weak died along the way. Only the strong made it through".</p>



<hr/>

<p class="style28">Thursday, July 30, 2015</p>

<p>We will be modifying some of the GPS Subscription Fees next month for various countries due to a number of different factors.</p>
<ul>

<li class="blue_bullet">A decrease in currency valuations for many currencies. This requires us to increase fees by an average of 5% for many countries, to keep the Pay Point and U.S. Dollar values similar to what they have been.</li>
<li class="blue_bullet">For some countries, if we kept the fees the same as they have been, the Pay Point value for a GPS BASIC subscription falls below the 10 Pay Point requirement to qualify as a Partner. For these countries and for any country which has had a value of 10 Pay Points or less, we have increase the fees by 10% to 20%, so that the minimum Pay Point value is 11 Pay Points per GPS BASIC subscription. This will protect all GPS subscribers of not going below the 10 Pay Point requirement due to future currency devaluations being more than 10% in a month. </li>
<li class="blue_bullet">For some French and Arabic speaking countries, we have been able to lower the PLUS subscription pricing, due to being able to advertise at lower rates to gain new members for the CO-OP and to fill PLUS orders. </li>
</ul>

<p>In all cases where we have increased fees, we are also providing an increase in the Pay Point values, which will provide a corresponding benefit to Partners to earn more in PRP Pay Points, GRP Pay Points and Team Pay Points. </p>
-->


<!--<p class="style28">Friday, June 26, 2015</p>

<p>I want to thank all of the Pay Agents that participated in our recent survey. Gaining your thoughts and feedback will help us to really improve the Pay Agent Program!</p>

<p>Here are the results from some of the survey questions we provided:</p>

<p class="medblue bold">Q - Which Partners should be allowed to be Pay Agents?</p>

<p>A - The majority opinion for this was Partners with higher pay levels and Executive Directors, that are serious and committed to performing the Pay Agent professionally. </p>

<p class="violet">My thoughts - In countries where we are first establishing a marketing presence and where we don't have any/many Pay Agents yet, we may need to allow a newer Partner to act as a Pay Agent, as it may be the only way for them to grow their business in their own country. Otherwise, we will only allow Marketing Directors and Executive Directors to act as Pay Agents.</p>


<p class="medblue bold">Q - How many Pay Agents should be allowed for each country?</p>
<p>A - There was no majority opinion for this question. Answers ranged from 2 or 3, to 4 or 5, to 20 - 25, to each Executive Director, to a variable and to the maximum!</p>

<p class="violet">My thoughts - Since we have no definitive response for this question, we will continue to allow as many Pay Agents in each country, as each country has Marketing and Executive Directors.</p>



<p class="medblue bold">Q - Should all Pay Agents be required to be able to send funds directly to the DHS Club?</p>

<p>A - 50% said yes and 50% said no. However most of the "no" responses were from Moroccan Pay Agents, as there is a maximum amount of money that can be sent out of Morocco each year. And most of their responses were accompanied with an exception - if they had an international pre-paid credit card (works similarly to a debit card), then most would be able to use their card to receive and send funds from/to the DHS Club.</p>

<p class="violet">My thoughts - We will be delaying any change in this rule until Pay Agents are able to have a pre-paid credit card and will continue to allow Pay Agents to receive commission advances to help them pay for the fees they receive from their members.</p>

<p class="medblue bold">Q - Should a Pay Agent that receives commission advances be required to send funds to the DHS Club to pay back their advances?</p>
<p>A - Like the last question, 50% said yes and 50% said no.</p>

<p class="violet">My thoughts - Again, we will be delaying any change in this rule until Pay Agents are able to have a pre-paid credit card and will continue to allow Pay Agents to receive commission advances without having to send matching funds to the DHS Club.</p>


<p class="medblue bold">Q - What ideas or recommendations can you make for us to improve the Pay Agent Program?</p>
<p>A - We had all kinds of responses to this question and some were very good and thoughtful. The two biggest responses were to help Pay Agents to get an international pre-paid credit card and for there to be a training course (and even a test to pass) for Pay Agents to take. We also had feedback about providing Pay Agents with "software" to help them perform their role.</p>

<p class="violet">My thoughts - I agree with these three recommendations and we have already begun to act to make the following improvements for our Pay Agent Program:</p>

<ol>

<li class="violet"> We have begun the programming needed so that Pay Agents will not need to receive or send emails in order to receive funds or send funds to members. Once the programming has been completed, we will ask each Pay Agent to go to a new interface we are creating so that they can provide all of their information to receive funds from members in their country.<br/><br/>

Partners will then be able to gain their Pay Agent's information and payment instructions from our website and application form, so they can send their funds to their Pay Agent and so they can send their national ID card and photo to the DHS Club from our website. Pay Agents will receive an email notification from the company with the funding information and Pay Agents will be able to use their interface to look at the ID cards and photos and to gain any information they will need to pay for the fees they will have received.<br/><br/>

And when a Partner requests funds to be sent to them from their ClubAccount by their Pay Agent, a similar process will take place so that the Pay Agent gains all the information they need to send their Partners the funds they are entitled to receive.<br/><br/>

We are still waiting for Pay Agents from Algeria and Egypt to send us the methods that may be available in their countries to receive and send funds, so that we can complete this aspect of the programming.<br/><br/>

Once we have this in place, this will streamline these processes to make them more efficient and effective for Partners and Pay Agents!</li>

<li class="violet"> Next month, we will creating a new Pay Agent Training Course which we will require each Pay Agent to take before they can be certified and approved as a Pay Agent.</li>

<li class="violet"> We have begun negotiations with u U.S. company (Payoneer) to gain a corporate account with them and with Bank of America so that we can enable Pay Agents to be able to apply for and receive by postal mail a physical (actual) Master Card, which will be able to be used online and offline to shop and can also be used at an ATM or bank to gain funds in each Pay Agent's own currency.<br/><br/>

Hopefully, we can get what we want and need from Payoneer, to be able to sign an agreement to put this in place in July. If and when we are able to do this, we will implement this new International Credit Card program in phases, to be able to begin paying some commissions to Pay Agents' cards to fund their cards.</li>

</ol>

<p>Again, I thank each Partner that took the time to respond to our survey to help us improve our Pay Agent Program!</p>







<hr/>-->
<!--

<p class="style28">Tuesday, June 16, 2015 2:00 PM</p>
<p>I recently responded to a Pay Agent that requested that they be paid some of their commissions by PayPal, even though they had not submitted any funds to the company for the same month the commissions were earned. I know that other Pay Agents may want to also gain a greater understand for why we don't allow Pay Agents to directly receive commissions, if they have sent us any funds, so here are some edited excerpts from my email.....</p>
<hr/>
<p>As a Pay Agent, you have the ability to receive funds and pay funds out, in order to develop your business in your country which has limited methods for Partners to send money out of the country or receive money from outside the country. </p>
<p>This provides you with the privilege and benefit of being able to receive advances of your commissions so that you can pay for fees that you receive, rather than having to send the funds that you have received to the company. It also provides a greater responsibility to you than non Pay Agent Partners have, as you actually have to run a business and make sure that you are able to pay your "bills" from the revenue you receive. For Pay Agents this means being able to pay the commissions to the Partners in their Group, from which you have received the funds in your ClubAccount that the commissions are earned from.</p>
<p>Like any business owner, we can't expect to keep and spend all of the money we receive from our sales proceeds. We have to save some to pay our bills too. </p>
<p>If the Partners in your Group were able to send their fees directly to the company, then the company would have actually received the money necessary to directly pay them and you the commissions from the sales that were made. And in cases where we do actually receive funds directly for purchases that have been made, we are very willing to directly pay (via PayPal for international Partners) the resulting commissions earned from these sales.</p>
<p>However if the company does not directly receive any proceeds from sales made, we should not be expected to pay any commissions on these sales! What should be obvious is that no company can allow a sales person or sales manager to receive the money from a sale they have made and allow them to keep the money (and not send it in to the company) and then be asked to pay commissions to the sales person and sales manager that kept the money! </p>
<p>This would result in the company not making any money (so we can pay our bills and our staff), but we would also be paying out commissions without receiving any money from which to pay them. When a company has to spend more money than it takes in (more expenses than income), it can and will go bankrupt. </p>
<p>When this occurs for a direct selling or network marketing company (which statistically occurs within 5 years for 90%) all of their "partners" will go unpaid and will have no chance to ever earn a long term or residual income. So I won't allow our company to put itself in a position, despite pressures from demanding Partners, to have the company have cash flow problems or operate at a loss. </p>
<p>In order to squash any rumors that may be out there, know that we do not have a cash flow problem, although we could soon have one if we didn't correct the unacceptable and excessive commission pay out percentages for Pay Agent countries, or if I allowed commissions to be paid directly from our bank or Pay Pal accounts without receiving the funds directly into our accounts from which the commissions could be paid. </p>
<hr/>
<p>All of this being said, I do want to thank all of our Pay Agents for performing their roles as Pay Agents, which has been a major factor that has allowed each Pay Agent to grow their business!</p>




<hr/>


<p class="style28">Tuesday, June 16, 2015</p>

<p>We have begun a complete review of our Pay Agent Program and are considering a large amount of feedback we have received from Pay Agents for the program. Included in this review will be what we can do to make the entire process of collecting fees and paying for fees more streamlined and easier for all members involved. </p>
<p>One of the first items that we have looked at changing is the Transfer Fee for Algerian Pay Agents. We have had many non Algerian Executive Directors ask us to lower the fee from 50% to 5% so that it is the same for all Pay Agent countries. </p>
<p>Now that we have multiple Executive Directors in Algeria, it makes sense for us to lower these fees, as most are able to use their commissions that are deposited into their ClubAccounts to pay for the fees for their members. And it will better enable more Algerians to become Partners as well. So although this may seem to be a sensible and fair modification to make, it doesn't take into account the times when an Algerian Pay Agent has to send funds to the DHS Club using other, more costly methods.</p>
<p>We have also had recommendations to lower the fees to 15% to 20% from Algerian Pay Agents. And since we have some that are willing to receive 15%, that will be the new Transfer Fee that we will allow Algerian Pay Agents to receive. </p>
<p>This is an important and potentially beneficial change to make for two reasons: </p>
<ol>
<li>The former 50% fee was allowing Algerian Partners to be able to collect much more than was needed (due to being able to pay for fees from commissions) to pay for fees to meet the PRP qualifications needed to gain promotions to higher pay levels in order to manipulate the Compensation Plan.
</li>
<li>By lowering the cost of the overall fees to be paid, we are enabling more Algerians with the opportunity to be able to afford the investment needed to become Partners. </li>
</ol>
<p>Over the next few months, we will be looking at how much we receive directly in funding from Algerian Pay Agents by credit card or PayPal to determine if a 15% Pay Agent Transfer Fee is justified as well. </p>

<br/>



<hr/>

<p class="style28">Wednesday, June 10, 2015</p>

<p>Yesterday our I.T. Department completed the programming to provide me with a new report which shows the fees received and the commissions paid out each month, for each team and for each country. Now and in the future I will be very aware each month, of exactly how much our revenue and commissions are for each country and each team. </p>
<p>What I was able to see for our commissions being paid out for the fees received in Pay Agent countries for April and May was very disturbing. For the past few months we have actually been providing Partners in Tunisia, Morocco and Algeria with a total commission payout greater than the fees that have been paid by the Partners in these same countries. </p>
<p>Fortunately, our other teams' pay out percentages have stayed within our target, because the fees we collect from the Partners in the US and EU average much more per Partner, than what we receive from the Partners in Pay Agent countries. Nevertheless, our total payout percentages for April and for May will exceed 100%, which is unacceptable and unsustainable. </p>
<p>Normally, we would have been able to see this problem growing before it got too large to handle, but the Pay Agent Program and having commissions deposited into ClubAccounts has obscured (hidden) this problem so that it was not readily seen as it should have been. If I had these types of reports created before now, I could have seen this problem getting bigger and dealt with it before it got as big as it has. And I should have. Now I need to deal with how to quickly and best correct this problem to enable the company and you and your most loyal Partners to continue to do business in Pay Agent countries.</p>
<p>We can't afford to wait to "tweak" things in the Compensation Plan to gradually to achieve this goal. Rather we have to make the changes as soon as possible in June, to start to lower the payout percentage in Pay Agent countries for June and to be able to have an 85% commission payout for July. </p>
<p>So I feel it that the best solution is to immediately increase fees for TN, MA and DZ, as I have seen many Algerians willing to pay 50% more in fees due to having to pay Transfer Fees to Pay Agents. Although the average incomes of Partners in these countries will make it more difficult for some to start and own a ClubShop Rewards Partner Business, we will be able to continue to allow Partners in these countries to earn the kind of commissions that they have been earning, so that they can earn much more than the average income for these countries.  </p>
<p>I also feel that once we increase fees to get the payout percentages down to 85%, we will not need to continue to make changes in the Compensation Plan each month, which should lead to greater stability for all of our Partners in the future. </p>
<p><b>Therefore, we will be increasing fees for Tunisia, Morocco and Algeria by 60% effective on Monday, June 15th.</b> Although fees for these countries will still not be as much as they are for US and EU Partners, these increases will allow us to stay within our targeted payout percentages without lowering any commission entitlements. </p>
<p>For any TN, MA or DZ Partners - if any past due monthly or renewal fees are paid before Monday, they can be paid at the current rates. Any new applications which are submitted before Monday, will also be allowed to pay the initial fees at the current rates, but any new Partner applications which are submitted on or after Monday, will need to pay the higher fees. </p>
<p>Finally, I want to ensure all Partners that live in Tunisia, Morocco and Algeria that the company will enable all commission entitlements earned in April and May to be received, either by PayPal (for those that meet the rule to receive commissions by Pay Pal) or by a Pay Agent, or by allowing you and other Partners to use the commissions in your ClubAccounts to pay for fees from the money you receive from Partners.
</p>






<hr/>
<p class="style28">Friday, June 5, 2015</p>

<p>I have now received some feedback and recommendations from some of our Pay Agent Executive Directors, about what we can do to prevent abuses and manipulations of our Partner Compensation Plan. And I have been and will continue to consult with staff and look at new reports we will be generating next week, so that any decisions I make are well thought out and will deal with the "root" cause of these issues and not just the effects. </p>
<p>As a result, we are now implementing two changes to help us deal with some of the issues that have recently cropped up in Pay Agent countries. </p>
<ol>

<li><b>COMMISSIONS POLICY UPDATED - "PayPal - Pay Agent Countries"</b> - <a href="http://www.clubshop.com/manual/business/commission.xml" target="_blank">http://www.clubshop.com/manual/business/commission.xml</a>
<br/><br/>
We want to have a way for Partners that live in Pay Agent countries to be able to receive their commissions directly via Pay Pal, rather than be paid to them by a Pay Agent. At the same time, we also want to ensure that the company does not directly pay out more in commissions than funds that we have actually received.
<br/><br/>
In many cases, the company has been receiving little to no funds from Pay Agents to pay for the fees they have received. Rather the Pay Agents that have not been sending us funds have kept these fees and been able to use their commissions (and advance commissions) and other funds transferred to their ClubAccounts to pay for the fees. By our providing Pay Agents with this enhanced ability to pay for fees from the funds in their ClubAccount and from advances of their commissions, we have enabled some dynamic growth to occur in Pay Agent countries.
<br/><br/>
It has been and continues to be our goal and target to pay out 85% of the revenue we receive in commissions to Partners. And this is an an industry high payout. No other company or opportunity offers such a high payout percentage in commissions. But we can't and won't enable Partners to get paid commissions directly from us when we haven't actually received the fees or funds from them and/or from the members in their Group from which their commission entitlement is based. Why? Because they need to get paid by the person that received the funds and retained these funds.
<br/><br/>
We enable this to be done by transferring the commission amount to be paid out by the Pay Agent to their ClubAccount, before asking them to pay out the commissions in their own currency. And we enable each Pay Agent to receive a Transfer Fee and Pay Agent fees in conjunction with each receipt or disbursement of funds. Nothing could be fairer to the Pay Agents and their Partners and the company.
<br/><br/>
And this is why we have now implemented this policy. 

</li>

<li><b>ABUSE &amp; FRAUD POLICY UPDATED - MEMBERSHIP FRAUD</b> - <a href="http://www.clubshop.com/manual/policies/abusefraud.xml" target="_blank">http://www.clubshop.com/manual/policies/abusefraud.xml</a><br/><br/>

Another issue that has emerged with our expansion and improved empowerment of the Pay Agent Program, which stems from our need to eliminate the former policy of limiting a Partner's ability to pay for no more than 3 Partner's fees. This issue is the abuse of the Pay Agent privileges to use their commissions and advances of commissions to pay for fees.
<br/><br/>
This abuse has created a manipulation of the Compensation Plan, the impact of which if mishandled could hurt all Partners. I don't believe in "throwing out the baby with the bathwater". This phrase may not translate well, so let me explain what I am referring to. Just because there is something that is bad or no longer usable or valuable, doesn't mean that we should get rid of everything that is related to it that may be good or has value.
<br/><br/>
In the treatment of cancer cells, sometimes good cells are destroyed at the same time the cancer cells are being treated. Which is why many cancer treatments fail to work and can actually make it more difficult for the patient's system to fight the cancer on its own. Yet we know that it is best to cut out or eradicate the cancer before it grows too large and it is better yet to eliminate what may actually be causing the cancer.
<br/><br/>
A reactive solution for us could be to get rid of the Partners that are currently manipulating the Compensation Plan, but that really doesn't get rid of what may cause the same cancer to occur again. Which is why we have to identify the "root" cause of a problem so we can deal with it permanently.
<br/><br/>
And I believe after looking at various reports that the root cause of this problem is the creation and maintenance of fake memberships which are easily and selectively upgraded to help some Partners qualify for higher pay levels and/or are upgraded at the bottom of Trial Partner lines to increase the incentive for the upline Trial Partners to upgrade too. Although this may seem like a clever thing to do, this just spreads and furthers the deception involved, for personal gain for a few, which threatens the company and all of our good, honest, hard working Partners.
<br/><br/>
So rather than have a "knee jerk" reaction to solve this problem (like dramatically increasing qualifications to protect the company from such abuses), I feel that we need to first deal with what is causing the cancer to occur and grow.
<br/><br/>
We have already learned that it is impossible to efficiently and cost effectively require all new Partners that are having their fees paid for by a Pay Agent, to provide a picture ID. Sounds like a good idea, but it creates another hoop for prospective Partner to jump through, in addition to having to pay a Pay Agent. And it would also require us to have staff look at over a thousand photo ID's a month to determine if they are legitimate.<img src="face_unhappy.gif"/>  And to pay for this, we would have to increase the cost of the Initial GPS subscription for all Partners in Pay Agent countries.<img src="face_unhappy.gif"/>
<br/><br/>
It is not as difficult or time consuming for us to look at a report to determine who are the most likely suspects and then require some of the new Partners they have paid the fees for to produce a photo ID so we can verify the legitimacy of the membership.
</li>
</ol>
<p>So this is why we are implementing this policy.</p>

<p>It shouldn't be too long now for us to catch the culprits and discipline or terminate them. And we should also see the limiting and even cessation of this kind of abuse taking place.</p>

<p>I now have other options and recommendations to consider over the next few days, to see what else we can do to completely resolve these and the other remaining issues we have been having with our Pay Agent Program. <img src="face_wink.gif"/></p>

<p>Upward and onward!</p>


<hr/>

<p class="style28">Wednesday,June 3, 2015</p>

<p>I just received a monthly report for May's sales and two statistics stood out to me, which reveal that the Compensation Plan is being manipulated by some Pay Agents:</p>
<ol>
<li>We had three times more Executive Directors than Diamond Directors!</li>
<li>The total number of Executive Directors (including Stars) is greater than the total number of Ruby, Emerald and Diamond Directors we have!</li>
</ol>
<p>In a natural form of development, we should typically see a declining number of Partners for each higher pay level. This is what we see in all of our non Pay Agent countries. But in most of our Pay Agent countries we see a dramatic increase for Executive Directors. This means that either the PRP/GRP requirements are too little for these countries and/or that some Partners are using their Pay Agent position (and the ability to gain advanced commissions in their ClubAccounts) to selectively pay for fees to meet the qualifiers needed.</p>
<p>Although most of our Pay Agents are conducting themselves professionally and honorably to create and maintain actual sales to real members, some are not.</p>
<p>Let me provide you with a specific example. Consider that we have a 3 Star Executive Director that has 13 PRPs, of which 11 of these PRPs have no PRPs! All 11 of these PRPs have had their fees paid for by this person, who is their Sponsor and also their Pay Agent and Executive Director. Of the two PRPs that have downline Groups, one of the Groups has been able to have just enough GRP to meet the SV qualifier. And one of the new (in May) Executive Directors in this Group only became a Partner in April. Getting the picture?</p>
<p>These kinds of manipulations are forcing us to consider a number of options in order to deal with these problems on a permanent basis.</p>
<p>Because most Partners in many international countries do not have wide access to Pay Pal or credit cards, we have been relying on Pay Agents to receive fees and disburse commissions. Yet this allowance has enabled Partners to selectively pay for fees and even create fake memberships in specific parts of their Groups, to be able to earn more in potential commissions than the total amount paid in fees.</p>
<p>And this problem is further exasperated for the company when a Pay Agent expects to be directly paid commissions (via Pay Pal or Bank Wire) in greater amounts than funds than the company has actually received from the Pay Agents and/or Partners in their Group (via Pay Pal, Credit Cards or Bank Wire).
</p>
<p>What should be obvious is that a company cannot pay commissions on sales that have made without receiving the proceeds from the sale. For example, if a company hires salespeople to sell vacuum cleaners by going "door to door" to potential purchasers and the salesperson collects the money for the sale, but doesn't submit these funds to the company, but rather retains them (keeps the money). Then the salesperson requests to be paid a commission for their sale, how does the company have the funds to pay them?
</p>
<p>Or to provide another similar scenario, if the same sales person makes a sale and their sales manager collects the money from the purchaser and retains it (as their commissions) and the sales person requests to be paid a commission for their sale, how does the company have the funds to pay them?</p>
<p>Furthermore, these manipulations magnify an ongoing issue we have had with our Compensation Plan paying all international Partners (outside the US and EU) the same amounts in commissions (based on meeting the PRP and GRP) per pay level, despite having about 40% less in actual sales.</p>
<p>For example, a new BASIC GPS subscriber in Algeria pays $60.31 USD to upgrade to Partner, of which only $39.57 is placed in their ClubAccount to pay for fees. This allows the Pay Agent to earn $20.64 per new Partner when they use their own commissions to pay for the fees (rather than having to find a way to send the company $39.57 to pay for the fees). Why so much? Because of the difficulty of getting money out of the country, requiring Pay Agents to utilize 3rd parties or the black market to convert Algerian money (DZD) to Euros or USD. But if the fees are never sent to the company (because the Pay Agent is able to use their commissions or commission advances to pay for the fees), then the Pay Agent keeps the $20.64, the upline Partners earn commissions and the company doesn't actually receive any money.
</p>
<p>And even in cases where the company is sent the fees of $39.57, look at the difference in what the company receives for a new US GPS BASIC subscriber - $95.00. Yet both Partners count as the same PRP and GRP credit for pay level qualifications and commissions. This has created a big disparity between the commissions earned for the sales made, even if we were to receive all the fees directly from international Partners.</p>
<p>In addition to these problems, we are also continuing to have other problems all associated with the Pay Agent Program:</p>

<ul>

<li class="blue_bullet">Pay Agents not promptly paying for fees they have received.</li>
<li class="blue_bullet">Pay Agents in Algeria demanding to receive an additional 50% in Transfer Fees, but not having to send the funds to the company as they are paying for the fees from their commissions.</li>
<li class="blue_bullet">Pay Agents spending all the money they have received in fees, so that they are unable to pay commissions to the Partners in their Group.</li>
<li class="blue_bullet">Pay Agents soliciting Partners to join under them.</li>
</ul>

<p>In view of all of these problems and the recent "petition" I received from some Moroccan Executive Directors to discontinue the Pay Agent Program for Morocco (or at least their personal participation as a Pay Agent), I now need to consider all of this and make some decisions to protect the company and the many good Partners we have all around the world, from the potential harm that can be done to all of us if I allow these problems to continue.</p>
<p>I am seeking feedback now from our Ambassador's Club members that live outside of the US and EU, to include in my considerations before I make any decisions to resolve all of these issues.
</p>
<p>On Monday we celebrated our official 18 year anniversary as a company, which has allowed us to survive longer than 99% of the network marketing companies that came into existence before 2010. This statistic alone should prove that I have always sought long term solutions to problems in order to provide long term, residual incomes to our Partners and our employees. So I am seeking to do the same now to solve these problems so that we all can continue on our journey together to provide the best business opportunity in the world to more and more entrepreneurs and people seeking greater financial freedom.</p>
<p>Know that every decision that I end up making will be to mutually protect the best interests of the company and the many good and loyal Partners that depend on us for their incomes and future incomes.</p>
<p>
Onward and upward! </p>


<hr/>


<p class="style28">Friday, May 29, 2015</p>
<p>One of the challenges we have had to deal with in order to expand internationally is how to get money out of and into countries where there is no PayPal and where most members don't have a credit card. Since "necessity is the mother of invention", we created and have been developing our Pay Agent Program to overcome this challenge. </p>
<p>Pay Agents were originally individuals that may have a way to send or receive funds from the DHS Club, so that they could in turn pay for fees or pay commissions. Over the past year we have continued to develop the Pay Agent Program to allow more Partners to participate by using their commissions (and advances of anticipated commissions) to pay for fees.</p>
<p>In order to offset any problems or potential problems that the Pay Agent Program can cause the company and/or ClubShop members, we have had to create policies for Pay Agents and continue to monitor how the program is working. To see our Pay Agent rules and policy, go to <br/><a href="http://www.clubshop.com/manual/policies/feespayagent.xml" target="_blank">http://www.clubshop.com/manual/policies/feespayagent.xml </a> - note the very last rule which we recently put in place, allows Pay Agents which have sent the company funds, to receive commissions via Pay Pal or Bank Wire from the funds that have been sent to us. </p>
<p>If a Pay Agent only uses their commissions (and advances of commissions) to pay for fees, this means that the company has not received any funds from this Pay Agent in order to directly pay them commissions. Obviously, we can only pay commissions directly from the sales proceeds that the company has actually received.</p>
<p>So in addition to having the ability to be able to pay for fees to help their business grow, Pay Agents also gain two other benefits that non Pay Agent Partners do not get:</p>

<ol>
<li>Pay Agents are able to receive and retain a Transfer Fee, over and above the fees they may receive. In all cases where a Pay Agent is able to use their commissions to pay for fees, there are no additional costs for the Pay Agent to transmit the funds to the company. This enables Pay Agents to actually earn more money than their commission entitlement.</li>
<li>Because Pay Agents can use their advances of commissions to pay for fees that they receive, Pay Agents are actually able to receive their commissions in their own currency sooner than non Pay Agents. For example, a non Pay Agent that earns commissions in April, can receive them in June. Whereas a Pay Agent that earns commissions in April, can actually receive and retain them whenever they receive fees from the members in their Group.
</li>
</ol>

<p>For the most part, the Pay Agent Program has been very successful to help our Partner numbers grow internationally and I believe that some of the improvements we have planned for the program will make it even better. </p>
<p>The next improvements we will be making to the program over the next few months are:
</p>

<ol>

<li>Automatic assignment of Pay Agents to members that are submitting fees based on these rules:<br/>
	<ul>
	<li class="blue_bullet">The first Pay Agent (for the same country) in their "upline" Group will be the Pay Agent assigned to a member. This will lessen the burden for Pay Agents that have had to be responsible to receive fees or pay commissions for members that are "downline" under other Pay Agents.</li>
	<li class="blue_bullet">For members that do not have a Pay Agent (for their country) in their "upline" Group, they will be assigned a Pay Agent for the same country that has achieved the highest pay level. This will better provide a responsible Pay Agent to these members and better enable these Pay Agents to retain the funds they receive as commissions.</li>
	</ul>

</li>

<li>When a member seeks to send money to a Pay Agent, a form will be provided to them to provide them with the funding information for their assigned Pay Agent. This will streamline this process and make it more effective and efficient at the same time. </li>
<li>When a member seeks to receive money (commissions, ClubAccount withdrawals, ClubCash redemption's) from a Pay Agent, a form will be provided to the member to complete to provide their assigned Pay Agent with the funding information they need to send the funds to the member. This will also streamline this process and make it more effective and efficient at the same time.</li>
<li>Manual deductions will start being made from a Pay Agent's account, whenever a Pay Agent has not provided the transfer of funds needed to pay for fees received, with 72 hours of their receipt of the funds. </li>
</ol>

<p>Collectively, these new or revised policies and improvements will really help Pay Agents to grow their businesses in their own country! </p>





<hr/>


<p class="style28">Friday, May 22, 2015</p>
<p>Now that we have been able to release the new newest GPS provided emails to send whenever a ClubShop Survey has been completed by a member, we have also been able to program into the use of these emails, a new capability to use with future emails which we will be creating to be sent from the Member Followup Records. </p>
<p>Although having these new "ClubShop Survey Completed" emails to send isn't (of itself) a significant improvement to your GPS, what we have done for these (and future user GPS emails) is very important! Why? Because now we have programmed all three aspects for your GPS followup emails:</p>
<ol>
<li> Email is customized (merge fields) for the recipient.</li>
<li>Different emails are presented to be sent, based on the role of the sender (Sponsor, Coach, Exec)</li>
<li>Now with these new emails, different emails are presented to be sent, <b>based on the member type.</b></li>
</ol>

<p>So as we create emails for other activities, the emails that each of you will have to send will automatically be customized for each or all of these scenarios, to enable each Partner to have professional, personalized and customized emails to send to members!
</p>
<p>And in the near future we will also be enabling the system to provide different emails based on an activity tracking system we will be putting in place. For example, we will start to track the number of times a Trial Partner looks at their Trial Partner Report and then generate a different email to be sent to them, each time this activity occurs!
</p>
<p>Once we have all of this in place, we will have everything covered when it comes to email followup for each type of activity, for each member type and for each sender type! WOW!!! Your GPS (Global Partner System) just keeps getting better and better!</p>
<hr/>
-->



<!--<p class="style28">Tuesday, May 5, 2015</p>
<p>We recently had 15 credit card charges contested due to fraudulent use of credit cards. This led us to refund 10 other cards to prevent us from gaining additional contests from credit cards used to upgrade "Partners" in the same Group. </p>
<p>Whenever the DHS Club has a credit card charge contested for fraud, it has the following negative impacts on the company and for upline Partners:</p>
<ul>
<li class="blue_bullet">Our credit rating is damaged and the resulting fees we have to pay to process international credit cards can be increased.</li>
<li class="blue_bullet">The charges previously made are automatically taken from our bank account to reimburse the cardholders the money that was "stolen" from them.</li>
<li class="blue_bullet">We are charged an additional $30.00 contest fee for each fraudulent charge that was made. </li>
<li class="blue_bullet">Many staff hours are needed to be used doing research, terminating fake or fraudulent Partners and adjusting past commission entitlements which may be impacted by having less PRPs and GRPs.</li>
<li class="blue_bullet">Partners upline from the fraud may see deductions made from their ClubAccount or future commissions to pay for each $30.00 contest fee that the company has been charged. </li>
</ul>
<p>Although we want to be able to allow international members to use the credit cards that they own to pay for fees and other purchases, we also have to create filters to protect the company from credit cards being used fraudulently. This most recent barrage of credit card fraud was able to bypass a new filter we put in place earlier this year, so we will now need to put another filter in place for some new, international Partner upgrades. </p>
<p>Unfortunately, the better our filters work to prevent fraudulent use of credit cards, the more difficult it can be for legitimate card owners and potential Partners to use their cards to pay for their initial fees. </p>
<p>Although we are always able to catch these perpetrators before they are to actually receive any commissions, once a credit card charge has been contested, the other damage has already been done.</p>
<p>Therefore to protect the company and yourself from being charged a $30.00 credit card contest fee, we are asking all Coaches and Executive Directors to be on the lookout whenever you see a number of new upgrades occurring in the same Group, all of which fail to respond to your contact attempts. Whenever this type of scenario occurs, it usually means that one or more newer Partners has a list of credit cards and owners which they are using to create the upgrades, which they hope to profit from by earning future commissions from the fees paid for with the fraudulent cards. </p>
<p>So if you see this type of scenario occurring in one of your Groups, report it to our Accounting Dept. so we can contact the new Partners to determine if they are the owners of the credit cards that were used to create their upgrades. This way we can catch the fraud and process the refunds before any charges can be contested so that no contest fees will be assessed. </p>
-->



<!--<hr/>

<p class="style28">Thursday, April 30, 2015</p>

<p>I had my cell phone placed on my nightstand last night, anticipating a call from my son Stephen with news about the arrival of his first child, Bradley Burke. And the phone rang at 5:28 this morning, 12 minutes after Bradley was born. </p>

<p>So we headed up to the hospital early this morning to welcome Brad into our family. He is very healthy and really cute! He has dark hair but his skin tone seems lighter than his mother Savita's (her father is from India and her mother is from Puerto Rico).</p>
<p>Pat didn't want to stop holding him, but had to relinquish her rights when Savita's father arrived. <img src="face_smile.gif"/></p>
<p>We plan to head back up to the hospital after work today to get another visit in, as we are planning on starting our trip back to "Ducks' Nest" (TN mountains) on Sunday to arrive on Monday. So I won't be able to respond to any emails until about noon (EST) on Monday. </p>
<p>I will then work from my office in the lodge at Ducks' Nest for the next 6 - 7 weeks, before returning back to Florida in late June to spend more time with Brad and celebrate both of our son's birthdays. Then we head back to the cooler mountains for the rest of the Summer, except for taking a week's vacation to hopefully go back to our home state of Maine in August to enjoy some lobster dinners and spend some time with family and friends that we have in Maine. </p>

<p>Life is good and we are very thankful for all of our blessings! </p>



<hr/>




<p class="style28">Tuesday, April 21, 2015</p>

<p>Sometimes, little hinges can swing big doors! In other words, sometimes a little or small change can produce big results or benefits!</p>
<p>Over the past 18 years we have worked continuously to improve our Partner Business Opportunity. And some of the improvements may have seemed small in size, but have provided us with a major improvement. </p>
<p>This is how I see our latest improvement of limiting the coach and taproot developer roles for Team Captains to their "Side Volume" Group. In a prior TNT (Taproot Networking Technique) business plan model, we recognized the roles of "Builder" and "Builder Intern" to provide for a transition and training period to enable Partners to successfully assume the main, first line role and responsibility of making the taproot grow and get deeper. </p>
<p>This change incorporates much of this design, but requires Partners to "prove" themselves capable of being able to assume the all important taproot development role for their first, main line, before they are designated as the Coach and Taproot Developer (feeder and follow up roles) for this line. </p>
<p>Over the past few months I have been able to observe that many times that a new Team Captain may not be ready yet to assume the role and responsibility to take over the Coach and Taproot Developer roles for their first, main line Partner Group and taproot. This perspective has allowed me to see that too many first line taproots have failed to grow and increase by at least one new Partner a month and/or by not getting enough Partners in the line involved in building their own second line. </p>
<p>While at the same time, I have seen the need for the Sponsor builder of second+ lines, to act as the Taproot Developer and Coach for these lines, as they have been (or should have been) the Partner most responsible for the development and sponsoring of Partners in their "Side Volume" Group. </p>
<p>Consequently, we are making an important change in how our TNT business plan works, by enabling a Team Captain to be recognized as the Coach and Taproot Developer for their second+ lines (Side Volume Group), while continuing to show the first upline Sales Manager in a Group as the Coach and Taproot Developer for their/the first line. Then when a Team Captain qualifies as a Bronze Manager, they will become the designated Coach and Taproot Developer for their first line too. </p>
<p>The Group Reports and Followup Records will now reflect these designations, so that they can be implemented where needed. In some cases, Sales Managers will need to coordinate the resumption/assumption of their Coach and Taproot Developer roles for/from Team Captains that have been performing these roles for their first lines, which enable and allow each Team Captain to better observe and perform these roles to create the increase needed in their Side Volume Group to qualify as a Bronze Manager and prove themselves ready and able to successfully be the Coach and Taproot Developer for their first, main line too. </p>
<p>By implementing this small to our TNT business model, I expect that we will significantly improve the taproot development and training for all Partners. </p>





<hr/>

<p class="style28">Friday, April 17, 2015</p>
<p>I have been working from our corporate retreat property in the TN, US "Blue Ridge" mountains the past few weeks, but my wife Pat and I are headed back to Florida today to be able to be home for the arrival of our new grandson, Bradley Burke! My son Stephen and his wife Savita are expecting their first child any day now, so we want to be able to be there for the grand arrival! </p>
<p>I do have a meeting scheduled for this afternoon, which I will try and connect to using a wireless, portable "mifi" with a car antenna. Otherwise, I won't be able to answer any emails today through Sunday. But unless Bradley is born Monday morning, I plan to be back to work then. </p>
<p>If I have been acting as your Coach, I will be "incognito" for a few days, but I plan to be back in touch with you by Monday.</p>
<p>Success is found in the journey, not in any one destination, so enjoy the journey! Have a nice weekend everyone! </p>




<p class="style28">Tuesday, March 10, 2015</p>

<p>I just learned today that our projected commission payouts for February will exceed our revenue received in February and that similar projections are expected for March, if we don't do something to protect the company from paying out more in commissions than the revenue we have received or are projected to see. </p>
<p>How did this happen? Two reasons:</p>
<ol>
<li>I created an element in our Compensation Plan that became overly generous to Partners that were able to earn additional bonus commissions due to having gained a very large number of Team Pay Points. </li>
<li>The bonuses provided didn't anticipate the tremendous exponential growth that we have seen the past few months, which has resulted in "blowing out" this aspect of the Compensation Plan. </li>
</ol>

<p>It is great that so many Team Pay Points have been gained so quickly for so many Partners the past few months, but the exponential growth of the Team Pay Points for many Partners has created an increasingly larger imbalance in the pay out of commissions. For example, I could see that a Team Player (a Partner with just one GPS subscription sold) with over 30,000 Team Pay Points had earned $29.25 USD in Team Commissions, despite the fact that the GPS subscription that was sold by this Partner only cost $17.00 USD, never mind the upline entitlements to earn commissions based on having one more GRP to earn commissions from.</p>
<p>Ironically, the over generosity of our plan (which has been created by these bonuses), has actually fueled the growth that has contributed the most to the problem we are now experiencing. When a person can earn more in commissions than what their sales amount to, it creates a recipe for disaster to strike - a kind of bonfire that is fueled even more with credit card and PayPal fraud to pay for fees (both of which we have experienced a drastic increase in). </p>
<p>When we first created this simplistic and generous type of Compensation Plan, I knew that we would have to make adjustments to the plan as the number of Partners grew and the sales increased for the company. And I should have anticipated better the potential for the Compensation Plan to be too generous for some Partners. For this, I have to take full responsibility and blame for, for which I apologize. </p>
<p>I did create a "contingency fund" over the past year to have enough funds readily available to be able to pay out commissions should they exceed 85% of revenue received (if we were unable to adjust the plan quickly enough) and we may have to tap into this fund next month. But we can't and won't allow the part of the compensation plan that is causing the over payments, to stay in effect as we have know it.</p>
<p>Although I don't want to stymy growth from occurring, but if growth is creating payouts of commissions greater than the sales amounts the commissions are derived from, then I have to make the modifications necessary so that this condition doesn't continue. </p>
<p>So we have had to place a moratorium on allowing the Team Pay Point bonuses to be paid. Although we won't make this moratorium to be retroactive for commissions that have been paid out for January and prior months' entitlement, but we do have to make them be retroactive for February and March. This does not impact any Partners that had Team Pay Points less than 4,700 in February and less than 5,200 in March. </p>
<p>Once I have the data available to me for revenue received and projected payouts at the end of March, I will be able to see if and how we can continue to offer a Team Pay Point bonus type of entitlement. I very much want to provide this type of entitlement, but we may just need to place caps on it for each pay level and/or modify how much each incremental entitlement will be. </p>
<p>Two things I know for sure:</p>
<ol>
<li>We can't pay out more than we take in.</li>
<li>Increases in sales cannot create reductions in operating capital or net profits.</li>
</ol>

<p>Making decisions like this are very difficult, but sometimes leaders have to make tough decisions to protect the greater good. </p>

<hr/>
<p class="style28">Monday, February 2, 2015</p>


<p>It may help some of you to understand why we make the changes we do in our Policies and Compensation Plan, so here are my 

thoughts for the changes we have made, effective for February:</p>

<ul>
<li class="blue_bullet"><b>Abuse and Fraud Policy</b> - basically, we have deleted the portion of this 

policy that restricted the payment of other Partners fees. No more restrictions will be in place, limiting the number of 

fees that can be paid. <br/><br/>I shared in my last blog entry (shown below), the thinking behind making this change, so I 

won't repeat my self here. </li>

<li class="blue_bullet"><b>Fees and Pay Agent Policy</b> - Since we will now be allowing all Partners to 

collect funds and pay for fees, we also needed to provide new terminology, guidelines and rules to do so.</li>


<li class="blue_bullet"><b>Compensation Plan</b> - My biggest concern for removing the payment of more 

than 3 fees restriction rule, is that many Partners will seek to take advantage of this new ability to help them more 

readily qualify for higher pay levels without actually making the requisite sales to do so. And this is especially a most 

genuine concern, as many times a small contribution of just a few dollars combined with funds available in a Partner's 

ClubAccount will easily pay for GPS BASIC subscription fee, now that all Partners can earn commissions.

<br/><br/>
What person won't want to spend $50 to help pay for multiple fees, in order to receive one or more pay level promotions to 

earn hundreds more in commissions? And based on a past experience we had when no restrictions existed with a similar 

Compensation Plan model, I am confident that we will see an increase in fees being paid by Sponsors or Coaches. 

<br/><br/>
Since our Compensation Plan payouts and pay levels are based on "averages", if we were to keep our qualifiers the same for 

higher pay levels, our averages will change. This would threaten our ability to stay within our 85% payout target, which is 

simply something I cannot allow to happen.
<br/><br/>
The most logical qualifier to increase to deal with this potential problem, would be to increase the PRPs needed for higher 

pay levels. But I feel that it is important (especially to prospective and new Partners) that they can see that they can 

meet these qualifiers, if they are willing to work hard to do so.
<br/><br/>
The next logical qualifiers to increase (for higher pay levels) are the GRP and GRP SV qualifiers, which is what we are 

doing. While at the same time, we are reducing the optional GRP and SV GRP Pay Point qualifiers to go from 25 PP per Partner 

to 22 PP per Partner to offset the increase in the Partner number increases. This will enable us to keep these Pay Point 

qualifiers at about the same level that we had in January, to encourage more Partners to meet these same qualifiers to gain 

pay level promotions. This will also help encourage more Merchant sponsorship (online and offline) to increase shopping 

venues, sales and Power Pay Points to qualify for higher pay levels.
<br/><br/>
It is my hope and desire that these changes will provide us with a more stabilized Compensation Plan that will only need to 

be tweaked from time to time, based on the averages (average number of each Pay Level per 1,000 Partners) we see going 

forward. We won't know how things will work out exactly until the end of February.


</li>
</ul>

<p>Although no one wants to see fees or qualifiers increased, or commissions decreased, it is my duty and responsibility to 

ensure that we stay within our 85% targeted pay out of commissions, so that we can continue to provide the support you need, 

improve your GPS and strengthen our infrastructure. </p>
<p>It is easy and tempting to just look at any increases as "negatives", when they are actually important modifications that 

need to be made to enable you to have a company that you can rely on to provide you with an increasing, long term, residual 

income. </p>
<p>And the same standard or principle remains in place, as it always has and will in sales and marketing - if you want to 

increase your commissions, then increase your sales! Focus on the things that can and will increase your Pay Points and 

don't confuse activity (staying "busy") with actual accomplishment - gaining more Partners and earning more Pay Points. </p>

<p>Upward and onward!</p>
-->

<!--<hr/>
<p class="style28">Monday, January 19, 2015</p>

<p>There is a standard, 3 step business management process that I follow whenever we have a "problem" to deal with:</p>
<ol>
<li>Fix the problem at hand - stop the bleeding. </li>
<li>Determine the root cause of the problem and fix that, so the same problem doesn't keep reoccurring.</li>
<li>Communicate effectively and emphatically to anyone that may be impacted by the problem or the solutions. </li>

</ol>

<p>In December I received a report which indicated that we may be seeing an increase in violations of our Abuse and Fraud 

Policy:</p>

<ul>
<li class="blue_bullet">Potential credit card fraud - this problem for a cell of new Partners was dealt with.</li>
<li class="blue_bullet">Non Pay Agent Partners paying fees for more than 3 Partners - we discovered that 18 Partners were 

definitely guilty of violating this policy, just based on 

transfers made from their ClubAccounts, half of which may have gained one or more promotions due to violating this policy. 

<br/><br/>

In addition to these Partners, we were also able to see that a number of Partners may not have made direct transfers to 

other Partner's ClubAccounts, but were able to pay for 

other Partner's fees by using different credit cards that they may own, or another person's credit card, or by transferring 

money from their ClubAccount to a Pay Agent, so that 

the Pay Agent could pay for their fees. <br/><br/>

Although these Partners may not have violated the written policy, they certainly violated the intent of the policy, which 

was put in place to prevent manipulations of the 

Compensation Plan, which creates another problem for the company and for the Partners that "play by the rules". 

</li>
<li class="blue_bullet">Partners logging into other Members' accounts - we have not yet completed this audit.</li>

</ul>

<p>We were able to deal with credit card fraud problem and now that we have completed our audit of ClubAccounts used to pay 

for more than 3 Partner's fees, we will follow the 

process outlined above to fix this problem. Here's how:</p>

<ol>
<li>Fix the problem at hand - stop the bleeding. <br/>
Each Partner that violated the policy will receive an email by Monday to notify them of them violation and request that they 

read through all of our policies to be aware of what 

they are. <br/><br/>
However, each Partner that violated the policy and may have gained one or more promotions as a result of violating the 

policy, will have their pay level reduced to the pay level 

they would have achieved, if they had not violated the policy. Each of these Partners will be notified of the actions we 

will take to reduce their pay levels for December, along 

with the evidence to support the action we will take, along with any upline Partners that may also be impacted by our 

discrediting their downline Partners.
</li>

<li>Determine the root cause of the problem and fix that, so the same problem doesn't keep reoccurring.<br/><br/>

I have learned that a society cannot have rules in place that can't be adequately policed or adjudicated, or that the 

society does not want. The "rule of law" has no value if it 

can't or won't be followed. This is true for governments, societies, businesses and organizations.<br/><br/>

So we had two options to consider here - either police potential violations and adjudicate them each month or do away with 

the policy. Each option presents us with other issues 

that we would have to deal with.<br/><br/>

The monthly/policing option requires more staff time and energy taken away from productive efforts. It also emphasizes a 

negative stimulus for Partners, which can discourage them 

from being more productive also. And we won't be able to completely police this without requiring copies of credit cards and 

modifying the Pay Agent rules to disallow them from 

receiving funds from Partners, to pay for other Partner's fees.<br/><br/>

The second option of doing away with the policy, will open the doors for easier qualification of pay levels due to 

manipulations of the Compensation Plan and for the company to 

possibly be in violation of anti-pyramid scheme laws in place in countries where the 1 Personal Pay Point requirement isn't 

in place.<br/><br/>

Based on the downsides presented by each option, and due to the widespread acceptance and usage of Partner's wanting to be 

able to pay for other Partner's fees, we have decided to conduct another audit at the end of January to deal with any policy 

violators this month, then do away with the policy effective February 1st. <br/><br/>

 What we will need to put in place of the policy (effective in February also) are: <br/><br/>

	<ul>

<li class="blue_bullet">Although the PRP qualifications are the ones that have been the most manipulated, we do not want to 

increase these. Rather, we will be increasing the GRP 

Side Volume Partner qualifiers, while reducing the GRP Pay Points per Partner to keep the Side Volume Pay Point 

qualifications close to the same that they currently are.
</li>-->
<!--<li class="blue_bullet">Any country outside the U.S., Canada and EU that has at least one Executive Director in it, will 

also have the 1 Personal Pay Point requirement put in 

place for Partners in these countries to be entitled to earn commissions greater than the commissions earned to satisfy the 

Minimum Commission Guarantees to pay for (auto finance) 

GPS BASIC and PRO subscriptions.</li>-->
<!--<li class="blue_bullet">New "Fee Agent" rules for all Partners that are paying fees for other Partners.
</li>


	</ul>
	<br/>
<b>So effective in February, there will no more restrictions on the number of fees that a Partner can pay for other 

Partners.</b><br/><br/>


</li>


<li>Communicate effectively and emphatically to anyone that may be impacted by the problem or the solutions. <br/>

I have just done this. <img src="face_smile.gif"/><br/>
And with the emails that we will be sending out by Monday, and by providing everyone with advance notices about SV Partners 

qualifier increases and the 1 PPP requirement by the 

end of next week, we will have effectively communicated about and dealt with this problem.<br/>
<br/>
Then I can return to working on the GPS Followup Improvement Project!<br/><br/>
A final thought about having problems in our businesses...... I learned a long time ago that if we don't have problems to 

deal with, then we don't have much business either. So 

never let problems discourage you. Rather adopt a similar policy as I have to deal with your problems and be glad that your 

business is getting bigger and providing you with more 

problems to resolve. Because the BIGGEST problem that you don't want to have - is not having any or much business! ;)

<br/><br/>

Use every problem as an opportunity to turn your lemons into lemonade! :))<br/>

</li>


</ol>-->









<!--<hr/>
<p class="style28">Friday, January 9, 2015</p>
<p>Happy New Year!</p>
<p>We decided to start the new year with a bang, by providing you with a major improvement to your GPS! Within the next few 

weeks, Partner Sponsors, Coaches and Executive 

Directors will be able to do the requisite followups more efficiently and more effectively, by being provided with emails 

and/or links to instructions in the Member Followup 

Records (MFURs) for each activity that may be shown there.</p>
<p class="style31">WOW!</p>
<p>Imagine being able to see an activity performed by a member in your Activity Report and when you click on their ID number 

to look at their MFUR, you will be able to click on 

the appropriate email to send and/or be provided specific guidance and instructions on how you should followup on the 

activity, as the Sponsor, Coach or Executive Director for the 

member!</p>
<p class="style31">WOW! </p>
<p>For example, when you see a Trial Partner visit their Group Report and you are their Partner Sponsor, what should you do? 

Currently, you can go to the Training Guide and look 

up the information to determine what you should do. Or with this improvement, you will be able to go to the Trial Partner's 

MFUR and select from one or more emails to send and/or 

be provided with additional instructions on what you can do (based on your role) to followup most effectively with your 

Trial Partner.
</p>
<p class="style31">WOW!</p>
<p>And when you click that an activity has been followed up with, the system will automatically post the Followup Notes AND 

set up a Followup Reminder in the MFUR, if 

necessary.</p>
<p class="style31">WOW!</p>
<p>Just think how much easier this will make it for new Partners and reactivated Partners to quickly get involved with and 

and be able to do their own followups! And think how 

much time will be saved for all Partners to do the followups and post the records! No more need to create and use activity 

related template emails to send individually! </p>
<p class="style31">WOW!</p>
<p>We have already removed the email envelope icons from the Activity Report, so that they will just show in the MFURs, as 

we will be adding many new envelopes and emails to be 

used at the MFURs for other activities! </p>
<p>And we are just waiting to get a French translation for the first new email envelope we will making available to be sent 

by the Partner Sponsor (or Coach if the Sponsor isn't 

active) to a newly transferred Trial Partner that is now in their Group. </p>
<p>Once we have provided envelopes, emails and/or followup instructions for each activity type, we will also be adding some 

new features to the MFURs which will provide each step 

(Email, Skype, Social Networking, Call) with emails and/or instructions for the Sponsor, Coach and Executive Director to 

welcome new Members, Affiliates, Trial Partners and/or 

Partners! </p>
<p class="style31">WOW!</p>
<p>And if a step is not completed, it will show up as an assignment that still needs to be completed. In addition, we also 

plan to continue to show all past activity not followed 

up on, in every Activity Report, so that no activity goes without being followed up on. ;)</p>
<p class="style31">WOW!</p>
<p>And we hope to provide emails in the MFURs that have been translated into languages other than English, Italian and 

French, to include Arabic, Dutch, Spanish and Afrikaans. 

</p>
<p>We plan to complete this project, so that all the improvements are in place by the end of January or early February. And 

once it is completed, you and every Partner will just 

need to use your GPS (starting by looking at their Activity Report each day) to more efficiently, effectively and better 

navigate your to become an Executive Director with more 

and more stars!!!</p>
<p>Now let's make 2015 be your best year ever as a Partner!</p>
<p>Happy New Year! </p>-->



<!--<hr/>

<p class="style28">Wednesday December 31, 2014</p>
<p>For many years, we required all Partners to pay the same amount for GPS Fees, regardless of what country they lived in. 

This allowed us to modify the amount paid in 

international currencies to be adjusted to the U.S. Dollar on a daily basis. </p>
<p>A few years back, in order to encourage more international growth, we allowed for much lower rates to be paid for 

Partners outside of the U.S., Canada or the EU. The rates were 

first established based on the average incomes (and therefore the ability to pay fees) for each country. </p>
<p>Since then, our system automatically increases or decreases the Pay Point values each month, for each currency when a 

currency value changes. This has resulted in Pay Points 

either increasing or decreasing slightly from month to month. </p>
<p>This past month, some currencies were devalued so much over the past year, that the Pay Points for a GPS BASIC 

subscription fell below the 10 PPP requirement to qualify as a 

Partner. So we had to make an adjustment in Fees and Pay Points for these currencies, effective in January so that the Pay 

Points were at least 10 for a GPS BASIC Fee. </p>
<p>In addition to currency valuation adjustments needed, we also made some changes in fees for countries where economic 

conditions have improved or where we have seen dramatic 

growth. Most of these changes have been small (10% or less) and still do not even come close to charging most international 

Partners the same rates that U.S., Canadian and EU 

Partners pay. </p>
<p>The bottom line is that we will continue to modify Fees, Pay Points and the Compensation Plan as needed, to further 

strengthen and stabilize our marketing  plan for years to 

come. </p>
<p>I know that most people only look at the short term and see things subjectively with that perspective. Success is gained 

in this business and in life when we think long term, 

set goals, make plans and work hard to achieve our goals.
</p>
<p>One of the questions that I ask Partners when I speak at a Conference is, "Would you rather have more money now or 

later?". Most everyone in attendance say they want more money 

NOW. This always provides me with a teaching opportunity to explain the principles of long term thinking, sowing and reaping 

and the benefit of planting apple seeds rather than 

wheat seeds. </p>
<p>All the trees that grow in the next year and and fruit to be picked in future years, come from the seeds we plant today. 

</p>
<p>So think big and think long term so that you can have prosperity for a lifetime!</p>



 <hr/>-->

<!--<p class="style28">Tuesday December 30, 2014</p>

<p>It is exciting to see all of the new promotions taking place this month on the Thouraya Shili team! <a 

href="http://www.clubshop.com/p/promotions/promotions.html" 

target="_blank">https://www.clubshop.com/p/promotions/promotions.html</a></p>
<p>Thouraya herself has now qualified as a 9 Star Ambassador's Club member for December and now Bouzid Hassan has also 

qualified as our next Ambassador's Club member. 

Congratulations to them both for their accomplishments! </p>
<p>With dramatic growth like this (when Partners are able to achieve multiple new pay levels in the same month) also comes 

other issues, like violations of our Abuse and Fraud 

Policy, to abuse our Compensation Plan (paying for more than 3 Partner's fees and creating fake memberships and logging in 

as another Member to upgrade them without their 

knowledge) and fraudulent use of credit cards to pay for upgrades, all of which "help" Partners to illegitimately gain 

promotions. </p>
<p>Over the holidays we experienced a massive attempt to defraud the company and the many credit card holders whom had their 

credit card numbers stolen and attempted to be used to 

pay for Initial Fees. Fortunately, we have some fraud protection alerts in place to notify our Accounting Dept. of any 

potential fraud attempts, so we were able to quickly catch 

what was occurring to either void pending transactions or provide refunds to the applicable cardholders. </p>
<p>Unfortunately, a staff member and I needed to get involved with this during our holiday in order to deal with the matter 

as quickly as possible, as allowing fraudulent charges 

being made to stolen credit card numbers can have serious ramifications to the company. We already have to pay a higher than 

normal discount and transaction fee to process 

international credit cards, as we are unable to utilize the AVS (Address verification System) for cardholders that live 

outside the U.S. or Canada to verify that the address they 

provide matches the credit cardholder's address, which means that there is a greater chance for fraud to occur. </p>
<p>And as our credit card decline rates and credit card contests percentages increase, so will the fees we have to pay to 

process international credit cards. Although an increase 

of $.30 or $.40 per transaction may not seem like much, when you multiply that by tens of thousands of transactions, it 

creates an additional expense to the company of thousands 

of dollars. And if our decline and contest percentages increase too much, we can actually lose the privilege to accept and 

process international credit cards. </p>
<p>Consider too the bad reputation that the company could get if Members paid their Sponsor or a Pay Agent their GPS fees in 

cash, only to discover that their fees were not 

actually received by the company. These members too would be defrauded. </p>
<p>Obviously, we can not and will not allow these types of potential liabilities to take place, so we will be utilizing some 

new filters to prevent fraudulent attempts from 

occurring and we have also added some new rules for Pay Agents, to better enable them to perform their roles and to protect 

the company from having Pay Agents either not sending 

funds to us or trying to use fraudulently use credit cards to send us funds they have received from the fees they collect. 

</p>
<p>Although the Internet provides all of us with the opportunity to create global businesses, with every opportunity comes 

risks to be dealt with and responsibilities that need to 

be taken care of to protect the security of our businesses. </p>
<p>With these new Pay Agent rules in place and with the new credit card processing security filters we will be putting in 

place as well, we can confidently and securely grow our 

businesses in even more countries and markets around the world in 2015!</p>
<p>So the journey together continues! And remember, that success is found not in any one destination, but in the journey 

itself. </p>
<p>Happy New Year!</p>


<hr/>-->
<!--

<p class="style28">Wednesday December 3, 2014</p>
<p>Happy December! </p>
<p>November provided us with a net increase of 20% in the number of Partners we have, with the Thouraya Shili Team leading 

the way!</p>
<p>Congratulations to Thouraya on qualifying as a 4 Star Executive Director in November! She is now just 1 star away from 

qualifying for Ambassador's Club and a free cruise in 

2015!</p>
<p>I know that Thouraya has worked tirelessly as a Pay Agent and as an Executive Director, keeping her hopes and dreams 

alive for a better life and financial future. Now she is 

beginning to see her dreams come true!  Congratulations Thouraya!!!! </p>
<p>Furthermore, Thouraya has not accomplished the building of her business to the 4 Star pay level on her own. Fellow 

Tunisian Executive Director Bouzid Hassan has really taken a 

key leadership role to help ClubShop Rewards expand not only in Tunisia, but also in other French and Arabic speaking 

countries as well. So congratulations to Bouzid on achieving 

his promotion to 3 Star Executive Director in November too! </p>
<p>Thouraya and Bouzid have paved the way for their Groups and are now able to see more Partners joining them as Executive 

Directors, including new 1 Star Executive Director Wael 

Salem (also from Tunisia) and new Executive Director Afef Bouzid (who is Bouzid Hassan's sister!). And now we also have our 

first Executive Director in Morocco - Najib Kounsasse! 

Funtastic! </p>
<p>I expect that we will be seeing new Executive Directors in other French speaking countries as the fire continues to grow 

from North Africa down throughout the entire continent, 

all the way to Madasgar and Reunion! It is growing in Algeria, Burkina Faso, Togo, Senegal and many other countries. And 

this can only help the Partners in all African countries 

to see the potential for their country too! </p>
<p>Each Partner is just a decision and (renewed) commitment away from doing what it takes to gain a promotion and taking the 

next step to earning more income. Whatever we believe 

- we can achieve! </p>
-->






<!--
<p class="style28">Thursday October 30, 2014</p>
<p>I have been tied up this week with unanticipated issues that cropped up relative to programming and implementation issues 

we have experienced with Phase 1 of TNT 2.2, and then 

with an increased urgency and need to move our web server to a new server, with many changes needed to convert much of our 

content to new or upgraded software.
</p>
<p>We had been steadily making progress on converting everything to be able to move to a new server (we have already spent 

hundreds of man hours working on this intermittently 

since last year), but we have recently had some hardware issues with our current server and a new "urgency" thrust upon us 

by one of our credit card processors, to upgrade and 

update our SSL (security) software by November 4th. </p>
<p>Know that there are countless scripts running to integrate and coordinate our web server with our database server, all of 

which we have created or modified many times over the 

past 17 years, to run our business and to enable you to run your businesses. And we have spent over $1,000,000 dollars to do 

all of this programming so that all of our systems 

(including your GPS) can function so well. As a result, we have MANY different softwares loaded on our server to help us run 

things and some of them are just not compatible with 

newer servers. </p>
<p>So this has now become our top priority, as there is nothing more important for us than to protect our data and our 

ability to keep our website functioning at high levels. </p>
<p>And I am still having to work with doing some things manually and personally to implement Phase 1 of TNT 2.2. 

Consequently, I have not been able to perform my coach role with 

any Partners again this week and I don't feel that I can block out any of my time to meet with Partners at this time. I had 

hoped to open the ClubHouse for an afternoon this week 

to meet with Partners that I am acting as the coach for, but until we make the changeover and troubleshoot everything, I 

have to be able to devote my attention and time to deal 

with conversion issues as they arise. </p>
<p>I am also unable to send our I.T. department any programming work needed to be done to make improvements to our GPS or to 

begin Phase 2 of TNT 2.2, until we have the new web 

server up and running smoothly without any incidents of software malfunctions. So we have put a temporary "hold" on making 

any improvements to our GPS or to do the programming 

needed to release Phase 2 of TNT 2.2.
</p>
<p>Nevertheless, when life gives us lemons, we make lemonade! So I will use my "free" time the next week working on 

improving the Training Courses, as I have been able to see 

common needs for further and better explanations, illustrations and guidance for a number of the courses. Although I can't 

be distracted when writing, I can stop my work to deal 

with other issues as they crop up and may need my attention. So this is my game plan for the next week!</p>
<p>Next week, we plan to close up the property here in the mountains for the winter and return back to our Florida home by 

Friday. So I won't be able to work much on DHS Club 

matters on Thursday or Friday of next week. If it appears that I can start allocating time to work as a Coach again the week 

beginning 11/10, then I will definitely start 

scheduling appointments to meet then, as well as open the ClubHouse too.
It will be GREAT to be able to use Skype with my high speed, cable Internet connection once I am back in Florida! This will 

allow me to meet and TALK impromptu with Partners, as 

well as make international phone calls using Skype. </p>
<p>Although we have really enjoyed working from our retreat property here in the mountains, we are also looking forward to 

being back in Florida again!  </p>
<p>Meanwhile (until we can meet together personally), here is my general guidance and directions for the Partners I am 

acting as the coach for, until we can meet and discuss each 

of your personal situations:</p>


<ol>
<p><b>Non-coaches</b> - just do the best you can and don't be afraid to do something wrong or fail. The biggest thing you 

can do wrong is to not do anything! ;)</p>
<li>Look at your "personal" Activity Report regularly and send the welcome emails provided to new Members, Affiliates and 

Trial Partners.</li>
<li>Refer to the Partner Sponsoring Course to followup with other related activity.</li>
<li>Post the Followup Records as you send emails or messages or make calls.</li>

<p><b>Coaches</b> - stop trying to communicate with or motivate inactive Partners! Rather, use TNT 2.1 and the Partner 

Coaching Course to build Trial Partner lines under the 

deepest Partners you have under each PRP. The best and most important things that you can do to gain your Partners' 

ATTENTION, INTEREST and INVOLVEMENT is to build depth under 

them to help them get a PRP and/or to increase their GRP.
</p>
<p>You should not have multiple Trial Partners on your frontline. Rather, they need to be transferred to taproots under your 

deepest Partners, or to your personal Trial Partner 

Group if you are also working to sponsor a new PRP to qualify for a promotion.</p>

</ol>-->
<!--<hr/>


<p class="style28">Thursday October 23, 2014</p>
<p>We will soon be announcing the release of Phase I of our TNT 2.2 project! </p>
<p>I will share more specific information about this in my "View From The Top" article that we will be posting in 

conjunction with our release announcement. Basically, TNT 2.2 

will focus on making more improvements to our Trial Partner Program. </p>
<p>Working on TNT 2.2 has kept me very busy and has made it almost impossible for me to function properly as a Coach for 

those Partners that I am supposed to be coaching. I know 

that I can only do so much and there are only so many hours in the day and work week, but I have been feeling very guilty 

nonetheless. </p>
<p>Many Partners have sent me Skype messages and emails, either returning my messages/emails or asking for help or 

assistance - most of which I have yet to respond to. I hope that 

no one takes my lack of communications personally. Just know that I have been working on some wonderful improvements that 

will benefit all Partners, including each of you that I 

am personally coaching. </p>
<p>Although I will begin work next week on Phase II of TNT 2.2, I also expect to have more time freed up starting next week, 

to start working with and coaching Partners more 

closely again. To help me do so, I plan to have the ClubHouse open from 1 - 5 EDT for all English speaking Partners that I 

am the Coach for, so I can meet with those that can meet 

with me. </p>
<p>So if you've been waiting to hear from me, thank you for your patience and you can expect an email or message from me 

very soon! </p>




<hr/>

<p class="style28">Friday October 10, 2014</p>

<p>We have now completed all of the improvements and changes needed to fully and best implement our tremendous TNT (Taproot 

Networking Technique) 2.1 version of our Partner 

Marketing Plan!</p>

<p>The final changes that needed to be made were to our Compensation Plan, so that it would "synch" better with the way TNT 

2.1 has been designed to work, which is to help you to 

build "depth" and increase the size and sales within your personal Groups. And the better your Groups are built, the better 

your Team Pay Points are increased as well!</p>

<p>Here is a summary for the changes for the Compensation Plan that will go into effect this month:</p>

<ol>
<li>We have created a new 200 Team Pay Points column, which will provide small increases in Team Commissions for Active 

Partners, Team Players and Team Leaders, once their Team 

Pay Points exceed 200. <br/><br/>Although this will increase our overall payout of commissions slightly, it will enable new 

Partners to better see and understand how their 

position on the team can and will provide them with corresponding increases in commissions!</li>

<li>We have reduced the PRP/PP requirement for all Sales Manager pay levels, so that just ONE more PRP is needed to gain 

promotions from Team Captain to Bronze Manager, and from 

Bronze Manager to Silver Manager, and from Silver Manager to Gold Manager! Although we have lowered the PRP/PP requirement 

for these pay levels, we have kept the GRP/PP 

requirements the same.<br/><br/> 
This will also increase our overall payout of commissions, as it will be easier for Partners to reach each Sales Manager pay 

level, due to the PRP requirement being lowered. So 

Partners will now only need to develop ONE personal Trial Partner line to gain the one additional PRP needed to go from Team 

Captain to Bronze Manager and for each subsequent 

promotion as well!<br/><br/>

Rather than having to supply two or more personal Trial Partner lines, Trial Partner transfers can now be made to better 

supply one personal line and to 'feed" taproots in each 

Partner Group!
<br/><br/></li>
<li>We have also reduced the PRP/PP requirement for all Director and Executive Director pay levels, so that just one more 

PRP is needed to gain a promotion to the next pay level. 

So this has reduced the PRP requirement by two PRPs from the previous plan (and reduced the PRP requirement for Executive 

Directors from 15 to 10, from what was required in 

August!).
<br/><br/>
In order to balance the increases in payouts for all of the lower pay levels, we have increased the GRP/PP requirements 

slightly for the Director and Executive Director pay 

levels. And to ensure that Directors and Executive Directors are actually performing their role to build and increase ALL of 

their Group and coach ALL of the Partners in their 

Group, we have also created new "Side Volume" requirements for all Director and Executive Director pay levels.
<br/><br/>
Side Volume is defined as the number of GRP/PP other than the Partner Group that has the highest GRP/PP for each 

Director/Executive Director. We have set GRP/PP Side Volume 

requirements at 40% of what the total GRP/PP is for Directors and Executive Directors. For example, if a Partner has 50 GRP 

with one of their Groups having 40 GRP, then they have 

a Side Volume of 10 GRP.<br/><br/>

The reason for instituting Side Volume requirements for these pay levels, is so that Partners don't just let their upline 

Coach create almost all of their GRP/PP for them in their 

biggest line. This requires all Directors and Executive Directors to perform the Depth Builder and Coach roles for ALL of 

their PRPs and their GRPs.
<br/><br/>
This also prevents Partners from manipulating the Compensation Plan to earn more in commissions, by just meeting the new, 

lower PRP requirements - while having the great majority 

of the GRP in one bigger line. If we allow the Compensation Plan to be manipulated by some, it will mean reduced commissions 

for the other Partners that are actually performing 

their designated roles for each pay level. 

</li>
<li>In addition to reducing the PRP requirements for the Manager, Director and Executive Director pay levels, we have also 

greatly reduced the PRP requirements for 1 Star and 

higher Executive Directors, while keeping the GRP Side Volume requirements at the same level for each promotion! 

<br/><br/>Again, this puts the emphasis on Executive Directors to 

help their PRPs and GRPs to increase the size of their Groups and to encourage Partners in their Group and Team to qualify 

as Executive Directors too, rather than having to 

continue to personally sponsor higher numbers of PRPs. </li>
<li>We have lowered the number of Stars needed to qualify for Ambassador's Club and for Crown Ambassador so that we can 

better and sooner reward our hardest working and most 

productive Executive Directors!</li>
</ol>


<p>Cumulatively, we now have a Compensation Plan that will synchronize well with how TNT works, to motivate and pay Partners 

to use TNT to build Trial Partner lines to gain new 

PRPs and GRPs, which can result in increasing sales, Team Pay Points and commissions!</p>





<hr/>-->
<!-- 
<p class="style28">Wednesday, July 16, 2014</p>
<p>It took a few months of reviews and development, but our Partners now have the best compensation plan that we have ever 

provided!</p>

<p>Because we have been able to move our HQ servers to a "cloud" and allow all employees to work from home, we have also 

been able to cut our HQ overhead expenses by 90%! And I 

have decided to pass most of these savings on to our Partners in the form of reduced GPS Coach's Fees and by increasing our 

overall commissions payout to Partners. </p>


<p><b>This is now the biggest percentage payout we have ever provided in our Partner Compensation Plan!</b></p>

<p>In addition to this, we will also be releasing a new policy and method this week to allow former Partners to reactivate 

their GPS subscriptions and return as Partners (so they 

can take advantage of the new Compensation Plan), simply by paying the current month's GPS BASIC Fee. Then next week we will 

release our marketing plan so that we can all 

communicate with former Partners to invite them to become Trial Partners and then Partners. Included in this initiative will 

be:<br/><br/>

<ul>
<li class="blue_bullet">Transfers of former Partners from inactive Sponsors to active Sponsors and/or Coaches.</li>
<li class="blue_bullet">Customized Affiliate Business Center and Trial Partner Report webpage changes to alert former 

Partners of our new policy and to be redirected from the 

Partner Upgrade Application links to a new webpage with information and instructions to reactivate their GPS subscriptions 

and Partner status.</li>
<li class="blue_bullet">A modification for the GPS List Generator (available to all PRO subscribers and Coaches) to enable 

them to gain a list of former Partners to contact, that 

are in their Group.</li>
<li class="blue_bullet">Targeted, special blast emails from HQ to former Partners that are currently Members/Affiliates or 

Trial Partners.</li>
</ul>

</p>
-->


 <!--<hr/>

<p class="style28">Thursday, July 3, 2014</p>

<p>Get ready for a Partner revival! I have decided to go ahead with modifying the Partner Compensation Plan this month, so 

that ALL Partners can start earning Team Commissions and 

so that Team Commissions are able to increase for ALL Partners, as your Team Pay Points increase! </p>
<p>We plan to implement these changes next week, but we first will need to remove the team line positioning part of the 

proposal and consequently, modify the entitlements to share 

in team commissions, from the proposal we were considering. So look for an announcement next week which will include the 

finalized, revised Compensation Plan for this exciting 

development!</p>
<p>Trial Partner invitation emails will start going out in English today and in other languages on Monday and Tuesday. And I 

will be hosting some webinars next week to explain 

everything so that you can join in on the excitement and create your own personal marketing plans to share the great news 

with your Members and to prospect with. </p>
<p><b>This may be the biggest and best improvement we have ever made to our Partner Compensation Plan!</b></p>
-->


<!--<hr/>


<p class="style28">Friday, June 27, 2014</p>
<p>This past month I shared the good news in ClubHouse meetings that we were hoping to release some important improvements 

to the Compensation Plan, effective for July. And I 

shared the proposal we had in place with some Coaches. Unfortunately, we ran into some programming and coordination issues 

that will prevent us from carrying out the proposed 

changes in July. So we be delaying implementing these changes until August 1st.</p>

<p>The two significant changes that will occur are:</p>
<ul>

<li class="blue_bullet">ALL Partners will be entitled to share in Team Commissions!</li>
<li class="blue_bullet">Team Commissions will increase for all pay levels, as Team Pay Points increase></li>

</ul>


<p>We expect that these improvements will significantly help to retain more Partners AND help to gain more new Partners!</p>
<p>In addition, we are working on an improvement that will coincide with the release of the Compensation Plan improvements 

to how the positions in the Team line can be improved 

for our most productive Partners. By improving your position in the team and moving up the line, a Partner can improve and 

increase their Team Pay Points and Team Commissions. 

</p>

<p>We do expect to be able to release another improvement in our Marketing Plan in July, to allow former GPS subscribers 

(from the past year) to renew their subscriptions simply 

by paying the current month's fees, rather than have to pay a new Initial Fee. So look to see an announcement about this 

next week. </p>
<p>Once former Partners learn that they will automatically start earning Team Commissions, we expect that many may return to 

be Partners to take advantage of the Compensation Plan 

improvements, now scheduled to go into effect in August.</p>
<p>Funtastic!</p>-->



<!--<p class="style28">Friday, May 23, 2014</p>

<p>I am back from a great vacation earlier this month, where I relaxed completely and celebrated my 40th wedding anniversary 

with my wife Pat. I literally did nothing for a week, 

which I really needed. The only work I did, was work on my tan! <img src="face_sunglasses.gif"/></p>

<p>This past week we drove up to "Ducks' Nest" here in the TN, US mountains, where I will be working from for most of the 

next six months. I have been catching up with things here 

and plan to resume my Executive Director role for a few teams on Monday. </p>
<p>I have ordered a new antenna to use with my wireless jetpack booster to improve the signal here at the lodge, so that I 

can talk on Skype and do webinars from my office here, 

beginning in June. Hopefully it will enable me to do so, as in the past, I have had to go to the top of the mountain here to 

get a good signal. </p>
<p>We have a few more improvements to make to TNT, GPS and the Compensation Plan by the end of June and I plan to spend most 

of my Summer writing eBooks to be used in the new 

eBusiness Institute program we will be offering later this year. I am very excited about this,. as it will also provide 

Partners with a new way to get paid to prospect for new 

Members! </p>
<p>The best is yet to come! </p>-->








<!--<p class="style28">Friday, May 2, 2014</p>
 
 <p>Today begins the start of a long awaited two week vacation for me! It has been more than a year since I have had a 

vacation and "all work and no play can make Dick a dull 

boy"! <img src="face_smile.gif"/></p>
 <p>My wife Pat and I will be celebrating our 40th wedding anniversary on May 11th on the Caribbean island of St. Martin and 

I don't have many other plans for our trip except to 

just relax. I'm sure that Pat will offer me a few of her "mystery novels" she loves to read, for me to read on the beach or 

by the pool, and I may just read one or two. But I am 

more likely to think and plan creatively about some of the ideas and projects that I hope to work on this summer while I am 

working from our retreat property in the mountains. 

</p>
 <p>I am excited about some TNT 2.1 improvements we have planned and very excited about working on a new sister program that 

will pay Partners to prospect for new members! So I 

will be taking along a few legal pads and good pens to continue the thinking and planning for these things, between naps and 

just soaking up some sun. <img 

src="face_sunglasses.gif"/></p>
 <p>I don't plan to respond to any emails for the next two weeks, so if any Partners that I am the Coach for need anything, 

please send your email to my assistant Lisa Young (<a 

href="mailto:supportspvr@clubshop.com">supportspvr@clubshop.com</a>).
</p>
  <p>I do plan to be back to work full time on Monday, May 19th. </p>
   <p>So let the R &amp; R begin! </p>
-->
 
 <!--

<p class="style28">Thursday, April 17, 2014</p>

<p>When I started to work as a Partner in January, one of my chief motivations was to see how TNT 2.0, your GPS and the 

Training Guide were all working and coordinated to provide 

you with a terrific marketing plan. </p>
<p>It has been a real learning experience for me and it has allowed me to see things from the perspective of a Partner, 

Sponsor, Coach and Executive Director. One of the things 

that I discovered along the way was that many new and reactivated Partners were struggling with creating template emails 

that looked good and with links that worked properly.
</p>
<p>This problem created other problems associated with the need to create and send template emails to new Members, 

Affiliates and Trial Partners. <br/><br/>

<ul>

<li class="blue_bullet">Unprofessional looking emails, many with bad links, were going out to new Members, Affiliates and 

Trial Partners. These emails were returning poor 

responses and results. </li>
<li class="blue_bullet">Many new and reactivated Partners quickly got discouraged and were unable to quickly gain confidence 

that they could correctly make the necessary first 

efforts to be successful in their business. </li>
<li class="blue_bullet">This in turn, created a bigger burden for the Sponsor or Coach to either spend more time training 

and motivating Partners or doing the work themselves.
</li>
<li class="blue_bullet">In many cases, neither has been done, leaving new Members Affiliates and Trial Partners without 

quickly seeing welcome emails to begin the process of being 

able to make a connection with their Sponsor and/or Coach.</li>



</ul>
</p>
<p>As I encountered these issues with new and reactivated Partners that I was coaching, I realized that we had to provide a 

solution to this problem to remove this first obstacle 

so that Partners could be able to get started and gain confidence quickly and so that Coaches could more successfully get 

Partners trained per the Partner Sponsoring Course so 

that they could see good duplication of the training, so that delegation of the role can occur! </p>
<p><b>DUPLICATION > DELEGATION > MULTIPLICATION!!!</b></p>
<p>Now we have the solution in place, as all Partners will no longer need to create template welcome emails! Rather, you can 

just use your GPS to create and send these emails! 

Just click on the little envelope <img src="https://www.clubshop.com/images/icons/envelope-16x12.png"/> shown for an 

activity in the Activity Report or Followup Record and you 

will be able to copy a personalized email, then click on the link shown to open a new email (with the subject in place) to 

paste and send! Simple, easy and professional!
</p>
<p>And there is another benefit that this improvement to GPS provides - new Partners will now be able to quickly see how 

powerful and beneficial their GPS is, which will increase 

the appreciation for their subscription! Super!! </p>
<p>We will be modifying the training courses next to remove the template welcome email instructions and emails, and provide 

new information on how to get these emails, and we will 

continue to add new template emails for each activity type that requires a followup email to be sent! </p>
<p>Funtastic! Use your improved GPS and Training Guide to "Follow the yellow brick road"! </p>

            
            
            
            <hr/>
            <p class="style28">Wednesday, April 2, 2014</p>
            
            <p>Spring time means anticipating and enjoying better weather conditions and new growth! It is also a time to 

plow the ground and plant new seeds.  Having TNT 2.0 in 

place now and working, provides you with new machinery and tools to plow new ground, plant new seed and see new growth for 

your businesses too! </p>

            <p>This year, I purposed to work over the Winter months to release and establish our TNT 2.0 marketing plan. 

Yesterday, I just completed my 90 day "mission" to work as 

a Partner Sponsor and Coach to personally sponsor some new Partners and to help a number of Partners learn how to use TNT 

2.0. As I worked as a Partner, I have been able to see 

from a Partner's perspective, what we can do to improve how TNT 2.0 and your GPS works. This has also enabled me to update 

and improve the training courses as well.
 </p>
            	
            <p>In the process of working as a Partner, I found myself being stretched way too thin and not being able to 

"focus and burn" the way one needs to, in order to achieve 

superior results. I just gained too many Partners to work with effectively, either in helping Group taproots grow as they 

are capable of, or in trying to spend time each week 

coaching active Partners and Coaches. Some Partners just didn't get the attention and help I intended and wanted to provide. 

</p>
            <p>Fortunately, some of the Partners that I coached have been able to achieve various degrees of success in 

their personal Partner sponsoring and are now able to help 

their PRPs and taproot GRPs to gain new Partners too. This allows me to have to meet with them less frequently and/or 

delegate their Group taproot building role to them, which 

frees up some of my time to be a better and more attentive Coach to the Partners that I am still the designated Coach for 

and to the Coaches that I am still the designated 

Executive Director for.
</p>
            <p>Hopefully, I will be able to delegate all of my coaching and Executive Director roles to "up and coming" 

Partners within the next 90 days, so I can return to a less 

strenuous work schedule while knowing that all Partners will have good Coaches and Executive Directors to work with. This 

week, I will be setting up and coordinating a new 

schedule to work with the Partners that I need to continue to work with, so that I can manage my time with more efficiency 

and effectiveness the next few months. </p>

<p>Many good things are happening for the Partners that are following the training courses to communicate with members, and 

sponsor and Coach Partners! </p>

<p>Congratulations to all the Partners that gained a promotion last month! </p>
            
<p>I am expecting the momentum to continue and increase in April, especially when we announce and release our next set of 

improvements to TNT this month! Maybe we will call them 

TNT 2.1? <img src="face_smile.gif"/></p>           
            
       -->     
            
            
            
            
            
            
            
    
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
           	<br/><br/><br/>	<br/><br/><br/>	<br/>
           <hr/>
           
           
            
            
            
            
                  
                  
            

 </div>        			<br/><br/><br/>
</div>
</div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row"> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2016
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>
  

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
