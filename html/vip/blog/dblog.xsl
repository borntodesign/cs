<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


  <xsl:template match="/">
    
         <html><head>
            <title>Dick &amp; Will Burke's Weblog</title>
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="https://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="https://www.clubshop.com/js/partner/app.js"></script>
    <script src="https://www.clubshop.com/js/partner/flash.js"></script>
    <script src="https://www.clubshop.com/js/panel.js"></script>



<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>



<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="https://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<div align="center"><h4><span class="topTitle"><xsl:value-of select="//lang_blocks/bb"/></span></h4>
	<p><span class="medblue"><xsl:value-of select="//lang_blocks/bb1"/></span></p></div>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">





                        <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            
            
            <!--<p><xsl:value-of select="//lang_blocks/google-disclaimer"/><br/><br/>
            <xsl:value-of select="//lang_blocks/google-instruction"/></p>-->
           <hr/>
           <p class="style28"><xsl:value-of select="//lang_blocks/may25"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may25b"/><xsl:text> </xsl:text> <img src="face_surprise.gif"/></p>
           <p><xsl:value-of select="//lang_blocks/may25c"/></p>
           <p><xsl:value-of select="//lang_blocks/may25d"/></p>
           <p><xsl:value-of select="//lang_blocks/may25e"/></p>
           <p><xsl:value-of select="//lang_blocks/may25f"/></p>
           
           <hr/>
           
           <p class="style28"><xsl:value-of select="//lang_blocks/may23"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may23a"/></p>
           <p><xsl:value-of select="//lang_blocks/may23b"/></p>
           <p><xsl:copy-of select="//lang_blocks/may23c/*|//lang_blocks/may23c/text()"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may23d"/></p>
           <p><xsl:value-of select="//lang_blocks/may23e"/></p>
           <p><xsl:value-of select="//lang_blocks/may23f"/></p>
           
           <ol>
           <li><xsl:value-of select="//lang_blocks/may23g"/></li>
           <li><xsl:value-of select="//lang_blocks/may23h"/></li>
           </ol>
           
           <p><xsl:copy-of select="//lang_blocks/may23i/*|//lang_blocks/may23i/text()"/></p>
           <p><xsl:value-of select="//lang_blocks/may23j"/></p>
           <p><xsl:value-of select="//lang_blocks/may23k"/></p>
           <p><xsl:copy-of select="//lang_blocks/may23l/*|//lang_blocks/may23l/text()"/></p>
           <p><xsl:value-of select="//lang_blocks/may23m"/></p>
           <p><xsl:value-of select="//lang_blocks/may23n"/></p>
           
          
           <hr/>
           <p class="style28"><xsl:value-of select="//lang_blocks/may17"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may17a"/></p>
           <p><xsl:value-of select="//lang_blocks/may17b"/></p>
           <p><xsl:value-of select="//lang_blocks/may17c"/></p>
           
           <ul>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may17d"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may17e"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may17f"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may17g"/></li>
           </ul>
           
           <p><xsl:value-of select="//lang_blocks/may17h"/></p>
           <p><xsl:value-of select="//lang_blocks/may17i"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/cs/customer_loyalty_program.shtml" target="_blank"><xsl:value-of select="//lang_blocks/may17j"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/may17k"/></p>
           <p><xsl:value-of select="//lang_blocks/may17l"/></p>
           
           <hr/>
            <!-- 
           <p class="style28"><xsl:value-of select="//lang_blocks/may11"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may11a"/></p>
           <p><xsl:value-of select="//lang_blocks/may11b"/></p>
           <p><xsl:value-of select="//lang_blocks/may11c"/></p>
          
           
           
           
           
           
           
           <hr/>
              <p class="style28"><xsl:value-of select="//lang_blocks/may4"/></p>
           
           <p><xsl:value-of select="//lang_blocks/may4a"/></p>
           <p><xsl:value-of select="//lang_blocks/may4b"/></p>
           <p><xsl:value-of select="//lang_blocks/may4c"/></p>
           <p><xsl:value-of select="//lang_blocks/may4d"/></p>
           
           <ul>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may4e"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may4f"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may4g"/></li>
           <li class="blue_bullet"><xsl:value-of select="//lang_blocks/may4h"/></li>
           
           </ul>
           
           <p><xsl:value-of select="//lang_blocks/may4i"/></p>
           <p><xsl:value-of select="//lang_blocks/may4j"/></p>
           <p><xsl:value-of select="//lang_blocks/may4k"/></p>
           <p><xsl:value-of select="//lang_blocks/may4l"/></p>
           <p><xsl:value-of select="//lang_blocks/may4m"/></p>
           <ol>
           
           <li><xsl:value-of select="//lang_blocks/may4n"/></li>
           <li><xsl:value-of select="//lang_blocks/may4o"/></li>
           
           </ol>
           
           
           <p><xsl:value-of select="//lang_blocks/may4p"/></p>
      		<p><xsl:value-of select="//lang_blocks/may4q"/></p>
            <p><xsl:value-of select="//lang_blocks/may4r"/></p>
           <p><xsl:value-of select="//lang_blocks/may4s"/></p>
           <p><xsl:value-of select="//lang_blocks/may4t"/></p>
           <p><xsl:value-of select="//lang_blocks/may4u"/><xsl:text> </xsl:text><a href="http://www.dhsclubkids.org/" target="_blank">http://www.dhsclubkids.org/</a></p>
           <p><xsl:value-of select="//lang_blocks/may4v"/></p>
    
           
           
           <p><xsl:value-of select="//lang_blocks/may4w"/></p>
           <p><xsl:value-of select="//lang_blocks/may4x"/></p>
    
           
           
           
           
           
           
           
           
           
           <hr/>
           
           <p class="style28"><xsl:value-of select="//lang_blocks/may3"/></p>
           
           <p><xsl:value-of select="//lang_blocks/p1"/></p>
           <p><xsl:value-of select="//lang_blocks/p2"/></p>
           <p><xsl:value-of select="//lang_blocks/p3"/></p>
           <p><xsl:value-of select="//lang_blocks/p4"/></p>
           
           
           <ol>
           
           	<li><xsl:copy-of select="//lang_blocks/p5/* | //lang_blocks/p5/text()"/><br/><br/><xsl:value-of select="//lang_blocks/p6"/></li>
           	
           	
           	<li><xsl:value-of select="//lang_blocks/p7"/></li>
           	
           	
           	<li><xsl:copy-of select="//lang_blocks/p8/* | //lang_blocks/p8/text()"/><br/><br/>
           	
           	
           	<xsl:value-of select="//lang_blocks/p9"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p10"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p11"/><br/><br/>
           	<xsl:copy-of select="//lang_blocks/p12/* | //lang_blocks/p12/text()"/>
           	</li>
           	
           	
           	
           	<li>
           	<xsl:value-of select="//lang_blocks/p13"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p14"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p15"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p16"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p17"/><br/><br/>
           	<xsl:copy-of select="//lang_blocks/p18/* | //lang_blocks/p18/text()"/>
           	
           	</li>
           	
           	
           	<li>
           	<xsl:value-of select="//lang_blocks/p19"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p20"/><br/><br/>
           	
           		<ul>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p21"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p22"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p23"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p24"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p25"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p26"/></li>
           		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p27"/></li>
           		</ul>
           		
           		<xsl:value-of select="//lang_blocks/p28"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p29"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p30"/>
           	</li>
           	
           	
           	<li>
           	
           	<xsl:copy-of select="//lang_blocks/p31/* | //lang_blocks/p31/text()"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p32"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p33"/><br/><br/>
           	
           		<ol>
           		<li><xsl:value-of select="//lang_blocks/p34"/></li>
           		<li><xsl:value-of select="//lang_blocks/p35"/></li>
           		<li><xsl:value-of select="//lang_blocks/p36"/></li>
           		</ol>
           	
           	<br/>
           	<xsl:value-of select="//lang_blocks/p37"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p38"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p39"/><br/><br/>
           	<xsl:copy-of select="//lang_blocks/p40/* | //lang_blocks/p40/text()"/>
           	
           	</li>
           	
           	
           	<li>
           	<xsl:value-of select="//lang_blocks/p41"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p42"/><br/><br/>
           	</li>
           	
           	
           	<li>
           	<xsl:value-of select="//lang_blocks/p43"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p44"/><br/><br/>
           	<xsl:value-of select="//lang_blocks/p45"/><br/><br/>
           	</li>
           	
           	
           	
           	
           	
           	
           	</ol>
           	
           	
           	<p><xsl:value-of select="//lang_blocks/p46"/></p>
           <p><xsl:value-of select="//lang_blocks/p47"/></p>
           <p><xsl:value-of select="//lang_blocks/p48"/></p>
           <p><xsl:value-of select="//lang_blocks/p49"/></p>
           <p><xsl:value-of select="//lang_blocks/p50"/></p>
           
           
           
           
           
           
           
           
           
           
           
            
           <hr/>
           
           <p class="style28"><xsl:value-of select="//lang_blocks/apr30"/></p>
            <p><xsl:value-of select="//lang_blocks/apr30a"/></p>
             <p><xsl:value-of select="//lang_blocks/apr30b"/></p>
             <p><xsl:value-of select="//lang_blocks/apr30c"/></p>
             <p><xsl:value-of select="//lang_blocks/apr30d"/></p>
              <p><xsl:value-of select="//lang_blocks/apr30e"/></p>
              -->
           
           
           
           
           
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <hr/>
            
            
            
            
            
                  
                  
            

 </div>        			<br/><br/><br/>
</div>
</div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row"> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>
  

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
