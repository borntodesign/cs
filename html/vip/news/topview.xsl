<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
         <title>View From the Top - DHS Club President Dick Burke's Monthly Newsletter</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p1"/></h4>

	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">





            <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<!--<p><xsl:value-of select="//lang_blocks/google-disclaimer"/><br/><br/>
<xsl:value-of select="//lang_blocks/google-instruction"/></p> -->
<hr/>
      <p><img src="images/DickB_Pic.gif" alt="DHS Club President and CEO, Dick Burke" width="82" height="115"/> </p>


<p class="style28"><xsl:value-of select="//lang_blocks/p1"/></p>

<p><xsl:value-of select="//lang_blocks/p2"/>


<ul>

<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p3/*|//lang_blocks/p3/text()"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p4/*|//lang_blocks/p4/text()"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p5/*|//lang_blocks/p5/text()"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p7/*|//lang_blocks/p7/text()"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/p8/*|//lang_blocks/p8/text()"/></li>

</ul>
</p>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<p><xsl:value-of select="//lang_blocks/p12"/></p>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<p><xsl:value-of select="//lang_blocks/p14"/><xsl:text> </xsl:text> <img src="images/face_smile.gif"/></p>
<p><xsl:value-of select="//lang_blocks/p15"/><xsl:text> </xsl:text> <img src="images/face_wink.gif"/></p>
<p><xsl:value-of select="//lang_blocks/p16"/></p>
<p><xsl:value-of select="//lang_blocks/p17"/></p>
<p><xsl:value-of select="//lang_blocks/p18"/></p>
<p><xsl:value-of select="//lang_blocks/p19"/>

<ul>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p20"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p21"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p22"/></li>

</ul>
</p>

<p><xsl:value-of select="//lang_blocks/p23"/></p>
<p><xsl:value-of select="//lang_blocks/p24"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<p><xsl:value-of select="//lang_blocks/p26"/></p>
<p><xsl:value-of select="//lang_blocks/p27"/></p>

<p>Dick Burke<br/>President</p>


</div>
</div>
<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
