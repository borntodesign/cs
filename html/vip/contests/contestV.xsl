<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title><xsl:value-of select="//lang_blocks/pp"/></title>


     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
    <link href="http://fonts.googleapis.com/css?family=Tienne:400,900" rel="stylesheet" type="text/css"/> 



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>

h4{text-indent: 15px;}

.special{
font-family: 'Tienne', sans-serif; 
font-size: 16px;}

</style>



</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>

	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
	<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>



<div align="right"><a href="contestIV.xml" target="_blank">April Contest</a></div>

<p class="style28"><xsl:value-of select="//lang_blocks/p1"/></p>

<ol>

<li class="half"><span class="special royal"><xsl:value-of select="//lang_blocks/p3"/></span></li>
<li class="half"><span class="special royal"><xsl:value-of select="//lang_blocks/p4"/></span></li>
<li class="half"><span class="special royal"><xsl:value-of select="//lang_blocks/p5"/></span></li>
<li class="half"><span class="special royal"><xsl:value-of select="//lang_blocks/p6"/></span></li>
<li class="half"><span class="special royal"><xsl:value-of select="//lang_blocks/p7"/></span></li>	
</ol>




<hr/>
</div></div>
<div class="row">
<div class="ten columns offset-by-two">

Results as of: 5/31/12 4:00 PM EDT<br/><br/>

<table width="100%" border="1" cellpadding="4" cellspacing="1" bgcolor="#999999">



<tr class="d">
<td><span class="style24"><xsl:value-of select="//lang_blocks/p11"/></span></td>
<td><span class="style24"><xsl:value-of select="//lang_blocks/p12"/></span></td>
<td><span class="style24"><xsl:value-of select="//lang_blocks/p14"/></span></td>
</tr>




<tr>
<td><img src="prize1.png"/><br/><h4 class="darkblue">$500</h4></td>
<td class="style28" style="padding-top:70px;">Sidi Zidani</td>
<td class="style28" style="padding-top:70px;">8</td>
</tr>

<tr>
<td><img src="prize2.png"/><br/><h4 class="darkblue">$400</h4></td>
<td class="style28" style="padding-top:70px;">Marcel Maronko</td>
<td class="style28" style="padding-top:70px;">5</td>
</tr>


<tr>
<td><img src="prize3.png"/><br/><h4 class="darkblue">$300</h4></td>
<td class="style28" style="padding-top:70px;">Lorenzo Cicetti</td>
<td class="style28" style="padding-top:70px;">4</td>
</tr>


<tr>
<td><img src="prize4.png"/><br/><h4 class="darkblue">$200</h4></td>
<td class="style28" style="padding-top:70px;">TIE: Christian Vanoeteghem, Cece Simeoni, Fabrizio Perotti, Belinda Riquelme Rodriguez, Imer Zoboli and Antonella Bottelli</td>
<td class="style28" style="padding-top:70px;">3</td>
</tr>

<tr>
<td><img src="prize5.png"/><br/><h4 class="darkblue">$100</h4></td>
<td class="style28" style="padding-top:70px;"></td>
<td class="style28" style="padding-top:70px;"></td>
</tr>


<tr>
<td><img src="prize6.png"/><br/><h4 class="darkblue">$80</h4></td>
<td class="style28" style="padding-top:70px;"></td>
<td class="style28" style="padding-top:70px;"></td>
</tr>


<tr>
<td><img src="prize7.png"/><br/><h4 class="darkblue">$60</h4></td>
<td class="style28" style="padding-top:70px;"></td>
<td class="style28" style="padding-top:70px;"></td>

</tr>


<tr>
<td><img src="prize8.png"/><br/><h4 class="darkblue">$40</h4></td>
<td class="style28" style="padding-top:70px;"></td>
<td class="style28" style="padding-top:70px;"></td>
</tr>


<tr>
<td><img src="prize9.png"/><br/><h4 class="darkblue">$20</h4></td>
<td class="style28" style="padding-top:70px;"></td>
<td class="style28" style="padding-top:70px;"></td>
</tr>


<tr>
<td><img src="prize10.png"/><br/><h4 class="darkblue">$10</h4></td>
<td class="style28" style="padding-top:70px;">TIE: Jeanine Uwitonze, Mohamed Amine Tabet, Marie France Santucci, Patrick Willems, Giuseppe Narduzzi, Gino Descamps, Tiberio Guglielmi,
Nataliya Stechyshyn, Elena Aiani, Aline Kayange, Dhouaibi Fayçal, Scott Secord and Laurence Colle </td>
<td class="style28" style="padding-top:70px;">2</td>

</tr>


</table>








<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
