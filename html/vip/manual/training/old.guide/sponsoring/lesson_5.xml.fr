﻿<?xml-stylesheet type="text/xsl" href="lesson_5.xsl" ?><lang_blocks>  <lesson>Leçon 5</lesson>
  <next>Suivant</next>
  <p10>VOTRE NOM</p10>
  <p11>Lien de site internet personnel</p11>
  <p2>Réunion avec les potentiels prospects</p2>
  <p23>EMAIL - Message téléphonique laissé</p23>
  <p36>SKYPE</p36>
  <p37>Une fois qu'un prospect vous a ajouté en tant que contact Skype, vous devriez lui envoyé un IM (messagerie instantanée) afin de déterminer quel est leur meilleur moment pour les rencontrer, comme ce qui suit:</p37>
  <p38>Je vérifie juste si vous êtes en ligne et si vous êtes disponible pour discuter pendant quelques minutes? Je voudrais voir si vous comprenez comment fonctionne notre Opportunité d'Affaire Partner et répondre aux questions que vous pourriez avoir. Si nous ne pouvons pas parler maintenant, n'hésitez pas à me contacter via Skype lorsque vous voyez ce que je suis en ligne. A quels moments êtes-vous habituellement en ligne et disponible pour discuter?</p38>
  <p39>Documenter ceci dans leur dossier de suivi et mettre en place un rappel de suivi pour 3 jours plus tard.</p39>
  <p4>SKYPE</p4>
  <p40>S'ils ne répondent pas à votre messagerie instantané (IM) dans les trois prochains jours, essayer de les appeler si vous voyez qu'ils sont en ligne. Si vous êtes incapable de parler avec eux, envoyer leur à nouveau un IM:</p40>
  <p41>Je voudrais vraiment parler avec vous pendant quelques minutes pour voir si vous comprenez bien comment fonctionne notre Opportunité d'Affaire Partner et répondre aux questions que vous pourriez avoir. Surtout n’hésitez pas à m'appeler via Skype si vous voyez que je suis en ligne. A quels moments êtes-vous habituellement en ligne et disponible pour discuter?</p41>
  <p42>Documenter ceci dans leur dossier de suivi et mettre en place un rappel de suivi pour 5 jours plus tard (pour faire varier le jour de la semaine où vous essayez de parler avec eux) pour essayer de les appeler. Si vous êtes incapable de parler avec eux, il suffit de continuer de leur envoyer le même IM.</p42>
  <p43>Continuez de faire le suivi avec eux jusqu'à ce qu'ils assistent à un Webinaire, qu'ils discutent avec vous ou qu'ils soient rétrograder pour être un membre. Un bon suivi vous fournira beaucoup plus de nouveaux Partners! En étant persistant, vous montrez à vos membres que vous avez une forte croyance dans l'Opportunité de Revenu Partner et en vous-même - les deux choses que vos potentiels Partners veulent et ont besoin comme parrain / entraîneur d'équipe !</p43>
  <p44>TELEPHONE</p44>
  <p45>Si le potentiel prospect n'a pas un bon nom Skype, alors vous devriez essayer de parler avec lui par téléphone. Utilisez Skype pour faire les appels téléphonique longue distance et les appels internationaux pour économiser</p45>
  <p46>Si vous ne parvenez pas à joindre le potentiel prospect et vous pouvez lui laisser un message, laisser un message simple:</p46>
  <p47>C'est</p47>
  <p47a>du ClubShop Rewards. Je vous appelle pour vous souahiter la bienvenue au ClubShop Rewards et pour vous inviter à assister à une présentation sur l'Opportunité d'Affaires afin que vous puissiez voir comment fonctionne notre l'Opportunité d'Affaires Partner. Puisque je ne peux pas parler avec vous, je vous enverrai un Email avec un lien vers votre Centre d'Affaires Affiliate et vers un formulaire de réservation pour une présentation afin que vous puissiez réserver une place. Je suis désolé de vous avoir manqué et j'espère que nous pourrons bientôt nous parler. Au revoir.</p47a>
  <p48>Gardez ce &quot;message&quot; très pratique pour l'utiliser à nouveau lors d'une tentative afin d'établir un contact téléphonique.</p48>
  <p49>Si une autre personne répond au téléphone, faites en sorte de savoir quand le potentiel prospect sera disponible pour parler avec mui, puis il suffit de laisser votre nom et leur dire que vous enverrez un Email au potentiel prospect .</p49>
  <p50>Documenter ceci dans leur dossier de suivi, y compris le bon moment pour leur appeler, le cas échéant. Mettre en place un Rappel de suivi en conséquence.</p50>
  <p51>Je viens juste d'essayer de vous appeler, mais je n'ai pas pu vous joindre et j'ai dû laisser un message. Je tenais à vous accueillir personnellement au ClubShop Rewards et à vous inviter à assister à une présentation sur l'Opportunité d'Affaires, afin que vous puissiez voir comment fonctionne notre passionnante d'Affaires Partner !</p51>
  <p52>La présentation de l'Opportunité d'Affaires est prévue pour</p52>
  <p52a>JOUR à HEURE.</p52a>
  <p52b>Allez ici pour réserver une place:</p52b>
  <p53>Si vous êtes dans l'impossibilité d'y assister, faites le moi savoir quels sont les bons jours/heures pour que je puisse vous appeler pendant environ 30 minutes afin que nous puissions en discuter.</p53>
  <p54>et connectez vous. Votre numéro ID est: &lt;b&gt;IDNUMBER&lt;/b&gt;</p54>
  <p55>J'espère vous rencontrer bientôt !</p55>
  <p56>Merci de ne pas hésiter à m'envoyer toutes questions que vous pouvez avoir.</p56>
  <p57>Notez cela dans leur dossier de suivi, puis continuez à envoyer les Emails de suivi adéquat chaque semaine. Continuez à faire un suivi avec eux jusqu'à ce qu'ils parlent avec vous ou qu'ils soient rétrograder en tant que membre.</p57>
  <p58>Un bon suivi vous fournira de nombreux nouveaux Partnrts!  En étant persévérant vous montrez à vos Membres que vous avez une forte confiance dans notre Opportunité de revenus pour Partners et en vous-même - deux choses dont vos Partners Prospects ont besoin et qu’ils recherchent chez leur Parrain/Entraîneur d'équipe !</p58>
  <p59>CONTACT ETABLI</p59>
  <p6>Bonjour</p6>
  <p60>Votre objectif lorsque vous appelez les gens n'est pas d'essayer d'expliquer comment notre Opportunité d'Affaires fonctionne ou de répondre à leurs questions. Il s'agit plutôt de les inviter à assister à une présentation de Opportunité d'Affaires ou si ils sont incapables d'y assister, de prendre un rendez-vous avec eux pendant 30 minutes. Lorsque vous les appelez, assurez-vous que vous avez leur dossier de suivi d'Affiliate ouvert afin de vous y référer.</p60>
  <p61>Votre liste de contrôle lors de la conversation téléphonique:</p61>
  <p62>Présentez-vous et invitez-les à assister à une présentation sur l'Opportunité d'Affaires. &quot;Bonjour NOM. C'est VOTRE NOM et je suis un Partner ClubShop Rewards. Je veux juste vous souhaiter la bienvenue au ClubShop Rewards et vous inviter à participer à une Présentation en  ligne sur l'Opportunité d'Affaires. Seriez-vous disponible pour venir à ce webinaire en ligne JOUR à HEURE ?</p62>
  <p63>Si NON, demandez-leur quand est le bon moment pour vous rencontrer pendant environ 30 minutes afin de pouvoir leur expliquer comment fonctionne notre Opportunité d'Affaires Partner. &quot;Ok, alors laissez-moi voir quand je peux revenir vers vous, afin que nous puissions parler de comment cela fonctionne. Quels jours/heures de la semaine pouvez-vous me rencontrer pendant environ 30 minutes&quot;. Essayez de trouver un temps commun pour vous rencontrer.</p63>
  <p63b>Une fois que vous avez pris un rendez-vous, demandez-leur s'ils ont Skype. Si oui, expliquez que ce serait mieux de vous rencontrer via Skype afin de pouvoir partager votre écran avec eux. S'ils n'ont pas Skype, dites-leur que vous allez leur envoyer de l'information pour l'obtenir gratuitement, et si vous êtes capable d'utiliser Skype, vous les rencontrerez dessus. Sinon, vous les appelerez et leur expliquerez les choses pendant qu'ils sont en ligne.</p63b>
  <p63c>Si vous ne pouvez pas convenir d'un bon moment, voyez s'ils peuvent venir à votre prochaine réunion d'équipe où vous présenterez l'Opportunité d'Affaires. Dites-leur que vous allez leur envoyer le lien pour s'inscrire. Envoyer un Email de suivi et affichez leurs dossiers de suivi pour le jour et l'heure pour les rappeler et mettez en place une date de rappel.</p63c>
  <p64>Si OUI, remerciez-les pour leur temps et dites-leur de vérifier leur Email, car vous allez leur envoyer le lien pour qu'il inscrive à la réunion.</p64>
  <p65>EnvoyeZ un Email de suivi qui contient cette déclaration: «Si quelque chose arrive et que vous êtes incapable de venir à notre rendez-vous, merci de me prévenir à l'avance, afin que je puisse ajuster mon agenda et que nous puissions fixer un nouveau rendez-vous pour se rencontrer. Merci! &quot;. Puis postez leur dossier et définisez un suivi à la date de la réunion et envoyez un autre email de rappel ou un message Skype le matin du rendez-vous.</p65>
  <p6a>PRENOM,</p6a>
  <pindex>Index du cours</pindex>
  <pp>Partner Sponsoring Course</pp>
  <previous>Précédent</previous>
</lang_blocks>
