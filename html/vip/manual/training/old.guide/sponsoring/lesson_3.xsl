<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>Partner Sponsoring Course</title>
<script type="text/javascript" src="/cgi/PSA/script/8"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>

     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>






</head>

<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">
	
	
	
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p11"/></b></span>

<hr/>


<span class="liteGrey"><b><xsl:value-of select="//lang_blocks/lesson2"/></b></span>
<br/><br/>
<ol>

<li><span class="style24"><xsl:value-of select="//lang_blocks/p12"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>

	<ol>
	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p14"/></li>
	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p15"/></li>
	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p17"/></li>

		<ol>
		<li class="half"><xsl:value-of select="//lang_blocks/affjoin"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/mbraff"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/cooptrans"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/visitedlogin"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/bcaccess"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/visitedlogin"/></li>
		</ol>
		
	<li class="blue_bullet medblue"><xsl:value-of select="//lang_blocks/p22"/></li><br/>
	
	</ol>
	
	



<li><span class="style24"><xsl:value-of select="//lang_blocks/p23"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24"/>
	<ol>
	<li><xsl:value-of select="//lang_blocks/p25"/></li>
	<li><xsl:value-of select="//lang_blocks/bcaccess"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bcaccess_desc"/></li>
	<li><xsl:value-of select="//lang_blocks/mbraff"/></li>
	<li><xsl:value-of select="//lang_blocks/p28"/></li>
	<li><xsl:value-of select="//lang_blocks/affjoin"/></li>
	<li><xsl:value-of select="//lang_blocks/cooptrans"/></li>
	<li><xsl:value-of select="//lang_blocks/visitedlogin"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/visitedlogin_desc"/></li>
	
	</ol>
</li>	

<li><span class="style24"><xsl:value-of select="//lang_blocks/p29"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p30"/></li>

	
	<ol><li class="blue_bullet medblue"><xsl:value-of select="//lang_blocks/p31"/></li>
			<ol>
			<li class="half"><xsl:value-of select="//lang_blocks/p32"/></li>
			<li class="half"><xsl:value-of select="//lang_blocks/p33"/></li>
			<li class="half"><xsl:value-of select="//lang_blocks/p34"/></li>
			<li class="half"><xsl:value-of select="//lang_blocks/p35"/></li>
			</ol>
	</ol>		



<li><span class="style24"><xsl:value-of select="//lang_blocks/p36"/></span></li>
	
		<ol>
			<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p37"/><br/><br/>
			<b><xsl:value-of select="//lang_blocks/p38"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p40"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p41"/><br/></li><br/>
			
			<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p43"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p44"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p45"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p46"/>
			</li>
			
			
			
			</ol>
		
<li><span class="style24"><xsl:value-of select="//lang_blocks/p47"/></span></li>
			
			<ol>
			<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p48"/></li>
			
			</ol>
	
</ol>


<p><xsl:value-of select="//lang_blocks/p42"/></p>


<div align="center"> <a href="lesson_2.xml"><xsl:value-of select="//lang_blocks/previous"/></a><xsl:text> </xsl:text>|<xsl:text> </xsl:text><a href="lesson_4.html"><xsl:value-of select="//lang_blocks/next"/></a></div>




<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2013
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
