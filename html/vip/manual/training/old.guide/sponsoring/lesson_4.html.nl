﻿<!DOCTYPE html>
 <html>   
         <head>
            <title>Partner Sponsoring Course</title>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
	 <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation2.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />

    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->

<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/sprintf.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.capitalize {text-transform: uppercase;}
.smaller_text{font-size: 10px;
line-height: 15px;}
</style>

<script language="javascript">

   

	$(document).ready(function() {

	var showText='Learn More';

	var hideText='Hide';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

 

</script>

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script>
</head>

<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<div align="left">
<div id="google_translate_element" style="margin-top:0.5em;"></div>
<div align="left">
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
<hr />
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns">
<h4 class="Mainline">Partner Sponsoring Cursus</h4>
<hr />
<span class="liteGrey"><strong>Les 4</strong></span> <br /> <br />
<p><span class="ContactForm_TextField"><strong>Contact opnemen met Prospects</strong></span></p>
<p>Uw primaire doel wanneer u contact opneemt met Affiliates, is hen een Zakelijke Mogelijkheid Presentatie te laten zien en horen, die oftewel gehost wordt door uw Executive Director als onderdeel van hun weekelijkse Team Meeting, of die persoonlijk wordt gegeven door uw Coach of uzelf. De meeste prospects hebben meer dan 1 contactpoging nodig om een reactie van hen te krijgen, dus u kunt hen uitnodigen om "het plan te zien".</p>
<p>Click on the Webinars tab at your Business Center to see a list and the links for all the webinars that are scheduled for the next 30 days.</p>

<p>Volg deze eenvoudige 4 stappen procedure, om het grootst mogelijke aantal reacties te krijgen van uw Affiliates, zodat u hen "het plan" kan laten zien:</p>
<ol>
<li>Stuur een Welcome E-mail en Skype-verzoek en zet dit in hun Follow Up Record </li>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Zodra u hebt vastgestelt of de Affiliate een goede Skype naam heeft, stuurt u 1 van de 2 onderstaande e-mails om hen te verwelkomen en vraag een reactie van hen.</p>
<p><span class="bold">Welkom E-mail voor Affiliates (geen skype naam)</span></p>
<p>Subject: Aan <span class="style3">VOORNAAM (ID #)</span> van uw ClubShop Rewards Sponsor</p>
Hallo <span class="style3">VOORNAAM</span>Sta mij alstublieft toe mijzelf voor te stellen. Mijn naam is <span class="style3">UW NAAM</span> en ik ben een ClubShop Rewards Partner en uw Sponsor/Coach.
<p> </p>
<p>Ik wil u helpen om zo optimaal mogelijk uw voordeel te doen van uw lidmaatschap bij ClubShop Rewards door u te helpen geld te besparen en geld te verdienen als Affiliate, of om uw eigen ClubShop Rewards zaak te bezitten als Partner, zoals ik.</p>
<p>Hier is de link naar uw Affiliate Business Center, waar u toegang heeft tot het ClubShop Rewards Winkelcentrum en waar u informatie kunt vinden over het geld terug verdienen op uw aankopen en over hoe u geld kunt verdienen als Affiliate of Partner: https://www.clubshop.com/cgi/apbc?alias=<span class="style3">IDNUMMER</span></p>
<p>I would like to invite you to attend an online Business Opportunity Presentation on <span class="style3">DATE</span> at <span class="style3">TIME</span>, so you can learn more about how you can make money and have your own Partner ClubShop business. Here is the link to register:<span class="style3">LINK</span></p>

<p>If you are unable to attend, let me know what days/times are good for me to call you, so we can talk about how our Partner Business Opportunity works. I look forward to meeting you and answering any questions you may have!</p>

<p>Ik kijk er naar uit om met u af te spreken en u te helpen van start te gaan!</p>
<p>Met vriendelijke groeten,<br /><br /> <span class="style3">UW NAAM</span><br /> ClubShop Rewards Partner</p>
<hr />
<p class="bold">Welkom e-mail voor Affiliates (skype naam)</p>
<p>Onderwerp: Aan <span class="style3">VOORNAAM (ID #)</span> van uw ClubShop Rewards Sponsor</p>
<p>Hallo <span class="style3">VOORNAAM</span>,</p>
<p>Graag stel ik mij aan u voor. Mijn naam is <span class="style3">UW NAAM</span> en ik ben een ClubShop Rewards Partner en uw Sponsor/Coach.</p>
<p>Ik wil u helpen om zoveel mogelijk voordeel te halen uit uw lidmaatschap bij ClubShop Rewards door u te helpen geld te besparen en geld te verdienen als een Affiliate of bij het bezitten uw eigen ClubShop Rewards zaak als Partner, net als ik.</p>
<p>Hier is de link naar uw Affiliate Business Center, waar u toegang heeft tot het ClubShop Rewards Winkelcentrum en waar u informatie kunt vinden over geld terug verdienen op uw aankopen en over hoe u geld kunt verdienen als Affiliate of Partner: https://www.clubshop.com/cgi/apbc?alias=<span class="style3">IDNUMBER</span></p>
<p>I would like to invite you to attend an online Business Opportunity Presentation on <span class="style3">DATE</span> at <span class="style3">TIME</span>, so you can learn more about how you can make money and have your own Partner ClubShop business. Here is the link to register:<span class="style3">LINK</span></p>

<p>If you are unable to attend, let me know what days/times are good for me to call you, so we can talk about how our Partner Business Opportunity works. I look forward to meeting you and answering any questions you may have!</p>

<p>Ik zie dat u ook gebruik maakt van Skype. Dat is geweldig, want nu kunnen we afspreken op skype. Ik stuur u een verzoek mij als contact toe te voegen op skype.</p>
<p>Ik kijk er naar uit om met u af te spreken en u te helpen van start te gaan!</p>
<p>Met vriendelijke groeten,<br /><br /> <span class="style3">UW NAAM</span><br /> ClubShop Rewards Partner</p>
<hr />
<p class="bold">Indien de Affiliate een Skypenaam heeft, stuur dan tevens dit verzoek om u toe te voegen aan hun skype contactpersonen (inplaats het standaardverzoek dat wordt geboden door Skype):</p>
<p>Hallo <span class="style3">VOORNAAM!</span></p>
<p>Voeg mij alstublieft toe als contact op Skype zodat we kunnen afspreken en elkaar kunnen spreken over de ClubShop Rewards Zakelijke Mogelijkheid. Ik kijk er naar uit u te spreken!</p>
<br />
<p>Nadat u bovenstaan de e-mail /Skype-verzoek hebt verstuurd, zet u dit als volgt in hun Follow Up record:</p>
<ul>
<li class="blue_bullet">Opvolg notities: Welkom e-mail verstuurd (en Skype-verzoek - indien zij een Skype naam hebben)</li>
<li class="blue_bullet">Herinnering notities: opvolgen voor reactie</li>
<li class="blue_bullet">Herinnering datum: Instellen voor 3 dagen later</li>
</ul>
<p>Na drie dagen verschijnt deze herinnering in uw Activiteiten Rapport, zodat u verder kunt gaan met de Opvolg procedure indien zij geen reactie hebben gegeven op uw welkom e-mail en/ of Skype-verzoek.</p>
</div>
</div>
</div>
</div>
<li>Stuur een Opvolg E-mail en Skype-verzoek en zet dit in hun Follow Up Record</li>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Wanneer u geen reactie hebt ontvangen op uw welkom e-mail of Skype Contactverzoek, stuur uw Affiliate dan onderstaande e-mail en Skype bericht.</p>
<p class="bold">Opvolg e-mail (geen Skypenaam) - Haal uw welkom e-mail terug uit uw map met Verzonden e-mails (de datum van verzenden zou in uw Followup Record moeten staan) en stuur deze door naar de Affiliate met dit bericht:</p>
<p>Hallo <span class="style3">VOORNAAM</span>,</p>
<p>Did you receive my email below? I didn't see you attend the Business Opportunity Presentation and I haven't received a response from you yet. I really want to get together with you and talk about the ClubShop Rewards Partner Business Opportunity!</p>

<p>If you are seriously looking for a legitimate way to make money by having your own home based business, ClubShop Rewards may be just the vehicle you are looking for! This works and I am committed to helping the Partners that I refer and Coach, to succeed. When we get together I want to share with you the benefits of our Global Partner System (GPS) and how TNT and the Team Pools work to help you create a vested, residual income.</p>

<p>Er is geen risico aan verbonden om Partner te worden en dit kan zomaar de mogelijkheid zijn waar u naar op zoek bent!</p>
<p>Laat me weten welke dagen en tijden u vrij bent zodat we een afspraak kunnen maken. Wie niet waagt, die niet wint! :)</p>
<p>Met vriendelijke groeten,</p>
<p class="style3">UW NAAM</p>
<p>P.S. - Heeft u Skype? Zo ja, voeg mij dan toe aan uw contacten: <span class="style3">UW SKYPENAAM</span></p>
<hr />
<p class="bold">Opvolg e-mail (Skype naam) - Haal uw welkom e-mail terug uit uw map met Verzonden e-mails en stuur deze door naar de Affiliate met dit bericht:</p>
<p>Hallo <span class="style3">VOORNAAM</span>,</p>
<p>Heeft u mijn onderstaande e-mail ontvangen? Ik heb nog geen reactie van u gekregen en ik wil echt graag met u afspreken om te praten over de ClubShop Rewards Partner Zakelijke Mogelijkheid.</p>
<p>Wanneer u serieus op zoek bent naar een legitieme manier om geld te verdienen door het hebben van uw eigen zaak vanuit huis, kan ClubShop Rewards de manier zijn om dit te realiseren! Dit werkt en ik heb me er op toegelegd de Partners, die ik heb verwezen en Coach, te helpen om succesvol te worden. Tijdens ons afspraak zal ik u de voordelen laten zien van ons Global Partner Systeem (GPS), hoe het Marketing &amp; Compensatieplan werkt, de "Minimum Commissie Garantie" en de "Geld Terug Garantie".</p>
<p>Er is geen risico aan verbonden om Partner te worden en dit kan zomaar de mogelijkheid zijn waar u naar op zoek bent!</p>
<p>Laat me weten welke dagen en tijden u gewoonlijk vrij bent om te praten zodat we kunnen afspreken op Skype, of ik kan u bellen. Wie niet waagt, die niet wint! :)</p>
<p>Met vriendelijke groeten,</p>
<p class="style3">UW NAAM</p>
P.S. - Ik stuurde u ook een verzoek via Skype om mij toe te voegen als Skype contact. Mijn Skypenaam is <span class="style3">UW SKYPENAAM</span>
<p> </p>
<hr />
<p class="bold">Opvolgen Skypeverzoek</p>
<p>Hallo <span class="style3">VOORNAAM</span>!</p>
<p>Kunt u mij toevoegen als contactpersoon op skype, zodat we kunnen afspreken om te praten over de ClubShop Rewards Zakelijke Mogelijkheid? Ik wil er zeker van zijn dat u alle informatie hebt die nodig is om een goede beslissing te nemen!</p>
<span class="style3">UW NAAM</span> - ClubShop Rewards
<p> </p>
<hr />
<p>Nadat u bovenstaande e-mail/ Skype-verzoek hebt verstuurd, zet u dit als volgt in hun Opvolg Record:</p>
<ul>
<li class="blue_bullet">Opvolg notities: 1ste Opvolg e-mail verstuurd (en Skype-verzoek - indien zij een Skype naam hebben)</li>
<li class="blue_bullet">Herinnering notities: opvolgen voor reactie</li>
<li class="blue_bullet">Herinnering datum: Instellen voor 3 dagen later</li>
</ul>
<p>Na drie dagen verschijnt deze herinnering in uw Activiteiten Rapport, zodat u verder kunt gaan met de Opvolg procedure indien zij geen reactie hebben gegeven op uw 1ste opvolg e-mail en/ of Skype-verzoek.</p>
</div>
</div>
</div>
</div>
<li>Stuur een 2de opvolg e-mail en zet dit in hun Opvolg Record </li>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Indien u geen reactie hebt ontvangen op uw Opvolg e-mail of Skype contact-verzoek, stuur uw Affiliate dan onderstaande e-mail.</p>
<span class="bold">2de Opvolg e-mail - Haal uw welkom e-mail terug uit uw map met Verzonden e-mails (de datum van verzenden zou in uw Followup Record moeten staan) en stuur deze door naar de Affiliate met dit bericht:</span>
<p>Hi <span class="style3">VOORNAAM</span>,</p>
<p>Did you receive my email below? I didn't see you attend the Business Opportunity Presentation and I haven't received a response from you yet. I really want to get together with you and talk about the ClubShop Rewards Partner Business Opportunity!</p>

<p>If you are seriously looking for a legitimate way to make money by having your own home based business, ClubShop Rewards may be just the vehicle you are looking for! This works and I am committed to helping the Partners that I refer and Coach, to succeed. When we get together I want to share with you the benefits of our Global Partner System (GPS) and how TNT and the Team Pools work to help you create a vested, residual income.</p>

<p>Beantwoord alstublieft deze e-mail en laat me weten welke dagen/ tijden het best voor u uitkomen om af te spreken, oke?</p>
<p>Heel erg bedankt, ik kijk uit naar uw reactie,</p>
<p><span class="style3">UW NAAM</span> - ClubShop Rewards</p>
<p>Na het versturen van bovenstaande e-mail, zet u dit als volgt in hun Opvolg Record:</p>
<ul>
<li class="blue_bullet">Opvolg notities: 2de Opvolg e-mail verstuurd (en Skype-verzoek - indien zij een Skype naam hebben)</li>
<li class="blue_bullet">Herinnering notities: opvolgen voor reactie</li>
<li class="blue_bullet">Herinnering datum: Instellen voor 3 dagen later</li>
</ul>
<p>Na drie dagen verschijnt deze herinnering in uw Activiteiten Rapport, zodat u verder kunt gaan met de Opvolg procedure indien zij geen reactie hebben gegeven op uw 2de opvolg e-mail.</p>
</div>
</div>
</div>
</div>
<li>Stuur een SMS tekstbericht en/ of bel hen en zet dit in hun Opvolg Record</li>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Indien u geen reactie krijgt op uw 2de Opvolg e-mail, controleer dan in hun Opvolg Record of uw Affiliate een mobiel telefoonnummer heeft opgegeven. Indien dit het geval is, stuur dan onderstaand SMS tekst bericht naar hun mobiel. Gebruikt u skype om SMS tekstberichten te versturen, dan kunt u onderstaand bericht kopieren en plakken (gebruik de Control + V toetsen om te plakken) om te verzenden.</p>
<p><span class="style3">VOORNAAM</span><br /><br />Ik heb u een aantal e-mails gestuurd naar <span class="style3">E-MAIL ADRES</span> over ons ClubShop Zakelijke Mogelijkheid. <br /><br />Indien u interesse heeft, stuur dan een reactie naar <span class="style3">UW E-MAIL ADRES</span>.<br /><br /><span class="style3"> UW NAAM</span></p>
</div>
</div>
</div>
</div>
</ol>
<p>Krijgt u, na de vier bovenstaande pogingen om contact te maken, nog steeds geen reactie, dan zou u moeten stoppen met contact te zoeken en met u af te spreken, totdat en tenzij hun Affiliate Business Center bezoeken. Indien en wanneer een Affiliate die u in het verleden hebt opgevolgd, diens Business Center bezoekt, dan zou u bovenstaande vier stappen procedure moeten herhalen, maar gebruik een ander e-mail adres dan die u gebruikt hebt in bovenstaande stap 1.</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Subject: To <span class="style3">VOORNAAM(ID #)</span> van uw ClubShop Rewards Sponsor</p>
<p>Hallo <span class="style3">VOORNAAM</span>,</p>
<p>Ik zie dat u onlangs uw Affiliate Business Center hebt bezocht, misschien om meer informatie te krijgen over hoe u uw ClubShop Rewards lidmaatschap kunt gebruiken om geld te verdienen. Is dit het geval, laten we dan samen afspreken en hierover praten. Ik ben er zeker van dat ik u kan helpen om van start te gaan geld te verdienen met uw lidmaatschap, dus laten we erover praten hoe dit te doen, oke?</p>
<p>Welke dagen/ tijden komen voor u goed uit om af te spreken? Laat het me weten en ik ga kijken of ik u kan inplannen.</p>
<p>Indien u Skype gebruikt, dan kunnen we een skype afspraak maken.</p>
<p>Ik kijk er naar uit u te spreken en u van start te helpen!</p>
<p>Vriendelijke groet,<br /><br /> <span class="style3">UW NAAM</span><br /> ClubShop Rewards Partner</p>
<hr />
<p>Nadat u bovenstaande e-mail hebt verstuurd, zet u dit als volgt in hun Opvolg record:</p>
<ul>
<li class="blue_bullet">Opvolg notities: Business Center bezocht e-mail gestuurd</li>
<li class="blue_bullet">Herinnering notities: opvolgen voor reactie</li>
<li class="blue_bullet">Herinnering datum: Instellen voor 3 dagen later</li>
</ul>
<p>Na drie dagen verschijnt deze herinnering in uw Activiteiten Rapport, zodat u verder kunt gaan met de Opvolg procedure (startend met Stap 2), wanneer zij niet reageren op uw Business Center bezocht e-mail.</p>
</div>
</div>
</div>
</div>
<hr />
<div align="right"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><a href="lesson_5.xml" target="_blank">Volgende</a></div>
<br /><br /><br /> <br /><br /><br /> <br /><br /><br /> <br /><br /><br /> <br /></div>
</div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2013           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>