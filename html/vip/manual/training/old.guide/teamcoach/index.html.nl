<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>
<title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation.css" />
<!--  <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" /> -->
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/reveal.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://www.clubshop.com/js/foundation/foundation.js"></script>
<!--  <script src="http://www.clubshop.com/js/foundation/app.js"></script> -->
<script src="http://www.clubshop.com/js/foundation/flash.js"></script>
<script src="http://www.clubshop.com/js/foundation/jquery.reveal.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<style>

div.b{
/*	display: none; */
	font-size: 75%;
	background-color: #DFEAFF;
	padding: 3px 6px;
	border: 1px solid silver;
	margin-top: 2px;
	width: 50%;
}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">TEAM COACH LES</span> 
<hr />
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<p>De persoon die is aangewezen als de Coach van een Partner is de eerste Team Captain of hoger (Pay Level) "upline" van een Partner, die een hoger Pay Level heeft dan de "downline" Partner.</p>
<p>Bijvoorbeeld, wanneer een Partner een Team Player is, dan is de eerste Team Captain of hoger, upline van die Partner, aangewezen als diens Coach. Wanneer een Partner Team Captain is, dan is de eerste Bronze Manager of hoger, upline van hen diens Coach.</p>
<p>Dit geeft alle Partners een ervaren Coach die een hoger Pay Level heeft bereikt.</p>
<p class="style24">Coach Rol</p>
<p>Als Coach zou u moeten handelen als de primaire support Partner voor de Partners, Team Players en Team Leaders in uw Team. Wanneer een andere Partner in het team Team Captain wordt, dan neemt deze de rol van Coach over voor diens Team.</p>
<p>Als Coach moet u:</p>
<ul>
<li class="blue_bullet">Nieuwe Partners verwelkomen en met hen afspreken</li>
<li class="blue_bullet">Alle vragen beantwoorden die u toegestuurd krijgt</li>
<li class="blue_bullet">Communiceren met de potentiële partners in uw team om hen uit te nodigen om Proefpartners te worden</li>
<li class="blue_bullet">Ervoor zorgen dat alle proefpartners in uw Team worden opgevolgd</li>
<li class="blue_bullet">Uw Partners in uw team aanmoedigen en toestaan om uw communicaties en bijeenkomsten te observeren die u heeft met hun persoonlijk verwezen Proefpartners en Partners</li>
<li class="blue_bullet">Leid door het goede voorbeeld te geven aan persoonlijk verwezen Members en persoonlijk gesponsorde Partners</li>
</ul>
<br />
<p class="style24">Verwelkomen van nieuwe Partners</p>
<p>De Partner die een andere Partner sponsorde moet diens nieuwe Partner een e-mail sturen om hem/ haar te verwelkomen in het team. Als de Coach doet u hetzelfde, behalve dat u tevens moet proberen een ontmoeting te regelen met de nieuwe Partner en nodig, indien mogelijk, diens sponsor uit bij de ontmoeting.</p>
<p>Naast het versturen van een e-mail, moet u ook proberen contact met hen te zoeken via SKYPE en Facebook om een datum en tijd vast te stellen om elkaar te spreken.</p>
<p>Controleer voor het gesprek, of zij al Members of Proef Partners hebben verwezen. Wanneer u elkaar spreekt, moet u, naast het kennismaken met elkaar:</p>
<ul>
<li class="blue_bullet">Nagaan of zij vragen hebben.</li>
<li class="blue_bullet">Uitleggen hoe het sponsoren van twee Partners hen kan helpen om genoeg geld te verdienen om hun maandelijkse abonnement bijdrage van te betalen.</li>
<li class="blue_bullet">Nagaan of zij zijn begonnen met de "Van start gaan" Les in de Training Gids en of zij de "uitnodiging om een Member te worden" e-mail hebben verstuurd zoals is aangegeven in die les. </li>
<li class="blue_bullet">Uitleggen hoe u hen kan helpen om hun Proefpartners op te volgen om hen te helpen hun eerste Partners te krijgen en om hen te trainen in het opvolgen van Proefpartners. Met hun toestemming, begint u met de opvolgprocedure voor ieder Proefpartner die zij hebben.</li>
<li class="blue_bullet">Hebben zij nog geen Proefpartners, vraag dan of ze mensen kennen waarvan ze denken dat deze misschien interesse hebben in het verdienen van meer geld en vraag of u deze mensen mag bellen om hen uit te nodigen gratis Members en Proefpartners te worden. Doe, indien nodig, een rollenspel met hen, om ze op hun gemak te stellen.</li>
</ul>
<br />
<p class="style24">Het opvolgen van proef Partners voor nieuwe partners</p>
<ol>
<li>U voert de telefoongesprekken (terwijl zij meeluisteren) om deze prospects in te schrijven als gratis ClubShop Rewards Members en Proefpartners. Hou altijd rekening met de tijdzone waarin zij leven en bel niet te vroeg (met name in de weekenden) of te laat (met name de avond voor werkdagen). <br /> Hier is een website waarop u wereldwijd alle tijdzones terug kunt vinden, zet deze bij uw favorieten of maak er een shortcut van op uw browser balk: <a href="http://www.timeanddate.com/worldclock/" target="_blank">De Wereldklok - Tijdzones</a></li>
<li>Doel: Help elke nieuwe Partner om tenminste twee Partners te sponsoren zodat zij kunnen beginnen geld te verdienen en een goed sponsor voorbeeld hebben gekregen om te dupliceren, zodat ze zelfstandig verder gaan met het persoonlijk sponsoren van Partners.</li>
<li>Als met uw hulp, een nieuwe Partner nieuwe Partners sponsort, werk dan op dezelfde manier met deze nieuwe Partners. Blijf dit doen "in de diepte" totdat u ziet dat er een Team Captain in die lijn is gekomen. Draag dan de Coach rol over aan deze Team Captain voor diens team.</li>
</ol> <br />
<p class="style24">Begrijp hoe de Training Bonus werkt</p>
<p>Aanspraak op de Training Bonus staat gelijk aan 1/2 van de Referral Commissies die een Partner ontvangt en wordt verdient door de Sponsor of upline Partner met een hogere Pay Titel dan de downline Partner. Dit is nog een reden waarom u al uw Persoonlijk aangesloten Partners zou moeten helpen om twee Partners te krijgen - om hen te helpen hun Referral Commissies te verhogen, omdat u 1/2 van hun Referral Commissies zult verdienen als Training Bonus.</p>
<p>Echter, zodra een Partner van uw Team hetzelfde pay level bereikt als u, dan heeft u niet langer recht op het verdienen van een Training Bonus over hen. Dit recht "gaat omhoog" naar de eerstvolgende Partner upline met een hogere Pay Titel. Dit is waarom u de Coach rol zou moeten overdragen aan een Partner zodra deze Team Captain wordt, met name wanneer uzelf nog steeds Team Captain bent.</p>
<br />
<p class="style24">Gebruik de GIBBS Rapporten</p>
<p>Als GIBBS abonnee heeft u de beschikking over twee zeer krachtige software programma's die gekoppeld zijn aan de ClubShop Rewards database, zij werken samen om u te helpen om uw rol professioneler en succesvoller uit te voeren:</p>
<ul>
<li class="blue_bullet"><span class="medblue">ACTIVITEITEN RAPPORT</span> - Besteed speciale aandacht aan de Proefpartner activiteit in uw team, door elke dag op dit rapport te kijken. Volg dan de richtlijnen in de Partner Prospecting Les om efficiënt nieuwe Partners in uw team te sponsoren.</li>
<li class="blue_bullet"><span class="medblue">FOLLOW UP RECORDS - OPVOLG REGISTRATIE</span> - Zet al uw opvolginformatie over de Proefpartners in hun Registratie en stel ook een nieuwe opvolgdatum in, welke in uw activiteitenrapport zal verschijnen op de ingestelde datum. Controleer deze registraties om te zien hoe de Partners in uw Team het doen met hun eigen opvolging. </li>
</ul>
<p>Als Coach, kunt u het zich niet permitteren om opvolgverantwoordelijkheden te delegeren aan de Partners in uw Team totdat zij het Team Captain level hebben bereikt. Totdat zij dit doen, moet u alle opvolging voor hen doen (terwijl zij observeren) om hen te helpen twee Partners te sponsoren en om hen te trainen in hoe zij hun eigen Partner Prospecting doen zodra ze Team Leader worden.</p>
<br />
<p class="style24">Enkele laatste gedachten over uw rol</p>
<ul>
<li class="blue_bullet">U bent niemands baas - vertel mensen niet wat zij moeten doen, tenzij ze om advies vragen. </li>
<li class="blue_bullet">U kunt alleen de Partners coachen die willen leren en luisteren. </li>
<li class="blue_bullet">U kunt mensen niet naar succes duwen, maar u kunt leren hoe u ze samen met u kunt meetrekken.</li>
<li class="blue_bullet">Het kan mensen niets schelen hoeveel u weet, totdat zij weten hoeveel het u kan schelen. </li>
</ul>
<hr />
<p><a href="http://www.clubshop.com/manual/"><img alt="Home Pagina Handleiding" height="64" src="http://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>