﻿<!DOCTYPE html>
 <html>   
         <head>
            <title>GETTING STARTED AS A PARTNER</title>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
         <link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
	 <link rel="stylesheet" type="text/css" href="/css/partner/foundation2.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css" />

    <script src="/js/partner/foundation.js"></script>
    <script src="/js/partner/app.js"></script>
    <script src="/js/partner/flash.js"></script>
    <!--<script src="/js/panel.js"></script>-->

<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/sprintf.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.capitalize {text-transform: uppercase;}
.smaller_text{font-size: 10px;
line-height: 15px;}
</style>

<script language="javascript">

   

	$(document).ready(function() {

	var showText='Learn More';

	var hideText='Hide';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

 

</script>

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script>
</head>

<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<div align="left">
<div id="google_translate_element" style="margin-top:0.5em;"></div>
<div align="left">
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
<hr />
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns">
<h4 class="Mainline">VAN START GAAN ALS EEN PARTNER</h4>
<hr />
<p>Gefeliciteerd met uw beslissing om van start te gaan als ClubShop Rewards Partner!</p>
<p>Veel van onze Partners zijn van start gegaan in de hoop elke maand wat extra geld te verdienen. Vervolgens hebben zij, door gebruik te maken van ons bewezen systeem, een fulltime inkomen ontwikkeld. Als ClubShop Rewards Partner, kunt u in uw eigen tempo werken en gaandeweg uw inkomen laten groeien.</p>
<p><span class="style24">Uw eerste 30 dagen</span></p>
<p>Wij weten dat u waarschijnlijk veel vragen zult hebben wanneer u van start gaat en dat u wellicht een aantal ideeën hebt die u wilt testen om te zien of zij werken. Voor de beste resultaten, adviseren wij dat u uw eerste dertig dagen gebruikt om het systeem eigen te maken door gebruik te maken van de TrainingGids lessen om te leren hoe u een solide glocal zaak creëert en een grote, wereldwijde verkooporganisatie. Als u dit doen creëert u het juiste fundament om uw bedrijf op te bouwen.</p>
<p>U heeft twee mensen (uw "Team Coach" en uw "Executive Director") om u te helpen alle vragen te beantwoorden die u heeft en om u te begeleiden bij het leren hoe u ten eerste members aansluit/ verwijst en vervolgens hoe u Partners, Affiliates, Non-profit organisaties en Winkels/ bedrijven sponsort. U zou van hen een welkom e-mail moeten ontvangen en een uitnodiging om met hen of uw nieuwe team af te spreken, om u te helpen van start te gaan. Wij raden u aan gebruik te maken van dit zeer belangrijke coaching aspect van ons Partner Inkomsten Mogelijkheid door hen te leren kennen en een goede zakenrelatie met hen op te bouwen.</p>
<p>Aarzel niet om contact met hen op te nemen wanneer u het nodig heeft. KLIK HIER of overal waar u de "HELP" link ziet staan op uw Business Center om toegang te krijgen tot hun contactinformatie.</p>

<p><img src="gps.png"> <span class="style24">USE YOUR GPS TO NAVIGATE YOUR JOURNEY</span></p>
<p>Your GPS Training Guide will enable you to learn everything you need to learn, WHEN you need to learn it. Het is uw wegenkaart om u te helpen de "yellow brick road te volgen", wat de meest rechtstreekse route is om een Executive Director te worden en een fulltime inkomen te creëren.</p>
<p>Wij moedigen u aan om niet opnieuw "het wiel uit te vinden", maar om uw reis stap-voor-stap voort te zetten, van de ene Training Gids Les naar de volgende en iedere geadviseerde opdracht te volooien terwijl u verder gaat. Vraag hulp bij uw Team Coaches wanneer u dit nodig hebt.</p>
<p>It takes most traditional business owners as much as one year to re-coup their initial business investment to begin to make a profit. By following your GPS Training Guides and using our proven Global Partner System, you can re-coup your initial investment AND begin earning a monthly profit within your first month.</p>
<p><img src="dynamite.png"> <span class="style24 navy">HOW TNT (TAPROOT NETWORKING TECHNIQUE) WILL HELP YOU BUILD A LARGE, GLOBAL SALES TEAM AND A VESTED, RESIDUAL INCOME</span></p>
	
		
<p>As a Partner, you now have TNT automatically working for you and ALL the other Partners that are on your team and that will join your team! To learn more about how TNT works, go to the Affiliate TNT Information Page
<a href="http://www.clubshop.com/affiliate/2.0.html" target="_blank">http://www.clubshop.com/affiliate/2.0.html</a></p>

<p>View your <a href="http://www.clubshop.com/cgi/vip/tree.cgi/team" target="_blank">Team Report</a> frequently to see how TNT and your Partner Team help you to build a large, global sales team to earn Team Commissions from! As your Team Pay Points grow, so does your vested, residual income potential!</p>

<br/>
<br/>
	
	
	
	<p><span class="style24 navy">HOW THE MEMBER TEAM POOLS WORK</span></p>

<p>When you view your <a href="http://www.clubshop.com/cgi/vip/tree.cgi/team" target="_blank">Team Report</a> you can see a Member Pool under the "taproot" of Partners you have on your team. In this pool are Affiliates that have been referred by the other Partners on your team. Whenever any of the Affiliates in your pool upgrade to Partner, they become part of your team and increase your Team Pay Points to help you qualify for higher Pay Levels and increased Team Commissions!</p>


<p><span class="style24 navy">REFERRAL MARKETING</span></p>

<div class="toggle">
					<div class="row">
						<div class="twelve columns">
							<div class="panel">

<p>ClubShop Rewards biedt u een volledig nieuwe en andere manier om geld te verdienen als Partner, dan welke andere verkoop organisatie, multi-level marketing organisatie of affiliate verkoopprogramma dan ook.</p>
<table width="100%">
<tbody>
<tr>
<td width="14%"><strong>TYPE MOGELIJKHEID<br /></strong></td>
<td width="39%">
<div align="center"><span class="bold">WAT ZIJ BIEDEN</span></div>
</td>
<td width="47%">
<div align="center"><span class="bold royal">WAT CLUBSHOP REWARDS BIEDT</span></div>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Directe Verkoop</strong></td>
<td>
<ul>
<li class="black_bullet">Kan een verkoop-uitrustig verlangen om van start te kunnen gaan.</li>
<li class="black_bullet">Kan van u verlangen om product demonstraties te geven, bestellingen op te nemen en te plaatsen en bestellingen te leveren en geld te innen.</li>
<li class="black_bullet">Vereist van consumenten om alleen de producten aan te kopen die het Directe Verkoop bedrijf aanbiedt.<br /><br /></li>
<li class="black_bullet">De meeste directe verkoop mogelijkheden bieden u alleen een manier om locaal geld te verdienen.</li>
<li class="black_bullet">Een beperkte mogelijkheid om geld te verdienen, als gevolg van de beperkte locale markt en een beperkt aantal beschikbare producten om te verkopen.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Gratis om van start te gaan als een Affiliate.</li>
<li class="blue_bullet">U geeft gratis lidmaatschappen weg. Uw referrals winkelen zelf.<br /><br /> </li>
<li class="blue_bullet">U stelt uw referrals in staat om de dingen te kopen die zij anders ook kopen bij de populairste online winkels of bij lokale winkels/ ondernemers EN hierbij geld te besparen, geld terug te krijgen en/ of prijzen te winnen.</li>
<li class="blue_bullet">U kunt locaal en wereldwijd mensen verwijzen om members te worden om zo een "glocal" inkomen op te bouwen.</li>
<li class="blue_bullet">Een mogelijkheid om onbeperkt geld te verdienen, als gevolg van het hebben van een internationale markt en allerlei soorten producten en diensten waarop uw referrals geld kunnen besparen.</li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Multi-Level Marketing</strong></td>
<td>
<ul>
<li class="black_bullet">Kan inschrijfgeld vereisen om van start te gaan.</li>
<li class="black_bullet">Kan tevens van u eisen om directe verkoop te doen en kan een beperkt aantal producten of diensten aanbieden om te verkopen.</li>
<li class="black_bullet">Marked up (duurder dan normale winkelprijzen) producten, als gevolg van het moeten betalen van commissies aan meerdere people in een lijn van  sponsorshap.</li>
<li class="black_bullet">De mogelijkheid kan beperkt zijn tot bepaalde landen.</li>
<li class="black_bullet">Wanneer het bedrijf minder dan vijf jaar oud is, dan heeft het 90% kans om uit zaken te gaan.</li>
<li class="black_bullet">Kan worden verward met of kan een illegale pyramide zijn.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Gratis om van start te gaan als een Affiliate.</li>
<li class="blue_bullet">Geen directe verkoop en potentieel een onbeperkt aantal producten en diensten die door leden kunnen worden gekocht.</li>
<li class="blue_bullet">Geld terug, kortingen en/ of prijzen voor het aankopen van dezelfde producten en diensten als iemand die geen lid is.<br /><br /></li>
<li class="blue_bullet">Onbeperkte, wereldwijde mogelijkheid.</li>
<li class="blue_bullet">ClubShop Rewards doet al zaken sinds 1997.<br /><br /></li>
<li class="blue_bullet">A+ <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">classificatie </span></span>bij het Better Business Bureau. </li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Affiliate Verkopen</strong></td>
<td>
<ul>
<li class="black_bullet">Verplicht geld riskeren om internet advertenties te maken.<br /><br /></li>
<li class="black_bullet">Moet ervaren zijn in het creëren van advertenties.<br /><br /></li>
<li class="black_bullet">Weinig tot geen training aangeboden.<br /><br /></li>
<li class="black_bullet">Commissies worden alleen verdient op individuele aankopen.<br /><br /></li>
<li class="black_bullet">Geen referral consumenten informatie beschikbaar.<br /><br /><br /></li>
<li class="black_bullet">Geen mogelijkheid om te sponsoren en <span class="short_text" id="result_box" lang="nl"><span class="hps">override</span> <span class="hps">commissies </span></span>te verdienen op andere Affiliates.<br /><br /></li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Niet nodig om geld te riskeren om advertenties te maken. Mogelijkheid om gezamenlijk te adverteren aanwezig voor GPS abonnees.</li>
<li class="blue_bullet">Geen ervaring nodig. Gezamenlijk adverteren mogelijk voor GPS abonnees.</li>
<li class="blue_bullet">GPS abonnees hebben vrije toegang tot marketing en management cursussen en webinars.</li>
<li class="blue_bullet">Verdien residuele commissies, elke keer wanneer een door u verwezen Member een gekwalificeerde aankoop doet.</li>
<li class="blue_bullet">Member lijsten en rapporten beschikbaar. GPS PRO (en hogere) abonnees hebben tevens gratis toegang tot uitgebreide lijsten, rapporten en Follow Up Records (Opvolgsysteem).</li>
<li class="blue_bullet">Partners kunnen Affiliates, winkels/ bedrijven, non-profit organisaties en andere Partners sponsoren.</li>
</ul>
</td>
</tr>
</tbody>
</table>

</div></div></div></div>

<br />
<p>To learn more about the privileges and benefits of becoming a Partner, see our <a href="http://www.clubshop.com/present/bizop.xml" target="_blank"><span style="text-decoration: underline;">Partner Business Opportunity Presentation.</span></a></p>
<!--
<p>Als een Affiliate, kunt u tevens kwalificeren als een Partner, welke uw mogelijkheid zal vergroten om geld te verdienen, las gevolg van de toename van privileges die Partners genieten.</p>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Als <span class="style24 royal">Partner</span>, kunt u tevens verwijzen:</p>
<ul>
<li class="blue_bullet">Winkels/ bedrijven die deelnemen aan ons programma (online en offline) en commissies verdienen op hun aankopen EN potentieel van de aankopen die bij hen worden gedaan door ALLE ClubShop Rewards Members.</li>
<li class="blue_bullet">Non-profit organisaties om hen te helpen fondsen te werven en commissies te verdienen op de ClubShop Rewards kaarten die zij kunnen aankopen en verkopen EN potentieel van de aankopen die worden gedaan door de ClubShop Rewards Members die zij eventueel verwijzen.</li>
<li class="blue_bullet">Affiliates zoals uzelf, waarbij u commissies kunt verdienen op de aankopen die gedaan worden door de Members die zij verwijzen.</li>
<li class="blue_bullet">Partners, om een wereldwijde verkoop organisatie uit te bouwen waarover u override commissies en dividenden kunt verdienen (revenue shared by ClubShop Rewards met onze Partners).</li>
</ul>
<p>Om meer te weten te komen over de privileges en voordelen van het worden van een Partner, bekijkt u onze <a href="http://www.clubshop.com/present/bizop.xml" mce_href="http://www.clubshop.com/present/bizop.xml" target="_blank"><span style="text-decoration: underline;" mce_style="text-decoration: underline;">Partner Zakelijke Mogelijk Presentatie.</span></a></p>
</div>
</div>
</div>
</div>
-->
<p><span class="style24 navy">HOE LEDEN GELD BESPAREN</span></p>
<p>ClubShop Rewards Members/ leden kunnen geld besparen wanneer zij online winkelen bij het ClubShop Rewards winkelcentrum en door zaken te doen met of offline aankopen te doen bij deelnemende ClubShop Rewards winkels en bedrijven. Hoeveel voordeel iemand kan genieten door een Member te zijn is grotendeels gebaseerd op het aantal online winkels in het ClubShop Rewards winkelcentrum in het land waar de member woont en/ of het aantal deelnemende offline winkels en bedrijven waar iemand woont of waar zij wellicht naartoe reizen.</p>
<ul>
<li class="blue_bullet"><span class="style30">ONLINE WINKELEN</span> - Members/ leden kunnen geld terug krijgen (ClubCash) wanneer zij online aankopen doen via het ClubShop Rewards winkelcentrum, deze ClubCash telt op in de persoonlijke ClubCash account van iedere Member/ lid  account totdat deze het bedrag laat uitbetalen. Het ClubShop Rewards winkelcentrum is uitgegroeit tot het grootste online winkelportaal ter wereld, met beschikbare winkels in meer dan 50 landen! 							<br /><br /> <a href="http://www.clubshop.com/mall/" target="_blank"><span style="text-decoration: underline;">KLIK HIER</span></a> om naar het ClubShop Rewards winkelcentrum te gaan en kijk of er een winkelcentrum in uw land is en bij hoeveel winkels u en de Members/ leden die u verwijst, kunnen winkelen en ClulubCash kunnen verdienen.  						 								                     
<ul>
<li class="half">Indien er een ClubShop Rewards winkelcentrum in uw land is, staat het bedrag aan ClubCash dat u en de Members/ leden die u verwijst kunnen verdienen, weergegeven achter elke winkel.
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<table>
<tbody>
<tr>
<td class="orange capitalize" width="153">
<div align="center">Online Winkel</div>
</td>
<td class="orange capitalize" width="202">
<div align="center">Rewards</div>
</td>
<td class="orange capitalize" width="125">
<div align="center">Deals</div>
</td>
<td class="orange capitalize" width="1039">
<div align="center">Details winkel</div>
</td>
</tr>
<tr>
<td colspan="4"></td>
</tr>
<tr>
<td><img alt="Disney Store" height="101" src="../../../../../affiliate/images/disney_store.gif" width="153" /></td>
<td>Geld terug: 1.2% <br />Pay punten: 0.6%</td>
<td></td>
<td><span class="smaller_text">DisneyStore.com is de belangrijkste online superwinkel voor Disney producten, inclusief een grote selectie van collectibles, home décor, kleding, speelgoed, top-selling Mini Bean Bags, en meer. Van Mickey tot Pooh, Princessen tot Buzz Lightyear, u zult er zeker uw favoriete figuren vinden.</span></td>
</tr>
<tr>
<td colspan="4"></td>
</tr>
<tr>
<td><img border="0" height="101" src="../../../../../affiliate/images/netflix.gif" width="153" /></td>
<td><a href="../../../../../affiliate/images/netflix_desc.gif" onclick="window.open(this.href,'help', 'width=414,height=275,scrollbars'); return false;"><span style="text-decoration: underline;">Percentage varieert <img alt="rate varies" height="15" src="http://www.clubshop.com/mall/icons/icon_info_15x15.png" width="15" /></span></a></td>
<td></td>
<td><span class="smaller_text">Bekijk direct en onbeperkt TV series &amp; films via het internet, rechtstreeks op uw TV, computer en verschillende mobiele apparaten. Kijk zoveel u wilt, zo vaak u wilt voor slechts $7.99 per maand. Start vandaag nog uw proefperiode!</span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</li>
<li class="half">Wanneer een winkelcentrum winkel u 5% geld terug (cash back) biedt op uw aankoop en u doet een aankoop van <span class="transposeME">$100.00</span>, dan verdient u <span class="transposeME">$5.00</span> aan ClubCash! Als Partners verdient u tevens Referral Pay punten en Referral Commissies op uw eigen aankopen (behalve op GPS abonnementbijdragen). </li>
<li class="half">Wanneer uw land niet op de lijst staat of op dit moment maar een paar winkels beschikbaar heeftom te gaan winkelen, dan heeft u een enorme mogelijkheid om als Partner geld te verdienen door het verwijzen van online winkels naar opname op de lijst op het ClubShop Rewards winkelcentrum!! </li>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>1 van de vele privileges die u hebt als <span class="style24 royal">Partner</span>, is de mogelijkheid om online winkels te verwijzen om opgenomen te worden in het ClubShop Rewards winkelcentrum. Landen met weinig tot geen winkels bieden Partners een enorme "ground floor" mogelijkheid om online winkels te verwijzen die producten of diensten bieden in uw land, om opgenomen te worden in het winkelcentrum. Als Partner, wanneer u een online winkel verwijst om te worden opgenomen in het ClubShop Rewards winkelcentrum, krijgt u 3 voordelen!</p>
<ol>
<li>U verdient Referral Commissies op de listing bijdrage die door de online winkels die u verwijst wordt betaald.</li>
<li>Elke keer dat een Member een aankoop doet bij een online winkel die u hebt verwezen, verdient u residueel Referral Commissies.</li>
<li>Hoe meer winkels uw land op de lijst heeft, hoed beter de winkel mogelijkheden zijn voor de Members/ leden die u verwijst om ClunCash te verdienen en voor u om Referral Commissies te verdienen.</li>
</ol></div>
</div>
</div>
</div>
<p>Als GPS abonnee, hebt u Online Merchant Marketing Training Gidsen tot uw beschikking waarin u leert hoe u online winkels kunt aansluiten op de lijst van het ClubShop Winkelcentrum.</p>
</ul>
</li>
<li class="blue_bullet"><span class="style30">OFFLINE WINKELEN</span> - Members geld terug verdienen (ClubCash) of een directe korting krijgen wanneer zij een ClubShop Rewards kaart laten zien aan een deelnemende offline winkelier. ClubShop Rewards kaarten kunnen worden gebruikt om geld te besparen gedurende ten minste een jaar en zijn erg voordelig, wat Members is staat stelt om ten minste net zoveel geld te besparen als de kaart kost, met slechts een paar aankopen! De ClubShop Rewards kaart kan wereldwijd worden gebruikt om geld mee te besparen bij een groeiend aantal bedrijven en winkels.
<p><a href="http://www.clubshop.com/cs/rewardsdirectory">KLIK HIER</a> om te zien of er deelnemende winkels en/ of bedrijven zijn dichtbij waar u woont.</p>
<ul>
<li>Wanneer u deelnemende winkels en bedrijven in uw omgeving hebt, dan zou u een kaart voor uzelf moeten kopen of u zou er met korting 10 of meer moeten kopen, zodat u zelf een kaart heeft om mee van start te gaan met het besparen van geld en zodat u kaarten kunt verkopen en een mooie winst kunt verdienen op elke kaart die u verkoopt. <a href="https://www.clubshop.com/cgi/cart/cart.cgi?DT=1&amp;SearchDept.x=24&amp;SearchDept.y=10" target="_blank">KLIK HIER</a> om 1 of meerdere ClubShop Rewards kaarten  te kopen. <br /><br /> Als Partner kunt u locale non-profit organisaties verwijzen (Affinity Groups) om kaarten te verkopen als een fantastische manier om fondsen te werven, waarover u commissies kunt verdienen op elke kaart die door de non-profit organisatie(s) verkocht worden die u hebt verwezen! <br /><br /> De hoogte van het percentage ClubCash dat u kunt verdienen of de korting die u krijgt, wordt weergegeven op elke lijst met winkels en bedrijven. U kunt tevens gebruik maken van ons Google Maps app om lokale deelnemende winkels en bedrijven te vinden! </li>
<li>Als Partner kunt u locale non-profit organisaties verwijzen om kaarten te verkopen, als een fantastische manier om fondsen te werven, waarover u commissies kunt verdienen op elke kaart die door de door u verwezen non-profit organisatie(s) verkocht worden! Indien u geen of slechts enkele deelnemende winkeliers hebt in de regio waar u woont, dan hebt u een geweldige mogelijkheid als Partner om geld te verdienen door het verwijzen van offline winkels en bedrijven om deel te laten nemen in ons ClubShop Rewards WinkeliersProgramma
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Een andere van de vele voordelen die u hebt als Partner, is de mogelijkheid om offline winkels te verwijzen om de ClubShop Rewards kaart te accepteren in ruil voor het bieden van een kostenbesparende adverteer- en trouwe klanten programma. Wanneer u weinig tot geen aangesloten winkels in uw omgeving hebt, dan hebt u een geweldige "ground floor" mogelijkheid om offline winkels te verwijzen om deel te nemen in ons ClubShop Rewards Winkel Programma. Als Partner, wanneer u offline winkels en bedrijven verwijst om mee te doen in onze ClubShop Rewards Winkel Programma, krijgt u 3 voordelen!</p>
<ol>
<li>U verdient Referral Commissies op elk marketing pakket dat door de winkels wordt gekocht die u hebt verwezen.</li>
<li>U kunt residuele Referral Commissies verdienens, elke keer dat een Member/ lid winkelt in een offline winkel of bedrijf en hiermee ClubCash verdienen.</li>
<li>Hoe meer winkels/ bedrijven u lokaal inschrijft, hoe beter de bespaarmogelijkheden zijn voor de kaarthouder Members/leden die u verwijst. Hoe meer waarde de kaart heeft, hoe gemakkelijker het is voor u om kaarten te verkopen!</li>
</ol>
<p>Als GPS abonnee, krijgt u Locale Marketing Training Gidsen tot uw beschikking om te leren hoe u offline winkels verwijst om hen de ClubShop Rewards Kaart te laten accepteren en hoe u non-profit organisaties verwijst om de kaarten te verkopen als fondsenwerver.</p>
</div>
</div>
</div>
</div>
</li>
</ul>
</li>
</ul>
<p><span class="style24 navy">CLUBCASH</span></p>
<p>Zoals hierboven uitgelegd, kunnen ClubShop Reward Members/ leden geld terug krijgen wanneer zij online aankopen doen via het ClubShop Rewards winkelcentrum of wanneer zij offline een ClubShop Rewards kaart laten zien aan een deelnemende winkelier die de verkoop registreert. Deze manier van geld terug krijgen noemen wij "Club Cash" voor Members/ leden, welke wordt opgetelt in de ClubCash account van iedere Member totdat deze tenminste <span class="transposeME">$25.00</span> bedraagt, vanaf dat moment kan de member/ het lid de cash back laten uitbetalen in de eigen valuta.</p>
<p><a href="http://www.clubshop.com/members/clubcash_details.shtml" target="_blank">KLIK HIER</a> om te zien hoe uw ClubCash account er uit ziet.</p>
<ul>
<li class="blue_bullet"><span class="style30">ONTVANG GRATIS UW EERSTE CLUBCASH!</span> Als Member/lid kunt u onmiddelijk uw eerste Club Cash verdienen door het invullen van een korte 'klanten interesse' vragenlijgenlijst. <a href="http://www.clubshop.com/mall/other/questionnaire.xsp" target="_blank">KLIK HIER</a> om meer te lezen en om de vragenlijst in te vullen, zodat wij u kunnen belonen met een eerste strorting op uw ClubCash account!</li>
<li class="blue_bullet"><span class="style30">WORD UW EERSTE KLANT!</span> Wanneer u uw eerste online aankoop doet, via het ClubShop Rewards winkelcentrum, verdient u ook een Club Cash Bonus storting op uw account, bovenop de Club Cash die u verdient op uw aankoop. Hier <span class="short_text" id="result_box" lang="nl"><span class="hps">enkele ideeën van de eerste aankopen die u kunt doen om een</span></span> Club Cash bonus en credits te verdienen: 									<br /><br /> 
<ul>
<!--<li class="half"><span class="orange bold">SKYPE</span> - Geeft u de mogelijkheid om gratis te bellen met andere SKYPE gebruikers en met uw Partner Sponsor en Coach. Doe een kleine storting om Club Cash te verdienen en gebruik SKYPE om internationale telefoongesprekken en video gesprekken te voeren tegen zeer lage tarieven.</li>-->
<li class="half"><span class="orange bold">VISTA PRINT</span> - Ontvang 500 gratis gedrukte visitekaartjes (om te gebruiken voor uw nieuwe ClubShop                  Rewards zaak) en betaal alleen verzendkosten, met internationale verzending.</li>
<li class="half"><span class="orange bold">ZAKELIJKE DIENSTEN, KANTOORBENODIGDHEDEN, COMPUTERS &amp; SOFTWARE, WEBHOSTING &amp; ISPS</span> - Bekijk deze <span class="short_text" id="result_box" lang="nl"><span class="hps">categorieën om te zien welke winkels voor u beschikbaar zijn en verdien</span></span> ClubCash.</li>
</ul>
<p><a href="http://www.clubshop.com/mall" target="_blank">KLIK HIER</a> om uw eerste aankoop te doen en verdien ClubCash en een ClubCash bonus</p>
</li>
<li class="blue_bullet"><span class="style30">WORD UW EIGEN BESTE KLANT!</span> In addition to earning Club Cash on your personal purchases, as a Partner you can also earn "Referral Pay Points" on the same purchases that you earn Club Cash for. </li>
</ul>
<p><span class="style24 navy">PAY PUNTEN &amp; REFERRAL COMMISSIES</span></p>






<div class="row">
				<div class="twelve columns">
	
						<div class="row">
						<div class="three columns">
							<br/><br/><img src="http://www.clubshop.com/manual/compensation/images/customers.png" height="300" width="241" alt="Pay Points"><br/></div>
							
							<div class="nine columns">
<p>As explained above, you will earn "Personal Pay Points" on qualified purchases you make and on qualified purchases made by the members you refer. 1 Pay punt staat gelijk aan <span class="transposeME">$1.00</span> in waarde. Pay punten worden elke maand opgetelt en worden gebruikt om het volgende vast te stellen:</p>
<ul>
<li class="blue_bullet">Uw Referral Commissies, welke gelijk staan aan 50% van de Pay punten die u hebt gekregen voor de aankopen die gedaan zijn door de Members die u verwijst. Opmerking: Als Partner verdient u tevens Referral Commissies op uw persoonlijke aankopen en op de aankopen die gedaan zijn door de Affiliates, winkels en bedrijven en de non-profit organisaties die u hebt verwezen. </li>
<li class="blue_bullet">Your Pay Level, which also determines your entitlement to earn Team Commissions.</li>
</ul>
							<p><img src="http://www.clubshop.com/manual/compensation/images/powerpoints1.png" height="100" width="100" alt="PowerPoints"> When you earn Personal Pay Points, these are converted into POWER POINTS based on being multiplied by a Power Multiplier. This can convert 2 Personal Pay Points into 20 POWER POINTS to be used to determine you Pay Level!</p>
<br/><br/>
							</div>
					</div></div></div>








<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<div align="center"><span class="orange bold">PAY PUNTEN VOORBEELDEN</span></div>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize bold">U doet een aankoop van <span class="transposeME">$20.00</span> voor printer cartridges bij 123 Ink Jets</span> (een winkel op de U.S. lijst van het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,6,mall_id=1"><img border="0" height="79" src="../../../../../affiliate/images/123inkjets.gif" width="258" /></a></td>
<td width="193">Geld terug: 11.2%<br /> Pay punten: 5.6%</td>
<td width="626"><span class="smaller_text">123 Inkjets is toegewijd om u premium inkjet printer cartridges &amp; laser toner, fotopapier en andere inkjet printer benodigdheden te leveren tegen gereduceerde prijzen.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">U verdient </span><span class="transposeME royal bold">$2.24</span> <span class="bold">aan ClubCash en 1.12 aan Pay punten</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize bold">Een Member dat u hebt verwezen doet een aankoop van <span class="transposeME">$50.00</span> voor een boeket bloemen bij Flower Delivery </span>(een andere winkel op de U.S. lijst in het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,1611,mall_id=1"><img border="0" height="79" src="../../../../../affiliate/images/flowerdelivery.gif" width="258" /></a></td>
<td width="193">Geld terug: 8.0%<br />Pay punten: 4.0%</td>
<td width="626"><span class="smaller_text">Flower Delivery werkt wereldwijd, </span><span class="short_text" id="result_box" lang="nl"><span class="hps">bij voorkeur met</span> <span class="hps">geschenkmand</span> <span class="hps">verkopers</span></span><span class="smaller_text"> en bloemen leveranciers, om het hoogste niveau van dienstverlening en klanttevredenheid te bieden.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">De member verdient</span> <span class="transposeME royal bold">$4.00</span> <span class="bold">aan ClubCash en u verdient 2.00 aan Pay punten, waarover u tevens </span><span class="transposeME royal bold">$1.00</span><span class="bold"> aan Referral Commissies verdient.</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize bold">Een Member die u hebt verwezen doet een aankoop van <span class="transposeME">$500.00</span> voor een flatscreen TV bij Buy.com </span>(een andere winkel op de U.S. lijst in het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,286,mall_id=1"><img border="0" height="79" src="../../../../../affiliate/images/buy.gif" width="258" /></a></td>
<td width="193">Geld terug: 2.0%<br />Pay punten: 1.0%</td>
<td width="626"><span class="smaller_text">Wij bieden meer dan 3 miljoen producten in </span><span class="short_text" id="result_box" lang="nl"><span class="hps">categorieën, waaronder</span></span><span class="smaller_text">: computers, electronica, digitale camera´s, GPS, boeken, DVDs, CDs, sportartikelen, video games, speelgoed, tassen, baby, sieraden, huis &amp; outdoor en meer!</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">De member verdient</span> <span class="transposeME royal bold">$10.00</span> <span class="bold">aan Club Cash en u verdient 5.00 aan Pay punten, waarover u tevens </span><span class="transposeME royal bold">$2.50</span><span class="bold"> aan Referral Commissies verdient.</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /></div>
</div>
</div>
<br /><br /> 
					<div><span class="largehead_blue_bold">CREATING A VESTED, LONG TERM, RESIDUAL INCOME</span></div>
						<div class="row">
							<div class="twelve columns">
							
							
<p>The greatest benefit that TNT provides you as a Partner, is the ability to develop a vested, long term, residual income based on being part of a Partner Team!</p>
	<ul>
    <li class="blue_bullet"><span class="style30">HOW THIS HAPPENS</span> - As more and more members are referred by members in your team, your Team Pool grows. And as more and more Affiliates in your Team Poll become Partners, the bigger YOUR team gets! As you team increases so do the Team Pay Points, from which you earn Team Commissions from. So as you see your team growing and your Team Pay Points increasing, know that your position on your Partner team also increases in value!<br/><br/>

    Imagine seeing your Team Pay Points increase over the next few years, so that they exceed the requirement needed to qualify as an Executive Director. Even if you have been inactive in personal sponsoring, you then will have the benefit of meeting the Team Pay Point requirement to qualify as an Executive Director to earn an Executive Director income, once you meet the Personally Referred Partner requirement. And if you gain an Executive Director on your team before you qualify as an Executive Director, you can still earn the residual income of the Executive Director Bonus, once you meet the Personally Referred Partner requirement.</li>

    <li class="blue_bullet"><span class="style30">VESTED INCOME</span> - Creating and developing a vested income is typically done by being earning the entitlement to a pension income after working for "X" number of years for the same employer, or by investing money that you can redeem at a later date with interest (like a savings bond or 401K plan in the U.S.). As a Partner, you can also create, develop and even increase a potential vested income that you will be entitled to earn once you satisfy the Personally Referred Partners requirement for the applicable Pay Levels in the Partner Compensation Plan.</li>

    <li class="blue_bullet"><span class="style30">LONG TERM INCOME</span> - As your Pool and Team grows, so does your ability to earn a long, term income. The "deeper" your Partner line gets, the more secure your long term income becomes. This can actually result in a lifetime income that you can pass on to someone else in the future!

    <li class="blue_bullet"><span class="style30">RESIDUAL INCOME</span> - This type of income is based on earning future income, based on efforts you have made in the past and in the present. Typically, residual income can only be earned for "royalties" earned for books or songs written, television and movie acting or from a trust. As a Partner, your vested income potential is also a residual income potential. And as an Executive Director, you can earn a residual income in Executive Director bonuses that do not require you to perform any roles or responsibilities, as they will have been delegated to other Executive Directors on your team.</li>
</ul>
<p>As a future Executive Director, this can means thousands of dollars/euros a month in vested, long term, residual income!</p>
							

							
							</div>
						</div>
					</div>
	<br/><br/>
<div><span class="largehead_blue_bold">GEBRUIK GPS OM UW ZAAK OP TE BOUWEN</span></div>

<div class="row">
				<div class="twelve columns">
	
						<div class="row">
						<div class="four columns">
		<img src="http://www.clubshop.com/manual/gps/med_gps.png" height="246" width="332" alt="GPS"><br></div>
		
		<div class="eight columns"><br/><br/><br/>
<p>Door u te abonneren op ons Global Partner Systeem (GPS), verdient u automatisch ten minste 10 Pay punten, waardoor u automatisch elke maand kwalificeert als Partner. GPS is beschikbaar in een aantal verschillende paketten, om u de hulpmiddelen en training te bieden die u zullen helpen nieuwe Pay Levels te behalen. <a href="http://www.clubshop.com/manual/gps/overview.html" target="_blank">KLIK HIER</a> om meer te lezen over GPS.</p></div>
<br /><br /><br /> </div></div></div>







<hr />
<div align="right"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><a href="http://www.clubshop.com/vip/manual/training/guide/referralmarketing/index.html" target="_blank">REFERRAL MARKETING GIDS</a></div>
<br /><br /><br /></div>
</div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2013           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>