﻿<!DOCTYPE HTML>

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Handleiding</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />
    <!--  <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" /> -->
	<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
	<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />
	<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
	<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
	
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="/js/foundation/foundation.js"></script>
   <!--  <script src="http://www.clubshop.com/js/foundation/app.js"></script> -->
    <script src="/js/foundation/flash.js"></script>
	<script src="/js/foundation/jquery.reveal.js"></script>
    <script src="http://www.clubshop.com/js/panel.js"></script>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">VOORBEREIDING</span> 
<hr />
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns">
<p><span class="style24">PARTNER BUSINESS CENTER</span></p>
<p>Indien u het nog niet hebt gedaan, neem dan nu een paar minuten de tijd om rond te kijken in uw Partner Business Center, kijk gewoon eens wat er achter elke link te vinden is. Naast het lezen van het laatste nieuws en de updates in het hoofdmenu, kunt u ook de laatste "insider" informatie lezen die daar geplaatst is door ClubShop Rewards President Dick Burke of Partner Support medewerkers op het <a href="http://www.clubshop.com/p/blog/dblog.xml" target="_blank">Burke Blog</a>.</p>
<p>You will want to visit your Partner Business Center each time you go online, to stay up to date on things that may impact your business. You will want to look at your Activity Report each day when you visit your Business Center to see and followup on the activity occurring in your team.</p>
<p>De beste manier om dit te doen is door van uw Business Center uw Startpagina te maken van uw internet browser - de eerste pagina die u ziet wanneer u uw op het internet gaat bij het openen van uw browser. Deze pagina is vaak voor u gekozen door uw internet provider of als onderdeel van een browser software, maar kunt u zelf veranderen.</p>
<p>Zo doet u dit:</p>
<p>Kopieer deze link - <a href="https://www.clubshop.com/p/index.shtml" target="_blank">https://www.clubshop.com/p/index.shtml</a></p>
<p>Wijzig uw startpagina instellingen voor uw browser</p>
<p>Wanneer u internet explorer gebruikt als uw browser, klik dan op "Extra" (bovenin); klik op "Internet Opties"; verander uw "Startpagina" adres in: <a href="https://www.clubshop.com/p/index.shtml" target="_blank">https://www.clubshop.com/p/index.shtml</a> en controleer of dit klopt. Klik op "OK". Voor een voorbeeld, <a href="#">klik hier</a>.</p>
<!-- IE POP UP MOD -->
<div class="reveal-modal" id="myModal">
<p><strong>Internet Explorer:</strong> <br /> <span style="font-family: Arial,Helvetica,sans-serif; font-size: x-small;">Onder 'Extra', klik 'Internet  			Opties'. <br /> Wijzig uw 'startpagina' adres in: https://www.clubshop.com/p/index.shtml</span><span style="font-family: Arial,Helvetica,sans-serif; font-size: x-small;"><br /> Klik 'OK'. </span></p>
<a class="close-reveal-modal"></a>
<p><img height="458" src="/vip/manual/images/homepgie.png" width="404" /></p>
</div>
<!-- / IE POP UP MOD -->
<p>Wanneer u Mozilla Firefox gebruikt als uw browser, klik op "Extra" (bovenin); klik op "Opties"; klik "Algemeen" (bovenin); Onder 'Als Firefox Start" kies: - Startpagina, Onder 'als Startpagina tonen': Type <a href="https://www.clubshop.com/p/index.shtml" target="_blank">https://www.clubshop.com/p/index.shtml</a>.Klik 'OK'. Voor een voorbeeld, <a href="#">klik hier</a>.</p>
<!-- Mozilla POP UP MOD -->
<div class="reveal-modal" id="firefox">
<p><strong>Mozilla:</strong> <br /> Onder 'Extra', klik 'Opties'. <br /> wijzig uw 'Startpagina' adres in: https://www.clubshop.com/p/index.shtml<br /> Klik 'OK'.</p>
<a class="close-reveal-modal"></a>
<p><img height="458" src="/vip/manual/images/homepg_mozilla.png" width="404" /></p>
</div>
<!-- / Mozilla POP UP MOD -->
<p>Indien u gebruik maakt van een andere Browser, zoek dan in de hulpfunctie naar "Startpagina" en volg de instructies.</p>
<p>Wanneer u nu uw computer elke dag "opstart", kunt u direct inloggen als Partner om het laatste nieuws, aankondigen en updates te lezen.</p>
<p><span class="style24">DEELNEMEN AAN ONLINE WEBINARS</span></p>
<p>In aanvulling op uw Partner Training Gids en de begeleiding van een persoonlijke Partner Coach, heeft u ook toegang tot online webinars om u te helpen uw ClubShop Rewards zaak op te bouwen.  Click on the Webinars tab at your Business Center to see a list and the links for all the webinars that are scheduled for the next 30 days. </p>
<ul>
<li> <a href="http://www.clubshop.com/affiliate/webinars.html" target="_blank">Partner Inkomsten Mogelijkheid Presentatie</a> - These presentations are made by Directors and Executive Directors and are a great "tool" to help you gain personally referred Partners, by inviting your Affiliates to join you at a meeting. Learn from accomplished Partners how to personally present the Partner Income Opportunity to your prospects too.</li>
<li><a href="https://www.clubshop.com/vip/manual/training/interactive_online_schedule.html" target="_blank"> Training Webinars</a> - GPS abonnees hebben tevens gratis toegang tot            online webinars, welke normaalgesproken wekelijks bijgewoond kunnen            worden. Deze interactieve webinars worden gehost door ClubShop Rewards            ten behoeve van de abonnees om u waardevolle tips en methoden te bieden            om een succesvolle eBusiness op te bouwen.</li>
<li><a href="https://www.clubshop.com/vip/manual/training/interactive_online_schedule.html" target="_blank">PARTNER CLUBHOUSE</a> - The ClubHouse is like an online, open house available to Partners to meet and chat with their fellow Partners and to observe and receive training from a Marketing Director that is hosting the ClubHouse. CEO Dick Burke also uses the ClubHouse to meet with Partners for his monthly CEO Chat. </li>

</ul>
<p><span class="style24">GEBRUIK SKYPE VOOR UW INTERNATIONALE TELEFOONGESPREKKEN</span></p>
<p>Wij raden aan om bij voorkeur gebruik te maken van SKYPE om te bellen, om de volgende drie redenen:</p>
<ol>
<li> Het is GRATIS om het programma te downloaden en het is GRATIS om internationale telefoongesprekken te voeren en wereldwijd met andere SKYPE gebruikers te bellen! Video en conferentie gesprekken zijn ook beschikbaar voor SKYPE gebruikers.</li>
<li> U kunt uw computer gebruiken om telefoongesprekken te voeren wanneer u de beschikking heeft over een microfoon en speakers (een headset met microfoon is aanbevolen). Online zijn terwijl u in gesprek bent met uw prospects, uw Partners en uw Coaches, maakt het mogelijk om samen met hen webpagina's te bekijken, terwijl u in gesprek bent.</li>
<li> SKYPE biedt ook voordelige tarieven bij het bellen naar niet-Skype-gebruikers.</li>
</ol>
<p>Indien u al SKYPE gebruiker bent, controleer dan uw ClubShop Rewards <a href="https://www.clubshop.com/cgi/appx.cgi/update" target="_blank">Profiel</a> om u er van te verzekeren dat uw SKYPE naam juist is weergegeven.</p>
<p>Heeft u nog geen SKYPE, wij raden u aan om een klein bedrag op uw account te zetten zodat u dit kunt gebruiken voor internationale telefoongesprekken naar niet-SKYPE gebruikers. </p>
<p>Zodra u een skypenaam heeft, kunt u inloggen op SKYPE en een chatbericht sturen naar uw <a href="http://www.clubshop.com/cgi/mytcex.cgi" target="_blank">Team Coach en Executive Director</a> zodat zij u kunnen toevoegen aan hun skype contactlijst en zodat u hen kunt toevoegen aan uw lijst. Dit maakt het gemakkelijk en handig om elkaar te spreken wanneer dit nodig is en jullie tegelijk online zijn en het is GRATIS!!</p>
<p><span class="style24">Visitekaartjes</span></p>
<p>U zou altijd visitekaartjes bij u moeten hebben om uit te delen aan mogelijke Members, met daarop een link (met uw ID nummer) naar <a href="http://www.clubshop.com/" target="_blank">www.clubshop.com</a> zodat zij zich kunnen inschrijven.</p>
<p>Indien dit mogelijk is, raden wij u aan dat u uw visitekaartjes besteld via <a href="http://www.clubshop.com/mall/tools/tools.html" target="_blank">Vista Print</a> via het ClubShop Rewards winkelcentrum, om drie hele goede redenen:</p>
<ol>
<li>U krijgt bij uw eerste bestelling 500 GRATIS kaartjes. U betaald alleen verzendkosten.</li>
<li>Vista Print bezorgen internationaal naar de meeste landen. </li>
<li>U verdient ClubCash op uw aankoop (en een kans om een prijs te winnen) en wanneer dit uw eerste aankoop is via het ClubShop Rewards winkelcentrum, verdient u tevens een ClubCash Bonus.</li>
</ol>
<p>U kunt als functie "ClubShop Rewards Partner" invullen en deze URL gebruiken op uw kaartjes: http://www.clubshop.com/cgi-bin/rd/4,,refid=<span class="style4">123456</span> (zorg ervoor dat u <span class="style4">123456</span> verandert in <span class="bold">UW EIGEN ID</span> nummer).</p>
<p><span class="style24">PROFITEER VAN UW CLUBSHOP REWARDS "CLUB CASH" LIDMAATSCHAP VOORDELEN</span></p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="Gold_Bold">CLUB CASH BONUS # 1</span> - U kunt uw eerste Club Cash verdienen door het invullen van een korte vragenlijst met persoonlijke interesses: <br /><a href="https://www.clubshop.com/mall/other/questionnaire.xsp" target="_blank">https://www.clubshop.com/mall/other/questionnaire.xsp</a></p>
<p>Elke maand kijkt het team van het ClubShop Rewards Hoofdkantoor of zij een speciale aanbieding kunnen vinden die overeenkomt met uw interesses en plaatst deze als uw persoonlijke "Top Aanbieding" op <a href="http://www.clubshop.com" target="_blank">www.clubshop.com</a> - en met het invullen van de vragenlijst verdient u uw eerste ClubCash!</p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="Gold_Bold">CLUB CASH BONUS # 2</span> - Doe uw eerste aankoop bij een ClubShop Rewards Winkelcentrum Winkel en u verdient een ClubCash Bonus in aanvulling op de ClubCash die u al verdient op uw aankoopbedrag. ClubCash percentages staan in de lijst naast iedere winkel in het ClubShop Rewards winkelcentrum.</p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="Gold_Bold">CLUB CASH BONUS # 3</span> - Elke keer dat u een aankoop doet bij een ClubShop Rewards Winkelcentrum winkel, krijgt u een kans om ook nog eens geweldige prijzen te winnen!</p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="Gold_Bold">BONUS # 4</span> - Als Partner hebt u recht op het verdienen van uw eigen Referral Commissies op uw persoonlijke aankopen (anders dan GPS abonnementsbijdrage)</p>
<p>Wanneer u voldoende ClubCash verzameld heeft voor het bedrag dat u wenst - laat het dan eenvoudig uitbetalen!</p>
<p><span class="style24"> ONLINE NETWERKEN</span></p>
<p>Volg ons op Facebook en Twitter om mededelingen en updates te ontvangen over ClubShop Rewards:</p>
<p><a href="http://www.facebook.com/ClubShopRewards" target="_blank"><img height="28" src="/images/icons/icon_face_book.png" width="28" /></a> <a href="http://twitter.com/clubshop" target="_blank"><img height="28" src="/images/icons/icon_twitter.png" width="28" /> </a></p>
<p>Volg ons op onze ClubShop Rewards Partners Facebook pagina om het laatste Partner nieuws, wedstrijd- en promotie updates en meer te lezen...</p>
<p><a href="http://www.facebook.com/pages/ClubShop-Rewards-Partners/294328860618046" target="_blank"><img height="28" src="/images/icons/icon_face_book.png" width="28" /></a></p>
<p><span class="style24">COMMISSIE UITBETALING</span></p>
<p>Neem even de tijd en selecteer op welke manier u wilt dat wij uw Partner Inkomen overmaken. Wanneer u geen keuze maakt, worden uw verdiensten toegevoegd aan uw <a href="https://www.clubshop.com/manual/business/clubaccount_help.xml" target="_blank">ClubAccount</a> waar u het kunt gebruiken om bijdragen te betalen en sommige aankopen te betalen of om het op een later tijdstip uit te laten betalen.</p>
<p><a href="https://www.clubshop.com/manual/business/commission.xml" target="_blank">Kies de uitbetalingmethode van uw inkomsten</a></p>
<p><span class="style24">GPS ABONNEMENT BIJDRAGE </span></p>
<p>Wanneer u zich heeft ingeschreven voor het GPS, dan is het belangrijk ervoor te zorgen dat uw bijdrage voor het eind van iedere maand door ons ontvangen is, om automatisch te blijven kwalificeren als Partner en volgende maand gebruik te blijven maken van het systeem.</p>
<p>Zodra u meer heeft verdient aan Partner Inkomsten dat u moet betalen als maandelijkse bijdrage, dan zal de bijdrage in mindering worden gebracht op uw inkomstenen en is het niet meer nodig om de bijdrage zelf te betalen. Tot het zover is, is het belangrijk dat u een manier heeft gekozen om uw bijdrage elke maand te betalen:</p>
<p><a href="https://www.clubshop.com/manual/business/fees.xml" target="_blank">GPS          Bijdrage Betaalmethode</a></p>
<p><span class="style24"> NIEUWE MEMBERS VERWELKOMEN</span></p>
<p>Elke keer dat u een nieuwe Member verwijst, ontvangt u een melding van hun lidmaatschap, zodat u hen een persoonlijke welkom e-mail kunt sturen.</p>
<p>Hier is een voorbeeld van het soort e-mail dat u zou moeten versturen aan al uw nieuwe door u persoonlijk verwezen Members:</p>
<div class="toggle" style="display:none;">
						<div class="row">
							<div class="eleven columns offset-by-one">
								<div class="panel"> 
<p>Subject: <span class="style4">[VOORNAAM]</span> - Welkom bij de Club!</p>
<p>Hallo <span class="style4">[VOORNAAM]</span>,</p>
<p>Bedankt dat u een ClubShop Rewards Member bent geworden en ons helpt de voordelen te vergroten die we allemaal kunnen ontvangen door meer groepskoopkracht.</p>
<p>Wanneer u dit nog niet heeft gedaan, dan wil ik u aanmoedigen om hier naartoe te gaan  <a href="http://www.clubshop.com" target="_blank">www.clubshop.com</a> en de volgende twee dingen te doen om uw eerste ClubCash (geld terug) in uw account te laten storten:</p>
<ol>
<li>Neem een paar minuten van uw tijd om de volgende korte vragenlijst met uw persoonlijke voorkeuren in te vullen <a href="https://www.clubshop.com/mall/other/questionnaire.xsp" target="_blank">vragenlijst</a>. Elke maand zal het ClubShop Rewards team dan kijken of zij speciale aanbiedingen kunnen vinden die overeenkomen met uw interesses en deze publiceren als uw persoonlijke "Top Aanbiedingen" op www.clubshop.com - <span class="bold">en u verdient er direct uw eerste ClubCash mee, alleen voor het invullen van de vragenlijst!</span></li>
<li> Wanneer u een volgende aankoop wilt doen, kijk dan even of de winkel waar u de aankoop wilt doen opgenomen is in het ClubShop Rewards Winkelcentrum. Indien dit zo is, klik daar dan op de link en winkel. <span class="bold">Voor uw eerste aankoop verdient u een ClubCash Bonus bovenop de ClubCash die u al verdient op het bedrag van uw aankoop.</span> ClubCash percentages staan naast elke winkel op de lijst in het ClubShop Rewards winkelcentrum.</li>
</ol>
<p>Elke keer dat u online gaat winkelen, kijk dan even of er een winkel in het ClubShop Rewards Winkelcentrum staat waar ze aanbieden wat u zoekt. Indien dit het geval is, doe uw aankoop dan via die winkel en <span class="bold">verdien meer ClubCash</span>.</p>
<p>Wanneer u vervolgens genoeg ClubCash hebt gespaard om een uitbetaling aan te vragen voor het bedrag dat u wenst - laat u het gewoon uitbetalen! Het is nu heel eenvoudig en gemakkelijk om geld terug te krijgen wanneer u online winkelt! Elke keer dat u een aankoop doet, heeft u ook nog eens een kans om geweldige prijzen te winnen!</p>
<p>Indien u vragen heeft over uw lidmaatschap of het winkelen via het ClubShop Rewards winkelcentrum, laat het mij dan even weten.</p>
<p>Nogmaals bedankt dat u een ClubShop Rewards Member bent geworden en meehelpt mijn zaak te laten groeien.</p>
<p>Met vriendelijke groeten,<br /> <span class="style4">UW NAAM</span></p>
<p>P.S. - Indien u interesse heeft om uw lidmaatschap te gebruiken om tevens geld te verdienen, bekijk dan ons gratis Affiliate programma:<br /> <a href="http://www.clubshop.com/become_an_affiliate.html">http://www.clubshop.com/become_an_affiliate.html</a></p>
</div></div></div></div>
<p>Kopieer deze e-mail en bewaar deze als een Template (voorbeeld) en maak de nodige veranderingen voordat u deze verstuurd naar uw nieuwe Members.</p>
<br /> 
 <br/>
      
	  
	  
	  <p>Once you have sent the above email out, post their Follow Up record as follows:
	  <br/>
	  		<ul>
				<li class="blue_bullet">Followup Notes: Sent Welcome Email </li>
				<li class="blue_bullet">Reminder Notes: F/U for survey</li>
				<li class="blue_bullet">Reminder Date: Set for 5 days later</li>
			
			</ul>
	  </p>
	  
	  <p>In five days, this reminder will appear in your Activity Report, so you can continue the Follow Up Procedure if they don't complete the survey.</p>

	
 <p><span class="style24">SURVEY NOT COMPLETED?</span>- If you don't see the "Survey Completed" entry in the Activity Report by the time the Followup Reminder appears, send this as a "Followup For Survey" email:</p>
 <div class="toggle" style="display:none;">
						<div class="row">
							<div class="eleven columns offset-by-one">
								<div class="panel"> 
								<p>Subject: <span class="style3">FIRSTNAME</span> - how to start earning CASH BACK with your ClubShop Rewards membership!</p>

<p>Hello <span class="style3">FIRSTNAME</span>,</p>

<p>As your ClubShop Rewards Membership Consultant, I want to help you to take full advantage of your ClubShop Rewards membership to save money and earn cash back (ClubCash) whenever you shop online at one of the hundreds of stores listed at our ClubShop Mall.</p>

<p>You can help us understand the type of stores you may shop at, or the type of things that you may purchase when shopping online, by completing a short "Personal Preference" Survey, which provides you with two membership benefits:</p>
<ol>
    <li>You will actually earn some ClubCash, which will be deposited into your personal ClubCash account and which can be redeemed for actual cash money once your ClubCash balance reaches $25.00 USD.</li>

    <li>Whenever ClubShop finds a great deal or special offer that matches one of your shopping preferences, it will be shown in your "Top Picks" list.</li>
</ol>
<p>You can view your purchases, ClubCash credits and balance, and "Top Picks" from the <a href="http://www.clubshop.com">www.clubshop.com</a> home page.</p>

<p>How can you earn more ClubCash? Simple! Just make your first purchase at any of the hundreds of stores listed at the ClubShop Mall. The percentage of ClubCash you can earn is shown at each store listing. As a ClubShop Rewards Member, you can now earn CASH BACK whenever you click on the store listing at the ClubShop Mall and make a purchase with that store! That's right! As a ClubShop Rewards Member, you will earn CASH BACK when you shop at any of the stores listed at the ClubShop Mall.</p>

<p><span class="bold">And when you make your very first purchase at a <a href="http://www.clubshop.com">www.clubshop.com</a> store, ClubShop Rewards will add a ClubCash Bonus to your account to thank you for using your membership and helping is to increase our group buying power!</span></p>

<p>As a ClubShop Rewards Partner, I will act as your personal consultant to help you with any questions you may have about how to use your membership to save money and earn ClubCash. Just send me an email or contact me on Skype with your questions and I'll be glad to help you.</p>

<p>Thanks again for becoming a ClubShop Rewards Member!</p>

<p><span class="style3">YOUR NAME</span><br/>
ClubShop Rewards Partner<br/>
Skype Name: <span class="style3">YOUR SKYPE NAME</span></p>

<p>P.S. - If you are interested in referring others to become ClubShop Rewards Members and also using your membership to make money, check out our free Affiliate program:
<a href="http://www.clubshop.com/become_an_affiliate.html">http://www.clubshop.com/become_an_affiliate.html</a></p>
								
								</div> 
							</div>
						</div>
					</div>
					
					
					
<p><span class="style24">SURVEY COMPLETED?</span> -  If your Member does complete the survey, send them this email to encourage them to make their first purchase:</p>
 <div class="toggle"  style="display:none;">
						<div class="row">
							<div class="eleven columns offset-by-one">
								<div class="panel">  
	  
	  <p>Subject: <span class="style3">FIRSTNAME</span> - you've earned ClubCash with your ClubShop Rewards membership!</p>

Hello <span class="style3">FIRSTNAME</span>,

<p>Congratulations on completing your ClubShop Rewards "Personal Preference" Survey, which provides you with two membership benefits:<br/>
<ol>
  <li>  You've earned your first ClubCash, which can be redeemed for actual cash money once your ClubCash balance reaches $25.00 USD.</li>

    <li> Whenever ClubShop finds a great deal or special offer that matches one of your shopping preferences, it will be shown in your "Top Picks" list.</li>
</ol></p>
<p>You can view your purchases, ClubCash credits and balance and "Top Picks" from the <a href="http://www.clubshop.com">www.clubshop.com</a> home page.</p>

<p>How can you earn more ClubCash? Simple! Just make your first purchase at any of the hundreds of stores listed at the ClubShop Mall. The percentage of ClubCash you can earn is shown at each store listing. As a ClubShop Rewards Member, you can will earn CASH BACK whenever you click on the store listing at the ClubShop Mall and make a purchase with that store!</p>

<p>And when you make your very first purchase at a <a href="http://www.clubshop.com">www.clubshop.com</a> store, ClubShop Rewards will add a ClubCash Bonus to your account as well to thank you for using your membership and helping is to increase our group buying power.</p>

<p>As a ClubShop Rewards Partner, I will act as your personal consultant to help you with any questions you may have about how to use your membership to save money and earn ClubCash. Just send me an email or contact me on Skype with your questions and I'll be glad to help you.</p>

<p>Thanks again for becoming a ClubShop Rewards Member!</p>

<span class="style3">YOUR NAME</span><br/>
ClubShop Rewards Partner<br/>
Skype Name: <span class="style3">YOUR SKYPE NAME</span><br/><br/>

P.S. - If you are interested in referring others to become ClubShop Rewards Members and also using your membership to make money, check out our free Affiliate program:
<a href="http://www.clubshop.com/become_an_affiliate.html">http://www.clubshop.com/become_an_affiliate.html</a>
	  
								</div> 
							</div>
						</div>
					</div>
					
					

					
		
					
<p><span class="style24">NEXT STEP?</span> - Your Coach or Executive Director should be including your Member in an email list for them to send out emails no more than once a week and at least once a month. When you have a Member make a purchase, make sure to send them a thank you email. </p>
					
	  






    
<hr />
<p align="center"><img src="/images/icons/icon_bullet_orannge.png" /> <a href="../sponsoring/index.xml">Next Lesson</a></p>
<p><img alt="Startpagina Handleiding" height="64" src="/images/icons/icon_home.png" width="64" /></p>
</div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2013           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>