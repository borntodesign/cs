﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>
<script type="text/javascript" src="/cgi/PSA/script/11"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>

<title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />

<!--  <link rel="stylesheet" type="text/css" href="/css/foundation/app.css" /> -->

<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />

<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

<script src="/js/foundation/foundation.js"></script>

<!--  <script src="http://www.clubshop.com/js/foundation/app.js"></script> -->

<script src="/js/foundation/flash.js"></script>

<script src="/js/foundation/jquery.reveal.js"></script>

<script src="/js/panel.js"></script>

<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style>

.b{
/*	display: none; */
	font-size: 95%;
	background-color: #DFEAFF;
	padding: 3px 6px;
	border: 1px solid silver;
	margin-top: 2px;
	width: 90%;
}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">Chief Director Course </span> 
<hr />
</div>
</div>
</div>
<div class="row">
<div class="twelve columns"><span class="style24">INTRODUCTIE</span>
<p>Executive Director worden is de belangrijkste stap om meer controle te krijgen over uw ClubShop Rewards Zaak en vooruitgang te boeken bij het verhogen van uw inkomen om een full time inkomen te ontwikkelen. To do so, you will need to continue to increase your number of Personally Referred Partners, your Group Referred Partners and your Team Pay Points, while also helping your most productive downline Coaches to become Executive Directors too.</p>
<p>As you increase the number of "Stars" you have, you will also increase your Personal and Team Commissions. Zodra u kwalificeert als een Zes Star Executive Director, wordt u lid van de felbegeerde "Ambassadors' Club" met de mogelijkheid om niet alleen een full time inkomen te hebben, maar bent u ook in staat een doel te realiseren waar de meeste mensen alleen maar van kunnen dromen - tijd EN financiële vrijheid.</p>
<span class="style24">ALGEMENE ROL</span><br/><br/>
<p>De Executive Director positie biedt u de hoogste en de uiteindelijke leidinggevende rol voor uw team.  With this role comes the overall responsibility to ensure that your organization continues to grow and thrive due to good duplication of efforts by the Coaches in your team.</p>
<p>As a new Executive Director, you should meet with your Executive Director and review the Followup Records for the Marketing Directors that you will now be the Coach for. And you should contact these Directors to set up your first mentoring meeting with them. Remember, no Partner wants a new boss, but everyone needs a good coach.</p>
<p>As an Executive Director you are the Chief Coach for the Partners in your organization.  U heeft nu vele assistent coaches, player coaches, players en trainees in uw team en uw toekomstige succes ia afhankelijk van uw vermogen om uw team te leiden door:</p>
<ul>
<li class="blue_bullet">De goodwill ambassadeur te worden van uw team door het toevoegen van uw bevestiging, erkenning en lof aan uw Coaches, voor de inspanningen en prestaties van de partners in uw team.</li>
<li class="blue_bullet">Het lokaliseren en het ontwikkelen van zelf-gemotiveerde mensen en mensen met zelfdiscipline om Coaches en toekomstige Executive Directors te worden binnen uw team.</li>
<li class="blue_bullet">Een "Trainer van Trainers" te worden door toezicht houden, begeleiding en coaching van de Coaches in uw team.</li>
<li class="blue_bullet">Host elke maand webinars voor uw team, om nieuwe Partner orientatie in op te nemen, Partner Training en Team bijeenkomsten te geven.</li>
<li class="blue_bullet">Trouble shooting en problemen oplossen die zich voordoen binnen uw team.</li>
<li class="blue_bullet">Het optreden als de contactpersoon van het bedrijf om geschillen tussen partners of vermeende schendingen van een beleid of de Algemene Voorwaarden te melden.</li>
<li class="blue_bullet">Verantwoordelijkheid te nemen voor de Executive Directors Team Pagina.</li>
<li class="blue_bullet">Een Pay Agent te worden (Alleen Internationaal).</li>
</ul>
<hr />
<p class="style28">LEIDERSCHAP PRINCIPES</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">

<p class="ContactForm_TextField bold">STARTING A NEW LINE AND BUILDING A NEW TEAM USING TNT</p>
	  <p>Once a Marketing Director in a team qualifies as an Executive Director, they are then assigned to lead the team, allowing the next upline Executive Director to start a new line and build a new team. This will provide the upline Executive Director with greater profitability and enable them to lay the foundation to become an Executive Director with multiple "Stars".
		</p>  
	  <p>Before you begin to refocus your efforts on building a new team, you should meet with your new Executive Director and review the Followup Records for the Directors that they will now be the Coach for. And you should contact these Directors to let them know of their change in coaches, to help your new Executive Director have a smooth transition with taking over the leadership for the team. </p>
	  <p>Although you will no longer be adding new Partners to this team, you should continue to acknowledge newly promoted Partners and praise the Sponsors on the team! Remember, this team is still part of your overall sales organization from which you will continue to earn commissions, so don't stop paying attention to the people that are making the team grow! </p>
	  
	  
	  
	  
      <br/><br/>								


<ol>
<li><span class="ContactForm_TextField bold">Het goede voorbeeld geven</span> - De belangrijkste verantwoordelijkheid die u heeft als een leider, is om consequent het goede voorbeeld te geven. Veel leiders denken dat zij anderen succesvol kunnen leiden, motiveren en coachen om dingen te doen die zij zelf niet doen. De grootste inschattingsfout die u kunt maken is te denken dat uw positie of titel of wat u in het verleden heeft gedaan, u de geloofwaardigheid geeft om met succes te leiden. Dat zal het beslist niet.<br /><br /> Om succesvol te zijn in deze business, MOET u het goede voorbeeld geven in alles wat u doet. Om u te helpen dit consequent te doen, stel uzelf de volgende vraag om vast te stellen welke keuze u zou moeten maken: <br /><br /> <span class="style3 bold">"Wat als iedereen in mijn organisatie dit zou doen? Zou dit dan goed zijn?"</span> </li>
<br /><br />
<li><span class="ContactForm_TextField bold">Interne groei gaat vooraf aan externe groei</span> - Wat er nodig is om successvol een team te ontwikkelen van marketeers, marketing managers en marketing directors is uniek en niet te vergelijken met alles wat u in het verleden heeft gedaan, met uitzondering van het ouderschap. Het leerproces kan moeilijk zijn, zelfs als u al eerder succesvol was in sales, marketing of management. <br /><br /> Op basis van eerdere ervaringen, zal ieder persoon op andere momenten op diens reis moeilijkere momenten hebben om door te gaan, dan iemand anders. Onderwijs is nodig bij elke stap omdat het een vereiste is dat iemand persoonlijk groeit om professioneel te kunnen groeien. Om succes te hebben als Executive Director en een Ambassadors Club Member te worden, moet u leren hoe u bedreven raakt in verkopen en marketing, in coachen en in leiden. <br /><br /> Veel mensen denken dat het creëren van succes extern is, gebaseerd op wat er met ons gebeurd. Dit is een verkeerd geloof. Het creëren van succes is intern, gebaseerd op wat er gebeurd in onze geest en gedachten. </li>
<br /><br />
<li><span class="ContactForm_TextField bold">Leiders zijn lezers</span> - Alhoewel ons Training Gids en Handleiding en het GPS geweldige hulpmiddelen zijn, is het nodig dat u zich continue zelf persoonlijk blijft ontwikkelen en deskundige te worden op elk gebied dat betrekking heeft op uw business. Hier zijn een paar onderwerpen om meer over te lezen om u te helpen het beste uit u zelf te halen, zowel persoonlijk en als leider voor uw organisatie: <br /><br /> 
<ul>
<li class="half">Spirituele bewustwording, Geloof en Kracht</li>
<li class="half">Positieve mentale houding</li>
<li class="half">Zelfbeeld</li>
<li class="half">Doelen stellen</li>
<li class="half">Verkopen en marketing</li>
<li class="half">Mensen vaardigheden</li>
<li class="half">Tijd management</li>
<li class="half">Coachingsvaardigheden</li>
<li class="half">Financieel beheer en Planning</li>
</ul>
<p>Kies een categorie uit bovenstaande lijst en waarvan u voelt dat u deze nog kunt verbeteren. Ga naar het ClubShop Rewards Winkelcentrum en bestel een boek over dit onderwerp. Bestel 1 of meerdere boeken waarvan u denkt dat ze u kunnen helpen.</p>
<p>Ga vervolgens door met bovenstaande lijst en blijf lezen. Stop nooit met leren en verbeteren wie u bent, dit is niet alleen in uw eigen voordeel, maar ook voor vele anderen.</p>
<p>Maak er een gewoonte van om elke dag tenminste 15 minuten te lezen. Lezen voor het slapen gaan zal zelf uw onderbewustzijn in staat stellen om beter te absorberen wat u heeft gelezen.</p>
</li>
<blockquote><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="ltgreen bold">Aanbevolen literatuur</span> <img alt="Bringing Out the Best In People" height="119" src="bringbestbook.png" width="72" /> - <em><strong>"Bringing Out the Best in People"</strong></em> door Alan Loy McGinnis<br /><br /> <span class="style1">"Dit kan een van de beste boeken zijn om u te helpen coachen, beheren en leiden van uw team!" ClubShop Rewards Oprichter - Dick Burke</span></blockquote>
<br /><br />
<li><span class="ContactForm_TextField bold">Vermenigvuldiging vereist Duplicatie</span> - Uw Coaches helpen om succesvolle marketingmethoden en opvolgmethoden te dupliceren is de enige manier om uw team te laten groeien door "Vermenigvuldiging" in plaats van toevoeging. Duplicatie is de sleutel! De meeste mensen hebben het nodig om een goed voorbeeld te zien om hen aan te moedigen de volgende training te volgen en de meeste mensen hebben tevens een coach, mentor en vriend nodig, om hen te helpen leren hoe zij succesvolle methoden en technieken dupliceren.	 <br /><br /> Hoe uw Partners te trainen:<br /><br /> <ol>
<li>U voert een taak uit terwijl zij observeren (het best hebben zij ook een geschreven handleiding om te volgen terwijl zij observeren). </li>
<li>Laat hen vragen stellen en u vertellen wat zij observeerden.</li>
<li>Zij voeren een taak uit terwijl u observeert (misschien is een "rollenspel" vereist, voordat zij het vertrouwen hebben om te communiceren met mensen).</li>
<li>U stimuleert, spoort aan en corrigeert.</li>
<li>Zij voeren de taak/ taken uit zonder u, maar observeer van een afstandje. Stimuleer, moedig aan en corrigeer totdat u hen succesvol de methode of techniek ziet dupliceren.</li>
</ol> </li>
<br /><br />
<li><span class="ContactForm_TextField bold">Delegeer of Stagneer</span> - Zodra u zeker weet dat u een rol of verantwordelijkheid hebt kunnen delegeren aan een Partner of Coach in uwteam, dan moet u deze rol naar hen delegeren - zelfs als dit betekent dat u de controle uit handen geeft of iets opgeeft dat u graag doet. <br /> <br />
<p>Veel mensen nemen nooit de tijd om mensen goed te trainen of de vaardigheden te ontwikkelen om kundig te worden als Coach of Leider. Dit vereist van iemand voortdurend het werk te doen dat nodig is om hun verkopen en inkomen te onderhouden. Wat betekent dat geen duplicatie = geen delegatie = stagnatie (gebrek aan groei), wat uiteindelijk kan leiden tot een daling van de omzet en inkomen.</p>
<p>Sommige van uw Partners kunnen sommige dingen niet zo goed als u ze doet, ondanks al uw inspanningen om hen op te leiden. Dat is niet erg. Het belangrijkste is dat zij de dingen kunnen doen die van het vereist worden op een manier die bekwaam genoeg is om hun omzet en inkomen te verhogen - zelfs wanneer dit in een lager tempo is dan wanneer u het voor hen zou doen.</p>
<p>Uw ultieme succes zal afhangen van het hebben van veel mensen die uw inspanningen kunnen "vermenigvuldigen".</p>
</li>
</ol></div>
</div>
</div>
</div>





<p class="style28">EXECUTIVE DIRECTOR COMPENSATIE</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><span class="medblue"> ( <a href="http://www.clubshop.com/manual/compensation/partner.html" target="_blank">Partner Compensatieplan</a> )</span> <br /><br /> <br /><br /> 

<span class="ContactForm_TextField bold">Personal Commissions</span> - As you continue to increase the number of customers and Personally Referred Partners you have, your Personal Commissions will increase. Continue to lead by example in your personal purchases, sales and sponsoring.<br/><br /> 
When you increase your Personal Commissions, you will also increase your Team Commissions because of how TNT works. And you can influence the Partners on you team to become active due to their seeing new Partners being added to their team by YOU. This will also increase your credibility to coach Partners, Managers and Directors.
<br /><br /> 

<span class="ContactForm_TextField bold">TEAM COMMISSIONS</span> 
              - To increase your Team Commissions, you also need to identify the 
              best prospective Executive Directors for each line that you have, 
              so you can begin to observe their performance and coach them to 
              improve their results. <br/>
              <br/>
When you increase your Personal Commissions, you will also increase your Team Commissions because of how TNT works. And you can influence the Partners on you team to become active due to their seeing new Partners being added to their team by YOU. This will also increase your credibility to coach Partners, Managers and Directors.
<br /><br /> 


<p><img alt="play" height="28" src="../../../images/play.png" width="27" /> <a class="Directory_Grey_Underline" href="../flash/Executive_Director_Course_x3.swf" target="_blank">ZIE VOORBEELD </a></p>
<br /> <span class="ContactForm_TextField bold">Executive Director Bonuses</span> - ou are able to earn a "Star" credit for each Partner that becomes an Executive Director on your team, to help you qualify to earn Executive Director Bonuses, which are paid as additional bonus shares of Team Commissions. <br /><br /> 

</div>
</div>
</div>
</div>
<p class="style28">UW ROL NAAR UW ORGANIZATION</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<ul>
<li><span class="style24">Maak Team Pagina's</span></li>
<div class="toggle">
<div class="panel">
<p>Ten eerste heeft u de rol van Hoofdcoach en de eerstverantwoordelijke                  leider. Om dit efficiënt te doen zou u een Team Facebook Pagina                  moeten maken en een Executive Director Team Pagina. Uw teampagina                  zal worden gekoppeld aan onze database om te laten zien welke                  nieuwe Partners, nieuwe Promoties en de Coaches er in uw team                  zijn. Volg deze instructies om uw Executive Director Teampagina                  te maken.</p>
<p>Maak vervolgens gebruik van de mogelijkheid om uw eigen webpagina                  te krijgen - <a href="http://www.clubshop.com/mall/tools/tools.html" target="_blank">http://www.clubshop.com/mall/tools/tools.html</a> Vervolgens moet u alles kopieren wat er tussen de <a href="team_page.txt" target="_blank">Team                  Page (text document)</a> gestreepte lijnen staat hieronder en                  plakt u dit in uw web pagina editor. Volg dan de instructies hieronder                  over hoe u deze pagina kunt bewerken en stel ons op de hoogte                  van uw URL. Verander niets anders dan in de instructies is aangegeven,                  anders zal uw pagina niet correct worden weergegeven.</p>
<p class="medblue">UW ID NUMMER: 12346 zonder initialen</p>
<span class="ContactForm_TextField bold">Voer hieronder uw inhoud                in</span> <br /> <br /> Dit is het middelste gedeelte van uw teampagina... u kunt hier alles                op uw pagina plaatsen waarvan u wilt dat uw team van op de hoogte                is... nieuwe promoties, team bijeenkomsten, aankondigingen... alles                wat betrekking heeft tot het informeren van uw team. U kunt ook                afbeeldingen toevoegen. Sla uw bestand op in de UTF-8 character                set wanneer u deze mogelijkheid heeft, anders kunt u vreemde '?'                tekens in uw tekst krijgen. Sla het vervolgens op op <span class="bold">UW</span> domein. <br /> <br /> Als je eenmaal hebt ingesteld Team webpagina selecteert u simpelweg uw                      URL naar onze <a href="mailto:supportspvr@clubshop.com?Subject=Team Page URL">lidmaatschap                      Department</a> zodat kan worden beoordeeld en geactiveerd.<br /> <br />
<p class="medblue">Opmerking: Introduceer uzelf, in de eerste versien,                  als de nieuwe Executive Director zodat daar geen verwarring over                  ontstaat.</p>
<br /> <!--ends the panel--></div>
<!--ends the toggle--></div>
<li><span class="style24">Weekelijkse Team Bijeenkomsten</span></li>
<div class="toggle">
<div class="panel"><!--enter new here-->
<p>Als Executive Director, zou u wekelijkse Team bijeenkomsten moeten hosten of co-hosten met uw downline of upline Executive Director(s), op een vaste avond in de week (Maandag - Donderdag, op een andere avond dan ingeplande Webinars van het hoofdkantoor of CEO Chats). GPS PREMIER abonees kunnen gebruik maken van de Online Meeting Room voordelen om dit te organiseren.</p>
<p>Alle Affiliates en Partners in uw team zou u wekelijks moeten uitnodigen om deel te nemen aan deze bijeenkomsten via e/mail en door berichten en aankondigingen te plaatsen en deze te linken aan uw Team Pagina. Verzeker u ervan dat u de link telkens aan het eind van de week aanpast, voor de Team bijeenkomst van volgende week.</p>
<p>Open de vergaderruimte tenminste 5 minutes voordat de bijeenkomst begint, zodat u als "presenter" alles in orde kunt maken en de eerste dia van de Partner Zakelijke Mogelijkheid Presentatie in beeld kunt zetten, voordat u met de bijeenkomst begint.</p>
<p>Start de bijeenkomst niet later dan 5 minuten volgens planning door te beginnen met het "Laten zien van het Plan" door gebruik te maken van de Dia presentatie. Indien u de meeting room deelt met andere Executive Directors, dan zou u elke Executive Director moeten uitnodigen om hun verantwoordelijkheden van het `laten zien van het Plan` te delen. Heeft u een GROEIENDE Director, dan kunt u deze uitnodigen om "Het Plan te laten zien" om hem- haar te erkennen als leider en voor te bereiden om vaker de host rol op zich te nemen voor Team bijeenkomsten, zodra deze Executive Director is geworden.</p>
<p class="style24">Meeting Agenda:</p>
<ol>
<li>Eerste 30 minuten - Laat het Plan zien door gebruik te maken van de Partner Zakelijke Mogelijkheid Presentatie slide show. </li>
<li>15 minuten - Nodig niet meer dan 3 andere Executive Directors en GROEIENDE Partner uit om testimonials te geven, gevolgd door wat tijd om vragen van Affiliate's (geen Partners) te beantwoorden. </li>
<li>45 minuten - Partner Training (zie hieronder). Indien u de meeting room deelt met andere Executive Directors, dan zou u elke Executive Director moeten uitnodigen om om de beurt de Partner Training te geven.</li>
<li>5 minuten - Partner Vragen &amp; Antwoorden</li>
<li>5 minuten - Partner Erkenning - Verwelkom nieuwe Partners en erken Partners die in de afgelopen week nieuwe Partners hebben aangesloten, die kwalificeren in de Verkoop wedstrijd, of een promotie hebben gekregen. </li>
<li>5 minuten - Executive Director <a href="http://www.thefreedictionary.com/pep+talk" target="_blank"><span style="text-decoration: underline;">Pep Talk</span></a> - Indien u de meeting room deelt met andere Executive Directors, dan zou u de andere Executive Director moeten uitnodigen om de beurt wekelijks een Pep Talk te geven.</li>
</ol> 
<hr />
<p class="style24">Partner Training</p>
<p>Het hoofdkantoor kan u een Trainingsgids geven dat u kunt volgen en gebruiken, met name wanneer training nodig is over veranderingen of verbeteringen die zijn doorgevoert in het Marketing en-of Compensatieplan. Het hoofdkantoor kan tevens een Training Gids bieden voor andere specifieke onderwerpen die moeten worden opgepakt.</p>
<p>Voor de weken dat u geen Training gids krijgt, dient u zelf training te verzorgen in de VERKOOP &amp; MARKETING BEGINSELEN die vereist zijn om uit te voeren voor toename van verkopen en de grootte van elke Partners Verkoop team, door gebruik te maken van de Handleiding en Training Cursussen al referentie en support webpagina´s.</p>
<hr />
<p>Indien u niet in staat bent of zich ongemakkelijk voelt bij het hosten van uw eigen Team bijeenkomsten, dan zou u uw Affiliates en Partners moeten doorsluizen naar de team bijeenkomsten van een upline Executive Director, totdat u in staat bent of het zelfvertrouwen hebt in het hosten van uw eigen Team bijeenkomsten.</p>
<p>Stuur wekelijks een herinnering e/mail naar al uw Affiliates en Partners in uw team, doe dit uiterlijk vrijdags voor de Team bijeenkomst van de volgende week met dag- tijd en agenda. Werk dan tevens uiterlijk vrijdags  uw Team Pagina bij.</p>
</div>
<!--ends toggle--></div>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<p class="style28">BESTEED AANDACHT AAN UW BESTE PARTNERS</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Wanneer u genoeg mensen wilt helpen te krijgen wat zij wwillen, dat krijgt u automatisch wat u zelf wilt!</p>
<p>Bloemen groeien richting de zon! 1 van de belangrijkste dingen die u kunt doen om de beste omgeving te creëren om promoties in uw team aan te moedigen en te realiseren is eenvoudig door aandacht te besteden aan mensen:</p>
<ul>
<li class="half">Accepteer hen</li>
<li class="half">Waardeer hen</li>
<li class="half">Bevestig hen</li>
</ul>
<p>Om dit te doen, gebruikt u uw GPS Activiteiten Rapport om te erkennen:</p>
<ul>
<li class="blue_bullet">Wie de nieuwe Partners zijn. Stuur hen allemaal een welkom e-mail of een bericht of bel ze op om ze te verwelkomen.</li>
<li class="blue_bullet">Wie de Sponsors zijn. Stuur hen allen een korte e-mail of een berichtje om hun prestatie te erkennen.</li>
<li class="blue_bullet">Wie een promotie hebben gekregen. Maak hier een big deal van!
<p>In het boek, <em><strong>"How To Win Friends and Influence People"</strong></em> , zegt auteur Dale Carnegie ," Wees hartelijk in uw goedkeuring en kwistig in uw lof." Een definitie van "goedkeuring" is "officiële erkenning".</p>
<p>Als de HoofdCoach vaan uw team, dient te hartelijk te zijn in uw goedkeuring (officiële erkenning) en kwistig (extravagant) met uw lof" wanneer het gaat om promoties door het creëren van een Promotie Traditie binnen uw team:</p>
<br />
<blockquote><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="ltgreen bold">Aanbevolen literatuur</span> <img alt="How to Win Friends and Influence People" height="119" src="wininfluencebook.png" width="75" /> <em><strong>"How To Win Friends and Influence People"</strong></em> door Dale                    Carnegie<br /><br /> <span class="style1">"Indien u dit boek niet hebt, het is sterk aanbevolen dat u deze besteld via het ClubShop Winkelcentrum. Dit is 1 van de belangrijkste 'people skills' en boek over menselijke relaties aller tijden." ClubShop Rewards Oprichter - Dick Burke</span></blockquote>
</li>
</ul>
<ul>
<li class="blue_bullet"><span class="ContactForm_TextField bold">ZEGT HET VOORT!</span> 
<ul>
<li class="half">Stuur eerst een e-mail uit naar de nieuwe gepromoveerde Partner. Maak er een speciale e-mail van en hoe groter de promotie, hoe beter de e-mail moet zijn. Gebruik grote, gekleurde letters en lettertypen om uiting te geven aan uw lof. Zet alle upline Partners van de nieuw gepromoveerde Partner in cc.</li>
<li class="half">Stuur dan een e-mail naar alle downline Partners van de nieuw gepromoveerde Partner om de Partner "op te bouwen" en hen aan te moedigen om de "yellow brick road" te volgen om zelf ook promoties te verdienen. Geef een link mee naar de Training Gids in deze e-mails en probeer ook een dioloog aan te gaan door hen uit te nodigen contact met u op te nemen.</li>
</ul>
</li>
<li class="blue_bullet"><span class="ContactForm_TextField bold">ERKEN PUBLIEKLIJK WAT ZIJ HEBBEN BEREIKT</span> 
<ul>
<li class="half">Plaats een bericht op uw Team- en Facebook Pagina's.</li>
<li class="half">Neem het "nieuws" op in uw Maandelijkse Nieuwsbrief.</li>
<li class="half">Erken hen tijdens uw Team Bijeenkomst.</li>
</ul>
</li>
<li class="blue_bullet"><span class="ContactForm_TextField bold">ZORG DAT ZIJ HUN PAY TITEL PIN TOEGEZONDEN KRIJGEN</span> 
<ul>
Communiceer met hun Coach om na te gaan of zij hun "oude" pin kunnen sturen (als ze deze nog steeds hebben) of dat zij een nieuwe pin willen bestellen via <a href="http://www.clubshop.com/outletsp/bcenterpins.html" target="_blank">http://www.clubshop.com/outletsp/bcenterpins.html</a> <br /> <br /> Wanneer hun Coach hen geen pin wil geven, dan raden wij aan dat u dit doet. Deze Partner kan 1 van uw beste kandidaten zijn om te begeleiden en coachen om een toekomstige Executive Director te worden en "het kan mensen niets schelen hoeveel u weet, totdat zij weten hoeveel het u kan schelen". U wordt geen Ambassadors Club Member door het zijn van een goede leider - u wordt dit door een goede leider VAN leiders te zijn. 
</ul>
</li>
</ul>
<span class="style24">CONTROLEER DE GLOCAL GENERATION WEBPAGINA VAN UW PARTNERS</span><br />
<p>Zodra een Partner een persoonlijke Glocal Generation webpagina heeft gemaakt, krijgt u een bericht met de URL naar hun pagina. U dient dan hun pagina door te nemen om te controleren op spelfouten, slechte grammatica, een aanstootgevende foto, geen foto en/of twijfelachtige uitspraken.</p>
<p>U moet er ook op letten of zij goede zoekwoorden hebben gebruikt. Zoekwoorden die gebruikt zijn in de URL bestandsnaam zouden ook gebruikt moeten worden in het begin van de  Titel Meta Tags. De zoekwoorden die gebruikt zijn in de Titel Meta Tags moeten ook meerdere malen gebruikt worden in de inhoud van de pagina. Deze twee dingen zijn erg belangrijk bij het maken van een zoekmachine-vriendelijke PRW!</p>
<p>Gebruik makend van goede coaching vaardigheden, neemt u contact op met de Partner en bespreekt u of en hoe zij hun pagina kunnen verbeteren om de beste resultaten te krijgen voor hun pagina.</p>
<p>Wanneer u ontdekt dat er eventuele ongepaste opmerkingen of foto's zijn geplaatst, neemt dat onmiddelijk contact op met het hoofdkantoor zodat wij de pagina ontoegankelijk kunnen maken totdat het probleem is opgelost.</p>
</div>
</div>
</div>
</div>
<p class="style28">POTENTIELE TEAMZIEKTEN EN PROBLEMEN</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p><img alt="microscope" src="microscope.jpg" /> Als Executive Director bent u verantwoordelijk voor de algemene "gezondheid" van uw team. Daarom moet u zich bewust zijn van bepaalde "ziekten" die van tijd tot tijd uw team zullen infecteren en van hun symptomen en behandelingen.</p>
<p><span class="ContactForm_TextField bold">"Mijngroepitis"</span> - Deze ziekte steekt de kop op wanneer u een sterke, onafhankelijke, controlerende en egocentrisch persoon heeft die DE leider van diens groep wil zijn. Zij willen geen deel uitmaken van een team van leiders of de credits voor de groei in hun team delen en het kan zijn dat zij niet willen dat hun Coach betrokken is in "hun groep".</p>
<p><span class="ContactForm_TextField bold">Stinking Thinking</span> - Ook bekend als "geestelijke halitose" (halitose = slechte adem), deze ziekte kan uitgaan van een leider die geïnfecteerd is met "mijngroepitis" of een individuele Partner die het kan verspeiden. Het heeft doorgaans alleen invloed op het individu zelf, maar wanneer negativiteit over ALLES wordt uitgedrukt aan andere partners in uw team (vooral negativiteit naar de downline), deze ziekte kan fataal worden.</p>
<p><img alt="Skunk - Stinking Thinking" src="skunk.jpg" /> Het tegengif voor deze beide ziekten is niet te werken "via" iemand die tekenen van Mijngroepitis of Stinking Thinking heeft, maar er "omheen" te werken. U kunt een persoon niet tegenhouden relaties op te bouwen met Partners in hun team, maar u kunt en zou moeten werken aan een goede relatie met de beste werkers en leiders "onder" de Mijngroepitis of Stinking Thinking Partner door het erkennen en prijzen van de inspanningen, prestaties en resultaten van de  Partners in hun team, zelfs wanneer u niet hun aangewezen coach bent. Maak een soort levenslijn tussen u en deze Partners zodat u hen kunt helpen een goede leiderschapsrol te vervullen voor hun team.</p>
<p>Bekritiseer of denigreer hun coach niet terwijl u dit doet, omdat u dan zelf schuldig zou zijn van het afgeven van negativiteit richting uw downline. Er zou NOOIT, OOIT enige negativiteit of kritiek moeten worden geuit over het bedrijf of hun upline bij Partners in uw team. Dit lost het probleem niet op, maar exasperates het en als gevolg van duplicatie, vermenigvuldig het, dat is wanneer de ziekte dodelijke afloop.</p>
<p>Partners dupliceren leiders in goede en slechte dingen. Onthou altijd het belang van het Duplicatie Principe wanneer u met Partners in uw organisatie communiceert.</p>
<span class="style24">MAGNETRON MENTALITEIT</span><br />
<p><img alt="Magnetron Mentaliteit" src="microwaves.jpg" /> Ook bekend als korte termijn denken, deze aandoening is algemeen verspreid in het grootste deel van de moderne wereld. We staan 's ochtend op, zetten de douche aan en hebben snel heet water. Dan gebruiken we onze koffiezetapparaten om snel koffie voor ons te zetten. Dan schuiven we snel iets in de magnetron om snel op te warmen. Dan springen we in onze auto en rijden we snel naar ons werk. Wanneer we thuis komen gebruiken we de afstandbediening om direct de televisie aan te zetten. Ziet u het plaatje?</p>
<p>We zijn eraan gewend snel te krijgen wat we willen hebben en de meeste mensen zijn niet bereid om het principe van investeren te eren (ook wel bekend als zaaien en oogsten) om succesvol te zijn. Dit geconditioneerde denken kan beïnvloed worden door goedbedoelende familieleden en vrienden die niet begrijpen of geloven in ons zakelijke mogelijkheid. Het weerspiegelt zich op de volgende manieren:</p>
<ul>
<li class="blue_bullet"><span class="ContactForm_TextField bold">Partners (of echtgenoten) behandelen hun zaak als een baan.</span> Net zoals het starten van een boerderij of boomgaard een investering vereist van tijd en geld voordat er resultaat te zien is, zo is dat bij elk startend bedrijf van elke aard, inclusief een ClubShop Rewards zaak. Indien iemand (of iemands echtgenoot) dit niet begrijpt, dan vergelijken zij als snel de gewerkte uren met het geld dat verdient wordt en stellen zij vast dat "verspilling van hun tijd" is.</li>
<li class="blue_bullet"><span class="ContactForm_TextField bold">Partners behandelen hun zaak als een snel-rijken systeem.</span> Omdat er online zoveel "programma's", pyramide systemen en MLM mogelijkheden beschikbaar zijn, een ongeschoold persoon kan aannemen dat wat wij aanbieden gelijkwaardig kan zijn aan deze dingen. Wanneer deze persoon niet snel veel geld verdient, dan kan deze snel de interesse verliezen.</li>
<li class="blue_bullet"><span class="ContactForm_TextField bold">Partners behandelen hun zaak als een investeringsprogramma.</span> Dit duidt op een partner die de initiële en maandelijkse kosten betaalt, maar nooit van start gaat met de training of zelfs maar uw e-mails beantwoord. Zij gaan ervan uit dat alles wat zij hoeven te doen is hun geld investeren om geld te verdienen. Hoewel ons systeem sterk geautomatiseerd is, is het nog steeds nodig om deel uit te maken van het 'bouwproces' om een groot inkomen te verdienen.</li>
<li class="blue_bullet"><span class="ContactForm_TextField bold">Partners haasten zich door de training.</span> Iedereen wil liever eerder dan later geld verdienen, maar wanneer een Partner zich door de training haast of bewezen methoden negeert, dan zijn zij gedoemd te mislukken nog voordat zij zelfs van start zijn gegaan. Moedig Partners aan om een Magnetron Mentaliteit toe te passen wanneer het gaat om het leren van wat zij moeten doen om succesvol te worden.</li>
</ul>
<p>De enige manier om dit soort van denken te overwinnen is leren en het goede voorbeeld geven. U moet herhaaldelijk partners helpen om te begrijpen dat dit een echte zaak is die hen kan voorzien van de financiële en het residuele inkomen dat zij verlangen. Maar om dat te doen, moeten zij eerst een commitment aangaan in inspanning om die financiële en het residuale inkomen te krijgen, maar zij moeten eerst een investering doen in inspanning om te krijgen wat zij willen. Gebruik het principe van zaaien en oogsten van appelpitten om mensen het belang te leren van het niet hebben van een Magnetron mentaliteit en hoe ons bedrijf werkt. Eerst moeten het land, gereedschap en appelpitten worden gekocht (monetaire investering). Vervolgens moet de grond moet worden voorbereid voor de pitten (tijd en inspanning investeren).</p>
<p>Niet alle pitten zullen gaan groeien zij die dat wel doen zullen in het begin niet veel fruit dragen. In de loop van de tijd en met de juiste zorg zullen er veel bomen zijn die veel fruit dragen zonder dat u iets anders moet doen dan zorgen voor de bomen en het fruit te plukken. In ons zaak, door ons systeem en training programma, kunnen we al deze activiteiten delegeren, waardoor de boomgaard eigenaar een groot inkomen heeft zonder meer werk te moeten verrichten of nog meer investereingen te moeten doen!</p>
<img alt="Onderlinge afhankelijkheid" height="227" src="interdependent.png" width="149" /> <br /><br /> <span class="ContactForm_TextField bold">Afhankelijkheid</span> - Dit gebeurt wanneer Coaches niet bereid zijn om rollen over te nemen of wanneer of wanneer zij anderen toestaan om anderen hun werk te laten doen voor hen. In beide gevallen, dient deze eigenschap te worden uitgedaagd. U runt een bedrijf, niet een liefdadigheidsinstelling en uw zaak vereist het nodige werk dat gedelegeerd moet worden en uitgevoerd door elke Coach in uw team. <br /><br /> <span class="ContactForm_TextField bold">Onafhankelijkheid</span> - Hoewel het een kenmerk is van "Mijngroepitis", kan dit probleem worden gezien als individueel probleem binnen uw organisatie. De oorzaak hiervan is doorgaans een "ik weet alles al" houding, versus een "Ik wil het allemaal weten" houding. Moedig dit soort Partners geduldig aan om niet "opnieuw het wiel uit te vinden", maar liever het systeem en de training te volgen. Iedere bijdrage die zij kunnen leveren om van het team een succes te maken zou moeten worden erkend en uitbundig geprezen, om hen aan te moedigen om een teamplayer te willen worden. <br /><br />
<p>Zowel afhankelijkheid als onafhankelijkheid zal niet het soort inspanningen en resultaten produceren die nodig zijn om succesvol te zijn in een ClubShop Rewards zaak. In plaats daarvan is onderlinge afhankelijkheid noodzakelijk en vereist. Iemand kan uiteraard onafhankelijk zijn en een succesvol marketeer zijn, maar als een persoon een team wil hebben dat moet worden getraind, beheerd en geleid om de marketing te doen, dan moet iemand onderlinge afhankelijkheid met hun Coach en Executive Director en met hun downline Partners en Coaches aangaan.</p>
<div align="center"><span class="medblue bold">Teamwerk maakt de droom werkelijkheid!</span></div>
<img alt="Complacency" src="complacency.jpg" /> <br /><br /> <span class="ContactForm_TextField bold">Zelfgenoegzaamheid en Inactiviteit</span>
<p>Dit is een veelvoorkomend probleem bij mensen en als het zich voordoet in uw leiderschap, dan kan het fataal worden, als gevolg van het duplicatie Principe. U zult geduld willen en moeten hebben met mensen terwijl zij de motivatie verkrijgen om de commitment aan te gaan hun eigen zaak op te bouwen, door consequent en volhardend te zijn in hun inspanningen en in het uitvoeren van hun rollen.</p>
<p>Echter, u en uw team kunnen het zich niet veroorloven om ongedisiplineerde of ongemotoveerde mensen te hebben als Coaches. Slechte prestaties te wijten aan onervarenheid en een gebrek aan training zijn te verwachten, maar voortdurende slechte prestaties, zelfingenomenheid of inactiviteit kan niet worden getolereerd. De enige reden dat het iemand niet zal lukken na verloop van tijd, is hun gebrek aan het ijverig volgen en toepassen van de training.</p>
<p>Wanneer tijdelijke situaties (ziekte, emotionele- en relationele problemen, vakanties, etc.) er voor zorgen dat een Coach tijdelijk niet in staat is om diens rollen uit te voeren, dan dient hun Coach het van hen over te nemen zodat de continuïteit en momentum niet verloren gaat.</p>
<p>Als een Team Captain stopt met het helpen van diens Partners om nieuwe Partners te verwijzen en sponsoren om diepte te bouwen, dan kan het nodig zijn dat hun Coach de rol over neemt voor diens Partners, met name wanneer er meer dan twee Partners in 1 lijn onder de inactieve Team Captain staan. U kunt zich niet veroorloven de groei in de diepte te zien stoppen die in ten minste 1 lijn ontstaan is onder elke Team Captain.</p>
<br /><br /> <img alt="Cross Line" src="crossover.gif" /> <br /><br /> <span class="ContactForm_TextField bold">Cross Lining</span>
<p>Dit gebeurt wanneer Partners die niet in hetzelfde team zitten, met elkaar gaan communiceren of elkaar ontmoeten, buiten uw teambijeenkomsten of bijeenkomsten die worden gesponsored door het bedrijf. Hoewel dit kan leiden tot enkele gezonde en stimulerende relaties tussen partners, kan het tevens problemen veroorzaken. Zoals vuur en water, cross lining kan gevaarlijk worden wanneer iets negatiefs wordt gedeeld (over upline of het bedrijf) of wanneer een training, methode of hulpmiddelen worden gedeeld zonder dat het bedrijf of hun Coaches hiervan op de hoogte zijn. Cross lining kan ook de deur openen naar Partners die andere Partners werven voor andere inkomsten mogelijkheden, wat erg schadelijk kan zijn voor uw zaak.</p>
<p>Wees u bewust van de relaties die worden ontwikkeld door partners binnen uw organisatie en doe navraag over wat er gaande is om eventuele problemen op te vangen voordat zij ontstaan. To expect - inspect.</p>
</div>
</div>
</div>
</div>
<p class="style28">WERVEN EN ANDERE SCHENDINGEN VAN HET BELEID</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Als een Executive Director bent u eindverantwoordelijk voor het rapporteren van ieder verdachte beleidsovertreding of overtreding van de algemene voorwaarden door Partners binnen uw team of zelfs buiten uw team. Zie schendingen van het beleid niet over het hoofd en praat het niet goed. Dit te doen, kan fataal worden. Complete Executive Director teams hebben schade opgelopen en zijn bijna vernietigd door het niet rapporteren van van beleidsovertredingen.</p>
<p>1 van de gevaarlijkste overtredingen is het hebben van Partners in uw team die werven voor deelname aan andere "programma's" of om leads te kopen, software of "hulpmiddelen" te kopen. Ondanks dat de intenties van het werven van de Partner goed kan zijn en zelfs in ieders belang kan zijn, is dit beleid er voor een erg belangrijke reden.</p>
<p>Trap niet in de val het gevoel te hebben een "klokkenluider" of "rat" te zijn voor het rapporteren van verdenking van overtredingen. om elke samenleving wettelijk en orderlijk te houden, moet rechtvaardigheid worden geëerd en gediend door de leden van de samenleving. Anders te doen, nodigt uit tot een samenleving vol overtredingen en overtreders, wat zal resulteren in een wetteloze, chaotische of onrechtvaardige maatschappij.</p>
<br /> <span class="ContactForm_TextField bold">Communiceren met Partners in andere Executive Director teams</span>
<p>Zodra een Partner van uw team kwalificeert als een Executive Director, dan neemt deze de verantwoordelijkheden van de Executive Director voor diens team. Vanaf dit moment zou u niet langer moeten optreden als de Hoofdcoach voor de Partners in het team van andere Executive Directors. Alle uitnodigingen of promoties die u wilt aanbieden aan deze Partners, moeten worden aangeboden door elke Executive Director aan hun eigen team.</p>
<p>Zie het <a href="http://www.clubshop.com/manual/policies/communications.xml" target="_blank">Communicatie beleidy</a> voor meer informatie over communiceren met Partners in teams van andere Executive Director.</p>
</div>
</div>
</div>
</div>
<p class="style28">UW ROL BIJ HET BEDRIJF</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Als het officiële aanspreekpunt voor uw team, kunt u direct communiceren met het personeel op het hoofdkantoor om te helpen vragen te beantwoorden of problemen of geschillen op te lossen.</p>
<p>Uw Coaches moeten reageren op alle vragen van Partners in hun teams. Dus tenzij u optreedt in een Coach rol voor een Partner, moeten hun vragen worden verwezen naar hun toepasselijke Coach. De enige vragen die u direct zou moeten beantwoorden zijn vragen van uw Coaches en zelfs dan moet u refereren naar de URL en gedeelte in de handleiding die hun vraag beantwoord. Dit maakt hen duidelijk dat zij bijna alle antwoorden zelf kunnen vinden zonder u te moeten vragen.</p>
<p><span class="bold">Voor contact op te nemen met het Hoofdkantoor</span>, geef het goede voorbeeld door eerst de website goed door te nemen voordat u vragen stelt. In de loop van de tijd hebben hard gewerkt aan het maken van een rijk gevulde website en u zou zich ervan bewust moeten worden waar u moet kijken om de juiste informatie en antwoorden te vinden om vragen mee te beantwoorden die gesteld worden.</p>
<p>Kunt u echter het antwoord op een vraag niet vinden of heeft u duidelijkheid nodig of moet u een probleem oplossen, print en raadpleeg dan onderstaande tabel voordat u contact opneemt met het bedrijf. <span class="bold">U zou nooit het hoofdkantoor moeten bellen, tenzij het een noodgeval is of een extreem tijdgevoelig onderwerp.</span></p>
<p><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <a href="orgchart.xml" target="_blank">Hoofdkantoor Contact Tabel Link</a></p>
<p><span class="ContactForm_TextField bold">Bedrijf Facebook Pagina's</span> - Ziet u een ongepast of lasterlijk commentaar dat geplaatst is op een zakelijke Facebook-pagina, neem dan contact onmiddelijk contact op met het Hoofdkantoor zodat wij dit soort berichten kunnen verwijderen.</p>
<br />
<p><span class="ContactForm_TextField bold">Internationale commissie betalingen</span></p>
<p>Sommige Partners en Members wonen in landen die niet worden ondersteund door de verschillende methoden die wij hanteren om hen commissies uit te betalen of hun ClubCash in te wisselen. Dus hebben wij onze internationale Pay Agent programma opgezet om alle Partners en Members de mogelijkheid te bieden om snel en economisch te ontvangen waar zij recht op hebben.</p>
<p>Internationale Pay Agents krijgen een lijst met de uitbetalingen en vervolgens storten we het totaalbedrag van de betalingen op de ClubAccount van de agent of we maken het geld over naar hun bankrekening. Agents krijgen tevens de bank gegevens zodat zij de individuele betalingen kunnen uitvoeren van hun bankrekening naar de rekening van de ontvangers. Partners en Members betalen lage transactiekosten om uit te worden betaald door een Pay Agent, welke voor de Pay Agent is die verantwoordelijk is voor het uitvoeren van de betalingen.</p>
<p>Wanneer u, als Executive Director, buiten de Verenigde Staten woont, moet u overwegen de Pay Agent verantwoordelijkheid op u te nemen voor de Members in uw organisatie die in uw land of regio wonen. Lees hier meer information over ons Pay Agent programma: <a href="http://www.clubshop.com/intl_pay_agent.html" target="_blank">LINK</a></p>
</div>
</div>
</div>
</div>
<p class="style28">UW ROL MET "CROSSLINE" PARTNERS</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><img src="crossover.gif" /> <br /><br />
<p>Als Executive Director, heeft u een toename in mogelijkheden om andere Executive Directors en Partners te ontmoeten en tijd met hen door te brengen die nieut in uw team zitten (aka: crossline). Het is nieut uw rol om raad te geven, te trainen of antwoord te geven op hun vragen, tenzij u handelt in een officiële hoedanigheid bij een bedrijf gesponsorde Webinar, Conferentie of Conventie. Wanneer benaderd, wijs hen dan beleefd door naar hun Coach of Executive Director</p>
<p>Wij moedigen u en alle Executive Directors aan om op te treden als een rolmodel en bemoediger voor alle Partners, of ze nu wel of niet in uw team zitten. Hierdoor zal eenheid en vertrouwen worden gebouwd voor alle partners en het erkent het principe van 'zaaien en oogsten'. Zaai positieve zaden van aanmoediging en vriendschap in andere Partners en andere Executive Directors zullen hetzelfde doen voor die van u. Alles wat we doen komt later en groter naar ons terug!</p>
</div>
</div>
</div>
</div>
<p class="style28">UW ROL MET UW UPLINE EXECUTIVE DIRECTORS</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Ondanks dat uw upline Executive Directors niet langer verantwoordelijk zijn voor enige rollen of verantwoordelijkheden voor uw team zodra u Executive Director bent geworden, hebben zij nog steeds een gevestigd belang in het succes van uw organisatie en relaties die zij ontwikkelden met Partners in uw team. Het zelfde geldt voor u zodra u Executive Directors heeft om deze rol aan te delegeren.</p>
<p>Als zodanig, zou u hun ervaring en kennis moeten respecteren en door met hen te blijven overleggen en hen te eren door ze uit te nodigen bij een aantal van uw online teambijeenkomsten en door hen op de hoogte te stellen van enige positieve ontwikkelingen binnen uw organisatie.</p>
</div>
</div>
</div>
</div>
<p class="style28">WEDSTRIJDEN EN AANMOEDIGINGEN</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p><img alt="contest" src="contest.jpg" /> Als de Executive Director, wilt u misschien wedstrijden houden en stimulansen creëren om groei te bevorderen of creëer een impuls voor uw team. Indien verstandig en goed uitgevoerd, kan dit erg leuk zijn en enthousiastme, opwinding, geconcentreerde inzet en positieve resultaten creëren!</p>
<p><span style="text-decoration: underline;">Voordat u een wedstrijd of stimulans opzet</span>, controleer eerst even bij het personeel van het hoofdkantoor om na te gaan wat voor soort wedstrijden of promoties wij in de planning hebben. Op deze manier kunt u een stimulans aanbieden die samenwerkt met de bedrijfswedstrijd.</p>
<p>Bijvoorbeeld, wanneer het bedrijf een Partner Sponsoring wedstrijd heeft, kunt u op hetzelfde moment een stimulans opzetten voor de beste Partner Sponsors in uw team.</p>
<p>Overweeg zorgvuldig de investering die u doet wanner u een wedstrijd organiseert. Vaak zijn mensen gemotiveerder om te werken voor een beloning dan voor geld, zoals:</p>
<ul>
<li class="blue_bullet">Erkenning - zowel publiek als prive.</li>
<li class="blue_bullet">Competitie en het gevoel van winnen.</li>
<li class="blue_bullet">Worden opgenomen met de leiders of een groep winnaars.</li>
</ul>
<p>In al deze gevallen, kan er een betere en meer betaalbaare prijs zijn dan geld of cadeaubonnen. Website berichten en erkenning op bijeenkomsten in combinatie met certificaten, plaquettes of trofeeën kunnen dienen als betere prijzen dan geld. Ontbijt, lunch of diner of opname in een groep met kaartjes die worden gekocht bij een evenement of thema park is ook een goede beloning. Wedstrijden zuden met tussenpozen gehouden moeten worden zodat mensen ze niet gaan verwachten of het gevoel hebben dat zij ze nodig hebben om hen te motiveren. Voordat u een wedstrijd opzet, overweeg dan het principe van "oorzaak en gevolg" om u eran te verzekeren dat de wedstrijd het soort resultaat brengt waar u naar op zoek bent om uw lange termijn doelen te bereiken en verzeker u er ook van dat mensen geen beloning kunnen krijgen voor valsspelen of het dien van persoonlijke aankopen.</p>
</div>
</div>
</div>
</div>
<p class="style28">TEAM COMMUNICATIES</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Als de Executive Director zult u diverse bijeenkomsten willen organiseren en hosten voor uw team. Deze zijn ontworpen om te inspireren, motiveren en eenheid in het team te creëren.</p>
<p><span class="ContactForm_TextField bold">Maandelijkse Online Team Bijeenkmsten</span> - U zou eens per maand een teambijeenkomst moeten houden voor uw team. Overweeg extra teambijeenkomsten te houden wanneer u Partners hebt die een andere taal spreken of in verschillende tijdzones leven.</p>
<p>Hou uw bijeenkomsten positief en interactief. Hier zijn een aantal onderwerpen die u kunt bespreken bij deze bijeenkomsten:</p>
<ul>
<li class="blue_bullet">Welkom nieuwe Partners</li>
<li class="blue_bullet">Erken recente prestaties of promoties</li>
<li class="blue_bullet">Deel zaken die zijn besproken in Bedrijfswebinars of Rally </li>
<li class="blue_bullet">bespreek nieuwe verbeteringen of wijzigingen van het Hoofdkantoor en gebruik de mogelijkheid het hoofdkantoor, GPS en de training op te bouwen</li>
<li class="blue_bullet">Nodig upline en groeiende crossline leiders uit om deel te nemen en deel hun "verhaal". Het interviewen van hen kan een effectieve vorm van motivatie zijn voor iedereen</li>
<li class="blue_bullet">Deel marketing tips met hen die u binnen uw team wilt houden.</li>
</ul>
<p><span class="ContactForm_TextField bold">Corporate Webinars and Business Opportunity Presentations</span> - Overweeg vrijwillig een online bedrijfsbijeenkomst te hosten. Stuur een e-mail naar <a href="mailto:partnersales@clubshop.com?Subject=Volunteer_for_Meeting">PartnerSales@ClubShop.com</a> om opgenomen te worden als een potentiële kandidaat voor bijeenkomsten. Geselecteerd worden om een zakelijke bijeenkomst te organiseren geeft u een uitstekende geloofwaardigheid als leider van uw team, wat uw mogelijkheden om uw Partners met meer succes te beïnvloeden, motiveren en coachen zal verhogen. Click on the Webinars tab at your Business Center to see a list and the links for all the webinars that are scheduled for the next 30 days.</p>
<p><span class="ContactForm_TextField bold">Offline bijeenkomsten</span> - Wanneer u een locale groep ontwikkelt, dan zou u moeten overwegen een offline bijeenkomst te houden in deze regio's. U heeft geen toestemming om entree kosten aan te rekenen voor vergaderingen, tenzij u hiervoor eerst eerst toestemming hebt gekregen van ClubShop Rewards. Wanneer uw bijeenkomst is goedgekeurd door het bedrijf zodat u een toegangsprijs kunt rekenen, kunt u vragen dat wij een aankondiging plaatsen voor de bijeenkomst en uitnodigingen versturen naar andere Partners in de regio om deel te nemen aan de bijeenkomst, wat kan helpen de kosten in verband met de vergadering te compenseren.</p>
<p>Na de door het bedrijf goedgekeurde bijeenkomst is afgelopen, moet u een Bijeenkomst Accounting Formulier invullen en opsturen. Een Bijeenkomst Balans sheet zal worden gehandhaafd op het hoofdkantoor om de kosten en toegansprijzen bij te houden voor al uw bijeenkomsten. Indien het geldbedrag dat u ophaalde hoger was dan de kosten van de bijeenkomst, dan zal het verschil worden ingehouden van uw verdiensten om misbruik of belangenconflicten te voorkomen. Wanneer u minder ophaalde dan de kosten van de bijeenkomst waren, dan laat de Balans sheet een negatief saldo zien en zullen toekomstige overschotten hiervan worden afgetrokken totdat het een positief saldo weergeeft, op dat moment zal het overschot worden ingehouden van uw verdiensten.</p>
<blockquote>
<p><span class="ContactForm_TextField bold">FORMULIEREN</span></p>
<ul>
<li class="half">Conferentie Hosting-instructie sheet [<a href="hosts.pdf" target="_blank">PDF file</a>]</li>
<li class="half">Conferentie Aanwezigheidsformulier [<a href="sign-in.pdf" target="_blank">PDF file</a>]</li>
<li class="half">Conferentie Accounting formulier [<a href="reconcile.pdf" target="_blank">PDF file</a>]</li>
</ul>
</blockquote>
</div>
</div>
</div>
</div>
<p class="style28">DOOR HET BEDRIJF GOEDGEKEURDE CONFERENTIES</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Het van tijd tot tijd organiseren van offline, regionale Conferenties, voor uw Partners, is van vitaal belang voor de gezondheid van uw zaak. In staat zijn om oog in oog te staan met upline Coaches en downline Partners en leiders te ontmoeten, geeft Partners de inspiratie die onmogelijk te verkrijgen is via online bijeenkomsten. Veel leiders hebben verklaard dat zij hun "commitment" kregen op een regionale conferentie.</p>
<p>Als een Executive Director, zou u er een prioriteit van moeten maken om de werkdruk te delen die ervoor nodig is om een enthousiast ondersteunde conferentie te hosten in uw regio. U zou niet alleen moeten proberen een conferentie bij te wonen wanneer deze in uw regio wordt gehouden, maar u zou tevens iedere Partner in uw organisatie moeten uitnodigen om deel te nemen aan de conferentie wanneer deze wordt gehouden in de regio waar zij wonen.</p>
<p><span class="ContactForm_TextField bold">HET HOSTEN VAN EEN CONFERENTIE</span></p>
<p>Executive Directors komen in aanmerking om een door het bedrijf goedgekeurde conferentie te organiseren en hosten, of op te treden als een co-host samen met andere Executive Directors, met goedkeuring van het bedrijf. Hosts kunnen tot maximaal twee andere Executive Directors vragen hen te helpen als co-host voor een Conference. Hosts en co-hosts mogen een hosting bijdrage vragen, afgetrokken van de Conference bijdragen, als tegemoetkoming om de persoonlijke kosten te vergoeden in verband met de reis, maaltijden en incidentele kosten die gerelateerd zijn aan het voorbereiden en hodsten van een conferentie. Een maximum van $500 (USD) kan betaald worden als individuele hosting kosten als er voldoende financiële middelen over zijn na het betalen voor de benodigde kosten (vergaderruimte, maaltijden, audio/video kosten, printkosten, kamer van spreker en bijdrage van de spreker) van een conferentie.</p>
<p>Bovendien, hebben conferentie hosts er recht op 50% van de conferentie opbrengsten te behouden nadat alle kosten zijn betaald, waarbij de overige 50% door de host wordt aan het bedrijf moet worden overgemaakt in combinatie met het vereiste Conferentie Accounting formulier. Als de entree opbrengsten niet toereikend genoeg zijn om te betalen voor de benodigde kosten, dan is de Host verantwoordelijk voor het betalen van deze kosten. In beide gevallen is het vereist om binnen 7 dagen na de conferentie het conferentie accounting formulier in te sturen.</p>
<p>Zodra een aankondiging werd gepubliceerd, dat een regionale conferentie zal worden gehouden in uw regio, kunt u zich vrijwillig aanmelden om te helpen de bijeenkomst te hosten door een e-mail te sturen naar <a href="mailto:partnersales@clubshop.com?Subject=Volunteer_for_Meeting">PartnerSales@ClubShop.com</a> - een personeelslid zal contact met u opnemen om u te laten weten hoe u kunt helpen.</p>
<blockquote>
<p><span class="ContactForm_TextField bold">FORMULIEREN</span></p>
<ul>
<li class="half">Conferentie Hosting Instructies sheet [<a href="hosts.pdf" target="_blank">PDF file</a>]</li>
<li class="half">Conferentie Aanwezigen formulier [<a href="sign-in.pdf" target="_blank">PDF file</a>]</li>
<li class="half">Conferentie Accounting formulier [<a href="reconcile.pdf" target="_blank">PDF file</a>]</li>
</ul>
</blockquote>
</div>
</div>
</div>
</div>
<p class="style28">SPREKEN OP EEN CONFERENTIE</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Het is een eer en een voorrecht om te worden uitgenodigd om te spreken op een regionale conferentie en uw ervaringen te delen als Partner, uw gedachten over het bedrijf en om motivatie en training te geven aan mede-Partners. Doorgaans worden alleen 1 Star of hoger Executive Directors of functionarissen of directors van het hoofdkantoor uitgenodigd om te spreken op een regionale conferentie, er kunnen echter uitzonderingen worden gemaakt door het bedrijf, afhankelijk van de beschikbaarheid van sprekers dicht in de buurt of in de regio.</p>
<p>Conferentie sprekers hebben recht op een Sprekers toelage om te helpen hun kosten te dekken in verband met hun reiskosten en maaltijden en als een honorarium voor het vrijmaken van hun tijd om te reizen en spreken op een conferentie. De hotelkamer van een spreker en reiskosten van en naar het hotel, moeten worden geregeld en betaald door de conferentie Host. Een $1,000 (USD) Sprekers vergoeding moet worden betaald aan de spreker die binnen hetzelfde continent reist en een $2,000 (USD) sprekers vergoeding moet worden betaald aan een spreker die vanuit een ander continent reist om te spreken op de conferentie, tenzij de spreker ervoor kiest om van het ene continent naar het andere te rijden, in dat geval moet een $750 (USD) sprekersvergoeding worden betaald.</p>
<p>In het geval waarin een gezamenlijke partner of echtgenoot ook wordt uitgenodigd om de spreker te begeleiden, is een extra $ 500 (USD) verschuldigd aan de Spreker voor intercontinentale reizen indien men gaat vliegen, of een extra $1,000 (USD) verschuldigd aan de Spreker, indien gevlogen zal worden van continent naar continent.</p>
<p>De vergoeding van de spreker moet altijd betaald worden, voordat de spreker de reis gaat maken en kan worden betaald van de voorverkoop van de verkochte entree kaarten.</p>
<blockquote>
<p><span class="ContactForm_TextField bold">FORM</span></p>
<ul>
<li class="half">Conferentie Sprekersinstructies sheet [<a href="speakers.pdf" target="_blank">PDF file</a>]</li>
</ul>
</blockquote>
</div>
</div>
</div>
</div>
<p class="style28">DOOR HET BEDRIJF GESPONSORDE CONFERENTIES EN CONVENTIES</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Van tijd tot tijd, kan ClubShop Rewards een nationale conferentie of continentale conventie hosten. Hierbij zal ClubShop Rewards een percentage van de ticket kosten als Pay punten toewijzen.</p>
<p>Van het ondersteunen en bijwonen van een conventie, indien gehouden in uw continent, zal u enorm profiteren en u zou erg hard moeten werken om al uw Coaches en zoveel mogelijk Partners als u kunt, deel te laten nemen. De teams die de meeste Partners aanwezig hebben op een Conventie, zijn de teams die het snelst en het best groeien!</p>
<p>Wanneer u een conventie bijwoont, doe dan wat de beste leiders doen - kom vroeg en blijf laat voor de conventie en elke andere bijeenkomst. Wees enthousiast, ontmoet zoveel mogelijk mensen als mogelijk en introduceer de Partners van uw team aan de aanwezige Executive Directors en personeelsleden van het Hoofdkantoor. Probeer met uw team samen te gaan zitten en maak aantekeningen tijdens de bijeenkomst.</p>
<p>Conventies zijn tevens een goed moment voor sociale ontmoetingen en om samen te eten met uw team en uw upline leiders. Controleer voor het plannen van team vergaderingen of uitstapjes, eerst even de conventie agenda.</p>
<p>Partner Conventies zijn niet alleen erg leuk en vriendschappelijk, maar ze bieden ook enorme hands on training door het personeel van het hoofdkantoor en successvolle Executive Directors. Vergeet niet om al uw bonnetjes te bewaren (en hou de kilometerstand bij wanneer u rijdt), omdat de kosten die verbonden zijn aan het bijwonen van een conferentie of een conventie, aftrekbaar zijn van de bealsting in de meeste landen die een inkomstenbelasting hebben.</p>
</div>
</div>
</div>
</div>
<p class="style28">FINANCIELE VRIJHEID BEREIKEN</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Naar de termen "residuaal inkomen" en "financiële vrijheid" is terloops verwezen en jarenlang beloofd door vele MLM programma's. De werkelijkheid is dat erg weinig deelnemers dit ooit hebben gezien. Van dit minderheid percentage dat in staat is geweest om een groot inkomen te realiseren in MLM, hebben de meesten het weer zien verdwijnen doordat het bedrijf gestopt is.</p>
<p>Alleen het creëren van een groot "residueel inkomen" geeft iemand nog geen echte financiële vrijheid. Vier andere factoren zijn vereist om echt financieel vrij te zijn.</p>
<ol>
<li><span class="bold">Het "residuale inkomen" moet naar u blijven toekomen zonder dat u er nog werk voor moet verrichten om het te ontvangen. </span><br /> <br /> Als u moet blijven werken, dan bent u niet "vrij". Dit is slechts een droom wanneer er geen legitiem plan is dat erkent dat het werk moet worden voortgezet om de niet-werkenden te voorzien van een residuaal inkomen. Dit geldt voor de sociale zekerheid van de overheid of pensioenregelingen, pensioenplannen van bedrijven of zelfs voor een persoonlijk pensioenplan, waarbij iets (geld, aandelen en obligaties, huur van woning, etc.) nodig is dat voor u werkt, zodat u niet zelf hoeft te werken, tenzij u ervoor KIEST om te werken. het hebben van deze "keus" is een factor die vereist is om echte financiële vrijheid te krijgen.</li>
<li><span class="bold">Een veilig residuaal inkomen vereist een bestendigde bron waaruit het wordt getrokken.</span> <br /> <br /> Net als vertrouwen op een bron voor water - wanneer de bron opdroogt, dan geldt dat ook voor uw vermogen om er water uit te putten. De bron van een residuaal inkomen moet intact blijven en moet een leven lang meegaan. Om dit te laten gebeuren, moet de bron voortdurend financieel gezond zijn, succesvol onderhouden worden en in staat zijn om toekomstige uitgaven te ondersteunen. <br /> <br /> Wij zien meer en meer bedrijfspensioenplannen afgestoten worden door de moedermaatschappij als resultaat van een slecht businessplan van het bedrijf om het fonds in stand te houden, of omdat het bedrijf slecht wordt geleid (inclusief het toestaan dat bedrijfsleiders het bedrijf verlaten met een "gouden handdruk"), of omdat het bedrijf schulden heeft gemaakt om mislukte investeringen te doen of om cash flow problemen op te lossen. <br /> <br /> Het is van vitaal belang voor een bedrijf om echte producten en diensten aan te bieden die in de toekomstige markt zullen blijven bestaan, een lange termijn zakenplan dat de verwachte druk kan opvangen dat impact heeft op hun compensation plan en leiderschap die zich inzet voor het bouwen van een sterke bedrijfserfenis en het bedrijf schuldenvrij houdt. <br /> <br /> Dit bedrijf heeft niet alleen bewezen dat het de tijd kan doorstaan, we hebben ook een lange-termijn business plan die rekening houdt met toekomstige concurrentie en verzadeging van de markt en zich schuldenvrij blijft inzetten. Ondanks dat er geen garanties zijn in het leven, is de ClubShop Rewards Partner Mogelijkheid zo gebouwd dat de erfenis zal doorgaan van generatie op generatie. </li>
<li><span class="bold">Een groot inkomen hoeft niet beslist financiële vrijheid te bieden.</span> <br /> <br /> Wanneer iemand net zoveel moet uitbetalen als er binnen komt, dan is er geen sprake van financiële vrijheid. Enige maniet om financieel vrij te worden is ervan te verzekeren dat uw toekomstige pensioen inkomen hoger zal zijn dan uw toekomstige kosten. Om dit te doen, moeten alle schulden en verplichtingen geleidelijk worden verminderd en vervolgens geëlimineerd. <br /> <br /> Terwijl uw ClubShop Rewards Partner inkomsten toeneemt, kunt u in de verleiding komen om geld te lenen om nieuwe verplichtingen aan te gaan omdat u zich de "afbetaling kunt veroorloven". Elke vreugde die dit aanvankelijk met zich meebrengt zal tijdelijk zijn, omdat de nieuwigheid snel zal slijten en u achterlaat met de verplichting door te moeten gaan met afbetalen. Dit is niet het pad naar financiële vrijheid. <br /> <br /> Echter, u zou in staat moeten zijn te genieten van de "vruchten van uw werk" en zo nu en dan moeten kunnen "stoppen om aan de rozen te ruiken" tijdens uw reis. Door dit te doen, ontwerp een financieel plan en maak het een persoonlijke verplichting om schulden die u heeft te verminderen en dan te elimineren, te beginnen met credit cards, dan consumentenschulden, dan leningen tegen dalende activa (auto's, boten, etc.). <br /> <br /> De enige schuld die u zou moeten hebben om finacieel vrij te zijn is een hypotheel voor onroerende goederen die dezelfde waarde zal houden of in waarde zal toenemen. Zelfs dan zouden uw betalingen hiervoor moeten worden gedaan met uw pensioen inkomen, terwijl er genoeg geld overblijft waar uw familie van kan rondkomen. Na verloop van tijd zou u gestaag moeten werken omuw hypotheek te verminderen en dan af te betalen, zodat u volledig schuldenvrij bent en geen verplichtingen meet heeft, behalve voor eten, nutsvoorzieningen en verzekeringen. <br /> <br /> Stel u een levensstijl voor waarin u volledig financieel vrij bent om te doen en laten wat u wilt, wanneer u maar wilt. Uit eten gaan wanneer u dat wilt of uw boodschappen doen zonder op de uitgaven te hoeven letten. Geld heeft niet langer de controle over u, maar u heeft de controle over het geld. Misschien wilt u nauw samenwerken met een aantal charitatieve instellingen en uw tijd en geld doneren aan waardevolle goede doelen. Of u wilt reizen in stijl, op een cruise gaan of gaan kamperen als u hiervan geniet. De keuze is aan u omdat u vrij bent! <br /> <br /> Misschien wilt u blijven werken omdat u uw baan leuk leuk vind, maar niet omdat het moet omdat u het geld nodig heeft. Misschien wilt u doorgaan met het persoonlijk werven van Members en Partners om uw inkomten te verhogen zodat u een nieuw huis, auto, boot of vakantiehuis kunt aanschaffen. U kunt uw extra inkomsten ook gebruiken om te investeren in uw toekomst of de toekomst van uw familie. Of u kunt investeringen doen met uw extra geld. <br /> <br /> Dromen worden werkelijkheid wanneer we het juiste voertuig hebben en wanneer we hard willen werken en in onszelf willen investerenom voor lange-term resultaten. Het komt erop neer dat u echt financieel vrij kunt worden door het uitbouwen van een ClubShop Rewards zaak en schuldenvrij kunt worden. </li>
<li><span class="bold">Een laatste overweging... mensen kunnen niet financieel vrij zijn, wanneer zij geen persoonlijke vrijheid hebben.</span> <br /> <br /> Wanneer een persoon toegang heeft tot het internet en in een land woont dat diens inwoners economische-, politieke- en religieuze vrijheid geeft, dan kan deze persoon financieel vrij worden met een ClubShop Rewards zaak, zelfs wanneer hun locale economie arm is met beperkte mogelijkheden. <br /> <br /> Helaas wonen niet alle Partners die in uw team komen in een "vrij" land of hebben zij gemakkelijke, betaalbare of betrouwbare toegang tot het internet. Toch kunnen zij hoop hebben voor een betere toekomst en financiële vrijheid door ClubShop Rewards en door u. <br /> <br /> terwijl u aan uw zaak werkt, krijgt u Partners van overal ter wereld. Sommigen wonen in armere landen en sommigen in landen met onstabiele of tirannieke regeringen. Om succesvol te zijn in uw zaak, moet u anderen helpen om ook succesvol te worden. U kunt geen succes hebben, tenzij u anderen helpt om succesvol te zijn. Door dit te doen, doet u misschien meer dan u zich realiseert, om de wereld te veranderen en het tot een betere plaats te maken. <br /> <br /> Economische kracht is vereist voor het hebben van politieke kracht en politieke vrijheid gaat vooraf aan religieuze vrijheid. Om voor mensen in arme landen al deze vrijheden te verkrijgen, moeten zij eerst economische sterk worden. Voor de meesten kan dit niet worden gerealiseerd, maar door het bouwen van uw ClubShop Rewards zaak, heeft u de mogelijkheid om mensen van over de hele wereld te helpen om economische kracht te verkrijgen, zodat zij de gemachtigd worden om invloed te hebben op het verkrijgen van politieke kracht en vrijheid. </li>
</ol></div>
</div>
</div>
</div>
<p class="style28">DE ENIGE MANIER OM EEN SUCCESVOLLE VERKOOPORGANISATIE TE CREEREN</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p><img alt="of eggs and baskets" src="eggsbasket.jpg" /> Omdat zoveel mensen betrokken zijn geweest bij falende mogelijkheden, Er is een tendens om de eigen belangen te diversifiëren en betrokken te zijn bij meerdere "programma's". Dus de filosofie van "niet al je eieren in 1 mand leggen" heerst.</p>
<p>Ondanks dat deze filosofie in theorie iemand kan beschermen van een uiteindelijke mislukking, is de realiteit dat het iemand ook tegenhoudt om het soort commitment te doen dat nodig is om succes te hebben in een ClubShop Rewards zaak. Zelfs wanneer een persoons diens betrokkenheid in een ander "programma" verbergt, kunnen zij hun gebrek aan loyaliteit, commitment en oprechte enthousiasme voor hun ClubShop Rewards zaak niet verbergen voor hun team.</p>
<p>Wanneer u een Partner vraagt of deze betrokken, loyale en trouwe Partners in hun organisatie willen, zeggen zij "Natuurlijk". Maar door het niet eenduidig zijn in hun focus en betrokkenheid, voldoen zij niet aan het duplicatie principe. Laten we de leiderschapsvraag toepassen op dit onderwerp: <span class="bold">"Wat indien iedereen in mijn organisatie dit zou doen? Zou dat dan goed zijn?"</span> Het antwoord is duidelijk, nietwaar?</p>
<div align="center">
<p class="style28">FOLLOW THE YELLOW BRICK ROAD</p>
</div>
</div>
</div>
</div>
</div>
<hr />
<p><a href="http://www.clubshop.com/manual/"><img alt="Manual Home Page" height="64" src="http://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>