<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>


     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>

	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<table>
<tr class="a"><td><img src="http://www.clubshop.com/vip/execs/hometeam.jpg"/></td>

<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr>
</table>
<ol>
<li><xsl:value-of select="//lang_blocks/p2" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p3" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p4" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p5" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p6" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p60" /></li><br/>
</ol>

<table  border="1" cellpadding="3">

<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/p7" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p15" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p23" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p31" /></b></td>
</tr>

<tr class="bcolor"><!--accounting-->
<td><b><xsl:value-of select="//lang_blocks/p8" /></b></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td>Cindy Hennessy<br/>acfspvr@dhs-club.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>

</tr>

<tr class="acolor"><!--membership support-->
<td><b><xsl:value-of select="//lang_blocks/p9" /></b></td>
<td>Sales, Membership &amp; Consumer Support</td>
<td>Lisa Young<br/>support1@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>


<tr class="bcolor"><!--membership support fr-->
<td><b><xsl:value-of select="//lang_blocks/p21" /></b></td>
<td><xsl:value-of select="//lang_blocks/p21b" /></td>
<td>Enrica Alesiani<br/></td>
<td>Lisa Young<br/>l.young@dhs-club.com</td>
</tr>



<tr class="bcolor"><!--membership support fr-->
<td><b><xsl:value-of select="//lang_blocks/p24" /></b></td>
<td><xsl:value-of select="//lang_blocks/p26" /></td>
<td>Sabine Lefrere<br/></td>
<td>Lisa Young<br/>l.young@dhs-club.com</td>
</tr>

<tr class="bcolor"><!--membership support nl-->
<td><b><xsl:value-of select="//lang_blocks/p25" /></b></td>
<td><xsl:value-of select="//lang_blocks/p27" /></td>
<td>Betty Slouwerhof<br/></td>
<td>Lisa Young<br/>l.young@dhs-club.com</td>
</tr>

<tr class="bcolor"><!--membership support nl-->
<td><b><xsl:value-of select="//lang_blocks/p30" /></b></td>
<td><xsl:value-of select="//lang_blocks/p31a" /></td>
<td>Belinda Riquelme Rodriguez<br/></td>
<td>Lisa Young<br/>l.young@dhs-club.com</td>
</tr>



<tr class="acolor"><!--GI and EBiz-->
<td><b><xsl:value-of select="//lang_blocks/ebiz" /></b></td>
<td><xsl:value-of select="//lang_blocks/p29" /></td>
<td>Debra Corsetti<br/>partnersales@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>




<tr class="bcolor"><!--IT-->
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td>Bill MacArthur<br/>webmaster@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>

</tr>

<tr class="acolor"><!--IT-->
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/p28a" /></td>
<td>Bill MacArthur<br/>webmaster@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>

<!--<tr class="bcolor">
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/p100" /></td>
<td>George Baker<br/>george.baker@dhs-club.com</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>

</tr>-->

<tr class="bcolor"><!--Offline Consumer Development-->

<td><b><xsl:value-of select="//lang_blocks/offline" /></b></td>
<td><xsl:value-of select="//lang_blocks/advertising"/></td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>


<tr class="acolor"><!--ClubShop Rewards Development-->

<td><b><xsl:value-of select="//lang_blocks/crw" /></b></td>
<td><xsl:value-of select="//lang_blocks/p21a" /></td>
<td>Carina Burke<br/>mallspecials@clubshop.com</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
</tr>

<tr class="bcolor"><!--Online Consumer Development-->

<td><b><xsl:value-of select="//lang_blocks/p12" /></b></td>
<td><xsl:value-of select="//lang_blocks/p20" /></td>
<td>Carina Burke<br/>sales@clubshop.com</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
</tr>


<!--<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/website"/></b></td>
<td><xsl:value-of select="//lang_blocks/design"/></td>
<td>Luis Montanez<br/>l.montanez@clubshop.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>-->

<tr class="acolor"><!--Website Design Maintenance-->
<td><b><xsl:value-of select="//lang_blocks/p14" /></b></td>
<td><xsl:value-of select="//lang_blocks/maintenance" /></td>
<td>Lisa Young<br/>l.young@dhs-club.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>
</tr>
</table>



<br/>

<hr/>
     
      
<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>

