<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>

            <title>ClubShop Rewards Partner Manual</title>





<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <script src="http://www.clubshop.com/js/panel.js"></script>



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">REFERRAL MARKETING GUIDE</span> 
<hr />
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<!--
<p>Gefeliciteerd met uw beslissing om van start te gaan als ClubShop Rewards Partner!</p>
<p>Veel van onze Partners zijn van start gegaan in de hoop elke maand wat extra geld te verdienen. Vervolgens hebben zij, door gebruik te maken van ons bewezen systeem, een fulltime inkomen ontwikkeld. Als ClubShop Rewards Partner, kunt u in uw eigen tempo werken en gaandeweg uw inkomen laten groeien.</p>
<p><span class="style24">Uw eerste 30 dagen</span></p>
<p>Wij weten dat u waarschijnlijk veel vragen zult hebben wanneer u van start gaat en dat u wellicht een aantal ideeën hebt die u wilt testen om te zien of zij werken. Voor de beste resultaten, adviseren wij dat u uw eerste dertig dagen gebruikt om het systeem eigen te maken door gebruik te maken van de TrainingGids lessen om te leren hoe u een solide glocal zaak creëert en een grote, wereldwijde verkooporganisatie. Als u dit doen creëert u het juiste fundament om uw bedrijf op te bouwen.</p>
<p>U heeft twee mensen (uw "Team Coach" en uw "Executive Director") om u te helpen alle vragen te beantwoorden die u heeft en om u te begeleiden bij het leren hoe u ten eerste members aansluit/ verwijst en vervolgens hoe u Partners, Affiliates, Non-profit organisaties en Winkels/ bedrijven sponsort. U zou van hen een welkom e-mail moeten ontvangen en een uitnodiging om met hen of uw nieuwe team af te spreken, om u te helpen van start te gaan. Wij raden u aan gebruik te maken van dit zeer belangrijke coaching aspect van ons Partner Inkomsten Mogelijkheid door hen te leren kennen en een goede zakenrelatie met hen op te bouwen.</p>
<p>Aarzel niet om contact met hen op te nemen wanneer u het nodig heeft. KLIK HIER of overal waar u de "HELP" link ziet staan op uw Business Center om toegang te krijgen tot hun contactinformatie.</p>

<div style="background-image: url(http://www.clubshop.com/manual/images/ybr.png); background-repeat: no-repeat; width: 275px; height:41px; align:right;">
<div style="text-align:center; position:relative; top:17px; height:auto; width:auto; right:auto; font-weight:900; font-size:22px; font-family:georgia,serif;"><span class="text_normal bold">VOLG DE YELLOW BRICK ROAD</span></div>
</div>

<p><span class="style24">VOLG DE YELLOW BRICK ROAD</span></p>
<p>Via uw Training Gids kunt u alles leren wat u nodig heeft, WANNEER u het nodig heeft om te leren. Het is uw wegenkaart om u te helpen de "yellow brick road te volgen", wat de meest rechtstreekse route is om een Executive Director te worden en een fulltime inkomen te creëren.</p>
<p>Wij moedigen u aan om niet opnieuw "het wiel uit te vinden", maar om uw reis stap-voor-stap voort te zetten, van de ene Training Gids Les naar de volgende en iedere geadviseerde opdracht te volooien terwijl u verder gaat. Vraag hulp bij uw Team Coaches wanneer u dit nodig hebt.</p>
<p>Het kost de meeste ondernemers vijf jaar om hun eerste investeringen er uit te halen en te beginnen met winst te maken. Door het volgen van onze training en het gebruik maken van ons bewezen methoden, kunt u uw eerste investering terugverdienen EN beginnen met het verdienen van een maandelijkse winst, in uw eerste maand.</p>

-->


<p><span class="style24">HOE TE BEGINNEN MET GELD VERDIENEN</span></p>
<p>Het allereerste waar u aan denkt is waarschijnlijk, "Hoe ga ik geld verdienen?". Om deze vraag te beantwoorden, moet u eerst begrijpen hoe u geld kunt verdienen door het verwijzen van mensen om ClubShop Reward Members te worden. Zo werkt het:</p>
<ol>
<li>Wanneer u anderen verwijst om gratis ClubShop Reward Members te worden, kunnen zij "ClubCash" (geld terug vergoeding) verdienen wanneer zij online winkelen bij de winkels in onze <a href="https://www.clubshop.com/mall/" target="_blank">ClubShop Rewards Winkelcentrum</a>. Zij komen in aanmerking om een aantal mooie prijzen te verdienen door Member te zijn en door gebruik te maken van hun lidmaatschap wanneer zij winkelen.<br /> <br />Members die een ClubShop Rewards kaart hebben, kunnen tevens wereldwijd kortingen krijgen of ClubCash verdienen wanneer zij uit eten gaan of gaan winkelen bij deelnemende winkels/ bedrijven.</li>
<li>Telkens wanneer een Member die u hebt verwezen, ClubCash verdient, kunt u Referral Rewards en Referral Commissies verdienen op hun aankopen. De Members die u verwijst zijn uw potentiële "klanten", behalve dan dat u hen niets hoeft te verkopen. Zij doen hun eigen aankopen, terwijl u geld verdient door hen te helpen geld te besparen.</li>
<li>Sommige van de Members die u verwijst, zullen ook hun lidmaatschap willen gebruiken om anderen te verwijzen zodat zij bijpassende prijzen kunnen winnen of Referral Rewards krijgen. Wanneer de Members die zij verwijzen, een aankoop doen, dan verdient u ook Referral Commissies op hun aankopen.</li>
<li>Sommige van deze Members die u verwijst zullen ook Partner worden door het aankopen van een GPS abonnement, waarover u ook weer Referral Commissies verdient. </li>
</ol>
<p>In de komende lessen, zult u meer leren over hoe u inkomsten verdient gebaseerd op "Pay punten", het voordeel van het hebben van wereldwijde Partners die worden toegevoegd aan uw Partner verkooporganisatie, hoe u winkels/ bedrijven en non-profit organisaties werft en over het verkopen van ClubShop Rewards kaarten - kortom alle manieren om verschillende soorten extra inkomsten te creëren uit uw ClubShop Rewards zaak. (Indien u op dit moment meer informatie wenst over het verdienen van een inkomen als Partner, dan leest u in ons <a href="http://www.clubshop.com/manual/compensation/partner.html" target="_blank">Partner Compensatieplan</a> de volledige en gedetailleerde informatie over hoe u dit doet.)</p>
<p>Wij raden u aan om NIET te proberen, mensen die u kent, te sponsoren als Partner. Zodra u een aantal gratis members heeft verwezen en uw zaak begint te vestigen, dan bent u in een betere positie om met anderen te praten over hoe ons Partner Inkomsten Mogelijkheid werkt.</p>
<p>Om een snelle start te maken, raden wij u aan om eerst te leren hoe u de mensen die u kent te verwijzen om ClubShop Rewards Members te worden. Waarom? Omdat het lidmaatschap gratis is en iedereen graag korting krijgt tijdens het winkelen en graag prijzen wint!</p>
<p>Laat ons systeem u vervolgens helpen ontdekken of er door u verwezen Members zijn, die interesse hebben om hun lidmaatschap te gebruiken om ook geld te verdienen. Hier leest u hoe:</p>
<ol>
<li>U stuurt een e-mail (zie hieronder) aan de mensen die in uw e-mail adresboek staan, om hen uit te nodigen om gratis ClubShop Rewards Members te worden zodat zij geld kunnen besparen en in aanmerking komen om geweldige prijzen te winnen.</li>
<li>Wanneer zij op de link in de e-mail klikken (uw IDnummer is verwerkt in de link zodat u automatisch het referral credit krijgt) komen zij op een eenvoudig inschrijfformulier waar zij zich kunnen registreren als Member. Zodra het formulier is ingestuurd, zien zij een bevestiging webpagina en ontvangen zij een bevestiging e-mail met hun ID nummer en een link naar <a href="http://www.clubshop.com/" target="_blank">www.clubshop.com</a> zodat zij kunnen beginnen met het verdienen van ClubCash. <br /> <br /> Op de bevestiging webpagina en in de e-mail, krijgen zij de mogelijkheid aangeboden om een gratis, Proefpartner positie te nemen wanneer ze ook interesse hebben in het onderzoeken hoe zij hun lidmaatschap kunnen gebruiken om geld te verdienen. Sommige van de Members die u verwijst, zullen deze mogelijkheid selecteren en beslissen daarna om Partner te worden. </li>
</ol>
<p>Hoe eerder u van start gaat met het verwijzen van mensen om ClubShop Rewards Members te worden, des te eerder u kunt beginnen met het verdienen van een inkomen als Partner!</p>
<!--
<p><span class="style24">MINIMUM COMMISSIE GARANTIE</span></p>
<p>Om u te helpen om snel te gaan verdienen en winst te maken met uw zaak, geeft ClubShop Rewards de "Minimum Commissie Garantie":</p>
<p>"Een Partner met twee persoonlijk verwezen Partners (aan het einde van de maand) verdient gegarandeerd ten minste het inkomen dat nodig is om de GIBBS maandelijkse  abonnementbijdrage voor de volgende van hun verdiensten af te laten trekken."</p>
<p>Dit betekent dat indien u tenminste twee persoonlijk verwezen Partners hebt aan het eind van deze maand of in de toekomst in andere maanden, u uw maandelijkse GIBBS abonnement niet hoeft te betalen voor de volgende maand, omdat deze zal worden afgetrokken van uw verdiensten. Breng er twee bij en u bent betalingvrij!</p>
<p>Hoe eerder u start met het verwijzen van mensen om ClubShop Rewards Members te worden, hoe eerder u er twee zult hebben als uw persoonlijk verwezen Partners!</p>
-->

<p><span class="style24">HOE U START MENSEN TE VERWIJZEN OM MEMBERS TE WORDEN</span></p>
<p>Het eerste dat u wilt doen is "het woord verspreiden" aan de mensen die u kent, dat u een manier heeft ontdekt om hen te helpen geld te besparen en prijzen te winnen, eenvoudig door gratis ClubShop Rewards Member te worden. Er zijn een aantal manieren om dit te doen, hierover zult u meer leren in de MEMBERS VERWIJZEN Les. Om echter snel van start te gaan, adviseren wij dat u onderstaande e-mail verstuurd aan de mensen waarvan u het e-mail adres in uw e-mail adresboek heb staan. Om echter snel van start te gaan, raden wij aan dat u de volgende twee dingen doet:</p>
<ol>
<li>Plaats het volgende bericht op uw persoonlijke Facebook Wall en/ of op alle andere Sociale of Zakelijke netwerk pagina's die u heeft. <br /><br />
<blockquote><span class="style1 medblue">Ik ben zojuist GRATIS lid geworden van een geweldig Rewards Programma waarmee ik geld bespaar wanneer ik online of offline winkel - regionaal en wereldwijd. Ik kan tevens geld verdienen wanneer mensen die ik verwezen heb, geld besparen! Help me alstjeblieft door je gratis aan te melden via deze link en dan kan ik geld verdienen terwijl jij geld kan besparen of ik kan je helpen om ook geld te verdienen! http://www.clubshop.com/cgi-bin/rd/4,,refid=123456 </span></blockquote>
<br /> Verander de 123456 in UW ID nummer. Wanneer u onzeker bent hoe dit te doen, neem dan contact op met uw <a href="http://www.clubshop.com/cgi/mytcex.cgi" target="_blank">Team Coach</a> voor hulp. </li>
<li>Stuur een e-mail naar de mensen die in uw e-mail adres boek staan, nodig hen uit om een gratis lidmaatschap te nemen om geld mee te besparen wanneer zij online of offline winkelen. Zo doet u dat:</li>
</ol></div>
<br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="ContactForm_TextField">STAP ÉÉN</span> - Bekijk onderstaande emails en kies de e-mail die u wilt versturen, kopieer deze e-mail en plak 'm in een lege e-mail:<br />
<blockquote>
<p><span class="style24">Professionele stijl e-mail</span></p>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><br />
<p>Onderwerp: Aan <span class="style4">[[VOORNAAM]]</span> van <span class="style4">[[UW NAAM]]</span></p>
<p>Hallo <span class="style4">[[VOORNAAM]]</span>,</p>
<p>Ik heb kort geleden een geweldige manier ontdekt om online te winkelen en GELD TERUG te krijgen op al mijn aankopen die ik doe wanneer ik gebruik maak van mijn ClubShop Rewards lidmaatschap bij een winkel die op de lijst staat in het ClubShop Rewards Winkelcentrum.</p>
<p>Dan nu het goede nieuws - het is GRATIS om ClubShop Rewards Member te worden! Echt waar - er zijn geen kosten aan verbonden om lid te worden, maar ik kan geld terug verdienen wanneer ik winkel bij een aantal van de populairste online winkels. Dat is niet alles, ClubShop Rewards biedt hun leden tevens de kans om ook nog eens een aantal fantastische prijzen te winnen - gewoon alleen door lid (member) te zijn!</p>
<p>Als lid, heb ik ook de mogelijkheid gekregen om niet alleen geld te besparen, maar ook geld te verdienen met een wereldwijde zaak als ClubShop Rewards Partner. Als Partner kan ik offline bedrijven en winkels verwijzen om deel te nemen aan het ClubShop Rewards Winkel Programma, wat ik van plan ben binnenkort te gaan doen.</p>
<p>Wanneer ik dan locale winkels en bedrijven inschrijf, kan je ook kortingen krijgen of geld terug verdienen wanneer je uit eten gaat of bij de locale winkels gaat winkelen, eenvoudig door de ClubShop Rewards kaart te laten zien aan de deelnemende Winkelier. Maar later meer daarover....</p>
<p>Ik weet dat in de huidige moeilijke economische tijden, wij allemaal op zoek zijn naar manieren om geld te besparen, dit is wat ik als ClubShop Rewards Member doe - en het kan jou ook helpen, eenvoudig door te registreren als gratis lid/ member. Er zijn geen kosten, risico's of verplichtingen aan verbonden. Gewoon gratis meedoen en start met geld terug te ontvangen en misschien zelfs een prijs winnen, alleen maar door lid te zijn!</p>
<p>Ik ben erg enthousiast over het starten van mijn nieuwe zaak en wil je graag helpen om op deze nieuwe manier geld te besparen wanneer je online en offline gaat winkelen!</p>
<p>KLIK HIER om te registreren en om je lidmaatschap ID nummer te ontvangen die je moet gebruiken wanneer je gaat winkelen.</p>
<p>BEDANKT voor je registratie om Lid/ Member te worden en de credit mag ontvangen omdat ik je mag verwijzen. Ik waardeer je steun enorm!</p>
<p>Met vriendelijke groeten,<br /> <span class="style4">UWVOORNAAM</span></p>
</div>
</div>
</div>
</div>
<p><span class="style24">Casual Style Email</span></p>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Onderwerp: <span class="style4">[[VOORNAAM]]</span> - Kunt u mij een plezier doen?</p>
<p>Hallo <span class="style4">[[VOORNAAM]]</span>,</p>
<p>Ik ben zojuist gratis lid geworden bij deze geweldige website die geld terug geeft door te winkelen bij een aantal van de grootste online winkels ter wereld. Leden kunnen ook winkelen bij locale winkels/ ondernemers en dan directe kortingen krijgen. Het helpt u echt om te besparen wanneer u online en in de regio gaat winkelen.</p>
<p>Ik zou het erg leuk vinden als u ook gratis lid zou worden door gebruik te maken van onderstaande link. Als u lid wordt en en besluit online via hen te gaan winkelen, dan krijgt u geld terug op uw aankopen (en kunt u zelfs een prijs winnen) en ik verdien een referral commissie. U heeft echter geen enkele verplichting om iets te kopen.</p>
<p>Als u later als Member besluit om 1 van hun goedkope reward kaarten te kopen, dan kunt u geld besparen door in de regio directe kortingen te krijgen en verdien ik ook een commissie op uw kaartaankopen. Ze hebben ook nog eens een aantal geweldige wedstrijden en prijzen waar we beiden kunnen winnen!</p>
<p>Klik hier voor uw gratis lidmaatschap en besparingen: http://www.clubshop.com/cgi-bin/rd/4,,refid=<span class="style3">123456</span></p>
<p>Ik waardeer uw steun en ben erg enthousiast dat we nu beide geld kunnen besparen wanneer we online en/ of lokaal winkelen.</p>
<p>Dank u,<br /> <span class="style3">UW NAAM</span></p>
</div>
</div>
</div>
</div>
<p><span class="style24">BINNENKORT e-mail</span> (voor gebruik in landen waar momenteel weinig winkels in het ClubShop Rewards winkelcentrum staan)</p>
<div class="toggle" style="display:none;">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Subject: To <span class="style4">[[VOORNAAM]]</span> van <span class="style4">[[UW NAAM]]</span></p>
<p>Hi <span class="style4">[[VOORNAAM]]</span>,</p>
<p>Ik heb kortgeleden een fantastische manier ontdekt dat dit jaar beschikbaar zal zijn voor ons om online te winkelen en GELD TERUG te krijgen op al onze aankopen wanneer we winkelen terwijl we gebruik maken van een ClubShop Rewards lidmaatschap bij de winkels die zijn opgenomen in het ClubShop Rewards winkelcentrum.</p>
<p>Hier is het goede nieuws - een ClubShop Rewards Member worden is GRATIS! Inderdaad - er zijn geen kosten aan verbonden om lid te worden, maar wij kunnen geld terug verdienen wanneer we winkelen bij verschillende populaire online winkels die in <span class="style4">[[LAND NAAM]]</span> ClubShop Rewards winkelcentrum. Dat niet alleen, ClubShop Rewards biedt hun Members tevens de kans om een aantal geweldige prijzen te winnen - alleen al door lid te zijn!</p>
<p>Als een Member, heb ik tevens de mogelijkheid gekregen om niet alleen geld te besparen, maar ook geld te verdienen, met een global zaak als een ClubShop Rewards Partner. Als een Partner, kan ik ook offline winkels en bedrijven verwijzen om deel te nemen aan het ClubShop Rewards Winkel Programma, waar ik van plan ben binnenkort mee te starten.</p>
<p>Wanneer ik locale winkels aansluit, kan je ook kortingen krijgen of geld terug verdienen wanneer je uit eten gaat of locaal gaat winkelen, eenvoudig door een ClubShop Rewards kaart te laten zien aan de deelnemende winkel/ ondernemer. Maar later meer hierover....</p>
<p>Ik weet dat in de huidige economisch moeilijke tijden, wij allemaal op zoek zijn naar manieren om geld te besparen, wat precies het zijn van een ClubShop Rewards Member me helpt te doen - en het kan jou ook helpen, door eenvoudig te registreren als een gratis Member. Er zijn kosten, risico's of verplichtingen aan verbonden om dit te doen. Gewoon gratis inschrijven en ik laat het je weten wanneer er nieuwe winkels bijkomen waar je korting kan krijgen en misschien zelfs een prijs kan winnen, alleen maar door Member te worden!</p>
<p>Ik ben enthousiast over het starten van mijn nieuwe zaak en jou te helpen met een nieuwe manier om geld te besparen wanneer je online en offline gaat winkelen!</p>
<p><span class="style4">KLIK HIER</span> om te registreren en om je lidmaatschap ID nummer te gebruiken wanneer je gaat winkelen. http://www.clubshop.com/cgi-bin/rd/4,,refid=<span class="style3">123456</span></p>
<p>BEDANKT voor het registreren als Member en mij de credit te gunnen voor jou verwijzing. Ik stel je steun zeer op prijs!</p>
<p>Bedankt,<br /> <span class="style3">UWVOORNAAM</span></p>
</div>
</div>
</div>
</div>
</blockquote>
<br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><span class="ContactForm_TextField">STAP TWEE</span> - Verander in de tekst waar uw naam moet staan.  <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="ContactForm_TextField">STAP DRIE</span> - Bewaar deze e-mail als een "Template(voorbeeld)", zodat u deze naar alle mensen kunt sturen die in uw adresboek staan of waarvan u de e-mailadressen heeft. (<span class="darkblue">Indien u hierbij hulp nodig heeft, neemt dan contact op met uw <a href="http://www.clubshop.com/cgi/mytcex.cgi" target="_blank">Team Coach</a></span>) <br /> <br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="ContactForm_TextField">STAP VIER</span> - Open uw e-mail en start de voorbereidingen om deze naar alle e-mail adressen te sturen die u hebt, één voor één. Verander voor het verzenden de twee <span class="style4">[VOORNAAM]</span> vermeldingen om de ontvanger aan te spreken met eigen naam.  <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="ContactForm_TextField">STAP VIJF</span> - Verstuur uw e-mails één voor één totdat u een e-mail heeft verstuurd aan iedere persoon die u kent en waarvan u een e-mail adres heeft. <br /><br />
<p><strong>BELANGRIJKE OPMERKING:</strong> ClubShop Rewards hanteert een erg strikt, <a href="http://www.clubshop.com/manual/policies/spam.xml" target="_blank">anti-spam beleid</a> welke elke Member verbiedt om e-mails uit te sturen die een verwijzing bevatten naar ClubShop Rewards, aan een persoon die zij niet persoonlijk kennen of waar zij geen toestemming van hebben ontvangen om hen te mailen. Verstuur bovenstaande e-mail ALLEEN naar mensen die u kent.</p>
<p><span class="style24">OPVOLGEN ALS NODIG</span></p>
<p>Vergelijk de nieuwe Members die hebben ingeschreven via uw e-mail en berichten, met diegenen waar u contact mee hebt opgenomen. Indien u na 72 uur nog geen reactie of inschrijving hebt ontvangen, neem dan opnieuw contact op om na te gaan of zij uw e-mail of bericht hebben ontvangen. Herinner hen eraan dat inschrijven gratis is en dat zij elk moment weer kunnen uitschrijven.</p>
<p><span class="style24">Wat gaat er vervolgens gebeuren?</span></p>
<p>Zodra de mensen die u de e-mails stuurde, zich beginnen aan te melden als Members, ontvangt u voor iedere aanmelding van hen een e-mail ter bevestiging en wanneer u een GIBBS abonnee bent, ziet u hen op de lijst staan op uw <a href="http://www.glocalincome.com/cgi/activity_rpt" target="_blank">Activiteiten Rapport</a> als "Nieuwe Member".</p>
<p>Stuur elke nieuwe Member die u hebt verwezen een welkom e-mail om uw hulp aan te bieden wanneer zij vragen hebben en om hen aan te moedigen om te beginnen ClubCash te verdienen door alleen even de korte vragenlijst in te vullen en nog eens door een eerste aankoop te doen via het ClubShop Rewards Mall winkelcentrum.</p>
<p>Een voorbeeld van een welkom e-mail is beschikbaar in de volgende les (<span class="medblue bold">VOORBEREIDING</span>) samen met andere waardevolle informatie om uw zaak op te starten en te organiseren, zodat u vol vertrouwen verder kunt gaan om een wereldwijde zaak en een wereldwijde verkooporganisatie uit te bouwen.</p>
<p>Neem contact op met uw Coach zodat deze, samen met u, uw activiteiten rapport en de Member Follow Up Records kan doornemen, zodat uw coach u kan helpen met het opvolgen van de nieuwe Proefpartners die u kunt hebben. Laat hen afspreken met uw eerst Proefpartners, terwijl u observeert hoe uw coach uitleg geeft van hun Proefpositie en hoe ons Partner inkomsten mogelijkheid werkt. U bent nu op weg!</p>
<hr />
<p align="center"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <a href="http://www.clubshop.com/vip/manual/training/guide/getsetup/index.html">Volgende Les</a></p>
<br />
<p><a href="http://www.clubshop.com/manual/"><img alt="Handleiding Homepagina" height="64" src="http://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>