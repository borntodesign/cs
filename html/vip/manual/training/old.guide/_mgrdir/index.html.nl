<!DOCTYPE HTML>

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>
<script type="text/javascript" src="/cgi/PSA/script/10"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>
<title>ClubShop Rewards Partner Manual</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/foundation.css" />
<!--  <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" /> -->
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/reveal.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="http://www.clubshop.com/js/foundation/foundation.js"></script>
<!--  <script src="http://www.clubshop.com/js/foundation/app.js"></script> -->
<script src="http://www.clubshop.com/js/foundation/flash.js"></script>
<script src="http://www.clubshop.com/js/foundation/jquery.reveal.js"></script>
<!--<script src="http://www.clubshop.com/js/panel.js"></script>-->
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<style>

div.b{
/*	display: none; */
	font-size: 75%;
	background-color: #DFEAFF;
	padding: 3px 6px;
	border: 1px solid silver;
	margin-top: 2px;
	width: 50%;
}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">Partner Manager &amp; Director Les</span> 
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
<p class="style24">Inzicht in uw rol</p>
<p>Als Manager of Director, zult tu opreden als de Coach voor de Team Captains          in uw team, wanneer u de eerste Manager+ upline bent van een Team Captain.          U zult misschien ook optreden als Coach voor de Managers of Directors          in uw team, wanneer u de eerste Partner bent met een hogere Pay Titel          dan die zij hebben.</p>
<p>Als gevolg hiervan, zult u optreden als een Coach voor andere Coaches, naast dat u verder gaat met het uitvoeren van de Team Captain Coach rol voor de Partners in uw Team die nog geen andere Team Captain+ dan u in hun upline hebben.</p>
<p>Dit zal vereisen dat u <span class="bold"> uw communicatie- en managementvaardigheden moet eigen maken, toepassen en verbeteren</span> om u te helpen in de toekomst een succesvolle Executive Director te zijn. Er is een pakket aan vaardigheden voor nodig om een geweldige speler te worden en een ander pakket aan vaardigheden om een geweldige coach te zijn. In deze fase van uw onderneming, zult u willen beginnen met het lezen van boeken over het managen, coachen en motiveren van mensen.</p>
<br />
<p class="style24">Uw Marketing hulpmiddelen bijwerken</p>
<p>Elke keer dat u een promotie krijgt als een Manager of Director, zou u uw persoonlijke webpagina en welkom e-mails ( aan nieuwe Partners en Proefpartners) moeten aanpassen om uw nieuwe Pay Titel weer te geven.</p>
<p class="style24">Inzicht in de Coaching Commissions aanspraak</p>
<p>De Coaching Commissions wordt betaald aan de eerste upline Partner met een hogere Pay Titel dan elke Partner en staat gelijk aan 1/2 van de Referral Commissies die betaald wordt aan de Partner.</p>
<img alt="play" height="28" src="https://www.clubshop.com/play.png" width="27" /> <a href="http://www.clubshop.com/partner/flash/trainingBonus.html" target="_blank">ZIE VOORBEELD</a> <br /><br />
<p>Om uw recht op Coaches Commissions te maximaliseren, zult u het volgende          willen doen:</p>
<ol>
<li><span class="lightblue bold">Het goede voorbeeld geven</span> - In            plaats van alleen te proberen om uw Frontline en Downline Partners actief            te laten worden, zou u door moeten gaan met het verhogen van het aantal            Partners en Pay punten op en voor uw Frontline. Naast het verhogen van            uw winst, geeft u hiermee ook het goede voorbeeld aan uw Coaches EN            blijft u de eerste Partner upline met een hogere Pay titel. </li>
<li><span class="lightblue bold">Bouw diepte</span> - Wees erop voorbereid            om een Partnerlijn van tenminste 10 Partners diep te bouwen, om een            Partner te vinden die u kunt trainen en waarmee u kunt werken om Team            Captain te worden en hopelijk een toekomstige Executive Director. Zodra            u de Partners heeft gevonden die Team Captains worden, kunt u nog steeds            in aanmerking komen voor een Coaching Commissions te verdienen op hun verkoopvolume,            omdat de Coaches Commissions wordt "doorgegeven" aan de eerste Partner            met een hogere Pay titel. <br /> <br /> Wanneer u dus in een lijn aan het bouwen bent die meerdere Team Captains            produceert, dan verdient u de Coaches Commissies voor ieder van hen,            indien en tot 1 van de team Captains boven de andere Team Captain(s)            een Bronze Manager wordt. Dat zou dus uw doel moeten zijn - om diepte            te bouwen onder elke Partner die gestart is of die wil starten met het            verwijzen van Members, door hen te trainen hoe zij moeten opvolgen (door            de eerste opvolgingen voor hen te doen terwijl zij meekijken). </li>
<li><span class="lightblue bold">Gebruik een goede strategie om te bouwen </span> - Omdat u niet effectief diepte kunt bouwen met elke afzonderlijke            partner in uw team, werkt u met de beste(n) in tot zes verschillende            lijnen, zodat zij zelf het verwijzen en opvolgen zullen overnemen om            te kwalificeren als Team Captains. Ga dan verder met het coachen van            en werken met willekeurig welke Partner in een lijn verschijnt die de            beste kandidaat is om een hogere Pay Titel te krijgen in die lijn. Zou            deze dezelfde of een hogere Pay titel halen dan de Partners tussen u            en hen, dan blijft u een Coaches Commissions voor hen verdienen, <span class="bold">zolang            u een hogere Pay titel blijft houden dan zij hebben.</span> <br /> <br /> Naast het verhogen van uw Coaching Commissions, zult u tevens de fundering            leggen voor het ontwikkelen van 6 toekomstige Executive Directors om            zo lid te worden van de Ambassador's Club. </li>
</ol>
<p class="style24">Members verplaatsen</p>
<p>U kunt gebruik maken van de Transfer Functie om diverse frontline lidmaatschaptypes te verplaatsen naar ontvangers die hiervoor in aanmerking komen, om uw verkooporganisatie beter te beheren. Maar aangezien elke transfer permanent is, <span class="bold">moet u eerst overleggen met uw Coach of Executive Director</span> voor u dit doet. Normaal gezien, zou u geen transfers moeten uitvoeren totdat u Executive Director bent geworden, omdat iedere transfer die u doet uw potentieel vermindert om Referral Commissies te verhogen en een promotie te krijgen.</p>
<p class="style24">Hoe u de team coaches in uw team helpt</p>
<ol>
<li><span class="lightblue bold">Besteed aandacht aan hen!</span> - Het is belangrijk om snel, inspirerende berichten te versturen zodat zij weten dat u hen in het oog heeft. Bloemen groeien richting de zon! <br /><br />Communiceer consequent met uw coaches en <strong>moedig aan</strong> om hen te helpen realistische doelen te stellen, maak goede plannen, werk op prioriteit en leer effectief op te volgen. Regeer zo snel mogelijk op ze zodat zij vol vertrouwen verder kunnen gaan met hun werkzaamheden. <br /><br />Wanneer u communiceert met Coaches die Coaches tussen u en hen hebben, zet deze andere coaches dan in cc in uw communicaties. Vergeet niet dat dit ook hun team is. </li>
<li><span class="lightblue bold">Coaches controleren!</span> - Elke week zou u op hun activiteiten rapporten moeten kijken om te zien hoe zij hun proefpartners opvolgen en hoe zij de Partners in hun team helpen. Applaud hun inspanningen en resultaten. Wanneer u kunt zien dat zij misschien hulp nodig hebben om hun vaardigheden te verbeteren, vraag hen dan een afspraak te maken om samen de activiteiten in hun Activiteiten Rapport en hun Follow Up Records door te nemen, of om <span class="bold">rollenspellen</span> te doen. <br /><br />De meeste mensen vragen er niet om of willen niet worden gecorrigeerd of bekritiseerd, u moet dus de juiste coaching vaardigheden toe kunnen passen voordat u probeert om uw Partners of Coaches te "helpen". <span class="bold">Onthoud altijd dat het mensen niet kan schelen hoeveel u weet, totdat zij weten hoeveel het u kan schelen.</span> </li>
<li><span class="lightblue bold">Vervang voor hen</span> - Er zullen tijden zijn dat een Coach niet in staat zal zijn om diens verantwoordelijkheden te nemen, tijdens een vakantie, ziekte of andere persoonlijke problemen. Wanneer dit zich voordoet, laat hen dan weten dat u beschikbaar bent zodat u <span class="bold">tijdelijk</span> kunt bijspringen om de hun rol uit te voeren gedurende hun afwezigheid. <br /><br />Zij moeten ook de Proefpartners en Partners in hun team ervan op de hoogte stellen dat zij contact opnemen met u en indien mogelijk, een auto responder instellen voor hun zakelijke e-mail om eventuele vragen rechtstreeks aan u te sturen terwijl zij weg zijn. </li>
<li><span class="lightblue bold">Algemene Coaching Tips</span> <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Focus op de Grondbeginselen.</span> <br /> Focus op de Grondbeginselen van het werven van nieuwe Members en communiceer effectief met Proefpartners. Doe het, onderwijs het, doe het, onderwijs het, doe het, onderwijs het! De zaak is eenvoudig en leuk wanneer de focus blijft bij de basisprincipes van persoonlijk werven van Members, om nieuwe Proefpartners te krijgen, om Frontline Partners te sponsoren, om diepte te bouwen om nieuwe Team Captains in elke lijn te krijgen. <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Gebruik de "Sandwich Techniek" bij het instrueren en corrigeren.</span> <br /> Sandwich uw instructies en correcties tussen positieve en bemoedigende commentaren. Begin met het prijzen van de dingen die ze goed doen. Sandwich dan uw instructies en correcties erin. Sluit af door hen te bemoedigen over de toekomst. Laat mensen altijd achter met het gevoel dat u kritiek heeft op een handeling, niet op hen als persoon. <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Verwijs naar de training</span><br /> Wanneer u instructies geeft, geef er dan links bij naar de training die van toepassing is op hun situatie of kopieer en plak het uit de les en verwijs naar de les. Dit zal hen helpen om in te zien dat u hen niet persoonlijk bekritiseerd, maar maakt hen ervan bewust hoe hun handelen verschilt van de training. Dit maakt hen ook meer van bewust om eerst hun Handleiding of Training Gids te raadplegen, voordat zij naar u komen met hun vragen. <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Bouw uw Coaches op naar hun teams</span><br /> Wanneer u communiceert met Partners die in de downline staan van een Coach, zoek dan altijd naar manieren om hun Team Coachen naar hen toe op te bouwen (iets positiefs over hen te benoemen). Verspreid de erkenning in het rond en bouw "esprit de corps" binnen uw team. <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Begeleid en geef sturing aan uw Downline Partners</span><br /> Als u activiteit en groei ziet in uw team, dan is het uw rol om de erkenning hiervoor door te geven aan diegenen die werken. Zet altijd de Partners tussen u en uw downline Partners in cc in uw e-mails, zodat zij kunnen zien wat er gebeurd en meedelen in de lof die u geeft. <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Benadruk de waarde van het systeem</span><br /> Maak het onderdeel van uw dagelijkse communicaties om uw Partners er aan te herinneren dat ons Referral Marketing Plan en Partner Inkomsten mogelijkheid zoveel mogelijk maken voor hen en de mensen die zij verwijzen. Moedig Partners aan om te beginnen met de training en om de volgende stap te zetten om een Team Player, Team Leader en Team Captain te worden.  Promoot het Global Partner Systeem (GPS) herhaaldelijk, zodat de Partners in uw team zich bewust worden van de waarde van hun GPS abonnement.  Wat je benadrukt, vermenigvuldigt! <br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Bouw je Coach, Executive Director en het ClubShop Rewards personeel op </span><br /> De meeste succesvolle Partners zijn diegenen die de schijnwerpers en hun leiderschap willen delen. Pas op voor een dodelijke organisatie marketing ziekte genoemd, <span class="bold">"Mijngroepitis"</span>. Deze ziekte infecteert een organisatie wanneer een "leider" geen erkenning geeft, opbouwt of werkt met diens Coach, Executive Director en ClubShop Rewards personeel. Blijft dit onopgemerkt, dan zullen deze "leiders" dit gaan dupliceren. Het resultaat? Gebrek aan teamwerk, verdeeldheid en tweedracht. Het team valt dan uit elkaar. 		<br /><br /> <img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <span class="style30">Gebruik Teambijeenkomsten, Webinars, Conferenties en conventies om eenheid te creëren</span><br /> Online Team bijeenkomsten en Webinars en offline regionale Conferenties en jaarlijkse  continentale conventies zullen de meest belangrijke zaken zijn om motivatie en vertrouwen op te bouwen in uw team. Loyaliteit wordt niet opgebouwd door e-mails. Het ontstaat wanneer persoonlijke reacties worden gecreëerd en deze ontwikkelen zich het best wanneer mensen samen tijd doorbrengen. 	  <br /><br /> </li>
</ol>
<p class="style24">Gebruik het Partner Compensatieplan om uw doelen te bepalen</p>
<p>Ons Partner Compensatieplan maakt het erg realistisch om elke maand een nieuw Pay Level te bereiken totdat iemand het Executive Director level bereikt. Dit is zo ontworpen. Wees u ervan bewust wat u exact nodig heeft aan PERSOONLIJKE MARKETING &amp; TEAM MARKETING Pay Level Pay punten (PLPP) om uw Pay level te blijven behouden EN om een promotie te realiseren.</p>
<p><span class="bold">Schrijf uw doelen op en kijk hier elke dag naar.</span> Geloof in uzelf en in uw potentie om een Executive Director en Ambassadors Club member te worden, zodat u een "full time" inkomen kunt ontwikkelen van uw ClubShop Rewards zaak.</p>
<hr />
<p><a href="http://www.clubshop.com/manual/"><img alt="Manual Home Page" height="64" src="http://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
</div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>