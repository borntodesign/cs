﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="lesson_6.xsl" ?><lang_blocks>
  <detour>Until we have competed the new presentation, use the</detour>
  <detour1>TNT Q &amp; A Page</detour1>
  <detour2>explain to Affiliates and Partner prospects how TNT 2.0 works.</detour2>
  <detour3>Team Pool Mini Slide Show</detour3>
  <detour3a>and the</detour3a>
  <lesson>Les 6</lesson>
  <next>Volgende</next>
  <p10>Vraag hen over hun huidige werksituatie: &quot;Wilt u iets over uzelf vertellen, wat doet u voor werk?&quot;</p10>
  <p11>Deel wat informatie over uzelf - waar u woont en wat uw rol is als Sponsor en/of Coach</p11>
  <p12>Kwalificatie - stel vast wat hun motivatie en toewijding is, door hen twee vragen te stellen:</p12>
  <p13>Bent u serieus op zoek naar een manier om meer geld te verdienen en uw eigen zaak te starten?</p13>
  <p14>Bent u bereidt om 5 - 10 uren per week te investeren om u te laten trainen en te werken aan uw zaak?</p14>
  <p15>Indien zij reageren met een &quot;NEE&quot; om 1 van de vragen, beëindig dan het gesprek en nodig hen uit hun lidmaatschap te gebruiken om geld te besparen.</p15>
  <p16>Laat het plan zien - laat de dia presentatie zien</p16>
  <p17>Vragen - Vraag of zij vragen hebben. Leg hen nogmaals de Geld Terug Garantie uit om hen te helpen alle bezwaren die zij kunnen hebben te overwinnen.</p17>
  <p18>Beloof te helpen - doe de belofte om Affiliates naar hun account te verplaatsen indien zij deze week Partner worden, om hen te helpen van start te gaan en hopelijk hun eerste Partner(s) te krijgen.</p18>
  <p19>Geef hen via het chat/ tekstvak de link om aan te melden en adviseer hen om van start te gaan met GPS BASIS of BASIS PLUS.</p19>
  <p2>AFSPREKEN MET PROSPECTS OM HET PLAN TE LATEN ZIEN</p2>
  <p20>Zodra het gesprek afgelopen is, stuurt u hen onderstaande Opvolg e-mail, werk vervolgens hun</p20>
  <p21>Follow Up Record</p21>
  <p22>bij en stel een herinneringsdatum in over 2 dagen. Indien zij zich niet op GPS hebben geabonneerd en Partner zijn geworden op uw herinneringsdatum, stuur dan de tweede opvolg e-mail om na te gaan of er nog vragen zijn.</p22>
  <p22a>Opvolg e-mail</p22a>
  <p23 literal="1">&lt;span class=&quot;bold&quot;&gt;ONDERWERP:&lt;/span&gt; Het was prettig u te spreken &lt;span class=&quot;style3&quot;&gt;VOORNAAM&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;ONDERWERP:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;VOORNAAM&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;ONDERWERP:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;ONDERWERP:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;VOORNAAM&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;SUBJECT:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;SUBJECT:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;SUBJECT:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;bold&quot;&gt;SUBJECT:&lt;/span&gt;
    &lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;<span class="bold">SUBJECT:</span>
    <span class="style3">FIRSTNAME</span>
  </p23>
  <p23a>VOORNAAM,</p23a>
  <p24>Het was prettig u te spreken en met u te delen hoe de ClubShop Rewards Zakelijke Mogelijkheid voor u kan werken.</p24>
  <p25>Een aantal zaken om in overweging te nemen bij het nemen van het besluit om u te abonneren op GPS en Partner te worden:</p25>
  <p26>Dit zal UW zaak zijn. U bepaalt uw eigen uren en werkt wanneer u zelf wilt.</p26>
  <p27>U doet zaken voor uzelf, maar u doet dit niet alleen. Ik zal u helpen om van start te gaan en op gang te komen zodat u snel een aantal Members (potentiële klanten), Affiliates en Partners hebt.</p27>
  <p28>Uw initiële GPS abonnement betaald voor uw toegang tot GPS gedurende volgende maand. Onthou dat alles wat u nodig hebt, is twee Partners voor het eind van volgende maand om voldoende te verdienen om de maand erna de bijdrage van te betalen ;)</p28>
  <p29 literal="1">Indien u deze week Partner wordt, zal ik &lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt; Affiliate accounts naar uw sponsorschap verplaatsen om u van start te helpen. Vervolgens zal ik u helpen om hen op te volgen, net zoals ik met u heb gedaan. Wij kunnen u misschien helpen om uw eerste (twee) Partners te krijgen uit deze Affiliates.&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;&lt;span class=&quot;style3 bold&quot;&gt;X&lt;/span&gt;<span class="style3 bold">X</span>
  </p29>
  <p3>Wanneer u online afspreekt, gebruik dan Skype en kies de &quot;Deel Scherm&quot; mogelijkheid, uw prospect kan dan uw scherm zien. Zorg ervoor dat de eerste dia van de presentatie zichtbaar is op uw scherm.</p3>
  <p30>Ik wil u aanraden om van start te gaan met een GPS BASIS abonnement. Zodra u zich inschrijft, hebt u 30 dagen om alles door te nemen, uit te proberen en de beslissing te nemen of u door wilt gaan als Partner. Indien u uw lidmaatschap binnen 30 dagen annuleert, krijgt u uw initiële abonnementsbijdrage teruggestort.</p30>
  <p31 literal="1">&lt;span class=&quot;style3&quot;&gt;VOORNAAM&lt;/span&gt;, u loopt geen enkel risico en hebt niets te verliezen, maar u kunt er VEEL mee winnen door Partner te zijn!&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;&lt;span class=&quot;style3&quot;&gt;FIRSTNAME&lt;/span&gt;<span class="style3">FIRSTNAME</span>
  </p31>
  <p32>Hier is de link om u in te schrijven:</p32>
  <p33>Indien u problemen heeft met inschrijven of het insturen hiervan, laat het me dan even weten en ik help u. Laat het me ook even weten wanneer u verder nog vragen heeft.</p33>
  <p34>Zodra u Partner bent geworden, krijgt u informatie over hoe u van start dient te gaan en ik zal hiervan ook op de hoogte worden gesteld. we kunnen dan een nieuwe afspraak maken zodat ik u kan laten zien hoe u de Affiliate accounts kunt bekijken die ik naar u verplaats en om een strategie op te zetten die het best voor u werkt.</p34>
  <p35>Ik hoop dat u beslist om die ene kleine stap van vertrouwen te zetten en met ons mee te doen als Partner!</p35>
  <p36>Met vriendelijke groeten,</p36>
  <p37>UW NAAM</p37>
  <p38 literal="1">&lt;span class=&quot;orange bold&quot;&gt;OPTIONEEL&lt;/span&gt; - Als u in staat bent en bereid bent om (3 - 10) Affiliates te verplaatsen naar een nieuwe Partner, voeg dit hieronder dan toe als # 4. Zo niet, voeg dit aanbod dan niet toe en wijzig nummer 5 in 4. &lt;span class=&quot;orange bold&quot;&gt;OPTIONEEL&lt;/span&gt;&lt;span class=&quot;orange bold&quot;&gt;OPTIONEEL&lt;/span&gt;&lt;span class=&quot;orange bold&quot;&gt;OPTIONEEL&lt;/span&gt;&lt;span class=&quot;orange bold&quot;&gt;OPTIONEEL&lt;/span&gt;&lt;span class=&quot;orange bold&quot;&gt;OPTIONAL&lt;/span&gt;&lt;span class=&quot;orange bold&quot;&gt;OPTIONAL&lt;/span&gt;<span class="orange bold">OPTIONAL</span>
  </p38>
  <p4>Engels/USD</p4>
  <p5>Engels/Euro</p5>
  <p6>Italiaans/Euro</p6>
  <p7>Frans/Euro</p7>
  <p7a>Nederlands/Euro</p7a>
  <p8>Introductie</p8>
  <p9>Stel uzelf voor en laat hen weten dat u blij bent dat u hen kunt ontmoeten.</p9>
  <pindex>Cursus Index</pindex>
  <pp>SPREEK AF MET UW PROSPECTS OM HET PLAN TE LATEN ZIEN</pp>
  <previous>Vorige</previous>
  <underconstruction>The Business Opportunity Power Point (Slide Show) Presentation is being updated to show how TNT 2.0 and the revised Partner Compensation Plan work.</underconstruction>
</lang_blocks>
