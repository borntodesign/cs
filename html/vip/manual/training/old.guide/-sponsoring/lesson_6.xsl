<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>Partner Prospecting Course</title>
<script type="text/javascript" src="/cgi/PSA/script/8"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>

    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Header_Main {
	font-size: 14px;
	color: #0089B7;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.2em;
	font-family: verdana;
	text-transform: uppercase;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.Text_Content_heighlight {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #0089B7;
	text-transform: uppercase;
	font-weight:bold;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.ContactForm_TextField {
    
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}

.style1{font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
}


.style3 {
	font-size: 12px;
	font-family: verdana;
	color: #A91B07;
	font-weight:bold;
}

.style24 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-transform: uppercase;
	color: #2A5588;
}
.red {color: #A91B07;}
.grey{color: #999999;}
.darkblue {color: #011A52;}

.medblue {color:#004A95;}

.bold{font-weight:bold;}

.smaller{font-size: 13px;}

.blue_bullet{list-style: url(//www.clubshop.com/images/partner/bullets/bluedot.png);}

.half{list-style: url(//www.clubshop.com/images/partner/bullets/halfcir.png);}


.topTitle{text-align:center; color: #5080B0; }

.liteGrey{color:#b3b3b3;}

li.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(//www.clubshop.com/images/minions/icon_blue_bulleted.png);
}	

	
li.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(//www.clubshop.com/images/minions/icon_gold_bulleted.png);
}


li.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;

}




.img_heads {
	margin-bottom: -10px;
}
	
	
td.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	background-color: #FFFFFF;
}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

</style>



</head>

<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">




<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p2"/></b></span>

<hr/>

<span class="liteGrey"><b><xsl:value-of select="//lang_blocks/lesson"/></b></span>
<br/><br/>





<p><xsl:value-of select="//lang_blocks/detour"/><xsl:text> </xsl:text><a href="//www.clubshop.com/affiliate/2.0.html" target="_blank"><u><xsl:value-of select="//lang_blocks/detour1"/></u></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/detour3a"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/present/biz-opp/pps_TNT.html" target="_blank"><xsl:value-of select="//lang_blocks/detour3"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/detour2"/></p>


<p><xsl:value-of select="//lang_blocks/p3"/></p>
<!--

		<ul>
		<li class="blue_bullet"><a href="http://www.clubshop.com/present/biz-opp/pps_aff_en.html" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></li>
		<li class="blue_bullet"><a href="http://www.clubshop.com/present/biz-opp/pps_aff_en-eur.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></li>
		<li class="blue_bullet"><a href="http://www.clubshop.com/present/biz-opp/pps_aff_it.html" target="_blank"><xsl:value-of select="//lang_blocks/p6"/></a></li>
		<li class="blue_bullet"><a href="http://www.clubshop.com/present/biz-opp/pps_aff_fr.html" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a></li>
		<li class="blue_bullet"><a href="http://www.clubshop.com/present/biz-opp/pps_aff_nl.html" target="_blank"><xsl:value-of select="//lang_blocks/p7a"/></a></li>

		</ul>
-->

	
	<ol>
	<li><xsl:value-of select="//lang_blocks/p8"/><br/>
		<ul>
		<li class="half"><xsl:value-of select="//lang_blocks/p9"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/p10"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/p11"/></li>
		
		</ul>	
	
	</li>
	
	<li><xsl:value-of select="//lang_blocks/p12"/><br/>
	
	<ul>
		<li class="half"><xsl:value-of select="//lang_blocks/p13"/></li>
		<li class="half"><xsl:value-of select="//lang_blocks/p14"/></li>
		
		
		</ul>	
	<p><xsl:value-of select="//lang_blocks/p15"/></p>
	</li>
	<li><xsl:value-of select="//lang_blocks/p16"/></li>
	<li><xsl:value-of select="//lang_blocks/p17"/></li>
	<li><xsl:value-of select="//lang_blocks/p18"/></li>
	<li><xsl:value-of select="//lang_blocks/p19"/></li>
	
	
	</ol>


<p><xsl:value-of select="//lang_blocks/p20"/><xsl:text> </xsl:text><a href="" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/></p>

<p><span class="style24"><xsl:value-of select="//lang_blocks/p22a"/></span></p>
<blockquote>

<p><xsl:value-of select="//lang_blocks/p23"/><xsl:text> </xsl:text><span class="style3 bold"><xsl:value-of select="//lang_blocks/p23a"/></span></p>
<p><span class="style3"><xsl:value-of select="//lang_blocks/p23a"/></span>,</p>

<p><xsl:value-of select="//lang_blocks/p24"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p26"/></li>
<li><xsl:value-of select="//lang_blocks/p27"/></li>
<li><xsl:value-of select="//lang_blocks/p28"/></li>
<p><span class="orange bold"><xsl:value-of select="//lang_blocks/p38"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38a"/></p>
<li><xsl:value-of select="//lang_blocks/p29"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p29b"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29c"/></li>
<li><xsl:value-of select="//lang_blocks/p30"/></li>

</ol>

<p><span class="style3"><xsl:value-of select="//lang_blocks/p23a"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></p>
<p><xsl:value-of select="//lang_blocks/p32"/><xsl:text> </xsl:text>https://www.clubshop.com/cgi/appx.cgi/upg?alias=<span class="style3">IDNUMBER</span></p>
<p><xsl:value-of select="//lang_blocks/p33"/></p>
<p><xsl:value-of select="//lang_blocks/p34"/></p>
<p><xsl:value-of select="//lang_blocks/p35"/><br/>
<xsl:value-of select="//lang_blocks/p36"/><br/>
<span class="style3"><xsl:value-of select="//lang_blocks/p37"/></span></p>

</blockquote>

















<br/><br/><br/><br/><br/><br/>


<div align="center"><p>
<a href="lesson_7.xml"><xsl:value-of select="//lang_blocks/next" /></a></p> </div>        
     

         
         
         
</div>         
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2013
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
