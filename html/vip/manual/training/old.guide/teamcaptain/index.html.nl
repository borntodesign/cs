﻿<!DOCTYPE HTML>

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
<head>
<title>ClubShop Rewards Partner Handleiding</title>
<script type="text/javascript" src="/cgi/PSA/script/9"><!-- Indien het lidmaatschap van de Partner niet hoog genoeg genoeg is, dan zal het javascript hen doorsturen --></script>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />
<!--  <link rel="stylesheet" type="text/css" href="/css/foundation/app.css" /> -->
<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="/js/foundation/foundation.js"></script>
<script src="/js/foundation/flash.js"></script>
<script src="/js/foundation/jquery.reveal.js"></script>
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<style>

div.b{
/*	display: none; */
	font-size: 75%;
	background-color: #DFEAFF;
	padding: 3px 6px;
	border: 1px solid silver;
	margin-top: 2px;
	width: 50%;
}

</style>
<style>
.special{
font-family: 'Nunito', sans-serif; 
font-size: 16px;}

.special2{
font-family: 'Nunito', sans-serif; 
font-size: 14px;}
.column1{
background: rgb(136,191,232);
background: -moz-linear-gradient(top,  rgb(136,191,232) 0%, rgb(112,176,224) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(136,191,232)), color-stop(100%,rgb(112,176,224)));
background: -webkit-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: -o-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: -ms-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#88bfe8', endColorstr='#70b0e0',GradientType=0 );

}

.column2 {
background: rgb(20,106,181);
background: -moz-linear-gradient(top,  rgb(20,106,181) 0%, rgb(14,103,181) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(20,106,181)), color-stop(100%,rgb(14,103,181)));
background: -webkit-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: -o-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: -ms-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#146ab5', endColorstr='#0e67b5',GradientType=0 );

}
.white {color: #F0F0F0;}


#translate-this .translate-this-button {
   background-image: url(http://www.clubshop.com/images/translator.jpg) !important;
   width: 180px !important;
   height: 20px !important;
   margin-top: 10px !important;                   
}

</style>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="https://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><!-- Begin TranslateThis Button -->
<div id="translate-this"><a class="translate-this-button" href="http://www.translatecompany.com/" style="width: 180px; height: 18px; display: block;">Translate Company</a></div>
<script src="http://x.translateth.is/translate-this.js" type="text/javascript"></script>
<script type="text/javascript">// <![CDATA[
TranslateThis();
// ]]></script>
<!-- End TranslateThis Button --> <br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">TEAM CAPTAIN LES</span> 
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
<p>De persoon die is aangewezen als de Coach van een Partner is de eerste Team Captain of hoger (Pay Level) "upline" van een Partner, die een hoger Pay Level heeft dan de "downline" Partner.</p>
<p>Bijvoorbeeld, wanneer een Partner een Team Player is, dan is de eerste Team Captain of hoger, upline van die Partner, aangewezen als diens Coach. Wanneer een Partner Team Captain is, dan is de eerste Bronze Manager of hoger, upline van hen diens Coach.</p>
<p>Dit geeft alle Partners een ervaren Coach die een hoger Pay Level heeft bereikt.</p>
<p class="style24">Team Captain Rol</p>
<p>Als Team Captain zou u moeten optreden als de primaire Coach voor de Partners, Team Players en Team Leaders in uw Team. Wanneer een andere Partner in het team Team Captain wordt, dan neemt deze de rol van Coach over voor diens Team.</p>
<p>Als Coach moet u:</p>
<ul>
<li class="blue_bullet">Een welkom sturen en <span class="bold">ontmoet</span> u nieuwe Partners</li>
<li class="blue_bullet">Alle vragen beantwoorden die u toegestuurd krijgt</li>
<li class="blue_bullet">Communiceren met de potentiële partners in uw team om hen uit te nodigen om Proefpartners te worden</li>
<li class="blue_bullet">Ervoor zorgen dat alle proefpartners in uw Team worden opgevolgd</li>
<li class="blue_bullet">Uw Partners in uw team aanmoedigen en toestaan om uw communicaties en bijeenkomsten te observeren die u heeft met hun persoonlijk verwezen Proefpartners en Partners</li>
<li class="blue_bullet">Leid door het goede voorbeeld te geven aan persoonlijk verwezen Members en persoonlijk gesponsorde Partners</li>
</ul>
<br />
<p class="style24">ZORG DAT NIEUWE PARTNERS GOED VAN START GAAN</p>
<p>Het belangrijkste dat u kunt doen om uw nieuwe Partner te helpen om goed van start te gaan, is hen te laten beginnen met het verwijzen van nieuwe Members. Hier is waarom:</p>
<ul>
<li class="blue_bullet">Het helpt hen <span class="short_text" id="result_box" lang="nl"><span class="hps">om te beginnen met</span> <span class="hps">"eigendom"</span><span class="hps"> en verantwoordelijkheid te nemen voor hun zaak</span></span>. </li>
<li class="blue_bullet">Het geeft hen het vertrouwen dat zij nodig hebben om meer betrokken en actief te worden. Zien hoe hun eerste "U hebt een nieuwe Member" e-mail notificaties toenemen binnen hun eerste week, zal hen een grote boost in zelfvertrouwen geven!</li>
<li class="blue_bullet">Zodra zij hun eerste nieuwe Members hebben, kunnen ze ook 1 of een aantal voordelen krijgen om hun enthousiastme voor het zijn van een Partner te laten groeien: 	  <br /><br /> 
<ul>
<li class="half">Hun nieuwe Member kan de enquête invullen om hun eerste ClubCash te verdienen. </li>
<li class="half">Hun nieuwe Member kan een aankoop doen om extra ClubCash te verdienen en waarover de nieuwe Partner diens eerste commissie verdient.</li>
<li class="half">Hun nieuwe Member kan snel anderen verwijzen als Members zodanig dat deze begint te zien dat diens consumentennetwerk zich begint te ontwikkelen. </li>
<li class="half">Hun nieuwe Member kan ervoor kiezen om een ​​Proefpartner te worden, om hun coach te voorzien van een "Partner Prospect" om op te volgen. Dit kan op zijn beurt diens nieuwe Partner in staat stellen om diens eerste nieuwe Partner te sonsoren EN de Coach in staat stellen om diepte te bouwen en diens Training Bonus te verhogen.</li>
</ul>
</li>
<br />
<li class="blue_bullet">Zodra zij een aantal Members verwijzen, is het minder waarschijnlijk dat ze stoppen - met name wanneer er Members bij zijn die zij kennen.</li>
<li class="blue_bullet">Zelfs wanneer de nieuwe Partner stopt, dan zal hun Coach nog steeds diens Members of Partners hebben om van te profiteren.</li>
</ul>
<br />
<p class="style24">Verwelkomen van nieuwe Partners</p>
<p>De Partner die een andere Partner sponsorde moet diens nieuwe Partner een e-mail sturen om hem/ haar te verwelkomen in het team. Als de Coach doet u hetzelfde, behalve dat u tevens moet proberen een <span class="bold">ontmoeting te regelen met de nieuwe Partner en nodig, indien mogelijk,</span> diens sponsor uit bij de ontmoeting.</p>
<p>Naast het versturen van een e-mail, moet u ook proberen contact met hen te zoeken via SKYPE en Facebook om <span class="bold">een datum en tijd vast te stellen om elkaar te spreken.</span></p>
<p>Controleer voor het gesprek, of zij al Members of Proef Partners hebben verwezen. Wanneer u elkaar spreekt, moet u, naast het kennismaken met elkaar:</p>
<ul>
<li class="blue_bullet">Nagaan of zij vragen hebben.</li>
<li class="blue_bullet">Uitleggen hoe het sponsoren van twee Partners hen kan helpen om genoeg geld te verdienen om hun maandelijkse abonnement bijdrage van te betalen.</li>
<li class="blue_bullet">Controleren of zij zijn begonnen met de "Van start gaan" Les in de Training Gids en of zij de "Uitnodiging om een Member te worden" e-mail hebben verstuurd, die in de les staat en of zij een member uitnodiging hebben geplaatst op hun Facebook Wall (en op alle andere sociale of zakelijke netwerk pagina's die zij hebben).</li>
<li class="blue_bullet">Wanneer zij deze e-mail nog niet hebben vezonden, laat hen dan de twee e-mails zien die onder aan in de les staan en laat hen er 1 uitkiezen die hen het meest aanspreekt. Laat zien hoe zij hun ID in de link plaatsen en de e-mail persoonlijk maken en laat deze direct naar u toesturen om door te nemen. <span class="bold">Doe dit terwijl u met hen in gesprek bent.</span>Vraag hen dan om de e-mail naar de mensen in hun e-mail adresboek te versturen.</li>
<li class="blue_bullet">Wanneer ze deze e-mail nog niet hebben verstuurd, laat hen dan de twee e-mails uit de les zien en laat hen er 1 kiezen die hen het meest aanspreekt. Help hen hun ID goed in de link te plaatsen en de e-mail persoonlijk te maken en laat deze eerst naar u sturen om te controleren. <span class="bold">Doe dit terwijl u met hen in gesprek bent.</span> Vraag hen dan om de e-mail naar de mensen in hun e-mail adresboek te versturen.</li>
<li class="blue_bullet">Uitleggen hoe u hen kan helpen om hun Proefpartners op te volgen om hen te helpen hun eerste Partners te krijgen en om hen te trainen in het opvolgen van Proefpartners. Met hun toestemming, begint u met de opvolgprocedure voor ieder Proefpartner die zij hebben.</li>
</ul>
<br /> <!--
<p class="style24">Het opvolgen van proef Partners voor nieuwe partners</p>
<ol>
<li>U voert de telefoongesprekken (terwijl zij meeluisteren) om deze prospects in te schrijven als gratis ClubShop Rewards Members en Proefpartners. Hou altijd rekening met de tijdzone waarin zij leven en bel niet te vroeg (met name in de weekenden) of te laat (met name de avond voor werkdagen). <br /> Hier is een website waarop u wereldwijd alle tijdzones terug kunt vinden, zet deze bij uw favorieten of maak er een shortcut van op uw browser balk: <a href="http://www.timeanddate.com/worldclock/" mce_href="http://www.timeanddate.com/worldclock/" target="_blank">De Wereldklok - Tijdzones</a></li>
<li>Doel: Help elke nieuwe Partner om tenminste twee Partners te sponsoren zodat zij kunnen beginnen geld te verdienen en een goed sponsor voorbeeld hebben gekregen om te dupliceren, zodat ze zelfstandig verder gaan met het persoonlijk sponsoren van Partners.</li>
<li>Als met uw hulp, een nieuwe Partner nieuwe Partners sponsort, werk dan op dezelfde manier met deze nieuwe Partners. Blijf dit doen "in de diepte" totdat u ziet dat er een Team Captain in die lijn is gekomen. Draag dan de Coach rol over aan deze Team Captain voor diens team.</li>
</ol> <br />-->
<p class="style24">Uitleg hoe u Team Commissies verdient</p>
<p>Team Commissiis zijn gelijk aan 1/2 van de Referral Commissies die een Partner krijgt en wordt verdient door de upline Team Captain of hoger, met een hogere Pay Titel dan de downline Partner. Dit is nog een reden waarom u al uw persoonlijk verwezen Partners wilt helpen om twee Partners te krijgen - om hen te helpen hun hun Referral Commissies te verhogen, omdat u de helft van hun Referral Commissies kunt verdienen als Team Commissies. Echter, zodra een Partner in uw Team hetzelfde pay level bereikt als u, heeft u niet langer recht op het verdienen van Team Commissies over hen.</p>
<p>Dit recht "gaat omhoog" naar de eerstvolgende Partner upline met een hogere Pay Titel. Dit is waarom u de Coach rol zou moeten overdragen aan een          Partner zodra deze Team Captain wordt, met name wanneer uzelf nog steeds          Team Captain bent.</p>
<br />
<p class="style24">Gebruik de GPS Rapporten</p>
<p>Als GPS abonnee heeft u de beschikking over twee zeer krachtige software programma's die gekoppeld zijn aan de ClubShop Rewards database, zij werken samen om u te helpen om uw rol professioneler en succesvoller uit te voeren:</p>
<ul>
<li class="blue_bullet"><span class="medblue">ACTIVITEITEN RAPPORT</span> - Besteed speciale aandacht aan de Proefpartner activiteit in uw team, door elke dag op dit rapport te kijken. Volg dan de richtlijnen in de Partner Prospecting Les om efficiënt nieuwe Partners in uw team te sponsoren.</li>
<li class="blue_bullet"><span class="medblue">FOLLOW UP RECORDS - OPVOLG REGISTRATIE</span> - Zet al uw opvolginformatie over de Proefpartners in hun Registratie en stel ook een nieuwe opvolgdatum in, welke in uw activiteitenrapport zal verschijnen op de ingestelde datum. Controleer deze registraties om te zien hoe de Partners in uw Team het doen met hun eigen opvolging. </li>
</ul>
<p>Als de Coach, kunt u het zich niet verooloven om 1 van de volgende verantwoordelijkheden te delegeren aan de Partners in uw Team totdat zij het Team Captain level hebben bereikt. Totdat zij zover zijn, moet U alle opvolging voor hen doen (terwijl zij observeren) om hen te helpen en te trainen hoe zij hun eigen Partner Prospecting moeten doen zodra zij een Team Leader worden.</p>
<p class="style24">GPS PRO Coach bijdragen</p>
<p>Bij een abonnement op GPS PRO, PRO PLUS, PREMIER of PREMIER PLUS, dient u maandelijks een extra Coach bijdrage te betalen, welke zal worden afgetrokken van uw commissies. Coach bijdragen helpen bij het compenseren van de toenemende opslag en bandwidth kosten die ontstaan door uw gebruik van de GPS rapporten en functies. Bekijk onderstaande tabel om vast te stellen wat de bijdrage is (in USD) voor elk pay level.</p>
<div align="center"><span class="style28">COACH BIJDRAGEN</span></div>
<table width="100%">
<tbody>
<tr>
<td class="column1 white special">
<div align="center">PAY LEVEL</div>
</td>
<td class="column2 white special">
<div align="center">EXTRA KOSTEN</div>
</td>
<td class="column1 white special">
<div align="center">EXTRA PP</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">TP &amp; TL</div>
</td>
<td class="special">
<div align="center">0</div>
</td>
<td class="special">
<div align="center">0</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">TC</div>
</td>
<td class="special">
<div align="center">$5</div>
</td>
<td class="special">
<div align="center">2</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">BM</div>
</td>
<td class="special">
<div align="center">$8</div>
</td>
<td class="special">
<div align="center">3</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">SM</div>
</td>
<td class="special">
<div align="center">$11</div>
</td>
<td class="special">
<div align="center">4</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">GM</div>
</td>
<td class="special">
<div align="center">$13</div>
</td>
<td class="special">
<div align="center">5</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">RD</div>
</td>
<td class="special">
<div align="center">$19</div>
</td>
<td class="special">
<div align="center">7</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">ED</div>
</td>
<td class="special">
<div align="center">$25</div>
</td>
<td class="special">
<div align="center">9</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">DD</div>
</td>
<td class="special">
<div align="center">$30</div>
</td>
<td class="special">
<div align="center">11</div>
</td>
</tr>
<tr>
<td class="special">
<div align="center">EXEC</div>
</td>
<td class="special">
<div align="center">$45</div>
</td>
<td class="special">
<div align="center">15</div>
</td>
</tr>
</tbody>
</table>
<br />
<p class="style24">Contact opnemen met Members om Affiliates te worden</p>
<p>Een verkooprapport dat voor u beschikbaar is om te gebruiken, is het <a href="http://clubshop.com/reports/sales_reports.shtml" target="_blank">Frontline Rapport</a>, welke u de lijsten geeft van uw persoonlijk verwezen member types.</p>
<p>U zou de Member en Affiliate lijsten tenminste eenmaal per maand moeten raadplegen om een e-mail of bericht te sturen of om contact op te nemen met de beste Partner Prospects op de lijsten om hen uit te nodigen om gratis Proefpartners te worden.</p>
<p>Om vast te stellen welke Members en Affiliates uw beste Partner Prospects kunnen zijn en welke methoden beschikbaar zijn om contact met hen te zoeken, raadpleeg de gegevens bovenaan het rapport:</p>
<ul>
<li class="blue_bullet"><span class="medblue">PROSPECT RANG</span> - Hoe hoger de rang, hoe beter de prospect. Wanneer een Member eenmaal meer informatie heeft gevraagd over hoe geld te verdienen met hun lidmaatschap, hebben zij een ranking van tenminste 1. Wanneer zij ergens in het verleden Proefpartner zijn geworden, hebben zij een ranking van tenminste 3. Elke keer dat zij hun rapport bekijken, krijgen zij een extra punt. </li>
<li class="blue_bullet"><span class="medblue">ACTIEF</span> - Een "Y" geeft aan dat zij de afgelopen 30 dagen actief op zoek zijn geweest naar meer informatie. Dit zijn uw beste prospects om te gaan opvolgen. Klik op hun ID nummer om hun Follow up Record te bekijken om te zien welke opvolging al heeft plaatsgevonden. Raadpleeg de Partner Prospecting Les om te leren hoe prospect Partners op te volgen. Een "N" geeft aan dat zij de afgelopen 30 dagen niet op zoek zijn geweest naar informatie.</li>
<li class="blue_bullet"><span class="medblue">E-MAIL STATUS</span> 
<ul>
<li class="half"><span class="bold">-2 = Blocked Domain</span> - Dit betekent dat u nog steeds in staat bent om een e-mail sturen, maar dat onze servers niet is staat zijn om hen e-mail te sturen.</li>
<li class="half"><span class="bold">-1 = Bad Email</span> - Dit betekent dat de e-mail die wij sturen "bouncen", terug komen. U zou deze e-mail adressen zo nu en dan moeten controleren om vast te stellen of ze nog niet werken. Merkt u dat een -1 email niet naar u terug komt "bouncen", ga dan naar de <a href="https://www.clubshop.com/vip/MemberModification.html" target="_blank">Member Modification/ wijziging Functie</a> en verander hun e-mail status terug naar 1. </li>
<li class="half"><span class="bold">0 = Undefined</span> - dit betekent dat zij ons geen e-mail adres hebben gegevens toen zij zich in het begin inschreven of dat zij hun e-mail adres hebben verwijdert of verandert nadat zij registreerden. Ziet u dat een 0 status een e-mail adres heeft dat werktof bounces (terugkomt), ga dan naar de <a href="https://www.clubshop.com/vip/MemberModification.html" target="_blank">Member Modification Functie</a> en pas hun status dienovereenkomstig aan. </li>
<li class="half"><span class="bold">1 = Text Only</span> - dit betekent dat wanneer zij in het begin registreerden hebben verzocht dat zij "text only" e-mails willen ontvangen. HTML type e-mails (gekleurde lettertypen, pagina's en afbeeldingen) kunnen nog steeds door hen geaccepteerd worden, maar zij kunnen nog steeds de voorkeur hebben voor het ontvangen van alleen tekst berichten.</li>
<li class="half"><span class="bold">2 = HTML</span> - dit betekent dat zij openstaan voor het ontvangen van Tekst of HTML e-mails.</li>
</ul>
</li>
</ul>
<p>Om contact op te nemen met een aantal van deze Members of Affiliates op een andere manier dan via e-mail, klikt u op hun ID nummer om te kijken of zij een telefoonnummer, Skypenaam, Facebook of Twitter pagina, etc. hebben</p>
<p class="style24">Enkele laatste gedachten over uw rol</p>
<ul>
<li class="blue_bullet">U bent niemands baas - vertel mensen niet wat zij moeten doen, tenzij ze om advies vragen. </li>
<li class="blue_bullet">U kunt alleen de Partners coachen die willen leren en luisteren. </li>
<li class="blue_bullet">U kunt mensen niet naar succes duwen, maar u kunt leren hoe u ze samen met u kunt meetrekken.</li>
<li class="blue_bullet">Het kan mensen niets schelen hoeveel u weet, totdat zij weten hoeveel het u kan schelen. </li>
</ul>
<hr />
<p align="center"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /> <a href="http://www.clubshop.com/vip/manual/training/guide/mgrdir/index.html">Volgende Les</a></p>
<br />
<p><a href="http://www.clubshop.com/manual/"><img alt="Home Pagina Handleiding" height="64" src="https://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
</div>
</div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>