<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>Partner Sponsoring Course</title>
<script type="text/javascript" src="/cgi/PSA/script/8"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>

     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="/js/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
<script src="//www.clubshop.com/js/panel.js"></script>
<script src="//www.clubshop.com/js/na_Hilight.js"></script>
<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style type="text/css">
.Header_Main {
	font-size: 14px;
	color: #0089B7;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.2em;
	font-family: verdana;
	text-transform: uppercase;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.Text_Content_heighlight {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #0089B7;
	text-transform: uppercase;
	font-weight:bold;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.ContactForm_TextField {
    
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}

.style1{font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
}


.style3 {
	font-size: 12px;
	font-family: verdana;
	color: #A91B07;
	font-weight:bold;
}

.style24 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-transform: uppercase;
	color: #2A5588;
}
.red {color: #A91B07;}
.grey{color: #999999;}
.darkblue {color: #011A52;}

.medblue {color:#004A95;}

.bold{font-weight:bold;}

.smaller{font-size: 13px;}

.blue_bullet{list-style: url(//www.clubshop.com/images/partner/bullets/bluedot.png);}

.half{list-style: url(//www.clubshop.com/images/partner/bullets/halfcir.png);}


.topTitle{text-align:center; color: #5080B0; }

.liteGrey{color:#b3b3b3;}

li.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(//www.clubshop.com/images/minions/icon_blue_bulleted.png);
}	

	
li.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(//www.clubshop.com/images/minions/icon_gold_bulleted.png);
}


li.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;

}




.img_heads {
	margin-bottom: -10px;
}
	
	
td.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	background-color: #FFFFFF;
}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

</style>



</head>

<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
<br/>
	<div id="google_translate_element"></div><script src="/js/googleTranslate.js"></script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<br/>
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>

	<hr />
	
	

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">




<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p2"/></b></span>

<hr/>

<span class="liteGrey"><b><xsl:value-of select="//lang_blocks/lesson"/></b></span>
<br/><br/>


<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p46"/></b></span><br/><br/>


<p><xsl:value-of select="//lang_blocks/p47"/></p>
<ul>
<li class="blue_bullet"><span id="started-upgrade"><xsl:value-of select="//lang_blocks/p48"/></span><br/><br/>
<xsl:value-of select="//lang_blocks/p49"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/manual/training/guide/sponsoring/lesson_7.xml" target="_blank"><xsl:value-of select="//lang_blocks/p50"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51"/></li>
<li class="blue_bullet"><span id="submitted-application"><xsl:value-of select="//lang_blocks/p52"/></span><br/><br/>
<xsl:value-of select="//lang_blocks/p53"/><xsl:text> </xsl:text>"<a href="http://www.clubshop.com/vip/manual/training/guide/sponsoring/lesson_8.xml" target="_blank"><xsl:value-of select="//lang_blocks/p54"/></a>".<br/><br/><xsl:value-of select="//lang_blocks/p55"/></li>
</ul>

<br/>



<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p4"/></b></span> <br/><br/>

<p><xsl:value-of select="//lang_blocks/p3"/></p>

<p><xsl:value-of select="//lang_blocks/p5"/></p>
	<!--<ol>
		<li><xsl:value-of select="//lang_blocks/p6"/></li>
		<li><xsl:value-of select="//lang_blocks/p7"/></li>
		<li><xsl:value-of select="//lang_blocks/p8"/></li>

	</ol>

</p>-->

<p><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/compensation/partner.php" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p12"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></p>
<br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p14"/></b></span>
<p><xsl:value-of select="//lang_blocks/p15"/></p>
<p><xsl:value-of select="//lang_blocks/p16"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/policies/partnersponsor.xml" target="_blank"><xsl:value-of select="//lang_blocks/p17"/></a></p>


<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p18"/></b></span>

<p><xsl:value-of select="//lang_blocks/p19"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/manual/training/guide/coaching/index.html" target="_blank"><xsl:value-of select="//lang_blocks/p20"/></a></p>

<p><xsl:value-of select="//lang_blocks/p21"/></p>
<p><xsl:value-of select="//lang_blocks/p22"/></p>


<p><xsl:value-of select="//lang_blocks/p43"/></p>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p40"/></b></span>

<p><xsl:value-of select="//lang_blocks/p41"/></p>
<p><xsl:value-of select="//lang_blocks/p44"/><xsl:text> </xsl:text><a href="//www.clubshop.com/cgi/vip/gps_modification.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p45"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41a"/></p>



<p><xsl:value-of select="//lang_blocks/p42"/></p>



<br/><br/><br/><br/><br/><br/>


<div align="center"><p>
<a href="lesson_8.xml"><xsl:value-of select="//lang_blocks/previous" /></a></p> </div>        
     

         
         
  </div>       
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                    
          <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
