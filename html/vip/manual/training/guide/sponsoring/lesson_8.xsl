<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/pp"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="/cgi/PSA/script/8"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>
            
   <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <script src="//www.clubshop.com/js/panel.js"></script>



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>




</head>

<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
<br/>
<br/>
	<div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/pp"/></h4>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">
	<span class="liteGrey"><b><xsl:value-of select="//lang_blocks/lesson"/></b></span>
	
	
	

<hr/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p2"/></b></span>
<br/>  <br/>     <xsl:value-of select="//lang_blocks/p3"/>



<table><tr><td>
<ul>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop.html" target="_blank"><xsl:value-of select="//lang_blocks/p4"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-euro.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>


<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-in.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-ng.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-php.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-pk.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_EN-za.html" target="_blank"><xsl:value-of select="//lang_blocks/p5"/></a></span></li>
</ul>
</td></tr>
<tr><td>
<ul>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop.html.it" target="_blank"><xsl:value-of select="//lang_blocks/p6"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop.html.fr" target="_blank"><xsl:value-of select="//lang_blocks/p7"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_FR-tn.html" target="_blank"><xsl:value-of select="//lang_blocks/p7b"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_FR-bf.html" target="_blank"><xsl:value-of select="//lang_blocks/p7c"/></a></span></li>
<li><span class="style24"><a href="http://www.clubshop.com/present/biz-opp/pps_bop_FR-ma.html" target="_blank"><xsl:value-of select="//lang_blocks/p7d"/></a></span></li>
</ul>
</td></tr>

</table>



<p><span class="style24"><xsl:value-of select="//lang_blocks/p8"/></span></p>
<ul>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p9"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p10"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p11"/></li>

</ul>


<p><span class="style24"><xsl:value-of select="//lang_blocks/p12"/></span></p>
<ul>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p13"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p14"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p15"/></li>

</ul>


<p><span class="style24"><xsl:value-of select="//lang_blocks/p16"/></span></p>
<ul>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p17"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p18"/></li>
<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p19"/></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p20"/> <xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/vip/member-followup.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p21"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/></p>

<!--

<ul>

<li class="d"><xsl:value-of select="//lang_blocks/p5"/></li><br/>

<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>
</ul>


<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p8"/></li></ol>





<ul>
<li class="d"><xsl:value-of select="//lang_blocks/p12"/></li>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">

<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>



<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p14"/></li></ol>




<p><xsl:value-of select="//lang_blocks/p15"/></p>
</ul>




<p class="style24"><xsl:value-of select="//lang_blocks/p16"/></p>

<p><xsl:value-of select="//lang_blocks/p17"/></p>
<ul><li class="d"><xsl:value-of select="//lang_blocks/p18"/></li>

<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_green_cell_phone.png" alt="Cell Phone" height="131" width="106" align="left"/><xsl:text> </xsl:text>
<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p19/*|//lang_blocks/p19/text()"/></p>

</td></tr></table>

<br/>
<ol>
<li class="half"><xsl:value-of select="//lang_blocks/p20"/></li>
<li class="half"><xsl:value-of select="//lang_blocks/p21"/></li>
<li class="style3 half"><xsl:value-of select="//lang_blocks/p22"/></li>
</ol>

</ul>



<p class="style24"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_email.png"/>

<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<p><xsl:value-of select="//lang_blocks/p26"/></p>
<p><xsl:value-of select="//lang_blocks/p27"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com" class="Nav_Green_Plain">http://www.glocalincome.com</a><xsl:text> </xsl:text>
<xsl:copy-of select="//lang_blocks/p28/*|//lang_blocks/p28/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p29"/></p>
<p><xsl:value-of select="//lang_blocks/p30"/></p>
<p><xsl:value-of select="//lang_blocks/p31"/></p>
<p><b><xsl:value-of select="//lang_blocks/p32"/></b></p>



</td></tr></table><br/>

<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p33"/></li></ol>

</ul>
<p><xsl:value-of select="//lang_blocks/p34"/></p>
-->

<p><img src="http://www.clubshop.com/vip/manual/images/email_icon.gif"/><b><xsl:value-of select="//lang_blocks/p22a"/></b></p>
<div class="toggle"  style="display:none;"> 
            <div class="row"> 
              <div class="eleven columns offset-by-one"> 
                <div class="panel">
                
                <p><xsl:value-of select="//lang_blocks/p23"/></p>

Hello <span class="style3"><xsl:value-of select="//lang_blocks/p23a"/></span>,<br/><br/>

<xsl:value-of select="//lang_blocks/p24"/><br/><br/>
<xsl:value-of select="//lang_blocks/p25"/><br/><br/>
<ul>
<li><xsl:value-of select="//lang_blocks/p26"/></li>
<li><xsl:value-of select="//lang_blocks/p27"/></li>
<li><xsl:value-of select="//lang_blocks/p28"/></li>
</ul>


<p><xsl:value-of select="//lang_blocks/p29"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p29b"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29c"/></p>
<p><xsl:value-of select="//lang_blocks/p30"/></p>
<p><span class="style3"><xsl:value-of select="//lang_blocks/p23a"/></span><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p31"/></p>
<p><xsl:value-of select="//lang_blocks/p32"/><xsl:text> </xsl:text><u>https://www.clubshop.com/cgi/appx.cgi/upg?alias=<span class="style3">IDNUMBER</span></u></p>
<p><xsl:value-of select="//lang_blocks/p33"/></p>
<p><xsl:value-of select="//lang_blocks/p34"/></p>
<p><xsl:value-of select="//lang_blocks/p35"/></p>
<p><xsl:value-of select="//lang_blocks/p36"/></p>

<span class="style3"><xsl:value-of select="//lang_blocks/p37"/></span><br/>

                
           </div>
		</div>
	</div>
</div>





<!--<p class="style24"><xsl:value-of select="//lang_blocks/p35"/></p>
<p class="style24"><xsl:value-of select="//lang_blocks/p36"/></p>
<p><xsl:value-of select="//lang_blocks/p37"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>

<p><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><b><xsl:copy-of select="//lang_blocks/p6a"/></b></p>
<p><xsl:value-of select="//lang_blocks/p38"/></p>

</td></tr></table>
<br/>
<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p39"/></li></ol>
</ul>


<p><xsl:value-of select="//lang_blocks/p40"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="//www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p6a"/></b></p>
<p><xsl:value-of select="//lang_blocks/p41"/></p>

</td></tr></table>
<br/>
<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p42"/></li></ol>

</ul>
<p><xsl:value-of select="//lang_blocks/p43"/></p>


<p class="style24"><xsl:value-of select="//lang_blocks/p44"/></p>
<p><xsl:value-of select="//lang_blocks/p45"/></p>




<p class="style24"><xsl:value-of select="//lang_blocks/p59"/></p>

<p><xsl:value-of select="//lang_blocks/p60"/></p>

<p><xsl:value-of select="//lang_blocks/p61"/></p>

<p><xsl:value-of select="//lang_blocks/p62"/></p>
	
		<ul>
		<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p63"/>
		<p><xsl:value-of select="//lang_blocks/p63b"/></p>
		<p><xsl:value-of select="//lang_blocks/p63c"/></p>
		
		</li>
		
	<li class="blue_bullet"><xsl:value-of select="//lang_blocks/p64"/></li>
		
		</ul>
	
	<p><xsl:value-of select="//lang_blocks/p65"/></p>

		













<p><xsl:value-of select="//lang_blocks/p46"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="//www.clubshop.com/vip/ebusiness/images/icons/icon_green_cell_phone.png" alt="Cell Phone" height="131" width="106" align="left"/><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p6a"/></b></p>
<p><xsl:value-of select="//lang_blocks/p47"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47a"/></p>

</td></tr></table>
<br/>

<ol>
<li class="half"><xsl:value-of select="//lang_blocks/p48"/></li>
<li class="half"><xsl:value-of select="//lang_blocks/p49"/></li>
<li class="style3 half"><xsl:value-of select="//lang_blocks/p50"/></li>
</ol>

</ul>


<p class="style24"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="//www.clubshop.com/vip/ebusiness/images/icons/icon_email.png"/>
<xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p6a"/></b></p>
<p><xsl:value-of select="//lang_blocks/p51"/></p>

<p><xsl:value-of select="//lang_blocks/p52"/><xsl:text> </xsl:text><span class="style3"><xsl:value-of select="//lang_blocks/p52a"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p52b"/><xsl:text> </xsl:text><span class="style3">LINK</span></p>
<p><xsl:value-of select="//lang_blocks/p53"/></p>
<p><xsl:value-of select="//lang_blocks/p55"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>
</ul>




<ol><li class="style3 half"><xsl:value-of select="//lang_blocks/p57"/></li></ol>


<p><xsl:value-of select="//lang_blocks/p58"/></p>-->
<hr/>





     
       
         
 






<div align="center"> <a href="lesson_7.xml"><xsl:value-of select="//lang_blocks/previous"/></a><xsl:text> </xsl:text>|<xsl:text> </xsl:text><a href="lesson_9.xml"><xsl:value-of select="//lang_blocks/next"/></a></div>


<br/><br/><br/>
</div>
</div>
</div>

<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
          <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
