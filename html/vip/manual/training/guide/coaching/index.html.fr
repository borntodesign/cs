﻿<!DOCTYPE HTML>



<html>



<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    

<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    

<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->

<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->

<!-- [if gt IE 8]> <!-- --> 

 

<html xmlns="http://www.w3.org/1999/xhtml">

 

<!--<![endif]-->

<script type="text/javascript" src="/cgi/PSA/script/9"><!-- If the Partner does not have a high enough subscription, the javascript is going to redirect them --></script>
<title>ClubShop Rewards Partner Manual</title>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />

<!--  <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/foundation/app.css" /> -->

<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />

<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

<script src="/js/foundation/foundation.js"></script>

<!--  <script src="http://www.clubshop.com/js/foundation/app.js"></script> -->

<script src="/js/foundation/flash.js"></script>

<script src="/js/foundation/jquery.reveal.js"></script>

<script src="/js/panel.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/transpose.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/js/Currency.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/cgi/transposeMEvarsJS.cgi"></script>

<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style>



div.b{

/*	display: none; */

	font-size: 75%;

	background-color: #DFEAFF;

	padding: 3px 6px;

	border: 1px solid silver;

	margin-top: 2px;

	width: 50%;

}

.toggleLink {
font-style: italic;
font-size: 10px;
}

</style>

<style>
.special{
font-family: 'Nunito', sans-serif; 
font-size: 16px;}

.special2{
font-family: 'Nunito', sans-serif; 
font-size: 14px;}
.column1{
background: rgb(136,191,232);
background: -moz-linear-gradient(top,  rgb(136,191,232) 0%, rgb(112,176,224) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(136,191,232)), color-stop(100%,rgb(112,176,224)));
background: -webkit-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: -o-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: -ms-linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
background: linear-gradient(top,  rgb(136,191,232) 0%,rgb(112,176,224) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#88bfe8', endColorstr='#70b0e0',GradientType=0 );

}

.column2 {
background: rgb(20,106,181);
background: -moz-linear-gradient(top,  rgb(20,106,181) 0%, rgb(14,103,181) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(20,106,181)), color-stop(100%,rgb(14,103,181)));
background: -webkit-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: -o-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: -ms-linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
background: linear-gradient(top,  rgb(20,106,181) 0%,rgb(14,103,181) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#146ab5', endColorstr='#0e67b5',GradientType=0 );

}
.white {color: #F0F0F0;}

#translate-this .translate-this-button {
   background-image: url(http://www.clubshop.com/images/translator.jpg) !important;
   width: 180px !important;
   height: 20px !important;
   margin-top: 10px !important;                   
}
.lblue {color:#039;}
.capital {text-transform: uppercase;}
</style>

</head>

<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84" src="https://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><!-- Begin TranslateThis Button -->
<div id="translate-this"><a class="translate-this-button" href="http://www.translatecompany.com/" style="width: 180px; height: 18px; display: block;">Traduction faite par la compagnie</a></div>
<script src="http://x.translateth.is/translate-this.js" type="text/javascript"></script>
<script type="text/javascript">// <![CDATA[
TranslateThis();
// ]]></script>
<!-- End TranslateThis Button --><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">COURS ENTRAÎNEMENT / COACHING DE PARTNER</span></div>
</div>
<div class="row">
<div class="twelve columns">
<hr />
</div>
</div>
<div class="row">
<div class="twelve columns">
<ul>
<li><span class="style24">Leçon 1</span>- <a href="lesson_1.html" target="_blank">APERÇU</a></li>
<br />
<li><span class="style24">Leçon 2</span>- <a href="lesson_2.html" target="_blank">COACHING DDES NOUVEAUX PARTNERS &amp; DES PARTNERS RÉACTIVÉS</a></li>
<br />
<li><span class="style24">Leçon 3</span>- <a href="lesson_3.html" target="_blank">COMMENT CONSTRUIRE VOTRE GROUPE PARTNER</a></li>
<br />
<li><span class="style24">Lesson 4</span>- <a href="lesson_4.html" target="_blank">UTILISATION DE VOTRE GPS EN TANT QU'ENTRAÎNEUR</a></li>
<br /> 
</ul>
<hr />
<p><a href="http://www.clubshop.com/manual/"><img alt="Manual Home Page" height="64" src="https://www.clubshop.com/images/icons/icon_home.png" width="64" /></a></p>
<div align="center"><a href="lesson_1.html">Leçon 1</a></div>
<p> </p>
</div>
</div>
</div>
<p><br /><br /><br /><br /><br /><br /> <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></p>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p style="text-align:center;">Copyright © 1997-  
<!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>


Proprofit Worldwide Ltd., Tous droits réservés.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
<script type="text/javascript">// <![CDATA[
    $(document).ready(function() {

        Transpose.transpose_currency();

    });
// ]]></script>
</body>
</html>