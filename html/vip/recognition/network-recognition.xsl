<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href="/home/clubshop.com/html/global-xml/roles/roles.xsl" />

	<xsl:template match="/">
		<html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>New Management Achievements</title>
		<style type="text/css">
		body { font:normal 90% Arial,Helvetica,sans-serif; }
		tr.header { background-color: #ff0; }
		table#main { margin: 0 auto; border-collapse:collapse; }
		th {padding: 0.3em 0.5em; border:1px solid #eee; font-size:90%; }
		td {padding: 0.3em 0; font-size:90%; }
		tr.rowf td {
			border-top: 1px solid #eaeaea;
			padding-top: 1em;
		}
		td.country { text-align: center; }
		td.level,td.name { font-weight: bold; }
		tr.rowf td.level {
			text-align:center;
			padding: 0 0.4em 0.3em 0.2em;
		}

		div.bg, div.bgl { padding: 0.3em 0.5em; }
		div.r5 { background-color: #ffd500; }	/* bronze mgr. */
		div.r6 { background-color: #eaeaea; }
		div.r7 { background-color: #ffffcc; }
		div.r8 { background-color: #f66; }		/* ruby dir */
		div.r9 { background-color: #66ff66; }
		div.r10 { background-color: #ddeaff; }	/* diamond dir */
		div.r11 { background-color: #fff; }		/* Exec  */
		</style>
		</head><body>
			<div style="text-align:center;">

			<p style="color:#333399; font-style:italic; font-weight:bold;">The Partners listed below have achieved a new Management Promotion this month.</p></div>

			<table id="main">
			<tr class="header"><th>Level</th><th>Name</th><th>Country</th>
				<th>
				Executive Director</th>
			</tr>
			<xsl:for-each select="//graduates/*">
			<xsl:sort select="@a_level" order="descending" data-type="number" />
			<tr>
				<xsl:if test = ". = //graduates/vip[@a_level = current()/@a_level][1]">
				<xsl:attribute name="class">rowf</xsl:attribute>
				<td class="level"><xsl:attribute name="rowspan">
						<xsl:value-of select="count(//graduates/vip[@a_level = current()/@a_level])" />
				    </xsl:attribute>
				    <xsl:call-template name="gen_level_cell">
				       	<xsl:with-param name="level"><xsl:value-of select="@a_level" /></xsl:with-param>
				    </xsl:call-template>
				</td>
			</xsl:if>

			<td class="name"><div><xsl:attribute name="class">bg r<xsl:value-of select="@a_level" /></xsl:attribute>
				<a><xsl:attribute name="href"><xsl:value-of select="pwl" /></xsl:attribute><xsl:value-of select="name" /></a>
				</div></td>
			<td class="country"><div><xsl:attribute name="class">bg r<xsl:value-of select="@a_level" /></xsl:attribute>
				<xsl:value-of select="country" /></div></td>

			<td class="chief"><div><xsl:attribute name="class">bg r<xsl:value-of select="@a_level" /></xsl:attribute>
				<xsl:value-of select="//upline/leader[@id = current()/@chief]" /></div></td>
			</tr>
			</xsl:for-each>
			</table>
			
			<p style="font-size:80%; margin-top:2em;">Last updated: <xsl:value-of select="//last_updated" /> PST</p>
		</body></html>
			
	</xsl:template>
	
	
	<xsl:template name="gen_level_cell">
	<xsl:param name="level" />
	<div><xsl:attribute name="class">bgl</xsl:attribute>
	     <xsl:value-of select="document('/home/clubshop.com/html/global-xml/level_values.xml')//lang_blocks/level[@value = $level]" /></div>
	     
	     <xsl:copy-of select="document('network-recognition-img-list.xml')/img_list/img[@id = concat('img', $level)]" />
	     
	</xsl:template>

</xsl:stylesheet>
