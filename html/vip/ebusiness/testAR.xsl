<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>
<script type="text/javascript" src="/js/gi/blurb.js"></script>
<script type="text/javascript" src="/js/gi/banner.js"></script>
<link rel="stylesheet" type="text/css" href="/css/gi_container.css" />

<link href="https://www.glocalincome.com/css/cgi/main.css" rel="stylesheet" type="text/css" />

<style type="text/css">
table.a {border: thin solid #999966;
}
table.b {text-align: center;}

td {font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: .85em;
border: 1px solid #999966;
}
tr.top{
	background-color: #FFFFCC;

}
tr.a {
	background-color: #FFFFFF;
}
tr.b {
	background-color: #F7F7F2;
}
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;}

</style>

</head>
<body>
<!-- the following line must remain on -one- line or IE creates gaps between the images -->
<div style="width:975px; margin-right:auto; margin-left:auto; margin-bottom:1em;"><img src="https://www.glocalincome.com/images/header/glocal_income_interface_header.png" width="975" height="254" alt=""/><div style="background-image: url(https://www.glocalincome.com/images/headbar/glocal_income_interface_plain.png); height:52px; text-align:center;"><span style="line-height:1.9; color:white; font-weight: bold; font-size: 17px; font-family: arial,helvetica,sans-serif;"><xsl:value-of select="//lang_blocks/p0"/></span></div><div style="background-image: url(https://www.glocalincome.com/images/bg/glocal_income_interface_04.png)">

<div style="width:850px; margin-right:auto; margin-left:auto;">



<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999" align="center">

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p1"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p2"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p3"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p4"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p5"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p6"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p7"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>

<!--<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p8"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>-->


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p9"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>

</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p10"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>

</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p11"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p12"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>

<!--<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p13"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/md"/></td>
</tr>-->

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p14"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/tn"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p15"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/tn"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p16"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/tn"/></td>
</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p17"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p18"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p19"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p20"/></td>
<td bgcolor="#FFFFFF"></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p21"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>

</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p22"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>


</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p23"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>

</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p24"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/pd"/></td>

</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p25"/></td>
<td bgcolor="#FFFFFF"></td>

</tr>



<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p26"/></td>
<td bgcolor="#FFFFFF"></td>

</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p27"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>

</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p28"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/st"/></td>

</tr>


<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p29"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p30"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p31"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/bi"/>,<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/bu"/></td>
</tr>

<tr>
<td bgcolor="#FFFFFF">Jump Start Request</td>
<td bgcolor="#FFFFFF">BU</td>
</tr>

</table>
</div>

</div>
<img src="https://www.glocalincome.com/images/glocal_income_interface_08.png" width="975" height="60" alt="" />
</div>

<!--<p align="center" class="Text_Copy_Right_Green">&copy; 2010 Glocal Income</p>-->


</body>
</html>

</xsl:template>

</xsl:stylesheet>
