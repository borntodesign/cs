<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p,td { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	
li{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;}

p.small{  	font-family: Verdana, Arial, sans-serif;
font-size: 60%;}
</style>
</head>

<body>
<div><img src="/vip/ebusiness/images/ebiz_header_mkt.gif" width="702" height="93" alt="" /></div>

<h5><xsl:value-of select="//lang_blocks/p1" /></h5>
<sub><xsl:value-of select="//lang_blocks/p2" /></sub>

<hr />
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><b><xsl:value-of select="//lang_blocks/p5" /></b></p>

<table size="100%" border="1" cellspacing="5">
<tr>
<td><b><xsl:value-of select="//lang_blocks/p6" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p7" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p8" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p9" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p10" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p12" /></b></td>

</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p13" /></td>
<td><xsl:value-of select="//lang_blocks/p14" /></td>
<td><xsl:value-of select="//lang_blocks/p15" /></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td><xsl:value-of select="//lang_blocks/p17" /></td>
<td><xsl:value-of select="//lang_blocks/p18" /></td>
<td><xsl:value-of select="//lang_blocks/p19" /></td>

</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><b><xsl:value-of select="//lang_blocks/p21" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22" /> [<a onClick="" href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/presentation.xml" target="_blank">Link</a>]</p>

<p><b><xsl:value-of select="//lang_blocks/p23" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24" /></p>

<p><b><xsl:value-of select="//lang_blocks/p25" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26" /></p>
<table><tr><td>
<xsl:value-of select="//lang_blocks/p27" /></td>
<td><div align="right"><img src="http://www.clubshop.com/vip/ebusiness/images/boattied.gif" width="200" height="123" alt="boat tied"/></div></td>
</tr></table>
<table><tr><td><img src="http://www.clubshop.com/vip/ebusiness/images/monkeysfist.gif" width="124" height="141" alt="monkeys fist"/></td>
<td><xsl:value-of select="//lang_blocks/p28" /></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p29" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p30" /></li>
<li><xsl:value-of select="//lang_blocks/p31" /></li>
<li><xsl:value-of select="//lang_blocks/p32" /></li>
</ul>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/keypeople.gif" width="110" height="74" alt="Key People"/> <xsl:text> </xsl:text><b> <xsl:value-of select="//lang_blocks/p33" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34" /></p>

<p><xsl:value-of select="//lang_blocks/p35" /></p>
<p><xsl:value-of select="//lang_blocks/p36" /></p>
<p><xsl:value-of select="//lang_blocks/p37" /></p>

<p><b><xsl:value-of select="//lang_blocks/p38" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39" /></p>
<p><b><xsl:value-of select="//lang_blocks/p40" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41" /></p>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/sale.gif" width="73" height="110" alt="sale"/> <xsl:text> </xsl:text><b> <xsl:value-of select="//lang_blocks/p42" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p43" /></p>
<p><xsl:value-of select="//lang_blocks/p44" /></p>

<p><xsl:value-of select="//lang_blocks/p45" /></p>

<p><b><xsl:value-of select="//lang_blocks/p50" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51" /></p>
<p><xsl:value-of select="//lang_blocks/p52" /></p>
<p><xsl:value-of select="//lang_blocks/p53" /></p>
<p><xsl:value-of select="//lang_blocks/p54" /></p>
<p><xsl:value-of select="//lang_blocks/p55" /></p>
<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p><a href="http://www.clubshop.com/outletsp/bcenterbucks.html" target="_blank"><xsl:value-of select="//lang_blocks/p57" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p58" /></p>
<hr/>

<p style="text-align: center">
            <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/ag-index.xml">
               <xsl:value-of select="//lang_blocks/toc" />
            </a>

            

            </p>

            <br />

            <p style="text-align: right" class="small">
  Copyright @ ClubShop.com 1997-2010 
  <br /> 
  All Rights Reserved.  
<!--  <img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/>
  
  --> 
  </p>



















         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>