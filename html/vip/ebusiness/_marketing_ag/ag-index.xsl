<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	
li{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;}
p.small{  	font-family: Verdana, Arial, sans-serif;
font-size: 60%;}
  	


</style>
</head>

<body>
<div><img src="/vip/ebusiness/images/ebiz_header_mkt.gif" width="702" height="93" alt="" /></div>

<h5><xsl:value-of select="//lang_blocks/p1" /></h5>
<img src="https://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" width="468" height="60" alt="" />
<p style="text-align: right"><i><xsl:value-of select="//lang_blocks/p2" /></i></p>

<hr />
<sub><xsl:value-of select="//lang_blocks/p3" /></sub>
<p><xsl:value-of select="//lang_blocks/p4" /> "<xsl:value-of select="//lang_blocks/p5" />"
<xsl:value-of select="//lang_blocks/p6" /></p>
<p><xsl:value-of select="//lang_blocks/p7" /> "<xsl:value-of select="//lang_blocks/p8" />" 
<xsl:value-of select="//lang_blocks/p9" /> (<xsl:value-of select="//lang_blocks/p10" />) <xsl:value-of select="//lang_blocks/p11" />:</p>

<ol>
<li><xsl:value-of select="//lang_blocks/p12" /> </li>
<li><xsl:value-of select="//lang_blocks/p13" /></li>
<li><xsl:value-of select="//lang_blocks/p14" /> "<xsl:value-of select="//lang_blocks/p15" />".</li>
</ol>


<hr/>

<sub><xsl:value-of select="//lang_blocks/p16" />:</sub>

<ol>
<li><xsl:value-of select="//lang_blocks/p17" /></li>
<li><xsl:value-of select="//lang_blocks/p18" /></li>
<li><xsl:value-of select="//lang_blocks/p19" /></li>

<li><xsl:value-of select="//lang_blocks/p20" /></li>
<li><xsl:value-of select="//lang_blocks/p25" /></li>

<li><xsl:value-of select="//lang_blocks/p26" /></li>



</ol>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p29" /></sub>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /> "<xsl:value-of select="//lang_blocks/p32" />" <xsl:value-of select="//lang_blocks/p33" />&#40;<xsl:value-of select="//lang_blocks/p34" />&#41; <xsl:value-of select="//lang_blocks/p35" />&#40;<xsl:value-of select="//lang_blocks/p34" />&#41; <xsl:value-of select="//lang_blocks/p36" />&#40;<xsl:value-of select="//lang_blocks/p34" />&#41;
<xsl:value-of select="//lang_blocks/p37" />&#40;<xsl:value-of select="//lang_blocks/p34" />&#41;<xsl:value-of select="//lang_blocks/p38" />&#40;<xsl:value-of select="//lang_blocks/p34" />&#41;<xsl:value-of select="//lang_blocks/p39" />

</p>
<p><xsl:value-of select="//lang_blocks/p40" /></p>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p41" />:</sub>
<ol>
<li><xsl:value-of select="//lang_blocks/p42" /></li>
<li><xsl:value-of select="//lang_blocks/p43" /></li>
<li><xsl:value-of select="//lang_blocks/p44" /></li>


</ol>



<hr/>

<p style="text-align: center">
 <a href="http://www.clubshop.com/vip/ebusiness/index.html"><xsl:value-of select="//lang_blocks/toc" /></a>| <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/lesson_1.xml"><xsl:value-of select="//lang_blocks/lesson1" /></a></p>
<p style="text-align: right" class="small">Copyright @ ClubShop.com 1997-2010<br/> All Rights Reserved.<!--<img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/>--></p>
                        
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>