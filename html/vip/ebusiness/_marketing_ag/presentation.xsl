<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>


   <xsl:template match="/">
    
         <html><head>
            <title>The ClubShop Affinity Group Program</title>
<link href="presentation.css" rel="stylesheet" type="text/css" media="print"/>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	
li{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;}
  	
table.e{align:center;
        text-indent: 2em;
        font-family: Verdana, Arial, sans-serif;
  	    font-size:100%;
  	    cellpadding:5;
        border: thin double;
        width: 80%;
        text align: center;
  	}


</style>

</head>
<body>

<div align="center"><h5><xsl:value-of select="//lang_blocks/head"/></h5></div>

<p><xsl:value-of select="//lang_blocks/p1"/></p>

<p><xsl:value-of select="//lang_blocks/p2"/><b><xsl:value-of select="//lang_blocks/p3"/></b></p>

<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<div align="center"><table class="e">
<tr><td>
<p><xsl:value-of select="//lang_blocks/p6"/></p>

<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>

</td></tr>
</table></div>

<div align="center"><h3><xsl:value-of select="//lang_blocks/p9"/></h3></div>

<p><b><xsl:value-of select="//lang_blocks/p10"/></b><br/>

<xsl:value-of select="//lang_blocks/p11"/></p>

<p><b><xsl:value-of select="//lang_blocks/p12"/></b><br/>

<xsl:value-of select="//lang_blocks/p13"/></p>

<p><b><xsl:value-of select="//lang_blocks/p14"/></b><br/>

<xsl:value-of select="//lang_blocks/p15"/></p>

<p><b><xsl:value-of select="//lang_blocks/p16"/></b><br/>

<xsl:value-of select="//lang_blocks/p17"/></p>
<p><b><xsl:value-of select="//lang_blocks/p18"/></b><br/>

<xsl:value-of select="//lang_blocks/p19"/></p>
<br/>

<div align="center"><h3><i><xsl:value-of select="//lang_blocks/p20"/></i></h3></div>

</body></html>


</xsl:template>
</xsl:stylesheet>