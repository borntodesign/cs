<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body{
    font-size: 100%;
    background-color: white;
    color: black;
}

h5 {
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
p,li,td { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
    font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    color: #060;
    font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    }
    p.small{  	font-family: Verdana, Arial, sans-serif;
font-size: 60%;}


</style>
         </head>

         <body>
            <div>
               <img src="../images/ebiz_header_mkt.gif" width="702" height="93" alt="" />
            </div>

            <h5>
               <xsl:value-of select="//lang_blocks/p1" />
               
            </h5>

            
               <sub>
                  <xsl:value-of select="//lang_blocks/p2" />
               </sub><br/>
               <p><xsl:value-of select="//lang_blocks/p2a" /></p>
            
<hr/>
<h4><xsl:value-of select="//lang_blocks/p3" /></h4>
<p><xsl:value-of select="//lang_blocks/p4" /><xsl:text> </xsl:text> <a href="https://www.clubshop.com/cgi/tl-redir" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p5" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p6" /></p>

<p><b><xsl:value-of select="//lang_blocks/p7" />:</b></p>



<p><b><xsl:value-of select="//lang_blocks/p8" />:</b><br/> <xsl:value-of select="//lang_blocks/p9" /> '<xsl:value-of select="//lang_blocks/p10" />' <xsl:value-of select="//lang_blocks/p11" /></p>

<p><b><xsl:value-of select="//lang_blocks/p12" />:</b><br/> <xsl:value-of select="//lang_blocks/p13" /></p>
<table width="500"><tr>
<td align="center"><a href="http://www.google.com"><xsl:value-of select="//lang_blocks/p14" /></a></td>
<td align="center"><a href="http://www.yahoo.com"><xsl:value-of select="//lang_blocks/p15" /></a></td>
<td align="center"><a href="http://www.lycos.com"><xsl:value-of select="//lang_blocks/p16" /></a></td>

</tr></table>
<p><xsl:value-of select="//lang_blocks/p17" /></p>
<p><b><xsl:value-of select="//lang_blocks/p18" />:</b><br/>
<xsl:value-of select="//lang_blocks/p19" /> (<a href="http://www.clubshop.com/vip/ebusiness/_marketing_ba/example1.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p20" /></a>)
<xsl:value-of select="//lang_blocks/p21" /> "<xsl:value-of select="//lang_blocks/p22" />" <xsl:value-of select="//lang_blocks/p23" /></p>

<p><b><xsl:value-of select="//lang_blocks/p24" />:</b><br/>
<xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text> <a href="https://www.clubshop.com/cgi/tl-redir" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p26" /></a><xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p27" /> </p>

<p><b><xsl:value-of select="//lang_blocks/p28" />:</b><xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p29" />
"<xsl:value-of select="//lang_blocks/p30" />" <xsl:value-of select="//lang_blocks/p31" /> "<xsl:value-of select="//lang_blocks/p32" />", <xsl:value-of select="//lang_blocks/p33" />

</p>

<p><b><xsl:value-of select="//lang_blocks/p34" />:</b><br/>
<xsl:value-of select="//lang_blocks/p35" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p36" />)
<xsl:value-of select="//lang_blocks/p37" />
"<xsl:value-of select="//lang_blocks/p38" />"
</p>

<p><xsl:value-of select="//lang_blocks/p39" /></p>

<p><b><xsl:value-of select="//lang_blocks/p40" />: </b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p41" /></p>

<p><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p42" />:</b></font><xsl:text> </xsl:text> 
<xsl:value-of select="//lang_blocks/p43" /><xsl:text> </xsl:text>
<a href="http://www.clubshop.com/manual/policies/spam.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p44" /></a> </p>

<p><b><xsl:value-of select="//lang_blocks/p45" />:</b><br/>

<xsl:value-of select="//lang_blocks/p46" />
"<xsl:value-of select="//lang_blocks/p47" />" "<xsl:value-of select="//lang_blocks/p48" />"
<xsl:value-of select="//lang_blocks/p49" /><br/>
</p>

<p><xsl:value-of select="//lang_blocks/p50" /> "<xsl:value-of select="//lang_blocks/p51" />"
<xsl:value-of select="//lang_blocks/p52" /> </p>
<p><b><xsl:value-of select="//lang_blocks/p52a" /> <br/><a href="https://www.clubshop.com/cgi/co_donate.cgi" target="_blank">https://www.clubshop.com/cgi/co_donate.cgi</a></b></p>

<p><b><xsl:value-of select="//lang_blocks/p53" /></b></p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p54" /></sub>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/yellowpages.gif"/> <xsl:value-of select="//lang_blocks/p55" /></p>


<hr/>


<sub><xsl:value-of select="//lang_blocks/p56" /></sub>
<p><xsl:value-of select="//lang_blocks/p57" />:</p>
<ol>
<li><xsl:value-of select="//lang_blocks/p58" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p59" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p60" /></li><br/>
</ol>

<p><b><xsl:value-of select="//lang_blocks/p40" />:</b><xsl:text> </xsl:text>

<xsl:value-of select="//lang_blocks/p61" />

</p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p62" /></sub>

<p><xsl:value-of select="//lang_blocks/p63" />:<br/><br/>
<b>http://www.clubshop.com/cgi-bin/rd/10,,refid=YOURIDNUMBER/nonprofit.html </b>
</p>
<p><xsl:value-of select="//lang_blocks/p64" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p65" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p66" /></p>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p67" /></sub>

<p><xsl:value-of select="//lang_blocks/p68" />"<xsl:value-of select="//lang_blocks/p69" />",<xsl:value-of select="//lang_blocks/p70" /> </p>

<p><xsl:value-of select="//lang_blocks/p71" /></p>

<p><xsl:value-of select="//lang_blocks/p72" /></p>

<p><xsl:value-of select="//lang_blocks/p73" /></p>

<sub><xsl:value-of select="//lang_blocks/p74" /></sub>


<p><b><xsl:value-of select="//lang_blocks/p8" />: </b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p75" /><xsl:text> </xsl:text> 
"<xsl:value-of select="//lang_blocks/p10" />"</p>


<p><b><xsl:value-of select="//lang_blocks/p12" />: </b><xsl:value-of select="//lang_blocks/p76" />:</p>

<table border="1" width="700">
<tr><td><b><xsl:value-of select="//lang_blocks/p78" /></b></td><td><xsl:value-of select="//lang_blocks/p80" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p81" /></font>) <xsl:value-of select="//lang_blocks/p82" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:value-of select="//lang_blocks/p84" />? (<xsl:value-of select="//lang_blocks/p85" />)</td></tr>

<tr><td><b><xsl:value-of select="//lang_blocks/p79" /></b></td>
<td>"<xsl:value-of select="//lang_blocks/p86" />"</td></tr>



<tr><td><b><xsl:value-of select="//lang_blocks/p78" /></b> </td><td><xsl:value-of select="//lang_blocks/p87" />  (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p88" /></font>) <xsl:value-of select="//lang_blocks/p89" /> (<font color="#CC3300"> <xsl:value-of select="//lang_blocks/p90" /></font>) <xsl:value-of select="//lang_blocks/p91" /> " <xsl:value-of select="//lang_blocks/p92" />" (<xsl:value-of select="//lang_blocks/p93" />) 
</td></tr>

</table>
<table border="1" width="700">
<tr><td colspan="2"><b><xsl:value-of select="//lang_blocks/p94a" />?</b></td></tr>

<tr><td><b><xsl:value-of select="//lang_blocks/p79" /></b> </td>
<td><b><xsl:value-of select="//lang_blocks/p78" /></b></td></tr>
<tr><td><b>"<xsl:value-of select="//lang_blocks/p95" />"</b></td>
<td><xsl:value-of select="//lang_blocks/p96" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p97" /></td>

</tr>

<tr><td><b>"<xsl:value-of select="//lang_blocks/p98" />"</b></td>

<td>"<xsl:value-of select="//lang_blocks/p99" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>)<xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p97" /></td>
</tr>
<tr><td colspan="2"><b><xsl:value-of select="//lang_blocks/p100a" />?</b></td></tr>

<tr>
<td><b>"<xsl:value-of select="//lang_blocks/p101" />"</b></td>
<td>"<xsl:value-of select="//lang_blocks/p102" />" <xsl:value-of select="//lang_blocks/p103" /></td>
</tr>



<tr>
<td><b>"<xsl:value-of select="//lang_blocks/p105" />"</b></td>
<td>"<xsl:value-of select="//lang_blocks/p104" />! <xsl:value-of select="//lang_blocks/p106" />?"<xsl:text> </xsl:text>  (<xsl:value-of select="//lang_blocks/p107" />) <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p108" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p88" /></font>) <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p109" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p110" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p111" /> 
(<xsl:value-of select="//lang_blocks/p112" />),<xsl:value-of select="//lang_blocks/p113" /> <xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p114" /><xsl:text> </xsl:text>  (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" />)</font> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p115" />!"


</td>
</tr>
<tr><td colspan="2"><b><xsl:value-of select="//lang_blocks/p116" />?</b></td></tr>

<tr><td><b>"<xsl:value-of select="//lang_blocks/p105" />"</b></td>

<td>"<xsl:value-of select="//lang_blocks/p117" />" <br/><xsl:value-of select="//lang_blocks/p118" /></td>
</tr>

<tr>
<td><b>"<xsl:value-of select="//lang_blocks/p101" />"</b></td>
<td>"<xsl:value-of select="//lang_blocks/p119" />?"<br/><br/>
<xsl:value-of select="//lang_blocks/p108" /> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p88" /></font>) <xsl:value-of select="//lang_blocks/p109" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p110" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p111" /> 
(<xsl:value-of select="//lang_blocks/p112" />),<xsl:value-of select="//lang_blocks/p113" /> <xsl:value-of select="//lang_blocks/p114" /><xsl:text> </xsl:text> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" />)</font> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p115" />!"
</td>
</tr>
</table>

<p><b><xsl:value-of select="//lang_blocks/p18" />: <xsl:value-of select="//lang_blocks/p120" /></b></p>

<p><b><xsl:value-of select="//lang_blocks/p121" /></b></p>
<p><xsl:value-of select="//lang_blocks/p122" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p123" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p124" /> </p>
<p><xsl:value-of select="//lang_blocks/p125" /><xsl:text> </xsl:text><font color="#CC3300"><xsl:value-of select="//lang_blocks/p126" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p127" /><br/>
http://www.clubshop.com/cgi-bin/rd/10,,refid=<b>1234567</b>/nonprofit.html </p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p128" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p129" /></p>

<p><xsl:value-of select="//lang_blocks/p130" /><xsl:text> </xsl:text><font color="#CC3300"><xsl:value-of select="//lang_blocks/p131" /></font>,</p>
<p><xsl:value-of select="//lang_blocks/p132" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>).</p>
<p><xsl:value-of select="//lang_blocks/p133" /><xsl:text> </xsl:text><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p135" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) 
<xsl:value-of select="//lang_blocks/p136" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>)<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p137" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) </p>

<p><img src="https://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" height="60" width="468" alt="ClubShop.com"/></p>

<p><xsl:value-of select="//lang_blocks/p138" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p139" /></p>
<p><xsl:value-of select="//lang_blocks/p140" /><xsl:text> </xsl:text><b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<b>YOURIDNUMBER</b>/nonprofit.html </b></p>
<p><xsl:value-of select="//lang_blocks/p141" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p142" /></font>)</p>

<p><xsl:value-of select="//lang_blocks/p143" />,<br/>
<font color="#CC3300"><xsl:value-of select="//lang_blocks/p144" /></font><br/>
<font color="#CC3300"><xsl:value-of select="//lang_blocks/p145" /></font></p>

<p><xsl:value-of select="//lang_blocks/p146" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="><xsl:value-of select="//lang_blocks/p126" /></a><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p147" /><xsl:text> </xsl:text><b> "<xsl:value-of select="//lang_blocks/p148" />"</b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p149" />
</p>

<p><b><font color="#CC3300"><xsl:value-of select="//lang_blocks/p150" /></font></b></p>
<p>------------------------------------------------------------------------</p>

<p><b><xsl:value-of select="//lang_blocks/p24" />:</b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p151" /> <xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p10" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p152" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p153" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p154" /></p>

<p><b><xsl:value-of select="//lang_blocks/p34" />:</b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p155" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p156" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p157" /></p>

<p><xsl:value-of select="//lang_blocks/p158" /></p>

<hr/>
<sub><b><xsl:value-of select="//lang_blocks/p159" /></b></sub>
<p><xsl:value-of select="//lang_blocks/p160" /></p>

<p><b><xsl:value-of select="//lang_blocks/p8" />:</b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p161" /> <xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p162" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p163" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p164" />)<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p165" /></p>
<p><b><xsl:value-of select="//lang_blocks/p12" />:</b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p166" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p167" />"<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p168" /></p>
<p><b><xsl:value-of select="//lang_blocks/p18" />:</b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p169" /><xsl:text> </xsl:text>( <xsl:value-of select="//lang_blocks/p170" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p171" /></b><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p172" />),<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p173" /></p>

<p><xsl:value-of select="//lang_blocks/p174" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p175" /></p>
<p align="right"><b><xsl:value-of select="//lang_blocks/p176" /></b></p>
<p><b><xsl:value-of select="//lang_blocks/p177" /></b><br/>
<b><xsl:value-of select="//lang_blocks/p178" /></b></p>

<p><xsl:value-of select="//lang_blocks/p179" /></p>
<p><xsl:value-of select="//lang_blocks/p180" /></p>
<p><xsl:value-of select="//lang_blocks/p181" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p81" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p182" /> </p>
<p><xsl:value-of select="//lang_blocks/p183" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p184" /></p>
<p><xsl:value-of select="//lang_blocks/p185" /> </p>
<p><b><xsl:value-of select="//lang_blocks/p186" /></b></p>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" height="60" width="468" alt="ClubShop.com"/></p>

<p><xsl:value-of select="//lang_blocks/p187" /><xsl:text> </xsl:text> (<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p188" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p189" />: 
http://www.clubshop.com/cgi-bin/rd/10,,refid=<b>YOURIDNUMBER</b>/nonprofit.html </p>



<p><xsl:value-of select="//lang_blocks/p190" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p145" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p191" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p142" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p192" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p193" /></p>

<p><xsl:value-of select="//lang_blocks/p143" />,<br/>
<xsl:value-of select="//lang_blocks/p144" /></p>
<hr/>
<sub><b><xsl:value-of select="//lang_blocks/p194" /></b></sub>
<p><xsl:value-of select="//lang_blocks/p195" /></p>
<p><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p42" />:</b></font><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p196" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p197" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p198" />
<xsl:text> </xsl:text> <a href="http://www.clubshop.com/manual/policies/spam.xml" onclick="window.open(this.href); return false;"> <xsl:value-of select="//lang_blocks/p199" /></a></p>

<p></p>
<p><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?action=search" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p200" /></a></p>

<p><xsl:value-of select="//lang_blocks/p201" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p202" /></b></p>

<p><xsl:value-of select="//lang_blocks/p203" /></p>
<p>http://www.clubshop.com/cgi-bin/rd/10,,refid=<b>YOURIDNUMBER</b>/nonprofit.html</p>
<p><b><xsl:value-of select="//lang_blocks/p8" />:</b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p204" /></p>


<p><b><xsl:value-of select="//lang_blocks/p12" />:</b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p205" /></p>

<p><b><xsl:value-of select="//lang_blocks/p206" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p207" /></p>
<p><xsl:value-of select="//lang_blocks/p208" /></p>

<p><xsl:value-of select="//lang_blocks/p209" /></p>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p210" /></sub>
<p><xsl:value-of select="//lang_blocks/p211" /></p>

<p><xsl:value-of select="//lang_blocks/p212" /></p>
<p><xsl:value-of select="//lang_blocks/p213" /><xsl:text> </xsl:text>( <font color="#CC3300"><xsl:value-of select="//lang_blocks/p81" /></font>) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p214" /></p>

<p><xsl:value-of select="//lang_blocks/p215" /><xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) </p>
<p><xsl:value-of select="//lang_blocks/p216" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p83" /></font>) 

<xsl:value-of select="//lang_blocks/p217" /><br/>http://www.clubshop.com/cgi-bin/rd/10,,refid=<b>YOURIDNUMBER</b>/nonprofit.html</p>


<p><xsl:value-of select="//lang_blocks/p218" /> <xsl:text> </xsl:text>(<font color="#CC3300"><xsl:value-of select="//lang_blocks/p142" /></font>)</p>

<p><xsl:value-of select="//lang_blocks/p143" /><br/>
<xsl:value-of select="//lang_blocks/p144" /><br/>
<xsl:value-of select="//lang_blocks/p145" /></p>
<p><xsl:value-of select="//lang_blocks/p219" /><xsl:text> </xsl:text> <a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="><xsl:value-of select="//lang_blocks/p220" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p221" /></p>

<p><font color="#CC3300"><xsl:value-of select="//lang_blocks/p222" /></font></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p24" />:</b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p223" /></p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p224" /></sub>
<p><b><xsl:value-of select="//lang_blocks/p225" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p226" /></p>

<p><b><xsl:value-of select="//lang_blocks/p227" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p228" /></p>

<p><b><xsl:value-of select="//lang_blocks/p229" /></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p230" /></p>


<hr/>

            <p style="text-align: center">
            <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/ag-index.xml">
               <xsl:value-of select="//lang_blocks/toc" />
            </a>

            | 
           <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/lesson_2.xml">
               <xsl:value-of select="//lang_blocks/lesson2" /></a>

            </p>

            <br />

            <p style="text-align: right" class="small">
  Copyright @ ClubShop.com 1997-2010 
  <br /> 
  All Rights Reserved. 
- <!-- <img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/>
  --> 
  </p>

         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

