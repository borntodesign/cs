<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p,td { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	
li{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;}
p.small{  	font-family: Verdana, Arial, sans-serif;
font-size: 60%;}

</style>
</head>

<body>
<div><img src="/vip/ebusiness/images/ebiz_header_mkt.gif" width="702" height="93" alt="affinty group" /></div>

<h5><xsl:value-of select="//lang_blocks/p1" /></h5>
<div align="right"><img src="https://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" width="468" height="60" alt="affinity group" /></div><br/>
<sub><xsl:value-of select="//lang_blocks/p2" /></sub>

<hr />
<sub><xsl:value-of select="//lang_blocks/p3" /></sub>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p5" /></li>
<li><xsl:value-of select="//lang_blocks/p6" /></li>
<li><xsl:value-of select="//lang_blocks/p7" /></li>
<li><xsl:value-of select="//lang_blocks/p8" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p9" /></p>

<p><xsl:value-of select="//lang_blocks/p10" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /></p>

<hr />
<sub><xsl:value-of select="//lang_blocks/p12" /></sub>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>

<p><xsl:value-of select="//lang_blocks/p15" /></p>

<p><xsl:value-of select="//lang_blocks/p16" /></p>
<hr/>
<sub><xsl:value-of select="//lang_blocks/p17" /></sub>
<p><xsl:value-of select="//lang_blocks/p18" /> <a href="https://www.clubshop.com/cgi/tl-redir" target="_blank"><xsl:value-of select="//lang_blocks/p19" /></a><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>

<p><b><xsl:value-of select="//lang_blocks/p22" /></b> <xsl:value-of select="//lang_blocks/p23" /> </p>

<p><b><xsl:value-of select="//lang_blocks/p24" /></b> <xsl:value-of select="//lang_blocks/p25" /></p> 

<table><tr>
<td align="center"><a href="http://www.google.com" target="_blank">www.google.com</a></td>
<td></td><td align="center"><a href="http://www.yahoo.com" target="_blank">www.yahoo.com</a></td>
<td></td><td align="center"><a href="http://www.lycos.com" target="_blank">www.lycos.com</a></td>
</tr></table>
<br/>
<p><xsl:value-of select="//lang_blocks/p26" />
</p>

<p><b><xsl:value-of select="//lang_blocks/p27" /></b> <xsl:value-of select="//lang_blocks/p28" /> <a href="example1.html" target="_blank"><xsl:value-of select="//lang_blocks/p29" /></a><xsl:value-of select="//lang_blocks/p30" /></p>

<p><b><xsl:value-of select="//lang_blocks/p31" /></b> <xsl:value-of select="//lang_blocks/p32" /><a href="https://www.clubshop.com/cgi/tl-redir" target="_blank"><xsl:value-of select="//lang_blocks/p19" /></a> <xsl:value-of select="//lang_blocks/p33" /></p>

<p><b><xsl:value-of select="//lang_blocks/p34" /></b> <xsl:value-of select="//lang_blocks/p35" /></p>

<p><xsl:value-of select="//lang_blocks/p36" /></p>

<p><b><xsl:value-of select="//lang_blocks/p37" /></b> <xsl:value-of select="//lang_blocks/p38" /></p>

<p><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p39" /></b> </font><xsl:value-of select="//lang_blocks/p40" /> <a href="http://www.clubshop.com/manual/policies/spam.xml" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a></p>

<p><b><xsl:value-of select="//lang_blocks/p42" /></b> <xsl:value-of select="//lang_blocks/p43" /></p>
<p><xsl:value-of select="//lang_blocks/p44" /></p>
<p><b><xsl:value-of select="//lang_blocks/p45" /></b></p>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p46" /></sub>
<ul>
<li><xsl:value-of select="//lang_blocks/p47" /></li>
<li><xsl:value-of select="//lang_blocks/p48" /></li>
<li><xsl:value-of select="//lang_blocks/p49" /></li>
</ul>
<p><xsl:value-of select="//lang_blocks/p49a" /></p>
<hr/>
<sub><xsl:value-of select="//lang_blocks/p50" /></sub>
<p><xsl:value-of select="//lang_blocks/p51" /></p>


<ol>
<li><xsl:value-of select="//lang_blocks/p52" /></li>
<li><xsl:value-of select="//lang_blocks/p53" /></li>
<li><xsl:value-of select="//lang_blocks/p54" /></li>
</ol>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p55" /></sub>
<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p><b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">YOURIDNUMBER</font>/affinity/intro.xml</b></p>

<p><xsl:value-of select="//lang_blocks/p57" /><b><xsl:value-of select="//lang_blocks/p58" /></b><xsl:value-of select="//lang_blocks/p59" /></p>


<hr/>
<sub><xsl:value-of select="//lang_blocks/p60" /></sub>
<p><xsl:value-of select="//lang_blocks/p61" /></p>
<p><xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p63" /></p>
<p><xsl:value-of select="//lang_blocks/p64" /></p>
<p><xsl:value-of select="//lang_blocks/p65" /></p>
<p><xsl:value-of select="//lang_blocks/p66" /></p>


<p><b><xsl:value-of select="//lang_blocks/p22" /></b> <xsl:value-of select="//lang_blocks/p67" /></p>

<p><b><xsl:value-of select="//lang_blocks/p24" /></b> <xsl:value-of select="//lang_blocks/p68" /></p>

<sub><b><xsl:value-of select="//lang_blocks/p69" /></b></sub>

<p><b><xsl:value-of select="//lang_blocks/p70" /></b> <xsl:value-of select="//lang_blocks/p71" /></p>
<ul>
<li><font color="0033CC"><b><xsl:value-of select="//lang_blocks/p72" /></b> <xsl:value-of select="//lang_blocks/p73" /></font></li>
</ul>
<p><b><xsl:value-of select="//lang_blocks/p70" /></b> <xsl:value-of select="//lang_blocks/p74" /></p>


<table width="100%" cellpadding="5" border="1"><tr>
<td><font color="0033CC"><b><xsl:value-of select="//lang_blocks/p72" /></b></font></td>
<td><b><xsl:value-of select="//lang_blocks/p70" /></b></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p75" /></td>
<td><xsl:value-of select="//lang_blocks/p76" /></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p77" /></td>
<td><xsl:value-of select="//lang_blocks/p78" /></td>
</tr>

<tr><td colspan="2"><b><xsl:value-of select="//lang_blocks/p79" /></b></td></tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p80" /></td>
<td><xsl:value-of select="//lang_blocks/p81" /></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p82" /></td>
<td><xsl:value-of select="//lang_blocks/p83" /></td>
</tr>

<tr><td colspan="2"><b><xsl:value-of select="//lang_blocks/p84" /></b></td></tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p82" /></td>
<td><xsl:value-of select="//lang_blocks/p85" /></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p80" /></td>
<td><xsl:value-of select="//lang_blocks/p86" /></td>
</tr>

<tr><td colspan="2"><xsl:value-of select="//lang_blocks/p87" /></td></tr>

</table>

<p><b><xsl:value-of select="//lang_blocks/p27" /></b> <xsl:value-of select="//lang_blocks/p88" /></p>




<p><xsl:value-of select="//lang_blocks/p90" /> <b><xsl:value-of select="//lang_blocks/p91" /> </b><xsl:value-of select="//lang_blocks/p92" /><br/><br/><xsl:value-of select="//lang_blocks/p93" /></p>
<p><b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">123456</font>/affinity/intro.xml</b></p>



<table width="650" border="1">
<tr>
<td>
<font color="0033CC"><xsl:value-of select="//lang_blocks/p89" /></font><br/><br/>

<b><xsl:value-of select="//lang_blocks/p94" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p95" /> <font color="#CC3300"><xsl:value-of select="//lang_blocks/p96" /></font><br/><br/>

<xsl:value-of select="//lang_blocks/p97" /><br/>

<xsl:value-of select="//lang_blocks/p98" /><br/><br/>

<xsl:value-of select="//lang_blocks/p99" /><br/><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" width="468" height="60" border="0" alt="affinity banner"/><br/><br/>
<xsl:value-of select="//lang_blocks/p100" /><br/><br/>
<xsl:value-of select="//lang_blocks/p101" /><br/><br/>
<b><xsl:value-of select="//lang_blocks/p102" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p103" /><br/><br/>
<xsl:value-of select="//lang_blocks/p104" /><br/><br/>
<b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">123456</font>/affinity/intro.xml</b><br/><br/>

<xsl:value-of select="//lang_blocks/p105" /><font color="#CC3300"><xsl:value-of select="//lang_blocks/p106" /></font><br/><br/>

<xsl:value-of select="//lang_blocks/p107" /><br/>

<xsl:value-of select="//lang_blocks/p108" /><br/>
<xsl:value-of select="//lang_blocks/p109" /><br/>
<xsl:value-of select="//lang_blocks/p110" /><br/><br/>

<xsl:value-of select="//lang_blocks/p111" /><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="><xsl:value-of select="//lang_blocks/p112" /></a><xsl:value-of select="//lang_blocks/p113" />

<br/><br/><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p114" /></b></font>


</td>
</tr>
</table>


<p><b><xsl:value-of select="//lang_blocks/p31" /></b> <xsl:value-of select="//lang_blocks/p115" /></p>

<p><b><xsl:value-of select="//lang_blocks/p34" /></b> <xsl:value-of select="//lang_blocks/p116" /></p>

<p><b><xsl:value-of select="//lang_blocks/p37" /></b><xsl:value-of select="//lang_blocks/p117" /></p>
<p><xsl:value-of select="//lang_blocks/p118" /></p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p119" /></sub>
<p><xsl:value-of select="//lang_blocks/p120" /></p>

<p><b><xsl:value-of select="//lang_blocks/p22" /></b> <xsl:value-of select="//lang_blocks/p121" /></p>
<p><b><xsl:value-of select="//lang_blocks/p24" /></b> <xsl:value-of select="//lang_blocks/p122" /></p>
<p><b><xsl:value-of select="//lang_blocks/p27" /></b> <xsl:value-of select="//lang_blocks/p123" /></p>

<p><xsl:value-of select="//lang_blocks/p124" /></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p125" /></b></p>

<table border="1"><tr><td>
<div align="center"><b><xsl:value-of select="//lang_blocks/p126" /></b></div><br/><br/>
<b><xsl:value-of select="//lang_blocks/p127" /></b><br/>
<b><xsl:value-of select="//lang_blocks/p128" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p129" /><b><xsl:value-of select="//lang_blocks/p130" /></b>


<br/><br/><xsl:value-of select="//lang_blocks/p95" /> <b><xsl:value-of select="//lang_blocks/p131" /></b><br/><br/>

<xsl:value-of select="//lang_blocks/p132" /> <font color="#CC3300"><xsl:value-of select="//lang_blocks/p133" /></font><xsl:value-of select="//lang_blocks/p134" />
<br/><br/><xsl:value-of select="//lang_blocks/p135" />

<br/><br/><xsl:value-of select="//lang_blocks/p136" />

<br/><br/><xsl:value-of select="//lang_blocks/p137" />
<br/><br/><img src="http://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" width="468" height="60" border="0" alt="affinity banner"/><br/><br/>
<xsl:value-of select="//lang_blocks/p138" /><br/><br/>
<xsl:value-of select="//lang_blocks/p139" /><br/><br/>
<b><xsl:value-of select="//lang_blocks/p140" /></b><br/><br/>

<xsl:value-of select="//lang_blocks/p141" />
<br/><br/>

<xsl:value-of select="//lang_blocks/p142" /><br/><br/>
 <b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">123456</font>/affinity/intro.xml</b><br/><br/>
 <xsl:value-of select="//lang_blocks/p143" /> <font color="#CC3300"> (<xsl:value-of select="//lang_blocks/p110" />)</font> <xsl:value-of select="//lang_blocks/p144" /><font color="#CC3300"><xsl:value-of select="//lang_blocks/p106" /></font><br/><br/>

<xsl:value-of select="//lang_blocks/p145" /><br/><br/>
<xsl:value-of select="//lang_blocks/p108" /><br/>
<font color="#CC3300"><xsl:value-of select="//lang_blocks/p109" /></font><br/><br/>


</td></tr></table>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p146" /></sub>
<p><xsl:value-of select="//lang_blocks/p147" /></p>
<p><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p39" /></b></font><xsl:value-of select="//lang_blocks/p148" /> <a href="http://www.clubshop.com/manual/policies/spam.xml" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a></p>

<p><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?action=search"><xsl:value-of select="//lang_blocks/p149" /></a></p>

<p><xsl:value-of select="//lang_blocks/p150" /><b><xsl:value-of select="//lang_blocks/p151" /></b>
<xsl:value-of select="//lang_blocks/p152" /></p>
<br/>
<b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">123456</font>/affinity/intro.xml</b>

<p><b><xsl:value-of select="//lang_blocks/p22" /></b> <xsl:value-of select="//lang_blocks/p153" /></p>

<p><b><xsl:value-of select="//lang_blocks/p24" /></b> <xsl:value-of select="//lang_blocks/p154" /></p>

<p><b><xsl:value-of select="//lang_blocks/p27" /></b> <xsl:value-of select="//lang_blocks/p155" /></p>

<p><xsl:value-of select="//lang_blocks/p156" /></p>
<p><font color="0033CC"><xsl:value-of select="//lang_blocks/p157" /></font></p>
<hr/>

<table border="1"><tr><td>
<br/><b><xsl:value-of select="//lang_blocks/p158" /></b><br/>
<xsl:value-of select="//lang_blocks/p159" /><br/><br/>
<xsl:value-of select="//lang_blocks/p160" /><br/><br/>
<xsl:value-of select="//lang_blocks/p161" /><font color="#CC3300"><xsl:value-of select="//lang_blocks/p133" /></font><xsl:value-of select="//lang_blocks/p162" /><br/><br/>

<xsl:value-of select="//lang_blocks/p163" /><br/><br/>
<xsl:value-of select="//lang_blocks/p164" /><br/><br/>

<xsl:value-of select="//lang_blocks/p165" /><br/><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/banner_468_b.gif" width="468" height="60" border="0" alt="affinity banner"/><br/><br/>

<xsl:value-of select="//lang_blocks/p166" /><br/><br/>
<xsl:value-of select="//lang_blocks/p167" /><br/><br/>
<b><xsl:value-of select="//lang_blocks/p168" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p169" /><br/><br/>

<xsl:value-of select="//lang_blocks/p170" /><br/><br/>
<b>http://www.clubshop.com/cgi-bin/rd/10,,refid=<font color="#CC3300">123456</font>/affinity/intro.xml</b><br/><br/><br/>

<xsl:value-of select="//lang_blocks/p143" /> <font color="#CC3300"> (<xsl:value-of select="//lang_blocks/p110" />)</font> <xsl:value-of select="//lang_blocks/p144" /><font color="#CC3300"><xsl:value-of select="//lang_blocks/p106" /></font><br/><br/>
<xsl:value-of select="//lang_blocks/p171" /><br/><br/>


<xsl:value-of select="//lang_blocks/p108" /><br/><br/>
<font color="#CC3300"><xsl:value-of select="//lang_blocks/p109" /></font><br/>

<br/><br/><xsl:value-of select="//lang_blocks/p111" /><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="><xsl:value-of select="//lang_blocks/p112" /></a><xsl:value-of select="//lang_blocks/p113" />

<br/><br/><font color="#CC3300"><b><xsl:value-of select="//lang_blocks/p114" /></b></font><br/>
</td></tr></table>
<hr/>

<p><b><xsl:value-of select="//lang_blocks/p31" /></b> <xsl:value-of select="//lang_blocks/p172" /></p>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p173" /></sub>
<p><b><xsl:value-of select="//lang_blocks/p174" /></b><xsl:value-of select="//lang_blocks/p175" /></p>
<p><b><xsl:value-of select="//lang_blocks/p176" /></b><xsl:value-of select="//lang_blocks/p177" /></p>
<hr/>

<p style="text-align: center">
            <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/ag-index.xml">
               <xsl:value-of select="//lang_blocks/toc" />
            </a>

            | 
           
               <a href="http://www.clubshop.com/vip/ebusiness/_marketing_ag/lesson_3.xml"><xsl:value-of select="//lang_blocks/lesson3" /></a>

            </p>

            <br />

            <p style="text-align: right" class="small">
  Copyright @ ClubShop.com 1997-2010 
  <br /> 
  All Rights Reserved.  
<!--  <img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/>
  
  --> 
  </p>



















         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>