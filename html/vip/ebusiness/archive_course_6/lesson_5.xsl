<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<!--<p><img src="../images/c6_l5a.jpg" height="75" width="120" alt="day runner"/>-->

<!--<xsl:value-of select="//lang_blocks/p1" /></p>

<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>

<div align="center"> 
<form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
<textarea name="ppf" cols="50" rows="5"></textarea><br/>
<input type="submit" value="Submit"/>
<input type="reset" name="Reset" value="Reset"/>

<input type="hidden" name="action" value="confirm"/>
<input type="hidden" name="type" value="9"/>
<input type="hidden" name="u_key" value="tsfpps"/>
</form>

</div>-->


<table class="xcolor">
<tr><td>
<div align="center"><h5><xsl:value-of select="//lang_blocks/p12"/></h5></div>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<ul>
<!--<li><xsl:value-of select="//lang_blocks/p14"/></li>-->
<li><xsl:value-of select="//lang_blocks/p30"/></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p25"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>
<li><xsl:value-of select="//lang_blocks/p17"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><xsl:value-of select="//lang_blocks/p18"/></li>
<li><xsl:value-of select="//lang_blocks/p22"/></li>
<li><xsl:value-of select="//lang_blocks/p19"/></li>

<!--li><xsl:value-of select="//lang_blocks/p20"/></li-->
<li><xsl:value-of select="//lang_blocks/p23"/> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->

<p><xsl:value-of select="//lang_blocks/p21"/></p>
</td></tr>
</table>







<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_6/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/3214">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>