<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p class="red">
<xsl:value-of select="//lang_blocks/p1" /></p>

<p><xsl:value-of select="//lang_blocks/p2" /><font color="#FF0000"> <xsl:value-of select="//lang_blocks/p3" /><xsl:value-of select="//lang_blocks/p4" /></font><xsl:value-of select="//lang_blocks/p7" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p5" /></font><xsl:value-of select="//lang_blocks/p8" /></p>



<p><xsl:value-of select="//lang_blocks/p9" /> <font color="#FF0000"> <xsl:value-of select="//lang_blocks/p3" /></font>!</p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p10" /></font></p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p11" /></font><xsl:value-of select="//lang_blocks/p12" /> </p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p13" /></font><xsl:value-of select="//lang_blocks/p14" /> </p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p15" /></font><xsl:value-of select="//lang_blocks/p16" /> </p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p17" /><br/><a href="https://www.clubshop.com/cgi-bin/Login.cgi"><xsl:value-of select="//lang_blocks/p18" /></a> <xsl:value-of select="//lang_blocks/p19" /><font color="#FF0000"> <xsl:value-of select="//lang_blocks/p6" /></font></p>

<p><xsl:value-of select="//lang_blocks/p20" /><b><xsl:value-of select="//lang_blocks/p21" /></b><xsl:value-of select="//lang_blocks/p22" /><b><xsl:value-of select="//lang_blocks/p23" /></b></p>


<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p28" /></li>
<li><xsl:value-of select="//lang_blocks/p29" /></li>
<li><xsl:value-of select="//lang_blocks/p30" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p31" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/cancel.html" target="_blank"><xsl:value-of select="//lang_blocks/p32" /></a><b><xsl:value-of select="//lang_blocks/p33" /></b><xsl:value-of select="//lang_blocks/p34" /></p>
<p><xsl:value-of select="//lang_blocks/p35" /><b><xsl:value-of select="//lang_blocks/p36" /></b><xsl:value-of select="//lang_blocks/p37" /></p>

<p><xsl:value-of select="//lang_blocks/p38" /></p>


<p><xsl:value-of select="//lang_blocks/p39" /><br/><font color="#FF0000"><xsl:value-of select="//lang_blocks/p5" /></font></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p25" /></font></p>




<hr/>
<div align="center"><p class="close"><a href="javascript:self.close();">
<xsl:value-of select="//lang_blocks/close" /></a>  
</p> </div>                
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>