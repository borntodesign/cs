<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p1a" /></p>
<hr/>
<!--<h5><xsl:value-of select="//lang_blocks/p2" /></h5>

<p><xsl:value-of select="//lang_blocks/p3" /></p>

<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><b><xsl:value-of select="//lang_blocks/p5" /></b><xsl:value-of select="//lang_blocks/p6" /></p>
<p><b><xsl:value-of select="//lang_blocks/p7" /></b><xsl:value-of select="//lang_blocks/p8" /></p>


<hr/>-->
<h5><xsl:value-of select="//lang_blocks/p9" /></h5>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>


<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr>
<td><p><b><xsl:value-of select="//lang_blocks/p13" /></b></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
</td>
</tr></table>-->
<hr/>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<ul>
<li><b><xsl:value-of select="//lang_blocks/p17" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p18" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p19" /></b></li>
</ul>

<p><b><xsl:value-of select="//lang_blocks/p20" /></b><xsl:value-of select="//lang_blocks/p21" /><a onclick="" href="http://www.glocalincome.com/cancel.html"  target="_blank"><xsl:value-of select="//lang_blocks/p22" /></a><xsl:value-of select="//lang_blocks/p23" /></p>


<p><b><xsl:value-of select="//lang_blocks/p24" /></b><xsl:value-of select="//lang_blocks/p25" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /> (<a onclick="" href="epcfemail.xml" target="_blank"><xsl:value-of select="//lang_blocks/p27" /></a>)</p>

<!--<p><xsl:value-of select="//lang_blocks/p28" /></p>-->
<p><xsl:value-of select="//lang_blocks/p29" />(<a onclick="" href="ecan.xml"  target="_blank"><xsl:value-of select="//lang_blocks/p30" /></a>)</p>

<img src="../images/pennies.jpg" height="70" width="110" alt="A penny saved..."/>
<hr/>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<p><b><xsl:value-of select="//lang_blocks/p32" /></b><xsl:value-of select="//lang_blocks/p33" /></p>

<!--<p><b><xsl:value-of select="//lang_blocks/p34" /></b><xsl:value-of select="//lang_blocks/p35" /></p>-->

<!--<table class="assign">
<tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><b><xsl:value-of select="//lang_blocks/p36" /></b></p>
<img src="../images/clubcontact.gif" height="37" width="187" alt="ClubContact"/>

<p><xsl:value-of select="//lang_blocks/p37" /></p>
<ul>
<li><b><xsl:value-of select="//lang_blocks/p38" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p39" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p40" /></b></li>
<ul>
<li>First Name</li>
<li>Last Name</li>
<li>Email Address</li>
<li>Numeric ID</li>
<li>Network Pay Points</li>
<li>Personal Pay Points</li>
<li>Address 1</li>
<li>Address 2</li>
<li>City</li>
<li>State/Province</li>
<li>Postal Code</li>
<li>Country</li>
<li>Area Code</li>
<li>Telephone</li>
<li>Select Network Level as "Equals", "All" and Apply to "Individual"</li>
<li>Select Dream Team Level as "Equals", "All" and Apply to "Individual"</li>
<li>Exclude the first Strategist that you may have in each of your lines, if any.(This will also exclude their downline)</li>

</ul>

</ul>
<div align="center">
<p><a href="contactnpays.html"  target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a></p>
</div>
</td>
</tr>
</table>-->

<table class="assign">
<tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p67" /></p>
<p><xsl:value-of select="//lang_blocks/p68" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/vip/unpaid_vips.cgi" target="_blank">https://www.clubshop.com/cgi/vip/unpaid_vips.cgi</a></p>
<p><xsl:value-of select="//lang_blocks/p70" /></p>
<p><xsl:value-of select="//lang_blocks/p71" /></p>

</td></tr></table>







<p><xsl:value-of select="//lang_blocks/p42" /><b><xsl:value-of select="//lang_blocks/p43" /></b><xsl:value-of select="//lang_blocks/p44" /></p>

<p><xsl:value-of select="//lang_blocks/p45" /></p>

<p><b><xsl:value-of select="//lang_blocks/p46" /></b></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p47" /></li>
<li><xsl:value-of select="//lang_blocks/p48" /></li>
<li><xsl:value-of select="//lang_blocks/p49" /></li>
</ul>

<table class="xcolor">
<tr><td><h5><xsl:value-of select="//lang_blocks/p50" /></h5>

<p class="blue"><xsl:value-of select="//lang_blocks/p51" /></p>
<p><xsl:value-of select="//lang_blocks/p52" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p53" /></p>
<p><xsl:value-of select="//lang_blocks/p54" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p55" /></p>
<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p57" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
<p><xsl:value-of select="//lang_blocks/p60" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p61" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p63" /></p>
<p class="blue"><xsl:value-of select="//lang_blocks/p64" /></p>
<p><xsl:value-of select="//lang_blocks/p65" /></p>
<p><xsl:value-of select="//lang_blocks/p66" /></p>
<p><xsl:value-of select="//lang_blocks/p72" /></p>
</td></tr>

</table>













<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_5.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>