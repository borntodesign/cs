<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>

<p><b><xsl:value-of select="//lang_blocks/p3"/></b></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
<li><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><a onclick="" href="letters.xml" target="_blank"><xsl:value-of select="//lang_blocks/p9"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10"/></li>

</ol>
<p><xsl:value-of select="//lang_blocks/p11"/></p>

<table class="xcolor">
<tr><td>
<div align="center"><h5><xsl:value-of select="//lang_blocks/p12"/></h5></div>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<ul><!--<li><xsl:value-of select="//lang_blocks/p14"/></li>-->
<li><xsl:value-of select="//lang_blocks/p30"/></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p25"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>
<li><xsl:value-of select="//lang_blocks/p17"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity"/></li>
<li><xsl:value-of select="//lang_blocks/p18"/></li>
<li><xsl:value-of select="//lang_blocks/p24"/></li>
<li><xsl:value-of select="//lang_blocks/p19"/></li>
<!--li><xsl:value-of select="//lang_blocks/p19a"/></li-->
<!--li><xsl:value-of select="//lang_blocks/p20"/></li-->
<li><xsl:value-of select="//lang_blocks/p25a"/><xsl:text> </xsl:text><a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->

<p><xsl:value-of select="//lang_blocks/p21"/></p>
</td></tr>
</table>
<!--p><xsl:value-of select="//lang_blocks/p22"/></p>

<p><xsl:value-of select="//lang_blocks/p23"/></p-->

<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>