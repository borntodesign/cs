<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />
<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p class="red">
<xsl:value-of select="//lang_blocks/p1" /></p>

<p><xsl:value-of select="//lang_blocks/p2" /><xsl:text> </xsl:text><font color="#FF0000"> <xsl:value-of select="//lang_blocks/p3" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p4" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7" /><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p5" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8" /></p>



<p><xsl:value-of select="//lang_blocks/p9" /><xsl:text> </xsl:text> <font color="#FF0000"> <xsl:value-of select="//lang_blocks/p3" /></font>,</p>

<p><xsl:value-of select="//lang_blocks/p10" /><xsl:text> </xsl:text><a href="http://www.glocalincome.com" target="_blank"><xsl:value-of select="//lang_blocks/p11"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12"/><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p6" /></font></p>
<p><xsl:value-of select="//lang_blocks/p13" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p14" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19" /></p>


<p><xsl:value-of select="//lang_blocks/p20" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/ebusiness/" target="_blank"><xsl:value-of select="//lang_blocks/p21" /></a></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<p><xsl:value-of select="//lang_blocks/p24" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p5" /></font></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p25" /></font></p>




<hr/>
<div align="center"><p class="close"><a href="javascript:self.close();">
<xsl:value-of select="//lang_blocks/close" /></a>  
</p> </div>                
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>