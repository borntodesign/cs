<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p2"/></p>

<p><xsl:value-of select="//lang_blocks/p3"/></p>

<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:copy-of select="//lang_blocks/p5/*|//lang_blocks/p5/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p6"/></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
<li><xsl:value-of select="//lang_blocks/p8"/></li>
<li><xsl:value-of select="//lang_blocks/p9"/></li>
<li><xsl:value-of select="//lang_blocks/p10"/></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p11"/></p>
<p><xsl:value-of select="//lang_blocks/p12"/></p>

<p><xsl:value-of select="//lang_blocks/p13"/></p>























<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>