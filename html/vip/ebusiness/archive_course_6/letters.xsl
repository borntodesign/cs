<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c6_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t6_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p2" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3" /><xsl:text> </xsl:text><font color="#FF00000"><xsl:value-of select="//lang_blocks/p4" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5" /></p>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p6" /></h5>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p10" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p9" />,</b></p>

<p><xsl:value-of select="//lang_blocks/p12" /><xsl:text> </xsl:text>(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p13" /><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p14" /></font><xsl:value-of select="//lang_blocks/p15" /></p>

<p><xsl:value-of select="//lang_blocks/p16" /></p>

<p><xsl:value-of select="//lang_blocks/p17" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font>
</p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p19" /></font></p>

<hr/>
<!--Letter 2

<h5><xsl:value-of select="//lang_blocks/p20" /></h5>
<p><xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /><b><xsl:value-of select="//lang_blocks/p9" /></b> <xsl:value-of select="//lang_blocks/p22" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /><b><xsl:value-of select="//lang_blocks/p9" /></b>,</p>

<p><xsl:value-of select="//lang_blocks/p23" />(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p13" /> <xsl:value-of select="//lang_blocks/p24" /></font>) <xsl:value-of select="//lang_blocks/p25" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /><b><xsl:value-of select="//lang_blocks/p27" /></b></p>

<p><xsl:value-of select="//lang_blocks/p28" /></p>

<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/>
<xsl:value-of select="//lang_blocks/p32" /></p>

<p><xsl:value-of select="//lang_blocks/p33" /></p>

<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p class="red"><xsl:value-of select="//lang_blocks/p35" /></p>
-->


<!--letter 3
<h5><xsl:value-of select="//lang_blocks/p36" /></h5>


<p><xsl:value-of select="//lang_blocks/p37" /></p>

<p><xsl:value-of select="//lang_blocks/p8" /><b><xsl:value-of select="//lang_blocks/p9" /></b> <xsl:value-of select="//lang_blocks/p38" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /> <b><xsl:value-of select="//lang_blocks/p9" /></b>,</p>
<p><xsl:value-of select="//lang_blocks/p39" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p40" /></font><font color="#FF0000"><xsl:value-of select="//lang_blocks/p24" /></font>)
<xsl:value-of select="//lang_blocks/p42" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p43" /></font><xsl:value-of select="//lang_blocks/p44" /></p>


<p><xsl:value-of select="//lang_blocks/p45" /></p>

<p><xsl:value-of select="//lang_blocks/p46" /><b><xsl:value-of select="//lang_blocks/p14" /></b></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>
<p><xsl:value-of select="//lang_blocks/p48" /></p>
<p><xsl:value-of select="//lang_blocks/p49" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/>
<xsl:value-of select="//lang_blocks/p32" /></p>


<p><xsl:value-of select="//lang_blocks/p33" /></p>

<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p class="red"><xsl:value-of select="//lang_blocks/p35" /></p>
<hr/>
-->
<h5><xsl:value-of select="//lang_blocks/p50" /></h5>
<p><xsl:value-of select="//lang_blocks/p51" /></p>



<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p52" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p9" /></b>,</p>
<p><xsl:value-of select="//lang_blocks/p53" /><xsl:text> </xsl:text>(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p54" /></font>)<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p42" /><xsl:text> </xsl:text>(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p43" /></font>)<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p55" /></p>
<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p><xsl:value-of select="//lang_blocks/p57" /></p>
<p><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p84" /></h5>
<p><xsl:value-of select="//lang_blocks/p85" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p86" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b>,</p>
<p><xsl:value-of select="//lang_blocks/p87" /><br/><a href="http://www.clubshop.com/cgi/marketing_questionnaires.cgi">http://www.clubshop.com/cgi/marketing_questionnaires.cgi</a></p>
<p><xsl:value-of select="//lang_blocks/p88" /></p>
<p><xsl:value-of select="//lang_blocks/p89" /></p>
<p><xsl:value-of select="//lang_blocks/p90" /></p>
<p><xsl:value-of select="//lang_blocks/p91" /></p>

<p><xsl:value-of select="//lang_blocks/p17" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/><xsl:value-of select="//lang_blocks/p92" /></p>
<p><xsl:value-of select="//lang_blocks/p93" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/">http://www.clubshop.com</a></p>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p94" /></h5>
<p><xsl:value-of select="//lang_blocks/p95" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p96" /></p>
<p><xsl:value-of select="//lang_blocks/p97" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p27" /></b></p>
<p><xsl:value-of select="//lang_blocks/p98" /></p>
<p><xsl:value-of select="//lang_blocks/p17" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/><xsl:value-of select="//lang_blocks/p92" /></p>
<p><xsl:value-of select="//lang_blocks/p99" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/marketing_questionnaires.cgi/">http://www.clubshop.com/cgi/marketing_questionnaires.cgi</a></p>




<hr/>
<h5><xsl:value-of select="//lang_blocks/p60" /></h5>
<!--<p><xsl:value-of select="//lang_blocks/p61" /></p>-->

<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /> <b><xsl:value-of select="//lang_blocks/p9" /></b>,</p>

<p><xsl:value-of select="//lang_blocks/p63" /><xsl:text> </xsl:text>(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p64" /></font>)<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p65" /><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p66" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p67" /> </p>
<p><xsl:value-of select="//lang_blocks/p68" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p27" /></b></p>

<p><xsl:value-of select="//lang_blocks/p69" /></p>
<p><xsl:value-of select="//lang_blocks/p70" /></p>
<p><xsl:value-of select="//lang_blocks/p71" /></p>
<p><xsl:value-of select="//lang_blocks/p72" /></p>
<p><xsl:value-of select="//lang_blocks/p73" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/>
<xsl:value-of select="//lang_blocks/p32" /></p>

<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p35" /></font></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p74" /></h5>
<p><xsl:value-of select="//lang_blocks/p75" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p76" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" />,</b></p>

<p><xsl:value-of select="//lang_blocks/p77" /><xsl:text> </xsl:text>(<font color="#FF0000"><xsl:value-of select="//lang_blocks/p78" /></font>)<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p79 " /><xsl:text> </xsl:text><font color="#FF0000"><xsl:value-of select="//lang_blocks/p66" /></font><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p80" /></p>

<p><xsl:value-of select="//lang_blocks/p81" /></p>
<p><xsl:value-of select="//lang_blocks/p82" /></p>

<p><xsl:value-of select="//lang_blocks/p73" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font></p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p19" /></font></p>

<hr/>
<p align="center"><A HREF="javascript:self.close();"><font face="Arial, Helvetica, sans-serif" size="1">Close 
              this Window</font></A></p>



     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>