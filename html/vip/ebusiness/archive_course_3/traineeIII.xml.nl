<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../import_master.xsl" ?>
<?xml-stylesheet type="text/xsl" href="traineeIII.xsl" ?>




<lang_blocks>
  <daily1>Beantwoord al uw zakelijke email</daily1>
  <daily2>Schrijf nieuwe Leden in</daily2>
  <daily3>Controleren van mijn activity rapport en verzenden van de members welkom emails</daily3>
  <daily4>Mijn Netwerk Rapport en Member Lijn Rapporten nalopen en Member Welkom emails sturen aan leden overgeplaatst naar mijn netwerk.</daily4>
  <daily5>Besteed ______ minuten aan het adverteren en aanbrengen van nieuwe leden</daily5>
  <fri>Vrij - </fri>
  <future1>Volbreng de Builder Intern Cursus en Stappen</future1>
  <future2>Behaal 100 Netwerk Pay Points en wordt een Team Player</future2>
  <goals1>Volbreng de Trainee III Cursus en Stappen</goals1>
  <goals2>Begin gebruik te maken van een marketing methode</goals2>
  <goals3>Schrijf in 30 dagen tijd 5 nieuwe leden in</goals3>
  <goals4>Behaal 75 Netwerk Pay Points en wordt Producer, aanspraak makend op Netwerk Inkomen</goals4>
  <goals5>Start de volgende cursus en Stappen - Builder Intern</goals5>
  <mon>Ma - </mon>
  <sat>Zat - </sat>
  <sun>Zon - </sun>
  <thurs>Do - Bijwonen van de Interactieve Training OF luisteren naar de opname. </thurs>
  <title>Trainee III - Persoonlijke Planner</title>
  <todo1>Heeft u een persoonlijke webpagina opgesteld en de URL naar uw Netwerk Builder gestuurd ter goedkeuring? Les 6</todo1>
  <todo2>Heeft u een persoonlijke Member Welkom Email opgesteld en naar uw Netwerk Builder gestuurd ter goedkeuring? Les 7</todo2>
  <todo3>Heeft u zelfstandig minimaal 1 member aangesloten van wie een ClubBucks kaart geactiveerd is? </todo3>
  <todo4> Staat uw Auto Email (nog) aan en heeft u de URL naar uw persoonlijke webpagina er geplaatst?</todo4>
  <tues>Di -De doelen, projecten, Te doen lijst en toekomstige doelen op mijn Persoonlijke Planner herzien en veranderen, verwijderen of toevoegen waar nodig. </tues>
  <wed>Woe - </wed>
</lang_blocks>





