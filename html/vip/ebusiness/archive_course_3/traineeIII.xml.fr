<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../import_master.xsl" ?>
<?xml-stylesheet type="text/xsl" href="traineeIII.xsl" ?>





<lang_blocks>
  <daily1>Répondre tous les emails liés à l'affaire</daily1>
  <daily2>Inscrivez des nouveaux Membres</daily2>
  <daily3>Regarder mon Rapport d'Activité et envoyer des Emails de Bienvenue aux Membres</daily3>
  <daily4>Regarder Mon Rapport de Réseau et les Rapports des Lignées de Membre et envoyer des Emails de Bienvenue aux Membres transférés dans mon réseau.</daily4>
  <daily5>Prenez ______ minutes pour promouvoir votre affaire pour trouver des nouveaux membres</daily5>
  <fri>Vendredi - </fri>
  <future1>Compléter le Cours et l'étape de Builder Intern </future1>
  <future2>Avoir 100 Network Pay Points et devenir un Team Player</future2>
  <goals1>Achever le Cours Trainee III </goals1>
  <goals2>Commencer à utiliser une méthode de Vente </goals2>
  <goals3>Parrainer 5 nouveaux Membres en 30 jours</goals3>
  <goals4>Avoir 75 Network Pay Points et devenir un Producer, éligible à reçevoir Revenu de Réseau</goals4>
  <goals5>Commencer le prochain Cours et Etape - Builder Intern</goals5>
  <mon>Lundi - </mon>
  <sat>Samedi - </sat>
  <sun>Dimanche - </sun>
  <thurs>Jeudi - Soyez présent à une formation interactive en ligne OU écouter la session enrégistré. </thurs>
  <title>Trainee III - Planificateur Personnel</title>
  <todo1>Avez-vous créé un site personnel et avez-vous envoyé le URL à votre Network Builder pour l'avoir approuvé? Leçon 6</todo1>
  <todo2>Avez-vous créé un Email de Bienvenue personnel et l'avez-vous envoyé à votre Network Builder pour l'avoir approuvé? Leçon 7</todo2>
  <todo3>Avez-vous référencé au moins un membre qui a eu une carte ClubBucks activé?</todo3>
  <todo4>Est votre Auto Email (encore) mis sur active et avez-vous mis le URL vers votre page personnel là?</todo4>
  <tues>Mardi -Reviser mes Buts, Projets, Liste à faire et Futurs Buts de mon Planificateur Personnel. Changer, supprimer ou ajouter si nécessaire. </tues>
  <wed>Mercredi - </wed>
</lang_blocks>




