<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>

table.assign{
	border: thin solid #4F5736;
	width: 100%;
}

td.quote{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #4F5736;
	border-bottom : Gray;
}
hr{  color: #4F5736;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #4F5736;
 text-indent: 5%;
  }
h5 {color: #4F5736;
}

</style>
</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t3_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>



<p><xsl:value-of select="//lang_blocks/p1" /></p>


<ul>
<li><xsl:value-of select="//lang_blocks/p2" /></li>
<li><xsl:value-of select="//lang_blocks/p3" /></li>
<li><xsl:value-of select="//lang_blocks/p4" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p5" /></p>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p6" /></h5>


<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>

<!--<h5><xsl:value-of select="//lang_blocks/p10" /></h5>
<p><xsl:value-of select="//lang_blocks/p11" /></p>

<p><xsl:value-of select="//lang_blocks/p12" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/vip/ebusiness/" target="_blank">https://www.clubshop.com/vip/ebusiness/</a></p>

<h5><xsl:value-of select="//lang_blocks/p13" /></h5>


<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /><xsl:text> </xsl:text><a href="http://www.glocalincome.com/present/bizop.xml" target="_blank">http://www.glocalincome.com/present/bizop.xml</a></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<div align="center"><img src="/images/cards/rewards_club.gif" width="200" height="129" alt="ClubShop Rewards Card"/></div>








<p><xsl:value-of select="//lang_blocks/p17" /></p>
<div align="center"><img src="/vip/ebusiness/images/carry_cbcards.gif" width="300" height="75" alt="Carry ClubShop Rewards Cards"/></div>
<p><xsl:value-of select="//lang_blocks/p18" /></p> -->


<h5><xsl:value-of select="//lang_blocks/p19" /> </h5>

<p><xsl:value-of select="//lang_blocks/p20" /><xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com/localmarketing/" target="_blank">http://www.glocalincome.com/localmarketing/</a></p>




<br/><br/>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_3/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_4.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>