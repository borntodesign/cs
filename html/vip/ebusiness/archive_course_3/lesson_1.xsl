<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
   table.assign{
	border: thin solid #4F5736;
	width: 100%;
}

td.quote{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #4F5736;
	border-bottom : Gray;
}
hr{  color: #4F5736;}

p.indent{text-indent: 2em;}
div.indent{text-indent: 1em;}
 sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #4F5736;
 text-indent: 5%;
  }
  
h5{color: #4F5736;}
</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t3_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="../images/traineeline.gif" />
<br/>
<table><tr>
<td><img src="../images/sjohnson.gif" height="104" width="91" alt="Samuel Johnson"/></td>
<td><p><xsl:value-of select="//lang_blocks/quote" /> <b><xsl:value-of select="//lang_blocks/quote2" /></b></p></td>
</tr></table>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<table><tr>
<td><img src="../images/posse.gif" width="149" height="99" alt="team posse"/></td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr></table>
<p><xsl:value-of select="//lang_blocks/p2" /></p>


<p><xsl:value-of select="//lang_blocks/p3" /></p>

<table class="assign" align="center" cellpadding="6">
<tr><td><p>
<xsl:value-of select="//lang_blocks/p4" /></p></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p5" />
</p></td>
</tr>
</table>


<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<div align="center"><h5><xsl:value-of select="//lang_blocks/p9" /></h5>
<img src="../images/holding_hands.jpg" width="115" height="24" alt="handsholding"/></div>


<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_3/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet> 