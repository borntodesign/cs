<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
	width: 100%;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #4F5736;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: medium;
 font-weight: bold;
 color: #4F5736;
 text-indent: 5%;
  }
h5 {color: #4F5736;
}

tr.acolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 background-color: #DEDEBC;
 text-align: center;
 }
 tr.bcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 background-color: #A2A251;
 text-align: center;
 }
 
 

</style>
</head>

<body>

<table width="900" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t3_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><b><xsl:value-of select="//lang_blocks/p54" /></b></p>
<p><xsl:value-of select="//lang_blocks/p55" /></p>


<sub><xsl:value-of select="//lang_blocks/p1" /></sub>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>

<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>

<p><xsl:value-of select="//lang_blocks/p6" />
<br/>
<xsl:value-of select="//lang_blocks/p7" /></p>

<p><xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<hr/>



<!--sub><xsl:value-of select="//lang_blocks/p11" /></sub>


<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p12" /><a onclick="" href="http://www.clubshop.com/manual/policies/name_usage.html" target="_blank"><xsl:value-of select="//lang_blocks/p13" /></a><xsl:value-of select="//lang_blocks/p14" /></font></p>


<p><xsl:value-of select="//lang_blocks/p15" /> <a onclick="" href="http://www.geocities.com/" target="_blank"><xsl:value-of select="//lang_blocks/p16" /></a> <xsl:value-of select="//lang_blocks/p17" /></p-->




<!--sub><xsl:value-of select="//lang_blocks/p21" /></sub>


<p><xsl:value-of select="//lang_blocks/p23" /></p>


<table class="assign"><tr><td>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<ul><li><xsl:value-of select="//lang_blocks/p26" /></li>
<li><xsl:value-of select="//lang_blocks/p27" /></li>
<li><xsl:value-of select="//lang_blocks/p28" /></li></ul>

<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>
<p><xsl:value-of select="//lang_blocks/p32" /></p>
<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p><xsl:value-of select="//lang_blocks/p35" /></p>
<p><xsl:value-of select="//lang_blocks/p36" /></p>
<p><xsl:value-of select="//lang_blocks/p37" /></p>
</td></tr></table-->

<sub><xsl:value-of select="//lang_blocks/p38" /></sub>
<p><xsl:value-of select="//lang_blocks/p58" /> <xsl:text> </xsl:text><a href="https://www.clubshop.com/vip/ebusiness/prw.xml" target="_blank"><xsl:value-of select="//lang_blocks/p59" /></a></p>
<p><xsl:value-of select="//lang_blocks/p39" /></p>
<p><xsl:value-of select="//lang_blocks/p40" /></p>


<sub><xsl:value-of select="//lang_blocks/p57" /></sub>
<p><xsl:value-of select="//lang_blocks/p20" /></p>

<sub><xsl:value-of select="//lang_blocks/p43" /></sub>
<p><xsl:value-of select="//lang_blocks/p44" /></p>

<!--<sub><xsl:value-of select="//lang_blocks/p45" /></sub>
<p><xsl:value-of select="//lang_blocks/p46" /></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>-->

<!--<div align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/c3_l6_a.gif" width="201" height="182" alt="Auto-Email"/></div>-->


<p><xsl:value-of select="//lang_blocks/p48" /></p>



<table class="assign"><tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<p><b><xsl:value-of select="//lang_blocks/p49" /></b><br/><a href="https://www.clubshop.com/vip/ebusiness/prw.xml" target="_blank">
<xsl:value-of select="//lang_blocks/p50" /></a></p>

<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p12" /> <xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com/manual/policies/name_usage.xml" target="_blank"><xsl:value-of select="//lang_blocks/p13" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14" /></font></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p56" /></font></p>


<p><b><xsl:value-of select="//lang_blocks/p51" /></b></p>
<!--<p><b><xsl:value-of select="//lang_blocks/p52" /></b><br/>
<a onclick="" href="https://www.clubshop.com/cgi/pwl-mgr.cgi" target="_blank">https://www.clubshop.com/cgi/pwl-mgr.cgi</a></p>-->


<!--<p><b><xsl:value-of select="//lang_blocks/p53" /></b></p>

<div align="center"> 
<form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
<input type="text" name="url" size="60" />
<br/>
<input type="submit" value="Submit" />
<input type="reset" name="Reset" value="Reset" />
<input type="hidden" name="action" value="confirm" />
<input type="hidden" name="type" value="3" />
<input type="hidden" name="u_key" value="uwps" />
</form>

</div>-->
</td>
</tr>
</table>
<hr/>
<sub><xsl:value-of select="//lang_blocks/p41" /></sub>
<p><xsl:value-of select="//lang_blocks/p42" /></p>

<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_7.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
</td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>