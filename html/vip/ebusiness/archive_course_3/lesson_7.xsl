<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

            <link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />

<style>
  table.assign{
    border: thin solid #404269;
}

td.assign{
    font-family: Arial, Helvetica, sans-serif;
    font-size: medium;
    font-weight: bold;
    color: White;
    background-color: #404269;
    border-bottom : Gray;
}
hr{  color: #404269;}

h5 {color: #4F5736;
}
</style>
         </head>

         <body>
            <table width="1000" margin="25px">
               <tr>
                  <td class="top">
                     <img src="../images/t3_logo.jpg" width="705" height="101" alt="ebiz" />
                  </td>
               </tr>
            </table>

            <table width="900">
               <tr>
                  <td>
                     <h5>
                        <xsl:value-of select="//lang_blocks/p0" />
                     </h5>

                     <br />

                     <img src="../images/traineeline.gif" />

                     <br />

                     <h5>
                        <xsl:value-of select="//lang_blocks/head" />
                     </h5>

                     <p>
                        <xsl:value-of select="//lang_blocks/p1" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p2" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p3" />
                     </p>

                     <!--<h5>
                        <xsl:value-of select="//lang_blocks/p4" />
                     </h5>

                     <p>
                        <xsl:value-of select="//lang_blocks/p5" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p6" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p7" />
                     </p>

                     <ul>
                        <li>
                           <xsl:value-of select="//lang_blocks/p8" />
                        </li>

                        <li>
                           <xsl:value-of select="//lang_blocks/p9" />
                        </li>

                        <li>
                           <xsl:value-of select="//lang_blocks/p10" />
                        </li>
                     </ul>

                     <p>
                        <xsl:value-of select="//lang_blocks/p11" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p12" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p13" />
                     </p>-->

                     <hr />

                     <h5>
                        <xsl:value-of select="//lang_blocks/p14" />
                     </h5>

                     <p>
                        <xsl:value-of select="//lang_blocks/p15" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p16" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p17" />
                     </p>

                     <hr />

                     <h5>
                        <xsl:value-of select="//lang_blocks/p18" />
                     </h5>

                     <p>
                        <xsl:value-of select="//lang_blocks/p19" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p20" />
                     </p>

                     <table class="assign" cellpadding="5">
                        <tr>
                           <td>
                              <p>
                                 <xsl:value-of select="//lang_blocks/p21" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p22" />

                                 <b>
                                    <xsl:value-of select="//lang_blocks/p23" />
                                 </b>

                                 <xsl:value-of select="//lang_blocks/p24" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p25" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p26" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p27" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p28" />
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p29" />

                                 <br />

                                 <a onclick="" href="https://www.clubshop.com/remove-me.xml" target="_blank">https://www.clubshop.com/change-Shopper.html</a>
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p30" />

                                 <br />

                                 <a onclick="" href="https://www.clubshop.com/remove-me.xml" target="_blank">https://www.clubshop.com/remove-me.xml</a>
                              </p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p31" />
                              </p>
                           </td>
                        </tr>
                     </table>

                     <p>
                        <xsl:value-of select="//lang_blocks/p32" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p33" />
                     </p>

                     <table class="assign" cellpadding="5">
                        <tr>
                           <td class="assign">
                              <xsl:value-of select="//lang_blocks/assignment" />
                           </td>
                        </tr>

                        <tr>
                           <td>
                              <p><xsl:value-of select="//lang_blocks/p34" /></p>

                              <div align="center">
                                 <form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">

                                 <textarea name="email" cols="50" rows="5"></textarea>

                                 <br/>

                                 <input name="submit" type="submit" value="Submit"/>

                                 <input type="reset" name="Reset" value="Reset"/>

                                 <input type="hidden" name="action" value="confirm"/>

                                 <input type="hidden" name="type" value="8"/>

                                 <input type="hidden" name="u_key" value="ewps"/>

                                 </form>
                              </div>
                           </td>
                        </tr>
                     </table>

                     <br />

                     <table class="assign" cellpadding="5">
                        <tr>
                           <td>
                              <h5>
                                 <xsl:value-of select="//lang_blocks/course" />
                              </h5>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p35" />
                              </p>

                              <ul>
                                <!-- <li>
                                    <b>
                                       <xsl:value-of select="//lang_blocks/p36" />
                                    </b>

                                    <a href="lesson_3.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_3/lesson_3.xml</a>
                                 </li>-->

                                 <br />

                                <!-- 9/30/09 <li>
                                    <b>
                                       <xsl:value-of select="//lang_blocks/p37" />
                                    </b>

                                    <a href="lesson_6.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_3/lesson_6.xml</a>
                                 </li>

                                 <br />-->

                                 <li>
                                    <b>
                                       <xsl:value-of select="//lang_blocks/p38" />
                                    </b>

                                    <a href="lesson_6.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_3/lesson_6.xml</a>
                                 </li>

                                 <br />

                                 <li>
                                    <b>
                                       <xsl:value-of select="//lang_blocks/p39" />
                                    </b>

                                    <a href="lesson_7.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_3/lesson_7.xml</a>
                                 </li>

                                 <br />
                              </ul>
                              
                              
                                  <hr />
                                  
                                  
                                  <h5><xsl:value-of select="//lang_blocks/p43" /></h5>
                           </td>
                        </tr>
                     </table>
                     
                     

                     <hr />

                     <div align="center">
                        <p>
                        <a href="index.xml">
                           <xsl:value-of select="//lang_blocks/tr3index" />
                        </a>

                        | 
                        <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/25">
                           <xsl:value-of select="//lang_blocks/lesson" />
                        </a>
                        </p>
                     </div>
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

