<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #4F5736;
	width: 100%;
}

td.quote{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #4F5736;
	border-bottom : Gray;
}
hr{  color: #4F5736;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: medium;
 font-weight: bold;
 color: #4F5736;
 text-indent: 5%;
  }
h5 {color: #4F5736;
}

tr.acolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 background-color: #DEDEBC;
 text-align: left;
 }
 tr.bcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 background-color: #A2A251;
 text-align: left;
 }

</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t3_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1" /></p>


<sub><xsl:value-of select="//lang_blocks/p2" /></sub>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>

<p><xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p10" /></sub>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>

<p><xsl:value-of select="//lang_blocks/p13" /></p>

<p><xsl:value-of select="//lang_blocks/p14" /></p>
<!--<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>-->
<hr/>
<sub><xsl:value-of select="//lang_blocks/p17" /></sub>
<p><xsl:value-of select="//lang_blocks/p18" /></p>
<p><xsl:value-of select="//lang_blocks/p19" /></p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p20" /></sub>

<p><xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>


<table>
<tr>

<td>
<table cellpadding="3" width="475">
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p26" /></td>
<td><xsl:value-of select="//lang_blocks/p32" /></td>
<td><xsl:value-of select="//lang_blocks/p27" /> </td></tr>
<tr class="bcolor"><td>75</td><td>Non-Builder</td><td>0%</td></tr>
<tr class="acolor"><td>75</td><td>Builder</td><td>34%</td></tr>
<tr class="bcolor"><td>150</td><td>Builder</td><td>36%</td></tr>

<tr class="acolor"><td>250</td><td>Builder</td><td>39%</td></tr>



<tr class="bcolor"><td>400</td><td>Strategist</td><td>42%</td></tr>
<tr class="acolor"><td>600</td><td>Strategist</td><td>46%</td></tr>
<tr class="bcolor"><td>1,000</td><td>Trainer</td><td>49%</td></tr>
<tr class="acolor"><td>1,500</td><td>Trainer</td><td>52%</td></tr>
<tr class="bcolor"><td>2,250</td><td>Marketing Director</td><td>56%</td></tr>
<tr class="acolor"><td>3,250</td><td>Promotions Director</td><td>59%</td></tr>
<tr class="bcolor"><td>4,500</td><td>Chief Director</td><td>62%</td></tr>
<tr class="acolor"><td>6,000</td><td>Non-Chief</td><td>65%</td></tr>
<tr class="bcolor"><td>6,000</td><td>Chief Director</td><td>67%</td></tr>
<tr class="acolor"><td>Leadership Bonus</td><td></td><td>72%</td></tr>
</table>
</td>

<td><p><xsl:value-of select="//lang_blocks/p24" /></p>
<br/>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<br/>
<p><xsl:value-of select="//lang_blocks/p28" /></p>

</td></tr></table>

<hr/>


<sub><xsl:value-of select="//lang_blocks/p29" /></sub>

<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>




<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_3/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_6.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>