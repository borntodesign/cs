<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
h5 { color: #666600;}
</style>
</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t3_logo.jpg" alt="ebiz"/></td>

</tr>
</table>
<table width="900">
<tr><td>

<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>


<table><tr><td class="quote">
<img src="http://www.clubshop.com/vip/ebusiness/images/carnegie.gif" width="80" height="105" alt="Dale Carnegie"/></td>
<td><p class="quote"><xsl:value-of select="//lang_blocks/quote" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/quote2" /></b></p>
</td></tr></table>


<!--<p>
  <img src="../images/under_construction.png" height="117" width="100" alt="Under Construction" /> 
  <xsl:text /> 
  <xsl:value-of select="//lang_blocks/under_construction" /> 
  </p>
  <br /> 
  <br />-->





<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_1.xml">
<xsl:value-of select="//lang_blocks/p2" /></a></li></ul>

<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_2.xml">
<xsl:value-of select="//lang_blocks/p3" /></a></li></ul>

<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_3.xml">
<xsl:value-of select="//lang_blocks/p4" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_4.xml">
<xsl:value-of select="//lang_blocks/p5" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_5.xml">
<xsl:value-of select="//lang_blocks/p6" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_6.xml">
<xsl:value-of select="//lang_blocks/p7" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_7.xml">
<xsl:value-of select="//lang_blocks/p8" /></a></li></ul>

<ul><li><a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/25">
<xsl:value-of select="//lang_blocks/p9" /></a></li></ul>








</td></tr></table>

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_1.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

