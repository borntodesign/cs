<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #4F5736;
	width: 100%;
}

td.quote{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #4F5736;
	border-bottom : Gray;
}
hr{  color: #4F5736;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #4F5736;
 text-indent: 5%;
  }
h5 {color: #4F5736;
}

</style>
</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="../images/t3_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="../images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<table width="100%" align="center">
<tr><td class="quote">
<div align="center"><xsl:value-of select="//lang_blocks/p1" /></div>
</td></tr>
</table>


<ol>
<li><xsl:value-of select="//lang_blocks/p2" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p3" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p4" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p5" /></li><br/>
</ol>

<!--
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>











<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>

<table>
<tr>
<td class="a"><h5><xsl:value-of select="//lang_blocks/p6" /></h5><b><xsl:value-of select="//lang_blocks/p13" /></b><br/>
<xsl:value-of select="//lang_blocks/p14" /><br/>
<xsl:value-of select="//lang_blocks/p15" /><br/>
<xsl:value-of select="//lang_blocks/p16" /><br/>
<xsl:value-of select="//lang_blocks/p17" /><br/>
</td></tr>

<tr>
<td><b><xsl:value-of select="//lang_blocks/p18" /></b></td>
</tr>

<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p26" /></b><br/>
<xsl:value-of select="//lang_blocks/p27" /><br/>
<xsl:value-of select="//lang_blocks/p28" /><br/>
<xsl:value-of select="//lang_blocks/p29" /><br/>
<xsl:value-of select="//lang_blocks/p30" /><br/>
<xsl:value-of select="//lang_blocks/p31" /><br/>
<xsl:value-of select="//lang_blocks/p32" /><br/>
<xsl:value-of select="//lang_blocks/p33" /><br/>

</td></tr>
<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p35" /></b></td></tr>

<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p36" /></b></td></tr>

<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p41" /></b><br/>
<xsl:value-of select="//lang_blocks/p42" /></td></tr>



<tr>
<td><hr/><hr/></td>
</tr>


<tr>
<td class="a"><h5><xsl:value-of select="//lang_blocks/p5" /></h5><b><xsl:value-of select="//lang_blocks/p7" /></b><br/>

<xsl:value-of select="//lang_blocks/p8" /><br/>
<xsl:value-of select="//lang_blocks/p9" /><br/>
<xsl:value-of select="//lang_blocks/p10" /><br/>
<xsl:value-of select="//lang_blocks/p11" /><br/>
<xsl:value-of select="//lang_blocks/p12" /><br/>

</td>
</tr>

<tr>
<td class="a"><b><xsl:value-of select="//lang_blocks/p19" /></b><br/>
<xsl:value-of select="//lang_blocks/p20" /><br/>
<div class="indent"><xsl:value-of select="//lang_blocks/p21" /></div>
<div class="indent"><xsl:value-of select="//lang_blocks/p22" /></div>
<xsl:value-of select="//lang_blocks/p23" /><br/>
<xsl:value-of select="//lang_blocks/p24" /><br/>

</td>
</tr>

<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p35" /></b> <br/>
<xsl:value-of select="//lang_blocks/p34" /></td></tr>

<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p36" /></b> <br/>
<xsl:value-of select="//lang_blocks/p34" /></td></tr>



<tr><td class="a"><b><xsl:value-of select="//lang_blocks/p37" /></b><br/>
<xsl:value-of select="//lang_blocks/p38" /><br/>
<xsl:value-of select="//lang_blocks/p39" /><br/>


</td></tr>
</table>
</td></tr>
</table>
<br/>
<br/>

<div align="center"><b><xsl:value-of select="//lang_blocks/p43" /></b><br/><br/>
<br/>


<table width="100%" align="center">
<tr><td class="quote" align="center"><xsl:value-of select="//lang_blocks/p44" /></td></tr>
</table></div>
<br/>
-->

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_3/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>


