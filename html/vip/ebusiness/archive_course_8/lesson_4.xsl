<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c8_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t8_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<table>
<tr>
<td><img src="../images/tinycart2.gif" height="67" width="67" alt="shopping cart"/></td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr>
</table>


<h5><xsl:value-of select="//lang_blocks/p2" /></h5>

<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr><td>
<ol>
<li><a onclick="" href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p5" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p6" /></li>
<li><xsl:value-of select="//lang_blocks/p7" /></li>
<li><xsl:value-of select="//lang_blocks/p8" /></li>
</ol></td></tr>
</table>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p9" /></h5>
<!--<img src="../images/c8_l4b.gif" height="100" width="341" alt="Marketing Director Activity Report Communications"/>-->

<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>


<ol>
<li><b><xsl:value-of select="//lang_blocks/p12" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13" /></li>
<li><b><xsl:value-of select="//lang_blocks/p14" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15" /></li>
<li><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /></li>
<!--<li><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:value-of select="//lang_blocks/p19" /></li>-->
<li><b><xsl:value-of select="//lang_blocks/p20" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p21" /></li>
<!--<li><b><xsl:value-of select="//lang_blocks/p22" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23" /></li>-->
</ol>
<hr/>



<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p24" /></p>
</td></tr></table>

<h5><xsl:value-of select="//lang_blocks/p25" /></h5>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr>
<td>
<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" /></p>
<p>
<form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
<div  align="center">
<textarea name="test" cols="50" rows="5">
</textarea><br/>
<input type="submit" value="Submit"/>
<input type="reset" name="Reset" value="Reset"/>
<input type="hidden" name="action" value="confirm"/>
<input type="hidden" name="type" value="10"/>
<input type="hidden" name="u_key" value="dmtps"/>
</div>

</form>
</p>
</td></tr></table>
            
<hr/>
<table class="xcolor">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p31"/></h5>
<p><xsl:value-of select="//lang_blocks/p29"/></p>
<p><xsl:value-of select="//lang_blocks/p30"/></p>

<ul>
<!--<li><xsl:value-of select="//lang_blocks/r1"/></li>-->
<li><xsl:value-of select="//lang_blocks/r2"/></li>
<li><xsl:value-of select="//lang_blocks/r14"/></li>
<li><xsl:value-of select="//lang_blocks/r3"/></li>
<li><xsl:value-of select="//lang_blocks/r4"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<!--<li><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_8/MD.xml"><xsl:value-of select="//lang_blocks/p32"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33"/></li>-->
<li><xsl:value-of select="//lang_blocks/r6"/></li>
<li><xsl:value-of select="//lang_blocks/r7"/></li>
<li><xsl:value-of select="//lang_blocks/r8"/></li>
<li><xsl:value-of select="//lang_blocks/r9"/>
(<a href="/vip/ebusiness/nforgsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/r9a"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/r9b"/><xsl:value-of select="//lang_blocks/r9c"/>

<a onclick="" href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/r10"/></a></li>
<li><xsl:value-of select="//lang_blocks/r11"/></li>
<li><xsl:value-of select="//lang_blocks/p34"/></li>
<li><xsl:value-of select="//lang_blocks/p35"/><a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /> </li>
<li><xsl:value-of select="//lang_blocks/r12"/></li>


</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->

</td></tr></table>





<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/4294">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>