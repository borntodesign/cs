<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c8_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t8_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>

<hr/>

<h5><img src="../images/c8_l1b.gif" height="56" width="60" alt="sales"/><xsl:value-of select="//lang_blocks/p6"/></h5>
<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>
<p><xsl:value-of select="//lang_blocks/p9"/> </p>
<p><xsl:value-of select="//lang_blocks/p10"/></p>

<table class="xcolor">
<tr>
<td><img src="../images/vip_pay100.gif" height="125" width="100" alt="vippay"/>
<img src="../images/100pp.gif" height="60" width="325" alt="100pp"/></td>
</tr>

<tr>
<td><img src="../images/vip_pay1000.gif" height="125" width="100" alt="vippay2"/>
<img src="../images/1000pp.gif" height="60" width="400" alt="1000pp"/></td>

</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p12"/></h5>


<ul>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<li><xsl:value-of select="//lang_blocks/p14"/></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>
<li><xsl:value-of select="//lang_blocks/p17"/></li>
</ul>

<hr/>
<table class="xcolor">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p18"/></h5>
<p><xsl:value-of select="//lang_blocks/p19"/></p>
<p><xsl:value-of select="//lang_blocks/p20"/></p>

<ul>
<!--<li><xsl:value-of select="//lang_blocks/r1"/></li>-->
<li><xsl:value-of select="//lang_blocks/r2"/></li>
<li><xsl:value-of select="//lang_blocks/r14"/></li>
<li><xsl:value-of select="//lang_blocks/r3"/></li>
<li><xsl:value-of select="//lang_blocks/r4"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><xsl:value-of select="//lang_blocks/r6"/></li>
<li><xsl:value-of select="//lang_blocks/r7"/></li>
<li><xsl:value-of select="//lang_blocks/r8"/></li>
<li><xsl:value-of select="//lang_blocks/r9"/>
(<a href="/vip/ebusiness/nforgsales_list.xml"><xsl:value-of select="//lang_blocks/r9a"/></a><xsl:value-of select="//lang_blocks/r9b"/><xsl:value-of select="//lang_blocks/r9c"/>
<a onclick="" href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/r10"/></a></li>
<li><xsl:value-of select="//lang_blocks/r11"/></li>

<li><xsl:value-of select="//lang_blocks/p21"/></li>
<li><xsl:value-of select="//lang_blocks/r12"/></li>
<li><xsl:value-of select="//lang_blocks/p22"/> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:value-of select="//lang_blocks/nfpsales3" /></li>


</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->
</td></tr></table>








<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>