<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c8_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t8_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<table>
<tr>
<td><img src="../images/c8_l3a.gif" height="148" width="123" alt="ideas"/></td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr>
</table>


<!--<h5><xsl:value-of select="//lang_blocks/p2" /></h5>

<p><xsl:value-of select="//lang_blocks/p4" /></p>
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr><td><p><a onclick="" href="http://www.clubshop.com/cgi/md_research.cgi"><xsl:value-of select="//lang_blocks/p5" /></a><xsl:value-of select="//lang_blocks/p6" /></p></td></tr>
</table>
-->

<h5><xsl:value-of select="//lang_blocks/p7" /></h5>
<p><xsl:value-of select="//lang_blocks/p8" /></p>

<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><a onclick="" href="http://www.clubshop.com/cgi/member_actions.cgi?type=12;u_key=swdms"><xsl:value-of select="//lang_blocks/p11" /></a></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>

</td>
</tr></table>-->







<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_8/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_4.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>