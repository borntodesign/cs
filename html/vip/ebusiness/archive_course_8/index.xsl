<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c8_ebiz.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t8_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>

<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>


<table><tr><td>
<img src="http://www.clubshop.com/vip/ebusiness/images/maxwell.gif" width="104" height="104" alt="John C. Maxwell"/></td>
<td class="quote"><xsl:value-of select="//lang_blocks/quote" /><br/><b><xsl:value-of select="//lang_blocks/quote2" /></b>
</td></tr></table>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt=""/></p>

<h5><xsl:value-of select="//lang_blocks/head_8" /></h5>


<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_1.xml"><xsl:value-of select="//lang_blocks/p2" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_2.xml"><xsl:value-of select="//lang_blocks/p3" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_3.xml"><xsl:value-of select="//lang_blocks/p4" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_4.xml"><xsl:value-of select="//lang_blocks/p5" /></a></li></ul>
<ul><li><a href="https://www.clubshop.com/cgi/e/ebiz-test.cgi/4294"><xsl:value-of select="//lang_blocks/p6" /></a></li></ul>






</td></tr></table>

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_1.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
