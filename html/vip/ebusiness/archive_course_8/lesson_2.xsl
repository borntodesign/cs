<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c8_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t8_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<table>
<tr>
<td><img src="../images/c8_l2c.gif" height="261" width="223" alt="Band Leader"/></td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr>
</table>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p2" /></h5>

<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>

<hr/>

<p><xsl:value-of select="//lang_blocks/p7" /></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p8" /></p></td>
</tr>
</table>

<hr/>
<h5><img src="../images/c8_l2b.gif" height="120" width="100" alt="Fill Your Cup Up"/>
<xsl:value-of select="//lang_blocks/p9" /></h5>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p12" /></li>
<li><xsl:value-of select="//lang_blocks/p13" /></li>
<li><xsl:value-of select="//lang_blocks/p14" /></li>
<li><xsl:value-of select="//lang_blocks/p15" /></li>
<li><xsl:value-of select="//lang_blocks/p16" /></li>
<li><xsl:value-of select="//lang_blocks/p17" /></li>
<!--<li><xsl:value-of select="//lang_blocks/p18" /></li>-->
<!--<li><xsl:value-of select="//lang_blocks/p19" /></li>-->

</ul>
<!--
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p></td></tr>
</table>-->


<hr/>

<h5><xsl:value-of select="//lang_blocks/p22" /></h5>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<ol>

<li><xsl:value-of select="//lang_blocks/p24" /></li>
<!--<li><xsl:value-of select="//lang_blocks/p25" /></li>-->
<li><xsl:value-of select="//lang_blocks/p26" /></li>
</ol>
<p><b><xsl:value-of select="//lang_blocks/p27" /></b><xsl:value-of select="//lang_blocks/p28" /></p>


<hr/>

<p><xsl:value-of select="//lang_blocks/p29" /></p>

<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<p><xsl:value-of select="//lang_blocks/p32" /><b><xsl:value-of select="//lang_blocks/p33" /></b></p>





<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_8/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_8/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>