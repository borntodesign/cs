<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
<title><xsl:value-of select="//lang_blocks/title" /></title>
<style type="text/css">
body{ 
font-family: Arial, Helvetica, sans-serif;
font-size: small;
 }
.header{font-weight: bold;
 }
</style>
   </head>

<body>
<p class="header"><xsl:value-of select="//lang_blocks/title" /></p>

<div class="header"><xsl:value-of select="//lang_blocks/head2" /></div>

<ol>
<li><xsl:value-of select="//lang_blocks/goals1" /></li>
<li><xsl:value-of select="//lang_blocks/goals2" /></li>
<li><xsl:value-of select="//lang_blocks/goals3" /></li>
<li><xsl:value-of select="//lang_blocks/goals4" /></li>
<li><xsl:value-of select="//lang_blocks/goals5" /></li>
<li><xsl:value-of select="//lang_blocks/goals6" /></li>
<li><xsl:value-of select="//lang_blocks/goals7" /></li>
<li><xsl:value-of select="//lang_blocks/goals8" /></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
</ol>


<div class="header"><xsl:value-of select="//lang_blocks/head4" /></div>

<ol>
<li><xsl:value-of select="//lang_blocks/daily1" /></li>
<li><xsl:value-of select="//lang_blocks/daily2" /></li>
<li><xsl:value-of select="//lang_blocks/daily3" /></li>
<li><xsl:value-of select="//lang_blocks/daily4" /></li>
<li><xsl:value-of select="//lang_blocks/daily5" /></li>
</ol>

<p class="header"><xsl:value-of select="//lang_blocks/head5" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/mon" /></li>
<li><xsl:value-of select="//lang_blocks/tues" /></li>
<li><xsl:value-of select="//lang_blocks/wed" /></li>
<li><xsl:value-of select="//lang_blocks/thurs" /></li>
<li><xsl:value-of select="//lang_blocks/fri" /></li>
<li><xsl:value-of select="//lang_blocks/sat" /></li>
<li><xsl:value-of select="//lang_blocks/sun" /></li>
</ul>

<p class="header"><xsl:value-of select="//lang_blocks/monthly" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/monthly1" /></li>
<li><xsl:value-of select="//lang_blocks/monthly2" /></li>
<li><xsl:value-of select="//lang_blocks/monthly3" /></li>

<li><xsl:value-of select="//lang_blocks/monthly7" /></li>

</ul>

<p class="header"><xsl:value-of select="//lang_blocks/head7" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/todo1" /></li>
<li><xsl:value-of select="//lang_blocks/todo2" /></li>
<li><xsl:value-of select="//lang_blocks/todo3" /></li>
<li><xsl:value-of select="//lang_blocks/todo4" /></li>
<li><xsl:value-of select="//lang_blocks/todo5" /></li>
<li><xsl:value-of select="//lang_blocks/todo6" /></li>
<li><xsl:value-of select="//lang_blocks/todo7" /></li>

</ul>

<p class="header"><xsl:value-of select="//lang_blocks/head8" /></p>

<ol>
<li><xsl:value-of select="//lang_blocks/future1" /></li>
<li><xsl:value-of select="//lang_blocks/future2" /></li>
<li><xsl:value-of select="//lang_blocks/future3" /></li>
<li><xsl:value-of select="//lang_blocks/future4" /></li>
<li><xsl:value-of select="//lang_blocks/future5" /></li>
<li><xsl:value-of select="//lang_blocks/future6" /></li>
<li><xsl:value-of select="//lang_blocks/future7" /></li>
<li><xsl:value-of select="//lang_blocks/future8" /></li>
<li><xsl:value-of select="//lang_blocks/future9" /></li>
<li><xsl:value-of select="//lang_blocks/future10" /></li>
</ol>


</body>
</html>
</xsl:template>
</xsl:stylesheet>