<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Non-Fee Personal Sales</title>

<style>
h5 {font-family: Verdana, Arial, sans-serif;
font-size: 12px;
weight: bold;
}
p, td{font-family: Verdana, Arial, sans-serif;
font-size: 12px;}

</style>

</head>
<body>

<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<table>
<tr><td><img src="/vip/ebusiness/images/flags/au.gif" height="25" width="50" alt="australia"/></td><td></td><td><xsl:value-of select="//lang_blocks/p3"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/at.gif" height="25" width="50" alt="austia"/></td><td></td><td><xsl:value-of select="//lang_blocks/p4"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/be.gif" height="25" width="50" alt="belgium"/></td><td></td><td><xsl:value-of select="//lang_blocks/p5"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/ca.gif" height="25" width="50" alt="canada"/></td><td></td><td><xsl:value-of select="//lang_blocks/p6"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/dk.gif" height="25" width="50" alt="denmark"/></td><td></td><td><xsl:value-of select="//lang_blocks/p7"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/fr.gif" height="25" width="50" alt="france"/></td><td></td><td><xsl:value-of select="//lang_blocks/p8"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/de.gif" height="25" width="50" alt="germany"/></td><td></td><td><xsl:value-of select="//lang_blocks/p9"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/ie.gif" height="25" width="50" alt="ireland"/></td><td></td><td><xsl:value-of select="//lang_blocks/p10"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/it.gif" height="25" width="50" alt="italy"/></td><td></td><td><xsl:value-of select="//lang_blocks/p11"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/nl.gif" height="25" width="50" alt="netherlands"/></td><td></td><td><xsl:value-of select="//lang_blocks/p12"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/ch.gif" height="25" width="50" alt="switzerland"/></td><td></td><td><xsl:value-of select="//lang_blocks/p13"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/gb.gif" height="25" width="50" alt="uk"/></td><td></td><td><xsl:value-of select="//lang_blocks/p14"/></td></tr>
<tr><td><img src="/vip/ebusiness/images/flags/us.gif" height="25" width="50" alt="usa"/></td><td></td><td><xsl:value-of select="//lang_blocks/p15"/></td></tr>
</table>

<hr/>
<img src="http://www.clubshop.com/images/valid-xhtml10-blue.png " alt="passed validation"/>

</body></html>


</xsl:template>
</xsl:stylesheet>