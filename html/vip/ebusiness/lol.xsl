<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/intropage.css" rel="stylesheet" type="text/css"/>


<style>
.under{
		width:907px;
		height:245px;
		position:absolute;
		top: 310px;
		left:3px;
		background:url("underTable.jpg") no-repeat;
		z-index: 1000;	
}
.safety{
	background: #FFF;
	font-size: 1.5em
	color:#E05707;
	width:40%;
	text-align:center;	
	border:3px solid #E05707;
	margin:10px 0px 0px 325px;
}
</style>


</head>

<body>

	

	<div class="under">
	
		<div class="safety">
			<h1><xsl:value-of select="//lang_blocks/under_constructionA" /></h1>
			<p><xsl:value-of select="//lang_blocks/under_constructionB" /></p>
		</div>
	</div>

	


<table width="900" margin="25px">
<tr>
<td class="top"><img src="/vip/ebusiness/images/ebiz_header.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0" /></h5>

<p><b><xsl:value-of select="//lang_blocks/p0a" /><br/><a href="http://www.clubshop.com/referral_plan.html" target="_blank">http://www.clubshop.com/referral_plan.html</a></b></p>
<img src="/vip/ebusiness/images/traineeline.gif" /><!--
<p><img src="images/under_construction.png" height="117" width="100" alt="Under Construction"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/under_construction" /></p>
<br/><br/>-->


<!--<p><xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text><a onclick="" href="https://www.clubshop.com/vip/ebusiness/steps_10.html" target="_blank"><xsl:value-of select="//lang_blocks/p2" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p3"/></p> -->

<!--<p><xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text><a onclick="" href="https://www.clubshop.com/vip/ebusiness/recording_pg/ebiz_intro.html" target="_blank"><xsl:value-of select="//lang_blocks/p2" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p4"/></p>-->

<hr/>
<!--ebiz Courses-->


<table class="xcolor" width="100%">

<tr class="cell">
<td><b><xsl:value-of select="//lang_blocks/p44" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p45" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p46" /></b></td>
</tr>
<!--<tr class="acolor">
<td></td>
<td><a href="http://www.clubshop.com/cgi/coursecheck.cgi?page=sqla&amp;C=1"><xsl:value-of select="//lang_blocks/p7" /></a></td>
<td><xsl:value-of select="//lang_blocks/p8" /></td>
</tr>-->

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p9" /></td>
<td><a onclick="" href="course_1/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p10" /></a></td>
<td><xsl:value-of select="//lang_blocks/p10a" /></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p11" /></td>
<td><a onclick="" href="course_2/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p12" /></a></td>
<td><xsl:value-of select="//lang_blocks/p12a" /></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p13" /></td>
<td><a onclick="" href="course_3/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p14" /></a></td>
<td><xsl:value-of select="//lang_blocks/p14a" /></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p15" /></td>
<td><a onclick="" href="course_4/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p16" /></a></td>
<td><xsl:value-of select="//lang_blocks/p16a" /></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p17" /></td>
<td><a onclick="" href="course_5/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p18" /></a></td>
<td><xsl:value-of select="//lang_blocks/p18a" /></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td><a onclick="" href="course_6/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p20" /></a></td>
<td><xsl:value-of select="//lang_blocks/p20a" /></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p21" /></td>
<td><a onclick="" href="course_7/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p22" /></a></td>
<td><xsl:value-of select="//lang_blocks/p22a" /></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p23" /></td>
<td><a onclick="" href="course_8/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24" /></a></td>
<td><xsl:value-of select="//lang_blocks/p24a" /></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p25" /></td>
<td><a onclick="" href="course_9/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p26" /></a></td>
<td><xsl:value-of select="//lang_blocks/p26a" /></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p27" /></td>
<td><a onclick="" href="course_10/index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p28" /></a></td>
<td><xsl:value-of select="//lang_blocks/p28a" /></td>
</tr></table>
<!--<tr><td colspan="3">
<p><xsl:value-of select="//lang_blocks/p29" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/Direct_Percentage.html"> <xsl:value-of select="//lang_blocks/p30" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31" /></p>
</td></tr>-->
<hr/>
<h5><xsl:value-of select="//lang_blocks/p32" /></h5>



<!--Marketing Courses-->

<table class="xcolor">

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p51" /></td>
<td><a href="http://www.glocalincome.com/localmarketing/" target="_blank"><xsl:value-of select="//lang_blocks/plm" /></a></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p50" /></td>
<td><a href="http://www.glocalincome.com/localmarketing/pages/_text_training.xml" target="_blank"><xsl:value-of select="//lang_blocks/p34" /></a></td>
</tr>


<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/_marketing_ag/ag-index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p34a" /></a></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/prw.xml" target="_blank"><xsl:value-of select="//lang_blocks/p47" /></a></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/ba-index.xml" target="_blank"><xsl:value-of select="//lang_blocks/p35" /></a></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/images.html" target="_blank"><xsl:value-of select="//lang_blocks/p37" /></a></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/_marketing_search/lesson_1.xml" target="_blank"><xsl:value-of select="//lang_blocks/p38" /></a></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/banners/select.xsp" target="_blank"><xsl:value-of select="//lang_blocks/p48" /></a></td>
</tr>

<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/marketing_course/lesson_2.html" target="_blank"><xsl:value-of select="//lang_blocks/p39" /></a></td>
</tr>
<!--<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><a href="https://www.clubshop.com/vip/ebusiness/marketing_course/mom_training.html" target="_blank"><xsl:value-of select="//lang_blocks/p40" /></a></td>
</tr>-->
</table>
<hr/>

<!--Communication Courses-->
<h5><xsl:value-of select="//lang_blocks/p41" /></h5>

<table class="xcolor">

<tr class="bcolor">
<td><a href="https://www.clubshop.com/vip/ebusiness/infacta/index.html"><xsl:value-of select="//lang_blocks/p43" /></a></td>

</tr>
</table>






<hr/>
     
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
