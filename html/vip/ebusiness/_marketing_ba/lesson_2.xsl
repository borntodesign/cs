<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
    font-size: 100%;
    background-color: white;
    color: black;
}

h5 {
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }
p.c{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #FF0000;
  text-align : right;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
    font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    color: #060;
    font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    }
li{font-family: Verdana, Arial, sans-serif;
    font-size:80%;}
td{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: dashed;
    width: 500px;
    margin: 1em;
    }
td.a{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: dashed;
    width: 500px;
    margin: 2em;}
    
td.b{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: solid;
    width: 500px;
    margin: 2em;
    

}
    


</style>
         </head>

         <body>
            <div>
               <img src="../images/ebiz_header_mkt.gif" width="702" height="93" alt="" />
            </div>

            <h5>
               <xsl:value-of select="//lang_blocks/p1" />
            </h5>

            <p>
               <sub>
                  <xsl:value-of select="//lang_blocks/p2" />
               </sub>
            </p>

            <i>
               <xsl:value-of select="//lang_blocks/p3" />
            </i>

            <hr />

            <p>
               <xsl:value-of select="//lang_blocks/p4" />
            </p>

            <ol>
               <li>
                  <xsl:value-of select="//lang_blocks/p5" />
               </li>

               <li>
                  <xsl:value-of select="//lang_blocks/p6" />
               </li>

               <li>
                  <xsl:value-of select="//lang_blocks/p7" />
               </li>
            </ol>

            <p>
            <b><xsl:value-of select="//lang_blocks/p8" />: </b><xsl:value-of select="//lang_blocks/p9" />
            </p>

            <p>
               <xsl:value-of select="//lang_blocks/p10" />
            </p>

            <p>
            <xsl:value-of select="//lang_blocks/p11" />: <b>
               <xsl:value-of select="//lang_blocks/p12" />
            </b>

            -
            <xsl:value-of select="//lang_blocks/p13" />

            <b>
               <xsl:value-of select="//lang_blocks/p14" />
            </b>

            <xsl:value-of select="//lang_blocks/p15" />
            </p>

            <p>
               <b>
               <xsl:value-of select="//lang_blocks/p16" />: </b>

               <xsl:value-of select="//lang_blocks/p17" />
            </p>

            <hr />

            <sub>
               <xsl:value-of select="//lang_blocks/p18" />
            </sub>

            <p>
               <xsl:value-of select="//lang_blocks/p19" />
            </p>

            <ol>
               <li>
                  <xsl:value-of select="//lang_blocks/p20" />
               </li>

               <li>
                  <xsl:value-of select="//lang_blocks/p21" />

                  <a href="http://www.clubshop.com/manual/policies/spam.xml">
                     <xsl:value-of select="//lang_blocks/p22" />
                  </a>
               </li>
            </ol>

            <p>
               <b>
                  <a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?action=search">
                     <xsl:value-of select="//lang_blocks/p23" />
                  </a>
               </b>
            </p>

            <p>
               <xsl:value-of select="//lang_blocks/p24" />

               <b>
                  <xsl:value-of select="//lang_blocks/p25" />
               </b>

               <xsl:value-of select="//lang_blocks/p26" />

               <font color="#FF0000"> CLICK HERE </font>

               <xsl:value-of select="//lang_blocks/p27" />
            </p>

            <sub>EMAIL</sub>

            <table>
               <tr>
                  <td>
                     <p>
                        <b>
                        <xsl:value-of select="//lang_blocks/p28" />

                        : </b>

                        <xsl:value-of select="//lang_blocks/p29" />

                        <font color="#FF0000">
                        <xsl:value-of select="//lang_blocks/p30" />

                        ?</font>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p31" />

                        <font color="#ff0000"> (or FIRST NAME if you have it),</font>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p32" />

                        <font color="#ff0000"> YOUR NAME </font>

                        <xsl:value-of select="//lang_blocks/p33" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p34" />

                        <font color="#ff0000">(URL)</font>

                        <xsl:value-of select="//lang_blocks/p35" />
                     </p>

                     <p>
                        <img src="http://www.clubshop.com/banners/banner_468.gif" alt="clubshop.com" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p36" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p37" /><br/>

                        <a href="http://www.clubshop.com/ba/?referer=YOURIDNUMBER">CLICK HERE.  </a>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p38" />

                        <font color="#ff0000">PHONE NUMBER. </font>

                        <xsl:value-of select="//lang_blocks/p39" />
                     </p>
                     
                     <p><xsl:value-of select="//lang_blocks/p40" /></p>
                     <p><font color="#FF0000">Your Name<br/>Your Email Address</font></p>
                     
                     
                     <p><xsl:value-of select="//lang_blocks/p41" /> "<xsl:value-of select="//lang_blocks/p23" />"
                     <xsl:value-of select="//lang_blocks/p42" /><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="> CLICK HERE </a><xsl:value-of select="//lang_blocks/p43" /></p>
                     
                     
                     <p><font color="#FF0000">YOUR MAILING ADDRESS MUST BE SHOWN HERE</font></p>
                     <p></p>
                     
                  </td>
               </tr>
            </table>
            <p><xsl:value-of select="//lang_blocks/p44" /></p>
            <p><xsl:value-of select="//lang_blocks/p45" /><b>EBA-1.</b></p>

            
            <hr/>
            <sub><xsl:value-of select="//lang_blocks/p46" /></sub>
            
            <p><xsl:value-of select="//lang_blocks/p47" /></p>
            
            <dl><dt><xsl:value-of select="//lang_blocks/p48" />:</dt>
            <dd><xsl:value-of select="//lang_blocks/p49" /></dd></dl>
            
            <dl><dt><xsl:value-of select="//lang_blocks/p50" />:</dt>
            <dd><xsl:value-of select="//lang_blocks/p51" /></dd>
            </dl>
            <p><xsl:value-of select="//lang_blocks/p52" />: </p>
            
            <sub><b><xsl:value-of select="//lang_blocks/p53" /></b> </sub>
            
            
            
            <p><xsl:value-of select="//lang_blocks/p54" />(Website Name)<xsl:value-of select="//lang_blocks/p55" />?
            (Get name then ask to speak with them).
            </p>
            <p><b><xsl:value-of select="//lang_blocks/p56" /></b>: "This is Joe"</p>
            <p>Hi {Name}, this is {YOUR NAME} <xsl:value-of select="//lang_blocks/p57" />?</p>
            <table><tr>
            <td class="b"><b>Them</b></td>
            <td class="b"><b>You</b></td>
            </tr>
            <tr>
            <td class="b">"NO"</td>
            <td class="b">"Thanks for your time". Get off phone.</td>
            </tr>
            
            <tr>
            <td class="b">YES!</td>
            <td class="b">
            <p>"Great! I would like to send you some information on the ClubShop Mall's new Banner Advertising Program that will allow your website to simply and automatically refer a customer once, so that you can earn money each and every time they shop at hundreds of stores from around the world. What is your email address?" (Confirm the spelling of their email address)</p>
            <p>"Okay <font color="#FF000">{Name},</font> I will get that information right off to you. So you can identify my email, the subject will be "ClubShop Mall Banner Ad Program". Thanks for taking my call and I hope that we can do some business together!</p>
            <p>For even better results, take it one step further and ask them for a good time to get back with them and go over the information.</p>
            <p>"May I ask when do you feel you will have a chance to review the information I am sending? (Set up a time to call them after they will have a chance to review it, or if they say right away, set a time to talk to them in a few hours. )
            </p></td>
            </tr>
             <tr>
            <td class="b">Questions</td>
            
            <td class="b"><p>"Are you online right now or can you get online while I'm on the phone with you?"</p>
            <p><font color="#0066CC">YES - </font>"Okay great, this will just take a few minutes". Give them the URL to your Banner Ad marketing website and be sure to include your ID number. Then review the site with them and answer any questions they may have. Emphasize the benefits listed below and that there is no cost or obligation to become a Member or use the program.)</p>
            <p><font color="#FF0000">NO - </font>"Okay no problem. I'll just send you the information by email. So you can identify my email, the subject will be "ClubShop Mall Banner Ad Program". Thanks for taking my call and I hope that we can do some business together!</p>
          
            
     
            </td></tr></table>
            <sub><xsl:value-of select="//lang_blocks/p58" /></sub>
            <ul>
            <li><xsl:value-of select="//lang_blocks/p59" /></li>
            <li><xsl:value-of select="//lang_blocks/p60" /></li>
            <li><xsl:value-of select="//lang_blocks/p61" /></li>
            <li><xsl:value-of select="//lang_blocks/p62" /></li>
            <li><xsl:value-of select="//lang_blocks/p63" /></li>
                  <li><xsl:value-of select="//lang_blocks/p65" /></li>
            </ul>
            
            <table><tr>
            
            <td>
            <sub><xsl:value-of select="//lang_blocks/p66" /></sub>
            <p><xsl:value-of select="//lang_blocks/p67" /><font color="#FF0000"> CLICK HERE </font>
            <xsl:value-of select="//lang_blocks/p68" /></p><hr/>
            <p><b><xsl:value-of select="//lang_blocks/p69" />: </b><xsl:value-of select="//lang_blocks/p70" /></p>
            <p>Hello <font color="#FF0000">FIRST NAME,</font></p>
            <p><xsl:value-of select="//lang_blocks/p71" /></p>
            <p><xsl:value-of select="//lang_blocks/p72" /></p>
            <p>
            <img src="http://www.clubshop.com/banners/banner_468.gif" alt="clubshop.com" /></p>
        
        <p>
                        <xsl:value-of select="//lang_blocks/p36" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p37" /><a href="http://www.clubshop.com/ba/?referer=YOURIDNUMBER">CLICK HERE.  </a>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p38" />

                        <font color="#ff0000">PHONE NUMBER. </font>

                        <xsl:value-of select="//lang_blocks/p39" />
                     </p>
                     
                     <p><xsl:value-of select="//lang_blocks/p40" /></p>
                     <p><font color="#FF0000">Your Name<br/>Your Email Address</font></p>
                     
                     
                     <p><xsl:value-of select="//lang_blocks/p41" /> "<xsl:value-of select="//lang_blocks/p23" />"
                     <xsl:value-of select="//lang_blocks/p42" /><a href="http://www.clubshop.com/cgi/do_not_solicit.cgi?emailaddress="> CLICK HERE </a><xsl:value-of select="//lang_blocks/p43" /></p>
                     
                     
                     <p><font color="#FF0000">YOUR MAILING ADDRESS MUST BE SHOWN HERE</font></p>
                     
            </td>
            
            </tr>
           
            </table>
            <dl><dt><xsl:value-of select="//lang_blocks/p73" /></dt>
            <dd><xsl:value-of select="//lang_blocks/p74" /></dd>
            </dl>
            
            
            <hr/>
            
            <sub><xsl:value-of select="//lang_blocks/p75" /></sub>
     
            <p><xsl:value-of select="//lang_blocks/p76" /></p>
            
            <p><xsl:value-of select="//lang_blocks/p77" /></p>
            
             <p><xsl:value-of select="//lang_blocks/p78" /></p>
             <table><tr><td>
              <p><img src="http://www.clubshop.com/banners/banner_468.gif" alt="clubshop.com" /></p>
              <p class="c"><font color="#FF0000">DATE: </font></p>
              <p>
                        <xsl:value-of select="//lang_blocks/p31" />

                        <font color="#ff0000"> (or FIRST NAME if you have it),</font>
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p32" />

                        <font color="#ff0000"> YOUR NAME </font>

                        <xsl:value-of select="//lang_blocks/p33" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p34" />

                        <font color="#ff0000">(URL)</font>

                        <xsl:value-of select="//lang_blocks/p35" />
                     </p>

<p> <xsl:value-of select="//lang_blocks/p36" /></p>
<p> <xsl:value-of select="//lang_blocks/p79" />:</p>
<p> <xsl:value-of select="//lang_blocks/p80" /><font color="#FF0000">YOUR ID NUMBER</font></p>
             
            <p>
                        <xsl:value-of select="//lang_blocks/p38" />

                        <font color="#ff0000">PHONE NUMBER. </font>

                        <xsl:value-of select="//lang_blocks/p39" />
                     </p>
                     
                     <p><xsl:value-of select="//lang_blocks/p40" /></p>
                     <p><font color="#FF0000">Your Name<br/>Your Email Address</font></p>
                     
                     
                     </td></tr></table>
            
            
            
            <hr/>
            

            <p style="text-align: center">
            <a href="../ba-index.xml">
               Back to Index Page
            </a>

            | 
            <a href="lesson_1.xml">
              Back to Lesson 1            </a>
              |
              
             <a href="lesson_3.xml">
             On to Lesson 3
               
            </a>

 
            </p>

            <hr />

            <p style="text-align: right">
               <img src="/images/copyright_.gif" width="173" height="19" alt="copyright" />
            </p>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

