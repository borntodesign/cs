<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}


</style>
</head>

<body>
<div><img src="../images/ebiz_header_mkt.gif" width="702" height="93" alt="" /></div>

                     <h5><xsl:value-of select="//lang_blocks/p1" />                    </h5>

                     <p><sub><xsl:value-of select="//lang_blocks/p2" /></sub></p>

                     <i>
                        <xsl:value-of select="//lang_blocks/p3" />
                     </i>

                     <hr />

                     <p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text><a href="example2.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p5a" /></a>

<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5b" /><xsl:text> </xsl:text><a href="example2.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p5c" /></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5d" /></p>

<p><xsl:value-of select="//lang_blocks/p6" /></p>

<p><xsl:value-of select="//lang_blocks/p7" /></p>

<dl>
<dt><xsl:value-of select="//lang_blocks/p8" />:</dt>
                        
<dd><xsl:value-of select="//lang_blocks/p9" /></dd>
</dl>

<dl>
<dt><xsl:value-of select="//lang_blocks/p10" />:</dt>
<dd><xsl:value-of select="//lang_blocks/p11" /></dd>
</dl>
<dl>
<dt><xsl:value-of select="//lang_blocks/p12" />:</dt>
<dd><xsl:value-of select="//lang_blocks/p13" /><xsl:text> </xsl:text><a href="example1.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p13a" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13b" /></dd>
</dl>
<dl>
<dt><xsl:value-of select="//lang_blocks/p14" />:</dt>
<dd><xsl:value-of select="//lang_blocks/p15" /></dd>
</dl>
                

<p><xsl:value-of select="//lang_blocks/p16" /></p>
<p><b><xsl:value-of select="//lang_blocks/p17" />:</b><xsl:text></xsl:text><xsl:value-of select="//lang_blocks/p18" /></p>

<p><xsl:value-of select="//lang_blocks/p19" /></p>

<p><xsl:value-of select="//lang_blocks/p20" /></p>

<p><xsl:value-of select="//lang_blocks/p21" /></p>

<p><b><xsl:value-of select="//lang_blocks/p22" />:</b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p23" /></p>

<dl>
<dt><xsl:value-of select="//lang_blocks/p24" />:</dt>
<dd><xsl:value-of select="//lang_blocks/p25" /></dd>
</dl>
                     
<p><xsl:value-of select="//lang_blocks/p25a" /></p>

<p><b><xsl:value-of select="//lang_blocks/p26" />:</b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27" /></p>

<p><xsl:value-of select="//lang_blocks/p28" /></p>
<p><xsl:value-of select="//lang_blocks/p29" />:<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p30" /> <xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/policies/spam.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p30a" /></a></p>

<dl>
<dt><xsl:value-of select="//lang_blocks/p31" />:</dt>
<dd><xsl:value-of select="//lang_blocks/p32" /></dd>
</dl>
                     

<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /></p>

<p><b><xsl:value-of select="//lang_blocks/p35" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36" /></p>

<p><b><xsl:value-of select="//lang_blocks/p37" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38" /></p>

<p style="text-align: center"><a href="../ba-index.xml"><xsl:value-of select="//lang_blocks/p39" /></a> | <a href="lesson_2.xml"><xsl:value-of select="//lang_blocks/p40" /></a></p>
                           <br/>
                           <p style="text-align: right"><img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/></p>
                        
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

