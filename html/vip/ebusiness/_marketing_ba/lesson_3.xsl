<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
    font-size: 100%;
    background-color: white;
    color: black;
}

h5 {
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
    font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }
p.c{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #FF0000;
  text-align : right;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
    font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    color: #060;
    font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
    font-size:80%;
    }
li{font-family: Verdana, Arial, sans-serif;
    font-size:80%;}
td{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: dashed;
    width: 500px;
    margin: 1em;
    }
td.a{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: dashed;
    width: 500px;
    margin: 2em;}
    
td.b{
    Verdana, Arial, sans-serif;
    font-size:90%;
    border: solid;
    width: 500px;
    margin: 2em;
    

}
    


</style>
         </head>

         <body>
            <div>
               <img src="../images/ebiz_header_mkt.gif" width="702" height="93" alt="" />
            </div>

            <h5>
               <xsl:value-of select="//lang_blocks/p1" />
            </h5>

            <p><sub>
                  <xsl:value-of select="//lang_blocks/p2" />
               </sub>
            </p>

            <i>
               <xsl:value-of select="//lang_blocks/p3" />
            </i>

            <hr />

            <p>
               <xsl:value-of select="//lang_blocks/p4" />
            </p>

            <p><img src="http://www.clubshop.com/vip/ebusiness/images/group_list.gif" width="100" height="95" alt="group" /></p>
            <p><sub><xsl:value-of select="//lang_blocks/p5" /></sub></p>

               
                  <p><xsl:value-of select="//lang_blocks/p6" /></p>
             

               
                  <p><sub><xsl:value-of select="//lang_blocks/p7" /></sub></p>
           

            <p>
            <xsl:value-of select="//lang_blocks/p8" /></p>
            <p><sub><xsl:value-of select="//lang_blocks/p9" />
            </sub></p>

            <p>
               <xsl:value-of select="//lang_blocks/p10" />
            </p>

            <p>
            <xsl:value-of select="//lang_blocks/p11" /></p>
               
              <p> <xsl:value-of select="//lang_blocks/p12" /></p>
              <p><div align="left"><img src="http://www.clubshop.com/vip/ebusiness/images/followupcall.gif" width="75" height="108" alt="followup call" /></div></p>
            

         <p><sub>
            <xsl:value-of select="//lang_blocks/p13" /></sub></p>

            
               <p><xsl:value-of select="//lang_blocks/p14" />
            </p>

            <p><xsl:value-of select="//lang_blocks/p15" />
            </p>

            <ul><li>
            # of minutes collecting leads, # of leads, leads per minute</li>
<li># of EBA-1 emails sent, # of Members</li>
<li># of phone calls made, # of Members</li>
<li># of postal cards or letters sent, # of Members</li>
<li># of follow up emails sent, # of Members for each letter sent</li></ul>
            
            <p><xsl:value-of select="//lang_blocks/p15" /></p>
            
            
            <hr/><p><i><xsl:value-of select="//lang_blocks/p16" /></i></p>
            
            <p><sub><div align="center"><xsl:value-of select="//lang_blocks/p17" /></div></sub></p>

            <p><div align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/c5_l1a.gif" width="655" height="105" alt="apple trees"/></div></p>
<br/><br/><hr/>
            <p style="text-align: center">
            <a href="../ba-index.xml">
               Back to Index Page
            </a>

          
              |
              
             <a href="lesson_2.xml">
             Back to Lesson 2
               
            </a>

 
            </p>

            <hr />

            <p style="text-align: right">
               <img src="/images/copyright_.gif" width="173" height="19" alt="copyright" />
            </p>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

