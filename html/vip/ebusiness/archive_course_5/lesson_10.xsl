<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title>e-Business Institute</title>
<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--p,li{font-family: Verdana, Arial, sans-serif;
font-size: 12px;
}
h5{font-family: Verdana, Arial, sans-serif;
font-size: 1em;
}
a {
font-size: 12px;
}
.Nav_Green_Plain:link {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:visited {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:hover {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;	
	font-weight:bold;	
	text-decoration: underline;
}
.Nav_Green_Plain:active {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}


body {
   background-color: #245923;
   	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 20px;
	color: #333333;
}



td.back{background-image:url(/images/bg_header.jpg);}

.Green_Large_Bold_head {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #34420F;
	font-weight:bold;
}


.Green_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #485B15;
	font-weight:bold;
}
.footer_divider {
	font-size: 12px;
	text-transform:uppercase;
	font-weight: bold;
	color: #41712B;
}
.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FDB100;
	font-weight:bold;
	text-transform:uppercase;
}
.Green_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #779724;
	font-weight:bold;
}

ol.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
	
li.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
div.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F8FFF2;
		padding: 8px;
}

li.b {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #485B15;
	list-style: numeric;}

li.c {
	list-style: url("http://www.glocalincome.com/images/icons/icon_bullet_ltgreen.png");
}

li.d { 
	list-style: url("http://www.glocalincome.com/images/icons/icon_arrow.png");
}
-->
tr.a {font-family: Verdana, Arial;
font-size: 12px;
background-color: #f4f4f4;
}


</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<hr align="left" width="100%" size="1" color="#A35912" />
<h5><xsl:value-of select="//lang_blocks/p2"/></h5>




<p align="center" class="footer_divider"><xsl:value-of select="//lang_blocks/p3"/></p>
<p class="footer_divider"><xsl:value-of select="//lang_blocks/p4"/></p>


<ul>
<li class="d"><xsl:value-of select="//lang_blocks/p5"/></li>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>
</ul>


<ul><li class="c"><xsl:value-of select="//lang_blocks/p8"/></li></ul>





<ul>
<li class="d"><xsl:value-of select="//lang_blocks/p12"/></li>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">

<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p13"/></p>

<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>
</ul>


<ul><li class="c"><xsl:value-of select="//lang_blocks/p14"/></li></ul>



<p><xsl:value-of select="//lang_blocks/p15"/></p>





<p class="footer_divider"><xsl:value-of select="//lang_blocks/p16"/></p>

<p><xsl:value-of select="//lang_blocks/p17"/></p>
<ul><li class="d"><xsl:value-of select="//lang_blocks/p18"/></li>

<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_green_cell_phone.png" alt="Cell Phone" height="131" width="106" align="left"/><xsl:text> </xsl:text>
<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p19/*|//lang_blocks/p19/text()"/></p>

</td></tr></table>

<br/>
<ul>
<li class="c"><xsl:value-of select="//lang_blocks/p20"/></li><br/>
<li class="c"><xsl:value-of select="//lang_blocks/p21"/></li><br/>
<li class="c"><xsl:value-of select="//lang_blocks/p22"/></li>
</ul>

</ul>



<p class="footer_divider"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_email.png"/>

<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<p><xsl:value-of select="//lang_blocks/p26"/></p>
<p><xsl:value-of select="//lang_blocks/p27"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com" class="Nav_Green_Plain">http://www.glocalincome.com</a><xsl:text> </xsl:text>
<xsl:copy-of select="//lang_blocks/p28/*|//lang_blocks/p28/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p29"/></p>
<p><xsl:value-of select="//lang_blocks/p30"/></p>
<p><xsl:value-of select="//lang_blocks/p31"/></p>
<p><b><xsl:value-of select="//lang_blocks/p32"/></b></p>



</td></tr></table><br/>

<ul><li class="c"><xsl:value-of select="//lang_blocks/p33"/></li></ul>

</ul>
<p><xsl:value-of select="//lang_blocks/p34"/></p>


<hr align="left" width="100%" size="1" color="#A35912" />





<p align="center" class="footer_divider"><xsl:value-of select="//lang_blocks/p35"/></p>
<p class="footer_divider"><xsl:value-of select="//lang_blocks/p36"/></p>
<p><xsl:value-of select="//lang_blocks/p37"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>

<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p38"/></p>

</td></tr></table>
<br/>
<ul><li class="c"><xsl:value-of select="//lang_blocks/p39"/></li></ul>

</ul>


<p><xsl:value-of select="//lang_blocks/p40"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<img src="https://www.clubshop.com/vip/ebusiness/images/Skype.png"/>
<p><xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p41"/></p>

</td></tr></table>
<br/>
<ul><li class="c"><xsl:value-of select="//lang_blocks/p42"/></li></ul>

</ul>
<p><xsl:value-of select="//lang_blocks/p43"/></p>


<p class="footer_divider"><xsl:value-of select="//lang_blocks/p44"/></p>
<p><xsl:value-of select="//lang_blocks/p45"/></p>

<p><xsl:value-of select="//lang_blocks/p46"/></p>
<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_green_cell_phone.png" alt="Cell Phone" height="131" width="106" align="left"/><xsl:text> </xsl:text>
<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p47/*|//lang_blocks/p47/text()"/></p>

</td></tr></table>
<br/>

<ul>
<li class="c"><xsl:value-of select="//lang_blocks/p48"/></li><br/>
<li class="c"><xsl:value-of select="//lang_blocks/p48"/></li><br/>
<li class="c"><xsl:value-of select="//lang_blocks/p50"/></li>
</ul>

</ul>


<p class="footer_divider"><xsl:value-of select="//lang_blocks/p23"/></p>

<ul>
<br/>
<table width="70%" border="0" cellpadding="6" cellspacing="1" bgcolor="#9A6843"><tr><td bgcolor="#FFFFFF">
<p><img src="https://www.clubshop.com/vip/ebusiness/images/icons/icon_email.png"/>
<xsl:copy-of select="//lang_blocks/p6/*|//lang_blocks/p6/text()"/></p>
<p><xsl:value-of select="//lang_blocks/p51"/></p>

<p><xsl:value-of select="//lang_blocks/p52"/></p>
<p><xsl:value-of select="//lang_blocks/p53"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cgi/my_pool.cgi?data=IDNUMBER
" class="Nav_Green_Plain">http://www.glocalincome.com/cgi/my_pool.cgi?data=IDNUMBER
</a></p>
<p><xsl:value-of select="//lang_blocks/p55"/></p>
<p><xsl:value-of select="//lang_blocks/p56"/></p>
<p><b><xsl:value-of select="//lang_blocks/p10"/><br/>
<xsl:value-of select="//lang_blocks/p11"/></b></p>
</td></tr></table>
</ul>




<ul><li class="c"><xsl:value-of select="//lang_blocks/p57"/></li></ul>


<p><xsl:value-of select="//lang_blocks/p58"/></p>
<hr/>
<!--
<p class="footer_divider"><xsl:value-of select="//lang_blocks/example_role_playing"/></p>

<p class="footer_divider"><xsl:value-of select="//lang_blocks/example_role_playinga"/></p>
<ul>
<b><xsl:value-of select="//lang_blocks/example_role_playingb"/></b>
<ul>
<li><xsl:value-of select="//lang_blocks/example_role_playingc"/></li>
<li><xsl:value-of select="//lang_blocks/example_role_playingd"/></li>

<li><xsl:value-of select="//lang_blocks/example_role_playinge"/></li>
<li><xsl:value-of select="//lang_blocks/example_role_playingf"/></li>
<li><xsl:value-of select="//lang_blocks/example_role_playingg"/></li>
</ul>

<br/>


<b><xsl:value-of select="//lang_blocks/example_role_playingh"/></b><br/>

<ul><li><xsl:value-of select="//lang_blocks/example_role_playingi"/></li>
<ol>
<li><xsl:value-of select="//lang_blocks/example_role_playingj"/></li>
<li><xsl:value-of select="//lang_blocks/example_role_playingk"/></li>
</ol></ul></ul>
<p><xsl:value-of select="//lang_blocks/example_role_playingl"/></p>


<p class="footer_divider"><xsl:value-of select="//lang_blocks/example_role_playingm"/></p>
-->





<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/pindex" /></a>  |  <a href="lesson_9.xml"><xsl:value-of select="//lang_blocks/previous" /></a> | <a href="lesson_11.xml"><xsl:value-of select="//lang_blocks/next" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>