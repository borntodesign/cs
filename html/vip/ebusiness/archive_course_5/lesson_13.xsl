<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<style>
body {
	font-size: 100%;
	background-color: white;
	color: black;
}
p{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 }

table.assign{
	border: thin solid #404269;
	width: 100%;
	}


td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #9C6300;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

p.sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: medium;
 font-weight: bold;
 color: #9A6843;
 text-indent: 5%;
  }
h5 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: Medium;
	color: #9A6843;
}
h4 {color: #404269;
}

table.xcolor{border: thin solid #9C6300;
width: 90%;
}


tr.acolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #DCAF72;
 text-align: left;
 }
 tr.bcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FFF9F0;
 text-align: left;
  }
  
 tr.cell{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 text-align: center;
 }
 
 
 	table.notice{
	border: thin solid #FF9C00;
	width: 100%;
	text-align: center;
	background-color : #FFFFCE;
}
td.notice{font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: Black;
	text-align: center;
	}
	
td.quote{font-family: Verdana, Arial, sans-serif;
	font-size: smaller;
	color: Black;
}
 ul{ font-family: Verdana, Arial, sans-serif;
  font-size: small;
  }

li { font-family: Verdana, Arial, sans-serif;
  font-size: small; }

table.xcolor{border: thin solid #9C6300;
width: 90%;
}

p.red{ color: #FF0000;}
p.close {
	font-family: Arial, Helvetica, sans-serif;
	font-size : x-small;
}

tr.ccolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #EFE7DE;

 }
 tr.dcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FAF5F1;

  }
</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0" /></h5><br/>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>

<p><xsl:value-of select="//lang_blocks/p3" /></p>

<h5><xsl:value-of select="//lang_blocks/p4" /></h5>
<p><xsl:value-of select="//lang_blocks/p5" /></p>

<ol>
<li><b><xsl:value-of select="//lang_blocks/p6" /></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p7" /></li>
<li><b><xsl:value-of select="//lang_blocks/p8" /></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p9" /></li>
<li><b><xsl:value-of select="//lang_blocks/p10" /></b> <xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p11" /></li>
</ol>


<h5><xsl:value-of select="//lang_blocks/p12" /></h5>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>



<p><xsl:value-of select="//lang_blocks/p16" /></p>

<ol>
<p><b><xsl:value-of select="//lang_blocks/p17" /></b></p>
<li><xsl:value-of select="//lang_blocks/p18" /></li>
<li><xsl:value-of select="//lang_blocks/p19" /></li>
<li><xsl:value-of select="//lang_blocks/p20" /></li>
</ol>
<p><b><xsl:value-of select="//lang_blocks/p21" /></b></p>

<h5><xsl:value-of select="//lang_blocks/p22" /></h5>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<p><xsl:value-of select="//lang_blocks/p24" /></p>

<h5><xsl:value-of select="//lang_blocks/p25" /></h5>

<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>







<table>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p28" /><xsl:text> </xsl:text><a onclick="" href="http://www.timeanddate.com/worldclock/" target="_blank"><xsl:value-of select="//lang_blocks/p29" /></a><br/><br/><xsl:value-of select="//lang_blocks/p30" /></p></td>
<td><img src="../images/c5_l6a.gif" height="225" width="225" alt="international calling"/></td>
</tr>
</table>

<hr/>

<!--<h5><xsl:value-of select="//lang_blocks/p31" /></h5>
<p><xsl:value-of select="//lang_blocks/p32" /></p>

<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /><a href="http://www.clubshop.com/3forfreeprogram.html" target="_blank"><xsl:value-of select="//lang_blocks/p35" /></a></p>
-->

<h5><xsl:value-of select="//lang_blocks/p36" /></h5>
<p><xsl:value-of select="//lang_blocks/p37" /></p>

<p><b><xsl:value-of select="//lang_blocks/p38" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39" /></p>

<p class="red"><xsl:value-of select="//lang_blocks/p40" /></p>
<p><xsl:value-of select="//lang_blocks/p41" /></p>

<p><b><xsl:value-of select="//lang_blocks/p42" /></b></p>
<p><xsl:value-of select="//lang_blocks/p43" /></p>
<!--<xsl:value-of select="//lang_blocks/p44" />-->

<h5><xsl:value-of select="//lang_blocks/p45" /></h5>


<p><xsl:copy-of select="//lang_blocks/p200/*|//lang_blocks/p200/text()" /></p>

<p><xsl:copy-of select="//lang_blocks/p201/*|//lang_blocks/p201/text() "/></p>



<!--<table class="assign">
<tr>
<td><b><xsl:value-of select="//lang_blocks/p46" /></b></td>
</tr>
<tr>
<td>

<ol>
<li><xsl:value-of select="//lang_blocks/p47" /></li>
<li><xsl:value-of select="//lang_blocks/p48" /></li>
<li><xsl:value-of select="//lang_blocks/p49" /></li>
</ol>
</td>
</tr>
</table>
<p><b><xsl:value-of select="//lang_blocks/p50" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51" /></p>
<h5><xsl:value-of select="//lang_blocks/p52" /></h5>
<p><xsl:value-of select="//lang_blocks/p53" /></p>
<p><b><xsl:value-of select="//lang_blocks/p54" /></b></p>

<table width="100%" border="0" cellpadding="6" cellspacing="1">
<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/step1" /></b></td>
<td><xsl:value-of select="//lang_blocks/p56" /></td>
</tr>
<tr class="bcolor">
<td><b><xsl:value-of select="//lang_blocks/step2" /></b></td>
<td><xsl:value-of select="//lang_blocks/p57" /></td>
</tr>
<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/step3" /></b></td>
<td><xsl:value-of select="//lang_blocks/p58" /></td>
</tr>
<tr class="bcolor">
<td><b><xsl:value-of select="//lang_blocks/step4" /></b></td>
<td><xsl:value-of select="//lang_blocks/p197" /></td>
</tr>
</table>


<table width="100%" cellpadding="5">
  <tr class="acolor"> 
    <td rowspan="3"><b><xsl:value-of select="//lang_blocks/stepother" /></b></td>
    <td><xsl:value-of select="//lang_blocks/p59" /></td>
  </tr>
  <tr class="bcolor"> 
    <td><xsl:value-of select="//lang_blocks/p60" /></td>
  </tr>
  <tr class="acolor"> 
    <td><xsl:value-of select="//lang_blocks/p61" /></td>
  </tr>
</table>-->

<hr/>

<h5><xsl:value-of select="//lang_blocks/p62" /></h5>
<p><xsl:value-of select="//lang_blocks/p63" /></p>

<!--table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><b><xsl:value-of select="//lang_blocks/p64" /></b></p>
<p><b><xsl:value-of select="//lang_blocks/p65" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p66" /></p>
<p><b><xsl:value-of select="//lang_blocks/p67" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p68" /></p>

</td></tr>
</table>-->

<h5><xsl:value-of select="//lang_blocks/p69" /></h5>

<p><img src="../images/coach.gif" height="104" width="121" alt="builder"/><xsl:text> </xsl:text><img src="../images/trainer.gif" height="104" width="121" alt="trainer"/><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p70" /></p>

<p><xsl:value-of select="//lang_blocks/p71" /></p>
<p><xsl:value-of select="//lang_blocks/p72" /></p>
<p><xsl:value-of select="//lang_blocks/p73" /></p>



<table width="100%" cellpadding="5">
  <tr class="acolor"> 
    <td><b><xsl:value-of select="//lang_blocks/p74" /></b></td>
    <td><b><xsl:value-of select="//lang_blocks/p75" /></b></td>
    <td colspan="3"><b><xsl:value-of select="//lang_blocks/p76" /></b></td>
  </tr>
  <tr class="bcolor"> 
    <td colspan="2"></td>
    <td><xsl:value-of select="//lang_blocks/p77" /></td>
    <td><xsl:value-of select="//lang_blocks/p78" /></td>
    <td><xsl:value-of select="//lang_blocks/p79" /></td>
  </tr>
  <!--<tr class="acolor"> 
    <td><xsl:value-of select="//lang_blocks/p80" /></td>
    <td><xsl:value-of select="//lang_blocks/p81" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td></td>
  </tr>
  <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p83" /></td>
    <td><xsl:value-of select="//lang_blocks/p84" /></td>
    <td></td>
    <td></td>
     <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p85" /></td>
    <td><xsl:value-of select="//lang_blocks/p86" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
     <td></td>
    </tr>-->
    
    
    
     <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p87" /></td>
    <td><xsl:value-of select="//lang_blocks/p88" /></td>
    <td></td>
    <td></td>
     <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    
        <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p89" /></td>
    <td><xsl:value-of select="//lang_blocks/p90" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
     <td></td>
    </tr>
    
  <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p91" /></td>
    <td><xsl:value-of select="//lang_blocks/p92" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
  <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p93" /></td>
    <td><xsl:value-of select="//lang_blocks/p94" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td></td>
    </tr>
    
    
      <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p95" /></td>
    <td><xsl:value-of select="//lang_blocks/p96" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    
         <!--<tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p97" /></td>
    <td><xsl:value-of select="//lang_blocks/p98" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
            <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p97" /></td>
    <td><xsl:value-of select="//lang_blocks/p99" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>-->
    
              <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p100" /></td>
    <td><xsl:value-of select="//lang_blocks/p101" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
         <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p102" /></td>
    <td><xsl:value-of select="//lang_blocks/p103" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    
    
          <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p102" /></td>
    <td><xsl:value-of select="//lang_blocks/p104" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
              <!--<tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p102" /></td>
    <td><xsl:value-of select="//lang_blocks/p105" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
                <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p106" /></td>
    <td><xsl:value-of select="//lang_blocks/p107" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td></td>
    </tr>-->
    
                <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p106" /></td>
    <td><xsl:value-of select="//lang_blocks/p108" /></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    <td></td>
    </tr>
    
                <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p109" /></td>
    <td><xsl:value-of select="//lang_blocks/p110" /></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    </tr>
    
                   <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p109" /></td>
    <td><xsl:value-of select="//lang_blocks/p111" /></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    </tr>
                   <!--<tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p112" /></td>
    <td><xsl:value-of select="//lang_blocks/p113" /></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    <td></td>
    </tr>-->
    
                     <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p114" /></td>
    <td><xsl:value-of select="//lang_blocks/p115" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
                     <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p114" /></td>
    <td><xsl:value-of select="//lang_blocks/p116" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
                     
                     <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p120" /></td>
    <td><xsl:value-of select="//lang_blocks/p121" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    
                     <tr class="bcolor">
  <td><xsl:value-of select="//lang_blocks/p120" /></td>
    <td><xsl:value-of select="//lang_blocks/p122" /></td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>
    
    
    
                    <!-- <tr class="acolor">
  <td><xsl:value-of select="//lang_blocks/p139" /></td>
    <td><xsl:value-of select="//lang_blocks/p140" /><br/>
    <xsl:value-of select="//lang_blocks/p141" /><br/>
    <xsl:value-of select="//lang_blocks/p142" /><br/>
    <xsl:value-of select="//lang_blocks/p143" /><br/>
    <xsl:value-of select="//lang_blocks/p144" /><br/>
    
    
    </td>
    <td></td>
    <td></td>
    <td><b><xsl:value-of select="//lang_blocks/p82" /></b></td>
    </tr>-->
    
    
</table>

<h5><xsl:value-of select="//lang_blocks/p145" /></h5>

<p><xsl:value-of select="//lang_blocks/p146" /></p>
<!--<p><xsl:value-of select="//lang_blocks/p147" /></p>-->
<p><xsl:value-of select="//lang_blocks/p148" /></p>

<p><xsl:value-of select="//lang_blocks/p149" /><xsl:text> </xsl:text> <a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/clublist.html" target="_blank"><xsl:value-of select="//lang_blocks/p150" /></a></p>

<p><xsl:value-of select="//lang_blocks/p151" /></p>


<table class="assign">
<tr>
<td>
<p><b><xsl:value-of select="//lang_blocks/p152" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p153" /></p>
<p><xsl:value-of select="//lang_blocks/p154" /><xsl:text> </xsl:text><font color="#CC0000"><xsl:value-of select="//lang_blocks/p155" /></font>,</p>
<p><xsl:value-of select="//lang_blocks/p156" /><xsl:text> </xsl:text><font color="#CC0000"><xsl:value-of select="//lang_blocks/p157" /></font></p>
<p><xsl:value-of select="//lang_blocks/p159" /></p>
<p><xsl:value-of select="//lang_blocks/p160" /></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p161" /></li>
<li><xsl:value-of select="//lang_blocks/p162" /></li>
<li><xsl:value-of select="//lang_blocks/p163" /></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p164" /></p>
<p><xsl:value-of select="//lang_blocks/p165" /></p>
<p><xsl:value-of select="//lang_blocks/p166" /><br/>
<font color="#CC0000"><xsl:value-of select="//lang_blocks/p167" /></font>
</p>
</td>
</tr>
</table>


<p><xsl:value-of select="//lang_blocks/p168" /></p>
<p><xsl:value-of select="//lang_blocks/p169" /></p>
<p><xsl:value-of select="//lang_blocks/p170" /></p>
<p><xsl:value-of select="//lang_blocks/p171" /></p>
<p><xsl:value-of select="//lang_blocks/p172" /></p>
<p><xsl:value-of select="//lang_blocks/p173" /></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p174" /></li>
<li><xsl:value-of select="//lang_blocks/p175" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p176" /></p>

<p><img src="../images/reminder.jpg" height="102" width="60" alt="reminder"/><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p177" /></p>

<p><xsl:value-of select="//lang_blocks/p178" /></p>
<p><ul><b><xsl:value-of select="//lang_blocks/p179" /></b>
</ul></p>


<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><b><xsl:value-of select="//lang_blocks/p180" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p181" /></p></td></tr>
</table>

<br/><br/>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" />
</td></tr>
<tr><td><p><b><xsl:value-of select="//lang_blocks/p182" /></b>
<xsl:value-of select="//lang_blocks/p183" /></p>

<div align="center"> 
                    <form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
                      <textarea name="ppf" cols="50" rows="5"></textarea>
                      <br/>
                      <input type="submit" value="Submit"/>
                      <input type="reset" name="Reset" value="Reset"/>
                      <input type="hidden" name="action" value="confirm"/>

                      <input type="hidden" name="type" value="7"/>
                      <input type="hidden" name="u_key" value="ubfpps"/>
                    </form>
                  </div>



</td></tr>
</table>-->
<hr/>
<table class="assign">
<tr><td>
<div align="center"><h5><xsl:value-of select="//lang_blocks/p184" /></h5></div>
<p><b><xsl:value-of select="//lang_blocks/p185" /></b></p>
<p><xsl:value-of select="//lang_blocks/p186" /></p>

<ul>
<!--<li><xsl:value-of select="//lang_blocks/p188" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/BU.xml" target="_blank"><xsl:value-of select="//lang_blocks/p189" /></a><xsl:value-of select="//lang_blocks/p190" /></li>-->
<li><xsl:value-of select="//lang_blocks/p191" /></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><xsl:value-of select="//lang_blocks/p192" /></li>
<li><xsl:value-of select="//lang_blocks/p193" /></li>
<li><xsl:value-of select="//lang_blocks/p194" /></li>
<li><xsl:value-of select="//lang_blocks/p196" /> <xsl:text> </xsl:text><a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->
</td></tr></table>

<p><xsl:value-of select="//lang_blocks/p195" /></p>























<hr/>
<div align="center"><p>
<a href="index.xml"><xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_12.xml"><xsl:value-of select="//lang_blocks/previous" /></a> |<a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/2391"><xsl:value-of select="//lang_blocks/lesson" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>