<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />

<table width="600">
<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/p2" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p3" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p4" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p5" /></b></td>
</tr>

<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p6" /></td>
<td><xsl:value-of select="//lang_blocks/p7" /></td>
<td><xsl:value-of select="//lang_blocks/p8" /></td>
<td><xsl:value-of select="//lang_blocks/p9" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p10" /></td>
<td><xsl:value-of select="//lang_blocks/p11" /></td>
<td><xsl:value-of select="//lang_blocks/p12" /></td>
<td><xsl:value-of select="//lang_blocks/p13" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p14" /></td>
<td><xsl:value-of select="//lang_blocks/p15" /></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td><xsl:value-of select="//lang_blocks/p17" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p18" /></td>
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td><xsl:value-of select="//lang_blocks/p20" /></td>
<td><xsl:value-of select="//lang_blocks/p21" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p22" /></td>
<td><xsl:value-of select="//lang_blocks/p23" /></td>
<td><xsl:value-of select="//lang_blocks/p24" /></td>
<td><xsl:value-of select="//lang_blocks/p25" /></td>
</tr>


</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
