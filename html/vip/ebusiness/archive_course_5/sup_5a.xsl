<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>

<img src="../images/traineeline.gif" />
<br/>
<p class="sub"><xsl:value-of select="//lang_blocks/head" /></p>


<p><xsl:value-of select="//lang_blocks/p1" /></p>



<p><img src="../images/traineeline.gif" alt=""/></p>

<p class="sub"><xsl:value-of select="//lang_blocks/p2" /></p>
<img style="float:left; padding: 0px 10px" src="../images/ernestine.gif" height="115" width="74" alt="ernestine"/>

<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><img src="../images/traineeline.gif" alt=""/></p>

<p class="sub"><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_5/Script_1.doc"><xsl:value-of select="//lang_blocks/p10" /></a>   |   <a href="http://www.clubshop.com/vip/ebusiness/course_5/New_Affiliate_Partner_Follow_Up.doc"><xsl:value-of select="//lang_blocks/p11" /></a><br/>
<a href="http://www.clubshop.com/vip/ebusiness/course_5/Sample_Questions.doc"><xsl:value-of select="//lang_blocks/p19" /></a> </p></div>


<p><img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt=""/></p>

<p class="sub"><xsl:value-of select="//lang_blocks/p12" /></p>

<p><xsl:value-of select="//lang_blocks/p13" /></p>

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_5/Script_1.doc"><xsl:value-of select="//lang_blocks/p10" /></a>   |   <a href="http://www.clubshop.com/vip/ebusiness/course_5/Member_Follow_Up1.doc"><xsl:value-of select="//lang_blocks/p14" /></a>   <br/> <a href="http://www.clubshop.com/vip/ebusiness/course_5/Member_Follow_Up2.doc"><xsl:value-of select="//lang_blocks/p14a" /></a></p></div>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt=""/></p>

<p class="sub"><xsl:value-of select="//lang_blocks/p15" /></p>

<p><xsl:value-of select="//lang_blocks/p16" /></p>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_5/Script_1.doc"><xsl:value-of select="//lang_blocks/p10" /></a>   |   <a href="http://www.clubshop.com/vip/ebusiness/course_5/Old_Member_Follow_Up.doc"><xsl:value-of select="//lang_blocks/p17" /></a></p></div>

<p><img src="../images/traineeline.gif" alt=""/></p>

<p><xsl:value-of select="//lang_blocks/p18" /></p>





</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="lesson_6.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
