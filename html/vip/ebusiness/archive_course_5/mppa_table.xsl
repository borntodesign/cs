<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />

<table width="690">
<tr class="acolor">

<td><b><xsl:value-of select="//lang_blocks/p1" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p2" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p3" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p4" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p5" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p6" /></b></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p7" /></td>
<td><xsl:value-of select="//lang_blocks/p8" /></td>
<td><xsl:value-of select="//lang_blocks/p9" /></td>
<td><xsl:value-of select="//lang_blocks/p10" /></td>
<td><xsl:value-of select="//lang_blocks/p11" /></td>
<td><xsl:value-of select="//lang_blocks/p12" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p13" /></td>
<td><xsl:value-of select="//lang_blocks/p14" /></td>
<td><xsl:value-of select="//lang_blocks/p15" /></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td><xsl:value-of select="//lang_blocks/p17" /></td>
<td><xsl:value-of select="//lang_blocks/p18" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td><xsl:value-of select="//lang_blocks/p20" /></td>
<td><xsl:value-of select="//lang_blocks/p21" /></td>
<td><xsl:value-of select="//lang_blocks/p22" /></td>
<td><xsl:value-of select="//lang_blocks/p23" /></td>
<td><xsl:value-of select="//lang_blocks/p24" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p25" /></td>
<td><xsl:value-of select="//lang_blocks/p26" /></td>
<td><xsl:value-of select="//lang_blocks/p28" /></td>
<td><xsl:value-of select="//lang_blocks/p29" /></td>
<td><xsl:value-of select="//lang_blocks/p30" /></td>
<td><xsl:value-of select="//lang_blocks/p31" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p32" /></td>
<td><xsl:value-of select="//lang_blocks/p33" /></td>
<td><xsl:value-of select="//lang_blocks/p34" /></td>
<td><xsl:value-of select="//lang_blocks/p35" /></td>
<td><xsl:value-of select="//lang_blocks/p36" /></td>
<td><xsl:value-of select="//lang_blocks/p37" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p38" /></td>
<td><xsl:value-of select="//lang_blocks/p39" /></td>
<td><xsl:value-of select="//lang_blocks/p40" /></td>
<td><xsl:value-of select="//lang_blocks/p41" /></td>
<td><xsl:value-of select="//lang_blocks/p42" /></td>
<td><xsl:value-of select="//lang_blocks/p43" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p44" /></td>
<td><xsl:value-of select="//lang_blocks/p45" /></td>
<td><xsl:value-of select="//lang_blocks/p46" /></td>
<td><xsl:value-of select="//lang_blocks/p47" /></td>
<td><xsl:value-of select="//lang_blocks/p48" /></td>
<td><xsl:value-of select="//lang_blocks/p49" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p50" /></td>
<td><xsl:value-of select="//lang_blocks/p51" /></td>
<td><xsl:value-of select="//lang_blocks/p52" /></td>
<td><xsl:value-of select="//lang_blocks/p53" /></td>
<td><xsl:value-of select="//lang_blocks/p54" /></td>
<td><xsl:value-of select="//lang_blocks/p55" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p56" /></td>
<td><xsl:value-of select="//lang_blocks/p57" /></td>
<td><xsl:value-of select="//lang_blocks/p58" /></td>
<td><xsl:value-of select="//lang_blocks/p59" /></td>
<td><xsl:value-of select="//lang_blocks/p60" /></td>
<td><xsl:value-of select="//lang_blocks/p61" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p62" /></td>
<td><xsl:value-of select="//lang_blocks/p63" /></td>
<td><xsl:value-of select="//lang_blocks/p64" /></td>
<td><xsl:value-of select="//lang_blocks/p65" /></td>
<td><xsl:value-of select="//lang_blocks/p66" /></td>
<td><xsl:value-of select="//lang_blocks/p67" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p68" /></td>
<td><xsl:value-of select="//lang_blocks/p69" /></td>
<td><xsl:value-of select="//lang_blocks/p70" /></td>
<td><xsl:value-of select="//lang_blocks/p71" /></td>
<td><xsl:value-of select="//lang_blocks/p72" /></td>
<td><xsl:value-of select="//lang_blocks/p73" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p74" /></td>
<td><xsl:value-of select="//lang_blocks/p75" /></td>
<td><xsl:value-of select="//lang_blocks/p76" /></td>
<td><xsl:value-of select="//lang_blocks/p77" /></td>
<td><xsl:value-of select="//lang_blocks/p78" /></td>
<td><xsl:value-of select="//lang_blocks/p79" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p80" /></td>
<td><xsl:value-of select="//lang_blocks/p81" /></td>
<td><xsl:value-of select="//lang_blocks/p82" /></td>
<td><xsl:value-of select="//lang_blocks/p83" /></td>
<td><xsl:value-of select="//lang_blocks/p84" /></td>
<td><xsl:value-of select="//lang_blocks/p85" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p86" /></td>
<td><xsl:value-of select="//lang_blocks/p87" /></td>
<td><xsl:value-of select="//lang_blocks/p88" /></td>
<td><xsl:value-of select="//lang_blocks/p89" /></td>
<td><xsl:value-of select="//lang_blocks/p90" /></td>
<td><xsl:value-of select="//lang_blocks/p91" /></td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p92" /></td>
<td><xsl:value-of select="//lang_blocks/p93" /></td>
<td><xsl:value-of select="//lang_blocks/p94" /></td>
<td><xsl:value-of select="//lang_blocks/p95" /></td>
<td><xsl:value-of select="//lang_blocks/p96" /></td>
<td><xsl:value-of select="//lang_blocks/p97" /></td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p98" /></td>
<td><xsl:value-of select="//lang_blocks/p99" /></td>
<td><xsl:value-of select="//lang_blocks/p100" /></td>
<td><xsl:value-of select="//lang_blocks/p101" /></td>
<td><xsl:value-of select="//lang_blocks/p102" /></td>
<td><xsl:value-of select="//lang_blocks/p103" /></td>
</tr>
<tr class="bcolor">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="acolor">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="bcolor">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>



</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
