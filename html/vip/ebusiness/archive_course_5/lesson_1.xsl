<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>


</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>

<ol><b>
<li><b><xsl:value-of select="//lang_blocks/p3" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p4" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p5" /></b></li>
</b></ol>

<p><xsl:value-of select="//lang_blocks/p6" /></p>

<ol><b>
<li><b><xsl:value-of select="//lang_blocks/p7" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p9" /></b></li>
<li><b><xsl:value-of select="//lang_blocks/p10" /></b></li>
</b></ol>


<p><xsl:value-of select="//lang_blocks/p11" /></p>

<h5><xsl:value-of select="//lang_blocks/p12" /></h5>
<p><xsl:value-of select="//lang_blocks/p13" /></p>

<p><xsl:value-of select="//lang_blocks/p14" /></p>
<div align="center"><b><xsl:value-of select="//lang_blocks/p15" /></b></div> 
<p><xsl:value-of select="//lang_blocks/p16" /></p>

<h5><xsl:value-of select="//lang_blocks/p17" /></h5>
<p><xsl:value-of select="//lang_blocks/p18" /></p>
<p><xsl:value-of select="//lang_blocks/p19" /></p>
<p><xsl:value-of select="//lang_blocks/p20" /></p>

<h5><xsl:value-of select="//lang_blocks/p21" /></h5>

<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><a href="http://www.glocalincome.com/fees/full.xml" target="_blank"><xsl:value-of select="//lang_blocks/p23a" />
</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23b" /></p>

<table class="assign" cellpadding="5" border="1">
<tr class="cell">
<td><xsl:value-of select="//lang_blocks/p24" /></td>
<td><xsl:value-of select="//lang_blocks/p25" /></td>
<td><xsl:value-of select="//lang_blocks/p26" /></td>
</tr>
<tr>
<td valign="top"><img src="../images/c5_1b.gif" height="300" width="205" alt="One Line Only"/></td>
<td><img src="../images/c5_1c.gif" height="400" width="230" alt="Two Lines Only"/></td>
<td><img src="../images/c5_1d.gif" height="400" width="205" alt="Three Lines Only"/></td>
</tr>
</table>
<p><b><xsl:value-of select="//lang_blocks/p27" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p27a" /></p>

<h5><xsl:value-of select="//lang_blocks/p28" /></h5>
<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>
<p><xsl:value-of select="//lang_blocks/p32" /></p>
<img src="../images/c5_l1a.gif" width="655" height="105" alt="Your Orchard"/>






<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  
<a href="lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>