<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />

<table width="600">
<tr>
<td>
<ol>
<li><xsl:value-of select="//lang_blocks/p1" /></li>
<li><xsl:value-of select="//lang_blocks/p2" /></li>
<li><xsl:value-of select="//lang_blocks/p3" /></li>
<li><xsl:value-of select="//lang_blocks/p4" /> <font color="#0000FF"><xsl:value-of select="//lang_blocks/p5" /></font>(<a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/bu_act_report.html" target="_blank"><xsl:value-of select="//lang_blocks/p6" /></a>)</li>
<li><xsl:value-of select="//lang_blocks/p7" /></li>
</ol>
</td>
</tr>
</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>