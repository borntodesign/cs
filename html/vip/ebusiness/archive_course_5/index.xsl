<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>

<img src="../images/traineeline.gif" />
<br/>


<table><tr><td>
<img src="../images/winfrey.gif" width="83" height="114" alt="Oprah Winfrey"/></td>
<td class="quote"><xsl:value-of select="//lang_blocks/quote" /><br/><b><xsl:value-of select="//lang_blocks/quote2" /></b>
</td></tr></table>

<p><img src="../images/traineeline.gif" alt=""/></p>

<p class="sub"><xsl:value-of select="//lang_blocks/head" /></p>


<ul><li><a href="lesson_1.xml"><xsl:value-of select="//lang_blocks/p2" /></a></li></ul>
<ul><li><a href="lesson_2.xml"><xsl:value-of select="//lang_blocks/p3" /></a></li></ul>
<ul><li><a href="lesson_3.xml"><xsl:value-of select="//lang_blocks/p4" /></a></li></ul>
<ul><li><a href="lesson_4.xml"><xsl:value-of select="//lang_blocks/p5" /></a></li></ul>
<ul><li><a href="lesson_5.xml"><xsl:value-of select="//lang_blocks/p6" /></a></li></ul>
<ul><li><a href="lesson_6.xml"><xsl:value-of select="//lang_blocks/p7" /></a></li></ul>
<ul><li><a href="lesson_7.xml"><xsl:value-of select="//lang_blocks/p11" /></a></li></ul>
<ul><li><a href="lesson_8.xml"><xsl:value-of select="//lang_blocks/p12" /></a></li></ul>
<ul><li><a href="lesson_9.xml"><xsl:value-of select="//lang_blocks/p13" /></a></li></ul>
<ul><li><a href="lesson_10.xml"><xsl:value-of select="//lang_blocks/p14" /></a></li></ul>
<ul><li><a href="lesson_11.xml"><xsl:value-of select="//lang_blocks/p15" /></a></li></ul>
<ul><li><a href="lesson_12.xml"><xsl:value-of select="//lang_blocks/p16" /></a></li></ul>
<ul><li><a href="lesson_13.xml"><xsl:value-of select="//lang_blocks/p8" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/2391"><xsl:value-of select="//lang_blocks/p9" /></a></li></ul>





</td></tr></table>

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_5/lesson_1.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
