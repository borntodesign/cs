<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}
p{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 }

table.assign{
	border: thin solid #404269;
	width: 100%;
	}


td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #9C6300;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

p.sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: medium;
 font-weight: bold;
 color: #9A6843;
 text-indent: 5%;
  }
h5 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: Medium;
	color: #9A6843;
}
h4 {color: #404269;
}

table.xcolor{border: thin solid #9C6300;
width: 90%;
}


tr.acolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #DCAF72;
 text-align: center;
 }
 tr.bcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FFF9F0;
 text-align: center;
  }
  
 tr.cell{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 text-align: center;
 }
 
 
 	table.notice{
	border: thin solid #FF9C00;
	width: 100%;
	text-align: center;
	background-color : #FFFFCE;
}
td.notice{font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: Black;
	text-align: center;
	}
	
td.quote{font-family: Verdana, Arial, sans-serif;
	font-size: smaller;
	color: Black;
}
 ul{ font-family: Verdana, Arial, sans-serif;
  font-size: small;
  }

li { font-family: Verdana, Arial, sans-serif;
  font-size: small; }

table.xcolor{border: thin solid #9C6300;
width: 90%;
}

p.red{ color: #FF0000;}
p.close {
	font-family: Arial, Helvetica, sans-serif;
	font-size : x-small;
}

tr.ccolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #EFE7DE;

 }
 tr.dcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FAF5F1;

  }
</style>
</head>

<body>

<table style="width:1000px">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0" /></h5>
<img src="../images/traineeline.gif" alt="" width="705" height="10" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><b><xsl:value-of select="//lang_blocks/p3" /></b><xsl:value-of select="//lang_blocks/p4" /></p>

<h5><xsl:value-of select="//lang_blocks/p5" /></h5>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><b><xsl:value-of select="//lang_blocks/p7" /></b></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><b><xsl:value-of select="//lang_blocks/p12" /></b></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><b><xsl:value-of select="//lang_blocks/p14" /></b><xsl:value-of select="//lang_blocks/p15" /></p>

<p><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:value-of select="//lang_blocks/p17" /></p>
<p><xsl:value-of select="//lang_blocks/p18" /></p>


<img src="../images/c5_l5b.gif" height="370" width="614" alt=""/>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p19" /></h5>

<p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p22" /></p></td></tr>
</table>

<h5><xsl:value-of select="//lang_blocks/p23" /></h5>

<table cellpadding="5">
<tr class="dcolor"><td><xsl:value-of select="//lang_blocks/p24" /></td></tr></table>

<table cellpadding="10">
<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p25" /></td>
<td><a href="/vip/ebusiness/course_5/esw.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p26" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p28" /></td>
<td><a href="/vip/ebusiness/course_5/non_pool_ap_welcome_email.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p28" /></a></td>
</tr>


<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p28aa" /></td>
<td><a href="/vip/ebusiness/course_5/pool_ap_welcome_email.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p28aa" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p28a" /></td>
<td><a href="/vip/ebusiness/course_5/amp_welcome_email.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p28b" /></a></td>
</tr>






<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p29" /></td>
<td><a href="/vip/ebusiness/course_5/evw.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p30" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p31" /></td>
<td><a href="/vip/ebusiness/course_5/eppo.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p32" /></a></td>
</tr>

<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p131" /></td>
<td><a href="/vip/ebusiness/course_5/pru.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p130" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p35" /></td>
<td><a href="/vip/ebusiness/course_5/eap1.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p36" /></a></td>
</tr>


<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p37" /></td>
<td><a href="/vip/ebusiness/course_5/eap2.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p38" /></a></td>
</tr>


<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p39" /></td>
<td><a href="/vip/ebusiness/course_5/eap3.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p40" /></a></td>
</tr>


<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p41" /></td>
<td><a href="/vip/ebusiness/course_5/eap4.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p42" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p43" /></td>
<td><a href="/vip/ebusiness/course_5/eap5.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p44" /></a></td>
</tr>

<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p45" /></td>
<td><a href="/vip/ebusiness/course_5/eap6.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p46" /></a></td>
</tr>




<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p136" /></td>
<td><a href="eap7.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p137" /></a></td>
</tr>


<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p138" /></td>
<td><a href="eap8.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p139" /></a></td>
</tr>


<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p140" /></td>
<td><a href="eap9.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p141" /></a></td>
</tr>



<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p142" /></td>
<td><a href="eap10.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p143" /></a></td>
</tr>














<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p132" /></td>
<td><a href="ecm.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p133" /></a></td>
</tr>

<tr class="ccolor">
<td><xsl:value-of select="//lang_blocks/p134" /></td>
<td><a href="ecp.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p135" /></a></td>
</tr>









<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p47" /></td>
<td><xsl:value-of select="//lang_blocks/p48" /></td>
</tr>
</table>


<h5><xsl:value-of select="//lang_blocks/p49" /></h5>
<p><xsl:value-of select="//lang_blocks/p50" /></p>
<p><b><xsl:value-of select="//lang_blocks/p51" /></b><xsl:value-of select="//lang_blocks/p52" /></p>
<p><xsl:value-of select="//lang_blocks/p53" /></p>

<h5><xsl:value-of select="//lang_blocks/p54" /></h5>


<table><tr>
<td bgcolor="#EFE7DE">
<p><xsl:value-of select="//lang_blocks/p55a" /></p>
<br/>
<p><xsl:value-of select="//lang_blocks/p56" /></p>
<p><xsl:value-of select="//lang_blocks/p57" /></p>
<p><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
<p><xsl:value-of select="//lang_blocks/p60" /></p>
<p><xsl:value-of select="//lang_blocks/p61" /><br/>
<xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p63" /><br/>
<xsl:value-of select="//lang_blocks/p64" /></p>
<br/>
<p><xsl:value-of select="//lang_blocks/p65" /></p>


</td>
<td bgcolor="#FAF5F1">
<p><xsl:value-of select="//lang_blocks/p55a" /></p>
<p><xsl:value-of select="//lang_blocks/p66" /></p>
<p><xsl:value-of select="//lang_blocks/p67" /></p>
<p><xsl:value-of select="//lang_blocks/p68" /></p>
<p><xsl:value-of select="//lang_blocks/p69" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
<p><xsl:value-of select="//lang_blocks/p60" /></p>
<p><xsl:value-of select="//lang_blocks/p61" /><br/>
<xsl:value-of select="//lang_blocks/p62" /></p>
<p><xsl:value-of select="//lang_blocks/p63" /><br/>
<xsl:value-of select="//lang_blocks/p64" /></p>
<p><xsl:value-of select="//lang_blocks/p65" /></p>
</td>
</tr>
</table>

<!--<hr/>
<table>
<tr><td><img src="../images/c5_l5f.gif" height="86" width="147" alt="postal mail"/> <img src="/vip/ebusiness/images/c5_l5e.gif" height="86" width="147" alt="post card"/></td></tr>
</table>
<h5><xsl:value-of select="//lang_blocks/p70" /></h5>
<p><xsl:value-of select="//lang_blocks/p71" /></p>
<p><xsl:value-of select="//lang_blocks/p72" /></p>
<p><xsl:value-of select="//lang_blocks/p73" /></p>

<table cellpadding="10">
<tr class="ccolor"><td colspan="2"><xsl:value-of select="//lang_blocks/p74" /></td></tr>
<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p75" /></td>
<td><a href="/vip/ebusiness/course_5/psw.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p76" /></a></td>
</tr>

<tr class="dcolor">
<td><xsl:value-of select="//lang_blocks/p77" /></td>
<td><a href="/vip/ebusiness/course_5/pmw.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p78" /></a></td>
</tr>

<tr class="dcolor"><td><xsl:value-of select="//lang_blocks/p79" /></td>
<td><a href="/vip/ebusiness/course_5/pap1.xml" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p80" /></a></td></tr>
</table>-->

<hr/>

<h5><xsl:value-of select="//lang_blocks/p81" /></h5>
<p><xsl:value-of select="//lang_blocks/p82" /> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p82a" /><xsl:text> </xsl:text> <a href="/vip/ebusiness/course_5/sup5a.xml" onclick="window.open(this.href,'nuwin'); return false;">/vip/ebusiness/course_5/sup5a.xml</a></p>


<!--<h5><xsl:value-of select="//lang_blocks/p83" /></h5>
<p><xsl:value-of select="//lang_blocks/p84" /></p>
<p><xsl:value-of select="//lang_blocks/p85" /></p>
<p><xsl:value-of select="//lang_blocks/p86" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p88" /></li>
<li><xsl:value-of select="//lang_blocks/p89" /></li>
<li><xsl:value-of select="//lang_blocks/p90" /></li>
<li><xsl:value-of select="//lang_blocks/p91" /></li>
<li><xsl:value-of select="//lang_blocks/p92" /></li>
<li><xsl:value-of select="//lang_blocks/p93" /></li>
</ul>


<p><xsl:value-of select="//lang_blocks/p94" /></p>
<h5><xsl:value-of select="//lang_blocks/p95" /></h5>

<p><xsl:value-of select="//lang_blocks/p96" /></p>
<p><xsl:value-of select="//lang_blocks/p97" /></p>

<h5><xsl:value-of select="//lang_blocks/p98" /></h5>
<p><xsl:value-of select="//lang_blocks/p99" /></p>


<table cellpadding="5">
<tr class="ccolor">
<td><a href="/vip/ebusiness/course_5/phone_script1.html" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p100" /></a></td>
<td><a href="/vip/ebusiness/course_5/phone_script2.html" onclick="window.open(this.href,'nuwin'); return false;"><xsl:value-of select="//lang_blocks/p101" /></a></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p102" /><br/>
<xsl:value-of select="//lang_blocks/p103" /></p>

<p><xsl:value-of select="//lang_blocks/p104" /></p>-->



<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p105" /><xsl:text> </xsl:text> <a href="/cgi/tl-redir">Upline Link</a></p>
<p><xsl:value-of select="//lang_blocks/p105a" /></p>


</td>
</tr>
</table>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p106" /></h5>
<p><xsl:value-of select="//lang_blocks/p107" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p108" /></li>
<li><xsl:value-of select="//lang_blocks/p109" /></li>
<li><xsl:value-of select="//lang_blocks/p109a" /></li>
</ul>
<p><xsl:value-of select="//lang_blocks/p109b" /></p>
<p><xsl:value-of select="//lang_blocks/p120" /><xsl:text> </xsl:text>
<a href="../images/5.6_follow_up_report.gif"><xsl:value-of select="//lang_blocks/p121"/></a></p>


<hr/>
<h5><xsl:value-of select="//lang_blocks/p110" /></h5>
<p><xsl:value-of select="//lang_blocks/p111" /></p>
<p><xsl:value-of select="//lang_blocks/p112" /></p>

<p><xsl:value-of select="//lang_blocks/p113" /></p>

<h5><xsl:value-of select="//lang_blocks/p114" /></h5>
<p><xsl:value-of select="//lang_blocks/p115" /></p>
<p><xsl:value-of select="//lang_blocks/p116" /></p>
<p><xsl:value-of select="//lang_blocks/p117" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p118" /></h5>

<p><xsl:value-of select="//lang_blocks/p119" /></p>





<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_7.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>