<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>
<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<!--<style type="text/css">
p,li{font-family: Verdana, Arial, sans-serif;
font-size: 12px;
}
h5{font-family: Verdana, Arial, sans-serif;
font-size: 1em;
}
a {
font-size: 12px;
}
.Nav_Green_Plain:link {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:visited {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:hover {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;	
	font-weight:bold;	
	text-decoration: underline;
}
.Nav_Green_Plain:active {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}


body {
   background-color: #245923;
   	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 20px;
	color: #333333;
}



td.back{background-image:url(/images/bg_header.jpg);}

.Green_Large_Bold_head {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #34420F;
	font-weight:bold;
}


.Green_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #485B15;
	font-weight:bold;
}
.footer_divider {
	font-size: 12px;
	text-transform:uppercase;
	font-weight: bold;
	color: #41712B;
}
.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FDB100;
	font-weight:bold;
	text-transform:uppercase;
}
.Green_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #779724;
	font-weight:bold;
}

ol.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
	
li.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
div.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F8FFF2;
		padding: 8px;
}

li.b {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #485B15;
	list-style: numeric;}

li.c {
	list-style: url("http://www.glocalincome.com/images/icons/icon_bullet_ltgreen.png");
}

li.d { 
	list-style: url("http://www.glocalincome.com/images/icons/icon_arrow.png");
}

</style>-->
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0"/></h5>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p11"/></h5>

<ol>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p12"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>
<br/>
	<ul>
	<li class="d"><xsl:value-of select="//lang_blocks/p14"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p15"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p16"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p17"/></li>
		<ul>
		<li class="c"><xsl:value-of select="//lang_blocks/p18"/></li>
		<li class="c"><xsl:value-of select="//lang_blocks/p19"/></li>
		<li class="c"><xsl:value-of select="//lang_blocks/p20"/></li>
		<li class="c"><xsl:value-of select="//lang_blocks/p21"/></li>
		</ul>
		
	<li class="d"><xsl:value-of select="//lang_blocks/p22"/></li><br/>
	
	</ul>
	
	
<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p23"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24"/></li><br/>
	<ul>
	<li class="d"><xsl:value-of select="//lang_blocks/p25"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p26"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p27"/></li>
	<li class="d"><xsl:value-of select="//lang_blocks/p28"/></li>
	</ul>
<br/>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p29"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p30"/></li><br/>

	<ul>
	<li class="d"><xsl:value-of select="//lang_blocks/p31"/></li><br/>
			<ul>
			<li class="c"><xsl:value-of select="//lang_blocks/p32"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p33"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p34"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p35"/></li>
			</ul>
			
			
	
	
	</ul>
	<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p36"/></span></li>
		<br/>
			<ul>
			<li class="d"><xsl:value-of select="//lang_blocks/p37"/><br/><br/>
			<b><xsl:value-of select="//lang_blocks/p38"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p40"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p41"/><br/></li><br/>
			
			<li class="d"><xsl:value-of select="//lang_blocks/p43"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p44"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p45"/><br/><br/>
			<xsl:value-of select="//lang_blocks/p46"/><br/><br/>
			</li>
			
			
			
			</ul>
			<br/>
			
			<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p47"/></span></li>
			
			<ul><li class="d"><xsl:value-of select="//lang_blocks/p48"/></li></ul>
	
</ol>


<p><xsl:value-of select="//lang_blocks/p42"/></p>






<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/pindex" /></a>  |  <a href="lesson_7.xml"><xsl:value-of select="//lang_blocks/prev_lesson" /></a> | <a href="lesson_9.xml"><xsl:value-of select="//lang_blocks/next_lesson" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>