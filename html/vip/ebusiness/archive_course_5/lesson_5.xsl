<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p4" /><b><xsl:value-of select="//lang_blocks/p5" /></b><xsl:value-of select="//lang_blocks/p6" /></p>
</td></tr>
</table>

<h5><xsl:value-of select="//lang_blocks/p7" /></h5>
<table class="assign">
<tr><td>
<p><img src="../images/c5_l4builder.gif" height="93" width="136" alt="The Builder"/><h4><xsl:value-of select="//lang_blocks/p8" /></h4></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p9" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p10" /></li><br/>
<ul>
<li><xsl:value-of select="//lang_blocks/p10a" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p10b" /></li>
</ul>
<br/>
<li><xsl:value-of select="//lang_blocks/p11" /></li><br/>
</ol>

</td></tr></table>

<h5><xsl:value-of select="//lang_blocks/p12" /></h5>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /><br/>
<a onclick="" href="ap_welcome_email.xml" target="_blank"><xsl:value-of select="//lang_blocks/p15" /></a><br/>
<a onclick="" href="amp_welcome_email.xml" target="_blank"><xsl:value-of select="//lang_blocks/p15a" /></a></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p16" /></h5>
<p><xsl:value-of select="//lang_blocks/p17" /></p>
<p><xsl:value-of select="//lang_blocks/p18" />(<a href="webpage_guide.xml" target="_blank"><xsl:value-of select="//lang_blocks/p19" /></a>) </p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p20" /></p></td>
</tr>
</table>

<hr/>

<!--<table class="assign">
<tr>
<td><a onclick="" href="http://www.clubshop.com/vip/ebusiness/recording_pg/builders_up.html" target="_blank"><img src="../images/reels.gif" height="64" width="126" alt="listen to recording"/></a></td>
<td><p><xsl:value-of select="//lang_blocks/p29" /> <br/><xsl:value-of select="//lang_blocks/p30" /><a href="http://www.clubshop.com/vip/ebusiness/MP3s/builders_up.mp3" target="_blank"><xsl:value-of select="//lang_blocks/p31" /></a></p></td>
</tr>
</table>-->


<p><xsl:value-of select="//lang_blocks/p32" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33" /></p>
<p> <xsl:value-of select="//lang_blocks/p34" /></p>








<hr/>
<h5><xsl:value-of select="//lang_blocks/p22" /></h5>
<p><xsl:value-of select="//lang_blocks/p23" /></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /></p>

</td></tr>
</table>

<hr/>
<!--<p><xsl:value-of select="//lang_blocks/p41" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/bu_comm_table.xml" target="_blank"><xsl:value-of select="//lang_blocks/p42" /></a></p>

<p><xsl:value-of select="//lang_blocks/p43" /></p>
<p><xsl:value-of select="//lang_blocks/p44" /></p>
<p><xsl:value-of select="//lang_blocks/p45" /> <a href="http://www.clubshop.com/vip/ebusiness/course_5/mppa_table.xml" target="_blank"><xsl:value-of select="//lang_blocks/p46" /></a></p>

<h5><xsl:value-of select="//lang_blocks/p47" /></h5>
<p><xsl:value-of select="//lang_blocks/p48" /> <b><xsl:value-of select="//lang_blocks/p49" /></b> <xsl:value-of select="//lang_blocks/p50" /></p>

<p><xsl:value-of select="//lang_blocks/p51" /></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p52" /></li>
<li><xsl:value-of select="//lang_blocks/p53" /></li>
<li><xsl:value-of select="//lang_blocks/p54" /></li>
<li><xsl:value-of select="//lang_blocks/p55" /></li>
<li><xsl:value-of select="//lang_blocks/p56" /></li>
</ol>
<p><xsl:value-of select="//lang_blocks/p57" /></p>
<p><xsl:value-of select="//lang_blocks/p58" /> <br/><b><xsl:value-of select="//lang_blocks/p59" /></b> <xsl:value-of select="//lang_blocks/p60" /></p>

<table class="assign">
<tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p61" /></p></td>

<table class="assignment" border="1" cellpadding="5">
<tr>
<td><b><xsl:value-of select="//lang_blocks/p62" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p63" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p64" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p65" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p66" /></b></td>
</tr>

<tr>
<td><xsl:value-of select="//lang_blocks/p67" /></td>
<td><xsl:value-of select="//lang_blocks/p68" /></td>
<td><xsl:value-of select="//lang_blocks/p69" /></td>
<td><xsl:value-of select="//lang_blocks/p70" /></td>
<td><xsl:value-of select="//lang_blocks/p71" /></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p72" /></td>
<td><xsl:value-of select="//lang_blocks/p73" /></td>
<td><xsl:value-of select="//lang_blocks/p74" /></td>
<td><xsl:value-of select="//lang_blocks/p75" /></td>
<td><xsl:value-of select="//lang_blocks/p76" /></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p77" /></td>
<td><xsl:value-of select="//lang_blocks/p78" /></td>
<td><xsl:value-of select="//lang_blocks/p79" /></td>
<td><xsl:value-of select="//lang_blocks/p80" /></td>
<td><xsl:value-of select="//lang_blocks/p81" /></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p82" /></td>
<td><xsl:value-of select="//lang_blocks/p83" /></td>
<td><xsl:value-of select="//lang_blocks/p84" /></td>
<td><xsl:value-of select="//lang_blocks/p85" /></td>
<td><xsl:value-of select="//lang_blocks/p86" /></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p87" /></p>-->
<p><b><xsl:value-of select="//lang_blocks/p88" /></b></p>




















<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="sup5a.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>