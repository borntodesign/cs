<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title>e-Business Institute</title>
<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<!--<style type="text/css">
p,li{font-family: Verdana, Arial, sans-serif;
font-size: 12px;
}
h5{font-family: Verdana, Arial, sans-serif;
font-size: 1em;
}
a {
font-size: 12px;
}
.Nav_Green_Plain:link {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:visited {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}
.Nav_Green_Plain:hover {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;	
	font-weight:bold;	
	text-decoration: underline;
}
.Nav_Green_Plain:active {
	color: #779724;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight:bold;	
	text-decoration: none;
}


body {
   background-color: #245923;
   	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 20px;
	color: #333333;
}



td.back{background-image:url(/images/bg_header.jpg);}

.Green_Large_Bold_head {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #34420F;
	font-weight:bold;
}


.Green_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #485B15;
	font-weight:bold;
}
.footer_divider {
	font-size: 12px;
	text-transform:uppercase;
	font-weight: bold;
	color: #41712B;
}
.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FDB100;
	font-weight:bold;
	text-transform:uppercase;
}
.Green_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #779724;
	font-weight:bold;
}

ol.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
	
li.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	color: #485B15;
	font-weight:bold;
	}
div.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color: #F8FFF2;
		padding: 8px;
}

li.b {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #485B15;
	list-style: numeric;}

li.c {
	list-style: url("http://www.glocalincome.com/images/icons/icon_bullet_ltgreen.png");
}

li.d { 
	list-style: url("http://www.glocalincome.com/images/icons/icon_arrow.png");
}

</style>-->
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<hr align="left" width="100%" size="1" color="#A35912" />





<h5><xsl:value-of select="//lang_blocks/p2"/></h5>

<span class="footer_divider"><xsl:value-of select="//lang_blocks/p3"/></span>










<ol>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p4"/></span></li>

		<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p5"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p6"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p7"/></li>
		</ul>
<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p8"/></span></li>

		<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p9"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p10"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p11"/></li>
		</ul>

<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p12"/></span></li>

		<ul>
		<li class="d"><xsl:copy-of select="//lang_blocks/p13/* |//lang_blocks/p13/text()"/></li><br/>
		<li class="d"><xsl:value-of select="//lang_blocks/p14"/></li>
		<br/>
				<ul>
				<li class="c"><xsl:copy-of select="//lang_blocks/p15/* |//lang_blocks/p15/text()"/></li><br/>
				<li class="c"><xsl:copy-of select="//lang_blocks/p16/* |//lang_blocks/p16/text()"/></li>
				</ul>
		
		

		</ul>
		<br/>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p17"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17a"/></li>
<br/>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p18"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p18a"/></li>

		</ol>


















<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/pindex" /></a>  |  <a href="lesson_10.xml"><xsl:value-of select="//lang_blocks/previous" /></a> | <a href="lesson_12.xml"><xsl:value-of select="//lang_blocks/next" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>