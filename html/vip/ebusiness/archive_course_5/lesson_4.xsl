<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><b><xsl:value-of select="//lang_blocks/p1" /></b></p>



<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/ppoolinfo" /><br/><a href="http://www.glocalincome.com/help/partnerpool_training.xml" target="_blank">http://www.glocalincome.com/help/partnerpool_training.xml</a></p>

<p><xsl:value-of select="//lang_blocks/p120"/></p>
<p><xsl:value-of select="//lang_blocks/p121"/></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_7.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_1/lesson_7.xml</a></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_2.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_2/lesson_2.xml</a></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_5.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_3/lesson_5.xml</a></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_5.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_4/lesson_5.xml</a></p>

<hr/>

<p><img src="../images/c5_l3a.jpg" height="97" width="110" alt="Get The Picture"/> <h5><xsl:value-of select="//lang_blocks/p5" /></h5></p>

<!--<p><xsl:value-of select="//lang_blocks/p6" /></p>-->
<p><xsl:value-of select="//lang_blocks/p7" /></p>


<table class="assign">
<tr>
<td><p><xsl:value-of select="//lang_blocks/p8" /></p></td>
<td class="assign"></td>
<td><p><xsl:value-of select="//lang_blocks/p9" /></p></td>
</tr>
<tr>
<td class="assign"></td>
<td class="assign"></td>
<td class="assign"></td>
</tr>
<tr>
<td align="top"><img src="../images/c5_l3c.gif" height="200" width="255" alt="4 Levels Deep"/></td>

<td class="assign"></td>
<td><img src="../images/c5_l3d.gif" height="350" width="255" alt="Anywhere in your organization"/></td>
</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p10" /> <a href="amb_club.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></p>
<p><xsl:value-of select="//lang_blocks/p12" /> <a href="6_lines_bu.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<hr/>


<h5><p><xsl:value-of select="//lang_blocks/p14" /></p></h5>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<ul> <xsl:value-of select="//lang_blocks/p16" /><br/>
<xsl:value-of select="//lang_blocks/p16a" /><br/>
</ul>


<!--<p><xsl:value-of select="//lang_blocks/p18" /></p>

<p><xsl:value-of select="//lang_blocks/p19" /></p>
<div align="center">
<h5><xsl:value-of select="//lang_blocks/p20" />
</h5></div>

<div align="center">
<table class="xcolor">
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p21" /></td>
<td><xsl:value-of select="//lang_blocks/p22" /></td>
<td><xsl:value-of select="//lang_blocks/p23" /></td>
</tr>

<tr class="bcolor">
<td>20-24</td>
<td>2</td>
<td><xsl:value-of select="//lang_blocks/p24" /></td>
</tr>
<tr class="acolor">
<td>25-29</td>
<td>2</td>
<td><xsl:value-of select="//lang_blocks/p25" /></td>
</tr>
<tr class="bcolor">
<td>30-40</td>
<td>2</td>
<td><xsl:value-of select="//lang_blocks/p25a" /></td>
</tr>
<tr class="acolor">
<td>41-50</td>
<td>2</td>
<td><xsl:value-of select="//lang_blocks/p26" /></td>
</tr>
<tr class="bcolor">
<td>51-65</td>
<td>2</td>
<td><xsl:value-of select="//lang_blocks/p27" /></td>
</tr>
<tr class="acolor">
<td>66-80</td>
<td>3</td>
<td><xsl:value-of select="//lang_blocks/p28" /></td>
</tr>
<tr class="bcolor">
<td>81-95</td>
<td>3</td>
<td><xsl:value-of select="//lang_blocks/p29" /></td>
</tr>
<tr class="acolor">
<td>96-110</td>
<td>4</td>
<td><xsl:value-of select="//lang_blocks/p30" /></td>
</tr>
<tr class="bcolor">
<td>111-125</td>
<td>4</td>
<td><xsl:value-of select="//lang_blocks/p31" /></td>
</tr>
<tr class="acolor">
<td>126-140</td>
<td>5</td>
<td><xsl:value-of select="//lang_blocks/p32" /></td>
</tr>
<tr class="bcolor">
<td>141-155</td>
<td>5</td>
<td><xsl:value-of select="//lang_blocks/p33" /></td>
</tr>
<tr class="acolor">
<td>156-170</td>
<td>6</td>
<td><xsl:value-of select="//lang_blocks/p34" /></td>
</tr>
<tr class="bcolor">
<td>171-200</td>
<td>6</td>
<td><xsl:value-of select="//lang_blocks/p35" /></td>
</tr>
<tr class="acolor">
<td>200+</td>
<td>6</td>
<td><xsl:value-of select="//lang_blocks/p36" /></td>
</tr>
<tr class="bcolor">
<td>400+</td>
<td>6</td>
<td><xsl:value-of select="//lang_blocks/p37" /></td>
</tr>

</table>

</div>-->
<h5><xsl:value-of select="//lang_blocks/p38" /></h5>
<p><xsl:value-of select="//lang_blocks/p39" /> <b><xsl:value-of select="//lang_blocks/p40" /></b></p>
<p><xsl:value-of select="//lang_blocks/p41" /></p>
<!--<div align="center"><p><img src="http://www.clubshop.com/vip/ebusiness/images/view_vertical_transfer.jpg" height="170" width="150" alt="Transferring a Member"/></p></div>-->

<p><xsl:value-of select="//lang_blocks/p42" /></p>

<p><xsl:value-of select="//lang_blocks/p43" /></p>

<p><xsl:value-of select="//lang_blocks/p45" /><b> <xsl:value-of select="//lang_blocks/p46" /></b> <xsl:value-of select="//lang_blocks/p47" /></p>
<p><xsl:value-of select="//lang_blocks/p48a" /></p>
<p><xsl:value-of select="//lang_blocks/p48" /></p>


<!--<p><xsl:value-of select="//lang_blocks/p50" /></p>

<p><xsl:value-of select="//lang_blocks/p51" /></p>
<p><xsl:value-of select="//lang_blocks/p52" /></p>

<table>

<tr>
<td><p><b><xsl:value-of select="//lang_blocks/p53" /></b></p></td>
</tr>
<tr>
<td>
<ol>
<li><xsl:value-of select="//lang_blocks/p54" /></li>
<li><xsl:value-of select="//lang_blocks/p55" /></li>
<li><xsl:value-of select="//lang_blocks/p56" /></li>
<li><xsl:value-of select="//lang_blocks/p57" /></li>
<li><xsl:value-of select="//lang_blocks/p58" /></li>
<li><xsl:value-of select="//lang_blocks/p59" /></li>
<li><xsl:value-of select="//lang_blocks/p60" /></li>
<li><xsl:value-of select="//lang_blocks/p61" /></li>
<li><xsl:value-of select="//lang_blocks/p62" /></li>
</ol>
</td>
</tr></table>

<hr/>-->
<!--<h5><xsl:value-of select="//lang_blocks/p63" /></h5>
<p><xsl:value-of select="//lang_blocks/p64" /></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p65" /></li>
<li><xsl:value-of select="//lang_blocks/p66" /></li>
<li><xsl:value-of select="//lang_blocks/p67" /></li>
</ol>

<p><b><xsl:value-of select="//lang_blocks/p68" /></b></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p69" /></li>
<li><xsl:value-of select="//lang_blocks/p70" /></li>
<li><xsl:value-of select="//lang_blocks/p71" /></li>
<li><xsl:value-of select="//lang_blocks/p72" /></li>
</ol>
<div align="center">
<table class="assign">
<tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p73" /><a onclick="" href="http://www.clubshop.com/cgi/vipacc.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p74" /></a><xsl:value-of select="//lang_blocks/p75" />
</p></td>
</tr>
</table></div>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p76" /></h5>

<p><xsl:value-of select="//lang_blocks/p77" /></p>
<p><xsl:value-of select="//lang_blocks/p78" /></p>

<p><a onclick="" href="../view_autostack.html" target="_blank"><xsl:value-of select="//lang_blocks/p79" /></a></p>

<p><xsl:value-of select="//lang_blocks/p80" /></p>
<div align="center"><p><img src="http://www.clubshop.com/vip/ebusiness/images/c5_l3h.gif" height="172" width="283" alt=""/></p></div>

<p><xsl:value-of select="//lang_blocks/p81" /></p>
<p><i><xsl:value-of select="//lang_blocks/p82" /></i></p>-->


<h5><xsl:value-of select="//lang_blocks/p122" /></h5>
<p><xsl:value-of select="//lang_blocks/p123" /></p>
<p><xsl:value-of select="//lang_blocks/p124" /></p>
<ul>
<xsl:value-of select="//lang_blocks/p125" /><br/>
<xsl:value-of select="//lang_blocks/p126" /><br/>
<xsl:value-of select="//lang_blocks/p127" /><br/>
<xsl:value-of select="//lang_blocks/p125" /><br/>

</ul>






<!--<h5><xsl:value-of select="//lang_blocks/p83" /></h5>
<p><xsl:value-of select="//lang_blocks/p84" /></p>
<p><xsl:value-of select="//lang_blocks/p85" /></p>
<div align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/view_rollup.gif" height="214" width="289" alt="Roll Up Before and After"/></div>

<p><xsl:value-of select="//lang_blocks/p86" /></p>
<p><xsl:value-of select="//lang_blocks/p87" /></p>


<h5><xsl:value-of select="//lang_blocks/p88" /></h5>
<p><xsl:value-of select="//lang_blocks/p89" /> <a onclick="" href="http://www.clubshop.com/SIP.xml" target="_blank"><xsl:value-of select="//lang_blocks/p90" /></a></p>
<p><xsl:value-of select="//lang_blocks/p91" /></p>

<p><xsl:value-of select="//lang_blocks/p92" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p93" /></h5>
<p><xsl:value-of select="//lang_blocks/p94" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p95" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/vip_transfer.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></li>
<li><xsl:value-of select="//lang_blocks/p96" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/transfunc_2.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></li>
<li><xsl:value-of select="//lang_blocks/p97" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/transfer_multi.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></li>
<li><xsl:value-of select="//lang_blocks/p98" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/transfer_pp.html" target="_blank"><xsl:value-of select="//lang_blocks/p11" /></a></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p99" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p100" /></h5>
<ul>
<li><xsl:value-of select="//lang_blocks/p101" /></li>
<li><xsl:value-of select="//lang_blocks/p102" /></li>
</ul>

<p><b><xsl:value-of select="//lang_blocks/p103" /></b></p>
<p><xsl:value-of select="//lang_blocks/p104" /></p>

<p><xsl:value-of select="//lang_blocks/p105" /></p>

<p><xsl:value-of select="//lang_blocks/p106" /></p>
<p><b><xsl:value-of select="//lang_blocks/p107" /></b></p>
<p><xsl:value-of select="//lang_blocks/p108" /></p>
<p><xsl:value-of select="//lang_blocks/p109" /></p>-->



<h5><xsl:value-of select="//lang_blocks/p128" /></h5>

<p><xsl:value-of select="//lang_blocks/p129" /></p>
<p><xsl:value-of select="//lang_blocks/p130" /></p>

<h5><xsl:value-of select="//lang_blocks/p110" /></h5>

<p><xsl:value-of select="//lang_blocks/p111" /></p>

<p><xsl:value-of select="//lang_blocks/p112" /></p>

<p><xsl:value-of select="//lang_blocks/p113" /></p>
<div align="center"><h5><xsl:value-of select="//lang_blocks/p114" /></h5></div>







<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a 
href="lesson_5.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>