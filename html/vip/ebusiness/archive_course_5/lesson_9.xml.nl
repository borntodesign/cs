<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="lesson_9.xsl" ?><lang_blocks><p0>Les 9</p0><p1>Les 9</p1><p2>Contact opnemen met Affiliate Partners</p2><p3>Welkom e-mail -</p3><p4>Skype</p4><p5>Indien u heeft vastgesteld dat de Affiliate Partner een goede Skypenaam heeft, vraag hen dan u toe te voegen als Skype Contact NADAT u uw welkom e-mail heeft gestuurd. Vervang de contact-verzoek tekst van skype voor de volgende tekst (en bewaar deze als template om steeds opnieuw te gebruiken):</p5><p6 literal="1">Hallo <b>VOORNAAM</b>,</p6><p7>U bent onlangs Glocal Income Affiliate Partner geworden en ik wil u graag persoonlijk verwelkomen en mij beschikbaar stellen om al uw vragen te beantwoorden die u misschien heeft over onze Zakelijke Mogelijkheid. Voegt u mij alstublieft toe als Skype contact, zodat wij binnenkort kunnen afspreken indien u dit wilt bespreken of mij vragen wilt stellen.</p7><p8>Hier is tevens de link voor u, om in te loggen in uw Affiliate Partner Business Center.</p8><p8a literal="1">Uw ID nummer is: <b>IDNUMMER</b></p8a><p9>Welkom bij Glocal Income en ik kijk er naar uit een afspraak met u te maken om u te spreken wanneer u hiervoor tijd heeft!</p9><p10>UW NAAM</p10><p11>Persoonlijke Website Link</p11><p12>Indien u onzeker bent of de Skypenaam die u hebt gevonden bij de Affiliate Partner hoort, vervang dan de contact-verzoek tekst van skype, voor de volgende tekst (en bewaar deze als template om in de toekomst steeds weer te gebruiken) voor elke potentiële Skypename match:</p12><p13 literal="1">Bent u dezelfde <b>VOLLEDIGE NAAM</b> die onlangs Glocal Income Affiliate Partner is geworden? Zo ja, dan ben ik uw Team Coach en wil ik u graag persoonlijk verwelkomen en mij beschikbaar stellen om alle vragen die u heeft over onze Zakelijke Mogelijkheid te beantwoorden. Voeg mij alstublieft toe als Skype Contact, zodat we eens kunnen afspreken indien u dit wilt bespreken of mij vragen wilt stellen.</p13><p14>Indien u niet dezelfde persoon bent, dan verontschuldig ik mij voor dit ongemak.</p14><p15>Anders, welkom bij Glocal Income en ik kijk er naar uit met u af te spreken om u hierover te spreken!</p15>



<pindex>Cursus Index</pindex>
<previous>Vorige</previous>
<next>Volgende</next>




</lang_blocks>
