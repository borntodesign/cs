<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /> <a onclick="" href="http://www.glocalincome.com/CompensationPlan.xml" target="_blank"><xsl:value-of select="//lang_blocks/p2" /></a><xsl:value-of select="//lang_blocks/p3" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p4" /></h5>

<p><xsl:value-of select="//lang_blocks/p5" /></p> 
<p><xsl:value-of select="//lang_blocks/p6" /></p> 
<table class="assign">
<tr>
<td>
<ul>
<li><xsl:value-of select="//lang_blocks/p7" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p8" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p9" /></li><br/>
</ul>
</td>
</tr></table>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p10" /></h5>

<table class="assign">
<tr>
<td><img src="../images/get2feesthrough.gif" width="113" height="136" alt="Get Two and your Fees are Through"/></td>
<td><p><xsl:value-of select="//lang_blocks/p11" /></p><p><xsl:value-of select="//lang_blocks/p12" /></p></td>
</tr>
</table>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p13" /></h5>

<p><xsl:value-of select="//lang_blocks/p14" /></p>

<table>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p15" /></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td>600 x 46% = $276.00</td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p17" /></td>
<td><xsl:value-of select="//lang_blocks/p18" /></td>
<td>(-) less $120.00 pay out = $156.00 net pay</td>
</tr>
<tr class="acolor">
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td><xsl:value-of select="//lang_blocks/p20" /></td>
<td>(-) less $55.00 monthly fees = $101.00</td>
</tr>
<tr class="bcolor">
<td><xsl:value-of select="//lang_blocks/p21" /></td>
<td><xsl:value-of select="//lang_blocks/p22" /></td>
<td>(+) $2.00 = $103.00 net pay</td>
</tr>

</table>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p23" /></h5>


<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p25" /></p>

<h5><xsl:value-of select="//lang_blocks/p26" /></h5>
<p><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p29" /></h5>
<p><xsl:value-of select="//lang_blocks/p30" /></p>








<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_4.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>