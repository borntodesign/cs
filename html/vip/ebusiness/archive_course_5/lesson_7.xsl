<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<!--<style>
body {
	font-size: 100%;
	background-color: white;
	color: black;
}
p{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 }

table.assign{
	border: thin solid #404269;
	width: 100%;
	}


td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #9C6300;
}

p.indent{text-indent: 2em;
}

div.indent{text-indent: 1em;
}

p.sub{
 font-family: Verdana, Arial, sans-serif;
 font-size: medium;
 font-weight: bold;
 color: #9A6843;
 text-indent: 5%;
  }
h5 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: Medium;
	color: #9A6843;
}
h4 {color: #404269;
}

table.xcolor{border: thin solid #9C6300;
width: 90%;
}


tr.acolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #DCAF72;
 text-align: left;
 }
 tr.bcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FFF9F0;
 text-align: left;
  }
  
 tr.cell{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 font-weight: bold;
 text-align: center;
 }
 
 
 	table.notice{
	border: thin solid #FF9C00;
	width: 100%;
	text-align: center;
	background-color : #FFFFCE;
}
td.notice{font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: Black;
	text-align: center;
	}
	
td.quote{font-family: Verdana, Arial, sans-serif;
	font-size: smaller;
	color: Black;
}
 ul{ font-family: Verdana, Arial, sans-serif;
  font-size: small;
  }

li { font-family: Verdana, Arial, sans-serif;
  font-size: small; }

table.xcolor{border: thin solid #9C6300;
width: 90%;
}

p.red{ color: #FF0000;}
p.close {
	font-family: Arial, Helvetica, sans-serif;
	font-size : x-small;
}

tr.ccolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
  background-color: #EFE7DE;

 }
 tr.dcolor{font-family: Verdana, Arial, sans-serif;
 font-size: smaller;
 background-color: #FAF5F1;

  }
</style>-->
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>




<h5><xsl:value-of select="//lang_blocks/p0"/></h5>

<h5><xsl:value-of select="//lang_blocks/p1"/></h5>




<p><b><xsl:value-of select="//lang_blocks/p2"/></b></p>



<ol>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p3"/></span></li>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p4"/></span></li>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p5"/></span></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p6"/></p>

<ul>
		<li class="c"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p7"/></span></li>
	
		<br/><xsl:value-of select="//lang_blocks/p8"/><br/>

	<ol>
		<li class="d"><xsl:value-of select="//lang_blocks/p9"/></li><br/>
		<li class="d"><xsl:value-of select="//lang_blocks/p10"/></li>
	</ol>
</ul>


<h5><xsl:value-of select="//lang_blocks/p11"/></h5>

<ol>
<li><img src="../images/icon_email.png" height="48" width="54" alt="email_icon"/><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p12"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>
<br/>
	<ul>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p14"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/><br/><xsl:value-of select="//lang_blocks/p16"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p17"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p18"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p19"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p21"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/><br/><xsl:value-of select="//lang_blocks/p23"/></li><br/>
	</ul>
<br/>

<li><img src="../images/Skype.png" height="35" width="75" alt="skype_logo"/><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p24"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p25"/></li>
	<br/><ul>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p26"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/><br/><b><xsl:value-of select="//lang_blocks/p27a"/></b></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p28"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p30"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p32"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33"/></li><br/>
	<li class="d"><span class="footer_divider"><xsl:value-of select="//lang_blocks/p34"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p35"/></li><br/>
	</ul>
<br/>
<li><img src="../images/icon_webpage.png" height="52" width="80" alt="wepage_icon"/><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p36"/></span></li>

	<br/><ul>
	<li class="d"><xsl:value-of select="//lang_blocks/p37"/></li><br/>
	<li class="d"><xsl:value-of select="//lang_blocks/p38"/></li><br/>
	<li class="d"><xsl:value-of select="//lang_blocks/p39"/></li><br/>
<br/>
		<div class="colora">
		<i>
		<xsl:value-of select="//lang_blocks/p40"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p41"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p42"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p43"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p44"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p45"/><br/>
		</i>
		</div>
	
	</ul>

<br/>
<li><img src="../images/icon_social.png" height="75" width="76" alt="social_media_icons"/><xsl:text> </xsl:text><span class="footer_divider"><xsl:value-of select="//lang_blocks/p46"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47"/><xsl:text> </xsl:text><a href="http://www.facebook.com" target="_blank">http://www.facebook.com</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47a"/><br/><br/><xsl:value-of select="//lang_blocks/p48"/></li>

	
</ol>



<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/pindex" /></a>  |  <a href="lesson_6.xml"><xsl:value-of select="//lang_blocks/previous" /></a> | <a href="lesson_8.xml"><xsl:value-of select="//lang_blocks/next" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>