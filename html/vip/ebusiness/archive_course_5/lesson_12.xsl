<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title>e-Business Institute</title>
<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.q {
	font-family: Verdana, Arial, sans-serif;
	text-transform:uppercase;
	letter-spacing : .5px;
	font-weight : bold;
	
}
.ans {
	font-family: Verdana, Arial, sans-serif;

}

tr.a {font-family: Verdana, Arial;
font-size: 12px;
background-color: #f4f4f4;
}


tr.b {font-family: Verdana, Arial;
font-size: 12px;
background-color: #FBFDF4;
}

tr.c {font-family: Verdana, Arial;
font-size: 12px;
background-color: #FFFFFF;
}

</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<hr align="left" width="100%" size="1" color="#A35912" />

<h5><xsl:value-of select="//lang_blocks/p2"/></h5>

<p><span class="footer_divider"><xsl:value-of select="//lang_blocks/p3"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3a"/></p>

<ol>
<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p4"/></span></li>

		<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p5"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p6"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p7"/></li>
		</ul>
<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p8"/></span></li>
	
		
		<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p9"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p10"/></li>
		<br/>
			<ul>
			<li class="c"><xsl:value-of select="//lang_blocks/p11"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p12"/></li>
			<br/>
					<ul>
						<li><xsl:value-of select="//lang_blocks/p13"/></li>
						<li><xsl:value-of select="//lang_blocks/p14"/></li>
					</ul>
			
			</ul>
		</ul>

<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p15"/></span></li>

	<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p16"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p17"/></li>
		</ul>



<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p18"/></span></li>

		<ul>
		<li class="d"><xsl:value-of select="//lang_blocks/p19"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p20"/></li>
		<br/>
			<ul>
			<li class="c"><xsl:value-of select="//lang_blocks/p21"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p22"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p23"/></li>
			<li class="c"><xsl:value-of select="//lang_blocks/p24"/></li>
			</ul><br/>
		<li class="d"><xsl:value-of select="//lang_blocks/p25"/></li>
		<li class="d"><xsl:value-of select="//lang_blocks/p26"/></li>
		</ul>
		
		
<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p27"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27a"/></li>
		
	<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p28"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p28a"/></li>
<br/>

<li><span class="footer_divider"><xsl:value-of select="//lang_blocks/p29"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29a"/></li>

</ol>

<hr align="left" width="100%" size="1" color="#A35912" />

<p class="footer_divider"><xsl:value-of select="//lang_blocks/example_role_playing1"/></p>
<table width="60%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

	  <tr class="a"> 
		<td>Role Play Example</td>
		<td><embed type="application/x-shockwave-flash" flashvars="audioUrl=../MP3s/teamcoach_lesson1_intro_revised.mp3" src="http://www.google.com/reader/ui/3523697345-audio-player.swf" width="400" height="27" quality="best"></embed></td>
	  </tr>
</table>
<br/>

<hr align="left" width="100%" size="1" color="#A35912" />
<p><span class="footer_divider"><xsl:value-of select="//lang_blocks/p30"/></span></p>

<ul><ul>
<li class="d"><xsl:value-of select="//lang_blocks/p31"/></li>
<li class="d"><xsl:value-of select="//lang_blocks/p32"/></li>
</ul>


<p class="footer_divider"><xsl:value-of select="//lang_blocks/p33"/></p>

<p><span class="q"><xsl:value-of select="//lang_blocks/p34"/></span><br/>
<span class="ans"><xsl:value-of select="//lang_blocks/p35"/></span></p>

<p><span class="q"><xsl:value-of select="//lang_blocks/p36"/></span><br/>
<span class="ans"><xsl:value-of select="//lang_blocks/p37"/></span></p>

<p><span class="q"><xsl:value-of select="//lang_blocks/p38"/></span><br/>
<span class="ans"><xsl:value-of select="//lang_blocks/p39"/></span></p>

<p><span class="q"><xsl:value-of select="//lang_blocks/p40"/></span><br/>
<span class="ans"><xsl:value-of select="//lang_blocks/p41"/></span></p>


<p><xsl:value-of select="//lang_blocks/p42"/></p>

	</ul>
<hr/>

<p class="footer_divider"><xsl:value-of select="//lang_blocks/role_playing"/></p>

<p><xsl:value-of select="//lang_blocks/role_playinga"/></p>
<table width="60%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

	  <tr class="a"> 
		<td><xsl:value-of select="//lang_blocks/role_playingb"/></td>
		<td><embed type="application/x-shockwave-flash" flashvars="audioUrl=../MP3s/group-1.mp3" src="http://www.google.com/reader/ui/3523697345-audio-player.swf" width="400" height="27" quality="best"></embed></td>
	  </tr>
	  
	  <tr class="c"><td colspan="2"></td></tr>
	  
	  
	  
	   <tr class="b"> 
	  <td><xsl:value-of select="//lang_blocks/role_playingc"/></td>
		<td><embed type="application/x-shockwave-flash" flashvars="audioUrl=../MP3s/group-2.mp3" src="http://www.google.com/reader/ui/3523697345-audio-player.swf" width="400" height="27" quality="best"></embed></td>
	  </tr>
	  
</table>


<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/pindex" /></a>  |  <a href="lesson_11.xml"><xsl:value-of select="//lang_blocks/previous" /></a> | <a href="lesson_13.xml"><xsl:value-of select="//lang_blocks/next" /></a></p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>