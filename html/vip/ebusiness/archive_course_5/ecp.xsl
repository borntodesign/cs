   <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
   
   
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>

<img src="../images/traineeline.gif" />

<table>
<tr>
<td><p><xsl:copy-of select="//lang_blocks/p1/ * | //lang_blocks/p1/text()" /></p>


<p><xsl:copy-of select="//lang_blocks/p2/ * | //lang_blocks/p2/text()" /></p>

<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:copy-of select="//lang_blocks/p4" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/members/clubcash_details.shtml" target="_blank">http://www.clubshop.com/members/clubcash_details.shtml</a></p>
<p><xsl:copy-of select="//lang_blocks/p5/ * | //lang_blocks/p5/text()" /></p>

<p><xsl:value-of select="//lang_blocks/p6"/></p>

<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><b><xsl:copy-of select="//lang_blocks/p17" /></b><br/>

<b><xsl:value-of select="//lang_blocks/p8"/></b></p>





</td>
</tr>
</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>