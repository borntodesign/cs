<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" /> 
 <xsl:template match="/">
 <html>
 <head>
  <title>E-Business Institute</title> 
  <link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css" /> 
  <style>body { font-size: 100%; background-color: white; color: black; } p{font-family: Verdana, Arial, sans-serif; font-size: smaller; } table.assign{ border: thin solid #404269; width: 100%; } td.assign{ font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: bold; color: White; background-color: #404269; border-bottom : Gray; } hr{ color: #9C6300; } p.indent{text-indent: 2em; } div.indent{text-indent: 1em; } p.sub{ font-family: Verdana, Arial, sans-serif; font-size: medium; font-weight: bold; color: #9A6843; text-indent: 5%; } h5 { font-family: Arial, Helvetica, sans-serif; font-size: Medium; color: #9A6843; } h4 {color: #404269; } table.xcolor{border: thin solid #9C6300; width: 90%; } tr.acolor{font-family: Verdana, Arial, sans-serif; font-size: smaller; background-color: #DCAF72; text-align: center; } tr.bcolor{font-family: Verdana, Arial, sans-serif; font-size: smaller; background-color: #FFF9F0; text-align: center; } tr.cell{font-family: Verdana, Arial, sans-serif; font-size: smaller; font-weight: bold; text-align: center; } table.notice{ border: thin solid #FF9C00; width: 100%; text-align: center; background-color : #FFFFCE; } td.notice{font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: bold; color: Black; text-align: center; } td.quote{font-family: Verdana, Arial, sans-serif; font-size: smaller; color: Black; } ul{ font-family: Verdana, Arial, sans-serif; font-size: small; } li { font-family: Verdana, Arial, sans-serif; font-size: small; } table.xcolor{border: thin solid #9C6300; width: 90%; } p.red{ color: #FF0000;} p.close { font-family: Arial, Helvetica, sans-serif; font-size : x-small; }</style> 
  </head>
 <body>
 <table width="1000">
 <tr>
 <td class="top">
  <img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /> 
  </td>
  </tr>
  </table>
 <table width="900">
 <tr>
 <td>
 <sub>
  <xsl:value-of select="//lang_blocks/p0" /> 
  </sub>
  <br /> 
  <img src="../images/traineeline.gif" /> 
  <br /> 
 <p class="red">
  <xsl:value-of select="//lang_blocks/p1" /> 
  </p>
 <p>
 <b>
 <xsl:value-of select="//lang_blocks/p2" /> 
  </b>
  <xsl:value-of select="//lang_blocks/p13" /> 
 <font color="#FF0000">
  <xsl:value-of select="//lang_blocks/p3" /> 
  </font>
  <xsl:value-of select="//lang_blocks/p14" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p15" /> 
 <font color="#FF0000">
  ( 
  <xsl:value-of select="//lang_blocks/p3" /> 
  )! 
  </font>
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p16" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text><a href="http://www.glocalincome.com/" target="_blank"><xsl:value-of select="//lang_blocks/p7" /> </a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p18" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p19" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p20" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p21" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p22" /> 
 <font color="#FF0000">
  <xsl:value-of select="//lang_blocks/p22a" /> 
  </font>
  <xsl:value-of select="//lang_blocks/p22b" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p22c" /> 
 <font color="#FF0000">
  <xsl:value-of select="//lang_blocks/p26" /> 
  </font>
  ( 
  <xsl:value-of select="//lang_blocks/p29" /> 
 <font color="#FF0000">
  <xsl:value-of select="//lang_blocks/p27" /> 
  </font>
  ) 
  <xsl:value-of select="//lang_blocks/p30" /> 
 <font color="#FF0000">
  <xsl:value-of select="//lang_blocks/p28" /> 
  </font>
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p31" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p23" /> 
  </p>
 <p>
 <font color="#FF0000">
 <xsl:value-of select="//lang_blocks/p8" /> 
  </font>
  <br /> 
  <xsl:value-of select="//lang_blocks/p24" /> 
  </p>
 <p>
  <xsl:value-of select="//lang_blocks/p25" /> 
  </p>
  <hr /> 
 <div align="center">
 <p class="close">
 <a href="javascript:self.close();">
  <xsl:value-of select="//lang_blocks/close" /> 
  </a>
  </p>
  </div>
  </td>
  </tr>
  </table>
  </body>
  </html>
  </xsl:template>
  </xsl:stylesheet>