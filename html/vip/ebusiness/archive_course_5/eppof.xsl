   <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
   
   
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />

<table>
<tr>
<td><p><xsl:copy-of select="//lang_blocks/p1/ * | //lang_blocks/p1/text()" /></p>


<p><xsl:copy-of select="//lang_blocks/p2/ * | //lang_blocks/p2/text()" /></p>

<p><xsl:copy-of select="//lang_blocks/p3/ * | //lang_blocks/p3/text()" /><br/>
<a href="http://www.glocalincome.com/">http://www.glocalincome.com</a></p>


<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>

<p><xsl:copy-of select="//lang_blocks/p7/ * | //lang_blocks/p7/text()" /></p>

<p><xsl:value-of select="//lang_blocks/p8"/></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:copy-of select="//lang_blocks/p10/ * | //lang_blocks/p10/text()" /></p>




<p><xsl:copy-of select="//lang_blocks/p11/ * | //lang_blocks/p11/text()" /></p>


<p><xsl:value-of select="//lang_blocks/p13" /> <br/>
<a href="https://www.clubshop.com/change-Shopper.html" target="_blank">https://www.clubshop.com/change-Shopper.html</a></p>


<p><xsl:value-of select="//lang_blocks/p15" /> <br/>
<a href="https://www.clubshop.com/remove-me.html" target="_blank">https://www.clubshop.com/remove-me.html</a></p>

<p><xsl:copy-of select="//lang_blocks/p16/ * | //lang_blocks/p16/text()" /></p>




</td>
</tr>
</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
