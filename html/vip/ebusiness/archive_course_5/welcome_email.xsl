<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c5_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t5_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />

<table width="600">
<tr>
<td><p class="red"><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p3" /></font><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p3" /></font>!</p>
<p><xsl:value-of select="//lang_blocks/p6" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p3" /> <xsl:value-of select="//lang_blocks/p7" /></font> <xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>

<p><xsl:value-of select="//lang_blocks/p11" /><a href="http://www.clubshop.com/"><xsl:value-of select="//lang_blocks/p12" /></a><xsl:value-of select="//lang_blocks/p13" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p14" /></font></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<p><xsl:value-of select="//lang_blocks/p17" /><br/>
<font color="#FF0000"><xsl:value-of select="//lang_blocks/p18" /></font><br/><xsl:value-of select="//lang_blocks/p19" /><font color="#FF0000"><xsl:value-of select="//lang_blocks/p20" /></font></p>
<p><xsl:value-of select="//lang_blocks/p21" /> <a href="https://www.clubshop.com/change-Shopper.html"><xsl:value-of select="//lang_blocks/p22" /></a></p>

<p><xsl:value-of select="//lang_blocks/p23" /><a href="https://www.clubshop.com/remove-me.html"><xsl:value-of select="//lang_blocks/p24" /></a></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p25" /></font></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p26" /></b></p>



</td>
</tr>
</table>

<p class="close"><a href="javascript:self.close();"><xsl:value-of select="//lang_blocks/p27" /></a></p>


     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
