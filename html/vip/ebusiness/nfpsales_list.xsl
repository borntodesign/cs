<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
    
         <html><head>
            <title>Non-Fee Personal Sales</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

body {
   background-color: #245923;
} 
td.back{background-image: url(http://www.clubshop.com/images/bg_header.jpg);}

</style>

</head>
<body>
<div align="center"> 
<table width="950px" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      
    	
      
      
      <tr>
       <td class="back"><img src="http://www.clubshop.com/images/gi.jpg" width="640" height="70" align="left"/></td>

      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">

  
            <tr><td align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  <a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle" /></a><a href="javascript:window.close();" class="nav_footer_brown"></a><span class="Header_Main"> </span>
<span class="Header_Main"></span><hr align="left" width="100%" size="1" color="#A35912" />
</td></tr>




<tr><td>
      <table width="100%" cellpadding="5">
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"> 
            
            <div align="left">

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p0"/></span>
<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>




<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr><td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p16"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p3"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p4"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p17"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p5"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p6"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p18"/></td></tr>
<tr><td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p19"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p20"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p21"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p7"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p8"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p22"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p9"/></td></tr>

<tr><td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p23"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p10"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p11"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p24"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p25"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p12"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p26"/></td></tr>
<tr><td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p28"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p29"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p30"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p31"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p13"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p14"/></td>
<td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p15"/></td></tr>
</table>

















<hr/>
</div>

</td></tr></table>


  
    </td>
  </tr></table>
  



</td></tr></table>

<tr><td align="center"><div align="center"><img src="/images/icons/icon_copyright.gif" height="20" width="306" alt="copyright"/></div></td>
</tr>


</td></tr></table>
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>