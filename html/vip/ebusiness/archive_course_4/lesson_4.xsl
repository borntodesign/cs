<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="../images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/title" /></sub>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><b><xsl:value-of select="//lang_blocks/p7" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7a" /><xsl:text> </xsl:text><a href="http://www.glocalincome.com/manual/policies/mbr_processing.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p8" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p9" /></p>

<p><xsl:value-of select="//lang_blocks/p35" /></p>
<p><xsl:value-of select="//lang_blocks/p36" /><br/><b>http://www.glocalincome.com/cgi/rd/1/123456</b> <br/><xsl:value-of select="//lang_blocks/p37" /></p>

<table>
<tr><td class="a"><xsl:value-of select="//lang_blocks/p2" /></td><td><img src="../images/sm_cr_card.gif"/></td></tr>
<tr><td><hr/></td></tr>


<tr><td class="a"><xsl:value-of select="//lang_blocks/p4" /><xsl:text> </xsl:text><a href="signup_vipcontrol.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/example" /></a></td><td><img src="../images/email_optin.gif"/></td></tr>
<tr><td><hr/></td></tr>
</table>




<hr/>

<table border="1" bordercolor="#3E6768" cell-padding="5">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub>
<p class="a"><xsl:value-of select="//lang_blocks/p10" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/vip/coop.shtml" target="_blank"><xsl:value-of select="//lang_blocks/p10a" /></a>.</p>
<br/>

<p><xsl:copy-of select="//lang_blocks/p38/*|//lang_blocks/p38/text()" /></p>
<br/>
</td></tr>

</table>
<br/>
<sub><xsl:value-of select="//lang_blocks/p11" /></sub>

<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>

<table border="0" bordercolor="#3E6768">

<tr><td class="a"><xsl:copy-of select="//lang_blocks/p15/*|//lang_blocks/p15/text()" /></td></tr>

<tr><td class="a"><xsl:copy-of select="//lang_blocks/p16/*|//lang_blocks/p16/text()" /></td></tr>

<tr><td class="a"><xsl:copy-of select="//lang_blocks/p18/*|//lang_blocks/p18/text()" /></td></tr>

</table>
<p><xsl:value-of select="//lang_blocks/p20" /></p>


<sub><xsl:value-of select="//lang_blocks/p23" /></sub>
<br/><hr/>

<sub><xsl:value-of select="//lang_blocks/p24" /></sub>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<p><b><xsl:value-of select="//lang_blocks/p26" /></b></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" /></p>

<ul><li><xsl:value-of select="//lang_blocks/p29" /></li>
<li><xsl:value-of select="//lang_blocks/p30" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p31" /></p>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p32" /></sub>
<p><xsl:value-of select="//lang_blocks/p33" /></p>

<h5><xsl:value-of select="//lang_blocks/p34" /></h5>














</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_5.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
