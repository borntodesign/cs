<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" width="705" height="110" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p3a" /><br/>
<img src="../images/greenline.gif"/></sub> 
<p><i><xsl:value-of select="//lang_blocks/p5" /></i></p>

         
<h5><xsl:value-of select="//lang_blocks/p4" /></h5>     
<p><xsl:value-of select="//lang_blocks/p6" /></p>   
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>     


         
<p><xsl:value-of select="//lang_blocks/p9" /></p>   
         
<hr/>
 </td></tr></table>       
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_2.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>