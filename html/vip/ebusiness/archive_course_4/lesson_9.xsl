<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="../images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/head" /></sub>

<p><xsl:value-of select="//lang_blocks/p1" /></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p2" /></li>

<br/><br/>
<li><xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p6" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7" />
<br/><br/><xsl:value-of select="//lang_blocks/p7a" /></li><br/>
<br/>
<li><xsl:value-of select="//lang_blocks/p8" /></li>  <br/>
<p><xsl:value-of select="//lang_blocks/p10" /><br/>
<b>http://www.glocalincome.com/cgi/apbc?a=IDNUMBER&amp;submit=Submit</b></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<table border="1"><tr><td>
<a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l9_folder.html" onclick="window.open(this.href); return false;"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" border="0" alt=""/></a></td>
<td><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l9_folder.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p12" /></a>
</td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<!--<p><b><xsl:value-of select="//lang_blocks/p15" /></b> <xsl:text> </xsl:text><a href="mailto:support1@glocalincome.com?Subject=Team_Coach_Follow_Up_Problem">support1@glocalincome.com</a></p>-->





<li><xsl:value-of select="//lang_blocks/p21" /></li>

</ol>
<sub><xsl:value-of select="//lang_blocks/p26" /><xsl:text> </xsl:text><img src="http://www.clubshop.com/vip/ebusiness/images/bounce.gif"/></sub>

<p><xsl:value-of select="//lang_blocks/p27" /></p>

<ul><li><xsl:value-of select="//lang_blocks/p28" /></li>

<li><xsl:value-of select="//lang_blocks/p29" /></li>
<li><xsl:value-of select="//lang_blocks/p30" /></li>
</ul>
<table width="500" border="1" bordercolor="#3E6768" cellpadding="5" align="center">
<tr><td class="a"><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>

<tr><td><p><xsl:value-of select="//lang_blocks/p32" /></p>
<p><b><xsl:value-of select="//lang_blocks/p33" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34" /><xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p35" />"<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36" /> <xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p37" />".<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38" /></p>
</td></tr></table>
<br/>
<sub><xsl:value-of select="//lang_blocks/p39" /></sub>
<br/><br/>

<table width="500" border="1" bordercolor="#3E6768" cellpadding="5" align="center">
<tr><td class="a"><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p40" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41" /><br/>

<a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l9_remove.html" onclick="window.open(this.href); return false;"> <img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" border="0"/></a>
<xsl:value-of select="//lang_blocks/p42" />
</td></tr>
</table>
<p><xsl:value-of select="//lang_blocks/p43" /><xsl:text> </xsl:text>" <xsl:value-of select="//lang_blocks/p44" /> " <xsl:value-of select="//lang_blocks/p45" /></p>
<sub><xsl:value-of select="//lang_blocks/p46" /></sub>
<p><xsl:value-of select="//lang_blocks/p47" /></p>
<p><xsl:value-of select="//lang_blocks/p48" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p49" /></p>
<ul>
<!--<li><b><xsl:value-of select="//lang_blocks/p50" /></b><br/><xsl:value-of select="//lang_blocks/p51" />http://www.clubshop.com/vip/ebusiness/course_4/lesson_2.xml</li><br/>-->
<li><b><xsl:value-of select="//lang_blocks/p52" /></b><br/><xsl:value-of select="//lang_blocks/p53" />http://www.clubshop.com/vip/ebusiness/course_4/lesson_4.xml</li>

<!--<li><b><xsl:value-of select="//lang_blocks/p54" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p55" />http://www.clubshop.com/vip/ebusiness/course_4/lesson_7.xml</li>
<li><b><xsl:value-of select="//lang_blocks/p56" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p57" />http://www.clubshop.com/vip/ebusiness/course_4/lesson_8.xml</li>
<li><b><xsl:value-of select="//lang_blocks/p59" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p60" />http://www.clubshop.com/vip/ebusiness/course_4/lesson_8.xml</li>
-->
</ul>
<hr/>
<!--<sub><xsl:value-of select="//lang_blocks/p62" /></sub>
<p>( <a href="http://www.clubshop.com/vip/ebusiness/course_4/BI.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p63" /></a> <xsl:value-of select="//lang_blocks/p64" />)</p>
-->
<p><xsl:value-of select="//lang_blocks/p65" /></p>

</td></tr></table>

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_4/index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/1562"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>