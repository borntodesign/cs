<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

.question {font-weight:bold;}

</style>

</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/head" /></sub>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>  
<p><xsl:value-of select="//lang_blocks/p3" />( <xsl:value-of select="//lang_blocks/p4" />) <xsl:value-of select="//lang_blocks/p5" /> </p> 

<sub><xsl:value-of select="//lang_blocks/p_6" /></sub>

<p><xsl:value-of select="//lang_blocks/p_7" /></p>



<ul>
<span class="question"><xsl:value-of select="//lang_blocks/p_8" /></span><br/>
<br/>
<br/><xsl:value-of select="//lang_blocks/p_9" /><br/>
<br/><xsl:value-of select="//lang_blocks/p_10" /><br/>
<br/><xsl:value-of select="//lang_blocks/p_10a" /><br/>
<br/><br/>
<span class="question"><xsl:value-of select="//lang_blocks/p_11" /></span><br/>
<p><xsl:value-of select="//lang_blocks/p_12" /></p>
<br/>

<xsl:value-of select="//lang_blocks/p_13" /><br/><br/>
<xsl:value-of select="//lang_blocks/p_14" /><br/><br/>
<xsl:value-of select="//lang_blocks/p_15" /><br/><br/>


<p><xsl:value-of select="//lang_blocks/p_16" /></p>

<span class="question"><xsl:value-of select="//lang_blocks/p_17" /></span><br/><br/>

<xsl:value-of select="//lang_blocks/p_18" /><br/><br/>
<xsl:value-of select="//lang_blocks/p_19" /><br/><br/>
<xsl:value-of select="//lang_blocks/p_20" /><br/><br/>

<p><xsl:value-of select="//lang_blocks/p_21" /></p>
<p><xsl:value-of select="//lang_blocks/p_22" /></p>
<p><xsl:value-of select="//lang_blocks/p_23" /></p>
<p><xsl:value-of select="//lang_blocks/p_24" /></p>
<p><xsl:value-of select="//lang_blocks/p_24a" /></p>
<p><xsl:value-of select="//lang_blocks/p_25" /></p>
<p><xsl:value-of select="//lang_blocks/p_26" /></p>
</ul>










     
<!--<p><xsl:value-of select="//lang_blocks/p6" /></p>  
<p><xsl:value-of select="//lang_blocks/p7" />( <xsl:value-of select="//lang_blocks/p8" /> ).</p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><b><xsl:value-of select="//lang_blocks/p11" />: </b> <xsl:value-of select="//lang_blocks/p12" /><xsl:value-of select="//lang_blocks/p13" />
<xsl:value-of select="//lang_blocks/p14" /></p>

<ul><li><xsl:value-of select="//lang_blocks/p15" /></li></ul>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<ul><li><xsl:value-of select="//lang_blocks/p17" /></li></ul>

<ul><li><xsl:value-of select="//lang_blocks/p18" /></li></ul>

<sub><xsl:value-of select="//lang_blocks/p19" /></sub>
<p><xsl:value-of select="//lang_blocks/p20" /></p>

<table width="500" border="1" bordercolor="#3E6768" cellpadding="5" align="center">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
<tr><td><p><b><xsl:value-of select="//lang_blocks/p21" /></b></p>

<br/><xsl:value-of select="//lang_blocks/p22" />

<ol><li><xsl:value-of select="//lang_blocks/p23" /> <xsl:value-of select="//lang_blocks/p24" /></li>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/networkrpt.gif"/></p>
<li><xsl:value-of select="//lang_blocks/p25" />"<xsl:value-of select="//lang_blocks/p26" />". <xsl:value-of select="//lang_blocks/p27" /><xsl:value-of select="//lang_blocks/p28" /><xsl:value-of select="//lang_blocks/p29" /></li> 
<br/><br/><li><xsl:value-of select="//lang_blocks/p30" /></li>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/transfer_single.gif"/></p>

<li><xsl:value-of select="//lang_blocks/p31" /><xsl:value-of select="//lang_blocks/p32" /><xsl:value-of select="//lang_blocks/p33" /></li>

</ol>
<p><xsl:value-of select="//lang_blocks/p34" /></p>
<p><xsl:value-of select="//lang_blocks/p35" /> ( <xsl:value-of select="//lang_blocks/p36" />). <xsl:value-of select="//lang_blocks/p37" /></p>
</td></tr>

</table>
<br/>

<sub><xsl:value-of select="//lang_blocks/p38" /></sub>
<br/>
<table width="500" border="1" bordercolor="#3E6768" cellpadding="5" align="center">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
<tr><td class="a"><p><i><xsl:value-of select="//lang_blocks/p39" /></i></p>

<p><b><xsl:value-of select="//lang_blocks/p40" /></b></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p41" /></li>
<li><xsl:value-of select="//lang_blocks/p42" /></li>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/networkrpt.gif"/></p>
<li><xsl:value-of select="//lang_blocks/p43" /> <xsl:value-of select="//lang_blocks/p26" />  <xsl:value-of select="//lang_blocks/p44" /><xsl:value-of select="//lang_blocks/p45" /></li>
<li><xsl:value-of select="//lang_blocks/p46" /></li>
<p><img src="http://www.clubshop.com/vip/ebusiness/images/transfer_single.gif"/></p>
<li><xsl:value-of select="//lang_blocks/p47" /> <xsl:value-of select="//lang_blocks/p48" /> <xsl:value-of select="//lang_blocks/p49" /></li>


</ol>
<p><b><xsl:value-of select="//lang_blocks/p50" /></b></p>
<p><xsl:value-of select="//lang_blocks/p51" /><xsl:value-of select="//lang_blocks/p52" /><xsl:value-of select="//lang_blocks/p53" />"<xsl:value-of select="//lang_blocks/p54" />".<xsl:value-of select="//lang_blocks/p55" />!</p>
</td></tr>

</table>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p56" /></sub>

<p><xsl:value-of select="//lang_blocks/p57" /><br/><a href="http://www.clubshop.com/vip/ebusiness/course_4/sip.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p58" /></a></p>

<p><xsl:value-of select="//lang_blocks/p59" />" <xsl:value-of select="//lang_blocks/p60" />". <xsl:value-of select="//lang_blocks/p61" />( <xsl:value-of select="//lang_blocks/p62" />)
"<xsl:value-of select="//lang_blocks/p63" />" <xsl:value-of select="//lang_blocks/p64" /> ( <xsl:value-of select="//lang_blocks/p65" /> ) <xsl:value-of select="//lang_blocks/p66" /></p>


<p><xsl:value-of select="//lang_blocks/p67" /></p>
<sub><xsl:value-of select="//lang_blocks/p68" /></sub>

<p><xsl:value-of select="//lang_blocks/p70" /></p>

<p><xsl:value-of select="//lang_blocks/p71" /></p>-->




















</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_9.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>