<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="100%" margin="25px">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" width="705" height="110" alt="ebiz" /></td>
</tr>
</table>
<table width="100%"><tr><td>
<sub><xsl:value-of select="//lang_blocks/p3a"/><br/>
<img src="../images/greenline.gif"/></sub>
<h5><xsl:value-of select="//lang_blocks/p4" /></h5>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<h5><xsl:value-of select="//lang_blocks/p8" /></h5>



<table>
<tr>
<td><img src="../images/c4_l2_plan.gif" width="300" height="155" alt="vacation" /></td>
<td class="a"><xsl:value-of select="//lang_blocks/p9" /></td>
</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p10" />?</p>

<p><b><xsl:value-of select="//lang_blocks/p11" /></b>  <xsl:value-of select="//lang_blocks/p12" /></p>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p13" /></h5>
<table><tr>
<td><img src="../images/c4_l1b.gif" width="104" height="150" alt="target" /></td>
<td><p><xsl:value-of select="//lang_blocks/p14" /></p><p><xsl:value-of select="//lang_blocks/p15" /></p></td>
</tr></table>


<h5><xsl:value-of select="//lang_blocks/p16" /></h5>
<p><xsl:value-of select="//lang_blocks/p17" />
<b><xsl:value-of select="//lang_blocks/p18" /></b>
  <xsl:value-of select="//lang_blocks/p19" /></p>

<h5><xsl:value-of select="//lang_blocks/p20" /></h5>
  
<p><xsl:value-of select="//lang_blocks/p21" /></p> 
<table width="60%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr bgcolor="#FFFFFF" align="center"><td><b>AMOUNT</b></td><td><b>LIFESTYLE CHANGE</b></td></tr>
<tr bgcolor="#FFFFFF"><td>$100 (USD) per month?</td><td></td></tr>
<tr bgcolor="#F7F7F2"><td>$250 (USD) per month?</td><td></td></tr>
<tr bgcolor="#FFFFFF"><td>$500 (USD) per month?</td><td></td></tr> 
<tr bgcolor="#F7F7F2"><td>$750 (USD) per month?</td><td></td></tr>
<tr bgcolor="#FFFFFF"><td>$1,000 (USD) per month?</td><td></td></tr> 
<tr bgcolor="#F7F7F2"><td>$1,500 (USD) per month?</td><td></td></tr>
<tr bgcolor="#FFFFFF"><td>$2,000 (USD) per month?</td><td></td></tr>
<tr bgcolor="#F7F7F2"><td>$5,000 (USD) per month?</td><td></td></tr>
<tr bgcolor="#FFFFFF"><td>$10,000 (USD) per month?</td><td></td></tr>

  </table>
  
  <p><xsl:value-of select="//lang_blocks/p22" />
<h4><i><xsl:value-of select="//lang_blocks/p23" /></i></h4></p>

<hr/>
<table><tr>
<td><img src="../images/c4_l1c.gif" width="140" height="120" alt="road map" /></td>
<td><p><xsl:value-of select="//lang_blocks/p24" />40% to 50% <xsl:value-of select="//lang_blocks/p25" /></p></td>
</tr>
</table>
  
<table width="60%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
<tr bgcolor="#FFFFFF"><td colspan="4"><div align="center"><b>Amounts are in US dollars</b></div></td></tr>
<tr bgcolor="#F7F7F2" align="center"><td><b><xsl:value-of select="//lang_blocks/p26" /></b></td><td><b><xsl:value-of select="//lang_blocks/p27" /></b></td><td><b><xsl:value-of select="//lang_blocks/p28" /></b></td><td><b><xsl:value-of select="//lang_blocks/p29" /></b></td></tr>
<tr bgcolor="#FFFFFF"><td>$100</td><td>600</td><td><xsl:value-of select="//lang_blocks/p30" /></td><td><xsl:value-of select="//lang_blocks/p39" /></td></tr>
<tr bgcolor="#F7F7F2"><td>$250</td><td>1,000</td><td><xsl:value-of select="//lang_blocks/p31" /></td><td><xsl:value-of select="//lang_blocks/p40" /></td></tr>
<tr bgcolor="#FFFFFF"><td>$500</td><td>1,500</td><td><xsl:value-of select="//lang_blocks/p32" /></td><td><xsl:value-of select="//lang_blocks/p41" /></td></tr>
<tr bgcolor="#F7F7F2"><td>$750</td><td>2,500</td><td><xsl:value-of select="//lang_blocks/p33" /></td><td><xsl:value-of select="//lang_blocks/p42" /></td></tr>
<tr bgcolor="#FFFFFF"><td>$1,000</td><td>3,500</td><td><xsl:value-of select="//lang_blocks/p34" /></td><td><xsl:value-of select="//lang_blocks/p43" /></td></tr>
<tr bgcolor="#F7F7F2"><td>$1,500</td><td>4,500</td><td><xsl:value-of select="//lang_blocks/p35" /></td><td><xsl:value-of select="//lang_blocks/p44" /></td></tr>
<tr bgcolor="#FFFFFF"><td>$2,000</td><td>6,000</td><td><xsl:value-of select="//lang_blocks/p36" /></td><td><xsl:value-of select="//lang_blocks/p44" /></td></tr>
<tr bgcolor="#F7F7F2"><td>$5,000</td><td>10,000 + 3 Exec Groups</td><td><xsl:value-of select="//lang_blocks/p37" /></td><td><xsl:value-of select="//lang_blocks/p44" /></td></tr>
<tr bgcolor="#FFFFFF"><td>$10,000</td><td>6 Exec Groups</td><td><xsl:value-of select="//lang_blocks/p38" /></td><td><xsl:value-of select="//lang_blocks/p45" /></td></tr>
</table>
         <br/>
<hr/>

<div align="center"><img src="../images/timetable.gif" width="240" height="40" alt="time table" /></div>

<p><xsl:value-of select="//lang_blocks/p46" /></p>

<p><xsl:value-of select="//lang_blocks/p47" /></p>
<!--<p><xsl:value-of select="//lang_blocks/p48" /></p>

<table><tr><td>
<img src="http://www.clubshop.com/vip/ebusiness/images/person.gif" width="68"  height="199" /></td>

<td>
<table width="200" bgcolor="red"><tr><td class="x"><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l2_5.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p49" /></a></td></tr></table>
<table width="300" bgcolor="yellow"><tr><td class="x"><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l2_10.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p50" /></a></td></tr></table>
<table width="350" bgcolor="orange"><tr><td class="x"><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l2_15.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p51" /></a></td></tr></table>
<table width="400" bgcolor="green"><tr><td class="x"><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l2_20.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p52" /></a></td></tr></table>
<table width="486" bgcolor="#6699CC"><tr><td class="x"><a href="http://www.clubshop.com/vip/ebusiness/course_4/c4_l2_40.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p53" /></a></td></tr></table>
</td>

</tr>
</table>

<h5><xsl:value-of select="//lang_blocks/p54" /></h5>

<p><xsl:value-of select="//lang_blocks/p55" /></p>

<h5><xsl:value-of select="//lang_blocks/p56" /></h5>

<p><xsl:value-of select="//lang_blocks/p57" /></p>

<table cellpadding="2" border="1">
<tr class="a"><td>Date</td><td>Monthly Income</td><td>System Role</td><td>Pay Title</td><td>Lifestyle Change</td></tr>
<tr class="a"><td>________</td><td>$75</td><td>Builder Intern</td><td>Team Captain</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$100</td><td>Builder</td><td>Bronze Manager</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$250</td><td>Strategist</td><td>Silver Manager</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$500</td><td>Trainer</td><td>Gold Manager</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$750</td><td>Marketing Director</td><td>Ruby Director</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$1,000</td><td>Promotion</td><td>Emerald Director</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$1,500</td><td>Chief</td><td>Diamond Director</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$2,000</td><td>Chief</td><td>Executive Director</td><td>________</td></tr>
<tr class="a" ><td>________</td><td>$5,000</td><td>Chief</td><td>3 Star Executive Director</td><td>________</td></tr>
<tr class="a"><td>________</td><td>$10,000</td><td>none</td><td>6 Star Ambassadors Club Director</td><td>________</td></tr>
</table>
           
<br/>
-->
<!--<table border="dotted"  border-color="#3E6768">
<tr><td><h3><font color="#3E6768"><xsl:value-of select="//lang_blocks/assignment" /></font></h3>
<p><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
</td></tr> 
</table>-->




<!--<div align="center">
<form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
<textarea name="ppf" cols="50" rows="5">
</textarea><br/>

<input type="submit" name="Submit">
   <xsl:attribute name="value">
   <xsl:value-of select="//lang_blocks/submit_button"/>
   </xsl:attribute>
</input>

<input type="reset" name="Reset" value="Reset">
  <xsl:attribute name="value">
   <xsl:value-of select="//lang_blocks/reset_button"/>
   </xsl:attribute>
</input>

<input type="hidden" name="action" value="confirm"/>
<input type="hidden" name="type" value="6"/>
<input type="hidden" name="u_key" value="ibfpps"/>

</form>
</div>

        
<hr/>
<p><img src="../images/keystosucceed2.jpg" alt=""/></p>
<br/>
<p class="a"><b><xsl:value-of select="//lang_blocks/p60" /></b></p> -->

 <hr/> </td></tr></table>   
 

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_4/index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_3.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
      
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
