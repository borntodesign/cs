<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="../images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/head" /></sub>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>

<p><b><xsl:value-of select="//lang_blocks/p4" /></b></p>

<p><xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text> (<a href="http://www.clubshop.com/vip/ebusiness/course_3/l5_ex2.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p6" /></a>)</p>

<sub><xsl:value-of select="//lang_blocks/p7" /></sub>

<p><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text> (<a href="http://www.clubshop.com/vip/ebusiness/course_3/lesson_5.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p9" /></a>) </p>

<p><xsl:value-of select="//lang_blocks/p10" /></p>



<table border="1">
<tr><td>
<p><xsl:value-of select="//lang_blocks/p11" /><br/><xsl:value-of select="//lang_blocks/p12" />:</p>
<p align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/275pp.gif" alt=""/></p></td>

<td valign="top">
<p><xsl:value-of select="//lang_blocks/p13" /><br/><xsl:value-of select="//lang_blocks/p14" />:</p>
<p align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/300pp.gif" alt=""/></p></td></tr>

<tr><td class="a"><xsl:value-of select="//lang_blocks/p15" /><br/><b><xsl:value-of select="//lang_blocks/p16" /></b></td>
<td class="a"><xsl:value-of select="//lang_blocks/p17" /></td>

</tr></table>

<br/>
<sub><xsl:value-of select="//lang_blocks/p18" /></sub>
<p><xsl:value-of select="//lang_blocks/p19" /></p>
<p>(<a href="http://www.clubshop.com/vip/ebusiness/view_1.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p20" /></a>)</p>

<p><xsl:value-of select="//lang_blocks/p21" /></p>
<p>(<a href="http://www.clubshop.com/vip/ebusiness/view_2.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p22" /></a>)</p>

<p><b><xsl:value-of select="//lang_blocks/p23" /></b></p>


<p><xsl:value-of select="//lang_blocks/p24" /></p>

<p><xsl:value-of select="//lang_blocks/p25" /></p>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p26" /></sub>

<p><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" /></p>

<p><xsl:value-of select="//lang_blocks/p29" /><b><xsl:value-of select="//lang_blocks/p30" /></b></p>


<p><xsl:value-of select="//lang_blocks/p31" /></p>


<sub><xsl:value-of select="//lang_blocks/p32" /> </sub>
<p><xsl:value-of select="//lang_blocks/p33" /> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p34" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p35" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p36" /></b>.<xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p37" /></p>

<p><xsl:value-of select="//lang_blocks/p38" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p39" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p41" /></b></p>


<p><xsl:value-of select="//lang_blocks/p42" /></p>

<!--<p><xsl:value-of select="//lang_blocks/p43" /></p>-->


</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_6.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
