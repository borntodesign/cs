<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../import_master.xsl" ?>
<?xml-stylesheet type="text/xsl" href="BI.xsl" ?>






<lang_blocks>
  <daily1>Beantwoord al uw zakelijke email</daily1>
  <daily2>Schrijf nieuwe Leden in</daily2>
  <daily3>Controleren van mijn activity rapport en verzenden van de members welkom emails</daily3>
  <daily4>Controleren van mijn Netwerk rapport en member lijn rapporten en versturen van welkom emails naar members die verplaatst zijn naar mijn netwerk.</daily4>
  <daily5>Ten minste______ minuten spenderen aan het adverteren en prospecteren naar nieuwe members</daily5>
  <fri>Vrij -  De eerste opvolg brief versturen aan mijn lijst van persoonlijke contacten.</fri>
  <future1>Werk de netwerk Builder stappen en lessen af</future1>
  <goals1>Afwerken van de Builder Intern stappen en lessen</goals1>
  <goals2>Kwalificeer voor netwerk inkomen indien dit nog niet het geval is</goals2>
  <mon>Ma - </mon>
  <monthly1>5de: Via de post een opvolg brief sturen aan de lijst van persoonlijke contacten op mijn bureaublad</monthly1>
  <sat>Zat - </sat>
  <sun>Zon - </sun>
  <thurs>Do - De Interactieve training bijwonen OF naar de opname van een sessie luisteren. </thurs>
  <title>Builder Intern- Persoonlijke Planner</title>
  <todo1>Hebt u uw persoonlijke planner aangepast?</todo1>
  <todo10>Lees de member inschrijf policy ( member proseccing) les4</todo10>
  <todo11>Maak en copieër een originele en persoonlijke contact brief</todo11>
  <todo12>Verstuur uw persoonlijke contact brief</todo12>
  <todo13>Bel uw persoonlijke contacten op</todo13>
  <todo14>Verstuur een brief per post naar uw persoonlijke contacten</todo14>
  <todo15>Eerste follow up brief voor persoonlijke contacten</todo15>
  <todo16>Tweede follow up brief voor persoonlijke contacten</todo16>
  <todo17>Bekijk het auto email systeem</todo17>
  <todo2>Hebt u een lijst gemaakt van persoonlijke contacten?</todo2>
  <todo3>Hebt u een persoonlijke contacten brief en/of opvolg brieven verstuurd?</todo3>
  <todo4>Hebt u een tweede lijn gestart?</todo4>
  <todo5>Hebt u een autostacker aan staan?</todo5>
  <todo6>Hebt u een member transfer uitgevoerd binnen de laatste 30 dagen?</todo6>
  <todo7>Hebt u uw doelen gesteld en op papier geschreven?</todo7>
  <todo8>Maak een persoonlijk contacten lijst met email adressen.Les 3</todo8>
  <todo9>Maak een persoonlijk contacten lijst zonder email adressen.Les 3</todo9>
  <tues>Di -Mijn Doelstellingen, Projecten, Nog-te doen en toekomstige doelen in mijn Persoonlijke Planner aanpassen.  Verwijderen, wijzigen en zo nodig toe voegen.</tues>
  <wed>Woe - </wed>
</lang_blocks>





