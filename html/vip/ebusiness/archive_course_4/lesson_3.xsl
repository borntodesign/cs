<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/pd" /><br/>
<img src="../images/greenline.gif"/></sub> 
<h5><xsl:value-of select="//lang_blocks/pe" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>

<h5><xsl:value-of select="//lang_blocks/p3" /></h5>

<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><b>"<xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p5aa"/>" 
(<xsl:value-of select="//lang_blocks/p5a1" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p5b" />")</b></p>

<p><xsl:value-of select="//lang_blocks/p6" /><xsl:text> </xsl:text> <a href="help1_4.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p7" /></a>
<xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><a href="help2_4.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p9" /></a>
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10" /></p>
<p><b><xsl:value-of select="//lang_blocks/p11" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p12" /><xsl:text> </xsl:text>
"<xsl:value-of select="//lang_blocks/p12a" />",<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p12b" /><xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p12c" />"<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12d" /></p>

<table><tr>
<td><a href="help3_4.html" onclick="window.open(this.href); return false;"><img src="../images/raised_hand.gif" alt="click for help"/></a></td>
<td bgcolor="#DAE8E9"><xsl:value-of select="//lang_blocks/p13" /></td>
</tr>
</table>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p14" /></h5>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><b>John,Smith,123 Main St., Englewood, FL,34224, USA, 941-475-3435</b></p>
<p><xsl:value-of select="//lang_blocks/p17" /></p>
<p><b>John,Smith,,,,,,941-475-3435</b></p>
<p><b><xsl:value-of select="//lang_blocks/p19" /></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p19a" /></p>

<table><tr>
<td><a href="http://www.clubshop.com/vip/ebusiness/course_4/help4_4.html" onclick="window.open(this.href); return false;"><img src="../images/raised_hand.gif" alt=" click for help"/></a></td>
<td bgcolor="#DAE8E9"><xsl:value-of select="//lang_blocks/p20" /></td>
</tr>
</table>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p21" /><xsl:text> </xsl:text> (<xsl:value-of select="//lang_blocks/p21a" />)</h5>
<p><b><xsl:value-of select="//lang_blocks/p22" />:</b><xsl:text> </xsl:text>  <xsl:value-of select="//lang_blocks/p22a" />
<a onclick="" href="http://www.clubshop.com/mall/cata/tools.html"><xsl:value-of select="//lang_blocks/p23" /></a>

<xsl:value-of select="//lang_blocks/p24" /></p>

<table align="center"><tr class="a"><td bgcolor="#DAE8E9"><br/>
<a href="/groupmail_listsetup.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p25" /></a>
<br/><br/>
</td></tr>
</table>

<h5><xsl:value-of select="//lang_blocks/p26" /></h5>

<p><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" />:</p>

<ol>
<li><xsl:value-of select="//lang_blocks/p29" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p30" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p31" /></li><br/>
</ol>

<p><xsl:value-of select="//lang_blocks/p32" /></p>

<table border="1" align="center" bordercolor="#3E6768"><tr><td>
<h5><xsl:value-of select="//lang_blocks/assignment" /></h5></td>
<td class="a"><img src="../images/stopsign.gif" alt="stop sign"/>
<xsl:value-of select="//lang_blocks/p33" /></td>

</tr></table>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p34" /></h5>
<p><xsl:value-of select="//lang_blocks/p35" /></p>
<p><xsl:value-of select="//lang_blocks/p36" /></p>


<table border="1" align="center"><tr><td>
<p><b><xsl:value-of select="//lang_blocks/p37" /></b></p>
<ol>

<li><b><xsl:value-of select="//lang_blocks/p38" /></b></li><br/><br/>
<li><b><xsl:value-of select="//lang_blocks/p39" /></b></li><br/><br/>
<li><b><xsl:value-of select="//lang_blocks/p40" /></b></li><br/><br/>
<li><b><xsl:value-of select="//lang_blocks/p41" /></b></li><br/><br/>
<ul>
<b><xsl:value-of select="//lang_blocks/p42" /></b></ul><br/><br/>

<ul><b><xsl:value-of select="//lang_blocks/p43" /></b></ul><br/><br/>
<ul><b><xsl:value-of select="//lang_blocks/p44" /></b></ul>


</ol>

</td></tr></table>

<br/>
<h5><xsl:value-of select="//lang_blocks/p45" /></h5>
<p><xsl:value-of select="//lang_blocks/p46" /></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>



</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_4.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>






























        