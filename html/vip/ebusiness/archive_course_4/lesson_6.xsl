<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="../images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/head" /></sub>

<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/cs/rewardsdirectory" target="_blank"><xsl:value-of select="//lang_blocks/p6" /></a>.</p>


<p><xsl:value-of select="//lang_blocks/p_7" /><xsl:text> </xsl:text><a href="http://www.glocalincome.com/localmarketing/pages/text_presentation.html" target="_blank"><xsl:value-of select="//lang_blocks/p6" /></a>.</p>

<sub><xsl:value-of select="//lang_blocks/p_8" /></sub>
<p><xsl:value-of select="//lang_blocks/p_9" /></p>























<p><xsl:value-of select="//lang_blocks/p7" /></p>
<table border="1" bordercolor="#3E6768"><tr><td>

<p class="a" align="center"><a href="groupmail_emailsetup.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p8" /></a>
</p>

</td></tr></table>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<table bgcolor="#EAF2F2" cellpadding="5"><tr><td class="a" align="center">

<xsl:value-of select="//lang_blocks/p10" /><br/><xsl:value-of select="//lang_blocks/p11" /><hr/></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<p><xsl:value-of select="//lang_blocks/p17" /><br/> http://www.clubshop.com/cgi-bin/rd/4,,refid=YourIDNumber</p>





<p><xsl:value-of select="//lang_blocks/p19" /><br/>
<xsl:value-of select="//lang_blocks/p20" /><br/>
<xsl:value-of select="//lang_blocks/p21" /></p>


<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<br/>
</td></tr>
</table>



<sub><xsl:value-of select="//lang_blocks/p36" /></sub>
<p><xsl:value-of select="//lang_blocks/p37" /></p>

<p><xsl:value-of select="//lang_blocks/p38" /> <xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p39" />) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p41" />).<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p42" /></p>
<p><xsl:value-of select="//lang_blocks/p43" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p44" />) <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45" /><xsl:text> </xsl:text>( <a href="gm_initial.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p46" /></a>)</p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>

<p><xsl:value-of select="//lang_blocks/p48" /><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p49" /><xsl:text> </xsl:text> (<a href="gm_followup.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p50" /></a>)</p>

<p><xsl:value-of select="//lang_blocks/p51" /><xsl:text> </xsl:text>(<a href="gm_weekly.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p50" /></a>)</p>

<p><xsl:value-of select="//lang_blocks/p53" /></p>

<hr/>
<p><xsl:value-of select="//lang_blocks/p54" /></p>


<table border="1" bordercolor="#3E6768" cellpadding="5">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
<tr><td><p class="a"><xsl:value-of select="//lang_blocks/p55" /><xsl:text> </xsl:text>"<xsl:value-of select="//lang_blocks/p56" />"

<br/><b><xsl:value-of select="//lang_blocks/p58" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p59" />
</p>
</td></tr>

</table>
<hr/>




<sub><xsl:value-of select="//lang_blocks/p83" /></sub>
<p><xsl:value-of select="//lang_blocks/p84" /></p>
<!--<table border="1" bordercolor="#3E6768" cellpadding="5">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>

<tr><td><p><xsl:value-of select="//lang_blocks/p85" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p86" />):</p>
<p><b><xsl:value-of select="//lang_blocks/p87" /></b></p>
</td></tr>

</table>-->



<p><b><xsl:value-of select="//lang_blocks/p68" /></b> <xsl:value-of select="//lang_blocks/p88" /></p>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p89" /></sub>
<p><xsl:value-of select="//lang_blocks/p90" /><xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p91" />)<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p92" /></p>

<p><xsl:value-of select="//lang_blocks/p93" /></p>

<!--<table border="1" bordercolor="#3E6768" cellpadding="5">
<tr><td><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>

<tr><td><p><xsl:value-of select="//lang_blocks/p94" /></p>


</td>
</tr></table>-->

<sub><xsl:value-of select="//lang_blocks/p95" /></sub>


<p><xsl:value-of select="//lang_blocks/p96" /></p>
<p><xsl:value-of select="//lang_blocks/p97" /></p>

<table border="1" bgcolor="#EAF2F2" cellpadding="5">

<tr><td><sub><xsl:value-of select="//lang_blocks/p98" /></sub>
<p><xsl:value-of select="//lang_blocks/p99" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p100" /></p>
<p><xsl:value-of select="//lang_blocks/p101" /></p>
<p><xsl:value-of select="//lang_blocks/p102" /></p>
<p><xsl:value-of select="//lang_blocks/p103" /></p>
<p><xsl:value-of select="//lang_blocks/p104" /></p>
<p><xsl:value-of select="//lang_blocks/p105" /></p>
<p>
<xsl:value-of select="//lang_blocks/p20" /><br/>
<xsl:value-of select="//lang_blocks/p21" /></p>
<p><b><xsl:value-of select="//lang_blocks/p58" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p106" /></b></p>

</td>
</tr></table>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p107" /></sub>
<p><xsl:value-of select="//lang_blocks/p108" /></p>
<p><xsl:value-of select="//lang_blocks/p109" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_6.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p110" /></a><br/>
<a href="http://www.clubshop.com/vip/ebusiness/course_2/msgtag.html" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p111" /></a></p>

<p><xsl:value-of select="//lang_blocks/p112" /></p>
<table border="1" bgcolor="#EAF2F2" cellpadding="5">

<tr><td>
<p><xsl:value-of select="//lang_blocks/p113" /></p>
<p><xsl:value-of select="//lang_blocks/p101" /></p>

<p><xsl:value-of select="//lang_blocks/p114" /></p>
<p><xsl:value-of select="//lang_blocks/p115" /></p>


<p><xsl:value-of select="//lang_blocks/p19" /><br/>
<xsl:value-of select="//lang_blocks/p20" /><br/>
<xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p118" /></p>

<p><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p121" /></p>
</td></tr></table>



</td></tr></table>

<hr/>
<div align="center"><p><a href="index.xml"><xsl:value-of select="//lang_blocks/biindex" /></a>  |  <a href="lesson_7.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
