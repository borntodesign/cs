<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="900">
<tr>
<td class="top"><img src="../images/bi_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
<img src="../images/greenline.gif"/></sub> 
<br/>
<sub><xsl:value-of select="//lang_blocks/head" /></sub>

<table><tr><td class="quote">
<img src="../images/helenkeller.gif" alt="helen keller"/></td>
<td><p class="quote"><xsl:value-of select="//lang_blocks/quote11" /><b><xsl:value-of select="//lang_blocks/quote11a" /></b></p>
</td></tr></table>

<p><img src="../images/greenline.gif" alt=""/><br/>

<!--<p>
  <img src="../images/under_construction.png" height="117" width="100" alt="Under Construction" /> 
  <xsl:text /> 
  <xsl:value-of select="//lang_blocks/under_construction" /> 
  </p>
  <br /> 
  <br /> -->

<sub><xsl:value-of select="//lang_blocks/p4" /></sub></p>
<ul><li><a href="lesson_1.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p5" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_2.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p6" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_3.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p7" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_4.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p8" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_5.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p9" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_6.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p10" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_7.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p11" /></a></li></ul>

<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_9.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p13" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/1562" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p14" /></a></li></ul>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/greenline.gif" alt=""/><br/></p>

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/ "><xsl:value-of select="//lang_blocks/toc" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_4/lesson_1.xml"><xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>  

</td></tr></table>

         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>