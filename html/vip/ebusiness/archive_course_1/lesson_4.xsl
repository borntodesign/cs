<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

            <link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />
         <style type="text/css">
         
table.notice{
	border: thin solid #FF9C00;
	width: 100%;
	text-align: left;
	background-color : #FFFFCE;
}

         
td.notice{font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-weight: bold;
color: Black;
text-align: left;
}
	</style>

         </head>

         <body>
            <table width="1000">
               <tr>
                  <td class="back">
                     <img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" />
                  </td>
               </tr>
            </table>

            <table width="1000">
               <tr>
                  <td>
                  <hr/>

                  <sub>
                     <xsl:value-of select="//lang_blocks/p0" />
                  </sub>

               
                   <hr/>

                 

                     <p><sub>
                        <xsl:value-of select="//lang_blocks/head" />
                     </sub>
                  </p>




<p><xsl:value-of select="//lang_blocks/p_1" /></p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p2" />
                  </p>

                  

                  <p>
                  <xsl:value-of select="//lang_blocks/p5" />

                  </p>

                  <p>
                     <b>
                        <xsl:value-of select="//lang_blocks/p6" />
                     </b><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p_7" />
                  </p>

                  <p>
                  <b>
                     <xsl:value-of select="//lang_blocks/p8" />
                  </b>

                  - 
                  <xsl:value-of select="//lang_blocks/p9" /><xsl:text> </xsl:text>

                  <xsl:value-of select="//lang_blocks/p10" /><xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p_11" /><xsl:text> </xsl:text>

                  <xsl:value-of select="//lang_blocks/p12" /> <xsl:text> </xsl:text>

                  <xsl:value-of select="//lang_blocks/p13" />
                  </p>

                  <p><b><xsl:value-of select="//lang_blocks/p14" /><xsl:text> </xsl:text></b>-  <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15" /></p>

                  <p><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:text> </xsl:text>-<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p18" /></b>

                  <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19" /><xsl:text> </xsl:text>
                  
<xsl:value-of select="//lang_blocks/p_20" /><xsl:text> </xsl:text>

                  <xsl:value-of select="//lang_blocks/p21" /><xsl:text> </xsl:text>
                  </p>

                  <hr />

                  <p><sub>
                     <b>
                        <xsl:value-of select="//lang_blocks/p22" />
                     </b>
                  </sub></p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p23" />
                  </p>

                  2 X 2 = 4 X 2 = 8 X 2 = 16 X 2 = 32 X 2 = 64 X 2= 128 X 2 = 256 X 2 = 512 X 2 = 1024 1024 X 2 = 2048 X 2 = 4096 
                  <p>
                     <xsl:value-of select="//lang_blocks/p24" /><xsl:text> </xsl:text>

                     <xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text>

                     <xsl:value-of select="//lang_blocks/p26" /><xsl:text> </xsl:text>

                     <xsl:value-of select="//lang_blocks/p27" /><xsl:text> </xsl:text>

                     <xsl:value-of select="//lang_blocks/p28" />
                  </p>

                  <p align="center">
                     <xsl:value-of select="//lang_blocks/p29" />
                  </p>

                  <hr />

                  <p><sub>
                     <xsl:value-of select="//lang_blocks/p30" />
                  </sub></p>

                  <p><sub>
                     <xsl:value-of select="//lang_blocks/p31" />
                  </sub></p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p32" />
                  </p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p33" />
                  </p>

                  <p>
                  <xsl:value-of select="//lang_blocks/p34" /><xsl:text> </xsl:text>

                  60% to 80% <xsl:text> </xsl:text>
                  <xsl:value-of select="//lang_blocks/p35" /><xsl:text> </xsl:text>

                

              
                  <xsl:value-of select="//lang_blocks/p37" />
                  </p>

                  <p>
                  <xsl:value-of select="//lang_blocks/p38" /><xsl:text> </xsl:text>

                  80%. <xsl:text> </xsl:text> 
                  <xsl:value-of select="//lang_blocks/p39" />
<xsl:text> </xsl:text>
                  <xsl:value-of select="//lang_blocks/p40" /><xsl:text> </xsl:text>

                  <xsl:value-of select="//lang_blocks/p41" /><xsl:text> </xsl:text>

                  50% <xsl:text> </xsl:text>
                  <xsl:value-of select="//lang_blocks/p42" />
                  </p>

                  <p>
                     <b>
                        <xsl:value-of select="//lang_blocks/p43" />
                     </b>
                  </p>

                  <ol>
                     <li>
                        <xsl:value-of select="//lang_blocks/p44" />
                     </li>

                     <br />

                     <li>
                        <xsl:value-of select="//lang_blocks/p45" />
                     </li>

                     <br />

                     <li>
                        <xsl:value-of select="//lang_blocks/p46" />
                     </li>

                     <br />

                     <li>
                        <xsl:value-of select="//lang_blocks/p47" />
                     </li>

                     <br />

                     <li>
                        <xsl:value-of select="//lang_blocks/p48" />
                     </li>

                     <br />
                  </ol>

                  <p>
                     <xsl:value-of select="//lang_blocks/p49" />
                  </p>

                  <hr />

                  <p><sub>
                     <xsl:value-of select="//lang_blocks/p50" />
                  </sub></p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p51" />
                  </p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p52" />
                  </p>


                  <p>
                     <xsl:value-of select="//lang_blocks/p_53" />
                  </p>

                  <ul>
                     <li>
                        <xsl:value-of select="//lang_blocks/p54" />
                     </li>
                  </ul>

                  <ul>
                     <li>

                        <xsl:value-of select="//lang_blocks/p_55" />
                     </li>
                  </ul>

                  <ul>
                     <li>
                        <xsl:value-of select="//lang_blocks/p56" />
                     </li>
                  </ul>

                  <ul>
                     <li>
<xsl:value-of select="//lang_blocks/p_57" />            
                        
                     </li>
                  </ul>

                  <ul>
                     <li>
                        <xsl:value-of select="//lang_blocks/p64" />
                     </li>
                  </ul>

                  <ul>
                     <li>
                 
                        <xsl:value-of select="//lang_blocks/p_65" />
                     </li>
                  </ul>

                  <ul>
                     <li>
<xsl:value-of select="//lang_blocks/p_66" />
                        
                     </li>
                  </ul>

                  <ul>
                     <li>
<xsl:value-of select="//lang_blocks/p_67" />
                     </li>
                  </ul>

                  <ul>
                     <li>
<xsl:value-of select="//lang_blocks/p_68" />
                     </li>
                  </ul>

                  <ul>
                     <li>
<xsl:value-of select="//lang_blocks/p_69" />
                     </li>
                  </ul>

                  <p><sub>
                     <xsl:value-of select="//lang_blocks/p70" />
                  </sub></p>
               

                  <table class="notice"><tr><td class="notice"><xsl:value-of select="//lang_blocks/p71" /> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p_71a" />
                  </td></tr></table>

                  <p>
                     <xsl:value-of select="//lang_blocks/p72" />
                  </p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p73" /><xsl:text> </xsl:text>
                     <a href="http://www.ftc.gov/">
                        <xsl:value-of select="//lang_blocks/p74" />
                     </a>
                  </p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p75" />
                  </p>

                  <p>
                     <xsl:value-of select="//lang_blocks/p76" />
                  </p>
                  </td>
               </tr>
            </table>

            <hr />

            <div align="center">
               <p>
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
                  <xsl:value-of select="//lang_blocks/tr1index" />
               </a>

               | 
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_5.xml">
                  <xsl:value-of select="//lang_blocks/lesson" />
               </a>
               </p>
            </div>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

