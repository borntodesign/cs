<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

            <link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />
         </head>

         <body>
            <table width="1000">
               <tr>
                  <td class="back">
                     <img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" />
                  </td>
               </tr>
            </table>

<table width="1000">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>

<hr/>



<table>
<tr>
<td>
<img src="http://www.clubshop.com/vip/ebusiness/images/kenblanchard.jpg" />
</td>

<td class="a">
<xsl:value-of select="//lang_blocks/quote" />

<b><xsl:value-of select="//lang_blocks/quote2" /></b>
</td>
</tr></table>

<hr/>

<p><sub> <xsl:value-of select="//lang_blocks/head" /></sub></p>

<p><b><xsl:value-of select="//lang_blocks/p1" /></b> - <xsl:value-of select="//lang_blocks/p2" /></p>

<p><xsl:value-of select="//lang_blocks/p3" /></p>
<hr/>
<table><tr>
<td><img src="http://www.clubshop.com/vip/ebusiness/images/rburke.gif"/></td>
<td class="a"><xsl:value-of select="//lang_blocks/quote3" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/quote4" /></b></td>
</tr></table>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p4" /></b> - <xsl:value-of select="//lang_blocks/p5" /> <xsl:text> </xsl:text>(<xsl:value-of select="//lang_blocks/p6" />) <xsl:value-of select="//lang_blocks/p7" /></p>
<p><b><xsl:value-of select="//lang_blocks/p8" /></b> - <xsl:value-of select="//lang_blocks/p9" /></p>


<p><b><xsl:value-of select="//lang_blocks/p10" /></b> - <xsl:value-of select="//lang_blocks/p11" /></p>
<p><b><xsl:value-of select="//lang_blocks/p12" /></b> - <xsl:value-of select="//lang_blocks/p13" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p14" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15" /></p>
<p><b><xsl:value-of select="//lang_blocks/p16" /></b> - <xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p18" />" <xsl:value-of select="//lang_blocks/p19" /></p>

<p><b><xsl:value-of select="//lang_blocks/p20" /></b> - <xsl:value-of select="//lang_blocks/p21" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p22" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23" /></p>
<p><b><xsl:value-of select="//lang_blocks/p24" /></b> - <xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p26" />". <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27" /></p>
<p><b><xsl:value-of select="//lang_blocks/p28" /></b> - <xsl:value-of select="//lang_blocks/p29" /></p>

<p><xsl:value-of select="//lang_blocks/p30" /></p>

<p><xsl:value-of select="//lang_blocks/p31" /></p>
<p><xsl:value-of select="//lang_blocks/p32" /></p>
<hr/>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/clock.jpg"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p33" /></b> - <xsl:value-of select="//lang_blocks/p34" /><xsl:text> </xsl:text> "<xsl:value-of select="//lang_blocks/p35" />" <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36" /></p>

<p><xsl:value-of select="//lang_blocks/p37" /></p>
<p><xsl:value-of select="//lang_blocks/p38" /></p>


<p><b><xsl:value-of select="//lang_blocks/p41" /></b><br/>
<a href="https://www.glocalincome.com/manual/index.xml" target="_blank">https://www.glocalincome.com/manual/index.xml</a></p>

<hr/>


<p><sub><xsl:value-of select="//lang_blocks/p46" /></sub></p>
<p><xsl:value-of select="//lang_blocks/p42" /></p>
<p><b><xsl:value-of select="//lang_blocks/p43" /></b><br/><xsl:value-of select="//lang_blocks/p44" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_1.xml" target="_blank">http://www.clubshop.com/vip/ebusiness/course_1/lesson_1.xml</a></p>

<p><xsl:value-of select="//lang_blocks/p45" /></p>








</td></tr></table>

<hr />

<div align="center">
<p><a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" />
</a>
 | 
<a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/42">
<xsl:value-of select="//lang_blocks/test" />
</a>
</p>
</div>
</body>
      </html>
   </xsl:template>
</xsl:stylesheet>

