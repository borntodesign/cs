<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="back"><img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<hr/>
<br/>


<table><tr><td class="quote">
<img src="http://www.clubshop.com/vip/ebusiness/images/rburke.gif" alt="Richard Burke"/></td>
<td><p class="quote">"<xsl:value-of select="//lang_blocks/quote" />"<xsl:value-of select="//lang_blocks/quote2" />"
<xsl:value-of select="//lang_blocks/quote3" /> <xsl:value-of select="//lang_blocks/quote4" /> "<xsl:value-of select="//lang_blocks/quote5" />
<xsl:value-of select="//lang_blocks/quote6" />"<br/>
<b><xsl:value-of select="//lang_blocks/quote7" /></b></p>
</td></tr></table>

<hr/>

<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>


<p><b><xsl:value-of select="//lang_blocks/p1" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p4" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5" /></p>


<p><b><xsl:value-of select="//lang_blocks/p_6" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7" /></p>

<p><b><xsl:value-of select="//lang_blocks/p_8" /></b></p>
<p><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10" /></p>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_4.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       </td></tr></table>
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>