<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="back"><img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<hr/>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub>
<hr/>

<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>
<p><xsl:value-of select="//lang_blocks/p1" /></p>

<p><b><xsl:value-of select="//lang_blocks/p2" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p3" /></p>

<p><b><xsl:value-of select="//lang_blocks/p4" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5" /></p>

<p><b><xsl:value-of select="//lang_blocks/p6" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7" /></p>
<p><b><xsl:value-of select="//lang_blocks/p50" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p51" /></p>
<p><b><xsl:value-of select="//lang_blocks/p8" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p9" /></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p10" /></b></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /> ( <xsl:value-of select="//lang_blocks/p13" /> )
<xsl:value-of select="//lang_blocks/p14" /><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/CompensationPlan.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p15" /></a></p>

<p><xsl:value-of select="//lang_blocks/p16" /> "<xsl:value-of select="//lang_blocks/p17" />" <xsl:value-of select="//lang_blocks/p18" /></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p19" /></b></p>
<p><xsl:value-of select="//lang_blocks/p20" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p21" /></li></ul>

<ul><li><xsl:value-of select="//lang_blocks/p22" /></li></ul>


<p><xsl:value-of select="//lang_blocks/p25" /> <xsl:text> </xsl:text><a href="http://www.glocalincome.com/CompensationPlan.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p28" /></a></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p52" /></b></p>
<p><xsl:value-of select="//lang_blocks/p53" /></p>
<p><xsl:value-of select="//lang_blocks/p54" /></p>
<p><xsl:value-of select="//lang_blocks/p55" /></p>

<div align="center">

<table class="calcs">
<tr class="calcs"><td><b><xsl:value-of select="//lang_blocks/p56" /></b></td><td><b><xsl:value-of select="//lang_blocks/p57" /></b></td></tr>                  
<tr class="calcs"><td>75</td><td>34%</td></tr>   
<tr class="calcs"><td>150</td><td>36%</td></tr> 
<tr class="calcs"><td>250</td><td>39%</td></tr> 
<tr class="calcs"><td>400</td><td>42%</td></tr> 
<tr class="calcs"><td>600</td><td>46%</td></tr> 
<tr class="calcs"><td>1,000</td><td>49%</td></tr> 
<tr class="calcs"><td>1,500</td><td>52%</td></tr> 
<tr class="calcs"><td>2,250</td><td>56%</td></tr> 
<tr class="calcs"><td>3,250</td><td>59%</td></tr> 
<tr class="calcs"><td>4,500</td><td>62%</td></tr>
<tr class="calcs"><td>6,000</td><td>65%</td></tr>                
</table>
</div>
<p><xsl:value-of select="//lang_blocks/p58" /></p>





<hr/>
<p><b><xsl:value-of select="//lang_blocks/p29" /></b></p>
<p><xsl:value-of select="//lang_blocks/p30" /><br/>
<a href="http://www.glocalincome.com/CompensationPlan.xml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/p31" /></a></p>

<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt="line"/><br/>


<p><b><xsl:value-of select="//lang_blocks/p32" /></b></p>
<p><xsl:value-of select="//lang_blocks/p33" /> "<xsl:value-of select="//lang_blocks/p34" />" <xsl:value-of select="//lang_blocks/p35" /> "<xsl:value-of select="//lang_blocks/p36" />"</p>
<p><xsl:value-of select="//lang_blocks/p38" /> "<xsl:value-of select="//lang_blocks/p34" />" <xsl:value-of select="//lang_blocks/p39" /> (<xsl:value-of select="//lang_blocks/p40" />)</p>

<p><xsl:value-of select="//lang_blocks/p41" /> "<xsl:value-of select="//lang_blocks/p36" />" <xsl:value-of select="//lang_blocks/p42" /></p>


<p><xsl:value-of select="//lang_blocks/p43" /></p> 
<p><xsl:value-of select="//lang_blocks/p48" /> <b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p49" /></b>
</p>



</td></tr></table>

<hr/>
<div align="center">

<a href="course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" /></a>  |  

<a href="lesson_6.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
 </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>