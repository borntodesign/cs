<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="back"><img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<hr/>
<br/>
<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>


<p><xsl:value-of select="//lang_blocks/p1" /></p>

<p><sub><xsl:value-of select="//lang_blocks/p2" /></sub></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p3" /></li><br/>

<ul>
<li><xsl:value-of select="//lang_blocks/p4" /></li><br/>
<li><xsl:copy-of select="//lang_blocks/p5/*|//lang_blocks/p5/text()" /></li><br/>
</ul>
<br/>

<li><xsl:value-of select="//lang_blocks/p6" /></li><br/>

<li><xsl:value-of select="//lang_blocks/p7" /></li><br/>



</ol>

<p><sub><xsl:value-of select="//lang_blocks/p8" /></sub></p>


<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>


<p><sub><xsl:value-of select="//lang_blocks/p12" /></sub></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>


<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>


<p><sub><xsl:value-of select="//lang_blocks/p16" /></sub></p>

<p><xsl:value-of select="//lang_blocks/p17" /></p>


<img src="../images/you_aps_to_pool.png" height="300" width="350" alt="AP's being transferred to the Pool"/>


<p><xsl:value-of select="//lang_blocks/p18" /></p>
<p><xsl:value-of select="//lang_blocks/p19" /></p>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       </td></tr></table>
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>