<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>Trainee I Bijkomende vragen</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body style="background-color:white; font-family:sans-serif; font-size: 90%">
<ul>
<li value="0">Hebt u de lijst van deelnemende ondernemers, de Handelaren Directory, in het Clubshop programma al ingekeken?</li>
<li value="0">Hebt u al business kaartjes of een order geplaatst bij VistaPrint?</li>
<li value="0">Hebt u een microfoon en luidsprekers of een PC headset?</li>
<li value="0">Hebt u al deelgenomen aan of geluisterd naar (vooraf opgenomen) een online interactieve trainingsessie?</li>
<li value="0">Hebt u al uw marketing website bezocht en de URL genoteerd?</li>
<li value="0">Hebt u al uw Organisatierapport ingekeken?</li>
<li value="0">Hebt u al uw Frontline rapport ingekeken?</li>
<li value="0">Hebt u al de gedragsregels gelezen in de Business Manual?</li>
</ul>
</body></html>