<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="back"><img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>

<hr/>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub>
<hr/>

		<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>
		<p><xsl:value-of select="//lang_blocks/p1" /> "<xsl:value-of select="//lang_blocks/p2" /> " <xsl:value-of select="//lang_blocks/p3" /></p>

		<ol>
		<li><xsl:value-of select="//lang_blocks/p4" /> ( <xsl:value-of select="//lang_blocks/p5" />) <xsl:value-of select="//lang_blocks/p6" /></li>
		<br/><br/>
		<li><xsl:value-of select="//lang_blocks/p7" /></li>
		</ol>

		<p><sub><xsl:value-of select="//lang_blocks/p8" /></sub></p>
		
		
<p><xsl:value-of select="//lang_blocks/p_9" /></p>


<p><sub><xsl:value-of select="//lang_blocks/p_10" /></sub></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p_10a" /></li>
<li><xsl:value-of select="//lang_blocks/p_10b" /></li>
<li><xsl:value-of select="//lang_blocks/p_10c" /></li>
</ul>
		
		


			<table>
			<tr >
				<td class="a"><xsl:value-of select="//lang_blocks/p11" /></td>
			</tr>
			</table>


			<p><b><xsl:value-of select="//lang_blocks/p11a" /></b></p>
			<table width="500" border="1" border-color="#3F4164" align="center" cellpadding="5">
			<tr>
			<td class="a"><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
			<tr><td>
			<ol>
			<li><xsl:value-of select="//lang_blocks/p12" /><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13" /></li><br/><br/>

			</ol>

			</td>
			</tr>
			</table>

			<p><b><xsl:value-of select="//lang_blocks/p15" /></b></p>
			<hr/>
			<p><xsl:value-of select="//lang_blocks/p16" /></p>
			<table>
			<tr>
			<td class="a"><a href="http://www.clubshop.com/mall/" target="_blank"><img src="http://www.clubshop.com/vip/ebusiness/images/new_mall_button.gif"/></a></td>
			<td class="a"><xsl:value-of select="//lang_blocks/p17" /></td>
			</tr></table>

			<table width="500" border="1" border-color="#3F4164" align="center" cellpadding="5">
			<tr>
				<td class="a"><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
			<tr><td class="a">
				<ol>
			<li><xsl:value-of select="//lang_blocks/p18" /></li><br/><br/>
			<li><xsl:value-of select="//lang_blocks/p19" /></li><br/><br/>
			<li><xsl:value-of select="//lang_blocks/p20" /></li>
			</ol>

			</td>
			</tr>
			</table>


			<p><b><xsl:value-of select="//lang_blocks/p22" /></b></p>

			<hr/>



			<table><tr>
			<td class="a"><a href="http://www.clubshop.com/outletsp/bcenter.html" target="_blank"><img src="https://www.clubshop.com/images/new_businesscenter_button.gif"/></a></td>
			<td class="a"><xsl:value-of select="//lang_blocks/p29" /> </td>
			</tr>
			</table>
			<p><b><xsl:value-of select="//lang_blocks/p29a" /></b></p>

			<table width="500" border="1" border-color="#3F4164" align="center" cellpadding="5">
			<tr>
			<td class="a"><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></td></tr>
			<tr><td class="a">



			<xsl:value-of select="//lang_blocks/p30" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/outletsp/bcenter.html" target="_blank"><xsl:value-of select="//lang_blocks/p31" /></a><xsl:text> </xsl:text>

			<xsl:value-of select="//lang_blocks/p32" />





			</td>
			</tr>
			</table>

			<hr/>






			</td></tr></table>


				<div align="center">
			<a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
			<xsl:value-of select="//lang_blocks/tr1index" /></a>  |  

			<a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_7.xml">
			<xsl:value-of select="//lang_blocks/lesson" /></a>
 			</div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>