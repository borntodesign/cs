<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

            <link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />
         </head>

         <body>
            <table width="1000">
               <tr>
                  <td class="back">
                     <img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" />
                  </td>
               </tr>
            </table>

            <table width="1000">
               <tr>
                  <td>
                     <hr/>

                     <sub>
                        <xsl:value-of select="//lang_blocks/p0" />
                     </sub>

                    
                        <hr/>
                       

                          <p><sub>
                           <xsl:value-of select="//lang_blocks/head" />
                        </sub>
                     </p>

                     

                     <p>
                        <xsl:value-of select="//lang_blocks/p3" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/training_schedule.html" target="_blank"><xsl:value-of select="//lang_blocks/p4a" /></a>.</p>
                  
                     <p><xsl:value-of select="//lang_blocks/p7" /></p>
   <p><xsl:value-of select="//lang_blocks/p4" /></p>
                    
                     <table width="500" border="1" border-color="#3F4164" align="center" cellpadding="5">
                        <tr>
                           <td>
                              <sub>
                                 <xsl:value-of select="//lang_blocks/assignment" />
                              </sub>
                           </td>
                        </tr>

                        <tr>
                           <td>
                              <ol>
                                 <li><xsl:value-of select="//lang_blocks/p5" /></li><br/><br/>

                                 <li><xsl:value-of select="//lang_blocks/p9" /><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p10" /></li>

                                 
                              </ol>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>

            <hr />

            <div align="center">
               <p>
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
                  <xsl:value-of select="//lang_blocks/tr1index" />
               </a>

               | 
               <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_8.xml">
                  <xsl:value-of select="//lang_blocks/lesson" />
               </a>
               </p>
            </div>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

