<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="index.xsl" ?>


<lang_blocks><head>Kazalo tečaja</head><quote>Večji del vsega zla na svetu nastane iz dejstva, da ljudje ne naredijo dovolj, da bi razumeli njihove lastne cilje. Lotevajo se zgraditi stolp ter porabijo veliko več truda za temelj, kot pa bi bilo potrebno, da zgradijo hišo.</quote><quote2>Johann Wolfang von Goethe</quote2><p1>Ta razvojna stopnja je temeljna in najbolj pomembna razvojna stopnja za razvoj vašega posla ter članske mreže. Če ta temelj ni močan, potem vaš posel ne bo močan in "grajen kot kopito". Zato bodite potrpežljivi, ko se učite spretnosti in uporabite vsa orodja in vaje, ki so potrebne, da zgradite močno in rastočo člansko mrežo. V mesecih, ki prihajajo boste spoznali kaj vam koristi in kaj pomeni močan temelj za vaš posel.</p1><p2>O vaši vlogi</p2><p3>Marketinški plan</p3><p4>Kako zaslužiti</p4><p5>Proizvodi in storitve</p5><p6>Spletne interaktivne vaje</p6><p7>Pričakovanja</p7><p8>Razvoj mreže</p8><p9>Poslovni menedžment</p9><p10>Zaključni tečajni test</p10><tr1index>Kazalo Pripravnik I</tr1index><lesson>Lekcija 1</lesson></lang_blocks>


