<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="1000">
<tr>
<td class="back"><img src="http://www.clubshop.com/vip/ebusiness/images/t1_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<hr/>
<br/>
<p><sub><xsl:value-of select="//lang_blocks/head" /></sub></p>


<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><sub><xsl:value-of select="//lang_blocks/p3" /></sub></p>
<p><b><xsl:value-of select="//lang_blocks/p4" /></b></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>

<img src="../images/no_monthly_fee.png" height="172" width="200" alt="Get 2 and your fees are through"/>
<br/>

<p><sub><xsl:value-of select="//lang_blocks/p6" /></sub></p>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p9" /></li>
<li><xsl:value-of select="//lang_blocks/p10" /></li>
<li><xsl:value-of select="//lang_blocks/p11" /></li>

</ul>


<p><sub><xsl:value-of select="//lang_blocks/p12" /></sub></p>

<p><xsl:copy-of select="//lang_blocks/p13/*|//lang_blocks/p13/text()" /></p>


<img src="../images/partner_pool.png" height="200" width="200" alt="Partner Pool Example"/>
<br/>


<p><xsl:value-of select="//lang_blocks/p14" /></p>
<img src="../images/vip_and_partner_pool.png" height="200" width="200" alt="VIP with a Partner Pool"/>
<br/>


<p><xsl:value-of select="//lang_blocks/p15" /></p>
<img src="../images/you_4vip_pool.png" height="300" width="200" alt="You with 4VIPs with a Partner Pool"/>
<br/>


<p><xsl:value-of select="//lang_blocks/p16" /></p>

<p><xsl:value-of select="//lang_blocks/p17" /></p>
<img src="../images/you_create_2nd_pool.png" height="200" width="300" alt="You Create a 2nd Pool"/>
<br/>


<p><xsl:value-of select="//lang_blocks/p18" /></p>

<img src="../images/you_2vips_2pools.png" height="300" width="350" alt="You have 2 VIPs and your fees are through"/>
<br/>



<p><xsl:value-of select="//lang_blocks/p19" /></p>








<p><sub><xsl:value-of select="//lang_blocks/p20" /></sub></p>

<p><xsl:value-of select="//lang_blocks/p21" /></p>


<img src="../images/you_create_2pools.png" height="300" width="350" alt="You Create 2 new pools"/>


<br/><br/>



<table border="1" border-color="#3F4164" align="center" cellpadding="5">
		<tr><td>
		
		<p><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></p>
		
		</td></tr>
		<tr><td>
		<p><b><xsl:value-of select="//lang_blocks/p23" /></b></p>
	
		<p><xsl:value-of select="//lang_blocks/p24" /><br/>
		<xsl:copy-of select="//lang_blocks/p25/*|//lang_blocks/p25/text()" />
		<br/><br/>
		<xsl:copy-of select="//lang_blocks/p26/*|//lang_blocks/p26/text()" />

		</p>

</td></tr>

<tr><td>
<p><sub><xsl:value-of select="//lang_blocks/assignment" /></sub></p>

</td></tr>
		<tr><td>
		<p><b><xsl:value-of select="//lang_blocks/p27" /></b></p>
		
		<p><xsl:value-of select="//lang_blocks/p28" /><br/>
		
			<xsl:copy-of select="//lang_blocks/p25/*|//lang_blocks/p25/text()" /></p>
		

</td></tr>

</table>



<p><xsl:value-of select="//lang_blocks/p29" /></p>


<hr/>



<p><xsl:value-of select="//lang_blocks/p30" /></p>








<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_1/index.xml">
<xsl:value-of select="//lang_blocks/tr1index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_1/lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       </td></tr></table>
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>