<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiz05-06.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="images/prwmkt.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /><br/>
</sub> 
<br/>



<sub><xsl:value-of select="//lang_blocks/p4" /></sub>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>

<sub><xsl:value-of select="//lang_blocks/p7" /></sub>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>

<sub><xsl:value-of select="//lang_blocks/p10" /></sub>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p12a" /></p>
<p><a href="http://freekeywords.wordtracker.com" target="_blank"><xsl:value-of select="//lang_blocks/p13"/></a></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>

<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>

<p><xsl:value-of select="//lang_blocks/p17" /></p>

<p><xsl:value-of select="//lang_blocks/p18" /><b><xsl:value-of select="//lang_blocks/p19" /></b><xsl:value-of select="//lang_blocks/p20" /></p>

<sub><a name="url"><xsl:value-of select="//lang_blocks/p21" /></a></sub>

<p><xsl:value-of select="//lang_blocks/p22" /></p>

<p style="color: #FF0000"><xsl:value-of select="//lang_blocks/p23" /><xsl:value-of select="//lang_blocks/p24" /></p>
<p style="color: #009900"><xsl:value-of select="//lang_blocks/p25" /><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p26a" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>




<sub><a name="meta"><xsl:value-of select="//lang_blocks/p32" /></a></sub>
<p><xsl:value-of select="//lang_blocks/p33" /></p>
<p><xsl:value-of select="//lang_blocks/p34" /></p>
<div align="center"><img src="images/title_meta_tag.gif" height="200" width="407" alt="Title Meta Tag"/></div>




<sub><a name="about"><xsl:value-of select="//lang_blocks/p38" /></a></sub>

<p><xsl:value-of select="//lang_blocks/p39" /></p>

<p style="color: #FF0000"><xsl:value-of select="//lang_blocks/p40" /></p>
<p style="color: #009900"><xsl:value-of select="//lang_blocks/p41" /></p>

<p><xsl:value-of select="//lang_blocks/p42" /></p>
<p><xsl:value-of select="//lang_blocks/p43" /></p>

<div align="center"><img src="images/miami_search.gif" height="98" width="301" alt="Miami Search" /></div>
<p><xsl:value-of select="//lang_blocks/p44" /></p>

<br/><br/>
<sub><xsl:value-of select="//lang_blocks/p50" /></sub>

<p><xsl:value-of select="//lang_blocks/p51" /></p>

<!--<sub><xsl:value-of select="//lang_blocks/p45" /></sub>
<p><xsl:value-of select="//lang_blocks/p46" /></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>

<p><xsl:value-of select="//lang_blocks/p52" /></p>
<p><xsl:value-of select="//lang_blocks/p53" /></p>
<p><xsl:value-of select="//lang_blocks/p54" /></p>-->

<!--<sub><xsl:value-of select="//lang_blocks/p56" /></sub>
<p><xsl:value-of select="//lang_blocks/p57" /></p>
<p><xsl:value-of select="//lang_blocks/p58" /></p>
<p><xsl:value-of select="//lang_blocks/p59" /></p>
-->
<br/><br/>

<div align="center"><a href="http://www.clubshop.com/cgi/pwp-edit.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p49" /></a></div>
<!-- | 
<a href="https://www.clubshop.com/cgi/pwp-edit.cgi/0/coach" target="_blank"><xsl:value-of select="//lang_blocks/p55" /></a></div>-->
<br/>
<hr/>

<!--<div align="center"><a href="http://www.clubshop.com/vip/ebusiness/ "><xsl:value-of select="//lang_blocks/toc" /></a>
 </div>  -->

</td></tr></table>

         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>