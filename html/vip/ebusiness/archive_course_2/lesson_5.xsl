<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}
li {padding-bottom: 7px:}
h5 {color: #683154;} 

</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<table><tr><td>
<p><xsl:value-of select="//lang_blocks/p1" /></p></td>



<!--td>
<<img src="http://www.clubshop.com/vip/ebusiness/images/c2_l5_bizreport.gif" width="247" height="200" alt="Business Reports"/></td>-->
</tr></table>
<br/><br/>
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p2" /></p>
</td>
</tr>
</table>

<ol>

<li><b><xsl:value-of select="//lang_blocks/p18" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/vip/tree.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p19" /></a></b>. <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20" /></li>


<li><b><xsl:value-of select="//lang_blocks/p3" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/activity_rpt" target="_blank"><xsl:value-of select="//lang_blocks/p4" /></a></b>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p5" /></li>

<li><b><xsl:value-of select="//lang_blocks/p6" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/genealogy.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p7" /></a></b>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8" /></li>

<li><b><xsl:value-of select="//lang_blocks/p22" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23" /></li>



<li><b><xsl:value-of select="//lang_blocks/p9" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/member_trans_rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p10" /></a></b>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11" /></li>

<li><b><xsl:value-of select="//lang_blocks/p12" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/member_ppp_rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p13" /></a></b>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14" /></li>

<li><b><xsl:value-of select="//lang_blocks/p15" /><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/pireport.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p16" /></a></b>.<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p21" /></p>


<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_6.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>