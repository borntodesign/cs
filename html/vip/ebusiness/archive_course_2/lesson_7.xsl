<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}
</style>
</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>


<table><tr>
<td><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/ex_save_personal.html" target="_blank"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" width="50" height="50" alt="Click Here for Help"/></a></td>
<td><p><xsl:value-of select="//lang_blocks/p3" /></p></td>
</tr></table>

<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" /></p>

<p><xsl:value-of select="//lang_blocks/p6" /></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p7" /></p>
<table><tr>
<td><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/ex_createfolder.html" target="_blank"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" width="50" height="50" alt="Click Here for Help"/></a></td>
<td><p><xsl:value-of select="//lang_blocks/p8" /></p></td>
</tr></table>




<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_8.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>