<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
	
}
hr{  color: #404269;}
</style>
</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="700">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /></p>

<!--<p><xsl:value-of select="//lang_blocks/p18" /></p>
<p><xsl:value-of select="//lang_blocks/p19" /></p>-->


<!--<p><b><xsl:value-of select="//lang_blocks/p2" /></b></p>-->


<!--<div align="center">
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p4" /><a onclick="" href="http://www.clubshop.com/mall/cata/tools.html" target="_blank"><xsl:value-of select="//lang_blocks/p5" /></a><br/><xsl:value-of select="//lang_blocks/p6" />
<br/><hr/>
<a href="http://www.clubshop.com/vip/ebusiness/course_2/msgtag.html"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" height="50" width="50" alt="Click for Help"/></a> <xsl:value-of select="//lang_blocks/p7" /></td>

</tr>
</table></div>-->
<br/><br/>
<hr/>
<p><b><xsl:value-of select="//lang_blocks/p8" /></b></p>

<table bgcolor="#99CCFF" >
<tr><td align="center"><a href="https://www.clubshop.com/cgi-bin/rd/14,1235,mall_id=1" target="_blank"><img src="http://www.clubshop.com/vip/ebusiness/images/infacta/infacta.gif" height="60" width="468" alt="Infacta Group Mail"/></a><br/>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
</td></tr>
</table>
<br/><br/>
<div align="center">
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><xsl:value-of select="//lang_blocks/p10" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/mall/cata/tools.html" target="_blank"><xsl:value-of select="//lang_blocks/p5" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p11" />
<br/><br/><hr/>
<a href="http://www.clubshop.com/vip/ebusiness/infacta/index.html"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" height="50" width="50" alt="Click for Help"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12" /></td>

</tr>
</table></div>
<br/><br/>
<table>
<tr>
<td><a href="https://www.clubshop.com/cgi-bin/rd/14,1235,mall_id=1" target="_blank"><img src="http://www.clubshop.com/vip/ebusiness/images/infacta/infacta.gif" height="60" width="468" alt="Infacta Group Mail"/></a></td>
</tr>
</table>
<p><b><xsl:value-of select="//lang_blocks/p13" /></b></p>

<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /><xsl:text> </xsl:text> (<a onclick="" href="http://www.clubshop.com/vip/ebusiness/infacta/index.html" target="_blank"><xsl:value-of select="//lang_blocks/p16" /></a>)</p>

<p><xsl:value-of select="//lang_blocks/p17" /></p>




<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_7.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>