<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8"
	doctype-public="//W3C//DTD HTML 4.01 Transitional//EN"
	doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<xsl:template match="/">

<html>
<head>
<title><xsl:value-of disable-output-escaping="yes" select="//ebiz_title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/css/newresourcecenter.css" rel="stylesheet" type="text/css"/>

<style>
a {
	text-decoration:none
}

BODY, TD {
	font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	color: #000000;
}

A {
	text-decoration: none;
	color: #000099;
}

A:Hover {
	text-decoration: underline;
	color: green;
}

H1{font-family : Verdana, Arial, sans-serif ;
	font-size : 12pt;
	font-weight : bold;
	color: #000000;
	}
	H2{font-family : Verdana, Arial, sans-serif ;
	font-size : 12pt;
	font-weight : bold;
	color: #000000;
	}
	H3{font-family : Verdana, Arial, sans-serif ;
	font-size : 14pt;
	font-weight : bold;
	color: #000000;
	}
	H4{font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	font-weight : bold;
	color: #003366;
	}
	H5{font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	font-weight : bold;
	color: #000000;
	}
	</style>
</head>

<body bgcolor="#FFFFFF" topmargin="0">
<div align="center">
<a name="top"></a>
<img src="../images/ebiz_header.gif" width="702" height="93"/> 
</div>

<table width="699" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#C6D3DE" bgcolor="#FFFFFF">
  <!--DWLayoutTable-->
  <tr> 
    <td width="7" height="" bgcolor="#C6D3DE"> </td>
    <td width="677" valign="top"><table width="663" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="661" height="" valign="top">
            <p align="center"><strong><img src="../images/c2_l2.gif" width="550" height="100"/></strong></p>
            <hr align="center" width="600" color="#C6D3DE"/> 
			<p align="left"><xsl:value-of disable-output-escaping="yes" select="//content1"/></p>

            <p align="center"><img src="../images/view_vertical.jpg" width="80" height="131"/></p>
            <p align="left"><xsl:value-of disable-output-escaping="yes" select="//content2"/></p>
            <p align="center"><img src="../images/view_no_vertical.jpg" width="131" height="80"/></p>
            <p>
              <xsl:value-of disable-output-escaping="yes" select="//content3"/></p>

            <p>
              <strong><xsl:value-of disable-output-escaping="yes" select="//sub_title1"/>
				</strong></p>
            <table width="600" border="1" align="center" cellpadding="0">
              <tr> 
                <td width="81" height="202"><img src="../images/taproot_view1.gif" width="75" height="198" border="1"/></td>
                <td width="507">
				<p>
				<xsl:value-of disable-output-escaping="yes" select="//content4"/></p>
				<p>
				<xsl:value-of disable-output-escaping="yes" select="//content5"/></p>
				</td>

              </tr>
            </table>
            <p><strong>
              <xsl:value-of disable-output-escaping="yes" select="//sub_title2"/>
			  </strong></p>
            <p>
              <xsl:value-of disable-output-escaping="yes" select="//content6"/></p>
			<p>
              <xsl:value-of disable-output-escaping="yes" select="//content7"/></p>
              <p align="center"><img src="../images/view_vertical_upgrade.jpg" width="144" height="144"/></p>
            <p align="left">
			<xsl:value-of disable-output-escaping="yes" select="//content8"/></p>
              <p><strong>
			  <xsl:value-of disable-output-escaping="yes" select="//content9a"/>
              </strong>
			  <xsl:value-of disable-output-escaping="yes" select="//content9b"/></p>
              <p>
			  <xsl:value-of disable-output-escaping="yes" select="//content10"/></p>
              <p> </p>

              <hr align="center" width="600" color="#C6D3DE"/>
              <p> </p>
            <table width="558" border="0" align="center" cellpadding="2">
              <tr> 
                <td width="269" height="20"> <div align="center"><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.html">Course 
                    2 Index</a></div></td>
                <td width="275"><div align="center"><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_3.html">Lesson 
                    3</a></div></td>
              </tr>

            </table></td>
        </tr>
      </table></td>
    <td width="8" bgcolor="#C6D3DE"> </td>
  </tr>
</table>
<table width="702" border="0" align="center" cellpadding="0" cellspacing="0" class="footer">
  <!--DWLayoutTable-->
  <tr> 
    <td width="681" height="4"></td>

    <td width="97"></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td height="15" valign="top"> <div align="center"><img src="/images/copyright_.gif" width="173" height="19"/></div></td>
    <td></td>
  </tr>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>