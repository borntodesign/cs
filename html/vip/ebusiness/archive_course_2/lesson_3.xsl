<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="xml" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" omit-xml-declaration="yes" media-type="text/html" />

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

            <link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  table.assign{
    border: thin solid #404269;
}

td.assign{
    font-family: Arial, Helvetica, sans-serif;
    font-size: medium;
    font-weight: bold;
    color: White;
    background-color: #404269;
    border-bottom : Gray;
}
hr{  color: #404269;}
tr.white{background-color: #FFFFFF;
text-align: center;
font-family: Arial, Helvetica, sans-serif;
 font-size: smaller;
}
tr.green{background-color: #F7F7F2;
text-align: center;
font-family: Arial, Helvetica, sans-serif;
 font-size: smaller;
}
h5 {color: #683154;} 
</style>
         </head>

         <body>
            <table width="925">
               <tr>
                  <td class="top">
                     <img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" />
                  </td>
               </tr>
            </table>

            <table width="900">
               <tr>
                  <td>
                     <sub>
                        <xsl:value-of select="//lang_blocks/p0" />
                     </sub>

                     <br />

                     <img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt="line" />

                     <br />

                     
                        <h5>
                           <xsl:value-of select="//lang_blocks/head" />
                        </h5>
                     

                     <p>
                        <b>
                           <xsl:value-of select="//lang_blocks/p1" />
                        </b><xsl:text> </xsl:text>

                        <xsl:value-of select="//lang_blocks/p2" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p3" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p4" />
                     </p>

                     <hr />

                     <p>
                        <b>
                           <xsl:value-of select="//lang_blocks/p5" />
                        </b>

                        <xsl:value-of select="//lang_blocks/p6" />
                     </p>

                     <p>
                        <xsl:value-of select="//lang_blocks/p7" />

                        
                           <xsl:value-of select="//lang_blocks/p8" />
                        

                        <xsl:value-of select="//lang_blocks/p9" />
                     </p>

                     <hr />

                     <p>
                        <b>
                           <xsl:value-of select="//lang_blocks/p10" />
                        </b>

                        <xsl:value-of select="//lang_blocks/p11" />
                     </p>

                     <table class="assign" border="0" cellpadding="6" cellspacing="1" width="100%">
                        <tr class="white">
                           <td><b><xsl:value-of select="//lang_blocks/p23" /></b></td>
                           <td><b><xsl:value-of select="//lang_blocks/p24" /></b></td>
                           <td><b><xsl:value-of select="//lang_blocks/p25" /></b></td>
                           <td><b><xsl:value-of select="//lang_blocks/p26" /></b></td>
                           <td><b><xsl:value-of select="//lang_blocks/p27" /></b></td>
                           </tr>

                        <tr class="green">
                           <td>75</td>
                           <td>34%</td>
                           <td>$25 - $51</td>
                           <td>$25 - $35</td>
                           <td>Producer</td>
                        </tr>

                        <tr class="white">
                           <td>150</td>
                           <td>36%</td>
                           <td>$55 - $92</td>
                           <td>$30 - $45</td>
                           <td>Team Player</td>
                        </tr>

                        <tr class="green">
                           <td>250</td>
                           <td>39%</td>
                           <td>$100 - $160</td>
                           <td>$35 - $80</td>
                           <td>Team Leader</td>
                        </tr>

                        <tr class="white">
                           <td>400</td>
                           <td>42%</td>
                           <td>$172 - $258</td>
                           <td>$60 - $125</td>
                           <td>Team Captain</td>
                        </tr>

                        <tr class="green">
                           <td>600</td>
                           <td>46%</td>
                           <td>$264 - $460</td>
                           <td>$90 - $230</td>
                           <td>Bronze Manager</td>
                        </tr>

                        <tr class="white">
                           <td>1,000</td>
                           <td>49%</td>
                           <td>$490 - $735</td>
                           <td>$160 - $350</td>
                           <td>Silver Manager</td>
                        </tr>

                        <tr class="green">
                           <td>1,500</td>
                           <td>52%</td>
                           <td>$780 - $1,299</td>
                           <td>$260 - $600</td>
                           <td>Gold Manager</td>
                        </tr>

                        <tr class="white">
                           <td>2,250</td>
                           <td>56%</td>
                           <td>$1,375 - $1,924</td>
                           <td>$450 - $950</td>
                           <td>Ruby Director</td>
                        </tr>

                        <tr class="green">
                           <td>3,250</td>
                           <td>59%</td>
                           <td>$2,030 - $2,610</td>
                           <td>$600 - $1,300</td>
                           <td>Emerald Director</td>
                        </tr>

                        <tr class="white">
                           <td>4,500</td>
                           <td>62%</td>
                           <td>$2,700 - $3,600</td>
                           <td>$900 - $1,800</td>
                           <td>Diamond Director</td>
                        </tr>

                                              
                            <tr class="green">
                           <td>6,000</td>
                           <td>67%</td>
                           <td>$4,200+</td>
                           <td>$1,400 - $2,100</td>
                           <td>Executive Director</td>
                        </tr>
                     </table>

                     <p><b><xsl:value-of select="//lang_blocks/p12" /></b><xsl:value-of select="//lang_blocks/p13" /></p>

                     <!--<img src="http://www.clubshop.com/vip/ebusiness/images/netpay.gif" height="325" width="518" alt="calculating earnings" />

                     <p><xsl:value-of select="//lang_blocks/p14" /></p>

                     <hr />

                     <p>
                        <xsl:value-of select="//lang_blocks/p15" />
                     </p>-->

                     <table class="assign">
                        <tr>
                           <td class="assign">
                              <xsl:value-of select="//lang_blocks/p16" />
                           </td>
                        </tr>

                        <tr>
                           <td><p>
                              <xsl:value-of select="//lang_blocks/p17" /><xsl:text> </xsl:text>

                              <a onclick="" href="http://www.glocalincome.com/CompensationPlan.xml" target="_blank">
                                 <xsl:value-of select="//lang_blocks/p18" />
                              </a>

                              <xsl:value-of select="//lang_blocks/p19" /></p>

                              <p>
                                 <xsl:value-of select="//lang_blocks/p20" />
                              </p>
                          

                                 
                         </td></tr>
                     </table>
                     
                     
                     
                     <p><xsl:value-of select="//lang_blocks/p22" /></p>

                     <hr />

                     <div align="center">
                        <p>
                        <a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
                           <xsl:value-of select="//lang_blocks/tr2index" />
                        </a>

                        | 
                        <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_4.xml">
                           <xsl:value-of select="//lang_blocks/lesson" />
                        </a>
                        </p>
                     </div>
                  </td>
               </tr>
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>

