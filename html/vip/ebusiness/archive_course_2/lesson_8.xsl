<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>


  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}

p.indent{ margin: 2em;}
</style>
</head>

<body>

<table width="900" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><b><xsl:value-of select="//lang_blocks/p2" /></b></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>


<p class="indent"><xsl:value-of select="//lang_blocks/p5" /><xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com/manual/policies/name_usage.xml" target="_blank"> <xsl:value-of select="//lang_blocks/p6" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p7" /></p>

<hr/>

<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><b><xsl:value-of select="//lang_blocks/p9" /></b></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><b><xsl:value-of select="//lang_blocks/p11" /></b></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>


<p><xsl:value-of select="//lang_blocks/p13" /></p>

<table class="assign">
<tr>
<td><p><xsl:value-of select="//lang_blocks/p14" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/ex_bookmark.html" target="_blank"> <xsl:value-of select="//lang_blocks/p15" /></a></p><br/></td></tr>

<tr>
<td><p><xsl:value-of select="//lang_blocks/p16" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/ex_addtofavorites.html" target="_blank"> <xsl:value-of select="//lang_blocks/p15" /></a></p></td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p17" /></p>


<hr/>

<p><b><xsl:value-of select="//lang_blocks/p18" /></b></p>

<p><xsl:value-of select="//lang_blocks/p19" /></p>
<p><b><xsl:value-of select="//lang_blocks/p20" /></b></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p22" /></b></p>

<p><xsl:value-of select="//lang_blocks/p23" /><xsl:text> </xsl:text> <a href="http://www.glocalincome.com/manual/policies/spam.xml" target="_blank">http://www.glocalincome.com/manual/policies/spam.xml</a></p>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p><xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p26" /> </b></p>

<p><xsl:value-of select="//lang_blocks/p27" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p26" /> </b></p>

<hr/>
<p><b><xsl:value-of select="//lang_blocks/p28" /> </b></p>

<p><xsl:value-of select="//lang_blocks/p29" /></p>
<hr/>

<!--<p><b><xsl:value-of select="//lang_blocks/p30" /> </b></p>

<p>(<a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/traineeII.xml" target="_blank"><xsl:value-of select="//lang_blocks/p31" /></a>) <xsl:value-of select="//lang_blocks/p32" /></p>-->




<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/31">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
  
 
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>