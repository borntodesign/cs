<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}
</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="910">
<tr><td>
<sub><xsl:value-of select="//lang_blocks/p0" /></sub><br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p_1" /></p>
<div align="center"><img src="../images/trans_ap_to_pool.png" height="264" width="146" alt="Upline VIPs transferring AP's to your Partner Pool"/></div>


<p><xsl:value-of select="//lang_blocks/p_2" /></p>
<div align="center"><img src="../images/view_no_pool.png" width="135" height="100" alt="No Pool/No Incentive"/></div>

<p><xsl:value-of select="//lang_blocks/p_3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p4" /></b></p>


<table cellpadding="5"><tr>
<td><img src="../images/taproot_view1.gif" height="198" width="75" alt="taproot"/></td>
<td><p><xsl:value-of select="//lang_blocks/p5" /></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p></td>
</tr></table>

<p><b><xsl:value-of select="//lang_blocks/p7" /></b></p>

<p><xsl:value-of select="//lang_blocks/p8" /></p>

<p><xsl:value-of select="//lang_blocks/p9" /></p>

<div align="center"><img src="../images/duplication_.png" height="300" width="900" alt="VIP Upgrade from Pool to VIP Line"/></div>
<p><xsl:value-of select="//lang_blocks/p10" /></p>

<p><b><xsl:value-of select="//lang_blocks/p13" /></b><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>






<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>