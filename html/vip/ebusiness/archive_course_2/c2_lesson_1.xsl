<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8"
	doctype-public="//W3C//DTD HTML 4.01 Transitional//EN"
	doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<xsl:template match="/">

<html>
<head>
<title><xsl:value-of disable-output-escaping="yes" select="//ebiz_title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/css/newresourcecenter.css" rel="stylesheet" type="text/css"/>

<style>
a {
	text-decoration:none
}

	BODY, TD {
	font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	color: #000000;
}

A {
	text-decoration: none;
	color: #000099;
}

A:Hover {
	text-decoration: underline;
	color: green;
}

H1{font-family : Verdana, Arial, sans-serif ;
	font-size : 12pt;
	font-weight : bold;
	color: #000000;
	}
	H2{font-family : Verdana, Arial, sans-serif ;
	font-size : 12pt;
	font-weight : bold;
	color: #000000;
	}
	H3{font-family : Verdana, Arial, sans-serif ;
	font-size : 14pt;
	font-weight : bold;
	color: #000000;
	}
	H4{font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	font-weight : bold;
	color: #003366;
	}
	H5{font-family : Verdana, Arial, sans-serif ;
	font-size : 10pt;
	font-weight : bold;
	color: #000000;
	}
	</style>
</head>

<body bgcolor="#FFFFFF" topmargin="0">
<div align="center"><img src="../images/ebiz_header.gif" width="702" height="93" /> 
</div>
<table width="699" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#C6D3DE" bgcolor="#FFFFFF">
  <!--DWLayoutTable-->
  <tr> 
    <td width="7" height="" bgcolor="#C6D3DE"> </td>
    <td width="677" valign="top">
	<table width="663" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="661" height="" valign="top"> 
              <p align="center"><img src="../images/c2_l1.gif" width="550" height="100" /></p>
              <h3><strong><xsl:value-of disable-output-escaping="yes" select="//course_title"/></strong> </h3>
      
                <hr align="center" width="600" color="#C6D3DE" />
 
            <p><xsl:value-of disable-output-escaping="yes" select="//content1"/></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content2"/></p>

              <table width="615" border="0" cellpadding="0" valign="top">
                <tr> 
                  <td width="238" height="236" valign="top"><div align="center"><img src="../images/3leggedstool.jpg" width="164" height="236" /></div></td>
                  <td width="371" valign="top">
	    <xsl:value-of disable-output-escaping="yes" select="//table_content1"/><br /> <br />
                  <xsl:value-of disable-output-escaping="yes" select="//table_content2"/><br /> <br />
                  <xsl:value-of disable-output-escaping="yes" select="//table_content3"/><br /> <br />
                  <xsl:value-of disable-output-escaping="yes" select="//table_content4"/><br /></td>
                </tr>
              </table>
            <p><xsl:value-of disable-output-escaping="yes" select="//content3"/></p>
			
            <hr align="center" width="600" color="#C6D3DE" />
			
            <br /> <strong><xsl:value-of disable-output-escaping="yes" select="//sub_title1"/></strong>
	<p><xsl:value-of disable-output-escaping="yes" select="//content4"/></p>
	<p><xsl:value-of disable-output-escaping="yes" select="//content5a"/>

	<a><xsl:attribute name="href">http://www.clubshop.com</xsl:attribute>
	<xsl:value-of disable-output-escaping="yes" select="//homelink"/>
	</a>

	<xsl:value-of disable-output-escaping="yes" select="//content5b"/></p>

            <br /> <strong><xsl:value-of disable-output-escaping="yes" select="//sub_title2"/></strong>
            <p><xsl:value-of disable-output-escaping="yes" select="//content6"/></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content7"/></p>
            <table width="526" border="1" align="center" cellpadding="0" bordercolor="#C6D3DE">
              <tr>
                <td height="18"><img src="../images/assignment.gif" width="238" height="28" /></td>
              </tr>
              <tr> 
                <td height="310">
			<p><xsl:value-of disable-output-escaping="yes" select="//content8a"/> 
	<strong><xsl:value-of disable-output-escaping="yes" select="//content8b"/></strong>
                <xsl:value-of disable-output-escaping="yes" select="//content9"/></p>
            <p><strong><xsl:value-of disable-output-escaping="yes" select="//content10"/></strong>
				<xsl:value-of disable-output-escaping="yes" select="//content11"/>
					<a href="http://www.clubshop.com/vip/ebusiness/help/help1b.html" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;">
					<xsl:value-of disable-output-escaping="yes" select="//content12"/></a></p>
            <p><strong><xsl:value-of disable-output-escaping="yes" select="//content13"/></strong>
				<xsl:value-of disable-output-escaping="yes" select="//content14"/>
					 <a href="http://www.clubshop.com/vip/ebusiness/help/help1a.html" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;"> 
                    <xsl:value-of disable-output-escaping="yes" select="//content15"/></a></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content16"/></p>
                  <p>&#160;</p></td>
              </tr>
            </table>

            <p><xsl:value-of disable-output-escaping="yes" select="//content17"/></p>
            <p></p>
            <hr align="center" width="600" color="#C6D3DE" />
            <p><strong><xsl:value-of disable-output-escaping="yes" select="//content18"/></strong> 
              <font face="Georgia, Times New Roman, Times, serif">
			  <xsl:value-of disable-output-escaping="yes" select="//content19"/></font></p>
            <table border="0" align="center" bgcolor="silver" width="80%">
			<tr><td><xsl:value-of disable-output-escaping="yes" select="//content19a"/></td></tr>
			</table>
            <p><xsl:value-of disable-output-escaping="yes" select="//content20"/></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content21"/></p>
            <p><strong><xsl:value-of disable-output-escaping="yes" select="//content22"/></strong>
              <xsl:value-of disable-output-escaping="yes" select="//content23"/></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content24"/></p>
            <table width="563" border="1" align="center" cellpadding="0" bordercolor="#C6D3DE">
              <tr> 
                <td height="35" colspan="2"><img src="../images/assignment.gif" width="238" height="28" /></td>
              </tr>
              <tr> 
                <td height="119" colspan="2">
			<p><a href="http://www.clubshop.com/vip/ebusiness/course_2/personal_planner.doc">
				<xsl:value-of disable-output-escaping="yes" select="//content25"/></a>
					<xsl:value-of disable-output-escaping="yes" select="//content26"/></p>
            <p><a href="http://www.clubshop.com/vip/ebusiness/course_2/personal_planner.txt">
				<xsl:value-of disable-output-escaping="yes" select="//content27"/></a>
					<xsl:value-of disable-output-escaping="yes" select="//content28"/></p>
            <p><xsl:value-of disable-output-escaping="yes" select="//content29"/></p>
                  </td>
              </tr>
              <tr> 
                <td width="62" height="35" align="center"><a href="http://www.clubshop.com/cgi/coursecheck.cgi?page=sqla&amp;C=1" onclick="window.open(this.href,'help', 'width=600,height=400,scrollbars'); return false;"><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" width="50" height="50" border="0"/></a></td>
                <td width="489"><font size="1"><xsl:value-of disable-output-escaping="yes" select="//content30"/> 
                  </font></td>
              </tr>
            </table>

            <hr align="center" width="600" color="#C6D3DE" />
            <p><strong><xsl:value-of disable-output-escaping="yes" select="//content31"/></strong></p>
            <p><br/>
              <strong><font color="#678AA7"><u><xsl:value-of disable-output-escaping="yes" select="//content32"/></u></font></strong></p>
            <p><br/>
              <strong><xsl:value-of disable-output-escaping="yes" select="//content33"/></strong>
			  <xsl:value-of disable-output-escaping="yes" select="//content34"/></p>
            <p> <br/><strong><xsl:value-of disable-output-escaping="yes" select="//content35"/></strong>
				<xsl:value-of disable-output-escaping="yes" select="//content36"/></p>
            <p> <strong><xsl:value-of disable-output-escaping="yes" select="//content37"/></strong>
			  <xsl:value-of disable-output-escaping="yes" select="//content38"/>
			  <font color="#0000FF"><xsl:value-of disable-output-escaping="yes" select="//content39"/></font>
			  <xsl:value-of disable-output-escaping="yes" select="//content40"/></p>
            <p> <strong><xsl:value-of disable-output-escaping="yes" select="//content41"/></strong>
				<xsl:value-of disable-output-escaping="yes" select="//content42"/></p>
            <p><br/>
              <strong><xsl:value-of disable-output-escaping="yes" select="//content43"/></strong><br/>
              <xsl:value-of disable-output-escaping="yes" select="//content44"/></p>
            <blockquote> 
              <p><br/>
                <img src="../images/specific.gif" width="82" height="30"/>
				<xsl:value-of disable-output-escaping="yes" select="//content45"/><br/>
                <br/>
                <img src="../images/measurable.gif"/>
				<xsl:value-of disable-output-escaping="yes" select="//content46"/><br/>
                <br/>
                <img src="../images/attainable.gif"/>
				<xsl:value-of disable-output-escaping="yes" select="//content47"/><br/>
                <br/>
                <img src="../images/realistic.gif" width="99" height="30"/>
				<xsl:value-of disable-output-escaping="yes" select="//content48"/><br/>
                <br/>
                <img src="../images/tangible.gif" width="99" height="30"/>
				<xsl:value-of disable-output-escaping="yes" select="//content49"/></p>
            </blockquote>
            <p><br/>
              <xsl:value-of disable-output-escaping="yes" select="//content50"/>
			  <strong><xsl:value-of disable-output-escaping="yes" select="//content51"/></strong></p>
            <p><br/>
              <xsl:value-of disable-output-escaping="yes" select="//content52"/></p>
            <p><br/>
            </p>
            <hr align="center" width="600" color="#C6D3DE"/> <p></p>
            <p>&#160;</p>
            <table width="558" border="0" align="center" cellpadding="2">
              <tr> 
                <td width="269" height="20">
				<div align="center">
				<a href="http://www.clubshop.com/vip/ebusiness/course_2/index.html">
					<xsl:value-of disable-output-escaping="yes" select="//content53"/>
				</a></div></td>
                <td width="275">
				<div align="center">
				<a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_2.html">
					<xsl:value-of disable-output-escaping="yes" select="//content54"/>
				</a></div></td>
              </tr>
            </table>
            <p>&#160;</p></td>
        </tr>
      </table></td>
    <td width="8" bgcolor="#C6D3DE">&#160;</td>
  </tr>
</table>
<table width="702" border="0" align="center" cellpadding="0" cellspacing="0" class="footer">
  <!--DWLayoutTable-->
  <tr> 
    <td width="681" height="4"></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td height="15" valign="top"> <div align="center"><img src="../images/copyright_.gif" width="173" height="19"/></div></td>
  </tr>
</table>

</body>
</html>

</xsl:template>
</xsl:stylesheet>