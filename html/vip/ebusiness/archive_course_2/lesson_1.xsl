<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}

h5 {color: #683154;}

</style>
</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt="" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<table><tr>
<td><img src="http://www.clubshop.com/vip/ebusiness/images/3leggedstool.jpg" width="164" height="236" alt="3 Legged Stool"/></td>
<td><p><xsl:value-of select="//lang_blocks/p3" /></p> 
<ol>
<li><xsl:value-of select="//lang_blocks/p4" /></li>
<li><xsl:value-of select="//lang_blocks/p5" /></li>
<li><xsl:value-of select="//lang_blocks/p6" /></li>
</ol>
</td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p7" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p8" /></h5>

<p><xsl:value-of select="//lang_blocks/social_network" /><br/>
<b><xsl:value-of select="//lang_blocks/facebook" /></b><xsl:text> </xsl:text> <a href="http://www.facebook.com/pages/Glocal-Income/74938236794" target="_blank"> http://www.facebook.com/pages/Glocal-Income/74938236794</a><br/>
<b><xsl:value-of select="//lang_blocks/twitter" /></b><xsl:text> </xsl:text> <a href="http://twitter.com/Glocal_Income" target="_blank">http://twitter.com/Glocal_Income</a><br/></p>

<!--<p><xsl:value-of select="//lang_blocks/p9" /></p>-->

<p><xsl:value-of select="//lang_blocks/p10" /><xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com" target="_blank"> http://www.glocalincome.com</a></p>

<!--<p><xsl:value-of select="//lang_blocks/p64" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/cgi/pwp-edit.cgi" target="_blank">http://www.clubshop.com/cgi/pwp-edit.cgi</a></p>-->


<h5><xsl:value-of select="//lang_blocks/p11" /></h5>

<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/p14" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p15" /> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:value-of select="//lang_blocks/p17" />  </p>
<p><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/vip/ebusiness/help/help1b.html" target="_blank"><xsl:value-of select="//lang_blocks/p20" /></a>  </p>

<p><b><xsl:value-of select="//lang_blocks/p21" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22" /><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/vip/ebusiness/help/help1a.html" target="_blank"><xsl:value-of select="//lang_blocks/p20" /></a> </p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>

</td></tr>
</table>
<p><xsl:value-of select="//lang_blocks/p24" /></p>

<hr/>
<p><b><xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26" /></p>

<table>
<tr>
<td><img src="http://www.clubshop.com/vip/ebusiness/images/waitley.gif" width="90" height="108" alt="denis waitley"/></td>
<td><p><xsl:value-of select="//lang_blocks/p27" /> -<xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p28" /></b></p></td>
</tr></table>
<p><xsl:value-of select="//lang_blocks/p29" /></p>

<p><xsl:value-of select="//lang_blocks/p30" /></p>


<!--<p><b><xsl:value-of select="//lang_blocks/p31" /></b></p>

<p><xsl:value-of select="//lang_blocks/p32" /></p>
<p><xsl:value-of select="//lang_blocks/p33" /></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/p14" /></td></tr>
<tr><td><p><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/personal_planner.doc" target="_blank"><xsl:value-of select="//lang_blocks/p34" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p35" /></p>

<p><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_2/personal_planner.txt" target="_blank"><xsl:value-of select="//lang_blocks/p34" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p36" /></p>


<p><xsl:value-of select="//lang_blocks/p37" /></p></td></tr>
<tr>
<td><hr/><a onclick="" href="http://www.clubshop.com/cgi/coursecheck.cgi?page=sqla&amp;C=1" target="_blank"><xsl:text> </xsl:text><img src="http://www.clubshop.com/vip/ebusiness/images/raised_hand.gif" width="50" height="50" alt="Click for Help"/></a> 
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38" /></td>
</tr>
</table>

<p><b><xsl:value-of select="//lang_blocks/p39" /></b></p>
<p><xsl:value-of select="//lang_blocks/p40" /></p>

<p><b><xsl:value-of select="//lang_blocks/p41" /></b><xsl:text> </xsl:text><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42" /></p>

<p><b><xsl:value-of select="//lang_blocks/p43" /></b><xsl:text> </xsl:text><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p44" /></p>
<p><b><xsl:value-of select="//lang_blocks/p45" /></b><xsl:text> </xsl:text><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p46" /></p>
<p><b><xsl:value-of select="//lang_blocks/p47" /></b><xsl:text> </xsl:text><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p48" /></p>
<hr/>
<p><b><xsl:value-of select="//lang_blocks/p49" /></b><br/>
<xsl:value-of select="//lang_blocks/p50" /></p>

<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p51" /></font></b> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p52" /></p>

<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p53" /></font></b> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p54" /></p>

<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p55" /></font></b> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p56" /></p>

<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p57" /></font></b> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p58" /></p>

<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p59" /></font></b> <xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/p60" /></p>


<p><xsl:value-of select="//lang_blocks/p61" /><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p62" /></b></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/p14" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p63" /></p>
</td>
</tr>
</table>-->





















<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>