<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>Trainee II Supplemental Questions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body style="background-color:white; font-family:sans-serif; font-size: 90%">
<ul>
<li value="0">Hebt u <a href="http://www.glocalincome.com/">www.glocalincome.com</a> al als uw home pagina ingesteld?</li>
<li value="0">Hebt u het compensatieplan al afgeprint en bestudeerd?</li>
<li value="0">Hebt u uw Activity rapport al bekeken?</li>
<li value="0">Bent u al op verkenning geweest in uw rapporten, nl. uw Organisatie, Partner Pool, persoonlijk, Transactie en Inkomen rapport?</li>
<li value="0">Hebt u al MSGTAG gedownload?</li>
<li value="0">Hebt u al Infacta Group Mail gedownload?</li>
<li value="0">Bent u op uw PC in staat een document en een folder te creëren?</li>
<li value="0">Kunt u opslaan in Favorieten?</li>
<li value="0">Hebt u de Spam Policy gelezen?</li>
</ul>
</body></html>