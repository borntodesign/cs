<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>

<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>


<table><tr><td class="quote">
<img src="http://www.clubshop.com/vip/ebusiness/images/peale.gif" width="84" height="114" alt="Norman Vincent Peale"/></td>
<td><p class="quote"><xsl:value-of select="//lang_blocks/quote" /><br/><b><xsl:value-of select="//lang_blocks/quote2" /></b></p>
</td></tr></table>

<p><img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" alt=""/><br/>


<!--<p>
  <img src="../images/under_construction.png" height="117" width="100" alt="Under Construction" /> 
  <xsl:text /> 
  <xsl:value-of select="//lang_blocks/under_construction" /> 
  </p>
  <br /> 
  <br /> -->



<sub><xsl:value-of select="//lang_blocks/head" /></sub></p>


<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_1.xml"><xsl:value-of select="//lang_blocks/p2" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_2.xml"><xsl:value-of select="//lang_blocks/p3" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_3.xml"><xsl:value-of select="//lang_blocks/p4" /></a></li></ul>

<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_4.xml"><xsl:value-of select="//lang_blocks/p5" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_5.xml"><xsl:value-of select="//lang_blocks/p6" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_6.xml"><xsl:value-of select="//lang_blocks/p7" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_7.xml"><xsl:value-of select="//lang_blocks/p8" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_8.xml"><xsl:value-of select="//lang_blocks/p9" /></a></li></ul>
<ul><li><a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/31"><xsl:value-of select="//lang_blocks/p10" /></a></li>
</ul>





</td></tr></table>

<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_1.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>