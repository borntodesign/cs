<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/ebiztrainee.css" rel="stylesheet" type="text/css"/>
<style>
  table.assign{
	border: thin solid #404269;
}

td.assign{
	font-family: Arial, Helvetica, sans-serif;
	font-size: medium;
	font-weight: bold;
	color: White;
	background-color: #404269;
	border-bottom : Gray;
}
hr{  color: #404269;}
tr.white{background-color: #FFFFFF;
text-align: center;
font-family: Arial, Helvetica, sans-serif;
	font-size: smaller;
}
tr.purple{background-color: #E0C2E0;
text-align: center;
font-family: Arial, Helvetica, sans-serif;
	font-size: smaller;
}
p.indent{ margin: 2em;}

h5 {color: #683154;} 

</style>
</head>

<body>

<table width="725" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t2_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table >
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p0" /></h5>
<br/>
<img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" />
<br/>

<h5><xsl:value-of select="//lang_blocks/head" /></h5>


<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<div align="center"><b><xsl:value-of select="//lang_blocks/p4" /></b></div>

<table class="assign" align="center">
<tr class="white"><td><xsl:value-of select="//lang_blocks/p5" /></td></tr>
<tr class="purple"><td><b><xsl:value-of select="//lang_blocks/p6" /></b></td></tr>

<tr class="white"><td><xsl:value-of select="//lang_blocks/p7" /></td></tr>
<tr class="purple"><td><b><xsl:value-of select="//lang_blocks/p6" /></b></td></tr>
<tr class="white"><td><xsl:value-of select="//lang_blocks/p8" /></td></tr>
</table>


<p><b><xsl:value-of select="//lang_blocks/p9" /></b></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><b><xsl:value-of select="//lang_blocks/p11" /></b></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>


<hr/>
<img src="http://www.clubshop.com/vip/ebusiness/images/c2_l4pay.gif" width="200" height="50" alt="pay options"/>

<p><b><xsl:value-of select="//lang_blocks/p13" /></b></p>
<p><b><xsl:value-of select="//lang_blocks/p14" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14a" />
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p41" /><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/business/clubaccount_help.xml" target="_blank"><xsl:value-of select="//lang_blocks/p27" /></a></p>


<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p15" /></font></b></p>
<p><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /></p>

<p><b><xsl:value-of select="//lang_blocks/p38" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p39" /></p>







<p><b><font color="#683154"><xsl:value-of select="//lang_blocks/p18" /></font></b></p>

<p><b><xsl:value-of select="//lang_blocks/p24" /></b><xsl:value-of select="//lang_blocks/p25" />
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26" /><xsl:text> </xsl:text><a onclick="" href="https://www.ecocard.com/Registration.aspx?Referee=DHS" target="_blank">
<xsl:value-of select="//lang_blocks/p27" /></a></p>

<p><b><xsl:value-of select="//lang_blocks/p19" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20" />
<br/>
<xsl:value-of select="//lang_blocks/p40" /><a href="http://www.alertpay.com/" target="_blank"><xsl:value-of select="//lang_blocks/p27" /></a>

</p>

<p><b><xsl:value-of select="//lang_blocks/p21" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22" />
<xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com/manual/business/fees.xml" target="_blank">
http://www.glocalincome.com/manual/business/fees.xml</a></p>




<p><xsl:value-of select="//lang_blocks/p28" /></p>

<p><xsl:value-of select="//lang_blocks/p29" /></p>


<p><b><xsl:value-of select="//lang_blocks/p30" /></b><xsl:text> </xsl:text><a onclick="" href="https://www.clubshop.com/cgi/comm_prefs.cgi" target="_blank">https://www.clubshop.com/cgi/comm_prefs.cgi</a></p>


<hr/>

<p><b><xsl:value-of select="//lang_blocks/p31" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p32" /></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p33" /></li><br/><br/>
<li><xsl:value-of select="//lang_blocks/p34" /></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p35" /></p>


<p><xsl:value-of select="//lang_blocks/p37" /></p>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_2/index.xml">
<xsl:value-of select="//lang_blocks/tr2index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_2/lesson_5.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
