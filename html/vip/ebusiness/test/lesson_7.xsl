<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>Partner Prospecting Course</title>


     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Header_Main {
	font-size: 14px;
	color: #0089B7;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.2em;
	font-family: verdana;
	text-transform: uppercase;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.text_normal{font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;} 

.Text_Content_heighlight {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #0089B7;
	text-transform: uppercase;
	font-weight:bold;
	border-bottom-style: solid;
	border-bottom-color: #0089b7;
	border-bottom-width: thin;
}

.ContactForm_TextField {
    
	color: #000066;
	font-family: verdana;
	font-size: 10pt;
	text-transform: uppercase;
	
}

.style1{font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
}


.style3 {
	font-size: 12px;
	font-family: verdana;
	color: #A91B07;
	font-weight:bold;
}

.style24 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-transform: uppercase;
	color: #2A5588;
}


.darkblue {color: #011A52;}

.medblue {color:#004A95;}

.bold{font-weight:bold;}

.smaller{font-size: 13px;}

.blue_bullet{list-style: url(http://www.clubshop.com/images/partner/bullets/bluedot.png);}

.half{list-style: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);}


.topTitle{text-align:center; color: #5080B0; }

.liteGrey{color:#b3b3b3;}

li.a {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(http://www.clubshop.com/images/minions/icon_blue_bulleted.png);
}	

	
li.b {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	list-style: url(http://www.clubshop.com/images/minions/icon_gold_bulleted.png);
}


li.c {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;

}




.img_heads {
	margin-bottom: -10px;
}
	
	
td.a {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	background-color: #FFFFFF;
}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

</style>



</head>

<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p1"/></h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">




<!--h5><xsl:value-of select="//lang_blocks/p0"/></h5-->

<!--h5><xsl:value-of select="//lang_blocks/p1"/></h5-->




<p><b><xsl:value-of select="//lang_blocks/p2"/></b></p>



<ol>
<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p3"/></b></span></li>
<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p4"/></b></span></li>
<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p5"/></b></span></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p6"/></p>

<ul>
		<li><span class="ContactForm_TextField"><xsl:value-of select="//lang_blocks/p7"/></span></li>
	
		<br/><span class="style24"><xsl:value-of select="//lang_blocks/p8"/></span><br/>

	<ol>
		<li class="d"><xsl:value-of select="//lang_blocks/p9"/></li><br/>
		<li class="d"><xsl:value-of select="//lang_blocks/p10"/></li>
	</ol>
</ul>


<h5><xsl:value-of select="//lang_blocks/p11"/></h5>

<ol>
<li><img src="../images/icon_email.png" height="48" width="54" alt="email_icon"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/p12"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p13"/></li>
<br/>
	<ul>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p14"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p15"/><br/><xsl:value-of select="//lang_blocks/p16"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p17"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p18"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p19"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p21"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/><br/><xsl:value-of select="//lang_blocks/p23"/></li><br/>
	</ul>
<br/>

<li><img src="../images/Skype.png" height="35" width="75" alt="skype_logo"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/p24"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p25"/></li>
	<br/><ul>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p26"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27"/><br/><b><xsl:value-of select="//lang_blocks/p27a"/></b></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p28"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p29"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p30"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p32"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p33"/></li><br/>
	<li><span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p34"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p35"/></li><br/>
	</ul>
<br/>
<li><img src="../images/icon_webpage.png" height="52" width="80" alt="wepage_icon"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/p36"/></span></li>

	<br/><ul>
	<li><xsl:value-of select="//lang_blocks/p37"/></li><br/>
	<li><xsl:value-of select="//lang_blocks/p38"/></li><br/>
	<li><xsl:value-of select="//lang_blocks/p39"/></li><br/>
<br/>
		<div class="colora">
		<i>
		<xsl:value-of select="//lang_blocks/p40"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p41"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p42"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p43"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p44"/><br/><br/>
		<xsl:value-of select="//lang_blocks/p45"/><br/>
		</i>
		</div>
	
	</ul>

<br/>
<li><img src="../images/icon_social.png" height="75" width="76" alt="social_media_icons"/><xsl:text> </xsl:text><span class="style24"><xsl:value-of select="//lang_blocks/p46"/></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47"/><xsl:text> </xsl:text><a href="http://www.facebook.com" target="_blank">http://www.facebook.com</a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p47a"/><br/><br/><xsl:value-of select="//lang_blocks/p48"/></li>

	
</ol>



<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>