<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 90%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 85%;
 font-weight: bold;
 color: #003399;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
hr{color:#0099CC;
}
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }
  p.b{ font-family: Verdana, Arial, sans-serif;
  }


  li { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #003399;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	

</style>
</head>

<body>

<div><img src="images/marketing1.gif" width="663" height="75" alt="" /></div>

<h5><xsl:value-of select="//lang_blocks/p1" /></h5>
<hr/>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<dl><dt><xsl:value-of select="//lang_blocks/p3" /></dt>
<dd><xsl:value-of select="//lang_blocks/p4" /></dd></dl>

<dl><dt><xsl:value-of select="//lang_blocks/p5" /></dt>
<dd><xsl:value-of select="//lang_blocks/p6" /></dd></dl>

<dl><dt><xsl:value-of select="//lang_blocks/p7" /></dt>
<dd><xsl:value-of select="//lang_blocks/p8" />


<br/><br/><xsl:value-of select="//lang_blocks/p9" />
<br/><br/><xsl:value-of select="//lang_blocks/p10" /></dd></dl>

<dl><dt><xsl:value-of select="//lang_blocks/p11" /></dt>
<dd><xsl:value-of select="//lang_blocks/p12" />

<br/><br/><xsl:value-of select="//lang_blocks/p13" />
<br/><br/><xsl:value-of select="//lang_blocks/p14" />
<br/><br/><xsl:value-of select="//lang_blocks/p15" />
<br/><br/>
<!--<xsl:value-of select="//lang_blocks/p16" />-->
</dd></dl>

<!--<table cellspacing="5" border="1">
<tr><td>

<p><b><xsl:value-of select="//lang_blocks/p17" />-</b>($50 USD Deposit, No Fee and $50 USD bonus click credit) - 
<xsl:value-of select="//lang_blocks/p18" /></p>
<p><b><xsl:value-of select="//lang_blocks/p19" />-</b>($50 USD Deposit, $199 USD Fee and $50 USD bonus click credit) - 
<xsl:value-of select="//lang_blocks/p20" /></p>
</td></tr>

</table>-->
<dl><dt><xsl:value-of select="//lang_blocks/p21" /></dt>
<dd><xsl:value-of select="//lang_blocks/p22" /> 
<a href="http://advertising.yahoo.com/adsolution#SponsoredSearch" target="_blank">
http://advertising.yahoo.com/adsolution#SponsoredSearch</a>

</dd></dl>

<dl><dt><xsl:value-of select="//lang_blocks/p25" /></dt>
<dd><xsl:value-of select="//lang_blocks/p26" /> 
<p class="b"><b><xsl:value-of select="//lang_blocks/p26a" /></b> 
<a href="http://www.clubshop.com/manual/policies/advertising.xml"><b><xsl:value-of select="//lang_blocks/p26b" /></b> </a>
<b><xsl:value-of select="//lang_blocks/p26c" /></b></p>

</dd></dl>
<dl><dt><xsl:value-of select="//lang_blocks/p27" /></dt>
<dd><xsl:value-of select="//lang_blocks/p28" />
$49.99, <xsl:value-of select="//lang_blocks/p28a" />
<xsl:value-of select="//lang_blocks/p28b" /><br/><br/>
<b><xsl:value-of select="//lang_blocks/p28c" /></b><xsl:value-of select="//lang_blocks/p28d" /><br/><br/>
<xsl:value-of select="//lang_blocks/p28e" /><br/><br/>
<xsl:value-of select="//lang_blocks/p28f" /><br/>
<xsl:value-of select="//lang_blocks/p28g" /><br/>
<xsl:value-of select="//lang_blocks/p28h" /><br/>
 <xsl:value-of select="//lang_blocks/p28i" /><br/>   <br/>                                       
                                            	
</dd></dl>

<p><xsl:value-of select="//lang_blocks/p29b" /> </p>
<p><xsl:value-of select="//lang_blocks/p31c" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p32" />: <xsl:value-of select="//lang_blocks/p33" /></b></p>

<hr/>

<sub><xsl:value-of select="//lang_blocks/p200" /></sub>


<p><xsl:value-of select="//lang_blocks/p204" />:</p>
<p><b>http://www.clubshop.com/cgi/appx/123456/af

</b></p>


<p><xsl:value-of select="//lang_blocks/p202" /></p>


<hr/>
<p><xsl:value-of select="//lang_blocks/p205" /></p>

<p><b>http://www.clubshop.com/cgi/appx/123456/af</b></p>








<hr/>
<sub><xsl:value-of select="//lang_blocks/p_34" /></sub>

<p><xsl:value-of select="//lang_blocks/p_34_1" /></p>
<p><b>http://www.clubshop.com/cgi-bin/rd/4,,refid=123456</b><br/><br/><xsl:value-of select="//lang_blocks/p75" /> </p>
<hr/>

<sub><xsl:value-of select="//lang_blocks/p203" /></sub>
<p><xsl:value-of select="//lang_blocks/p88" /></p>
<p><b>http://www.clubshop.com/cgi-bin/rd/8,<font color="red">7</font>,cid=123456</b></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/_marketing_search/vendorlist.html"><xsl:value-of select="//lang_blocks/p89" /></a><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p90" /></p>
<p><xsl:value-of select="//lang_blocks/p202" /></p>
<hr/>
<!--<sub><xsl:value-of select="//lang_blocks/p34" /></sub>
<p><xsl:value-of select="//lang_blocks/p35" /></p>
<p><b>http://www.clubshop.com/cgi-bin/rd/11,,refid=123456</b><br/><br/><xsl:value-of select="//lang_blocks/p75" /> </p>
<hr/>


<sub><xsl:value-of select="//lang_blocks/p83" /></sub>
<p><xsl:value-of select="//lang_blocks/p84" /></p>
<p>http://www.clubshop.com/cgi-bin/members/<b>WB123456</b> -<xsl:value-of select="//lang_blocks/p85" /> <xsl:value-of select="//lang_blocks/p86" /></p>
-->







<dl>
<dt><xsl:value-of select="//lang_blocks/p37" /></dt>
<dd><xsl:value-of select="//lang_blocks/p38" />$0.10 <xsl:value-of select="//lang_blocks/p39" />($0.10) <xsl:value-of select="//lang_blocks/p40" /></dd></dl>

<dl><dd><xsl:value-of select="//lang_blocks/p41" /> $50 initial deposit. Five-hundred $0.10 clicks equals $50. If you raise your bid amount to $0.25 per click, then two-hundred $0.25 clicks equals $50. 
<xsl:value-of select="//lang_blocks/p42" />
</dd></dl>

<dl>
<dt><xsl:value-of select="//lang_blocks/p43" /></dt>
<dd><xsl:value-of select="//lang_blocks/p44" /> If your balance reaches $0 <xsl:value-of select="//lang_blocks/p45" />$50 non-refundable deposit.

</dd>
</dl>

<dl><dt><xsl:value-of select="//lang_blocks/p46" /></dt>
<dd><xsl:value-of select="//lang_blocks/p47" /></dd></dl>
<dl><dd><xsl:value-of select="//lang_blocks/p48" /></dd></dl>

<dl><dd><a href="images/ex1.gif" onclick="window.open(this.href); return false;" >
[view example]</a></dd></dl>

<dl><dd><xsl:value-of select="//lang_blocks/p49" /></dd></dl>

<dl><dd><a href="images/ex2.gif" alt="marketing" onclick="window.open(this.href); return false;" >
[view example]</a></dd></dl>

<dl><dd><xsl:value-of select="//lang_blocks/p50" /></dd></dl>

<dl><dd><a href="images/ex3.gif" alt="marketing" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a></dd></dl>

<dl><dd><xsl:value-of select="//lang_blocks/p51" /> $3.05 <xsl:value-of select="//lang_blocks/p52" /> $3.05 <xsl:value-of select="//lang_blocks/p53" />
$1.26 <xsl:value-of select="//lang_blocks/p53a" />$0.45. <xsl:value-of select="//lang_blocks/p54" />
</dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p55" /> $0.01 <xsl:value-of select="//lang_blocks/p56" /> So if you have a $1.00 pay per click bid and the second position has a bid for $0.10 per click, then the $1.00 pay per click bid will only pay $0.11 per click. 
</dd></dl>
<dl><dd><a href="images/ex4.gif" alt="marketing" onclick="window.open(this.href); return false;" >
[view example]</a></dd></dl>


<dl><dd>
<xsl:value-of select="//lang_blocks/p57" /></dd></dl>
<dl><dd><a href="images/ex5.gif" alt="marketing" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p58" /></dd></dl>
<dl><dd><a href="images/ex6.gif" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p59" /> $0.01 <xsl:value-of select="//lang_blocks/p60" /> $0.11 <xsl:value-of select="//lang_blocks/p61" /> $0.10. <xsl:value-of select="//lang_blocks/p62" />
<br/><br/><a href="images/ex8.png" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a>
</dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p63" /></dd></dl>

<hr/>
<dl><dd>
<xsl:value-of select="//lang_blocks/p64" /></dd></dl>

<dl><dd><a href="images/ex9.gif" onclick="window.open(this.href); return false;" >
[view example]</a></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p65" /></dd></dl>

<dl><dd><a href="images/ex10.gif" onclick="window.open(this.href); return false;" >
[view example]</a></dd></dl>
<dl><dd>
<xsl:value-of select="//lang_blocks/p66" /></dd></dl>

<dl><dt><xsl:value-of select="//lang_blocks/p67" /></dt>
<dd><xsl:value-of select="//lang_blocks/p68" /></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p69" /></dd></dl>
<dl><dd><a href="images/ex11.gif" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p70" /></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p71" /></dd></dl>
<dl><dd><a href="images/ex13.gif" onclick="window.open(this.href); return false;" >
[view example (click image to enlarge)]</a></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p72" /></dd></dl>

<dl><dd>
<xsl:value-of select="//lang_blocks/p73" /></dd></dl>




          <p style="text-align: center">
                           <a href="http://www.clubshop.com/vip/ebusiness/">
                              <xsl:value-of select="//lang_blocks/p74" /></a> 
                           </p>

                           
                           <br/>
                           <p style="text-align: right"><img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/></p>
                        
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>
