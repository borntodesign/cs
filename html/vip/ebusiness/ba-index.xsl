<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<style type="text/css">
body {
	font-size: 100%;
	background-color: white;
	color: black;
}

h5 {
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 }

 sub{
	font-family: Verdana, Arial, sans-serif;
 font-size: 100%;
 font-weight: bold;
 color: #287B28;
  }
  p { font-family: Verdana, Arial, sans-serif;
  font-size:80%;}
  
   hr{color:#009966;  }
  
p.a{ font-family: Verdana, Arial, sans-serif;
  font-size:80%;
  color: #006600;
  
  }

a: link{text-decoration: underline;
    color: green;
}
a:hover {
    text-decoration: underline;
    color: green;
}

dt { 
	font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	color: #060;
	font-weight:bold;
}
dd{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;
	}
	
li{font-family: Verdana, Arial, sans-serif;
  	font-size:80%;}


</style>
</head>

<body>
<div><img src="/vip/ebusiness/images/ebiz_header_mkt.gif" width="702" height="93" alt="" /></div>

<h5><xsl:value-of select="//lang_blocks/p1" /></h5>
<img src="/banners/banner_468.gif" width="468" height="60" alt="" />
<p style="text-align: right"><i><xsl:value-of select="//lang_blocks/p2" /></i></p>

<hr />
<sub><xsl:value-of select="//lang_blocks/p3" /></sub>
<p><xsl:value-of select="//lang_blocks/p4" /></p>

<p><xsl:value-of select="//lang_blocks/p5" />:</p>
<ol><li><xsl:value-of select="//lang_blocks/p6" /></li>
<li><xsl:value-of select="//lang_blocks/p7" /></li>
<li><xsl:value-of select="//lang_blocks/p8" /></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<hr />

<sub><xsl:value-of select="//lang_blocks/p10" />:</sub>
<ol><li><xsl:value-of select="//lang_blocks/p11" /></li>
<li><xsl:value-of select="//lang_blocks/p12" /></li>
<li><xsl:value-of select="//lang_blocks/p13" /></li>
<li><xsl:value-of select="//lang_blocks/p14" /></li>
<li><xsl:value-of select="//lang_blocks/p15" /></li>
</ol>
<p><b><xsl:value-of select="//lang_blocks/p16" />: </b><xsl:value-of select="//lang_blocks/p17" /></p>
<hr />
<sub><xsl:value-of select="//lang_blocks/p18" />:</sub>
<ol><li><xsl:value-of select="//lang_blocks/p19" /></li>
<li><xsl:value-of select="//lang_blocks/p20" /></li>
<li><xsl:value-of select="//lang_blocks/p21" /></li>
</ol>





          <p style="text-align: center">
<a href="/vip/ebusiness/">
<xsl:value-of select="//lang_blocks/p22" /></a> |
                           

<a href="/vip/ebusiness/_marketing_ba/lesson_1.xml">
<xsl:value-of select="//lang_blocks/p23" /></a></p>
<br/>
<p style="text-align: right"><img src="/images/copyright_.gif" width="173" height="19" alt="copyright"/></p>
                        
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>