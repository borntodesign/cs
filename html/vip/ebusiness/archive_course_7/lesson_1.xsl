<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c7_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t7_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p2"/></li>
<li><xsl:value-of select="//lang_blocks/p3"/></li>
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li>
<li><xsl:value-of select="//lang_blocks/p7"/></li>
</ul>
<!--<p><xsl:value-of select="//lang_blocks/p54"/></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/recording_pg/trainer_overview.html" onclick="" target="_blank"><xsl:value-of select="//lang_blocks/p52"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p53"/></p>-->
<h5><xsl:value-of select="//lang_blocks/p60"/></h5>
<p><xsl:value-of select="//lang_blocks/p57"/></p>
<p><xsl:value-of select="//lang_blocks/p58"/></p>
<p><xsl:value-of select="//lang_blocks/p59"/><xsl:text> </xsl:text> <a href="https://www.clubshop.com/vip/ebusiness/prw.xml" target="_blank">https://www.clubshop.com/vip/ebusiness/prw.xml</a></p>
<p></p>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p8"/></h5>
<p><xsl:value-of select="//lang_blocks/p9"/></p>
<ul>
<!--<li><xsl:value-of select="//lang_blocks/p10"/></li>-->
<li><xsl:value-of select="//lang_blocks/p11"/></li>
<li><xsl:value-of select="//lang_blocks/p61"/></li>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p55"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>

<li><xsl:value-of select="//lang_blocks/p56"/><xsl:text> </xsl:text> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
</ul>

<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->
<hr/>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p18"/></p>
<p><xsl:value-of select="//lang_blocks/p19"/></p>

</td></tr>
</table>
<br/><br/>
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p20"/></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p21"/><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p23"/></li>
<ul>
<li><xsl:value-of select="//lang_blocks/p24"/> </li>
<li><xsl:value-of select="//lang_blocks/p25"/> </li>
<li><xsl:value-of select="//lang_blocks/p26"/> </li>
<li><xsl:value-of select="//lang_blocks/p27"/> </li>
</ul>
<div align="center"><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_7/example1.html" target="_blank"><xsl:value-of select="//lang_blocks/p28"/></a></div>
<li><xsl:value-of select="//lang_blocks/p29"/></li>
<p><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_7/letter1.xml" target="_blank"><xsl:value-of select="//lang_blocks/p30"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p31"/></p>
<p><xsl:value-of select="//lang_blocks/p32"/></p>





</ol>
</td></tr>
</table>

<hr/>
<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr><td>
<p><xsl:value-of select="//lang_blocks/p33"/></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p21"/> <xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p22"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p34"/></li>

<ul>
<li><xsl:value-of select="//lang_blocks/p35"/> </li>
<li><xsl:value-of select="//lang_blocks/p36"/> </li>
<li><xsl:value-of select="//lang_blocks/p37"/> </li>
<li><xsl:value-of select="//lang_blocks/p38"/> </li>
</ul>
<p><b><xsl:value-of select="//lang_blocks/p39"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40"/></p>
<div align="center"><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_7/example2.html" target="_blank"><xsl:value-of select="//lang_blocks/p28"/></a></div>
<li><xsl:value-of select="//lang_blocks/p41"/></li>
<p><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_7/letter2.xml" target="_blank"><xsl:value-of select="//lang_blocks/p30"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p42"/></p>
</ol>

</td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p43"/></p>
<hr/>
<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr><td>
<p><b><xsl:value-of select="//lang_blocks/p44"/></b><br/>
<xsl:value-of select="//lang_blocks/p45"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p46"/></li>
<li><xsl:value-of select="//lang_blocks/p47"/></li>
</ul>
</td>
</tr>
</table>-->
<p><b><xsl:value-of select="//lang_blocks/p48"/></b></p>
<hr/>

<p><xsl:value-of select="//lang_blocks/p49"/></p>

<h5><xsl:value-of select="//lang_blocks/p50"/></h5>
<p><xsl:value-of select="//lang_blocks/p51"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p51a"/></b></p>


<p><a href="http://www.clubshop.com/vip/ebusiness/recording_pg/trainer_tailwagdog.html" onclick="" target="_blank"><xsl:value-of select="//lang_blocks/p52"/></a> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p53"/></p>






<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_7/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_7/lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>