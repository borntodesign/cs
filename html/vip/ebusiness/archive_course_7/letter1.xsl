<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c7_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000" margin="25px">
<tr>
<td class="top"><img src="../images/t7_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />
<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><b><xsl:value-of select="//lang_blocks/p1" /></b></p>
<p><xsl:value-of select="//lang_blocks/p2" /><font color="#FF0000">(<xsl:value-of select="//lang_blocks/p3" />)</font>,</p>
<p><xsl:value-of select="//lang_blocks/p4" /><font color="#FF0000">_____________</font><xsl:value-of select="//lang_blocks/p5" /> </p>
<p><xsl:value-of select="//lang_blocks/p6" /><a href="http://www.clubshop.com/vip/ebusiness/"><xsl:value-of select="//lang_blocks/p7" /></a></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>

<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><font color="#FF0000"><xsl:value-of select="//lang_blocks/p11" /><br/>
<xsl:value-of select="//lang_blocks/p12" /></font></p>




<hr/>
<div align="center"><p class="close"><a href="javascript:self.close();">
<xsl:value-of select="//lang_blocks/close" /></a>  
</p> </div>                
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>