<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c7_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t7_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="h../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<ul>
<li><b><xsl:value-of select="//lang_blocks/p2"/></b></li>
<li><b><xsl:value-of select="//lang_blocks/p3"/></b></li>
<li><b><xsl:value-of select="//lang_blocks/p4"/></b></li>
</ul>

<hr/>
<table>
<tr>
<td><img src="../images/trainer_comm.gif" height="150" width="150" alt="Instant Messenger"/></td>
<td><p><xsl:value-of select="//lang_blocks/p5"/></p></td>
</tr>
</table>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p6"/></p></td>
</tr>
</table>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p7"/></h5>

<img src="../images/studying.jpg" height="110" width="83" alt="studying"/>

<h4><xsl:value-of select="//lang_blocks/p8"/></h4>

<p><xsl:value-of select="//lang_blocks/p9"/></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p10"/></li>
<li><xsl:value-of select="//lang_blocks/p11"/></li>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<li><xsl:value-of select="//lang_blocks/p14"/></li>
</ol>

<!--<p><xsl:value-of select="//lang_blocks/p15"/><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_5/lesson_4.xml" target="_blank"><xsl:value-of select="//lang_blocks/p16"/></a><xsl:value-of select="//lang_blocks/p17"/></p>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr>
<td>
<p><xsl:value-of select="//lang_blocks/p18"/></p>
<p><xsl:value-of select="//lang_blocks/p19"/></p>
<p><xsl:value-of select="//lang_blocks/p20"/></p>
<p><xsl:value-of select="//lang_blocks/p21"/></p></td>
</tr>
</table>-->

<table>
<tr>
<td><img src="../images/checkup.gif" height="111" width="152" alt="check up"/></td>
<td><p><xsl:value-of select="//lang_blocks/p22"/></p></td>
</tr>
</table>

<p><b><xsl:value-of select="//lang_blocks/p23"/></b> <xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24"/></p>

<hr/>
<h4><xsl:value-of select="//lang_blocks/p25"/></h4>
<table>
<tr>
<td><img src="../images/coursecomplete.gif" width="250" height="200" alt="scorecard"/></td>
<td><p><xsl:value-of select="//lang_blocks/p26"/></p>
<p><xsl:value-of select="//lang_blocks/p27"/></p>
<p><xsl:value-of select="//lang_blocks/p28"/></p>
<p><xsl:value-of select="//lang_blocks/p29"/></p>
</td>
</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p30"/></p>
<p><xsl:value-of select="//lang_blocks/p31"/></p>
<p><xsl:value-of select="//lang_blocks/p32"/></p>
<p><xsl:value-of select="//lang_blocks/p33"/></p>
<p><xsl:value-of select="//lang_blocks/p34"/></p>
<hr/>

<h4><xsl:value-of select="//lang_blocks/p35"/></h4>


<p><xsl:value-of select="//lang_blocks/p36"/></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p37"/></li>
<li><xsl:value-of select="//lang_blocks/p38"/></li>
<li><xsl:value-of select="//lang_blocks/p39"/></li>
</ol>

<table class="assign">
<tr>
<td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p40"/></p>
<p><xsl:value-of select="//lang_blocks/p41"/></p></td>
</tr>
</table>
<h4><xsl:value-of select="//lang_blocks/p42"/></h4>
<p><xsl:value-of select="//lang_blocks/p43"/></p>

<table class="xcolor">
<tr class="acolor"><td><a onclick="" href="newvip25.xml" target="_blank"><xsl:value-of select="//lang_blocks/p44"/></a></td><td></td><td><a onclick="" href="bi_fu.xml" target="_blank"><xsl:value-of select="//lang_blocks/p45"/></a></td></tr>
<tr class="bcolor"><td><a onclick="" href="newvipnopp.xml" target="_blank"><xsl:value-of select="//lang_blocks/p46"/></a></td><td></td><td><a onclick="" href="bi_complete.xml" target="_blank"><xsl:value-of select="//lang_blocks/p47"/></a></td></tr>
<tr class="acolor"><td><a onclick="" href="tr_invite.xml" target="_blank"><xsl:value-of select="//lang_blocks/p48"/></a></td><td></td><td><a onclick="" href="bu.xml" target="_blank"><xsl:value-of select="//lang_blocks/p49"/></a></td></tr>
<tr class="bcolor"><td><a onclick="" href="nv_tr_invite.xml" target="_blank"><xsl:value-of select="//lang_blocks/p50"/></a></td><td></td><td><a onclick="" href="bu_complete.xml" target="_blank"><xsl:value-of select="//lang_blocks/p51"/></a></td></tr>
<tr class="acolor"><td><a onclick="" href="t1.xml" target="_blank"><xsl:value-of select="//lang_blocks/p52"/></a></td><td></td><td><a onclick="" href="bu_downline.xml" target="_blank"><xsl:value-of select="//lang_blocks/p53"/></a></td></tr>
<tr class="bcolor"><td><a onclick="" href="t1_complete.xml" target="_blank"><xsl:value-of select="//lang_blocks/p54"/></a></td><td></td><td></td></tr>
<tr class="acolor"><td><a onclick="" href="t2.xml" target="_blank"><xsl:value-of select="//lang_blocks/p56"/></a></td><td></td><td><a onclick="" href="continue_ebiz.xml" target="_blank"><xsl:value-of select="//lang_blocks/p57"/></a></td></tr>
<tr class="bcolor"><td><a onclick="" href="t2_complete.xml" target="_blank"><xsl:value-of select="//lang_blocks/p58"/></a></td><td></td><td><a onclick="" href="tr_fu_inactive.xml" target="_blank"><xsl:value-of select="//lang_blocks/p59"/></a></td></tr>
<tr class="acolor"><td><a onclick="" href="t2c_t3start.xml" target="_blank"><xsl:value-of select="//lang_blocks/p60"/></a></td><td></td><td><a onclick="" href="tr_cancel.xml" target="_blank"><xsl:value-of select="//lang_blocks/p61"/></a></td></tr>
<tr class="bcolor"><td><a onclick="" href="t3_complete.xml" target="_blank"><xsl:value-of select="//lang_blocks/p62"/></a></td><td></td><td><a onclick="" href="tr_downgrade.xml" target="_blank"><xsl:value-of select="//lang_blocks/p63"/></a></td></tr>
<tr class="acolor"><td><a onclick="" href="bi.xml" target="_blank"><xsl:value-of select="//lang_blocks/p64"/></a></td><td></td><td><a onclick="" href="tr_nopay.xml" target="_blank"><xsl:value-of select="//lang_blocks/p65"/></a><br/><a onclick="" href="tr_fu_letter.xml" target="_blank"><xsl:value-of select="//lang_blocks/p66"/></a></td></tr>
</table>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_7/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_7/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>