<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c7_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t7_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p3"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/></p>




<h5><xsl:value-of select="//lang_blocks/p26a"/></h5>

<p><xsl:value-of select="//lang_blocks/p28a"/></p>
<p><xsl:value-of select="//lang_blocks/p29a"/></p>

<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment"/></td></tr>

<tr>
<td><p><xsl:value-of select="//lang_blocks/p57a" /></p>
<p><xsl:value-of select="//lang_blocks/p58a" /></p></td>
</tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p30a"/> <b><xsl:value-of select="//lang_blocks/p31a"/></b><xsl:value-of select="//lang_blocks/p32a"/></p>
<p><xsl:value-of select="//lang_blocks/p33a"/></p></td>
</tr>
</table>-->








<h5><xsl:value-of select="//lang_blocks/p54" /></h5>
<p><xsl:value-of select="//lang_blocks/p55"/></p>
<!--<xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p56"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p57"/></p>
<p><a href="http://www.clubshop.com/vip/ebusiness/recording_pg/trainer_inspect.html" onclick="" target="_blank"><xsl:value-of select="//lang_blocks/p58"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p59"/></p>-->

<hr/>

<h5><xsl:value-of select="//lang_blocks/p6"/></h5>
<p><xsl:value-of select="//lang_blocks/p7"/></p>

<p><xsl:value-of select="//lang_blocks/p8"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10"/></p>
<ul>
<li><b><xsl:value-of select="//lang_blocks/p11"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12"/></li>
<li><b><xsl:value-of select="//lang_blocks/p13"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14"/></li>
<li><b><xsl:value-of select="//lang_blocks/p15"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p16"/></li>
</ul>


<h5><xsl:value-of select="//lang_blocks/p17"/></h5>
<p><xsl:value-of select="//lang_blocks/p18"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p19"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p21"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22"/></p>

<p><xsl:value-of select="//lang_blocks/p23"/></p>

<p><xsl:value-of select="//lang_blocks/p24"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p25"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p26"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p27"/></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p28"/></p>

<table class="xcolor">
<tr>
<td><a onclick="" href="tr_traineeI.xml" target="_blank"><xsl:value-of select="//lang_blocks/p29"/></a></td>
<td><a onclick="" href="tr_traineeII.xml" target="_blank"><xsl:value-of select="//lang_blocks/p30"/></a></td>
<td><a onclick="" href="tr_traineeIII.xml" target="_blank"><xsl:value-of select="//lang_blocks/p31"/></a></td>
<td><a onclick="" href="tr_bi.xml" target="_blank"><xsl:value-of select="//lang_blocks/p32"/></a></td>
<td><a onclick="" href="tr_bu.xml" target="_blank"><xsl:value-of select="//lang_blocks/p33"/></a></td>
<td><a onclick="" href="tr_strategist.xml" target="_blank"><xsl:value-of select="//lang_blocks/p34"/></a></td>
</tr>
</table>

<h5><xsl:value-of select="//lang_blocks/p35"/></h5>

<p><xsl:value-of select="//lang_blocks/p36"/></p>
<p><b><xsl:value-of select="//lang_blocks/p37"/></b></p>
<p><xsl:value-of select="//lang_blocks/p38"/></p>

<h5><xsl:value-of select="//lang_blocks/p39"/></h5>
<p><xsl:value-of select="//lang_blocks/p40"/></p>

<p><img src="../images/cap.jpg" height="106" width="124" alt="continuing education"/><xsl:value-of select="//lang_blocks/p41"/></p>

<table>
<tr>
<td><img src="../images/mcginnis_book.jpg" height="159" width="95" alt="Mcginnis Book"/></td>
<td><p><xsl:value-of select="//lang_blocks/p42"/><xsl:text> </xsl:text><b><i><xsl:value-of select="//lang_blocks/p43"/></i></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p44"/><xsl:text> </xsl:text><a onclick="" href="http://www.clubshop.com/" target="_blank"><xsl:value-of select="//lang_blocks/p45"/></a></p></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p46"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p47"/></li>
<li><xsl:value-of select="//lang_blocks/p48"/></li>
</ul>
<p><xsl:value-of select="//lang_blocks/p49"/></p>

<h5><xsl:value-of select="//lang_blocks/p60"/></h5>

<p><xsl:value-of select="//lang_blocks/p61"/><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p62"/></b></p>

<!--<p><a href="http://www.clubshop.com/vip/ebusiness/recording_pg/trainer_delegate_stagnate.html" onclick="" target="_blank"><xsl:value-of select="//lang_blocks/p58"/></a><xsl:value-of select="//lang_blocks/p59"/></p>-->



<h5><xsl:value-of select="//lang_blocks/p50"/></h5>
<p><xsl:value-of select="//lang_blocks/p51"/></p>
<p><xsl:value-of select="//lang_blocks/p52"/></p>
<table class="xcolor">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p8r"/></h5>
<p><xsl:value-of select="//lang_blocks/p9r"/></p>
<ul>
<!--<li><xsl:value-of select="//lang_blocks/p10r"/></li>-->
<li><xsl:value-of select="//lang_blocks/p11r"/></li>
<li><xsl:value-of select="//lang_blocks/p18r"/></li>
<li><xsl:value-of select="//lang_blocks/p12r"/></li>
<li><xsl:value-of select="//lang_blocks/p13r"/></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<!--<li><xsl:value-of select="//lang_blocks/p14rb"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/vip/ebusiness/course_7/TR.xml" target="_blank">(<xsl:value-of select="//lang_blocks/p14rc"/></a>
<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14rd"/>)</li>-->
<li><xsl:value-of select="//lang_blocks/p15r"/></li>
<li><xsl:value-of select="//lang_blocks/p63"/></li>
<li><xsl:value-of select="//lang_blocks/p16r"/></li>

<li><xsl:value-of select="//lang_blocks/p64"/><xsl:text> </xsl:text> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->

</td></tr></table>
<hr/>

<p><xsl:value-of select="//lang_blocks/p53"/></p>












<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_7/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/4105">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>