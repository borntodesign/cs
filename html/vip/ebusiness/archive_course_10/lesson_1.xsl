<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table>
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table>
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>
<p><b><xsl:value-of select="//lang_blocks/p3"/></b></p>
<h5><xsl:value-of select="//lang_blocks/p4"/></h5>
<p><xsl:value-of select="//lang_blocks/p5"/></p>
<p><b><xsl:value-of select="//lang_blocks/p6"/></b></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>
<p><xsl:value-of select="//lang_blocks/p9"/></p>

<h5><xsl:value-of select="//lang_blocks/p10"/></h5>
<p><xsl:value-of select="//lang_blocks/p11"/></p>
<hr/>
<h5><img src="../images/role.jpg" height="88" width="116" alt="Your Role"/><xsl:value-of select="//lang_blocks/p12"/></h5>

<p><xsl:value-of select="//lang_blocks/p13"/></p>
<p><xsl:value-of select="//lang_blocks/p14"/></p>


<ul>
<li><xsl:value-of select="//lang_blocks/p15"/></li>
<li><xsl:value-of select="//lang_blocks/p16"/></li>
<li><xsl:value-of select="//lang_blocks/p17"/></li>
<li><xsl:value-of select="//lang_blocks/p18"/></li>
<li><xsl:value-of select="//lang_blocks/p19"/></li>
<li><xsl:value-of select="//lang_blocks/p20"/></li>
<li><xsl:value-of select="//lang_blocks/p21"/></li>
<li><xsl:value-of select="//lang_blocks/p22"/></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p115"/></p>

<ul>
<li><xsl:copy-of select="//lang_blocks/p116/*|//lang_blocks/p116/text()"/></li>
<li><xsl:copy-of select="//lang_blocks/p117/*|//lang_blocks/p117/text()"/></li>
<li><xsl:copy-of select="//lang_blocks/p118/*|//lang_blocks/p118/text()"/></li>
<li><xsl:copy-of select="//lang_blocks/p119/*|//lang_blocks/p119/text()"/></li>
</ul>


<hr/>


<h5><xsl:value-of select="//lang_blocks/p120"/></h5>
<p><xsl:value-of select="//lang_blocks/p121"/></p>
<p><xsl:value-of select="//lang_blocks/p122"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/manual/policies/communications.xml" target="_blank"><xsl:value-of select="//lang_blocks/p123"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p124"/></p>


<hr/>

<h5><xsl:value-of select="//lang_blocks/p23"/></h5>
<p><xsl:value-of select="//lang_blocks/p24"/></p>
<p><xsl:value-of select="//lang_blocks/p25"/></p>
<ul>
<li><xsl:value-of select="//lang_blocks/p26"/></li>
<li><xsl:value-of select="//lang_blocks/p27"/></li>
<li><xsl:value-of select="//lang_blocks/p28"/></li>
<li><xsl:value-of select="//lang_blocks/p29"/></li>
<li><xsl:value-of select="//lang_blocks/p30"/></li>
</ul>

<p><xsl:value-of select="//lang_blocks/p31"/></p>
<p><xsl:value-of select="//lang_blocks/p32"/></p>
<p><xsl:value-of select="//lang_blocks/p33"/></p>
<p><xsl:value-of select="//lang_blocks/p34"/></p>

<p><a onclick="" href="orgchart.xml" target="_blank"><xsl:value-of select="//lang_blocks/p35"/></a></p>



<!--<p><b><xsl:value-of select="//lang_blocks/p36"/></b><xsl:value-of select="//lang_blocks/p37"/></p>

<p><xsl:value-of select="//lang_blocks/p38"/><xsl:text> </xsl:text><a href="mailto:vipsales@dhs-club.com?Subject=Advisory Board">vipsales@dhs-club.com</a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p38b"/></p>-->


<hr/>

<h5><xsl:value-of select="//lang_blocks/p40"/></h5>
<img src="../images/crossover.gif" height="105" width="161" alt="crossline"/>
<p><xsl:value-of select="//lang_blocks/p41"/></p>

<p><xsl:value-of select="//lang_blocks/p42"/><b><xsl:value-of select="//lang_blocks/p43"/></b><xsl:value-of select="//lang_blocks/p44"/></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p45"/></h5>
<p><xsl:value-of select="//lang_blocks/p46"/></p>
<p><xsl:value-of select="//lang_blocks/p47"/></p>




<hr/>

<h5><xsl:value-of select="//lang_blocks/p98" /></h5>
<p><xsl:value-of select="//lang_blocks/p99" /></p>


<ul>
<li><xsl:value-of select="//lang_blocks/p100" /></li>
<li><xsl:value-of select="//lang_blocks/p101" /></li>
<li><xsl:value-of select="//lang_blocks/p103" />
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
( <a href="/vip/ebusiness/nforgsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p103a" /></a><xsl:value-of select="//lang_blocks/p103b" /><a href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p103c" /></a>

</li>
<li><xsl:value-of select="//lang_blocks/p104" /></li>
<li><xsl:value-of select="//lang_blocks/p105" /></li>
<li><xsl:value-of select="//lang_blocks/p108" /></li>
<li><xsl:value-of select="//lang_blocks/p109" /> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
<li><xsl:value-of select="//lang_blocks/p130" /></li>
<li><xsl:value-of select="//lang_blocks/p106" /></li>
<!--<li><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_10/CH.xml" target="_blank"><xsl:value-of select="//lang_blocks/p48" /></a><xsl:value-of select="//lang_blocks/p107" /></li>-->
<!--removed 6/1/10<li><xsl:value-of select="//lang_blocks/p110" /></li>-->

</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->



<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>