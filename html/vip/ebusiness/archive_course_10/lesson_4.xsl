<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<table>
<tr><td>
<img src="../images/trainer_man.jpg" height="109" width="79" alt="trainer"/>
</td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr></table>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><b><xsl:value-of select="//lang_blocks/p5" /></b></p>
<p><xsl:value-of select="//lang_blocks/p6" /><b><xsl:value-of select="//lang_blocks/p7" /></b></p>

<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /><b><xsl:value-of select="//lang_blocks/p15" /></b></p>
<div align="center"><img src="../images/stagnate_lg.jpg" height="170" width="256" alt="Stagnate"/></div>
<br/>
<br/>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p16" /></p></td>
</tr>
</table>

<br/>
<br/>
<table><tr><td><img src="http://www.clubshop.com/vip/ebusiness/images/thought.gif" height="115" width="125" alt="final thought"/></td>
<td><p><xsl:value-of select="//lang_blocks/p17" /></p></td>
</tr>
</table>




















<hr/>

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_10/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_10/lesson_5.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>