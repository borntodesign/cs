<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="lesson_1.xsl" ?>
<lang_blocks>
<p0>Lesson 1</p0>
<head>Chief Director Role, Responsibilities and Requirements</head>
<p1>The most important responsibility that you will have as a Chief Director is to consistently lead by example. Many leaders think that they can successfully lead, motivate and coach others to do things that they aren't doing themselves. The greatest error in judgment that you can make is to think that your position or title or what you have done in the past, gives you the credibility to lead successfully. It absolutely will not.</p1>
<p2>To be successful in this business, you MUST lead by example in everything you do. To help you to do this consistently, ask your self this question to determine what choice you should make:</p2>
<p3>"What if everyone in my organization did this? Would that be a good thing?"</p3>
<p4>Understanding the Principle of Duplication</p4>
<p5>What it takes to successfully develop an organization of marketers, marketing managers and marketing directors is unique and unlike anything that you may have done in the past, with the exception of parenting. The learning curve can be steep even if you have been previously successful in sales, marketing or management. That is why the Glocal Income Business Building System and Training Program are so important and necessary to help everyone to succeed.</p5>
<p6>A person's success in this business will be based almost totally on the commitment one has to succeed by diligently following the training.</p6>
<p7>Based on past experience, each person will have different places in their journey that will be more difficult than others. Learning what is needed at each step to overcome these difficulties is simple, but it is not easy, because it requires one to grow personally in order to grow professionally.</p7>
<p8>Some will have a difficult time gaining new ClubShop Rewards Members or Partners. Some will have a difficult time gaining new VIPs. Some will struggle with getting a good percentage of their VIPs to take and complete the Trainee courses. Some will struggle with getting their Builder Interns to become Builders. Some will struggle with getting their Builders to stay focused and motivated to keep building. Managing people or being a director will challenge others. And some may become proficient in all of these roles and abilities, but then struggle at becoming a good Chief Director and Trainer of Trainers.</p8>
<p9>To become an Executive Director and an Ambassadors Club Member, you need to learn how to become proficient in each and every role. You can't pick and choose or skip over what you don't like or want to do. And NEVER assume that you do not need to go back to prior role training courses once you become a Chief Director. Rather, you now have a greater need to go back and become proficient in each role so that you can not only build new groups better and faster, but also so you can help those that follow you to proficiently assume their roles.</p9>
<p10>Duplication must precede delegation!</p10>
<p11>The Glocal Income Business Building System and Training Program can provide anyone with the "power to prosper", but most people will need to see a good example be set for themselves to encourage them to take and follow the training and most people will also need a coach, mentor and friend to help them at each of the ten steps to financial freedom. To be a good coach, you have to know what you are talking about and not just have book knowledge. It has to be experiential knowledge. You can't teach what you don't know and you can't give what you don't have.</p11>
<p12>  Your Role To Your Organization</p12>
<p13>The Chief Director position is the highest and final role in the Glocal Income Business Building System's built-in management structure. With this role comes the overall responsibility to ensure that your organization continues to grow and thrive due to good duplication of efforts and good delegation of responsibilities.</p13>
<p14>You are basically the Head Coach of your team. You now have many assistant coaches, player coaches, players and trainees on your team and it is your job to make sure that they are performing their roles and working together. Here is a summary of your responsibilities as a Chief Director:</p14>
<p15>Locate and develop self-motivated and self-disciplined people to become your leaders within your organization.</p15>
<p16>Become a Trainer of Trainers by overseeing and supervising the Team Coaches on your team.</p16>
<p17>Trouble shoot and problem solve issues that arise within your organization.</p17>
<p18>Act as a liaison with the company when disputes arise between VIPs or to report suspected violations of our policies or Terms of Agreement.</p18>
<p19>Be the goodwill ambassador for your team by adding your acknowledgment, recognition and praise to your Team Coaches, for the efforts and achievements of the VIPs in your organization.</p19>
<p20>Assume the responsibility for your organization's VIP Team Page.</p20>
<p21>Work closely with and prepare your Promotions Directors to assume the Chief Director role from you for their organizations.</p21>
<p22>Become a Pay Agent (International Only).</p22>

<p115 new="3/30/11" literal="1">In addition to the above, you should organize, lead and host online webinars and presentations for your organization, so that there is at least one meeting available for VIPs to attend each week. Here are the recommended meeting guidelines:</p115>
<p116 new="3/30/11" literal="1"><b>1st Week</b> - Team Meeting - Recognize new VIPs, Trainees and Role and Pay Promotions. Make any announcement and provide business building tips. Invite upline or crossline Executive Directors to provide a testimonial or do an interview with.</p116>
<p117 new="3/30/11" literal="1"><b>2nd Week</b> - Pool Party - Business Opportunity Presentation - Invite all APs to attend. </p117>
<p118 new="3/30/11" literal="1"><b>3rd Week</b> - Builder Workshop - Builder Training that includes "role playing" and Q&amp;A sessions (AP &amp; VIP). Have growing Builders demonstrate how they call/Skype their APs.</p118>
<p119 new="3/30/11" literal="1"><b>4th Week</b> - Pool Party - Business Opportunity Presentation - Invite all APs to attend. Delegate the presentation to an "up and coming" Director to develop leaders in your organization.</p119>




<p23>Your Role With The Company</p23>
<p24>As the official liaison for your organization, you will be able to communicate directly with HQ staff to help answer questions or resolve problems or disputes.</p24>
<p25>Your Team Coaches should be responding to all of the questions posed by VIPs in their organizations. So unless you are acting in a Team Coach role for a VIP, their questions should be referred to the applicable Team Coach.</p25>
<p26>Member questions should go to the Builder Team Coach.</p26>
<p27>Questions about TNT, Partner Pools, using the auto filler, etc. should go to the Strategist Team Coach.</p27>
<p28>Questions about the training should go to the Trainer Team Coach.</p28>
<p29>Questions about advertising or marketing should go to the Marketing Director.</p29>
<p30>Questions about the Compensation Plan and promotions should go to the Promotions Director.</p30>
<p31>The only questions that you should be answering directly are questions from your Team Coaches and even then, you should reference the URL and section that may answer their question. This indicates to them that they can find most of the answers on their own without asking you.</p31>
<p32>There are exceptions to this rule, based on the need for you to build relationships downline, which will be discussed later in this course.</p32>
<p33>Before contacting HQ, lead by example by researching the website first, for answers to questions. Over time, we have worked hard to create a reference rich website and you should become aware of where to look up or locate information and answers relative to any questions that may be asked.</p33>
<p34>If however, you are unable to find the answer to your questions or need clarification or need to deal with a problem, print out and consult the chart linked below before contacting the company. You should never call HQ unless it is an emergency or an extremely time sensitive issue.</p34>
<p35>HQ Contact Chart Link</p35>
<!--<p36>Advisory Board Meetings </p36>
<p37> - An online Advisory Board meeting will be scheduled for selected Executive Directors to talk directly with HQ Executive staff about VIP concerns, to provide feedback and for us to share and discuss our plans with leaders. Typically, CEO Dick Burke will host these meetings so that he is aware of, and can address the needs "in the field". </p37>
<p38>In advance of these meetings, Advisory Board Members will need to send an email to </p38>
<p38a>vipsales@dhs-club.com</p38a> 
<p38b> to provide us with any items they would like discussed or questions they may have.</p38b>-->
<!--<p39>In addition to monthly online meetings, Advisory Board Members will also be invited to attend an Annual Retreat together.</p39>-->



<p40>Your Role With "Crossline" VIPs</p40>
<p41>As a Chief Director, you will have increased opportunities to meet and spend time with other Chief Directors and VIPs that are not in your organization (aka: crossline). It is not your role to counsel, train or answer any of their questions, unless you are acting in an official capacity at a company sponsored meeting, Conference or Convention. When approached, politely refer them to the applicable Team Coach or Chief Director.</p41>
<p42>In a later lesson, we will address some problems that can occur due to "crosslining" occurring within your organization. For the purpose of this lesson, we do want to encourage you and all Chief Directors to act as a role model and an encourager for all VIPs, whether or not they are in your organization. This will build unity and </p42>
<p43> confidence </p43>
<p44>for all VIPs and it acknowledges the principal of "sowing and reaping". Sow positive seeds of encouragement and friendship into other VIPs and other Chief Directors will do the same for yours. Whatever we do comes back to us, later and greater!</p44>
<p45>Your Role With Your Upline</p45>
<p46>Although your upline leaders are no longer responsible for any roles or responsibilities for your organization once you become a Chief Director, they still have a vested interest in the success of your organization and relationships that they have developed with VIPs in your organization. The same will apply to you once you have Chief Directors to delegate the role to.</p46>
<p47>As such, you should respect their experience and knowledge by continuing to counsel with them and honor them by inviting them to some of your online team meetings and by making them aware of any positive developments within your organization. </p47>

<p48> Click Here</p48>


<p98>Chief Requirements:</p98>
<p99>The requirements that you will need to meet before you can take the end of course test are shown below. Those shown in bold print have been modified from the PD requirements.</p99>
<p100>Accessed your Genealogy Report at least 15 times in the past 30 days.</p100>
<p101>Accessed your Activity Report at least 15 times in the past 30 days.</p101>
<!--p102>You must review and approve or return any Counseling Worksheets assigned to you within 15 days of submission.</p102-->

<pactivity>You must follow up within 7 days, on all activities that show in the Activity Report for which you are personally responsible.</pactivity>
<p103>Have at least 1% of your Organizational Pay Points be generated from purchases other than monthly subscription fees for the Glocal Income Business Building System or reports.</p103>
<p103a>View</p103a>
<p103b> the Country List that this requirement applies to.) Monitor your Marketing Director's Report available from your VIP Business Center - Business Reports Menu - Choose </p103b>
<p103c>Marketing Director's Report. </p103c>
<p104>Created and submitted your own Chief Team Page.</p104>
<p105>Qualified at the 4,500 Pay Point (Diamond Director) level.</p105>
<p106>Been at the Promotions Director Role level for at least 30 days before taking the test.</p106>
<!--<p107>to see an example of a Personal Planner for this level, with the minimum number of entries you should have) </p107>-->
<p108>Have 1,500 "Side Volume" Pay Points. Side volume consists of all Pay Points (including your Personal Pay Points), other than the Pay Points from your highest producing line of sponsorship.</p108>
<p109>Have at least as much in personal non-fee sales (personal purchases and/or frontline non-VIP Member purchases) as you do in personal Glocal Income Business Building System subscription fee purchases. </p109>
<p110>Sales Volume may not decrease for 6 consecutive months.</p110>

<nfpsales2>View</nfpsales2>
<nfpsales3> the Country List that this requirement applies to.</nfpsales3>

<nfpsalesexample new="4/19/11">Note: Stores that pay a "flat fee" commission on sales do not report the actual sales amount so that you can be credited for the sale to fulfill this requirement. The Mall listings for these type of stores will not show a percentage value for ClubCash or Pay Points. Rather, they will show just a "flat fee" Point value that will look like this: </nfpsalesexample>
<nfpsalesexample2>Therefore any sales of this type will not qualify towards this requirement.</nfpsalesexample2>
<storename>Store Name</storename>
<category>Category Type</category>



<p120 new="4/14/11">Communicating With VIPs In Other Executive Director Organizations</p120>
<p121 new="4/14/11">Once a VIP in your organization qualifies as an Executive Director, their organization and Pay Points "break away" from your organization. At this point, you should no longer act as the Chief Team Coach for the VIPs in another Executive Director's organization. Any invitations or promotions that you would like to provide to these VIPs, should be provided by their Executive Director. </p121>
<p122 new="4/14/11">See the </p122>
<p123 new="4/14/11">Communications Policy</p123>
<p124 new="4/14/11">for more information about communicating with VIPs in another Executive Director's organization.</p124>

<p130 new="4/29/11">Have at least six new VIP upgrades in your Organization in 30 days.</p130>




<tr3index>Chief Director Index</tr3index>
<assignment>Lesson Assignment</assignment>
<lesson>Lesson 2</lesson>
</lang_blocks>