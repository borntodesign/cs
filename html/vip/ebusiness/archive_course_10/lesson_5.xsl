<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /><br/><b>
<xsl:value-of select="//lang_blocks/p3" /></b></p>

<img src="../images/ambassadors_club.png" height="107" width="97" alt="Ambassadors Club"/>

<p><xsl:value-of select="//lang_blocks/p4" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p5" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p6" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p7" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8" /><xsl:text> </xsl:text><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>

<div align="center"><img src="../images/profit.gif" height="114" width="198" alt="Profitability"/></div>


<p><b><xsl:value-of select="//lang_blocks/p12" /></b></p>


<table class="xcolor" width="100%">
<tr class="acolor"><td><b><xsl:value-of select="//lang_blocks/p13" /></b></td><td><b><xsl:value-of select="//lang_blocks/p14" /></b></td><td><b><xsl:value-of select="//lang_blocks/p15" /></b></td><td><b><xsl:value-of select="//lang_blocks/p16" /></b></td></tr>
<tr class="bcolor"><td>Bronze</td><td>2</td><td>1</td><td>$100.00</td></tr>
<tr class="acolor"><td>Silver</td><td>3</td><td>1</td><td>$150.00</td></tr>
<tr class="bcolor"><td>Gold</td><td>3</td><td>2</td><td>$250.00</td></tr>
<tr class="acolor"><td>Ruby</td><td>4</td><td>3</td><td>$375.00</td></tr>
<tr class="bcolor"><td>Emerald</td><td>5</td><td>4</td><td>$500.00</td></tr>
<tr class="acolor"><td>Diamond</td><td>6</td><td>5</td><td>$750.00</td></tr>
<tr class="bcolor"><td>Executive Director</td><td>6+</td><td>6+</td><td>$1,000.00</td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p17" /></p>

<p><b><xsl:value-of select="//lang_blocks/p18" /></b></p>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p19" /></h5>
<p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /><xsl:text> </xsl:text><a onclick="" href="http://www.glocalincome.com/CompensationPlan.xml" target="_blank"><xsl:value-of select="//lang_blocks/p23" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24" /></p>


<hr/>

<div align="center"><img src="../images/ffreedom.gif" height="190" width="264" alt="Financial Freedom"/></div>

<h5><xsl:value-of select="//lang_blocks/p25" /></h5>
<p><xsl:value-of select="//lang_blocks/p26" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p27" /></li>
<li><xsl:value-of select="//lang_blocks/p28" /></li>
<li><xsl:value-of select="//lang_blocks/p29" /></li>
</ul>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<ol>
<li><b><xsl:value-of select="//lang_blocks/p32" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p33" /><br/><br/>
<xsl:value-of select="//lang_blocks/p34" /></li>
<br/>
<li><b><xsl:value-of select="//lang_blocks/p35" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p36" /><br/><br/>
<xsl:value-of select="//lang_blocks/p37" /><br/><br/>
<xsl:value-of select="//lang_blocks/p38" /><br/><br/>
<xsl:value-of select="//lang_blocks/p39" /></li>
<br/>

<li><b><xsl:value-of select="//lang_blocks/p40" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p41" /><br/><br/>
<xsl:value-of select="//lang_blocks/p42" /><br/><br/>
<xsl:value-of select="//lang_blocks/p43" /><br/><br/>
<xsl:value-of select="//lang_blocks/p44" /><br/><br/>
<xsl:value-of select="//lang_blocks/p45" /><br/><br/>
<xsl:value-of select="//lang_blocks/p46" /><br/><br/>
<xsl:value-of select="//lang_blocks/p47" /></li>
<br/>

<li><b><xsl:value-of select="//lang_blocks/p48" /></b><br/><br/>
<xsl:value-of select="//lang_blocks/p49" /><br/><br/>
<xsl:value-of select="//lang_blocks/p50" /><br/><br/>
<xsl:value-of select="//lang_blocks/p51" /><br/><br/>
<xsl:value-of select="//lang_blocks/p52" /></li>
</ol>








<hr/>

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_10/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_10/lesson_6.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>