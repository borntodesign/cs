<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<table width="800" margin="25px">
<tr>
<td class="top"><img src="http://www.clubshop.com/vip/ebusiness/images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>
</tr>
</table>
<table ><tr><td>
<h5><xsl:value-of select="//lang_blocks/p0" /><br/>  
<xsl:value-of select="//lang_blocks/head" /></h5> 

       
<p><img src="http://www.clubshop.com/vip/ebusiness/images/traineeline.gif" /></p>

<table>
<tr class="a"><td><img src="http://www.clubshop.com/vip/execs/hometeam.jpg"/></td>

<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr>
</table>
<ol>
<li><xsl:value-of select="//lang_blocks/p2" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p3" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p4" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p5" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p6" /></li><br/>
<li><xsl:value-of select="//lang_blocks/p60" /></li><br/>
</ol>

<table  border="1" cellpadding="3">

<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/p7" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p15" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p23" /></b></td>
<td><b><xsl:value-of select="//lang_blocks/p31" /></b></td>
</tr>

<tr class="bcolor"><!--accounting-->
<td><b><xsl:value-of select="//lang_blocks/p8" /></b></td>
<td><xsl:value-of select="//lang_blocks/p16" /></td>
<td>Cindy Hennessy<br/>acfspvr@glocalincome.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>

</tr>

<tr class="acolor"><!--membership support-->
<td><b><xsl:value-of select="//lang_blocks/p9" /></b></td>
<td>All sales, membership <br/>&amp; consumer support issues</td>
<td>Ann Walker<br/>support1@glocalincome.com</td>
<td>Lisa Young<br/>supportspvr@glocalincome.com</td>
</tr>

<tr class="bcolor"><!--membership support fr-->
<td><b><xsl:value-of select="//lang_blocks/p24" /></b></td>
<td><xsl:value-of select="//lang_blocks/p26" /></td>
<td><xsl:value-of select="//lang_blocks/sl" /><br/><xsl:value-of select="//lang_blocks/p45" /></td>
<td>Lisa Young<br/>supportspvr@glocalincome.com</td>
</tr>

<tr class="acolor"><!--membership support nl-->
<td><b><xsl:value-of select="//lang_blocks/p25" /></b></td>
<td><xsl:value-of select="//lang_blocks/p27" /></td>
<td><xsl:value-of select="//lang_blocks/wp" /><br/><xsl:value-of select="//lang_blocks/p46" /></td>
<td>Lisa Young<br/>supportspvr@glocalincome.com</td>
</tr>

<tr class="bcolor"><!--membership support it-->
<td><b><xsl:value-of select="//lang_blocks/p28" /></b></td>
<td><xsl:value-of select="//lang_blocks/p21" /></td>
<td><xsl:value-of select="//lang_blocks/ea" /><br/><xsl:value-of select="//lang_blocks/p48" /></td>
<td>Lisa Young<br/>supportspvr@glocalincome.com</td>
</tr>


<tr class="acolor"><!--GI and EBiz-->
<td><b><xsl:value-of select="//lang_blocks/ebiz" /></b></td>
<td><xsl:value-of select="//lang_blocks/bizop" /></td>
<td><xsl:value-of select="//lang_blocks/dc" /><br/>vipsales@glocalincome.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>
</tr>




<tr class="bcolor"><!--IT-->
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/p19" /></td>
<td>Bill MacArthur<br/>webmaster@glocalincome.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>

</tr>

<tr class="acolor"><!--IT-->
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/p28a" /></td>
<td>Bill MacArthur<br/>webmaster@glocalincome.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>
</tr>

<!--<tr class="bcolor">IT
<td><b><xsl:value-of select="//lang_blocks/p11" /></b></td>
<td><xsl:value-of select="//lang_blocks/program" /></td>
<td>Keith Hasely<br/>k.hasely@clubshop.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>

</tr>-->

<tr class="bcolor"><!--Offline Consumer Development-->

<td><b><xsl:value-of select="//lang_blocks/offline" /></b></td>
<td>ClubShop Reward Card Merchants <br/>&amp; Card Distribution</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>
</tr>


<tr class="acolor"><!--ClubShop Rewards Development-->

<td><b><xsl:value-of select="//lang_blocks/crw" /></b></td>
<td><xsl:value-of select="//lang_blocks/p21a" /></td>
<td>Carina Burke<br/>mallspecials@clubshop.com</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
</tr>

<tr class="bcolor"><!--Online Consumer Development-->

<td><b><xsl:value-of select="//lang_blocks/p12" /></b></td>
<td><xsl:value-of select="//lang_blocks/p20" /></td>
<td>Carina Burke<br/>sales@clubshop.com</td>
<td>Will Burke<br/>salesmgr@clubshop.com</td>
</tr>


<!--<tr class="acolor">
<td><b><xsl:value-of select="//lang_blocks/website"/></b></td>
<td>Overall website appearance,<br/> New Webpages &amp; Graphic Design</td>
<td>Carsten Rieger<br/>c.rieger@dhs-club.com</td>
<td>Dick Burke<br/>rburke@dhs-club.com</td>

</tr>-->

<tr class="acolor"><!--Website Design Maintenance-->
<td><b><xsl:value-of select="//lang_blocks/p14" /></b></td>
<td>VIP Control Center postings<br/>Translations, Modifications</td>
<td>Lisa Young<br/>supportspvr@glocalincome.com</td>
<td>Dick Burke<br/>rburke@glocalincome.com</td>
</tr>
</table>



<br/>

<hr/>
 </td></tr></table>       
      
         
         
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>