<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<img src="../images/teamdevelop.jpg" height="110" width="74" alt="Team"/>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<p><xsl:value-of select="//lang_blocks/p3" /></p>
<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /></p>
<div align="center">
<img src="../images/teamunity.jpg" height="70" width="182" alt="team unity"/></div>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p6" /></h5>

<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<ol>
<li><xsl:value-of select="//lang_blocks/p11" /></li>
<li><xsl:value-of select="//lang_blocks/p12" /></li>
<li><xsl:value-of select="//lang_blocks/p13" /></li>
</ol>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>


<div align="center"><img src="../images/teach3.gif" height="202" width="318" alt="Teach 3 things"/></div>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p16" /></h5>
<img src="../images/multiply.jpg" height="110" width="83" alt="soar"/>
<p><xsl:value-of select="//lang_blocks/p17" /><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:value-of select="//lang_blocks/p19" /><b><xsl:value-of select="//lang_blocks/p20" /></b><xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /><b><xsl:value-of select="//lang_blocks/p24" /></b></p>
<p><xsl:value-of select="//lang_blocks/p25" /></p>
<p><xsl:value-of select="//lang_blocks/p26" /></p>

<table>
<tr><td><img src="../images/select.jpg" height="110" width="68" alt="YOU"/></td>
<td><p><b><xsl:value-of select="//lang_blocks/p27" /></b></p></td>
</tr>
</table>
<p><xsl:value-of select="//lang_blocks/p28" /></p>

<h5><xsl:value-of select="//lang_blocks/p29" /></h5>
<p><xsl:value-of select="//lang_blocks/p30" /></p>

<ol>
<li><xsl:value-of select="//lang_blocks/p31" /></li>
<li><xsl:value-of select="//lang_blocks/p32" /></li>
<li><xsl:value-of select="//lang_blocks/p33" /></li>
<li><xsl:value-of select="//lang_blocks/p34" /></li>
<li><xsl:value-of select="//lang_blocks/p35" /></li>
<li><xsl:value-of select="//lang_blocks/p36" /></li>
</ol>

<p><xsl:value-of select="//lang_blocks/p37" /></p>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p38" /></h5>

<p><xsl:value-of select="//lang_blocks/p39" /></p>
<p><xsl:value-of select="//lang_blocks/p40" /></p>
<p><xsl:value-of select="//lang_blocks/p41" /></p>
<p><xsl:value-of select="//lang_blocks/p42" /></p>
<p><xsl:value-of select="//lang_blocks/p43" /></p>
<p><xsl:value-of select="//lang_blocks/p44" /></p>
<p><xsl:value-of select="//lang_blocks/p45" /></p>

<div align="center"><img src="../images/relationship.gif" height="144" width="264" alt="relationships"/></div>

<hr/>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<ol>
<li><xsl:value-of select="//lang_blocks/p46" /></li>
<li><xsl:value-of select="//lang_blocks/p47" /></li>
<li><xsl:value-of select="//lang_blocks/p48" /></li>
</ol>
<p><xsl:value-of select="//lang_blocks/p49" /></p>
</td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p50" /></p>



<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_10/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_10/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>