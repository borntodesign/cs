<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="1000">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<h4><xsl:value-of select="//lang_blocks/p1" /></h4>
<p><xsl:value-of select="//lang_blocks/p2" /></p>


<table width="75%">
<tr>
<td><img src="../images/readersleaders.gif" height="150" width="70" alt="Readers are Leaders"/></td>

<td>
<li><xsl:value-of select="//lang_blocks/p3" /></li>
<li><xsl:value-of select="//lang_blocks/p4" /></li>
<li><xsl:value-of select="//lang_blocks/p5" /></li>
<li><xsl:value-of select="//lang_blocks/p6" /></li>
<li><xsl:value-of select="//lang_blocks/p7" /></li>
<li><xsl:value-of select="//lang_blocks/p8" /></li>
<li><xsl:value-of select="//lang_blocks/p9" /></li>
<li><xsl:value-of select="//lang_blocks/p10" /></li>
<li><xsl:value-of select="//lang_blocks/p11" /></li>
<li><xsl:value-of select="//lang_blocks/p12" /></li>
</td>
</tr>
</table>
<br/><br/>

<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p13" /></p></td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<table>
<tr><td><img src="../images/bringbestbook.jpg" height="200" width="116" alt="Bringing Out the Best in People"/></td>
<td><p><xsl:value-of select="//lang_blocks/p16" /><b><i><xsl:value-of select="//lang_blocks/p17" /></i></b><xsl:value-of select="//lang_blocks/p18" /></p></td>
</tr></table>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p19" /></h5>
<p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>


<p><xsl:value-of select="//lang_blocks/p22" /><a onclick="" href="http://www.clubshop.com/intl_pay_agent.html" target="_blank"><xsl:value-of select="//lang_blocks/p23" /></a></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><b><xsl:value-of select="//lang_blocks/p24" /></b> <xsl:value-of select="//lang_blocks/p25" />
<br/><a onclick="" href="http://www.clubshop.com/pay_agent_app.xml" target="_blank"><xsl:value-of select="//lang_blocks/p26" /></a></p></td>
</tr>
</table>


<hr/>
<h5><xsl:value-of select="//lang_blocks/p27" /></h5>
<img src="../images/contest.jpg" height="90" width="120" alt="Contests"/>
<p><xsl:value-of select="//lang_blocks/p28" /></p>
<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>

<p><xsl:value-of select="//lang_blocks/p31" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p32" /></li>
<li><xsl:value-of select="//lang_blocks/p33" /></li>
<li><xsl:value-of select="//lang_blocks/p34" /></li>
</ul>


<p><xsl:value-of select="//lang_blocks/p35" /></p>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p36" /></h5>
<p><xsl:value-of select="//lang_blocks/p37" /></p>

<p><b><xsl:value-of select="//lang_blocks/p38" /></b><xsl:value-of select="//lang_blocks/p39" /></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p40" /><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_10/teampage_instructions.html" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p42" /></p>

<div align="center"> 
<form action="http://www.clubshop.com/cgi/member_actions.cgi" method="post" target="_confirm">
<input type="text" name="url" size="60"/>
<br/>
<input type="submit" value="Submit"/>
<input type="reset" name="Reset" value="Reset"/>
<input type="hidden" name="action" value="confirm"/>
<input type="hidden" name="type" value="13"/>
<input type="hidden" name="u_key" value="uptcs"/>
</form>
</div> 
<p><b><xsl:value-of select="//lang_blocks/p43" /></b></p>

</td></tr>
</table>

<p><b><xsl:value-of select="//lang_blocks/p44" /></b><xsl:value-of select="//lang_blocks/p45" /></p>
<p><xsl:value-of select="//lang_blocks/p46" /></p>

<ul>
<li><xsl:value-of select="//lang_blocks/p47" /></li>
<li><xsl:value-of select="//lang_blocks/p48" /></li>
<li><xsl:value-of select="//lang_blocks/p49" /></li>
<li><xsl:value-of select="//lang_blocks/p50" /></li>
<li><xsl:value-of select="//lang_blocks/p51" /></li>
<li><xsl:value-of select="//lang_blocks/p52" /></li>
</ul>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p53" /></p></td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p54" /></p>

<p><b><xsl:value-of select="//lang_blocks/p55" /></b><xsl:value-of select="//lang_blocks/p56" /></p>

<p><xsl:value-of select="//lang_blocks/p57" /></p>

<hr/>

<p><b><xsl:value-of select="//lang_blocks/p109" /></b><a onclick="" href="meeting_request.html" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p58" /></p>
<p><b><xsl:value-of select="//lang_blocks/p110" /></b><a onclick="" href="sign-in.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p59" /></p>
<p><b><xsl:value-of select="//lang_blocks/p111" /></b><a onclick="" href="reconcile.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p60" /></p>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p61" /></h5>

<p><xsl:value-of select="//lang_blocks/p62" /></p>

<p><xsl:value-of select="//lang_blocks/p63" /></p>



<h5><xsl:value-of select="//lang_blocks/p67" /></h5>
<p><xsl:value-of select="//lang_blocks/p68" /></p>

<p><xsl:value-of select="//lang_blocks/p69" /></p>
<p><xsl:value-of select="//lang_blocks/p70" /></p>

<hr/>
<p><a onclick="" href="hosts.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p71" /></p>
<p><a onclick="" href="sign-in.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p72" /></p>
<p><a onclick="" href="reconcile.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p73" /></p>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p74" /></h5>
<p><xsl:value-of select="//lang_blocks/p75" /></p>

<p><xsl:value-of select="//lang_blocks/p76" /></p>
<p><xsl:value-of select="//lang_blocks/p77" /></p>
<p><xsl:value-of select="//lang_blocks/p77a" /></p>
<p><img src="http://wwww.glocalincome.com/images/icons/icon_pdf.png"/><xsl:text> </xsl:text><a onclick="" href="speakers.pdf" target="_blank"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p78" /></p>

<hr/>

<h5><xsl:value-of select="//lang_blocks/p79" /></h5>
<p><xsl:value-of select="//lang_blocks/p80" /></p>

<p><xsl:value-of select="//lang_blocks/p81" /></p>
<p><xsl:value-of select="//lang_blocks/p82" /></p>
<p><xsl:value-of select="//lang_blocks/p83" /></p>
<p><xsl:value-of select="//lang_blocks/p84" /></p>

<hr/>

<img src="../images/eggsbasket.jpg" height="125" width="111" alt="eggs in a basket"/>
<h5><xsl:value-of select="//lang_blocks/p85" /></h5>
<p><xsl:value-of select="//lang_blocks/p86" /></p>
<p><xsl:value-of select="//lang_blocks/p87" /></p>
<p><xsl:value-of select="//lang_blocks/p88" /><b><xsl:value-of select="//lang_blocks/p89" /></b><xsl:value-of select="//lang_blocks/p91" /></p>


<!--<table class="xcolor"><tr><td>
<ul>
<li><xsl:value-of select="//lang_blocks/p93" /></li>
<li><xsl:value-of select="//lang_blocks/p94" /></li>
<li><xsl:value-of select="//lang_blocks/p95" /></li>
<li><xsl:value-of select="//lang_blocks/p96" /></li>
<li><xsl:value-of select="//lang_blocks/p97" /></li>
</ul>
</td></tr></table>-->

<hr/>

<h5><xsl:value-of select="//lang_blocks/p98" /></h5>
<p><xsl:value-of select="//lang_blocks/p99" /></p>


<ul>
<li><xsl:value-of select="//lang_blocks/p100" /></li>
<li><xsl:value-of select="//lang_blocks/p101" /></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><xsl:value-of select="//lang_blocks/p103" /><xsl:text> </xsl:text>
(<a href="/vip/ebusiness/nforgsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p103a" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p103b" /><a href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p103c" /></a></li>
<li><xsl:value-of select="//lang_blocks/p104" /></li>
<li><xsl:value-of select="//lang_blocks/p105" /></li>
<li><xsl:value-of select="//lang_blocks/p108" /></li>
<li><xsl:value-of select="//lang_blocks/p130" /></li>
<li><xsl:value-of select="//lang_blocks/p112" /> <a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
<!--<li><a onclick="" href="http://www.clubshop.com/vip/ebusiness/course_10/CH.xml"><xsl:value-of select="//lang_blocks/p41" /></a><xsl:value-of select="//lang_blocks/p107" /></li>-->
<!--removed 6/1/10<li><xsl:value-of select="//lang_blocks/p113" /></li>-->
</ul>

<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->
<hr/>

<div align="center"><img src="http://www.clubshop.com/vip/ebusiness/images/emeraldcity.jpg" height="302" width="402" alt="Emerald City! The End of the Yellow Brick Road!"/></div>









<hr/>

<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/cgi/e/ebiz-test.cgi/12966">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>