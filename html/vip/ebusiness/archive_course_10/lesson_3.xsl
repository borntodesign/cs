<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c10_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t10_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<table>
<tr><td>
<img src="../images/microscope.jpg" height="134" width="74" alt="microscope"/>
</td>
<td><p><xsl:value-of select="//lang_blocks/p1" /></p></td>
</tr></table>

<p><b><xsl:value-of select="//lang_blocks/p2" /></b><xsl:value-of select="//lang_blocks/p3" /></p>
<p><b><xsl:value-of select="//lang_blocks/p4" /></b><xsl:value-of select="//lang_blocks/p5" /></p>


<table><tr><td><img src="../images/skunk.jpg" height="84" width="82" alt="skunk"/>
</td>
<td><p><xsl:value-of select="//lang_blocks/p6" /></p></td>
</tr></table>
<p><xsl:value-of select="//lang_blocks/p7" /></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>

<hr/>

<table>
<tr><td><img src="../images/microwaves.jpg" height="113" width="90" alt="microwave mentality"/></td>
<td><p><b><xsl:value-of select="//lang_blocks/p9" /></b><xsl:value-of select="//lang_blocks/p10" /></p></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p11" /></p>

<ul>
<li><b><xsl:value-of select="//lang_blocks/p12" /></b><xsl:value-of select="//lang_blocks/p13" /></li>
<li><b><xsl:value-of select="//lang_blocks/p14" /></b><xsl:value-of select="//lang_blocks/p15" /></li>
<li><b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:value-of select="//lang_blocks/p17" /></li>
<li><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:value-of select="//lang_blocks/p19" /></li>

</ul>
<p><xsl:value-of select="//lang_blocks/p20" /></p>
<p><xsl:value-of select="//lang_blocks/p21" /></p>
<hr/>
<p><b><xsl:value-of select="//lang_blocks/p22" /></b><xsl:value-of select="//lang_blocks/p23" /></p>
<p><b><xsl:value-of select="//lang_blocks/p24" /></b><xsl:value-of select="//lang_blocks/p25" /></p>

<div align="center"><img src="../images/interdependent.gif" height="227" width="149" alt="interdependancy"/></div>

<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>

<hr/>
<table><tr><img src="../images/complacency.jpg" height="74" width="111" alt="complacency"/><td></td>
<td><p><b><xsl:value-of select="//lang_blocks/p28" /></b><xsl:value-of select="//lang_blocks/p29" /></p></td>
</tr></table>

<p><xsl:value-of select="//lang_blocks/p30" /></p>
<p><xsl:value-of select="//lang_blocks/p31" /></p>
<p><xsl:value-of select="//lang_blocks/p32" /></p>

<hr/>

<table><tr><td><img src="../images/crossover.gif" height="105" width="161" alt="Crossline"/></td>
<td><p><b><xsl:value-of select="//lang_blocks/p33" /></b><xsl:value-of select="//lang_blocks/p34" /></p></td>
</tr></table>

<p><xsl:value-of select="//lang_blocks/p35" /></p>
<hr/>

<p><b><xsl:value-of select="//lang_blocks/p36" /></b><xsl:value-of select="//lang_blocks/p37" /></p>
<p><xsl:value-of select="//lang_blocks/p38" /></p>
<p><xsl:value-of select="//lang_blocks/p39" /></p>









<hr/>

<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_10/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_10/lesson_4.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>