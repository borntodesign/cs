<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c9_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t9_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><xsl:value-of select="//lang_blocks/p1"/></p>
<p><xsl:value-of select="//lang_blocks/p2"/></p>

<ul>

<li><xsl:value-of select="//lang_blocks/p3"/></li>
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li>

</ul>

<p><xsl:value-of select="//lang_blocks/p7"/></p>
<p><xsl:value-of select="//lang_blocks/p8"/></p>


<table class="xcolor">
<tr class="bcolor"><td><b><xsl:value-of select="//lang_blocks/p9"/></b></td>
</tr></table>


<hr/>

<table>
<tr><td><img src="../images/promotions.gif" height="202" width="150" alt="Climbing Ladder"/></td>
<td><b><xsl:value-of select="//lang_blocks/p10"/></b>
<ol>
<li><xsl:value-of select="//lang_blocks/p11"/></li>
<li><xsl:value-of select="//lang_blocks/p12"/></li>
<li><xsl:value-of select="//lang_blocks/p13"/></li>
<li><xsl:value-of select="//lang_blocks/p14"/></li>
<li><xsl:value-of select="//lang_blocks/p15"/></li>

</ol>
</td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p16"/></p>
<hr/>


<table class="xcolor">
<tr><td>
<h5><xsl:value-of select="//lang_blocks/p17"/></h5>
<p><xsl:value-of select="//lang_blocks/p18"/></p>
<ul>

<!--<li><xsl:value-of select="//lang_blocks/p19"/></li>-->
<li><b><xsl:value-of select="//lang_blocks/p20"/></b></li>
<li><xsl:value-of select="//lang_blocks/p20r"/></li>
<li><b><xsl:value-of select="//lang_blocks/p21"/></b></li>
<li><b><xsl:value-of select="//lang_blocks/p22"/></b></li>
<li><xsl:value-of select="//lang_blocks/pactivity" /></li>
<li><b><xsl:value-of select="//lang_blocks/p24"/></b>
(<a href="/vip/ebusiness/nforgsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/p24a"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24b"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/sales-rpt.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p24c"/></a>
</li>

<li><xsl:value-of select="//lang_blocks/p25"/></li>
<li><xsl:value-of select="//lang_blocks/p28"/></li>
<li><xsl:value-of select="//lang_blocks/p29"/> <xsl:text> </xsl:text><a href="/vip/ebusiness/nfpsales_list.xml" target="_blank"><xsl:value-of select="//lang_blocks/nfpsales2" /></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/nfpsales3" /></li>
<li><xsl:value-of select="//lang_blocks/p26"/></li>

</ul>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample" /><xsl:text> </xsl:text>
ClubCash $2.00<xsl:text> </xsl:text>Pay Points 2.0</p>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
<!--<table class="assign"><tr>
<td><xsl:value-of select="//lang_blocks/storename" /></td>
<td><xsl:value-of select="//lang_blocks/category"/></td><td>1725 Points</td><td>1150 Points</td><td>11.5 Points</td></tr></table>
<p><xsl:value-of select="//lang_blocks/nfpsalesexample2" /></p>
-->
</td></tr></table>




<hr/>
<div align="center"><p><a href="index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="lesson_2.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>