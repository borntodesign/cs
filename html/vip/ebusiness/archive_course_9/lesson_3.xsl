<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c9_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t9_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>

<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td>
</tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p2" /></p></td></tr>
</table>

<p><xsl:value-of select="//lang_blocks/p3" /></p>-->



<p><xsl:value-of select="//lang_blocks/p4" /></p>


<p><i><xsl:value-of select="//lang_blocks/p5" /></i></p>

<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p><i><xsl:value-of select="//lang_blocks/p7" /></i></p>
<hr/>
<h5><xsl:value-of select="//lang_blocks/p8" /></h5>

<p><xsl:value-of select="//lang_blocks/p9" /></p>
<img src="../images/picture.gif" height="100" width="100" alt="picture"/>

<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>

<table>
<tr>
<td><img src="../images/c9_l3.gif" height="160" width="160" alt="no pay"/></td>
<td><ul>
<li><xsl:value-of select="//lang_blocks/p15" /></li>
<li><xsl:value-of select="//lang_blocks/p16" /></li>
<li><xsl:value-of select="//lang_blocks/p17" /></li></ul>
<p><xsl:value-of select="//lang_blocks/p18" /></p>
</td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p19" /></p>
<p><xsl:value-of select="//lang_blocks/p20" /></p>
<hr/>


<h5><xsl:value-of select="//lang_blocks/p21" /></h5>

<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<img src="../images/lighton.jpg" height="110" width="73" alt="lightson"/>

<h5><xsl:value-of select="//lang_blocks/p25" /></h5>
<p><xsl:value-of select="//lang_blocks/p26" /></p>
<p><xsl:value-of select="//lang_blocks/p27" /></p>

<table>
<tr>
<td><img src="../images/connectdots.jpg" height="132" width="121" alt="follow dots"/></td>
<td><p><xsl:value-of select="//lang_blocks/p28" /></p></td>
<td><img src="../images/pbynumb.gif" height="117" width="155" alt="by the numbers"/></td>
</tr></table>

<p><xsl:value-of select="//lang_blocks/p29" /></p>
<hr/>

<h5><xsl:value-of select="//lang_blocks/p30" /></h5>
<p><xsl:value-of select="//lang_blocks/p31" /></p>

<p><xsl:value-of select="//lang_blocks/p32" /></p>


<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p33" /></p></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p34" /></p>



<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p35" /></p></td>
</tr>
</table>



<hr/>
<h5><xsl:value-of select="//lang_blocks/p37" /></h5>

<p><xsl:value-of select="//lang_blocks/p38" /></p>



<ul><li><xsl:value-of select="//lang_blocks/p39" /></li></ul>

<p><xsl:value-of select="//lang_blocks/p40" /></p>

<ul><li><xsl:value-of select="//lang_blocks/p41" /></li>
<li><xsl:value-of select="//lang_blocks/p42" /></li>
<li><xsl:value-of select="//lang_blocks/p43" /></li>
</ul>

<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p44" /></p></td>
</tr>
</table>-->
<hr/>

<h5><xsl:value-of select="//lang_blocks/p45" /></h5>

<img src="../images/mojo.gif" height="111" width="111" alt="mojo"/>

<p><xsl:value-of select="//lang_blocks/p46" /></p>
<p><xsl:value-of select="//lang_blocks/p47" /></p>
<p><xsl:value-of select="//lang_blocks/p48" /></p>



















<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_9/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_9/lesson_4.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>