<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c9_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t9_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>
<p><xsl:value-of select="//lang_blocks/p1" /></p>
<p><xsl:value-of select="//lang_blocks/p2" /></p>
<h5><xsl:value-of select="//lang_blocks/p3" /></h5>
<img src="../images/magnifyup.gif" height="73" width="110" alt="magnifyup"/>
<p><b><xsl:value-of select="//lang_blocks/p4" /></b><xsl:value-of select="//lang_blocks/p5" /><a onclick="" href="http://www.glocalincome.com/CompensationPlan.xml" target="_blank"><xsl:value-of select="//lang_blocks/p6" /></a><xsl:value-of select="//lang_blocks/p7" /></p>

<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>



<table class="xcolor">
<tr class="bcolor"><td><xsl:value-of select="//lang_blocks/p10" /></td>
</tr>
</table>

<p><xsl:value-of select="//lang_blocks/p11" /></p>

<p><b><xsl:value-of select="//lang_blocks/p12" /></b><xsl:value-of select="//lang_blocks/p13" /></p>

<p><xsl:value-of select="//lang_blocks/p14" /></p>

<table>
<tr>
<td><p><xsl:value-of select="//lang_blocks/p15" /></p></td>
<td><img src="../images/grill.gif" height="103" width="80" alt="fire"/></td>
</tr></table>

<p><img src="../images/magnifydown.gif" height="73" width="110" alt="magnifydown"/>

<b><xsl:value-of select="//lang_blocks/p16" /></b><xsl:value-of select="//lang_blocks/p17" /></p>
<p><b><xsl:value-of select="//lang_blocks/p18" /></b><xsl:value-of select="//lang_blocks/p19" /></p>


<p><xsl:value-of select="//lang_blocks/p20" /></p>
<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td><p><xsl:value-of select="//lang_blocks/p21" /></p></td></tr>
</table>-->




<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_9/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_9/lesson_5.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>