<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
   <xsl:template match="/">
      <html>
         <head>
            <title>E-Business Institute</title>

<link href="/css/c9_ebiz.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<table width="1000">
<tr>
<td class="top"><img src="../images/t9_logo.jpg" width="705" height="101" alt="ebiz" /></td>

</tr>
</table>
<table width="900">
<tr><td>
<p class="sub"><xsl:value-of select="//lang_blocks/p0" /></p>
<img src="../images/traineeline.gif" />


<h5><xsl:value-of select="//lang_blocks/head" /></h5>

<p><img src="../images/oilrig.jpg" height="123" width="85" alt="Pump It Up!"/><xsl:value-of select="//lang_blocks/p1"/></p>

<p><xsl:value-of select="//lang_blocks/p2" /></p>

<p><xsl:value-of select="//lang_blocks/p3" /></p>

<p><xsl:value-of select="//lang_blocks/p4" /></p>
<p><xsl:value-of select="//lang_blocks/p5" /> <b><xsl:value-of select="//lang_blocks/p6" /></b></p>



<table class="xcolor">
<tr>
<td>


<p><b><xsl:value-of select="//lang_blocks/p7" /></b></p>
<p><xsl:value-of select="//lang_blocks/p8" /></p>
<p><xsl:value-of select="//lang_blocks/p9" /></p>
<p><xsl:value-of select="//lang_blocks/p10" /></p>
<p><xsl:value-of select="//lang_blocks/p11" /></p>
<p><xsl:value-of select="//lang_blocks/p12" /></p>
<p><xsl:value-of select="//lang_blocks/p13" /></p>
<p><xsl:value-of select="//lang_blocks/p14" /></p>
<p><xsl:value-of select="//lang_blocks/p15" /></p>
<p><xsl:value-of select="//lang_blocks/p16" /></p>
<p><xsl:value-of select="//lang_blocks/p17" /></p>
<p><xsl:value-of select="//lang_blocks/p18" /></p>
</td>
</tr>
<tr><td>
<div align="center"><p><img src="../images/scroll2.gif" height="93" width="263" alt="moral"/></p></div>

<h5><xsl:value-of select="//lang_blocks/p19" /></h5>

<p><xsl:value-of select="//lang_blocks/p20" /></p>
<table class="xcolor">
<tr><td>
<p><xsl:value-of select="//lang_blocks/p21" /></p>
<p><xsl:value-of select="//lang_blocks/p22" /></p>
<p><xsl:value-of select="//lang_blocks/p23" /></p>
<p><xsl:value-of select="//lang_blocks/p24" /></p>
<p class="indent"><xsl:value-of select="//lang_blocks/p25" /></p>
<p class="indent"><xsl:value-of select="//lang_blocks/p26" /></p>
<p class="indent"><xsl:value-of select="//lang_blocks/p27" /></p>
<p><xsl:value-of select="//lang_blocks/p28" /></p>
<p><xsl:value-of select="//lang_blocks/p29" /></p>
<p><xsl:value-of select="//lang_blocks/p30" /></p>
</td></tr></table>


<div align="center"><p><img src="../images/scroll2.gif" height="93" width="263" alt="moral"/></p></div>

</td>
</tr>
</table>

<hr/>
<h5><xsl:value-of select="//lang_blocks/p31" /></h5>
<p><xsl:value-of select="//lang_blocks/p32" /></p>



<table class="xcolor">
<tr class="acolor"><td></td><td>1st Line</td><td>2nd Line</td><td>3rd Line</td><td>4th Line</td><td>5th Line</td><td>6th Line</td></tr>
<tr class="bcolor"><td>75</td><td>25+</td><td>25</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>
<tr class="acolor"><td>150</td><td>75+</td><td>25+</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>
<tr class="bcolor"><td>250</td><td>150</td><td>50</td><td>25</td><td>-</td><td>-</td><td>-</td></tr>
<tr class="acolor"><td>400</td><td>200</td><td>100</td><td>50</td><td>25</td><td>-</td><td>-</td></tr>
<tr class="bcolor"><td>600</td><td>300</td><td>150</td><td>75</td><td>50</td><td>-</td><td>-</td></tr>
<tr class="acolor"><td>1,000</td><td>500</td><td>250</td><td>150</td><td>75</td><td>25</td><td>-</td></tr>
<tr class="bcolor"><td>1,500</td><td>750</td><td>400</td><td>200</td><td>100</td><td>50</td><td>25</td></tr>
<tr class="acolor"><td>2,250</td><td>1,125</td><td>600</td><td>300</td><td>150</td><td>75</td><td>50</td></tr>
<tr class="bcolor"><td>3,250</td><td>1,625</td><td>800</td><td>400</td><td>200</td><td>100</td><td>75</td></tr>
<tr class="acolor"><td>4,500</td><td>2,250</td><td>1,125</td><td>600</td><td>300</td><td>150</td><td>100</td></tr>
<tr class="acolor"><td>6,000</td><td>3,000</td><td>1,500</td><td>750</td><td>400</td><td>200</td><td>150</td></tr>

</table>




<!--<table class="assign">
<tr><td class="assign"><xsl:value-of select="//lang_blocks/assignment" /></td></tr>
<tr><td>
<ol>
<li><xsl:value-of select="//lang_blocks/p33" /></li>
<li><xsl:value-of select="//lang_blocks/p34" /></li>
<li><xsl:value-of select="//lang_blocks/p35" /></li>

</ol>

</td>
</tr></table>-->






<hr/>
<div align="center"><p><a href="http://www.clubshop.com/vip/ebusiness/course_9/index.xml">
<xsl:value-of select="//lang_blocks/tr3index" /></a>  |  <a href="http://www.clubshop.com/vip/ebusiness/course_9/lesson_3.xml">
<xsl:value-of select="//lang_blocks/lesson" /></a>
</p> </div>        
     
       
         
         </td></tr></table>
         
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>