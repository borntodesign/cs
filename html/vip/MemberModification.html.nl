﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">



<html>



<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    

<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    

<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->

<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->

<!-- [if gt IE 8]> <!-- --> 

 

<html xmlns="http://www.w3.org/1999/xhtml">

 

<!--<![endif]-->

<head>



            <title>ClubShop Rewards Partner Manual</title>











<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />

    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />

    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>

    <script src="http://www.clubshop.com/js/partner/app.js"></script>

    <script src="http://www.clubshop.com/js/partner/flash.js"></script>

    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->







<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



</head>

<body>



	<div class="container blue">

	<div class="row">

		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>

		<div class="six columns"></div>



	</div></div>



	<div class="container">

	<div class="row">

	<div class="twelve columns ">

	<br />

	<h4 class="topTitle"></h4>



	<hr />
      <span class="style28">Member <strong>Modificatie Functies</strong></span> 
      <hr/>

	</div></div></div>

			



      

	<div class="row">

	<div class="twelve columns">
    <p>De leden <strong>Modificatie Functies</strong> interface werd gecreëerd 
      om een aantal zaken gemakkelijker te maken, met name: een Affiliate veranderen 
      naar ClubShop Rewards Member, een slecht telefoonnummer melden of, voor 
      het melden van lidmaatschappen met een "slecht" e-mail adres, een "geblokkeerde 
      domeinnaam" en een "verwijderd" of "goed e-mail adres".</p>
<p>Wanneer u de e-mail status van een lid wijzigt, vergewis u alle onderstaande regels te begrijpen voor u verder gaat.</p>
    <ul style="font-size: 90%; background-color: #ffffee; border: 1px inset gray; padding: 0.5em 0 0.5em 1.5em;">
      <li class="blue_bullet">U moet dubbels bijhouden van alle e-mail waarop 
        u zich baseert om de wijziging te laten uitvoeren. Men kan u vragen bewijs 
        te leveren wanneer een lid klacht indient.</li>
      <li class="blue_bullet">Er staat een limiet op het aantal adressen die u 
        kunt verwerken. (Momenteel 100 per dag per categorie)</li>
      <li class="blue_bullet"">Adressen die u inzendt als zijnde "slecht" is e-mail 
        die naar u "terugkeert" omdat het een ongeldig adres betreft. Markeer 
        GEEN e-mail adres als slecht als het terugkeerde omdat hun mailbox vol 
        was. Slechte e-mail adressen worden als dusdanig gemarkeerd en zullen 
        in vele gevallen kort daarna volledig verwijderd worden.</li>
      <li class="blue_bullet">Adressen die u indient als "geblokkeerd" zijn bedoeld 
        voor e-mail die wordt geblokkeerd door de ISP van de ontvanger, dit gebeurt 
        omdat ze gebruik maken van 'black hole lists' zoals Spamhaus, RBL, SPEWS, 
        enz. Ze zullen ook als dusdanig gemarkeerd worden en u bent verantwoordelijk 
        om communicatie met deze leden te bewerkstelligen.</li>
      <li class="blue_bullet">U moet alle adressen die u indient als "goed" bevestigen 
        als werkende e-mail adressen. Wijzig GEEN slecht e-mail adres in een "goed" 
        voor u dit zelf hebt gecontroleerd en kunt beamen.</li>
      <li class="blue_bullet">Adressen die u indient voor "verwijdering" en Affiliate 
        naar ClubShop Rewards Member downgrades hebben als onmiddellijke gevolg 
        dat iedereen onder hen naar boven oprolt. (Nota: U moet alle verzoeken 
        tot verwijdering of downgrades bijhouden. U mag geen verwijderingen of 
        downgrades uitvoeren tenzij u dat via e-mail gevraagd werd.)</li>
      <li class="blue_bullet">Inzendingen worden gecontroleerd.</li>
    </ul>
<p>Door verder te gaan, bevestigt u de bovenstaande regels te begrijpen.</p>
<p class="nav_footer_brown"><a href="/cgi/vip/MemberModification.cgi">Ga naar de interface</a>.</p>
</div>

<br/><br/><br/><br/><br/><br/>





<hr align="left" width="100%" size="1" color="#A35912"/>

<br/><br/><br/><br/><br/><br/><br/><br/>

</div>

</div>

</div>



<!--Footer Container -->



<div class="container blue"> 

  <div class="row "> 

    <div class="twelve columns"> 

      <div class="push"></div>

    </div>



  </div>

  <div class="row "> 

    <div class="twelve columns centered"> 

      <div id="footer"> 

        <p>Copyright &#xA9; 1997-2018

          <!-- Get Current Year -->

          

          ClubShop Rewards, All Rights Reserved. </p>

      </div>



    </div>

  </div>



<!--End Footer Container -->



</div>



</body>

</html>