<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 
<xsl:template match="/">

<html>
<head>
<title>Understanding the Income Report</title>
<style type="text/css">

h1{ font-family: Arial, Helvetica, sans-serif;
font-size: medium;
font-weight: bold;
 }

body{ font-family: Arial, Helvetica, sans-serif;
 font-size: smaller;
  }
tr.a{ background-color: #F2FFF8; }

tr.b{ background-color: #FFFACD;}




</style>

</head>

<body>
<h1><xsl:value-of select="//lang_blocks/heading"/></h1>

<p><xsl:value-of select="//lang_blocks/minihead"/></p>

<table cellspacing="2" style="empty-cells:show" border="1">
<tr>
<th><a href="#role_link" target="_self"><xsl:value-of select="//lang_blocks/dh1"/></a></th>
<th><a href="#title_link" target="_self"><xsl:value-of select="//lang_blocks/dh2"/></a></th>
<th><a href="#PPP_link" target="_self"><xsl:value-of select="//lang_blocks/dh3"/></a></th>
<th><a href="#NPP_link" target="_self"><xsl:value-of select="//lang_blocks/dh4"/></a></th>
<th><a href="#po_link" target="_self"><xsl:value-of select="//lang_blocks/dh5"/></a></th>
<th><a href="#nc_link" target="_self"><xsl:value-of select="//lang_blocks/dh6"/></a></th>
<th><a href="#npo_link" target="_self"><xsl:value-of select="//lang_blocks/dh7"/></a></th>
<th><a href="#cc_link" target="_self"><xsl:value-of select="//lang_blocks/dh8"/></a></th>
<th><a href="#mm_link" target="_self"><xsl:value-of select="//lang_blocks/dh9"/></a></th>
<th><a href="#ni_link" target="_self"><xsl:value-of select="//lang_blocks/dh10"/></a></th>
</tr>
<tr class="a">
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
</tr></table>

<p></p>
<p><b><xsl:value-of select="//lang_blocks/dh1"/><a id="role_link"></a></b><xsl:value-of select="//lang_blocks/d1"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh2"/><a id="title_link"></a></b><xsl:value-of select="//lang_blocks/d2"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh3"/><a id="PPP_link"></a></b><xsl:value-of select="//lang_blocks/d3"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh4"/><a id="NPP_link"></a></b><xsl:value-of select="//lang_blocks/d4"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh5"/><a id="po_link"></a></b><xsl:value-of select="//lang_blocks/d5"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh6"/><a id="nc_link"></a></b><xsl:value-of select="//lang_blocks/d6"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh7"/><a id="npo_link"></a></b><xsl:value-of select="//lang_blocks/d7"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh8"/><a id="cc_link"></a></b><xsl:value-of select="//lang_blocks/d8"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh9"/><a id="mm_link"></a></b><xsl:value-of select="//lang_blocks/d9"/></p>
<p><b><xsl:value-of select="//lang_blocks/dh10"/><a id="ni_link"></a></b><xsl:value-of select="//lang_blocks/d10"/></p>



<h1><xsl:value-of select="//lang_blocks/heading2"/></h1>
<table cellspacing="2" style="empty-cells:show" border="1" >
<tr>
<th></th>
<th><xsl:value-of select="//lang_blocks/r1"/></th>
<th><xsl:value-of select="//lang_blocks/r2"/></th>
<th><xsl:value-of select="//lang_blocks/r3"/></th>
<th><xsl:value-of select="//lang_blocks/r4"/></th>
<th><xsl:value-of select="//lang_blocks/r5"/></th>
</tr>


<tr class="a">
<th><xsl:value-of select="//lang_blocks/sh1"/></th>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>

</tr>
<tr class="b">
<th><xsl:value-of select="//lang_blocks/sh2"/></th>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>

</tr></table>

<p><b><xsl:value-of select="//lang_blocks/sh1"/></b><xsl:value-of select="//lang_blocks/s1"/></p>
<p><b><xsl:value-of select="//lang_blocks/sh2"/></b><xsl:value-of select="//lang_blocks/s2"/></p>

<h1><xsl:value-of select="//lang_blocks/heading3"/></h1>
<table cellspacing="2" style="empty-cells:show" border="1" >
<tr>
<th></th>
<th><xsl:value-of select="//lang_blocks/dh3"/></th>
<th><xsl:value-of select="//lang_blocks/dh4"/></th>
<th><xsl:value-of select="//lang_blocks/dh5"/></th>
<th><xsl:value-of select="//lang_blocks/dh6"/></th>
<th><xsl:value-of select="//lang_blocks/dh7"/></th>
<th><xsl:value-of select="//lang_blocks/dh8"/></th>
<th><xsl:value-of select="//lang_blocks/dh9"/></th>
<th><xsl:value-of select="//lang_blocks/dh11"/></th>
</tr>
<tr class="a">
<th><xsl:value-of select="//lang_blocks/sh3"/></th>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>

</tr>

<tr class="b">
<th><xsl:value-of select="//lang_blocks/sh4"/></th>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>
<td>*</td>

</tr>
</table>
<p><b><xsl:value-of select="//lang_blocks/sh3"/></b><xsl:value-of select="//lang_blocks/s3"/></p>
<p><b><xsl:value-of select="//lang_blocks/sh4"/></b><xsl:value-of select="//lang_blocks/s4"/></p>
      
</body>
      </html>
   </xsl:template>
</xsl:stylesheet>