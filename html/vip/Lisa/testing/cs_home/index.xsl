<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "stylesheet02.xsl" />

<xsl:template match="/">
<html>
<head><title>ClubShop Help Desk</title>
<link rel="stylesheet" href="conv2006.css" type="text/css" />
<style type="text/css">
p{ font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
 font-size:smaller;}


 </style>
</head>
<body>
<table><tr><td class="shell" colspan="2"><xsl:call-template name="head" 

/></td></tr><tr>
<td class="shell"><xsl:call-template name="side" /></td><td class="main"><xsl:call-template name="caption" />
<!-- ######## variable content starts here ######### -->

<p><xsl:value-of select="//lang_blocks/p1" /></p>



<!-- ######## variable content stops here ######### -->
</td></tr></table></body></html>
</xsl:template>
</xsl:stylesheet>