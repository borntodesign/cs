<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="head">

<table width="100%" cellpadding="0">
  <tr> 
    <td width="22%" height="116" valign="top" bgcolor="#999900" id="mytd"><br/>DHS Club <br/>European<br/> Convention<br/> 2006<br/> May 5-7,2006</td>
    <td rowspan="2" valign="top" style="width: 54%; background-color: #bfb4a4" align="center">
 <img src="/conventions/conv2006/images/hotel_lobby.jpg" width="379" height="165" alt="hotel lobby" />
</td>
    <td width="24%" valign="top" bgcolor="#663366" align="center"><br/><img src="/conventions/conv2006/images/join.gif" width="120" height="120" alt="join us"/></td>
  </tr>
  <tr> 
    <td height="19" valign="top" bgcolor="#663366"></td>
    <td valign="top" bgcolor="#999900"></td>
  </tr>
</table>


</xsl:template>

<xsl:template name="side">

<h4><xsl:value-of select="//lang_blocks/lh1"/></h4>
<h4><xsl:value-of select="//lang_blocks/lh2"/></h4> 
<h4><xsl:value-of select="//lang_blocks/lh3"/></h4>
<h4><xsl:value-of select="//lang_blocks/lh4"/></h4> 
<h4><xsl:value-of select="//lang_blocks/lh5"/></h4>
<h4><xsl:value-of select="//lang_blocks/lh6"/></h4>


</xsl:template>

<xsl:template name="caption">
 <!--h2><xsl:value-of select="//lang_blocks/mh1"/></h2-->

</xsl:template>

</xsl:stylesheet>