<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "common.xsl" />
<xsl:param name="id" />
<xsl:param name="dn" />
   <xsl:template match="/">
    
         <html><head>
            <title>VIP Signup</title>
<link href="presentation.css" rel="stylesheet" type="text/css"/><link href="local.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<xsl:call-template name = "header" />
<table><tr>
<td id="sidelinks" valign="top">
<xsl:call-template name = "nav" />
</td>
    <td width="581" id="main">

<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<p><xsl:value-of select="//lang_blocks/p1"/></p>

<ul>
<p>
<b><img src="http://www.clubshop.com/av/thumb_barbour.gif" width="34" height="47" alt="cynthia barbour"/> <xsl:value-of select="//lang_blocks/p2"/></b> - <xsl:value-of select="//lang_blocks/p3"/>
"<xsl:value-of select="//lang_blocks/p4"/>"
</p>

<p>
<b><img src="http://www.clubshop.com/av/thumb_dindo.gif" width="33" height="30" alt="dindo barriga"/><xsl:value-of select="//lang_blocks/p9"/></b> - "<xsl:value-of select="//lang_blocks/p10"/>
$<xsl:value-of select="//lang_blocks/p11"/>."

</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_perotti.gif" width="32" height="39" alt="fabrizio perotti"/><xsl:value-of select="//lang_blocks/p12"/></b> - "<xsl:value-of select="//lang_blocks/p13"/>"
</p>

<p>
<b><img src="http://www.clubshop.com/av/thumb_ffinch.gif" width="32" height="32" alt="frank finch"/><xsl:value-of select="//lang_blocks/p14"/></b> - "<xsl:value-of select="//lang_blocks/p15"/>."
</p>



<p>
<b><img src="http://www.clubshop.com/av/thumb_gdescamps.gif" width="27" height="31" alt="gino descamps"/><xsl:value-of select="//lang_blocks/p16"/></b> - "<xsl:value-of select="//lang_blocks/p17"/>."

</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_hill.gif" width="29" height="33" alt="harold hill"/><xsl:value-of select="//lang_blocks/p18"/></b> - "<xsl:value-of select="//lang_blocks/p19"/>.
'<xsl:value-of select="//lang_blocks/p20"/>', <xsl:value-of select="//lang_blocks/p21"/>."
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_griffith.gif" width="35" height="24" alt="jim griffith"/><xsl:value-of select="//lang_blocks/p22"/></b> - "<xsl:value-of select="//lang_blocks/p23"/>
'<xsl:value-of select="//lang_blocks/p24"/>' <xsl:value-of select="//lang_blocks/p25"/>!"
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_marrow.gif" width="33" height="22" alt="lee marrow"/><xsl:value-of select="//lang_blocks/p29"/></b> - "<xsl:value-of select="//lang_blocks/p30"/>
."
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_walker.gif" width="23" height="29" alt="linda walker"/><xsl:value-of select="//lang_blocks/p31"/></b> - "<xsl:value-of select="//lang_blocks/p32"/>
"<xsl:value-of select="//lang_blocks/p33"/>" <xsl:value-of select="//lang_blocks/p34"/>! <xsl:value-of select="//lang_blocks/p35"/>!"
</p>

<p>
<b><img src="http://www.clubshop.com/av/thumb_chambers.gif" width="27" height="27" alt="louise chambers"/><xsl:value-of select="//lang_blocks/p36"/></b> - "<xsl:value-of select="//lang_blocks/p36a"/> '<xsl:value-of select="//lang_blocks/p36b"/>'
<xsl:value-of select="//lang_blocks/p37"/>!"
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_verdickt.gif" width="28" height="23" alt="manuela verdickt"/><xsl:value-of select="//lang_blocks/p38"/></b> - "<xsl:value-of select="//lang_blocks/p39"/> (<xsl:value-of select="//lang_blocks/p40"/>!)
<xsl:value-of select="//lang_blocks/p41"/>"
</p>

<p>
<b><img src="http://www.clubshop.com/av/thumb_griffin.gif" width="28" height="23" alt="paula griffin"/><xsl:value-of select="//lang_blocks/p42"/></b> - "<xsl:value-of select="//lang_blocks/p43"/>"
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_brown.gif" width="28" height="23" alt="pere brown"/><xsl:value-of select="//lang_blocks/p44"/></b> - "<xsl:value-of select="//lang_blocks/p45"/>?

<xsl:value-of select="//lang_blocks/p46"/>."
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_gustafsson.gif" width="25" height="24" alt="peter gustafsson"/><xsl:value-of select="//lang_blocks/p47"/></b> - "<xsl:value-of select="//lang_blocks/p48"/>

(<xsl:value-of select="//lang_blocks/p49"/>) <xsl:value-of select="//lang_blocks/p50"/>!"
</p>

<p>
<b><img src="http://www.clubshop.com/av/thumb_royaards.gif" width="26" height="22" alt="rene royaards"/><xsl:value-of select="//lang_blocks/p51"/></b> - "<xsl:value-of select="//lang_blocks/p52"/>
(<xsl:value-of select="//lang_blocks/p55"/>) <xsl:value-of select="//lang_blocks/p56"/>
'<xsl:value-of select="//lang_blocks/p57"/>'<xsl:value-of select="//lang_blocks/p58"/>
$<xsl:value-of select="//lang_blocks/p59"/>! <xsl:value-of select="//lang_blocks/p60"/>."
</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_bracco.gif" width="30" height="19" alt="robbie bracco"/><xsl:value-of select="//lang_blocks/p61"/></b> - "<xsl:value-of select="//lang_blocks/p62"/>"

</p>




<p>
<b><img src="http://www.clubshop.com/av/thumb_dufault.gif" width="33" height="24" alt="roland dufault"/><xsl:value-of select="//lang_blocks/p63"/></b> - "<xsl:value-of select="//lang_blocks/p64"/>"

</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_cunningham.gif" width="26" height="28" alt="scott cunningham"/><xsl:value-of select="//lang_blocks/p65"/></b> - "<xsl:value-of select="//lang_blocks/p66"/>"

</p>


<p>
<b><img src="http://www.clubshop.com/av/thumb_secord.gif" width="27" height="25" alt="scott secord"/><xsl:value-of select="//lang_blocks/p67"/></b> - "<xsl:value-of select="//lang_blocks/p68"/>!
<xsl:value-of select="//lang_blocks/p69"/>!"
</p>


</ul>

<h1 align="center"><xsl:value-of select="//lang_blocks/p70"/></h1>
<p align="center"><a>   <xsl:attribute name="href"><xsl:value-of select="document('nav.xml')//teams/a/@href" />?flg=dv&amp;sponsorID=<xsl:choose><xsl:when test="$id"><xsl:value-of select="$id" /></xsl:when><xsl:otherwise>1</xsl:otherwise></xsl:choose></xsl:attribute><img src="http://www.clubshop.com/ca/ca_logo_250x37.gif"/></a></p>


</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>
