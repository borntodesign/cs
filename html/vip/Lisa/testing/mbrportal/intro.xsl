<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href = "common.xsl" />
<xsl:param name="id" />
<xsl:param name="dn" />

   <xsl:template match="/">
    
         <head>
            <title>VIP Signup</title>
<link href="/css/autovip.css" rel="stylesheet" type="text/css"/>
<xsl:if test="$id"><link rel="stylesheet" type="text/css">
<xsl:attribute name="href">/cgi/setck.cgi?id=<xsl:value-of select="$id" /></xsl:attribute>
</link></xsl:if>
</head>
<body>
<xsl:call-template name = "header" />
<table><tr>
<td id="sidelinks" valign="top">
<xsl:call-template name = "nav" />
</td>
    <td width="581" id="main" valign="top">

<h5 align="center"><xsl:value-of select="//lang_blocks/p0"/></h5>
<div><xsl:value-of select="//lang_blocks/pa"/> - <xsl:value-of select="//lang_blocks/pb"/>:</div>
<ul>

<li><xsl:value-of select="//lang_blocks/p1"/></li>
<li><xsl:value-of select="//lang_blocks/p2"/></li>
<li><xsl:value-of select="//lang_blocks/p3"/></li> 
<li><xsl:value-of select="//lang_blocks/p4"/></li>
<li><xsl:value-of select="//lang_blocks/p5"/></li>
<li><xsl:value-of select="//lang_blocks/p6"/></li> 
</ul>

<p><xsl:value-of select="//lang_blocks/p7"/> (<xsl:value-of select="//lang_blocks/p8"/>).</p>

<p><xsl:value-of select="//lang_blocks/p10"/></p>

<p><xsl:value-of select="//lang_blocks/p11"/></p>
<h2><xsl:value-of select="//lang_blocks/p12"/><br/><xsl:value-of select="//lang_blocks/p13"/></h2>



</td></tr></table>






</body>
</xsl:template>
</xsl:stylesheet>
