<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>


	<xsl:param name="referer" />

<xsl:template match="/">

<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VIP SignUp</title>
<link href="av.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table id="container">
<tr><td colspan="2" id="banner" style="text-align:left">
<img src="header.gif" alt="" width="694" height="100" />
</td></tr>
<tr>
<td id="sidelinks">
<h4>Links</h4>
<h4><a><xsl:attribute name="href">index.xsp?referer=<xsl:call-template name = "referer" /></xsl:attribute>Introduction</a>
</h4>
<h4><a><xsl:attribute name="href">howitworks.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>Company Information</a>
</h4>
<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>Business Plan</a>
</h4>
<h4><a><xsl:attribute name="href">/cgi/appx.cgi/<xsl:call-template name = "referer" /></xsl:attribute>Marketing Plan</a>
</h4>
<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>Compensation Plan</a>
</h4>
<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>VIP ClubAdvantage Benefits</a>
</h4>

<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>VIP Testimonials</a>
</h4>

<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>Bonus and Guarantee</a>
</h4>
<h4><a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>VIP ClubAdvantage Application</a>
</h4>
</td>
<!--end heading and Nav-->

<td style="width: 581px" id="main">
<!--tdmain-->

<xsl:copy-of select = "*/htm/."/>

<!--end td main-->

</td></tr></table>

</body></html>

</xsl:template>


<xsl:template name="referer">
<xsl:choose>
	<xsl:when test="*/referer/id">
	
	<xsl:value-of select = "*/referer/id" />
	</xsl:when>
  
	<xsl:otherwise>
	<xsl:value-of select="$referer"/>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>


