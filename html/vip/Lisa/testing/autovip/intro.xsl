<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

   <xsl:template match="/">
    
         <head>
            <title>VIP Signup</title>
<link href="/css/autovip.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<h5><xsl:value-of select="//lang_blocks/p0"/></h5>
<p><xsl:value-of select="//lang_blocks/p1"/> - <xsl:value-of select="//lang_blocks/p2"/></p>

<p><xsl:value-of select="//lang_blocks/p3"/> - <xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p4"/> - <xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p5"/> - <xsl:value-of select="//lang_blocks/p2"/></p>
<p><xsl:value-of select="//lang_blocks/p6"/> - <xsl:value-of select="//lang_blocks/p2"/></p>

<p><xsl:value-of select="//lang_blocks/p7"/> - <xsl:value-of select="//lang_blocks/p8"/><xsl:value-of select="//lang_blocks/p9"/></p>

<p><xsl:value-of select="//lang_blocks/p10"/></p>

<p><xsl:value-of select="//lang_blocks/p11"/></p>
<h2><xsl:value-of select="//lang_blocks/p12"/><br/><xsl:value-of select="//lang_blocks/p13"/></h2>










</body>
</xsl:template>
</xsl:stylesheet>