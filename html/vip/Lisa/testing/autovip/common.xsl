<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template name="header">
<div><img src="/images/autoviphead.gif" alt="" height="86" width="733"/></div>
</xsl:template>


<xsl:template name="nav">
<xsl:for-each select = "document('nav_autovip.xml')//teams/leader/*">
		<a><xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
<xsl:value-of select="." /></a><br />
</xsl:for-each>

<a>
   <xsl:attribute name="href"><xsl:value-of select="document('nav_autovip.xml')//teams/a/@href" />?sponsorID=<xsl:choose><xsl:when test="$id"><xsl:value-of select="$id" /></xsl:when><xsl:otherwise>1</xsl:otherwise></xsl:choose></xsl:attribute>
   <xsl:value-of select="document('nav_autovip.xml')//teams/a" /></a>
</xsl:template>
</xsl:stylesheet>