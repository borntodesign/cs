/* teampages.js
a collection of do-dads to make the teampages work */

function body_load()
{	// a handler we can adjust as needed
	hideUplineDetails();
}

// this function hides the email and personal web page links for the Upline Coaches
// if they have javascript shut off, they get to see it all right up front
//var dlist = new Array('rx','r140','r130','r120','r115','r110','r100');
var dlist = new Array('rx','r4','r11');

var me;
function hideUplineDetails(){
  if (document.getElementById)
  {
	for (var item=0; item < dlist.length; item++){
  		me = document.getElementById(dlist[item]);

		// some of these levels are not available yet
		if (me != null){ me.style.display='none' }
	}
  }
}

function toggleUplineDetails(myid)
{
// let's close any open ones
	for (var item=0; item < dlist.length; item++){
		var me = document.getElementById(dlist[item]);

		if (dlist[item] == myid){
			if (me.style.display == 'block'){ me.style.display='none' }
			else { me.style.display='block' }
		}else{

		// some of these levels are not available yet
			if (me != null){ me.style.display='none' }
		}
	}
}