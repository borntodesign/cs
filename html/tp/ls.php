<?php

include '/home/httpd/php-includes/Get_local_url.php';

$id = $_GET['id'];
if (preg_match('/\D/', $id)) {
    $id = '' ;
}

$xml = "<root><page_owner><id>$id</id></page_owner>";

$xsp = GetRelativeURI('master.xml');
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);

$myid = isset($_COOKIE['id']) ? $_COOKIE['id'] : '';
$xml .= file_get_contents("http://192.168.250.1/cgi/rpc.cgi?proc=ls_xsp;leader_id={$id};id={$myid}");
$xml .= '</root>';

//error_log($xml);

$xsl = file_get_contents('team-pages-l.xsl');

$xslDD = new DOMDocument();
$xslDD->loadXML($xsl);
$processor = new XsltProcessor();
$processor->importStyleSheet($xslDD);

$xmlDD = new DOMDocument();
$xmlDD->loadXML($xml);

$rv = $processor->transformToDoc($xmlDD);
// if our path is merely /tp/ then we are rendering HTML
if (preg_match('/\/tp\//', $_SERVER['PHP_SELF'])) {
    echo $rv->saveHTML();
}
else {
    // otherwise we are converting the result to javascript
    $rv = preg_replace('/^.*<body.*?>|<\/body.*/s', '', $rv->saveHTML());
    $rv = preg_replace('/"/s', '\\"', $rv);
    $rv = preg_replace('/\n/', '\\\n', $rv);
    
    header('Content-type: text/javascript; charset=UTF-8');
//    echo 'doing conversion'.PHP_EOL;
    echo "document.write(\"$rv\");";
}