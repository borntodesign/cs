<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes" 
	media-type="text/html" />

<xsl:variable name="id" select="/root/page_owner/id" />

<xsl:include href="../global-xml/roles/roles.xsl" />

<!-- Our starting point where we'll call the separate components -->
<xsl:template match="/">

	<html><head><title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript" src="//www.clubshop.com/tp/teampages.js"></script>

	<link type="text/css" href="/tp/teampage.css" rel="stylesheet" />
	</head><body onload="body_load()">

<!-- our first section is our list of upline coaches -->
	<div class="col_heading"><xsl:value-of select="//lang_blocks/coaches" /></div>

	<xsl:choose>
		<xsl:when test="//error">
	<!-- we could just as easily silently fail and not produce any output for this section -->
			<div class="col_description">Couldn't make a database connection.<br />
			<a target="_top"><xsl:attribute name="href">
			<xsl:value-of select="//error" /></xsl:attribute><xsl:value-of select="//lang_blocks/errors/more_info" /></a>
			</div>
		</xsl:when>

		<xsl:otherwise>

			<xsl:call-template name="my_coaches" />
		
	<!-- we put our Exec here at the bottom of the list -->
			<xsl:for-each select = "*/team_leader">
				<xsl:call-template name="upline_headings">
					<xsl:with-param name="rx">rx</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="upline_links">
					<xsl:with-param name="id">rx</xsl:with-param>
				</xsl:call-template>

			</xsl:for-each>	
		</xsl:otherwise>
	</xsl:choose>
<!-- Done list of upline coaches -->

<!-- Our Dream Team -->
	<p></p><div class="col_heading"><xsl:value-of select="//lang_blocks/dream_team" /></div>
	
	<div class="col_description"><xsl:value-of select="concat(//lang_blocks/coaches_blurb-p1, ' ', //team_leader/lastname, ' ', //lang_blocks/coaches_blurb-p2)" />.</div>
	
	<div class="fp_gray_bg">
	<xsl:for-each select = "document('../vip/team.xml')//team_leader[@id=$id]/*/member">
		<xsl:if test="position() &gt; 1">, </xsl:if>
		<xsl:value-of select="concat(firstname, ' ', lastname)" />
	</xsl:for-each>
	</div>
<!-- End of Dream Team list -->

<!-- ClubNews! -->
	<p></p><div class="col_heading"><xsl:value-of select="//lang_blocks/clubnews" /></div>
	<p></p>
	<div><a target="_top" href="//www.clubshop.com/weeklyspecial.html"><xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/weekly_special" /></xsl:attribute><img src="//www.clubshop.com/images/mpp/weeklyspecial.gif" width="160" height="150" class="wsp" /></a>

	<!-- add other stuff in here as desired for the ClubNews section -->
	</div>
<!-- END of ClubNews! -->

	</body></html>
</xsl:template>
<!-- ######################### END OF MAIN CONTAINER ##################### -->

<!-- ######################### My Coaches ##################### -->
<xsl:template name="my_coaches">
	<xsl:choose>
	<xsl:when test="not(//user/id)">
<!-- We could silently fail here as well and just not output anything.
	Or we could provide a login link. In that case we would need to consider the Team Page as a whole and how we would refresh it in the case of iframes or a direct javascript write. -->
		<div>Not logged IN</div>
	</xsl:when>

	<xsl:otherwise>

		<xsl:choose>
		<xsl:when test="//upline_coaches/wrong_team">
		<!-- We could put anything in here,.... or nothing -->
			<div class="col_description"><xsl:value-of select="//lang_blocks/errors/wrong_team" />
			</div>
		</xsl:when>
  
		<xsl:otherwise>
			<xsl:for-each select = "//upline_coaches/*">
				<xsl:call-template name = "upline_headings" />
				<xsl:call-template name = "upline_links">
					<!-- xsl:with-param name="id">r<xsl:value-of select="role" /></xsl:with-param-->
					<xsl:with-param name="id">rx</xsl:with-param>
				</xsl:call-template>
			</xsl:for-each>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:otherwise></xsl:choose>
</xsl:template>
<!-- ######################### End of My Coaches ##################### -->

<!-- ##################### Create the 'heading' blocks for our COACHES ##############-->
<xsl:template name="upline_headings">
<xsl:param name="rx" />
<!-- the context node has all the data we need except for our call to get the role specifics -->
	<xsl:variable name="code">
		<xsl:choose>
			<xsl:when test="not ($rx) and role">	<!--  this section is bogus as it is relying on the old roles stuff -->
				<xsl:call-template name="get_role" >
					<xsl:with-param name="level"><xsl:value-of select="role" /></xsl:with-param>
					<xsl:with-param name="_node">code</xsl:with-param>
     		   	</xsl:call-template>
       		</xsl:when>
       		<xsl:otherwise>
        		<xsl:value-of select="//lang_blocks/exec_abbrev" />
       		</xsl:otherwise>
       	</xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="role"><xsl:choose>
		<xsl:when test="not ($rx) and role != ''">r<xsl:value-of select="role" /></xsl:when>
        <xsl:otherwise>rx</xsl:otherwise>
        </xsl:choose></xsl:variable>
    
	<div>
		<xsl:attribute name="class">
			<xsl:choose>
			  <xsl:when test="role">coach_heading</xsl:when>
			  <xsl:otherwise>exec_heading</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		
		<xsl:choose>
			<xsl:when test="not ($rx) and role">
				<a class="coach_lbl" href="//www.clubshop.com/tp/roles.xml" target="_blank" onclick="window.open(this.href, 'roles', 'height=450,width=600,scrollbars=yes,resizable=yes,status=no'); return false;">
				<xsl:attribute name="title">
					<xsl:call-template name="get_role" >
					<xsl:with-param name="level"><xsl:value-of select="role" /></xsl:with-param>
					<xsl:with-param name="_node">responsibility</xsl:with-param>
					</xsl:call-template></xsl:attribute>
				<xsl:value-of select="$code" /></a> : 
			</xsl:when>
  
			<xsl:otherwise>
				<span class="coach_lbl"><xsl:value-of select="$code" /></span>:<br /><span style="margin-left: 2em"></span>
			</xsl:otherwise>
		</xsl:choose> 
		
		<a href="#">
			<xsl:attribute name="onclick">toggleUplineDetails('<xsl:value-of select="$role" />'); return false;</xsl:attribute>
			<xsl:attribute name="title"><xsl:value-of select="//lang_blocks/coach_anchor_title" /></xsl:attribute>
			<xsl:value-of select="concat(firstname, ' ', lastname)" />
		</a>
	</div>
</xsl:template>
<!-- ##################### END of 'heading' blocks for our COACHES ##############-->

<!-- ##################### Create the 'detail' boxes for our COACHES ################-->
<xsl:template name="upline_links">
<xsl:param name="id" />
<div class="upline_detail">
<xsl:attribute name="id"><xsl:value-of select="$id" /></xsl:attribute>
<a><xsl:attribute name="href">mailto:<xsl:value-of select="emailaddress" /></xsl:attribute><xsl:value-of select="//lang_blocks/email_me" /></a>
</div>
</xsl:template>
<!-- ##################### END 'detail' boxes for our COACHES ################-->

</xsl:stylesheet>
