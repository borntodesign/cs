<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes" 
	media-type="text/html" />

<xsl:variable name="docroot" select="'/home/httpd/html/tp/'" />
<xsl:variable name="id" select="/root/page_owner/id" />

<!-- Our starting point where we'll call the separate components -->
<xsl:template match="/">

	<html><head><title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" href="/tp/teampage.css" rel="stylesheet" />
	<style type="text/css">
		html, body
  		{
			border: none;
			overflow: auto;
		}
	</style>
	</head><body>

<!-- New VIPs this Month -->
	<div class="col_heading"><xsl:value-of select="//lang_blocks/new_vips" /></div>
	<xsl:call-template name = "newvips" />
<!-- END of New VIPs this Month -->

<!-- Newest Promotions ... that should be pay levels -->
	<p></p>
	<div class="col_heading"><xsl:value-of select="//lang_blocks/new_promos" /></div>
	<div class="col_description"><xsl:value-of select="//lang_blocks/new_promos_blurb" /></div>
	<div class="fp_gray_bg">
	
	<xsl:for-each select = "document('new-promos.xml')//team_leader[@id=$id]/member">
		<xsl:value-of select="concat(firstname, ' ', substring(lastname,1,1))" /><br />
	</xsl:for-each>
	</div>
<!-- END of Newest Promotions -->

<!-- HQ Meetings schedule -->
	<p></p>
	<div class="col_heading"><xsl:value-of select="//lang_blocks/hq_meetings" /></div>
	<p class="default"><a target="_top" href="http://www.clubshop.com/vip/manual/training/interactive_online_schedule.html"><xsl:attribute name="title"><xsl:value-of select="//lang_blocks/paltalk_sessions" /></xsl:attribute><xsl:value-of select="//lang_blocks/paltalk_sessions" /></a></p>

	</body></html>
</xsl:template>
<!-- ######################## END OF MAIN HTML TEMPLATE ################## -->

<!-- ##### our New VIPs for the Month section ##### -->
<xsl:template name="newvips">
	<table class="new_vips">

	<xsl:for-each select = "document('_newvips_.xml')//tr">
		<xsl:if test = "leader=$id">
			<tr><td class="name"><xsl:value-of select="concat(td[@class='firstname'], ' ', substring(td[@class='lastname'],1,1))"/></td>
				<xsl:copy-of select = "td[@class='country']"/></tr>

			<tr><td colspan="2" class="builder">Sponsor: <xsl:value-of select="td[@class='sponsor']"/></td></tr>

		</xsl:if>
	</xsl:for-each>

	</table>
</xsl:template>

</xsl:stylesheet>
