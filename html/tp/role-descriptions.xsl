<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes" 
	media-type="text/html" />

<xsl:template match="/">

	<html><head><title><xsl:value-of select="//lang_blocks/title" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		body
  		{
  			background-color: white;
			border: none;
			font-family: sans-serif;
			font-size: 80%;
		}
		.code {
			background-color: #ccc;
			padding: 0.2em 0.1em 0.2em 0.2em;
			font-weight: bold;
			font-size: 90%;
		}
		.resp {
			background-color: #eee;
			padding: 0.5em 2em;
			font-weight: normal;
			font-size: 100%;
		}
	</style>
	</head><body>
	<h3><xsl:value-of select="//lang_blocks/title" /></h3>

	<xsl:for-each select = "//lang_blocks/role[@value &gt;= 100]">
		<p></p>
		<div class="code"><xsl:text> </xsl:text><xsl:value-of select="@code" />: <xsl:value-of select="name" />
		<div class="resp"><xsl:value-of select="responsibility" /></div></div>
	</xsl:for-each>
	<p style="font-size: 85%"><a href="javascript:self.close()">Close Window</a>.</p>
	</body></html>
</xsl:template>

</xsl:stylesheet>