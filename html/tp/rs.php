<?php

include '/home/httpd/php-includes/Get_local_url.php';

$id = $_GET['id'];
if (preg_match('/\D/', $id)) {
    $id = '' ;
}

$xml = "<root><page_owner><id>$id</id></page_owner>";

$xsp = GetRelativeURI('master.xml');
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);

//$xml .= GetURI("/cgi/rpc.cgi?proc=memberinfo_by_ck;Cookie={$_COOKIE['AuthCustom_Generic']}");
$xml .= '</root>';

//error_log($xml);

$xsl = file_get_contents('team-pages-r.xsl');

$xslDD = new DOMDocument();
$xslDD->loadXML($xsl);
$processor = new XsltProcessor();
$processor->importStyleSheet($xslDD);

$xmlDD = new DOMDocument();
$xmlDD->loadXML($xml);

$rv = $processor->transformToDOC($xmlDD);
// if our path is merely /tp/ then we are rendering HTML
if (preg_match('/\/tp\//', $_SERVER['PHP_SELF'])) {
    echo $rv->saveHTML();
}
else {
    // otherwise we are converting the result to javascript
    $rv = preg_replace('/^.*<body.*?>|<\/body.*/s', '', $rv->saveHTML());
    $rv = preg_replace('/"/s', '\\"', $rv);
    $rv = preg_replace('/\n/', '\\\n', $rv);
    
    header('Content-type: text/javascript; charset=UTF-8');
//    echo 'doing conversion'.PHP_EOL;
    echo "document.write(\"$rv\");";
}

?>