<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><xsl:value-of select="//lang_blocks/p1"/></title>

     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="//www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>


<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>
	<div align="right"><a href="#" class="nav_footer_grey" onclick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a></div>
	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">


<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p2"/></b>________________________________</td>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p3"/></b>________________________________</td>
    
  </tr>
  
    <tr>
    <td colspan="2" bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p4"/></b>________________________________<br/><span class="style2"><xsl:value-of select="//lang_blocks/p5"/></span></td>
    <!--<td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p6"/></b>________________________________<br/><span class="style2"><xsl:value-of select="//lang_blocks/p7"/></span></td>-->
    
  </tr>

<tr>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p8"/></b>__________________________<br/></td>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p10"/></b>________________________________</td>
    
     
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p21"/></b>__________________________%<br/></td>
   
    
     
  </tr>
  
  
  
  <tr><td bgcolor="#EFEFEF" colspan="2"><b><xsl:value-of select="//lang_blocks/p11"/></b></td></tr>
  
  <tr><td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p22"/></b>___________</td></tr>
  <tr><td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p23"/></b>___________</td></tr>
  <tr><td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p24"/></b>___________</td></tr>  
  <tr><td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p12"/></b>___________<xsl:text> </xsl:text><a href="/manual/forms/wirerequest.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p13"/></a></td></tr>
  
  <!-- <tr>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p14"/></b></td>
    <td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p15"/>__________________________</td>
    
     
  </tr>
  
   
   <tr>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p19"/></b></td>
    <td bgcolor="#FFFFFF"><xsl:value-of select="//lang_blocks/p20"/>______________________________</td>
    
     
  </tr>-->
  <tr><td colspan="2" bgcolor="#FFFFFF">
  <p><xsl:value-of select="//lang_blocks/dis"/></p></td>
  
  </tr>

  <tr align="center" height="100">
  <br/><br/> <br/>
  <td  bgcolor="#FFFFFF">_________________________<br/><i><xsl:value-of select="//lang_blocks/p17"/></i></td>
  <td  bgcolor="#FFFFFF">_________________________<br/><i><xsl:value-of select="//lang_blocks/p18"/></i></td>
  </tr>
  



</table>


<!-- end page text here-->



<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>