
/***********************************************
* Ultimate Fade In Slideshow v2.0- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/


var mygallery=new fadeSlideShow({
	wrapperid: "fadeshow1", //ID of blank DIV on page to house Slideshow
	dimensions: [946, 174], //width/height of gallery in pixels. Should reflect dimensions of largest image
	imagearray: [
		["../../../images/italy/naples/slides/clubshop_rewards_italy_1.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_2.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_3.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_4.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_5.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_6.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_7.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_8.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_9.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_10.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_11.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"],
		["../../../images/italy/naples/slides/clubshop_rewards_italy_12.png", "", "_parent", "ClubShop Rewards, Napoli, Italia- RISPARMIA OFFLINE E ONLINE IN TUTTO IL MONDO!"] //<--no trailing comma after very last image element!
	],
	displaymode: {type:'auto', pause:5000, cycles:0, wraparound:false},
	persist: false, //remember last viewed slide and recall within same session?
	fadeduration: 500, //transition duration (milliseconds)
	descreveal: "ondemand",
	togglerid: ""
})
