// Bill: I don't remember where this originally came from, but it had issues, probably related to not applying it properly
// 04/18/14 I revised it to use position() to get the coordinates as they related to the containing element
// since the span tag inside the anchor tag will be positioned in relation to that instead of the body
// the original write of this probably worked fine for an element positioned off of html, but the application of that has been lost

$(document).ready(function() {
			//Tooltips
			$(".tip_trigger").hover(function(){
				tip = $(this).find('.tip');
				tip.show(); //Show tooltip
			}, function() {
				tip.hide(); //Hide tooltip
			}).mousemove(function(e) {
//				var mousex = e.pageX + 20; //Get X coodrinates
//				var mousey = e.pageY + 20; //Get Y coordinates
				var position = $(e.target).position();
				var mousex = position.left + 20; //Get X coodrinates
				var mousey = position.top + 20; //Get Y coordinates
				
				var tipWidth = tip.width(); //Find width of tooltip
				var tipHeight = tip.height(); //Find height of tooltip
//alert('mousex:'+mousex+'  mousey:'+mousey+'  tipWidth:'+tipWidth+'  tipHeight:'+tipHeight+'  window-width:'+$(window).width());
				//Distance of element from the right edge of viewport
				var tipVisX = $(window).width() - (mousex + tipWidth);
				//Distance of element from the bottom of viewport
				var tipVisY = $(window).height() - (mousey + tipHeight);

				if ( tipVisX < 20 ) { //If tooltip exceeds the X coordinate of viewport
					mousex = e.pageX - tipWidth - 20;
				} if ( tipVisY < 20 ) { //If tooltip exceeds the Y coordinate of viewport
					mousey = e.pageY - tipHeight - 20;
				}
				//Absolute position the tooltip according to mouse position
				tip.css({  top: mousey, left: mousex });
			});
		});	