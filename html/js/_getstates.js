var preserve = new Array();
var select_prompt = '- Select a State/Province -';	// our default
var _getStates = {
	select_id	: 'state_select',
	field_name	: 'state',
	text_id		: 'state_text',
	country_id	: 'country',
	setValue	: function(name, val){
		if (typeof val != 'null' && typeof val != 'undefined'){
			this[name] = val;
		}
	},
	backgroundColor : 'yellow',
	cache		: {}
};

function adjustInitialStateCtlFocus(my_field_name)
{
	_getStates.setValue('field_name', my_field_name);
	
// if the application has created a prepopulated state_select control,
//then we'll display that instead of the default state_text
	var x= new getObj(_getStates.select_id);
	if (x.obj.options.length <= 1) return;
	x= new getObj(_getStates.text_id);
	
	turnON(_getStates.select_id);
	turnOFF(_getStates.text_id);
}

function getObj(name)
{
	if (_getStates.cache[name]){
		return _getStates.cache[name];
	}
alert('name='+name);
	if (document.getElementById)
	{
		this.obj = document.getElementById(name);
		this.style = document.getElementById(name).style;
	}
	else if (document.all)
	{
		this.obj = document.all[name];
		this.style = document.all[name].style;
	}
	alert('setting cache');
	_getStates.cache[name] = this;
	return _getStates.cache[name];
}

function getStates(my_field_name)
{
// preserve the focus states in case they press the reset button
	preserveFocus();

// field_name is the form name for the 'state' field
// it will default to 'state'
	_getStates.setValue('field_name', my_field_name);

// figure out what country is currently selected
	var x= new getObj(_getStates.country_id);
	var country = x.obj.options[x.obj.selectedIndex].value;
	if (country == '')
	{
		needs_country(x);
	}
// since we should have been called by an onchange event,
// we'll reset the background color if it was changed
	else { resetCountryColor() }

// if we don't have a valid array defined for the country, we'll revert to the text field
	if (typeof sc[country] == 'null' || typeof sc[country] == 'undefined')
	{
		turnON(_getStates.select_id);
		turnOFF(_getStates.text_id);
		return;
	}

	x= new getObj(_getStates.select_id);
// this reinitializes the list
	x.obj.options.length=0;
// insert our prompt as line 1
	x.obj.options[0] = new Option(select_prompt, '');

	for (var y=0; y < sc[country].length; y++)
	{
		x.obj.options[y+1] = new Option(sn[country][y],sc[country][y]);
	}

	turnON(_getStates.select_id);
	turnOFF(_getStates.text_id);
}

function needs_country(x)
{
// if there is no country value defined, then we surely cannot look up any states
// the idea is to call this function on focus of the 'state' text field
	if (typeof x == 'undefined') x = new getObj(_getStates.country_id);

// we could raise an alert box, but instead we'll just shift focus back
// this way we can avoid having to provide translatable alerts
	if (x.obj.options[x.obj.selectedIndex].value == '')
	{
		x.obj.focus();

	// save the existing background color
		preserve['country'] = x.obj.style.backgroundColor;
		x.obj.style.backgroundColor = _getStates.backgroundColor;
	}
}

function preserveFocus()
{
// if we have initialized values, then we are done
	if (typeof preserve['state_text_display'] != 'undefined') return;
	
	var x = new getObj(_getStates.text_id);
	preserve['state_text_display'] = x.style.display;
	preserve['state_text_name'] = x.obj.name;
	
	x = new getObj(_getStates.select_id);
	preserve['state_select_display'] = x.style.display;
	preserve['state_select_name'] = x.obj.name;
}

function restoreStateFocus()
{
// the reset will not bring the proper control into focus, it will only restore values
// if we don't have initialized values, then we are done
	if (typeof preserve['state_text_display'] == 'undefined') return;

	var x = new getObj(_getStates.text_id);
	x.style.display = preserve['state_text_display'];
	x.obj.name = preserve['state_text_name'];
	
	x = new getObj(_getStates.select_id);
	x.style.display = preserve['state_select_display'];
	x.obj.name = preserve['state_select_name'];
}

function resetCountryColor()
{
	if (typeof preserve['country'] == 'undefined') return;
	var x = new getObj(_getStates.country_id);
	x.obj.style.backgroundColor = preserve['country'];
}

function setSelectPrompt(prmpt)
{
	if (typeof prmpt == 'undefined') return;
	select_prompt = prmpt;
}

function turnON(ctl_id)
{
	var x = new getObj(ctl_id);
	x.style.display='inline';
	x.obj.name = _getStates.field_name;
}

function turnOFF(ctl_id)
{
	var x = new getObj(ctl_id);
	x.style.display='none';
	x.obj.name = null;
}
