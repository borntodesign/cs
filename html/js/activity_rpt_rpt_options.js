/*
activity_rpt.js
routines for use with the VIP activity_rpt
*/

function _do_all(flg)
{
	for(var i=0; i<document.forms[0].elements.length; i++)
	{
		document.forms[0].elements[i].checked = flg;
	}
	return false;
}

function _action(_action_)
{
	document.forms[0].action.value = _action_;
}

function bodyLoad()
{
	if (window.opener){
		var pwin = unescape(window.opener.location.pathname);
		if (pwin.indexOf('mem-cpl.html') > -1){ window.opener.location.href = pwin; }
	}
}

// this section is for creating the option/s when one clicks on the ID at the activity reports

var hdr = "<html><head><title></title></head><script type=\"text/javascript\">function loadReport(rpturl){window.open(rpturl,'_blank');self.close();}</script>\n"
hdr += "<style type=\"text/css\"><!--\nbody{font-size: 11px;}\n--></style></body>\n";
var ftr = "</body></html>";
var anchor_start = "<a href=\"javascript:void(0)\" onclick=\"loadReport('";
var line_rpt = "/cgi/line_rpt.cgi?mid=";
var line_rpt_end = "')\">Upline Report</a><br /><br />";
var md = "/cgi/member_detail.cgi?refid=";
var md_end = "')\">Member Detail</a>";

function presentOptions(mid,spid)
{
   // if they are the sponsor or at least Builder Intern role level, then we'll give them the option
   // the applications will still validate this, but we'll avoid unnecessary calls to the application
	if (spid == meid || myrole >= 40)
	{
		// display a little options window
		var nuwin= window.open('', 'reportoptions', 'height=100,width=250');
		nuwin.document.open();
		nuwin.document.write(hdr + anchor_start + line_rpt + mid + line_rpt_end + anchor_start + md + mid + md_end + ftr);
		nuwin.document.close();
	}
	else
	{
		window.open(line_rpt + mid);
	}
}
