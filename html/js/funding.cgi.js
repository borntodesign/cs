/* this script was developed way back before jquery and used Yahoo's framework
 * as of Feb, 2015, we are going to use some jquery, but only in new functions that will be called by pages that load jquery
 * it would be way too much bother at this point to rework all the screens that use this script to work with jquery
 */

// a global added after deployment (hopefully it won't clash with another of the same name)
var submitted;

// these first three functions are/were used with PayPal
function initMain(){
	window.name = 'funding';
	document.forms[0].amount.focus();
}

function initConfirm(frm){
	window.name = 'confirm';	/*opera will block popups but open the window in another tab
									so it may not get the name like it is supposed to*/
// I've had it with popup blockers and jumping through hoops to determine the name of the 'opener'
// which is not really the opener if the popup was blocked
	document.getElementById(frm).target = 'funding';
}

function openConfirm(frm){
	if (window.open('','confirm','width=400,height=200,left=100,top=100'))
	{
		return true;
	} else {
		frm.target = window.name;
		setTimeout('frm.submit()',10);
		return false;
	}
}

// this is the main statement init
function init(){
	var Yobj = YAHOO.util.Event;
	Yobj.addListener(window, 'focus', ReLoad);
}

// used to pre-validate the form submission for step 1 of a disbursement request
// all of this was previously embedded in the form onsubmit action with XSL tags pulling the text
// now the XSL is pulling the text into the javascript vars within the document
function ckPayoutFormStep1(frm){
	if (submitted) {
		alert("You have already submitted this request. Please wait....");
		return false;
	} else {
		submitted = CheckRequired('0','amount', xml_cashout, xml_empty_field);
		if (submitted) {
			submitted = CheckDollars('0', 'amount', xml_cashout, xml_bad_amount);
			if (submitted) {
				submitted = CheckMinimum('0', 'amount', xml_cashout, xml_payout_minimum, '5.00');
			}
		}
	// now check to see that they have submitted a payment option
		if (submitted){
			var opts = document.getElementById('payout_types').getElementsByTagName('input');
			for (var x=0; x < opts.length; x++){
				if (opts[x].checked){
					submitted = true;
					break;
				} else {
					submitted = false;
				}
			}
			if (! submitted){ alert(xml_cashout_a); }
		}
		return submitted;
	}
}

// keep them from submitting a search for a range of dates when they have omitted a drop-down
function checkDateRanges(frm){
	if (document.getElementById('range').checked == true){
		if (
			frm.start_mon.selectedIndex == 0 ||
			frm.stop_mon.selectedIndex == 0 ||
			frm.start_year.selectedIndex == 0 ||
			frm.stop_year.selectedIndex == 0
		){
			alert(dateRangeError);
			return false;
		}
	}
	return true;
}
