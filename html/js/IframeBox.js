var $YuD = YAHOO.util.Dom;

function hideIframeBox(){
	$YuD.get('iframe_box').style.visibility = 'hidden';
// this causes the iframe to reload to empty so that if they click on another report, the previous contents are no longer there
	self.frames['rpt_iframe'].location.href = 'https://www.clubshop.com/_empty_';
}
function showIframeBox(){
	var bx = document.getElementById('iframe_box');
	bx.style.top = ($YuD.getDocumentScrollTop() + 10) + 'px';
	bx.style.visibility = 'visible';
}
