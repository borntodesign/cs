// this file has been around since at least 2001 with just the sent() function
// I don't know if that is used anymore, but this file seems like a good place to put some
// general global type functions
// modified 03-25-04 by Bill (added function _limit_field()
// modified 04/28/05 by Bill (added msg argument to sent)

var check = 0;
function sent(msg)
{
/* assign a default message
we could receive an internationalized version as an argument */
	if (! msg) { msg = "You have already SUBMITTED\rPlease Wait :)" }
	check++;
	if (check > 1){
		alert (msg);
		return false;
	}else{
		return true;
	}
}

function _limit_field(fld,lim,fld_desc)
{
// this pesky little function will keep the user from leaving the field until the desired
// criteria are met

	if (lim == null)
	{
		alert('To use this function, the maximum field size must be defined.');
		return ;
	}

	if (fld_desc == null) fld_desc = 'the field you are working in';

	if (fld.value.length > lim)
	{

		alert('The maximum size for ' + fld_desc + ' is ' + lim + ' characters.');
		fld.focus();
		fld.select()
	}
	return;
}