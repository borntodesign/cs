/*
 * If I was opened by javascript by another page, then redirect the other page and close me, otherwise redirect me.
 */

function OpenerOrMe(url)
{
	if (window.opener)
	{
		window.opener.location = url;
		window.close();
	}
	else
	{
		window.location = url;
	}
}
