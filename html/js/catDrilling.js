/*
catDrilling.js
a library to support the flyout and stay open category drilling search functions
*/

// some 'constants
var CatControlsContainer;	/* the container for our category menus */
var MenuBoxXOffset = -1;	/* how much horiz space between sub-menu boxes and their parent */
var MenuBoxYOffset = 10;/* how much vert. space between sub-menu boxes and their parent */
var gotoCatURL = 'cat-results.xsp?mall_id=' + $MALL_ID + ';catid=';
var catexpanderObj = {
	src : 'icons/catexpander.gif',
	height : '4',
	width : '8',
	alt : $CATX		// this value should be populated within the main document
};
function attachCatExpandEvent(elm){
		if (! $E.addListener(elm, 'click', catExpand)){
//			alert('Failed to setup listener on: expander link');
		}
}

function attachGotoCatEvent(elm){
		if (! $E.addListener(elm, 'click', gotoCat)){
//			alert('Failed to setup listener on: category link');
		}
}

function BuildMenu(o){
	newlist = document.createElement('ul');
	newlist.className = 'catlist';
	BuildMenuOptions(o, newlist);
	return newlist;
}

function BuildMenuOptions(o, newlist){
	var opts = o.getElementsByTagName('c');

	for(var x=0; x < opts.length; x++){
		var newopt = document.createElement('li');
		var newa, xclass;
	// if we decide to put the icon after, just move this next chunk below the catname anchor
		if (opts[x].getAttribute('children') > 0){
			xclass = null;
			newa = document.createElement('a');
			newa.className = 'catexpander';
			newa.href = '#' + opts[x].getAttribute('catid');
			var img = document.createElement('img');
			img.src = catexpanderObj.src;
			img.alt = catexpanderObj.alt;
			img.title = catexpanderObj.alt;
			img.width = catexpanderObj.width;
			img.height = catexpanderObj.height;
			newa.appendChild(img);
			newopt.appendChild(newa);
			attachCatExpandEvent([newa]);
		}
		else {
		// we will apply an extra class to the catname link to provide uniform left column edge
			xclass = 'push-right';
		}
		newa = document.createElement('a');
		newa.className = 'gotoCat' + (xclass ? ' ' + xclass : '');
		newa.href = '#' + opts[x].getAttribute('catid');
		newa.appendChild(document.createTextNode( opts[x].firstChild.nodeValue ));
		newopt.appendChild(newa);
		attachGotoCatEvent([newa]);

		newlist.appendChild(newopt);
	}
}

function catExpand(e){
// the anchor object is named ctl because this scripting was originally based on select controls
	var ctl = $E.getTarget(e);
	while (ctl.nodeName.toUpperCase() != 'A'){
		ctl = ctl.parentNode;
	}

	$E.preventDefault(e);
	// my parent is an li, li's parent is the ul we want
	var ul = ctl.parentNode.parentNode;
	hilightCategoryItem(ctl, ul);

// delete any child forms/lists
	var lists = CatControlsContainer.getElementsByTagName('div');
	var box_id = ul.parentNode.id.match(/\d+/) *1;
//alert('catExpand='+box_id);
	for (var x=0; x < lists.length; x++){
		if (box_id < lists[x].id.match(/\d+/)){
			CatControlsContainer.removeChild(lists[x]);
			// lists.length will decrease by one upon removeChild,
			// so we will have to decrement the loop counter to compensate for the list reordering
			x--;
		}
	}
	var catid = ctl.href.match(/\d+$/);
	if (catid == undefined){ return; }
	var callbackLoadCats = 
	{
		success: LoadCats,
		failure: GeneralResponseFailure,
		argument: [box_id, ctl]
	}

	var rv = YAHOO.util.Connect.asyncRequest("GET", 'parentcats/' + catid + '.npxml', callbackLoadCats);
	if (rv == null){ alert('XMLHttpRequest failure'); }
}

function closeMenuBox(elm){
// this routine is invoked upon a click of a MenuBoxCloser
// we will delete the boxnum we receive and any higher numbered ones
// the only exception is that we will not delete boxnum 0 since it is our root categories
	var boxnum;
	while(boxnum == undefined){
		elm = elm.parentNode;
		boxnum = elm.id.match(/\d+/);
	}
	var boxes = $D.getElementsByClassName('catMenuBox', 'div', CatControlsContainer);
	for (var x=0; x < boxes.length; x++){
		var boxid = boxes[x].id.match(/\d+/);
		if (boxid < boxnum){}
		else if (boxid == 0){ boxes[x].style.left = '-2000px'; }
		else { CatControlsContainer.removeChild(boxes[x]); }
	}
}

function doCatList0(){
// this routine is very similar to the regular catExpand flow,
// but it differs in that we are loading the root categories into an existing container
// also since we are not unloading this root catlist after pulling it up
// we can reuse it if it's already loaded
	var ulck = document.getElementById('catlist0').getElementsByTagName('ul')[0];
	if (ulck != undefined){
		positionMenuBox(0);
		return;
	}
	var callbackLoadCat0 = 
	{
		success: loadCatList0,
		failure: GeneralResponseFailure
	}

	var rv = YAHOO.util.Connect.asyncRequest("GET", 'parentcats/0.npxml', callbackLoadCat0);
	if (rv == null){ alert('XMLHttpRequest failure'); }
}

function GeneralResponseFailure(o) {
	alert("There has an unexpected error:\n" + o.responseText);
}

function gotoCat(e){
	var catid = $E.getTarget(e).href.match(/\d+$/);
	$E.preventDefault(e);
// pull up our vendor page matching that category
	document.location = gotoCatURL + catid;
}

function hilightCategoryItem(elm, ul){
	// first we'll clear any other categories that may be highlighted
	var cats = ul.getElementsByTagName('li');
	for (var x=0; x < cats.length; x++){
		if (cats[x].className == 'highlighted-category'){
			cats[x].className = cats[x].orig_className;
		}
	}
	elm.parentNode.orig_className = elm.parentNode.className;
	elm.parentNode.className = 'highlighted-category';
}

function initCatDrilling(){
	CatControlsContainer = document.getElementById('CategoryNavigation');
}

function loadCatList0(o){
	if (! o.responseXML){
		alert("Unanticipated error:\n" + o.responseText);
		return;
	}
	var newBox = document.getElementById('catlist0');
	newBox.appendChild( BuildMenu(o.responseXML) );
	CatControlsContainer.appendChild(newBox);
	styleListItems(0);
	positionMenuBox(0);
}

function LoadCats(o) {
	if (! o.responseXML){
		alert("Unanticipated error:\n" + o.responseText);
		return;
	}
	var boxid = o.argument[0] + 1;
//alert('LoadCats='+boxid);
	var newBox = document.getElementById('catlist0').cloneNode(true);
	newBox.id = 'catlist' + boxid;
	newBox.style.left = '-2000px';
	newBox.removeChild(newBox.getElementsByTagName('ul')[0]);
	newBox.appendChild( BuildMenu(o.responseXML) );
	CatControlsContainer.appendChild(newBox);
	styleListItems(boxid);
	positionMenuBox(boxid, o.argument[1]);
}

function positionMenuBox(boxnum, ctl){
	var box = document.getElementById('catlist' + boxnum);
	var boxY = box.offsetHeight;
	var availY = $D.getViewportHeight();
	var scrollY = getScrollingPosition()[1];
//alert('boxY:'+boxY+' scrollY:'+scrollY);
	if (boxnum == 0){
		box.style.left = '5px';
		box.style.top = (MenuBoxYOffset + scrollY) + 'px';
		return;
	}
	var box2Side = document.getElementById('catlist' + (boxnum -1));

//alert('boxY:'+boxY+' scrollY:'+scrollY+' box2side'+$D.getY(box2Side));
	// if our box is bigger than our viewport, put it at the top of the viewport
	if (boxY > availY){
		box.style.top = (MenuBoxYOffset + scrollY) + 'px';
	} else {
		var ctlY = $D.getY(ctl);
		// if we were to position the top of our box the right of our cursor,
		// would we be below the viewport?
		if ((ctlY + boxY) > availY + scrollY){
			box.style.top = (availY - boxY + scrollY) + 'px';
		} else {
			box.style.top = ctlY + 'px';
		}
	}

	var left = ($D.getX(box2Side) + box2Side.offsetWidth) + MenuBoxXOffset;
// we are starting to lay these out from left to right
// if we are going to be too far right with our new box, we'll place it left instead
// if we are too far left, we'll go right
// we'll keep track of our direction with an additional property tacked onto our box
	if (box2Side.menuDirection == 'left'){
		// we are too far left, so lay this one to the right of the previous
		if ( ($D.getX(box2Side) - box.offsetWidth) < 0){
			box.style.left = left + 'px';
		}
		// otherwise we can place it left
		else {
			box.menuDirection = 'left';
			box.style.left = ($D.getX(box2Side) - box.offsetWidth - MenuBoxXOffset) + 'px';
		}
	}
	else if ((left + box.offsetWidth) > $D.getViewportWidth()){
		// we are too far right, so go left
		box.menuDirection = 'left';
		box.style.left = ($D.getX(box2Side) - box.offsetWidth - MenuBoxXOffset) + 'px';
	}
	else {
			box.style.left = left + 'px';
	}
}

function styleListItems(boxnum){
	var items = document.getElementById('catlist' + boxnum).getElementsByTagName('li');
	for (var x=0; x < items.length; x++){
		items[x].className = x%2==0 ? 'even':'odd';
	}
}
