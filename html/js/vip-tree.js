// This script will open a 'popup' and populate it with the necessary links

function rptWindow(rid,rtype)
{
	if (! rtype) rtype=1;
	window.open(location.protocol + '//' + location.hostname + location.pathname + '?rptwin=' + rtype + ';rid=' + rid,'links','width=250,height=300,resizable=yes');
}

function openLink(url)
{
	window.open(url); self.close(); return false;
}

function newTreeReport(rid)
{
	opener.document.forms[0].id.value = rid;
	opener.document.forms[0].submit();
	self.close(); return false;
}

function newReport(rid,rpturl)
{
	opener.document.forms[0].action = rpturl;
	opener.document.forms[0].id.value = rid;
	opener.document.forms[0].submit();
	self.close(); return false;
}
