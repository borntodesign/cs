/*
transpose.js

Part of a collection of scripts to transpose USD based numbers on a page into other currency values.
This script assumes the presence of the jQuery library.
It also presumes that sprintf.js has already been loaded and that the xrates.js list has been loaded.
Naturally, it will also be helpful if the script /cgi/MyCurrencyCode.js has been/will be loaded to attempt to get the user's currency.

*/

/*
This will be our namespace.
*/

var Transpose = {

	/*
	Once our page is loaded this function can safely be called.
	It is left to the developer to call this from the page at the appropriate time.
	*/
	
	transpose_currency: function(myClass,currcode)
	{
//	alert('calling init');
		this.init(myClass,currcode);
//	alert('calling transpose');
		this.transpose();
//	alert('done with transpose');
	},
	
	format_currency: function(myClass,currcode)
	{
		this.init(myClass,currcode);
		this.format();
	},
	
	init: function(myClass,currcode)
	{
		if (! myClass){ myClass = 'transposeME'; }
		this.myClass = myClass;

		if (! currcode)
		{
			if (typeof(my_currency_code) != "undefined" && my_currency_code)
			{
				currcode = my_currency_code;
			}
			else
			{
				currcode = 'USD';
			}
		}
		this.currcode = currcode;

		// we need to determine our country so that we can apply country specific formatting to what would otherwise be the default currency formatting
		if (typeof(my_country_code) != "undefined" && my_country_code)
		{
			this.country = my_country_code;
		}
		else
		{
			this.country = 'US';
		}

		// we need to determine our language so that we can apply language specific formatting to what would otherwise be the default currency formatting
		if (typeof(my_language_code) != "undefined" && my_language_code)
		{
			this.language_code = my_language_code;
		}
		else
		{
			this.language_code = 'US';
		}
	},
	
	identifyNodes: function()
	{
		// identify all the nodes who have the 'transpose' css class
		var elems = new Array();
		this.elems = new Array();
		jQuery('.' + this.myClass).each( function() { elems.push(this) } );

		var z = elems.length;

		for (var x=0; x < z; x++)
		{
			elem = jQuery(elems[x]);

			var val = elem.text();
			// remove leading and trailing whitespace
			var rv = val.replace(/^\s+|\s+$/g, '');
			// remove any $ signs or commas
			rv = rv.replace(/\$|,/g, '');
			
			// if what we have left is not really a number, then we will simply ignore that element
			if (rv.Nan)
			{
				continue;
			}
			elem.text(rv);
			this.elems.push(elem);
		}
	},
	
	transpose: function()
	{
		this.identifyNodes();

		var z = this.elems.length;
		for (var x=0; x < z; x++)
		{
			var elem = this.elems[x];
			var rv = elem.text();
			// apply our exchange rate
//alert('b4 xrate rv: '+rv);
//alert('currcode: '+this.currcode);
			rv = rv * eval('Currency.' + this.currcode + '.xrate');
//alert('after xrate rv: '+rv);
			rv = this.formatCurrency(rv);
//alert('after format rv: '+rv);
			elem.html( rv );
		}
	},

	format: function()
	{
		this.identifyNodes();

		var z = this.elems.length;
		for (var x=0; x < z; x++)
		{
			var elem = this.elems[x];
			var rv = elem.text();
			rv = this.formatCurrency(rv);
//alert('after format rv: '+rv);
			elem.html( rv );
		}
	},

	// we should be receiving a purely numeric value
	formatCurrency: function(val)
	{
		// now setup our formatting elements based upon the country then the currency and then the defaults
		if (typeof(this.fmt) == 'undefined')
		{
			this.fmt = {
				thousands_separator: this.getFormatting('thousands_separator'),
				decimal_separator: this.getFormatting('decimal_separator'),
				prepend_text: this.getFormatting('prepend_text'),
				append_text: this.getFormatting('append_text'),
				exponent: this.getFormatting('exponent')
			}
			// if we have not explicitly defined an empty, then make this our default
			if (this.fmt.append_text == '' && ! this.fmt.prepend_text){ this.fmt.prepend_text = this.currcode + ' '; }
		}

	// some currencies do not have only 2 digits after the dot, they could have from 0 to 3
	// so we will multipy our value enough to make it possible to round to what they would consider fractions beyond their exponent
		var mult = Math.pow(10, this.fmt.exponent);
	// we want to round .5 down instead of up, so we need to deal with that
	// by multiplying by our exponent + 1 and then dropping everything after the decimal,
	// we will end up with a single value after the decimal point when we divide by 10
	// from there we can actually deal with a .5 to lower it to .4 which will end up rounding down instead of up
		val = Math.floor(val * Math.pow(10, (this.fmt.exponent + 1)));
		val = val/10;
		val = this.round5down(val);

//		val = Math.round(val * mult);
//alert('after round: '+val);
		val = val/mult;	// bring it back now rounded
//alert('before tofixed val: '+val);		
		val = val.toFixed(this.fmt.exponent);
//alert('after tofixed val: '+val);		
		var vals = new Array(2);

		vals = val.split(".");
		
		var x=0;
		var dollars = '';
		var vl = vals[0].length -1;

		for(var y= vl; y > -1 ; y--)
		{
			var c = vals[0].substr(y,1);
			x++;

			dollars = c + dollars;
			
		// once we get to 3 digits, we need to add a thousands_separator
			if (x==3 && y > 0)
			{
				dollars = this.fmt.thousands_separator + dollars;
				x=0;
			}
		}

		var cents = vals[1] == undefined ? '' : this.fmt.decimal_separator + vals[1];
		return this.fmt.prepend_text + dollars + cents + this.fmt.append_text;
	},
	
	getFormatting: function(key)
	{
//alert('get fmt: key: '+key + 'lang: '+this.language_code + ' conty:'+this.country+' curr: '+this.currcode);
		var rv;
	// unfortunately I do not know another way to this and avoid 'is not defined' errors/program stoppages
		try
		{
			rv = eval('Currency.language.' + this.language_code + '.' + this.currcode + '.' + key);
		}
		catch(err){}

		if (rv != undefined){ //alert('match lang rv:'+rv);
		return rv; }
		try
		{
			rv = eval('Currency.country.' + this.country + '.' + this.currcode + '.' + key);
		}
		catch(err){}

		if (rv != undefined){ //alert('match country rv:'+rv);
		return rv; }
		try
		{
			rv = eval('Currency.' + this.currcode + '.' + key);
		}
		catch(err){}
		
		if (rv != undefined){ //alert('match curr rv:'+rv);
		return rv; }
		try
		{
			rv = eval('Currency.defaults.' + key);
		}
		catch(err){}

		//alert('match def rv:'+rv);
		return rv;
	},
	
	round5down: function(val)
	{
		val = val.toString();
		var ar = new Array(2);
		ar = val.split(".");
		if (! ar[1]){ return val; }
		if (ar[1] == 5){ return (val - .1); }
		return val;
	}
};

