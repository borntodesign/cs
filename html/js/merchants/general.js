/**
 * Enable, or hide the Credit Card Payment fields
 * @param show	bool	True to show, false to hide.
 * @return	null
 */
	function creditCardVisibility(show)
	{

		document.getElementById('creditCard').className = show ? 'style5':'';
		document.getElementById('creditCardNumber').className = show ? 'style5':'';
		document.getElementById('ccvNumber').className = show ? 'style5':'';
		document.getElementById('cardExpirationDate').className = show ? 'style5':'';
		document.getElementById('nameOnCard').className = show ? 'style5':'';
		document.getElementById('billingAddress').className = show ? 'style5':'';
		document.getElementById('billingCity').className = show ? 'style5':'';
		document.getElementById('billingCountry').className = show ? 'style5':'';
		document.getElementById('billingState').className = show ? 'style5':'';
		document.getElementById('billingZip').className = show ? '':'';
		
		document.getElementById('creditCardTypeRow').className = show ? 'show':'hide';

		document.getElementById('creditCardNumberRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCCVRow').className = show ? 'show':'hide';
		document.getElementById('creditCardExpirationDateRow').className = show ? 'show':'hide';
		document.getElementById('creditCardNameRow').className = show ? 'show':'hide';
		document.getElementById('creditCardDividerRow').className = show ? 'show':'hide';
		document.getElementById('creditCardAddressRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCityRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCountryRow').className = show ? 'show':'hide';
		document.getElementById('creditCardStateRow').className = show ? 'show':'hide';
		document.getElementById('creditCardPostalCodeRow').className = show ? 'show':'hide';
		
	}
	
/**
* Enable, or hide the Eco Card Payment fields
* @param show	bool	True to show, false to hide.
* @return	null
*/
	function ecoCardVisibility(show)
	{
		document.getElementById('ecoCardNumber').className =  show ? 'style5':'';
		document.getElementById('ecoCardAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('ecoCardExplanationRow').className = show ? 'show':'hide';
	}
	/**
	* Enable, or hide the Club Account Payment fields
	* @param show	bool	True to show, false to hide.
	* @return	null
	*/
	function clubAccountVisibility(show)
	{
		document.getElementById('clubAccountNumber').className =  show ? 'style5':'';
		document.getElementById('clubAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('clubAccountExplanationRow').className = show ? 'show':'hide';
	}
	/**
	* Enable, or hide the International Funds Transfer Payment fields
	* @param show	bool	True to show, false to hide.
	* @return	null
	*/
	function internationalFundsTransferVisibility(show)
	{
		//document.getElementById('internationalFundsTransferNumber').className = show ? 'style5':'hide';
		document.getElementById('internationalFundsTransferNumberRow').className = show ? 'show':'hide';
		document.getElementById('internationalFundsTransferExplanationRow').className = show ? 'show':'hide';
	}

	/**
	 * Determin the Payment Type, and display the appropriate fields.
	 * @param PaymentMethodSelectBox	obj	The Select box oject
	 * @return
	 */
	function paymentRequirements(PaymentMethodSelectBox)
	{
		switch(PaymentMethodSelectBox[PaymentMethodSelectBox.selectedIndex].value)
		{
			case 'cc':
				creditCardVisibility(true);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				break;
			
			case 'ec':
				creditCardVisibility(false);
				ecoCardVisibility(true);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				break;		
			
			case 'ca':
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(true);
				internationalFundsTransferVisibility(false);
				break;
			
			case 'if':
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(true);
				break;
				
			default:
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);

		}
		
	}
	