function CheckRequired(form_num, fields_string, real_names_string, msg)
 {
    var focus_field = '';
    emptyfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");

    for ( fieldname in the_fields )
    {
	   	reqid_empty = true;
    	for ( i=0; i < eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".length"); i++ )
   	 {

		if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + "[i].type") == 'radio'
			&& eval("document.forms[" + form_num + "]." + the_fields[fieldname] + "[i].checked") == true )
		{
			reqid_empty = false;
		}
		if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".type") == 'select-one' 
			&& i!=0
			&& eval("document.forms[" + form_num + "]." + the_fields[fieldname] + "[i].selected") == true )
		{
			reqid_empty = false;
		}
    	}
  	if ( reqid_empty == true && eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".length")
		&& ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + "[0].type") == 'radio'
		||  eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".type") == 'select-one' ) )
  	{
		emptyfields = emptyfields + "..." + real_names[fieldname] + "\n";
		if ( focus_field == '' )
		{
  			if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + "[0].type") == 'radio' )
			{
				focus_field = "self.document.forms[" + form_num + "]." + the_fields[fieldname] + "[0]";
			}
  			else if (eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".type") == 'select-one')
			{
				focus_field = "self.document.forms[" + form_num + "]." + the_fields[fieldname];
			}
    	}
	}
  	if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value") == ''
		&& ! eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".length") )
	{
  		emptyfields = emptyfields + "..." + real_names[fieldname] + "\n";
		if ( focus_field == '' )
		{
			focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
		}
	}
    }
    if ( emptyfields != "" )
    {
  		msg = "- " + msg + "\n" + emptyfields;
  		alert(msg);
		// Set focus back to the appropriate field.
		eval(focus_field + ".focus()");
		return false;
    }
    return true;
 }

function CheckDollars(form_num, fields_string, real_names_string, msg)
 {
    var focus_field = '';
    var good_number = /(^\d+$|^\d+\.\d{2}?$)/;
	
    nonumfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");

	for ( fieldname in the_fields )
    {
		if ( ! good_number.test(eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value")))
		{
  			nonumfields = nonumfields + "..." + real_names[fieldname] + "\n";
			if ( focus_field == '' )
			{
				focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
			}
    	}
    	if ( nonumfields != "" )
    	{
  		  	msg = "- " + msg + "\n" + nonumfields;
  			alert(msg);
			// Set focus back to the appropriate field.
			eval(focus_field + ".focus()");
			return false;
		}
    }
    return true;
 }

function CheckMinimum(form_num, fields_string, real_names_string, msg, min)
 {
    var focus_field = '';
	
    nominfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");

	for ( fieldname in the_fields )
    	{
		if ( parseFloat(eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value")) < min )
		{
  			nominfields = nominfields + "..." + real_names[fieldname] + "\n";
			if ( focus_field == '' )
			{
				focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
			}
    		}
    		if ( nominfields != "" )
    		{
  		  	msg = "- " + msg + "\n" + nominfields;
  			alert(msg);
			// Set focus back to the appropriate field.
			eval(focus_field + ".focus()");
			return false;
		}
    	}
    	return true;
 }

function CheckNumber(form_num, fields_string, real_names_string, msg)
 {
    var focus_field = '';
    var bad_number = /(^(\D))/;
	
    nonumfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");

	for ( fieldname in the_fields )
    {
		if ( bad_number.test(eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value")))
		{
  			nonumfields = nonumfields + "..." + real_names[fieldname] + "\n";
			if ( focus_field == '' )
			{
				focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
			}
    	}
    	if ( nonumfields != "" )
    	{
  		  	msg = "- " + msg + "\n" + nonumfields;
  			alert(msg);
			// Set focus back to the appropriate field.
			eval(focus_field + ".focus()");
			return false;
		}
    }
    return true;
 }

function CheckPercentage(form_num, fields_string, real_names_string, msg)
 {
    var focus_field = '';
    var max_percentage = 100;
	
	if ( eval("document.forms[" + form_num + "].how.value") == "percentage" )
	{
   		badfields = "";
  		the_fields = fields_string.split(",");
 		real_names = real_names_string.split(",");

		for ( fieldname in the_fields )
   	 	{
			if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value") > max_percentage )
			{
  				badfields = badfields + "..." + real_names[fieldname] + "\n";
				if ( focus_field == '' )
				{
					focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
				}
    		}
    		if ( badfields != "" )
    		{
  			  	msg = "- " + msg + "\n" + badfields;
  				alert(msg);
				// Set focus back to the appropriate field.
				eval(focus_field + ".focus()");
				return false;
			}
    	}
	}
    return true;
}