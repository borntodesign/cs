

	var percentages = new Array();
	percentages['platinum'] = new Array();
	percentages['platinum']['1%'] = 0.01;
	percentages['platinum']['2%'] = 0.02;
	percentages['platinum']['3%'] = 0.03;
	percentages['platinum']['4%'] = 0.04;
	percentages['platinum']['5%'] = 0.05;
	percentages['platinum']['6%'] = 0.06;
	percentages['platinum']['7%'] = 0.07;
	percentages['platinum']['8%'] = 0.08;
	percentages['platinum']['9%'] = 0.09;
	percentages['platinum']['10%'] = 0.1;
	percentages['platinum']['15%'] = 0.15;
	percentages['platinum']['20%'] = 0.2;
	percentages['platinum']['25%'] = 0.25;
	percentages['platinum']['30%'] = 0.3;
	percentages['platinum']['35%'] = 0.35;
	percentages['platinum']['40%'] = 0.4;
	
	percentages['gold'] = new Array();
	percentages['gold']['2%'] = 0.02;
	percentages['gold']['3%'] = 0.03;
	percentages['gold']['4%'] = 0.04;
	percentages['gold']['5%'] = 0.05;
	percentages['gold']['6%'] = 0.06;
	percentages['gold']['7%'] = 0.07;
	percentages['gold']['8%'] = 0.08;
	percentages['gold']['9%'] = 0.09;
	percentages['gold']['10%'] = 0.1;
	percentages['gold']['15%'] = 0.15;
	percentages['gold']['20%'] = 0.2;
	percentages['gold']['25%'] = 0.25;
	percentages['gold']['30%'] = 0.3;
	percentages['gold']['35%'] = 0.35;
	percentages['gold']['40%'] = 0.4;
	
	percentages['silver'] = new Array();
	percentages['silver']['3%'] = 0.03;
	percentages['silver']['4%'] = 0.04;
	percentages['silver']['5%'] = 0.05;
	percentages['silver']['6%'] = 0.06;
	percentages['silver']['7%'] = 0.07;
	percentages['silver']['8%'] = 0.08;
	percentages['silver']['9%'] = 0.09;
	percentages['silver']['10%'] = 0.1;
	percentages['silver']['15%'] = 0.15;
	percentages['silver']['20%'] = 0.2;
	percentages['silver']['25%'] = 0.25;
	percentages['silver']['30%'] = 0.3;
	percentages['silver']['35%'] = 0.35;
	percentages['silver']['40%'] = 0.4;
	
	percentages['bronze'] = new Array();
	percentages['bronze']['4%'] = 0.04;
	percentages['bronze']['5%'] = 0.05;
	percentages['bronze']['6%'] = 0.06;
	percentages['bronze']['7%'] = 0.07;
	percentages['bronze']['8%'] = 0.08;
	percentages['bronze']['9%'] = 0.09;
	percentages['bronze']['10%'] = 0.1;
	percentages['bronze']['15%'] = 0.15;
	percentages['bronze']['20%'] = 0.2;
	percentages['bronze']['25%'] = 0.25;
	percentages['bronze']['30%'] = 0.3;
	percentages['bronze']['35%'] = 0.35;
	percentages['bronze']['40%'] = 0.4;
	
	percentages['basic'] = new Array();
	percentages['basic']['5%'] = 0.05;
	percentages['basic']['6%'] = 0.06;
	percentages['basic']['7%'] = 0.07;
	percentages['basic']['8%'] = 0.08;
	percentages['basic']['9%'] = 0.09;
	percentages['basic']['10%'] = 0.1;
	percentages['basic']['15%'] = 0.15;
	percentages['basic']['20%'] = 0.2;
	percentages['basic']['25%'] = 0.25;
	percentages['basic']['30%'] = 0.3;
	percentages['basic']['35%'] = 0.35;
	percentages['basic']['40%'] = 0.4;
	
	percentages['free'] = new Array();
	percentages['free']['10%'] = 0.1;
	percentages['free']['15%'] = 0.15;
	percentages['free']['20%'] = 0.2;
	percentages['free']['25%'] = 0.25;
	percentages['free']['30%'] = 0.3;
	percentages['free']['35%'] = 0.35;
	percentages['free']['40%'] = 0.4;
	
// if you change the percentages for free above you also must change cgi form 10903	
	
	function creditCardVisibility(show)
	{
		
		$('#creditCard').toggleClass('style5', show);
		$('#creditCardNumber').toggleClass('style5', show);
		$('#ccvNumber').toggleClass('style5', show);
		$('#cardExpirationDate').toggleClass('style5', show);
		$('#nameOnCard').toggleClass('style5', show);
		$('#billingAddress').toggleClass('style5', show);
		$('#billingCity').toggleClass('style5', show);
		$('#billingCountry').toggleClass('style5', show);
		$('#billingState').toggleClass('style5', show);
		$('#billingZip').toggleClass('style5', show);		
		
		
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#creditCardExplanationRow').toggle(show);
		}
		else
		{
			$('#creditCardExplanationRow').toggle(false);
		}
		
		$('#creditCardTypeRow').toggle(show);
		$('#creditCardNumberRow').toggle(show);
		$('#creditCardCCVRow').toggle(show);
		$('#creditCardExpirationDateRow').toggle(show);
		$('#creditCardNameRow').toggle(show);
		$('#creditCardDividerRow').toggle(show);
		$('#creditCardDividerRowOne').toggle(show);
		$('#creditCardAddressRow').toggle(show);
		$('#creditCardCityRow').toggle(show);
		$('#creditCardCountryRow').toggle(show);
		$('#creditCardStateRow').toggle(show);
		$('#creditCardPostalCodeRow').toggle(show);
	
		
	}
	

	function ecoCardVisibility(show)
	{
		$('#ecoCardNumber').toggleClass('style5', show);		
		
		$('#ecoCardAccountNumberRow').toggle(show);
		$('#ecoCardExplanationRow').toggle(show);
	}

	
	function clubAccountVisibility(show)
	{
		$('#clubAccountNumber').toggleClass('style5', show);		
		
		$('#clubAccountNumberRow').toggle(show);
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#clubAccountExplanationRow').toggle(show);
			
			$('#clubAccountExplanationOfflineRewardsRow').toggle(false);
		}
		else
		{
			$('#clubAccountExplanationRow').toggle(false);
			
			$('#clubAccountExplanationOfflineRewardsRow').toggle(show);
		}
		
		
	}

	function internationalFundsTransferVisibility(show)
	{
		//$('#internationalFundsTransferNumber').toggleClass('style5', show);	
		$('#internationalFundsTransferNumberRow').toggle(show);
		
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#internationalFundsTransferExplanationRow').toggle(show);
			$('#internationalFundsTransferOfflineRewardsExplanationRow').toggle(false);
		}
		else
		{
			$('#internationalFundsTransferExplanationRow').toggle(false);
			$('#internationalFundsTransferOfflineRewardsExplanationRow').toggle(show);
		}
		
	}
	
	/*
	 * These functions pertain to Payment Options.
	 */
		
		function paymentRequirements(PaymentMethodSelectBox)
		{
			switch(PaymentMethodSelectBox[PaymentMethodSelectBox.selectedIndex].value)
			{
				case 'cc':
					creditCardVisibility(true);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
					break;
				
				case 'ec':
					creditCardVisibility(false);
					ecoCardVisibility(true);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
					break;		
				
				
				case 'ca':
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(true);
					internationalFundsTransferVisibility(false);
					break;
				
				case 'if':
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(true);
					break;
				
				default:
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
				
			}
			
		}
	 
		
	 	/*
		* These functions pertain to package options.
		*
		* 
		* $.each(selectValues, function(key, value)
		*		 {   
		*		      $('#mySelect').
		*		           append($("<option/>"), {
		*		               value: key,
		*		               text: value});
		*		 });
		* 
		* $('#mySelect').empty()
		* 
		 */
			function disableBasic()
			{

				$('.basic').attr("disabled","disabled");
				$("input[class='basic']").removeAttr("checked");
				$("option[class='basic']").removeAttr("selected");
			}

			function disableBronze()
			{
				$("option[class='bronze']").attr("disabled","disabled");
				$("option[class='bronze']").removeAttr("selected");
			}
			
			function disableSilver()
			{

				$('.silver').attr("disabled","disabled");
				$("input[class='silver']").removeAttr("checked");
				$("option[class='silver']").removeAttr("selected");
			}
			
			
			function disableGold()
			{

				$('.gold').attr("disabled","disabled");
				$("input[class='gold']").removeAttr("checked");
				$("option[class='gold']").removeAttr("selected");
			}
			
			
			function disablePlatinum()
			{

				$('.platinum').attr("disabled","disabled");
				$("input[class='platinum']").removeAttr("checked");
				$("option[class='platinum']").removeAttr("selected");
			}
			
			function disableOfflineBronze()
			{
				$("option[class='bronze']").attr("disabled","disabled");
				$("option[class='bronze']").removeAttr("selected");
			}
			
			function disableFlatFeeOff()
			{
				$("input[name='flat_fee_off']").val('');
				$("input[name='flat_fee_off']").attr("disabled","disabled");
			}

			function disableSpecial()
			{
				$('#special').attr("disabled","disabled");
				$("#special > option").removeAttr("selected");
			}
			
			function disablePercentageOff()
			{
				$('#percentage_off').attr("disabled","disabled");
				$("#percentage_off > option").removeAttr("selected");
			}
			
			function clearPercentageOfSale()
			{
				$("#percentage_of_sale > option").removeAttr("selected");
			}

			function enableBasic()
			{
				$("option[class='basic']").removeAttr("disabled");
			}

			function enableBronze()
			{
				$("option[class='bronze']").removeAttr("disabled");
			}
			
			function enableOfflineBronze()
			{
				$("option[class='bronze']").removeAttr("disabled");
			}
			
			function enableSilver()
			{
				$("input[class='silver']").removeAttr("disabled");
				$("option[class='silver']").removeAttr("disabled");
			}

			function enableGold()
			{
				$("input[class='gold']").removeAttr("disabled");
				$("option[class='gold']").removeAttr("disabled");
			}

			function enablePlatinum()
			{
				$("input[class='platinum']").removeAttr("disabled");
				$("option[class='platinum']").removeAttr("disabled");
			}

		function setPackageBenefits(package_id)
		{
			package_id = parseInt(package_id);

//alert(package_id);
			
			switch(package_id)
			{
				// Annual Package, with Tracking
			case 0:
			        disableBronze();
			        disableSilver();
			        disableGold();
			        disablePlatinum();
			        disableBronze();
			        $('#payment_types').toggle(true);
			        $('#package_options_divider').toggle(true);
			        $('#reward_merchant_benefits').toggle(true);
			        $('#free_reward_merchant_benefits').toggle(flase);
			        paymentRequirements(document.getElementById('payment_method'));
			        $("input[name='discount_subtype']").removeAttr("checked");
					disableFlatFeeOff();
			        $("#offline_reward_discounts_flat_fee_off").removeAttr("checked");
			        $('#free_reward_merchant_benefits_flat_fee').toggle(false);
					disableSpecial();
					disablePercentageOff();
					clearPercentageOfSale();
					$("select[name='percentage_off']").empty()
					break;
				//Annual Packages with Monthly Packages
			case 10://Bronze
					enableBasic();
					enableBronze();
					disableSilver();
					disableGold();
					disablePlatinum();
					$('#payment_types').toggle(true);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);
					
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					for ( var index_key in percentages['bronze'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['bronze'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					break;
			case 11://Silver
					enableBasic();
					enableBronze();
					enableSilver();
					disableGold();
					disablePlatinum();
					$('#payment_types').toggle(true);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);
					$("#offline_reward_discounts_flat_fee_off").removeAttr("disabled");
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					for ( var index_key in percentages['silver'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['silver'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					break;
			case 12://Gold
					enableBasic();
					enableBronze();
					enableSilver();
					enableGold();
					disablePlatinum();
					$('#payment_types').toggle(true);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);
					
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					for ( var index_key in percentages['gold'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['gold'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					break;
			case 13://Platinum
					enableBasic();
					enableBronze();
					enableSilver();
					enableGold();
					enablePlatinum();
					$('#payment_types').toggle(true);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);
					
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					for ( var index_key in percentages['platinum'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['platinum'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					break;
			case 14://Basic
					enableBasic();
					disableSilver();
					disableGold();
					disablePlatinum();
					disableBronze();
					$('#payment_types').toggle(true);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);

					disableFlatFeeOff();
					$("#offline_reward_discounts_flat_fee_off").removeAttr("checked");
					// $('#free_reward_merchant_benefits_flat_fee').toggle(false);
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					for ( var index_key in percentages['basic'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['basic'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					break;
				case 15://Free
					disableBasic();
					disableSilver();
					disableGold();
					disablePlatinum();
					disableBronze();
					$('#payment_types').toggle(false);
					$('#package_options_divider').toggle(true);
					$('#reward_merchant_benefits').toggle(false);
					$('#free_reward_merchant_benefits').toggle(true);

					disableFlatFeeOff();
					$("#offline_reward_discounts_flat_fee_off").removeAttr("checked");
					$('#free_reward_merchant_benefits_flat_fee').toggle(true);
					paymentRequirements(document.getElementById('payment_method'));
					clearPercentageOfSale();
					
					var selected_value = $("select[name='percentage_off'] option:selected").val();
					$("select[name='percentage_off']").empty()
					$("select[name='percentage_off']").append($("<option />"));
					
					for ( var index_key in percentages['free'] )
					{
						$("select[name='percentage_off']").append($("<option value='" + percentages['free'][index_key] + "'>" + index_key + "</option>"));
					}
					
					$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
					
					
					break;
			}
		}
		
$(document).ready(function(){
	/**
	 * @author Keith
	 * @description	This will allow the "Submit" button to be pressed only once.
	 */
	//$('input[type="image"]').attr("disabled", false); // This is probably not needed, but you can enable it if you desire.
	$("form").submit(function(){
		$('input[type="image"]').attr("src", "/images/btn/btn_processing_91x27.png");
		$('input[type="image"]').attr("disabled", true);
		return true;
	});
	
	
	/********************************************************
		These set the available options for reward merchants
	*/
	
	$("input[name='merchant_package']").change(
		function()
		{
			setPackageBenefits(this.value);
		}
	);

	$("input[name='merchant_package']:checked").each(
			function()
			{
				setPackageBenefits(this.value);
			}
		);


	$('#offline_reward_discounts_percentage_off').change(function() {
		$("#percentage_off").removeAttr("disabled");
		disableFlatFeeOff();
		disableSpecial();
		clearPercentageOfSale();
	});
	
	$('#offline_reward_discounts_flat_fee_off').change(function() {
		disablePercentageOff();
		$('#flat_fee_off').removeAttr("disabled");
		disableSpecial();
		clearPercentageOfSale();
	});
	
	$('#offline_reward_special').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		$('#special').removeAttr("disabled");
		clearPercentageOfSale();
	});
	
	$('#percentage_of_sale').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		disableSpecial();
		$("input[name='special']").removeAttr("checked");
	});
	
	/********************************************************/
	
	$('#percentage_off').change(function(){
		$("input[name='special']").removeAttr('checked');
		$("input[name='flat_fee_off']").val('');	
	});

	$('#flat_fee_off').change(function(){
		$("input[name='special']").removeAttr('checked');
		$("#percentage_off option").removeAttr("selected");
	});
	
	$("input[name='special']").change(function(){
		$("input[name='flat_fee_off']").val('');
		$("#percentage_off option").removeAttr("selected");
	
	});
	
	
	
	
	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	
	
	/**
		This resets the package options when the merchant changes 
	*/
	$("input[name='merchant_type']").change(function(){
		
		$("input[name='flat_fee_off']").val('');
	
		$("#percentage_off option").removeAttr("selected");
		
		$("input[name='special']").removeAttr('checked');
		
		$("input[name='merchant_package']").removeAttr('checked');
		
		$("#percentage_of_sale option").removeAttr("selected");
		
		$("#exception_percentage_of_sale option").removeAttr("selected");
		
	});
	
	
	$('#_membership_member_id').change(function() {
		$("#member_id").removeAttr("disabled");
		$("#referral_id").val('');		
		$("#referral_id").attr("disabled","disabled");
	});

	
	$('#_membership_referral_id').change(function() {
		$("#referral_id").removeAttr("disabled");
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
	});

	$('#_membership_new_member').change(function() {
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
		$("#referral_id").val('');
		$("#referral_id").attr("disabled","disabled");
	});


	$("input[id='_membership_referral_id']:checked").each(function(){
		$("#referral_id").removeAttr("disabled");
	});


	$("input[id='_membership_member_id']:checked").each(function(){
		$("#member_id").removeAttr("disabled");
	});
	
	
	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	
	$("textarea[name='discount_blurb']").maxlength({'feedback' : '.charsLeft'});

});




