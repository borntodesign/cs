/*
activity_rpt.js
routines for use with the Partner activity_rpt
*/

/* Globals */
var myLocalObj = new Object();


var responseSuccess = function(data, textStatus, jqXHR)
{
	// we should have a CSV list of pk values, so we'll try to match on that
	if (! data.match(/^\d+,*/) ){
		alert($js_err.err2 + '\n' + data);	//js_err2
		return;
	}
	var pks = data.split(',');
	for (var x=0; x < pks.length; x++){
		unsetFlaggedItem(pks[x]);
	}

// we should have set this property just before we sent off the ajax request
// if we were successful, we don't need the followup form showing any more
	if (myLocalObj.activeFollowupFrm){
		$('#' + myLocalObj.activeFollowupFrm).hide();
		delete myLocalObj.activeFollowupFrm;
	}
};

var responseFailure = function()
{
	alert($js_err.err1);	//js_err1
}

function submitAtypeFollowUp(atype)
{
	// set the hidden field in the actual list form to the value of the textarea in the selected followup form
	$('#activities_frm_followup').val( $('#activity_followup_text_' + atype).val() );
	
	// this is actually a fieldset
	myLocalObj.activeFollowupFrm = 'activity_followup_' + atype;
	submitFollowUp();
}

//our Ajax form submission
function submitFollowUp()
{
	var myfrm = $('#activities_frm');
	// make sure we have comments and something checked
	if (! $('#activities_frm_followup').val() ){
		alert($js_err.err3);	//js_err3
		return;
	}
	if ($('#activities_frm input:checkbox:checked').length == 0){
		alert($js_err.err4);	//js_err4
		return;
	}
	
	$.ajax({
        type        : 'GET',
        url         : 'https://' + location.hostname + '/cgi/vip/member-followup.cgi/bulkfollowup',
        data        : myfrm.serialize(), // our data object
        dataType    : 'text', // what type of data do we expect back from the server
        encode      : true,
        success		: responseSuccess
    })
    .fail(responseFailure);
	
	return false;
}

function _do_all(ck)
{
	ck = ck ? true : false;
	$('#activities_tbl input[type="checkbox"]').prop('checked', ck);
}

function _action(_action_)
{
	document.forms[0].action.value = _action_;
}

function bodyLoad()
{
	if (window.opener){
		var pwin = unescape(window.opener.location.pathname);
		if (pwin.indexOf('mem-cpl.html') > -1){ window.opener.location.href = pwin; }
	}
}

function csvFocusSelect(atype)
{
	$('#activity_followup_' + atype).hide();

	var csv = $('#csvlist_type' + atype);
	csv.show();
	csv.select();
}

function csvSectionHide(atype)
{
	$('#activity_followup_' + atype).hide();
	$('#csvlist_type' + atype).hide();
}

// this section is for creating the option/s when one clicks on the ID at the activity reports

var hdr = "<html><head><title></title></head><script type=\"text/javascript\">function loadReport(rpturl){window.open(rpturl,'_blank');self.close();}</script>\n"
hdr += "<style type=\"text/css\"><!--\nbody{font-size: 11px;}\n--></style></body>\n";
var ftr = "</body></html>";
var anchor_start = "<a href=\"javascript:void(0)\" onclick=\"loadReport('";
var line_rpt = "/cgi/line_rpt.cgi?mid=";
var line_rpt_end = "')\">Upline Report</a><br /><br />";
var md = "/cgi/vip/member-followup.cgi?rid=";
var md_end = "')\">Member Followup</a>";

function presentOptions(mid,spid)
{
   // if they are the sponsor or at least Builder Intern role level, then we'll give them the option
   // the applications will still validate this, but we'll avoid unnecessary calls to the application
	if (spid == meid)
	{
		// display a little options window
		var nuwin= window.open('', 'reportoptions', 'height=100,width=250');
		nuwin.document.open();
		nuwin.document.write(hdr + anchor_start + line_rpt + mid + line_rpt_end + anchor_start + md + mid + md_end + ftr);
		nuwin.document.close();
	}
	else
	{
		window.open(line_rpt + mid);
	}
}

function showFollowup(atype)
{
	$('#csvlist_type' + atype).hide();
	$('#activity_followup_' + atype).show();
}

// this unflags items and removes the checkboxes
function unsetFlaggedItem(pk)
{
	var mybx = $('input[data-pk="' + pk + '"]');
	if (! mybx){
		alert('did not find box: ' + pk);
		return;
	}

	// first, remove the checkbox... myparent is the <label>, grandparent should be the <td>
	var myparent = mybx.parent();
	mybx.remove();
	
	// now insert a green tick in it's place
	myparent.prepend('<img src="/images/icons/green_tick_8x8.png" width="8" height="8" alt="Complete" class="greenTick" />');

	// next, look up and then down to remove the flagging on the ID
	// unless we still have unchecked boxes
	var grandparent = myparent.closest('tr');

	// if we have any remaining checkboxes -or- any remaining list elements that are not done, then we are done in here
	if (grandparent.find('input').length > 0 || grandparent.find('li.fun').length > 0){
		return false;
	}
	
	grandparent.find('a').removeClass('fun');
	grandparent.find('a').addClass('fud');

	return false;
}

// this one handles checking or unchecking the whole kit-n-kaboodle
function setAll(ck)
{
	ck = ck ? true : false;
	$('.reportTbl input[name="activities"]').prop('checked', ck);
}

// just check off the ones pertaining to a specific activity
function setCkboxX(atype)
{
	setAll();
	$('.ckbx_' + atype).prop( "checked", true );
}
