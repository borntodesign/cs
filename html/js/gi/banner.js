/*
gi_banner.js

Used by the Glocal Income look pages to provide the sort of scrolling banner across the top

Also added the conditonalCloseWindowLink function on 04/20/09

To use this, simply put these 2 tags in the head of the document:
<script type="text/javascript" src="/js/gi/blurb.js"></script>
<script type="text/javascript" src="/js/gi/banner.js"></script>

And then put this into the body of the document where the message should go:
<script type="text/javascript">DoIt();</script>
*/

var colorone="#CCCCCC";
var colortwo="#CC6600";
var flashspeed=100;  //in milliseconds
var n=0;
var m;

function crossref(number){
	var crossobj= document.all ? eval("document.all.changecolor" + number) : document.getElementById("changecolor" + number);
	return crossobj;
}

function color(){

//Change all letters to base color
	if (n==0){
		for (var _m=0; _m <  message.length; _m++){
			//eval("document.all.changecolor"+m).style.color=colorone
			crossref(_m).style.color=colorone;
		}
	}
	//cycle through and change individual letters from colorone to colortwo
	crossref(n).style.color=colortwo;

	if (n < message.length -1){
		n++;
	} else {
		n=0;
		clearInterval(flashing);
		setTimeout("begincolor()",12000);
	}
	return;
}

function begincolor(){
	if (document.all || document.getElementById){
		flashing = setInterval("color()",flashspeed);
	}
}

// by placing these items in here, we don't have to worry about escaping the HTML in an XML or XHTML document
function _a(){ return '<font color="'+colorone+'">'; }

function _b(){
	return '<span id="changecolor' + m + '">' + message.charAt(m) + '</span>';
}

function _c(){ return '</font>'; }

// there is a link on many pages called "Close Window" that uses a window.close method that is not going to work unless the page was opened by JS
// we will make that link/section go away if the window was not opened by JS
function conditionalCloseWindowLink(){
	// if we were opened by javascript, we're done
	if (window.opener){ return; }
	var elm = document.getElementById('hide_me_if_nojsopen');
	// if the element to hide is not in the document, we are done
	if (! elm){ return; }
	elm.style.display = 'none';
}

function DoIt(){
	if (document.all || document.getElementById){
       	document.write( _a() );
	       for (m=0; m <  message.length;m++){
       		document.write( _b() );
			document.write( _c() );
		}
	}
	else {
       	document.write(message);
	}
	begincolor();
}