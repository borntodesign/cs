// last modified 12/23/09	Bill MacArthur
// 08/14/03 changed the size definition of the barcode image
// 12/23/09 changed the definition of the card backs and fronts to work with the new card look

// this array will hold our special card definitions
var cardDefs = new Array();

// our 'blank' card
cardDefs[0] = '<img src="/images/blanklin.gif" width="320" height="180" border="0" alt="" />';

// our info box for use when printing both a personal card
cardDefs[1] = "<div align=\"right\">\
	<img src=\"/images/blanklin.gif\" width=\"320\" height=\"1\" border=\"0\" alt=\"\"><br />\
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\
	<tr><td class=\"inst\">Your ClubBucks Card</td><td><img src=\"/clubbucks/images/arrow_l_grey.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"\"></td></tr>\
	</table></div>";

// another info box to indicate a personal card and the other cards for distribution
cardDefs[2] = "<div align=\"right\">\
	<img src=\"/images/blanklin.gif\" width=\"320\" height=\"1\" border=\"0\" alt=\"\"><br />\
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\
	<tr><td class=\"inst\">Your ClubBucks Card</td><td><img src=\"/clubbucks/images/arrow_l_grey.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"\"></td></tr>\
	<tr><td class=\"inst\">For you to distribute </td><td><img src=\"/clubbucks/images/arrow_down_grey.gif\" width=\"15\" height=\"15\" border=\"0\" alt=\"\"></td></tr>\
	</table></div>";

// a simple box for test printing
cardDefs[3] = "<img src=\"/clubbucks/images/box.png\" width=\"320\" height=\"180\" border=\"0\" alt=\"\">";

// the definition of the front of our Card
//cardDefs[4] = "<img src=\"/clubbucks/images/bizcard-front.jpg\" width=\"320\" height=\"180\" border=\"0\" alt=\"ClubBucks Card Front\">";
cardDefs[4] = '<img src="/clubbucks/images/ClubShop-Rewards-card-72-dpi-Front.jpg" width="320" height="180" border="0" alt="ClubBucks Card Front" />';

var defLM = 1;		// our default left page margin
var defCM = 20;		// our default center 'margin'
var defTM = 30;		// our default top margin
var defSPC = 12;	// default vertical spacing of cards

// the document we are refering to, sometimes it will be the 'opener'
// in those cases we'll explicitly set it in the page
var _docTarget = 'self.document';

// the same goes for our images array
var docImgsArr = self.document.images;

var part1 = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\
<tr>\
<td><img src=\"/clubbucks/images/bizcard_1-1.jpg\" width=\"137\" height=\"27\" border=\"0\" alt=\"\"></td>\
<td background=\"/clubbucks/images/bizcard_1-2.jpg\"><img src=\"/images/blanklin.gif\" width=\"183\" height=\"1\" border=\"0\" alt=\"\"><br>&nbsp;&nbsp;<span class=\"cbid\">";

var part2 = "</span></td></tr>\
<tr>\
<td colspan=\"2\"><img src=\"/clubbucks/images/bizcard_2.jpg\" width=\"320\" height=\"78\" border=\"0\" alt=\"\"></td></tr>\
<tr>\
<td colspan=\"2\">\
<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\
<tr>\
<td rowspan=\"2\"><img src=\"/clubbucks/images/bizcard_3-1.jpg\" width=\"38\" height=\"75\" border=\"0\" alt=\"\"></td>\
<td bgcolor=\"#ffffff\" align=\"center\"><img src=\"/images/blanklin.gif\" width=\"244\" height=\"1\" border=\"0\" alt=\"\"><br>\
<img width=\"240\" height=\"50\" border=\"0\" alt=\"\" name=\"barcode\" src=\"/cgi-bin/barcode/";

var part3 = "\"></td>\
<td rowspan=\"2\"><img src=\"/clubbucks/images/bizcard_3-3.jpg\" width=\"38\" height=\"75\" border=\"0\" alt=\"\"></td>\
</tr>\
<tr><td valign=\"bottom\"><img src=\"/clubbucks/images/bizcard_3-2.jpg\" width=\"244\" height=\"10\" border=\"0\" alt=\"\"></td>\
</tr></table>\
</td></tr></table>";

var part4 = '<div style="width:320px;height:180px;position:relative;">\
<img width="320" height="180" src="/clubbucks/images/ClubShop-Rewards-card-72-dpi-Back.jpg" alt="" />\
<img style="position:absolute;top:140px;left:56;" width="208" height="35" alt="" name="barcode" border="0" alt="" src="/cgi-bin/barcode/';

var part5 = '?h=35" /></div>';

function displayCRD(code){
// we'll branch according to any special codes received
	if (typeof code == 'undefined'){ return }

// we'll assign special codes to a particular placeholder in the array
	if ((code -0) <= cardDefs.length){
		document.write( cardDefs[ (code -0) ] );
	}
	else{
//		document.write( part1 + code + part2 + code + part3 );
		document.write( part4 + code + part5 );
	}
}

function handleErr(msg, url, line){
// we'll handle the case where the graphics cannot be resized with a special error message
	if ( msg.indexOf('read-only') != -1 ){
		alert('Your browser will not allow the page to be dynamically altered.\nPlease refer to the help link.');
// if we were opened by another document, then we'll close since we cannot do anything
		if (_docTarget != 'self.document'){ self.close() }
	}
	else{
		alert('Javascript error: ' + msg + "\nOn line: " + line);
		return false;
	}
}

function lmSet(val, flg)
{
// if we don't receive the flag, we know that we are incrementing rather than just setting
	if (typeof flg == 'undefined')
	{
		val = (fmt.LM -0) + (val -0);
	}

// since the val passed will probably be recognized as text, we'll force it to INT by subtracting zero
	if ((val -0) > 0)
	{
		fmt.LM = val;
		fmt.store();
		docImgsArr.leftmargin.width = val;
	}
	else { warnInvalid() }
}	

function cmSet(val, flg)
{
// if we don't receive the flag, we know that we are incrementing rather than just setting
	if (typeof flg == 'undefined')
	{
		val = (fmt.CM -0) + (val -0);
	}

// since the val passed will probably be recognized as text, we'll force it to INT by subtracting zero
	if ((val -0) > 0)
	{
		fmt.CM = val;
		fmt.store();
		docImgsArr.ctrmargin.width = val;
	}
	else{ warnInvalid() }
}

function spreadSet(val, flg)
{
// if we don't receive the flag, we know that we are incrementing rather than just setting
	if (typeof flg == 'undefined')
	{
		val = (fmt.SPC -0) + (val -0);
	}

// since the val passed will probably be recognized as text, we'll force it to INT by subtracting zero
	if ((val -0) > 0)
	{
		fmt.SPC = val;
		fmt.store();

		var x = 1;

	// we'll loop through all the associated images until we come up empty
		while (x){
			var imgN = eval(_docTarget + ".images.sep" + x);
			if (typeof imgN == 'undefined'){
				break;
			}
			else {
				imgN.height = val;
				x++;
			}
		}
	}
	else{ warnInvalid() }
}

function tmSet(val, flg)
{
// if we don't receive the flag, we know that we are incrementing rather than just setting
	if (typeof flg == 'undefined')
	{
		val = (fmt.TM -0) + (val -0);
	}

// since the val passed will probably be recognized as text, we'll force it to INT by subtracting zero
	if ((val -0) > 0)
	{
		fmt.TM = val;
		fmt.store();
		docImgsArr.topmargin.height = val;
	}
	else{ warnInvalid() }
}

function init_format()
{
	tmSet(fmt.TM, 1);
	lmSet(fmt.LM, 1);
	cmSet(fmt.CM, 1);
	spreadSet(fmt.SPC, 1);
}

function resize_Test(myImg)
{
// it appears that the javascript error handling may not always be invoked when trying to resize
// images that the browser has set as read-only
// so we'll embed a test image and call this function on it
	if (typeof myImg == 'undefined'){ return false }

// attempt to set and then read an img property
	myImg.width = 2;
	if (myImg.width != 2){
		alert('Your browser does not support resizing of images dynamically.\nYou will not be able to adjust the page layout.\nPlease see the Help link.');
		return false;
	}
	else { return true }
}

function warnInvalid(){ alert("Values cannot be assigned below 1") }