function check_move(){
	var msg = '';
	var focus_field = '';

	// Data validity parameter definitions:
	// At least one number which may or may not be prefixed with DHS.
	var good_cbid = /(^DHS[0-9]+$)|(^[0-9]+$)/i;
	// From 0 to 6 digits, optionally followed by 
	//  a decimal point and up to 2 more digits.
	// Removed negative option (^-(\d{0,6})(\.\d{1,2})?$) 07-17-03. Karl
	var good_amount = /(^(\d{0,6})(\.\d{1,2})?$)/;
	// A date in the 21st century of the format mm-dd-yyyy.
	// I, Keith, changed the good_date from "var good_date = /^\d{2}-\d{2}-200\d{1}$/;" to "var good_date = /^\d{2}-\d{2}-20\d{2}$/;"
	var good_date = /^\d{2}-\d{2}-20\d{2}$/;
	// Get the current year.
	// Get the date furthest backwards we will accept.
	earliestday = new Date();
	earliestday.setDate(earliestday.getDate() - 31);
	the_month = (earliestday.getMonth()+1).toString();
	if ( the_month.length == 1 ) {
		 the_month = "0" + the_month;
	}
	the_day = earliestday.getDate().toString();
	if ( the_day.length == 1 ) {
		 the_day = "0" + the_day;
	}
	the_year = earliestday.getYear();
	the_year_string = the_year.toString();
	if ( the_year_string.length < 4 ) {
		 the_year = the_year + 1900;
	}
	earliestday = the_year + "-" + the_month + "-" + the_day;

	// Get the date furthest ahead we will accept.
	latestday = new Date();
	latestday.setDate(latestday.getDate() + 1);

	the_month = (latestday.getMonth()+1).toString();
	if ( the_month.length == 1 ) {
		 the_month = "0" + the_month;
	}
	the_day = latestday.getDate().toString();
	if ( the_day.length == 1 ) {
		 the_day = "0" + the_day;
	}
	the_year = latestday.getYear();
	the_year_string = the_year.toString();
	if ( the_year_string.length < 4 ) {
		 the_year = the_year + 1900;
	}
	latestday = the_year + "-" + the_month + "-" + the_day;

	the_date = self.document.transaction.trans_date.value.substr(6, 4)
				 + "-" + 
				self.document.transaction.trans_date.value.substr(0, 2) 
				 + "-" + 
				self.document.transaction.trans_date.value.substr(3, 2);
	// Run the validity tests as defined above.
	// Reverse order so that focus gets set on the leftmost error.
	if ( (!good_date.test(self.document.transaction.trans_date.value))
			|| (the_date < earliestday)
			|| (the_date > latestday) ) { 
		msg = msg + "Date error...\n- Use the format mm-dd-yyyy (ex. 05-31-2005).\n- Confirm that the month and year are correct.\n- No more than 31 days behind or 1 day ahead.\n\n";
		focus_field = 'self.document.transaction.trans_date';
	}
	if (( !good_amount.test(self.document.transaction.amount.value)) || self.document.transaction.amount.value == '' ) {
		msg = msg + "Amount error...\n- Letters are not allowed.\n- Positive values up to 999999.99 only.\n- No more than 2 decimal places.\n\n";
		focus_field = 'self.document.transaction.amount';
	}
// Now that we accept aliases for MarketPlace merchants, this check is no longer valid.
//  Leaving it here just in case we change our minds later. (As of 11/17/2005)
//	if ( !good_cbid.test(self.document.transaction.cbid.value) ) {
//	msg = msg + "ClubBucks ID error...\n- Letters other than 'DHS' are not allowed.\n- No spaces before or after the number.\n\n";
//		focus_field = 'self.document.transaction.cbid';	
//	}

	// If we have any messages, show them.
	if (msg != '')
	{
		alert(msg);
	}
	// Otherwise, write the values to the Transaction List box.
	else
	{
		var one_transaction = self.document.transaction.cbid.value + ", " + self.document.transaction.amount.value + ", " + self.document.transaction.trans_date.value + "\n";
		document.trans_list.trans_list_text.value += one_transaction;
	
		// Clear the CBID and amount fields, set focus to the CBID field.
		self.document.transaction.cbid.value = '';
		self.document.transaction.amount.value = '';
		focus_field = 'self.document.transaction.cbid';
	}
	
	// Set focus back to the appropriate field.
	eval(focus_field + ".focus()");
}
