/*
This script depends on jQuery being loaded.
It also requires that jquery.cookie.js be loaded.
It handles creation of the inb1 cookie on an inbound link that contains that parameter.
As of 09/15/11, this is intended for a lead source that needs to get that value back when an applicant confirms.

This also sets the inb1 variable when the appropriate function call is used in the confirmation page.
This will allow a javascript code snippet to be used in the page that can access the value.
*/

var inb1;

function setInb1Var()
{
	inb1 = $.cookie("inb1");
}

function setInb1Ck()
{
	parseUri.options.strictMode = true;
	var _inb1 = parseUri(window.location.search).queryKey.inb1;
	if (typeof _inb1 == 'undefined'){
		return;
	}

	$.cookie("inb1", _inb1, { expires: 10, path: '/' });
}

