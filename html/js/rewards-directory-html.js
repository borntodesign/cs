/* javascript for use in the rewards directory html container page */

var qsParm = new Array();
/* we will be looking for these params in particular
	country
	state
	city
*/

/* we'll insert our iframe dynamically since we may need to set the URL search path */
function insertIframe(){
	var ifrm = document.getElementById('iframe_container').createElement('iframe');
	ifrm.href = '/cgi/ma_real_rpt2.cgi' + setReportUrl();
	
}

function qs() {
	var query = window.location.search.substring(1);
	var parms = query.split('&');
	for (var i=0; i < parms.length; i++) {
		var pos = parms[i].indexOf('=');
		if (pos > 0) {
			var key = parms[i].substring(0,pos);
			var val = parms[i].substring(pos+1);
			qsParm[key] = val;
		}
	}
}

function setReportUrl(){
	var qry = '';
	if (qsParm['country']){
		qry = '?country=' + qsParm['country'];
		// we will only do the other parameters if the parent is present
		if (qsParm['state']){
			qry += '&state=' + qsParm['state'];
			if (qsParm['city']){
				qry += '&city=' + qsParm['city'];
			}
		}
	}
	return qry;
}