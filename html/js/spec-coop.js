// spec-coop.js - a collection of code to work with the special coop transfer interface

/*
For marketplace merchants we calculate 25% PP off 80% of the commission
FOR offline merchants we calculate 20% off 100% commission
So... that means the same thing as far a calculating PP.
Since we are only paying PP on Coop purchases, we will display these things using a fixed formula with figures plugged in.

12/15/15 disabling calls to setPPValue since that field is gone away now that this is an admin only interface
*/

var native_comm = 0;
var usd_comm = 0;
var xrate = 1;
var mincomm = 0;
var my_form;

function ensureMinimumCommission(ctl, frm)
{
	my_form = frm;

	if (native_comm == 0)
	{
		// do nothing
	}
	else if (mincomm == 0)
	{
		// do nothing
	}
	else if (ctl.value < mincomm)
	{
		alert('The minimum USD commission must be at least: ' + mincomm);
		ctl.value = mincomm;
	}
	usd_comm = ctl.value;
//	setPPValue();
}

function Set_native_comm()
{
	native_comm = document.getElementById('sale_price_' + my_form).value * commission_percentage;
}

function setNativeCommission(frm)
{
	my_form = frm;
	Set_native_comm();
	document.getElementById('native_comm_' + my_form).value = native_comm;
	setUSDCommission();
}

function setPPValue()
{
	var pp_value = usd_comm * 0.20 * pp_multiplier;
	document.getElementById('pp_value_' + my_form).value = pp_value;
}

function Set_usd_comm()
{
	usd_comm = sprintf("%01.2f",native_comm/xrate);
	mincomm = usd_comm;
}

function setUSDCommission()
{
	Set_xrate();
	document.getElementById('xr_display_' + my_form).value = xrate;
	Set_usd_comm();
	document.getElementById('usd_comm_' + my_form).value = usd_comm;
//	setPPValue();
}

function Set_xrate()
{
	var xrcbo = document.getElementById('sale_price_currency_' + my_form);
	xrate = xrates[ xrcbo.options[xrcbo.selectedIndex].value ];
}

function specrst()
{
	document.forms.mainfrm.recipient.value = '';
	document.forms.mainfrm.num_to_transfer.value = '';
}

/*
function transferTypeChange(cbo, frm)
{
	my_form = frm;
	var pn = cbo.parentNode.parentNode;
	alert('pn:'+pn);
	if (cbo.options[cbo.selectedIndex].value == 5)	// we selected other
	{
		pn.style.display = 'table-row';
	}
	else
	{

		document.getElementsByName('other_transfer_reason').item(0).value = '';
		pn.style.display = 'none';
	}
}
*/
