//console.log("start payment online");
var times_through = 0;
var percentages = new Array();
var pctlabels = ['1%','2%','3%','4%','5%','6%','7%','8%','9%','10%','15%','20%','25%','30%','35%','40%'];
var pctamt = ['.01','.02','.03','.04','.05','.06','.07','.08','.09','.10','.15','.20','.25','.30','.35','.40'];
percentages['platinum'] = new Array();

for (ii=0; ii<pctlabels.length; ii++) {
	if (package_info['platinum'][2] == pctlabels[ii] || times_through == 1) {
		percentages['platinum'][pctlabels[ii]] = pctamt[ii];
		times_through = 1;
	}
}

        times_through = 0;
        percentages['gold'] = new Array();
        for (ii=0; ii<pctlabels.length; ii++) {
             if (package_info['gold'][2] == pctlabels[ii] || times_through == 1) {
                 percentages['gold'][pctlabels[ii]] = pctamt[ii];
                 times_through = 1;
             }
        }
        times_through = 0;
        percentages['silver'] = new Array();
        for (ii=0; ii<pctlabels.length; ii++) {
             if (package_info['silver'][2] == pctlabels[ii] || times_through == 1) {
                 percentages['silver'][pctlabels[ii]] = pctamt[ii];
                 times_through = 1;
             }
        }
        times_through = 0;
        percentages['bronze'] = new Array();
        for (ii=0; ii<pctlabels.length; ii++) {
             if (package_info['bronze'][2] == pctlabels[ii] || times_through == 1) {
                 percentages['bronze'][pctlabels[ii]] = pctamt[ii];
                 times_through = 1;
             }
        }
        times_through = 0;
        percentages['basic'] = new Array();
        for (ii=0; ii<pctlabels.length; ii++) {
             if (package_info['basic'][2] == pctlabels[ii] || times_through == 1) {
                 percentages['basic'][pctlabels[ii]] = pctamt[ii];
                 times_through = 1;
             }
        }
        times_through = 0;
	
//console.log("start payment online 2");
	
 	/*
	* These functions pertain to package options.
	*
	* 
	* $.each(selectValues, function(key, value)
	*		 {   
	*		      $('#mySelect').
	*		           append($("<option/>"), {
	*		               value: key,
	*		               text: value});
	*		 });
	* 
	* $('#mySelect').empty()
	* 
	 */
		function disableSpecial()
		{
			$('#special').attr("disabled","disabled");
			$("#special > option").removeAttr("selected");
		}
		
		function disablePercentageOff()
		{
			$('#percentage_off').attr("disabled","disabled");
			$("#percentage_off > option").removeAttr("selected");
		}
		
		function clearPercentageOfSale()
		{
			$("#percentage_off").removeAttr("selected");
		}

	function setPackageBenefits(package_id)
	{

		package_id = parseInt(package_id);
console.log("start setPackageBenefits: package_id: "+package_id);
		var package = '';
		$('#free_reward_merchant_benefits').toggle(true);
		
		switch(package_id)
		{
			case package_info['basic'][3]:
				package = 'basic';
				$('#payment_types').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
				$('#percentage_off').removeAttr("disabled");
				if (times_through > 0) {
					$('#flat_fee_off').removeAttr("value");
					$("[name=discount_subtype]").filter('[value="2"]').prop("checked", true);
				    clearPercentageOfSale();
				}
				
				paymentRequirements(document.getElementById('pay_payment_method'));
				
				var selected_value = $("select[name='percentage_off'] option:selected").val();
				$("select[name='percentage_off']").empty()
				$("select[name='percentage_off']").append($("<option />"));
				for ( var index_key in percentages['basic'] )
				{
console.log('selected_value: '+selected_value+' percentages[basic][index_key]:'+percentages['basic'][index_key]);
					if (selected_value == percentages['basic'][index_key])
					{
						var make_selected = " selected=\"selected\" ";
					}
					else
					{
						var make_selected = "";
					}
					
					$("select[name='percentage_off']").append($("<option value='" + percentages['basic'][index_key] +  "' " + make_selected + " >" + index_key + "</option>"));
				}
				
				$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
                                times_through = 1;
//                                $('#blurb').attr('maxlength', package_info['basic'][4]);
				
				break;
			case package_info['bronze'][3]:
                                package = 'bronze';
				$('#payment_types').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
                                $('#percentage_off').removeAttr("disabled");
                                if (times_through > 0) {
                                    $('#flat_fee_off').removeAttr("value");
			  	    $("[name=discount_subtype]").filter('[value="2"]').prop("checked", true);
				    clearPercentageOfSale();
                                }
				
				paymentRequirements(document.getElementById('pay_payment_method'));
				
				var selected_value = $("select[name='percentage_off'] option:selected").val();
				$("select[name='percentage_off']").empty()
				$("select[name='percentage_off']").append($("<option />"));
				for ( var index_key in percentages['bronze'] )
				{
                                        if (selected_value == percentages['bronze'][index_key]) {
                                            var make_selected = " selected=\"selected\" ";
                                        }
                                        else {
                                            var make_selected = "";
                                        }
					$("select[name='percentage_off']").append($("<option value='" + percentages['bronze'][index_key] +  "' " + make_selected + " >" + index_key + "</option>"));
				}
				
				$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
                                times_through = 1;
//                                $('#blurb').attr('maxlength', package_info['bronze'][4]);
				
				break;
			case package_info['silver'][3]:
                                package = 'silver';
				$('#payment_types').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
                                $('#percentage_off').removeAttr("disabled");
                                if (times_through > 0) {
                                    $('#flat_fee_off').removeAttr("value");
			  	    $("[name=discount_subtype]").filter('[value="2"]').prop("checked", true);
				    clearPercentageOfSale();
                                }

				$('#package_options_divider').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
				paymentRequirements(document.getElementById('pay_payment_method'));
				
				var selected_value = $("select[name='percentage_off'] option:selected").val();
				$("select[name='percentage_off']").empty()
				$("select[name='percentage_off']").append($("<option />"));
				for ( var index_key in percentages['silver'] )
				{
                                        if (selected_value == percentages['silver'][index_key]) {
                                            var make_selected = " selected=\"selected\" ";
                                        }
                                        else {
                                            var make_selected = "";
                                        }
					$("select[name='percentage_off']").append($("<option value='" + percentages['silver'][index_key] +  "' " + make_selected + " >" + index_key + "</option>"));
				}
				
				$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
                                times_through = 1;
//                                $('#blurb').attr('maxlength', package_info['silver'][4]);
				
				break;
			case package_info['gold'][3]:
                                package = 'gold';
				$('#payment_types').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
                                $('#percentage_off').removeAttr("disabled");
                                if (times_through > 0) {
                                    $('#flat_fee_off').removeAttr("value");
			  	    $("[name=discount_subtype]").filter('[value="2"]').prop("checked", true);
				    clearPercentageOfSale();
                                }

				$('#package_options_divider').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
				
				paymentRequirements(document.getElementById('pay_payment_method'));
				
				var selected_value = $("select[name='percentage_off'] option:selected").val();
				$("select[name='percentage_off']").empty()
				$("select[name='percentage_off']").append($("<option />"));
				for ( var index_key in percentages['gold'] )
				{
                                        if (selected_value == percentages['gold'][index_key]) {
                                            var make_selected = " selected=\"selected\" ";
                                        }
                                        else {
                                            var make_selected = "";
                                        }
					$("select[name='percentage_off']").append($("<option value='" + percentages['gold'][index_key] +  "' " + make_selected + " >" + index_key + "</option>"));
				}
				
				$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
                                times_through = 1;
//                                $('#blurb').attr('maxlength', package_info['gold'][4]);
				
				break;
			case package_info['platinum'][3]:
                                package = 'platinum';
				$('#payment_types').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				$('#free_reward_merchant_benefits').toggle(true);
                                $('#percentage_off').removeAttr("disabled");
                                if (times_through > 0) {
                                    $('#flat_fee_off').removeAttr("value");
			  	    $("[name=discount_subtype]").filter('[value="2"]').prop("checked", true);
				    clearPercentageOfSale();
                                }

				$('#package_options_divider').toggle(true);
				$('#reward_merchant_benefits').toggle(false);
				
				paymentRequirements(document.getElementById('pay_payment_method'));
				
				var selected_value = $("select[name='percentage_off'] option:selected").val();
				$("select[name='percentage_off']").empty()
				$("select[name='percentage_off']").append($("<option />"));
				for ( var index_key in percentages['platinum'] )
				{
                                        if (selected_value == percentages['platinum'][index_key]) {
                                            var make_selected = " selected=\"selected\" ";
                                        }
                                        else {
                                            var make_selected = "";
                                        }
					$("select[name='percentage_off']").append($("<option value='" + percentages['platinum'][index_key] +  "' " + make_selected + " >" + index_key + "</option>"));
				}
				
				$("select[name='percentage_off'] option[value='" + selected_value + "']").attr("selected", "selected");
                                times_through = 1;
//                                $('#blurb').attr('maxlength', package_info['platinum'][4]);
				
				break;
				
		}
	}

	
/*
 * These functions pertain to Payment Options.
 */
	
	function paymentRequirements(PaymentMethodSelectBox)
	{
		creditCardVisibility(true);
		clubAccountVisibility(false);
		internationalFundsTransferVisibility(false);
	$("input[name='merchant_package']").change(
		function()
		{
			setPackageBenefits(this.value);
		}
	);

	$("input[name='merchant_package']:checked").each(
			function()
			{
				setPackageBenefits(this.value);
			}
		);
	



	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	
	
	/**
		This resets the package options when the merchant changes 
	*/
	$('#_membership_member_id').change(function() {
		$("#member_id").removeAttr("disabled");
		$("#referral_id").val('');		
		$("#referral_id").attr("disabled","disabled");
	});

	
	$('#_membership_referral_id').change(function() {
		$("#referral_id").removeAttr("disabled");
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
	});

	$('#_membership_new_member').change(function() {
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
		$("#referral_id").val('');
		$("#referral_id").attr("disabled","disabled");
	});


	$("input[id='_membership_referral_id']:checked").each(function(){
		$("#referral_id").removeAttr("disabled");
	});


	$("input[id='_membership_member_id']:checked").each(function(){
		$("#member_id").removeAttr("disabled");
	});
	
	
	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	
	
}

