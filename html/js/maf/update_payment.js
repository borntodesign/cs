

function MM_preloadImages()
{ //v3.0
	var d=document; 
	if(d.images)
	{
		if(!d.MM_p) d.MM_p=new Array();
	}
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
    for(i=0; i < a.length; i++)
    {
    	if (a[i].indexOf("#")!=0)
    	{
    		d.MM_p[j]=new Image; 
    		d.MM_p[j++].src=a[i];
    	}
    }
}

function MM_swapImgRestore()
{ //v3.0
  var i,x,a=document.MM_sr;
	for(i=0 ;a && i < a.length && (x=a[i]) && x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d)
{ //v4.01
	var p,i,x;
	if(!d) d=document;
	if((p=n.indexOf("?")) > 0 && parent.frames.length)
	{
		d=parent.frames[n.substring(p+1)].document;
		n=n.substring(0,p);
	}
	
	if(!(x=d[n]) && d.all) x=d.all[n];
	for (i=0; !x && i < d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x && d.layers && i < d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n);
	return x;
}

function MM_swapImage()
{ //v3.0
  var i,j=0,x,a=MM_swapImage.arguments;
	document.MM_sr=new Array;
	for(i=0; i < (a.length-2); i+=3)
		if ((x=MM_findObj(a[i]))!=null)
		{
			document.MM_sr[j++]=x;
			if(!x.oSrc) x.oSrc=x.src; 
			x.src=a[i+2];
		}
}





	function creditCardVisibility(show)
	{
		$('#credit_card_payment_options').toggle(show);
	}
	
	
	function clubAccountVisibility(show)
	{
		$('#club_account_payment_options').toggle(show);
	}

	function internationalFundsTransferVisibility(show)
	{
		$('#international_funds_transfer_payment_options').toggle(show);
	}
	

	

	
	
	
	
	

$(document).ready(function(){
	
	/**
	 * @author Keith
	 * @description	This will allow the "Submit" button to be pressed only once.
	 */
	//$('input[type="image"]').attr("disabled", false); // This is probably not needed, but you can enable it if you desire.
	$("form").submit(function(){
		$('input[type="image"]').attr("src", "/images/btn/btn_processing_91x27.png");
		$('input[type="image"]').attr("disabled", true);
		return true;
	});

	
	 $("input[name='merchant_type']:checked").each(function(){
		
		
	 	if(this.id == 'merchant_type')
		{
			$('#reward_merchant_benefits').toggle(true);
			$('#free_reward_merchant_benefits').toggle(false);
		}
		
	 	if(this.id == 'free_merchant_type')
		{
			$('#reward_merchant_benefits').toggle(false);
			$('#free_reward_merchant_benefits').toggle(true);
		}

	});


	/**
		This changes the visibility of the package options for the flat fee merchants.
	*/
	$('#merchant_type').change(function() {
	 	$('#reward_merchant_benefits').toggle(true);
		$('#free_reward_merchant_benefits').toggle(false);
		
	});
	
	/**
		This changes the visibility of the package options for the pay as you go merchants.
	*/
	$('#free_merchant_type').change(function() {
 		$('#reward_merchant_benefits').toggle(false);
		$('#free_reward_merchant_benefits').toggle(true);
	});
	
	
	/********************************************************
	These set the available options for reward merchants
	*/
	$("input[name='discount_type']").change(
		function(){
			setPackageBenefits(this.value);
		}
	);

	$("input[name='discount_type']:checked").each(
			function()
			{
				setPackageBenefits(this.value);
			}
		);
	
	$('#offline_reward_discounts_percentage_off').change(function() {
		$("#percentage_off").removeAttr("disabled");
		disableFlatFeeOff();
		disableSpecial();
		clearPercentageOfSale()
	});
	
	$('#offline_reward_discounts_flat_fee_off').change(function() {
		disablePercentageOff();
		$('#flat_fee_off').removeAttr("disabled");
		disableSpecial();
		clearPercentageOfSale()
	});
	
	$('#offline_reward_special').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		$('#special').removeAttr("disabled");
		clearPercentageOfSale()
	});
	
	$('#percentage_of_sale').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		disableSpecial();
		$("input[name='special']").removeAttr("checked");
	});
	
	
	/*
	 * These set the available options for reward merchants
	 */

	$("textarea[name='discount_blurb']").maxlength({'feedback' : '.charsLeft'});
	
	adjustInitialStateCtlFocus(); 
	setSelectPrompt('- Please select from this list -');
	
	
	MM_preloadImages('/images/btn/btn_processing_91x27.png','/images/nav/maf/nav_faq_1.gif','/images/nav/mi/nav_transaction_2.gif','/images/nav/mi/nav_profile_2.gif','/images/nav/mi/nav_payment_2.gif','/images/nav/mi/nav_setup_2.gif','/images/nav/mi/nav_partner_2.gif');
	
});

 



