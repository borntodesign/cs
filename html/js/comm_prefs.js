function ckSubmit(frm){
	var missing = 'Missing';
	var account_mb = document.getElementById('aplbl').innerHTML;
	var account_pp = document.getElementById('eplbl').innerHTML;
	var myopts = frm.payment_option;
	var mychoice = 0;
	for (var i = 0; i < frm.length; i++){
		if (myopts[i].checked){
			mychoice = myopts[i].value;
			break;
		}
	}
	if (mychoice == 8 && frm.account_ap.value == ''){
		alert(missing + ': ' + account_mb);
		frm.account_mb.focus();
		return false;
	} else if (mychoice == 9 && frm.account_ep.value == ''){
		alert(missing + ': ' + account_pp);
		frm.account_pp.focus();
		return false;
	}
	return true;
}

// when a radio is clicked, we will highlight the row if the row is not the current payment option
function hiliteRow(ctl){
	// look up until we find the tr node
	var p = ctl.parentNode;

	while (p.nodeName != 'TR'){
		p = p.parentNode;
	}

	// we won't muck with the row that is our current option
	if ( p.className.match('current') ){ return; }

	// if we already have a class, then add one, otherwise just set it
	p.className = (p.className ? p.className + ' ' : '') + 'working';

	// un-hilight any other rows
	var rows = document.getElementById('mainTbl').getElementsByTagName('tr');
	for (var x=0; x < rows.length; x++){
		// skip the real working row
		if (p == rows[x]){ continue; }

		// if no class has been defined, then skip it
		if (! rows[x].className){ continue; }

		// remove the working class and a leading space if present
	// BTW did I say that this method has got to be the most frustrating thing when you expect it to replace in place like perl ############
		rows[x].className = rows[x].className.replace(/\s*working/,'');
	}
}