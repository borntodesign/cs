// appx.js - some specific stuff for appx

var newwin;
function helpwin(href)
{
	newwin = window.open(href,'helpwin','width=300,height=200,scrollbars=yes,resizable=yes,status=no');
	newwin.focus();
}

function closehelpwin()
{
	if (newwin) newwin.close();
}

function ck_set_focus(ctl_name)
{
// check if the argument can have focus and if so, do it
	var ctl;
	if (document.getElementsByName){
		ctl = document.getElementsByName(ctl_name)[0];
	}
	if (ctl && ctl.focus) ctl.focus();
}
