/* tmp-footer.js
a temporary common footer to use on static pages
*/

document.write('\
      <table class="common_nav_footer"><tr>\
          <td><a href="/">Home</a></td>\
          <td><a href="/about.xml" onclick="window.open(this.href);return false;">About Us</a></td>\
          <td><a href="/mall_listing.xml" onclick="window.open(this.href);return false;">List your Store at the ClubShop Mall</a></td>\
          <td><a href="/nonprofit.xml" onclick="window.open(this.href);return false;">Non-Profit Fund Raising</a></td>\
          <td><a href="/termsofuse.html" onclick="window.open(this.href);return false;">Terms of Use</a></td>\
          <td><a href="/privacy.html" onclick="window.open(this.href);return false;">Privacy Policy</a></td>\
        </tr></table>');