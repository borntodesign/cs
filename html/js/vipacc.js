function SelectedRadio(buttonGroup, message) {
   // returns the array number of the selected radio button or -1 if no button is selected
   if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return true;
         }
      }
   } else {
      if (buttonGroup.checked) { return true; } // if the one button is checked, return zero
   }
   // if we get to this point, no radio button is selected
   alert(message);
   return false;
}

function CheckOK(question, message)
{
	if ( eval("document.forms[0]." + question + "_OK.checked") ){
		eval("document.forms[0]." + question + "_response.value='" + message + "'");
	}
}

function CheckArb(message)
{
	if ( document.forms[0].status[2].checked && document.forms[0].notes.value == "" ){
		alert(message);
   		return false;
	}
            return true;
}
