

	function creditCardVisibility(show)
	{
		
		$('#creditCard').toggleClass('style5', show);
		$('#creditCardNumber').toggleClass('style5', show);
		$('#ccvNumber').toggleClass('style5', show);
		$('#cardExpirationDate').toggleClass('style5', show);
		$('#nameOnCard').toggleClass('style5', show);
		$('#billingAddress').toggleClass('style5', show);
		$('#billingCity').toggleClass('style5', show);
		$('#billingCountry').toggleClass('style5', show);
		$('#billingState').toggleClass('style5', show);
		$('#billingZip').toggleClass('style5', show);		
		
		
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#creditCardExplanationRow').toggle(show);
		}
		else
		{
			$('#creditCardExplanationRow').toggle(false);
		}
		
		$('#creditCardTypeRow').toggle(show);
		$('#creditCardNumberRow').toggle(show);
		$('#creditCardCCVRow').toggle(show);
		$('#creditCardExpirationDateRow').toggle(show);
		$('#creditCardNameRow').toggle(show);
		$('#creditCardDividerRow').toggle(show);
		$('#creditCardDividerRowOne').toggle(show);
		$('#creditCardAddressRow').toggle(show);
		$('#creditCardCityRow').toggle(show);
		$('#creditCardCountryRow').toggle(show);
		$('#creditCardStateRow').toggle(show);
		$('#creditCardPostalCodeRow').toggle(show);
	
		
	}
	

	function ecoCardVisibility(show)
	{
		$('#ecoCardNumber').toggleClass('style5', show);		
		
		$('#ecoCardAccountNumberRow').toggle(show);
		$('#ecoCardExplanationRow').toggle(show);
	}

	
	function clubAccountVisibility(show)
	{
		$('#clubAccountNumber').toggleClass('style5', show);		
		
		$('#clubAccountNumberRow').toggle(show);
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#clubAccountExplanationRow').toggle(show);
			
			$('#clubAccountExplanationOfflineRewardsRow').toggle(false);
		}
		else
		{
			$('#clubAccountExplanationRow').toggle(false);
			
			$('#clubAccountExplanationOfflineRewardsRow').toggle(show);
		}
		
		
	}

	function internationalFundsTransferVisibility(show)
	{
		//$('#internationalFundsTransferNumber').toggleClass('style5', show);	
		$('#internationalFundsTransferNumberRow').toggle(show);
		
		
		if($("input[name='merchant_type']:checked").val()  == 'offline')
		{
			$('#internationalFundsTransferExplanationRow').toggle(show);
			$('#internationalFundsTransferOfflineRewardsExplanationRow').toggle(false);
		}
		else
		{
			$('#internationalFundsTransferExplanationRow').toggle(false);
			$('#internationalFundsTransferOfflineRewardsExplanationRow').toggle(show);
		}
		
	}
	
	/*
	 * These functions pertain to Payment Options.
	 */
		
		function paymentRequirements(PaymentMethodSelectBox)
		{
			switch(PaymentMethodSelectBox[PaymentMethodSelectBox.selectedIndex].value)
			{
				case 'cc':
					creditCardVisibility(true);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
					break;
				
				case 'ec':
					creditCardVisibility(false);
					ecoCardVisibility(true);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
					break;		
				
				
				case 'ca':
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(true);
					internationalFundsTransferVisibility(false);
					break;
				
				case 'if':
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(true);
					break;
				
				default:
					creditCardVisibility(false);
					ecoCardVisibility(false);
					clubAccountVisibility(false);
					internationalFundsTransferVisibility(false);
				
			}
			
		}
	 
		
	 	/*
		* These functions pertain to package options.
		*
		* 
		* $.each(selectValues, function(key, value)
		*		 {   
		*		      $('#mySelect').
		*		           append($("<option/>"), {
		*		               value: key,
		*		               text: value});
		*		 });
		* 
		* $('#mySelect').empty()
		* 
		 */
			function disableBasic()
			{

				$('.basic').attr("disabled","disabled");
				$("input[class='basic']").removeAttr("checked");
				$("option[class='basic']").removeAttr("selected");
			}

			function disableBronze()
			{
				$("option[class='bronze']").attr("disabled","disabled");
				$("option[class='bronze']").removeAttr("selected");
			}
			
			function disableSilver()
			{

				$('.silver').attr("disabled","disabled");
				$("input[class='silver']").removeAttr("checked");
				$("option[class='silver']").removeAttr("selected");
			}
			
			
			function disableGold()
			{

				$('.gold').attr("disabled","disabled");
				$("input[class='gold']").removeAttr("checked");
				$("option[class='gold']").removeAttr("selected");
			}
			
			
			function disablePlatinum()
			{

				$('.platinum').attr("disabled","disabled");
				$("input[class='platinum']").removeAttr("checked");
				$("option[class='platinum']").removeAttr("selected");
			}
			
			function disableOfflineBronze()
			{
				$("option[class='bronze']").attr("disabled","disabled");
				$("option[class='bronze']").removeAttr("selected");
			}
			
			function disableSpecial()
			{
				$('#special').attr("disabled","disabled");
				$("#special > option").removeAttr("selected");
			}
			
			function disablePercentageOff()
			{
				$('#percentage_off').attr("disabled","disabled");
				$("#percentage_off > option").removeAttr("selected");
			}
			
			function clearPercentageOfSale()
			{
				$("#percentage_of_sale > option").removeAttr("selected");
			}

			function enableBasic()
			{
				$("option[class='basic']").removeAttr("disabled");
			}

			function enableBronze()
			{
				$("option[class='bronze']").removeAttr("disabled");
			}
			
			function enableOfflineBronze()
			{
				$("option[class='bronze']").removeAttr("disabled");
			}
			
			function enableSilver()
			{
				$("input[class='silver']").removeAttr("disabled");
				$("option[class='silver']").removeAttr("disabled");
			}

			function enableGold()
			{
				$("input[class='gold']").removeAttr("disabled");
				$("option[class='gold']").removeAttr("disabled");
			}

			function enablePlatinum()
			{
				$("input[class='platinum']").removeAttr("disabled");
				$("option[class='platinum']").removeAttr("disabled");
			}

		
$(document).ready(function(){
	/**
	 * @author Keith
	 * @description	This will allow the "Submit" button to be pressed only once.
	 */
	//$('input[type="image"]').attr("disabled", false); // This is probably not needed, but you can enable it if you desire.
	$("form").submit(function(){
		$('input[type="image"]').attr("src", "/images/btn/btn_processing_91x27.png");
		$('input[type="image"]').attr("disabled", true);
		return true;
	});
	
	
	/********************************************************
		These set the available options for reward merchants
	*/
	
	$("input[name='merchant_package']").change(
		function()
		{
			setPackageBenefits(this.value);
		}
	);

	$("input[name='merchant_package']:checked").each(
			function()
			{
				setPackageBenefits(this.value);
			}
		);
	



	$('#offline_reward_discounts_percentage_off').change(function() {
		$("#percentage_off").removeAttr("disabled");
		disableFlatFeeOff();
		disableSpecial();
		clearPercentageOfSale();
	});
	
	$('#offline_reward_discounts_flat_fee_off').change(function() {
		disablePercentageOff();
		$('#flat_fee_off').removeAttr("disabled");
		disableSpecial();
		clearPercentageOfSale();
	});
	
	$('#offline_reward_special').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		$('#special').removeAttr("disabled");
		clearPercentageOfSale();
	});
	
	$('#percentage_of_sale').change(function() {
		disablePercentageOff();
		disableFlatFeeOff();
		disableSpecial();
		$("input[name='special']").removeAttr("checked");
	});
	
	/********************************************************/
	
	$('#percentage_off').change(function(){
		$("input[name='special']").removeAttr('checked');
		$("input[name='flat_fee_off']").val('');	
	});

	$('#flat_fee_off').change(function(){
		$("input[name='special']").removeAttr('checked');
		$("#percentage_off option").removeAttr("selected");
	});
	
	$("input[name='special']").change(function(){
		$("input[name='flat_fee_off']").val('');
		$("#percentage_off option").removeAttr("selected");
	
	});
	
	
	
	
	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	
	
	/**
		This resets the package options when the merchant changes 
	*/
	$("input[name='merchant_type']").change(function(){
		
		$("input[name='flat_fee_off']").val('');
	
		$("#percentage_off option").removeAttr("selected");
		
		$("input[name='special']").removeAttr('checked');
		
		$("input[name='merchant_package']").removeAttr('checked');
		
		$("#percentage_of_sale option").removeAttr("selected");
		
		$("#exception_percentage_of_sale option").removeAttr("selected");
		
	});
	
	
	$('#_membership_member_id').change(function() {
		$("#member_id").removeAttr("disabled");
		$("#referral_id").val('');		
		$("#referral_id").attr("disabled","disabled");
	});

	
	$('#_membership_referral_id').change(function() {
		$("#referral_id").removeAttr("disabled");
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
	});

	$('#_membership_new_member').change(function() {
		$("#member_id").val('');
		$("#member_id").attr("disabled","disabled");
		$("#referral_id").val('');
		$("#referral_id").attr("disabled","disabled");
	});


	$("input[id='_membership_referral_id']:checked").each(function(){
		$("#referral_id").removeAttr("disabled");
	});


	$("input[id='_membership_member_id']:checked").each(function(){
		$("#member_id").removeAttr("disabled");
	});
	
	
	$("input[name='biz_description']").focus(function(){
		$('#category_search').contents().find("input[name='search_value']").focus();
	});
	

});




