function is_loaded(cur_show_window, num_attempts)
{
	show_window = cur_show_window;
	attempts = num_attempts;

       // If the get_states.cgi script hasn't started or the Please Wait
       // window hasn't been closed, continue trying for
       //   up to max_attempts seconds(1 seconds = 1000 ms).
       //   NOTE: max_attempts defined in load_states.js script.
       if ((cgi_started == 0) && (attempts < max_attempts) && ((!nuwin.closed) || (show_window == 'win_no')))
       {
		++attempts;
               setTimeout( "is_loaded(show_window, attempts)", 1000);
       }
       // If the cgi script has started and the window is still open.
       else if ((cgi_started == 1) && ((!nuwin.closed) || (show_window == 'win_no')))
       {
                frm=document.forms[0];
                frm.state.options.length=0;
                frm.state.options.length = _states_.length;
		// If we returned states, load them into the form.
                if (_states_.length != 0)
                {
                	for (num_states=0; num_states < _states_.length; num_states++)
                	{
                       		frm.state.options[num_states].text = _states_[num_states][1];
                      		frm.state.options[num_states].value = _states_[num_states][0]
                	}
                	frm.final_state.value = 'Select a state from the list above.';
                }
                // If there were no states loaded for whatever reason.
		else
		{
       		      	frm.state.options.length=1;
       		      	frm.state.options[0].text = 'No list of states available.';
       		      	frm.state.options[0].value = '';
	             	frm.state.options[0].selected = 'true';
             		frm.final_state.value = 'Please type your State name here.';
		}
             	nuwin.close();
       }
        // If the window didn't open at all even after 5 attempts,
        //     or the get_states.cgi ran, but never loaded the states.
        else if ((attempts >= max_attempts) || ((cgi_started == 1) && (num_states == 0)))
        {
//alert('Try another JS method:' + '\n' + 'attempts = ' + attempts + '\n' + 'num_states = ' + num_states + '\n' + 'cgi_started = ' + cgi_started + '\n' + '\n' + 'nuwin.closed = ' + nuwin.closed + '\n' + 'show_window = ' + show_window);
                // If we had a window open, close it and try again.
		if (show_window == 'win_yes')
		{
             		nuwin.close();
			attempts = 0;
			load_states(parent.country, "win_no");
		}
                // If we had no window open, we're done trying.
		else
		{
			frm=document.forms[0];
       		      	frm.state.options.length=1;
       		      	frm.state.options[0].text = 'No list of states available.';
       		      	frm.state.options[0].value = 'NULL';
	             	frm.state.options[0].selected = 'true';
             		frm.final_state.value = 'Please type your State name here.';
			attempts = 0;
		}
        }

	// We should never get this error, but just in case let's get some data.
        else
        {
		alert('Error - ' + '\n' + 'attempts = ' + attempts + '\n' + 'num_states = ' + num_states + '\n' + 'cgi_started = ' + cgi_started + '\n' + '\n' + 'nuwin.closed = ' + nuwin.closed + '\n' + 'show_window = ' + show_window);
        }
}
