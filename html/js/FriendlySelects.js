var arr = new Array();

function InitializeState(list)
{
	// apparently, javascript will not return a valid length if the subscripts are alphanumeric
	// ie. array[column_name]
	// so we'll preserve our list in element -zero- that way we can iterate through it later
	arr[0] = list
	for(i=0; i< arr[0].length; i++)
	{
		// by defining this here, it can be changed easily if we need greater flexibility
		// in defining the actual form at some other date
		ctl = eval ('document.forms[0].' + list[i]);
		
		//arr[list[i]] = new Array(eval ('document.forms[0].' + list[i] + '.options.length'));
		arr[ctl.name] = new Array(ctl.length);
		RecordSettings(ctl);

	}
}
	
function ModifyList(ctl)
{
	// we have to retain this value since it appears to change once we start mucking with it below
	var si = ctl.selectedIndex;
	for (x=0; x<ctl.length; x++)
	{
		if (si != x)
		{
		//alert('si= '+si+' x= '+x+' option= '+ctl.options[x].selected+' arr value= '+arr[ctl.name][x]);
			ctl.options[x].selected = arr[ctl.name][x];
		}
		else ctl.options[x].selected = ! arr[ctl.name][x];
	}
	RecordSettings(ctl);
}
	
function RecordSettings(ctl)
{
		for (x=0; x<ctl.length; x++) arr[ctl.name][x] = ctl.options[x].selected;
}
