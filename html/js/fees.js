$(function() {

	// when we first load, let's just show the line applicable to the user... if we know their country
	if ((typeof my_country_code != 'undefined') && my_country_code > ''){
		$('#fulllistp').hide();
		$('#mainTblData tr').hide();
		$('#_' + my_country_code).show();
		$('#__' + my_country_code).show();
	}
	
	$('#showAll').click(function(){
		$('#fulllistp').show();
		$('#mainTblData tr').show();
	});
	
	$('a.jme').click(function() {
		var cc = $(this).html();
		$('#fulllistp').hide();
		$('#mainTblData tr').hide();
//		$('#mainTblData tr').not( $(this).closest('tr') ).hide();
		$('#_' + cc).show();
		$('#__' + cc).show();
	})
	
});


