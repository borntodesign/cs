/*
MemberFollowup.js
Some routines to use with the member-followup interface
*/

function formCk(frm){
	// if they haven't submitted anything, then there is no need to actually submit the form
	if (! (frm.followup.value || frm.reminder.value || frm.reminder_date.selectedIndex > 0)){
		return false;
	}
	else if (frm.reminder.value && ! frm.reminder_date.selectedIndex > 0){
		alert( vals['no_reminder_date'] );
		return false;
	}
	return reminderToCheckActivities(frm);
}

function reminderToCheckActivities(frm){
	var ckd;

	// if they haven't entered any followup comments, or there are no activities to followup on, then there is no need for a reminder
	if (! frm.activities || ! frm.followup.value){
		return true;
	}

// if there is only one item on the list, there is no length as there is no array
	if (! frm.activities.length){
		if (frm.activities.checked){
			ckd=1;
		}
	} else {
		for(var x=0; x < frm.activities.length; x++){
			if (frm.activities[x].checked){
				ckd=1;
				break;
			}
		}
	}
	
	if (frm.followup.value && ckd){
		return true;
	} else if ( confirm(vals['reminderToCheckActivities']) ){
		return true;
	} else {
		// we hit here if our confirm is "cancel"
		return false;
	}
}

function ckVipNotesSubmission(frm){
	if (! frm.vipnotes.value){
		alert(vals['missing_vipnotes']);
		return false;
	}
	return true;
}

// open new windows for the Quick Links
function openRptWindow(frm){
	var url = frm.href.options[frm.href.selectedIndex].value;
	if (url){
		return window.open(url, 'report');
	}
	return false;
}