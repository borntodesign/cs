// color attributes for our mouseover button events
var btn_on = new Array();
btn_on['reset'] = '#ffffff';
btn_on['select_all'] = 'ffffcc';
btn_on['cancel'] = '#ffaaaa';
btn_on['transfer'] = '#aaeeaa';

function ckSubmission(ME){
	if (! ME.pd.value)
	{
		alert(prompt1);
		ME.pd.focus();
		return false;
	}
	return true;
}

function _doit(frm)
{
	var ME = frm.transfer_type.selectedIndex;
//	if (ME == 0)
	if (frm.transfer_type.options[ME].value == '')
	{
		alert(select_xfer_type);
		return false;
	}

	if (ckSubmission(frm)){
		return true;
	} else {
		return false;
	}
}

function ListFrmSubmit(frm)
{
	for(var x=0; x<(frm.mvid.length || 1); x++)
	{
		if (frm.mvid[x].checked) return true;
	}
	alert('Please select at least one party to transfer.');
	return false;
}

function resetBtn(ME)
{
	// if we decide to do something more elaborate at least we have the function call in place
	ME.style.backgroundColor='';
}

function setBtn(ME,myfunc)
{
	ME.style.backgroundColor=btn_on[myfunc];
}

var helps = new Array();
helps['pp'] = "PP: Pay Points";
helps['ms'] = "MS: Mail Status\n2 = HTML mail preferred\n1 = Text mail preferred\n0 = Undeliverable\n-1 = Bad Email Address (BEM)\n-2 = Blocked Email Address";
helps['default'] = "There is no help defined for this item";

function whatIsThis(ME)
{
	if (! helps[ME])
	{
		ME='default';
	}
	alert(helps[ME]);
	
	return false;
}
