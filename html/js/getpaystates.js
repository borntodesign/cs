var preserve2 = new Array();
var select_prompt = '- Select a State/Province -';	// our default
var field_name2 = 'pay_state';	// our default

function adjustInitialPayStateCtlFocus(my_field_name)
{
// if the application has created a prepopulated state_select control,
//then we'll display that instead of the default state_text
	var x= new getObj('pay_state_select');
	if (x.obj.options.length <= 1) return;
	x= new getObj('pay_state_text');
	
	if (typeof my_field_name != 'null' && typeof my_field_name != 'undefined'){
		field_name2 = my_field_name;
	}
	else if (x.obj.name){
		field_name2 = x.obj.name;
	}
//	field_name2 = (typeof my_field_name != 'null' && typeof my_field_name != 'undefined') ? my_field_name  : x.obj.name;
	turnON('pay_state_select', field_name2);
	turnOFF('pay_state_text');
}

function getObj(name)
{
//alert('name='+name);
  if (document.getElementById)
  {
  	this.obj = document.getElementById(name);
	this.style = document.getElementById(name).style;
  }
  else if (document.all)
  {
	this.obj = document.all[name];
	this.style = document.all[name].style;
  }
}
function getPayStates(my_field_name)
{
// preserve2 the focus states in case they press the reset button
	preservePayFocus();

// field_name2 is the form name for the 'state' field
// it will default to 'state'
	if (typeof my_field_name != 'null' && typeof my_field_name != 'undefined') field_name2 = my_field_name;

// figure out what country is currently selected
// we are counting on there being a field with the ID of 'country'
	var x= new getObj('pay_country');
	var country = x.obj.options[x.obj.selectedIndex].value;
	if (country == '')
	{
		needs_pay_country(x);
	}
// since we should have been called by an onchange event,
// we'll reset the background color if it was changed
	else { resetPayCountryColor() }

// if we don't have a valid array defined for the country, we'll revert to the text field
	if (typeof sc[country] == 'null' || typeof sc[country] == 'undefined')
	{
		turnON('pay_state_text');
		turnOFF('pay_state_select');
		return;
	}

	x= new getObj('pay_state_select');
// this reinitializes the list
	x.obj.options.length=0;
// insert our prompt as line 1
	x.obj.options[0] = new Option(select_prompt, '');

	for (var y=0; y < sc[country].length; y++)
	{
		x.obj.options[y+1] = new Option(sn[country][y],sc[country][y]);
	}
	turnON('pay_state_select', field_name2);
	turnOFF('pay_state_text');
}

function needs_pay_country(x)
{
// we are going to assume that there is a field named 'country'
// if there is no country value defined, then we surely cannot look up any states
// the idea is to call this function on focus of the 'state' text field
	if (typeof x == 'undefined') x = new getObj('country');

// we could raise an alert box, but instead we'll just shift focus back
// this way we can avoid having to provide translatable alerts
	if (x.obj.options[x.obj.selectedIndex].value == '')
	{
		x.obj.focus();

	// save the existing background color
		preserve2['pay_country'] = x.obj.style.backgroundColor;
		x.obj.style.backgroundColor = 'yellow';
	}
}

function preservePayFocus()
{
// if we have initialized values, then we are done
	if (typeof preserve2['pay_state_text_display'] != 'undefined') return;
	var x = new getObj('pay_state_text');
	preserve2['pay_state_text_display'] = x.style.display;
	preserve2['pay_state_text_name'] = x.obj.name;
	x = new getObj('pay_state_select');
	preserve2['pay_state_select_display'] = x.style.display;
	preserve2['pay_state_select_name'] = x.obj.name;
// the reset doesn't even 'reset' the drop-down if it has been recreated
// so we'll simply save the controls
//	preserve2['pay_state_text'] = new getObj('state_text');
//	preserve2['pay_state_select'] = new getObj('state_select');
}

function restorePayStateFocus()
{
// the reset will not bring the proper control into focus, it will only restore values
// if we don't have initialized values, then we are done
	if (typeof preserve2['pay_state_text_display'] == 'undefined') return;

	var x = new getObj('pay_state_text');
	x.style.display = preserve2['pay_state_text_display'];
	x.obj.name = preserve2['pay_state_text_name'];
	x = new getObj('pay_state_select');
	x.style.display = preserve2['pay_state_select_display'];
	x.obj.name = preserve2['pay_state_select_name'];
/*
	var x = new getObj('pay_state_text');
	x.style = preserve2['pay_state_text'].style;
	x.obj.name = preserve2['pay_state_text'].obj.name;
	x = new getObj('pay_state_select');
	x.style.display = preserve2['pay_state_select'].style.display;
	x.obj.name = preserve2['paystate_select'].obj.name; */
}

function resetPayCountryColor()
{
	if (typeof preserve2['pay_country'] == 'undefined') return;
	var x = new getObj('pay_country');
	x.obj.style.backgroundColor = preserve2['pay_country'];
}



