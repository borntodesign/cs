<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8"
	omit-xml-declaration="yes"
	media-type="text/javascript"/>
<xsl:param name="minfo" />
	<xsl:template match="/">
<xsl:variable name="apos">'</xsl:variable>

document.write('<table class="qlinks">' +

<xsl:if test="//user/firstname">
'<tr>' + '<td class="hello" colspan="2">' +
'<xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text><xsl:value-of select="translate(//user/firstname, $apos, '`')" />!<br />' +
'<xsl:value-of select="concat(//lang_blocks/p2, ': ', //user/reward_points)" />' +
	'</td>'+'</tr>' +
</xsl:if>
	'<tr>' + '<td class="links">'+'<a href="/members/clubcash_redemptions.shtml" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/a6" /></a>' +
	'</td>'+'<td class="links">'+
	<xsl:choose><xsl:when test="//user/firstname">
		'<a href="/cgi-bin/wwg2"><xsl:value-of select="//lang_blocks/a3" /></a>' +
	</xsl:when><xsl:otherwise>
		'<a href="/cgi/appx.cgi" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/signup" /></a>' +
	</xsl:otherwise></xsl:choose>
	'</td>'+'</tr>' +
	'<tr>'+'<td class="links">'+'<a href="/helpdesk.xml"><xsl:value-of select="//lang_blocks/a4" /></a>' +
	'</td>'+'<td class="links">' +
<xsl:choose><xsl:when test="//user/firstname">
	'<a href="/cgi/miniLogin.cgi?logout=1" onclick="return confirmCallMiniLogin(this.href)"><xsl:value-of select="//lang_blocks/logout" /></a>' +
</xsl:when><xsl:otherwise>
	'<a href="/cgi/miniLogin.cgi" onclick="return CallMiniLogin(this.href)"><xsl:value-of select="//lang_blocks/login" /></a>' +
</xsl:otherwise></xsl:choose>
	'</td>'+'</tr>' +

<xsl:if test="$minfo">
	'<tr>'+'<td class="links" colspan="2"><a href="member information"><xsl:value-of select="//lang_blocks/a5" /></a>' +
	'</td>'+'</tr>' +
</xsl:if>
'</table>');

	</xsl:template>
</xsl:stylesheet>
