var Currency = {
		defaults: {
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: '',
			append_text: '',
			exponent: 0
		},
		AED: {
			xrate: 3.673028766,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		AFN: {
			xrate: 68.3413768718,
			html_entity: '&#1547;',
			currency_symbol: '',
			exponent: 2
		},
		ALL: {
			xrate: 114.7941030939,
			html_entity: 'Lek',
			currency_symbol: '',
			exponent: 2
		},
		AMD: {
			xrate: 482.3558412099,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		ANG: {
			xrate: 1.7899985016,
			html_entity: '&#402;',
			currency_symbol: 'ƒ',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: 'ƒ'
		},
		AOA: {
			xrate: 165.9175986982,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		ARS: {
			xrate: 17.6951435231,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		AUD: {
			xrate: 1.3005640714,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		AWG: {
			xrate: 1.79,
			html_entity: '&#402;',
			currency_symbol: 'ƒ',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: 'ƒ'
		},
		AZN: {
			xrate: 1.6878844432,
			html_entity: '&#1084;&#1072;&#1085;',
			currency_symbol: 'ман',
			exponent: 2
		},
		BAM: {
			xrate: 1.6797383477,
			html_entity: '',
			currency_symbol: 'KM',
			exponent: 2
		},
		BBD: {
			xrate: 2,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BDT: {
			xrate: 82.975859106,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BGN: {
			xrate: 1.6806823472,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BHD: {
			xrate: 0.3771530091,
			html_entity: '',
			currency_symbol: '',
			exponent: 3
		},
		BIF: {
			xrate: 1751.5579378171,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		BMD: {
			xrate: 1,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BND: {
			xrate: 1.3603343223,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BOB: {
			xrate: 6.9096347383,
			html_entity: '',
			currency_symbol: '$b',
			exponent: 2
		},
		BRL: {
			xrate: 3.2888789701,
			html_entity: '',
			currency_symbol: 'R$',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.'
		},
		BSD: {
			xrate: 1,
			html_entity: '',
			currency_symbol: '$',
			exponent: 2
		},
		BTN: {
			xrate: 64.7778539054,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		BWP: {
			xrate: 10.5241580436,
			html_entity: '',
			currency_symbol: 'P',
			exponent: 2
		},
		BYR: {
			xrate: 19759.8985404954,
			html_entity: '',
			currency_symbol: 'p.',
			exponent: 0
		},
		BZD: {
			xrate: 1.9985748569,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		CAD: {
			xrate: 1.2825758955,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: '.',
			thousands_separator: ','
		},
		CDF: {
			xrate: 1574.8367441121,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		CHF: {
			xrate: 0.9956914695,
			html_entity: '',
			currency_symbol: 'CHF',
			exponent: 2
		},
		CLP: {
			xrate: 639.3010692726,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		CNY: {
			xrate: 6.6270414352,
			html_entity: '&#165;',
			currency_symbol: '¥',
			exponent: 2
		},
		COP: {
			xrate: 3022.9985394812,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		CRC: {
			xrate: 569.1694615951,
			html_entity: '&#8353;',
			currency_symbol: '₡',
			exponent: 2
		},
		CUC: {
			xrate: 1,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		CUP: {
			xrate: 26.5,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		CVE: {
			xrate: 94.9789307225,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		CZK: {
			xrate: 22.0354707881,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		DJF: {
			xrate: 178.7688069677,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		DKK: {
			xrate: 6.3911016732,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		DOP: {
			xrate: 48.0246542327,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		DZD: {
			xrate: 115.8654418336,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: '.',
			thousands_separator: ','
		},
		EEK: {
			xrate: 14.1709910994,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		EGP: {
			xrate: 17.6501458563,
			html_entity: '',
			currency_symbol: '',
			exponent: 3,
			decimal_separator: ',',
			thousands_separator: '.'
		},
		ERN: {
			xrate: 15.3299105553,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		ETB: {
			xrate: 27.1393519881,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: 'ብር'
		},
		EUR: {
			xrate: 0.848126,
			html_entity: '&euro;',
			currency_symbol: '€',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: '&euro; '
		},
		FJD: {
			xrate: 2.0746522286,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		FKP: {
			xrate: 0.7564807776,
			html_entity: '&#163;',
			currency_symbol: '£',
			exponent: 2
		},
		GBP: {
			xrate: 0.7564807776,
			html_entity: '&pound;',
			currency_symbol: '£',
			exponent: 2,
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: '&pound;'
		},
		GEL: {
			xrate: 2.5999089264,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GGP: {
			xrate: 0.7564807776,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GHS: {
			xrate: 4.4123379487,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GIP: {
			xrate: 0.7564807776,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GMD: {
			xrate: 47.797992679,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GNF: {
			xrate: 9024.3084324114,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		GTQ: {
			xrate: 7.343900055,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		GYD: {
			xrate: 207.6503956403,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		HKD: {
			xrate: 7.8012456677,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		HNL: {
			xrate: 23.5197824896,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		HRK: {
			xrate: 6.4609022403,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		HTG: {
			xrate: 63.5503162662,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		HUF: {
			xrate: 267.0327790248,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		IDR: {
			xrate: 13560.1297607429,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		ILS: {
			xrate: 3.5245386275,
			html_entity: '&#8362;',
			currency_symbol: '₪',
			exponent: 2
		},
		IMP: {
			xrate: 0.7564807776,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		INR: {
			xrate: 64.7778539054,
			html_entity: '&#8377;',
			currency_symbol: '₹',
			exponent: 2,
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: '&#8337; '
		},
		IQD: {
			xrate: 1164.5169303271,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		IRR: {
			xrate: 34847.3818748154,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		ISK: {
			xrate: 104.8605504141,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		JEP: {
			xrate: 0.7564807776,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		JMD: {
			xrate: 126.6764439812,
			html_entity: '',
			currency_symbol: 'J$',
			exponent: 2
		},
		JOD: {
			xrate: 0.7089982845,
			html_entity: '',
			currency_symbol: '',
			exponent: 3
		},
		JPY: {
			xrate: 112.9989265508,
			html_entity: '&yen;',
			currency_symbol: '¥',
			exponent: 0,
			thousands_separator: ',',
			prepend_text: '&yen; '
		},
		KES: {
			xrate: 103.7492634263,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		KGS: {
			xrate: 68.6263114878,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		KHR: {
			xrate: 4034.9013244352,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		KMF: {
			xrate: 422.5198997376,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		KPW: {
			xrate: 130.9737911839,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		KRW: {
			xrate: 1119.6131951756,
			html_entity: '&#8361;',
			currency_symbol: '₩',
			exponent: 0
		},
		KWD: {
			xrate: 0.3025999701,
			html_entity: '',
			currency_symbol: '',
			exponent: 3,
			decimal_separator: ',',
			thousands_separator: '.'
		},
		KYD: {
			xrate: 0.8200000091,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		KZT: {
			xrate: 334.5880635092,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		LAK: {
			xrate: 8309.8541614978,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		LBP: {
			xrate: 1510.4933105223,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		LKR: {
			xrate: 153.5979214126,
			html_entity: '&#8360;',
			currency_symbol: '₨',
			exponent: 2
		},
		LRD: {
			xrate: 119.3129078922,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		LSL: {
			xrate: 14.0260750743,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		LTL: {
			xrate: 3.1271632803,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: ' '
		},
		LVL: {
			xrate: 0.6365182905,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		LYD: {
			xrate: 1.3798240269,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MAD: {
			xrate: 9.5023240082,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MDL: {
			xrate: 17.3187842564,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MGA: {
			xrate: 3174.7844726763,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		MKD: {
			xrate: 52.8344424814,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MMK: {
			xrate: 1351.714827285,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		MNT: {
			xrate: 2455.4919441052,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MOP: {
			xrate: 8.0352830377,
			html_entity: '',
			currency_symbol: '',
			exponent: 1
		},
		MRO: {
			xrate: 354.074251897,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		MUR: {
			xrate: 34.3282951092,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MVR: {
			xrate: 15.4100460269,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MWK: {
			xrate: 725.9239361991,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MXN: {
			xrate: 19.2542398774,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MYR: {
			xrate: 4.2304250659,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		MZN: {
			xrate: 60.6940273123,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		NAD: {
			xrate: 14.0260750743,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		NGN: {
			xrate: 360.4870875072,
			html_entity: '&#8358;',
			currency_symbol: '₦',
			exponent: 2
		},
		NIO: {
			xrate: 30.5408912121,
			html_entity: '',
			currency_symbol: 'C$',
			exponent: 2
		},
		NOK: {
			xrate: 8.1509477016,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		NPR: {
			xrate: 103.6935995869,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		NZD: {
			xrate: 1.4572847472,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		OMR: {
			xrate: 0.3849468096,
			html_entity: '',
			currency_symbol: '',
			exponent: 3
		},
		PAB: {
			xrate: 1,
			html_entity: '',
			currency_symbol: 'B/.',
			exponent: 2
		},
		PEN: {
			xrate: 3.2486956269,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		PGK: {
			xrate: 3.2102478947,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		PHP: {
			xrate: 51.583341154,
			html_entity: '&#8369;',
			currency_symbol: '₱',
			exponent: 2
		},
		PKR: {
			xrate: 105.3950212446,
			html_entity: '&#8360;',
			currency_symbol: '₨',
			exponent: 2
		},
		PLN: {
			xrate: 3.6493514371,
			html_entity: '&#122;&#322;',
			currency_symbol: 'zł',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: ' '
		},
		PYG: {
			xrate: 5646.0902237894,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		QAR: {
			xrate: 3.7766720029,
			html_entity: '&#65020;',
			currency_symbol: '',
			exponent: 2
		},
		RON: {
			xrate: 3.9490143189,
			html_entity: '',
			currency_symbol: 'lei',
			exponent: 2
		},
		RSD: {
			xrate: 102.5510166481,
			html_entity: '&#1044;&#1080;&#1085;&#46;',
			currency_symbol: 'Дин.',
			exponent: 2
		},
		RUB: {
			xrate: 57.9486753802,
			html_entity: '&#1088;&#1091;&#1073;',
			currency_symbol: 'руб',
			exponent: 2
		},
		RWF: {
			xrate: 853.9008395598,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		SAR: {
			xrate: 3.7501302844,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SBD: {
			xrate: 7.7730000644,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SCR: {
			xrate: 13.5767450519,
			html_entity: '&#8360;',
			currency_symbol: '₨',
			exponent: 2
		},
		SDG: {
			xrate: 6.6700015415,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SEK: {
			xrate: 8.347537148,
			html_entity: '',
			currency_symbol: 'kr',
			exponent: 2
		},
		SGD: {
			xrate: 1.3603343223,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SHP: {
			xrate: 0.7564807776,
			html_entity: '&#163;',
			currency_symbol: '£',
			exponent: 2
		},
		SLL: {
			xrate: 7659.5336188289,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		SOS: {
			xrate: 577.7599080059,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SPL: {
			xrate: 0.166666666,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SRD: {
			xrate: 7.4474703434,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		STD: {
			xrate: 20981.9375497103,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		SVC: {
			xrate: 8.75,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SYP: {
			xrate: 514.9944559197,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		SZL: {
			xrate: 14.0260750743,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		THB: {
			xrate: 33.2226086428,
			html_entity: '&#3647;',
			currency_symbol: '฿',
			exponent: 2
		},
		TJS: {
			xrate: 8.7986025486,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		TMM: {
			xrate: 17500.0133443668,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		TMT: {
			xrate: 3.5000026689,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: 'TMT '
		},
		TND: {
			xrate: 2.5005434591,
			html_entity: '',
			currency_symbol: '',
			exponent: 3
		},
		TOP: {
			xrate: 2.2094444737,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		TRY: {
			xrate: 3.7726943053,
			html_entity: '&#8356;',
			currency_symbol: '₤',
			exponent: 2
		},
		TTD: {
			xrate: 6.7424080432,
			html_entity: '',
			currency_symbol: 'TT$',
			exponent: 2
		},
		TVD: {
			xrate: 1.3005640714,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		TWD: {
			xrate: 30.1700777704,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		TZS: {
			xrate: 2245.5107797348,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		UAH: {
			xrate: 26.8349824431,
			html_entity: '&#8372;',
			currency_symbol: '₴',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: ' '
		},
		UGX: {
			xrate: 3654.9265266015,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		USD: {
			xrate: 1,
			html_entity: '',
			currency_symbol: '$',
			exponent: 2,
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: '$'
		},
		UYU: {
			xrate: 29.1800837417,
			html_entity: '',
			currency_symbol: '$U',
			exponent: 2
		},
		UZS: {
			xrate: 8079.0493544157,
			html_entity: '&#1083;&#1074;',
			currency_symbol: 'лв',
			exponent: 2
		},
		VEB: {
			xrate: 10282.3255421933,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		VEF: {
			xrate: 10.2823255422,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		VND: {
			xrate: 22691.4119173504,
			html_entity: '&#8363;',
			currency_symbol: '₫',
			exponent: 0
		},
		VUV: {
			xrate: 109.4324194808,
			html_entity: '',
			currency_symbol: '',
			exponent: 0
		},
		WST: {
			xrate: 2.5406451174,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XAF: {
			xrate: 563.3598663168,
			html_entity: '',
			currency_symbol: '',
			exponent: 0,
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: 'FCFA '
		},
		XAG: {
			xrate: 0.0592869026,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XAU: {
			xrate: 0.0007828714,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XCD: {
			xrate: 2.7000002849,
			html_entity: '',
			currency_symbol: '',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: '.',
			prepend_text: '$',
			append_text: ' XCD'
		},
		XDR: {
			xrate: 0.7118521098,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XOF: {
			xrate: 563.3598663168,
			html_entity: '',
			currency_symbol: '',
			exponent: 0,
			decimal_separator: '.',
			thousands_separator: ',',
			prepend_text: 'CFA '
		},
		XPD: {
			xrate: 0.0010243525,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XPF: {
			xrate: 102.4864653588,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		XPT: {
			xrate: 0.0010810048,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		YER: {
			xrate: 250.2685552323,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
		ZAR: {
			xrate: 14.0260750743,
			html_entity: '',
			currency_symbol: 'R',
			exponent: 2,
			decimal_separator: ',',
			thousands_separator: ' ',
			prepend_text: 'R'
		},
		ZMW: {
			xrate: 10.0204134562,
			html_entity: '',
			currency_symbol: '',
			exponent: 2
		},
	language: {
		
		ie: {
			EUR: {
				decimal_separator: ',',
				thousands_separator: '.',
				prepend_text: '',
				append_text: ' EUR'
			}
		},
		ar: {
			EGP: {
				decimal_separator: '.',
				thousands_separator: ',',
				prepend_text: '',
				append_text: 'ج.م.‏'
			}
		},
		ar: {
			DZD: {
				decimal_separator: '٫',
				thousands_separator: '٬',
				prepend_text: '',
				append_text: 'د.ج.‏'
			}
		}
	}
};