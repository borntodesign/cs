function CheckRequired(form_num, fields_string, real_names_string)
 {
    var msg = '';
    var focus_field = '';

    emptyfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");
    for ( fieldname in the_fields )
    {
  		if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value") == '' )
		{
  			emptyfields = emptyfields + "..." + real_names[fieldname] + "\n";
			if ( focus_field == '' )
			{
				focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
			}
		}
    }
    if ( emptyfields != "" )
    {
  		msg = msg + "- Please enter a value for the following fields:\n" + emptyfields;
  		alert(msg);
		// Set focus back to the appropriate field.
		eval(focus_field + ".focus()");
		return false;
    }
    return true;
 }

function CheckNumber(form_num, fields_string, real_names_string)
 {
    var msg = '';
    var focus_field = '';
	var bad_number = /(^(\D))/;
	
    nonumfields = "";
    the_fields = fields_string.split(",");
    real_names = real_names_string.split(",");

	for ( fieldname in the_fields )
    {
		if ( bad_number.test(eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value")))
		{
  			nonumfields = nonumfields + "..." + real_names[fieldname] + "\n";
			if ( focus_field == '' )
			{
				focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
			}
    	}
    	if ( nonumfields != "" )
    	{
  			msg = msg + "- Please use only numbers for the following fields:\n" + nonumfields;
  			alert(msg);
			// Set focus back to the appropriate field.
			eval(focus_field + ".focus()");
			return false;
		}
    }
    return true;
 }