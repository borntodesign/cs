// mallcat-admin.js - for the mall category administration functions - data provided by mallcat-admin.cgi

// some globals for use inside callbacks


// some constants
var Yobj = YAHOO.util.Event;
var SUBMIT_FRM;
var ACTIVE_FRM;
var ACTIVE_SPAN;

var callbackSubmitTranslation = 
{
  success: successfulSave,
  failure: GeneralResponseFailure
}

function CreateEditFormEvents()
{
	// this is our list container div
	var alist = document.getElementById('frmlist');
	// there may not have been any results
	if (alist == undefined){ return; }
	alist = alist.getElementsByTagName('form');
	for(var myp=0; myp < alist.length; myp++){
		if (! Yobj.addListener(alist[myp], 'submit', SubmitTranslation)){
				alert('Failed to setup listener on: list form');
		}
		if (! Yobj.addListener(alist[myp].catname, 'click', selectCatname)){
				alert('Failed to setup listener on: catname field');
		}
	}
}

function GeneralResponseFailure(o) {
	alert("There has an unexpected error:\n" + o.responseText);
}

function init(){
	CreateEditFormEvents();
	SUBMIT_FRM = document.getElementById('submit_form');
	var frm1 = document.getElementById('f1');
	if (frm1 != null){
		frm1.catname_translated.focus;
		frm1.catname_translated.select();
	}
}

function selectCatname(e){
	var fld = window.event ? window.event.srcElement : e.target;
	fld.select();
}

function stopDefaultAction(e)
{
	e.returnValue = false;
	if (typeof e.preventDefault != 'undefined'){ e.preventDefault(); }
}

function SubmitTranslation(e){
	// for the sake of simplicity, we are using a global to know where we are at
	// so... we don't want to overwrite an active global
	if (typeof ACTIVE_FRM != 'undefined'){
		alert('Please wait for your last submission to process');
		stopDefaultAction(e);
		return false;
	}
	// this is our form that submitted
	var frm = window.event ? window.event.srcElement : e.target;
	if (frm.type == 'text'){ frm = frm.parentNode; }

	// if there is no content submitted, then why dive in
	if (frm.catname_translated.value != ''){
		// remove any pre-existing span tags (which would have been instructions/labels)
		var spans = frm.getElementsByTagName('span');
		for (var x=0; x < spans.length; x++){ frm.removeChild(spans[x]); }
		
		SUBMIT_FRM.catname.value = frm.catname.value;
		SUBMIT_FRM.catname_translated.value = frm.catname_translated.value;
		SUBMIT_FRM.code.value = frm.code.value;
		YAHOO.util.Connect.setForm(SUBMIT_FRM);
		var rv = YAHOO.util.Connect.asyncRequest("GET", SUBMIT_FRM.action, callbackSubmitTranslation);
		if (rv == null){ alert('XMLHttpRequest failure'); }
		else {
			var newspan = document.createElement('span');
			newspan.className = 'please_wait';
		//	var newtxt = document.createTextNode('Please wait');
			newspan.appendChild( document.createTextNode('Please wait') );
			ACTIVE_SPAN = newspan;
			frm.appendChild(newspan);
			ACTIVE_FRM = frm;
		}
	}
	stopDefaultAction(e);
	return false;
}

function successfulSave(o)
{
//alert(o.responseText);
//alert('in successfulsave:'+o.responseText.search(/\D/));
	ACTIVE_FRM.removeChild(ACTIVE_SPAN);
	if (o.responseText.search(/\D/) > -1){
		alert("Unanticipated error:\n" + o.responseText);
		ACTIVE_FRM = undefined;
	} else {
		// reset the please wait message
		var newspan = document.createElement('span');
		newspan.className = 'update_success';
		var msg = o.responseText == 1 ? 'category' : 'categories';
		newspan.appendChild( document.createTextNode(o.responseText + ' ' + msg + ' updated') );
		ACTIVE_FRM.appendChild(newspan);
		
		// set the focus to the next text fieldset
		var nextfrm = document.getElementById('f' + ++ACTIVE_FRM.id.match(/\d+/)[0]);
		if (typeof nextfrm != 'undefined'){
			nextfrm.catname_translated.focus();
			nextfrm.catname_translated.select();
		}
		ACTIVE_FRM = undefined;
	}
}