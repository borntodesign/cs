// miniLogin accessories

// simple, but necessary for links that are document.write generated
// it's a nightmare trying to get the stuff below into a document.write through xsl ;-)
function CallMiniLogin(url)
{
	window.open(url,'login','width=400,height=200,resizeable,scrollbars');
	return false;
}

function confirmCallMiniLogin(url)
{
	if (confirm('Logout now?')){ CallMiniLogin(url); }
	return false;
}