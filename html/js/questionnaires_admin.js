				var distance_to_top = 0;
				var agent = navigator.userAgent.toLowerCase();

				var answer_key = 0;

				if((agent.indexOf("msie") > -1) && (agent.indexOf("opera") < 1)){
					var fudge = 10;
				} else {
					var fudge = 0;
				}

				var globalOutputElement = null;

				function displayFadeDive(){

					var client_height = document.getElementById("content").clientHeight - 5; //window.innerHeight;
					var client_width = document.getElementById("content").clientWidth - 5; //window.inner

					//alert('window.innerHeight: ' + window.innerHeight + ', window.document.innerHeight: ' + window.document.innerHeight + ', document.getElementById("content").clientHeight: ' + document.getElementById("content").clientHeight);

					/*
					document.getElementById("fadeDiv").style.height = (document.body.scrollHeight - 20) + "px";
					document.getElementById("fadeDiv").style.width = (document.body.scrollWidth - 20) + "px";
					*/


					document.getElementById("fadeDiv").style.height = client_height + 'px';
					document.getElementById("fadeDiv").style.width = client_width + 'px';

					document.getElementById("fadeDiv").style.visibility = "visible";
				}

				function hideFadeDiv(){
					document.getElementById("fadeDiv").style.visibility = "hidden";
				}

				function closeDivs(){
					hideFadeDiv()

					document.getElementById("addCategoryDiv").style.visibility = "hidden";
					document.getElementById('search_text').value =  "";
					document.getElementById('additional_categories').options.length = 0;
					window.onscroll = null;

				}// end function

				function showDivs(blog_entry_id){

					var location = getScrollXY();

					var client_height = (document.body.clientHeight/2) + location[1];
					var client_width = document.body.clientWidth/2;

					closeDivs();

					//alert('height: ' + (client_height - (400/2)) + ', width: ' + (client_width  - (400/2)));

					document.getElementById("addCategoryDiv").style.top = (client_height - (400/2)) + "px";
					document.getElementById("addCategoryDiv").style.left = (client_width  - (400/2)) + "px";

					/*
					fudge_width
					fudge_height
					*/

					document.getElementById("addCategoryForm").style.height = (300 -fudge) + "px";
					document.getElementById("addCategoryForm").style.width = (390 -fudge) + "px";

					document.getElementById("addCategoryClose").style.width = (395 -fudge) + "px";

					document.getElementById("addCategoryTable").width = 390 -fudge;

					displayFadeDive();
					document.getElementById("addCategoryDiv").style.visibility = "visible";

					window.onscroll = function(){ document.body.scrollTop = location[1]}//scrollHeight

				}// end function


				function getScrollXY()
				{
					var scrOfX = 0, scrOfY = 0;
					if( typeof( window.pageYOffset ) == 'number' ) {
						//Netscape compliant
						scrOfY = window.pageYOffset;
						scrOfX = window.pageXOffset;
					} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
						//DOM compliant
						scrOfY = document.body.scrollTop;
						scrOfX = document.body.scrollLeft;
					} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
						//IE6 standards compliant mode
						scrOfY = document.documentElement.scrollTop;
						scrOfX = document.documentElement.scrollLeft;
					}

					return [ scrOfX, scrOfY ];
				}

				function searchCategory(search_text, outputElement)
				{

					globalOutputElement = outputElement;

					var callback = {
										success: parseXML,
										failure: function(xml){alert("Failed")}
									};
					YAHOO.util.Connect.asyncRequest("GET", '/cgi/tmp_keith/get_categories.cgi?category_search=' + encodeURIComponent(search_text), callback);




				}

				function parseXML(xml_file)
				{
					try //Internet Explorer
					{
					  	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
					  	xmlDoc.async=false;
						xmlDoc.loadXML(xml_file.responseText);
					}
					catch(e)
					{
					 	 try //Firefox, Mozilla, Opera, etc.
					    {
					    	var parser = new DOMParser();
					   		var xmlDoc=parser.parseFromString(xml_file.responseText,"text/xml");
					    }
						catch(e)
					    {
					    	alert(e.message);
					    	return;
					    }
					}

					var categoryNode = xmlDoc.getElementsByTagName("category");
					//[0].getElementsByTagName("name")[0].childNodes[0].nodeValue

					globalOutputElement.options.length = 0;

					for (counter = 0; counter < categoryNode.length; counter++)
					{
						try{
							globalOutputElement.add(
													new Option(categoryNode[counter].getElementsByTagName("name")[0].childNodes[0].nodeValue,categoryNode[counter].getElementsByTagName("catid")[0].childNodes[0].nodeValue),null);
						}
						catch(error){
							try{
								globalOutputElement.add(new Option(categoryNode[counter].getElementsByTagName("name")[0].childNodes[0].nodeValue,categoryNode[counter].getElementsByTagName("catid")[0].childNodes[0].nodeValue));
							}
							catch(error)
							{
								//No worries
							}
						}

					}
				}

				function addCategory(fromElement, outputElement)
				{

					for (index = 0; index < fromElement.options.length; index++ )
					{
						if(!fromElement.options[index].selected || fromElement.options[index].value == 0)
						{
							continue;
						}

						try{
							document.getElementById(answer_key).add(new Option(fromElement.options[index].text,fromElement.options[index].value),null);
						}
						catch(error){
							document.getElementById(answer_key).add(new Option(fromElement.options[index].text,fromElement.options[index].value));
						}
						document.getElementById(answer_key).options[document.getElementById(answer_key).length -1].selected = 1;

					}

					/**
					* This may seem wierd but FireFox was not removing the second option in the select
					* list.  So I'm looping twice.
					*/
					for (outer_counter = 0; outer_counter < 2; outer_counter++)
					{
						for (index = 0; index <= fromElement.options.length; index++ )
						{

							try{
								if(fromElement.options[index].value == 0)
								{
									continue;
								}

								if(fromElement.options[index].selected)
								{
									fromElement.remove(index);
									index = 0;
								}
							}
							catch(e){

							}
						}
					}



				}