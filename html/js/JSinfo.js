/*
This little code piece is going to be expecting to have a hash available to it.
That should be loaded by a call to /cgi/JSInformation.cgi as the src of a script tag.

for example, we should get something like this returned from the script call.
See the script for the complete set of options and return values.

var _JS_Information = {
	personal : { firstname : 'Billea', lastname : 'Bob' },
	refcomp : { ppp : 100, fpp : 200 }
};

Also, you should define an array of elements you want to be inserted.
For example: var _JS_Replacements = ['personal_firstname','refcomp_ppp'];
This code will look through the document for nodes with those classes.
Then it will insert the values found from the hash where the values are personal.firstname and refcomp.ppp

We have deliberately omitted error trapping for undefined values as the assumption is that you will make sure it all works.
This eases the javascript load by omitting the additional logic needed to perform trapping.
*/

/*
This will be our namespace.
*/

var JSInfo = {
	insertData : function()
	{
		for (var x=0; x < _JS_Replacements.length; x++)
		{
			var s = jQuery('.' + _JS_Replacements[x]);
			var tokName = _JS_Replacements[x].replace('_','.');
			var val = eval('_JS_Information.' + tokName);
			s.text( val );
		}
	}
};
