
	function uncheckNone(element_id)
	{
		document.getElementById(element_id).checked = false;
	}

	function uncheckMyFriends(element)
	{
		var inputs = element.parentNode.getElementsByTagName('input');

		for (var array_index in inputs)
		{
			if(inputs[array_index].checked !== undefined && element.value !== inputs[array_index].value)
			{
					inputs[array_index].checked = false;
			}
		}
	}
