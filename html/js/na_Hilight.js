/**
 * just a little jquery tidbit to hilight a named anchor if one is present
 */

$(document).ready(function() {
	// highlight the named anchor if there is one
	var namedAnchor = window.location.hash.substring(1);
	if (namedAnchor) {
		$('<style type="text/css"> .na_Hilight { background-color: #ffc; } </style>').appendTo("head");
		$('#'+ namedAnchor).addClass('na_Hilight');
	}
});