/*
EventLogin.js
Some java for the EventLogin.cgi interfaces
*/

function activeReset(list)		// we have a reset button on the forms, but since the forms are reloaded on submit, the prepopulated vals cannot be reset ordinarily
{
	var lista = list.split(',');
	for (var x=0; x<lista.length; x++){
		var elem = document.getElementById(lista[x]);
		if (elem){ elem.value = ''; }
	}
}