/* set this up for defaults... a subsequent script can redefine them */
var $getStates = {state_select:'state_select', state_text:'state_text'};

var preserve = new Array();
var select_prompt = '- Select a State/Province -';	// our default
var field_name = 'state';	// our default

function adjustInitialStateCtlFocus(my_field_name)
{
// if the application has created a prepopulated state_select control,
//then we'll display that instead of the default state_text
	var x= new getObj($getStates.state_select);
	if (x.obj.options.length <= 1) return;
	x= new getObj($getStates.state_text);
	
	if (typeof my_field_name != 'null' && typeof my_field_name != 'undefined'){
		field_name = my_field_name;
	}
	else if (x.obj.name){
		field_name = x.obj.name;
	}
//	field_name = (typeof my_field_name != 'null' && typeof my_field_name != 'undefined') ? my_field_name  : x.obj.name;
	turnON($getStates.state_select, field_name);
	turnOFF($getStates.state_text);
}

function getObj(name)
{
//alert('name='+name);
	if (document.getElementById)
	{
		this.obj = document.getElementById(name);
	}
	else if (document.all)
	{
		this.obj = document.all[name];
	}
	
	if (this.obj){
		this.style = document.getElementById(name).style;
	}
}

function getStates(my_field_name)
{
// preserve the focus states in case they press the reset button
	preserveFocus();

// field_name is the form name for the 'state' field
// it will default to 'state'
	if (typeof my_field_name != 'null' && typeof my_field_name != 'undefined') field_name = my_field_name;

// figure out what country is currently selected
// we are counting on there being a field with the ID of 'country'
	var x= new getObj('country');
	var country = x.obj.options[x.obj.selectedIndex].value;
	if (country == '')
	{
		needs_country(x);
	}
// since we should have been called by an onchange event,
// we'll reset the background color if it was changed
	else { resetCountryColor() }

// if we don't have a valid array defined for the country, we'll revert to the text field
	if (typeof sc[country] == 'null' || typeof sc[country] == 'undefined')
	{
		turnON($getStates.state_text);
		turnOFF($getStates.state_select);
		return;
	}

	x= new getObj($getStates.state_select);
// this reinitializes the list
	x.obj.options.length=0;
// insert our prompt as line 1
	x.obj.options[0] = new Option(select_prompt, '');

	for (var y=0; y < sc[country].length; y++)
	{
		x.obj.options[y+1] = new Option(sn[country][y],sc[country][y]);
	}
	turnON($getStates.state_select, field_name);
	turnOFF($getStates.state_text);
}

function needs_country(x)
{
// we are going to assume that there is a field named 'country'
// if there is no country value defined, then we surely cannot look up any states
// the idea is to call this function on focus of the 'state' text field
	if (typeof x == 'undefined') x = new getObj('country');

// we could raise an alert box, but instead we'll just shift focus back
// this way we can avoid having to provide translatable alerts
	if (x.obj.options[x.obj.selectedIndex].value == '')
	{
		x.obj.focus();

	// save the existing background color
		preserve['country'] = x.obj.style.backgroundColor;
		x.obj.style.backgroundColor = 'yellow';
	}
}

function preserveFocus()
{
// if we have initialized values, then we are done
	if (typeof preserve['state_text_display'] != 'undefined') return;
	var x = new getObj($getStates.state_text);
	preserve['state_text_display'] = x.style.display;
	preserve['state_text_name'] = x.obj.name;
	x = new getObj($getStates.state_select);
	preserve['state_select_display'] = x.style.display;
	preserve['state_select_name'] = x.obj.name;
// the reset doesn't even 'reset' the drop-down if it has been recreated
// so we'll simply save the controls
//	preserve[$getStates.state_text] = new getObj($getStates.state_text);
//	preserve[$getStates.state_select] = new getObj($getStates.state_select);
}

function restoreStateFocus()
{
// the reset will not bring the proper control into focus, it will only restore values
// if we don't have initialized values, then we are done
	if (typeof preserve['state_text_display'] == 'undefined') return;

	var x = new getObj($getStates.state_text);
	x.style.display = preserve['state_text_display'];
	x.obj.name = preserve['state_text_name'];
	x = new getObj($getStates.state_select);
	x.style.display = preserve['state_select_display'];
	x.obj.name = preserve['state_select_name'];
/*
	var x = new getObj($getStates.state_text);
	x.style = preserve[$getStates.state_text].style;
	x.obj.name = preserve[$getStates.state_text].obj.name;
	x = new getObj($getStates.state_select);
	x.style.display = preserve[$getStates.state_select].style.display;
	x.obj.name = preserve[$getStates.state_select].obj.name; */
}

function resetCountryColor()
{
	if (typeof preserve['country'] == 'undefined') return;
	var x = new getObj('country');
	x.obj.style.backgroundColor = preserve['country'];
}

function setSelectPrompt(prmpt)
{
	if (typeof prmpt == 'undefined') return;
	select_prompt = prmpt;
}

function turnON(ctl_id)
{
	var x = new getObj(ctl_id);
	x.style.display='inline';
	x.obj.name = field_name;
}

function turnOFF(ctl_id)
{
	var x = new getObj(ctl_id);
	x.style.display='none';
	x.obj.name = null;
}
