var fieldnames = new Array (7)
        fieldnames[0] = "firstname"
 
        fieldnames[1] = "lastname"
        fieldnames[2] = "email"
 
        fieldnames[3] = "email_check"
        fieldnames[4] = "city"
 
        fieldnames[5] = "state"
        fieldnames[6] = "country"
 
        fieldnames[7] = "zip"
        
function validation(form) {

	var onoff=0
	var alertboxnames = ""
        
	var fields = new Array (7)
      	fields[0] = form.firstname.value.length
		fields[1] = form.lastname.value.length
		fields[2] = form.email.value.length
		fields[3] = form.email_check.value.length
		fields[4] = form.city.value.length
 		fields[5] = form.state.value.length
		fields[6] = form.country.value.length
		fields[7] = form.zip.value.length

	var emaildump = form.email.value
        
	if (emaildump.indexOf("@") == -1) {
	      alert ("Please input a valid email address!")
      	return false
	} else {      
      	for (var i=0; i < fields.length; i++) {
			if (fields[i] == 0) {
				alertboxnames = alertboxnames + fieldnames[i] + ", ";
	        		onoff ++;
      	      }
		}
		if (onoff == 0) {return true
		} else {
			if (onoff == 1) {
				var catness = alertboxnames.substring (0, alertboxnames.indexOf(","));
				alert ("The following fieldname is required: \r" + catness + "\r Please fill in this field before continuing.");
			} else {
				var catness = alertboxnames.substring (0, alertboxnames.length-2)
				alert ("The following fieldnames are required: \r" + catness + "\rPlease fill in these fields before continuing."); 
			}
			return false
	     }
	}
}