/* Since we cannot seem to embed javascript in our XSLT processed pages without the breakage of CDATA tags,
we'll provide a simple variable initialization here. "submitted' is used in pages where we don't want them to submit more than once.
"var x=y" doesn't actually work in the onload event.
*/
var submitted=false;
