// Some functions for the ClubCash Center.
// Created: 02/03/05 Karl Kohrt
// revised 09/25/07 Bill MacArthur

var reloaded = true;

function ReLoad(do_what)
 {
	if ( do_what == 'reset') { reloaded = false; }
	else if ( reloaded == false ) { location.reload(); reloaded = true; }
 }

function ClearForm(cat)
 {
	document.forms[0].name.value = '';
	document.forms[0].email.value = '';
	document.forms[0].address1.value = '';
	document.forms[0].address2.value = '';
	document.forms[0].city.value = '';
	document.forms[0].state.value = '';
	document.forms[0].country.value = '';
	document.forms[0].postal.value = '';
	if ( cat == 'comm' )  
	{ 
		document.forms[0].name.value = 'Add to My Commissions';
		document.forms[0].Submit.select();
		document.forms[0].Submit.focus();
	}
	else if ( cat == 'prepay' )  
	{ 
		document.forms[0].name.value = 'Add to My Pre-Pay Account';
		document.forms[0].Submit.select();
		document.forms[0].Submit.focus();
	}
	else
	{
		document.forms[0].name.select();
		document.forms[0].name.focus();
	}
	return true;
 }

function CheckRequired(cat)
 {
/*    if ( cat == 'cashback' && (document.forms[0].sendto[2].checked == true || document.forms[0].sendto[3].checked == true) )
    { 
    	var real_names = ["Name"];
    	var the_fields = ["name"];
    } */

	if (cat == 'cashback' && document.getElementById('sendtome') && document.getElementById('sendtome').checked == true){
		// we have to validate nothing
	}
	else if (cat == 'cashback' && document.getElementById('sendtoclubaccount').checked == true){
		// we have to validate nothing
	}
	else if (cat == 'cashback' && ( document.getElementById('sendtopaypal').checked == true || document.getElementById('sendtomoneybooker').checked == true)){
	    	var real_names = ["Email"];
    		var the_fields = ["email"];
	}
	else if (cat == 'cashback'){
		// they apparently have not chosen disbursement option
		alert('Please select a disbursement option');
		return false;
	}
    else
    {
    	var real_names = ["Name", "Email", "Address", "City", "State", "Country", "Postalcode"];
    	var the_fields = ["name", "email", "address1", "city", "state", "country", "postal"];
    }

    var form_num = 0;
    var msg = '';
    var focus_field = '';
	
    emptyfields = "";

    reqid_empty = true;
    for (var i=0; i < document.forms[0].reqid.length; i++ )
    {
	if ( document.forms[0].reqid[i].checked == true )
	{
		reqid_empty = false;
	}
    }
    if ( reqid_empty == true )
    {
	/*emptyfields = emptyfields + "...Select One\n";
	focus_field = "self.document.forms[" + form_num + "].reqid[0]";*/
	alert('Please select your redemption amount');
	return false;
    }

    for ( fieldname in the_fields )
    {
  	if ( eval("document.forms[" + form_num + "]." + the_fields[fieldname] + ".value") == '' )
	{
  		emptyfields = emptyfields + "..." + real_names[fieldname] + "\n";
		focus_field = "self.document.forms[" + form_num + "]."  + the_fields[fieldname];
	}
    }
    if ( emptyfields != "" )
    {
  		msg = msg + "- Please enter a value for the following fields:\n" + emptyfields;
  		alert(msg);
		// Set focus back to the appropriate field.
		//eval(focus_field + ".focus()");
		return false;
    }
    return true;
 }

