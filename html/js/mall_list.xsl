<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	omit-xml-declaration="yes"
	media-type="text/javascript"/>

<xsl:include href="/xml/head-foot.xsl"/>
<xsl:template match="/">document.write('<xsl:call-template name="mall_list"/>');</xsl:template>
</xsl:stylesheet>
