/*
a collection of routines to drive the vendor URLs admin management interface
*/

var Yobj = YAHOO.util.Event;
var subFrm;
var mainFrm;
var mainTblBody;
var activeRow = new Array();
var sub_frm_action = new Array();
sub_frm_action['add'] = 'Add';
sub_frm_action['edit'] = 'Edit';

var searchFrmSubmitCallback =
{
  success: searchFrmSubmitSuccess,
  failure: subFrmSubmitFailure
}
var subFrmSubmitCallback =
{
  success: subFrmSubmitSuccess,
  failure: subFrmSubmitFailure
}

adjustUrlCursor = new function() {
	this.on = function(e){Yobj.getTarget(e).className = 'input_url_hover';}
	this.off = function(e){Yobj.getTarget(e).className = 'input_url';}
}

function clearMainTbl() {
// don't know if this relationship is dynamic or not
	var rows = mainTblBody.getElementsByTagName('tr');
// but this evaluates to true until all the rows are gone
	while (rows[0]){
		mainTblBody.removeChild(rows[0]);
	}
}

function createAdRow(res) {
	var nurow = document.getElementById('template_row').cloneNode(true);
	initListeners(nurow);
	nurow.setAttribute('id', 'row'+ res['ad_id']);
// until we need row IDs, we'll just nullify this one
	nurow.setAttribute('id', null);
// set the value of the ad id anchors
// there should be only one
	nurow.getElementsByTagName('a')[0].appendChild(document.createTextNode(res['ad_id']));
	mainTblBody.appendChild(nurow);
// make the row and it's controls available vi the global activeRow
	init_activeRow(nurow);
}

function flipPAdUrls(d) {
	document.getElementById('no_ad_urls').style.display = (d == 'on') ? 'block' : 'none';
}

function init(){
	mainFrm = document.getElementById('main_frm');
	subFrm = document.getElementById('sub_frm');
	mainTblBody = document.getElementById('main_tbl_tbody');
// make the sub form invisible if there is no vendor ID
// this will only happen if we go to a multi-vendor listing
//	if (document.getElementById('sub_frm_vendor_id').value == ''){ subFrm.style.visibility = 'hidden'; }

	var rows = mainTblBody.getElementsByTagName('tr');

	for(var x=0;x < rows.length;x++){
		initListeners(rows[x]);
	}
// any other stuff to do?
}

function init_activeRow(row) {
	var ctls = row.getElementsByTagName('input');
	activeRow.ctls = new Array();
// capture the control objects in our active row collection for future use
	for (var x=0;x < ctls.length;x++){
		activeRow.ctls[ctls[x].name] = ctls[x];
	}
	activeRow.row = row;
// remember what we looked like when we started
	activeRow.previousClass = row.className;
}

function initAdRow(res){
//	for (var param in res) {
// the xml parser apparently leaves out empty nodes 
// so we'll have to define the control names we are expecting
	var ctlnames = new Array('ad_id','url','active','start_date','end_date','staff_notes','stamp','operator');

	for (var x=0;x < ctlnames.length;x++){
		var param = ctlnames[x];
//alert('field='+param+' ctl type='+typeof activeRow.ctls[param]);
		if (activeRow.ctls[param]){
			if (activeRow.ctls[param].type != 'checkbox'){
				activeRow.ctls[param].value = (typeof res[param] == 'undefined') ? '' : res[param];
			} else {
//alert('checked='+res[param]);
				activeRow.ctls[param].checked = res[param] == '1' ? true : false;
			}
		}
	}
}

function initListeners(row) {
// make all the controls readonly or disabled
	var ctls = row.getElementsByTagName('input');
	for(var y=0;y < ctls.length;y++){
		if (ctls[y].type == 'text'){ ctls[y].readOnly = true; }
		else { ctls[y].disabled = true; }
// make the URL control a hyperlink :)
		if (ctls[y].className == 'input_url'){
			Yobj.addListener(ctls[y], 'click', openURL);
// since IE doesn't recognize the :hover pseudo-class
// and since there may be some staff using IE.
// we'll control the cursor by code
			Yobj.addListener(ctls[y], 'mouseover', adjustUrlCursor.on);
			Yobj.addListener(ctls[y], 'mouseout', adjustUrlCursor.off);
		}
	}
// attach listeners to the ad_id anchors
	var adids = row.getElementsByTagName('a');
	for(var y=0;y < adids.length;y++){
		if (adids[y].className == 'a_ad_id'){
			Yobj.addListener(adids[y], 'click', makeRowActive);
		}
	}
}

function insertTxtNode(val,elem) {
	var currnode = (typeof elem == 'object') ? elem : document.getElementById(elem);
	if (currnode.hasChildNodes()){
		currnode.replaceChild(document.createTextNode(val), currnode.lastChild);
	} else {
		currnode.appendChild(document.createTextNode(val));
	}
}

function lowerMessage() {
	var div = document.getElementById('flipflop');
	div.className = '';
	div.style.visibility = 'hidden';
}

function makeRowActive(e){
// clear the active row
	resetRowsToDefault();
	var pr = (Yobj.getTarget(e).parentNode).parentNode;
// store the row object for future use
	init_activeRow(pr);
	pr.className = 'active';
// keep the hyperlink from activating
	Yobj.stopEvent(e);
// now load all the values from the fields into the editting form
//	var ctls = pr.getElementsByTagName('input');
	for (var x in activeRow.ctls)
	{
	// skip the row object itself
	//	if (x == 'row'){ continue; }

		var sctl = document.getElementById('sub_frm_' + x);
	// there are some row controls that do not exist in the sub form
		if (sctl) {
			if (sctl.type != 'checkbox'){ sctl.value = activeRow.ctls[x].value; }
			else {sctl.checked = activeRow.ctls[x].checked; }
		}
	}
	document.getElementById('sub_frm_submit').value = sub_frm_action['edit'];
	document.getElementById('sub_frm_action').value = 'edit';
}

function openURL(e) {
	if (confirm('Click OK to open URL in a new window.\nClick Cancel to copy select the text.')){
		window.open(Yobj.getTarget(e).value, 'vendorURL');
	} else { Yobj.getTarget(e).select(); }
}

function raiseMessage(msg,alert) {
	var div = document.getElementById('flipflop');
	div.className = alert;
	div.style.visibility = 'visible';
	document.getElementById('messages').innerHTML = msg;
}

function resetRowsToDefault() {
	resetSubForm();
	if (typeof activeRow.row == 'undefined'){ return; }
	activeRow.row.className = activeRow.previousClass ? activeRow.previousClass : 'default';
// reinitialize the active row
	activeRow = new Array();
}

function resetSubForm() {
	document.getElementById('sub_frm_submit').value = sub_frm_action['add'];
	document.getElementById('sub_frm_action').value = 'add';
	subFrm.reset();
	document.getElementById('sub_frm_url').focus();
}

function searchFrmSubmit(frm) {
	YAHOO.util.Connect.setForm(frm);
// it doesn't seem to work with this form... select controls maybe?
//	var vidctl = document.getElementById('popup_vendor_id');
//	var activectl = document.getElementById('vendor_search_frm_active');
//	var url = frm.action + '?vendor_id=' + vidctl.options[vidctl.selectedIndex].value +
//		';_action=' + document.getElementById('vendor_search_frm_action').value +
//		';active=' + activectl.options[activectl.selectedIndex].value;
//	var rv = YAHOO.util.Connect.asyncRequest("get", url, searchFrmSubmitCallback);
	var rv = YAHOO.util.Connect.asyncRequest("GET", frm.action, searchFrmSubmitCallback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else { raiseMessage('Please Wait', 'wait'); }
}

function searchFrmSubmitSuccess(o) {
// 'success' doesn't mean we were successful, it just means we got a 200 response
// to be really sure we will test to see if the response resembles what we expect
	if (o.responseText.search(/^<root>/) == -1){
		raiseMessage(o.responseText, 'alert');
		return;
	}
	lowerMessage();
	document.getElementById('addedit').style.visibility = 'visible';
	clearMainTbl();
	var vendor = XMLParse.xml2ObjArray(o.responseXML,'vendor');

	for (var val in vendor[0]){
		switch (val){
			case 'vendor_id':
				var anchor = document.getElementById('td_vendor_id_a');
				anchor.href = '/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=' + vendor[0][val];
				insertTxtNode(vendor[0][val], anchor);
				document.getElementById('sub_frm_vendor_id').value = vendor[0][val];
				break;
			case 'vendor_name':
				insertTxtNode(vendor[0][val], 'td_vendor_name');
				break;
			case 'url':
			// the page will contain an empty anchor tag to start with so we'll keep reusing it
				var anchor = document.getElementById('td_vendor_url_a');
				anchor.href = vendor[0][val];
				insertTxtNode(vendor[0][val], anchor);
				break;
		}
	}
// fill in the ads tbl
	var ads = XMLParse.xml2ObjArray(o.responseXML,'params');

	if (ads.length > 0){
		flipPAdUrls('off');
		for (var x=0;x < ads.length;x++){
			createAdRow(ads[x]);
			initAdRow(ads[x]);
		}
// reset the sub form so a record can be added
		resetSubForm();
	} else {
		flipPAdUrls('on');
	}
	document.getElementById('sub_frm_url').focus();
}

function subFrmSubmit(frm) {
// first do some values checking and what not
	var url = document.getElementById('sub_frm_url');
	var notes = document.getElementById('sub_frm_staff_notes');
	switch (false){
		case url.value > '':
			alert('The URL field is empty');
			url.focus();
			break;
		case url.value.search(/^https?:\/\//) == 0:
			alert("The URL must begin with 'http://' or 'https://'");
			url.focus();
			break;
		case notes.value > '':
			alert('Please include some descriptive notes for posterity');
			notes.focus();
			break;
		default:
			var startdate = document.getElementById('sub_frm_start_date').value;
		// if we have inserted a date there will be numbers
		// if not it will still say 'Today' or whatever
		// in that case we will empty it so that the default value will kick in server side
			if (startdate.search(/\d/) == -1){ startdate.value = ''; }
			YAHOO.util.Connect.setForm(frm);
			var rv = YAHOO.util.Connect.asyncRequest("GET", frm.action, subFrmSubmitCallback);
			if (rv == null){ alert('XMLHttpRequest failure'); }
			else { raiseMessage('Please Wait', 'wait'); }
	}
}

function subFrmSubmitSuccess(o) {
// 'success' doesn't mean we were successful, it just means we got a 200 response
// to be really sure we will test to see if the response resembles what we expect
	if (o.responseText.search(/^<params>/) == -1){
		raiseMessage(o.responseText, 'alert');
	} else {
		lowerMessage();
		var res = XMLParse.xml2ObjArray(o.responseXML,'params');

// if we are doing an Add, then we need to create the new row first
		if (document.getElementById('sub_frm_action').value == 'add')
		{
			flipPAdUrls('off');
			createAdRow(res[0]);
		}
// transfer the response values into the active row
		initAdRow(res[0]);
// mark the active row
		activeRow.row.className = 'success';
// now reinitialize the object so that the 'success' row will retain it's marking
		activeRow = new Array();
// reset the sub form so a record can be added
		resetSubForm();
	}
}

function subFrmSubmitFailure(o) {
	raiseMessage('There has an unexpected error:<br />\n' + o.responseText, 'alert');
}

function toggleHelp(flg){
	var help = document.getElementById('helpinner');
	var showhelp = document.getElementById('showhelp');
	var hidehelp = document.getElementById('hidehelp');
	if (flg=='on'){help.style.display = 'block'; hidehelp.style.display='inline'; showhelp.style.display='none';}
	else {help.style.display = 'none'; showhelp.style.display='inline'; hidehelp.style.display='none'; }
}
