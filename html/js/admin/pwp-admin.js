/*
pwp-admin.js
routines for use with the admin script pwp-admin.cgi
*/

function subFrmSubmit(frm){
	// copy the input element values from a 'sub' form to the main form for submitting
	// this saves having to create all the hidden tags in each sub form
	// plus it allows the main form to be submitted with the same query selections so it will reload
	var mainfrm = document.getElementById('mainfrm');
	mainfrm.pk.value = frm.pk.value;
	mainfrm.void.value = frm.void.checked == true ? 1:0;
	mainfrm.status.value = frm.status.options[frm.status.selectedIndex].value;
	mainfrm._action.value = 'update';
	mainfrm.notes.value = frm.notes.value;
	mainfrm.submit();
	return false;
}

function toggleNote(pk){
	var tx = document.getElementById('tx' + pk);
	tx.className = tx.className == 'txshow' ? 'txhide' : 'txshow';
}