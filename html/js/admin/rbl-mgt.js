function init() {
	document.forms.main.network.focus();

	if (typeof document.forms.main.stamp != 'undefined') {
		document.forms.main.stamp.readOnly = true;
	}
	if (document.forms.main.actionBtn.value != 'Add') {
		document.forms.main.network.readOnly = true;
	}
}