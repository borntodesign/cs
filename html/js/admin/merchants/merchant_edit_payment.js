
	function creditCardVisibility(show)
	{

		document.getElementById('creditCard').className = show ? 'style5':'';
		document.getElementById('creditCardNumber').className = show ? 'style5':'';
		document.getElementById('ccvNumber').className = show ? 'style5':'';
		document.getElementById('cardExpirationDate').className = show ? 'style5':'';
		document.getElementById('nameOnCard').className = show ? 'style5':'';
		document.getElementById('billingAddress').className = show ? 'style5':'';
		document.getElementById('billingCity').className = show ? 'style5':'';
		document.getElementById('billingCountry').className = show ? 'style5':'';
		document.getElementById('billingState').className = show ? 'style5':'';
		document.getElementById('billingZip').className = show ? 'style5':'';
		
		document.getElementById('creditCardTypeRow').className = show ? 'show':'hide';

		document.getElementById('creditCardNumberRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCCVRow').className = show ? 'show':'hide';
		document.getElementById('creditCardExpirationDateRow').className = show ? 'show':'hide';
		document.getElementById('creditCardNameRow').className = show ? 'show':'hide';
		document.getElementById('creditCardDividerRow').className = show ? 'show':'hide';
		document.getElementById('creditCardAddressRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCityRow').className = show ? 'show':'hide';
		document.getElementById('creditCardCountryRow').className = show ? 'show':'hide';
		document.getElementById('creditCardStateRow').className = show ? 'show':'hide';
		document.getElementById('creditCardPostalCodeRow').className = show ? 'show':'hide';
		
	}
	
	function ecoCardVisibility(show)
	{
		document.getElementById('ecoCardNumber').className =  show ? 'style5':'';
		document.getElementById('ecoCardAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('ecoCardExplanationRow').className = show ? 'show':'hide';
	}
	
	function clubAccountVisibility(show)
	{
		//document.getElementById('clubAccountNumber').className =  show ? 'style5':'';
		//document.getElementById('clubAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('clubAccountExplanationRow').className = show ? 'show':'hide';
		document.getElementById('payingSponsorClubAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('payingSponsorClubAccountBalanceRow').className = show ? 'show':'hide';
		document.getElementById('payingMonthlyClubAccountNumberRow').className = show ? 'show':'hide';
		document.getElementById('payingAnnuallyClubAccountNumberRow').className = show ? 'show':'hide';
	}

	function clubAccountBalanceVisibility(show)
	{
		document.getElementById('clubAccountBalanceRow').className = show ? 'show':'hide';
	}


	function internationalFundsTransferVisibility(show)
	{
		//document.getElementById('internationalFundsTransferNumber').className = show ? 'style5':'hide';
		//document.getElementById('internationalFundsTransferNumberRow').className = show ? 'show':'hide';
		document.getElementById('internationalFundsTransferExplanationRow').className = show ? 'show':'hide';
	}
	
	function paymentRequirements(PaymentMethodSelectBox)
	{
		switch(PaymentMethodSelectBox[PaymentMethodSelectBox.selectedIndex].value)
		{
			case 'cc':
				creditCardVisibility(true);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				clubAccountBalanceVisibility(false);
				break;
			
			case 'ec':
				creditCardVisibility(false);
				ecoCardVisibility(true);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				clubAccountBalanceVisibility(true);
				break;		
			
			case 'ca':
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(true);
				internationalFundsTransferVisibility(false);
				clubAccountBalanceVisibility(true);
				break;
			
			case 'if':
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(true);
				clubAccountBalanceVisibility(true);
				break;
			
			default:
				creditCardVisibility(false);
				ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				clubAccountBalanceVisibility(false);
		}
		
	}
	
	
	/**
	 * @author Keith
	 * @description	This will allow the "Submit" button to be pressed only once.
	 */
	$(document).ready(
		function()
		{
			
			$("form").submit(
				function(){
					$('input[type="submit"]').attr("value", "Processing");// This is used to rename the value that is displayed to the user, this is needed if you are using a button, instead of an image.
					$('input[type="submit"]').attr("disabled", true);
					return true;
				}
			);
			
			$("textarea[name='discount_blurb']").maxlength({'feedback' : '.charsLeft'});
			
			$("input[name='discount_type']:checked").each(
				function()
				{
					setPackageBenefits(this.value);
				}
			);
			
			$("input[name='discount_type']").change(
				function()
				{
					setPackageBenefits(this.value);
				}
			);
			
			
			$('#offline_reward_discounts_percentage_off').change(function() {
				$("#percentage_off").removeAttr("disabled");
				disableFlatFeeOff();
				disableSpecial();
				clearPercentageOfSale()
			});
			
			$('#offline_reward_discounts_flat_fee_off').change(function() {
				disablePercentageOff();
				$('#flat_fee_off').removeAttr("disabled");
				disableSpecial();
				clearPercentageOfSale()
			});
			
			$('#offline_reward_special').change(function() {
				disablePercentageOff();
				disableFlatFeeOff();
				$('#special').removeAttr("disabled");
				clearPercentageOfSale()
			});
			
			$('#percentage_of_sale').change(function() {
				disablePercentageOff();
				disableFlatFeeOff();
				disableSpecial();
				$("input[name='special']").removeAttr("checked");
			});
			
			
			adjustInitialStateCtlFocus();
			setSelectPrompt("- Please select from this list -");
			
		}
	);
