

function expandContract(objects_id, LinkObject)
{
	
	LinkObject.innerHTML = (LinkObject.innerHTML == '+') ? '--':'+';
	
	$('#' + objects_id + '_headers').toggle();
	$('#' + objects_id + '_information').toggle();
}

function searchBoxes(SearchSelectBox)
{
	switch(SearchSelectBox[SearchSelectBox.selectedIndex].value)
	{
		case 'country':
			$("select[name='country']").toggle(true);
			$("select[name='state']").toggle(false);
			$("select[name='discount_type']").toggle(false);
			$("input[name='search_value']").toggle(false);
			break;
			
		case 'state':
			$("select[name='country']").toggle(false);
			$("select[name='state']").toggle(true);
			$("select[name='discount_type']").toggle(false);
			$("input[name='search_value']").toggle(false);
			break;
			
		case 'discount_type':
			$("select[name='country']").toggle(false);
			$("select[name='state']").toggle(false);
			$("select[name='discount_type']").toggle(true);
			$("input[name='search_value']").toggle(false);
			break;
			
		default:
			$("select[name='country']").toggle(false);
			$("select[name='state']").toggle(false);
			$("select[name='discount_type']").toggle(false);
			$("input[name='search_value']").toggle(true);
	}
}


$(document).ready(
	function()
	{
		$('input[type="submit"]').attr("disabled", false);
		$("form").submit(
			function()
			{
					$('input[type="submit"]').attr("value", "Processing");// This is used to rename the value that is displayed to the user, this is needed if you are using a button, instead of an image.
					$('input[type="submit"]').attr("disabled", true);
					return true;
			}
		);


		$("select[name='search_field']").change(
				function()
				{
					searchBoxes(this);
				}
		);

		if(document.getElementById('search_field'))
			searchBoxes(document.getElementById('search_field'));
	
		$("#search_field").focus();
	
	}
);
