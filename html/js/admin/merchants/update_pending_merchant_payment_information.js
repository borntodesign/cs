
	function creditCardVisibility(show)
	{
		$('#credit_card_payment_options').toggle(show);
	}
	
	
	function clubAccountVisibility(show)
	{
		$('#club_account_payment_options').toggle(show);
	}

	function internationalFundsTransferVisibility(show)
	{
		$('#international_funds_transfer_payment_options').toggle(show);
	}
	

	
	
/*
 * These functions pertain to Payment Options.
 */
	
	function paymentRequirements(PaymentMethodSelectBox)
	{
		switch(PaymentMethodSelectBox[PaymentMethodSelectBox.selectedIndex].value)
		{
			case 'cc':
				creditCardVisibility(true);
				//ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				break;
			
/*			case 'ec':
				creditCardVisibility(false);
				ecoCardVisibility(true);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
				break;		
*/			
			
			case 'ca':
				creditCardVisibility(false);
				//ecoCardVisibility(false);
				clubAccountVisibility(true);
				internationalFundsTransferVisibility(false);
				break;
			
			case 'if':
				creditCardVisibility(false);
				//ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(true);
				break;
			
			default:
				creditCardVisibility(false);
				//ecoCardVisibility(false);
				clubAccountVisibility(false);
				internationalFundsTransferVisibility(false);
			
		}
		
	}



		 $(document).ready(
				 function()
				 {
					/**
					 * @author Keith
					 * @description	This will allow the "Submit" button to be pressed only once.
					 */
					//$('input[type="image"]').attr("disabled", false); // This is probably not needed, but you can enable it if you desire.
					$("form").submit(function(){
						$('input[type="image"]').attr("src", "/images/btn/btn_processing_91x27.png");
						$('input[type="image"]').attr("disabled", true);
						return true;
					});
					
					 adjustInitialStateCtlFocus();setSelectPrompt("- Please select from this list -");
					 
					 paymentRequirements(document.getElementById('payment_method'));
			
				 }
			);

