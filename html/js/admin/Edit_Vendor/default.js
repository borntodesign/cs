/**
 * 
 */

function validate(cform) {
	if ( cform.vendor_name.value == "" ) 
	{
		alert("Please enter a vendor name.");
		cform.vendor_name.focus();
		return false;
    }
	
	if ( isNaN(cform.discount.value) )
	{
	 	alert("Please enter a only digits as a discount value 0.16 etc...");	
	 	cform.discount.focus();
		return false;
	}
	
	if ( cform.discount.value == "" )
	{
	 cform.discount.value = '0'; 
	}
	
	if ( isNaN(cform.payout.value) )
	{
	 	alert("Please enter a only digits as a payout value 0.16 etc...");	
	 	cform.payout.focus();
		return false;
	}
	
	if ( cform.payout.value == "" )
	{
	 cform.payout.value = '0'; 
	}
	
	if ( cform.vendor_group.selectedIndex == -1 )
	{
	  	alert("Please select a Vendor Group");
		cform.vendor_group.focus();
		return false;
	}
	
	if ( cform.status.selectedIndex == -1 )
	{
	  	alert("Please select a Vendor Status");
		cform.status.focus();
		return false;
	}
	
	if ( cform.url.value == "" ) 
	{
		alert("Please enter the vendors URL http://");
		cform.url.focus();
		return false;
    }
	
	if ( cform.country.selectedIndex == -1 )
	{
	  	alert("Please select a Country");
		cform.country.focus();
		return false;
	}
	
	return true;
}
