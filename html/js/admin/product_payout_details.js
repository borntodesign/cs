// product_payout_details.js
// to work in conjunction with product_payout_details.cgi

var $E,$D,dataTblBody,workingFrm;

function btnClick(e){
	btn = $E.getTarget(e);
	var action = btn.value == 'Add' ? 'add' : 'update';
//	var row = btn.parentNode;
//	var row = myTr(row);
	submitData(action, myTr(btn));
}

function confirmChanges(o){
	if (! o.responseXML){
		alert("Unanticipated error:\n" + o.responseText);
		return;
	}

	var xml = o.responseXML.getElementsByTagName('tr')[0];

	populateRow(o.argument[0],xml,true);
	lowerMessage();
	// add another empty row if this was an insert
	if (o.argument[1] == 'add'){
		var row = dataTblBody.lastChild;
		var newRowId = row.id.match(/\d*$/);
		createRow(++newRowId, row.className == 'row0' ? 'rowE' : 'row0');
	}
}

function containerFrmSubmit(e){
	ctl = $E.getTarget(e);
	alert(ctl);
}

function createButton(rowID){
	var ctl = document.createElement('input');
	ctl.type = 'button';
	ctl.id = 'btn_' + rowID;
	ctl.value = 'Add';
	ctl.className = 'btn';
	$E.addListener( ctl , 'click' , btnClick);
	return ctl;
}

function createCheckBox(iname){
	var ctl = document.createElement('input');
	ctl.type = 'checkbox';
	ctl.name = iname;
	ctl.value = 1;
	// assuming this routine will only create 'void' checkboxes
	ctl.tabIndex = '-1';
	return ctl;
}

function createRow(rowID, className){
	if (! className){ className = 'rowE'; }
	var newRow = document.createElement('tr');
	newRow.className = className;
// create a memory of our initial class value
	newRow.original_className = className;
	newRow.id = 'row' + rowID;
	newRow.appendChild( createTD( 'pk', createTextInput('pk',true)));
	newRow.appendChild( createTD( 'description', createTextInput('description')));
	newRow.appendChild( createTD( 'rp', createTextInput('rp')));
	newRow.appendChild( createTD( 'pp', createTextInput('pp')));
	newRow.appendChild( createTD( 'stamp', createTextInput('stamp',true)));
	newRow.appendChild( createTD( 'operator', createTextInput('operator',true)));
	newRow.appendChild( createTD( 'void', createCheckBox('void')));
	newRow.appendChild( createTD( 'btn', createButton(newRow.id)));
	dataTblBody.appendChild(newRow);
	return newRow;
}

function createTD(className,ctl){
	var newTD = document.createElement('td');
	newTD.className = className;
	newTD.appendChild(ctl);
	return newTD;
}

function createTextInput(iname,RO){
	var ctl = document.createElement('input');
	ctl.type = 'text';
	ctl.name = iname;
	if (RO){ ctl.disabled = 'disabled'; ctl.className = 'disabled'; }
	return ctl;
}

function GeneralResponseFailure(o) {
	alert("There has an unexpected error:\n" + o.responseText);
}

function init()
{
	$E = YAHOO.util.Event;
	$D = YAHOO.util.Dom;
	workingFrm = document.getElementById('workingFrm');
	dataTblBody = document.getElementById('dataTblBody');
// we put an empty row in the table to keep the validator happy... now we'll remove it
	dataTblBody.innerHTML = ''; // ;)
	
	$E.addListener( document.getElementById('containerFrm') , 'submit' , containerFrmSubmit);

	var callback = 
	{
		success: loadData,
		failure: GeneralResponseFailure,
		argument: [0]
	}
	var url = '/cgi/admin/product_payout_details.cgi?mall_id=' + $MALL_ID + ';vendor_id=' + $VENDOR_ID + ';_action=list';
	var rv = YAHOO.util.Connect.asyncRequest("GET", url, callback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
}

function loadData(o){
	if (! o.responseXML){
		alert("Unanticipated error:\n" + o.responseText);
		return;
	}
	var rows = 	o.responseXML.getElementsByTagName('tr');
	var className = 'rowO';
	var x;
	for (x=0; x < rows.length; x++){
		populateRow( createRow(x,className), rows[x]);
		className = (className == 'rowE') ? 'rowO' : 'rowE';
	}
	createRow(x,className);
}

function lowerMessage() {
        var div = document.getElementById('flipflop');
        div.className = '';
        div.style.visibility = 'hidden';
}

function myTr(row){
	while (row.nodeName.toUpperCase() != 'TR'){
		row = row.parentNode;
	}
	return row;
}

function populateRow(row,xml,changed){
	if (changed && ! row.className.match(/modified/)){
		row.className = 'modified';
		row.original_className = row.className;
	}

	var tds = row.getElementsByTagName('td');

	for (var x=0; x < tds.length; x++){
		var ctl = tds[x].firstChild;
		if (ctl.type == 'text'){
			var txtNode = xml.getElementsByTagName(ctl.name)[0].firstChild;
			ctl.value = txtNode ? txtNode.nodeValue : '';
		}
		else if (ctl.type == 'checkbox'){
			var txtNode = xml.getElementsByTagName(ctl.name)[0].firstChild;
			if (txtNode && txtNode.nodeValue == '1'){
				ctl.checked =  'checked';
				if (! row.className.match(/void/)){ row.className += ' void'; }
			} else {
				ctl.checked = undefined;
				row.className = row.original_className;
			}
		}
		// our button defaults to 'Add'... so we need to make a change
		document.getElementById('btn_' + row.id).value = 'Update';
	}
}

function raiseMessage(msg,alert) {
        var div = document.getElementById('flipflop');
        div.className = alert;
        div.style.visibility = 'visible';
//        document.getElementById('messages').innerHTML = msg;
	div.innerHTML = msg;
}

function subFrmSubmitFailure(o) {
        raiseMessage('There has an unexpected error:<br />\n' + o.responseText, 'alert');
}

function submitData(_action,row){
	workingFrm._action.value = _action;
	var ctls = row.getElementsByTagName('input');
	for (var x=0; x < ctls.length; x++){
	// our buttons do not have a name
		if (ctls[x].name == undefined){ continue; }
	// we don't have hidden controls for values which are not desired parameters
		var h = document.getElementById('h_' + ctls[x].name);
		if (h){
			// we have to deal with our checkbox differently
			if (ctls[x].type.toUpperCase() == 'CHECKBOX'){
				h.value = ctls[x].checked ? 1 : 0;
			} else {
				h.value = ctls[x].value;
			}
		}
	}

	// let's make sure we have at least something to submit
	// we want a description and at least an rp or a pp
	if (! (workingFrm.description.value && (workingFrm.rp.value || workingFrm.pp.value))){
		alert('There should at least be a Description and an CC or PP value');
		return;
	}

	var callback = 
	{
		success: confirmChanges,
		failure: GeneralResponseFailure,
		argument: [row,_action]
	}
	
	YAHOO.util.Connect.setForm(workingFrm);
	var rv = YAHOO.util.Connect.asyncRequest("GET", workingFrm.action, callback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else { raiseMessage('Please Wait', 'wait'); }

}
