// started for vendor-mall-relations.cgi
var ctl_list = new Array('pp_range_lower','pp_range_upper','mrp_range_lower','mrp_range_upper', 'vrp_range_lower','vrp_range_upper');

function clearRanges(){
	for (var x=0; x < ctl_list.length; x++){
		document.getElementById(ctl_list[x]).value = '';
	}
}

function hiliteCtls(){
	for (var x=0; x < ctl_list.length; x++){
		document.getElementById(ctl_list[x]).className += ' hilite-imports';
	}
}

function importURL(){
	var ctl = document.getElementById('url');
	ctl.value = imported_url;
	ctl.className += ' hilite-imports';
}

function init(arg){
	if (arg == 'new'){ hiliteCtls(); }
}

function validate_frm(frm){
	return true;
}

function validate_new(frm){
	// make sure a mall has been selected
	if (document.getElementById('mall_id').selectedIndex == 0){
		alert('Please select a Mall Relation');
		return false;
	}
	return validate_frm(frm);
}

function ToggleHelp(){
	var help = document.getElementById('help');

	help.style.visibility = help.style.visibility == 'visible' ? 'hidden':'visible';
	return false;
}
