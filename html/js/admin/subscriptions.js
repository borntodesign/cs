/**
 * 
 */
function ckExtensionsFrm(frm){
				  // if there is only 1 radio button, an array will not be created
					if (frm.subscription_type.length == undefined){
						if (frm.subscription_type.checked == true){ return true; }
					}
					else {
						for (var x=0; x < frm.subscription_type.length; x++){
							if (frm.subscription_type[x].checked == true){ return true; }
						}
					}
					alert('Select a subscription type');
					return false;
}

function nuwin(arg){ return ! window.open(arg); }

function toggleContainer(cnt){
					cnt.style.display = (cnt.style.display != 'block' ? 'block' : 'none');
				}

function toggleNotes(trans_id){
					var div = document.getElementById('hidden'+trans_id);
					toggleContainer(div);
					return false;
				}

function cancelSubscription(a,pk){
					var cancelbx = document.getElementById('cancelbx');
					document.getElementById('pk').value = pk;
					document.getElementById('cancelfrm_submit').value = 'Cancel Subscription #' + pk;

					cancelbx.style.visibility = 'visible';
				}

function hideCancelBx(){
					var cancelbx = document.getElementById('cancelbx');
					cancelbx.style.visibility = 'hidden';
				}