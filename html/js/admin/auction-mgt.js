// prompt the user to let them know they are not using the highest bid and be sure they want to proceed
function confirmBid(){
	if (bid_id == high_bid_id){ return true; }
	else{
		return confirm('The bid you are collecting on is not the highest one for the auction. Proceed?');
	}
}