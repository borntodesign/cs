/**
 * Just a some stuff to use in the admin interface by the same name... Just too cumbersome to put into the header with CGI.pm
 */

function unlockSeq() {
	if ($('#input_seq').prop('disabled') == true) {
		var r = confirm("Assigning/changing this value determines the position of the party in the Trial Partner line.\nThis does not affect the real nspid sponsorship.\nLower numbers go above higher numbers.\n For example: 4000 would be above 4001\n\nDo you really want to set this value?");
		if (r == true) {
			$('#input_seq').prop("disabled", false).focus();
		}
	}
};