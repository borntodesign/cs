// mallcat-admin.js - for the mall category administration functions - data provided by mallcat-admin.cgi

// some globals for use inside callbacks
var ACTIVE_CATID = 0;
var VENDOR_ID = ''; // this is -not- used for simple category administration, only for vendor-category administration
var GLOBAL1;

// some constants
//var Mallcat_Admin_URL = 'https://www.clubshop.com/cgi/admin/mallcat-admin.cgi?';
var Yobj = YAHOO.util.Event;

var catidResultsCallback =
{
  success: catidResultsSuccess,
  failure: GeneralResponseFailure
}

var newcatnameSubmitCallback =
{
	success: newcatnameSubmitSuccess,
	failure: GeneralResponseFailure
}

var linkVendor2CatSubmitCallback =
{
	success: catidResultsSuccess,
	failure: GeneralResponseFailure
}

var linkVendor2CatFromText =
{
	success: handleLinkFromText,
	failure: GeneralResponseFailure
}

function CreateCatCtlEvents(root)
{
//	root = root.getElementById('td_' + ACTIVE_CATID);
	// for now we are looking for p's, that could change and then we'll have to redefine here
	var plist = root.getElementsByTagName('p');
	for(var myp=0; myp < plist.length; myp++){
		if (plist[myp].className == 'expandcat') {
			if (! Yobj.addListener(plist[myp], 'click', toggleCategory)){
				alert('Failed to setup listener on: expandcat p');
			}
		}
	}
}

function CreateCatLinkEvents()
{
	var alist = document.getElementById('taxonomy');
	// when the page first loads there is no 'taxonomy'
	if (alist == undefined){ return; }
	alist = alist.getElementsByTagName('a');
	for(var myp=0; myp < alist.length; myp++){
		if (! Yobj.addListener(alist[myp], 'click', linkCatFromText)){
			alert('Failed to setup listener on: taxonomy list anchor');
		}
	}
}
function catidResultsSuccess(o)
{
// 'success' doesn't mean we were successful, it just means we got a 200 response
// to be really sure we will test to see if the response resembles what we expect
	if (o.responseText.search(/^<table/) == -1){
		raiseMessage(o.responseText, 'alert');
	} else {
		lowerMessage();
		var dest = document.getElementById('td_' + ACTIVE_CATID);
		//dest.appendChild( document.createTextNode(o.responseText) );
		dest.innerHTML = o.responseText;
		CreateCatCtlEvents(dest);
		dest.getElementsByTagName('input')[0].focus();
		UpdateParentCatCount();
	}
	return true;
}

function GeneralResponseFailure(o) {
	raiseMessage('There has an unexpected error:<br />\n' + o.responseText, 'alert');
}

function getTableByCatid(catid)
{
	var rv = YAHOO.util.Connect.asyncRequest("GET", Mallcat_Admin_URL + 'catid=' + catid + ';vid=' + VENDOR_ID , catidResultsCallback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
		ACTIVE_CATID = catid;
		raiseMessage('Please Wait', 'wait');
	}
}

function handleLinkFromText(o)
{
// 'success' doesn't mean we were successful, it just means we got a 200 response
// to be really sure we will test to see if the response resembles what we expect
	if (o.responseText.match(/Software error/) != null){
		raiseMessage(o.responseText, 'alert');
	} else {
		lowerMessage();
		// the server response is a text string just like the existing category entries
		// so we will insert it at the top of the list on the taxonomy document
		var dest = window.opener.document.getElementById('taxonomy');
		//var firstli = dest.firstChild;	*this doesn't work in IE!
		var firstli = dest.getElementsByTagName('li')[0];
		//firstli = firstli[0];
		var newli = window.opener.document.createElement('li');
		newli.className = 'newlink';
		var newtxt = window.opener.document.createTextNode(o.responseText);
		newli.appendChild(newtxt);
		if (firstli == undefined){ dest.appendChild(newli); }
		else { dest.insertBefore(newli,firstli); }
		// we will also delete from the list on the search document itself
		// the anchor that activated this event is available in GLOBAL1
		GLOBAL1.parentNode.removeChild(GLOBAL1);
	}
	return true;
}

function linkCatFromText(e)
{
	var elem = window.event ? window.event.srcElement : e.target;
	// the event is triggered by the span tag when clicked there
	// even though there is not an event handler on it
	while (! elem.id){
		elem = elem.parentNode;
	}
	// our href is simply a named anchor like this #1234
	var catid = elem.id.match(/\d+/);
	if (catid == null){ alert('Failed to get the catid - elem='+elem); return; }
	var rv = YAHOO.util.Connect.asyncRequest("GET", TEXTLINK_URL + catid, linkVendor2CatFromText);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
		raiseMessage('Please Wait', 'wait');
		GLOBAL1 = elem;
	}
}

function linkVendor2Cat(frm)
{
	ACTIVE_CATID = frm.catid.value;
	YAHOO.util.Connect.setForm(frm);
	var rv = YAHOO.util.Connect.asyncRequest("GET", frm.action, linkVendor2CatSubmitCallback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
		raiseMessage('Please Wait', 'wait');
	}
}

function lowerCats2Add(catid){
	lowerCats2AddCtl(document.getElementById('cats2add_' + catid));
}

function lowerCats2AddCtl(ctl){ ctl.size=1; }

function lowerMessage() {
	var div = document.getElementById('flipflop');
	div.className = '';
	div.style.visibility = 'hidden';
}

function newcatnameSubmitSuccess(o) {
	lowerMessage();
	var dest = document.getElementById('table_' + ACTIVE_CATID).getElementsByTagName('tbody')[0];
	dest.innerHTML = o.responseText + dest.innerHTML;
	var sub_cat_cnt = document.getElementById('p_' + ACTIVE_CATID).innerHTML;
	sub_cat_cnt++;
	document.getElementById('p_' + ACTIVE_CATID).innerHTML = sub_cat_cnt;
	CreateCatCtlEvents(dest);
}

function raiseCats2AddCtl(ctl){ ctl.size=5; }

function raiseMessage(msg, alert) {
	var div = document.getElementById('flipflop');
	div.className = alert;
	div.style.visibility = 'visible';
	div.innerHTML = msg;
	if (alert != 'alert'){ return; }
	var newtxt = document.createTextNode('Close Error Message');
	var newnode = document.createElement('p');
	var newa = document.createElement('a');
	newa.href = '#';
	Yobj.addListener(newa, 'click', lowerMessage);
	newa.appendChild(newtxt);
	newnode.appendChild(newa);
	div.appendChild(newnode);
}

function submitNewCatName(frm)
{
	ACTIVE_CATID = frm.catid.value;
	YAHOO.util.Connect.setForm(frm);
	var rv = YAHOO.util.Connect.asyncRequest("GET", frm.action, newcatnameSubmitCallback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
		raiseMessage('Please Wait', 'wait');
		frm.catname.value = '';
	}
}

function toggleCategory(e)
{
	var elem = window.event ? window.event.srcElement : e.target;
	elem.style.color='red';
	// if the category has already been expanded, the resulting table can be tested for
	var catid = elem.id.match(/\d+/);
	var tbl = document.getElementById('table_' + catid);
	// we could just blow away the table if it existed
	// but instead, we'll simply make it disappear... ready to flash back if necessary
	if (! tbl) {
		getTableByCatid(catid);
	} else {

		if (! tbl.style.display || tbl.style.display == 'table'){ tbl.style.display = 'none'; }
		else {
			tbl.style.display = '';
			tbl.getElementsByTagName('input')[0].focus();
		}
	}
}

function toggleCats2Add(catid)
{
	var ctl = document.getElementById('cats2add_' + catid);
	if (ctl.size == 1 && ctl.length > 1){
		raiseCats2AddCtl(ctl);
	} else {
		// if we have anything selected besides option -0-
		// then we will 'submit' the form
		for (var i=1; i < ctl.length; i++){
			if (ctl.options[i].selected){
				linkVendor2Cat(document.getElementById('form_' + catid));
				break;
			}
		}
		lowerCats2AddCtl(ctl);
	}
}

function UnlinkVendorCat(elem,catid)
{
	var rgx = /table_(\d+)/;
	var tmp;
	var myParentCat = '';
	while (myParentCat == ''){
		elem = elem.parentNode;
		tmp = elem.id.match(rgx);
		if (tmp != undefined){
			var myParentCat = tmp[1];
		}
	}
// alert('parent='+myParentCat); return;
	var rv = YAHOO.util.Connect.asyncRequest("GET", Mallcat_Admin_URL + 'catid=' + catid + ';vid=' + VENDOR_ID + ';_action=unlink;parent=' + myParentCat, linkVendor2CatSubmitCallback);
	if (rv == null){ alert('XMLHttpRequest failure'); }
	else {
		ACTIVE_CATID = myParentCat;
		raiseMessage('Please Wait', 'wait');
	}
}

function UpdateParentCatCount()
{
// there is no element p_0 as that is the root category... so to avoid javascript errors at startup...
	if (ACTIVE_CATID ==0){ return; }
	var myp = document.getElementById('p_' + ACTIVE_CATID);
	var tbl = document.getElementById('table_' + ACTIVE_CATID);
	var rows = tbl.getElementsByTagName('tbody')[0];
	myp.innerHTML = rows.getElementsByTagName('tr').length;
}

function VendorCatAlert(elem,catid)
{
	if (confirm('Category ID: ' + catid + "\n\nDelete this category link?\n\n" +
		"All children will be deleted automatically\n\n"))
	{
		UnlinkVendorCat(elem,catid);
	}
}

