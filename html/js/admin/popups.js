/**
 * If the current window was open by a parent window, the current window will close.
 * 
 * @param Bool	reload	Set to true if you want the calling window to refresh, before this window closes.
 * 
 * @return	null
 */
function closeMe(reload)
{
	try{
		if(opener)
		{
			if(reload)
				opener.document.location.reload();
		
			self.close();
		}
	}
	catch(e)
	{
		//Nothing to do here.
	}
	
}