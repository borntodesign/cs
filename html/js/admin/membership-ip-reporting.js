// scripting to work with the membership-ip-reporting.cgi interface
var mainFrm;

function changeWindowType(rset)
{
	// invoked when one of the search type radio buttons is clicked
	if (rset.value == 'host'){
		set_window_type('host');
	}
	else {
		set_window_type('same');
	}
}

function ClickNgo(e)
{
	var mytd = e.target;
	mainFrm.search_value.value = mytd.innerHTML;
	set_search_type(mytd.className);
//	mainFrm.submit();
}

function dblClickNgo(e)
{
	var mytd = e.target;
	// just load the data in, set the type and load into the same window
	mainFrm.search_value.value = mytd.innerHTML;
	set_search_type(mytd.className);
	set_window_type('new');
//	mainFrm.submit();
}

function frmSubmit(frm)
{
//	alert(frm.window_type);
	for (var x=0; x < frm.window_type.length; x++){
		if (frm.window_type[x].checked == true && frm.window_type[x].value == 'new'){ frm.target = '_blank'; }
		if (frm.window_type[x].checked == true && frm.window_type[x].value == 'host'){ frm.target = 'iframe'; }
		else { frm.target = '_self'; }
	}
}

function init()
{
	mainFrm = document.getElementById('mainFrm');
	// setup event listener's on our ipaddr and ID fieldset
	// single click will simply open in the same window
	//dbl clicking will open in a new window
	var elems = document.getElementsByTagName('td');
	for (var x=0; x < elems.length; x++){
		if (elems[x].className == 'ip' || elems[x].className == 'id'){
			elems[x].addEventListener('click', ClickNgo, true);
			elems[x].addEventListener('dblclick', dblClickNgo, true);
		}
	}
	elems = document.getElementsByTagName('a');
	for (var x=0; x < elems.length; x++){
		if (elems[x].className == 'name'){
			elems[x].addEventListener('click', openNewWindow, true);
		}
	}
}

function openNewWindow(e)
{
	window.open(e.target.href);
	preventDefault();
}

function set_search_type(mytype)
{
	// indirectly invoked when an ID or an ip is clicked
	for (var x=0; x < mainFrm.search_type.length; x++){
		if (mainFrm.search_type[x].value == mytype){
			mainFrm.search_type[x].selected = true;
		}
		else { mainFrm.search_type[x].selected = false; }
	}
}

/*
{
	// indirectly invoked when an ID or an ip is clicked
	for (var x=0; x < mainFrm.search_type.length; x++){
		if (mainFrm.search_type[x].value == mytype){
			mainFrm.search_type[x].checked = true;
		}
		else { mainFrm.search_type[x].checked = false; }
	}
}
*/

function set_window_type(mytype)
{
	for (var x=0; x < mainFrm.window_type.length; x++){
		if (mainFrm.window_type[x].value == mytype){ mainFrm.window_type[x].checked = true; }
		else { mainFrm.window_type[x].checked = false; }
	}
}