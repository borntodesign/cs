// mall-mgt.js - started for the mall-mgt.cgi interfaces

// some constants
var Yobj = YAHOO.util.Event;

// some globals
var page_reload_flag;
var mall_id;

function init(){
	// setup simple window.open handlers on the mall links and the detail links
	var malls = document.getElementById('tbody-mall-mgt').getElementsByTagName('a');

	for (var x=0; x < malls.length; x++){
		switch(malls[x].className){
			case 'mall-url':
				create_URL_Handler(malls[x], create_URL_Event);
				break;
			case 'edit-url':
				if (malls[x].href.match(/#\d+$/)){
					// we are doing a mall relation edit
					// they don't have real URLs in the anchors
					create_URL_Handler(malls[x], create_MR_Edit_Event);
				} else {
			//	alert('creating event for:'+malls[x].href);
					create_URL_Handler(malls[x], create_Edit_URL_Event);
				}
				break;
			case 'vx-url':
				create_URL_Handler(malls[x], create_VX_Event);
				break;
			case 'ppods-url':
				create_URL_Handler(malls[x], create_PPoDs_Event);
				break;
			case 'label':
				create_URL_Handler(malls[x], create_Mall_Label_Event);
				break;
			case 'vid':
				create_URL_Handler(malls[x], create_VID_Event);
				break;
			case 'v-url':
				create_URL_Handler(malls[x], create_URL_Event);
				break;
		}
	}
	
	// now setup an unload handler on the body so that if we do any adds/edits, we will reload the page automatically
	setupReload();
}

function create_URL_Handler(elem, handler){
	if (! Yobj.addListener(elem, 'click', handler)){
		alert('Failed to setup listener on:' + elem);
	}
}

function create_Edit_URL_Event(evt){
	page_reload_flag = 1;
	window.open(Yobj.getTarget(evt).href);
	PreventDefault(evt);
}

function create_Mall_Label_Event(evt){
	var elem = Yobj.getTarget(evt);
	// the href should look like #123 where the digits are the mall_id
	var mall_id = elem.href.match(/\d+/);
	window.open(document.location.pathname + '?_action=vendor-list;mall_id=' + mall_id[0]);
	PreventDefault(evt);
}

function create_MR_Edit_Event(evt){
	// the href actually only contains something like #123,
	// but the browser may interpolate that to read as a complete URL
	// when referencing the href property of the object (Firefox does)
	window.open('/cgi/admin/vms/vendor-mall-relations.cgi?_action=detail;mall_id=' + mall_id + ';vendor_id=' + 	evt.target.href.match(/\d+$/)[0]);
	PreventDefault(evt);
}

function create_PPoDs_Event(evt){
	var elem = Yobj.getTarget(evt);
	window.open('/cgi/admin/product_payout_details.cgi?mall_id=' + mall_id + ';vendor_id=' + elem.href.match(/\d+$/)[0]);
	PreventDefault(evt);
}

function create_VX_Event(evt){
	var elem = Yobj.getTarget(evt);

	// the href actually only contains something like #123,
	// but the browser may interpolate that to read as a complete URL
	// when referencing the href property of the object (Firefox does)
//	window.open('/cgi/admin/vms/mall-vendor-extensions.cgi?mall_id=' + mall_id + ';vendor_id=' + evt.target.href.match(/\d+$/)[0]);
	window.open('/cgi/admin/vms/mall-vendor-extensions.cgi?mall_id=' + mall_id + ';vendor_id=' + elem.href.match(/\d+$/)[0]);
	PreventDefault(evt);
}

function create_URL_Event(evt){
//	window.open(getText(evt.target));
	window.open(getText(Yobj.getTarget(evt)));
	PreventDefault(evt);
}

function create_VID_Event(evt){
//	var elem = evt.target;
	var elem = Yobj.getTarget(evt);
	var txt = getText(elem);
	page_reload_flag = 1;
	window.open('/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=' + txt);
	PreventDefault(evt);
}

function create_VID_URL_Event(evt){
//	var elem = evt.target;
	var elem = Yobj.getTarget(evt);
	var txt = getText(elem);
	page_reload_flag = 1;
	window.open('/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=' + txt);
	PreventDefault(evt);
}

function getText(elem){
	// get the text from the container element
	// we are assuming that we have a single text node within the element
	return elem.firstChild.nodeValue;
}

function NewVendorLink(frm){	// used on the vendor-list page
	if (frm.vendor_id.selectedIndex == 0){
		alert('Please select a vendor first');
		return false;
	}
	page_reload_flag = 1;
	frm.target = '_blank';
	return true;
}

function PreventDefault(evt){
	if (evt.preventDefault){ evt.preventDefault(); }
	else { evt.returnValue = false; }
}

function setupReload(){
	if (! Yobj.addListener(document.getElementsByTagName('body')[0], 'focus', Reload)){
		alert('Failed to setup unload listener on body');
	}
}

function Reload(){
	if (page_reload_flag){
		page_reload_flag = undefined;
		document.location.reload();
	}
}

function ToggleHelp(){
	var help = document.getElementById('help');

	help.style.visibility = help.style.visibility == 'visible' ? 'hidden':'visible';
	return false;
}