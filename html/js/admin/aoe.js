function noChgDiscountAmount(me){
	if (vendor_group == 2)
	{
		alert('This value cannot be changed');
	} else {
	// we will allow changing of the field if it is not a mall store
		me.readOnly = false;
		me.select();
	}
}

function _validate() {
	if (! ckAffCode()){ return false; }

	if ( document.forms.aoe.discount.value == "" )
	{
		alert("Please supply a discount");
		document.forms.aoe.discount.focus();
		return false;
	}
  
	if ( document.forms.aoe.amount.value == "" )
	{
		alert("Please supply an amount");
		document.forms.aoe.amount.focus();
		return false;
	}
  
	if ( document.forms.aoe.discount_amount.value == "" || document.forms.aoe.discount_amount.value == 0 )
	{
		if (confirm("Please ensure this discount amount, ( " + document.forms.aoe.discount_amount.value + " ) ,is valid. If not click Cancel.")) {
			return true;
		} else {
			document.forms.aoe.discount_amount.focus();
			return false;
		}
	}
	return true;
}

function _hilite(me){ me.select(); }

function ckAffCode(){
	if (document.forms.aoe.affiliate_code.value == '' && vendor_group == 2){
		alert('This vendor is not linked to an affiliate.\nPlease have the vendor linked and then start this transaction afresh.');
		return false;
	}
	return true;
}

function init(){
	ckAffCode();
	document.forms.aoe.transaction_date.select()
	document.forms.aoe.transaction_date.focus();
}
