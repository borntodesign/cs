function biz_loaded(cur_show_window, num_attempts)
{
	show_window = cur_show_window;
	attempts = num_attempts;

       // If the get_business.cgi script hasn't started or the Please Wait
       // window hasn't been closed, continue trying for
       //   up to max_attempts seconds(1 seconds = 1000 ms).
       //   NOTE: max_attempts defined in load_business.js script.
       if ((cgi_started == 0) && (attempts < max_attempts) && ((!nuwin.closed) || (show_window == 'win_no')))
       {
		++attempts;
               setTimeout( "biz_loaded(show_window, attempts)", 1000);
       }
       // If the cgi script has started and the window is still open.
       else if ((cgi_started == 1) && ((!nuwin.closed) || (show_window == 'win_no')))
       {
                frm=document.forms[0];
                frm.business.options.length=0;
                frm.business.options.length = _business_.length;
		// If we returned business, load them into the form.
                if (_business_.length != 0)
                {
                	for (num_business=0; num_business < _business_.length; num_business++)
                	{
                       		frm.business.options[num_business].text = _business_[num_business][1];
                      		frm.business.options[num_business].value = _business_[num_business][0]
                	}
                }
                // If there were no businesses loaded for whatever reason.
		else
		{
       		      	frm.business.options.length=1;
       		      	frm.business.options[0].text = 'No list of businesses available.';
       		      	frm.business.options[0].value = '';
	             	frm.business.options[0].selected = 'true';
		}
             	nuwin.close();
       }
        // If the window didn't open at all even after 5 attempts,
        //     or the get_business.cgi ran, but never loaded the businesses.
        else if ((attempts >= max_attempts) || ((cgi_started == 1) && (num_business == 0)))
        {
//alert('Try another JS method:' + '\n' + 'attempts = ' + attempts + '\n' + 'num_business = ' + num_business + '\n' + 'cgi_started = ' + cgi_started + '\n' + '\n' + 'nuwin.closed = ' + nuwin.closed + '\n' + 'show_window = ' + show_window);
                // If we had a window open, close it and try again.
		if (show_window == 'win_yes')
		{
             		nuwin.close();
			attempts = 0;
			load_business(parent.country, "win_no");
		}
                // If we had no window open, we're done trying.
		else
		{
			frm=document.forms[0];
       		      	frm.business.options.length=1;
       		      	frm.business.options[0].text = 'No list of businesses available.';
       		      	frm.business.options[0].value = 'NULL';
	             	frm.business.options[0].selected = 'true';
			attempts = 0;
		}
        }

	// We should never get this error, but just in case let's get some data.
        else
        {
		alert('Error - ' + '\n' + 'attempts = ' + attempts + '\n' + 'num_business = ' + num_business + '\n' + 'cgi_started = ' + cgi_started + '\n' + '\n' + 'nuwin.closed = ' + nuwin.closed + '\n' + 'show_window = ' + show_window);
        }
}
