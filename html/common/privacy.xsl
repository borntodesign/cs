<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>
   
   
   <xsl:template match="/">
    
<html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Privacy Policy</title>



<style type="text/css">
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 20px;
	color: #333333;
}
.Blue_Dark_Bold_Regular_Text_Caps {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	text-transform:uppercase;
	color: #174497;
	font-weight:bold;
}



.Header_Dark_Grey_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #666666;
	font-weight:bold;
}


</style>
</head>


<!--<table width="100%" border="0" cellpadding="10" cellspacing="0">

        <tr>
        <td bgcolor="#FFFFFF">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="22">
          <tr>
            <td>-->
            
          <span class="Blue_Dark_Bold_Regular_Text_Caps"><xsl:value-of select="//p0prv" /></span><br/><br/>  

<xsl:value-of select="//p1prv" /><br/><br/>


<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p2prv" /></span><br/>

<xsl:value-of select="//p3prv" /><br/><br/>


<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p4prv" /></span><br/>

<xsl:value-of select="//p5prv" /><br/><br/>

<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p6prv" /></span><br/>

<xsl:value-of select="//p7prv" /><br/><br/>

<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p8prv" /></span><br/>

<xsl:value-of select="//p9prv" /><br/><br/>


<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p10prv" /></span><br/>

<xsl:value-of select="//p11prv" /><br/><br/>

<span class="Header_Dark_Grey_Bold"><xsl:value-of select="//p12prv" /></span><br/>

<xsl:value-of select="//p13prv" /><br/><br/>
<xsl:value-of select="//p14prv" /><xsl:text> </xsl:text><a href="mailto:legal@clubshop.com?Subject=Privacy_Policy">legal@clubshop.com</a>
<!--</td>
</tr>
</table>

</td></tr></table>-->

</html>
</xsl:template>
</xsl:stylesheet>
