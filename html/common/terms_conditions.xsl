<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/>
   
   
   <xsl:template match="/">
    
<html><head>
         
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>ClubShop: Terms and Conditions</title>



<style type="text/css">
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 20px;
	color: #333333;
}

.Blue_Dark_Bold_Regular_Text_Caps {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	letter-spacing:0.1em;
	text-transform:uppercase;
	color: #174497;
	font-weight:bold;
}

.Main_Header_Dark_Grey_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #000000;
	font-weight:bold;
}	





.Header_Dark_Grey_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #666666;
	font-weight:bold;
}


</style>
</head>
<body>

<span class="Blue_Dark_Bold_Regular_Text_Caps"><xsl:value-of select="/lang_blocks/p0tc" /></span><br/><br/>

<xsl:value-of select="/lang_blocks/p1tc" /><br/><br/>

<xsl:value-of select="/lang_blocks/p2tc" /><xsl:text> </xsl:text> <span class="Header_Dark_Grey_Bold"><xsl:value-of select="/lang_blocks/p3tc" /></span><xsl:value-of select="/lang_blocks/p4tc" /> <br/><br/>


<xsl:value-of select="/lang_blocks/p5tc" /><xsl:text> </xsl:text> <span class="Header_Dark_Grey_Bold"><xsl:value-of select="/lang_blocks/p6tc" /></span><xsl:value-of select="/lang_blocks/p7tc" /> <br/><br/>

<xsl:value-of select="/lang_blocks/p8tc" /><br/>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
