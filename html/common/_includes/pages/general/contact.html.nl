﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop Rewards Contact</title>
<link href="../../css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/cs/_includes/js/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="/cs/_includes/js/jquery-plugins/jquery.iframe.js"></script>
</head>

<body>
<table bgcolor="#001952" border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td height="185"><form enctype="application/x-www-form-urlencoded" id="form1" method="post" name="form1">
<table bgcolor="#ffffff" border="0" cellpadding="8" cellspacing="0" width="100%">
<tbody>
<tr>
<td class="style5" width="100%">
<table border="0" cellpadding="1" cellspacing="0" width="100%">
<tbody>
<tr>
<td colspan="2" style="border-bottom: 2px dotted #CECECE;" width="100%"><span class="Blue_Bold">Contact informatie</span></td>
</tr>
<tr>
<td align="center" bgcolor="#e5e5e5" colspan="2" style="border-bottom: 2px dotted #CECECE;">
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#ffffff" valign="top" width="50%"><img height="282" src="../../../images/design/clubshop_shopper.png" width="245" /><br /><br /><span class="Movie_Header">
<script language="JavaScript1.2" type="text/javascript">// <![CDATA[
var message="HOW THE WORLD SHOPS AND SAVES!"
var colorone="#CCCCCC"
var colortwo="#FC8B08"
var flashspeed=40  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('<font color="'+colorone+'">')
for (m=0;m<message.length;m++)
document.write('<span id="changecolor'+m+'">'+message.charAt(m)+'</span>')
document.write('</font>')
}
else
document.write(message)
function crossref(number){
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number)
return crossobj
}
function color(){

//Change all letters to base color
if (n==0){
for (m=0;m<message.length;m++)
//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo
if (n<message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",6000)
return
}
}
function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()
// ]]></script>
</span></td>
                                <td align="left" bgcolor="#ffffff" valign="top" width="50%">Proprofit Worldwide Ltd.<br />
                                  98 Chingford Mount Road <br />
                                  E4 9AA - London, UK<br /><br />
<table border="0" cellpadding="0" cellspacing="0" width="280">
<tbody>
<tr>
<td align="left" valign="top" width="50%">Lidmaatschap Support:<br />Kantooruren:</td>
<td align="left" valign="top" width="50%"><a class="Nav_Countries" href="mailto:support1@dhs-club.com">support1@clubshop.com</a><br />Maandag - Vrijdag<br />9:00 am - 5:00 pm EST</td>
</tr>
<tr>
<td align="left" colspan="2" valign="top"> </td>
</tr>
<tr>
<td align="left" colspan="2" valign="top">
<table border="0" cellpadding="0" cellspacing="0" style="border-bottom: 2px dotted #CECECE; border-top: 2px dotted #CECECE;" width="100%">
<tbody>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" colspan="2" height="15" valign="top"></td>
</tr>
<tr>
<td align="left" valign="top" width="50%">TELEFOON:</td>
<td align="left" valign="top" width="50%">(001)941.662.8068</td>
</tr>
<tr>
<td align="left" colspan="2" height="15" valign="top"><a class="Nav_Countries" href="/mall/other/missing_order.shtml" target="_blank">Missing Order indienen</a> <br /><a class="Nav_Countries" href="/remove-me.shtml" target="_blank">Uitschrijven</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" colspan="2"></td>
</tr>
<tr>
<td align="left" colspan="2" height="30"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td></td>
</tr>
</tbody>
</table>
</form></td>
</tr>
</tbody>
</table>
</body>
</html>
