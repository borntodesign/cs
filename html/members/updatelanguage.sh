#!/bin/bash
#
if [ $1 == "en" ];then
   echo "No Language update"
   exit
fi
sed 's/Member Account/'"`/usr/bin/gtranslate en $1 Member Account`"'/g' index.shtml.shtml > index.shtml.$1
#
#
cd _includes/pages
sed 's/All rights reserved/'"`/usr/bin/gtranslate en $1 All rights reserved`"'/g' clubshop_copyright.shtml.shtml |\
sed 's/Home/'"`/usr/bin/gtranslate en $1 Home`"'/g' | \
sed 's/Contact Us/'"`/usr/bin/gtranslate en $1 Contact Us`"'/g' | \
sed 's/About Us/'"`/usr/bin/gtranslate en $1 About Us`"'/g' | \
sed 's/Privacy Policy/'"`/usr/bin/gtranslate en $1 Privacy Policy`"'/g' | \
sed 's/Terms and Conditions/'"`/usr/bin/gtranslate en $1 Terms and Conditions`"'/g' | \
sed 's/Contact Us/'"`/usr/bin/gtranslate en $1 Contact Us`"'/g' > clubshop_copyright.shtml.$1
#
#
if [ $1 == 'it' ]; then
   sed 's/FOLLOW US ON YOUR FAVORITE SOCIAL NETWORK/SEGUICI NEL NOSTRO SOCIAL NETWORK PREFERITO/g' clubshop_footers.shtml.shtml > clubshop_footers.shtml.$1
else
   sed 's/FOLLOW US ON YOUR FAVORITE SOCIAL NETWORK/'"`/usr/bin/gtranslate en $1 FOLLOW US ON YOUR FAVORITE SOCIAL NETWORK`"'/g' clubshop_footers.shtml.shtml > clubshop_footers.shtml.$1
fi
#
#
sed 's/Account Login/'"`/usr/bin/gtranslate en $1 Account Login`"'/g' member_info.shtml.shtml |\
sed 's/My Account/'"`/usr/bin/gtranslate en $1 My Account`"'/g' |\
sed 's/Logout/'"`/usr/bin/gtranslate en $1 Logout`"'/g' > member_info.shtml.$1
cd nav
#
#
#
sed 's/SHOP ONLINE/'"`/usr/bin/gtranslate en $1 SHOP ONLINE`"'/g' login_dropmenu.shtml.shtml |\
sed 's/Online Mall/'"`/usr/bin/gtranslate en $1 Online Mall`"'/g' |\
sed 's/SHOP LOCAL/'"`/usr/bin/gtranslate en $1 SHOP LOCAL`"'/g' |\
sed 's/Rewards Directory/'"`/usr/bin/gtranslate en $1 Rewards Directory`"'/g' |\
sed 's/Redeem/'"`/usr/bin/gtranslate en $1 Redeem`"'/g' |\
sed 's/Transactions/'"`/usr/bin/gtranslate en $1 Transactions`"'/g' |\
sed 's/Redemption List/'"`/usr/bin/gtranslate en $1 Redemption List`"'/g' |\
sed 's/Bonus/'"`/usr/bin/gtranslate en $1 Bonus`"'/g' |\
sed 's/REWARDS CARD/'"`/usr/bin/gtranslate en $1 REWARDS CARD`"'/g' |\
sed 's/Activate Card/'"`/usr/bin/gtranslate en $1 Activate Card`"'/g' |\
sed 's/Buy Card/'"`/usr/bin/gtranslate en $1 Buy Card`"'/g' |\
sed 's/MEMBERSHIP/'"`/usr/bin/gtranslate en $1 MEMBERSHIP`"'/g' |\
sed 's/Update Profile/'"`/usr/bin/gtranslate en $1 Update Profile`"'/g' |\
sed 's/Cancel Membership/'"`/usr/bin/gtranslate en $1 Cancel Membership`"'/g' |\
sed 's/Change Membership/'"`/usr/bin/gtranslate en $1 Change Membership`"'/g' |\
sed 's/Consultant Contact/'"`/usr/bin/gtranslate en $1 Consultant Contact`"'/g' |\
sed 's/Important Links/'"`/usr/bin/gtranslate en $1 Important Links`"'/g' |\
sed 's/Missing Mall Order/'"`/usr/bin/gtranslate en $1 Missing Mall Order`"'/g' |\
sed 's/Member FAQ/'"`/usr/bin/gtranslate en $1 Member FAQ`"'/g' > login_dropmenu.shtml.$1
#
#
sed 's/SHOP ONLINE/'"`/usr/bin/gtranslate en $1 SHOP ONLINE`"'/g' nologin_dropmenu.shtml.shtml |\
sed 's/Online Mall/'"`/usr/bin/gtranslate en $1 Online Mall`"'/g' |\
sed 's/SHOP LOCAL/'"`/usr/bin/gtranslate en $1 SHOP LOCAL`"'/g' |\
sed 's/Rewards Directory/'"`/usr/bin/gtranslate en $1 Rewards Directory`"'/g' |\
sed 's/WHAT WE OFFER/'"`/usr/bin/gtranslate en $1 WHAT WE OFFER`"'/g' |\
sed 's/Save Money/'"`/usr/bin/gtranslate en $1 Save Money`"'/g' |\
sed 's/Customey Loyality/'"`/usr/bin/gtranslate en $1 Customer Loyality`"'/g' |\
sed 's/Fundraising/'"`/usr/bin/gtranslate en $1 Fundraising`"'/g' |\
sed 's/JOIN FOR FREE/'"`/usr/bin/gtranslate en $1 JOIN FOR FREE`"'/g' |\
sed 's/ClubShop Member/'"`/usr/bin/gtranslate en $1 ClubShop Member`"'/g' |\
sed 's/Rewards Merchant/'"`/usr/bin/gtranslate en $1 Rewards Merchant`"'/g' |\
sed 's/Affinity Group/'"`/usr/bin/gtranslate en $1 Affinity Group`"'/g' |\
sed 's/REWARDS CARD/'"`/usr/bin/gtranslate en $1 REWARDS CARD`"'/g' |\
sed 's/Activate Card/'"`/usr/bin/gtranslate en $1 Activate Card`"'/g' |\
sed 's/Buy Card/'"`/usr/bin/gtranslate en $1 Buy Card`"'/g' |\
sed 's/CLUBSHOP INFO/'"`/usr/bin/gtranslate en $1 CLUBSHOP INFO`"'/g' |\
sed 's/Home Page/'"`/usr/bin/gtranslate en $1 Home Page`"'/g' |\
sed 's/TV Show Video/'"`/usr/bin/gtranslate en $1 TV Show Video`"'/g' |\
sed 's/Testimonials/'"`/usr/bin/gtranslate en $1 Testimonials`"'/g' > nologin_dropmenu.shtml.$1
#
#
cd ../../js
export hi=`/usr/bin/gtranslate en $1 Hello`
export bal=`/usr/bin/gtranslate en $1 ClubCash Balance`
if [ $1 == "fr" ]; then
   export bal="Solde du ClubCash"
else
   if [ $1 == "it" ]; then
      export bal="Saldo ClubCash"
   fi
fi
/bin/sed 's/Hello/'"$hi"'/g' loadminidee.js.js | \
/bin/sed 's/ClubCash Balance/'"$bal"'/g' > loadminidee.js.$1
#
# 
