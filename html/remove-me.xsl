<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
            <title>DHS Club Free Membership Change Application</title>

<style type="text/css">
p{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;
}

td{font-family: Verdana, Arial, sans-serif, Helvetica;
font-size: smaller;
background-color: #DFEFFF;
border-color: #000066;
}

h3 {font-family: Arial, sans-serif, Helvetica;
font-size: medium;
color: navy;
}
table.a{
border: 1px solid #000066;
}
</style>

</head>
<body>
<div><h3><xsl:value-of select="//lang_blocks/p0"/></h3></div>

<p><b><xsl:value-of select="//lang_blocks/p1"/></b></p>
<p><xsl:value-of select="//lang_blocks/p2"/><br/>
<xsl:value-of select="//lang_blocks/p3"/></p>

<form action="/cgi/mailstatus.cgi">
<table class="a">
<tr>
<td><xsl:value-of select="//lang_blocks/p4"/></td>
<td><input type="text" name="email" size="30" maxlength="50"/></td>
</tr>
<tr>
<td><xsl:value-of select="//lang_blocks/p5"/></td>
<td><input type="text" name="oid" size="30" maxlength="15"/></td>


</tr>
</table>
<br/>
<input type="submit" value="Submit"/>
</form>

<p><xsl:value-of select="//lang_blocks/p6"/><xsl:text> </xsl:text><a href="/idlookup.html"><xsl:value-of select="//lang_blocks/p7"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p8"/></p>

</body></html>


</xsl:template>
</xsl:stylesheet>