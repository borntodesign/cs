<?php
require 'Clubshop/DB_Connect.php';
require 'Clubshop/NegotiateByLanguage.php';

// setup language negotiation stuff
$negotiator = new \Clubshop\NegotiateByLanguage();
$xml = $negotiator->bestCandidateFilename('pay_agent_list.xml');
//error_log("file: $xml");
$tmp = array();
// we will use that filname later on, but based upon that, we will make an assumption on what the page language will be
preg_match('/\.([a-z]+)$/', $xml, $tmp);
// that should give us the extension
$pageLanguage = $tmp[1];
$pageLocale = $negotiator->bestSupportedLocale($pageLanguage);
putenv("LC_ALL=$pageLocale");
setlocale(LC_ALL, $pageLocale);

try {
    $lang_blocks = simplexml_load_file($xml);
} catch (Exception $e) {
    error_log("Failed to load XML language pack: $xml\n" . $e->getMessage());
    die("Failed to load XML language pack");
}

$db = new \Clubshop\DB_Connect('pay_agent_list');
$agents = $db->fetchList_assoc("
    SELECT m.alias
        , m.id
        , m.firstname
        , m.lastname
        , m.firstname||' '||m.lastname AS name
        , COALESCE(pa.emailaddress, m.emailaddress) AS emailaddress
        , fta.country_code
        , fta.fee_authorized
        , cbc.currency_code
        , cc.description AS currency_name
    FROM members m
    JOIN fee_transmission_agents_by_country fta
        ON fta.id=m.id
    JOIN pay_agent.agents_by_country pa
        ON pa.id=m.id
        AND pa.country_code=fta.country_code    -- that join ensures we are a pay agent AND an fta for the country
    JOIN currency_by_country cbc
        ON cbc.country_code=fta.country_code
    JOIN currency_codes cc
        ON cbc.currency_code=cc.code
    ");

$countries = $db->fetchAll_assoc("SELECT country_code, country_name FROM country_names_translated(?)", 'country_code', array($pageLanguage));
$uniqCountries = array();
foreach ($agents as $agent) {
    $uniqCountries[$countries[$agent['country_code']]['country_name']][] = $agent;
}

?><html><head>
            <title>Pay Agent List</title>


     <link rel="stylesheet" type="text/css" href="/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="/js/partner/foundation.js"></script>
    <script src="/js/partner/app.js"></script>
    <script src="/js/partner/flash.js"></script>

<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
table#localtbl th {
	font-size:12px;
}
table#localtbl .alc {
	text-align:center;
}
</style>
</head>
<body>
<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28"><?=$lang_blocks->p1?></span>

	<hr/>

	</div></div></div>
      
	<div class="row">
	<div class="twelve columns">

<p><?=$lang_blocks->p3?></p>
<div align="center">
<span class="style24"><?=$lang_blocks->p2?></span>
</div>
<br/><br/>
<table id="localtbl">
<thead>
<tr align="center">
    <th><?=$lang_blocks->p4?></th>
    <th><?=$lang_blocks->p5?></th>
    <th class="alc"><?=$lang_blocks->addlxfee?></th>
    <th><?=$lang_blocks->p6?></th>
    <th><?=$lang_blocks->p7?></th>
</tr>
</thead>
<tbody>
<?php
    $unq = array_keys($uniqCountries);
    sort($unq);
    foreach ($unq as $country_name):?>
<?php
    $rowspan = count($uniqCountries[$country_name]);
    $agent = array_shift($uniqCountries[$country_name]);
?>
<tr>
    <td rowspan="<?=$rowspan?>"><?=$country_name?></td>
    <td rowspan="<?=$rowspan?>"><?="{$agent['currency_name']} ({$agent['currency_code']})"?></td>
    <td class="alc"><?=$agent['fee_authorized']?></td>
    <td><b><?=$agent['name']?></b></td>
    <td><?=$agent['emailaddress']?></td>
</tr>
    <!-- the next block is only when there is more than one agent per country -->
    <?php foreach ($uniqCountries[$country_name] as $agent):?>
        <tr>
        <td class="alc"><?=$agent['fee_authorized']?></td>
        <td><b><?=$agent['name']?></b></td>
        <td><?=$agent['emailaddress']?></td>
        </tr>
    <?php endforeach;?>
<?php endforeach;?>
</tbody>
</table>
  
<p style="text-align: center">
    <a href="/pay_agent_list.xml" class="nav_footer_brown"><?=$lang_blocks->p8?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="https://www.clubshop.com/cgi/comm_prefs.cgi" target="_blank" class="nav_footer_brown"><?=$lang_blocks->p9?></a>&nbsp;&nbsp;&nbsp;&nbsp;
    <!-- td><a href="/vip_pay_approval.xsp" target="_blank" class="nav_footer_brown"><?=$lang_blocks->p10?></a></td-->
    <a href="/pay_agent_app.xml" target="_blank" class="nav_footer_brown"><?=$lang_blocks->p11?></a>
</p>

<!-- end page text here-->

</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2015
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>