<?php
require 'Clubshop/Session/Session.php';
$cs_session = new Clubshop\Session\Session();

// empty the session in preparation for a new login
$_SESSION = array();

// this line calls a perl script with the value of the full login cookie created by perl, parses the returned information and creates the 'meminfo' key in the $_SESSION
$cs_session->meminfoFromCookie();

$url = isset($_GET['destination']) ? $_GET['destination'] : '';
$url = preg_replace( "/[\\x00-\\x20]+/" , '' , $url );
if (! $url || preg_match('/^http(s)*:/', $url)) {
    $url = '/cs/';
}
header('Location: https://www.clubshop.com' . $url, true, 303);