<lang_blocks>
<welcome>Bienvenido a la página de información y preguntas frecuentes (FAQs) del ClubShop</welcome>
<nav>
<a6 href="helpdesk.xml">Página Inicial</a6>
<a8 href="mallfaq.xml">Preguntas frecuentes sobre las compras en el Centro Comercial ClubShop</a8>
<a7 href="shoppinginstructions.xml">Instrucciones de compra en el Centro Comercial ClubShop</a7>
<a9 href="cb_shopping.xml">Instrucciones de compras en comercios del ClubShop Rewards offline</a9>
<a10 href="tools.xml">Traductor de Idioma y convertidor de moneda</a10>
<a11 href="contact.xml">Información de contacto</a11>
<a12 href="/mall/missing_order.xml">Missing Order Información</a12>
</nav></lang_blocks>
