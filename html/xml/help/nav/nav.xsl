<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	media-type="text/html"/>

<xsl:template name="helpdesk_nav">
<!-- we have to define cellspacing here for IE which is too braindead to recognize the style -->
    <table id="helpdesk_nav" cellspacing="0">
    

<xsl:for-each select = "document('nav.xml')//nav/*">
              <tr> 
                <td>
                <a><xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
                <xsl:value-of select="." /></a></td>
              </tr>

</xsl:for-each>
	</table>

</xsl:template>

<xsl:template name="helpdesk_welcome">	
<div id="helpdesk_welcome"><xsl:value-of select="document('nav.xml')//welcome" />
</div>
</xsl:template>
</xsl:stylesheet>