<lang_blocks><welcome>Dobrodošli na strani ClubShop pomoči!</welcome>
<nav><a6 href="helpdesk.xml">Pomoč domov</a6>
<a7 href="shoppinginstructions.xml">Instrukcije za nakupovanje v ClubShop nakupovalnem središču</a7>
<a8 href="mallfaq.xml">Vprašanja in odgovori o nakupovanju v ClubShop nakupovalnem središču</a8>
<a9 href="cb_shopping.xml">Instrukcije za nakupovanje pri nespletnih ClubShop Rewards trgovcih</a9>
<a10 href="tools.xml">Prevajalniki jezikov in pretvornik valut</a10>
<a11 href="contact.xml">Kontaktna informacija</a11>
<a12 href="/mall/missing_order.xml">Missing Order Information</a12>
</nav></lang_blocks>
