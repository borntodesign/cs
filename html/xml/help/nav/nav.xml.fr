<lang_blocks><welcome>Bienvenue au ClubShop Help Desk!</welcome>
<nav>
<a6 href="helpdesk.xml">Page d'accueil Help Desk</a6>
<a7 href="shoppinginstructions.xml">Instructions sur la façon de faire vos achats au ClubShop Mall</a7>
<a8 href="mallfaq.xml">Questions les plus fréquentes concernant le ClubShop Mall</a8>
<a9 href="cb_shopping.xml">Instructions sur la façon de faire vos achats hors ligne auprès d'un marchand ClubShop Rewards</a9>
<a10 href="tools.xml">Traducteur et Convertisseur de Devises</a10>
<a11 href="contact.xml">Information de Contact</a11>
<a12 href="/mall/missing_order.xml">Information sur une Commande Manquante</a12>
</nav></lang_blocks>
