<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- include_head_foot.xsl simply sucks in the global xml for feeding the global templates -->
	<xsl:template match="/">
	<base>
	<xsl:copy-of select = "*"/>
	<xsl:copy-of select = "document('/xml/head-foot.xml')/*"/>
	</base>
	</xsl:template>
</xsl:stylesheet>
