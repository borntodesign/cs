<?xml version="1.0"?> <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="meta_content_language">
	<xsl:if test="//meta-content-language != ''">
		<meta http-equiv="Content-Language">
			<xsl:attribute name="content"><xsl:value-of select="//meta-content-language" /></xsl:attribute>
		</meta>
	</xsl:if>
</xsl:template>

<xsl:template name="head">
 	<table class="div_head"><tr> 	<td class="img_container">
 	<xsl:call-template name="head_img" /></td> 	<td class="link_container">
 	 	<xsl:call-template name="qlinks" /> 	</td></tr>
 	 	 	<!--tr><td colspan="2" class="subhead">
 	 	<xsl:call-template name="sub_head" /></td></tr-->
  	</table>
</xsl:template>
 	 	
<xsl:template name="head_img">
<!-- in case we want to use just the global image, we'll place it in its own 'template' -->
<img src="/images/head_portal5.gif" height="69" width="442">
 	<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/common/t" /></xsl:attribute>
 	 	</img>
</xsl:template>

<xsl:template name="sub_head">
<!-- from my perspective this is just a decorative chunk going under the real header -->
<!--img src="/images/ani_storeline.gif" height="56" width="753" alt="" /-->
</xsl:template>

<xsl:template name="foot">
       <table class="common_nav_footer"><tr>           <td><a href="/"><xsl:value-of select="//lang_blocks/common/fa0" /></a></td>
           <td><a href="/about.xml" onclick="window.open(this.href);return false;"><xsl:value-of select="//lang_blocks/common/fa1" /></a></td>
           <!--td><a href="/affiliates.xml"><xsl:value-of select="//lang_blocks/common/fa2" /></a></td-->
           <td><a href="/mall_listing.xml" onclick="window.open(this.href);return false;"><xsl:value-of select="//lang_blocks/common/fa3" /></a></td>
           <td><a href="/nonprofit.xml" onclick="window.open(this.href);return false;"><xsl:value-of select="//lang_blocks/common/fa4" /></a></td>
           <td><a href="/termsofuse.xml" onclick="window.open(this.href);return false;"><xsl:value-of select="//lang_blocks/common/fa5" /></a></td>
           <td><a href="/privacy.xml" onclick="window.open(this.href);return false;"><xsl:value-of select="//lang_blocks/common/fa6" /></a></td>
            <!--td><a href="/legal.xml"><xsl:value-of select="//lang_blocks/common/fa8" /></a></td-->
         </tr></table>
</xsl:template>

<xsl:template name="mall_list">
<xsl:param name="onchange" />
<div id="mall_list_wrap">
<form class="mall_list" action="">
<select id="mall_drop_down" name="arg"><xsl:attribute name="onchange">
<xsl:choose><xsl:when test="$onchange = 'new'">window.open(this.options[this.selectedIndex].value);this.selectedIndex=0;</xsl:when>
<xsl:otherwise>window.location=this.options[this.selectedIndex].value;this.selectedIndex=0;</xsl:otherwise>
</xsl:choose></xsl:attribute>

<option value=""><xsl:value-of select="document('head-foot.xml')//lang_blocks/common/mall_prompt" /></option>
<xsl:for-each select="document('countries.xml')//*">
<xsl:sort/>
<xsl:variable name="name" select="." />
<xsl:variable name="cc" select="@val" />
<xsl:for-each select="document('malls.xml')//*[. = $cc]">
	<option><xsl:attribute name="value"><xsl:value-of select="@value" />
	</xsl:attribute><xsl:value-of select="$name"/></option>
</xsl:for-each>
</xsl:for-each>

<xsl:variable name="ww" select="document('head-foot.xml')//lang_blocks/common/mall_worldwide" />
<xsl:for-each select="document('malls.xml')//*[. = 'Worldwide']">
<option><xsl:attribute name="value"><xsl:value-of select="@value" />
	</xsl:attribute><xsl:value-of select="$ww" /></option>
</xsl:for-each>
</select>
</form></div>
</xsl:template>

<xsl:template name="qlinks"> 		<table class="qlinks"> 		<xsl:if test="//user/firstname"> 			<tr><td class="hello" colspan="2"> 			<xsl:variable name="apos">'</xsl:variable> 			<xsl:value-of select="//lang_blocks/common/hp1" /><xsl:text> </xsl:text><xsl:value-of select="translate(//user/firstname, $apos, '`')" />!<br /> 	<xsl:value-of select="concat(//lang_blocks/common/hp2, ': ', //user/reward_points)" /> 			</td></tr> 		</xsl:if> 		<tr><td class="links"><a href="/members/" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/common/redeem_rp" /></a> 		</td><td class="links"> 		<xsl:choose><xsl:when test="//user/firstname"> 			<a href="/cgi-bin/wwg2"><xsl:value-of select="//lang_blocks/common/ha1" /></a> 		</xsl:when><xsl:otherwise> 			<a href="/cgi/appx.cgi" onclick="window.open(this.href); return false;"><xsl:value-of select="//lang_blocks/common/signup" /></a> 		</xsl:otherwise></xsl:choose> 		</td></tr> 		<tr><td class="links"><a href="/helpdesk.xml"><xsl:value-of select="//lang_blocks/common/faq" /></a> 		</td><td class="links"> 		<xsl:choose><xsl:when test="//user/firstname"> 			<a href="/cgi/miniLogin.cgi?logout=1" onclick="return confirmCallMiniLogin(this.href)"><xsl:value-of select="//lang_blocks/common/logout" /></a> 		</xsl:when><xsl:otherwise> 			<a href="/cgi/miniLogin.cgi" onclick="return CallMiniLogin(this.href)"><xsl:value-of select="//lang_blocks/common/login" /></a> 		</xsl:otherwise></xsl:choose> 		</td></tr> <xsl:if test="not(//user/firstname)"><tr><td colspan="2" class="links"> <a href="/memberinfo.xml"><xsl:value-of select="//lang_blocks/common/ha3" /></a> </td></tr></xsl:if> 		</table>
</xsl:template>
</xsl:stylesheet>
