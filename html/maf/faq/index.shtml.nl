﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Welcome to CLUBSHOP REWARDS: Merchant Interface</title>



<meta name="title" content="clubshop rewards incentive and loyalty program"/>

<meta name="keywords" content="clubshop rewards incentive and loyalty program, reward popints"/>

<meta name="abstract" content="clubshop rewards incentive and loyalty program "/>

<meta http-equiv="content-language" content="en-us"/>

<meta http-equiv="content-language" content="english"/>

<meta http-equiv="content-language" content="lang_en"/>

<meta name="coverage" content="worldwide"/>

<meta name="distribution" content="global"/>

<meta name="author" content="clubshop rewards incentive and loyalty program"/>

<meta name="design-development" content="carsten rieger"/>



<meta name="publisher" content="clubshop rewards incentive and loyalty program"/>

<meta name="company" content="clubshop"/>

<meta name="copyright" content="copyright © 2009 clubshop rewards incentive and loyalty program. all rights reserved"/>

<meta name="page-topic" content=" clubshop rewards incentive and loyalty program "/> 

<meta name="robots" content="index,follow"/>

<meta name="rating" content="all"/>

<meta name="audience" content="general"/>

<meta name="revisit-after" content="7 days"/>





<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

<link href="/css/club-rewards-transparent.css" rel="stylesheet" type="text/css" />

<script src="../js/window.js"></script>





<script type="text/javascript">

<!--

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

//-->

</script>

</head>



<body onload="MM_preloadImages('/images/nav/maf/nav_faq_1.gif','/images/nav/mi/nav_transaction_2.gif','/images/nav/mi/nav_profile_2.gif','/images/nav/mi/nav_payment_2.gif','/images/nav/mi/nav_setup_2.gif','/images/nav/mi/nav_partner_2.gif')">



<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td align="center" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">

      <tr>

        <td valign="top"><table id="Table_01" width="1000" height="714" border="0" cellpadding="0" cellspacing="0">

          <tr>

            <td height="1" colspan="12" valign="top"><table id="Table_" width="1000" height="138" border="0" cellpadding="0" cellspacing="0">

              <tr>

                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_left.png" width="34" height="138" alt="" /></td>



                      <td colspan="3"><a href="/maf/home"><img src="/images/general/logo_clubshop_merchant_interface.gif" alt="" width="428" height="60" border="0" /></a></td>

                <td height="60" colspan="4" align="left" valign="top" background="/images/general/clubshop_merchant_interface_03.gif"><iframe id="ifrm" name="ifrm" src="/maf/_includes/headnav_main.html" scrolling="Auto" width="100%" height="60" frameborder="0" allowtransparency="true"></iframe></td>

                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_right.png" width="34" height="138" alt="" /></td>

              </tr>

		<!--#include virtual="../_includes/faq_navigation.html" -->

              <tr>

                <td colspan="7"><img src="/images/general/headbar_pages.jpg" width="932" height="34" alt="" /></td>

              </tr>

            </table></td>



          </tr>

          

          <tr>

            <td height="390" colspan="2" background="/images/bg/bg_left.png">&nbsp;</td>

            <td height="390" colspan="8" align="left" valign="top" bgcolor="#FFFFFF"><p><span class="style12">Veel gestelde vragen</span><span class="Text_Body_Content"></span><br />

            </p>

                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">

                  <tbody>

                    <tr>

                      <td colspan="3" align="center" valign="top" background="/images/bg/bg_header-orange-923x23.png" bgcolor="#FCA641" class="Text_Body_Content"><span class="style13">Vragen &amp; Antwoorden</span><br />

                      </td>

                    </tr>

                    <tr>

                      <td colspan="3" align="left" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content"><table width="100%" border="0" cellspacing="0" cellpadding="2">

                            <tr> 

                              <td align="left" valign="top" class="text_orange">DE 

                                KAART ACCEPTEREN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">1. <a href="#1" class="nav_blue_line">Wat 

                                moet ik doen als mijn barcode scanner of GSM niet 

                                werkt of de transactiegegevens niet doorstuurt?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">2. <a href="#2" class="nav_blue_line">Hoe 

                                lever ik een bewijs van aankoop met de Rewards 

                                kaart af aan een ClubShop lid over een aankoop?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">3. <a href="#3" class="nav_blue_line">Waarom 

                                moet de kassier of kassierster het kassaticket 

                                afstempelen of paraferen als bewijs van aankoop?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">TRANSACTIES 

                                INBOEKEN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">4. <a href="#4" class="nav_blue_line">Hoe 

                                boek ik een transactie manueel in?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">5. <a href="#5" class="nav_blue_line">Naar 

                                welk telefoonnummer moet ik het tekstbericht met 

                                mijn ClubShop Rewards transacties versturen?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">6. <a href="#6" class="nav_blue_line">Hoeveel 

                                ClubShop Rewards transacties kan ik in één tekstbericht 

                                versturen?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">RETOURS 

                                AFHANDELEN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">7. <a href="#7" class="nav_blue_line">Wat 

                                moet ik doen als iemand een artikel terug brengt 

                                nadat men de ClubShop Rewards punten over dit 

                                artikel al heeft ontvangen?</a> </td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">MIJN 

                                REFERRAL FEES BETALEN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">8. <a href="#8" class="nav_blue_line">Waar 

                                kan ik zien hoeveel ik zal aangerekend worden?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">9. <a href="#9" class="nav_blue_line">Wat 

                                gebeurt er als ik mijn Referral Fees niet betaal?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">CLUBSHOP.COM 

                                LISTING</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">10. <a href="#10" class="nav_blue_line">Waar 

                                kan ik mijn ClubShop Rewards listing bekijken?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">11. <a href="#11" class="nav_blue_line">Hoe 

                                worden de handelaren gerangschikt in de ClubShop 

                                Rewards Directory wanneer een zoekopdracht werd 

                                ingediend?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">12. <a href="#12" class="nav_blue_line">Kan 

                                ik mijn handelaren pakket upgraden en mijn listing 

                                verbeteren?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">13. <a href="#13" class="nav_blue_line">Wanneer 

                                kan een handelaar het pakket downgraden?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">FILIALEN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">14. <a href="#14" class="nav_blue_line">Ik 

                                heb verscheidene filialen onder dezelfde bedrijfsnaam. 

                                Kan ik meerdere bedrijfslocaties installeren onder 

                                dezelfde handelaren account?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">15. <a href="#15" class="nav_blue_line">Hoe 

                                kan ik een filiaal toevoegen?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">KAARTEN 

                                VERDELEN</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">16. <a href="#16" class="nav_blue_line">Van 

                                welke voordelen geniet een handelaar als hij zelf 

                                ClubShop Rewards kaarten uitdeelt?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">17. <a href="#17" class="nav_blue_line">Waar 

                                kan ik Rewards kaarten en ander ClubShop Rewards 

                                promotiemateriaal bestellen?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">18. <a href="#18" class="nav_blue_line">Hoe 

                                en waar kan ik mijn ClubShop Rewards kaart activeren?</a></td>

                            </tr>
							       <tr> 

                              <td align="left" valign="top">19. <a href="#19" class="nav_blue_line">Waar kan ik de leden zien die ik verwees door ClubShop Rewards kaarten uit te delen? </a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top" class="text_orange">BOEKHOUDING</td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">20.<a href="#20" class="nav_blue_line"> Hoe open ik een 

                                EcoCard account?</a></td>

                            </tr>

                            <tr> 

                              <td align="left" valign="top">21. <a href="#21" class="nav_blue_line">Hoe ga ik naar 

                                mijn ClubAccount?</a></td>

                            </tr>
							
							    <tr> 

                              <td align="left" valign="top">22. <a href="#22" class="nav_blue_line">Hoe maak ik een bankoverschrijving naar de ClubShop Bank of Ireland rekening want ik wil gebruik maken van de "Internationale bankoverschrijving" betalingsmethode?</a></td>

                            </tr>
							
							<tr> 
                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top" class="text_orange">MERCHANT MEMBERSHIP</td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">23. <a href="#23" class="nav_blue_line">Where do I go 
                                to print a ClubShop Rewards Card?</a></td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">24. <a href="#24" class="nav_blue_line">Where do I go 
                                to see and redeem my ClubCash balance?</a> </td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">25. <a href="#25" class="nav_blue_line">Where do I go 
                                to shop online at the ClubShop Mall?</a></td>
                            </tr>
							
							

                          </table></td>

                    </tr>

                    <tr>

                      <td height="8" colspan="3" align="left" valign="top" background="/images/bg/bg_header-orange-923x23.png" bgcolor="#FCA641" class="Text_Body_Content"></td>

                    </tr>

                  </tbody>

                </table>

                <br />

                <span class="style24"><a name="1" id="1"></a>1. Wat moet ik doen als mijn barcode scanner of GSM niet werkt of de transactiegegevens niet doorstuurt?<br />

                </span>De transactie informatie invullen op een handelaren transactie inboekformulier, dan invoeren op de transactiepagina op uw Handelaren Interface. Voor alle hulp en assistentie kunt u ook contact opnemen met uw consulent.<br />

                <br />

                <span class="style24"><a name="2" id="12"></a>2. Hoe lever ik een bewijs van aankoop met de Rewards kaart af aan een ClubShop lid over een aankoop?<br />

                  </span>De kassier(ster) dient het kassaticket op de achterkant 

                  af te stempelen of te voorzien van een paraaf. <a href="javascript:popupcenter('../window/purchase_stamp_window.html.nl', 666, 450)" class="nav_blue_line"> KLIK HIER</a> om 

                  een ClubShop Rewards stempel te bestellen.<br />

                <br />

                <span class="style24"><a name="3" id="13"></a>3.  Waarom moet de kassier of kassierster het kassaticket afstempelen of paraferen als bewijs van aankoop?<br />

                </span>Voor de ClubShop Rewards kaarthouder geldt dit als bewijs dat men een aankoop deed met hun <span class="style5">ClubShop Rewards Kaart</span>. Mocht om één of andere reden de transactie niet worden ingeboekt op de Handelaren Interface, dan heeft de kaarthouder die de aankoop deed een bewijs van aankoop die hij kan voorleggen aan de handelaar, om alsnog de verdiende punten te kunnen ontvangen.<br />

                <br />

En ook, mocht een Rewards kaarthouder een artikel terug brengen en een terugbetaling wensen, dan is dit een manier of een bewijs om de gegeven punten op dit artikel te verwijderen, zodat u geen Referral Fee hoeft te betalen op dit artikel.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="4" id="132"></a>4. Hoe boek ik een transactie manueel in?<br />

</span>

<ol>

  <li>Vul de transactie in op een handelaren transactie inboekformulier</li>

  <li> Op uw "Handelaren Interface" dient u bovenaan "Transacties" aan te klikken en op de transactiepagina selecteert u "Transactie Online Inboeken"</li>

  <li> Op de "Transactie Online Inboeken" pagina, klikt u het lege  "<span class="style5">ClubShop Rewards</span> ID Nummer" veld aan en scant u de "<span class="style5">ClubShop Rewards Kaart</span>" bar code. ” barcode. Door de  "<span class="style5">ClubShop Rewards  Card</span>" barcode te scannen, verschijnt het <span class="style5">ClubShop Rewards </span> ID nummer automatisch in het veld </li>

  <li> Klik in het veld “Bedrag aankoop” en vul het totale aankoopbedrag in (zonder BTW)</li>

  <li> Klik in het veld “Datum transactie” en vul de datum in</li>

  <li> Klik op de button “Toevoegen” om uw transactie in de “Cumulatieve Transactie Lijst” te zetten. Indien nodig, deze stappen herhalen om meerdere transacties in de “Cumulatieve Transactie Lijst” toe te voegen.</li>

  <li> Om alle transacties in de “Cumulatieve Transactie Lijst” in te dienen, eerst nog uw munteenheid selecteren</li>

  <li> Kijk even na of alle transacties correct zijn ingevuld en mocht u fouten vinden, dan kunnen deze in de “Cumulatieve Transactie Lijst” verbeteren</li>

  <li> Klik éénmaal op de button “Submit lijst” en wacht tot uw Transactie rapport te voorschijn komt</li>

  <li> U kunt uw transacties inkijken op uw “Transactie rapport”</li>

</ol>

<span class="style24"><a name="5" id="133"></a>5. Naar welk telefoonnummer moet ik het tekstbericht met mijn ClubShop Rewards transacties versturen?<br />

</span> Bent u niet in de VS, verstuur het bericht naar 001-941-662-8608. Vanuit de VS verstuurt u het bericht naar 1-941-662-8608. Het bericht met <span class="style5">ClubShop Rewards</span> transactie informatie zal automatisch verwerkt worden en toegevoegd worden aan uw Handelaren Interface. Gelieve nota te nemen dat het verzenden van tekstberichten enige extra kosten met zich kan meebrengen, zeker als u het bericht verstuurt van buiten de Verenigde Staten.<br />

<br />

<span class="style24"><a name="6" id="134"></a>6. Hoeveel ClubShop Rewards transacties kan ik in één tekstbericht versturen?<br />

</span>Dit hangt af van uw mobiele telefoon. Sommige GSM toestellen kunnen 160 karaktertekens versturen in één bericht, terwijl andere meer dan 500 karaktertekens kunnen versturen in één bericht. Kijk uw toestel even na om te zien hoeveel transacties u kunt versturen in één tekstbericht.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="7" id="1322"></a>7. Wat moet ik doen als iemand een artikel terug brengt nadat men de ClubShop Rewards punten over dit artikel al heeft ontvangen?<br />

</span>Op uw Handelaren Interface dient u eerst te klikken op “Transacties” en op de transactiepagina selecteert u “Transactie rapport”. Op de “Transactie rapport” pagina selecteert u een periode waarin de retour gebeurde. Klik dan op Transactie ID, vul het terugbetaalde bedrag in en submit. Om het retourbedrag te zien gaat u terug naar de Transactie rapport pagina en klik opnieuw op de datum waarin de retour gebeurde. Nu ziet u de terugbetaalde retour.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />



<span class="style24"><a name="8" id="1332"></a>8. Waar kan ik zien hoeveel ik zal aangerekend worden?<br />

</span>Om het bedrag te zien die zal aangerekend worden of bedragen die voorheen aangerekend werden, ga naar uw “Transactie rapport” pagina, selecteer de maand die u wenst te bekijken en laat het totaalbedrag van transacties zien en het te betalen totaalbedrag. Op de 5e van iedere maand, wordt het openstaande bedrag aangerekend van de transacties in de maand(en) ervoor. Print iedere maand dit rapport af als factuur om toe te voegen aan uw boekhouding.<br />

<br />

<span class="style24"><a name="9" id="1342"></a>9. Wat gebeurt er als ik mijn Referral Fees niet betaal?<br />

</span>U wordt gecontacteerd door  <span class="style5">ClubShop Rewards </span> windien wij niet in staat zijn het openstaande te innen. U dient ons dan te voorzien van een (andere) kredietkaart of uw ClubAccount te spijzen met genoeg geld om het openstaande bedrag te vereffenen. Indien wij uw Referral Fee niet kunnen innen, wordt uw account gedeactiveerd en worden de punten die de kaarthouders dachten te krijgen herroepen. Wij zullen dan ook alle kaarthouders in uw regio op de hoogte brengen dat uw account werd gedeactiveerd wegens openstaande schuld. Door de slechte publiciteit die dit voor uw bedrijf met zich kan meebrengen, zou het eervol betalen van openstaande Referral Fees, beter zijn dan eventueel een verzoek tot uitschrijving.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="10" id="1323"></a>10. Waar kan ik mijn ClubShop Rewards listing bekijken?<br />

</span> U gaat naar <a href="/rewardsdirectory" target="_blank" class="nav_blue_line">http://www.clubshop.com/rewardsdirectory</a> om u in de lijst terug te vinden.<br />

<br />

<span class="style24"><a name="11" id="1333"></a>11. Hoe wordt de ranking bepaald in de ClubShop Rewards Directory wanneer een zoekopdracht naar handelaren wordt ingediend?<br />

</span>De ranking wordt gebaseerd op het handelaren pakket dat werd geselecteerd, met het Goud pakket op het hoogste level en het Gratis Basis pakket op het laagste level. Ondernemers met hetzelfde pakket worden dan geplaatst volgens het Referral Fee percentage die werd geselecteerd, met het hoogste percentage op het hoogste level.<br />

<br />

<span class="style24"><a name="12" id="1343"></a>12. Kan ik mijn handelaren pakket upgraden en mijn listing verbeteren?<br />

</span>Ja, maar u zult het verschil moeten bijbetalen om uw handelaren pakket te upgraden en de datum van vernieuwing blijft de datum waarop u de eerste keer betaalde voor uw vorige pakket.<br />

<br />

<span class="style24"><a name="13" id="13432"></a>13. Wanneer kan een handelaar het pakket downgraden?<br />

</span> Tot 30 dagen na dag van vernieuwing.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="14" id="13222"></a>14. Ik heb verscheidene filialen onder dezelfde bedrijfsnaam. Kan ik meerdere bedrijfslocaties installeren onder dezelfde handelaren account?<br />

</span>Uw handelaren pakket bepaalt hoeveel filialen u kunt aanmelden<br />

<ul>

  <li><span class="text_orange">Gratis </span> handelaren pakket  – 1 hoofdzetel en 0 filialen</li>

  <li><span class="text_orange"> Brons </span> handelaren pakket  – 1 hoofdzetel en 2 filialen</li>

  <li> <span class="text_orange">Zilver</span> handelaren pakket  – 1 hoofdzetel en 4 filialen</li>

  <li> <span class="text_orange">Goud</span> handelaren pakket  – 1 hoofdzetel en 8 filialen</li>

</ul>

Een handelaar die 10 of meer filialen heeft, kan aanzienlijk veel dienstverlening nodig hebben en dient daarom rechtsreeks bij DHSC <span class="style5">ClubShop Rewards</span> ingeschreven te worden. <span class="style24"><br />

<br />

<a name="15" id="13433"></a>15. Hoe kan ik een filiaal toevoegen?<br />

</span>Als een handelaar een filiaal wenst toe te voegen, moet men op de “Update profiel” pagina de toets “Filiaal toevoegen” aanklikken. Ieder filiaal wordt toegevoegd met hun eigen e-mail adres en paswoord om in te loggen op hun aparte handelaren interface. Enkel de hoofdzetel zal toegang hebben tot de Hoofd Account informatie.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />
                  <span class="style24"><a name="16" id="132222"></a>16. Van welke 
                  voordelen geniet een handelaar als hij zelf ClubShop Rewards 
                  kaarten uitdeelt?<br />

                  </span>Een handelaar kan genieten van een aantal voordelen door <span class="style5">ClubShop Rewards </span> kaarten uit te delen, maar het grootste voordeel is het verdienen van Referral Commissie op iedere aankoop gemaakt door een kaarthouder bij deelnemende handelaren, offline of online in de ClubShop Mall. Inclusief het verdienen van Referral Commissies op elke aankoop gemaakt in uw winkel(s), wat uw net percentage vermindert met 20%. Voor meer informatie over hoe een Handelaren Partner worden en kaarten verdelen, klik op de Partner toets bovenaan deze pagina. Om te zien hoeveel Referral fees ik verdiend heb door ClubShop Rewards

kaarten te verdelen die zijn toegewezen aan mijn handelaren lidmaatschap:

<a href="https://www.clubshop.com/cgi/merchant_comm_rpt.cgi"

class="nav_blue_line"

target="_blank">https://www.clubshop.com/cgi/merchant_comm_rpt.cgi</a>

<br />



<br />

<span class="style24"><a name="17" id="134332"></a>17. Waar kan ik Rewards kaarten en ander ClubShop Rewards promotiemateriaal bestellen?<br />

</span>Ga naar uw  <span class="style5">ClubShop</span> Business Center op <a href="/outletsp/bcenter.html" class="nav_blue_line">https://www.clubshop.com/outletsp/bcenter.html</a> om de beschikbare <span class="style5">ClubShop Rewards</span> promotiematerialen te bekijken en te bestellen.<br />

<br />

<span class="style24"><a name="18" id="1343322"></a>18. Hoe en waar activeer ik mijn ClubShop Rewards kaart?<br />

</span><a href="/mycard" target="_blank" class="nav_blue_line">http://www.clubshop.com/mycard</a><br />
<br />

<span class="style24"><a name="19" id="1343322"></a>19. Waar kan ik de leden zien die ik verwees door ClubShop
Rewards kaarten uit te delen? <br />

</span><a href="https://www.clubshop.com/cgi/merch-frontline-rpt.cgi" target="_blank" class="nav_blue_line">https://www.clubshop.com/cgi/merch-frontline-rpt.cgi </a><br />


<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="19" id="132222"></a>20. Hoe open ik een EcoCard account?<br />

</span><a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" target="_blank" class="nav_blue_line">https://www.ecocard.com/Registration.aspx?Referee=DHS</a>

<br/><br/>

<span class="style24"><a name="20" id="132222"></a>21. Hoe ga ik naar mijn ClubAccount?<br />

</span><a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank" class="nav_blue_line">https://www.clubshop.com/cgi/funding.cgi?action=report</a>
<br/><br/>
<span class="style24"><a name="20" id="1343322"></a>22. Hoe maak ik een bankoverschrijving naar de ClubShop Bank of Ireland rekening want ik wil gebruik maken van de "Internationale bankoverschrijving" betalingsmethode? <br />
                 </span>De "Internationale bankoverschrijving" betalingsmethode wordt momenteel enkel ter beschikking gesteld voor de lidstaten van de Europese Unie.
Na ontvangst van fondsen op onze Bank of Ireland rekening en daarvan in kennis gesteld zijn (7 tot 10 werkdagen), zullen wij vervolgens de fondsen overmaken naar uw ClubAccount. Hier vindt u de gedetailleerde bankgegevens die u nodig hebt als u deze betalingsmethode wenst te gebruiken:<br/><br/>
  <b>BANK NAAM:</b> Bank of Ireland<br/>
  <b>BANK ADRES:</b> Northern Cross, 2-3 Burnell Green, Malahide
Road, Dublin 17<br/>
  <b>SORTING CODE:</b> 904587<br/>
  <b>REKENINGNUMMER:</b> 95145818<br/>
  <b>IBAN:</b> IE06 BOFI 9045 8795 1458 18<br/>
  <b>BIC CODE:</b> BOFIIE2D <br/> 

<br/>
   
                  <hr align="left" width="100%" size="2" color="#FCBF12" />
                  <span class="style24"><a name="23" id="1343323"></a>23. Where do I go to print a ClubShop Rewards Card?</span><br>
				   <a href="http://www.clubshop.com/getcards.html" target="_blank" class="nav_blue_line">http://www.clubshop.com/getcards.html</a> 
<br /><br />

					<span class="style24"><a name="24" id="1343323"></a>24. Where do I go to see and redeem my ClubCash balance?</span><br>
				   <a href="http://www.clubshop.com/cgi/reward_center.cgi" target="_blank" class="nav_blue_line">http://www.clubshop.com/cgi/reward_center.cgi</a> 
<br /><br />

					<span class="style24"><a name="25" id="1343323"></a>25. Where do I go to shop online at the ClubShop Mall?</span><br>
				   <a href="http://www.clubshop.com/mall" target="_blank" class="nav_blue_line">http://www.clubshop.com/mall</a> 
<br /><br />




</td>

            <td height="390" colspan="2" background="/images/bg/bg_right.png">&nbsp;</td>

          </tr>

          <tr>

            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="52" alt="" /></td>

          </tr>

          <tr>

            <td height="134" colspan="12" background="/images/general/footerbar.png"><iframe id="ifrm2" name="ifrm" src="/maf/_includes/footer.html" scrolling="Auto" width="100%" height="134" frameborder="0" allowtransparency="true"></iframe></td>

          </tr>



          <tr>

            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>



            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>

          </tr>

        </table></td>

      </tr>

      

    </table></td>

  </tr>



  <tr>

    <td align="center" valign="top">&nbsp;</td>

  </tr>

</table>



</body>

</html>

