﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Welcome to CLUBSHOP REWARDS: Merchant Interface</title>



<meta name="title" content="clubshop rewards incentive and loyalty program"/>

<meta name="keywords" content="clubshop rewards incentive and loyalty program, reward popints"/>

<meta name="abstract" content="clubshop rewards incentive and loyalty program "/>

<meta http-equiv="content-language" content="en-us"/>

<meta http-equiv="content-language" content="english"/>

<meta http-equiv="content-language" content="lang_en"/>

<meta name="coverage" content="worldwide"/>

<meta name="distribution" content="global"/>

<meta name="author" content="clubshop rewards incentive and loyalty program"/>

<meta name="design-development" content="carsten rieger"/>



<meta name="publisher" content="clubshop rewards incentive and loyalty program"/>

<meta name="company" content="clubshop"/>

<meta name="copyright" content="copyright © 2009 clubshop rewards incentive and loyalty program. all rights reserved"/>

<meta name="page-topic" content=" clubshop rewards incentive and loyalty program "/> 

<meta name="robots" content="index,follow"/>

<meta name="rating" content="all"/>

<meta name="audience" content="general"/>

<meta name="revisit-after" content="7 days"/>





<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

<link href="/css/club-rewards-transparent.css" rel="stylesheet" type="text/css" />

<script src="../js/window.js"></script>





<script type="text/javascript">

<!--

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

//-->

</script>

</head>



<body onload="MM_preloadImages('/images/nav/maf/nav_faq_1.gif','/images/nav/mi/nav_transaction_2.gif','/images/nav/mi/nav_profile_2.gif','/images/nav/mi/nav_payment_2.gif','/images/nav/mi/nav_setup_2.gif','/images/nav/mi/nav_partner_2.gif')">



<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td align="center" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">

      <tr>

        <td valign="top"><table id="Table_01" width="1000" height="714" border="0" cellpadding="0" cellspacing="0">

          <tr>

            <td height="1" colspan="12" valign="top"><table id="Table_" width="1000" height="138" border="0" cellpadding="0" cellspacing="0">

              <tr>

                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_left.png" width="34" height="138" alt="" /></td>



                      <td colspan="3"><a href="/maf/home"><img src="/images/general/logo_clubshop_merchant_interface.gif" alt="" width="428" height="60" border="0" /></a></td>

                <td height="60" colspan="4" align="left" valign="top" background="/images/general/clubshop_merchant_interface_03.gif"><iframe id="ifrm" name="ifrm" src="/maf/_includes/headnav_main.html" scrolling="Auto" width="100%" height="60" frameborder="0" allowtransparency="true"></iframe></td>

                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_right.png" width="34" height="138" alt="" /></td>

              </tr>

	      <!--#include virtual="../_includes/faq_navigation.html" -->

              <tr>

                <td colspan="7"><img src="/images/general/headbar_pages.jpg" width="932" height="34" alt="" /></td>

              </tr>

            </table></td>



          </tr>

          

          <tr>

            <td height="390" colspan="2" background="/images/bg/bg_left.png">&nbsp;</td>

            <td height="390" colspan="8" align="left" valign="top" bgcolor="#FFFFFF"><p><span class="style12">FAQ : Foire aux questions </span><span class="Text_Body_Content"><br /></span>

            </p>

                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">

                  <tbody>

                    <tr>

                      <td colspan="3" align="center" valign="top" background="/images/bg/bg_header-orange-923x23.png" bgcolor="#FCA641" class="Text_Body_Content"><span class="style13">Questions &amp; Réponses </span><br />

                      </td>

                    </tr>

                    <tr>

                      <td colspan="3" align="left" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content"><table width="100%" border="0" cellspacing="0" cellpadding="2">

                          <tr>

                            <td align="left" valign="top" class="text_orange">ACCEPTATION DE LA CARTE</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">1. <a href="#1" class="nav_blue_line">Que dois-je faire si mon lecteur de code-barres ou mon téléphone portable n’enregistre pas ou ne transmet pas la transaction?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">2. <a href="#2" class="nav_blue_line">Comment puis-je faire la preuve de l'achat effectué par un membre de Club shop qui a utilisé sa carte de récompense pour un achat?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">3. <a href="#3" class="nav_blue_line">Pourquoi la personne gérant le registre doit-elle tamponner le reçu ou écrire ses initiales sur un reçu pour le montrer comme une preuve d'achat?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">SOUMETTRE LES TRANSACTIONS</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">4. <a href="#4" class="nav_blue_line">Comment puis-je entrer une transaction manuelle?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">5. <a href="#5" class="nav_blue_line">Quel numéro de téléphone dois-je composer pour envoyer un message texte pour mes transactions Clubshop de récompenses? </a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">6. <a href="#6" class="nav_blue_line">Combien de transactions du Clubshop de récompenses peuvent contenir mon message texte? </a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">TRAITEMENT DES RETOURS</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">7. <a href="#7" class="nav_blue_line">Que dois-je faire si quelqu'un retourne un produit pour lequel il a déjà reçu du crédit pour l'utilisation de sa carte de récompense Clubshop pour avoir acheter le produit retourné? </a> </td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">PAYER MES FRAIS DE RETOUR</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">8. <a href="#8" class="nav_blue_line">Comment puis-je voir combien je vais être facturé?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">9. <a href="#9" class="nav_blue_line">Qu'arrive-t-il si je ne paie pas mes frais de retour?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">LISTE DU CLUBSHOP.COM </td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">10. <a href="#10" class="nav_blue_line">Où dois-je aller pour voir ma fiche d’information commerçante du Clubshop de récompense? </a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">11. <a href="#11" class="nav_blue_line">Comment sont classés les commerçants dans le répertoire Clubshop de récompenses lorsque j’effectue une recherche?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">12. <a href="#12" class="nav_blue_line">Puis-je augmenter mon pack commerçant et améliorer ma fiche de poste?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">13. <a href="#13" class="nav_blue_line">Quand un commerçant peut-il déclasser son pack commerçant?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">LOCALISATION DES FILIALES</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">14. <a href="#14" class="nav_blue_line">J'ai de multiples succursales dans le cadre de la même entreprise. Puis-je mettre en place d'autres filiales commerçantes sous le même compte commerçant?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">15. <a href="#15" class="nav_blue_line">Comment puis-je mettre en place une filiale commerçante?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top" class="text_orange">DISTRIBUTION DES CARTES</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">16. <a href="#16" class="nav_blue_line">Quels avantages y a t-il si un commerçant distribue des cartes de récompense Clubshop?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">17. <a href="#17" class="nav_blue_line">Comment puis-je commander des cartes récompense ou d'autres matériel promotionnel du Clubshop?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">18. <a href="#18" class="nav_blue_line">Où dois-je aller pour activer une carte de récompense Clubshop?</a></td>

                          </tr>

						  

						    <tr>

                            <td align="left" valign="top">19. <a href="#19" class="nav_blue_line">Où puis-je voir les membres que j’ai référé par la remise des cartes ClubShop Rewards ?</a></td>

                          </tr>

						  <tr>

                            <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>

                          </tr>

						  <tr>

                            <td align="left" valign="top" class="text_orange">ACCOUNTING</td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">20. <a href="#20" class="nav_blue_line">How do I sign-up for an Eco Card Account?</a></td>

                          </tr>

                          <tr>

                            <td align="left" valign="top">21. <a href="#21" class="nav_blue_line">How can I access my ClubAccount? </a></td>

                          </tr>

						  

						  <tr>

                            <td align="left" valign="top">22. <a href="#22" class="nav_blue_line">Comment puis-je envoyer un virement bancaire sur le compte Clubshop de la Banque d'Irlande en utilisant le mode de paiement "transferts de fonds Internationaux" ? </a></td>

                          </tr>
						  <tr> 
                              <td align="left" valign="top"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top" class="text_orange">MERCHANT MEMBERSHIP</td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">23. <a href="#23" class="nav_blue_line">Où dois-je faire pour imprimer une carte ClubShop Rewards?</a></td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">24. <a href="#24" class="nav_blue_line">Où dois-je aller voir et échanger le solde de mes points Rewards?</a> </td>
                            </tr>
                            <tr> 
                              <td align="left" valign="top">25. <a href="#25" class="nav_blue_line">Où dois-je aller pour acheter en ligne sur le centre commercial ClubShop?</a></td>
                            </tr>

						  

                      </table></td>

                    </tr>

                    <tr>

                      <td height="8" colspan="3" align="left" valign="top" background="/images/bg/bg_header-orange-923x23.png" bgcolor="#FCA641" class="Text_Body_Content"></td>

                    </tr>

                  </tbody>

                </table>

                <br />

                <span class="style24"><a name="1" id="1"></a>1. Que dois-je faire si mon lecteur de code-barres ou mon téléphone portable n’enregistre pas ou ne transmet pas la transaction? <br />

                </span>Enregistrez les informations de transaction sur un formulaire d'inscription de transaction commerçante, puis entrez dans la page de transaction sur votre interface commerçante. Vous pouvez également contacter votre conseiller pour toute l'assistance nécessaire.<br />

                <br />

                <span class="style24"><a name="2" id="12"></a>2. Comment puis-je faire la preuve de l'achat effectué par un membre de Club shop qui a utilisé sa carte de récompense pour un achat?<br />

                </span>La personne gérant le registre aura tamponné l’arrière du reçu ou écris ses initiales sur un reçu.<a href="javascript:popupcenter('../window/purchase_stamp_window.html', 666, 450)" class="nav_blue_line">Cliquez ici</a> pour commander un tampon du Clubshop de récompense.<br />

                <br />

                <span class="style24"><a name="3" id="13"></a>3.  Pourquoi la personne gérant le registre doit-elle tamponner le reçu ou écrire ses initiales sur un reçu pour le montrer comme une preuve d'achat?<br />

                </span>Cela donnera au titulaire de la carte de récompense la preuve qu'il a effectué un achat en utilisant sa <span class="style5"> carte de récompense Clubshop</span>. Si pour une raison ou une autre, la transaction du titulaire de la carte récompense n'a pas été inscrit dans l'interface du commerçant, le titulaire de la carte de récompense qui a fait l'achat a une preuve d'achat à faire montrer au commerçant, de sorte que le commerçant pourra lui donner le crédit requis. <br />

                <br />

De plus, si un titulaire de carte Rewards retourne un article ou demande un remboursement, cela sera votre tache de savoir qu’il faut supprimer le crédit pour l'article, de sorte que vous ne payerez pas de commission pour l'article. <br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="4" id="132"></a>4. Comment puis-je entrer une transaction manuelle?<br />

</span>

<ol>

  <li>Enregistrez les informations de transaction sur un formulaire d’entrée de transaction commerçante</li>

  <li> De votre «Interface commerçante», vous devez cliquer sur l’onglet "Transaction" et sélectionnez "Entrée de transaction" sur la page de transaction. </li>

  <li> De l’ «Entrée des transactions", cliquez dans la case "Numéro d'identification <span class="style5">Clubshop récompenses</span>» et ensuite scannez le code barre de "<span class="style5">la carte de récompense Clubshop</span>". En scannant le code barre de "<span class="style5">la carte de récompense Clubshop</span>", le numéro de <span class="style5">la carte de récompenses Clubshop</span> sera automatiquement inscrit dans la case par le scanner </li>

  <li> Cliquez dans la case "Montant d'achat» et tapez le montant total de l'achat (à l'exclusion de la taxe de vente) </li>

  <li> Cliquez dans la case "date de la transaction» et tapez la date</li>

  <li> Cliquez sur le bouton "Ajouter à la liste" pour envoyer votre transaction à la «Liste des transactions cumulatives". Répétez ces étapes si nécessaire, afin d'entrer plus de transactions dans la "Liste des transactions cumulatives"</li>

  <li> Pour soumettre toutes les transactions de la «Liste des transactions cumulatives", vous devez sélectionner votre devise</li>

  <li> Passez toutes les transactions en revue pour vérifier leurs exactitudes et si vous trouvez des erreurs, vous pouvez modifier les transactions de la «Liste des transactions cumulatives".</li>

  <li> Cliquez une fois sur le bouton "Soumettre la liste" et attendez votre rapport de transaction s’affiche en desous.</li>

  <li> Pour visualiser les transactions, merci d’aller dans le "rapport de Transaction" </li>

</ol>

<span class="style24"><a name="5" id="133"></a>5. Quel numéro de téléphone dois-je composer pour envoyer un message texte pour mes transactions <span class="style5">Clubshop de récompenses</span>?<br />

</span>Si vous envoyer votre texte hors du territoire US, le n° est 001-941-662-8608. Si c’est un texte US N° est1-941-662-8608. En nous envoyant un texte pour les transactions du Clubshop de récompense, nous le traiterons automatiquement et entrerons les informations de vos transactions dans votre interface commerçante. Notez qu'il peut y avoir un coût supplémentaire associé à l'envoi d'un texte, sur la base de votre forfait, surtout si vous l'envoyez de l'extérieur des États-Unis. <br />

<br />

<span class="style24"><a name="6" id="134"></a>6. Combien de transactions du Clubshop de récompenses peuvent contenir mon message texte?<br />

</span>Cela dépend de votre téléphone mobile. Certains téléphones mobiles peuvent contenir seulement 160 caractères, tandis que d'autres téléphones portables peuvent contenir plus de 500 caractères. Vérifiez ce que votre téléphone peut contenir, de sorte que vous puissiez envoyer des transactions multiples avec un seul message.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="7" id="1322"></a>7. Que dois-je faire si quelqu'un retourne un produit pour lequel il a déjà reçu du crédit pour l'utilisation de sa carte de récompense Clubshop pour avoir acheter le produit retourné?<br />

</span>De votre «Interface commerçante», vous devez cliquer sur la sur l’onglet "Interface commerçante" et sélectionnez l'onglet "Rapport de  Transaction" de la page de transaction. Dans cette page "Rapport de Transaction", cliquez sur la date à laquelle le retour a eu lieu. Puis, cliquez sur l'ID de la transaction, entrez le montant du remboursement et sélectionnez le bouton "soumettre". Pour visualiser le remboursement du retour, retournez sur la page "Rapport de Transaction" et cliquez sur la date à laquelle le retour a eu lieu. Depuis cette page vous pourrez voir le remboursement du retour. <br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="8" id="1332"></a>8. Comment puis-je voir combien je vais être facturé?<br />

</span>Pour consulter ce montant et les montants de factures précédentes, allez sur  votre "Rapport de Transaction", cherchez le mois que vous voulez voir et le montant total qui nous est dû sera dans le sommaire de transaction sur la page "Rapport de Transaction". <span class="style5">Le Clubshop de récompense</span> vous enverra également un email le 1er de chaque mois vous montrant le total des transactions et le montant que vous devez. Le 5 de chaque mois, vous serez facturé du montant dû pour les transactions des mois précédents. Imprimez ce rapport chaque mois ent tant que facture pour l'utiliser à des fins fiscales.<br />

<br />

<span class="style24"><a name="9" id="1342"></a>9. Qu'arrive-t-il si je ne paie pas mes frais de retour?<br />

</span> <span class="style5">Le Clubshop de récompense</span> prendra contact avec vous s’il n’est pas en mesure de collecter la somme qui lui est dû. Vous devrez ensuite prendre des dispositions pour nous fournir (une autre) carte de crédit ou pour financer votre compte Club de suffisamment d'argent pour payer ce qui est dû. Si nous ne sommes pas en mesure de recueillir vos frais de retour, il nous faudra désactiver votre compte et reprendre le crédit que les titulaires de cartes pensaient avoir obtenu. En outre, nous devrons informer tous les titulaires de carte dans votre région que votre compte a été désactivé pour cause de non-paiement des frais de retour. En raison de la mauvaise publicité que cela donnera à votre entreprise, même si vous décidez que vous voulez annuler votre compte, il serait honorable et mieux payer les frais de retour qui sont dus, pour ensuite demander une annulation de votre compte.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="10" id="1323"></a>10. Où dois-je aller pour voir ma fiche d’information commerçante du Clubshop de récompense? <br />

</span> Vous pouvez aller sur <a href="/rewardsdirectory" target="_blank" class="nav_blue_line">http://www.clubshop.com/rewardsdirectory</a> pour voir fiche d’information.<br />

<br />

<span class="style24"><a name="11" id="1333"></a>11. Comment sont classés les commerçants dans le répertoire Clubshop de récompenses lorsque j’effectue une recherche?<br />

</span>Ils sont classés sur la base du pack commerçant qu'ils ont choisis, avec la formule "Or", les commerçants seront les mieux classés et avec le forfait de base gratuit, les commerçants seront les plus bas dans la liste. Les commerçants ayant les mêmes packs commerçants sont ensuite classés sur la base du pourcentage des frais de renvoi qu'ils ont choisis, les frais les plus élevé gagnant le meilleur classement. <br />

<br />

<span class="style24"><a name="12" id="1343"></a>12. Puis-je augmenter mon pack commerçant et améliorer ma fiche de poste?<br />

</span>Oui, mais vous devez payer la différence pour la mise à jour de votre forfait et votre date de renouvellement de commerçant sera toujours la date initiale à laquelle vous avez originellement payé votre pack commerçant.<br />

<br />

<span class="style24"><a name="13" id="13432"></a>13. Quand un commerçant peut-il déclasser son pack commerçant?<br />

</span> Dans les 30 jours de la date de renouvellement du commerçant.<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="14" id="13222"></a>14. J'ai de multiples succursales dans le cadre de la même entreprise. Puis-je mettre en place d'autres filiales commerçantes sous le même compte commerçant?<br />

</span>Votre pack commerçant permettra de déterminer si et comment vous pourrez mettre en place les multiples succursales commerçantes <br />

<ul>

  <li><span class="text_orange">Pack commerçant gratuit</span> – 1 commerçant principal et 0 filiale commerçante</li>

  <li><span class="text_orange">Pack commerçant Bronze</span> – 1 commerçant principal et 2 filiales commerçantes</li>

  <li> <span class="text_orange">Pack commerçant Argent</span>  – 1 commerçant principal et 4 filiales commerçantes</li>

  <li> <span class="text_orange">Pack commerçant Or</span> – 1 commerçant principal et 8 filiales commerçantes</li>

</ul>

Un commerçant qui un total de 10 filiales ou plus peut avoir besoin d'un appui substantiel et devra donc être enregistré directement auprès du <span class="style5">DHSC Clubshop de récompenses</span>. <br />

<br />

<a name="15" id="13433"></a>15. Comment puis-je mettre en place une filiale commerçante?<br />

</span>Si un commerçant veut ajouter une filiale commerçante, il doit choisir l'onglet "Ajouter Service commerçant» dans la page  "Mise à jour du profil»$. Chaque filiale commerçante sera mise en place avec sa propre adresse e-mail et son propre mot de passe afin que chaque filiale puisse se connecter à son interface commerçante particilière. Seul le compte principal de connexion aura accès aux informations sur le compte principal. 

<br />

<hr align="left" width="100%" size="2" color="#FCBF12" />

<span class="style24"><a name="16" id="132222"></a>16. Quels avantages y a t-il si un commerçant distribue des cartes de récompense Clubshop?<br />

                  </span>Un commerçant peut avoir un certain nombre d'avantages en distribuant des cartes de récompense Clubshop, mais le plus grand avantage est que cela lui rapporte des commissions de référence pour chaque achat effectué par le titulaire de la carte avec un commerçant participant en ligne ou hors ligne au centre commerciale  de ClubShop. Cela inclut également l obtention de Commission de références pour les achats effectués à votre magasin, ce qui réduit votre pourcentage net de 20%. Pour de plus amples renseignements sur la façon de devenir un commerçant partenaire et sur la distribution des cartes, cliquez sur l'onglet partenaire en haut de cette page.  Pour voir les fais de référence que j'ai gagné en remettant les cartes Rewards du ClubShop qui sont assignées à mon adhésion commerçante, allez sur

<a href="https://www.clubshop.com/cgi/merchant_comm_rpt.cgi" class="nav_blue_line" target="_blank">https://www.clubshop.com/cgi/merchant_comm_rpt.cgi</a> <br />

<br />

<br />

<span class="style24"><a name="17" id="134332"></a>17. Comment puis-je commander des cartes récompense ou d'autres matériel promotionnel du <span class="style5">Clubshop</span>?<br />

</span>Visitez notre centre d’affaire du <span class="style5">Clubshop</span> à l'adresse<a href="/outletsp/bcenter.html" class="nav_blue_line">https://www.clubshop.com/outletsp/bcenter.html</a> pour visualiser et commander des articles promotionnels disponibles du <span class="style5">Clubshop de récompense</span>.<br />

<br />

<span class="style24"><a name="18" id="1343322"></a>18. Où dois-je aller pour activer une carte de <span class="style5">récompense Clubshop</span>?<br />

</span><a href="/mycard" target="_blank" class="nav_blue_line">http://www.clubshop.com/mycard<br />

<br />

</a>

<br />

<span class="style24"><a name="19" id="1343322"></a>19. Où puis-je voir les membres que j’ai référé par la remise des cartes ClubShop Rewards?<br />

</span><a href="https://www.clubshop.com/cgi/merch-frontline-rpt.cgi" target="_blank" class="nav_blue_line">https://www.clubshop.com/cgi/merch-frontline-rpt.cgi<br />

<br />

</a>



<hr align="left" width="100%" size="2" color="#FCBF12" />

<br />

<span class="style24"><a name="19" id="134332"></a>20. How do I sign-up for an Eco Card Account?<br />

</span><a href="https://www.ecocard.com/Registration.aspx?Referee=DHS" class="nav_blue_line">https://www.ecocard.com/Registration.aspx?Referee=DHS</a><br />

<br />

<span class="style24"><a name="20" id="1343322"></a>21. How can I access my ClubAccount? <br />

</span><a href="https://www.clubshop.com/cgi/funding.cgi?action=report " target="_blank" class="nav_blue_line">https://www.clubshop.com/cgi/funding.cgi?action=report </a><br />

<br />



<span class="style24"><a name="20" id="1343322"></a>22. Comment puis-je

envoyer un virement bancaire sur le compte Clubshop de la Banque d'Irlande

en utilisant le mode de paiement "transferts de fonds Internationaux" ? 

<br />

                  </span>Le mode de paiement de "Transferts de fonds

internationaux " est actuellement uniquement pour les

pays de l’Union Européenne.

Une fois que nous recevons de l'argent sur notre compte de la banque

d’Irlande et que nous en sommes notifié (7 à 10 jours ouvrables), nous

transférerons l'argent sur votre compte Club. Voici les détails pour le

virement bancaire si vous souhaitez envoyer de l'argent en utilisant ce

mode de paiement:<br/><br/>

   <b>NOM DE LA BANQUE:</b> Bank of Ireland <br/>

   <b>ADRESSE DE LA BANQUE:</b> Northern Cross, 2-3 Burnell Green, Malahide

Road, Dublin 17<br/>

   <b>CODE D'ETABLISSEMENT (NSC):</b> 904587<br/>

   <b>NUMERO DE COMPTE:</b> 95145818<br/>

   <b>CODE IBAN :</b> IE06 BOFI 9045 8795 1458 18<br/>

   <b>NUMERO BIC:</b> BOFIIE2D <br/>

<br/>
   
                  <hr align="left" width="100%" size="2" color="#FCBF12" />
                  <span class="style24"><a name="23" id="1343323"></a>23. Où dois-je faire pour imprimer une carte ClubShop Rewards?</span><br>
				   <a href="http://www.clubshop.com/getcards.html" target="_blank" class="nav_blue_line">http://www.clubshop.com/getcards.html</a> 
<br /><br />

					<span class="style24"><a name="24" id="1343323"></a>24. Où dois-je aller voir et échanger le solde de mes points Rewards?</span><br>
				   <a href="http://www.clubshop.com/cgi/reward_center.cgi" target="_blank" class="nav_blue_line">http://www.clubshop.com/cgi/reward_center.cgi</a> 
<br /><br />

					<span class="style24"><a name="25" id="1343323"></a>25. Où dois-je aller pour acheter en ligne sur le centre commercial ClubShop?</span><br>
				   <a href="http://www.clubshop.com/mall" target="_blank" class="nav_blue_line">http://www.clubshop.com/mall</a> 
<br /><br />









</td>

            <td height="390" colspan="2" background="/images/bg/bg_right.png">&nbsp;</td>

          </tr>

          <tr>

            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="52" alt="" /></td>

          </tr>

          <tr>

            <td height="134" colspan="12" background="/images/general/footerbar.png"><iframe id="ifrm2" name="ifrm" src="/maf/_includes/footer.html" scrolling="Auto" width="100%" height="134" frameborder="0" allowtransparency="true"></iframe></td>

          </tr>



          <tr>

            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>



            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>

          </tr>

        </table></td>

      </tr>

      

    </table></td>

  </tr>



  <tr>

    <td align="center" valign="top">&nbsp;</td>

  </tr>

</table>



</body>

</html>

