<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop Merchant - Tracking Forms</title>

<link rel="stylesheet" type="text/css" href="_includes/css/main.css" />

<style type="text/css">
body {
background-color:white;
background-image:none;
}
</style>

</head>
<body>


<!-- content begins -->
<p><span class="Blue_Large_Bold">Tracking Forms</span></p>
						
						Deze pagina is enkel voor handelaren die betalen op iedere getraceerde transactie.<br /> 
                    
                              <p>Er zijn twee manieren mogelijk om een ClubShop transactie in te dienen en bij te houden. We raden u aan om in het begin de manuele methode te gebruiken, om later eventueel over te schakelen op een meer geautomatiseerde methode. We raden u ook aan om, indien u een andere methode opteert, toch in het begin iedere ClubShop Rewards transactie manueel te noteren en bij te houden, bij wijze van backup.</p>
                                
								<p><img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> <span class="Blue_Large_Bold"> 1. MANUEEL</span> 
                                  - Print ons <a href="/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" target="_blank">Manueel Transactie formulier</a> en noteer ieder ClubShop Rewards kaartnummer, bedrag van transactie (Exclusief BTW) en datum (mm-dd-jjjj). Na de laatste klant, voert u aan de hand van het manuele formulier, iedere ClubShop Rewards transactie online in op de <a href="/cgi/maf/maf_transactions.cgi"> 
                                  Online Transactie pagina</a></p>
								  
								  
                              <p><img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> <span class="Blue_Large_Bold"> 2. SCANNER en COMPUTER</span> - Mocht u in het bezit zijn van een scanner die naast uw kassa verbonden is met uw computer, dan kunt u iedere ClubShop Rewards kaart rechtsreeks op de <a href="/cgi/maf/maf_transactions.cgi"> 
                                Online Transactie pagina</a> door deze in te scannen en vervolgens manueel het transactie bedrag (excl. BTW) en datum (mm-dd-jjj) invullen. Om een ClubShop Rewards kaart te scannen, dient u eerst met uw cursor het lege Rewards kaartnummer veld aan te klikken en dan de kaart scannen. Na de scan verschijnt het ClubShop Rewards kaartnummer automatisch in het kaartnummer veld. Daarna volgt u de richtlijnen over hoe een ClubShop Rewards transactie correct in te dienen.</p>
                          
						  
						  
						  <p> <span class="Blue_Large_Bold">Additional Links</span></p>
						  
                             <p><img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> <a href="/cgi/maf/maf_transactions.cgi" class="nav_blue_line">Transaction 
                                Online Entry</a> - Enter your ClubShop Reward 
                                transactions directly into your account</p>
								
								<p><img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/>  <a href="/maf/transaction/pdf/CS_Rewards_Transaction_Entry_Form.pdf" class="nav_blue_line" target="_blank">Transaction 
                                Manual Entry Form</a> - Keep track of your ClubShop 
                                Reward transactions to enter in your account at 
                                a later time.</p>
                           
                              <p><img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> <a  href="/cgi/maf/ma_trans_rpt.cgi" class="nav_blue_line">Transaction 
                                Report</a> - Review or Refund your entered ClubShop 
                                Reward transactions.</p>












</body>
</html>