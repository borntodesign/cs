<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
		media-type="text/html" />
		<xsl:include href = "/maf/_includes/navigation.xsl" />
		<!-- xsl:include href = "/Users/khasely/workspace/Club_Shop/cgi-templates/xsl/common.xsl" / -->
	<xsl:template match="/">
	<html>
	<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - Title - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
	<xsl:value-of select="//lang_blocks/title"/>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
</title>

<meta name="title" content="clubshop rewards incentive and loyalty program"/>
<meta name="keywords" content="clubshop rewards incentive and loyalty program, reward popints"/>
<meta name="abstract" content="clubshop rewards incentive and loyalty program "/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop rewards incentive and loyalty program"/>
<meta name="design-development" content="carsten rieger"/>

<meta name="publisher" content="clubshop rewards incentive and loyalty program"/>
<meta name="company" content="clubshop"/>
<meta name="copyright" content="copyright © 2009 clubshop rewards incentive and loyalty program. all rights reserved"/>
<meta name="page-topic" content=" clubshop rewards incentive and loyalty program "/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="general"/>
<meta name="revisit-after" content="7 days"/>

<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
<xsl:comment>
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i&lt;a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[i])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))&gt;0&amp;&amp;parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&amp;&amp;d.all) x=d.all[n]; for (i=0;!x&amp;&amp;i&lt;d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&amp;&amp;d.layers&amp;&amp;i&lt;d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x &amp;&amp; d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i&lt;(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//</xsl:comment>
</script>
</head>

<body onload="MM_preloadImages('/images/nav/maf/nav_faq_1.gif','/images/nav/maf/nav_merchant_2.gif','/images/nav/maf/nav_transaction_2.gif','/images/nav/maf/nav_update_payment_2.gif','/images/nav/maf/nav_advertising_2.gif')">

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><table id="Table_01" width="1000" height="714" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="1" colspan="12" valign="top"><table id="Table_" width="1000" height="138" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_left.png" width="34" height="138" alt="" /></td>

                <td colspan="3"><a href="/maf/home/"><img src="/images/general/logo_clubshop_merchant_interface.gif" alt="" width="428" height="60" border="0" /></a></td>
                <td height="60" colspan="4" align="left" valign="top" background="/images/general/clubshop_merchant_interface_03.gif"><iframe id="ifrm" name="ifrm" src="/maf/_includes/headnav_main.html" scrolling="Auto" width="100%" height="60" frameborder="0" allowtransparency="true"></iframe></td>
                <td rowspan="3"><img src="/images/general/clubshop_Merchant_Interface_right.png" width="34" height="138" alt="" /></td>
              </tr>
		<xsl:call-template name="build_navigation">
			<!-- xsl:with-param name="section" select="'transaction'" / -->
			<xsl:with-param name="section" select="'profile'" />
			<!-- xsl:with-param name="section" select="'payment'" / -->
			<!-- xsl:with-param name="section" select="'setup'" / -->
			<!-- xsl:with-param name="section" select="'faq'" / -->
			<!-- xsl:with-param name="section" select="'partner'" / -->
		</xsl:call-template>
              <tr>
                <td colspan="7"><img src="/images/general/headbar_pages.jpg" width="932" height="34" alt="" /></td>
              </tr>
            </table></td>

          </tr>
          
          <tr>
            <td height="390" colspan="2" background="/images/bg/bg_left.png">
            	<xsl:text> </xsl:text>
            </td>
            <td height="390" colspan="8" align="left" valign="top" bgcolor="#FFFFFF">
            <div align="center">
           		<span class="style12">
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!--  This is where the page header goes. -->
					<xsl:value-of select="//lang_blocks/title"/>
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
            	</span>
              	<span class="Text_Body_Content">
              		<!-- br / -->
                    <!-- br / -->
                    <!-- Blah Blah Blah goes here -->
                	<xsl:value-of select="//lang_blocks/introduction" />
                	<br />
                	<br />
                </span>
            	</div>              	
            	
	            <br />
				<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">
              
              
                	<tbody>
                  		<tr>
                    		<td colspan="3" align="center" valign="top" bgcolor="#FCA641" class="Text_Body_Content" background="/images/bg/bg_header-orange-923x23.png">
                    			<span class="style13"><xsl:value-of select="//lang_blocks/form_heading" /></span>
                    			<br />
                    		</td>
                    	</tr>            
            			<tr>
                    		<td colspan="3" align="center" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content">
                    
                    
				<xsl:if test="//data/errors/*">
					<div align="left">
						<xsl:call-template name="output_errors" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>
				<xsl:if test="//data/warnings/*">
					<div align="left">
						<xsl:call-template name="output_warnings" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>
				<xsl:if test="//data/notices/*">
					<div align="left">
						<xsl:call-template name="output_notices" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>

            
                <hr align="left" width="100%" size="2" color="#FCBF12" />
                
                <div align="left">
	            <span class="Text_Body_Content">
	            	<br />
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - Blah blah blah goes here  - - - - - -->
					<xsl:value-of select="//lang_blocks/master_login_to_do_that"/>
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
			<br /><br />
	            </span>
              </div>
           
            
            </td>
            </tr>
            </tbody>
            </table>
            
            
            </td>
            <td height="390" colspan="2" background="/images/bg/bg_right.png"><xsl:text> </xsl:text></td>

          </tr>
          <tr>
            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td>
          </tr>
          <tr>
            <td height="134" colspan="12" background="/images/general/footerbar.png"><iframe id="ifrm2" name="ifrm" src="/maf/_includes/footer.html" scrolling="Auto" width="100%" height="134" frameborder="0" allowtransparency="true"></iframe></td>
          </tr>
          <tr>

            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
          </tr>
        </table></td>
      </tr>
      
    </table></td>
  </tr>
  <tr>

    <td align="center" valign="top"><xsl:text> </xsl:text></td>
  </tr>
</table>

</body>
</html>

</xsl:template>

</xsl:stylesheet>

