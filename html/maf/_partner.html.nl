<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop Rewards Promotieartikelen</title>

<link rel="stylesheet" type="text/css" href="_includes/css/main.css" />

<!--#include virtual="linksnscripts.html" -->
<script type="text/javascript" src="_includes/js/iframeAutoResize.js"></script>
<style type="text/css">
body {
background-color:white;
background-image:none;
}
</style> 
</head>
<body>



<!-- content begins -->
<p><span class="Blue_Large_Bold">ClubShop Rewards Promotieartikelen</span></p>
                  <p><span class="Text_Body_Content">
                    Behalve ClubShop Rewards handelaar worden en de ClubShop Rewards kaart te accepteren in uw zaak, bieden wij u ook de mogelijkheid aan om ClubShop Rewards kaarten te verdelen! Hieronder vindt u enkele van de meerdere voordelen die het uitdelen van ClubShop Rewards kaarten met zich mee brengt!</span> </p>
                  <img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> Het versterkt de getrouwheid van uw bestaande klanten en creëert grotere klantengetrouwheid bij uw nieuwe klanten.
                    <br/>
                    <img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> Uw zaak krijgt meer geloofwaardigheid als bedrijf in een mondiale coalitie met de grootste en de beste ondernemers van overal ter wereld.
                    <br/>
                    <img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> Het baat uw bestaande en nieuwe klanten door hen te voorzien van een nieuwe manier om geld te besparen wanneer ze zaken doen met andere ondernemers, zowel online als offline.<br/>
                    <img src="images/icons/icon_bullet_orannge.png" width="39" height="9"  alt=""/> Het verrijkt uw huidig klantenbestand en uit zich in een andere bron van inkomsten. Iedere keer een kaarthouder, met de kaart die u hem gaf, online of offline een aankoop doet bij een deelnemend ClubShop Rewards handelaar, zult u op die aankoop een Referral Commissie verdienen. U verdient 20% op het bedrag dat iedere deelnemende handelaar ons betaalt voor die verkoop.<br/><br/>
					
                  <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
                    <tr> 
                      <td width="300" align="center"  bgcolor="#FFFFFF"><div align="center"><img src="/images/general/clubshop_rewads_card.gif" alt="Plastic ClubShop Rewards Card" width="282" height="177" vspace="20" /></div></td>
                      <td width="299" align="center"  bgcolor="#FFFFFF"><div align="center"><img src="/images/brochures/brochure.gif" height="161" width="208" alt="ClubShop Brochure"/></div></td>
                    </tr>
                    <tr> 
                      <td  bgcolor="#FFFFFF"><img src="/images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding_only" /> 
                        Koop Plastic ClubShop Rewards kaarten (USA) <br />
                        Klik hier om <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=1&SearchDept.x=15&SearchDept.y=13" target="_blank" class="nav_orange">plastic 
                        USA kaarten aan te schaffen</a><br /> <br /> <img src="/images/icons/icon_checkmark.png" width="12" height="12" border="0" class="image_right_padding_only" /> 
                        Koop plastic ClubShop Rewards kaarten (Internationaal)<br />
                        Klik hier om <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=2&SearchDept.x=32&SearchDept.y=14" target="_blank" class="nav_orange">plastic 
                        internationale kaarten aan te schaffen</a><br /></td>
                      <td  bgcolor="#FFFFFF"><img src="/images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding_only" /> 
                        ClubShop Rewards Brochure with detachable card (USA) <br />
                        Click here to <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=7&SearchDept.x=11&SearchDept.y=8" target="_blank" class="nav_orange">Purchase 
                        USA Brochure</a><br /> <br /> <img src="/images/icons/icon_checkmark.png" width="12" height="12" border="0" class="image_right_padding_only" /> 
                        ClubShop Rewards Brochure with detachable card (International)<br />
                        Click here to <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=8&SearchDept.x=22&SearchDept.y=9" target="_blank" class="nav_orange">Purchase 
                        International Brochure</a><br /></td>
                    </tr>
                    <tr> 
                      <td  bgcolor="#FFFFFF" colspan="2"><div align="center"><img src="/images/cs_merchant_decal.jpg" width="203" height="203" alt="Merchant Window Decal"/></div>
                        <div align="center"></div></td>
                    </tr>
                    <tr> 
                      <td  bgcolor="#FFFFFF" colspan="2"><div align="center"><img src="/images/icons/icon_checkmark.png" width="12" height="12" class="image_right_padding_only" /> 
                          Koop ClubShop Rewards raamklevers (USA) <br />
                          Klik hier om <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=3&SearchDept.x=27&SearchDept.y=9" target="_blank" class="nav_orange">USA 
                          raamklevers aan te schaffen</a><br />
                          <br />
                          <img src="/images/icons/icon_checkmark.png" width="12" height="12" border="0" class="image_right_padding_only" /> 
                          Koop ClubShop Rewards raamklevers (Internationaal)<br />
                          Klik hier om <a href="http://www.clubshop.com/cgi/cart/cart.cgi?DT=4&SearchDept.x=12&SearchDept.y=13" target="_blank" class="nav_orange">internationale 
                          raamklevers aan te schaffen</a></div></td>
                    </tr>
                  </table>










</div>




<script type="text/javascript" src="../loginLink.js"></script>
</body>
</htm