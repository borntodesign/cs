<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:include href="/xml/head-foot.xsl"/>
<xsl:template match="/">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="//lang_blocks/common/t" /></title>

<link rel="stylesheet" href="/css/global.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="/css/questionnaires.css" />
<link type="text/css" rel="stylesheet" href="/mall/US/css/xmall.css" />

<script src="/js/miniLogin.js" type="text/javascript"></script>
<script src="/js/questionnaires.js" type="text/javascript"></script>

</head>
<body>
<!--  div style="width:765px; margin-right:auto;margin-left:auto; border:1px solid navy; padding: 2px;" -->
<xsl:call-template name="head" />


<table class="xm_main">
<thead>
<tr>
<td colspan="2" align="center">
<xsl:call-template name="mall_list" />
</td>
</tr>
</thead>
<tfoot>
<tr>
<td colspan="2" align="center">

<div align="center"><xsl:call-template name = "foot" /></div>

<div class="xm_copyright"><img src="/images/copyright_.gif" width=
"173" height="19" alt="Copyright" /></div>
</td>
</tr>
</tfoot>
<tbody>
<tr>
<td>
<div id="maindiv"><xsl:copy-of select="//body/*" /></div>
<!-- /div -->
</td>
</tr>
</tbody>
</table>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
