<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	media-type="text/html"/>

<xsl:template match="/">
<b><content>
<!-- The HTML content for the right main part of the page goes here -->
<a name="top"/>

		<h3>
			<xsl:value-of select="//lang_blocks/p6"/>
		</h3>

<form action=""><select style="width:100%; font-size:85%"><xsl:attribute name="onchange">
document.location= '#'+this.options[this.selectedIndex].value;this.selectedIndex=0;
</xsl:attribute>
<option value=""><xsl:value-of select="//lang_blocks/select_option" /></option>
<xsl:for-each select = "//lang_blocks/*[@q != '']">
<option><xsl:attribute name="value"><xsl:value-of select="name()" /></xsl:attribute>
<xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">a</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>
<xsl:value-of select="." /></option>
</xsl:for-each>
</select></form>

<a name="p7"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p7"/></span><br />
	<xsl:value-of select="//lang_blocks/p8-a"/><xsl:text>
	</xsl:text><a class="a" href="shoppinginstructions.xml" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p8-b"/></a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p8-c"/></p>

<a name="p10"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p10"/></span><br />
	<xsl:value-of select="//lang_blocks/p11"/></p>

<p>

	<xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p13"/><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p14-a"/><xsl:text>
	</xsl:text><a class="a" href="mailto:purchases@dhs-club.com">purchases@dhs-club.com</a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p14-b"/></p>


<xsl:call-template name="top" />






<a name="p17"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p17"/></span><br />
<xsl:value-of select="//lang_blocks/p17_t"/> <xsl:text> </xsl:text>
<a class="a" href="javascript:void()" onclick="var converter=window.open('/cgi/converter.cgi', 'converter', 'width=500, height=240,resizable,scrollbars');converter.focus();return false;"><xsl:value-of select="//lang_blocks/p17_tc"/></a>
<xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p17_tc2"/>
<br/>	<br/><xsl:value-of select="//lang_blocks/p18-a"/><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p18_2-a" /><xsl:text>
	</xsl:text><a class="a" href="members/" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p18_2-b"/></a>
	<xsl:if test="//lang_blocks/p18_2-c != ''"><xsl:text>
		</xsl:text><xsl:value-of select="//lang_blocks/p18_2-c"/>
	</xsl:if>.</p>
<xsl:call-template name="top" />









<a name="p19"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p19"/></span><br />
	<xsl:value-of select="//lang_blocks/p19a"/></p>
<xsl:call-template name="top" />

<a name="p20"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p20"/></span><br />
	<xsl:value-of select="//lang_blocks/p21-a"/><xsl:text>
	</xsl:text><a class="a" href="/mall/missing_order.xml" onclick="window.open(this.href);return false;">
	<xsl:value-of select="//lang_blocks/p21-b"/></a><xsl:text>
	</xsl:text><xsl:value-of select="//lang_blocks/p21-c"/></p>
<xsl:call-template name="top" />

<a name="p23"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p23"/></span><br />
	<xsl:value-of select="//lang_blocks/p24"/></p>
<ol>
	<li><xsl:value-of select="//lang_blocks/li-1-a"/><xsl:text>
		</xsl:text><a class="a" href="shoppinginstructions.xml" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/li-1-b"/></a><xsl:text>
		</xsl:text><xsl:value-of select="//lang_blocks/li-1-c"/>
	</li>
	<li><xsl:value-of select="//lang_blocks/li-2"/>
		(<a class="a" href="/manual/technical/cookie_help.xml" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a17"/></a>)
	</li>
	<li><xsl:value-of select="//lang_blocks/li-3"/></li>
	<li><xsl:value-of select="//lang_blocks/li-4"/></li>
	<li><xsl:value-of select="//lang_blocks/li-5"/></li>
	<li><xsl:value-of select="//lang_blocks/li-6"/></li>
</ol>
<xsl:call-template name="top" />

<a name="p31"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p31"/></span><br />
	<xsl:value-of select="//lang_blocks/p32"/><xsl:text> </xsl:text><img src="/images/icons/icon_more_info.gif" alt="info_icon"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p32a"/> </p>
<xsl:call-template name="top" />

<a name="p33"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p33"/></span><br />
	<xsl:value-of select="//lang_blocks/p34"/></p>
<xsl:call-template name="top" />

<a name="p35"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p35"/></span><br />
	<xsl:value-of select="//lang_blocks/p36"/><xsl:text>
	</xsl:text><a class="a" href="https://www.clubshop.com/m-update.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a18"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p37"/></p>
<xsl:call-template name="top" />

<a name="p38"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p38"/></span><br />
	<xsl:value-of select="//lang_blocks/p39"/><xsl:text>
	</xsl:text><a class="a" href="/cs/testimonials.shtml" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a19"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p40"/></p>
<xsl:call-template name="top" />

<a name="p41"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p41"/></span><br />
	<xsl:value-of select="//lang_blocks/p42"/></p>
<xsl:call-template name="top" />

<a name="p43"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p43"/></span><br />
	<xsl:value-of select="//lang_blocks/p44"/><xsl:text>
	</xsl:text><a class="a" href="/cgi/appx.cgi" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a20"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45"/></p>
<xsl:call-template name="top" />
	
<a name="p46"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p46"/></span><br />
	<xsl:value-of select="//lang_blocks/p47"/><xsl:text> </xsl:text>
	<a class="a" href="/mall/US" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a21"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/a21aa"/></p>
<xsl:call-template name="top" />


<a name="p55"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p55"/></span><br />
	<xsl:value-of select="//lang_blocks/p56"/></p>
<xsl:call-template name="top" />

<a name="p57"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p57"/></span><br />
	<xsl:value-of select="//lang_blocks/p58"/><xsl:text> </xsl:text>
	<a class="a" href="/idlookup.html" onclick="window.open(this.href);return false;">
		<xsl:value-of select="//lang_blocks/a24"/></a>
	<xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p59"/></p>
<xsl:call-template name="top" />



<a name="p62"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p62"/></span><br />
	<a class="a" href="/mall_listing.xml">
		<xsl:value-of select="//lang_blocks/a25"/></a></p>
		<xsl:call-template name="top" />



<a name="p65"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p65"/></span><br />
			<xsl:value-of select="//lang_blocks/a66"/></p>
			<xsl:call-template name="top" />



<a name="p70"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p70"/></span><br />
			<xsl:value-of select="//lang_blocks/a70"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/reward_points.cgi"  class="a" target="_blank">ClubShop Rewards Points Report</a></p>
			<xsl:call-template name="top" />

<a name="p80"/>
<p><span class="bl"><xsl:value-of select="//lang_blocks/p80"/></span><br />
			<xsl:value-of select="//lang_blocks/a80"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/US"  class="a" target="_blank"><xsl:value-of select="//lang_blocks/a80a"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/a80b"/></p>
			<xsl:call-template name="top" />

<!-- END of the content -->
</content>
<xsl:copy-of select="*" />
</b>
</xsl:template>


<xsl:template name="top">
<div class="gt"><a href="#top"><xsl:value-of select="//lang_blocks/top" /></a></div>
</xsl:template>
</xsl:stylesheet>
