<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
	<xsl:template match="/">
	
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><xsl:value-of select="/lang_blocks/title" /></title>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script>

<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

    <table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto; width:1000px;">
      <tr>
        <td valign="top"><table id="Table_01" width="1000" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="12" valign="top"><img src="/images/headers/plain-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" /></td>
          </tr>
          
          <tr>
            <td colspan="2" style="background-image:url(/images/bg/bg_left.png)"></td>

            <td colspan="8" bgcolor="#FFFFFF">
           <span class="style12"><xsl:value-of select="/lang_blocks/title" /></span>
             
             <form action="/cgi/FormMail.cgi" method="post">
      
                <input name="recipient" type="hidden" value="acfspvr@dhs-club.com"/>
                <input name="email" type="hidden" value="apache@clubshop.com"/>
                <input name="required" type="hidden" value="VIP SPONSOR,Merchants_Name_of_Business,Merchants_Emailaddress_OR_ID"/>
                <input name="sort" type="hidden" value="order:VIP SPONSOR,Merchants_Name_of_Business,MERCHANTS_INITIAL_SET-UP,amount, paypoint,CUSTOM AMOUNT"/>
                <input name="redirect" type="hidden" value="/merchants/setup_approval/index_submitted.xml"/>
                <input name="subject" type="hidden" value="Merchant Transfer Funds Authorization Form"/>
                <input name="sort" type="hidden"/>
                <input name="env_report" type="hidden" value="HTTP_USER_AGENT"/>
             
		<script type="text/javascript">
<xsl:comment>

        function readCookie(cookie_name)
        {
        
        		cookie_name = cookie_name.replace(/\s*/g, "");
        		if(cookie_name=="") return "";
        		cookie_name = new RegExp("^" + cookie_name + "$");
        		var the_cookie_we_are_looking_for = new Array();
                var all_cookies=""+document.cookie;
                var cookies_array = all_cookies.split(";");
                
                for ( var cookie_index in cookies_array )
				{
					the_cookie_we_are_looking_for = cookies_array[cookie_index].split("=");
					the_cookie_we_are_looking_for[0] = the_cookie_we_are_looking_for[0].replace(/\s*/g, "");
					if (the_cookie_we_are_looking_for[0].match(cookie_name)) break;
					the_cookie_we_are_looking_for = new Array();
				} 
                
                return the_cookie_we_are_looking_for ? unescape(the_cookie_we_are_looking_for[1]): "";
                
		}

	document.write("&lt;input type='hidden' name='VIP SPONSOR' size='25' value='" + readCookie('id') + "'/&gt;");

//</xsl:comment>
</script>
<br/>
                <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
                 <tr> 
                     <tr> 
                  <td bgcolor="#FFFFFF"><div align="left"><b><xsl:value-of select="//lang_blocks/merchant_full_name"/></b></div></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="merchants_full_name" size="50"/></td>
                </tr>
                  <td bgcolor="#FFFFFF"><div align="left"><b><xsl:value-of select="//lang_blocks/name_of_business"/></b></div></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="Merchants_Name_of_Business" size="50"/></td>
                </tr>
                
                
                   <tr> 
                  <td bgcolor="#FFFFFF"><div align="left"><b><xsl:value-of select="//lang_blocks/merchant_emailaddress"/></b></div></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="Merchants_Emailaddress_OR_ID" size="50"/></td>
                </tr>
                
                </table>
        <br/>
                
                
                
                
                <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
	                <tr>
				<td bgcolor="#FFFFFF" colspan="3">
					<div align="left">
						<b><xsl:value-of select="//lang_blocks/setup_heading"/></b>
					</div>
				</td>
			</tr>
			
						<tr>
						
						<td bgcolor="#FFFFFF">Merchant Packages</td>
						<td bgcolor="#FFFFFF">Package Price</td>
						<td bgcolor="#FFFFFF">Pay Points</td>
						
						</tr>
			
			
			
			
			
			
					<tr>
                        <td bgcolor="#FFFFFF" colspan="3"><input type="radio" name="MERCHANTS_INITIAL_SET-UP">
                        <xsl:attribute name="value"><xsl:value-of select="//lang_blocks/none_sent" /></xsl:attribute>                                      </input>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="//lang_blocks/none_sent" />
                                </td>
                                
                               
                        </tr>
			
			
                
                       <tr>
                                <td bgcolor="#FFFFFF">
                                        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
                                                <xsl:attribute name="value"><xsl:value-of select="//lang_blocks/basic_pack" /><xsl:text> </xsl:text>Amount: $19.99  Pay Points $9.99</xsl:attribute>
                                        </input>
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="//lang_blocks/basic_pack" />
                                </td>
                                
                                <td bgcolor="#FFFFFF"><span class="transposeME">$19.99</span></td>
                                <td bgcolor="#FFFFFF"><span class="transposeME">$9.99</span></td>
                        </tr>
<!--<tr>
<td bgcolor="#FFFFFF">
<input type="radio" name="MERCHANTS_INITIAL_SET-UP">
<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/track_merch" /></xsl:attribute>
</input>
<xsl:text> </xsl:text>
<xsl:value-of select="//lang_blocks/track_merch" />
</td>
</tr>-->
               		<tr>
                        	<td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/bronze_package" /><xsl:text> </xsl:text>Amount: $39.99  Pay Points $19.99</xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/bronze_package" />
                        	</td>
                        	
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$39.99</span></td>
                                <td bgcolor="#FFFFFF"><span class="transposeME">$19.99</span></td>
	                </tr>
	                
	                	<!--<tr>
                        	<td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/bronze_package_m" /></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/bronze_package_m" />
                        	</td>
                        	
                        
	                </tr>-->
	                
	                
	                
	                
	                
	                
	                
	                
	                
	                
        	       <tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/silver_package" /><xsl:text> </xsl:text>Amount: $79.00  Pay Points $39.50</xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/silver_package" />
                        	</td>
                        	
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$79.00</span></td>
                                <td bgcolor="#FFFFFF"><span class="transposeME">$39.50</span></td>
	                </tr>
	                
	                	      <!-- <tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/silver_package_m" /></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/silver_package_m" />
                        	</td>
                        	
	                </tr>-->
	                
	                
	                
	                   <tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/gold_package" /><xsl:text> </xsl:text>Amount: $149.00  Pay Points $74.50</xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/gold_package" />
                        	</td>
                        	
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$149.00</span></td>
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$74.50</span></td>
	                </tr>
	                
	                
	                     <!--<tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/gold_package_m" /></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/gold_package_m" />
                        	</td>
	                </tr>-->
	                
	                
	                
	                <tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/platinum_package" /><xsl:text> </xsl:text>Amount: $299.00  Pay Points $149.50</xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/platinum_package" />
                        	</td>
                        	
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$299.00</span></td>
                        	<td bgcolor="#FFFFFF"><span class="transposeME">$149.50</span></td>
	                </tr>
	                
	                
	                     <!--<tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/platinum_package_m" /></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/platinum_package_m" />
                        	</td>
	                </tr>-->
	                
	                       <!--<tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="radio" name="MERCHANTS_INITIAL_SET-UP">
						<xsl:attribute name="value"><xsl:value-of select="//lang_blocks/merchant_self" /></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/merchant_self" />
                        	</td>
	                </tr>-->
	                
	                
	                <!--<tr>
                	        <td bgcolor="#FFFFFF">
                        	        <input type="text_box" name="CUSTOM AMOUNT">
						<xsl:attribute name="value"></xsl:attribute>
	                                </input>
        	                        <xsl:text> </xsl:text>
                	                <xsl:value-of select="//lang_blocks/fill_in_amount" />
                        	</td>
	                </tr>-->
	                
	                
	                
	                
        	       

		</table>
                <br/>

	

                
                <div align="center">
                      <input type="submit" name="Submit" value="Submit"/><xsl:text> </xsl:text><xsl:text> </xsl:text>
                      <input type="reset" name="Reset" value="Reset"/>
                    </div>
               
                </form>
                <br/>
                <p><b><xsl:value-of select="//lang_blocks/p9" /></b></p>
                
       
			
 
   

              
</td>
            <td colspan="2" style="background-image:url(/images/bg/bg_right.png)"></td>
          </tr>
          <tr>
            <td colspan="12"><img src="/images/general/clubshop_rewards_merchant_page_16.png" width="1000" height="62" alt="" /></td>
          </tr>
          <tr>

            <td height="134" colspan="12" style="background-image:url(/images/general/footerbar.png)"></td>
          </tr>
          <tr>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="14" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="146" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="142" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="119" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="90" height="1" alt="" /></td>

            <td><img src="/images/general/spacer.gif" width="38" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="128" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="133" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="109" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="13" height="1" alt="" /></td>
            <td><img src="/images/general/spacer.gif" width="34" height="1" alt="" /></td>
          </tr>
        </table></td>
      </tr>

      
    </table>
    
</body>
</html>

	</xsl:template>

</xsl:stylesheet>
