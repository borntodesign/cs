<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
<xsl:template match="/">	
<html>
<head>

<title><xsl:value-of select="//lang_blocks/title" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body
{
	background-color: white;
	font-family: sans-serif;
	font-size: 80%;
}
th 
{
	text-align: left;
	padding: 0.4em 1em;
	border-left: 1px dotted silver;
	vertical-align: top;
	background-color: #eee;
}
td
{
	padding: 0.4em;
	padding-right: 1em;
	vertical-align: top;
	font-size: 90%;
	border-width: 1px;
	border-color: silver;
	border-style: solid none solid dotted;
}
td.pn
{
	border-left-style: solid;
}
dd
{
	margin-bottom: 0.4em;
}
dl{ margin-left:1em; }
</style>
</head>
<body>
<h4><xsl:value-of select="//lang_blocks/title" /></h4>
<p><xsl:value-of select="//lang_blocks/baseurl" />: https://www.clubshop.com/IMG<br />
&lt;img src="<b>https://www.clubshop.com/IMG?[param/values list]</b>" height="1" width="1" alt="" /&gt;</p>
<div style="font-weight: bold; margin: 0.5em 0;"><xsl:value-of select="//recog" /></div>
<table style="border-collapse: collapse;">

<tr><th style="border-left-style: solid"><xsl:value-of select="//name" /></th>
<th><xsl:value-of select="//required" /></th>
<th><xsl:value-of select="//type" /></th>
<th><xsl:value-of select="//max_length" /></th>
<th><xsl:value-of select="//descr" /></th></tr>

<xsl:for-each select = "//parms/*">
<tr><td class="pn"><xsl:value-of select="name()" /></td>
<td>
<xsl:choose>
	<xsl:when test="@required = 1"><xsl:value-of select="//yes" /></xsl:when>
	<xsl:when test="@required = 0"><xsl:value-of select="//no" /></xsl:when>
	<xsl:otherwise>
	 	<xsl:value-of select="//conditional" />
	</xsl:otherwise>
</xsl:choose>
</td>
<td><xsl:value-of select="@type" /></td>
<td><xsl:value-of select="@max" /></td>
<td><xsl:value-of select="." /></td></tr>
</xsl:for-each>
</table>
<br />
<xsl:value-of select="//notez" />:<ul style="margin-top: 0.1em">

<xsl:for-each select = "//notes"><li><xsl:value-of select="." /></li></xsl:for-each>
</ul>
<div><xsl:value-of select="//ex" />:
<dl>
<dt><xsl:value-of select="//dt/basic" />:</dt>
<dd>https://www.clubshop.com/IMG?<b>vn=12345;amt=50.69</b></dd>
<dt><xsl:value-of select="//dt/basic_with_cur" />:</dt>
<dd>https://www.clubshop.com/IMG?<b>vn=12345;amt=50.69;cur=EUR</b></dd>
<dt><xsl:value-of select="//dt/basic_with_cid" />:</dt>
<dd>https://www.clubshop.com/IMG?<b>vn=12345;amt=50.69;cid=3412972</b></dd>
</dl>
</div>
</body></html>
</xsl:template>
</xsl:stylesheet>