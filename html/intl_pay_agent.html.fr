﻿?<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html><head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

            <title>Manuel d'entreprise</title>



     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />

    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />

    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>

    <script src="http://www.clubshop.com/js/partner/app.js"></script>

    <script src="http://www.clubshop.com/js/partner/flash.js"></script>

    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->







<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>



</head>



<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<h4 class="topTitle"></h4>
<hr />
<span class="style28">Programme d’Agent Payant international</span> 
<hr />
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<p>Une des difficultés dans la construction d'une organisation internationale de membres, Partners, est le coût élevé et la vitesse lente pour le paiement des commissions et le rachat de ClubCash. Notre programme d'Agent Payant International autorise et permet aux Partners - qui vivent dans les pays où les moyens de paiement via Paypal ou Western Union ne sont pas disponibles pour envoyer des fonds ou pour recevoir des commissions - de surmonter ce problème en agissant en tant qu'"Agent Payant".</p>
<p><span class="style24">Agents payants de remboursement/rachat de ClubCash</span></p>
<p>Chaque VIP qui vit en dehors des États-Unis ou du Canada peut devoir avoir à agir en tant qu’agent payant de remboursement de ClubCash, si un membre ClubShop Rewards de leur organisation décide de demander le remboursement de leurs ClubCash par "Virement bancaire". Lorsque cela se produit, le ClubShop Rewards déposera le montant de la demande de rachat sur le compte Club du VIP et ce VIP sera notifié par Email de la somme qui doit être déposée sur le compte bancaire du membre qui demande le remboursement, ainsi que de leurs coordonnées bancaires.</p>
<p>Ce montant qui doit être déposé sera déduit des frais de rachat applicables, frais qui seront conservé par le VIP agent payant pour aider à compenser les frais engagés pour les décaissements.</p>
<p>Exemple : Un membre demande de racheter 20 Euros dans ClubCash avec 2 euros de frais de rachat. Les 20 Euros seront ensuite converti dans son équivalence en USD et seront déposés sur le compte club du VIP Agent Payant, avec la notification que l’agent payant doit déposer 18 Euros (20-2 = 18) sur le compte bancaire du membre.</p>
<p>Les agents Payants VIPs ont 72 heures pour faire le dépôt, après avoir reçu l'Email de notification et d'information.</p>
<p><span class="style24">Les commissions d'Agents Payants </span></p>
<p>Les Commissions d'Agents Payants sont envoyées par virement bancaire ou par d'autres moyens électroniques, les commissions d'autres Partners qui peuvent vivre dans le même pays. À leur tour, ils transmettent aux membres Partners participants leurs paiements dans leur devise locale. Si plus d'un agent payant existe dans la même région, alors l'agent de niveau inférieur ne sera responsable que pour le paiement de leurs abonnés en aval (ligne inférieure).</p>
<p>Ce montant total des décaissements est envoyé électroniquement chaque mois, précédé par les informations de paiement et des données de conversion pour tous les partners à payer. Les Agents payants sont payés l'équivalent d'une taxe de 3,00$ par paiement de partner pour l’aider à compenser les frais engagés pour les décaissements. Les paiements aux Partners effectués par les agents payant doivent être faits dans les 72 heures suivant la réception des fonds.</p>
<p>Cela permet d’effectuer les paiements rapidement et dans la même devise pour les membres ClubShop Rewards applicables !</p>
<p>In order for Partners to be paid in their local currency by a Pay Agent, they have to submit a disbursement request to their assigned Pay Agent at their ClubAccount. </p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#ffffff"><strong><a href="https://www.clubshop.com/cgi/comm_prefs.cgi">Préférence de Commission </a></strong></td>

<td align="center" bgcolor="#ffffff"><a href="http://www.clubshop.com/pay_agent_app.xml">Candidature d'Agents Payants de Commissions</a></td>
<td align="center" bgcolor="#ffffff"><a href="http://www.clubshop.com/pay_agent_list.xml ">Agents Payants approuvés</a></td>
</tr>
</tbody>
</table>
<br /><br /><br /></div>
</div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2013 <!-- Get Current Year -->ClubShop Rewards, Tous droits rservés.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>

</html>