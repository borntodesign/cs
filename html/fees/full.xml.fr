﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="full.xsl"?>

<!-- Because we have to have the processing instruction (the stylesheet definition) in this document, it cannot be placed into the translation queue -->

<lang_blocks>
<you_will_be_paying_2 new="2012-09-01">Le montant de souscription initiale montré ci-dessous paie pour votre utilisation de l'option GPS que vous avez sélectionné pour le mois courant et le prochain mois.</you_will_be_paying_2>
<your_next_2 new="2012-09-01">L'abonnement mensuel n'est pas exigible avant le premier jour du mois suivant, à moins que vous vous qualifiez pour la Garantie de Commission Minimum. Dans ce cas, votre abonnement mensuel sera automatiquement déduit de vos revenus.</your_next_2>

<you_will_be_paying_3 new="1/15/14">Les Points Payants indiqués pour les frais initiaux sont crédités partiellement pour le mois d'inscription et partiellement pour le mois suivant. Pour déterminer les Points Payants crédités pour le mois d'inscription, il faut soustraire la valeur des Points Payants des frais mensuels de la valeur initiale des Points Payants des frais initiaux indiqués.</you_will_be_paying_3> 
<you_will_be_paying_4 new="1/15/14">Par exemple, si la valeur des Points Payants des frais initiaux est de 42 Points Payants et si la valeur des Points Payants des frais mensuels est de 19 Points Payants, alors 23 Points Payants seront crédités pour le mois d'inscription et 19 Points Payants seront crédités le mois suivant.</you_will_be_paying_4>



  <annual>Renouvellement annuel</annual>
  <ccode>Code d'Unité Monétaire</ccode>
  <country>Pays</country>
  <fee1>Inscription initiale</fee1>
  <fee3>Renouvellement</fee3>
  <inst1>Cliquez sur le Pays en tête pour élargir la liste</inst1>
  <inst2>Cliquez sur un Code Pays pour ne montrer que cette ligne</inst2>
  <monthly>Mensuel</monthly>
  <p0>Liste Complète des frais</p0>
  <p1>Voici les frais présentés pour chaque pays et l'unité monétaire internationale</p1>
  <p2>Note: Les frais payés en utilisant le compte Club glocal Income seront en Dollars Américains, d'après la valeur journalière de votre unité monétaire locale vis-à-vis du dollar.</p2>
  <point_values>Valeurs de Points Payant</point_values>
  <subs_fee>Frais de Souscription</subs_fee>
  <titles>
    <full>Liste des valeurs de Points Payants et des frais d'abonnement GPS</full>
  </titles>
  <vip_monthly>mensuel Partner</vip_monthly>
<click_country new="2012-09-01">Cliquez sur le Pays en haut pour dérouler la liste</click_country>
<click_cc new="2012-09-01">Cliquez sur un Code Pays de montrer uniquement cette ligne</click_cc>
</lang_blocks>
