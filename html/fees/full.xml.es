<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="full.xsl"?>

<!-- Because we have to have the processing instruction (the stylesheet definition) in this document, it cannot be placed into the translation queue -->

<lang_blocks>
<you_will_be_paying_2 new="2012-09-01">The Initial Subscription amount shown below pays for your use of the GPS Option you select for the current month and for next month.</you_will_be_paying_2>

<you_will_be_paying_3 new="1/15/14">The Pay Points shown for Initial Fees are credited partially for the month of enrollment and partially for the following month. To determine the Pay Points credited for the month of enrollment, subtract the Monthly Fee Pay Points value from the Initial Fee Pay Points value shown.</you_will_be_paying_3> 
<you_will_be_paying_4 new="1/15/14">For example, if the Initial Fee Pay Points value is 42 Pay Points and the Monthly Fee Pay Points value is 19 Pay Points, then 23 Pay Points will be credited for the month of enrollment and 19 Pay Points will be credited the following month. </you_will_be_paying_4>



<your_next_2 new="2012-09-01">The Monthly Subscription is not due until the first of the the following month, unless you qualify for the Minimum Commission Guarantee, in which case your Monthly Subscription will automatically be deducted from your earnings.</your_next_2>
<inst1>Click the Country heading to expand the list</inst1>
<inst2>Click a Country Code to show only that line</inst2>
  <annual>Annual</annual>
  <ccode>Código de Moneda</ccode>
  <country>País</country>
  <fee1>Inicial</fee1>
  <fee3>Renuevo</fee3>
  <monthly>Mensual</monthly>
  <p0>Lista completa de cuotas</p0>
  <p1>Estas son  las cuotas por cada país y en moneda internacional.</p1>
  <p2>Nota: Las cuotas que se pagan utilizando el ClubAccount serán efectuadas en dólares estadounidenses en base al tipo de cambio diario con cada moneda local.</p2>
  <point_values>Pay Point Values</point_values>
  <subs_fee>cuotas de suscripción</subs_fee>
  <titles>
    <full>GPS Subscription Prices and Pay Point Values List</full>
  </titles>
<point_values>Pay Point Values</point_values>
<subs_fee>Subscription Prices</subs_fee>
<click_country new="2012-09-01">Click the Country heading to expand the list</click_country>
<click_cc new="2012-09-01">Click a Country Code to show only that line</click_cc>
</lang_blocks>
