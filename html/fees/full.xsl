<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
<html><head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />


<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />

<!--link rel="stylesheet" type="text/css" href="/css/appx-tip.css" /-->

<title><xsl:value-of select="/lang_blocks/titles/full"/></title>
<!-- Included CSS Files -->
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/foundation.css" />
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/app.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script>
<script type="text/javascript" src="/js/jquery-min.js"></script>
<script type="text/javascript" src="/js/fees.js"></script>
<!--[if lt IE 9]>
		<link rel="stylesheet" href="https://www.clubshop.com/css/partner/ie.css" />
	<![endif]-->
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<style type="text/css">
table tbody tr:nth-child(2n)
{
    background: none repeat scroll 0 0 #EEE;
}

th {
    border: medium none;
    color: #333333;
    padding: 9px 10px;
    vertical-align: top;
    text-align: left;
    font-size: 12px;
}

th.cme {
	text-align:center;
	font-weight:bold;
}

#instructions {
	background-color:#ffe;
	font-size: 80%;
	line-height: 12px;
	padding: 0.5em;
}

tr.yy
{
	border-bottom: 1px dotted silver;
}

</style>	
</head>

<body style="background:#001A53;">

<!-- Top Blue Header Container -->
	<div class="container blue">
		
		<div class="row">		
			<div class="six columns">				
				<img width="288" height="84" style="margin-top:5px;" src="https://www.clubshop.com/images/partner/cs_logo.jpg" alt="" />
			</div>
			
			<div class="six columns">				
				<!--Extra Space Future Space -->				
			</div>			
		</div>		
	</div>	
<!--End Top Blue Header Container -->

<!--Content Container -->			
	<div class="container" style="background:#FFF;">	
		<div class="row" style="max-width:inherit;">	         
		<div class="twelve columns"> <!--Main Content -->
                 <br/>

<!-- CONTENT STARTS -->
<table style="margin: 0 auto; border:0;"><tr><td>
				<h4><xsl:value-of select="/lang_blocks/titles/full"/></h4>
				
				<p id="fulllistp"><b><xsl:value-of select="/lang_blocks/p1"/></b></p>
				
				<p><xsl:value-of select="/lang_blocks/p2"/></p>
				
				<p style="font-size:smaller"><xsl:value-of select="/lang_blocks/you_will_be_paying_2"/><br /><br /><xsl:value-of select="/lang_blocks/you_will_be_paying_3"/><br/><xsl:value-of select="/lang_blocks/you_will_be_paying_4"/><br/><br /><xsl:value-of select="/lang_blocks/your_next_2"/></p>

<table id="mainTbl">
<colgroup span="2"></colgroup><!-- country and currency columns -->
<colgroup><col style="background-color: #EEEEEE;"></col></colgroup><!-- the row label column... Price / PP Value -->

<colgroup><col span="2" style="background-color: #EFFFEF;"></col></colgroup><!-- BASIC -->

<colgroup><col span="2" style="background-color: #EEEEEE;"></col></colgroup><!-- BASIC PLUS -->

<colgroup><col span="1" style="background-color: #EFFFEF;"></col></colgroup><!-- PRO -->

<colgroup><col span="1" style="background-color: #EEEEEE;"></col></colgroup><!-- PRO PLUS -->

<colgroup><col span="1" style="background-color: #EFFFEF;"></col></colgroup><!-- PREMIER -->

<colgroup><col span="1" style="background-color: #EEEEEE;"></col></colgroup><!-- PREMIER PLUS -->

<colgroup><col style="background-color: #EFFFEF;"></col></colgroup><!-- Annual Renewal -->

<tbody>
<tr class="b">
<td colspan="2" id="instructions"><b><xsl:value-of select="/lang_blocks/click_country"/></b><br />
<xsl:value-of select="/lang_blocks/click_cc"/></td>
<td></td>
<th colspan="2" class="cme">BASIC</th>
<th colspan="2" class="cme">BASIC PLUS</th>
<th colspan="1" class="cme">PRO</th>
<th colspan="1" class="cme">PRO PLUS</th>
<th colspan="1" class="cme">PREMIER</th>
<th colspan="1" class="cme">PREMIER PLUS</th>
<th class="cme"><xsl:value-of select="/lang_blocks/annual"/></th>
</tr>

<tr class="top">
	<th><a href="#" id="showAll"><xsl:value-of select="/lang_blocks/country"/></a></th>
    <th><xsl:value-of select="/lang_blocks/ccode"/></th>
    <td></td>
	<th class="im"><xsl:value-of select="/lang_blocks/initial"/></th>
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<th class="im"><xsl:value-of select="/lang_blocks/initial"/></th>
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<!-- th class="im"><xsl:value-of select="/lang_blocks/initial"/></th-->
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<!-- th class="im"><xsl:value-of select="/lang_blocks/initial"/></th-->
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<!-- th class="im"><xsl:value-of select="/lang_blocks/initial"/></th-->
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<!-- th class="im"><xsl:value-of select="/lang_blocks/initial"/></th-->
	<th class="im"><xsl:value-of select="/lang_blocks/monthly"/></th>
	<td></td>
</tr>

</tbody>
<tbody id="mainTblData">
<xsl:for-each select="document('pricing.xml')/pricing/item">

<xsl:variable name = "rc">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">a</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<tr>
<xsl:attribute name="id">_<xsl:value-of select="country" /></xsl:attribute>
<xsl:attribute name="class"><xsl:value-of select="$rc" /></xsl:attribute>

<td rowspan="2"><a href="#" class="jme"><xsl:value-of select="country" /></a></td>
<td rowspan="2"><xsl:value-of select="currency_code" /> - <xsl:value-of select="currency_name" /></td>
<td><xsl:value-of select="/lang_blocks/subs_fee"/></td>

<td><xsl:value-of select="format-number(type2_fee + type40_fee + type41_fee, '#.00')" /></td><!-- BASIC initial ... -->
<td><xsl:value-of select="format-number(type41_fee, '#.00')" /></td>

<td><xsl:value-of select="format-number(type2_fee + type48_fee + type49_fee, '#.00')" /></td><!-- BASIC PLUS initial ... -->
<td><xsl:value-of select="format-number(type49_fee, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_fee + type1_fee + type3_fee, '#.00')" /></td> PRO initial ... -->
<td><xsl:value-of select="format-number(type3_fee, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_fee + type42_fee + type43_fee, '#.00')" /></td> PRO PLUS initial ... -->
<td><xsl:value-of select="format-number(type43_fee, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_fee + type44_fee + type45_fee, '#.00')" /></td> PREMIER initial ... -->
<td><xsl:value-of select="format-number(type45_fee, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_fee + type46_fee + type47_fee, '#.00')" /></td> PREMIER PLUS initial ... -->
<td><xsl:value-of select="format-number(type47_fee, '#.00')" /></td>

<td><xsl:value-of select="format-number(type4_fee, '#.00')" /></td><!-- Annual Renewal -->
</tr>

<tr>
<xsl:attribute name="id">__<xsl:value-of select="country" /></xsl:attribute>
<xsl:attribute name="class">yy <xsl:value-of select="$rc" /></xsl:attribute>

<td><xsl:value-of select="/lang_blocks/point_values"/></td>
<td><xsl:value-of select="format-number(type2_pp + type40_pp + type41_pp, '#.00')" /></td><!-- BASIC initial ... -->
<td><xsl:value-of select="format-number(type41_pp, '#.00')" /></td>

<td><xsl:value-of select="format-number(type2_pp + type48_pp + type49_pp, '#.00')" /></td><!-- BASIC PLUS initial ... -->
<td><xsl:value-of select="format-number(type49_pp, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_pp + type1_pp + type3_pp, '#.00')" /></td> PRO initial ... -->
<td><xsl:value-of select="format-number(type3_pp, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_pp + type42_pp + type43_pp, '#.00')" /></td> PRO PLUS initial ... -->
<td><xsl:value-of select="format-number(type43_pp, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_pp + type44_pp + type45_pp, '#.00')" /></td> PREMIER initial ... -->
<td><xsl:value-of select="format-number(type45_pp, '#.00')" /></td>

<!--<td><xsl:value-of select="format-number(type2_pp + type46_pp + type46_pp, '#.00')" /></td> PREMIER PLUS initial ... -->
<td><xsl:value-of select="format-number(type47_pp, '#.00')" /></td>

<td><xsl:value-of select="format-number(type4_pp, '#.00')" /></td><!-- Annual Renewal -->
</tr>
</xsl:for-each>
</tbody>
</table>

</td></tr></table>

</div>
		</div>
	</div>
	
<div class="container blue">
		
		<div class="row "><div class="twelve columns"><div class="push"></div></div></div>
	
		<div class="row ">
			<div class="twelve columns centered">			
			
				<div id="footer">
					<p>Copyright &#169; 1997-
					
						<!-- Get Current Year -->
						<script type="text/javascript"><xsl:comment>
							var dteNow = new Date();
							var intYear = dteNow.getFullYear();
							document.write(intYear);
						</xsl:comment></script>
						
						 Proprofit Worldwide Ltd.,  All Rights Reserved.
					</p>
				</div>				
			</div>
		</div>
	
	</div>	<!-- container -->
<!--End Footer Container -->	


</body>
</html>
</xsl:template>

<xsl:template match="/pricing/item">
<tr><xsl:copy-of select="*" /></tr>
</xsl:template>
</xsl:stylesheet>
