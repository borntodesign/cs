<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="full.xsl"?>

<!-- Because we have to have the processing instruction (the stylesheet definition) in this document, it cannot be placed into the translation queue -->
<lang_blocks>
<you_will_be_paying_2 new="2012-09-01">The Initial Subscription amount shown below pays for your use of the GPS Option you select for the current month and for next month.</you_will_be_paying_2>
<you_will_be_paying_3 new="1/15/14">The Pay Points shown for Initial Fees are credited partially for the month of enrollment and partially for the following month. To determine the Pay Points credited for the month of enrollment, subtract the Monthly Fee Pay Points value from the Initial Fee Pay Points value shown.</you_will_be_paying_3> 
<you_will_be_paying_4 new="1/15/14">For example, if the Initial Fee Pay Points value is 42 Pay Points and the Monthly Fee Pay Points value is 19 Pay Points, then 23 Pay Points will be credited for the month of enrollment and 19 Pay Points will be credited the following month. </you_will_be_paying_4>

<your_next_2 new="2012-09-01">The Monthly Subscription is not due until the first of the the following month, unless you qualify for the Minimum Commission Guarantee, in which case your Monthly Subscription will automatically be deducted from your earnings.</your_next_2>
  <annual>Jaarlijks</annual>
  <ccode>Munteenheid</ccode>
  <country>Land</country>
  <fee1>Initieel</fee1>
  <fee3>Vernieuwen</fee3>
  <inst1>Klik op het landoverzicht om de volledige lijst uit te vouwen</inst1>
  <inst2>Klik op een landcode om alleen dat land te laten zien</inst2>
  <monthly>Maandelijks</monthly>
  <p0>Bijdrage Lidmaatschap</p0>
  <p1>Hieronder de bijdragen voor ieder land en internationale munteenheid.</p1>
  <p2>Opmerking: Bijdragen overgemaakt via uw ClubShop Rewards ClubAccount dienen te gebeuren in USD en volgens de actuele wisselkoers tegenover uw lokale munteenheid.</p2>
  <point_values>Pay punten waarde</point_values>
  <subs_fee>Abonnementsbijdragen</subs_fee>
  <titles>
    <full>GPS Abonnement Bijdragen en Pay punten waarden lijst</full>
  </titles>
  <vip_monthly>Partner Maandelijks</vip_monthly>
<click_country new="2012-09-01">Click the Country heading to expand the list</click_country>
<click_cc new="2012-09-01">Click a Country Code to show only that line</click_cc>
</lang_blocks>
