<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop Non-Profit - Gratis Fundraiser</title>

<!--#include virtual="linksnscripts.html" -->

</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="1000">
<tbody>
<tr>
<td height="108" style="background-image:url(http://www.clubshop.com/cs/images/design/clubshop_01.png)" valign="bottom"><!--#include virtual="_includes/pages/nav/dropmenu.html" --></td>
</tr>
<tr>
<td><img height="24" src="http://www.clubshop.com/cs/images/design/clubshop_02.png" width="1000" /></td>
</tr>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
<tbody>
<tr>
<td style="background-image:url(images/bg/bg_left.png)" width="47"> </td>
<td bgcolor="#ffffff" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" valign="top">
<p><span class="Orange_Large_11">FUNDRAISING OPTIE #1</span> <br /><br /><span class="Blue_Large_Bolder">Gratis Fundraiser</span> <br /><span class="Blue_Large_Bold"><img align="right" alt="Shop Online" height="324" src="images/gallery_1.png" width="340" /> </span><br />Plaats een ClubShop banner advertentie op uw website en laat uw leden/supporters Gratis ClubShop leden worden. Wanneer een door u verwezen lid een aankoop doet in de online ClubShop Mall, zal uw non-profit organisatie een charitatieve donatie van ons krijgen. <br /><br />Uw leden verdienen ook cashback Reward punten op al hun ClubShop aankopen. Krijgt uw lid een ClubShop Rewards kaart en gaat daarmee winkelen bij een deelnemend offline handelaar, dan verdient uw non-profit organisatie een charitatieve donatie! <br /><br />De charitatieve donaties op de aankopen van uw leden zullen variëren, maar bedragen gemiddeld ongeveer 2% en kunnen oplopen tot 6% van de aankoop. <br /><br />Als het maandelijkse aankoopbedrag van een lid die u verwees gemiddeld $100 bedraagt, dan wordt ongeveer $2 geschonken als zijnde charitatieve donatie. Hieronder een voorbeeld van hoeveel deze donaties kunnen zijn: <br /><br /><span class="Blue_Large_Bold">500 verwezen leden X $2 elk = $1.000 per maand of $12,000 per jaar</span> <br /><br /><img height="9" src="images/icon_bullet_orannge.png" width="39" /><a class="nav_orange" href="banner_generator.shtml">Banner Generator</a><br /><br />Kopieer en plak de banner in uw webpagina. Het ID nummer van uw organisatie is ingebed in de link, dus wanneer één van uw supporters winkelt in de ClubShop Mall via uw link, dan wordt uw organisatie daarvoor beloond.</p>
</td>
</tr>
<tr>
<td align="left" height="20"></td>
</tr>
</tbody>
</table>
</td>
<td style="background-image:url(images/bg/bg_right.png)" width="47"> </td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td><img height="65" src="http://www.clubshop.com/cs/images/design/clubshop_06.png" width="1000" /></td>
</tr>
<tr>
<td align="left" height="127" style="background-image:url(http://www.clubshop.com/cs/images/design/clubshop_07.png)" valign="top"><!--#include virtual="_includes/pages/general/social_network.html" --></td>
</tr>
<tr>
<td align="center" class="Footer_Grey_Regular"><!--#include virtual="_includes/pages/general/footer_index.html" --></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>