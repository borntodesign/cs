<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>ClubShop Non-Profit - Kaart Fundraiser</title>



<!--#include virtual="linksnscripts.html" -->



</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="1000">
<tbody>
<tr>
<td height="108" style="background-image:url(http://www.clubshop.com/cs/images/design/clubshop_01.png)" valign="bottom"><!--#include virtual="_includes/pages/nav/dropmenu.html" --></td>
</tr>
<tr>
<td><img height="24" src="http://www.clubshop.com/cs/images/design/clubshop_02.png" width="1000" /></td>
</tr>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
<tbody>
<tr>
<td style="background-image:url(images/bg/bg_left.png)" width="47"></td>
<td bgcolor="#ffffff" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" valign="top">
<p><span class="Orange_Large_11">FUNDRAISING  OPTIE #2</span> <br /> <br /> <span class="Blue_Large_Bolder">Kaart Fundraiser</span> <br /> <span class="Blue_Large_Bold"> </span><br /> Koop ClubShop Rewards kaarten aan de groothandelsprijs en bijgevolg kunt u ze verkopen met nog meer rendement. Ook, wanneer iemand een aankoop doet bij een van onze offline Tracked ClubShop Rewards ondernemers of in onze ClubShop Online Mall, dan krijgt uw non-profit organisatie van ons een charitatieve donatie en verdient de kaarthouder cashback Reward punten, te verzilveren naar gelieven.  <br /> <br /> Dit is een win-win situatie! We zijn de enige fundraiser waar u charitatieve donaties kunt verdienen uit online en offline aankopen bij ClubShop Rewards winkeliers wereldwijd! Uw non-profit organisatie zal niet enkel fondsen werven met ons programma, U zult ook mensen helpen geld besparen terwijl ze winkelen, zowel online als offline.   <br /><br /> <span class="Blue_Large_Bold"><img align="left" alt="ClubShop Rewards Card" height="257" src="/cs/images/design/clubshop_shopper_landing_page.png" width="224" /> </span> Dit is de snelste manier om fondsen te werven voor uw non-profit organisatie! Het aantal kaarten dat u zich aanschaft en de verkoopprijs zullen bepalen hoeveel winst u zult maken. Hieronder enkele voorbeelden:<br /><br /></p>
<table bgcolor="#ffcc00" border="0" cellpadding="6" cellspacing="1" width="65%">
<tbody>
<tr align="center">
<td bgcolor="#ffefca" width="29%"><span class="Header_Dark_Grey_Bold"># Kaarten aangekocht</span></td>
<td bgcolor="#ffefca" width="26%"><span class="Header_Dark_Grey_Bold">Aankoopprijs</span></td>
<td bgcolor="#ffefca" width="23%"><span class="Header_Dark_Grey_Bold">Verkocht voor</span></td>
<td bgcolor="#ffefca" width="22%"><span class="Header_Dark_Grey_Bold">Winst</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffffff"><span class="Orange_Large_11">10</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$60</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$100</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$40</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffefca"><span class="Orange_Large_11">50</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$250</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$500</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$250</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffffff"><span class="Orange_Large_11">100</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$405</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$1,000</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$595</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffefca"><span class="Orange_Large_11">250</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$850</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$2,500</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$1,650</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffffff"><span class="Orange_Large_11">500</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$1,400</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$5,000</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$3,600</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffefca"><span class="Orange_Large_11">1000</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$2,200</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$10,000</span></td>
<td bgcolor="#ffefca"><span class="Orange_Large_11">$7,800</span></td>
</tr>
<tr align="center">
<td bgcolor="#ffffff"><span class="Orange_Large_11">5000</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$8,000</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$50,000</span></td>
<td bgcolor="#ffffff"><span class="Orange_Large_11">$42,000</span></td>
</tr>
</tbody>
</table>
<p><br /><br /> Hoewel de ClubShop Rewards kaart hetzelfde prijskaartje heeft als de andere kortingskaarten die gebruikt worden bij fondsenwerving, geen enkele andere kortingskaart kan aan de klant de unieke en blijvende waarde bieden die onze ClubShop Rewards kaart biedt. Hiermee heeft uw organisatie een "kortingskaart" die waardevoller is dan eender welke kortingskaart, u kunt ze verkopen en fondsen werven.   <br /> <br /> <img height="9" src="images/icon_bullet_orannge.png" width="39" /> We bieden meer potentiële plaatsen waar de kaart kan gebruikt worden dan er op de achterkant kant gedrukt worden. Een gewone kortingskaart is gelimiteerd tot 10 - 20 lokale bedrijven waar men korting kan ontvangen.  <br /> <img height="9" src="images/icon_bullet_orannge.png" width="39" /> In tegenstelling tot andere kaarten, is onze kaart mondiaal. Onze kaarthouders kunnen dus ook genieten van kortingen wanneer ze reizen, of verhuizen, of buiten hun eigen lokale regio stappen. Geen enkele andere kaart kan dit wereldwijde gebruik bieden.  <br /> <img height="9" src="images/icon_bullet_orannge.png" width="39" /> We bieden de kaarthouder een manier om van korting en cashbacks te genieten bij het online winkelen. Wij bieden de enige kaart, waar we tot op heden weet van hebben, die de kaarthouders offline handelskortingen of cashback voordeel biedt, plus, cashback voordeel bij het online winkelen.</p>
<p><br /> U kunt niet alleen geld verdienen door onze kaart te verkopen, u kunt ook een charitatieve donatie verdienen op de ClubShop aankopen gemaakt door uw leden.  <br /> <br /> Al wat uw organisatie hoeft te doen is “get the word out”! Help mensen cashback Reward punten verdienen op hun aankopen en verdien een percentage van hun ClubShop aankopen als donatie aan uw organisatie.  <br /> <br /> <img height="9" src="images/icon_bullet_orannge.png" width="39" /><a class="nav_orange" href="http://www.clubshop.com/cgi/cart/cart.cgi" target="_blank">ClubShop Rewards kaarten kopen</a></p>
</td>
</tr>
<tr>
<td align="left" height="20"></td>
</tr>
</tbody>
</table>
</td>
<td style="background-image:url(images/bg/bg_right.png)" width="47"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td><img height="65" src="http://www.clubshop.com/cs/images/design/clubshop_06.png" width="1000" /></td>
</tr>
<tr>
<td align="left" height="127" style="background-image:url(http://www.clubshop.com/cs/images/design/clubshop_07.png)" valign="top"><!--#include virtual="_includes/pages/general/social_network.html" --></td>
</tr>
<tr>
<td align="center" class="Footer_Grey_Regular"><!--#include virtual="_includes/pages/general/footer_index.html" --></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>

</html>