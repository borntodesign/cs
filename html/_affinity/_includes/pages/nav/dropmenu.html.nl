
<table border="0" cellpadding="0" cellspacing="0" id="Table_01" width="1000">

<tbody>

<tr>

<td align="center" colspan="8">

<table border="0" cellpadding="0" cellspacing="0" width="1000">

<tbody>

<tr>

<td align="right" height="80" valign="top" width="399"><a href="../../../index.shtml"><img alt="Welcome Page: Club Shop" border="0" height="43" src="https://www.clubshop.com/common/images/design/logo_clubshop_transparent.png" width="390" /></a> <span style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #ffffff"><strong>Affinity Group Account</strong></span></td>

<td align="right" valign="top" width="599"><a class="Nav_Signin_Yellow" href="../../../../general/login.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','../../../images/nav/btn_login_2.png',1)" rel="gb_page_center[540, 240]" title="e-Business Institute: Login"></a>

<table border="0" cellpadding="0" cellspacing="0" width="300">

<tbody>

 <tr>
                <td width="190" align="center">
<ul id="Languages" class="MenuBarHorizontal">
  <li><a class="MenuBarItemSubmenu" href="#">LANGUAGE</a>
      <ul>
        <li><a href="#" onclick="setLangPrefCookie('en');return false;">English</a></li>
        <li><a href="#" onclick="setLangPrefCookie('fr');return false;">Francais</a></li>
        <li><a href="#" onclick="setLangPrefCookie('it');return false;">Italiano</a></li>
        <li><a href="#" onclick="setLangPrefCookie('nl');return false;">Nederlands</a></li>

    </ul>
  </li>
</ul>
</td>
               <td style="padding-left:10px; padding-right: 10px;">
<div id="anchor_my_account" style="">

<a class="Nav_Footer_White" href="." style="margin-right:1em;">My Account</a>
<a class="Nav_Footer_White" href="/cgi-bin/Login.cgi/ag?action=logout">Logout</a>

</div>
</td>

              </tr>

<!--<a href="_includes/pages/general/login.html" mce_href="_includes/pages/general/login.html" title="ClubShop: Account Login" rel="gb_page_center[687, 572]" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/nav/btn_login_2.png',1)"><img src="images/nav/btn_login_1.png" mce_src="images/nav/btn_login_1.png" alt="ACCOUNT LOGIN" name="Image1" width="130" height="11" border="0" id="Image1" /></a></td>

-->

<tr>

<td align="right" width="49"> </td>

</tr>

</tbody>

</table>

</td>

</tr>

</tbody>

</table>

<table border="0" cellpadding="0" cellspacing="0" width="999">

<tbody>

<tr>

<td width="132"> </td>

<td width="819">

<ul class="MenuBarHorizontal" id="Navigation">

<li><a class="MenuBarItemSubmenu" href="#">FONDSENWERVERS</a> 

<ul>

<li><a href="free_fundraiser.shtml">Gratis Fondsenwerver</a></li>

<li><a href="card_fundraiser.shtml">Kaart Fondsenwerver</a></li>

</ul>

</li>

<li><a class="MenuBarItemSubmenu" href="#">RAPPORTEN</a> 

<ul>

<li><a href="members_referred.shtml">Aangesloten Leden</a></li>

<li><a href="referral_income.shtml">Referral Inkomen</a></li>

</ul>

</li>

<li><a class="MenuBarItemSubmenu" href="#">LIDMAATSCHAP</a> 

<ul>

<li><a href="update_profile.shtml">Profiel Bijwerken</a></li>

<li><a href="non_profit_terms.shtml">Voorwaarden Non-profit Organisatie</a></li>

<li><a href="member_signup.shtml">Leden Inschrijven</a></li>

<li><a href="consultant_help.shtml">Contact met Adviseur</a></li>

</ul>

</li>

<li><a class="MenuBarItemSubmenu" href="#">ONLINE WINKELEN</a> 

<ul>

<li><a href="/cgi-bin/wwg" target="_blank">Online Winkelcentrum</a></li>

</ul>

</li>

<li><a class="MenuBarItemSubmenu" href="#">LOCAAL WINKELEN</a> 

<ul>

<li><a href="/cs/rewardsdirectory" target="_blank">Overzicht Ondernemers</a></li>

</ul>

</li>

<li><a class="MenuBarItemSubmenu" href="#">REWARD PUNTEN</a> 

<ul>

<li><a href="transactions.shtml">Transacties</a></li>

<li><a href="reward_point_report.shtml">Maandrapport</a></li>

<li><a href="rewards_center.shtml">Inwissel Centrum</a></li>

</ul>

</li>

</ul>

</td>

</tr>

</tbody>

</table>

</td>

</tr>

</tbody>

</table>

<script type="text/javascript">// <![CDATA[

var Languages = new Spry.Widget.MenuBar("Languages", {imgDown:"_includes/SpryAssets/SpryMenuBarDownHover.gif", imgRight:"_includes/SpryAssets/SpryMenuBarRightHover.gif"});



var Navigation = new Spry.Widget.MenuBar("Navigation", {imgDown:"_includes/SpryAssets/SpryMenuBarDownHover.gif", imgRight:"_includes/SpryAssets/SpryMenuBarRightHover.gif"});

// ]]></script>

