<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" charset=iso-8859-1/>
	
	<title>ClubShop Rewards Affinity Group</title>
	
	<!--#include virtual="linksnscripts.html" -->

</head>

	<body>

		<div id="page-wrap">
			
			<div id="header">
			
				

				

					<div id="logged_in">
					<!--#include virtual="_includes/pages/nav/dropmenu.html" -->
					</div>
			</div><!-- Header END -->		
			
			<div id="headbar"></div>			
			
		
			<div id="main">
			
				<table width="980" border="0" cellspacing="0" cellpadding="0" style="margin:0px auto;">
					<tr>
						<td width="47"> </td>
							<td valign="top" bgcolor="#FFFFFF"> 
				  
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="left"><p class="Blue_Large_Bolder" align="center">Welkom bij uw ClubShop Rewards Affinity Group Business Area </p>
										
											<br/>
											<img src="http://www.clubshop.com/_affinity/images/Mixed-Group.gif" width="291" height="390" align="right" alt="" /> 
											
											<p>
												Het werven van fondsen voor een non-profit organisatie of een charitatieve organisatie te ondersteunen kan moeilijk en tijdrovend zijn. ClubShop maakt het gemakkelijk voor een non-profit organisatie om fondsen te werven snel en op een permanente basis.
											</p>
											
											<br/>
											
											<span class="Blue_Large_Bold">Hoe begin ik met:</span>
											
											<br/>
											<br/>
											
											<img src="images/icon_bullet_orannge.png" width="39" height="9" alt=""/>1. Winkel en Sla bij de ClubShop Mall voor items voor uw organisatie en verdien ClubShop <span style="margin-left:52px;">Rewards Points!</span>
											
											<br/>
											<br/>
						 
											<img src="images/icon_bullet_orannge.png" width="39" height="9" alt=""/>2. Winkelen bij een plaatselijke handelaars, waar u kunt verdienen directe kortingen.
											
											<br/>
											<br/>
								
											<img src="images/icon_bullet_orannge.png" width="39" height="9" alt=""/>3. Bekijk onze Fundraisers link om te zien hoe u kunt beginnen met het werven van fondsen voor uw <span style="margin-left:52px;">organisatie</span>.
											
											<br/>
											<br/>
											
											
											<br>Als u behoefte heeft aan alle hulp met onze fondsenwerving programma's, neem dan contact op met uw ClubShop Rewards Consultant. Indien u om enige reden niet in staat zijn om in contact te komen met uw ClubShop Rewards Consultant, stuur dan een e-mail naar 
												
												<a href"mailto:support1@clubshop.com?Subject=Affinity_Group_Help" class="nav_orange">support1@clubshop.com</a>.
					   
										</td>
									</tr>

									<tr>
									<td height="20" align="left"></td>
									</tr>
									
								</table>

							</td>
							
							<td width="47" ></td>
					</tr>
				</table>			
				
			</div><!-- Main END -->
			
			
			
			<div id="bottom_content"></div>
			
			<!--#include virtual="_includes/pages/general/clubshop_footers.shtml" -->
			<!--#include virtual="_includes/pages/general/clubshop_copyright.shtml" -->
			
		</div><!-- Page Wrap END -->
		
		<!-- Put Google Analytics Code Here -->
	<script type="text/javascript" src="_includes/js/loadminidee.js"></script>
	<script type="text/javascript" src="_includes/js/loginLink.js"></script>
	</body>

</html>
