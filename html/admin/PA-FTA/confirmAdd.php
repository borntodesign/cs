<?php
    require 'mc.php';
    $PageTitle = 'Pay Agent/Fee Transmission Agent: confirm addition';
    include 'dochead.php';?>
<h4>Confirm Addition of:
<?php echo "{$candidate['alias']} - {$candidate['firstname']} {$candidate['lastname']} - {$candidate['country']}"; ?>
</h4>
<form action="edit" method="get" id="addFrm">
Member ID: <input type="text" name="id" value="<?php echo $params['id'];?>"/>
&nbsp;&nbsp;&nbsp;Fee: <input name="fee" type="text" style="width:5em;" value="<?php echo $params['fee'];?>" />
&nbsp;&nbsp;&nbsp;Notes: <input name="notes" type="text" style="width:30em;" value="<?php echo $params['notes'];?>" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="add" />
<input type="hidden" name="confirm" value="1" />
<input type="hidden" name="country_code" value="<?=$candidate['country']?>" />
</form>
</body></html>