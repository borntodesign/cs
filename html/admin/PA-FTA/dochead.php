<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$PageTitle?></title>
<script type="text/javascript" src="/js/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="/css/admin/generic-report.css" />
<style type="text/css">
a.notxd {
	text-decoration:none;
}
a.rvm {
	font-style:italic;
}
.aok {
	color:green;
}
.nok {
	color:red;
}
span.inactive {
	text-decoration:line-through;
	color:#999;
}
table.report th.nob {
	border:none;
}
table.report th.pa {
	background-color:#cfc;
}
table.report th.fta {
	background-color:#ccf;
}
table.report th.empty {
	border:none;
	background-color:white;
}
table.report td.nopa {
	background-color:#bbb;
	text-align:left;
	padding-left: 10em;
	vertical-align:middle;
}
.nowrap {
	white-space: nowrap;
}
table.sr td,th {
	font-size:inherit;
}
table.sr .norb {
	border-right: none;
}
table.sr .nolb {
	border-left: none;
}
table.sr .notb {
	border-top: none;
}
table.sr .nobb {
	border-bottom: none;
}
</style>
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>