<?php
require 'mc.php';
include 'dochead.php';
?>
<p style="width:50%; float:right; font-size:70%; margin-left: 2em;">
Although Pay Agents and Fee Transmission Agents have entirely different responsibilities,
current business policy dictates that the roles be merged into a unified set of privileges and responsibilities.
That being the case, with the exception of existing Fee Transmission Agents who are not Pay Agents, changes made using this and related interfaces, will affect record sets in both systems.
Privileges/responsibilities granted to one role will automatically be granted to the other role.
</p>
<p>
<a href="" onclick="$('#addFrm').toggle(); return false;" class="fpnotes">Add a Pay Agent:</a>
<?php if ($params['id']) echo '<br /><a class="fpnotes" href="' . $_SERVER['PHP_SELF'] .'">Show all records</a>'; ?>
</p>
<form action="confirmAdd" method="get" id="addFrm" style="display:none">
Member ID: <input type="text" name="id" />
Fee: <input name="fee" type="text" style="width:5em;" />&nbsp;&nbsp;&nbsp;Notes: <input name="notes" type="text" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="add" />
</form>

<?php if ($PaFta->result()): ?>
    <table class="report" style="clear: right"><thead>
        <tr><th colspan="3" class="empty"></th>
        <th colspan="4" class="pa">Pay Agent Specifics
            <span class="fpnotes" style="padding-left:10em; color:navy;">
            Active Pay Agents will show up on this list: <a target="_blank" href="/pay_agent_list.php">pay_agent_list.php</a>
            </span>
        </th>
        <th colspan="7" class="fta">Fee Transmission Agent Specifics</th></tr>
        <tr><th><a href="#" title="Click the ID to pull the member's record">ID</a></th>
        <th><a href="#" title="Click the Agent to open the Agent's edit interface">Agent</a></th>
        <th><a href="#" title="Click the flag to toggle active status" onclick="return false">Active?</a></th>
        <th class="pa">Notes</th>
        <th class="pa">Timestamp</th>
        <th class="pa">Operator</th>
        <th class="pa">Countries</th>
        <th class="fta">Notes</th>
        <th class="fta">Timestamp</th>
        <th class="fta">Operator</th>
        <th class="fta">Countries</th>
        <th class="fta">Fee</th>
        <th class="fta"><a href="" title="Authorized to pay fees and transfer funds downline in this country">D</a></th>
        <th class="fta"><a href="" title="Authorized to pay fees and transfer funds crossline in this country">Xl</a></th></tr>
        </thead><tbody>
<?php $trclass = 'a'; ?>
<?php foreach ($PaFta->result() as $item): ?>
<?php
    $auth = $item['active'] ? "<a class=\"aok tauth\" href=\"#\" rel=\"{$item['id']}\">Yes</a>" : "<a class=\"nok tauth\" href=\"#\" rel=\"{$item['id']}\">No</a>";
    $ftaEnum = array();
    if (isset($item['fta'])) {
        $ftaEnum = enumerateFtaCountries($item['fta']);
    }
    $rowSpan = $ftaEnum ? count($ftaEnum) : 1;
?>
            <tr class="<?=$trclass?>">
            <td rowspan="<?=$rowSpan?>"><a target="_blank" href="/cgi/admin/memberinfo.cgi?id=<?=$item['id']?>"><?=$item['alias']?></a></td>
            <td rowspan="<?=$rowSpan?>"><a href="edit?id=<?=$item['id']?>"><?=$item['name']?></a></td>
            <td rowspan="<?=$rowSpan?>" class="alc"><?=$auth?></td>
    <?php if (isset($item['pa'])): ?>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['pa']['notes']) ? $item['pa']['notes'] : ''; ?></td>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['pa']['stamp']) ? $item['pa']['stamp'] : ''; ?></td>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['pa']['operator']) ? $item['pa']['operator'] : ''; ?></td>
            <td rowspan="<?=$rowSpan?>" class="nowrap"><?php echo enumeratePaCountries($item['pa']); ?></td>
    <?php else: ?>
            <td rowspan="<?=$rowSpan?>" colspan="4" class="nopa">Not Pay Agent</td>
    <?php endif; ?>
    <?php if (isset($item['fta'])): ?>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['fta']['notes']) ? $item['fta']['notes'] : ''; ?></td>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['fta']['stamp']) ? $item['fta']['stamp'] : ''; ?></td>
            <td rowspan="<?=$rowSpan?>"><?php echo isset($item['fta']['operator']) ? $item['fta']['operator'] : ''; ?></td>
            <?php if ($ftaEnum) {
                    echo array_shift($ftaEnum);
                } else {
                    echo '<td colspan="4">&nbsp;</td>';
                } ?>
            </tr>
            <?php foreach ($ftaEnum as $row){ echo '<tr>' . $row . '</tr>'; } ?>
    <?php else: ?>
            <td rowspan="<?=$rowSpan?>" colspan="7" class="nopa">Not a Fee Transmission Agent</td>
    <?php endif;?>
<?php $trclass = $trclass == 'a' ? 'b':'a'; ?>
<?php endforeach; ?>
    </tbody></table>
<?php else: ?>
    <h4>The list is empty</h4>
<?php endif; ?>

<form id="toggleauth" method="get" action="" style="display: none; padding: 1em; background-color: #cfc; border: 2px solid grey;">
<input type="hidden" name="id" id="toggleauth_id" value="" />
Notes: <input type="text" name="notes" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="toggleauth" />
</form>
<script type="text/javascript">
$('.tauth').click( function(event)
{
	$('#toggleauth_id').val($(event.target).attr('rel'));
	$("#toggleauth").css( {position:"absolute", top: event.pageY -10, left: event.pageX + 30});
	$("#toggleauth").toggle();
});
</script>
</body></html>
