<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';
require 'Clubshop/Params.php';
require 'Clubshop/Admin/PaFta.php';

//ini_set('display_errors', 1);

function DB()
{
    return \Clubshop\AdminDbConnect::DBI(array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}

$PaFta = new \Clubshop\Admin\PaFta(DB());

if (! isset($PageTitle))    // this could have been set in another script prior to including this
    $PageTitle = 'Pay Agent/Fee Transmission Agent List';
$errors = array();
$messages = array();
$params = \Clubshop\Params::loadParams(array(
    'action'    => array('default'=>''),
    'id'        => array('default'=>''),
    'notes'     => array('default'=>''),
    'confirm'   => array('default'=>''),
    'country_code'   => array('default'=>''),
    'downline_auth'     => array('default'=>''),
    'crossline_auth'    => array('default'=>''),
    'fee'       => array('default'=>''),
    'emailaddress' => array('default'=>'')
));

if (function_exists('overrideAction')) {   // give our "view script" a chance to influence the value before hitting the switch
    overrideAction();
}
    
switch ($params['action']) {
    case 'add':
        Add();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'addPaByID':
        addPaByID();    // this function is part of the edit page
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'addFtaByID':
        addFtaByID();    // this function is part of the edit page
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'addFtaCountry':
        addFtaCountry();
        addPaCountry(); // this function is part of the edit page
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'addPaCountry':
        // these functions are part of the edit page
        addFtaCountry();
        addPaCountry();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'edit':
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'editFtaFee':
        // this function is part of the edit page
        editFtaFee();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'delete':
        Delete();
        $PaFta->createCombinedObj();
        break;
    case 'toggleauth':
        ToggleAuth();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'editFtaCountry':
        // this function is part of the edit page
        editFtaCountry();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'editPaCountry':
        // this function is part of the edit page
        editPaCountry();
        $PaFta->createCombinedObj($params['id']);
        break;
    case 'deleteFtaCountry':
        // this function is part of the edit page
        deleteFtaCountry();
        $PaFta->createCombinedObj($params['id']);
        break;
    default:
        $PaFta->createCombinedObj($params['id']);
}

function Add()
{
    global $params, $errors, $messages, $candidate;
    if (! $params['id']) {
        $errors[] = 'No member ID parameter received';
        return;
    }
    
    // we could have received an alias or a plain numeric ID
    $qry = 'SELECT id, alias, firstname, lastname, membertype, country FROM members WHERE ';
    $qry .= (preg_match('/\D/', $params['id']) ? "alias= UPPER(?)" : "id= ?");
    $candidate = DB()->selectrow_array($qry, array($params['id']));
    if (! $candidate) {
        $errors[] = $params['id'] . " not found";
        return;
    }
    elseif (! isset($params['confirm'])) {
        // do nothing as the page form will take care of it
    } else {
        
        $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
        $fee = $params['fee'] ? DB()->db()->quote($params['fee']) : 'NULL';
        
        DB()->db()->beginTransaction();
        $rv = DB()->db()->exec("INSERT INTO pay_agent.pay_agents (id, notes) VALUES ({$params['id']}, $notes)");
        if ($rv == 1) {
            $messages[] = "Pay Agent record addition successful";
        } else {
            $err = DB()->db()->errorInfo();
            $errors[] = "Pay Agent record addition unsuccessful: " . $err[2];
            DB()->db()->rollBack();
            return;
        }
        
        $rv = DB()->db()->exec("INSERT INTO fee_transmission_agents (id, notes) VALUES ({$params['id']}, $notes)");
        if ($rv == 1) {
            $messages[] = "Fee Transmission Agent record addition successful";
        } else {
            $err = DB()->db()->errorInfo();
            
            // get rid of the last success message since we are rolling back
            array_pop($messages);
            $errors[] = "Fee Transmission Agent record addition unsuccessful: " . $err[2];
            DB()->db()->rollBack();
            return;
        }
        
        // the following function will be testing for parameters and using those as the edit form has provision for this at creation time
        // however, when adding an FTA, we do not provide that option, so we will default them both to TRUE
        $params['downline_auth'] = true;
        $params['crossline_auth'] = true;
        if (addFtaCountry()) {
            DB()->db()->commit();
        } else {
            // get rid of the last two success messages since we are rolling back
            array_pop($messages);
            array_pop($messages);
            DB()->db()->rollBack();
        }
    }
}

function addFtaCountry()
{
    global $messages, $errors, $params;
    // we should have a country_code, an ID and possibly a downline_auth and/or a crossline_auth parameter
    if ((! $params['country_code']) || ! preg_match('/^[A-Z]{2}$/', $params['country_code'])) {
        $errors[] = 'No country code/ or invalid parameter received: ' . $params['country_code'];
        return;
    }
    if (! $params['fee']) {
        $errors[] = 'No Fee value parameter received';
        return;
    }
    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }

    // let's make sure we have a fee_transmission_agents record to begin with as there is a primary key constraint on that
    $id = DB()->fetchSingleCol('SELECT id FROM fee_transmission_agents WHERE id= ?', array($params['id']));
    if (! $id) {
        $errors[] = 'No Fee Transmission Agent Country Record created as there is no Fee Transmission Agent master record';
        return;
    }

    // make sure we are not trying to insert a duplicate
    $id = DB()->fetchSingleCol('SELECT id FROM fee_transmission_agent_countries WHERE country_code= ? AND id= ?', array($params['country_code'], $params['id']));
    if ($id) {
        $errors[] = 'A Fee Transmission Agent Country Record already exists for: ' . $params['country_code'];
        return;
    }

    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    $fee = $params['fee'];
    $matched_digits = array();
    if ($fee && ! preg_match('/(\d+)%$/', $fee, $matched_digits)) {
        $errors[] = "Currently we only recognize percentage values formatted like this: 10%. What was received is this: $fee";
        return;
    } elseif ($fee) {
        $fee_as_percentage = $matched_digits[1];error_log($fee_as_percentage);
        $fee = DB()->db()->quote($fee);
    } else {
        $fee = 'NULL';
    }
    
    $downline_auth = isset($params['downline_auth']) ? 'TRUE' : 'FALSE';
    $crossline_auth = isset($params['crossline_auth']) ? 'TRUE' : 'FALSE';

    $rv = DB()->db()->exec("
        INSERT INTO fee_transmission_agent_countries (id, country_code, fee_authorized, notes, downline_auth, crossline_auth)
        VALUES ({$params['id']}, '{$params['country_code']}', $fee, $notes, $downline_auth, $crossline_auth)");

    if ($rv) {
        $messages[] = "FTA Country Record successfully added";
        return true;
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "FTA Country Record not added: $err";
        return false;
    }
}

function Delete()
{
    global $params, $errors, $messages;
    if (! $params['id']) {
        $errors[] = 'No ID number parameter received';
        return;
    }
    if (preg_match('/\D/', $params['id']))
        die("ID parameter should be numeric: " . $params['id']);
    
    $rv = DB()->db()->exec("DELETE FROM pay_agent.pay_agents WHERE id= {$params['id']}");
    if ($rv == 1) {
        $messages[] = "Pay Agent deletion successful";
    } else {
        $err = DB()->db()->errorInfo();
        if ($err)
            $errors[] = "Pay Agent deletion unsuccessful: " . $err[2];
    }
    
    $rv = DB()->db()->exec("DELETE FROM fee_transmission_agents WHERE id= {$params['id']}");
    if ($rv == 1) {
        $messages[] = "Fee Transmission Agent deletion successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Fee Transmission Agent deletion unsuccessful: " . $err[2];
    }
}

function ToggleAuth()
{
    global $params, $errors, $messages;
    if (! $params['id']) {
        $errors[] = 'No card number parameter received';
        return;
    }

    // ID should be an integer by the time we get to here, but let's make sure before we try to stuff into into the SQL below
    if (preg_match('/\D/', $params['id']))
        die("id parameter should be numeric only: {$params['id']}");
    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';

    $auth = DB()->selectrow_list("SELECT active FROM pay_agent.pay_agents WHERE id= {$params['id']}");
    
    // it is possible that there is no pay agent record, so we will perform logic based upon one or the other
    if ($auth) {
        $auth = $auth[0] ? 'FALSE':'TRUE';
        $rv = DB()->db()->exec("UPDATE pay_agent.pay_agents SET active= $auth, notes= $notes WHERE id= {$params['id']}");
        if ($rv == 1) {
            $messages[] = "Authorization toggle of Pay Agent record successful";
        } else {
            $err = DB()->db()->errorInfo();
            $errors[] = "Authorization toggle of Pay Agent unsuccessful: " . $err[2];
        }
    } else {
        $auth = DB()->selectrow_list("SELECT active FROM fee_transmission_agents WHERE id= {$params['id']}");
        if (! $auth) // theoretically this should never happen, but....
            return;
        $auth = $auth[0] ? 'FALSE':'TRUE';
    }

    // by here we may have an FTA record -or- maybe not, so we will attempt to update the record but will only render an error if one exists
    $rv = DB()->db()->exec("UPDATE fee_transmission_agents SET active= $auth, notes= $notes WHERE id= {$params['id']}");
    if ($rv == 1) {
        $messages[] = "Authorization toggle of Fee Transmission Agent record successful";
    } else {
        $err = DB()->db()->errorInfo();
        if (isset($err)) {
            $errors[] = "Authorization toggle of Fee Transmission Agent unsuccessful: " . $err[2];
        }
    }
}

// we have to create columns in this one
function enumerateFtaCountries(array $fta)
{
    $rv = array();
    foreach ($fta['authorized_countries'] as $c) {
        $row  = '<td class="nowrap">' . $c['country_name'] . '</td>';
        $row .= '<td class="alc">' . $c['fee_authorized'] . '</td>';
        $row .= '<td class="alc">' . ($c['downline_auth'] ? 'X' : '') . '</td>';
        $row .= '<td class="alc">' . ($c['crossline_auth'] ? 'X' : '') . '</td>';
        $rv[] = $row;
    }
    
    return $rv;
}

// we only need a text list from this one
function enumeratePaCountries(array $pa)
{
    $rv = '';
    $last = end($pa['authorized_countries']);
    foreach ($pa['authorized_countries'] as $c) {
        $rv .= $c['active'] ? $c['country_name'] : '<span class="inactive">' . $c['country_name'] . '</span>';
        // add on a newline only if this is not the last item
        if ($last['country_code'] != $c['country_code'])
            $rv .= '<br />';
    }
    
    return $rv;
}
?>