<?php
$PageTitle = 'Pay Agent/Fee Transmission Agent: edit Agent';

// function overrideAction()
// {
//     global $params;
//     $params['action'] = 'edit';
// }
require 'mc.php';


include 'dochead.php';

$auth = $PaFta->active ? "<a class=\"aok tauth\" href=\"#\" rel=\"{$PaFta->id}\">Yes</a>" : "<a class=\"nok tauth\" href=\"#\" rel=\"{$PaFta->id}\">No</a>";
$agent = $PaFta->result();
$agent = reset($agent);
?>
<p>
	<a href="index?action=delete&amp;id=<?=$params['id'];?>"
		onclick="return confirm('Confirm you want to delete all records for this Agent: <?=$PaFta->alias;?>')"
		class="fpnotes">Delete this Agent</a> <br />
	<a class="fpnotes" href="index">Show all records</a>
</p>

<table class="report">
  <tr>
    <th rowspan="3" class="empty"></th>
    <th>ID:</th><td><?=$PaFta->alias;?></td>
  </tr>
  <tr><th>Agent:</th><td><?=$PaFta->name;?></td></tr>
  <tr><th>Active:</th><td><?=$auth;?></td></tr>
  <tr>
    <th <?php echo array_key_exists('pa', $agent) ? 'rowspan="4" ' : ''; ?>class="pa" style="text-align:center"><?php foreach (str_split('PA') as $l){ echo $l . '<br />'; } ?></th>
  
  <?php if (array_key_exists('pa', $agent)): ?>
    <th>Notes:</th><td><?=$agent['pa']['notes']; ?></td></tr>
  <tr><th>Timestamp:</th><td><?=$agent['pa']['stamp']; ?></td></tr>
  <tr><th>Operator:</th><td><?=$agent['pa']['operator']; ?></td></tr>
  <tr><th>Countries:</th>
  <td style="padding: 0;">
    <table class="sr" style="border-collapse: collapse; width:100%;">
        <tr><th class="nolb notb">Country</th>
            <th class="notb">Email Address</th>
            <th class="notb">Notes</th>
            <th class="notb">Operator</th>
            <th class="notb norb">Stamp</th>
        </tr>
        <?php echo enumeratePaCountriesForEdit($agent['pa']); ?><!-- returns a set of table rows -->
        <tr><td colspan=5" class="nolb nobb norb">
        
        <form id="paCountryEditFrm" action="<?php echo basename(__FILE__); ?>" style="display: none" method="get">
            <input type="hidden" name="id" value="<?=$agent['id']?>" />
            <input type="hidden" name="country_code" value="" id="elem_pa_country_code" />
            <input type="hidden" name="action" value="editPaCountry" id="actionPaCountryEditFrm" />
            <span id="paCountryEditName"></span>:

            Email Address: <input type="text" name="emailaddress" id="emailaddress_countryEditFrm" style="width:15em;" />&nbsp;&nbsp;
            Notes: <input type="text" name="notes" style="width:30em;" /><br />
            <input type="submit" value="Update" style="float: right;" />
            <input type="submit" value="Delete" style="float: right; background-color:red; color:white; margin-right:3em;" onclick="$('#actionPaCountryEditFrm').val('deleteFtaCountry')"  />
            <br />
            <span style="color:red">Deleting the country will delete the corresponding Fees Transmission Agent country.</span>
        </form>
        
        <form action="<?php echo basename(__FILE__); ?>" method="get" id="paCountryAddFrm" style="display: none">
            <input type="hidden" name="id" value="<?=$agent['id']?>" />
            <input type="hidden" name="action" value="addPaCountry" />
            <select name="country_code">
            <option value="">Select a Country</option>
            <?php foreach (getPaPotentialCountryList() AS $c): ?>
            <option value="<?=$c['country_code']?>"><?=$c['country_name']?></option>
            <?php endforeach; ?>
            </select>
            Email Address: <input type="text" name="emailaddress" style="width:15em;" />&nbsp;&nbsp;
            Notes: <input type="text" name="notes" style="width:30em;" /><br />
            <input type="submit" value="Add Country" style="float: right;" /><br />
            Adding a country here will cause a Fees Transmission Agent country to be created if a Fees Transmission Agent master record exists.
        </form>
        <a href="#" onclick="$('#paCountryEditFrm').hide(); $('#paCountryAddFrm').toggle(); return false;">Add a Country</a>
        </td></tr>
    </table>
  </td>
  <?php else: ?>
    <td class="nopa" colspan="2">Not a Pay Agent<p class="fp"><a href="#" id="addPaLink">Click</a> to create a Pay Agent record</p></td>
  <?php endif;?>
  </tr>
  
  <tr>
    <th rowspan="4" class="fta" style="text-align:center"><?php foreach (str_split('FTA') as $l){ echo $l . '<br />'; } ?></th>
    
  <?php if (array_key_exists('fta', $agent)): ?>
    <th>Notes:</th><td><?=$agent['fta']['notes']; ?></td>
  </tr>
  <tr><th>Timestamp:</th><td><?=$agent['fta']['stamp']; ?></td></tr>
  <tr><th>Operator:</th><td><?=$agent['fta']['operator']; ?></td></tr>
  <tr><th>Countries:</th>
    <td style="padding: 0;">
    <table class="sr" style="border-collapse: collapse; width:100%;">
        <tr><th class="nolb notb">Country</th>
            <th class="notb">Fee</th>
            <th class="notb">D</th>
            <th class="notb">Xl</th>
            <th class="notb">Notes</th>
            <th class="notb">Operator</th>
            <th class="notb norb">Stamp</th>
        </tr>
        <?php echo enumerateFtaCountriesForEdit($agent['fta']); ?><!-- returns a set of table rows -->
        <tr><td colspan=7" class="nolb nobb norb">
        
        <form id="countryEditFrm" action="<?php echo basename(__FILE__); ?>" style="display: none" method="get">
            <input type="hidden" name="id" value="<?=$agent['id']?>" />
            <input type="hidden" name="country_code" value="" id="elem_country_code" />
            <input type="hidden" name="action" value="editFtaCountry" id="actionCountryEditFrm" />
            <span id="countryEditName"></span>:
            D: <input type="checkbox" name="downline_auth" id="downline_auth" value="1" />&nbsp;&nbsp;
            Xl: <input type="checkbox" name="crossline_auth" id="crossline_auth" value="1" />&nbsp;&nbsp;
            Fee: <input type="text" name="fee" id="fee_countryEditFrm" style="width:5em;" />&nbsp;&nbsp;
            Notes: <input type="text" name="notes" style="width:30em;" /><br />
            <input type="submit" value="Update" style="float: right;" />
            <input type="submit" value="Delete" style="float: right; background-color:red; color:white; margin-right:3em;" onclick="$('#actionCountryEditFrm').val('deleteFtaCountry')"  />
            <br />
            Changing these settings will not affect the Pay Agent countries.<br />
            <span style="color:red">Deleting the country will delete the corresponding Pay Agent country.</span>
        </form>
        
        <form action="<?php echo basename(__FILE__); ?>" method="get" id="countryAddFrm" style="display: none">
            <input type="hidden" name="id" value="<?=$agent['id']?>" />
            <input type="hidden" name="action" value="addFtaCountry" />
            <select name="country_code">
            <option value="">Select a Country</option>
            <?php foreach (getPotentialCountryList() AS $c): ?>
            <option value="<?=$c['country_code']?>"><?=$c['country_name']?></option>
            <?php endforeach; ?>
            </select>
            D: <input type="checkbox" name="downline_auth" id="downline_auth" value="1" checked="checked" />&nbsp;&nbsp;
            Xl: <input type="checkbox" name="crossline_auth" id="crossline_auth" value="1" checked="checked" />&nbsp;&nbsp;
            Fee: <input type="text" name="fee" style="width:5em;" />&nbsp;&nbsp;
            Notes: <input type="text" name="notes" style="width:30em;" /><br />
            <input type="submit" value="Update" style="float: right;" /><br />
            Adding a country here will cause a Pay Agent country to be created if a Pay Agent master record exists.
        </form>
        <a href="#" onclick="$('#countryEditFrm').hide(); $('#countryAddFrm').toggle(); return false;">Add a Country</a>
        </td></tr>
    </table></td>
  <?php else: ?>
    <td class="nopa" rowspan="4" colspan="2">Not a Fee Transmission Agent<p class="fp">
        <a href="#" id="addFtaLink">Click</a> to create a Fee Transmission Agent record</p>
    </td>
  <?php endif;?>
  </tr>

</table>

<form id="toggleauth" method="get" action="edit" style="display: none; padding: 1em; background-color: #cfc; border: 2px solid grey;">
<input type="hidden" name="id" id="toggleauth_id" value="" />
Notes: <input type="text" name="notes" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="toggleauth" />
</form>

<form id="addPaFrm" method="get" action="" style="display: none; padding: 1em; background-color: #cfc; border: 2px solid grey;">
<input type="hidden" name="id" value="<?=$params['id']?>" />
Notes: <input type="text" name="notes" style="width:30em;" />
<p class="fpnotes">The new Pay Agent record will be associated with the current country of the Fee Transmission Agent relationship. Add additional countries afterwards.</p>
<input type="submit" value="Add as Pay Agent" />
<input type="hidden" name="action" value="addPaByID" />
</form>

<form id="addFtaFrm" method="get" action="" style="display: none; padding: 1em; background-color: #cfc; border: 2px solid grey;">
<input type="hidden" name="id" value="<?=$params['id']?>" />
<input type="hidden" name="country_code" value="<?=$PaFta->country?>" />
Fee: <input type="text" name="fee" style="width:5em;" />&nbsp;&nbsp;
Notes: <input type="text" name="notes" style="width:30em;" />
<p class="fpnotes">The new Pay Agent record will be associated with the current country of the Fee Transmission Agent relationship. Add additional countries afterwards.</p>
<input type="submit" value="Add as Fee Transmission Agent" />
<input type="hidden" name="action" value="addFtaByID" />
</form>
<script type="text/javascript">
$('.tauth').click( function(event)
{
	$('#toggleauth_id').val($(event.target).attr('rel'));
	$("#toggleauth").css( {position:"absolute", top: event.pageY -10, left: event.pageX + 30});
	$("#toggleauth").toggle();
});

$('#addPaLink').click( function(event)
	{
		$("#addPaFrm").css( {position:"absolute", top: event.pageY -10, left: event.pageX + 30});
		$("#addPaFrm").toggle();
	});

$('#addFtaLink').click( function(event)
		{
			$("#addFtaFrm").css( {position:"absolute", top: event.pageY -10, left: event.pageX + 30});
			$("#addFtaFrm").toggle();
		});
		
$('.countryEditAnchor').click( function(event)
{
	$('#countryAddFrm').hide();
	var elms = $(event.target).attr('rel').split('-');
	var b4 = $('#elem_country_code').val();
	$('#elem_country_code').val(elms[0]);
	$('#countryEditName').text($(event.target).text());

	// it is possible that the form is open and we are clicking a different country, in which case we don't want to close it
	var cstatus = $('#countryEditFrm').is(":visible");
	if (b4 != elms[0]) {
	    $('#countryEditFrm').show();
	} else if (! cstatus) {
		$('#countryEditFrm').show();
	} else {
		$('#countryEditFrm').hide();
	}
			
	$('#downline_auth').prop('checked', elms[1]);
	$('#crossline_auth').prop('checked', elms[2]);
	$('#fee_countryEditFrm').val(elms[3]);
});

$('.paCountryEditAnchor').click( function(event)
		{
			$('#paCountryAddFrm').hide();
			var elms = $(event.target).attr('rel').split('-');
			var b4 = $('#elem_pa_country_code').val();
			$('#elem_pa_country_code').val(elms[0]);
			$('#paCountryEditName').text($(event.target).text());

			// it is possible that the form is open and we are clicking a different country, in which case we don't want to close it
			var cstatus = $('#paCountryEditFrm').is(":visible");
			if (b4 != elms[0]) {
			    $('#paCountryEditFrm').show();
			} else if (! cstatus) {
				$('#paCountryEditFrm').show();
			} else {
				$('#paCountryEditFrm').hide();
			}

			$('#emailaddress_countryEditFrm').val(elms[1]);
		});
</script>
</body>
</html>
<?php
function addPaByID()
{
    global $messages, $errors, $params;
    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    
    // let's make sure we have a pay_agent record to begin with as there is a primary key constraint on that
    $id = DB()->fetchSingleCol('SELECT id FROM pay_agent.pay_agents WHERE id= ?', array($params['id']));
    if ($id) {
        $errors[] = 'A Pay Agent Record with that ID already exists';
        return;
    }
    
    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    
    // the pay_agents table will automatically add the country of the agent to the authorized_countries table
    $rv = DB()->db()->exec("INSERT INTO pay_agent.pay_agents (id, notes) VALUES ({$params['id']}, $notes)");
    if ($rv) {
        $messages[] = "FTA Country Record successfully added";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "FTA Country Record not added: $err";
    }
}

function addFtaByID()
{
    global $messages, $errors, $params;
    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    if (! $params['fee']) {
        $errors[] = 'No Fee value parameter received';
        return;
    }
    // let's make sure we have a pay_agent record to begin with as there is a primary key constraint on that
    $id = DB()->fetchSingleCol('SELECT id FROM fee_transmission_agents WHERE id= ?', array($params['id']));
    if ($id) {
        $errors[] = 'A Fee Transmission Record with that ID already exists';
        return;
    }

    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    $fee = $params['fee'] ? DB()->db()->quote($params['fee']) : 'NULL';
    
    DB()->db()->beginTransaction();
    $rv = DB()->db()->exec("INSERT INTO fee_transmission_agents (id, notes) VALUES ({$params['id']}, $notes)");
    if ($rv) {
        $messages[] = "Fee Transmission Agent Record successfully added";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "FTA Country Record not added: $err";
    }
    
    // now create a related countries record
    addFtaCountry();
    DB()->db()->commit();
}

// we have to create columns in this one
function enumerateFtaCountriesForEdit(array $fta)
{
    $rv = '';
    foreach ($fta['authorized_countries'] as $c) {
        $rel = "{$c['country_code']}-{$c['downline_auth']}-{$c['crossline_auth']}-{$c['fee_authorized']}";
        
        $row  = '<tr><td class="nowrap nolb"><a href="#" class="countryEditAnchor notxd" rel="' . $rel . '">' . $c['country_name'] . '</a></td>';
        $row .= '<td class="nowrap">' . $c['fee_authorized'] . '</td>';
        $row .= '<td class="alc">' . ($c['downline_auth'] ? 'X' : '') . '</td>';
        $row .= '<td class="alc">' . ($c['crossline_auth'] ? 'X' : '') . '</td>';
        $row .= '<td class="alc">' . $c['notes'] . '</td>';
	    $row .= '<td class="alc">' . $c['operator'] . '</td>';
		$row .= '<td class="alc norb">' . $c['stamp'] . '</td>';
        $row .= '</tr>';
        $rv .= $row;
    }

    return $rv;
}

// we have to create columns in this one
function enumeratePaCountriesForEdit(array $pa)
{
    $rv = '';
    foreach ($pa['authorized_countries'] as $c) {
        $rel = "{$c['country_code']}-{$c['emailaddress']}";
        
        $row  = '<tr><td class="nowrap nolb"><a href="#" class="paCountryEditAnchor notxd" rel="' . $rel . '">' . $c['country_name'] . '</a></td>';
        $row .= '<td class="nowrap">' . $c['emailaddress'] . '</td>';
        $row .= '<td class="alc">' . $c['notes'] . '</td>';
	    $row .= '<td class="alc">' . $c['operator'] . '</td>';
		$row .= '<td class="alc norb">' . $c['stamp'] . '</td>';
        $row .= '</tr>';
        $rv .= $row;

    }

    return $rv;
}

function getPotentialCountryList()
{
    global $params;
    return DB()->fetchList_assoc("
        SELECT tc.country_code, tc.country_name
        FROM tbl_countrycodes tc
        LEFT JOIN fee_transmission_agent_countries ftac
            ON ftac.country_code=tc.country_code
            AND ftac.id= ?
        WHERE tc.restrictions=FALSE
        AND tc.active=TRUE
        AND ftac.country_code IS NULL
        ORDER BY tc.country_name
        ", array($params['id']));
}

function getPaPotentialCountryList()
{
    global $params;
    return DB()->fetchList_assoc("
        SELECT tc.country_code, tc.country_name
        FROM tbl_countrycodes tc
        LEFT JOIN pay_agent.authorized_countries ftac
            ON ftac.country_code=tc.country_code
            AND ftac.id= ?
        WHERE tc.restrictions=FALSE
        AND tc.active=TRUE
        AND ftac.country_code IS NULL
        ORDER BY tc.country_name
        ", array($params['id']));
}

function addPaCountry()
{
    global $messages, $errors, $params;
    // we should have a country_code, an ID and possibly a downline_auth and/or a crossline_auth parameter
    if ((! $params['country_code']) || ! preg_match('/^[A-Z]{2}$/', $params['country_code'])) {
        $errors[] = 'No country code/ or invalid parameter received: ' . $params['country_code'];
        return;
    }

    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    
    // let's make sure we have a pay_agent record to begin with as there is a primary key constraint on that
    $id = DB()->fetchSingleCol('SELECT id FROM pay_agent.pay_agents WHERE id= ?', array($params['id']));
    if (! $id) {
        $errors[] = 'No Pay Agent Country Record created as there is no Pay Agent master record';
        return;
    }
    
    // make sure we are not trying to insert a duplicate
    $id = DB()->fetchSingleCol('SELECT id FROM pay_agent.authorized_countries WHERE country_code= ? AND id= ?', array($params['country_code'], $params['id']));
    if ($id) {
        $errors[] = 'A Pay Agent Country Record already exists for: ' . $params['country_code'];
        return;
    }
    
    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    $emailaddress = $params['emailaddress'] ? DB()->db()->quote($params['emailaddress']) : 'NULL';
    
    $rv = DB()->db()->exec("
        INSERT INTO pay_agent.authorized_countries (id, country_code, notes, emailaddress)
        VALUES ({$params['id']}, '{$params['country_code']}', $notes, $emailaddress)");

    if ($rv) {
        $messages[] = "Pay Agent Country Record successfully added";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Pay Agent Country Record not added: $err";
    }
}

function editFtaCountry()
{
    global $messages, $errors, $params;
    // we should have a country_code, an ID and possibly a downline_auth and/or a crossline_auth parameter
    if ((! $params['country_code']) || ! preg_match('/^[A-Z]{2}$/', $params['country_code'])) {
        $errors[] = 'No country code/ or invalid parameter received: ' . $params['country_code'];
        return;
    }
    if (($params['downline_auth'] || $params['crossline_auth']) && ! $params['fee']) {  // we will forego the fee if we are basically deactivating the country
        $errors[] = 'No Fee value parameter received';
        return;
    }
    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    $fee = $params['fee'];
    $matched_digits = array();
    if ($fee && ! preg_match('/(\d+)%$/', $fee, $matched_digits)) {
        $errors[] = "Currently we only recognize percentage values formatted like this: 10%. What was received is this: $fee";
        return;
    } elseif ($fee) {
        $fee_as_percentage = $matched_digits[1];
        $fee_as_percentage *= 0.01; // reduce a textual representation to a numeric representation
        $fee = DB()->db()->quote($fee);
    } else {
        $fee = 'NULL';
    }
    
    $downline_auth = isset($params['downline_auth']) ? 'TRUE' : 'FALSE';
    $crossline_auth = isset($params['crossline_auth']) ? 'TRUE' : 'FALSE';
    
    $rv = DB()->db()->exec("
        UPDATE fee_transmission_agent_countries SET
            notes= $notes,
            downline_auth= $downline_auth,
            crossline_auth= $crossline_auth,
            fee_authorized= $fee,
            fee_authorized_percentage= $fee_as_percentage
        WHERE id= {$params['id']}
        AND country_code= '{$params['country_code']}'
        -- why bother `updating` when there are no changes
        AND (downline_auth != $downline_auth OR crossline_auth != $crossline_auth OR COALESCE(fee_authorized,'') != $fee)");
    
    if ($rv) {
        $messages[] = "Record successfully updated";
    } else {
        $errors[] = "Record not updated";
    }
}

function editPaCountry()
{
    global $messages, $errors, $params;
    // we should have a country_code, an ID and possibly an emailaddress
    if ((! $params['country_code']) || ! preg_match('/^[A-Z]{2}$/', $params['country_code'])) {
        $errors[] = 'No country code/ or invalid parameter received: ' . $params['country_code'];
        return;
    }

    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    $notes = $params['notes'] ? DB()->db()->quote($params['notes']) : 'NULL';
    $emailaddress = $params['emailaddress'] ? DB()->db()->quote($params['emailaddress']) : 'NULL';

    $rv = DB()->db()->exec("
        UPDATE pay_agent.authorized_countries SET notes= $notes, emailaddress= $emailaddress
        WHERE id= {$params['id']}
        AND country_code= '{$params['country_code']}'
        -- why bother `updating` when there are no changes
        AND (COALESCE(emailaddress,'') != COALESCE($emailaddress,'') OR COALESCE(notes,'') != COALESCE($notes,''))");

    if ($rv) {
        $messages[] = "Record successfully updated";
    } else {
        $errors[] = "Record not updated";
    }
}

function deleteFtaCountry()
{
    global $messages, $errors, $params;
    // we should have a country_code, an ID and possibly a downline_auth and/or a crossline_auth parameter
    if ((! $params['country_code']) || ! preg_match('/^[A-Z]{2}$/', $params['country_code'])) {
        $errors[] = 'No country code/ or invalid parameter received: ' . $params['country_code'];
        return;
    }
    if ((! $params['id']) || preg_match('/\D/', $params['id'])) {
        $errors[] = 'No ID parameter received or value is invalid: ' . $params['id'];
        return;
    }
    
    $rv = DB()->db()->beginTransaction();
    try {
        $rv = DB()->db()->exec("DELETE FROM fee_transmission_agent_countries WHERE id= {$params['id']} AND country_code='{$params['country_code']}'");
    } catch (Exception $e) {
        DB()->db()->rollBack();
        $errors[] = 'Delete from fee_transmission_agent_countries failed with error: ' . $e->getMessage();
        return;
    }
    try {
        $rv = DB()->db()->exec("DELETE FROM pay_agent.authorized_countries WHERE id= {$params['id']} AND country_code='{$params['country_code']}'");
    } catch (Exception $e) {
        DB()->db()->rollBack();
        $errors[] = 'Delete from pay_agent.authorized_countries failed with error: ' . $e->getMessage();
        return;
    }
        DB()->db()->commit();
    $messages[] = 'The country was successfully removed from both PA and FTA records';
}
?>