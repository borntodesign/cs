<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDB.php';

$member = array();
$errors = array();
$messages = array();
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : '';
$viewTmpl = 'view-index.php';
switch ($action) {
    case 'confirm':
        try {
            dbConnect();
        } catch (\Exception $e) {
            die("Uncaught exception: " . $e->getMessage());
        }
        $viewTmpl = LookupMember() ? 'view-confirm.php' : 'view-index.php';
        break;
    case 'adjust':
        try {
            dbConnect();
        } catch (\Exception $e) {
            die("Uncaught exception: " . $e->getMessage());
        }
        $viewTmpl = 'view-confirm.php';
        if (LookupMember()) {
            Adjust();
            // refresh the figures
            LookupMember();
        }
    default:
        
        //;
}

include $viewTmpl;

/* Start of functions */

function Adjust() {
    global $errors, $messages, $member;
    $id = isset($_GET['id']) ? htmlspecialchars($_GET['id']) : '';
    if (! $id) {
        $errors[] = 'No ID received';
        return;
    }
    
    $newxferable = trim( isset($_GET['newxferable']) ? htmlspecialchars($_GET['newxferable']) : 0);
    
    // no point in making the transferable greater than the account balance
    if ($newxferable > $member['account_balance'])
        $newxferable = $member['account_balance'];
    
    $transAmt = $newxferable - $member['transferrable_items_total'];
    
    if (! $transAmt) {
        $messages[] = "This action would result in -0- change, so no operation was performed";
        return;
    }
    
    $db = dbConnect()->db();
    $db->beginTransaction();
    try {
 //       $transAmtIn = $transAmt * -1;
        $db->exec("INSERT INTO soa (id, trans_type, amount) VALUES ({$member['id']}, 20001, $transAmt)");
        $transAmt = $transAmt * -1;
        $db->exec("INSERT INTO soa (id, trans_type, amount) VALUES ({$member['id']}, 20002, $transAmt)");
    } catch (Exception $e) {
        $db->rollBack();
        $errors[] = "Record creation failure: " . $e->getMessage();
    }
 
    $db->commit();
    $messages[] = 'Adjustment was successful';
}

function LookupMember() {
    global $errors, $member;
    $id = trim( isset($_GET['id']) ? htmlspecialchars($_GET['id']) : '' );
    if (! $id) {
        $errors[] = 'No ID received';
        return false;
    }
    
    $member = dbConnect()->selectrow_array('
        SELECT m.id, m.alias, m.firstname, m.lastname,
            COALESCE(soa.account_balance,0) AS account_balance,
            COALESCE(soa.transferrable_balance,0) AS transferable,
            COALESCE(soa.transferrable_items_total,0) AS transferrable_items_total
        FROM members m
        LEFT JOIN soa_account_balance_payable soa
            ON soa.id=m.id
        WHERE ' . (preg_match('/\D/', $id) ? 'm.alias=UPPER(?)' : 'm.id=?'), array($id));
    
    if ($member) {
        return true;
    } else {
        $errors[] = "No match for: $id";
        return false;
    }
}

function dbConnect() {
    static $dbi;
    if (! $dbi) {
        $operator = htmlspecialchars($_COOKIE['operator']);
        if (preg_match('/[^a-zA-Z0-9\.]/', $operator)) {
            die("The username looks bad: $operator");
        }
        $pwd = htmlspecialchars($_COOKIE['pwd']);
        
        try {
            $dbi = new \Clubshop\AdminDB($operator, $pwd);
        } catch (\Exception $e) {
            die("DB connect failed: " . $e->getMessage());
        }
    }

    return $dbi;
}
