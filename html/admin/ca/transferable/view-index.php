<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Adjust Club Account Transferability Amount - Start</title>
<link type="text/css" rel="stylesheet" href="styles.css" />
<body>
<?php 
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<p>This interface will allow you to modify the transferable balance for the specified ID.
It will do this by creating two invisible transactions that cancel each other out as far as the overall Club Account balance goes.
However the transaction types will cause the transferability of balances to change according to your desired amount.
Note: the amount entered can be -0- or negative to prevent any transfers from taking place until sufficient account activity has transpired that is transferable.
</p>
<form action="" method="get">
Enter the member ID to begin: <input type="text" name="id" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="confirm" />
</form>
</body></html>