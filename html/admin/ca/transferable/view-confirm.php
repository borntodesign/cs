<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Adjust Club Account Transferability Amount - Confirm</title>
<link type="text/css" rel="stylesheet" href="styles.css" />
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<form action="" method="get">
<input type="hidden" name="id" value="<?= htmlspecialchars($_GET['id'])?>" />
<p class="info">
<?php
echo "{$member['alias']} - {$member['firstname']} {$member['lastname']} -
Account Balance: {$member['account_balance']} - Transferable Balance: {$member['transferable']} (";
echo $member['transferrable_items_total'] < 0 ? '<span class="error">' . $member['transferrable_items_total'] . '</span>' : $member['transferrable_items_total'];
echo ')';
?>
</p>
Make the transferable balance =
<input type="text" name="newxferable" value="<?=$member['account_balance']?>" onclick="this.select()" />
<span class="fp">(Change if desired)</span>
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="adjust" />
</form>
<p><a href="./">Start Over</a>
</body></html>