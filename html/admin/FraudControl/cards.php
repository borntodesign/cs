<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$errors = array();
$messages = array();
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : '';
$id = strtoupper(trim( isset($_GET['id']) ? htmlspecialchars($_GET['id']) : '' ));
switch ($action) {
    case 'add':
        Add();
        break;
    case 'remove':
        Delete();
        break;
    case 'toggleauth':
        ToggleAuth();
    default:
        //;
}

function Add()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No card number parameter received';
        return;
    }
    $id = DB()->db()->quote($id);
    $notes = isset($_GET['notes']) ? htmlspecialchars($_GET['notes']) : null;
    $notes = $notes ? DB()->db()->quote($notes) : 'NULL';

    $rv = DB()->db()->exec("INSERT INTO fraud_control.authorized_cc (ccnum, notes) VALUES ($id, $notes)");
    if ($rv == 1) {
        $messages[] = "Record addition successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Record addition unsuccessful: " . $err[2];
    }
}

function Delete()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No card number parameter received';
        return;
    }
    $id = DB()->db()->quote($id);
    $rv = DB()->db()->exec("DELETE FROM fraud_control.authorized_cc WHERE ccnum= $id");
    if ($rv == 1) {
        $messages[] = "Delete successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Delete unsuccessful: " . $err[2];
    }
}

function ToggleAuth()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No card number parameter received';
        return;
    }
    $id = DB()->db()->quote($id);
    $notes = isset($_GET['notes']) ? htmlspecialchars($_GET['notes']) : null;
    $notes = $notes ? DB()->db()->quote($notes) : 'NULL';
    
    $auth = DB()->fetchSingleCol("SELECT auth_revoked FROM fraud_control.authorized_cc WHERE ccnum= $id");
    $auth = $auth ? 'FALSE':'TRUE';
    $rv = DB()->db()->exec("UPDATE fraud_control.authorized_cc SET auth_revoked= $auth, notes= $notes WHERE ccnum= $id");
    if ($rv == 1) {
        $messages[] = "Authorization toggle successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Authorization toggle unsuccessful: " . $err[2];
    }
}

function GetList()
{
    return DB()->fetchList_assoc("
        SELECT fc.ccnum, fc.auth_revoked, fc.notes, fc.stamp, fc.operator
        FROM fraud_control.authorized_cc fc
        ORDER BY fc.ccnum");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fraud Control - Authorized Credit Cards</title>
<script type="text/javascript" src="/js/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="/css/admin/generic-report.css" />
<style type="text/css">
a.rvm {
	font-style:italic;
}
.aok {
	color:green;
}
.nok {
	color:red;
}
</style>
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<p>
This is the list of credit cards registered for use by arbitrary parties in <a href="countries.php">Fraud Control Restricted Countries</a>. 
</p>
<p>
<a href="" onclick="$('#addFrm').toggle(); return false;" class="fpnotes">Add a record:</a>
</p>
<form action="" method="get" id="addFrm" style="display:none">
Credit Card Number: <input type="text" name="id" />
&nbsp;&nbsp;&nbsp;Notes: <input name="notes" type="text" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="add" />
</form>
<?php
$list = GetList();
if ($list) {
    echo '<table class="report"><tr><th>Card Number</th><th><a href="" title="Click the flag to toggle authorization" onclick="return false">Auth Revoked?</a></th>
        <th>Notes</th><th>Timestamp</th><th>Operator</th><th>Action</th></tr>';
    $trclass = 'a';
    foreach ($list as $item) {
//        $link = "toggleAuthFrm(this,'{$item['ccnum']}');return false;";
        $auth = $item['auth_revoked'] ? "<a class=\"aok tauth\" href=\"#\" rel=\"{$item['ccnum']}\">Yes</a>" : "<a class=\"nok tauth\" href=\"#\" rel=\"{$item['ccnum']}\">No</a>";
        echo <<<TR
            <tr class="$trclass">
            <td>{$item['ccnum']}</td>
            <td class="alc">$auth</td>
            <td>{$item['notes']}</td>
            <td>{$item['stamp']}</td>
            <td>{$item['operator']}</td>
            <td><a class="rvm" href="?action=remove;id={$item['ccnum']}">Remove</a></td>
            </tr>
TR;
        $trclass = $trclass == 'a' ? 'b':'a';
    }
    echo '</table>';
} else {
    echo "<h4>The list is empty</h4>";
}
?>
<form id="toggleauth" method="get" action="" style="display: none; padding: 1em; background-color: #cfc; border: 2px solid grey;">
<input type="hidden" name="id" id="toggleauth_id" value="" />
Notes: <input type="text" name="notes" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="toggleauth" />
</form>
<script type="text/javascript">
$('.tauth').click( function(event)
{
	$('#toggleauth_id').val($(event.target).attr('rel'));
	$("#toggleauth").css( {position:"absolute", top: event.pageY -10, left: event.pageX + 30});
	$("#toggleauth").toggle();
});
</script>
</body></html>
