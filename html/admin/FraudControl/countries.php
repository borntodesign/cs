<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$errors = array();
$messages = array();
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : '';
$id = strtoupper(trim( isset($_GET['id']) ? htmlspecialchars($_GET['id']) : '' ));
switch ($action) {
    case 'add':
        Add();
        break;
    case 'remove':
        Delete();
        break;
    default:
        //;
}

function Add()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No country code parameter received';
        return;
    }
    $id = DB()->db()->quote($id);
    $notes = isset($_GET['notes']) ? htmlspecialchars($_GET['notes']) : null;
    $notes = $notes ? DB()->db()->quote($notes) : 'NULL';

    $rv = DB()->db()->exec("INSERT INTO fraud_control.controlled_cc_countries (country_code, notes) VALUES ($id, $notes)");
    if ($rv == 1) {
        $messages[] = "Record addition successful";
    } else {
        $errors[] = "Record addition unsuccessful";
    }
}

function Delete()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No country code parameter received';
        return;
    }
    $id = DB()->db()->quote($id);
    $rv = DB()->db()->exec("DELETE FROM fraud_control.controlled_cc_countries WHERE country_code= $id");
    if ($rv == 1) {
        $messages[] = "Delete successful";
    } else {
        $errors[] = "Delete unsuccessful";
    }
}

function GetList()
{
    return DB()->fetchList_assoc("
        SELECT cc.country_code, cc.country_name, fc.notes, fc.stamp, fc.operator
        FROM fraud_control.controlled_cc_countries fc
        JOIN tbl_countrycodes cc
            ON cc.country_code=fc.country_code
        ORDER BY cc.country_code");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fraud Control - Members Approved</title>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin/generic-report.css" />
<style type="text/css">
a.rvm {
	font-style:italic;
}
</style>
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<p>
This is the list of Fraud Control Restricted Countries.
Members living in or using cards from these countries either have to have the
<a href="cards.php">card number pre-approved</a> or be <a href="members.php">pre-approved themselves</a>.
</p>
<p>
<a href="" onclick="$('#addFrm').toggle(); return false;" class="fpnotes">Add a record:</a>
</p>
<form action="" method="get" id="addFrm" style="display:none">
Country Code: <input type="text" name="id" />
&nbsp;&nbsp;&nbsp;Notes: <input name="notes" type="text" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="add" />
</form>
<?php
$list = GetList();
if ($list) {
    echo '<table class="report"><tr><th>Country Code</th><th>Name</th><th>Notes</th><th>Timestamp</th><th>Operator</th><th>Action</th></tr>';
    $trclass = 'a';
    foreach ($list as $item) {
        echo <<<TR
            <tr class="$trclass">
            <td class="alc">{$item['country_code']}</td>
            <td>{$item['country_name']}</td>
            <td>{$item['notes']}</td>
            <td>{$item['stamp']}</td>
            <td>{$item['operator']}</td>
            <td class="alc"><a class="rvm" href="?action=remove;id={$item['country_code']}">Remove</a></td>
            </tr>
TR;
        $trclass = $trclass == 'a' ? 'b':'a';
    }
    echo '</table>';
} else {
    echo "<h4>The list is empty</h4>";
}
?>

</body></html>
