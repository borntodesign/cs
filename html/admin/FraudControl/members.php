<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$errors = array();
$messages = array();
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : '';
$id = strtoupper(trim( isset($_GET['id']) ? htmlspecialchars($_GET['id']) : '' ));
switch ($action) {
    case 'add':
        Add();
        break;
    case 'remove':
        Delete();
        break;
    default:
        //;
}

function Add()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No ID parameter received';
        return;
    } elseif(preg_match('/\D/', $id)) {
        $id = DB()->selectrow_list("SELECT id FROM members WHERE alias= ?", array($id));
    }
    $notes = isset($_GET['notes']) ? htmlspecialchars($_GET['notes']) : null;
    $notes = $notes ? DB()->db()->quote($notes) : 'NULL';

    $rv = DB()->db()->exec("INSERT INTO fraud_control.authorized_members (id, notes) VALUES ($id, $notes)");
    if ($rv == 1) {
        $messages[] = "Record addition successful";
    } else {
        $errors[] = "Record addition unsuccessful";
    }
}

function Delete()
{
    global $id, $errors, $messages;
    if (! $id) {
        $errors[] = 'No ID parameter received';
        return;
    } elseif(preg_match('/\D/', $id)) {
        $id = DB()->selectrow_list("SELECT id FROM members WHERE alias= ?", array($id));
    }
    
    $rv = DB()->db()->exec("DELETE FROM fraud_control.authorized_members WHERE id= $id");
    if ($rv == 1) {
        $messages[] = "Delete successful";
    } else {
        $errors[] = "Delete unsuccessful";
    }
}

function GetList()
{
    return DB()->fetchList_assoc("
        SELECT m.id, m.alias, m.firstname||' '||m.lastname AS name, fc.notes, fc.stamp, fc.operator
        FROM fraud_control.authorized_members fc
        JOIN members m
            ON m.id=fc.id
        ORDER BY m.alias");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fraud Control - Members Approved</title>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/admin/generic-report.css" />
<style type="text/css">
a.rvm {
	font-style:italic;
}
</style>
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<p>
This is the list of members, who live in <a href="countries.php">Fraud Control Restricted Countries</a>,
who can freely use a credit card without having it entered in the <a href="cards.php">Fraud Control Authorized Credit Cards list</a>. 
</p>
<p>
<a href="" onclick="$('#addFrm').toggle(); return false;" class="fpnotes">Add a record:</a>
</p>
<form action="" method="get" id="addFrm" style="display:none">
Member ID: <input type="text" name="id" />
&nbsp;&nbsp;&nbsp;Notes: <input name="notes" type="text" style="width:30em;" />
<input type="submit" value="Submit" />
<input type="hidden" name="action" value="add" />
</form>
<?php
$list = GetList();
if ($list) {
    echo '<table class="report"><tr><th>ID</th><th>Name</th><th>Notes</th><th>Timestamp</th><th>Operator</th><th>Action</th></tr>';
    $trclass = 'a';
    foreach ($list as $item) {
        echo <<<TR
            <tr class="$trclass">
            <td>{$item['alias']}</td>
            <td>{$item['name']}</td>
            <td>{$item['notes']}</td>
            <td>{$item['stamp']}</td>
            <td>{$item['operator']}</td>
            <td><a class="rvm" href="?action=remove;id={$item['id']}">Remove</a></td>
            </tr>
TR;
        $trclass = $trclass == 'a' ? 'b':'a';
    }
    echo '</table>';
} else {
    echo "<h4>The list is empty</h4>";
}
?>

</body></html>
