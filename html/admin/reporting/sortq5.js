var sort_direction = 'desc';
var sortcol;

function sort(a)
{
	var reference_idx;
	var newlist = new Array;
	var mycol = a.parentNode;	// this is a th cell
	var cols = mycol.parentNode.getElementsByTagName('th');	//these are the th family under the header row
	for (var x=0; x < cols.length; x++){
		if (mycol == cols[x]){
			if (sortcol != undefined && sortcol == x){
				sort_direction = (sort_direction == 'desc' ? 'asc':'desc');
				sortcol *= 1;
			}
			else {sortcol = x; }
			break;
		}
	}
	var tbl = document.getElementById('data_table');
	var rows = new Array;
	var _rows = tbl.getElementsByTagName('tr');
	for (var x=0; x < _rows.length; x++){
		rows.push(_rows[x]);
	}
	_rows = null;
	while (rows.length > 0){
	  var reference_val = '';
	  for (var x=0; x < rows.length; x++){
		var sortval = rows[x].getElementsByTagName('td')[sortcol].innerHTML;
		if (! isNaN(sortval)){ sortval *= 1; }

		if (sort_direction == 'desc'){
			if (sortval >= reference_val){
				reference_val = sortval;
				reference_idx = x;
				newlist.push(rows[x]);
			}
		} else {
			if (reference_val == ''){ reference_val = sortval; }
			if (sortval <= reference_val){
				reference_val = sortval;
				reference_idx = x;
				newlist.push(rows[x]);
			}
		}
	  }
	  rows.splice(reference_idx, 1);
	}

	while(tbl.childNodes.length > 0){
		tbl.removeChild( tbl.lastChild );
	}
	var rclass = 'a';
	for (var x=0; x < newlist.length; x++){
//if (x<10){console.log('rclass='+rclass+' className='+newlist[x].className+' val='+newlist[x].firstChild.innerHTML);}
		newlist[x].className = rclass;
//if (x<10){console.log(newlist[x].className);}
		rclass = rclass == 'a' ? 'b':'a';
		tbl.appendChild(newlist[x]);
	}
}

function initRows(){
	var rclass = 'a';
	var trs = document.getElementById('data_table').getElementsByTagName('tr');
	for (var x=0; x < trs.length; x++){
		trs[x].className = rclass;
                rclass = rclass == 'a' ? 'b':'a';
	}
}

initRows();
