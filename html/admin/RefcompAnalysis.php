<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$monthsEnding = array();
$sth = DB()->prepareXSth('SELECT "close" FROM periods WHERE "type"=1 AND "open" >= ? AND "close" IS NOT NULL ORDER BY "close"', array('01-01-15'));
while (($col = $sth->fetchColumn()) != false) {
    $monthsEnding[] = $col;
}

$eom = '';
if (isset($_GET['eom'])) {
    $eom = $_GET['eom'];
}
if (! $eom || ! in_array($eom, $monthsEnding)) {
    $eom = DB()->fetchSingleCol("SELECT first_of_month() -1");
}

$schema = 'cgi_' . preg_replace('/-/', '_', $eom);
$rows = DB()->fetchList_assoc("
    WITH rund AS (SELECT MAX(run_date) AS run_date FROM archives.refcomp_me_dates WHERE commission_month= '$eom'),
    sdates AS (SELECT open, close FROM periods WHERE type=1 AND close= '$eom'),
    calcrefcomp AS (SELECT r.* FROM archives.refcomp_me_batches r JOIN rund ON rund.run_date=r.run_date),
    
    parts AS (
    SELECT r.a_level, r.prvs, r.my50, r.my25, r.plpp, r.minimum_commission_guarantee AS mcg, r.id,
        SUM(s.usd)::NUMERIC(7,2) AS pkg_price
    
    FROM calcrefcomp r
    JOIN sdates ON 1=1
    JOIN $schema.members m ON m.id=r.id
--    JOIN level_values lv ON lv.lvalue=r.a_level
    LEFT JOIN subscriptions_history s
        ON r.id=s.member_id
--        AND s.stamp::DATE BETWEEN sdates.open AND sdates.close
        AND s.start_date::DATE BETWEEN sdates.open AND sdates.close
        AND s.void=FALSE
    
    LEFT JOIN configurations.placeholder_partners c ON c.id=r.id
    WHERE c.id IS NULL
    AND m.membertype='v'
    GROUP BY r.a_level, r.prvs, r.my50, r.my25, r.plpp, r.minimum_commission_guarantee, r.id
    ),
    
    indv AS (
    SELECT   
        lv.lvalue AS pl,
        lv.lname,
        COUNT(r.*) AS num_at_lvl,
        AVG(r.prvs)::NUMERIC(3,1) AS avg_prvs,
        SUM(r.my50) AS refcom,
        AVG(r.my50)::NUMERIC(6,2) AS avg_refcom,
        SUM(r.my25) AS teamcom,
        AVG(r.my25)::NUMERIC(6,2) AS avg_teamcom,
        SUM(r.plpp) AS xbonus,
        AVG(r.plpp)::NUMERIC(7,2) AS avg_xbonus,
        SUM(r.mcg) AS mcg,
        AVG(r.mcg)::NUMERIC(6,2) AS avg_mcg,
        SUM(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg)::NUMERIC(8,2) AS ttl_for_lvl,
        AVG(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg)::NUMERIC(6,2) AS avg_gross_pm,
        AVG(r.pkg_price)::NUMERIC(6,2) AS avg_pkg_price,
        SUM(r.pkg_price) AS lvl_pkg_ttl,
        AVG(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg - r.pkg_price)::NUMERIC(7,2) AS avg_net_pm,
        ((SUM(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg)/SUM(r.pkg_price))*100)::NUMERIC(7,2) AS payout_perc
    FROM parts r
    JOIN level_values lv
        ON r.a_level=lv.lvalue
    GROUP BY lv.lname, lv.lvalue
    ORDER BY lv.lvalue
    ),
    ttls AS (
    SELECT    NULL::SMALLINT AS pl,
        'TOTALS'::TEXT AS lname,
        COUNT(r.*),
        NULL::NUMERIC AS avg_prvs,
        SUM(r.my50) AS refcom,
        NULL::NUMERIC AS avg_refcom,
        SUM(r.my25) AS teamcom,
        NULL::NUMERIC AS avg_teamcom,
        SUM(r.plpp) AS xbonus,
        NULL::NUMERIC AS avg_xbonus,
        SUM(r.mcg) AS mcg,
        NULL::NUMERIC AS avg_mcg,
        SUM(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg)::NUMERIC(8,2) AS ttl_for_lvl,
        NULL::NUMERIC(8,2) AS ttl_gross_pm,
--        SUM(r.pkg_price)::NUMERIC(8,2) AS ttl_pkg_price,
        NULL::NUMERIC AS avg_pkg_price,
        SUM(r.pkg_price) AS lvl_pkg_ttl,
        SUM(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg - r.pkg_price)::NUMERIC(8,2) AS ttl_net_pm,
        ((SUM(r.my50 + r.my25 + COALESCE(r.plpp,0) + r.mcg)/SUM(r.pkg_price))*100)::NUMERIC(7,2) AS payout_perc
    FROM parts r
    )
    SELECT * FROM indv
    UNION
    SELECT * FROM ttls
    ORDER BY pl ASC NULLS LAST
");

?>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/generic-report.css" />
<script type="text/javascript" src="/js/jquery.min.js"></script>
<style type="text/css">
/*table.report {font-size:110%}*/
</style>
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get">
<select name="eom">
<?php foreach ($monthsEnding AS $month):?>
<option value="<?php echo $month?>"<?php if ($month == $eom){ echo ' selected="selected"';}?>><?php echo $month?></option>
<?php endforeach;?>
</select>
<input type="submit" value="Go" style="margin-left: 1em" />
</form>
<div class="fpnotes"><b>Doing analysis on this month: <?=$eom?></b><br/>This analysis will not be accurate until after fees are run</div>
<table class="report"><thead>
    <tr>
		<th>pl</th>
		<th>lname</th>
		<th>Number at level</th>
		<th>Avg PRVs</th>
		<th>Referral Comm</th>
		<th>Avg Referral Comm</th>
		<th>Team Comm</th>
		<th>Avg Team Comm</th>
		<th>xbonus</th>
		<th>Avg xbonus</th>
		<th>MCG</th>
		<th>Avg MCG</th>
		<th>Total for level</th>
		<th>Avg Gross PM</th>
		<th>Avg Pkg Price</th>
		<th>Lvl Pkg Ttl</th>
		<th>Avg Net PM</th>
		<th>Payout Percentage</th>
	</tr>
</thead>
<tbody id="data_table">
<?php $myc = 'a'; foreach ($rows as $row):?>
<tr class="<?=$myc?>">
    <td><?=$row['pl']?></td>
    <td><?=$row['lname']?></td>
    <td><?=$row['num_at_lvl']?></td>
    <td><?=$row['avg_prvs']?></td>
    <td><?=$row['refcom']?></td>
    <td><?=$row['avg_refcom']?></td>
    <td><?=$row['teamcom']?></td>
    <td><?=$row['avg_teamcom']?></td>
    <td><?=$row['xbonus']?></td>
    <td><?=$row['avg_xbonus']?></td>
    <td><?=$row['mcg']?></td>
    <td><?=$row['avg_mcg']?></td>
    <td><?=$row['ttl_for_lvl']?></td>
    <td><?=$row['avg_gross_pm']?></td>
    <td><?=$row['avg_pkg_price']?></td>
    <td><?=$row['lvl_pkg_ttl']?></td>
    <td><?=$row['avg_net_pm']?></td>
    <td><?=$row['payout_perc']?></td>
</tr>
<?php $myc = $myc == 'a' ? 'b':'a'; endforeach;?>
</tbody>
</table>
</body></html>