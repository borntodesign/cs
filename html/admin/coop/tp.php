<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$errors = array();
$messages = array();
$params = array();

LoadParams();
if (! $errors) {
    switch ($params['action']) {
        case 'add':
            Add();
            break;
        case 'delete':
            Delete();
            break;
        case 'update':
            Update();
        default:
            //;
    }
}

$coops = DB()->fetchAll_assoc("
    SELECT
        c.coop_id,
        c.description,
        COALESCE(nps.pid::TEXT,'') AS pid,
        COALESCE(pns.notes,'') AS pid_description
    FROM coop.coops c
    LEFT JOIN network.pspids nps
        ON nps.id=c.coop_id
    LEFT JOIN network.partner_network_separators pns
        ON pns.pid=nps.pid
    WHERE c.house_acct=TRUE
    ORDER BY coop_id", 'coop_id');
$language_codes = DB()->fetchAll_assoc("SELECT code2, description FROM language_codes WHERE active=TRUE ORDER BY description", 'code2');
$country_codes = DB()->fetchAll_assoc("SELECT country_code, country_name FROM tbl_countrycodes WHERE active=TRUE ORDER BY country_name", 'country_code');
$assignments = DB()->fetchList_assoc("
    SELECT
        coop_id,
        language_code,
        country_code,
        COALESCE(notes,'') AS notes,
        pk,
        parent_coop_id,
        COALESCE(stamp::TEXT,'') AS stamp,
        COALESCE(operator,'') AS operator
    FROM coop.tp_container_assignment
    ORDER BY parent_coop_id, country_code ASC NULLS LAST, language_code ASC NULLS LAST");

function Add()
{
    global $errors, $messages;
    $vals = array();
    $pk = DB()->fetchSingleCol('SELECT MAX(pk) +1 FROM coop.tp_container_assignment');
    
    $p = array();
    foreach (array('notes','country_code','language_code','coop_id','parent_coop_id') AS $k) {
        $vals[$k] = DbQuoteOrNull($k);
    }

    $rv = DB()->db()->exec("
        INSERT INTO coop.tp_container_assignment (pk, parent_coop_id, country_code, language_code, coop_id, notes)
        VALUES ($pk, {$vals['parent_coop_id']}, {$vals['country_code']}, {$vals['language_code']}, {$vals['coop_id']}, {$vals['notes']})
    ");
    if ($rv == 1) {
        $messages[] = "Record addition successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Record addition unsuccessful: " . $err[2];
    }
}

function Delete()
{
    global $params, $errors, $messages;
    if (! $params['pk']) {
        $errors[] = 'No pk parameter received';
        return;
    }

    $rv = DB()->db()->exec("DELETE FROM coop.tp_container_assignment WHERE pk= {$params['pk']}");
    if ($rv == 1) {
        $messages[] = "Delete successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Delete unsuccessful: " . $err[2];
    }
}

function Update()
{
    global $params, $errors, $messages;
    if (! $params['pk']) {
        $errors[] = 'No pk parameter received';
        return;
    }
    
    $qry = 'UPDATE coop.tp_container_assignment SET';
    $p = array();
    foreach (array('notes','country_code','language_code','coop_id','parent_coop_id') AS $k) {
        if (isset($params[$k])) {
            $p[$k] = DbQuoteOrNull($k);
        }
    }
    if (! $p['notes']) $p['notes'] = 'NULL';
    foreach ($p AS $key => $val) {
        $qry .= " $key = $val,";
    }
    
    $rv = DB()->db()->exec( rtrim($qry, ',') . " WHERE pk= {$params['pk']}");
    if ($rv == 1) {
        $messages[] = "Update successful";
    } else {
        $err = DB()->db()->errorInfo();
        $errors[] = "Update unsuccessful: " . $err[2];
    }
}

function DbQuoteOrNull($key)
{
    global $params;
    return $params[$key] ? DB()->db()->quote($params[$key]) : 'NULL';
}

function LoadParams()
{
    global $params;
    foreach (array('notes','country_code','language_code','coop_id','parent_coop_id','action','pk') AS $k) {
        if (array_key_exists($k, $_GET) && $_GET[$k]) $params[$k] = htmlspecialchars($_GET[$k]);
    }
    
    // these should be integers only
    foreach (array('coop_id','parent_coop_id','pk') AS $k) {
        if (isset($params[$k]) && preg_match('/\D/', $params[$k])) {
            $errors[] = "$k should be numeric only: {$params[$k]}";
        }
    }

    // we want action to equal an empty instead of null
    if (! isset($params['action'])) $params['action'] = '';
}

$pageTitle = 'Co-Op Management: Trial Partner Container Assignment';
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $pageTitle?></title>
<script type="text/javascript" src="/js/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="/css/admin/generic-report.css" />
<style type="text/css">
.input_notes {
	width: 40em;
}
.btn_update, .btn_add {
	color: green;
}
.btn_delete {
	color: red;
}
td {
	vertical-align:middle;
}
textarea.input_notes {
	height: 2.5em;
	width: 50em;
}
select.coop_id {
	font-size:100%;
	color: navy;
}
</style>
<script type="text/javascript">
function deleteSubmit(btn) {
	tr = btn.closest('tr');
	$('#mainfrm [name="pk"]').val( $(tr).find('[name="pk"]').val() );
	$('#mainfrm [name="action"]').val('delete');
	$('#mainfrm').submit();
}

function updateSubmit(btn) {
	tr = btn.closest('tr');
	$('#mainfrm [name="pk"]').val( $(tr).find('[name="pk"]').val() );
	$('#mainfrm [name="coop_id"]').val( $(tr).find('[name="coop_id"]').val() );
	$('#mainfrm [name="notes"]').val( $(tr).find('[name="notes"]').val() );
	$('#mainfrm [name="action"]').val('update');
	$('#mainfrm').submit();
}

function addSubmit(btn) {
	tr = btn.closest('tr');
	$('#mainfrm [name="country_code"]').val( $(tr).find('[name="country_code"]').val() );
	$('#mainfrm [name="language_code"]').val( $(tr).find('[name="language_code"]').val() );
	$('#mainfrm [name="coop_id"]').val( $(tr).find('[name="coop_id"]').val() );
	$('#mainfrm [name="parent_coop_id"]').val( $(tr).find('[name="parent_coop_id"]').val() );
	$('#mainfrm [name="notes"]').val( $(tr).find('[name="notes"]').val() );
	$('#mainfrm [name="action"]').val('add');
	$('#mainfrm').submit();
}

$(document).ready(function() {

	$(".btn_update").click(function(){
		updateSubmit(this);
	});
	
	$(".btn_delete").click(function(){
		deleteSubmit(this);
	});

	$(".btn_add").click(function(){
		addSubmit(this);
	});
});	
</script>
</head>
<body>
<?php
    foreach ($messages as $message) {
        echo '<div class="message">' . $message . '</div>';
    }
    foreach ($errors as $error) {
        echo '<div class="error">' . $error . '</div>';
    }
?>
<div style="position:relative; float:right; top:10px; right:10px; width: 30%; background-color: #efefef;">
<span class="fpnotes">Notes: Rules are applied in the order shown.</span>
<ul class="fpnotes"><li>Country match takes precedence over language.</li>
<li>An empty column matches everything</li>
<li>Once a row matches, the TP ends up in the destination</li>
</ul>

Co-Op Descriptions - <a href="#" class="toggle_coop_descriptions">Toggle</a>
    <div style="position: absolute; display:none; padding:1em; background-color: #efefef;" id="coop_descriptions">
    <table class="report"><tr><th>Co-Op ID</th><th>Pool ID/Taproot</th><th>Co-Op Description</th></tr>
    <?php foreach ($coops AS $coop):?>
        <tr><td class="alr">
            <a href="https://www.clubshop.com/cgi/admin/memberinfo.cgi?id=<?=$coop['coop_id']?>" target="_blank"><?=$coop['coop_id']?></a>
            </td>
            <td><?=$coop['pid']?> - <?=$coop['pid_description']?></td>
            <td><?=$coop['description']?></td>
        </tr>
    <?php endforeach;?>
    </table></div>
</div>
<h4><?php echo $pageTitle?></h4>

<table class="report" style="font-size: 110%">
<thead><tr><th>Source Co-Op</th><th>Country</th><th>Language</th><th>Destination</th><th>Notes</th><th>Stamp</th><th>Operator</th><th></th></tr></thead>
<tbody>
<?php foreach ($assignments AS $coop):?>
<tr id="tr<?=$coop['pk']?>">
<td><a href="#" class="toggle_coop_descriptions"><?=$coop['parent_coop_id']?></a></td>
<td><?=isset($country_codes[$coop['country_code']]) ? $country_codes[$coop['country_code']]['country_name'] : ''?></td>
<td><?=isset($language_codes[$coop['language_code']]) ? $language_codes[$coop['language_code']]['description'] : ''?></td>
<td><select name="coop_id" class="coop_id">
<?php foreach ($coops AS $c):?>
<option value="<?=$c['coop_id']?>"<?php if ($c['coop_id'] == $coop['coop_id']) echo ' selected="selected"'?>><?=$c['coop_id']?></option>
<?php endforeach;?>
</select></td>
<td><textarea class="input_notes" name="notes"><?=$coop['notes']?></textarea></td>
<td><?=$coop['stamp']?></td>
<td><?=$coop['operator']?></td>
<td><input type="hidden" name="pk" value="<?=$coop['pk']?>" />
    <button type="submit" class="btn_update" name="action" value="update">Update</button>
    <button type="submit" class="btn_delete" name="action" value="delete">Delete</button>
</td>
</tr>
<?php endforeach;?>
</tbody>
<tbody>
<tr><th colspan="8">Create a New Entry</th></tr>
<tr>

<td colspan="8" style="vertical-align: bottom; font-size:70%;">
<select name="parent_coop_id" class="coop_id">
<option value="">Source Co-Op</option>
<?php foreach ($coops AS $c):?>
<option value="<?=$c['coop_id']?>"><?=$c['coop_id']?></option>
<?php endforeach;?>
</select>

<select name="country_code" class="coop_id">
<option value="">Country</option>
<?php foreach ($country_codes AS $c):?>
<option value="<?=$c['country_code']?>"><?=$c['country_name']?></option>
<?php endforeach;?>
</select>

<select name="language_code" class="coop_id">
<option value="">Language</option>
<?php foreach ($language_codes AS $c):?>
<option value="<?=$c['code2']?>"><?=$c['description']?></option>
<?php endforeach;?>
</select>

<select name="coop_id" class="coop_id">
<option value="">Destination Co-Op</option>
<?php foreach ($coops AS $c):?>
<option value="<?=$c['coop_id']?>"><?=$c['coop_id']?></option>
<?php endforeach;?>
</select>

Notes: <textarea class="input_notes" name="notes" style="margin-bottom:-5px; width: 35em;"></textarea>

<button type="submit" class="btn_add" name="action" value="delete">Create</button>
</td>
</tr>
</tbody>
</table>
<form action="" method="get" id="mainfrm">
<input type="hidden" name="action" value="" />
<input type="hidden" name="pk" value="" />
<input type="hidden" name="coop_id" value="" />
<input type="hidden" name="language_code" value="" />
<input type="hidden" name="country_code" value="" />
<input type="hidden" name="notes" value="" />
<input type="hidden" name="parent_coop_id" value="" />
</form>
<script type="text/javascript">
$('.toggle_coop_descriptions').click( function(event)
{
	$("#coop_descriptions").toggle();
});
</script>
</body>
</html>