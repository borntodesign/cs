<?php
use Zend\Form\Element\Checkbox;
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDB.php';

$operator = htmlspecialchars($_COOKIE['operator']);
if (preg_match('/[^a-zA-Z0-9\.]/', $operator)) {
    die("The username looks bad: $operator");
}
$pwd = htmlspecialchars($_COOKIE['pwd']);

try {
    $dbi = new \Clubshop\AdminDB($operator, $pwd);
} catch (\Exception $e) {
    die("DB connect failed: " . $e->getMessage());
}

function paramVal($key)
{
    $rv = htmlspecialchars(isset($_GET[$key]) ? $_GET[$key] : '');
    return $rv;
}

function Checkbox($paramName, $row)
{
    if ($row[$paramName]) {
        return "class=\"active\" checked=\"checked\" onchange=\"makeChange('deactivate','$paramName',{$row['id']})\"";
    } else {
        return "class=\"notactive\" onchange=\"makeChange('activate','$paramName',{$row['id']})\"";
    }
}

$errors = array();
$messages = array();
$params = array();

// take care of our actions before running the query
$action = paramVal('action');
$field = paramVal('field');
$trans_type = paramVal('trans_type');
if (isset($action) && isset($field) && isset($trans_type) && ($action == 'activate' || $action == 'deactivate'))  {
    $rv = 0;
    $val = $action == 'activate' ? 'TRUE' : 'FALSE';
    $dbi->db()->beginTransaction();

    if (preg_match('/^\d+$/', $trans_type) && in_array($field, array('admin_interface_available', 'comm_disbursement_related', 'transferrable', 'can_withdraw', 'cash_inflow'))) {
        $rv = $dbi->db()->exec("UPDATE soa_transaction_types SET $field= $val WHERE id= $trans_type");
    }
    
    if ($rv === false || $rv == 0) {
        $errors[] = 'Update failed';
        $dbi->db()->rollBack();
    } elseif ($rv == 1) {
        $messages[] = 'Update successful';
        $dbi->db()->commit();
    } elseif ($rv > 1) {
        $dbi->db()->rollBack();
        $errors[] = 'Update of a single record failed'; 
    } else {
        $errors[] = 'Update failed. Unexpected response.';
        $dbi->db()->rollBack();
    }
}

$qry_start = '
    SELECT id, name, description, admin_interface_available, comm_disbursement_related, transferrable, can_withdraw, cash_inflow,
        soa_fees.fee_label,
        soa_fees.fee_trans_type
    FROM soa_transaction_types stt
    LEFT JOIN soa_fees
        ON soa_fees.trans_type=stt.id
    WHERE 1=1';

foreach (array('admin_interface_available', 'comm_disbursement_related', 'transferrable', 'can_withdraw', 'cash_inflow') AS $key) {
    if (paramVal($key)) {
        $qry_start .= " AND $key =TRUE";
        $params[$key] = 1;
    }
}
$qry_end = ' ORDER BY id';
$data = $dbi->fetchList_assoc("$qry_start $qry_end");
?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<script type="text/javascript" src="/js/jquery.min.js"></script>
<link href="/css/admin/generic-report.css" type="text/css" rel="stylesheet" />
<title>Pay Agent Groups Funds Received</title>
<style type="text/css">
input.active {
    background-color: #cfc;
}
input.notactive {
    background-color: #fcc;
}
table.report {
    font-size: 100%;
}
select,input {
    font-size: 0.85em;
}
#mainFrm {
float: left;
margin-top: 1em;
margin-right: 5em;
text-align: right;
}
#mainFrm * {
margin-bottom: 0.5em;
}
label {
	font-size: smaller;
	margin-right: 2em;
}
thead th.filtered {
	background-color: #cfc;
}
.alc {
	text-align: center;
}
</style>
</head>
<body>
<h4>Club Account Transaction Types</h4>
<?php
foreach ($errors as $item) {
    echo '<div class="error">' . $item . '</div>';
}
foreach ($messages as $item) {
    echo '<div class="message">' . $item . '</div>';
}
?>
<form action="" method="get">
Filters:
<label>Cash Inflow: <input type="checkbox" name="cash_inflow" value="1" /></label>
<label>Admin Interface: <input type="checkbox" name="admin_interface_available" value="1" /></label>
<label>Transferrable: <input type="checkbox" name="transferrable" value="1" /></label>
<label>Comm Disbursement Related: <input type="checkbox" name="comm_disbursement_related" value="1" /></label>
<label>Can Withdraw: <input type="checkbox" name="can_withdraw" value="1" /></label>
<input type="submit" />
</form>

<form id="actionFrm" action="">
<p>Checking/unchecking an item will cause the change to be enacted immediately</p>
<table class="report">
<thead>
<tr>
    <th>ID</th>
    <th>Name</th>
    <th>Description</th>
    <th>Auto Fee</th>
    <th>Fee Trans Type</th>
    <th<?php if (isset($params['admin_interface_available'])) echo ' class="filtered"'?>>Admin Interface</th>
    <th<?php if (isset($params['comm_disbursement_related'])) echo ' class="filtered"'?>>Comm Disbursement Related</th>
    <th<?php if (isset($params['transferrable'])) echo ' class="filtered"'?>>Transferrable</th>
    <th<?php if (isset($params['can_withdraw'])) echo ' class="filtered"'?>>Can Withdraw</th>
    <th<?php if (isset($params['cash_inflow'])) echo ' class="filtered"'?>>Cash Inflow</th>
</tr>
</thead>
<?php $x=1; foreach ($data AS $row):?>
<tr class="<?php echo ($x & 1) ? 'a' : 'b'?>">
    <td><?php echo $row['id']?></td>
    <td><?php echo $row['name']?></td>
    <td><?php echo $row['description']?></td>
    <td><?php echo $row['fee_label']?></td>
    <td class="alc"><?php echo $row['fee_trans_type']?></td>
    <td class="alc"><input type="checkbox" name="" <?php echo Checkbox('admin_interface_available', $row)?>/></td>
    <td class="alc"><input type="checkbox" name="" <?php echo Checkbox('comm_disbursement_related', $row)?>/></td>
    <td class="alc"><input type="checkbox" name="" <?php echo Checkbox('transferrable', $row)?>/></td>
    <td class="alc"><input type="checkbox" name="" <?php echo Checkbox('can_withdraw', $row)?>/></td>
    <td class="alc"><input type="checkbox" name="" <?php echo Checkbox('cash_inflow', $row)?>/></td>
</tr>
<?php $x++; endforeach;?>
</table>
<?php foreach (array('admin_interface_available', 'comm_disbursement_related', 'transferrable', 'can_withdraw', 'cash_inflow') AS $key):?>
<?php if (paramVal($key)):?>
<input type="hidden" name="<?php echo $key?>" value="1" />
<?php endif?>
<?php endforeach;?>
<input id="action" type="hidden" name="action" value="" />
<input id="field" type="hidden" name="field" value="" />
<input id="trans_type" type="hidden" name="trans_type" value="" />
</form>
<script type="text/javascript">
function makeChange(changeType,paramName,trans_type) {
	$('#action').val(changeType);
	$('#field').val(paramName);
	$('#trans_type').val(trans_type);
	$('#actionFrm').submit();
}
</script>
</body>
</html>