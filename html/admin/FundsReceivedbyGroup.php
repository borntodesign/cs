<?php
// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDB.php';

$operator = htmlspecialchars($_COOKIE['operator']);
if (preg_match('/[^a-zA-Z0-9\.]/', $operator)) {
    die("The username looks bad: $operator");
}
$pwd = htmlspecialchars($_COOKIE['pwd']);

try {
    $dbi = new \Clubshop\AdminDB($operator, $pwd);
} catch (\Exception $e) {
    die("DB connect failed: " . $e->getMessage());
}

function paramVal($key) {
    $rv = htmlspecialchars(isset($_GET[$key]) ? $_GET[$key] : '');
    return $rv;
}

$errors = array();
$messages = array();
$rows = array();
$period = array();
$reportee = array();

$months = $dbi->fetchList_assoc('SELECT close FROM periods WHERE type=1 AND close IS NOT NULL ORDER BY close DESC LIMIT 6');
$agents = $dbi->fetchList_assoc("
    SELECT m.id, m.alias, m.firstname ||' '||m.lastname AS name
    FROM members m
    JOIN pay_agent.agents pa
        ON pa.id=m.id
    ORDER BY m.id");

// we need a month and a member ID to query on
$id = paramVal('id');
if ($id) {
    $qry = "SELECT m.id, m.alias, m.firstname, m.lastname FROM members m WHERE ";
    $qry .= preg_match('/\D/', $id) ? "m.alias= ?" : "m.id= ?";
    $reportee = $dbi->selectrow_array($qry, array($id));
}

$month = paramVal('month'); // the correct format is yyyy-mm-dd and should reflect a month ending date
if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $month)) {
    $period = $dbi->selectrow_array("SELECT open, close FROM periods WHERE type=1 AND close= ?", array($month));
    if (empty($period)) $errors[] = "Failed to identify a closed period ending $month";
} else {
    $month = '';
}

if (! empty($reportee) && ! empty($period)) {
    LoadData($reportee, $period);
    if (empty($rows) || count($rows) == 1)              // the query will "always" return one row for TOTALS
        $messages[] = 'Nothing found for this month';
} elseif (! empty($reportee) && empty($period)) {
    $errors[] = 'A month was not selected';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<link href="/css/admin/generic-report.css" type="text/css" rel="stylesheet" />
<title>Pay Agent Groups Funds Received</title>
<style type="text/css">
.itsme {
	background-color: #cfc;
	font-weight: bold;
}
table.report {
	font-size: 130%;
}
select,input {
	font-size: 0.85em;
}
#mainFrm {
	float: left;
	margin-top: 1em;
	margin-right: 5em;
	text-align: right;
}
#mainFrm * {
	margin-bottom: 0.5em;
}
</style>
</head>
<body>
<h4>Pay Agent Groups Funds Received: <? if (! empty($reportee)) echo $reportee['alias'] . ' - ' . $reportee['firstname'] . ' ' . $reportee['lastname'] . " - Month ending: $month"?></h4>
<?php
foreach ($errors as $item) {
    echo '<div class="error">' . $item . '</div>';
}
foreach ($messages as $item) {
    echo '<div class="message">' . $item . '</div>';
}
?>
<form action="" id="mainFrm">

<select onchange="document.getElementById('idparam').value=this.options[this.selectedIndex].value">
<option value="">Select Pay Agent or enter ID below</option>
<?php foreach ($agents AS $agent):?>
<option value="<?=$agent['id']?>"<?php if ($agent['id'] == $id) echo ' selected="selected"'?>><?=$agent['alias'] . ' - ' . $agent['name']?></option>
<?php endforeach;?>
</select><br/>

<input id="idparam" type="text" name="id" placeholder="Member ID" autofocus="autofocus" required="required" size="10" value="<?=$id?>" /><br/>

<select name="month" required="required">
<option value="">Month Ending</option>
<?php foreach ($months AS $m):?>
<option value="<?=$m['close']?>"<?php if ($m['close'] == $month) echo ' selected="selected"'?>><?=$m['close']?></option>
<?php endforeach;?>
</select><br/>

<input type="submit" value="Search" />

</form>
<table class="report">
<?php
    $rc = 'a';
    foreach ($rows AS $row):
    $qs = http_build_query(array(
        'bm' => substr($period['open'], 0, 7),
        'em' => substr($period['close'], 0, 7),
        'action' => 'FMON',
        'id' => $row['id']
        ));
?>
<tr class="<?php echo $row['id'] == $reportee['id'] ? 'itsme' : $rc?>">
    <td><a href="/cgi/admin/SOAadmin.cgi?<?=$qs?>"><?=$row['alias']?></a></td>
    <td><?=$row['firstname'] . ' ' . $row['lastname']?></td>
    <td class="alr"><?=$row['ttl']?></td>
</tr>
<?php
    $rc = $rc == 'a' ? 'b':'a';
    endforeach;?>
</table>
</body>
</html>
<?php 
function LoadData($reportee, $period)
{
    global $dbi, $errors, $rows;
    $schema = 'cgi_' . preg_replace('/-/', '_', $period['close']);
    
    // first off see if we are an Exec in that month
    $test = $dbi->selectrow_array("
        SELECT id, a_level
        FROM archives.refcomp_me_batches rmb
	    JOIN archives.refcomp_me_dates rmd
          ON rmd.run_date=rmb.run_date
		  AND rmd.commission_month= ?
	    WHERE id= ?
        ORDER BY rmb.run_date DESC LIMIT 1
        ", array($period['close'], $reportee['id']));
    
    if (! isset($test)) {
        $errors[] = "Could not find a pay record for {$reportee['alias']} in month ending {$period['close']}";
        return;
    }
    
    if ($test['a_level'] < 11) {
        $qry = "
            WITH
            myexec AS (SELECT upline FROM $schema.relationships WHERE id= {$reportee['id']} AND x1=TRUE),
            rc AS (
            	SELECT DISTINCT ON (rmb.id) rmb.id, rmb.a_level    /* this provides a list of all the Execs for that month, that will be excluded in the next CTE */
            	FROM archives.refcomp_me_batches rmb
            	JOIN archives.refcomp_me_dates rmd
            		ON rmd.run_date=rmb.run_date
            	WHERE rmb.a_level >= 11
            	AND rmd.commission_month= '{$period['close']}'
            	ORDER BY id, rmb.run_date DESC
            ),
            xt AS (
            	SELECT DISTINCT r.id           /* this generates a list of parties who had my Exec as their Exec */
            	FROM $schema.relationships r
            	JOIN myexec
            	   ON myexec.upline=r.upline
            	LEFT JOIN rc
            		ON rc.id=r.id
            	WHERE rc.id IS NULL	-- the downline were not an Exec themselves in that month
            	AND r.x1=TRUE
            ),
            x AS (
            	SELECT DISTINCT s.id, s.amount, s.trans_id
            	FROM soa s
            	JOIN soa_transaction_types st
            		ON st.id=s.trans_type
            	JOIN $schema.relationships r
            		ON r.id=s.id
            	JOIN xt
            		ON xt.id=r.id
            	LEFT JOIN pay_agent.agent_history pah
            	   ON pah.active_month= '{$period['open']}'
            	   AND pah.agent_id=r.id
            	   AND pah.agent_id != {$reportee['id']}   /* the party being reported on could have been an agent in that month */
            	WHERE s.void=FALSE
            	AND st.cash_inflow=TRUE
            	AND (r.upline= {$reportee['id']} OR r.id= {$reportee['id']})
            	AND s.entered::DATE BETWEEN '{$period['open']}' AND '{$period['close']}'
            	AND pah.agent_id IS NULL /* the particular downline party was not an agent that month, per Dick Oct 2015 */
            )
            SELECT x.id::TEXT AS id, SUM(x.amount) AS ttl, m.alias, m.firstname, m.lastname
            FROM x
            JOIN members m
                ON m.id=x.id
            GROUP BY x.id, m.alias, m.firstname, m.lastname
            UNION
            SELECT 'TOTALS'::TEXT AS id, SUM(x.amount) AS ttl, '' AS alias, '', 'TOTALS'::TEXT AS lastname
            FROM x
            ORDER BY id";
    } else {
        $qry = "
            WITH
            rc AS (
            	SELECT DISTINCT ON (rmb.id) rmb.id, rmb.a_level    /* this provides a list of all the Execs for that month, that will be excluded in the next CTE */
            	FROM archives.refcomp_me_batches rmb
            	JOIN archives.refcomp_me_dates rmd
            		ON rmd.run_date=rmb.run_date
            	WHERE rmb.a_level >= 11
            	AND rmd.commission_month= '{$period['close']}'
            	ORDER BY rmb.id, rmb.run_date DESC
            ),
            x AS (
            	SELECT DISTINCT r.id, s.amount, s.trans_id
            	FROM soa s
            	JOIN soa_transaction_types st
            		ON st.id=s.trans_type
            	JOIN $schema.relationships r
            		ON r.id=s.id
            	LEFT JOIN rc
            		ON rc.id=r.id
            		AND rc.id != {$reportee['id']}
                LEFT JOIN pay_agent.agent_history pah
            	   ON pah.active_month= '{$period['open']}'
            	   AND pah.agent_id=r.id
            	   AND pah.agent_id != {$reportee['id']}   /* the party being reported on could have been an agent in that month */
            	WHERE rc.id IS NULL	-- the downline were not an Exec themselves in that month
            	AND s.void=FALSE
            	AND st.cash_inflow=TRUE
            	AND ((r.upline= {$reportee['id']} AND r.x1=TRUE) OR r.id= {$reportee['id']})
            	AND s.entered::DATE BETWEEN '{$period['open']}' AND '{$period['close']}'
            	AND pah.agent_id IS NULL /* the particular downline party was not an agent that month, per Dick Oct 2015 */
            )
            SELECT x.id::TEXT AS id, SUM(x.amount) AS ttl, m.alias, m.firstname, m.lastname
            FROM x
            JOIN members m
                ON m.id=x.id
            GROUP BY x.id, m.alias, m.firstname, m.lastname
            UNION
            SELECT 'TOTALS'::TEXT AS id, SUM(x.amount) AS ttl, '' AS alias, '', 'TOTALS'::TEXT AS lastname
            FROM x
            ORDER BY id";
    }
//    error_log($qry);
    $rows = $dbi->fetchList_assoc($qry);
}
?>
