<?php
$rids = array(3077498,3887940,4145984);

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);
 
//Flush (send) the output buffer and turn off output buffering
//ob_end_flush();
while (@ob_end_flush());
 
// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);
?>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/generic-report.css" />
<script type="text/javascript" src="/js/jquery.min.js"></script>
<style type="text/css">
    table.report {font-size:110%}
    tbody td {
    	text-align:right;
    }
    thead td {
    	text-align:center;
    }
    td.id {
    	text-align:left;
    }
</style>
<script type="text/javascript">
$( document ).ready(function() {
	  $('#pleasewait').hide();
	});
</script>
</head>
<body>

<!-- div class="fpnotes"><b>Doing analysis on this month: <?php //echo $eom?></b><br/>This analysis will not be accurate until after fees are run</div-->
<p>Income / Commission Analysis for July</p>
<p id="pleasewait" style="text-align: center; display: inline-block;"><img alt="" src="/images/spinner.gif" width="100" height="100" />
<br /><br />This will take some time.</p>

<?php
//ob_flush();
//flush();

// it is expected that this will be protected by server side folder protection, in other words, this script will not be accessible unless they have the credentials
require 'Clubshop/AdminDbConnect.php';

function DB()
{
    return \Clubshop\AdminDbConnect::DBI();
}

$uplineList = implode(',', $rids);
$rootMonthSchema = 'cgi_2015_07_31';
$rootMonthFirst = '2015-07-01';
$rootMonthLast = '2015-07-31';

$priorMonthSchema = 'cgi_2015_06_30';
$priorMonthFirst = '2015-06-01';
$priorMonthLast = '2015-06-30';

//$eom = DB()->fetchSingleCol("SELECT first_of_month() -1");
//$schema = 'cgi_' . preg_replace('/-/', '_', $eom);

/* new partners and fees for the specific month */
$rootMonth = DB()->fetchAll_assoc("
    SELECT
        r.upline
        ,COUNT(DISTINCT r.id) AS upgrades
	    ,SUM(usd) AS income
    FROM $rootMonthSchema.relationships r
    JOIN $rootMonthSchema.members m ON m.id=r.id
	   AND m.vip_date >= '$rootMonthFirst'
	   AND m.membertype='v'
    JOIN subscriptions_history s ON s.member_id=r.id AND s.void=false AND s.stamp::DATE BETWEEN '$rootMonthFirst' and '$rootMonthLast'
    WHERE
	   r.upline IN ($uplineList)
    GROUP BY r.upline", 'upline');

/* new partners from prior month who are still partners in the specific month, with their fees in the specific month */
$priorMonth = DB()->fetchAll_assoc("
    SELECT
        r.upline
	   ,COUNT(DISTINCT r.id) AS upgrades
	   ,SUM(usd) AS income
    FROM $rootMonthSchema.relationships r
    JOIN $priorMonthSchema.members mformer
        ON mformer.id=r.id
        AND mformer.vip_date >= '$priorMonthFirst'
        AND mformer.membertype='v'
    JOIN $rootMonthSchema.members m
        ON m.id=r.id
        AND m.vip_date BETWEEN '$priorMonthFirst' AND '$priorMonthLast'
        AND m.membertype='v'
    LEFT JOIN subscriptions_history s
        ON s.member_id=r.id
        AND s.void=false
        AND s.stamp::DATE BETWEEN '$rootMonthFirst' and '$rootMonthLast'
    WHERE
       r.upline IN ($uplineList)
    GROUP BY r.upline", 'upline');

/* "existing" partners with their fees in the specific month */
$previousMonths = DB()->fetchAll_assoc("
    SELECT
        r.upline
    	,COUNT(DISTINCT r.id) AS upgrades
    	,SUM(usd) AS income
    FROM $rootMonthSchema.relationships r
    JOIN $rootMonthSchema.members m
        ON m.id=r.id
        AND m.vip_date < '$priorMonthFirst'
        AND m.membertype='v'
    JOIN subscriptions_history s
        ON s.member_id=r.id
        AND s.void=false
        AND s.stamp::DATE BETWEEN '$rootMonthFirst' and '$rootMonthLast'
    WHERE
	r.upline IN ($uplineList)
    GROUP BY r.upline", 'upline');

/* commissions for the specific month, sans the grouping party themselves */
$comms = DB()->fetchAll_assoc("
    WITH rd AS (SELECT max(run_date) AS run_date FROM archives.refcomp_me_dates rmd WHERE commission_month='$rootMonthLast')
    SELECT
    r.upline,
    SUM(ca.my50 + ca.my25 + ca.minimum_commission_guarantee + COALESCE(ca.plpp,0)) AS comms
    FROM $rootMonthSchema.relationships r
    JOIN archives.refcomp_me_batches ca
        ON ca.id=r.id
    JOIN rd
        ON rd.run_date=ca.run_date
    WHERE
	   r.upline IN ($uplineList)
    GROUP BY upline", 'upline');

/* commissions for the grouping party themselves */
$persComms = DB()->fetchAll_assoc("
    WITH rd AS (SELECT max(run_date) AS run_date FROM archives.refcomp_me_dates rmd WHERE commission_month='$rootMonthLast')
    SELECT
        ca.id,
        (ca.my50 + ca.my25 + ca.minimum_commission_guarantee + COALESCE(ca.plpp,0)) AS comms
    FROM archives.refcomp_me_batches ca
    JOIN rd
        ON rd.run_date=ca.run_date
    WHERE
	   ca.id IN ($uplineList)", 'id');

$subtotals = array('total'=>0,'root_upgrades'=>0,'root_income'=>0,'prior_upgrades'=>0,'prior_income'=>0,'previous_upgrades'=>0,'previous_income'=>0);
$totalFees = array('total'=>0);
$ttlComms = array('total'=>0);
$percentages = array('total'=>0);
foreach ($rids AS $id) {
    $ttlComms[$id] = $comms[$id]['comms'] + $persComms[$id]['comms'];
    $ttlComms['total'] += $ttlComms[$id];
    
    $totalFees[$id] = $rootMonth[$id]['income'] + $priorMonth[$id]['income'] + $previousMonths[$id]['income'];
    $totalFees['total'] += $totalFees[$id];
    
    $percentages[$id] = $ttlComms[$id]/$totalFees[$id];
    
    $subtotals['root_upgrades'] += $rootMonth[$id]['upgrades'];
    $subtotals['root_income'] += $rootMonth[$id]['income'];
    $subtotals['prior_upgrades'] += $priorMonth[$id]['upgrades'];
    $subtotals['prior_income'] += $priorMonth[$id]['income'];
    $subtotals['previous_upgrades'] += $previousMonths[$id]['upgrades'];
    $subtotals['previous_income'] += $previousMonths[$id]['income'];
}
$percentages['total'] += $ttlComms['total']/$totalFees['total'];
foreach ($percentages AS $key => $val) {
    $percentages[$key] = intval($val * 100);
}
?>

<table class="report">
      <thead>
        <tr>
          <td>ID</td>
          <td># New Partners</td>
          <td>New Partner Fees</td>
          <td># June New Partners <br />
            (still Partners in July) </td>
          <td>June New Partner Fees</td>
          <td># Other Partners <br />
            (new Partners before June)</td>
          <td>Fees</td>
          <td>TOTAL FEES</td>
          <td>COMMISSIONS</td>
          <td>PAYOUT %</td>
        </tr>
     </thead>
     <tbody id="data_table">
     <?php foreach ($rids AS $id):?>
        <tr>
          <td class="id"><?php echo $id?></td>
          <td><?php echo $rootMonth[$id]['upgrades']?></td>
          <td><?php echo $rootMonth[$id]['income']?></td>
          <td><?php echo $priorMonth[$id]['upgrades']?></td>
          <td><?php echo $priorMonth[$id]['income']?></td>
          <td><?php echo $previousMonths[$id]['upgrades']?></td>
          <td><?php echo $previousMonths[$id]['income']?></td>
          <td><?php printf('%.2f', $totalFees[$id])?></td>
          <td><?php printf('%.2f', $ttlComms[$id])?></td>
          <td><?php echo $percentages[$id]?></td>
        </tr>
        <?php endforeach;?>

        <tr>
          <td>TOTALS FOR COMPANY</td>
          <td><?php echo $subtotals['root_upgrades']?></td>
          <td><?php echo $subtotals['root_income']?></td>
          <td><?php echo $subtotals['prior_upgrades']?></td>
          <td><?php echo $subtotals['prior_income']?></td>
          <td><?php echo $subtotals['previous_upgrades']?></td>
          <td><?php echo $subtotals['previous_income']?></td>
          <td><?php printf('%.2f', $totalFees['total'])?></td>
          <td><?php printf('%.2f', $ttlComms['total'])?></td>
          <td><?php echo $percentages['total']?></td>
        </tr>

      </tbody>
    </table>
</body></html>