<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Welcome to Glocal Income</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body bgcolor="#163817" body leftmargin="1" topmargin="0" marginwidth="0" marginheight="0">
<table bgcolor="#163817" border="0" cellpadding="0" cellspacing="0" style="height: 100%;" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="650">
          <tbody>
            <tr> 
              <td align="center" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"><br />
                Bekijk in web <a href="http://www.glocalincome.com/email/associate/news/1/browserview.html" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">browser</a>.</td>
            </tr>
            <tr> 
              <td align="center" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top">Gelieve 
                <a href="mailto:Info@GlocalIncome.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Info@GlocalIncome.com</a> 
                en <a href="mailto:Info@ClubShop.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Info@ClubShop.com</a> 
                toe te voegen aan uw adresboek,<br />
                zo mist u in de toekomst geen enkele e-mail in uw inbox.<br /></td>
            </tr>
            <tr> 
              <td height="10">&nbsp;</td>
            </tr>
            <tr> 
              <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" style="height: 979px;" width="650">
                  <tbody>
                    <tr> 
                      <td colspan="3"><img height="166" src="http://www.glocalincome.com/email/images/news_1.png" width="650" /></td>
                    </tr>
                    <tr> 
                      <td><img height="650" src="http://www.glocalincome.com/email/images/welcome_2.png" width="49" /></td>
                      <td align="left" bgcolor="#ffffff" style="FONT-SIZE: 12px; COLOR: #160e0b; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="552"><span style="FONT-SIZE: 16px; COLOR: #50BC53; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold;">Een 
                        boeiende tijd breekt aan!</span><br /> <br /> <span style="FONT-SIZE: 12px; COLOR: #873C13; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold;">Ons 
                        nieuw lokale handelaren programma is gelanceerd!</span><br /> 
                        <br />
                        Als <strong>"Associate lid"</strong> krijgt u nu de geweldige 
                        kans om uw lidmaatschap te upgraden naar <strong>VIP lid</strong> 
                        en beginnen met het registreren van lokale handelaren 
                        die in uw regio onze <strong>ClubShop Rewards kaart</strong> 
                        zullen accepteren. Door dit te doen zult U voortdurend 
                        Referral commissies verdienen wanneer onze leden zullen 
                        winkelen bij UW geregistreerde handelaren en gebruik maken 
                        van hun <strong>ClubShop Rewards kaart</strong>.<br /> 
                        <br />
                        Bekijk vlug onze flash presentatie. Die zal aantonen hoe 
                        U voordeel kunt halen uit het registreren van lokale handelaren 
                        in uw buurt.<br /> <br /> <img height="12" src="http://www.glocalincome.com/email/images/icons/icon_checkmark_long.png" width="42" /><a href="http://www.glocalincome.com/localmarketing/pages/text_presentation.html" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier om de flash presentatie te bekijken</a><br /> <br />
                        Nadat u onze flash presentatie hierboven hebt bekeken, 
                        zult u best begrijpen waarom <strong>Associate leden</strong> 
                        van over de ganse wereld de upgrade doen naar <strong>VIP 
                        lid</strong> en de kans om deel te nemen aan deze wonderlijke 
                        zakelijke opportuniteit met beide handen grijpen. U kunt 
                        hieronder <strong>VIP lid</strong> worden.<br /> <br /> 
                        <img height="12" src="http://www.glocalincome.com/email/images/icons/icon_checkmark_long.png" width="42" /><a href="../appx.cgi/0/upg?flg=dv&amp;sponsorID=3" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier voor de upgrade en wordt nu VIP lid</a><br /> <br />
                        Wellicht wenst u ook te weten welke bedrijven reeds regionaal 
                        zijn ingeschreven. Denk eraan, als <strong>Associate lid</strong> 
                        kunt u andere mensen verwijzen naar ons gratis programma 
                        en Referral commissies verdienen op de mensen die u sponsort, 
                        die op hun beurt aankopen zullen doen in onze <strong>ClubShop 
                        Mall</strong> en/of bij de aangesloten lokale handelaren. 
                        Plus, als u de upgrade doet naar <strong>VIP lid</strong>, 
                        dan bent u in de mogelijkheid om override commissies te 
                        verdienen op een ganse verkoopsorganisatie van kaarthouders. 
                        Bekijk onze nieuwe lokale handelaren hieronder.<br /> 
                        <br /> <img height="12" src="http://www.glocalincome.com/email/images/icons/icon_checkmark_long.png" width="42" /><a href="http://www.clubshop.com/rewardsdirectory" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Klik 
                        hier om de lijst te zien</a><br /> <br />
                        Mochten er na het bekijken van ons marketing programma 
                        hierboven nog enige vragen resten, dan kunt u gerust uw 
                        upline Trainer contacteren voor verdere uitleg en hulp. 
                        U vindt hun contactinformatie op uw <strong>Associate 
                        portaalpagina</strong> , u vindt de link naar uw portaalpagina 
                        en uw inloggegevens onderaan terug.<br /> <br />
                        Dit is nieuw, dit is fascinerend &eacute;n baanbrekend! 
                        Doe de upgrade <strong>vandaag</strong> nog en start met 
                        het registreren van de handelaren in uw buurt voor een 
                        andere <strong>Glocal Income consulent</strong> u voor 
                        is!<br /> <br />
                        Veel succes,<br /> <br /> <strong>DHSC Glocal Income</strong><br />
                        2560 Placida Road<br />
                        Englewood, FL USA 34224 - 5412</td>
                      <td><img height="650" src="http://www.glocalincome.com/email/images/welcome_3.png" width="49" /></td>
                    </tr>
                    <tr> 
                      <td colspan="3"><img height="239" src="http://www.glocalincome.com/email/images/news_4.png" width="650" /></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr> 
              <td height="5"></td>
            </tr>
            <tr> 
              <td align="left" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"> 
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tbody>
                    <tr> 
                      <td align="center" colspan="2"><span style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #febc22; FONT-FAMILY: Arial, Helvetica, sans-serif">VOLG 
                        ONS OP UW FAVORIETE SOCIAAL NETWERK</span></td>
                    </tr>
                    <tr> 
                      <td align="center" width="50%"><span style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif">ClubShop</span></td>
                      <td align="center" width="50%"><span style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Arial, Helvetica, sans-serif">Glocal 
                        Income</span></td>
                    </tr>
                    <tr> 
                      <td align="center" width="50%"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"><img alt="Word fan van ClubShop Rewards op Facebook" border="0" height="28" hspace="5" src="http://www.glocalincome.com/email/images/icons/icon_face_book.png" vspace="3" width="28" /></a> 
                        <a href="http://twitter.com/clubshop" target="_blank"><img alt="Word fan van ClubShop Rewards op Twitter" border="0" height="28" hspace="5" src="http://www.glocalincome.com/email/images/icons/icon_twitter.png" vspace="3" width="28" /></a></td>
                      <td align="center" width="50%"><a href="http://www.facebook.com/pages/Glocal-Income/74938236794" target="_blank"><img alt="Word fan van Glocal Income op Facebook" border="0" height="28" hspace="5" src="http://www.glocalincome.com/email/images/icons/icon_face_book.png" vspace="3" width="28" /></a> 
                        <a href="http://www.youtube.com/watch?v=HCLvtEoFqNA" target="_blank"><img alt="Bekijk ClubShop Rewards op YouTube" border="0" height="28" hspace="5" src="http://www.glocalincome.com/email/images/icons/icon_you_tube.png" vspace="3" width="28" /></a></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr> 
              <td align="left" height="10" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"></td>
            </tr>
          </tbody>
        </table>
</td>
</tr>
</tbody>
</table>
</body>
</html>

