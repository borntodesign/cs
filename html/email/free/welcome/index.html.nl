<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Welcome to Glocal Income</title>
</head>
<body bgcolor="#163817" body leftmargin="1" topmargin="0" marginwidth="0" marginheight="0">
<table bgcolor="#163817" border="0" cellpadding="0" cellspacing="0" style="height: 100%;" width="100%">
<tbody>
<tr>
<td valign="top">
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr> 
              <td align="center" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"><br />
                  Bekijken&nbsp;in web <a href="http://www.glocalincome.com/email/free/welcome/browserview.html" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">browser</a>.</div></td>
            </tr>
            <tr> 
              <td style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"><div align="center">Gelieve&nbsp;<a href="mailto:Info@GlocalIncome.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Info@GlocalIncome.com</a>&nbsp;en 
                  <a href="mailto:Info@ClubShop.com" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">Info@ClubShop.com</a> 
                  toe te voegen aan uw adresboek,<br />
                  zo mist u in de toekomst geen enkele e-mail in uw&nbsp;inbox.<br />
                  <br />
                </div></td>
            </tr>
            <tr> 
              <td  align="center" height="10" style="FONT-SIZE: 12px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top"> 
                <table bgcolor="#febc22" border="0" cellpadding="10" cellspacing="5" width="100%">
                  <tbody>
                    <tr> 
                      <td bgcolor="#ffffff" valign="top"> <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr> 
                              <td><div align="center"><span style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif"><strong>[[firstname]],&nbsp;hieronder 
                                  vindt u uw&nbsp;persoonlijk ID nummer en paswoord.</strong></span></div></td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td> <table border="0" cellpadding="0" cellspacing="0" width="348">
                                  <tbody>
                                    <tr>
                                      <td align="left" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="159">&nbsp;</td>
                                      <td align="left" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="139">Uw 
                                        &nbsp;ID nummer:</td>
                                      <td align="left" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #00376F; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="50">[[alias]]</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr> 
                              <td> <table border="0" cellpadding="0" cellspacing="0" width="403">
                                  <tbody>
                                    <tr>
                                      <td align="left" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="159">&nbsp;</td>
                                      <td align="left" style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="139">Uw 
                                        paswoord:</td>
                                      <td align="left" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #00376F; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="105">[[password]]</td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td><div align="center"><span style="FONT-SIZE: 12px; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif">U 
                                  kunt inloggen op uw&nbsp;<strong>Portaalpagina</strong>&nbsp;door 
                                  hier te <a href="http://www.clubshop.com/ppreport.html" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #FC6F3F; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">klikken</a>.</span></div></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr> 
              <td height="10">&nbsp;</td>
            </tr>
            <tr> 
              <td valign="top"> <table border="0" cellpadding="0" cellspacing="0" style="height: 979px;" width="650">
                  <tbody>
                    <tr> 
                      <td colspan="3"><img height="166" src="http://www.glocalincome.com/email/images/header-mbrwelcome_01.png" width="650" /></td>
                    </tr>
                    <tr> 
                      <td><img height="574" src="http://www.glocalincome.com/email/images/welcome_02.png" width="49" /></td>
                      <td align="left" bgcolor="#ffffff" height="574" style="FONT-SIZE: 12px; COLOR: #160e0b; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top" width="552"><strong>Nu 
                        is die tijd aangebroken!</strong><br />
                        <br />
                        Eerst willen wij u bedanken voor uw inschrijving en om 
                        meer te willen horen over onze<strong> Glocal Income Home 
                        Business</strong> opportuniteit en daarbij een gratis 
                        <strong>ClubShop Rewards lidmaatschap</strong> te winnen. 
                        We zijn reeds 12 jaar in staat mensen te helpen geld te 
                        besparen en te helpen bij het opbouwen van succesvolle 
                        zaken van thuis uit.<br />
                        <br />
                        Hierboven vindt u uw persoonlijk <strong>ID nummer</strong> 
                        en p<strong>aswoord</strong> terug waarmee u kunt inloggen 
                        op uw eigen portaalpagina Interface.<br />
                        <br />
                        In de komende weken zult u e-mail van ons ontvangen die 
                        u zullen leren waarom wij &eacute;&eacute;n van meest 
                        succesvolle en langst bestaande thuiswerk bedrijven zijn 
                        op het Internet en hoe U daar voordeel uit kunt halen.<br />
                        <br />
                        We zullen u stap voor stap tonen hoe U een residueel inkomen 
                        kunt opbouwen door ons bewezen business plan en systeem 
                        te volgen, met behulp van onze uitmuntende hulpmiddelen 
                        en training. Plus, we zullen u ook laten zien hoe U als 
                        <strong>ClubShop Rewards lid </strong>geld kunt besparen 
                        op dagdagelijkse producten en hoe u van thuis uit, anderen 
                        kunt helpen hetzelfde te doen. Of u nu enkel een paar 
                        honderd dollar per maand aan uw huidig inkomen wilt toevoegen 
                        of, vroeger op pensioen wilt of, schulden wilt afbetalen 
                        of, een blijvend inkomen en een mondiale enterprise wilt 
                        opbouwen, wij kunnen u helpen.<br />
                        <br />
                        We bieden ook online en offline handelaren een geweldige 
                        manier aan om hun bedrijf in de kijker te stellen, we 
                        helpen non-profit organisaties fondsen binnen te rijven 
                        voor hun doel en bieden kleine en grote bedrijven een 
                        fantastische manier aan om hun trouwe personeelsleden 
                        en of leden te belonen.<br />
                        <br />
                        IK weet dat u ZEER benieuwd bent om alles te bekijken 
                        wat we te bieden hebben, dus gelieve de volgende dagen 
                        uw e-mail in het oog te houden en meer te leren over al 
                        de geweldige opties die we beschikbaar hebben voor onze 
                        <strong>Glocal Income Home Thuiswerkende Bedrijfsleiders 
                        </strong>en onze <strong>ClubShop Reward leden</strong>.<br />
                        <br />
                        We wensen u veel succes,<br />
                        <br />
                        Dick Burke<br />
                        <strong>CEO - DHSC Glocal Income &amp; ClubShop Rewards</strong><br />
                        2560 Placida Road<br />
                        Englewood, FL USA 34224 - 5412</td>
                      <td><img height="574" src="http://www.glocalincome.com/email/images/welcome_04.png" width="49" /></td>
                    </tr>
                    <tr> 
                      <td colspan="3"><img height="239" src="http://www.glocalincome.com/email/images/footer-mbrwelcome_01.png" width="650" /></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr> 
              <td height="10">&nbsp;</td>
            </tr>
            <tr> 
              <td align="left" style="FONT-SIZE: 11px; COLOR: #FFFFFF; FONT-FAMILY: Arial, Helvetica, sans-serif" valign="top">Indien 
                u geen verdere informatie over onze&nbsp;Glocal Income home business 
                mogelijkheden wenst te ontvangen, maar wel uw&nbsp;ClubShop Rewards 
                lidmaatschap wenst te behouden om punten te verdienen en geld 
                te besparen terwijl u offline of online winkelt, dan kunt u downgraden&nbsp;naar 
                "Clubshop Rewards Member"&nbsp;door hier te <a href="http://www.clubshop.com/cgi/mailstatus.cgi?e=deb.corsetti@gmail.com;o=19898087" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">klikken</a>.<br />
                <br />
                Klik hier om&nbsp;<a href="http://www.clubshop.com/cgi/mailstatus.cgi?e=deb.corsetti@gmail.com;o=19898087" style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: FEBC22; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: underline" target="_blank">uit 
                te schrijven</a>.<br />
                <br />
                &copy; 2009 Glocal Income. Alle rechten voorbehouden.<br />
                Dit is een bevestigingse-mail van&nbsp;Glocal Income, Inc., 2560 
                Placida Road, Englewood, FL 34224 Attn: Marketing Preferences</td>
            </tr>
            <tr> 
              <td align="left" height="10">&nbsp;</td>
            </tr>
          </tbody>
        </table>
</td>
</tr>
</tbody>
</table>
</body>
</html>
