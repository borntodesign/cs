<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	media-type="text/html"/>

<xsl:template match="/">
<b><content>
<!-- The HTML content for the right main part of the page goes here -->

<h3><xsl:value-of select="//lang_blocks/p8" /></h3>

<p><xsl:value-of select="//lang_blocks/p9" /><a href="contact.xml" target="_blank">http://www.clubshop.com/contact.xml</a><br/>
<xsl:value-of select="//lang_blocks/p9a" /></p>
<hr/>
<p><xsl:value-of select="//lang_blocks/p10" /></p>

<ol>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p11" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p12" /></li>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p13" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p14" /><xsl:text> </xsl:text><a href="/manual/technical/cookie_help.xml" target="_blank"><xsl:value-of select="//lang_blocks/p15" /></a></li>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p16" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p17" /></li>
<br/>	
	<ul>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p18" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p19" /></li>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p18" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p20" /></li>
	</ul>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p21" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p22" /></li>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p23" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p24" /></li>
<br/> 
	<ul>
<li><xsl:value-of select="//lang_blocks/p25" /><xsl:text> </xsl:text> <a href="mailto: purchases@dhs-club.com">purchases@dhs-club.com</a></li>
	</ul>
<br/>
<li><span class="bl"><xsl:value-of select="//lang_blocks/p26" /></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p27" /></li>
<br/>
<!--<li><b><xsl:value-of select="//lang_blocks/p28" /></b><xsl:text> </xsl:text></li>-->
</ol>






<!-- END of the content -->
</content>
<xsl:copy-of select="*" />
</b>
</xsl:template>

</xsl:stylesheet>