<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	<xsl:param name="node_id" />
	<xsl:param name="vendor_id" />
	<xsl:template match="/">	
		<html><head>

			<title>Vendor List Editor</title>
			<style type="text/css">
			body,html {
				background-color: #efffef;
				font-size: 80%;
				font-family: sans-serif;
			}
			tt {font-size: 110%;}
			input {margin-right: 1em; font-size: inherit;}
			.vid {width: 3em;}
			.name {width: 20em;}
			.vrp,.mrp,.pp {width: 8em;}
			</style>
		</head>
		<body>
		<!-- we don't want to have to deal with multiple records on the actual editting end at this time -->
		<xsl:choose>
		<!-- we will check the appropriate attribute depending on the parameter received -->
			<!--xsl:choose><xsl:when test="$vendor_id != ''"-->
				<xsl:when test="(count(//vendors/vendor[@vendor_id = $vendor_id]) &gt; 1) or (count(//vendors/vendor[@id = $node_id]) &gt; 1) ">
				<h4>Since there is more than one vendor with the same vendor ID or node ID, you cannot use this interface for editing.</h4>
			
		</xsl:when><xsl:otherwise>
		<p>This application will automatically escape special characters like &amp; &gt; &lt; &quot; and so forth. You shouldn't do it yourself.</p>
		<form action="https://www.clubshop.com/cgi/admin/xml-vendor-list-editor.cgi">
		<input type="hidden" name="filename">
			<xsl:attribute name="value"><xsl:value-of select="//filename" /></xsl:attribute>
		</input>
	<!-- place different buttons if this is a record creation rather than an update -->
		<xsl:choose><xsl:when test="($vendor_id = '') and ($node_id = '')">
			<h4>Add a Vendor</h4>
			<xsl:call-template name = "vendor" />
			<input type="submit" name="action" value="Add Vendor" />
		</xsl:when><xsl:otherwise>
		<xsl:choose><xsl:when test="$vendor_id != ''">
			<xsl:apply-templates select = "//vendors/vendor[@vendor_id = $vendor_id]" />
		</xsl:when><xsl:otherwise>
                        <xsl:apply-templates select = "//vendors/vendor[@id = $node_id]" />
		</xsl:otherwise></xsl:choose>

		<br /><br /><div>If you want to backup the file before making changes, enter a prefix below.
		<ul><li>It will be added to the filename like <tt><b>backup.</b>vendors.xml</tt>.</li>
		<li>The file will be available in the same folder/directory as the live <tt>vendors.xml</tt> file.</li>
		<li>If the backup already exists, it will be overwritten.</li></ul>
		<input type="text" name="backup_file" class="name" />
		</div><br />
                <input type="submit" value="Update" /><xsl:text>
                </xsl:text>
                <input type="reset" /><br /><br />
		<p style="border:1px solid gray; background-color: #ffefef; padding: 0.5em;">
		If you want to delete this record, click the button below. The record will be commented out effectively 'deleting' it. However, it will still be available for manual activation.<br /><br />
		<input type="submit" name="action" value="Delete" style="color: #f00"/></p>
		</xsl:otherwise></xsl:choose>
		</form>
		</xsl:otherwise></xsl:choose>
		</body>
		</html>
	</xsl:template>
	
	<xsl:template match="vendor">
<!-- although this template may look unnecessary or redundant in light of the named template,
do not remove it, it is used to fill out a form whereas the named template simply creates an empty form -->
	<xsl:call-template name = "vendor" />
	</xsl:template>
	
	<xsl:template name="vendor">
	<div>
	Name: <input type="text" name="name" class="name">
		<xsl:attribute name="value"><xsl:value-of select="@name" /></xsl:attribute></input></div>
	<p>
	Vendor ID: <input type="text" name="vendor_id" class="vid">
		<xsl:attribute name="value"><xsl:value-of select="@vendor_id" /></xsl:attribute></input>
	Ad ID: <input type="text" name="ad_id" class="vid">
		<xsl:attribute name="value"><xsl:value-of select="@ad_id" /></xsl:attribute></input>
	MP: <input type="checkbox" name="mp" class="mp" value="1">
		<xsl:if test="@mp = 1">
			<xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
		</input></p>
	<p>
	vrp: <input type="text" name="vrp" class="vrp">
		<xsl:attribute name="value"><xsl:value-of select="@vrp" /></xsl:attribute></input>
	mrp: <input type="text" name="mrp" class="mrp">
		<xsl:attribute name="value"><xsl:value-of select="@mrp" /></xsl:attribute></input>
	pp: <input type="text" name="pp" class="pp">
		<xsl:attribute name="value"><xsl:value-of select="@pp" /></xsl:attribute></input>
	</p>
	<p>Gift Certificate URL: <input type="text" name="gift_card" style="width:60em; font-size:inherit;">
		<xsl:attribute name="value"><xsl:value-of select="gift_card" /></xsl:attribute></input>
	</p>

	<div>
	Categories:<br />
	<select name="catid" multiple="multiple" size="5" style="font-size:inherit;">
		<xsl:if test="//vendors/vendor[@vendor_id=$vendor_id or @id=$node_id]/catid">
			<optgroup label="Current Categories">
			<xsl:for-each select = "//categories/category[@catid = //vendors/vendor[@vendor_id=$vendor_id or @id=$node_id]/catid]">
			<xsl:variable name="catid" select="@catid" /><xsl:variable name="cattxt" select="." />


			<xsl:if test="//vendors/vendor[@vendor_id=$vendor_id or @id=$node_id]/catid = $catid">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="$catid" /></xsl:attribute>
					<xsl:attribute name="selected">selected</xsl:attribute>

					<xsl:value-of select="$cattxt" />
				</option>
			</xsl:if>

			</xsl:for-each>
			</optgroup>
		</xsl:if>
		
		<optgroup label="Other Categories">
		<xsl:for-each select = "//categories/category">
		<xsl:choose><xsl:when test="@catid = //vendors/vendor[@vendor_id = $vendor_id]/catid"></xsl:when>
		<xsl:otherwise>
		<option>
		<xsl:attribute name="value"><xsl:value-of select="@catid" /></xsl:attribute>
		<xsl:value-of select="." />
		</option>
		</xsl:otherwise></xsl:choose>
	</xsl:for-each>
		</optgroup>
	</select></div>
	<input type="hidden" name="id"><xsl:attribute name="value"><xsl:value-of select="@id" /></xsl:attribute></input>
	</xsl:template>


</xsl:stylesheet>
