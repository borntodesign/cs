<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:param name="catid" />
<xsl:include href="/xml/head-foot.xsl"/>
<!--xsl:include href = "../common.xsl"/-->
<!-- for testing this variable can be used instead of the param above -->
<!--xsl:variable name = "catid" select = "'23'" /-->
	<xsl:template match="/">	
		<html><head>
		<script type="text/javascript" src="/js/miniLogin.js"></script>
<link rel="stylesheet" href="/css/global.css" type="text/css" />
		<link href="../xmall.css" rel="stylesheet" type="text/css" />
			<title><xsl:value-of select="concat(//country/title, ' - ', //categories/*[@catid = $catid])" />
			</title>
		</head><body>
<xsl:call-template name = "head" />
<xsl:call-template name="mall_list"/>

		<table class="xm_main">
		<tr><td>
		<table class="vendors">
		<tr><td id="xm_category_name">
			<xsl:choose>
				<xsl:when test="//categories/*/@catid = $catid">
					<xsl:value-of select="//categories/*[@catid = $catid]" />
				</xsl:when>
			  
				<xsl:otherwise>
					<xsl:value-of select="//lang_blocks/full_list" />
				</xsl:otherwise>
			</xsl:choose>
		</td></tr>
		<!-- if other hand inserted custom vendor stuff is desired, then it can be added
		as a another row before or after the generated list -->
			<!--xsl:call-template name = "catlist" /-->
			
			<xsl:apply-templates select = "//vendors/vendor">
				<xsl:sort select="@name" order="ascending" />
			</xsl:apply-templates>
		</table></td>
		<td id="catpage_banner_col">
		
			<xsl:for-each select = "//items/item">
<xsl:sort select="//vendors/vendor/@name[catid = $catid]"/>
			<xsl:if test="catid = $catid">
	<!-- it seems that the maintainers of the XML are putting block level formatting right in
	so we don't always need the p's -->
			<xsl:choose><xsl:when test="descendant-or-self::p or descendant-or-self::div">
				<xsl:copy-of select = "*[name() != 'catid']"/>
			</xsl:when><xsl:otherwise>
				<p><xsl:copy-of select = "*[name() != 'catid']"/></p>
			</xsl:otherwise></xsl:choose>
			</xsl:if>
			</xsl:for-each>
		</td>
		</tr></table>
<xsl:call-template name="foot"/>
		
		<div class="xm_copyright">
		<img src="/images/copyright_.gif" width="173" height="19" alt="Copyright" />
		</div>
		
		</body>
		</html>
	</xsl:template>
	
	<xsl:template match="vendor">
		<xsl:if test="$catid = ./catid or $catid = ''">
			<tr><td>
			<div class="xm_vname">
	<!-- it is unclear whether this is really needed/useful anymore -->
			<!--xsl:if test="@mp"><img class="icon_mp" src="/images/icon_mp.gif" height="20" width="20">
				<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/mp_merchant" /></xsl:attribute>
				</img>
			</xsl:if-->
			<a onclick="window.open(this.href);return false;">
			<xsl:attribute name="href">/cgi-bin/rd/<xsl:choose><xsl:when test="@mp">9</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>,<xsl:value-of select="./@vendor_id" />
				<xsl:if test="@ad_id != ''">,adid=<xsl:value-of select="@ad_id" /></xsl:if>
			</xsl:attribute><xsl:value-of select="./@name" /></a></div>
			<div class="xm_vrp"><xsl:value-of select="//lang_blocks/vrp" />:
			<xsl:value-of select="./@vrp" /></div>
			<!--div class="xm_mrp"><xsl:value-of select="//lang_blocks/mrp" />:
			<xsl:value-of select="./@mrp" /></div-->
			<div class="xm_pp"><xsl:value-of select="//lang_blocks/pp" />:
			<xsl:value-of select="./@pp" /></div>
			</td></tr>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
