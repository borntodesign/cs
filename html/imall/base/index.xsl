<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:include href="/xml/head-foot.xsl"/>
<xsl:param name="catid" />
<!--xsl:include href = "../common.xsl"/-->
<!-- for testing this variable can be used instead of the param above -->
<!--xsl:variable name = "catid" select = "'23'" /-->
	<xsl:template match="/">	
		<html><head>
<meta name="description"><xsl:attribute name="content"><xsl:value-of select="//meta_description" /></xsl:attribute></meta>
<meta name="keywords"><xsl:attribute name="content"><xsl:value-of select="//meta_keywords" /></xsl:attribute></meta>
<link rel="stylesheet" href="/css/global.css" type="text/css" />
		<script type="text/javascript" src="/js/miniLogin.js"></script>
		<link href="../xmall.css" rel="stylesheet" type="text/css" />
			<title><xsl:value-of select="concat(//country/title, ' - ', //title_append)" />
			</title>
		</head><body>
<xsl:call-template name = "head" />
<xsl:call-template name="mall_list"/>

		<table class="xm_main">
		<tr><td colspan="2">
		
		</td></tr>
		<tr><td>
		<table class="vendors">
		<tr><td><div class="vcat">
			<a href="cat.xsp"><xsl:value-of select="//lang_blocks/full_list" /></a>
		</div></td></tr>
		<tr><td class="xm_category_spacer">&nbsp;</td></tr>

		<tr><td class="xm_category_name">
			<xsl:value-of select="//lang_blocks/online_cats" />
		</td></tr>

		<!-- if other hand inserted custom vendor stuff is desired, then it can be added
		as a another row before or after the generated list -->
			<xsl:call-template name = "catlist" />

		<tr><td class="xm_category_spacer">&nbsp;</td></tr>
		<tr><td class="xm_category_name">
			<xsl:value-of select="//lang_blocks/offline" />
		</td></tr>
		<tr><td style="text-align:center">
		<form style="display:inline" id="cb_merchant" action="/cgi/ma_real_rpt.cgi" target="_blank">
		<input type="hidden" name="country"><xsl:attribute name="value"><xsl:value-of select="//ma_states/country/@code" /></xsl:attribute>
			</input>
		<input type="hidden" name="action" value="report_by_city" />
		<select name="state" onchange="document.forms['cb_merchant'].submit()">
		<option value="">
			<xsl:choose><xsl:when test="//ma_states/country/state">
				<xsl:value-of select="//lang_blocks/select_state" />
			</xsl:when><xsl:otherwise>
				<xsl:value-of select="//lang_blocks/no_merchants" />
			</xsl:otherwise></xsl:choose>
		</option>
		<xsl:for-each select="//ma_states/country/*">
			<option><xsl:attribute name="value"><xsl:value-of select="./@code" /></xsl:attribute>
				<xsl:value-of select="." /></option>
		</xsl:for-each></select><br />
		<input type="submit" value="Go" style="margin-top:2px; font-size: 85%;" /></form>
		</td></tr>
		</table></td>
		<td style="vertical-align: top">

		<xsl:copy-of select = "//html/*"/>
		</td>
		</tr></table>
<xsl:call-template name="foot"/>
		<div class="xm_copyright">
		<img src="/images/copyright_.gif" width="173" height="19" alt="Copyright" />
		</div>
		
		</body>
		</html>
	</xsl:template>
	
	
	<xsl:template name="catlist">
	<xsl:for-each select = "//categories/category">
	<xsl:sort select="." />
		<xsl:if test = "//vendors/vendor/catid = ./@catid">
			<tr><td>
			<div class="vcat"><a>
			<xsl:attribute name="href">cat.xsp?catid=<xsl:value-of select="./@catid" />
			</xsl:attribute><xsl:value-of select="." /></a></div>

			</td></tr>
		</xsl:if>

	</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
