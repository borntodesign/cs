<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>

<title>Vendor List for the <xsl:value-of select="//country/*[@val = //country_code]" /> imall</title>
<style type="text/css">
body {font-size: 75%; font-family: sans-serif; background-color:white;}
th {text-align: left; padding: 0.2em 1em; }
td {
	border: 1px solid silver;
	padding: 0.2em 1em 0.2em 0.2em;
	vertical-align: top;
}
tr.r1{background-color: #ffe}
tr.r2{background-color: #efefef}
a {text-decoration:none}
td.gc, td.mp { text-align:center; }
</style>
</head>
<body>
<h4>Vendor List for the <xsl:value-of select="//country/*[@val = //country_code]" /> imall</h4>
<ul><li>Click on the vendor name to edit/delete.</li>
<li>Click on the vendor ID to edit the vendor in the database.</li>
<li><a href="vendor-list-editor.xsp" onclick="window.open(this.href);return false;">Create a new vendor entry</a>.</li>
</ul>
<table style="border-collapse: collapse">
<tr>
<th>Vendor</th>
<th>ID</th>
<th>Ad ID</th>
<th>MP</th>
<th>GC</th>
<th>VIP RP</th>
<th>Mbr. RP</th>
<th>PP</th>
<th>Categories</th>
</tr>
<xsl:for-each select = "//vendors/vendor">
<xsl:sort select="@name"/>
<tr><xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">r1</xsl:when>
        <xsl:otherwise>r2</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>
<td class="name"><a onclick="window.open(this.href);return false;">
	<xsl:attribute name="href">vendor-list-editor.xsp?<xsl:choose>
		<xsl:when test="@id != ''">node_id=<xsl:value-of select="@id" /></xsl:when>
		<xsl:otherwise>vendor_id=<xsl:value-of select="@vendor_id" /></xsl:otherwise></xsl:choose></xsl:attribute>
	<xsl:value-of select="@name" /></a></td>
<td class="vid"><a onclick="window.open(this.href);return false;">
        <xsl:attribute name="href">https://www.clubshop.com/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=<xsl:value-of select="@vendor_id" /></xsl:attribute>
        <xsl:value-of select="@vendor_id" /></a></td>
<td class="adid"><xsl:value-of select="@ad_id" /></td>
<td class="mp"><xsl:if test="@mp != ''">Y</xsl:if></td>
<td class="gc"><xsl:if test="gift_card"><a onclick="window.open(this.href);return false;">
	<xsl:attribute name="href"><xsl:value-of select="gift_card" /></xsl:attribute>
	<xsl:attribute name="title"><xsl:value-of select="gift_card" /></xsl:attribute>Y</a></xsl:if>
</td>
<td class="vrp"><xsl:value-of select="@vrp" /></td>
<td class="mrp"><xsl:value-of select="@mrp" /></td>
<td class="pp"><xsl:value-of select="@pp" /></td>
<td class="cats">
	<xsl:for-each select="catid">
	<xsl:sort select="//category[@catid = current()]" />
	<xsl:value-of select="." /> -
	<xsl:value-of select="//category[@catid = current()]" /><br />
	</xsl:for-each>
</td>
</tr>
</xsl:for-each>
</table>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
