#!/usr/bin/perl -w
###### regx-bulk.pl
###### perform some mass changes on files
###### created: 07/26/06	Bill MacArthur
###### last modified:

use strict;

my $root_dir = '/home/httpd/html/imall';

###### slurp in files in a single shot
$/ = undef;
###### the list if directories I want to look in
#my @malls = qw(BE);
my @malls = qw(au CA GB NZ ZA NL IE it);
foreach my $c (@malls)
{
	chdir "$root_dir/$c";
	foreach my $fn (qw(_index.html catpages.xml))
	{
		if (open (IN, "$fn")){
		if (open (OUT, ">_$fn"))
		{
			my $txt = <IN>;
			close IN;
			$txt =~ s#href=" #href="#g;
			$txt =~ s#<a #<a onclick="window.open(this.href);return false;" #g;
			print OUT $txt;
			close OUT;
			if (rename($fn, "$fn.orig")){
				unless (rename("_$fn", $fn))
				{
					print "rename failed: _$fn\n";
					rename("$fn.orig", $fn);
				}
			} else {
				print "rename failed: $fn\n";
			}
		} else {
			print "Failed to open output file: $root_dir/$c/_$fn\n";
			close IN;
		}
		} else {print "Failed to open input file: $root_dir/$c/$fn\n";}
	}
}

exit;

