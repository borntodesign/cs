<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>


<xsl:template match="/">
<html><head>
<title></title>
<style type="text/css">
	td{padding: 0.2em 1em 0.2em 0.2em; border:1px solid silver;}
	th{text-align: left}
	table{border-collapse:collapse}
tr.r1{background-color: #ffe}
tr.r2{background-color: #efefef}

</style>
</head>
<body>
<table><tr><th>Category</th><th>ID</th></tr>
<xsl:for-each select="//categories/category">
	<tr><xsl:attribute name="class">
    <xsl:choose>
        <xsl:when test="position() mod 2 = 1">r1</xsl:when>
        <xsl:otherwise>r2</xsl:otherwise>
    </xsl:choose>
</xsl:attribute>


<td><xsl:value-of select="."/></td><td><xsl:value-of select="./@catid"/></td></tr>
</xsl:for-each>
</table>
</body></html>

</xsl:template></xsl:stylesheet>