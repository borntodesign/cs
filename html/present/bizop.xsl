<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>ClubShop Partner Presentations</title>
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>


</head>
<body class="blue">
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	
	<h4 class="topTitle"><br/><xsl:value-of select="//lang_blocks/p1"/></h4>

	<hr />
	

	</div></div></div>
			

    <div class="container white">  
	
			
			
			
			
			<div class="row">
			
			<div class="one column">
			</div>
			
			
			
				<div class="twelve columns push-four"> 
					<!--<span class="style28"><xsl:value-of select="//lang_blocks/p2"/></span><br/><br/>-->
				
					<ul>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop.html" target="_blank" class="style28 royal">ENGLISH - USD</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-euro.html" target="_blank" class="style28 royal">ENGLISH - EURO</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-au.html" target="_blank" class="style28 royal">ENGLISH - AUD</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-in.html" target="_blank" class="style28 royal">ENGLISH - INR</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-ng.html" target="_blank" class="style28 royal">ENGLISH - NGN - Nigeria</a></li>					
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-php.html" target="_blank" class="style28 royal">ENGLISH - PHP</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-pk.html" target="_blank" class="style28 royal">ENGLISH - PKR - Pakistan</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_EN-za.html" target="_blank" class="style28 royal">ENGLISH - ZAR</a></li>





					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop.html.fr" target="_blank" class="style28 royal">FRENCH - EURO</a></li>
<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_FR-ma.html" target="_blank" class="style28 royal">FRENCH - MAD</a></li>
<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_FR-bf.html" target="_blank" class="style28 royal">FRENCH - XOF - Burkina Faso</a></li>
<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop_FR-tn.html" target="_blank" class="style28 royal">FRENCH - TND</a></li>



					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_bop.html.it" target="_blank" class="style28 royal">ITALIAN - EURO</a></li>
					
					</ul>
			   </div>
			   						
					
			  <!-- <div class="five columns">
			   		<span class="style28"><xsl:value-of select="//lang_blocks/tnt"/></span><br/><br/>
			   		<ul>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_TNT.html" target="_blank" class="style28 royal">ENGLISH</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_TNT.html.fr" target="_blank" class="style28 royal">FRENCH</a></li>
					<li class="blue_bullet"><a href="//www.clubshop.com/present/biz-opp/pps_TNT.html.it" target="_blank" class="style28 royal">ITALIAN</a></li>
					
					</ul>
			  </div>-->
			 
			   
			   
			</div>
					
				
			
		

	
	
	
		
	
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>