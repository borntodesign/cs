<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />





<title>CLUBSHOP REWARDS PROMOTIEARTIKELEN</title>



<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />

</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;" width="1000">

<tbody>

<tr>

<td valign="top">

<table border="0" cellpadding="0" cellspacing="0" id="Table_01" width="1000">

<tbody>

<tr>

<td colspan="12" valign="top"><!--<img src="/images/headers/products-clubshop-1000x138.png" mce_src="../../images/headers/products-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" />-->

<div style="background-image: url(../../outletsp/products-clubshop-1000x138.png); width: 1000px; background-repeat: no-repeat; height: 138px;">

                  <div style="position: relative; text-align: center; width: 267px; height: 19px; top: 25px; left: 582px;"><span class="style2">|</span><a class="nav_footer_white_padding" href="../../outletsp/bcenter.html">Home</a><span class="style2">|</span></div>

</div>

</td>

</tr>

<tr>

<td colspan="2" style="background-image:url(../../images/bg/bg_left.png)"></td>

<td bgcolor="#ffffff" colspan="8">

<div align="center" class="style12">ClubShop Rewards Promotieartikelen <br /><br /><span class="style24">Verzendkosten zijn altijd in de prijs inbegrepen.</span><br />

<p class="Text_Body_Content">Denk eraan in te loggen met het correcte ID nummer (Lidmaatschap of Handelaar) <br />Kaarten &amp; brochures worden automatisch toegewezen aan het ID nummer waarmee is ingelogd en is onomkeerbaar.</p>

</div>

<hr />

<table align="center" bgcolor="#fcbf12" border="0" cellpadding="3" cellspacing="1" width="96%">

<tbody>

<tr>

<td align="center" bgcolor="#fca641" class="Text_Body_Content" colspan="3" valign="top"></td>

</tr>

<tr>

<td align="left" class="Text_Body_Content" colspan="3" valign="top">

<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
                          <tbody>
                            <tr> 
                              <td align="left" bgcolor="#ffffff" valign="top" width="27%"><span class="style12"><img alt="plastic cards" height="90" src="../../outletsp/cscard_key.gif" width="220" /></span></td>
                              <td align="left" bgcolor="#ffffff" valign="top" width="49%"><span class="style12">Plastic 
                                ClubShop Rewards kaarten met 2 afneembare mini-kaarten 
                                (sleutelhanger)</span> <br /> <br />
                                Deze kaarten zijn beschikbaar in de talen Engels, 
                                Nederlands, Frans en Italiaans en af te nemen 
                                per 10, 100, 500, 1000 of per 5000.<br /> <br /> 
                                <span class="text_orange">Pay punten vari&euml;ren 
                                volgens het bestelde aantal.</span></td>
                              <td align="left" bgcolor="#f4f4f4" valign="top" width="24%"> 
                                <ul>
                                  <li value="0"><a class="nav_blue_line" href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=12&SearchDept.x=18&SearchDept.y=9" target="_blank">INTERNATIONAAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff"><img alt="ClubShop Rewards Brochure" height="161" src="../../outletsp/cs_brochure.gif" width="208" /></td>
                              <td bgcolor="#ffffff"><span class="style12">ClubShop 
                                Rewards brochure met afneembare kaart</span> (<a href="../../outletsp/brochure_view.html" target="_blank">Bekijk</a>) 
                                <br /> <br />
                                Perfect om uit te delen als kennismaking of bij 
                                massa kaartverdeling en zijn verkrijgbaar in de 
                                talen Engels, Nederlands, Frans en Italiaans en 
                                af te nemen per 10, 100, 500, 1000 of per 5000. 
                                <br /> <br /> <br /> <br /> <span class="text_orange">Pay 
                                punten vari&euml;ren volgens het bestelde aantal.</span></td>
                              <td bgcolor="#f4f4f4"> <ul>
                                  <p>&nbsp;</p>
                                  <li value="0"><a href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=8&amp;SearchDept.x=10&amp;SearchDept.y=15" target="_blank">INTERNATIONAAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff"><img alt="window stickers" height="122" src="../../outletsp/cs_rwcard_window_sticker.png" width="122" /></td>
                              <td bgcolor="#ffffff"><span class="style12">ClubShop 
                                Rewards raamstickers</span><br /> <br />
                                Laat zien dat u een deelnemend ClubShop Rewards 
                                ondernemer bent met deze raamsticker met full 
                                color logo. De klever wordt aan de binnenzijde 
                                van het raam of deurvenster bevestigd en meet 
                                ongeveer 10 x 10 cm (4 x 4 inch). <br /> <br /> 
                                <br /> <span class="text_orange">Pay punten: 1.0</span></td>
                              <td> <ul>
                                  <li value="0"><a class="nav_blue_line" href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=6&SearchDept.x=17&SearchDept.y=13" target="_blank">INTERNATIONAAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff" height="187"><img height="175" src="../../outletsp/images/doorhangers.gif" width="110" /></td>
                              <td bgcolor="#ffffff"><span class="style12">Transparante 
                                plastic zakken - Deurhangers</span> <br />
                                Perfecte manier om kaarten te verdelen in de omgeving 
                                van lokale handelaren. Stop er een kaart met folder 
                                of een brochure met afneembare kaart in, een lijst 
                                met de deelnemende bedrijven en een brief met 
                                uw contactgegevens of visitekaartje en hang de 
                                zak aan de deurknop. 100 zakken per bundel. <br /> 
                                <br /> <br /> <p><span class="text_orange">Pay 
                                  punten vari&euml;ren volgens het bestelde aantal.</span></p></td>
                              <td> <ul>
                                  <li value="0"><a href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=23&amp;SearchDept.x=12&amp;SearchDept.y=15" target="_blank">INTERNATIONAAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff" colspan="3">Mocht u enige 
                                vragen hebben betreffende de prijsstelling van 
                                co-branded ClubShop Rewards kaarten, gelieve contact 
                                op te nemen met <a href="mailto:clubshoprewards@clubshop.com?Subject=ClubShop_Rewards_Card_Question">clubshoprewards@clubshop.com</a>. 
                              </td>
                            </tr>
                          </tbody>
                        </table>

</td>

</tr>

<tr>

<td align="left" bgcolor="#fca641" class="Text_Body_Content" colspan="3" height="7" valign="top"></td>

</tr>

</tbody>

</table>

<p>&nbsp;</p>

</td>

<td colspan="2" style="background-image:url(../../images/bg/bg_right.png)"></td>

</tr>

<tr>

<td colspan="12"><img height="62" src="../../images/general/clubshop_rewards_merchant_page_16.png" width="1000" /></td>

</tr>

<tr>

<td colspan="12" height="134" style="background-image:url(../../images/general/footerbar.png)"></td>

</tr>

<tr>

<td><img height="1" src="../../images/general/spacer.gif" width="34" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="14" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="146" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="142" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="119" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="90" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="38" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="128" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="133" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="109" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="13" /></td>

<td><img height="1" src="../../images/general/spacer.gif" width="34" /></td>

</tr>

</tbody>

</table>

</td>

</tr>

</tbody>

</table>

</body>

</html>