<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>   Produits promotionnels du Clubshop Rewards</title>

<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;" width="1000">
<tbody>
<tr>
<td valign="top">
<table border="0" cellpadding="0" cellspacing="0" id="Table_01" width="1000">
<tbody>
<tr>
<td colspan="12" valign="top"><!--<img src="/images/headers/products-clubshop-1000x138.png" mce_src="../../images/headers/products-clubshop-1000x138.png" width="1000" height="138" alt="Clubshop header" />-->
<div style="background-image: url(../../outletsp/products-clubshop-1000x138.png); width: 1000px; background-repeat: no-repeat; height: 138px;">
                  <div style="left: 582px; width: 267px; position: relative; top: 25px; height: 19px; text-align: center;"><span class="style2">|</span><a class="nav_footer_white_padding" href="../../outletsp/bcenter.html">Acceuil</a><span class="style2">|</span></div>
</div>
</td>
</tr>
<tr>
<td colspan="2" style="background-image:url(../../images/bg/bg_left.png)"></td>
<td bgcolor="#ffffff" colspan="8" valign="top">
<div align="center" class="style12">Produits promotionnels du Clubshop Rewards <br /><br /><span class="style24">La livraison est toujours inclue dans le prix.</span><br />
<p class="Text_Body_Content">Merci de vous assurez que vous &ecirc;tes connect&eacute; &agrave; cette zone en utilisant le bon num&eacute;ro d'identification. (d&rsquo;adh&eacute;sion ou de commer&ccedil;ant). <br />Les cartes et les brochures sont automatiquement affect&eacute;s au num&eacute;ro d'identification avec lequel vous vous &ecirc;tes connect&eacute; et ne peut &ecirc;tre chang&eacute;.</p>
</div>
<hr />
<table align="center" bgcolor="#fcbf12" border="0" cellpadding="3" cellspacing="1" width="96%">
<tbody>
<tr>
<td align="center" bgcolor="#fca641" class="Text_Body_Content" colspan="3" valign="top"></td>
</tr>
<tr>
<td align="left" class="Text_Body_Content" colspan="3" valign="top">
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
                          <tbody>
                            <tr> 
                              <td align="left" bgcolor="#ffffff" valign="top" width="27%"><span class="style12"><br />
                                <img alt="Cartes en plastique" height="90" src="../../outletsp/cscard_key.gif" width="220" /></span></td>
                              <td align="left" bgcolor="#ffffff" valign="top" width="47%"><span class="style12">Cartes 
                                en Plastique du ClubShop Rewards avec 2 mini Porte-cl&eacute;s 
                                - porte-cartes d&eacute;tachables</span><br /> 
                                <br />
                                Ces cartes en plastique sont disponibles en anglais, 
                                n&eacute;erlandais, fran&ccedil;ais et italien 
                                et peuvent &ecirc;tre command&eacute;es via un 
                                nombre de brochure de 10, 100, 500, 1000 ou 5000.<br /> 
                                <br /> <span class="text_orange">Les points payant 
                                varieront selon le montant des commandes de carte.</span> 
                                </td>
                              <td align="left" bgcolor="#f4f4f4" valign="top" width="26%"> 
                                <ul>
                                  <li value="0"><a class="nav_blue_line" href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=13&SearchDept.x=18&SearchDept.y=13" target="_blank">INTERNATIONAL</a></li>

                                </ul>
                                </td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td align="left" bgcolor="#ffffff"><img alt="ClubShop Rewards Brochure" height="161" src="../../outletsp/cs_brochure.gif" width="208" /></td>
                              <td bgcolor="#ffffff"><span class="style12">Brochure 
                                du ClubShop Rewards avec carte d&eacute;tachable</span> 
                                (<a href="../../outletsp/brochure_view.html" target="_blank">voir</a>) 
                                <br /> <br />
                                Parfaite pour &ecirc;tre donner &agrave; des &eacute;trangers 
                                ou pour la distribution de masse. Les cartes sont 
                                disponibles en anglais, n&eacute;erlandais, fran&ccedil;ais 
                                et italien et peuvent &ecirc;tre command&eacute;es 
                                via un nombre de brochure de 10, 100, 500, 1000 
                                ou 5000 . <br /> <br /> <br /> <br /> <span class="text_orange">Les 
                                points payants varieront selon les montants de 
                                la commande de brochure.</span></td>
                              <td> <ul>
                                  <li value="0"><a href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=8&amp;SearchDept.x=10&amp;SearchDept.y=15" target="_blank">INTERNATIONAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff"><img alt="window stickers" height="122" src="../../outletsp/cs_rwcard_window_sticker.png" width="122" /></td>
                              <td bgcolor="#ffffff"><span class="style12">D&eacute;calcomanies 
                                pour les vitrines des commer&ccedil;ants du Clubshop 
                                Rewards</span><br /> <br />
                                Montrez que vous participez en tant que commer&ccedil;ant 
                                du ClubShop Rewards gr&acirc;ce au d&eacute;calcomanie 
                                pour fen&ecirc;tre &agrave; face adh&eacute;sive 
                                blanc avec le logo en couleur. Le d&eacute;calcomanie 
                                fait d&rsquo;environ 10 x 10 cm.<br /> <br /> 
                                <br /> <span class="text_orange">Point payant: 1.0</span></td>
                              <td> <ul>
                                  <li value="0"><a class="nav_blue_line" href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=6&SearchDept.x=19&SearchDept.y=10" target="_blank">INTERNATIONAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff"><img height="175" src="../../outletsp/images/doorhangers.gif" width="110" /></td>
                              <td bgcolor="#ffffff"><span class="style12">Sacs 
                                d'affichette de porte transparente </span> <p>Parfaite 
                                  pour accrocher sur les portes afin de distribuer 
                                  des cartes dans les quartiers &agrave; proximit&eacute; 
                                  des commer&ccedil;ants locaux. N'oubliez pas 
                                  d'inclure une carte avec un d&eacute;pliant 
                                  ou une brochure, avec une liste de commer&ccedil;ants 
                                  locaux et une lettre d'accompagnement avec votre 
                                  carte de visite. 100 sacs/paquet.</p>
                                <span class="text_orange">Les points payants varieront 
                                selon les montants de la commande</span></td>
                              <td> <ul>
                                  <li value="0"><a href="http://www.clubshop.com/cgi/uscart/cart.cgi?DT=23&amp;SearchDept.x=12&amp;SearchDept.y=15" target="_blank">INTERNATIONAL</a></li>
                                </ul></td>
                            </tr>
                            <tr bgcolor="#f4f4f4"> 
                              <td bgcolor="#ffffff" colspan="3">Si vous avez des 
                                questions sur la fixation de prix des marques 
                                pour nos Cartes Clubshop Rewards, veuillez contacter 
                                 <a href="mailto:clubshoprewards@clubshop.com?Subject=ClubShop_Rewards_Question">clubshoprewards@clubshop.com</a></td>
                            </tr>
                          </tbody>
                        </table>
</td>
</tr>
<tr>
                      <td align="left" bgcolor="#fca641" class="Text_Body_Content" colspan="3" height="2" valign="top"></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</td>
<td colspan="2" style="background-image:url(../../images/bg/bg_right.png)"></td>
</tr>
<tr>
<td colspan="12"><img height="62" src="../../images/general/clubshop_rewards_merchant_page_16.png" width="1000" /></td>
</tr>
<tr>
<td colspan="12" height="134" style="background-image:url(../../images/general/footerbar.png)"></td>
</tr>
<tr>
<td><img height="1" src="../../images/general/spacer.gif" width="34" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="14" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="146" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="142" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="119" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="90" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="38" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="128" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="133" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="109" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="13" /></td>
<td><img height="1" src="../../images/general/spacer.gif" width="34" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>