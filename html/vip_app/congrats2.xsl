<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
<xsl:param name="alias" />	
   <xsl:template match="/">
      <html>
         <head>
            <title>GPS Partner Pending</title>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->


<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>
<link href="/css/vip_app.css" rel="stylesheet" type="text/css"/>




</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle">GPS PARTNER PENDING</h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">

<p><xsl:value-of select="//lang_blocks/p2a" /><!-- $49.95 (USD) <xsl:value-of select="//lang_blocks/p2b" />-->:
<a><xsl:attribute name="href">mailto:acfspvr@dhs-club.com?Subject=Partner Upgrade Payment - <xsl:value-of select="$alias" />
</xsl:attribute>(acfspvr@dhs-club.com)</a></p>
<p><b><xsl:value-of select="//lang_blocks/p3" />: <xsl:value-of select="$alias" /></b>
</p>
<p><b><xsl:value-of select="//lang_blocks/p5" />: <xsl:value-of select="//lang_blocks/p6" /></b></p>

<p><xsl:value-of select="//lang_blocks/p7" /></p>

<p class="royal"><xsl:value-of select="//lang_blocks/p8" /></p>


<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>


