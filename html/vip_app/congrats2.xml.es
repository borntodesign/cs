<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="congrats2.xsl" ?><lang_blocks>
  <p2a description="The first part of a sentence which has a dollar value embedded. The sentence closes with a semi-colon and is followed by an email address.">Tu solicitud de suscripciòn al GPS como Partner ClubShop Rewards, se encuentra pendiente a la recepciòn del pago de la cuota inicial. Ve a tu cuenta de Pay Pal o Payza para enviar la tasa aplicable a nuestra cuenta.</p2a>
  <p3>Asegùrate de incluir tu nùmero ID</p3>
  <p5>Importante</p5>
  <p6>No seràs aprobado como partner hasta que no recibamos los fondos en nuestra cuenta</p6>
  <p7>Dado que la forma de pago se hace manual, no seremos en grado de aprobar tu solicitud si has pagado un fin de semana o un dìa feriado, por lo tanto podremos aprobar la solicitud el primer dìa laboral despues de haber recibido el pago. Una vez que tu solicitud haya sido aprobada, te envieremos una email instruccional con la notifica de tu aprobaciòn como Partner.</p7>
  <p8>Felicitaciones por convertirte en un Partner e inciar un negocio propio CLubShop Rewards</p8>
</lang_blocks>
