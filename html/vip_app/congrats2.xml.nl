<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="congrats2.xsl" ?><lang_blocks>
  <p2a description="The first part of a sentence which has a dollar value embedded. The sentence closes with a semi-colon and is followed by an email address.">Uw aanmelding voor een abonnement op ons Global Partner Systeem (GPS) om Partner te worden is nu in behandeling in afwachting van het ontvangen van uw eerste bijdrage. ga nu naar uw PayPal of Payza account en maak het vantoepassing zijnde bedrag over naar ons account.</p2a>
  <p3>Vergeet niet uw ID nummer</p3>
  <p5>Belangrijk</p5>
  <p6>U zult niet worden goedgekeurd als Partner totdat wij een melding ontvangen dat uw betaling is overgemaakt naar ons account.</p6>
  <p7>Omdat dit handmatig moet worden uitgevoerd, kunnen wij uw aanmelding niet goedkeuren in het weekend of tijdens feestdagen, maar wij zullen deze verwerken op de eerste werkdag nadat wij de melding hebben ontvangen dat het bedrag is overgeschreven. Zodra uw aanvraag is goedgekeurd, sturen wij u een e-mail met uitleg om u op de hoogte te stellen van uw goedkeuring als Partner.</p7>
  <p8>Gefeliciteerd met het zetten van de eerste stap om Partner te worden en het starten van uw eigen ClubShop Rewards zaak!</p8>
</lang_blocks>
