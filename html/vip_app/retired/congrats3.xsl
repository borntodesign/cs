<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" 
 encoding="utf-8" 	
 doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" 	
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 	
 omit-xml-declaration="yes" 	media-type="text/html"/> 
 <xsl:param name="alias" />	  
 <xsl:template match="/">       
          
<html><head>
            <title>VIP Upgrade Pending - Receipt of Funds</title>


<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
a {
font-size: 12px;
}
a:link {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:visited {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}
a:hover {
color: #666666;
font-weight:bold;
height: 15px;
text-decoration: underline;
}
a:active {
color: #A35912;
font-weight:bold;
height: 15px;
text-decoration: none;
}

</style>

</head>
<body>
<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="10" bgcolor="#245923">
<tr>
<td valign="top" bgcolor="#FFBC23" class="Text_Content">
    
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">      
    	<tr>
        <td align="center" bgcolor="#000000"><span class="header">

<script type="text/javascript" language="JavaScript1.2">
<xsl:comment>
var message="<xsl:value-of select="//lang_blocks/p0"/> "
var colorone="#CCCCCC"
var colortwo="#CC6600"
var flashspeed=100  //in milliseconds

///No need to edit below this line///
var n=0
if (document.all||document.getElementById){
document.write('&lt;font color="'+colorone+'"&gt;')
for (m=0;m &lt;  message.length;m++)
document.write('&lt;span id="changecolor'+m+'"&gt;'+message.charAt(m)+'&lt;/span&gt;')
document.write('&lt;/font&gt;')
}
else
document.write(message)

function crossref(number){
//alert("changecolor"+number);
var crossobj=document.all? eval("document.all.changecolor"+number) : document.getElementById("changecolor"+number);
return crossobj
}

function color(){

//Change all letters to base color
if (n==0){
for (m=0;m &lt;  message.length;m++)

//eval("document.all.changecolor"+m).style.color=colorone
crossref(m).style.color=colorone
}

//cycle through and change individual letters from colorone to colortwo
crossref(n).style.color=colortwo

if (n &lt; message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("begincolor()",12000)
return
}
}

function begincolor(){
if (document.all||document.getElementById)
flashing=setInterval("color()",flashspeed)
}
begincolor()

//</xsl:comment>
</script>


</span></td>
      </tr>
      
      
      <tr>
       <td background="/images/bg_header.jpg"><img src="http://www.clubshop.com/images/gi.jpg" width="640" height="70" /></td>

      </tr>
      
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="10">


<tr><td align="right"><a href="#" class="nav_footer_grey" onClick="window.print();return false"><img src="/images/icon_print_page.gif" alt="Print Page" width="16" height="15" hspace="8" border="0" align="absmiddle" />PRINT PAGE</a>  <a href="javascript:window.close();"><img src="/images/icon_close_window.gif" alt="Close Window" width="88" height="16" border="0" align="absmiddle" /></a><a href="javascript:window.close();" class="nav_footer_brown"></a><span class="Header_Main"> </span>

              <hr align="left" width="100%" size="1" color="#A35912"/></td>
          </tr>


<tr><td>

<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1a" /></span>

 <p><xsl:value-of select="//lang_blocks/p1" /><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/pay2-1" /> $79.95 (USD) <xsl:value-of select="//lang_blocks/pay2-2" />: 
 <blockquote> <xsl:for-each select="//lang_blocks/p2/*"><xsl:value-of select="." /><br /> </xsl:for-each></blockquote>
   <p><xsl:value-of select="//lang_blocks/p8" /></p>
   <b><xsl:value-of select="//lang_blocks/p3" />
<!-- we have no parameters to use
: <xsl:value-of select="$alias" />
-->
</b></p>

<p><b><xsl:value-of select="//lang_blocks/p4" />: <xsl:value-of select="//lang_blocks/p5" /></b></p>
<p><xsl:value-of select="//lang_blocks/p6" /></p>
<p class="a"><xsl:value-of select="//lang_blocks/p7" /></p>   

<br/><br/><br/><br/><br/>



     
</td></tr>


</table></td>
      </tr>
    </table>
    </td>
  </tr>
  
  <tr><td align="center"><span class="footer_copyright">| <a href="/manual/index.xml" target="_blank" class="nav_header">Home</a> | <a href="https://www.clubshop.com/vip/control/index.shtml" target="_blank" class="nav_header">VIP Control Center</a> |  <a href="/manual/siteindex.html" target="_blank" class="nav_header">Site Index</a></span></td></tr>
                <tr><td align="center"><div align="center"><img src="/images/icons/icon_copyright.gif" height="20" width="306" alt="copyright"/></div></td></tr>

</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>