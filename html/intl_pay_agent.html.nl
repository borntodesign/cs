﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html><head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

            <title>Business Manual</title>





<link href="/css/gi_pages.css" rel="stylesheet" type="text/css"/>

<style type="text/css">



a {

font-size: 12px;

}

a:link {

color: #A35912;

font-weight:bold;

height: 15px;

text-decoration: none;

}

a:visited {

color: #A35912;

font-weight:bold;

height: 15px;

text-decoration: none;

}

a:hover {

color: #666666;

font-weight:bold;

height: 15px;

text-decoration: underline;

}

a:active {

color: #A35912;

font-weight:bold;

height: 15px;

text-decoration: none;

}



body {

   background-color: #245923;

} 



</style>



</head>

<body>
<div align="center">
<table bgcolor="#245923" border="0" cellpadding="3" cellspacing="10" width="950">
<tbody>
<tr>
<td bgcolor="#ffbc23" class="Text_Content" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td><img align="left" height="70" src="http://www.clubshop.com/images/gi.jpg" width="640" /></td>
</tr>
<tr>
<td bgcolor="#ffffff">
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="right"><a href="javascript:window.close();"><img alt="Close Window" border="0" height="16" src="/images/icon_close_window.gif" width="88" /></a><a class="nav_footer_brown" href="javascript:window.close();"></a><span class="Header_Main"> </span>
<hr align="left" size="1" width="100%" />
</td>
</tr>
<tr>
<td>
<table cellpadding="5" width="100%">
<tbody>
<tr>
<td bgcolor="#ffffff" valign="top">
<div align="left"><span class="Header_Main">Internationaal Pay Agent Programma</span>
<p>Eén van de struikelblokken bij het opbouwen van een internationale organisatie van leden, Partners en VIPs, zijn de hoge kosten, late uitbetalingen en het verzilveren van ClubCash. Our International Pay Agent Program authorizes and enables Partners that live in countries that do not have Pay Pal or Western Union available to send funds or Pay Pal to receive commissions, to overcome this problem by acting as "Pay Agents".</p>
<p><span class="Header_Main">ClubCash Pay Agents</span></p>
<p>Iedere VIP die buiten de V.S. of Canada woont, kan optreden als Pay Agent mocht een ClubShop Rewards lid in hun organisatie wensen hun ClubCash verzilverd te zien via bankoverschrijving. Bij dit gebeuren zal ClubShop Rewards het opgevraagde bedrag storten op de ClubAccount van de VIP, die op de hoogte wordt gebracht van het uit te betalen bedrag via e-mail, samen met de bankgegevens van het verzoekende lid.</p>
<p>Dit bedrag zal het toepasselijke tarief afgetrokken zien, daar het de VIP Pay Agent dient ter compensatie van de bankverrichting.</p>
<p>Bijvoorbeeld: Een lid wenst 20 Euro ClubCash te verzilveren, daarbij hoort een tarief van 2 Euro. <span style="color: #000000;">20 Euro </span>wordt geconverteerd naar het equivalent in U.S.D. en gestort op de ClubAccount van de VIP Pay Agent, daarvan dient de Pay Agent 18 Euro (20 - 2 = 18) over te schrijven naar de bankrekening van het verzoekende lid.</p>
<p>VIP Pay Agents hebben na ontvangst van het bericht en de nodige informatie via e-mail, 72 uur om te overschrijving uit te voeren.</p>
<p><span class="Header_Main">Commissies Pay Agents</span></p>
<p>Commissions Pay Agents are sent by wire transfer or other electronic means, the commissions of other Partners that may live in the same country. Op hun beurt sturen zij de gebruikmakende Partners hun verdiensten door in hun eigen lokale munteenheid. Zijn er meerdere Pay Agents in een regio, dan zullen de Agents met een lager level enkel verantwoordelijk zijn voor de uitbetaling in hun eigen downline.</p>
<p>Dit uit te betalen totaal wordt iedere maand elektronisch verstuurd, voorafgegaan door de gegevens en munteenheid conversie data van alle uit te betalen Partners. Pay Agents wordenen het equivalent van $3.00 vergoeding per Partner betaling vergoed, ter compensatie van de bankkosten. Na ontvangst van de uit te betalen fondsen moeten Pay Agents binnen 72 uur overschrijven.</p>
<p>Dit zorgt voor een snelle uitbetaling in de munteenheid van de desbetreffende leden!</p>
<p>In order for Partners to be paid in their local currency by a Pay Agent, they have to submit a disbursement request to their assigned Pay Agent at their ClubAccount. </p>
<table bgcolor="#999999" border="0" cellpadding="6" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#ffffff"><strong><a href="https://www.clubshop.com/cgi/comm_prefs.cgi">Voorkeur van Uitbetaling</a></strong></td>

<td align="center" bgcolor="#ffffff"><a href="http://www.clubshop.com/pay_agent_app.xml">Aanvraag tot Pay Agent</a></td>
<td  bgcolor="#FFFFFF" align="center"><a href="http://www.clubshop.com/pay_agent_list.xml ">Approved Pay Agents</a></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center"><span class="footer_copyright">| <a class="nav_header" href="/manual/index.xml" target="_blank">Home</a> | <a class="nav_header" href="https://www.clubshop.com/vip/control/index.shtml" target="_blank">VIP Control Center</a> | <a class="nav_header" href="/ppreport.html" target="_blank">Business Center</a> |</span></td>
</tr>
<tr>
<td align="center">
<div align="center"><img alt="copyright" height="20" src="/images/icons/icon_copyright.gif" width="306" /></div>
</td>
</tr>
</tbody>
</table>
</div>
</body>

</html>