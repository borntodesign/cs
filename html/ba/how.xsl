<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml"/>
<xsl:param name="referer"/>
<xsl:template match="/">
<base>
<xsl:copy-of select="*/referer" />
<htm>
<h2><xsl:value-of select="//lang_blocks/head1"/></h2>

<h5><xsl:value-of select="//lang_blocks/subhead1"/></h5>
<p class="a"><xsl:value-of select="//lang_blocks/p7"/></p>
<img src="pp_rebate1.gif" width="425" height="92" alt=""/><br/><br/>

<h5><xsl:value-of select="//lang_blocks/subhead2"/></h5>
<p class="a"><xsl:value-of select="//lang_blocks/p9"/></p>
<img src="pp_referral.gif" width="470" height="127"/>
<p class="a"><xsl:value-of select="//lang_blocks/p11"/></p>

<h5><xsl:value-of select="//lang_blocks/subhead3"/></h5>
<p class="a"><xsl:value-of select="//lang_blocks/p12"/></p>
<img src="pp_paypoints.gif" width="470" height="127" alt="" />
<p class="a"><xsl:value-of select="//lang_blocks/p14"/></p>
<p class="a"><xsl:value-of select="//lang_blocks/p15"/></p>


<p class="a"><img src="checkbox.gif" width="20" height="20" alt=""/>
<a><xsl:attribute name="href">/cgi/appx.cgi/<xsl:value-of select="$referer"/>/ba</xsl:attribute><xsl:value-of select="//lang_blocks/p16"/></a></p>

</htm></base>
</xsl:template>
</xsl:stylesheet>