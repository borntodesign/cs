<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>


	<xsl:param name="referer" />

<xsl:template match="/">

<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClubShop Mall Banner Advertising</title>
<link href="ba.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table id="container">
<tr><td colspan="2" style="text-align:left" background="/images/bg_header.jpg">
<img src="/images/headers/header_banner_adv.png" alt="" width="640" height="70" />
</td></tr>
<tr>
<td id="sidelinks">
<h4>Links</h4>
<a><xsl:attribute name="href">index.xsp?referer=<xsl:call-template name = "referer" /></xsl:attribute>Index</a>
<h4></h4>
<a><xsl:attribute name="href">howitworks.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>How It Works</a>
<h4></h4>
<a><xsl:attribute name="href">about.xml?referer=<xsl:call-template name = "referer" /></xsl:attribute>About The DHS Club</a>
<h4></h4>
<a><xsl:attribute name="href">/cgi/appx.cgi/<xsl:call-template name = "referer" />/ba</xsl:attribute>JOIN FOR FREE</a>
<h4></h4>


</td>
<!--end heading and Nav-->

<td style="width: 587px" id="main">
<!--tdmain-->

<xsl:copy-of select = "//htm/*"/>

<!--end td main-->

</td></tr></table>

</body></html>

</xsl:template>


<xsl:template name="referer">
<xsl:choose>
	<xsl:when test="*/referer/id">
	
	<xsl:value-of select = "*/referer/id" />
	</xsl:when>
  
	<xsl:otherwise>
	<xsl:value-of select="$referer"/>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>


