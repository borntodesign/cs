<?xml version="1.0" ?>
<lang>
<h2>Dobrodošli v baner oglaševalskem programu ClubShop trgovskega centra.</h2>
<p>DHS Klub (Domač Nakupovalni Klub s Popustom)ponuja brezplačno Članstvo v edinstvenem nakupovalnem klubu zato, da priskrbi našim članom resnično skupinsko kupno moč! Člani so sposobni nakupovati pri stotih prodajalnah na našem spletu, mednarodnem ClubShop trgovskem centru in po rastočem seznamu pridruženih prodajaln zaslužiti dragocene nagradne točke od nakupov, jih zamenjati za darilne bone ali za v denarju vrnjen rabat.</p>
<p style="text-align: center">
<img class="b" src="target.gif" width="29" height="41" alt="target" />
<img class="b" src="homedepot.gif" width="39" height="37" alt="homedepot" />
<img class="b" src="oldnavy.gif" width="65" height="23" alt="oldnavy" />
<img class="b" src="bestbuy.gif" width="73" height="45" alt="bestbuy" />
<img class="b" src="sears.gif" width="71" height="35" alt="sears" />
<img class="b" src="staples.gif" width="65" height="33" alt="staples" /></p>
<p>Kot član DHS Kluba imate tudi priložnost zaslužiti s priporočanjem da drugi postanejo člani enostavno tako, da namestite baner oglas na vašo spletno stran!</p>
<p class="a"><a href="http://www.clubshop.com/manual/policies/privacy.xml">Vaša zasebnost</a>je pomembna za nas</p>
</lang>