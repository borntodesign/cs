<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml"/>

<xsl:template match="/">
<base>

<xsl:copy-of select = "*/referer"/>
<htm>
<h2><xsl:value-of select="//lang_blocks/head1"/></h2>
<table cellspacing="5"><tr><td>
<img src="DickB_Pic.gif"/></td><td><xsl:value-of select="//lang_blocks/p17"/></td>
</tr></table>

<table><tr><td>
<xsl:value-of select="//lang_blocks/p18"/></td><td><img src="dhscheadquartersx.jpg"/></td>
</tr></table>
<xsl:value-of select="//lang_blocks/p19"/>
<xsl:value-of select="//lang_blocks/p20"/>

<h2></h2>

<h5><xsl:value-of select="//lang_blocks/subhead"/></h5>
<img src="cartworld.gif"/>
<ul><xsl:value-of select="//lang_blocks/p21"/></ul>
<ul><xsl:value-of select="//lang_blocks/p22"/></ul>
<ul><xsl:value-of select="//lang_blocks/p23"/></ul>
<ul><xsl:value-of select="//lang_blocks/p24"/></ul>
</htm></base>

</xsl:template>
</xsl:stylesheet>