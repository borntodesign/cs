<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
 
<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script> 


<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>





</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>
	
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">




<p class="style24"><xsl:value-of select="//lang_blocks/p2"/></p>



<p><xsl:value-of select="//lang_blocks/p3"/></p>
<ol>
		<li><xsl:copy-of select="//lang_blocks/p4/*|//lang_blocks/p4/text()"/><br/><br/>

			<ul>
				<li class="half"><span class="medblue"><b><xsl:value-of select="//lang_blocks/p44"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p44a"/><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p45"/> <br/><span class="darkblue"><xsl:value-of select="//lang_blocks/p46"/></span></li><br/>
				<li class="half"><xsl:copy-of select="//lang_blocks/p48/*|//lang_blocks/p48/text()"/><br/><br/>	<xsl:copy-of select="//lang_blocks/p47/*|//lang_blocks/p47/text()"/><br/>
				</li>

			</ul>
		</li>
		
		<li><span class="medblue"><b><xsl:value-of select="//lang_blocks/p59"/></b></span><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p60"/>
			<ul>
			<li class="half"><xsl:value-of select="//lang_blocks/p61"/></li>
			<li class="half"><xsl:value-of select="//lang_blocks/p62"/></li>
			<li class="half"><xsl:value-of select="//lang_blocks/p63"/></li>
			</ul>
		</li>
		
		
		
		
		
		
		
		<li><img src="//www.clubshop.com/cs/images/design/clubshop_shopper_landing_page.png" align="right" height="257" width="224" alt="clubshop rewards card"/><xsl:text> </xsl:text><xsl:copy-of select="//lang_blocks/p5/*|//lang_blocks/p5/text()"/>
				<br/><br/>
				<p><xsl:value-of select="//lang_blocks/p10"/></p>
				<p><xsl:value-of select="//lang_blocks/p55a"/><xsl:text> </xsl:text><a href="//www.clubshop.com/gps_new.html" target="_blank"><xsl:value-of select="//lang_blocks/p56a"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p57a"/></p>

		</li>
		
		<li><xsl:value-of select="//lang_blocks/p58"/><br/>https://www.clubshop.com/cgi/appx/<span class="style3">IDNUMBER</span>/af</li>
				
		
		

		
		
		
		</ol>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		







<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2016
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
