﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="affiliatefaq.xsl" ?><lang_blocks>
  <p0>Affiliate FAQ (Veel gestelde vragen)</p0>
  <p12>Is dit enkel voor mensen die online willen winkelen?</p12>
  <p13>NEE! ClubShop Rewards biedt wereldwijd online EN offline beloningen en besparingen bij hun deelnemende ondernemers. Dit maakt dat IEDEREEN een potentieel lid is die u kunt verwijzen.</p13>
  <p14>Welk soort ondernemer kan de ClubShop Rewards kaart accepteren?</p14>
  <p15>Elk bedrijf, winkel of ondernemer kan de kaart accepteren, inclusief restaurants, snackbars, detailhandel zaken, aannemers en professionele diensten.</p15>
  <p16>Waar kan ik een lijst met bedrijven vinden die kortingen of rewards (beloningen) aanbieden?</p16>
  <p17>U vindt hier een lijst met de lokale ondernemers:</p17>
  <p18>Ziet u deelnemende bedrijven in de regio waar u woont, dan is dit geweldig! U kunt nu een ClubShop Rewards kaart aanschaffen en zelf gebruiken bij die aangesloten ondernemers en iedere keer dat u uw kaart voorlegt genieten van directe kortingen of cashbacks. Het geeft u ook de mogelijkheid om onmiddellijk ClubShop Rewards kaarten te verdelen en te verkopen. Iedere keer dat een geregistreerde kaarthouder vervolgens ook online winkelt in de ClubShop Mall, verdient u een commissie op die aankoop.</p18>
  <p2>Welke producten en diensten zijn beschikbaar om te worden verkocht door ClubShop Rewards?</p2>
  <p21>Verdien ik iedere keer een commissie als een ClubShop Rewards lokaal winkelt?</p21>
  <p22>ClubShop Rewards heeft twee typen winkels en bedrijven in het programma. één type handelaar wordt een &quot;trackable&quot; handelaar genoemd. Dit is een handelaar die ClubShop Rewards een % betaalt op iedere gerealiseerde verkoop als referral fee en iedere aankoop gemaakt door een kaarthouder bijhoudt. Wanneer kaarthouders hun kaart gebruiken bij trackable winkels en bedrijven, dan verdient u een commissie. Het tweede type noemen we de Directe Korting Ondernemers. Deze winkels en bedrijven houden de verkopen/aankopen niet bij, zij geven een directe korting wanneer een ClubShop Rewards kaart voorgelegd wordt. Er kan geen commissie uitbetaald worden op verkopen bij Directe Korting Ondernemers.</p22>
  <p3>Zowat alles! Als mensen het kunnen kopen, kunt u uw ClubShop Rewards lidmaatschap gebruiken om het te verkopen, maar met één verschil - u kunt uw klanten de mogelijkheid aanbieden om op vrijwel alles waar ze geld aan uitgeven, kortingen of cashbacks te krijgen! Hoe? Door mensen te verwijzen naar de wereldwijde kopersclub - ClubShop Rewards.</p3>
  <p32>Heb ik toegang tot de basis trainingsmaterialen, marketing hulpmiddelen en rapporten om mij hierbij te helpen?</p32>
  <p33>Ja. Ga naar de Affiliate Informatie links op uw Business Center.  Als GPS abonnee heeft u toegang tot geavanceerde training cursussen om te leren hoe u een wereldwijde verkooporganisatie kunt opbouwen en hoe u non-profit organisaties en winkels/ bedrijven verwijst.</p33>
  <p4>Hoe krijg ik mijn potentiële klanten ertoe ClubShop Rewards Member te worden?</p4>
  <p47>Heb ik toegang tot rapporten die de verkopen en commissies bijhouden?</p47>
  <p48>Ja</p48>
  <p49>Kan ik anderen verwijzen waarvan ik weet dat zij misschien Affiliates willen worden om met zaak te helpen uitbreiden?</p49>
  <p5>Dat is heel eenvoudig! U geeft een gratis lidmaatschap aan mensen die enkel online willen winkelen in de ClubShop Rewards Mall en Cash Back Rewards verdienen. En u verkoopt en verdeelt ClubShop Rewards kaarten aan mensen die Directe  Korting of Cash Back Rewards willen ontvangen wanneer ze offline winkelen, eten of zaken doen bij deelnemende ondernemers. U dient enkel hun toestemming te verkrijgen om hen de URL van uw persoonlijke ClubShop Rewards website toe te sturen zodat ze zich kunnen inschrijven. U kunt uw URL online adverteren en op lokale plaatsen.</p5>
  <p50>Ja dat kunt u, maar u moet wel Partner worden om het recht te hebben anderen te verwijzen om Partners, Winkels/ bedrijven en non-profit organisaties te worden. Om meer te weten te komen over het kwalificeren om Partner te worden, klikt u op de &quot;Kwalificeer als Partner&quot; link in uw account.</p50>
  <p51>Hoeveel moet ik investeren om een Partner te worden?</p51>
  <p52>De gemakkelijkste en beste manier om Partner te worden is door het abonneren op het</p52>
  <p52a>Global Partner System (GPS)</p52a>
  <p52b>, maar u kunt als Partner kwalificeren door elke maand 12 Pay punten te verdienen op de aankopen van uw Members.</p52b>
  <p53>Wat gebeurt er als een Member of Affiliate die ik heb verwezen, besluit om Partner te worden?</p53>
  <p54>Dan verliest u alle sponsoring erkenning, privileges en voordelen voor hen, tenzij u Partner wordt, omdat uw Partner sponsor anders hun Sponsor zal worden. Om de sponsor te blijven voor alle Members of Affiliates die u heeft verwezen indien zij Partner worden), heeft u tot het einde van de maand waarin uw Member of Affiliate Partner is geworden, om zelf ook Partner te worden.</p54>
  <p57>NIEUW - KRACHTIG- EENVOUDIG - LEUK!</p57>
  <p6>Hoe kan ik geld verdienen wanneer de leden en kaarthouders die ik verwijs geld besparen?</p6>
  <p61>Ik heb een aantal ClubShop Rewards kaarten verkocht of weggegeven, maar waar kan ik de status van deze kaarten zien?</p61>
  <p7>Door de groepskoopkracht! ClubShop Rewards onderhandelt met de ClubShop winkelcentrum winkels om een referral fee te krijgen iedere keer dat een klant (ClubShop Rewards lid) in het winkelcentrum de link van een winkel aanklikt en daar een aankoop doet. Alles wordt automatisch elektronisch bijgehouden. Vervolgens betaalt ClubShop Rewards 40%, van de referral fee die ze ontvingen van de winkel, terug aan het aankopende lid in Cash Back Rewards. Nog eens 40% wordt uitbetaald in commissies en ClubShop Rewards behoudt de resterende 20% van de referral fee.</p7>
  <p8>Heb ik zelf toegang tot die besparingen?</p8>
  <p9>Jazeker! Als een Affiliate hebt u tevens een ClubShop Rewards lidmaatschap. Winkel via het ClubShop winkelcentrum met uw ID nummer en u verdient geld terug op uw aankoop in de vorm van ClubCash EN Pay punten. Hierdoor kunt u bijna twee keer zoveel besparen op uw persoonlijke aankopen als gewone ClubShop Rewards Members.</p9>
</lang_blocks>
