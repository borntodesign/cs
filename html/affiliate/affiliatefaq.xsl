<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>
            
            
	<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style type="text/css">

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FFB404;
	font-weight:bold;
}




</style>

</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
 








<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p2"/></b></span><br/><xsl:value-of select="//lang_blocks/p3"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p4"/></b></span><br/><xsl:value-of select="//lang_blocks/p5"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p6"/></b></span><br/><xsl:value-of select="//lang_blocks/p7"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p8"/></b></span><br/><xsl:value-of select="//lang_blocks/p9"/><br/><br/>


<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p12"/></b></span><br/><xsl:value-of select="//lang_blocks/p13"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p14"/></b></span><br/><xsl:value-of select="//lang_blocks/p15"/><br/><br/>


<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p16"/></b></span><br/><xsl:value-of select="//lang_blocks/p17"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/maps/rewardsdirectory.shtml" target="_blank" class="Nav_Green_Plain">http://www.clubshop.com/maps/rewardsdirectory.shtml</a> <br/><br/><xsl:value-of select="//lang_blocks/p18"/><br/><br/>

<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p19"/></b></span><br/><xsl:value-of select="//lang_blocks/p20"/><br/><br/>-->

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p21"/></b></span><br/><xsl:value-of select="//lang_blocks/p22"/><br/><br/>






<!--<p class="Gold_Large_Bold"><xsl:value-of select="//lang_blocks/p23"/></p>
<span class="footer_divider"><xsl:value-of select="//lang_blocks/p24"/></span><br/><xsl:value-of select="//lang_blocks/p25"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p26"/></b></span><br/><xsl:value-of select="//lang_blocks/p27"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p28"/></b></span><br/><xsl:value-of select="//lang_blocks/p29"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p30"/></b></span><br/><xsl:value-of select="//lang_blocks/p31"/><br/><br/>-->
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p32"/></b></span><br/><xsl:value-of select="//lang_blocks/p33"/><br/><br/>




<!--<p class="Gold_Large_Bold"><xsl:value-of select="//lang_blocks/p34"/></p>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p35"/></b></span><br/><xsl:value-of select="//lang_blocks/p36"/>

<ol>
<li><xsl:value-of select="//lang_blocks/p37"/></li>
<li><xsl:value-of select="//lang_blocks/p38"/></li>
<li><xsl:value-of select="//lang_blocks/p39"/></li>
</ol>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p40"/></b></span><br/><xsl:value-of select="//lang_blocks/p41"/><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p42"/></b></span><br/><xsl:value-of select="//lang_blocks/p43"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/registration.cgi" target="_blank" class="Nav_Green_Plain">https://www.clubshop.com/cgi/registration.cgi</a> <br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p44"/></b></span><br/><xsl:value-of select="//lang_blocks/p45"/><br/><br/>


<br/>-->

<!--<p class="Gold_Large_Bold"><xsl:value-of select="//lang_blocks/p46"/></p>-->

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p47"/></b></span><br/><xsl:value-of select="//lang_blocks/p48"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/cgi/member_ppp_rpt.cgi" target="_blank">https://www.clubshop.com/cgi/member_ppp_rpt.cgi</a><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p61"/></b></span><br/><a href="https://www.clubshop.com/cgi/referral_cards.cgi" target="_blank">https://www.clubshop.com/cgi/referral_cards.cgi</a><br/><br/>

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p49"/></b></span><br/><xsl:value-of select="//lang_blocks/p50"/><br/><br/>

<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p55"/></b></span><br/><xsl:value-of select="//lang_blocks/p56"/><br/><br/>-->
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p53"/></b></span><br/><xsl:value-of select="//lang_blocks/p54"/><br/><br/>



<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p58"/></b></span><br/>
<span class="bold"><xsl:value-of select="//lang_blocks/p59"/></span><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cgi/joinpool.cgi" target="_blank">http://www.glocalincome.com/cgi/joinpool.cgi</a><br/>
<span class="bold"><xsl:value-of select="//lang_blocks/p60"/></span><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cgi/poolout.cgi" target="_blank">http://www.glocalincome.com/cgi/poolout.cgi</a><br/><br/>
-->

<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p51"/></b></span><br/><xsl:value-of select="//lang_blocks/p52"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/gps_new.html" target="_blank"><xsl:value-of select="//lang_blocks/p52a"/></a><xsl:text> </xsl:text><xsl:value-of select="//lang_blocks/p52b"/><br/><br/>



<br/><br/>
<hr/>
<div align="center"><span class="Gold_Large_Bold"><xsl:value-of select="//lang_blocks/p57"/></span></div>


<br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2016
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>