﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="referring.xsl" ?><lang_blocks>
  <p0>GUIDE POUR MEMBRES REFERANTS</p0>
  <p10>Une des meilleures raisons pour laquelle les gens aiment référencer des gens à notre adhésion ClubShop Rewards ou vendre des cartes ClubShop Rewards est que cela leur permet de gagner des commissions de vente sur des millions de produits et services que les gens peuvent utiliser tous les jours, sans avoir à prendre les commandes, à maintenir l'inventaire des produits ou à s'inquiéter de la livraison des produits. Vous faites de l'argent en aidant les gens à économiser de l'argent !</p10>
  <p2>Comment référencer les membres ClubShop gratuits</p2>
  <p3>En tant qu'Affiliare, vous pouvez gagner de l'argent en référençant des gens à l'un des clubs d'acheteur ayant des remises locales et en ligne grandissant le plus vite dans le monde - ClubShop Rewards. Plutôt que d'acheter, vendre et livrer des produits pour faire de l'argent, vous pouvez gagner un revenu simplement en aidant les gens à économiser de l'argent. Voici comment faire:</p3>
  <p4 literal="1"><span class="medblue"><b>Vous pouvez référencer des gens pour qu'ils obtiennent une adhésion gratuite ClubShop Rewards.</b></span> Les Membres gratuits peuvent acheter en ligne et gagner du ClubCash qu'ils peuvent échanger contre des espèses. Vos Commissions Personnelles - qui sont égales à 50% des Points Payants qui vous sont crédités pour les achats effectués par les Membres que vous avez réfées - peuvent vous aider à vous qualifier en tant que Partner.</p4>
  <p44>Site Internet personnalisé</p44>
  <p44a>- Utilisez votre site internet personnalisé auto-répétitif  lorsque vous faites de la publicite en ligne aux consommateurs pour le Programme ClubShop Rewards.</p44a>
  <p45>Remplacer le Numéro d'Identification / IDNUMBER à la fin du lien ci-dessous afin de vous assurer le crédit. Si votre numéro d'identification est 1234567, voici à quoi devrait ressembler votre URL de publicité URL :</p45>
  <p46>http://www.clubshop.com/cgi-bin/rd/4,,refid=1234567</p46>
  <p47 literal="1">Veillez à tester votre URL est avant de faire la publicité, et à respecter par notre <a href="/manual/policies/spam.xml" target="_blank" class="Nav_Green_Plain"> Politique Anti-Spam</a> lors de l'envoi d'Emails à propos du Programme ClubShop Rewards.</p47>
  
  <p48 literal="1"><span class="medblue"><b>Bannière publicitaire</b></span>
   - Vous pouvez également placer une bannière publicitaire (ce qui inclut votre numéro ID ancrée) sur n'importe quelle(s) page(s) internet que vous avez, pour référencer automatiquement les autres pour qu'ils deviennent membres ClubShop Rewards.
   Quand ils achètent en ligne au centre commercial ClubShop Rewards ou hors ligne chez un commerçant participant ClubShop, vous gagnerez automatiquement une commission de référencement sur les achats qu'ils peuvent faire.
   Pour sélectionner et copier une bannière afin de la placer à votre page, allez sur le <a href="/banners/select.php" target="_blank" class="Nav_Green_Plain">Générateur de bannière.</a></p48>
  
  <p5 literal="1"><span class="medblue"><b>Vous pouvez également vendre des cartes ClubShop Rewards aux membres et aux futurs membres.</b></span> Les consommateurs peuvent utiliser leur cartes ClubShop Rewards pour acheter hors ligne avec les commerçants locaux et pour gagner les récompenses cashback ou des remises directes.
  Vous pouvez gagner un joli pourcentage de profit de détaillant en vendant chaque carte.</p5>
  
  <p55a>Si vous avez besoin de formation et d'aide pour commencer le Marketing en ligne, alors abonnez-vous</p55a>
  <p56a>GPS</p56a>
  <p57a>(Global Partner System) pour automatiquement vous qualifier en tant que Partner.</p57a>
  <p58>Si vous participez à notre Programme de Partner à l'essai gratuit, vous êtes en mesure de référer des Partners à l'essai et des Partners durant votre période d'essai de 30 jours. Vous devrez vous mettre à niveau pour devenir un Partner au cours de votre période d'essai de 30 jours afin de pouvoir conserver ces personnes référencées comme vos adhérents Personnellement Référés. Utilisez ce lien pour référer des Affiliates:</p58>
  <p59>Vous pouvez référer des gens de gagner/obtenir une adhésion gratuite d'Affiliate.</p59>
  <p60>Les Affiliates ont les mêmes avantages commerciaux que les membres qui Membres acheteurs, avec le privilège en plus d'être en mesure de référer des Affiliates, ce qui leurs donnent deux avantages supplémentaires :</p60>
  <p61>Bien que vous ne gagnerez pas de commissions de référencement pour les achats effectués par les Affiliates que vous avez référés (puisqu'ils garderont les commissions de référencement sur leurs achats personnels), vous serez crédité de tous Points Payants gagnés par un Affiliate, ce qui vous aidera à augmenter vos commissions d'équipe, si et quand vous vous qualifirez en tant que Partner.</p61>
  <p62>Vous pouvez commencer à construire une organisation de vente et de membre, qui pourra vous aider à gagner des commissions d'équipe plus élevées, quand vous serez qualifiez en tant que Partner.</p62>
  <p63>Si l'un des Affiliates que vous référez devient un Partner à l'essai ou un Partner, vous obtiendrez automatiquement la positon gratuite de Partner à l'essai pendant 30 jours, afin de vous donner des opportunités plus grandes de vous qualifier en tant que Partner afin de conserver les privilèges des adhésions pour les Partners à l'essai et les Partners que vous avez référés.</p63>
</lang_blocks>
