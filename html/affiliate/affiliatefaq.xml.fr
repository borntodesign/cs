﻿<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="affiliatefaq.xsl" ?><lang_blocks>
  <p0>FAQ pour Affiliate (Foire aux Questions)</p0>
  <p12>Est-ce seulement pour les acheteurs en ligne ?</p12>
  <p13>NON! Le ClubShop Rewards offre des points Rewards en ligne ET hors ligne et fait faire des économies chez tous les commerçants participants mondialement. Cela rend TOUT LE MONDE un potentiel membre que vous pouvez référencé.</p13>
  <p14>Quels genres de commerçants peuvent accepter la carte ClubShop Rewards?</p14>
  <p15>Chaque type d'entreprise ou magasin peut accepter la carte, y compris les restaurants, les fast-food, les détaillants, les entrepreneurs et les services professionnels.</p15>
  <p16>Où puis-je trouver une liste des commerçants locaux qui offrent des remises ou des points Rewards?</p16>
  <p17>Vous pouvez trouver une liste des commerçants locaux ici :</p17>
  <p18>Si vous voyez ici des commerçants listés pour votre région, ceci est merveilleux ! Vous pouvez acheter une carte ClubShop Rewards et l'utiliser vous-même pour obtenir des remises directes ou remises en argent chaque fois que vous présentez votre carte à l'un des commerçants participants locaux. Cela vous donne également la possibilité de démarrer la distribution et la vente de cartes ClubShop Rewards tout de suite. Et chaque fois qu'un titulaire de carte enregistré achète également en ligne au centre commercial ClubShop Rewards, vous gagnerez une commission sur cette vente.</p18>
  <p2>Quels sont les produits et services disponible vendus par le ClubShop Rewards?</p2>
  <p21>Est-ce que je gagne des commissions chaque fois qu'un membre ClubShop Rewards achète localement?</p21>
  <p22>Le ClubShop Rewards accepte deux types de commerçants dans leur programme. Un type de commerçant est appelé commerçant avec &quot;suivi&quot;. Il s'agit d'un commerçant qui paie au ClubShop Rewards un % sur chaque vente à titre de frais de référencement et qui fait un suivi de chaque achat effectué par les titulaires de carte. Lorsque les membres titulaires utilisent leur carte chez les Affiliate commerçants qui effectuent un suivi, vous recevrez une commission. Nous acceptons aussi ce que nous appelons les commerçants à remises directes. Ces commerçants ne font pas de suivi des ventes, mais ils accordent plutôt des remises directement aux membres titulaires lorsqu'ils présentent leur carte ClubShop Rewards en faisant un achat. Aucun commission n'est donnée ou payée sur les achats effectués chez les commerçants à Remise directe.</p22>
  <p3>À peu près tout! Si les gens peuvent l'acheter, vous pouvez utiliser votre entreprise ClubShop Rewards pour la vendre, mais avec une différence - vous pouvez offrir à vos clients la possibilité d'obtenir des remises ou une remise en argent sur à peu près tout ce qu'ils dépensent comme argent! Comment? En référant les gens pour qu'ils rejoignent leclub d'acheteur mondial - le ClubShop Rewards.</p3>
  <p32>Ai-je accès au matériel de formation basic, aux outils de marketing et aux rapports pour m'aider avec ceci?</p32>
  <p33>Oui. Reportez-vous aux liens d'information pour Affiliate sur votre centre d'affaires. En tant qu'abonné GPS, vous pouvez accéder aux cours de formation de perfectionnement afin d'apprendre à construire une organisation commerciale mondiale, à référer des groupes d'affinité et des commerçants.</p33>
  <p4>Comment puis-je faire que mes clients potentiels deviennent des membres ClubShop Rewards?</p4>
  <p47>Ai-je accés aux rapports de suivis de ventes et de commissions?</p47>
  <p48>Oui.</p48>
  <p49>Puis-je référencer d'autres personnes que je connais qui voudraient devenir Affiliates, Partners à l'essai ou Partners pour m'aider à développer mon entreprise ?</p49>
  <p5>C'est vraiment simple! Vous pouvez donner des adhésions gratuites aux gens pour qu'ils achètent en ligne au centre commercial ClubShop et qu'ils gagnent des points Rewards échangeables en argent et vous pouvez vendre et distribuer les cartes ClubShop Rewards aux personnes pour qu'elles obtiennent des remises directes ou des points rewards échangeable en argent quand elles achètent, mangent ou font des affaires avec les commerçants participants hors ligne. Vous avez juste à obtenir leur consentement pour leur envoyer l'URL de votre site Internet personnel ClubShop Rewards et elles peuvent s'inscrire eux-mêmes. Vous pouvez faire de la publicité sur votre ligne URL et dans des salles locales.</p5>
  <p50 new="10/16/14">Oui vous pouvez, mais si l'un de vos filleuls devient un Partner à l'essai ou un Partner, vous serez automatiquement inscrit dans notre Programme de Partner à l'essai et vous aurez 30 jours pour vous qualifier en tant que Partner pour conserver les avantages et les privilèges des parrainages pour les Partners à l'essai et les Partners que vous avez Personnellement Référés. Pour en savoir plus sur la qualification pour devenir un Partner, cliquez sur le lien "Se qualifier en tant que Partner" qui se trouve dans votre compte.</p50>

  <p51>Combien dois-je investir pour devenir un Partner?</p51>
  <p52>Le moyen le plus simple et le meilleur pour devenir Partner est en vous abonnant au</p52>
  <p52a>Global Partner Système (GPS)</p52a>
  <p52b>, mais vous pouvez vous qualifier en tant que Parnter chaque mois en gagnant 12 Points Payants provenant des achats de vos membres.</p52b>
  <p53>Qu'advient-il si un membre ou un Affiliate que j'ai référé décide de devenir un Partner?</p53>
  <p54>Vous perdrez toutes les reconnaissances de parrainage, les privilèges et les avantages que vous aviez pour eux - sauf si vous deveniez un Partner avant la fin de votre période de Partner à l'essai.</p54>
  <p57>NOUVEAU - PUISSANT - SIMPLE - AMUSANT!</p57>
  <p6>Comment puis-je gagner de l'argent quand les membres et les titulaires de la carte que j'ai référncé économisent de l'argent?</p6>
  <p61>J'ai vendu ou donné un certain nombre de cartes ClubShop Rewards, mais où puis-je voir le statut de ces cartes?</p61>
  <p7>Pouvoir d'achat du groupe! Le ClubShop Rewards négocie avec ses magasins du centre commercial ClubShop pour gagner des frais de référencement chaque fois qu'un client (membres ClubShop Rewards) clique sur un lien de magasin sur le centre commercial pour aller dans un magasin pour faire un achat. Tout est automatiquement enregistrés par voie électronique. Puis le ClubShop Rewards renvoie 40% des frais de référencement qui leur sont versés par le magasin aux points Rewards échangeable en Espèce. Un autre 40% est versé en commissions et le ClubShop Rewards garde 20% des frais de référencement.</p7>
  <p8>Ai-je personnellement accès à l'épargne?</p8>
  <p9>Oui! En tant qu'Affiliate, vous avez également un abonnement ClubShop Rewards. Achetez au centre commercial ClubShop avec votre numéro d'identification ID et vous gagnerez du Cash Back (Espèce en retour) sous la forme de ClubCash ET des Points Payants sur votre achat. Cela vous permet d'économiser près de deux fois plus sur vos achats personnels en tant que membre &quot;normal&quot; ClubShop Rewards.</p9>
</lang_blocks>
