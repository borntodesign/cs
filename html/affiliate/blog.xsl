<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


  <xsl:template match="/">
    
         <html><head>
            <title>Proprofit Weblog</title>
<link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="https://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="https://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="https://www.clubshop.com/js/partner/app.js"></script>
    <script src="https://www.clubshop.com/js/partner/flash.js"></script>
    <script src="https://www.clubshop.com/js/panel.js"></script>



<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>


<style>
.center {text-align: center;}

</style>

</head>



<body class="blue">

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="https://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container white">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<div align="center"><h4><span class="topTitle"><xsl:value-of select="//lang_blocks/bb"/></span></h4>
	<p><span class="medblue"><xsl:value-of select="//lang_blocks/bb1"/></span></p></div>

	<hr />

	</div></div>
			

      
	<div class="row">
	<div class="twelve columns">





                        <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            
            
            <!--<p><xsl:value-of select="//lang_blocks/google-disclaimer"/><br/><br/>
            <xsl:value-of select="//lang_blocks/google-instruction"/></p>-->
       <hr/>
       
       <p>A new era has just begun dear Clubshop Members and Partners! You can definitely start to be EXCITED like never before!</p>

<p>You've certainly read Dick Burke's message posted a few days ago, right? If not, please <a href="https://www.clubshop.com/affiliate/topview.xml" target="_blank">click here</a> to read it now.</p>

<p>So, you're now informed that the ClubShop Rewards business and saving opportunity has 2 new proud and hyper-excited owners: me; Fabrizio Perotti, and my long-time friend, business associate and Superhero; Giuseppe Francavilla (aka Mr. G)! We both own 

Proprofit Worldwide LTD., our UK based Company, which is the actual owner of Clubshop Rewards.</p>

<p>Dick already already introduced ourselves in his message, so I'm gonna save your valuable time, and not gonna do it again. </p>

<p>I just wanna assure you guys that, after more than two successful decades spent in the online business, embracing with infinite passion all its different aspects, we're here to add all our experience to this unique and extraordinary business, to revamp it, make it 

more modern, attractive and effective. We also want to build, together with you guys, some new amazing projects which will allow you to expand your personal business, knowledge, and...income of course, thanks to new and exciting spheres of interests and fields of 

action.</p>

<p>The Clubshop business (former DHS Club) has been in the past the first and greatest business of its kind, setting the example for many others who have tried to imitate it somehow. </p>

<p>We're determined to bring it up again to that leading position it deserves. We know there is only one way to do that, and it's exclusively by bringing tangible value to millions of people worldwide, to effectively help them to substantially improve their lifestyle and, one 

for all, get them out of the mud where, very often, their life is sadly stagnating.</p>

<p>We want to start by bringing joy, passion and enthusiasm in people's life thanks to a new and brighter vision they can now have about their future, starting today! </p>

<p>We're convinced that wealth is a right of every human being and every human being should have an easy access to it. Clubshop business opportunity is going to be one important part of the big picture which, through Proprofit Company, is going to be  shared with 

you and realized in the very next months.</p>

<p>If you haven't been thinking big so far, well...it's time you start to do so.  Because once you'll start to see what our full plans are, you'll realize you're an an essential part of something really, really great and amazing that you won't want to miss!</p>

<p>So, stay tuned guys, as very soon we're going to offer some great incentive to all former partners to join us again (and forever!) since the beginning of this new amazing adventure.</p>

<p>Fabrizio Perotti<br/>
Clubshop Rewards</p>

<p>P.S.: If you're still receiving your monthly commissions by paper check, please select PayPal as your favorite way to receive them starting from this month of May. This is because our company is based in the UK and unfortunately there is no way for us to keep 

sending checks to the US. Thank you for your understanding.</p>
       
       
       
       
       
       
       
       
       
       
       
       
           	<br/><br/><br/>	<br/><br/><br/>	<br/>
           <hr/>
           
           
            
            
            
            
                  
                  
            

 </div>        			<br/><br/><br/>
</div>
</div>



<!--Footer Container -->

<div class="container blue"> 
  <div class="row"> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>
  

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
