﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
            <title>GETTING STARTED AS AN AFFILIATE</title>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />

    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->

<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/sprintf.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.capitalize {text-transform: uppercase;}
.smaller_text{font-size: 10px;
line-height: 15px;}
</style>

<script language="javascript">

   

	$(document).ready(function() {

	var showText='Learn More';

	var hideText='Hide';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

 

</script>

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<div align="left">
<div id="google_translate_element" style="margin-top:0.5em;"></div>
<div align="left">
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
<hr />
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns">
<h4 class="Mainline">PRIMEROS PASOS COMO AFILIADO</h4>
<p><span class="style24 navy"> MARCADO DE REFERENCIACIÒN </span></p>
<p>ClubShop Rewards te proporciona una manera nueva y diferente de ganar dinero como Afiliado y Partner que ninguna otra oportunidad de multi nivel u otros programas de ventas ofrecen. Aquì puedes ver porque:</p>
<table width="100%">
<tbody>
<tr>
<td width="14%"><strong>TIPO DE OPORTUNIDAD</strong></td>
<td width="39%">
<div align="center">OFERTAS DE OTROS</div>
</td>
<td width="47%">
<div align="center"><span class="bold royal"> OFFERTAS DEL CLUBSHOP REWARDS</span></div>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Ventas Directas</strong></td>
<td>
<ul>
<li class="black_bullet">Puede requerir comprar un paquete con los primeros pasos </li>
<li class="black_bullet">Puede requerir que tu hagas demonstraciones del producto comprar, hacer entragas de pedidos y recolectar el dinero.</li>
<li class="black_bullet">Es necesario que lo clientes compren solo los productos que la compañìa de la Venta Directa ofrece.</li>
<li class="black_bullet">La mayorìa de las oportunidades de ingresos de venta directa te dan la posibilidad de ganar dinero localmente.</li>
<li class="black_bullet">Una oportunidad limitada de ganar dinero, un mercado local limitado y un nùmero limitado de productos para vender</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">No se paga nada para empezar a ganar como Afiliado.</li>
<li class="blue_bullet">Regalas membresìas gratis. Las personas que has referido compran por su cuenta.<br /><br /> </li>
<li class="blue_bullet">Permites a tus miembros de comprar las cosas que normalmente compran con las màs grandes tiendas en lìnea y fuera de lìnea AHORRANDO Y GANANDO dinero y la posibilidad de ganar premios. </li>
<li class="blue_bullet">Puedes referir personas para que se conviertan en miembros locales  y Globales y puedan crear un ingreso "Glocal" </li>
<li class="blue_bullet">Una oportunidad ilimitada de ganar dinero, debido a un mercado internacional y cualquier tipo de producto y servicios desde donde tus miembros  pueden ahorrar dinero.</li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Mercado Multi-Nivel</strong></td>
<td>
<ul>
<li class="black_bullet">Puede requerir una cuota de suscripciòn para comenzar como Afiliado.</li>
<li class="black_bullet">Puede requerir que hagas ventas directas, ventas de servicios y ofertas de productos limitados.</li>
<li class="black_bullet">El Mercado (es màs caro del normal) dado que debes pagar comiciones a intermediarios o patrocinadores </li>
<li class="black_bullet">La oportunidad podrìa ser limitada en algunos Paìses.</li>
<li class="black_bullet">Si la compañìa tiene menos de 5 años, la probabilidad que cese sus actividades es del 90% </li>
<li class="black_bullet">Podrìa confundirse con un esquema ilegal de Piràmide.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">No pagas nada para empezar como Afiliado</li>
<li class="blue_bullet">No hay ventas directas, ilimitado nùmero de productos y servicios para los miembros.</li>
<li class="blue_bullet">Cash back, descuentos y/o premios por las compras de los mismos productos y servicios como un no-miembro. </li>
<li class="blue_bullet">Oportunidad Global Ilimitada. </li>
<li class="blue_bullet">El ClubShop Rewards trabaja en el mercado mundial desde el 1997 <br /><br /></li>
<li class="blue_bullet">El Better Business Bureau (oficina del mejor negocio) le ha creditado la valutaciòn A+</li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Venta del  Afiliado</strong></td>
<td>
<ul>
<li class="black_bullet">Tiene que invertir dinero para crear anuncios en Internet de publicidad <br /><br /></li>
<li class="black_bullet">Tiene que ser hàbil en crear anuncios.<br /><br /></li>
<li class="black_bullet">la formaciòn que se ofrece es pequeña y a veces no se proporciona.<br /><br /></li>
<li class="black_bullet">las comisiones se ganan solo por la  compras individuales.<br /><br /></li>
<li class="black_bullet">No se ofrece ninguna informaciòn a los clientes.<br /><br /><br /></li>
<li class="black_bullet">Ninguna capacidad de patrocinar y ganar comisiones de reemplazo de otros afiliados.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">No hay necesidad de invertir ningùn dinero en la creaciòn de anuncios, Publicidad Cooperativa disponible para los suscriptores GPS.</li>
<li class="blue_bullet">No hace falta experiencia. Publicidad Cooperativa disponible para los suscriptores GPS. </li>
<li class="blue_bullet">Los suscriptores GPS tienen acceso libre a cursos manageriales de màrketing y a seminarios.</li>
<li class="blue_bullet">Ganas comisiones residuales cada vez que un miembro referido realiza una compra vàlida.</li>
<li class="blue_bullet">Los GPS PRO o màs tienen acceso libre a diferentes Informes como la lista de miembros y seguimientos de registros. </li>
<li class="blue_bullet">Los Partners pueden referir afiliados, mercantes, organizaciones sin fin de lucro como tambièn otros partners.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<br />
<p>Como Afiliado te puedes tambièn calificar como Partner, cosa que aumenta tu habilidad de hacer dinero debido a los tantos privilegios que garantizamos a nuestros Partners.</p>
<p>Como Partner puedes tambièn referir:</p>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<ul>
<li class="blue_bullet">Mercantes que participen en nuestro programa (en lìnea y fuera de lìnea) para que ganen comisiones a traves de sus compras Y las potenciales compras que realizan los clientes ClubShop Rewards che han referido.</li>
<li class="blue_bullet">Organizaciones sin fin de lucro para ayudarlos a recaudar fondos y ganar comisiones de las tarjetas fidelidad ClubShop Rewards que quieran comprar para despuès vender y ganar comiciones a travez de la compras potenciales que realizan los miembros que hayan referido. </li>
<!--<li class="blue_bullet">Afiliados como tu y ganar comiciones de las compras realizadas por los miembros que ellos han referido. </li>-->
<li class="blue_bullet">Partners para construir una organizaciòn global de ventas de la cual tu puedes ganar comiciones de reemplazo y dividendos (ingresos que se dividen entre los Partners del ClubShop Rewards a nivel mundial) </li>
</ul>
<p>Para a prender màs sobre los privilegios que tendràs si te conviertes en un Partner mira la <a href="http://www.clubshop.com/present/bizop.xml" target="_blank"><span style="text-decoration: underline;">Presentaciòn de la Oportunidad de Negocio del Partner.</span></a></p>
</div>
</div>
</div>
</div>
<p><span class="style24 navy">COMO AHORRAN DINERO LOS MIEMBROS</span></p>
<p>Los miembros del ClubShop Rewards pueden ahorrar dinero comprando en lìnea en el centro comercial ClubShop Rewards o comprando fuera de lìnea con los mercantes que participan  a nuestro programa. Cuanto uno puede beneficiar del ser un miembro està basado en la cantidad de tiendas en lìnea que se encuentran en nuestro centro comercial y al nùmero de mercantes fuera de lìnea que se encuentran en la zona donde vives o donde vas a viajar.</p>
<ul>
<li class="blue_bullet"><span class="style30">COMPRAR EN LÌNEA</span> - los miembros pueden ganar dinero en efectivo (ClubCash) cuando compran en las tiendas del centro comercial, el ClubCash se acumula en la cuenta personal de cada miembro hasta que no se canjee. El ClubShop Rewards ha crecido tanto en estos años que se ha convertido en el centro comercial màs grande del mundo ¡presente en 50 paìses! <br /><br /> 
<a href="http://www.clubshop.com/mall/" target="_blank"><span style="text-decoration: underline;">HAZ CLICK AQUÌ</span></a> para entrar en el centro comercial ClubShop Rewards y ver si hay ya tiendas en tu paìs donde tu y tus miembros pueden comprar y ganar ClubCash.                                 
<ul>
<li class="half">Si hay un centro comercial en tu paìs, la cantidad de ClubCash que tu y los miembros que refieres pueden ganar por cada compra que realizan se visualiza al lado de cada tienda.<a href="#" class="togglelink" ></a>

<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<table>
<tbody>
<tr>
<td class="orange capitalize" width="153">
<div align="center">Tienda en Lìnea</div>
</td>
<td class="orange capitalize" width="202">
<div align="center">Recompensas</div>
</td>
<td class="orange capitalize" width="125">
<div align="center">Ofertas</div>
</td>
<td class="orange capitalize" width="1039">
<div align="center">Detalles Tienda</div>
</td>
</tr>
<tr>
<td colspan="4"></td>
</tr>
<tr>
<td><img alt="Disney Store" height="101" src="images/disney_store.gif" width="153" /></td>
<td>Cash Back: 1.2% <br />Pay Points: 0.6%</td>
<td></td>
<td><span class="smaller_text">DisneyStore.com es el primer supermercado en lìnea para los productos Disney, decoraciòn del hogar, ropa, juguetes màs vendidos, incluyendo una gran selecciòn de coleccionismo, bolsas de arroz mini (las famosas bean bags) y mucho màs. Encuentras tambièn todos tus personajes favoritos desde Mickey Mouse a Pooh etc </span></td>
</tr>
<tr>
<td><img border="0" height="101" src="images/netflix.gif" width="153" /></td>
<td><a href="images/netflix_desc.gif" onclick="window.open(this.href,'help', 'width=414,height=275,scrollbars'); return false;"><span style="text-decoration: underline;">Tarifa Variable<img alt="rate varies" height="15" src="http://www.clubshop.com/mall/icons/icon_info_15x15.png" width="15" /></span></a></td>
<td></td>
<td><span class="smaller_text">Puedes mirar al instante un nùmero ilimitado de episodios y pelìculas desde tu televisiòn, computadora y diferentes dispositivos mòviles. Mira todo lo querias y cuanto quieras por solo $7.99 al mes. ¡Pruèbalo gratis hoy mismo! </span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</li>
<li class="half">Si una tienda del centro comercial te ofrece un percentaje del 5% de cash back por tu compra y has gastado <span class="transposeME">$100.00</span>, vas a ganar un ClubCash de <span class="transposeME">$5.00</span></li>
<li class="half">Si tu paìs no se encuentra en el listado o quizàs cuenta solo con algunas tiendas disponibles por el momento, entonces tienes una tremenda oportunidad como Partner de ganar dinero refiriendo tiendas que podràn ser incorporadas en el listado del centro comercial en lìnea del ClubShop Rewards.</li>
<li class="half">Uno de los muchos privilegios que tienes como Partner es la habilidad de patrocinar tiendas en lìnea que pueden ser incorporadas en nuestro centro comercial. Los paìses con pocas o ninguna tienda proporcionan a los partners una gran oportunidad de referir tiendas en lìnea que ofrecen productos o servicios en tu paìs, cuando patrocinas una tienda en lìnea para ser listada en nuestro centro comercial ClubShop Rewards ganas 3 beneficios.  <a class="togglelink" href="#"></a></li>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel"><ol>
<li>Ganas comiciones de Personal por el honorario de listado que paga la tienda que has referido. </li>
<li>Ganas una Personal comiciòn por cada compra que un miembro realiza en la tienda en lìnea que has referido.</li>
<li>Màs tiendas listadas hay en tu paìs, mejores seràn las oportunidades para los miembros que has referido de comprar y ganar ClubCash y para ti de ganar Comisiones de Personal.</li>
</ol></div>
</div>
</div>
</div>
<p>Si te has convertido en un Partner mediante la suscripciòn del GPS Bàsico con solo &#8364;22,00 al mes, te proporcionamos la GUÌA DE ENTRENAMIENTO BÀSICA DE MERCADO, para aprender como referir tiendas en lìnea para que sean listadas en nuestro centro comercial.  <a href="http://www.clubshop.com/gps_new.html" target="_blank">HAZ CLICK AQUÌ</a> para aprender màs sobre los beneficios que proporciona una suscripciòn GPS.</p>
</ul>
</li>
<li class="blue_bullet"><span class="style30">COMPRAR FUERA DE LÌNEA </span> - Los miembros pueden ganar dinero en efectivo (ClubCash) o descuentos directos cuando presentan la tarjeta ClubShop Rewards en las tiendas que participan en nuestro programa de mercantes locales o fuera de lìnea. La tarjeta ClubShop Rewards se usa para ahorrar dinero, por al menos 1 año y son muy asequibles  permite a los miembros de no solo ahorrar dinero sino tambièn amortizar el cuesto de la tarjeta en solo algunas compras. La tarjeta de fidelidad ClubShop Rewards se puede usar globalmente para ahorrar dinero en el creciente nùmero de negocios y tiendas que entran a formar parte de nuestro programa cada dìa.
<p><a href="http://www.clubshop.com/cs/rewardsdirectory">HAZ CLICK AQUÌ</a> para ver si hay ya tiendas locales donde vives.</p>
<ul>
<li>Si hay alguna tienda local donde vives, tendrìas que comprarte una tarjeta para empezar a ahorrar dinero o quizàs comprar 10 o màs tarjetas descontadas que puedes vender y obtener asì una buena ganancia por cada tarjeta vendida. <a href="https://www.clubshop.com/cgi/cart/cart.cgi?DT=1&amp;SearchDept.x=24&amp;SearchDept.y=10" target="_blank">HAZ CLICK AQUÌ</a> para comprar una o màs tarjetas ClubShop Rewards.<br /><br />Deberìas tambièn considerar la opciòn de convertirte en un Partner, de esta manera puedes referir organizaciones locales sin fin de lucro (Grupos de Afinidad) para vender la tarjetas como una manera fantàstica de recaudar fondos y de los cuales puedes ganar comiciones por cada tarjeta que compra la organizaciòn sin fin de lucro que has referido, Suscribete al GPS para convertirte en un partner que te PERMITE de acceder a los cursos en lìnea sobre como referir organizaciones sin fin de lucro. <br /><br />La cantidad de ClubCash que ganas o el descuento que vas a recibir se muestra en el listado de cada mercante. Puedes tambièn tomar ventaja de la aplicaciòn del mapa de Google para ver donde se encuentran las tiendas. </li>
<li>Si hay pocas o ninguna tienda que participan a nuestro programa donde vives, entonces tienes una gran oportunidad como Partner de ganar dinero patrocinando tiendas fuera de lìnea y negocios pqrq que participen en nuestro programa ClubShop Rewards. 

<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Otros de los muchos beneficios que tienes como Partner es la habilidad de referir tiendas fuera de lìnea que acepten nuestra tarjeta ClubShop Rewards , en cambio le ofrecemos un cuesto efectivo de publicidad y un programa de lealtà al cliente. Si hay pocas o ninguna tienda local donde vives entonces tienes una oportunidad genuina de patrocinar "tiendas fuera de lìnea" para que participen en nuestro programa ClubShop Rewards. Como Partner cada vez que patrocinas una tienda fuera de lìnea para que participe al programa de comerciante ClubShop Rewards, ganas 3 beneficios.</p>
<ol>
<li>Ganas comiciones de Personal por cada paquete mercantil comprado por el mercante que has patrocinado.</li>
<li>Ganas comiciones de Personal de los ClubCash que cualquier miembro gana a traves de la compra realizada en la tienda o negocio fuera de lìnea </li>
<li>Màs tiendas patrocinas, mejores seràn las oportunidades de todos los miembros poseedores de la tarjeta que has referido de ahorrar dinero a su vez màs valor tiene la tarjeta, màs fàcil será para ti venderla.</li>
<li>Si te has convertido en un Partner mediante la suscripciòn del GPS Bàsico con solo &#8364;22,00 al mes, te proporcionamos la GUÌA  DE ENTRENAMIENTO BÀSICA MERCANTIL , para aprender como patrocinar tiendas fuera de lìnea que seràn listadas en el Centro Comercial.  <a href="http://www.clubshop.com/gps_new.html" target="_blank">HAZ CLICK AQUÌ</a> para aprender màs sobre los beneficios que una suscripciòn GPS te ofrece.</li>
</ol>
</div>
</div>
</div>
</div>
</li>
</ul>
</li>
</ul>
<p><span class="style24 navy">CLUB CASH</span></p>
<p>Como explicado arriba, los miembros del ClubShop Rewards pueden ganar rebajas o dinero en efectivo cuando realizan compras en lìnea en el Centro Comercial ClubShop Rewards o fuera de lìnea presentando la tarjeta fidelidad en la tiendas que participan en nuestro programa mercantil ClubShop Rewards. El dinero en efectivo es conocido como "Club Cash" y se acumula (hasta que no se canjee)  en la cuenta del Club de cada miembro, la suma mìnima para poder canjear un Club Cash es de <span class="transposeME">$25 se puede hacer en cualquier momento y en la moneda local.</span></p>
<p><a href="http://www.clubshop.com/members/clubcash_details.shtml" target="_blank">HAZ CLICK AQUÌ</a> para chequear tu cuenta del club</p>
<ul>
<li class="blue_bullet"><span class="style30">¡OBTIENE TU PRIMER CLUB CASH GRATIS!</span> Como miembro puedes empezar inmediatamente a ganar Club Cash llenando una simple encuesta que nos permite de conocerte mejor. <a href="http://www.clubshop.com/mall/other/questionnaire.xsp" target="_blank">HAZ CLICK AQUÌ</a> para aprender màs y llenar la encuesta, te recompensaremos con el primer depòsito de ClubCash en tu cuenta.</li>
<li class="blue_bullet"><span class="style30">¡CONVIÈRTETE EN TU PRIMER CLIENTE!</span> Cuando realizas la primera compra en nuestro Centro Comercial en lìnea, recibiràs tambièn un Bono de ClubCash que depositeremos en tu cuenta, este bono es adicional al ClubCash que ganas por la compra efectuada. Aquì abajo ves algunos ejemplos de tiendas donde puedes hacer tu primera compra para ganar el ClubCash y tambièn el Bono adicional. </li>
<li class="blue_bullet"><br /> 
<ul>
<li class="half"><span class="orange bold">SKYPE</span> - Te permite de hacer llamadas Gratis con otros usuarios de Skype y de contactar tu entrenador y Partners de tu organizaciòn. Puedes hacer un pequeño depòsito para ganar ClubCash y utilizar SKYPE para hacer llamadas y video llamadas internacionales a tarifas muy bajas. </li>
<li class="half"><span class="orange bold">VISTA PRINT</span> -                  Consigue 500 tarjetas de visita gratis (que puedes utilizar en tu negocio ClubChop Rewards) pagaràs solo el transporte que se realiza a nivel internacional.</li>
<li class="half"><span class="orange bold">SERVICIOS COMERCIALES, PRODUCTOS DE OFICINA COMPUTADORAS  Y SOFTWARE, ALOJAMIENTO WEB E ISPS</span> - Mira todas esas categorìas para saber cuales son las tiendas disponibles donde puedes comprar y ganar Club Cash.</li>
</ul>
<p><a href="http://www.clubshop.com/mall" target="_blank">HAZ CLICK AQUÌ</a> Para hacer tu primera compra, ganar dinero en efectivo y un bono Club Cash.</p>
            <p><span class="style30">¡CONVIÈRTETE EN TU MEJOR CONSUMIDOR! ademàs de ganar</span> 
              Club Cash en tus compras personales como Afiliado, puedes ganar 
              tambièn "Pay Points" por las mismas compras por las cuales ganas 
              Club Cash. Once you gain 10 Pay Points in a month, you will automatically qualify to be a Partner and have the privilege to refer Partners, Affinity Groups and Merchants, and be able to earn Partner Income.. Nota: 
              Una suscripciòn Bàsica GPS BASIC cuesta solo &#8364;22,00 al mes 
              y te proporciona un crèdito de 10 Pay Points que te califica como 
              Partner cada mes. <a href="http://www.clubshop.com/gps_new.html" target="_blank">HAZ 
              CLICK AQUÌ</a> para calificarte como Partner y aprender màs sobre 
              el GPS.</p>
</li>
</ul>
<p><span class="style24 navy">PAY POINTS Y COMMICIONES DE Personal</span></p>
<p><span class="style24 navy"> </span>Como explicado arriba, ganas "Pay Points" por compras elegibles que realizas tu y los miembros que has referido. Un Pay Point equivale a <span class="transposeME">$1.00. Los </span>Pay Points se acumulan cada mes y se utilizan para determinar:</p>
        <p>Tus comiciones de Personal que equivalen al 50% de los Pay Points que te acreditamos 
          por las compras que realizan los miembros que refieres. Nota: Como Partner 
          puedes ganar tambièn comiciones de Personal por tus compras personales 
          y las compras que realizan los Afiliados, comerciantes y organizaciones 
          sin fin de lucro que has referido.</p>
<ul>
          <li class="blue_bullet">La Calificaciòn como Partner requiere que tengas 
            10 Pay Points cada mes que puedes obtener a traves de tus compras 
            personales o las compras que realizan los miembros que haz referido. 
            Como dicho anteriormente el cuesto de una suscripciòn GPS BÀSICA es 
            de solo &#8364;22,00 al mes y te proporciona un crèdito de 10 Pay 
            Points que te califica automàticamente como Partner cada mes. <a href="http://www.clubshop.com/gps_new.html" target="_blank">HAZ 
            CLICK AQUÌ</a> para aprender màs sobre el GPS y calificarte como Partner.</li>
</ul>
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<div align="center"><span class="orange bold">EJEMPLO DE PAY POINT (puntos pagos)</span></div>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">Compra cartuchos de impresora por  <span class="transposeME">$20.00 en la tienda</span> 123 Ink Jets</span> (aparece en el listado del Centro Comercial U.S.A):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,6,mall_id=1"><img border="0" height="79" src="images/123inkjets.gif" width="258" /></a></td>
<td width="193">Cash Back: 11.2%<br /> Pay Points: 5.6%</td>
<td width="626"><span class="smaller_text">123 Inkjets te ofrece cartuchos de impresora de primera calidad de inyecciòn de tinta y de tòner làser, papel fotogràfico, y otros suministros para impresoras de inyecciòn de tinta a precios reducidos.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">Ganas </span><span class="transposeME royal bold">$2.24</span> de<span class="bold"> Club Cash y 1.12 de Pay Points</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">Un miembro que refieres compra por <span class="transposeME">$50.00</span>en la tienda Flower Delivery </span>(otra de las muchas tiendas listadas en el Centro Comercial en lìnea U.S.A):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,1611,mall_id=1"><img border="0" height="79" src="images/flowerdelivery.gif" width="258" /></a></td>
<td width="193">Cash Back: 8.0%<br />Pay Points: 4.0%</td>
<td width="626"><span class="smaller_text">Flower Delivery trabaja con los vendedores preferidos de cestas regalo y realiza una distribuciòn mundial de flores ofreciendo el màs alto nivel de servicio y satisfacciòn para el cliente.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">El miembro gana </span><span class="transposeME royal bold">$4.00</span> 
                        e<span class="bold">n Club Cash y tu ganas 2.00 en Pay 
                        Points, de los cuales ganaràs tambièn</span> <span class="transposeME royal bold">$1.00</span><span class="bold"> 
                        en Commisiones de Personal..</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">Un miembro que refieres compra en la tienda Buy.com un televisor de pantalla plana por <span class="transposeME">$500.00</span> </span>(otra de las tiendas listadas en el Centro Comercial en lìnea U.S.A):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,286,mall_id=1"><img border="0" height="79" src="images/buy.gif" width="258" /></a></td>
<td width="193">Cash Back: 2.0%<br />Pay Points: 1.0%</td>
<td width="626"><span class="smaller_text">Ofrecemos màs de 3 milliones de productos que incluyen estas categorìas: computadoras, productos electrònicos, càmaras digitales, GPS, libros, DVDs, CDs, artìculos deportivos, videos juegos, juguetes, bolsas, bebè, joyerìa, hogar y al aire libre y mucho màs.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="transposeME royal bold">El miembro gana $10.00</span> 
                        en<span class="bold"> Club Cash y tu ganas 5.00 en Pay 
                        Points, por los cuales vas a ganar tambièn </span> <span class="transposeME royal bold">$2.50</span><span class="bold"> 
                        en Comisiones de Personal..</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /></div>
</div>
</div>
<br /><br />
<div><span class="largehead_blue_bold">CALIFÌCATE COMO PARTNER</span></div>
<hr />
<div class="row">
<div class="twelve columns">
<p>Una vez que ganas 10 Pay Points en un mes, te calificas automàticamente como Partner y tendràs todos los privilegios de patrocinar Afiliados, Partners, Organizaciones sin fin de lucro y comerciantes, vas a tener derecho a ganar ingresos como Partner. Estas son las 2 maneras de como puedes hacer:</p>
<ol>
<li><span class="larger_blue_bolder">CALIFÌCATE MÀS TARDE</span>  - by making enough personal purchases and referring enough Members to make purchases, so that you earn 10 Pay Points a month from these purchases.</li>
<li><span class="larger_blue_bolder">CALIFÌCATE AHORA</span> - Comprando una suscripciòn GPS que te proporciona todos los beneficios incluso el ayudarte a construir un negocio exitoso como Partner del ClubShop Rewards. </li>
</ol></div>
</div>
<div class="row">
<div class="eight columns">
<div class="eight columns centered">Aùn asì sabemos que no es necesario suscribirse al GPS para convertirse en un Partner,  màs del 90% de los Partners suscriben el GPS para acceder a todos los beneficios que offrece.</div>
<div class="eight columns centered"></div>
<div class="eight columns centered"></div>
<img alt="gps" src="http://www.clubshop.com/manual/gps/gps-box-version5.png" />
<div class="eight columns centered"></div>
</div>
<div class="four columns">
<div class="four columns push-one"><br /><br /><br /><br /><br />
<p><a class="nice large blue button" href="https://www.glocalincome.com/cgi/appx.cgi/0/upg" target="_blank">SUBSCRÌBETE AL GPS »</a></p>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<div class="twelve columns push-three"><br /><br />
<p>Para aprender màs sobre como suscribirte al GPS</p>
<a class="nice medium blue button" href="http://www.clubshop.com/gps_new.html" target="_blank">HAZ CLICK AQUÌ »</a></div>
</div>
</div>
<hr />
<div align="right"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><a href="http://www.clubshop.com/affiliate/referring.xml" target="_blank">GUÌA DE REFERENCIACIÒN PARA MIEMBROS</a></div>
<br /><br /><br /></div>
</div>
</div>
</div>
<<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" eight columns centered">          
                <div id=" footer ">
                    <p>Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();

                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         ClubShop Rewards, All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->
</body>
</html>
