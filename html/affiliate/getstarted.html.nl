﻿<!DOCTYPE html>
 <html>   
         <head>
            <title>GETTING STARTED AS AN AFFILIATE</title>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />

    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->

<script type="text/javascript" src="/js/transpose.js"></script>
<script type="text/javascript" src="/js/sprintf.js"></script>
<script type="text/javascript" src="/js/Currency.js"></script>
<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script> 

<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>



<style>
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}

.toggleLink {
font-style: italic;
font-size: 10px;
}

.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFB404;
	font-weight:bold;
}

.largehead_blue_bold {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 18px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.larger_blue_bolder {
	font-family: Arial, Verdana, Helvetica, sans-serif;
	font-size: 16px;
	text-transform: uppercase;
	letter-spacing: 0.1em;
	color: #000099;
	font-weight: bold;
}
.capitalize {text-transform: uppercase;}
.smaller_text{font-size: 10px;
line-height: 15px;}
</style>

<script language="javascript">

   

	$(document).ready(function() {

	var showText='Learn More';

	var hideText='Hide';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

 

</script>

<script type="text/javascript">
    $(document).ready(function() {
        Transpose.transpose_currency();
    });
</script>
</head>

<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="http://www.clubshop.com/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns "><br />
<div align="left">
<div id="google_translate_element" style="margin-top:0.5em;"></div>
<div align="left">
<script type="text/javascript">// <![CDATA[
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
// ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</div>
<hr />
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="twelve columns">
<h4 class="Mainline">VAN START GAAN ALS EEN AFFILIATE</h4>
<hr />
<p><span class="style24 navy">REFERRAL MARKETING</span></p>
<p>ClubShop Rewards <span class="short_text" id="result_box" lang="nl"><span class="hps">biedt u</span> <span class="hps">een geheel</span> <span class="hps">nieuwe en andere</span> <span class="hps">manier om geld te verdienen als een</span></span> Affiliate en Partner, dan welke andere directe verkoop organisatie, multi-level marketing  bedrijf of affiliate verkoopprogramma. Hier is waarom:</p>
<table width="100%">
<tbody>
<tr>
<td width="14%"><strong>TYPE MOGELIJKHEID<br /></strong></td>
<td width="39%">
<div align="center"><span class="bold">WAT ZIJ BIEDEN</span></div>
</td>
<td width="47%">
<div align="center"><span class="bold royal">WAT CLUBSHOP REWARDS BIEDT</span></div>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Directe Verkoop</strong></td>
<td>
<ul>
<li class="black_bullet">Kan een verkoop-uitrustig verlangen om van start te kunnen gaan.</li>
<li class="black_bullet">Kan van u verlangen om product demonstraties te geven, bestellingen op te nemen en te plaatsen en bestellingen te leveren en geld te innen.</li>
<li class="black_bullet">Vereist van consumenten om alleen de producten aan te kopen die het Directe Verkoop bedrijf aanbiedt.<br /><br /></li>
<li class="black_bullet">De meeste directe verkoop mogelijkheden bieden u alleen een manier om locaal geld te verdienen.</li>
<li class="black_bullet">Een beperkte mogelijkheid om geld te verdienen, als gevolg van de beperkte locale markt en een beperkt aantal beschikbare producten om te verkopen.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Gratis om van start te gaan als een Affiliate.</li>
<li class="blue_bullet">U geeft gratis lidmaatschappen weg. Uw referrals winkelen zelf.<br /><br /> </li>
<li class="blue_bullet">U stelt uw referrals in staat om de dingen te kopen die zij anders ook kopen bij de populairste online winkels of bij lokale winkels/ ondernemers EN hierbij geld te besparen, geld terug te krijgen en/ of prijzen te winnen.</li>
<li class="blue_bullet">U kunt locaal en wereldwijd mensen verwijzen om members te worden om zo een "glocal" inkomen op te bouwen.</li>
<li class="blue_bullet">Een mogelijkheid om onbeperkt geld te verdienen, als gevolg van het hebben van een internationale markt en allerlei soorten producten en diensten waarop uw referrals geld kunnen besparen.</li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Multi-Level Marketing</strong></td>
<td>
<ul>
<li class="black_bullet">Kan inschrijfgeld vereisen om van start te gaan.</li>
<li class="black_bullet">Kan tevens van u eisen om directe verkoop te doen en kan een beperkt aantal producten of diensten aanbieden om te verkopen.</li>
<li class="black_bullet">Marked up (duurder dan normale winkelprijzen) producten, als gevolg van het moeten betalen van commissies aan meerdere people in een lijn van  sponsorshap.</li>
<li class="black_bullet">De mogelijkheid kan beperkt zijn tot bepaalde landen.</li>
<li class="black_bullet">Wanneer het bedrijf minder dan vijf jaar oud is, dan heeft het 90% kans om uit zaken te gaan.</li>
<li class="black_bullet">Kan worden verward met of kan een illegale pyramide zijn.</li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Gratis om van start te gaan als een Affiliate.</li>
<li class="blue_bullet">Geen directe verkoop en potentieel een onbeperkt aantal producten en diensten die door leden kunnen worden gekocht.</li>
<li class="blue_bullet">Geld terug, kortingen en/ of prijzen voor het aankopen van dezelfde producten en diensten als iemand die geen lid is.<br /><br /></li>
<li class="blue_bullet">Onbeperkte, wereldwijde mogelijkheid.</li>
<li class="blue_bullet">ClubShop Rewards doet al zaken sinds 1997.<br /><br /></li>
<li class="blue_bullet">A+ <span class="short_text" id="result_box" lang="nl"><span class="hps alt-edited">classificatie </span></span>bij het Better Business Bureau. </li>
</ul>
</td>
</tr>
<tr>
<td><br /><br /><br /><br /><br /><strong>Affiliate Verkopen</strong></td>
<td>
<ul>
<li class="black_bullet">Verplicht geld riskeren om internet advertenties te maken.<br /><br /></li>
<li class="black_bullet"><span class="short_text" id="result_box" lang="nl"><span class="hps">Moet</span> <span class="hps alt-edited">ervaren</span> <span class="hps">zijn</span> in <span class="hps">het creëren van</span> <span class="hps">advertenties.</span></span><br /><br /></li>
<li class="black_bullet">Weinig tot geen training aangeboden.<br /><br /></li>
<li class="black_bullet">Commissies worden alleen verdient op individuele aankopen.<br /><br /></li>
<li class="black_bullet">Geen referral consumenten informatie beschikbaar.<br /><br /><br /></li>
<li class="black_bullet">Geen mogelijkheid om te sponsoren en <span class="short_text" id="result_box" lang="nl"><span class="hps">override</span> <span class="hps">commissies </span></span>te verdienen op andere Affiliates.<br /><br /></li>
</ul>
</td>
<td>
<ul>
<li class="blue_bullet">Niet nodig om geld te riskeren om advertenties te maken. Mogelijkheid om gezamenlijk te adverteren aanwezig voor GPS abonnees.</li>
<li class="blue_bullet">Geen ervaring nodig. Gezamenlijk adverteren mogelijk voor GPS abonnees.</li>
<li class="blue_bullet">GPS abonnees hebben vrije toegang tot marketing en management cursussen en webinars.</li>
<li class="blue_bullet">Verdien residuele commissies, elke keer wanneer een door u verwezen Member een gekwalificeerde aankoop doet.</li>
<li class="blue_bullet">Member lijsten en rapporten beschikbaar. GPS PRO (en hogere) abonnees hebben tevens gratis toegang tot uitgebreide lijsten, rapporten en Follow Up Records (Opvolgsysteem).</li>
<li class="blue_bullet">Partners kunnen Affiliates, winkels/ bedrijven, non-profit organisaties en andere Partners sponsoren.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<br />
<p>Als een Affiliate, kunt u tevens kwalificeren als een Partner, welke uw mogelijkheid zal vergroten om geld te verdienen, las gevolg van de toename van privileges die Partners genieten.</p>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Als <span class="style24 royal">Partner</span>, kunt u tevens verwijzen:</p>
<ul>
<li class="blue_bullet">Winkels/ bedrijven die deelnemen aan ons programma (online en offline) en commissies verdienen op hun aankopen EN potentieel van de aankopen die bij hen worden gedaan door ALLE ClubShop Rewards Members.</li>
<li class="blue_bullet">Non-profit organisaties om hen te helpen fondsen te werven en commissies te persoonlijk op de ClubShop Rewards kaarten die zij kunnen aankopen en verkopen EN potentieel van de aankopen die worden gedaan door de ClubShop Rewards Members die zij eventueel verwijzen.</li>
<li class="blue_bullet">Affiliates zoals uzelf, waarbij u commissies kunt persoonlijk op de aankopen die gedaan worden door de Members die zij verwijzen.</li>
<li class="blue_bullet">Partners, om een wereldwijde verkoop organisatie uit te bouwen waarover u override commissies en persoonlijk (revenue shared by ClubShop Rewards met onze Partners).</li>
</ul>
<p>Om meer te weten te komen over de privileges en voordelen van het worden van een Partner, bekijkt u onze <a href="http://www.clubshop.com/present/bizop.xml" target="_blank"><span style="text-decoration: underline;">Partner Zakelijke Mogelijk Presentatie.</span></a></p>
</div>
</div>
</div>
</div>
<p><span class="style24 navy">HOE LEDEN GELD BESPAREN</span></p>
<p>ClubShop Rewards Members/ leden kunnen geld besparen wanneer zij online winkelen bij het ClubShop Rewards winkelcentrum en door zaken te doen met of offline aankopen te doen bij deelnemende ClubShop Rewards winkels en bedrijven. Hoeveel voordeel iemand kan genieten door een Member te zijn is grotendeels gebaseerd op het aantal online winkels in het ClubShop Rewards winkelcentrum in het land waar de member woont en/ of het aantal deelnemende offline winkels en bedrijven waar iemand woont of waar zij wellicht naartoe reizen.</p>
<ul>
<li class="blue_bullet"><span class="style30">ONLINE WINKELEN</span> - Members/ leden kunnen geld terug krijgen (ClubCash) wanneer zij online aankopen doen via het ClubShop Rewards winkelcentrum, deze ClubCash telt op in de persoonlijke ClubCash account van iedere Member/ lid  account totdat deze het bedrag laat uitbetalen. Het ClubShop Rewards winkelcentrum is uitgegroeit tot het grootste online winkelportaal ter wereld, met beschikbare winkels in meer dan 50 landen! 							<br /><br /> <a href="http://www.clubshop.com/mall/" target="_blank"><span style="text-decoration: underline;">KLIK HIER</span></a> om naar het ClubShop Rewards winkelcentrum te gaan en kijk of er een winkelcentrum in uw land is en bij hoeveel winkels u en de Members/ leden die u verwijst, kunnen winkelen en ClulubCash kunnen verdienen.  						 								                  
<ul>
<li class="half">Indien er een ClubShop Rewards winkelcentrum in uw land is, staat het bedrag aan ClubCash dat u en de Members/ leden die u verwijst kunnen verdienen, weergegeven achter elke winkel.
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<table>
<tbody>
<tr>
<td class="orange capitalize" width="153">
<div align="center">Online Winkel</div>
</td>
<td class="orange capitalize" width="202">
<div align="center">Rewards</div>
</td>
<td class="orange capitalize" width="125">
<div align="center">Deals</div>
</td>
<td class="orange capitalize" width="1039">
<div align="center">Details winkel</div>
</td>
</tr>
<tr>
<td colspan="4"></td>
</tr>
<tr>
<td><img alt="Disney Store" height="101" src="images/disney_store.gif" width="153" /></td>
<td>Geld terug: 1.2% <br />Pay punten: 0.6%</td>
<td></td>
<td><span class="smaller_text">DisneyStore.com is de belangrijkste online superwinkel voor Disney producten, inclusief een grote selectie van collectibles, home décor, kleding, speelgoed, top-selling Mini Bean Bags, en meer. Van Mickey tot Pooh, Princessen tot Buzz Lightyear, u zult er zeker uw favoriete figuren vinden.</span></td>
</tr>
<tr>
<td colspan="4"></td>
</tr>
<tr>
<td><img border="0" height="101" src="images/netflix.gif" width="153" /></td>
<td><a href="images/netflix_desc.gif" onclick="window.open(this.href,'help', 'width=414,height=275,scrollbars'); return false;"><span style="text-decoration: underline;">Percentage varieert <img alt="rate varies" height="15" src="http://www.clubshop.com/mall/icons/icon_info_15x15.png" width="15" /></span></a></td>
<td></td>
<td><span class="smaller_text">Belijk direct en onbeperkt TV series &amp; films via het internet, rechtstreeks op uw TV, computer en verschillende mobiele apparaten. Kijk zoveel u wilt, zo vaak u wilt voor slechts $7.99 per maand. Start vandaag nog uw proefperiode!</span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</li>
<li class="half">Wanneer een winkelcentrum winkel u 5% geld terug (cash back) biedt op uw aankoop en u doet een aankoop van <span class="transposeME">$100.00</span>, dan verdient u <span class="transposeME">$5.00</span> aan ClubCash!</li>
<li class="half">Wanneer uw land niet op de lijst staat of op dit moment maar een paar winkels beschikbaar heeftom te gaan winkelen, dan heeft u een enorme mogelijkheid om als Partner geld te verdienen door het verwijzen van online winkels naar opname op de lijst op het ClubShop Rewards winkelcentrum!! </li>
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>1 van de vele privileges die u hebt als <span class="style24 royal">Partner</span>, is de mogelijkheid om online winkels te verwijzen om opgenomen te worden in het ClubShop Rewards winkelcentrum. Landen met weinig tot geen winkels bieden Partners een enorme "ground floor" mogelijkheid om online winkels te verwijzen die producten of diensten bieden in uw land, om opgenomen te worden in het winkelcentrum. Als Partner, wanneer u een online winkel verwijst om te worden opgenomen in het ClubShop Rewards winkelcentrum, krijgt u 3 voordelen!</p>
<ol>
<li>U verdient persoonlijk Commissies op de listing bijdrage die door de online winkels die u verwijst wordt betaald.</li>
<!--<li>Elke keer dat een Member een aankoop doet bij een online winkel die u hebt verwezen, verdient u residueel persoonlijk Commissies.</li>-->
<li>Hoe meer winkels uw land op de lijst heeft, hoed beter de winkel mogelijkheden zijn voor de Members/ leden die u verwijst om ClunCash te verdienen en voor u om persoonlijk Commissies te verdienen.</li>
</ol></div>
</div>
</div>
</div>
<p>Indien u een Partner wordt door u te abonneren op GPS BASIS voor slechts €22,00 per maand, krijgt u een BASIS MARKETING TRAINING GIDS om te leren hoe u online winkels verwijst om opgenomen te worden in het ClubShop winkelcentrum. <a href="http://www.clubshop.com/gps_new.html" target="_blank">KLIK HIER</a> om meer te weten te komen over de voordelen die worden geboden bij een GPS abonnement.</p>
</ul>
</li>
<li class="blue_bullet"><span class="style30">OFFLINE WINKELEN</span> - Members geld terug verdienen (ClubCash) of een directe korting krijgen wanneer zij een ClubShop Rewards kaart laten zien aan een deelnemende offline winkelier. ClubShop Rewards kaarten kunnen worden gebruikt om geld te besparen gedurende ten minste een jaar en zijn erg voordelig, wat Members is staat stelt om ten minste net zoveel geld te besparen als de kaart kost, met slechts een paar aankopen! De ClubShop Rewards kaart kan wereldwijd worden gebruikt om geld mee te besparen bij een groeiend aantal bedrijven en winkels.
<p><a href="http://www.clubshop.com/cs/rewardsdirectory">KLIK HIER</a> om te zien of er deelnemende winkels en/ of bedrijven zijn dichtbij waar u woont.</p>
<ul>
<li>Wanneer u deelnemende winkels en bedrijven in uw omgeving hebt, dan zou u een kaart voor uzelf moeten kopen of u zou er met korting 10 of meer moeten kopen, zodat u zelf een kaart heeft om mee van start te gaan met het besparen van geld en zodat u kaarten kunt verkopen en een mooie winst kunt verdienen op elke kaart die u verkoopt. <a href="https://www.clubshop.com/cgi/cart/cart.cgi?DT=1&amp;SearchDept.x=24&amp;SearchDept.y=10" target="_blank">KLIK HIER</a> om 1 of meerdere ClubShop Rewards kaarten te kopen.  										<br /><br /> U zou tevens moeten overwegen om Partner te worden, dan kunt u lokale non-profit organisaties verwijzen om kaarten te verkopen als een fantastische manier om fondsen te werven, hierover kunt u dan commissies verdienen op elke kaart die door de non-profit organisatie(s) gekocht wordt wanneer u deze hebt verwezen! Abonneer op GPS om Partner te worden EN krijg toegang tot de online training cursussen om te leren hoe u non-profit organisaties verwijst. 								    	<br /><br /> De hoogte van het percentage ClubCash dat u kunt verdienen of de korting die u krijgt, wordt weergegeven op elke lijst met winkels en bedrijven. U kunt tevens gebruik maken van ons Google Maps app om lokale deelnemende winkels en bedrijven te vinden! </li>
<li>Indien u geen of weinig deelnemende winkels/ bedrijven in uw omgeving hebt, dan heeft u een geweldige mogelijkheid om als Partner geld te verdienen met het verwijzen van offline winkels en bedrijven om hen deel te laten nemen aan ons ClubShop Rewards Winkel Programma.
<div class="toggle">
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<p>Een andere van de vele voordelen die u hebt als Partner, is de mogelijkheid om offline winkels te verwijzen om de ClubShop Rewards kaart te accepteren in ruil voor het bieden van een kostenbesparende adverteer- en trouwe klanten programma. Wanneer u weinig tot geen aangesloten winkels in uw omgeving hebt, dan hebt u een geweldige "ground floor" mogelijkheid om offline winkels te verwijzen om deel te nemen in ons ClubShop Rewards Winkel Programma. Als Partner, wanneer u offline winkels en bedrijven verwijst om mee te doen in onze ClubShop Rewards Winkel Programma, krijgt u 3 voordelen!</p>
<ol>
<li>U verdient persoonlijk Commissies op elk marketing pakket dat door de winkels wordt gekocht die u hebt verwezen.</li>
<li>U kunt residuele persoonlijk Commissies verdienens, elke keer dat een Member/ lid winkelt in een offline winkel of bedrijf en hiermee ClubCash verdienen.</li>
<li>Hoe meer winkels/ bedrijven u lokaal inschrijft, hoe beter de bespaarmogelijkheden zijn voor de kaarthouder Members/leden die u verwijst. Hoe meer waarde de kaart heeft, hoe gemakkelijker het is voor u om kaarten te verkopen!</li>
</ol>
                        <p>Wanneer u een Partner wordt door te abonneren op GPS 
                          BASIS voor slechts €22,00 per maand, dan krijgt u een 
                          BASIS MARKETING TRAINING GIDS om te leren hoe u offline 
                          winkels en bedrijven verwijst om deel te nemen. <a href="http://www.clubshop.com/gps_new.html" target="_blank">KLIK 
                          HIER</a> om meer te lezen over de voordelen die worden 
                          geboden bij een GPS abonnement.</p>
</div>
</div>
</div>
</div>
</li>
</ul>
</li>
</ul>
<p><span class="style24 navy">CLUBCASH</span></p>
<p>Zoals hierboven uitgelegd, kunnen ClubShop Reward Members/ leden geld terug krijgen wanneer zij online aankopen doen via het ClubShop Rewards winkelcentrum of wanneer zij offline een ClubShop Rewards kaart laten zien aan een deelnemende winkelier die de verkoop registreert. Deze manier van geld terug krijgen noemen wij "Club Cash" voor Members/ leden, welke wordt opgetelt in de ClubCash account van iedere Member totdat deze tenminste <span class="transposeME">$25.00</span> bedraagt, vanaf dat moment kan de member/ het lid de cash back laten uitbetalen in de eigen valuta.</p>
<p><a href="http://www.clubshop.com/members/clubcash_details.shtml" target="_blank">KLIK HIER</a> om te zien hoe uw ClubCash account er uit ziet.</p>
<ul>
<li class="blue_bullet"><span class="style30">ONTVANG GRATIS UW EERSTE CLUBCASH!</span> Als Member/lid kunt u onmiddelijk uw eerste Club Cash verdienen door het invullen van een korte 'klanten interesse' vragenlijgenlijst. <a href="http://www.clubshop.com/mall/other/questionnaire.xsp" target="_blank">KLIK HIER</a> om meer te lezen en om de vragenlijst in te vullen, zodat wij u kunnen belonen met een eerste strorting op uw ClubCash account!</li>
<li class="blue_bullet"><span class="style30">WORD UW EERSTE KLANT!</span> Wanneer u uw eerste online aankoop doet, via het ClubShop Rewards winkelcentrum, verdient u ook een Club Cash Bonus storting op uw account, bovenop de Club Cash die u verdient op uw aankoop. Hier <span class="short_text" id="result_box" lang="nl"><span class="hps">enkele ideeën van de eerste aankopen die u kunt doen om een</span></span> Club Cash bonus en credits te verdienen: 									<br /><br /> 
<ul>
<li class="half"><span class="orange bold">SKYPE</span> - Geeft u de mogelijkheid om gratis te bellen met andere SKYPE gebruikers en met uw Partner Sponsor en Coach. Doe een kleine storting om Club Cash te verdienen en gebruik SKYPE om internationale telefoongesprekken en video gesprekken te voeren tegen zeer lage tarieven.</li>
<li class="half"><span class="orange bold">VISTA PRINT</span> - Ontvang 500 gratis gedrukte visitekaartjes (om te gebruiken voor uw nieuwe ClubShop                  Rewards zaak) en betaal alleen verzendkosten, met internationale verzending.</li>
<li class="half"><span class="orange bold">ZAKELIJKE DIENSTEN, KANTOORBENODIGDHEDEN, COMPUTERS &amp; SOFTWARE, WEBHOSTING &amp; ISPS</span> - Bekijk deze <span class="short_text" id="result_box" lang="nl"><span class="hps">categorieën om te zien welke winkels voor u beschikbaar zijn en verdien</span></span> ClubCash.</li>
</ul>
<p><a href="http://www.clubshop.com/mall" target="_blank">KLIK HIER</a> om uw eerste aankoop te doen en verdien ClubCash en een ClubCash bonus</p>
</li>
          <li class="blue_bullet"><span class="style30">WORD UW EIGEN BESTE KLANT!</span> 
            In aanvulling op het verdienen van Club Cash op uw persoonlijke aankopen, 
            kunt u als Affiliate tevens "Pay punten" verdienen op dezelfde aankopen 
            waarop u ClubCash verdient. Zodra u 10 Pay punten verdient in een 
            maand, kwalificeert u als een Partner en krijgt u Partner sponsoring 
            privileges en verdient u inkomen op basis basis het Partner Compensatieplan. 
            Opmerking: EEN GPS BASIS abonnement kost slechts €22,00 per maand 
            en biedt u een credit van 10 Pay punten om automatisch elke maand 
            te kwalificeren als Partner. <a href="http://www.clubshop.com/gps_new.html" target="_blank">KLIK 
            HIER</a> om meer te lezen over GPS en kwalificeren als een Partner.</li>
</ul>
<p><span class="style24 navy">PAY PUNTEN & PERSOONLIJK COMMISSIES</span></p>
<p>Zoals hierboven werd uitgelegd, verdient u "Pay punten" op gekwalificeerde aankopen die u doet en op gekwalificeerde aankopen die worden gedaan door de members/ ledeen die u verwijst. 1 Pay punt staat gelijk aan <span class="transposeME">$1.00</span> in waarde. Pay punten worden elke maand opgetelt en worden gebruikt om het volgende vast te stellen:</p>
<ul>
<li class="blue_bullet">Uw persoonlijk Commissies, welke gelijk staan aan 50% van de Pay punten die u hebt gekregen voor de aankopen die gedaan zijn door de Members die u verwijst. Opmerking: Als Partner verdient u tevens persoonlijk Commissies op uw persoonlijke aankopen en op de aankopen die gedaan zijn door de Affiliates, winkels en bedrijven en de non-profit organisaties die u hebt verwezen. </li>
          <li class="blue_bullet">Once you gain 10 Pay Points in a month, you will automatically qualify to be a Partner and have the privilege to refer Partners, Affinity Groups and Merchants, and be able to earn Partner Income. Zoals 
            eerder aangegeven, kost een GPS BASIS abonnement slechts €22,00 per 
            maand en biedt het u een credit van 10 Pay punten om automatisch elke 
            maand te kwalificeren als Partner. <a href="http://www.clubshop.com/gps_new.html" target="_blank">KLIK 
            HIER</a> om meer te lezen over GPS en kwalificeren als Partner.</li>
</ul>
<div class="row">
<div class="eleven columns offset-by-one">
<div class="panel">
<div align="center"><span class="orange bold">PAY PUNTEN VOORBEELDEN</span></div>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">U doet een aankoop van <span class="transposeME">$20.00</span> voor printer cartridges bij 123 Ink Jets</span> (een winkel op de U.S. lijst van het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,6,mall_id=1"><img border="0" height="79" src="images/123inkjets.gif" width="258" /></a></td>
<td width="193">Geld terug: 11.2%<br /> Pay punten: 5.6%</td>
<td width="626"><span class="smaller_text">123 Inkjets is toegewijd om u premium inkjet printer cartridges &amp; laser toner, fotopapier en andere inkjet printer benodigdheden te leveren tegen gereduceerde prijzen.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">U verdient </span><span class="transposeME royal bold">$2.24</span> <span class="bold">aan ClubCash en 1.12 aan Pay punten</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">Een Member dat u hebt verwezen doet een aankoop van <span class="transposeME">$50.00</span> voor een boeket bloemen bij Flower Delivery </span>(een andere winkel op de U.S. lijst in het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,1611,mall_id=1"><img border="0" height="79" src="images/flowerdelivery.gif" width="258" /></a></td>
<td width="193">Geld terug: 8.0%<br />Pay punten: 4.0%</td>
<td width="626"><span class="smaller_text">Flower Delivery werkt wereldwijd, </span><span class="short_text" id="result_box" lang="nl"><span class="hps">bij voorkeur met</span> <span class="hps">geschenkmand</span> <span class="hps">verkopers</span></span><span class="smaller_text"> en bloemen leveranciers, om het hoogste niveau van dienstverlening en klanttevredenheid te bieden.</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">De member verdient</span> <span class="transposeME royal bold">$4.00</span> 
                        <span class="bold">aan ClubCash en u verdient 2.00 aan 
                        Pay punten, waarover u tevens </span><span class="transposeME royal bold">$1.00</span><span class="bold"> 
                        aan persoonlijk Commissies verdient.</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /> 
<ul>
<li class="blue_bullet"><span class="capitalize">Een Member die u hebt verwezen doet een aankoop van <span class="transposeME">$500.00</span> voor een flatscreen TV bij Buy.com </span>(een andere winkel op de U.S. lijst in het ClubShop winkelcentrum):</li>
</ul>
<table>
<tbody>
<tr>
<td width="301"><a href="http://www.clubshop.com/cgi-bin/rd/14,286,mall_id=1"><img border="0" height="79" src="images/buy.gif" width="258" /></a></td>
<td width="193">Geld terug: 2.0%<br />Pay punten: 1.0%</td>
<td width="626"><span class="smaller_text">Wij bieden meer dan 3 miljoen producten in </span><span class="short_text" id="result_box" lang="nl"><span class="hps">categorieën, waaronder</span></span><span class="smaller_text">: computers, electronica, digitale camera´s, GPS, boeken, DVDs, CDs, sportartikelen, video games, speelgoed, tassen, baby, sieraden, huis &amp; outdoor en meer!</span></td>
</tr>
<tr>
<td colspan="3">
<div align="center"><span class="bold">De member verdient</span> <span class="transposeME royal bold">$10.00</span> 
                        <span class="bold">aan Club Cash en u verdient 5.00 aan 
                        Pay punten, waarover u tevens </span><span class="transposeME royal bold">$2.50</span><span class="bold"> 
                        aan persoonlijk Commissies verdient.</span></div>
</td>
</tr>
</tbody>
</table>
<br /><br /></div>
</div>
</div>
<br /><br />
<div><span class="largehead_blue_bold">KWALIFICEER ALS PARTNER</span></div>
<hr />
<div class="row">
<div class="twelve columns">
<p>Zodra u in een maand 12 Pay punten hebt behaald, kwalificeert u automatisch als Partner en hebt u het privilege om Affiliates, Partners, Non/profit organisaties en Winkels en bedrijven te verwijzen en u kunt dan Partner Inkosten verdienen. Er zijn twee manieren waarop u dit kunt doen:</p>
<ol>
<li><span class="larger_blue_bolder">KWALIFICEER LATER</span> - by making enough personal purchases and referring enough Members to make purchases, so that you earn 10 Pay Points a month from these purchases.</li>
<li><span class="larger_blue_bolder">KWALIFICEER NU</span> - door het aankopen van een GPS abonnement en alle voordelen te ontvangen, inclusief de hulp om een succesvolle Partner ClubShop Rewards zaak op te bouwen. </li>
</ol>
<p>Ondanks dat het abobboneren op GPS niet vereist is om te kwalificeren als Partner, abonneert          meer dan 90% van onze Partners zich op GPS om toegang te hebben tot de vele voordelen die het hen biedt.</p>
</div>
</div>
<br /><br /><br />
<div class="row">
<div class="eight columns">
<div class="eight columns centered"><br /> <img alt="gps" src="http://www.clubshop.com/manual/gps/gps-box-version5.png" /></div>
</div>
<div class="four columns">
<div class="four columns push-one"><br /><br /><br /><br /><br />
<p><a class="nice large blue button" href="https://www.glocalincome.com/cgi/appx.cgi/0/upg" target="_blank">ABONNEER OP GPS »</a></p>
</div>
</div>
</div>
<div class="row">
<div class="twelve columns">
<div class="twelve columns push-three"><br /><br />
<p>Om meer te lezen over abonneren op GPS</p>
<a class="nice medium blue button" href="http://www.clubshop.com/gps_new.html" target="_blank">KLIK HIER »</a></div>
</div>
</div>
<hr />
<div align="right"><img src="https://www.clubshop.com/images/icons/icon_bullet_orannge.png" /><a href="http://www.clubshop.com/affiliate/referring.xml" target="_blank">MEMBERS VERWIJZEN GIDS</a></div>
<br /><br /><br /></div>
</div>
</div>
</div>
<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" eight columns centered">          
                <div id=" footer ">
                    <p>Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();

                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         ClubShop Rewards, All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->
</body>
</html>
