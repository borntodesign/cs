﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 

   <xsl:template match="/">
    
         <html><head>
         <title>View From the Top - DHS Club President Dick Burke's Monthly Newsletter</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
.brown {
font-family: Arial, Verdana,sans-serif;
font-size: 18px;
color: #960;
font-weight: bold;
}

</style>


</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="//www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">




            <div id="google_translate_element"></div><script><xsl:comment>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en'
  }, 'google_translate_element');
}
//</xsl:comment>
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<!--<p><xsl:value-of select="//lang_blocks/google-disclaimer"/><br/><br/>
<xsl:value-of select="//lang_blocks/google-instruction"/></p> -->
<hr/>
      <p><img src="DickB_Pic.gif" alt="DHS Club President and CEO, Dick Burke" width="82" height="115"/> </p>


<p class="style28"><xsl:value-of select="//lang_blocks/p1"/></p>
<br/>

<p class="style28">Monday, May 7, 2018</p>

<p>I am excited to announce that ClubShop Rewards now has new owners - Proprofit Worldwide, Ltd.! Proprofit is owned by Fabrizio "Fab" Perotti
and Giuseppe "Pino" Francavilla (AKA "Mr. G" - a nickname my son Will gave to him, when he couldn't pronounce his full name :)). They are both former Executive Directors whom are very knowledgeable
about the ClubShop Rewards program and experienced and successful network marketers. Fabrizio is also fluent in multiple languages, including Italian, French, English and Spanish, so these capabilities
will really help him to do webinars, and train/coach almost all Partners!  </p>

<p>As most active Partners know, I have been inactive as a coach and Executive Director for the past few years. This has led to a steady decline in sales during this time. Frankly, after 20 years of
personally and closely working with Partners and getting closer and closer to reaching my retirement years, I had lost my motivation to continue to do webinars, training sessions and even
recruiting and sponsoring. Don't get me wrong, I loved what I did in the past, but the time was quickly approaching to pass the baton to another runner, so that I could retire.
And my wife Pat has wanted me to do this for some time now, but it has been difficult to give up my baby and my passion.</p>

<p>Earlier this year I started considering how I could retire and still have the business continue on for Partners and for our many members. Selling the business to anyone not familiar with it was
not a desirable option, although I could benefit more financially by having more prospects to negotiate with. Just when I was preparing to go with one of my options, I received a friendly email
from my old friends and former Executive Directors Fab Perotti and Pino Francavilla, just inquiring as to how Pat and I were doing and thanking me for the opportunity I had
provided them and all the good memories we shared. Fab had come to HQ and Palm Island on his paid Exec trip and traveled to Belgium twice (once with Pino) to attend a Conference, and Fab and Pino
even traveled to the US together to attend a convention. We had also paid for Pino to have an expense paid trip with his family to Disney Paris. Add to these experiences, many conferences I did in
Italy where they were hosts, which included some wonderful dinners together and another expense paid stay together at a beautiful villa in Tuscany (except Pino did pay for the pizzas for everyone
the first night :)).</p>

<p>So we have a nice history of working together! Although we hadn't been in touch with each other the past few years, it was good to hear from them. I shared with them that I was considering selling
the business, so if they knew of anyone in the industry that may be interested, to let me know. Well that led to their inquiry as to if and how they could buy ClubShop Rewards. This thought got me
very encouraged, as they would be most excellent candidates to take over the reigns and run the company. Add to all of this, the many times Fab and I met to discuss and strategize the offline
marketing program, co-op and other matters. So we negotiated an agreement and they are now the new owners of ClubShop Rewards and clubshop.com!</p>

<p>Now we are in a transition period, so that we can train them and their new staff, how everything works - database and interface, accounting and finance, co-op, support, etc. So it may take a few
weeks before my name and the DHS Club are removed from webpages and emails. :)  We hope and plan that the transition will be completed by the end of May, at which time I will end my involvement
with the business that I started 22 years ago from the apartment over my garage to officially retire. </p>
<p>I am very grateful for the many friendships we have developed over the years with staff and Partners and hope to continue these friendships in my retirement years. I am also grateful to the many
loyal and dedicated Partners that are still with the company and pursuing their hopes and dreams, just as I did many years ago.</p>

<p>I also know that the future is now brighter for Partners and future Partners! I know Fab and Pino have many exciting plans to develop and improve ClubShop Rewards, which I will let them share with
you in the weeks ahead. So get ready to see a new burst of energy and enthusiasm from your new leaders, which will help and motivate everyone! Although I am excited to be able to retire, I think
I may be more excited for Fab, Pino and our active Partners for what is going to happen in all of your lives!</p>
<p> So thanks for the memories and may God bless all of you!</p>

<p>Dick Burke </p>

<p>P.S. - Send me a friend request on Facebook, if you would like us to keep in touch with each other. :)</p>

<hr/>


<br/><br/><br/><br/>
</div>
</div>
<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2018
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
