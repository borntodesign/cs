<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--

[% USE myodp = Local::odp %]

-->

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->

<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->

<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->

<head>

<meta charset="utf-8" />

<meta http-equiv="content-type" content="text/html" charset="UTF-8">



<!-- Set the viewport width to device width for mobile -->

<meta name="viewport" content="width=device-width" />

<title>PARTNER FAQ</title>

<!-- Included CSS Files -->

<link rel="stylesheet" type="text/css" href="../css/partner/foundation.css">

<link rel="stylesheet" type="text/css" href="../css/partner/app.css">

<link rel="stylesheet" type="text/css" href="../css/partner/general.css">

<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/manual_2012.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

<!--[if lt IE 9]>

		<link rel="stylesheet" href="stylesheets/ie.css">

	<![endif]-->

<!-- IE Fix for HTML5 Tags -->

<!--[if lt IE 9]>

		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

	<![endif]-->

<!-- Included JS Files -->

<script src="../js/partner/foundation.js"></script>

<script src="../js/partner/app.js"></script>

<script src="../js/partner/flash.js"></script>



<script type="text/javascript" src="/js/transpose.js"></script>

<script type="text/javascript" src="/js/Currency.js"></script>

<script type="text/javascript" src="/cgi/transposeMEvarsJS.cgi"></script>



<script language="javascript">

   

	$(document).ready(function() {

	var showText='Learn More';

	var hideText='Hide';

	var is_visible = false; 



	$('.toggle').prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

	$('.toggle').hide();

	$('a.toggleLink').click(function() { 



	is_visible = !is_visible;

	$(this).html( (!is_visible) ? showText : hideText);

	 

	// toggle the display - uncomment the next line for a basic "accordion" style

	//$('.toggle').hide();$('a.toggleLink').html(showText);

	$(this).parent().next('.toggle').toggle('slow');

	 

	// return false so any link destination is not followed

	return false;

	 

	});

	});

 

</script>



<script type="text/javascript">

            var windowSizeArray = [ "width=450,height=300" ];

 

            $(document).ready(function(){

                $('.newWindow').click(function (event){

 

                    var url = $(this).attr("href");

                    var windowName = "popUp";//$(this).attr("name");

                    var windowSize = windowSizeArray[$(this).attr("rel")];

 

                    window.open(url, windowName, windowSize);

 

                    event.preventDefault();

 

                });

            });

        </script>



<style type="text/css">

.darkblue {color: #011A52;}

.medblue {color:#004A95;}

.bold{font-weight:bold;}

.smaller{font-size: 13px;}

.blue_bullet{list-style: url(http://www.clubshop.com/images/partner/bullets/bluedot.png);}

.half{list-style: url(http://www.clubshop.com/images/partner/bullets/halfcir.png);}

.topTitle{text-align:center; color: #5080B0; }



.liteGrey{color:#b3b3b3;}

.centerbold {

	text-align: center;

	font-weight: bold;}



</style>

</head>

<body>

<!-- Top Blue Header Container -->

<div class="container blue">

<div class="row">

<div class="six columns"><a href="#"><img height="84px" src="../images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>

<div class="six columns"><!--Extra Space Future Space --></div>

</div>

</div>

<!--End Top Blue Header Container --> <!--Orbit Slider Container -->

<div class="container">

<div class="row">

<div class="twelve columns "><br />

<h4 class="topTitle">CLUBSHOP REWARDS PARTNER INKOMSTEN MOGELIJKHEID</h4>

<br /> 

<hr />

<h5 class="liteGrey">Veel gestelde vragen</h5>

</div>

</div>

</div>

<!--End Orbit Slider Container --> <!--Content Container -->

<div class="container ">

<div class="row">

<div class="nine columns"><!--Main Content -->

<div class="panel">

<h5 class="darkblue">V - Hoe werkt dit?</h5>

<p>A - Het is heel eenvoudig! Start gewoon met mensen te verwijzen om gratis ClubShop Rewards member te worden. Zij besparen en u verdient!</p>

<div class="toggle" style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<h6 class="medblue bold">V - Hoe verdien ik geld wanneer ik mensen verwijs om gratis lid te worden?</h6>

<p class="smaller">A - Zodra de mensen die u verwezen hebt gekwalificeerde aankopen gaan doen, verdient u commissies over hun aankopen.</p>

<h6 class="medblue bold">V - Wat voor soort marketing plan is dit?</h6>

<p class="smaller">A - Dit is een innovatief, nieuw soort "Referral Marketing" plan dat ontwikkelt is door ClubShop Rewards en dat onze leden betaald om andere mensen te verwijzen om ClubShop Rewards Members te worden. Ons plan stelt individuen in staat om samen te werken om persoonlijke ondernemingen op te bouwen en een wereldwijde consumenten eenheid, Door gebruik te maken van de kracht van het internet, sociale netwerken en mobiele communicatie technologie.</p>

</div>

</div>

</div>

</div>

<div class="panel">

<h5 class="darkblue">V - Wat voor soort aankopen doen ClubShop Rewards Members, waarover ik commissies kan verdienen?</h5>

<p>A - U kunt mensen helpen besparen op de meeste producten en diensten die zij dagelijk gebruiken. Wanneer zij aankopen doen die hiervoor in aanmerking komen, verdient u commissies.</p>

<div class="toggle" style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<ul>

<li class="blue_bullet">Ons online ClubShop Rewards Winkelcentrum heeft honderden van de meest populaire online winkels, in meer dan 50 landen, waar u en de leden die u verwijst "geld terug" kunnen verdienen van de aankopen die gedaan zijn.</li>

<li class="blue_bullet">Wij bieden tevens een ClubShop Rewards kortingkaart aan, die leden kunnen kopen om daarmee directe kortingen te krijgen bij deelnemende winkels/ ondernemers in hun directe omgeving of wanneer zij op reis zijn naar andere gebieden in de wereld. Als Partner, koopt u kaarten voor een lage, groothandelprijs en verkoopt u deze voor een hoger bedrag om persoonlijk nieuwe leden te werven.</li>

<li class="blue_bullet">Sommige van de Members die u hebt verwezen, zullen ook Affiliates of Partners willen worden om geld te verdienen en te besparen. Als de Partner die hen verwezen heeft, kunt u commissies verdienen over hun aankopen en abonnementen.</li>

<li class="blue_bullet">Non-profit organisaties, Winkels/ Ondernemers, Affiliates en Partners die u heeft verwezen, kunnen ook grote hoeveelheden ClubShop Rewards kortingkaarten aankopen voor wederverkoop. Als de Partner die hen heeft verwezen, kunt u ook commissies verdienen op deze verkopen.</li>

</ul>

</div>

</div>

</div>

</div>

<div class="panel">

<h5 class="darkblue">V - Hoe snel kan ik beginnen met verdienen en hoeveel geld kan ik verdienen als Partner?</h5>

<p>A - Uw recht om commissies te verdienen kan beginnen zodra u Partner bent geworden en u begint met het verwijzen van mensen om Members te worden. Er is geen limiet aan wat u kunt verdienen - dat is volledig aan u! U kunt elke maand wat extra zakgeld verdienen, of een uitstekend part-time inkomen creÃ«ren. Sommige van onze Partners hebben een full time inkomen kunnen opbouwen en zij genieten nu van de voordelen van het hebben van tijd en financiÃ«le vrijheid.</p>

<!--<a class="togglelink" href="#" mce_href="#"></a>

<div class="toggle" style="display:none;" mce_style="display:none;"><iframe frameborder="0" height="370px" scrolling="no" src="http://www.glocalincome.com/flash_slideshow/partner_com/partner_commission.html" mce_src="http://www.glocalincome.com/flash_slideshow/partner_com/partner_commission.html" width="100%"></iframe>

<p><a class="newWindow small blue button" href="https://www.clubshop.com/cgi/converter.cgi" mce_href="https://www.clubshop.com/cgi/converter.cgi" rel="0">KLIK HIER </a> om de inkomsten te converteren naar een andere valuta.</p>

<p class="darkblue bold">Hier zijn de WERKELIJKE gemiddelde maandelijkse inkomsten van onze Partners van de afgelopen maand, welke gebaseerd is op hun behaalde Pay Level</p>

<table style="width:100%;">

<tr>

<td class="darkblue"><b>PAY TITEL</b></td>

<td class="darkblue centerbold"><b>PARTNERS VERWEZEN</b></td>

<td class="darkblue centerbold"><b>GEMIDDELD INKOMEN</b></td>

</tr>

<tr class="alt">

<td class="bold">Team Player</td>

<td class="centerbold">1</td>

<td class="centerbold transposeME">$18.00</td>

</tr>

<tr>

<td><b>Team Leader</b></td>

<td class="centerbold">2</td>

<td class="centerbold transposeME" >$35.00</td>

</tr>

<tr class="alt">

<td class="bold">Team Captain</td>

<td class="centerbold">3</td>

<td class="centerbold transposeME">$65.00</td>

</tr>

<tr>

<td><b>Bronze Manager</b></td>

<td class="centerbold">5</td>

<td class="centerbold transposeME">$130.00</td>

</tr>

<tr class="alt">

<td class="bold">Silver Manager</td>

<td class="centerbold">7</td>

<td class="centerbold transposeME">$200.00</td>

</tr>

<tr>

<td><b>Gold Manager</b></td>

<td class="centerbold">9</td>

<td class="centerbold transposeME">$250.00</td>

</tr>

<tr class="alt">

<td class="bold">Ruby Director</td>

<td class="centerbold">12</td>

<td class="centerbold transposeME">$350.00</td>

</tr>

<tr>

<td><b>Emerald Director</b></td>

<td class="centerbold">16</td>

<td class="centerbold transposeME">$475.00</td>

</tr>

<tr class="alt">

<td class="bold">Diamond Director</td>

<td class="centerbold">20</td>

<td class="centerbold transposeME">$600.00</td>

</tr>

<tr>

<td><b>Executive Director</b></td>

<td class="centerbold">25</td>

<td class="centerbold transposeME">$1000.00</td>

</tr>

<tr class="alt">

<td class="bold">Ambassadors Club</td>

<td class="centerbold">60</td>

<td class="centerbold transposeME">$6000.00</td>

</tr>

</table>

</div>

--></div>

<div class="panel">

<h5 class="darkblue">V - Hoeveel tijd moet ik er wekelijks aan besteden om Partner te zijn?</h5>

<p>A - Dat is volledig aan u! Sommige van onze Partners zijn begonnen door in hun vrije tijd te werken en zijn gaandeweg naar een fulltime inkomen gegaan.</p>

<p>U krijgt betaald op basis van de aankopen die gedaan zijn door de mensen die u hebt verwezen en de mensen die zij weer hebben verwezen, zonder dat u zelf verkopen of leveringen hoeft uit te voeren. Indien u een bepaalde week totaal geen tijd kunt investeren, maar de members die u heeft verwezen doen in deze week aankopen of verwijzen andere mensen, dan verdient u inkomen op de aankopen die zij hebben gedaan. Echter, hoe meer tijd u investeert om Members te verwijzen en non-profit organisaties, winkels/ondernemers en Partners te sponsoren, des te hoger uw inkomen kan zijn!</p>

</div>

<div class="panel">

<h5 class="darkblue">V - Hoe kan ik persoonlijk voldoende mensen verwijzen om een full time inkomen te ontwikkelen?</h5>

<p>A - Er zijn veel methoden die u zult leren en die beschikbaar zijn voor u als Partner om members te verwijzen.</p>

<div class="toggle" style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<ul>

<li class="blue_bullet">In alle mogelijke GPS abonnementen is een Referral Marketing Trainingsprogramma opgenomen, dat u kunt gebruiken om te leren hoe:                                         

<ul>

<li class="half">u sociale metwerken kunt gebruiken om mensen                        die u kent te verwijzen om lid te worden.</li>

<li class="half">u advertenties op het internet plaatst om                        wereldwijd mensen te werven om lid te worden.</li>

<li class="half">u e-mail marketing kunt gebruiken om non-profit                        organisaties te verwijzen (zodat zij leden kunnen verwijzen                        en ClubShop Reward kaarten kunnen verkopen als fondsenwerver).</li>

<li class="half">u e-mail marketing kunt gebruiken om offline                        Winkels/ ondernemers te verwijzen (locale bedrijven - restaurants,                        pizza, kapper, groenteboer, tankstations, etc.) om de ClubShop                        Rewards kaart te accepteren en verkopen.</li>

<li class="half">u ClubShop Rewards kaarten kunt verkopen                        en distribueren.</li>

</ul>

</li>

<li class="blue_bullet">In het GPS PRO abonnement is tevens een sponsor en coach trainingsprogramma opgenomen, net als een volledig Contact- en Opvolg systeem waarvan u gebruik kunt maken om te leren hoe u uw andere partners kunt sponsoren en coachen.</li>

<li class="blue_bullet">De hulp en begeleiding van een Partner                    Sponsor en Team Coach, trainen u om succesvol referral methoden                    toe te passen die zij wellicht zelf ook gebruiken</li>

<li class="blue_bullet">Abonneer op GPS PRO PLUS, PREMIER of PREMIER PLUS en ontvang elke maand automatisch nieuwe Affiliates!</li>

</ul>

</div>

</div>

</div>

</div>

<div class="panel">

<h5 class="darkblue">V - Moet ik een bijdrage betalen om een Partner te kunnen zijn?</h5>

<p>A - Nee. U kwalificeert als Partner wanneer u 10 Pay Punten verdient op aankopen die gedaan zijn door uzelf of door de leden die u hebt verwezen. U kunt automatisch voldoen aan deze kwalificatie door het aankopen van elk soort <a href="http://www.clubshop.com/gps.html" target="_blank">GPS</a> abonnement.</p>

</div>

<!--

<div class="panel">

<h5 class="darkblue">V - Wat gebeurt er wanneer ik stop met het betalen van mijn maandelijkse bijdrage?</h5>

<p>A - U zult nog steeds uw ClubShop Rewards lidmaatschap behouden en verder kunnen gaan met het verdienen van een inkomen als Affiliate.</p>

<div class="toggle" style="display:none;" mce_style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<ul>

<li class="blue_bullet">Wanneer u niet meer kwaliciceert als Partner omdat u geen maandelijkse bijdrage meer betaald, zal uw lidmaatscap worden veranderd van Partner naar Affiliate en verliest u alle Partner voordelen en rechten die u had. </li>

<li class="blue_bullet">U kunt opnieuw kwalificeren als Partner, door eenvoudig opnieuw uw maandelijkse bijdrage te gaan betalen.</li>

</ul>

</div>

</div>

</div>

</div>

--> <!--

<div class="panel">

<h5 class="darkblue">V - Bieden jullie ook een vorm van geld-terug-garantie aan nieuwe Partners?</h5>

<p>A - Die hebben we! Word Partner door u in te schrijven voor het Glocal Income Business Building Systeem en neem gedurende 30 dagen de tijd om ons ClubShop Rewards Partner Inkomsten Mogelijkheid  te bekijken. Indien u niet volledig tevreden bent als Partner, annuleert u uw positie en uw abonnement en krijgt u 100% uw initiÃƒÂ«le of maandelijkse abonnementsgeld terug betaald. U heeft niets te verliezen en loopt geen enkel risico wanneer u Partner wordt!</p>

<div class="toggle" style="display:none;" mce_style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<h6 class="medblue bold">V - Hoe krijg ik Partners toegevoegd aan mijn organisatie?</h6>

<p class="smaller">A - Elke Member die na u, een gratis Proefpartner positie aanvraagd, krijgt onder u een positie in uw Partner Organisatie. Deze Members zijn verwezen door de vele Partners in onze wereldwijde Partner organisatie. Mocht een Member die onder u een proefpositie heeft gekregen, besluiten om Partner te worden, dan telt deze mee als een Partner in uw organisatie. Dit is echt teamwerk in actie!</p>

<h6 class="medblue bold">V - Wat is de waarde van het hebben van Partners in mijn organisatie?</h6>

<p class="smaller">A - Het aantal Partners in uw organisatie helpt u om te voldoen aan ÃƒÂ©ÃƒÂ©n van de belangrijkste kwalificaties om Partner inkomen te verdienen en te verhogen.</p>

</div>

</div>

</div>

</div>

-->

<div class="panel">

<h5 class="darkblue">V - Welke garanties heb ik dat ik in de toekomst blijvend commissies zal verdienen, voor de verwijzingen die ik gedaan heb ik het verleden?</h5>

<p>A - Dit is waarschijnlijk de belangrijkste vraag om antwoord op te krijgen wanneer u kijkt naar elke vorm van een zakelijke mogelijkheid. U moet kijken naar de geschiedenis van een bedrijf, reputatie en leiderschap, om vast te stellen of het bedrijf in staat zal zijn om zaken te blijven doen of bereid is om de inspanningen van hun marketing            vertegenwoordigers in de toekomst in ere te houden. ClubShop Rewards wordt ondersteund door een bedrijf dat altijd alle commissies heeft uitbetaald sinds het werd opgericht in 1997.</p>

<div class="toggle" style="display:none;">

<div class="row">

<div class="eleven columns offset-by-one">

<h6 class="medblue bold">V - Wie is het bedrijf dat de ClubShop Rewards Partner Inkomsten Mogelijkheid aanbiedt?</h6>

<p class="smaller">A - ClubShop Rewards is eigendom van en wordt geÃ«xploiteerd door de Discount Home Shoppers' Club, Inc. (AKA: DHS Club), een Florida, USA corporatie, opgericht in 1997.</p>

<h6 class="medblue bold">V - Wat is de financiÃ«le en juridische staat van dienst van het bedrijf? Het bedrijf heeft tevens een A+ notering bij het Better Business Bureau.</h6>

<p class="smaller">A - Het bedrijf is schuldenvrij en is nooit genoemd in een rechtszaak.</p>

<h6 class="medblue bold">V -Waar heeft het bedrijf vestigingen?</h6>

<p class="smaller">A - Het hoofdkantoor van ons bedrijf is gevestigd in Englewood, FL, USA. Het bedrijf heeft tevens personeelsleden die in andere staten van de USA wonen en internationale personeelsleden, om ons wereldwijde leden en Partners zo goed mogelijk van dienst te zijn.</p>

</div>

</div>

</div>

</div>

</div>

<!-- END OF NINE COLUMN --> <!--Main Content -->

<div class="three columns"><!--Side Content -->

<div class="bannerJoin">

<p><a class="nice radius large blue button" href="http://www.glocalincome.com/cgi/odp.cgi?id=[% myodp.me.id %]">Proefpartner Rapport:Â»</a></p>

<p>Klik hier om uw Proefpartner Rapport te bekijken</p>

</div>

</div>

<div class="three columns"><!--Side Content -->

<div class="bannerJoin">

<p><a class="nice radius large blue button" href="https://www.glocalincome.com/cgi/appx.cgi/upg?alias=[% myodp.me.alias %]">ClubShop Rewards Partner worden:</a></p>

<p>Klik hier om ClubShop Rewards Partner te worden</p>

</div>

</div>

<!-- END OF THREE COLUMN --> <!-- End Side Content --> 

<hr />

</div>

<!-- End of Content Row -->



</div>

<!-- End of Content Container --> <!--End Content --> <!--Footer Container -->

<div class="container blue">

<div class="row ">

<div class="twelve columns">

<div class="push"></div>

</div>

</div>

<div class="row ">

<div class="twelve columns centered">

<div id="footer">

<p>Copyright Ã‚Â© 1997-            <!-- Get Current Year -->

<script type="text/javascript">// <![CDATA[

							var dteNow = new Date();

							var intYear = dteNow.getFullYear();

							document.write(intYear);

// ]]></script>

ClubShop Rewards, All Rights Reserved.</p>

</div>

</div>

</div>

</div>

<!-- container --> <!--End Footer Container -->

<script type="text/javascript">// <![CDATA[

    $(document).ready(function() {

        Transpose.transpose_currency();

    });

// ]]></script>

</body>

</html>