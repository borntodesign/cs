<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
[% USE myodp = Local::odp %]
-->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8" />

<script type="text/javascript">
[% myodp.GetSetCookie() %]
</script>
<meta http-equiv="refresh" content="0;url=http://www.glocalincome.com/cgi/odp.cgi?id=[% myodp.me.id %]"> 
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />
<title>Trial Partner Information</title>
<!-- Included CSS Files -->
<link rel="stylesheet" type="text/css" href="../css/partner/foundation.css">
<link rel="stylesheet" type="text/css" href="../css/partner/app.css">
<link rel="stylesheet" type="text/css" href="../css/partner/general.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<!--[if lt IE 9]>
		<link rel="stylesheet" href="../css/partner/ie.css">
	<![endif]-->
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!-- Included JS Files -->
<script src="../js/partner/foundation.js"></script>
<script src="../js/partner/app.js"></script>
<script src="../js/partner/flash.js"></script>	

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
		<script type="text/javascript">
		swfobject.registerObject("prelaunch", "11.0.0", "expressInstall.swf");
		</script>
		
		
		
</head>
<body>
<!-- Top Blue Header Container -->
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="../images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"><!--Extra Space Future Space --></div>
</div>
</div>
<!--End Top Blue Header Container --> <!--Orbit Slider Container -->
<div class="container">
<div class="row">
<div class="twelve columns ">
<div class="hide-on-phones" style="margin:5px 0px 0px;">				
			
			<div>
				<embed type="application/x-shockwave-flash" src="http://www.clubshop.com/partner/flash/prelaunch.swf" width="980px" height="200px" bgcolor="FFFFFF" />

			<noembed>
				<div>
					<h4>Please downlaod the latest Flash Player</h4>
					<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" style="border:none;" /></a></p>
				</div>
			</noembed>
		</div>
	
	
      </div>
<hr />
</div>
</div>
</div>
<!--End Orbit Slider Container --> <!--Content Container -->
<div class="container ">
<div class="row ">
<div class="nine columns"><!--Main Content -->
<h4 class="center">PROEFPARTNER INFORMATIE</h4>
<p>Als ProefPartner heeft u voor de duur van 15 dagen een positie gekregen in onze wereldwijde Partner Organisatie, <span class="hps">waarmee u kunt </span><span class="hps">profiteren van</span> <span class="hps">de wereldwijde</span> referral <span class="hps">inspanningen van</span> <span class="hps">al onze</span> <span class="hps">Partners</span> tesamen. Alle Members die, na u, een proef partner positie krijgen en Partner worden voor uw deadline verstreken is, komen in UW Partner Organisatie terecht wanneer uzelf ook Partner bent geworden voor uw 15 proef dagen verstreken zijn.</p>
<p>Er zijn twee basisvereisten waaraan u moet voldoen om te beginnen met geld verdienen en om dit inkomen te verhogen. De ene is een Persoonlijke Verkoop vereiste en de andere is een Organisatie Verkoop vereiste. De Organisatie Verkoop pay levels worden bepaald door de verkopen die gedaan worden in UW Partner  					Organisatie, welke gebaseerd is op de inspanningen van alle Partners die in uw organisatie komen.</p>
<p>De eerste investering om Partner te worden is slechts [% myodp.SubscriptionPrice('initial') %].
Vervolgens kunt u zich blijvend kwalificeren als Partner en profiteert u van alle voordelen van ons
"Partner Business Building Systeem" met een lage maandelijkse abonnementsbijdrage van slechts [% myodp.SubscriptionPrice('monthly') %],
welke van uw inkomsten zal worden afgetrokken zodra u ten minste hetzelfde bedrag aan maandelijks inkomen gaat verdienen.</p>

<p>Uw persoonlijke Proef Partner Positie status:</p>

<ul class="disc">
<li>U heeft nu <strong>[% myodp.me.counts.member_count %]</strong> (indien meer dan 1.000, dan staat er "meer dan 1.000") Members die na u een proef positie hebben gekregen en die potentiele Partners zijn die aan UW Partner Organisaie worden toegevoegd.</li>
<li><strong>[% myodp.me.counts.partner_count %]</strong> van deze Members zijn Partners geworden, welke in UW Partner Organisatie komen indien u besluit Partner te worden voordat de termijn van uw proefpositie verstreken is.</li>
<li>U heeft nog <strong>[% myodp.me.days_left %] dagen</strong> om te beslissen of u
<a href="https://www.glocalincome.com/cgi/appx.cgi/upg?alias=[% myodp.me.alias %]">een Partner wilt worden</a> om uw positie veilig te stellen in onze wereldwijde Partner Organisatie en om alle huidige Partners die nu in UW potentiele Organisatie zitten, te behouden. ALLE mensen die in de toekomst Partners worden, komen dan tevens in UW Partner organisatie terecht. Indien u geen Partner bent op <strong>[% myodp.me.deadline %]</strong>, dan verliest u uw proeforganisatie en het voordeel van de mogelijkheid om deze en toekomstige Partners, op basis van uw huidige positie, te behouden.</li>
<li>Mocht u Partner worden voor bovenstaande datum, dan <strong>garanderen</strong> wij dat u binnen 30 dagen nadat u Partner bent geworden, ten minste 100 Partners in uw Partner Organisatie heeft, of anders krijgt u uw geld terug. </li>
</ul>

<p>Wilt u meer weten? Lees dan onze <a href="faq.html?id=[% myodp.me.id %]" target="_blank">FAQ</a> pagina...</p>
<hr />
<p>Heeft u nog onbeantwoorde vragen? Neem dan contact op met uw Partner Team Coach:
[% myodp.me.coach.firstname %] [% myodp.me.coach.lastname %] <br />
	  Email Address: <a href="mailto:[% myodp.me.coach.emailaddress %]">[% myodp.me.coach.emailaddress %]</a><br />
	  Skype: [% IF ! myodp.me.coach.skype %]N/A[% ELSE %][% myodp.me.coach.skype %][% END %]
</p>

</div>
<!-- END OF NINE COLUMN --> <!--Main Content -->
<div class="three columns"><!--Side Content -->
<div class="bannerJoin">
<p><a class="nice radius large blue button" href="http://www.glocalincome.com/cgi/odp.cgi?id=[% myodp.me.id %]" style="padding: 11px 20px 13px;" target="_blank">KLIK HIER»</a></p>
<p>om uw Proef Partner Organisatie Rapport en wereldkaart te bekijken.</p>
</div>
<div class="bannerJoin">
<p><a class="nice radius large blue button" href="https://www.glocalincome.com/cgi/appx.cgi/upg?alias=[% myodp.me.alias %]" style="padding: 11px 20px 13px;">KLIK HIER»</a></p>
<p>om een ClubShop Rewards Partner te worden.</p>
</div>
</div>
<!-- END OF THREE COLUMN --> <!-- End Side Content --> 
<hr />
</div>
<!-- End of Content Row --></div>
<!-- End of Content Container --> <!--End Content --> <!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997- 					 						<!-- Get Current Year -->
<script type="text/javascript">// <![CDATA[
							var dteNow = new Date();
							var intYear = dteNow.getFullYear();
							document.write(intYear);
// ]]></script>
ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
</div>
<!-- container --> <!--End Footer Container -->
</body>
</html>
