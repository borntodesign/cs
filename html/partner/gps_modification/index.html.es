<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <html xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<head>
<title>Change GPS Subscription</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation2.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />
<script src="/js/jquery-min.js"></script>
<script src="/js/foundation/foundation.js"></script>
<script src="/js/foundation/flash.js"></script>
<script src="/js/foundation/jquery.reveal.js"></script>
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<link href='http://fonts.googleapis.com/css?family=Nunito:400,700' rel='stylesheet' type='text/css'>
<style type="text/css">
.white {color: #F0F0F0;}
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}
.errors { color: red; font-size: larger;}
.alerts { background-color: yellow; font-size: larger; }
.messages { color: green; font-size: larger; }
</style>


<script>
    function PopupCenter(pageURL, title,w,h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="four columns "><br />
<h4 class="topTitle"><img height="246" src="/manual/gps/med_gps.png" width="332" /></h4>
</div>
<div class="eight columns "><br /><br />
<div align="center"><span class="Mainline">Modificar el GPS </span><br /> <br /><span class="style28">Tu suscripciòn es</span> <span class="style3 bold">"GPS PRO"</span>.</div>
<br /><br /><br />
<p>El propòsito de esta pàgina es de permitir a los suscriptores de modificar el tipo de suscripciòn. <br />Entra en <a href="https://www.clubshop.com/manual/gps/overview.html" target="_blank">Visiòn del GPS </a> para màs informaciòn sobre los beneficios y cuestos</p>
</div>
<hr />
</div>
<div class="row">
<div class="twelve columns">
<p class="style28 navy">COMO PUEDES ACTUALIZAR TU SUSCRIPCIÒN</p>
<ul>
<li class="blue_bullet">Si tu cuota de suscripciòn està ya pagada, puedes pagar la "diferencia"  del cuesto de tu suscripciòn actual con la opciòn GPS que has elegido. Una vez que has seleccionado y pagado por la opciòn elegida, tendràs derecho inmediatamente a todos los beneficios que la opciòn actualizada te ofrece. Si uno de estos beneficios es recibir nuevos afiliados, los mismos seràn transferidos en el plazo de 30 dìas de cuando tu suscripciòn fuè actualizada.</li>
<li class="blue_bullet">Si tu suscripciòn no ha sido pagada todavìa , la actualizaciòn se harà en el momento que el pago serà efectuado. </li>
</ul>
<p>Utiliza tu <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">Cuenta del Club</a> para seleccionar y pagar la opciòn de suscripciòn seleccionada. Si normalmente pagas tus cuotas con carta de crèdito/dèbito o te las deducen de tus comiciones entonces tus cuotas futuras seràn pagadas de la misma manera.</p>
</div>
</div>
<div class="row">
<div class="twelve columns"><br />
<p><span class="style28 navy">DEGRADACIÒN AUTOMÀTICA DE LA SUSCRIPCIÒN - En caso que sucediera algunos de los incidentes siguientes tu suscripciòn serà degradada: </span></p>
<ul>
<li class="blue_bullet">Si normalmente pagas con carta de crèdito/dèbito y el cargo de la cuota de suscripciòn es rechazada, tu suscripciòn serà degradada  automaticamente a Pro ( si tu suscripciòn era una opciòn màs alta de PRO)</li>
<li class="blue_bullet">Si normalmente tu cuota es deducida de tus comisiones pero no ganas suficiente para pagar dicha cuota, tu suscripciòn serà automaticamente degradada a PRO. </li>
</ul>
<p>In cualquiera de los dos casos mostrados arriba podràs hacer la actualizaciòn de tu suscripciòn despuès de haber sido degradada. Ve a COMO PUEDES ACTUALIZAR TU SUSCRIPCIÒN y sigue las instrucciones</p>
<br />
<p class="style28 navy">COMO DEGRADAR TU SUSCRIPCIÒN</p>
<ul>
<li class="blue_bullet">Si tu actual suscripciòn ha sido ya pagada, la degradaciòn serà actual el pròximo mes por el cual no has pagado  todavìa . Por ejemplo si has solicitado degradar tu actual suscripciòn en Junio pero tu cuota està pagada por junio, la degradaciòn serà efectuada en Julio. Si has pagado la cuota por Junio y Julio, la degradaciòn serà efectuada en Agosto. </li>
<li class="blue_bullet">Si no has pagado tu suscripciòn, la degradaciòn serà efectuada inmediatamente una vez que hayas pagado por la opciòn elegida. </li>
<li class="blue_bullet">Todos los suscriptores GIBBS (con una suscripciòn anterior a Junio 2012) no podràn degradar la actual suscripciòn a la opciòn Base y continuar a retener la organizaciòn propia de Partners, Afiliados, Grupos de Afinidad y Mercantes que habìan referido, o sea para activar una opciòn Base es necesario degradar la actual posiciòn de Partner hacia Afiliado y activar la opciòn Base para recualificarse otra vez como Partner. Si quieres cancelar tu actual suscripciòn y degradar tu posiciòn hacia Afiliado <a href="http://www.glocalincome.com/cgi/cancel.cgi" target="_blank">HAZ CLICK AQUÌ</a>.</li>
</ul>
</div>
</div>
<div class="row">
<div class="six columns"><!--
<div class="panel">-->
<div style="width: 100%;"><br /><br /><br /><br /> <form method="get"> 
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" /></td>
<td>CUOTA PAGADA - Tu cuota actual està pagada. Selecciona la opciòn preferida para degradar tu suscripciòn</td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td>CUOTA NO PAGADA  - Tu cuota actual no està pagada. Utiliza tu <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">Cuenta del Club</a> para seleccionar y pagar por la opciòn de suscripciòn que prefieres</td>
</tr>
</tbody>
</table>
</form></div>
</div>
<!--</div>
-->
<div class="six columns">
<div class="panel">
<div style="width: 100%;">
<p class="royal style24">Selecciona la opciòn GPS que prefieres para degradar tu suscripciòn:</p>
<form method="get"> 
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PREMIER</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PRO</span></td>
</tr>
</tbody>
</table>
<p style="border-bottom: 2px dotted gray"> </p>
<p align="center"><input type="submit" value="Apply" /></p>
<input name="_action" type="hidden" value="change" /></form></div>
</div>
</div>
</div>
<!--
<div class="row">
<div class="twelve columns">
<div class="panel">
<div style="width: 100%;">
<h5 class="royal">CHANGE SUBSCRIPTION TO:</h5>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<form action="/cgi/vip/gps_modification.cgi" method="get"> 
<table>
<tr>
<td><input type="radio" value="43" name="stype"></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span> (Only available to subscribers that pay by credit/debit card or to Bronze Managers and higher (based on your current Pay Level achievement)</td>
</tr>
<tr>
<td><input type="radio" value="45" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER</span> (Only available to subscribers that pay by credit/debit card or to Ruby Directors and higher (based on your current Pay Level achievement)</td>
</tr>
<tr>
<td><input type="radio" value="47" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER PLUS</span> (Only available to subscribers that pay by credit/debit card or to Executive Directors (based on your current Pay Level achievement)</td>
</tr>
</table>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<p>Note: If paying by credit/debit card and the charge is declined, your subscription will be reset to PRO. Upon our receiving your PRO subscription fee in June, you can then upgrade your subscription and pay the difference between the PRO fee and the upgraded fee. Once we have received the additional funds, your Affiliates will be transferred to you as soon as possible and within 30 days of our receipt of your upgrade fees.</p>
<p align="center"><input type="submit" value="Apply" /></p>
<input type="hidden" name="_action" value="change" /></form></div>
</div>
</div>
</div>
-->   <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>