<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <html xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<head>
<title>Change GPS Subscription</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation2.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />
<script src="/js/jquery-min.js"></script>
<script src="/js/foundation/foundation.js"></script>
<script src="/js/foundation/flash.js"></script>
<script src="/js/foundation/jquery.reveal.js"></script>
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<link href='http://fonts.googleapis.com/css?family=Nunito:400,700' rel='stylesheet' type='text/css'>
<style type="text/css">
.white {color: #F0F0F0;}
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}
.errors { color: red; font-size: larger;}
.alerts { background-color: yellow; font-size: larger; }
.messages { color: green; font-size: larger; }
</style>


<script>
    function PopupCenter(pageURL, title,w,h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84px" src="/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288px" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="four columns "><br />
<h4 class="topTitle"><img height="246" src="/manual/gps/med_gps.png" width="332" /></h4>
</div>
<div class="eight columns "><br /><br />
<div align="center"><span class="Mainline">GPS Functie wijzigen</span> <br /> <br /> <br /> <span class="style28">Uw GPS huidige abonnement is ingesteld op</span> <span class="style3 bold">"GPS PRO"</span>.</div>
<br /><br /><br />
<p>Het doel van deze pagina is om het mogelijk te maken voor GPS abonnees om hun abonnement type te wijzigen. <br />Voor meer informatie over de voordelen en kosten van de GPS abonnementen, ga naar <a href="https://www.clubshop.com/manual/gps/overview.html" target="_blank">GPS Overzicht</a></p>
</div>
<hr />
</div>
<div class="row">
<div class="twelve columns">
<p class="style28 navy">HOE U UW ABONNEMENT KUNT OPWAARDEREN</p>
<ul>
<li class="blue_bullet">Wanneer uw huidige abonnementsbijdrage betaald is, kunt u het verschil tussen de prijs van uw huidige pakket en het pakket waarnaar u wilt opwaarderen bijbetalen. Zodra u het pakket hebt geselecteerd en betaald, heeft u onmiddellijk toegang tot de voordelen die het pakket u bieden. Wanneer 1 van de voordelen van het pakket bestaat uit het krijgen van nieuwe Affiliates, zullen de Affiliates binnen 30 dagen nadat u het abonnement hebt genomen, naar u worden overgeschreven. </li>
<li class="blue_bullet">Wanneer uw huidige abonnementsbijdrage nog niet betaald is, zal de upgrade onmiddellijk plaatsvinden zodra u het opgewaardeerde pakket hebt betaald.</li>
</ul>
<p>Gebruik uw <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">ClubAccount</a> om het abonnement te selecteren en betalen waarnaar u wilt opwaarderen. Wanneer u normaalgesproken uw bijdragen betaald via credit/ debit kaart of ze direct van uw commissies laat betalen, zullen bijdragen in het vervolg op dezelfde manier worden betaald.</p>
</div>
</div>
<div class="row">
<div class="twelve columns"><br /><br />
<p><span class="style28 navy">HET AUTOMATISCH DOWNGRADEN VAN ABONNEMENTEN</span> - Zou het volgende gebeuren, dan wordt uw abonnement automatisch gedowngrade:</p>
<ul>
<li class="blue_bullet">Wanneer u normaalgesproken betaald via credit/debit kaart en het innen voor een abonnementsbijdrage wordt geweigerd, zal uw abonnement automatisch worden gedowngrade naar PRO, wanneer u een abonnement hebt dat hoger is dan PRO.</li>
<li class="blue_bullet">Wanneer u normaalgesproken uw abonnementsbijdrage van uw commissies laat betalen, maar uw verdiensten zijn niet hoog genoeg om uw huidige bijdrage van te betalen, dan zal uw abonnement automatisch worden gedowngrade naar een PRO lidmaatschap. </li>
</ul>
<p>In beide gevallen zoals hierboven omschreven, kunt u uw abonnement weer upgraden nadat het gedowngrade is. Zie hierboven voor instructies over HOE UW ABONNEMENT TE UPGRADEN.</p>
<br /><br />
<p class="style28 navy">HOE UW ABONNEMENT TE DOWNGRADEN</p>
<ul>
<li class="blue_bullet">Wanneer uw huidige abonnementsbijdrage betaald is, dan zal de downgrade plaatsvinden per de volgende maand dat waarvoor niet betaald is. Bijvoorbeeld, wanneer u in juni vraagt om een downgrade van uw huidige abonnement, maar uw bijdrage van juni is al betaald, dan zal de downgrade in juli plaatsvinden. Wanneer uw huidige bijdrage betaald is voor juni en juli, dan zal de downgrade plaatsvinden in augustus. </li>
<li class="blue_bullet">Wanneer uw huidige abonnementsbijdrage nog niet betaald is, dan zal de downgrade onmiddellijk plaatsvinden zodra u dit hebt geselecteerd voor de downgrade.</li>
<li class="blue_bullet">Het is voormalige GIBBS abonnees (abonnement gestart voor juni 2012) niet toegestaan om hun abonnement te downgraden naar BASIS met het behouden van hun Organisatie Partners of Affiliates, non-profit organisaties, winkeliers/ ondernemers of Partners zie zij weer hebben verwezen. In plaats hiervan moet u downgraden naar Affiliate om vervolgens van daar uit een BASIS abonnement te nemen om te her-kwalificeren als een Partner. Om uw GPS abonnement te annuleren en uw lidmaatschap te downgraden om een Affiliate te worden, <a href="http://www.glocalincome.com/cgi/cancel.cgi" target="_blank">KLIKT U HIER</a>.</li>
</ul>
</div>
</div>
<div class="row">
<div class="six columns"><!--
<div class="panel">-->
<div style="width: 100%;"><br /><br /><br /><br /> <form method="get"> 
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">BIJDRAGE BETAALD</span> - Uw huidige bijdrage is betaald. Selecteer rechts het abonnement waarnaar u wilt downgraden.</td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">Onbetaalde Bijdrage</span> - Uw huidige bijdrage is niet betaald. Gebruik uw <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">ClubAccount</a> om het abonnementspakket van uw voorkeur te selecteren en te betalen.</td>
</tr>
</tbody>
</table>
</form></div>
</div>
<!--</div>
-->
<div class="six columns">
<div class="panel">
<div style="width: 100%;">
<p class="royal style24">Selecteer het GPS abonnement waar u naar wilt downgraden:</p>
<form method="get"> 
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PREMIER</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" /></td>
<td><span class="ContactForm_TextField bold">PRO</span></td>
</tr>
</tbody>
</table>
<p style="border-bottom: 2px dotted gray"> </p>
<p align="center"><input type="submit" value="Apply" /></p>
<input name="_action" type="hidden" value="change" /></form></div>
</div>
</div>
</div>
<!--
<div class="row">
<div class="twelve columns">
<div class="panel">
<div style="width: 100%;">
<h5 class="royal">WIJZIG ABONNEMENT NAAR:</h5>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<form action="/cgi/vip/gps_modification.cgi" method="get"> 
<table>
<tr>
<td><input type="radio" value="43" name="stype"></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span> (Alleen beschikbaar voor abonnees die betalen via credit/ debit kaart of voor Brons Managers en hoger (op basis van uw huidige bereikte Pay Level)</td>
</tr>
<tr>
<td><input type="radio" value="45" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER</span> (Alleen beschikbaar voor abonnees die betalen via credit/ debit kaart of voor Ruby Directors en hoger (op basis van uw huidige bereikte Pay Level)</td>
</tr>
<tr>
<td><input type="radio" value="47" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER PLUS</span> (Alleen beschikbaar voor abonnees die betalen via credit/ debit kaart of voor Executive Directors (op basis van uw huidige bereikte Pay Level)</td>
</tr>
</table>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<p>Opmerking: Wanneer bij betaling via credit/ debit kaart de betaling geweigerd wordt, zal uw abonnement terug worden gezet naar PRO. Totdat wij uw PRO abonnementsbijdrage van juni hebben ontvangen, u kunt dan uw abonnement upgraden en het verschil tussen de PRO bijdrage en de ge-upgrade fee betalen. Zodra wij de extra fondsen hebben ontvangen, zullen uw Affiliates zo spoedig mogelijk en binnen 30 dagen na het ontvangen van uw upgrade bijdrage naar u worden verplaatst.</p>
<p align="center"><input type="submit" value="Apply" /></p>
<input type="hidden" name="_action" value="change" /></form></div>
</div>
</div>
</div>
-->   <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012           <!-- Get Current Year --> ClubShop Rewards, All Rights Reserved.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>