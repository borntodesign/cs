<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <html xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<head>
<title>Modification d'abonnement GPS</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/foundation2.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/general.css" />
<link rel="stylesheet" type="text/css" href="/css/foundation/reveal.css" />
<script src="/js/jquery-min.js"></script>
<script src="/js/foundation/foundation.js"></script>
<script src="/js/foundation/flash.js"></script>
<script src="/js/foundation/jquery.reveal.js"></script>
<link href="http://www.clubshop.com/css/manual_2012.css" rel="stylesheet" type="text/css"/>

<link href='http://fonts.googleapis.com/css?family=Nunito:400,700' rel='stylesheet' type='text/css'>
<style type="text/css">
.white {color: #F0F0F0;}
.Mainline {
	font-size: 20px;
	color: #2A5588;
	font-weight:bold;
	line-height:20px;
	letter-spacing:0.1em;
	font-family: verdana;
	text-transform: uppercase;
	}
.errors { color: red; font-size: larger;}
.alerts { background-color: yellow; font-size: larger; }
.messages { color: green; font-size: larger; }
</style>


<script>
    function PopupCenter(pageURL, title,w,h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>
</head>
<body>
<div class="container blue">
<div class="row">
<div class="six columns"><a href="#"><img height="84" src="/images/partner/cs_logo.jpg" style="margin-top:5px;" width="288" /></a></div>
<div class="six columns"></div>
</div>
</div>
<div class="container">
<div class="row">
<div class="four columns "><br />
<h4 class="topTitle"><img height="246" src="/manual/gps/med_gps.png" width="332" /></h4>
</div>
<div class="eight columns "><br /><br />
<div align="center"><span class="Mainline">Modification de la fonction GPS</span> <br /><br /><br /><span class="style28">Votre abonnement GPS est réglé sur</span> <span class="style3 bold">"GPS PRO"</span>.</div>
<br /><br /><br />
<p>Le but de cette page est de permettre aux abonnés de modifier leur type d'abonnement GPS. <br />Pour plus d'informations sur les avantages et les coûts dbonnements GPS, allez sur <a href="https://www.clubshop.com/manual/gps/overview.html" target="_blank">Aperçu GPS</a></p>
</div>
<hr />
</div>
<div class="row">
<div class="twelve columns">
<p class="style28 navy">COMMENT METTRE À JOUR VOTRE ABONNEMENT</p>
<ul>
<li class="blue_bullet" value="0">Si votre abonnement actuel est payé, vous pouvez payer la "différence" entre le coût de votre forfait actuel et le pack de mise à niveau sélectionnée. Une fois que vous avez choisi et payé pour votre pack de mise à niveau, vous serez immédiatement en droit d'avoir les avantages du pack de mise à niveau fournit. Si vous avez le droit de gagner de nouveaux Affiliates en tant que l'un de vos avantages, les Affiliates seront transférés dans un délai de 30 jours à compter de la mise à niveau de votre abonnement. </li>
<li class="blue_bullet" value="0">Si votre abonnement actuel n'est pas encore payé, la mise à niveau aura lieu immédiatement après votre paiement pour les frais de mise à niveau sélectionnée.</li>
</ul>
<p>Utilisez votre <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">Compte Club</a> pour sélectionner et payer le pack d'abonnement pour lequel vous souhaitez vous mettre à jour. Si habituellement, vous avez l'habitude de payer les frais par carte de crédit / carte de débit ou en les faisant déduire de vos commissions, des frais futurs seront pris en charge de la même manière.</p>
</div>
</div>
<div class="row">
<div class="twelve columns"><br /><br />
<p><span class="style28 navy">RETROGRADATIONS AUTOMATIQUES DES ABONNEMENTS</span> - Si l'un des incidents suivants se produit, votre abonnement sera automatiquement déclassé:</p>
<ul>
<li class="blue_bullet" value="0">Si vous payez normalement par carte de crédit /débit et la cotisation de l'abonnement est refusée, votre abonnement sera automatiquement rétrogradé à PRO, si vous aviez un abonnement qui est plus élevé que PRO.</li>
<li class="blue_bullet" value="0">Si votre abonnement est normallement déduit de vos commissions, mais que vos revenus ne sont pas aussi improtant que votre cotisation actuelle, votre abonnement sera automatiquement rétrogradé à un abonnement PRO. </li>
</ul>
<p>Dans les deux cas ci-dessus, vous pouvez mettre à jour votre abonnement après qu'il a été rétrogradé. Allez Voir COMMENT METTRE A JOUR VOTRE ABONNEMENT ci-dessus pour obtenir des instructions pour le faire.</p>
<br /><br />
<p class="style28 navy">COMMENT DECLASSER VOTRE ABONNEMENT</p>
<ul>
<li class="blue_bullet" value="0">Si votre abonnement en cours est payé, la rétrogradation/déclassement aura lieu le mois suivant celui^pour lequel vous avez payé. Par exemple, si vous demandez à déclasser votre abonnement actuel en Juin, mais que votre cotisation est payée pour le mois de Juin, le déclassement aura lieu en Juillet. Si votre cotisation actuelle est payée pour le mois de Juin et de Juillet, alors le déclassement aura lieu en Août. </li>
<li class="blue_bullet" value="0">Si votre abonnement actuel n'est pas encore payé, le déclassement aura lieu immédiatement après votre paiement de la cotisation de l'abonnement déclassé choisi.</li>
<li class="blue_bullet" value="0">Lzq anciens abonnés GIBBS (abonnement initialement achetés avant Juin 2012) ne seront pas autorisés à déclasser leurs abonnements BASIC et à conserver leurs Partners organisationnels ou leurs Affiliates, AGS, commmerçants ou Partners qu'ils ont référencésappelé. Au contraire, vous aurez besoin de vous déclasser en tant qu'Affiliate, puis vous pourrez acheter un abonnement BASIC pour vous re-qualifier en tant que Partner. Pour annuler votre abonnement GPS et déclasser votre type d'abonnement au niveau d'Affiliate, <a href="http://www.glocalincome.com/cgi/cancel.cgi" target="_blank">CLIQUEZ ICI</a>.</li>
</ul>
</div>
</div>
<div class="row">
<div class="six columns"><!--
<div class="panel">-->
<div style="width: 100%;"><br /><br /><br /><br /><form enctype="application/x-www-form-urlencoded" method="get">
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" value="on" /></td>
<td><span class="ContactForm_TextField bold">FRAIS PAYES</span> - Votre cotisation actuelle est payée. Sélectionnez l'abonnement à droite pour lequel vous souhaitez vous déclasser.</td>
</tr>
<tr>
<td><input name="stype" type="radio" value="on" /></td>
<td><span class="ContactForm_TextField bold">Frais non-payés</span> - Votre cotisation actuelle n'est pas payée. Utilisez votre <a href="http://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">Compte Club</a> pour sélectionner et payer les frais pour le pack d'abonnement que vous préférez.</td>
</tr>
</tbody>
</table>
</form></div>
</div>
<!--</div>
-->
<div class="six columns">
<div class="panel">
<div style="width: 100%;">
<p class="royal style24">Sélectionnez l'abonnement GPS auquel vous souhaitez être déclassé:</p>
<form enctype="application/x-www-form-urlencoded" method="get">
<table>
<tbody>
<tr>
<td><input name="stype" type="radio" value="on" /></td>
<td><span class="ContactForm_TextField bold">PREMIER</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" value="on" /></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span></td>
</tr>
<tr>
<td><input name="stype" type="radio" value="on" /></td>
<td><span class="ContactForm_TextField bold">PRO</span></td>
</tr>
</tbody>
</table>
<p style="border-bottom: 2px dotted gray"> </p>
<p align="center"><input type="submit" value="Postuler" /></p>
<input name="_action" type="hidden" value="change" /></form></div>
</div>
</div>
</div>
<!--
<div class="row">
<div class="twelve columns">
<div class="panel">
<div style="width: 100%;">
<h5 class="royal">CHANGER L'ABONNEMENT POUR:</h5>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<form action="/cgi/vip/gps_modification.cgi" method="get"> 
<table>
<tr>
<td><input type="radio" value="43" name="stype"></td>
<td><span class="ContactForm_TextField bold">PRO PLUS</span> (Disponible seulement pour les abonnés qui paient par carte de crédit/débit ou pour les managers de Bronze et plus (basé sur votre réalisation de niveau de rémunération actuel))</td>
</tr>
<tr>
<td><input type="radio" value="45" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER</span> (Disponible seulement pour les abonnés qui paient par carte de crédit/débit ou pour les managers de Ruby et plus (basé sur votre réalisation de niveau de rémunération actuel))</td>
</tr>
<tr>
<td><input type="radio" value="47" name="stype"></td>
<td><span class="ContactForm_TextField bold">PREMIER PLUS</span> (Disponible seulement pour les abonnés qui paient par carte de crédit/débit ou pour les Directeurs executifs et plus (basé sur votre réalisation de niveau de rémunération actuel))</td>
</tr>
</table>
<p style="border-bottom: 2px dotted gray" mce_style="border-bottom: 2px dotted gray"> </p>
<p>Note: Si vous payez par carte de crédit/débit et qur le paiement est refusé, votre abonnement sera remis au niveau PRO. A la réception de votre cotisation PRO en Juin, vous pourrez ensuite mettre à jour votre abonnement et payer la différence entre la cotisation PRO et la cotisation due pour celui mis à jour. Une fois que nous aurons reçu les fonds supplémentaires, vos Affiliates seront transférés sous vous dès que possible et dans les 30 jours suivant la réception de vos frais de mise à niveau.</p>
<p align="center"><input type="submit" value="Apply" /></p>
<input type="hidden" name="_action" value="change" /></form></div>
</div>
</div>
</div>
--><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>
<!--Footer Container -->
<div class="container blue">
<div class="row ">
<div class="twelve columns">
<div class="push"></div>
</div>
</div>
<div class="row ">
<div class="twelve columns centered">
<div id="footer">
<p>Copyright © 1997-2012 <!-- Get Current Year -->ClubShop Rewards, Tous droits réservés.</p>
</div>
</div>
</div>
<!--End Footer Container --></div>
</body>
</html>