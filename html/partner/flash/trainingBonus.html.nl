﻿
<!DOCTYPE html>
 
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->                    
<!-- [if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif] -->                    
<!-- [if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif] -->
<!-- [if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif] -->
<!-- [if gt IE 8]> <!-- --> 
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<!--<![endif]-->
 
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
     
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />
     
    <title>Training Bonus Slides</title>
     
    <!-- Included CSS Files -->
	
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css"/>
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css"/>
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css"/>    
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/anythingslider.css" /> 
     
    <!-- [if lt IE 9] >
        <link rel="stylesheet" href="http://www.clubshop.com/css/partner/ie.css">
    <![endif] -->
 
     
    <!-- IE Fix for HTML5 Tags -->
    <!-- [if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif] -->
	<!-- Older IE stylesheet, to reposition navigation arrows, added AFTER the theme stylesheet -->
	<!--[if lte IE 7]>
	<link rel="stylesheet" href="http://www.clubshop.com/css/anythingslider-ie.css" type="text/css" media="screen" />
	<![endif]-->
 
    <!-- Included JS Files -->
    <script src="http://clubshop.com/js/jquery/1.7.1/jquery.min.js"></script>    
    <script src="http://clubshop.com/js/partner/foundation.js"></script>    
    <script src="http://clubshop.com/js/partner/app.js"></script>   
	<script src="http://clubshop.com/js/jquery.anythingslider.js"></script>
	
	<script>
	$(function () {
		$('#slider1').anythingSlider({
//			theme : 'metallic',
			expand       : true,
			autoPlay     : true
		});

		

	});
	</script>
	
   <style>
		#wrapper1 {
    height: 450px;
    margin: 0 auto;
    width: 100%;
	}
	.quoteSlide blockquote {
    color: #444444;
    font: italic 24px;
    margin: 0 0 10px;
    text-align: center;
	}
	page.css (line 64)
	blockquote {
		margin-left: 30px;
	}
.quoteSlide {
    padding: 100px 50px 50px 50px;
	text-align:center;
}

</style>
   
   
   
</head>
<body class="blue">
 
<!-- Top Blue Header Container -->
    <div class=" container blue ">      
        <div class=" row ">     
            <div class=" six columns ">             
                <a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" style=" margin:5px; " height=" 84px " width=" 288px "></a>
            </div>
             
            <div class=" six columns ">             
                <!--Extra Space Future Space -->                
            </div>          
        </div>      
    </div>  
<!--End Top Blue Header Container -->   
 
<!--Main Container -->
    <div class=" container white">           
        <div class=" row ">
            <div class=" twelve columns "> 
			
			
			<hr/>
			
               <div id="wrapper1">
					<ul id="slider1">
						<li class="panel1">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P1.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">	
												<h4>Dit is Partner &#34;A&#34;</h4>
											</div>
										</div>							
									</div>										
								</div>								
							</div>							
						</li>

						<li class="panel2">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P2.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Partner &#34;A&#34; is een Team Captain</h4>
											</div>
										</div>							
									</div>										
								</div>								
							</div>		
						</li>

						<li class="panel3">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P3.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Partner &#34;A&#34; is aangesloten door Partner &#34;A&#34; die Team Player is</h4>
											</div>
										</div>							
									</div>										
								</div>								
							</div>	
									
						</li>

						<li class="panel4">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P3.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Omdat Partner &#34;A&#34; geen hoger pay level heeft dan Partner &#34;A&#34; heeft deze geen recht op het verdienen van een Training Bonus op Partner &#34;A&#34;</h4>
												
											</div>
										</div>							
									</div>										
								</div>								
							</div>		
						</li>

						<li class="panel5">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P4.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Partner &#34;A&#34; was aangesloten door &#34;A&#34; die Bronze Manager is</h4>
												
											</div>
										</div>							
									</div>										
								</div>								
							</div>			
						</li>
						
						<li class="panel6">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P4.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Omdat Partner &#34;A&#34; een hoger pay level heeft dan Partner &#34;A&#34; heeft deze recht op het verdienen van de Training Bonus op Partner &#34;A&#34;</h4>
											</div>
										</div>							
									</div>										
								</div>								
							</div>			
						</li>
						
						<li class="panel7">
							<div class=" row ">
								<div class=" twelve columns ">
									<div class=" row ">									
										<div class=" six columns ">										
								
											<img src="trainBonus/P4.jpg" />
								
										</div>	
										<div class=" six columns ">
											<div class="quoteSlide">
												<h4>Partner &#34;C&#34;is Bronze Manager. Omdat Partner &#34;C&#34; een hoger pay level heeft dan Partner &#34;A&#34; heeft deze recht op het verdienen van de Training Bonus op Partner &#34;A&#34;
Welke gelijk is aan 1/2 van Partner &#34;A&#34;'s Referral Commissies en Partner C wordt beschouwd als Team Coach van Partner &#34;A's&#34;</h4>
											</div>
										</div>							
									</div>										
								</div>								
							</div>			
						</li>
						

					</ul>
				</div>

			<hr/>	
            </div>
        </div>  
    </div>  <!-- container -->
<!--End Main Container -->  
 
<!--Footer Container -->
    <div class=" container blue ">      
        <div class=" row "><div class="twelve columns"><div class=" push "></div></div></div>   
        <div class=" row ">
            <div class=" six columns centered">          
                <div id=" footer ">
                    <p >Copyright © 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>
                         
                         ClubShop Rewards, All Rights Reserved.
                    </p>
                </div>              
            </div>
        </div>  
    </div>  <!-- container -->
<!--End Footer Container -->
 
</body>
</html>
 
 
 
    