<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
    
         <html><head>
            <title>Pay Agent List</title>


     <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="//www.clubshop.com/css/partner/general.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="//www.clubshop.com/js/partner/foundation.js"></script>
    <script src="//www.clubshop.com/js/partner/app.js"></script>
    <script src="//www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"></h4>

	<hr />
	
	<span class="style28"><xsl:value-of select="//lang_blocks/p1"/></span>

	<hr/>

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">



<p><xsl:value-of select="//lang_blocks/p3"/></p>
<div align="center">
<span class="style24"><xsl:value-of select="//lang_blocks/p2"/></span>
</div>
<br/><br/>
<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr align="center">
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p4"/></b></td>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p5"/></b></td>
    <td bgcolor="#FFFFFF"><b>Additional Transfer Fee Authorized</b></td>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p6"/></b></td>
    <td bgcolor="#FFFFFF"><b><xsl:value-of select="//lang_blocks/p7"/></b></td>
 
  </tr>
  
  
       
  
  
   
     
     
  
<tr>
    <td bgcolor="#FFFFFF">Algeria</td>
    <td bgcolor="#FFFFFF">Algerian Dirhams (DZD)</td>
    <td bgcolor="#FFFFFF">50%</td>
    <td bgcolor="#FFFFFF"><b>Oussama Mouici</b></td>
    <td bgcolor="#FFFFFF">oussamaclubshop@gmail.com</td>
 
  </tr> 


<tr>
    <td bgcolor="#FFFFFF">Algeria</td>
    <td bgcolor="#FFFFFF">Algerian Dirhams (DZD)</td>
    <td bgcolor="#FFFFFF">50%</td>
    <td bgcolor="#FFFFFF"><b>Sedik Bounab</b></td>
    <td bgcolor="#FFFFFF">bounabsedik31@gmail.com</td>
 <!--<a href="pay_agent_dz.html" onClick="window.open(this.href,'help', 'width=500,height=500,scrollbars'); return false;"><img src="https://www.clubshop.com/mall/images/icons/icon_disclaimer.png" border="0"/></a>-->
  </tr>

<!--<tr>
    <td bgcolor="#FFFFFF">Algeria</td>
    <td bgcolor="#FFFFFF">Algerian Dirhams (DZD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Oussama Mouici</b></td>
    <td bgcolor="#FFFFFF">oussamaclubshop@gmail.com</td>
 
  </tr> -->

<tr>
    <td bgcolor="#FFFFFF">Burkina Faso</td>
    <td bgcolor="#FFFFFF">Africaine Francs BCEAO (XOF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Chad</td>
    <td bgcolor="#FFFFFF">Communaute Financiere Africaine Francs (XAF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Egypt</td>
    <td bgcolor="#FFFFFF">EGP - Egypt Pounds (EGP)</td>
    <td bgcolor="#FFFFFF">15%</td>
    <td bgcolor="#FFFFFF"><b>Ahmed Elsherif</b></td>
    <td bgcolor="#FFFFFF">ahservice11@gmail.com</td>
 
  </tr>



<tr>
    <td bgcolor="#FFFFFF">Ivory Coast</td>
    <td bgcolor="#FFFFFF">Africaine Francs BCEAO (XOF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>





<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Najib Kounsasse</b></td>
    <td bgcolor="#FFFFFF">najib.kounsasse@gmail.com </td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Amina Soltane</b></td>
    <td bgcolor="#FFFFFF">minabhm@gmail.com</td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Youssef Rerhrhaye</b></td>
    <td bgcolor="#FFFFFF">youssefrer@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Si Bousmaha Bouchikhi</b></td>
    <td bgcolor="#FFFFFF">businessclick101@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Bouchra El Hassouni</b></td>
    <td bgcolor="#FFFFFF">hassounibouchra@gmail.com</td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Morocco</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Adel Taleb Abderrahmane</b></td>
    <td bgcolor="#FFFFFF">adiltaleb2010@gmail.com</td>
 
  </tr>



<tr>
    <td bgcolor="#FFFFFF">Niger</td>
    <td bgcolor="#FFFFFF">Africaine Francs BCEAO (XOF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>


<!--<tr>
    <td bgcolor="#FFFFFF">Nigeria</td>
    <td bgcolor="#FFFFFF">Nigerian Nairas (NGN)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Ranxell Media</b></td>
    <td bgcolor="#FFFFFF">ranxellmedia@gmail.com </td>
 
  </tr>-->


<tr>
    <td bgcolor="#FFFFFF">Senegal</td>
    <td bgcolor="#FFFFFF">Africaine Francs BCEAO (XOF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Togo</td>
    <td bgcolor="#FFFFFF">Africaine Francs BCEAO (XOF)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Puebie Francis Bationo</b></td>
    <td bgcolor="#FFFFFF">lefilsbat@gmail.com</td>
 
  </tr>



<tr>
    <td bgcolor="#FFFFFF">Tunisia</td>
    <td bgcolor="#FFFFFF">Tunisia Dinars (TND)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Thouraya Shili</b></td>
    <td bgcolor="#FFFFFF">topbusiness.ts@live.fr</td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Tunisia</td>
    <td bgcolor="#FFFFFF">Tunisia Dinars (TND)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Bouzid Hassan</b></td>
    <td bgcolor="#FFFFFF">bouzidcontact@gmail.com</td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Tunisia</td>
    <td bgcolor="#FFFFFF">Tunisia Dinars (TND)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Wael Salem</b></td>
    <td bgcolor="#FFFFFF">waelsalem98@gmail.com</td>
 
  </tr>



<tr>
    <td bgcolor="#FFFFFF">Western Sahara</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Najib Kounsasse</b></td>
    <td bgcolor="#FFFFFF">najib.kounsasse@gmail.com </td>
 
  </tr>

<!--<tr>
    <td bgcolor="#FFFFFF">Western Sahara</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Amina Soltane</b></td>
    <td bgcolor="#FFFFFF">minabhm@gmail.com</td>
 
  </tr>

<tr>
    <td bgcolor="#FFFFFF">Western Sahara</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Youssef Rerhrhaye</b></td>
    <td bgcolor="#FFFFFF">youssefrer@gmail.com</td>
 
  </tr>


<tr>
    <td bgcolor="#FFFFFF">Western Sahara</td>
    <td bgcolor="#FFFFFF">Moroccan Dirhams (MAD)</td>
    <td bgcolor="#FFFFFF">5%</td>
    <td bgcolor="#FFFFFF"><b>Si Bousmaha Bouchikhi</b></td>
    <td bgcolor="#FFFFFF">businessclick101@gmail.com</td>
 
  </tr>-->
  
  
  
  
   
  </table>
  <br/><br/>
  
<hr align="left" width="100%" size="1" color="#A35912"/>
  <br/>
  
  <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">

<tr align="center">
    <td bgcolor="#FFFFFF"><a href="/pay_agent_list.xml" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p8"/></a></td>
    <td bgcolor="#FFFFFF"><a href="https://www.clubshop.com/cgi/comm_prefs.cgi" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p9"/></a></td>
    <td bgcolor="#FFFFFF"><a href="/vip_pay_approval.xsp" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p10"/></a></td>
    <td bgcolor="#FFFFFF"><a href="/pay_agent_app.xml" target="_blank" class="nav_footer_brown"><xsl:value-of select="//lang_blocks/p11"/></a></td>
 
  </tr>
  
  </table>

<!-- end page text here-->



<br/><br/><br/><br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p style="text-align:center;">Copyright &#xA9; 1997-
                     
                        <!-- Get Current Year -->
                        <script type=" text/javascript ">
                            var dteNow = new Date();
                            var intYear = dteNow.getFullYear();
                            document.write(intYear);
                        </script>

                         
                         Proprofit Worldwide Ltd., All Rights Reserved.
                    </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>