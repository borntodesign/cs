<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>

<xsl:template match="/">	

<html>
<head>
<base href="https://www.clubshop.com"/>

<title><xsl:value-of select="//lang_blocks/title" /></title>

<link rel="stylesheet" href="/css/memberapp_default.css" type="text/css" />
<!--link rel="stylesheet" href="/css/appx.upg.css" type="text/css" /-->
<style type="text/css">
a.albl {
	font-weight: normal;
	text-transform: uppercase;
}
</style>
</head>
<body style="font-size: 80%; font-family:sans-serif">
<!-- we want to provide some additional emphasis in this section that isn't available as an ordinary 'blurb' -->
<p><a name="manual"></a><xsl:value-of select="//manual" /><br />
<b><xsl:value-of select="//manual2" /></b></p>
<div class="note"><a href="javascript:void(0)" onclick="self.close()"><xsl:value-of select="//lang_blocks/close_window" /></a></div>

<xsl:for-each select="//lang_blocks/blurbs/*">
<xsl:variable name="nname"><xsl:value-of select="name()"/></xsl:variable>
<p><a class="albl"><xsl:attribute name="name"><xsl:value-of select="$nname"/></xsl:attribute><xsl:value-of select="//lang_blocks/labels/child::*[name() = $nname]"/></a>
-
<xsl:value-of select="." /></p>
<!-- xsl:if test="(position() mod 2) != 1"-->
<div class="note"><a href="javascript:void(0)" onclick="self.close()"><xsl:value-of select="//lang_blocks/close_window" /></a></div>
<!-- /xsl:if-->
</xsl:for-each>

<!-- since we want to provide a link to the images of cards, we will stick this one on the end -->
<p><a class="albl" name="card_code"><xsl:value-of select="//lang_blocks/labels/card_code" /></a>
-
<xsl:value-of select="//lang_blocks/card_code" />
<br />
<a href="/help/cvv2.html"><xsl:value-of select="//lang_blocks/card_code_examples" /></a>
</p>
<div class="note"><a href="javascript:void(0)" onclick="self.close()"><xsl:value-of select="//lang_blocks/close_window" /></a></div>

</body>
</html>
</xsl:template></xsl:stylesheet>
