<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Discount Home Shoppers Club - CVV2 Credit Card Number Definition</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
a{text-decoration:none}
body, td
{
	font-family: Verdana, Arial, sans-serif;
	font-size: 10px;
	color: #000;
}

a:hover {
	text-decoration: underline;
	color: #0033CC;
}

h3{
	font-family: Verdana, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #000;
}
</style>
</head>

<body bgcolor="#ffffff">
<table align="center" border="0" width="402">
<tbody>
<tr>
<td height="116">
<div align="center">
<h3><strong>Waar staat het CVV Nummer?</strong></h3>
<table border="0" cellpadding="0" width="90%">
<tbody>
<tr>
<td width="284"><img alt="Visa Verification" height="230" src="https://www.clubshop.com/images/verify-visa.gif" width="284" /></td>
<td width="307"><img alt="Mastercard Verification" height="233" src="https://www.clubshop.com/images/verify-mastercard.gif" width="307" /></td>
</tr>
<tr>
<td colspan="2">
<p><span>Dit nummer staat geprint op uw MasterCard &amp; Visa cards in het handtekening veld of op de achterkant van de kaart. (het zijn de laatste 3 cijfers ACHTER het credit card nummer                  in het handtekeningveld van de kaart). </span></p>
</td>
</tr>
<tr>
<td colspan="2" height="18"></td>
</tr>
<tr>
<td colspan="2" height="180" valign="top">
<div align="center"><img alt="American Express Verification" height="180" src="/images/verify-amex.gif" width="259" /></div>
</td>
</tr>
<tr>
<td colspan="2">
<p><span>U kunt uw vier-cijferig kaart verrificatie nummer vinden op de voorkant van uw American Express credit                  card boven het credit card nummer aan de rechter kant of aan de linkerkant van uw credit card.</span></p>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td valign="top"><span style="font-family: Verdana,Arial,Helvetica,sans-serif;">
<p><span style="font-size: x-small;"><strong>Meer informatie:</strong></span></p>
</span>
<p><span><strong>Wat is het CVV Nummer?</strong><br /> CARD VERRIFICATIE VALUE CODE (CVV)</span></p>
<p><span>CVV is een nieuwe verrificatie procedure ingesteld door          credit card bedrijven om de kans op fraude verder te verkleinen bij internet          transacties. De kaarthouder voert het CVV nummer in tijdens de transactie ter controle dat deze de kaart bij de hand heeft. De CVV code is beveiligingsfunctie voor "kaart niet aanwezig" transacties (bijv., internet transacties), en staat nu op de meeste (maar niet op alle) grote     credit en debit kaarten. Deze nieuwe functie is een drie- of viercijferige code          welke een controle geeft van de informatie in reliëf op de kaart. Hierdoor maakt de CCV code geen deel uit van het kaartnummer.</span></p>
<p><span>De CVV code helpt vast te stellen dat de klant die de bestelling plaatst daadwerkelijk beschikt over de credit/debit kaart en dat de kaart account legitiem is. Elke credit kaart bedrijf heeft een eigen naam voor de CVV code, maar het werkt allemaal hetzelfde voor alle grote kaart types. (VISA refereert aan de code als CVV2, MasterCard noemt het CVC2 en American Express noemt het CID.)</span></p>
<p><span>De achterkant van de meeste Visa/MasterCard kaarten bevat het volledige 16-cijferifg account nummer, gevolgd door de CVV/CVC code. Sommige banken echter, zetten er alleen de laatste vier cijfers van het account nummer op, gevolgd door code. Om te helpen bij het ​​voorkomen van frauduleuze gebruik van creditcards, is het nu verplicht om de 3 of 4 cijferige code op de achterkant van uw credit kaart te gebruiken. Wanneer u uw credit kaart informatie inzend, zijn uw gegevens beveiligd door Secure Socket Layer (SSL) technology gecertificeerd door een digitaal certificaat uit Thawte Server CA.</span></p>
</td>
</tr>
</tbody>
</table>
<p align="center"><span style="font-family: Arial,Helvetica,sans-serif;"><span><a href="javascript:self.close();">Sluit dit venster</a></span></span></p>
</body>
</html>