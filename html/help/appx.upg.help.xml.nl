<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="appx.upg.help.xsl" ?>
<lang_blocks><title>Partner Upgrade Help</title><close_window>Sluit venster</close_window><manual>Omdat deze methode van betaling manueel verwerkt moet worden, zullen wij niet in staat zijn uw aanvraag goed te keuren tijdens het weekend, dat zal gebeuren de eerstvolgende werkdag. Van zodra dat gebeurd is ontvangt u van ons een e-mail ter bevestiging.</manual>
<manual2>Belangrijk: U zal niet goedgekeurd worden als Partner zolang de betaling niet ontvangen is.</manual2><blurbs><joint_applicant>Een joint applicant kan een echtgeno(o)t(e) zijn, levenspartner of business partner. Wees er van op de hoogte dat het verwijderen van de door u gekozen applicant, ook zijn of haar goedkeuring zal vereist zijn.</joint_applicant><language_pref>Onze vertaaldienst doet zijn uiterste best om ons in staat te stellen de e-mails die aan u gestuurd worden, te versturen in de door u gekozen taal.</language_pref><alt_email>Gelieve ook een alternatief e-mailadres te overhandigen zodat wij u toch kunnen bereiken mocht uw primair e-mail adres niet toegankelijk zijn.</alt_email><company_name>Een bedrijfsnaam dient enkel ingegeven te worden indien de te ontvangen commissie dient uitbetaald te worden op meer dan de naam en de voornaam.</company_name><state>Indien de provincie waarin u woont dient vermeld te worden voor het ontvangen van post, vergeet dan niet die te vermelden.</state><tin>Vereist voor US belastingbetalers, gebruik ofwel SSN, EIN of TIN.</tin><tel>Telefoonnummer voorzien van land en regio code.</tel></blurbs>
<card_code>Indien uw kredietkaart een CVV code heeft, dan dient u deze in te vullen om uw bewerking te vervolledigen.</card_code><card_code_examples>Kaart code voorbeelden</card_code_examples>
  <labels>
    <_password description="A simple field label for profile updating">Wachtwoord</_password>
    <acct_holder>Naam op kaart</acct_holder>
    <address1>Adres</address1>
    <address2>Adres vervolg</address2>
    <ag_name>Naam non-profit organisatie</ag_name>
    <agree>Overeenkomst</agree>
    <alias>ID</alias>
    <card_code>Kaartcode</card_code>
    <cbid>ClubShop Rewards ID</cbid>
    <cc_exp_date>Verval datum</cc_exp_date>
    <cc_exp_month>Verval maand</cc_exp_month>
    <cc_exp_year>Verval jaar</cc_exp_year>
    <cc_number>Kaartnummer</cc_number>
    <cellphone>mobiel telefoonnummer</cellphone>
    <city>Woonplaats</city>
    <company_name>Bedrijfsnaam</company_name>
    <contact_emailaddress>E-mail adres van contactpersoon</contact_emailaddress>
    <contact_firstname>Voornaam van uw contact persoon</contact_firstname>
    <contact_lastname>Achternaam van uw contactpersoon</contact_lastname>
    <country>Land</country>
    <email_check>Bevestig het e-mail adres</email_check>
    <email_pref>E-mail voorkeur</email_pref>
    <emailaddress>E-mail adres</emailaddress>
    <alt_email>Alternatief e-mail adres</alt_email>
    <fax_num>Fax nummer</fax_num>
    <firstname>Voornaam</firstname>
    <how_find>Hoe heeft u ons gevonden?</how_find>
    <language_pref>Taal</language_pref>
    <lastname>Achternaam</lastname>
    <login_id>Vul uw ID nummer of ClubShop Rewards kaartnummer in</login_id>
    <mdname>Tweede naam/ initialen</mdname>
    <membertype>Type lidmaatschap</membertype>
    <mop_address>Facturatie adres</mop_address>
    <mop_city>Facturatie stad</mop_city>
    <mop_country>Facturatie land</mop_country>
    <mop_postal_code>Facturatie postcode</mop_postal_code>
    <mop_state>Facturatie provincie</mop_state>
    <oid>Uniek nummer</oid>
    <other_contact_info description="Right now we are using this for skype name, but in the future, I'm sure it will be expanded in scope.">Uw Skype naam</other_contact_info>
    <password>Creëer wachtwoord</password>
    <password_check description="A simple field label for profile updating to verify one`s password">Bevestig wachtwoord</password_check>
    <password_note>Alleen letters en cijfers</password_note>
    <tel>Telefoonnummer</tel>
    <phone_note>Land en regio nummer(BVB: +31 en 056 of 03 enz...)</phone_note>
    <postalcode>Postcode</postalcode>
    <prefix>Met vriendelijke groet</prefix>
    <redurl>Redirect URL</redurl>
    <secfirstname>Voornaam</secfirstname>
    <seclastname>Achternaam</seclastname>
    <secmdname>Tweede naam/ initialen</secmdname>
    <secprefix>Met vriendelijke groet,</secprefix>
    <secsuffix>Suffix</secsuffix>
    <sponsorID>ID van de verwijzer</sponsorID>
    <state>Provincie</state>
    <subscription_type>Subscription Type</subscription_type>
    <suffix>Suffix</suffix>
    <temp_alias>Tijdelijk ID nummer</temp_alias>
    <temp_password>Tijdelijk wachtwoord</temp_password>
    <terms_agree>Ik heb de algemene voorwaarden gelezen en verklaar mij akkoord</terms_agree>
    <tin>BTW identificatienummer</tin>
    <url>Website</url>
    <joint_applicant>Mede inschrijver</joint_applicant>
  </labels>

</lang_blocks>
