<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Untitled document</title>
</head>
<body>
<table align="center" border="0" width="402">
<tbody>
<tr>
<td height="116">
<div align="center">
<h3><strong>¿Donde està el nùmero de seguridad CVV ?</strong></h3>
<table border="0" cellpadding="0" width="90%">
<tbody>
<tr>
<td width="284"><img alt="Visa Verification" height="230" src="https://www.clubshop.com/images/verify-visa.gif" width="284" /></td>
<td width="307"><img alt="Mastercard Verification" height="233" src="https://www.clubshop.com/images/verify-mastercard.gif" width="307" /></td>
</tr>
<tr>
<td colspan="2">
<p><span>Este nùmero està impreso en tu tarjeta de crèdito Master y Visa en la parte de atràs (son los ùltimos 3 dìgitos DESPUÈS del nùmero de la tarjeta en el àrea de la firma. </span></p>
</td>
</tr>
<tr>
<td colspan="2" height="18"></td>
</tr>
<tr>
<td colspan="2" height="180" valign="top">
<div align="center"><img alt="American Express Verification" height="180" src="/images/verify-amex.gif" width="259" /></div>
</td>
</tr>
<tr>
<td colspan="2">
<p><span>Puedes encontrar el nùmero de 4 dìgitos en el frente de tu tarjeta </span>American Express, encima del nùmero de la tarjeta de crèdito en la derecha o a la izquierda.</p>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td valign="top"><span style="font-family: Verdana, Arial, Helvetica, sans-serif; ">
<p><span style="font-size: x-small; "><strong>Màs Informaciòn:</strong></span></p>
</span>
<p><span><strong>Que es el nùmero CVV?</strong><br />Es una medida de SEGURIDAD ANTIFRAUDE (CVV)</span></p>
<p><span>El còdigo CVV es un procedimiento nuevo que las compañìas de las tarjetas de crèdito han establecido para reducir los fraudes por las compras en Internet. Cada vez que se realiza una transacciòn online se requiere de introducir el nùmero CVV como demostraciòn que te encuentras en poseciòn de la tarjeta de crèdito. El còdigo CVV es una caracterìstica de seguridad de 3-4 dìgitos que aparece en la mayorìa (no todas) de las tarjetas de crèdito/dèbito y proporciona una verificaciòn criptogràfica de la informaciòn grabada en la tarjeta. Por lo tanto el còdigo CVV no forma parte del nùmero de la tarjeta en si. </span></p>
<p><span>El còdigo CVV ayuda a determinar que el cliente realiza el pedido en realidad, posee la tarjeta de crèdito/dèbito y que la cuenta de la tarjeta es legìtima. Cada compañìa de tarjetas de crèdito tiene su proprio nombre de còdigo CVV, pero la funcionalidad es siempre la misma (VISA el còdigo se llama CVV2, Master Cards CVC2 y American Express CID)</span></p>
<p><span>El panel trasero de la mayorìa de las tarjetas Visa/MasterCard contiene un total de 16 dìgitos de nùmero de cuenta seguidos por el còdigo CVV/CVC. Sin embargo algunos bancos muestran solo 4 dìgitos seguidos por el còdigo. Para ayudarnos a prevenir el utilizo fraudulento de la tarjeta de crèdito, requerimos la introducciòn de los 3-4 dìgitos del còdigo de seguridad en el reverso de tu tarjeta. Al enviar la informaciòn de tu tarjeta de crèdito, tus datos estàn protegidos por Secure          Socket Layer (SSL) certificado digital tecnològico del servidor Thawte Server CA.</span></p>
</td>
</tr>
</tbody>
</table>
<p align="center"><span style="font-family: Arial, Helvetica, sans-serif; "><span><a href="javascript:self.close();">Close this Window</a></span></span></p>
</body>
</html>