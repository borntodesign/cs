<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>


<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>




<!--<style type="text/css">
p,li{font-family: Verdana, Arial, sans-serif;
font-size: 12px;
}
h5{font-family: Verdana, Arial, sans-serif;
font-size: 1em;
}






td.back{background-image:url(/images/bg_header.jpg);}

.Green_Large_Bold_head {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #34420F;
	font-weight:bold;
}


.Green_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-transform:uppercase;
	color: #485B15;
	font-weight:bold;
}
.footer_divider {
	font-size: 12px;
	text-transform:uppercase;
	font-weight: bold;
	color: #41712B;
}
.Gold_Large_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FDB100;
	font-weight:bold;
}
.Green_Bold {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	text-transform:uppercase;
	letter-spacing:0.1em;
	color: #779724;
	font-weight:bold;
}


</style>-->



</head>
<body>
	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
 







<p class="style24"><xsl:value-of select="//lang_blocks/p44"/></p>


<ul>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember1"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember2"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember3"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember4"/></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember5"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/manual/compensation/member.html" target="_blank">http://www.clubshop.com/manual/compensation/member.html</a></li>
<li class="blue_bullet"><xsl:copy-of select="//lang_blocks/pmember6"/></li>

</ul>









<p class="style24"><b><xsl:value-of select="//lang_blocks/p11"/></b></p>

<p><xsl:copy-of select="//lang_blocks/p12/*|//lang_blocks/p12/text()"/></p>

<p><xsl:copy-of select="//lang_blocks/p13/*|//lang_blocks/p13/text()"/></p>


<br/>

<p class="style24"><b><xsl:value-of select="//lang_blocks/p14"/></b></p>

<p><xsl:copy-of select="//lang_blocks/p15/*|//lang_blocks/p15/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p16/*|//lang_blocks/p16/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p17/*|//lang_blocks/p17/text()"/></p>

<br/>

<p class="style24"><b><xsl:value-of select="//lang_blocks/p18"/></b></p>

<ul>
<li><a href="http://www.clubshop.com/manual/compensation/member.html" target="_blank"><xsl:value-of select="//lang_blocks/p19ap"/></a></li>

</ul>

<br/>
<p class="style24"><b><xsl:value-of select="//lang_blocks/p24ap"/></b></p>


<ul>
<li><xsl:value-of select="//lang_blocks/p31ap"/><xsl:text> </xsl:text><a href="http://www.clubshop.com/mall/tools/tools.html" target="_blank"><xsl:value-of select="//lang_blocks/p42"/></a></li>
<br/>
<li><xsl:value-of select="//lang_blocks/p32ap"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/outletsp/bcenter.html" target="_blank"><xsl:value-of select="//lang_blocks/p43"/></a></li>
<br/>
<li><xsl:value-of select="//lang_blocks/p33ap"/><xsl:text> </xsl:text><a href="https://www.clubshop.com/outletsp/bcenter.html" target="_blank"><xsl:value-of select="//lang_blocks/p43"/></a></li>
</ul>


<p><xsl:copy-of select="//lang_blocks/p22ap/*|//lang_blocks/p22ap/text()"/></p>
<p><xsl:copy-of select="//lang_blocks/p23apa/*|//lang_blocks/p23apa/text()"/><xsl:text> </xsl:text><a href="http://www.glocalincome.com/cgi/joinpool.cgi" target="_blank"><xsl:value-of select="//lang_blocks/p23ap1"/></a><br/>
<xsl:value-of select="//lang_blocks/p23ap2"/><xsl:text> </xsl:text><a href="https://www.glocalincome.com/cgi/appx.cgi/0/upg" target="_blank"><xsl:value-of select="//lang_blocks/p23ap3"/></a>
</p>









<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
