<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 






   <xsl:template match="/">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
         <html><head>
            <title><xsl:value-of select="//lang_blocks/p0"/></title>
<link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/foundation.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/app.css" />
    <link rel="stylesheet" type="text/css" href="http://www.clubshop.com/css/partner/general.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="http://www.clubshop.com/js/partner/foundation.js"></script>
    <script src="http://www.clubshop.com/js/partner/app.js"></script>
    <script src="http://www.clubshop.com/js/partner/flash.js"></script>
    <!--<script src="http://www.clubshop.com/js/panel.js"></script>-->



<link href="/css/manual_2012.css" rel="stylesheet" type="text/css"/>

</head>
<body>

	<div class="container blue">
	<div class="row">
		<div class="six columns"><a href="#"><img src="http://www.clubshop.com/images/partner/cs_logo.jpg" width="288px" height="84px" style="margin-top:5px;" /></a></div>
		<div class="six columns"></div>

	</div></div>

	<div class="container">
	<div class="row">
	<div class="twelve columns ">
	<br />
	<h4 class="topTitle"><xsl:value-of select="//lang_blocks/p0"/></h4>
	<br />
	<hr />

	</div></div></div>
			

      
	<div class="row">
	<div class="twelve columns">
 


<span class="Header_Main"><xsl:value-of select="//lang_blocks/p1"/></span>
<br/>

<br/>

<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p5"/></b></span><br/><xsl:value-of select="//lang_blocks/p6"/><br/><br/>-->
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p7"/></b></span><br/><xsl:value-of select="//lang_blocks/p8"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p9"/></b></span><br/><xsl:value-of select="//lang_blocks/p10"/><br/><br/>
<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p11"/></b></span><br/><xsl:value-of select="//lang_blocks/p12"/><br/><br/>-->
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p13"/></b></span><br/><xsl:value-of select="//lang_blocks/p14"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p15"/></b></span><br/><xsl:value-of select="//lang_blocks/p16"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p17"/></b></span><br/><xsl:value-of select="//lang_blocks/p18"/><br/><br/>
<!--<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p19"/></b></span><br/><xsl:value-of select="//lang_blocks/p20"/><br/><br/>-->
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p21"/></b></span><br/><xsl:value-of select="//lang_blocks/p22"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p23"/></b></span><br/><xsl:value-of select="//lang_blocks/p24"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p25"/></b></span><br/><xsl:value-of select="//lang_blocks/p26"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p27"/></b></span><br/><xsl:value-of select="//lang_blocks/p28"/><br/><br/>
<span class="ContactForm_TextField"><b><xsl:value-of select="//lang_blocks/p29"/></b></span><br/><xsl:value-of select="//lang_blocks/p30"/><br/><br/>



<br/>




<hr align="left" width="100%" size="1" color="#A35912" />

<div align="center"><span class="Gold_Large_Bold"><xsl:value-of select="//lang_blocks/p31"/></span></div>



<br/><br/><br/>
</div>
</div>


<!--Footer Container -->

<div class="container blue"> 
  <div class="row "> 
    <div class="twelve columns"> 
      <div class="push"></div>
    </div>

  </div>
  <div class="row "> 
    <div class="twelve columns centered"> 
      <div id="footer"> 
        <p>Copyright &#xA9; 1997-2012
          <!-- Get Current Year -->
          
          ClubShop Rewards, All Rights Reserved. </p>
      </div>

    </div>
  </div>

<!--End Footer Container -->

</div>

</body>
</html>
</xsl:template>
</xsl:stylesheet>