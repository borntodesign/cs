<?php

$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
$filename = 'select.xsp.en';
if (file_exists('select.xsp.' . $lang)) {
    $filename = 'select.xsp.' . $lang;
}
else {
    $lang = 'en'; // the default filename is already set
}

$xml = "<root><xsl>$filename</xsl><lang>$lang</lang>" . PHP_EOL;

$xsp = file_get_contents($filename);
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);

$xsp = file_get_contents('banners.xml');
$xml .= preg_replace('/<\?.*?\?>/ms', '', $xsp);

$auth_custom_generic = isset($_COOKIE['AuthCustom_Generic']) ? $_COOKIE['AuthCustom_Generic'] : '';
$xml .= file_get_contents("http://192.168.250.1/cgi/rpc.cgi?proc=memberinfo_by_ck;Cookie={$auth_custom_generic}");
$xml .= '</root>';
//var_dump($xml);
$xslDD = new DOMDocument();
$xslDD->load('banners.xsl');
$processor = new XsltProcessor();
$processor->importStyleSheet($xslDD);

$xmlDD = new DOMDocument();
$xmlDD->loadXML($xml);

$xml = $processor->transformToDoc($xmlDD);
echo $xml->saveHTML();
?>