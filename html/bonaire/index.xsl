
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.clubshop.com/DTD/xhtml1-transitional.dtd"
    omit-xml-declaration="yes"
    media-type="text/html"/> 


   <xsl:template match="/">
    
         <html><head>
            <title>Bonaire Sign-Up</title>
            
            <meta name="keywords">
            <xsl:attribute name="content">
            <xsl:value-of select="//lang_blocks/tags"/>
            </xsl:attribute>
            </meta>
      

<style type="text/css">
body {background-color: #050C56;
}
td#main{background-color: #FFFFFF;
border: thin solid #006699;
font-family: Verdana, Arial, sans-serif;
font-size: small;
valign:top;

}

td#sidelinks{ }

hr{color: #006699;
}

h2 {font-family: Arial, Verdana, sans-serif;
font-size: 15px;
color: #006699;
}

p {font-family: Verdana, Arial, sans-serif;
font-size: small;
}

table.b{font-family: Verdana, Arial, sans-serif;
font-size: small;
text-align: center;
align: center;
width: 100%;

}
</style>

</head>
<body>
<div align="center">
<img src="/bonaire/images/bonaire_top_en.gif" height="80" width="770" alt="Bonaire Discount and Rewards"/>
</div>


<table><tr>
<td id="sidelinks">
<img src="/bonaire/images/cur_highlights.gif" height="798" width="100" alt="curacao"/>

</td>

<td id="main" valign="top">
<p><xsl:value-of select="//lang_blocks/p3"/><xsl:text> </xsl:text> <b><xsl:value-of select="//lang_blocks/p4"/></b><xsl:text> </xsl:text> <xsl:value-of select="//lang_blocks/p5"/></p>


<div align="center">
<img src="/bonaire/images/cr_card.gif" height="148" width="226" alt="ClubRewards Card"/>
</div>

<p><xsl:value-of select="//lang_blocks/p6"/></p>
<p><xsl:value-of select="//lang_blocks/p7"/></p>
<br/>
<div align="center"><img src="/bonaire/images/shopsave.gif" height="32" width="540" alt="Shop and Save"/></div>

<p><xsl:value-of select="//lang_blocks/p9"/><xsl:text> </xsl:text> <a href="/clubrewards/printcard.xml" target="_blank"><xsl:value-of select="//lang_blocks/p10"/></a></p>

<br/>
<hr/>
<table class="b">
<tr>

<td><b><xsl:value-of select="//lang_blocks/p11"/></b></td>
<td></td>
<td><b><xsl:value-of select="//lang_blocks/p12"/></b></td>
</tr>
<tr>
<td><a href="http://www.clubshop.com/" target="_blank"><img src="/bonaire/images/shop_mall.gif" height="100" width="213" border="0" alt="ClubShop Mall"/></a></td>
<td></td>
<td><a href="https://www.clubshop.com/cgi/ma_real_rpt.cgi?country=AN&amp;action=report_by_city&amp;state=BONAIRE" target="_blank"><img src="/bonaire/images/cr_card_sm.gif" height="69" width="100" border="0" alt="ClubRewards Card"/></a>
</td>
</tr>
</table>
<br/>
<br/>
<div align="center"><img src="/bonaire/images/copyright_.gif" height="19" width="173" alt="copyright"/></div>



</td></tr></table>

</body></html>


</xsl:template>
</xsl:stylesheet>