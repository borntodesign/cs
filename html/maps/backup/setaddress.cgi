#!/usr/local/bin/perl
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use LWP::UserAgent;
use URI::Escape;
use XML::Simple;
use Data::Dumper;
use CGI;
use CGI::Cookie;

my $cgi = new CGI;
my $agent = LWP::UserAgent->new;
my $xml = new XML::Simple;

my $def_location = $cgi->cookie('rewards_location');

my $PI = 4*atan2(1,1);    # pi to max digits of the system
my $earth_radius_mi = 3960.0;
my $deg2rad = $PI/180.0;
my $rad2deg = 180/$PI;
my @box_size_mi = (5.0, 10.0, 25.0, 50.0, 100.0);
my $result={};
my $params= {};
my $lat = 0;
my $long = 0;
my $mlat = 0;
my $mlong = 0;
my $qry = "";
my $def_address = "";
#
# Get The form input (using GET) these are currently location, change, check
#
my @formdata = split (/&/, $ENV{'QUERY_STRING'});
foreach  my $pair (@formdata) {
    my ($key, $value) = split(/=/, $pair);
    $value =~ tr/+/ /;
    $value =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
    $params->{$key} = $value;
}
#
# Check if location has our default stuff in it, and if so remove it
#
if ($params->{location}) {
    $params->{location} =~ s/ is your default location//;
    $params->{location} =~ s/ is your current location//;
}

warn Dumper($params);
if ($params->{change}) {
    $def_address = $params->{location};
#
# Convert the location typed into the location box and convert it to longitude/latitude
#
   my $url = 'http://maps.googleapis.com/maps/api/geocode/xml?address=' . $def_address . '&sensor=false';
   my $req = HTTP::Request->new(GET=>$url);
   my $res = $agent->request($req);
   if ($res->is_success) {
       $result = $xml->XMLin($res->content);
#    warn Dumper($result);
       if (ref($result->{result}) eq "HASH") {
           $mlat = $result->{result}->{geometry}->{location}->{lat};
           $mlong = $result->{result}->{geometry}->{location}->{lng};
       }
       elsif (ref($result->{result}) eq "ARRAY") {
           $mlat = $result->{result}[0]->{geometry}->{location}->{lat};
           $mlong = $result->{result}[0]->{geometry}->{location}->{lng};
           next;
       }
   }
#
# If the saved box is checked, we need to save the information into the default cookie otherwise save it to the current
# location cookie
#
  my $locdata = $def_address . " | ". $mlong . " | ". $mlat . " | " . $params->{range};
warn $locdata;
  my $cookie = $cgi->cookie(-name=>"rewards_def_location",-expires=>"+1y", -domain=>"clubshop.com",  -value=>$locdata);
  if ($params->{check}) {
      $cookie = $cgi->cookie(-name=>"rewards_def_location",-expires=>"+1y", -domain=>"clubshop.com",  -value=>$locdata);
  }
  else {
      $cookie = $cgi->cookie(-name=>"rewards_cur_location",-expires=>"+15m",-domain=>"clubshop.com",  -value=>$locdata);
  }
#    print $cgi->trailer();
#
# redirect to the ipaddress page
#    print "Location:http://www.clubshop.com/maps/rewardsdirectory.shtml\n\n";
    print $cgi->redirect(-URL=> "http://www.clubshop.com/maps/rewardsdirectory.shtml", -cookie=>$cookie);
}
