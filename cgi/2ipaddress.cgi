#!/usr/local/bin/perl
#
# This script gets your current location by either ip address, a default address set by you earlier, or if you type
# in an address. Given that address, it locates all merchants within a given diameter (actually a box). From that list,
# all of the categories are collected and placed in a pull down menu.  
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use CGI;
use CGI::Cookie;
use Data::Dumper;

my $PI = 4*atan2(1,1);    # pi to max digits of the system
my $earth_radius_mi = 3960.0;
my $deg2rad = $PI/180.0;
my $rad2deg = 180/$PI;
my $cgi = new CGI;
my @box = ();
my $cookiedata = "";
my $keyword_where = ";";
my $language_code = "en";
my $keyword_join = " ";
my $lng = 0;
my $lat = 0;
my $range = 0.0;
my %categories = {};
#
# Check to see if the default cookie is there
#
my $cgiini = "clubcash_monthly_detail.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
#
# determine if using the default  or current location
#
my @def_address = split ('\|', $cgi->cookie('rewards_def_location'));
my @cur_address = split ('\|', $cgi->cookie('rewards_cur_location'));
#
# We have a current address use it.
#
if (@cur_address) {
    $lng = @cur_address[1];
    $lat = @cur_address[2];
    $range = @cur_address[3];
}
#
# We have a default address use it.
elsif (@def_address) {
    $lng = @def_address[1];
    $lat = @def_address[2];
    $range = @def_address[3];
}
#
# no default or current found, use the ipaddress
#
else {
    my $ip = $cgi->remote_host();
    my @tokes = split('\.',, $ip);
    my $nip = @tokes[3] + @tokes[2] * 256 + @tokes[1] * 256 * 256 + @tokes[0] * 256 * 256 * 256;
    my $qry = "SELECT *
        FROM ip_latlong_lookup
        WHERE $nip <= ipTO and $nip >= ipFROM
        LIMIT 1;";
    warn $ip . "---" . $qry."\n";
    my $tdata = $db->prepare($qry);
    my $rv = $tdata->execute;
    while (my $tmp = $tdata->fetchrow_hashref) {
        $lng = $tmp->{iplongitude};
        $lat = $tmp->{iplatitude};
    }
    $range = 10.0;
}
#
# start loading the data for the merchants located within the desired box around the location
#
$cookiedata = "\"You Are Here^ $lng^ $lat |";
#
# Calculate the desired box size
#
my $box_lat_chg = (($range/2.0)/$earth_radius_mi) * $rad2deg;
my $box_lon_chg = (($range/2.0)/($earth_radius_mi * cos($lat*$deg2rad)))*$rad2deg;
@box[0] = $lng+$box_lon_chg;
@box[1] = $lat+$box_lat_chg;
@box[2] = $lng-$box_lon_chg;
@box[3] = $lat-$box_lat_chg;
$cookiedata .= "Top Left ^ " . @box[0] . "^" . @box[1] . " | Lower Right^ " . @box[2] . "^" . @box[3] . " | ";

my $qry = <<"EOD";
 SELECT DISTINCT
     mal.*,
     COALESCE(mam.url,'') AS url,
     mam.discount_type,
     COALESCE(mam.special,'') AS special,
     COALESCE(mam.discount_blurb,'') AS discount_blurb,
     mam.business_type,
     d.cap,
     d.discount,
     v.blurb,
     COALESCE(v.coupon,'') AS coupon,
     yp.yp_heading,
     bc.description AS category_description,
     CASE
         WHEN
             mam.discount_type =  13
         THEN
             1
         WHEN
             mam.discount_type IN (12, 3)
         THEN
             2
         ELSE
             0
     END AS preference
     FROM
         merchant_affiliate_locations mal
     JOIN
         merchant_affiliates_master mam
         ON
            mal.merchant_id = mam.id
     $keyword_join
     JOIN
         merchant_affiliate_discounts d
         ON
            mal.id = d.location_id
     JOIN
         merchant_discounts md
         ON
            md.type = mam.discount_type
     JOIN
         vendors v
         ON
            v.vendor_id = mal.vendor_id
     JOIN
         business_codes('$language_code') bc
         ON
            mam.business_type = bc.code
     JOIN
         ypcats2naics yp
         ON
            yp.pk = mam.business_type
     WHERE
         mal.longitude <= @box[0] and mal.longitude >= @box[2] and
         mal.latitude <= @box[1] and mal.latitude >= @box[3] and
         v.vendor_group = 5
         AND
             NOW()::date >= d.start_date
         AND
           (
             NOW()::date <= d.end_date
             OR
             d.end_date IS NULL
           )
         AND
             mam.discount_type IN (11, 3, 12, 2, 15, 14, 1, 0, 10, 13)
         AND
             v.status= 1
-- exclude our house testing account
         AND
             mam.id != 41
         $keyword_where

EOD
#
# load the merchant data into the array the javascript will use to place the pushpins
#
my $tdata2 = $db->prepare($qry);
my $rv = $tdata2->execute;
while (my $tmp2 = $tdata2->fetchrow_hashref) {
    warn Dumper($tmp2);
    my $blurb = $cgi->escapeHTML($tmp2->{discount_blurb});
    my $blurb2 = $cgi->escapeHTML($tmp2->{coupon});
    my $blurb3 = $cgi->escapeHTML($tmp2->{blurb});
    $tmp2->{location_name} =~ s/\"/\'/g;
    $blurb =~ s/\n|\r/ /g;
    $blurb2 =~ s/\n|\r/ /g;
    $blurb3 =~ s/\n|\r/ /g;
    my $blurb4 = "Percentage Off " . $tmp2->{special} * 100 . "&#37;";
    if ($tmp2->{special} eq "free") {
	$blurb4 = "Buy One and Get One Free";
    }
    if ($tmp2->{discount_type} == 10) {
        $blurb4 = "Percentage Off " . $tmp2->{special} . "&#37;";
    }
    $cookiedata .= "$tmp2->{location_name}^ $tmp2->{longitude}^ $tmp2->{latitude}^ $tmp2->{business_type}^ $tmp2->{yp_heading}^ $tmp2->{address1}^ $tmp2->{phone}^ $blurb4^ $blurb^ $blurb3^ $blurb2 |";
    %categories->{$tmp2->{yp_heading}} = $tmp2->{business_type};
}
$cookiedata .= "\";";
print $cgi->header();
print "<html>\n";
print "<head>\n";
print "<title>IP Address Display</title>\n";
#
# put the javascript out that will be used to display the map and push pins
#
print <<"EOT";
<style type="text/css">
    html { height: 100% }
body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
  #side_html { width:30%; height:610px; float:left; overflow:auto; background-color: #A5BFDD; border-color: #00174B;	border-style: none none solid solid; border-width: 0 0 0 1px; float: right;	height: 100%; line-height: 1.5em; overflow: auto; padding: 0 5px; text-decoration: none; position:relative; top:-13px;}
	#side_title { background-image: url('http://www.clubshop.com/maps/map_icon/title_bg.png');
					background-repeat:repeat-x;
					color: #FFFFFF;
					float: left;
					font-size: 1.3em;
					font-weight: bold;
					height: 38px;
					padding-top: 5px;
					position: relative;
					text-align: center;
					width: 278px;
					z-index: 10; }
  
</style>
<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript" src="http://www.clubshop.com/maps/_includes/js/rewardsmap.js"></script>
<script type="text/javascript" src="http://www.clubshop.com/mall/_includes/js/javacookie.js"></script>
<script type="text/javascript">
    var cook = $cookiedata;
</script>
EOT
#
# put out the rest of the data
#
    print "</head>\n";
    print "<body onload=\"initialize()\">\n";
	print "<form>";
        print "<select name=\"Category\" id=\"category\" onChange=\"Display_Cat_Map(this)\">";
        print "<option value=\"none\" selected=\"yes\">Category List</option>";
        foreach my $cat(sort keys %categories) { 
            if (%categories->{$cat} ne "") {
                my $catname = %categories->{$cat};
	        print "<option value=\"$catname\"> ".$cat." </option>";
	    }
        }
	print "</select>\n";
	print "</form>";
	print "<div id=\"main\" style=\"width:100%; height:100%;\">";
        print "<div id=\"map_canvas\" style=\"width:68%; height:100%; float:left;\"></div>";
        print "<div id=\"side_title\" >Merchant</div>";
        print "<div id=\"side_html\" >";
		
				print "<div id=\"side_title\" >Merchant";
				
				print "</div>";
				
		print "</div>";
		
        print "</div>";
print "<p>\n";
print "</body></html>\n";
