#!/usr/local/bin/perl
#
# This script gets your current location by either ip address, a default address set by you earlier, or if you type
# in an address. Given that address, it locates all merchants within a given diameter (actually a box). From that list,
# all of the categories are collected and placed in a pull down menu.  
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use CGI;
use CGI::Cookie;
use G_S;
use ParseMe
    require XML::Simple;
    require XML::local_utils;
use Data::Dumper;

my @TMPL = (10897);
my $cgi = new CGI;
my $gs = new G_S;
my $language_code = "en";
#
# Check to see if the default cookie is there
#
my $cgiini = "ipaddress.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
#
# Get the translation hash
#
my $lu = new XML::Simple;
for (my $ii; $ii<length(@TMPL); $ii++) {
    my $select = "Select * from object_translations where obj_key = @TMPL[$ii];";
    my $tdat = $db->prepare($select);
    my $rv = $tdat->execute;
    while (my $tmp = $tdat->fetchrow_hashref) {
        $tmp->{value} =~ s/\n|\r|^M//g;
        my $trans = $lu->XMLin($tmp->{value});
        foreach my $key (keys %{$trans}) {
            if ($key eq "translate") {
                foreach	my $key2 (keys %{$trans->{$key}}) {
                    my $dir = "";
                    foreach my $key3 (keys %{$trans->{$key}->{$key2}}) {
                        my @check = split('\.', $key3);
                        $dir = "/home/httpd/html/translate/$key2/";
                        if ($tmp->{language} eq "en") {
                            $dir .= $key3 . ".shtml";
                        }
			else {
                            $dir .= @check[0] . ".manual." . $tmp->{language};
			}
			print $dir . "\n";
#			print $trans->{$key}->{$key2}->{$key3} . "\n";
			open (TEXTOUT, ">$dir");
			print TEXTOUT $trans->{$key}->{$key2}->{$key3};
			close TEXTOUT;
		    }
		}
	    }
        }
    }
}

