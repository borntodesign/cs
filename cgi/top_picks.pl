#!/usr/bin/perl -w

=head1 NAME:

	top_picks.pl

=head1 DESCRIPTION:

	This script querries the "network" database for a members
	"Top Picks" then displayes them.  This is ment to be called
	from an XSP page in the mall.

=head1 DEPLOYED: 

	08/08

=head1 AUTHOR: 

	Keith

=head1 VERSION:

	0.1

=cut


=head1 PREREQUISITS:

	CGI::Carp qw(fatalsToBrowser);
	DB_Connect;
	CGI;
	Specials;

=cut

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use CGI;
use Specials;

=head1	GLOBALS

=cut

=head2	VARIABLES:

	%param	hash
	$rv	hashref

=cut

our %param = ();
our $rv = ();

=head2 OBJECTS:

	$CGI	obj	Holds the CGI object 
	$db	obj	Holds the database object
=cut

our $CGI = CGI->new();
our $db = '';


=head1 SUBROUTINES:

=head2 exitScript

=head3 DESCRIPTION:
	This sub came around because
	Bill was/is using a "goto" statement!

=cut

sub exitScript
{
	$db->disconnect if ($db);
	exit;
}

=head2 ampCK

=head3 DESCRIPTION:

	This converts ampersand sighns into XML friendly ampersands.

=cut

sub ampCK {
	my $amp = shift;
	return $amp if $amp eq '&amp;';
	$amp =~ s/&/&amp;/;
	return $amp;
}

=head2 Err

=head3 DESCRIPTION:

	Prints an error node, then exits the script

=head3 PARAMS:

	$params	string	The error message to print

=cut

sub Err
{
	my $params = shift;

	print $CGI->header('text/xml');
	print "<error>$params</error>";
	exitScript();
}

=head2 EscapeEntities

=head3 DESCRIPTION:

	globally replaces and "&" with "&amp;"

=head3 PARAMS:

	$args	string

=cut

sub EscapeEntities {
	my $arg = shift;
	$arg =~ s/\ &\ /\ &amp;\ /g;
	return $arg;
}

=head2 LoadParams

=head3 DESCRIPTION:

	Loads parameters, into the global "%param" hash,
	and confirms the existence of a "mall_id"
	and a "member_id".

=head3 PARAMS:

	none

=head3 RETURNS:

	returns string/undef a string is returned on error

=cut

sub LoadParams
{
	%param = ();
	###### we are only expecting these arguments
	my @expected = qw/mall_id member_id sort_by_date/;
	foreach(@expected){
		$param{$_} = $CGI->param($_);
	}

	return ('bad or missing mall_id param:' .  $param{mall_id} || '') if (! $param{mall_id}) or $param{mall_id} =~ /\D/;

	return "<not_logged_in>1</not_logged_in>" if (!defined $param{member_id} || (defined $param{member_id} && $param{member_id} =~ /\D|^$/));

	return undef;
}

=head2 _default_handler

=head3 DESCRIPTION:

	Instanciates the "Specials" class
	calls "$Specials->getMemberTopPicksXML"
	and prints the users top picks.

=cut

sub _default_handler
{
	my $xml = '';

	eval{
		my $Specials = Specials->new($db);
		$xml = $Specials->getMemberTopPicksXML($param{member_id},$param{mall_id}, $param{sort_by_date});
		
		$xml = EscapeEntities($xml);
		
	};
	if($@)
	{
		Err($@);
		return;
	}

	my $sort_by = $param{sort_by_date}?0:1;

	print $CGI->header('text/xml');
	print("<root>$xml<parameters><mall_id>$param{mall_id}</mall_id><sort_by>$sort_by</sort_by></parameters></root>");
}


################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exit unless $db;
$db->{RaiseError} = 1;

# we'll get textual URls back with the invocation method above, so we can test
# for a valid DB handle
if (! ref $db)
{
	Err("<error>$db</error>");
}
else
{
	if ( $rv = LoadParams() )
	{
		Err($rv);
		exitScript();
	}

 	_default_handler();
}

exitScript();


