#!/usr/bin/perl -w
###### state-list-js.cgi
###### a routine to create a javascript module that can be used within a page
###### to create state drop-downs on the fly
###### created: 01/25/05	Bill MacArthur
###### last modified:

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;

my $db = DB_Connect::DB_Connect('state-list-js.cgi') || exit;
my $gs = new G_S;
my $code_list = my $name_list = '';
my ($country) = (split /=/, $ENV{'QUERY_STRING'})[1] || '';
$country = '' if $country !~ /[A-Z]{2}/;	###### a simple security check
	
###### get our base document with functions
my $tmpl = $gs->Get_Object($db, 10376) || '';

if ($country)
{
	my $sth = $db->prepare("SELECT code, name FROM state_codes WHERE country=?");
	$sth->execute($country);
	while (my ($code, $name) = $sth->fetchrow_array)
	{
		$code_list .= qq|"$code",|;
		$name_list .= qq|"$name",|;
	}
	$sth->finish;
	chop $code_list;
	chop $name_list;
}
$tmpl =~ s/%%country%%/$country/;
$tmpl =~ s/%%code_list%%/$code_list/;
$tmpl =~ s/%%name_list%%/$name_list/;
print "Content-type: text/html\n\n$tmpl\n";

END:
$db->disconnect;
exit;

