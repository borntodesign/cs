#!/usr/bin/perl -w
###### coop-import
###### import coop members from approved lead sources
###### created: 11/08        Bill MacArthur
###### last modified: 

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use Mail::Sendmail;
#use Data::Dumper;
require 'excluded_domains.lib';

my $max_errors = 10;	# we'll abort if we exceed this many errors in a batch
my $DEF_MBR_TYPE = 'm';	# when we create memberships they will be...

# these fields should not be used in the SQL on the members table
my %NonDbFields = (signup_ipaddr=>1, landing_page=>1, signup_timestamp=>1);

# HQ ip address for global access
my $HQIPADDR = '12.32.98.254';
my $q = new CGI;
my $ip = $q->remote_addr;
my $coop_id = $q->param('coop_id') || die "Failed to receive a coop_id";
my $provider_id = $q->param('provider_id') || die "Failed to receive a provider_id";
my $list = $q->param('list') || die "Failed to receive a list";
my $db = DB_Connect('coop-import') || exit;
$db->{RaiseError} = 1;

my $provider = $db->selectrow_hashref("
	SELECT
		coop_id,
		provider_id,
		provider_name,
		ipaddress,
		required_fields,
		field_format,
		delimiter_value,
		COALESCE(encapsulation_character,'') AS encapsulation_character
	FROM coop.providers
	WHERE active=TRUE
	AND (ipaddress >>= ? OR '$HQIPADDR' = ?)
	AND coop_id= ?
	AND provider_id= ?", undef, $ip, $ip, $coop_id, $provider_id) ||
		die "Failed to find a coop provider matching the supplied parameters and your IP address: $ip";

my %Req = map {$_=>1} (split /,/, $provider->{required_fields});
my @Format = split /,/, $provider->{field_format};
my $format = '';		# this will end up being a regexp of what a line should look like
my $format_expr = '';	# this will end up as a textual example of what a line should look like
BuildFormats();

###### we will provide some feedback about what we are expecting in the submitted file
if ($q->param('information')){
	_test();
	goto 'END';
}

# get rid of windows carriage returns
$list =~ s/\r//g;
my $error_cnt = 0;
my $Return = '';
print $q->header(-type=>'text/plain', -charset=>'utf-8');

# in test mode, we won't commit our new memberships
my $TEST_MODE = $q->path_info eq '/test' ? 1:0;
if ($TEST_MODE)
{
	$db->{AutoCommit} = 0;
	print "TEST MODE\n";
}

my $accepted = my $rejected = 0;
foreach (split /\n/, $list)
{
	my $line = '';
	
	if ($_ !~ /$format/)
	{
		$rejected++;
		DoResults("REJECTED (format mismatch),$_");
		next;
	}
	my $item = $_;
	# remove leading and trailing encapsulation
	$item =~ s/^$provider->{encapsulation_character}|$provider->{encapsulation_character}$//g;
	my $eflag = 0;
	my @tokens = split /$provider->{encapsulation_character}$provider->{delimiter_value}$provider->{encapsulation_character}/, $item;

	my %potential = ();
	foreach my $field (@Format)
	{
		$potential{$field} = shift @tokens;
		if ($Req{$field} and not $potential{$field})
		{
			$error_cnt++;
			$eflag = 1;
			$rejected++;
			DoResults("REJECTED (required field missing: $field),$_");
			last;
		}
	}
	unless ($eflag)
	{
		# let's assume that we will always require an email address
		# what other field/s could identify dupes reliably?
		my($rv) = $db->selectrow_array('SELECT id FROM members WHERE emailaddress= ? OR emailaddress2= ?',
			undef, $potential{emailaddress}, $potential{emailaddress});
		if ($rv)
		{
			$rejected++;
			$line = 'REJECTED (duplicate email address)'
		}
		else
		{
			my @rv = Import(\%potential);
			if ($rv[0])
			{
				$accepted++;
				$line = "ACCEPTED ($rv[1])";
			}
			else
			{
				$rejected++;
				$line = "REJECTED ($rv[1])";
			}
		}
		DoResults("$line,$_");
	}
	elsif ($error_cnt > $max_errors)
	{
		DoResults("Error limit reached: $max_errors\nAborting due to too many errors\nSkipping the rest of your list");
		last;
	}
}

# save our source and response lists
my ($report_id) = $db->selectrow_array("SELECT NEXTVAL('coop.imports_pk_seq')");
$db->do("
	INSERT INTO coop.imports (pk, coop_id, provider_id, accepted, rejected, import_list, response_list)
	VALUES ($report_id, $provider->{coop_id}, $provider->{provider_id}, $accepted, $rejected,?,?)", undef, $list, $Return);

$db->rollback if $TEST_MODE;
Notify_Admin() unless $TEST_MODE;
END:

$db->disconnect;
exit;

sub BuildFormats
{
	my $regex_delimiter_value = $provider->{delimiter_value};
	# since this will be used in a regex, we need to escape certain characters
	$regex_delimiter_value = '\|' if $regex_delimiter_value eq '|';
	
	my $format_expr_delimiter_value = $provider->{delimiter_value};
	# for showing as an example, we need to make a tab seen
	$format_expr_delimiter_value = '&lt;TAB&gt;' if $format_expr_delimiter_value eq '\t';
	foreach (@Format)
	{
		$format .= "$provider->{encapsulation_character}.*?$provider->{encapsulation_character}$regex_delimiter_value";
		$format_expr .= "$provider->{encapsulation_character}$_$provider->{encapsulation_character}$format_expr_delimiter_value";
	}
	$format =~ s#$regex_delimiter_value$##;
	$format_expr =~ s#$format_expr_delimiter_value$##;
}

sub DoResults
{
	print "$_[0]\n";
	$Return .= "$_[0]\n";
}

sub EmailChecks
{
	return 'excluded domain' if eval 'grep $_[0] =~ /$_$/i, @excluded_domains::LIST';
	
	###### this catches common spelling errors and a few bogus domains
	my $rv = $db->selectrow_array("
                SELECT domain FROM domains_blocked WHERE ? LIKE ('\%\@' || domain)", undef, $_[0]);
	return $rv ? 'blocked domain' : 1;
}
sub Import
{
	# $p = the potential membership
	my $p = shift;
	# first things first... make sure we want the email address
	my $rv = EmailChecks($p->{emailaddress});
	return (0, $rv) unless $rv eq '1';
	
	$db->{RaiseError} = 0;
	my $qry = "INSERT INTO members (membertype, spid,";
	my $qry_end = "VALUES ('$DEF_MBR_TYPE', $coop_id,";
	my @args = ();
	foreach my $field (@Format)
	{
		my $rv = ParamChecks($field, $p->{$field});
		return (0, $rv) unless $rv eq '1';
		
		next if $NonDbFields{$field};
		next unless defined $p->{$field};

		$qry .= "$field,";
		$qry_end .= '?,';
		push @args, $p->{$field};
	}
	chop $qry;
	chop $qry_end;
	# until we get the OK for the extra "birth certificate" recordset, we'll just stuff this stuff into the memo
	my $memo = "Source: $provider->{provider_name}\n";
	$memo .= "IP Address: $p->{signup_ipaddr}\n";
	$memo .= "When: $p->{signup_timestamp}\n";
	$memo .= "Signup Page: $p->{landing_page}";
	
	my ($id) = $db->selectrow_array("SELECT NEXTVAL('members_id_seq')");
	###### we have to create the start of an alias
    ###### we'll start with the first characters of the names and move to the next
    ###### if we don't have ordinary letter characters
    $p->{'firstname'} =~ /^.*?([a-z])/i;
    my $fc = uc $1 || 'A';
    $p->{'lastname'} =~ /^.*?([a-z])/i;
    my $lc = uc $1 || 'A';

	my $alias = $fc . $lc . $id; 
	$rv = $db->do("$qry, id, alias, memo) $qry_end, $id, ?, ?)", undef, @args, $alias, $memo);
	return (1, $alias) if $rv eq '1';
	return (0, 'INTERNAL ERROR ' . $db->errstr);
}

sub Notify_Admin
{
# send an email to the Coop administrator
	my $coop = $db->selectrow_hashref("
		SELECT emailaddress, firstname||' '||lastname AS name FROM members WHERE id= $provider->{coop_id}
	");
	my $admins = $db->selectall_hashref("
		SELECT m.firstname||' '||m.lastname AS name, m.emailaddress, m.id
		FROM coop.coop_admins ca
		JOIN members m ON m.id=ca.member_id
		WHERE ca.coop_id= $provider->{coop_id}
	", 'id');

	my $cc = '';
	foreach (keys %{$admins})
	{
		$cc .= "$admins->{$_}{name}<$admins->{$_}{emailaddress}>,";
	}
	chop $cc;
	sendmail(
		'Content-Type'=>'text/plain; charset="UTF-8"',
		To=>"$coop->{name}<$coop->{emailaddress}>",
		Cc=>$cc,
		From=>'coop-import on www.clubshop.com<webmaster@dhs-club.com>',
		Subject=>'Coop Import Notification',
		Message=>"Here are the results of the latest import from: $provider->{provider_name}
Items accepted: $accepted
Items rejected: $rejected
Complete report here: <url>"
	);
}

sub ParamChecks
{
	my %checks = (
		country => {
			message => 'Country must be 2 character ISO country code',
			test => sub{return 1 if $_[0] =~ /^[A-Z]{2}$/}
		},
		signup_ipaddr => {
			message => 'IP address invalid',
			test => sub{return 1 if $_[0] =~ /^\d+\.\d+\.\d+\.\d+$/}
		}
	);
	my $field = shift;
	my $val = shift;
	return 1 unless exists $checks{$field};
	return $checks{$field}{message} unless &{$checks{$field}{test}}($val);
	return 1;
}

sub _test
{
	print $q->header(-charset=>'utf-8');
	print $q->start_html(
		-encoding=>'utf-8',
		-style=>{-code=>'body{font-size:90%}'}
		);
	print $q->h4("Provider Name: $provider->{provider_name}<br />
		Provider ID: $provider->{provider_id}<br />Coop ID: $provider->{coop_id}");
	print $q->p("The URL you will use:<br />
		http[s]://www.clubshop.com/cgi/coop-import?provider_id=$provider->{provider_id};coop_id=$provider->{coop_id};list=<b>
		A URL escaped list of potential memberships in the specified format separated by newlines</b>");
	print $q->p("Required fields:<br />
		$provider->{required_fields}");
	print $q->p("List format:<br />
		$format_expr");

	print $q->start_form(-action=>$q->url . '/test', -method=>'get');
	print '<fieldset><legend>Test your format here</legend>Enter a properly formatted line<br />';
	print $q->textfield(-name=>'list', -style=>'width:100%; margin:0.3em;'), '<br />';
	print $q->submit('Test'), $q->hidden('coop_id'), $q->hidden('provider_id');
	print $q->p({-style=>'font-size:75%'}, 'The data submitted will not be recorded');
	print '</fieldset>',
		$q->p('For assistance, contact: <a href="mailto:webmaster@dhs-club.com">webmaster@dhs-club.com</a>'),
		$q->end_form, $q->end_html;
}



