#!/usr/bin/perl -w

=head1 NAME:
coop_order.cgi

	This script handles the "One Time" Co-op purchases.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, FindBin, CGI, DB, DB_Connect, G_S, State_List, XML::Simple, XML::local_utils, Members, Coop::Factory, Business::CreditCard

=cut

use strict;
use FindBin;
use lib "$FindBin::Bin/../cgi-lib";
use DHSGlobals;
use CGI;
use G_S qw ($LOGIN_URL);
use DBI;
use DB_Connect;
use CGI;
use State_List;
use XML::Simple;
use XML::local_utils;
use Members;
require Coop::Factory;
use Business::CreditCard;
use Data::Dumper;
use CGI::Carp qw(fatalsToBrowser);

my $db = ();
our $gs = G_S->new();
our $CGI = CGI->new();
our $XML_Simple = XML::Simple->new();
our $XML_local_utils =  XML::local_utils->new();
my %config = ();
#	if (DHSGlobals::PRODUCTION_SERVER)
#	{
		$config{'content_xml'} = 10410;
		$config{'country_xml'} = 10407;
		$config{'pricing_xml'} = 10409;
		$config{'xsl'}		 = 10408;
#	} else {
#		$config{content_xml} = 10410;
#		$config{country_xml} = 10407;
#		$config{pricing_xml} = 10409;
#		$config{xsl}		 = 10408;
#	}

our $ME = $CGI->script_name;
our $db_access_string = 'coop_order.cgi';
our	$member_id = '';
our $member_info = '';

( $member_id, $member_info ) = $gs->authen_ses_key( $CGI->cookie('AuthCustom_Generic'));

unless ( $member_id && $member_id !~ m/\D/ )
{
	# Redirect the user to the login page.
	print $CGI->redirect( '-URL' => "$G_S::LOGIN_URL?destination=$ME");
	exitScript();
}

exitScript() unless $db = DB_Connect($db_access_string);

my $pricing_xml = $gs->Get_Object($db, $config{'pricing_xml'});
my $pricing_hashref = $XML_Simple->XMLin("<base>$pricing_xml</base>", 'ForceArray' => 1);

if($CGI->param('order_type'))
{
	processOrderForm();
} else {
	displayOrderForm();	
}

exitScript();

###### Start of sub-routines

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayOrderForm

=head3
Description:

	

=head3
Params:

	none

=head3
Returns:

	none

=cut

sub displayOrderForm
{
	
	my $data_hash = shift;
	my $notices = shift;
	my $warnings = shift;
	my $errors = shift;
	
	my $formatted_notices = '';
	my $formatted_warnings = '';
	my $formatted_errors = '';

	if( $errors && scalar @{$errors})
	{
		$formatted_errors = q(<div align="center" class="bad"><xsl:value-of select="//lang_blocks/warnings/empty_field" /><br/>) . join(' ', @{$errors}) . '</div><br />';
	}
	
	if( $warnings && scalar @{$warnings})
	{
		$formatted_warnings = q(<div align="center" class="bad"><xsl:value-of select="//lang_blocks/warnings/empty_field" /><br/>) . join(' ', @{$warnings}) . '</div><br />';
	}

	if( $notices && scalar @{$notices})
	{
		$formatted_notices = q(<div align="center" class="good">) . join(' ', @{$notices}) . '</div><br />';
	}
	
	my $member_information = {};

	my @countries_where_members_are_available = ();
	my $members_for_sale = 0;
	
	my $xsl = '';
	my $static_content_xml = '';
	my $country_xml = '';

	my $select_country_hashref = {};
	my $select_country_xml = '';
	
	#print $CGI->header('text/plain');	

#	eval{
		
		$xsl = $gs->Get_Object($db, $config{'xsl'});
		
		$xsl =~ s/%%ERRORS%%/$formatted_errors/;
		$xsl =~ s/%%WARNINGS%%/$formatted_warnings/;
		$xsl =~ s/%%NOTICES%%/$formatted_notices/;
		
	
		$static_content_xml = $gs->Get_Object($db, $config{'content_xml'});
		$country_xml = $gs->Get_Object($db, $config{'country_xml'});

		$select_country_hashref = $XML_Simple->XMLin($country_xml, 'ForceArray' => 1);
		
		my $Coop = Coop::Factory->new('',$db);

		$members_for_sale = $Coop->getTotalMembersForSale($pricing_hashref->{'donor_accounts'}->[0]);
		@countries_where_members_are_available = $Coop->getCountriesWhereMembersAreAvailable($pricing_hashref->{'donor_accounts'}->[0]);
		$members_for_sale = $members_for_sale > 500 ? '500+' : $members_for_sale;
		
		# If "data_hash" is not a hashref (If this subroutine is being called without parameters, first run)
		# If this subroutine is displayed due to an error, the data_hash will all ready contain the members input.
		if (ref $data_hash ne 'HASH')
		{
			my $Members = Members->new($db);
			
			$member_information = $Members->getMemberInformationByMemberID({'member_id'=>$member_id});
			$member_information->{'mop'} = $Members->getMemberMOPInformation({'member_id'=>$member_id});
			
			$data_hash->{'id'} = [$member_information->{'id'}];
			$data_hash->{'alias'} = [$member_information->{'alias'}];
			
			$data_hash->{'acct_holder'} = [$member_information->{'mop'}->{'acct_holder'}];
			$data_hash->{'addr'} = [$member_information->{'mop'}->{'address'}];
			$data_hash->{'city'} = [$member_information->{'mop'}->{'city'}];
			$data_hash->{'country'} = [lc($member_information->{'mop'}->{'country'})];
			$data_hash->{'postalcode'} = [$member_information->{'mop'}->{'postal_code'}];
			$data_hash->{'state'} = [$member_information->{'mop'}->{'state'}] if $member_information->{'mop'}->{'country'} =~ /^us$/i;
			$data_hash->{'province'} = [$member_information->{'mop'}->{'state'}] if $member_information->{'mop'}->{'country'} !~ /^us$/i;
			
		}
		
		$data_hash->{'members_for_sale'} = [$members_for_sale];
		
#	};
#	if($@)
#	{
##TODO Decide what to do!
#
#		print $CGI->header('text/plain');	
#		print $@ . "\n";
#		exitScript();
#	}
	

			
	#Format the country locations so the XSL stylesheet will display the selected countries.	
	if (exists $data_hash->{'selected_locations'})
	{

		my $temp_hashref = {'country'=>[]};
		
		foreach my $value (@{$data_hash->{'selected_locations'}})
		{
			if (exists $select_country_hashref->{$value})
			{
				push @{$temp_hashref->{'country'}}, {$value => $select_country_hashref->{$value}->[0]};
				undef $select_country_hashref->{$value};
			}
		}
		$data_hash->{'selected_locations'} = $temp_hashref;
		
	}
	
	foreach my $country (keys %$select_country_hashref)
	{
		$select_country_hashref->{$country}->[0]->{'disabled'} = 'disabled';
	}
	
	
	foreach my $country (@countries_where_members_are_available)
	{
		$country = lc($country);
		
		delete $select_country_hashref->{$country}->[0]->{'disabled'} if exists $select_country_hashref->{$country};
	}
	

	
	$select_country_xml = $XML_Simple->XMLout($select_country_hashref, 'RootName' => "select_countries");
	
	

	
	
	my $data_xml = $XML_Simple->XMLout($data_hash, 'RootName' => "data");
	
	my %states = %State_List::STATE_CODES;
	my $temp = '';
	
	foreach ( sort keys %states )
	{
		my $tag = lc $states{$_};
		$temp .= "<$tag>$_</$tag>\n";
	}
	
	my $states_xml = "<state>$temp</state>";
	
	undef $temp;
	
	my $cc_expiration_date = buildCreditCardExpirationDate();
	
	my $xml =<<EOS;
	
	<base>
		$data_xml
		$static_content_xml
		$pricing_xml
		<menus>
			$country_xml
			$select_country_xml
			$states_xml
			$cc_expiration_date
		</menus>
	</base>
	
EOS
	
	

	my $template = $XML_local_utils->xslt($xsl, $xml);
	
	
	$template =~ s/%%\w+%%//g;
	
	
	print $CGI->header('-charset'=>'utf-8');
	print $template;
	

}

=head2
buildCreditCardExpirationDate

=head3
Description:

	This subroutine creates an xml document of expiration dates for eight years into the future.
	
	Example:
	<cc_exp>
		<D122008>12/2008</D122008>
		<D012009>01/2009</D012009>
		<D022009>02/2009</D022009>
	</cc_exp>

=head3
Params:

	none

=head3
Returns:

	string

=cut

sub buildCreditCardExpirationDate
{
	use Date::Calc;
	
	my $xml = '';
	my $number_of_loops = 12 * 8 + 1;
	
	my ($year, $month, $day) = Date::Calc::Today();
	my $short_year = $year;
	
	$short_year =~ s/^\w\w//;
	$month = "0$month" if $month < 10;

#
#	Here is some logic if we ever start using
#	programmer friendly XML.
#
#	my $card_expriation_dates = {
#									date=>[
#											"$month/$year",
#										]
#	};
#
#	for (my $counter = 1; $counter < $number_of_loops; $counter++)
#	{	
#		push @{$card_expriation_dates->{date}}, $month . '/' . $year;
#	}
#
#	$xml = $XML_Simple->XMLout($card_expriation_dates, RootName => "cc_exp");
#
	
	
	
	$xml = "<D$month$short_year>$month/$year</D$month$short_year>";
	
	for (my $counter = 1; $counter < $number_of_loops; $counter++)
	{	
		($year,$month,$day) = Date::Calc::Add_Delta_YM( $year, $month, $day, 0, 1 );
		$short_year = $year;
		$short_year =~ s/^\w\w//;
		$month = "0$month" if $month < 10;
		$xml .= "<D$month$short_year>$month/$year</D$month$short_year>";
		
	}
	
	return "<cc_exp>$xml</cc_exp>"
	
}


sub processOrderForm
{
	
	my %data_hash = ();
	my %display_data_hash = ();
	
	my %required_cc_fields = ('id'=>0, 'quantity'=>0, 'price'=>0, 'pay_type'=>0, 'order_type'=>0, 'acct_holder'=>0, 'addr'=>0, 'city'=>0, 'state'=>0, 'postalcode'=>0, 'country'=>0, 'cc_number'=>0, 'cc_cvv2'=>0, 'cc_exp'=>0);
	my %required_pp_fields = ('id'=>0, 'quantity'=>0, 'price'=>0, 'pay_type'=>0, 'order_type'=>0);
	my @cc_fields = ('acct_holder', 'addr', 'city', 'state', 'postalcode', 'country', 'cc_number', 'cc_cvv2', 'cc_exp');
	
	my @notices = ();
	my @warnings = ();
	my @errors = ();
	
	#######################################################
	# Prep the data for order placement.
	#######################################################
	foreach ($CGI->param())
	{
		# there is an actual control on the form called member_locations
		# it is only supposed to be used as a source for countries to be placed into the selected_locations list
		# however, if javascript is not working or some other anomaly occurs
		# and an element is selected from the member_locations list, then the parameter comes through
		# and is blindly used in the creation of the SQL (Keith used bad judgement when developing this concept)
		# we will ignore the parameter if it comes through
		next if $_ eq 'member_locations';

		if ($_ ne 'selected_locations')
		{
			$data_hash{$_} = G_S::PreProcess_Input($CGI->param($_));
			
			$display_data_hash{$_} = [G_S::PreProcess_Input($CGI->param($_))];
			
			$required_cc_fields{$_} = 1 if (exists $required_cc_fields{$_} && $data_hash{$_});
			$required_pp_fields{$_} = 1 if (exists $required_pp_fields{$_} && $data_hash{$_});
		
		} 
		else 
		{
			# put the selected country list into a CSV.
			my @locations = ();
			foreach my $value ($CGI->param('selected_locations'))
			{
				push @locations, G_S::PreProcess_Input($value);
				push @{$display_data_hash{'selected_locations'}}, G_S::PreProcess_Input($value);
			}
			
			$data_hash{'country_list'} = G_S::PreProcess_Input(join(',', @locations));
			
			# uneeded for order placement once the country_list has been created.
			undef $data_hash{'selected_locations'};
		}
	}


	
#	($data_hash{'quantity'}, $data_hash{'price'}) = split '/', $data_hash{'quantity'} if (exists $data_hash{'quantity'});
# accepting the price based upon browser data sent it ludicrous and ended up having VIPs hack the prices
# instead we will look up the price based upon the quantity desired ;)
	if ($data_hash{'quantity'})
	{
#warn "starting loop through prices: " . Dumper($pricing_hashref->{'coop_prices'});
		foreach my $hr (@{$pricing_hashref->{'coop_prices'}->[0]->{'item'}})
		{
			if ($hr->{'quantity'} eq $data_hash{'quantity'})
			{
				$data_hash{'price'} = $hr->{'price'};
				last;
			}
		}
	}

	die "Could not identify a price for the quantity selected: $data_hash{'quantity'}" unless $data_hash{'price'};

	$required_cc_fields{'price'} = 1 if (exists $required_cc_fields{'price'} && $data_hash{'price'});
	$required_pp_fields{'price'} = 1 if (exists $required_pp_fields{'price'} && $data_hash{'price'});
			
	if (exists $display_data_hash{'quantity'})
	{
		($display_data_hash{'quantity'}, $display_data_hash{'price'}) = split '/', $display_data_hash{'quantity'}->[0] ;
		
		$display_data_hash{'quantity'} = [$display_data_hash{'quantity'}]; 
		$display_data_hash{'price'} = [$display_data_hash{'price'}];
	}
	
	
	# uneeded for order placement
	undef $data_hash{'alias'};
	
	delete $data_hash{'province'} if (exists $data_hash{'province'} && $data_hash{'country'} =~ /US/i);
	
	if ($data_hash{'country'} !~ /US/i)
	{
		$data_hash{'state'} = $data_hash{'province'};
		delete $data_hash{'province'};
	}
		
	#######################################################


	if ($data_hash{'pay_type'} =~ /CC/i)
	{
		foreach my $key (keys %required_cc_fields)
		{
			#push @warnings, qq(<xsl:value-of select="//lang_blocks/warnings/$key" /><br/>) if ! $required_cc_fields{$key};
			push @warnings, qq(<xsl:value-of select="//lang_blocks/warnings/$key" /><br/>) if ! $data_hash{$key};
		}
		
		
		$data_hash{'cc_number'} =~ s/\D//g if $data_hash{'cc_number'};
		
		$data_hash{'cc_exp'} =~ s/\D// if $data_hash{'cc_exp'};
		
		# If there is no credit card number it should have already been pushed into @warnings.
		if($data_hash{'cc_number'} && ! Business::CreditCard::validate($data_hash{'cc_number'}))
		{
			push @warnings, q(<xsl:value-of select="//lang_blocks/warnings/cc_number" /><br/>);
		}
		
	} 
	elsif ($data_hash{'pay_type'}  =~ /PP/i)
	{
		foreach my $key (keys %required_pp_fields)
		{
			push @warnings, qq(<xsl:value-of select="//lang_blocks/warnings/$key" /><br/>) if ! $required_pp_fields{$key};
		}
		
		#Remove the fields that are not needed for Pay Point Purchases
		foreach (@cc_fields)
		{
			undef $data_hash{$_} if ! exists $required_pp_fields{$_};
		}
	} 
	else 
	{
		
		push @warnings, q(<xsl:value-of select="//lang_blocks/warnings/pay_type" /><br/>);
	}

	
	if(! scalar @warnings)
	{
		eval{
			my $Coop = Coop::Factory->new('',$db);
			
			$data_hash{'status'} = 'p';
			$data_hash{'operator'} = $FindBin::Script;
			$data_hash{'country'} = uc($data_hash{'country'});
			
			$data_hash{'state'} = uc($data_hash{'state'}) if $data_hash{'country'} =~ /US/i;
			
			if($Coop->insertCoopOrder(\%data_hash))
			{
				push @notices, q(<xsl:value-of select="//lang_blocks/messages/success1" /><br/>);
				push @notices, q(<xsl:value-of select="//lang_blocks/messages/success2" /><br/>);
				push @notices, q(<xsl:value-of select="//lang_blocks/messages/success3" /><br/>);
				
			}
			else
			{
				push @warnings, q(<xsl:value-of select="//lang_blocks/warnings/invalid" /><br/>);
			}
		};
		if($@)
		{
			warn 'coop_order.cgi: ' . $@;
			push @errors, q(<xsl:value-of select="//lang_blocks/errors/fail_insert" /><br/>);
		}
	}
	
	#This isn't needed any longer.
	undef %data_hash;
	
	displayOrderForm(\%display_data_hash, \@notices, \@warnings, \@errors);
}

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

	02/17/11 Bill	modified processOrderForm to ignore the member_locations parameter if it comes through
	08/26/11 Bill	The most notable change was that of obtaining the pricing for an order submitted by consulting
					the configuration document instead of relying on the pricing that was previously embedded
					in the 'quantity' parameter. Also moved the subs below the "main" code and made some sub routine vars global
					like the pricing xml
					
=cut
