#!/usr/bin/perl

use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::Simple;
use Template;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use MerchantAffiliates;
use Members;
use DHS::UrlSafe;
use Locations;
use Data::Dumper;	# only needed for debugging

binmode STDOUT, ":encoding(utf8)";

my ($db, $url);
our $CGI = CGI->new();
$CGI->charset('utf-8');
our $DHS_UrlSafe = DHS::UrlSafe->new();
my %TMPL = (
	'xml' => 10680
#	'app' => 10903
);
my $TT = Template->new({'INCLUDE_PATH' => '/home/httpd/cgi-templates/TT/merchant'});
my $gs = G_S->new({'CGI'=>$CGI, 'db'=>\$db});


####################################################
# Main
####################################################

#
# Make sure data comming from secure source
#
if (! $CGI->https())
{
	$url = $CGI->self_url();
	$url =~ s/http:/https:/;
	print $CGI->redirect($url);	
	exitScript();
}

exit unless $db = DB_Connect('merchant_applications');
$db->{'RaiseError'} = 1;

if($CGI->param())
{
	processApplication();	
} 
else
{
	displayApplication();
}

exitScript();

############################

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub displayApplication
{
	my $merchant_information = shift;
	my $errors = shift;
	my $step = shift;	
	
	$step = 1 if ! $step;
	
#	my $html = '';
	my $configuration_xml = '';
	my $errhtml = '';
	my $langauges_xml = '';
	my @states = ();
	
	my $MerchantAffiliates = {};
	my @tkeys = ("introduction","or","print_form_language","click_here","short_form_heading","required_fields","please_select_one","member_id","referral_id","new_member",
                     "business_name","address1","city","state","postalcode","phone","fax","url","emailaddress1","firstname1","lastname1","language","username","password",
                     "choose_business_cats","choose_business_cat_instructions1","choose_business_cat_instructions2","choose_business_cat_instructions3",
                     "agree");
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	$configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
		
	if (! $merchant_information)
	{
		if ($CGI->cookie('AuthCustom_Generic'))
		{
			my ( $id, $meminfo ) = $gs->authen_ses_key( $CGI->cookie('AuthCustom_Generic'));
			$merchant_information->{'referral_id'} = $id if $meminfo->{'membertype'} eq 'v';
		}
		elsif($CGI->cookie('refspid'))
		{
			$merchant_information->{'referral_id'} = $CGI->cookie('refspid');	
		}
	}

#       	$html = $gs->GetObject($TMPL{'app'});
#
# Get the translated countries
#
	my $language_Preference = $gs->Get_LangPref();
	my $countries = $db->selectall_hashref("
		SELECT country_code, country_name
		FROM country_names_translated_no_restrictions(?)", 'country_name', undef, $language_Preference || 'en') ||       
                                               die "Failed to obtain a set of country_names_translated"; 

	my $configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	
	if ($configuration->{'areas_available_for_registration'})
	{
		foreach my $key (keys %{$countries})
		{
			delete ($countries->{$key}) if undefined $configuration->{'areas_available_for_registration'}->{$countries->{$key}->{'country_code'}};
	    }
	}
#
# Convert XML to HTML
#
	my $countries_html = '';
	foreach my $key (sort(keys %{$countries}))
	{
		if ($merchant_information->{'country'} && $merchant_information->{'country'} eq $countries->{$key}->{'country_code'})
		{
			$countries_html .= qq|<option value ="$countries->{$key}->{'country_code'}" selected="selected">$countries->{$key}->{'country_name'}</option>\n|;
		}
		else
		{
			$countries_html .= qq|<option value ="$countries->{$key}->{'country_code'}">$countries->{$key}->{'country_name'}</option>\n|;
		}
	}

# get a list of states/provinces if they are available for that country
	if ($merchant_information->{'country'})
	{
		@states = $gs->select_array_of_hashrefs('SELECT "code", "name" FROM state_codes WHERE country= ?', undef, $merchant_information->{'country'});
	}
#
# Get Languages
#
	my $languages = $db->selectall_hashref("SELECT code2, description FROM language_codes where code2 is not null and active = true;", 'description', undef) ||       
                                               die "Failed to obtain a set of language names"; 
	my $language_html = '';
	foreach my $lang (sort(keys %{$languages}))
	{
		if ($merchant_information->{'language'} && $merchant_information->{'language'} eq $languages->{$lang}->{'code2'})
		{
			$language_html .= qq|<option value="$languages->{$lang}->{'code2'}" selected="selected">$languages->{$lang}->{'description'}</option>\n|;
		}
		else
		{
			$language_html .= qq|<option value="$languages->{$lang}->{'code2'}">$languages->{$lang}->{'description'}</option>\n|;
		}
	}
	
#
# Convert Languages
#
	my $translation_xml = $gs->GetObject($TMPL{'xml'});
	my $translation = XML::Simple::XMLin($translation_xml, 'NoAttr' =>1);
#	$html = translateLanguage($html, $translation);
#
# display any errors if any
#
	if ($errors)
	{
#	    $html =~ s/NONE/block/;
#		my $errhtml = '';
		foreach my $key (keys %{$errors})
		{
			if (ref($translation->{'errors'}->{$key}) eq 'HASH')
			{
				$errhtml .= $translation->{'errors'}->{$key}->{'content'} . "<br/>";
	        }
   	        else
   	        {
   	        	if ($translation->{'errors'}->{$key})	# there have been cases where we have no key, like when new stuff is defined but there is no language node available
   	        	{
					$errhtml .= $translation->{'errors'}->{$key} . "<br/>";
   	        	}
   	        	else
   	        	{
   	        		warn "We are looking in document: $TMPL{'xml'} for a node under errors but it is not found: $key";
   	        		$errhtml .= $key . '<br />';
   	        	}
  	        }
	    }

#		$html =~ s/%%errors_out%%/$errhtml/;
	}

#
# business description information
#

	if($merchant_information)
	{
		$merchant_information->{'biz_description'} = $db->selectrow_array('SELECT description FROM business_codes(?,?)', undef, ($language_Preference, $merchant_information->{'business_type'})) if $merchant_information->{'business_type'}; 
	}
			
	print $CGI->header;
#	$TT->process(\$html, {
	$TT->process('short-registration.tt', {
		'merchant_information'=>$merchant_information,
		'lb'=>$translation,
		'languages'=>$language_html,
		'countries'=>$countries_html,
		'states'=>\@states,
		'errors_out'=>$errhtml,
		'url'=>$url
	}) || print $TT->error();
}


=head2
processApplication

=head3
Description:

	Process the application.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub processApplication
{
	my %merchant_information = ();
	
	my %missing_fields = ();
	my $Members = {};
	my $MerchantAffiliates = {};
	my $RewardCards = {};

	my $configuration = {};
	my $number_of_cards_in_a_region_is_ok = 0;
	
	$MerchantAffiliates = MerchantAffiliates->new($db, $gs);
	my $configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
	$configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");
	$RewardCards = RewardCards->new($db);
		
	my @required_fields = 
	(
			'business_name',
			'firstname1',
			'lastname1',
			'address1',
			'city',
			'state',
			'country',
			'phone',
			'language',
			'emailaddress1',
			'username',
			'password',
			'merchant_package',
			'business_type',
			'reward_discount'
	);
	
	
	# Assign the information the merchant submitted to the "$merchant_information" hash.
	foreach my $key ($CGI->param())
	{
		next if $key =~ /submit|biz_description/i;
		my $tmp = $CGI->param($key);
###### filter out leading & trailing whitespace
		$tmp =~ s/^\s*//;
		$tmp =~ s/\s*$//;

###### convert other duplicate whitespace into one space
		$tmp =~ s/\s{2,}/ /g;
		
		next unless defined $tmp;
		$merchant_information{$key} = $gs->Prepare_UTF8($tmp) if $key !~ /^_|^x$|^y$/;
#		$merchant_information{$key} = $CGI->param($key) if $key =~ /^merchant_package$/;
	}
	
	$merchant_information{'merchant_package'} = 15;
	$merchant_information{'merchant_type'} = 'offline_rewards';

#
# Check for missing fields.
#

	foreach my $key ( @required_fields)
	{
		
		$missing_fields{$key} = ['Missing Field'] if(! defined $merchant_information{$key} || ! $merchant_information{$key});
	}
	
	if (length($merchant_information{'discount_blurb'} || '') > 80 )
	{
		$merchant_information{'discount_blurb'} = substr($merchant_information{'discount_blurb'},0,80);
			$missing_fields{'blurb_adjusted'} = ['Longer than 80'];
	}

#
# Check their discount options, only one is allowed
#
	
        if (! $missing_fields{'reward_discount'} )
        {
            if ($merchant_information{'reward_discount'} ne 'pctoff' && 
                $merchant_information{'reward_discount'} ne 'flat' && 
                $merchant_information{'reward_discount'} ne 'bogo')
			{
                $missing_fields{'reward_discount'} = ['Missing Field'];
			}
		}

	#Look for missing, or misformatted information pertaining to the form.
	
	$Members = Members->new($db);
	my $member_id = '';
	my $referral_id = '';
			
# If the applicant supplied a Member ID we will use that... if not we will try for a referral ID
	if($merchant_information{'member_id'})
	{
		$member_id = $merchant_information{'member_id'};
		$member_id =~ s/\D//g;
		$member_id =~ s/^0*//;
				
		$missing_fields{'member_id_invalid'} = ['Missing Field'] if ($member_id && ! $Members->getMemberInformationByMemberID({'member_id'=>$member_id, 'field'=>'id'}));
	}
	elsif ($merchant_information{'referral_id'})
	{
				
		$referral_id = $merchant_information{'referral_id'};
		$referral_id =~ s/\D//g;
		$referral_id =~ s/^0*//;
		
		my $referal_member_type = $Members->getMemberInformationByMemberID({'member_id'=>$referral_id, 'field'=>'membertype'});
			
		$missing_fields{'referral_id_wrong_membertype'} = ['Missing Field']
		if (! $referal_member_type || ! grep $_ eq $referal_member_type, ('v'));
	}
			
	# why throw an error? instead we just will ignore referral ID if we have a member ID submitted
#	$missing_fields{'member_id_referral_id'} = ['Missing Field'] if ($member_id && $referral_id);	
			
	if (! $missing_fields{'emailaddress1'})
	{
				
		$missing_fields{'emailaddress1_preexisting'} = ['Missing Field'] if (! $Members->checkEmailDomain($merchant_information{'emailaddress1'}));
		$missing_fields{'emailaddress1_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{'emailaddress1'}));
	}
			
	if(! exists $missing_fields{'username'})
	{
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingUsernameDuplicate($merchant_information{'username'}));
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkMasterUsernameDuplicates($merchant_information{'username'}));
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkLocationUsernameDuplicates($merchant_information{'username'}));
	}
			
# If the referral ID is a house account we'll skip the Reward Card Requirment check.
	my $skip = 0;
	foreach (@{$configuration->{'house_accounts'}->{'house_account'}})
	{
		$skip = 1 if ($referral_id && $referral_id == $_);
	}
	$skip = 1 if ( exists $merchant_information{'country'} && $merchant_information{'country'} =~ /^IT$|^italy$|^italia$/i);
			
# If there are no error messages for the state, country, refferal_id or member_id, and the member_id or refferal_id hold a value we should check for card available
# If there is no ISO code for their region, or the applicant has Java Script dispabled the accuracy of the cards in an errea is questionable.
# TODO: Add an exclusion here for Italy.
	if (! $skip && ! defined $missing_fields{'state'} && ! defined $missing_fields{'country'} && ! defined $missing_fields{'referral_id_wrong_membertype'} && ! defined $missing_fields{'member_id_invalid'} && ($member_id || $referral_id))
	{
#check the reward card numbers.
		$MerchantAffiliates->setRewardCards();
		my $reward_cards_in_a_province = $MerchantAffiliates->{'RewardCards'}->getRewardsCardInAProvince($merchant_information{'country'}, $merchant_information{'state'});
				
		my $merchants_in_a_province = $MerchantAffiliates->retrieveNumberOfMerchantsInAProvince($merchant_information{'country'}, $merchant_information{'state'});
				
		$merchants_in_a_province = 0 if ! $merchants_in_a_province;
				
		if ($merchants_in_a_province && $reward_cards_in_a_province < $merchants_in_a_province * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'})
		{
			my $sponsor_id = 0;
					
		#The Member ID, and Refferal ID should have been checked, and sanitized by now.
			if($member_id)
			{
				my $member = $Members->getMemberInformationByMemberID({'member_id'=>$member_id});
				$sponsor_id = $member->{'spid'};
			}
			else
			{
				$sponsor_id = $referral_id
			}
					
#TODO: Add a check for the sponsor_id, so this is skiped if the spid is 1,2, 5, 6, 7, 28 or 29.
#This is probably the best spot for this.
					
			my $referal_member = $Members->getMemberInformationByMemberID({'member_id'=>$sponsor_id});
					
			$missing_fields{'not_enough_reward_cards'} = [$merchants_in_a_province * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'} - $reward_cards_in_a_province];
			$missing_fields{'not_enough_reward_cards_sponsors_name'} = ["$referal_member->{'firstname'} $referal_member->{'lastname'}"];
			$missing_fields{'not_enough_reward_cards_sponsors_email'} = [$referal_member->{'emailaddress'}];
					
		#set the notification
			my %notification_info = (
				'id' => $sponsor_id,
				'notification_type' => 105,
				'spid' => $missing_fields{'not_enough_reward_cards'}->[0],
				'memo' => $merchant_information{'business_name'}
			);
					
			$MerchantAffiliates->createNotification(\%notification_info);
					
			my $url = $CGI->url();
			$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%missing_fields);
			print $CGI->redirect('-uri'=>"$url");
			exitScript();
		}
	}

	my $translation_xml = $gs->GetObject($TMPL{'xml'});
	my $translation = XML::Simple::XMLin($translation_xml, 'NoAttr' =>1);
	
#
# If flat_fee_off contains information, then the discount_type  needs to be 13 and special needs to contain the amount off.
#
	if ($merchant_information{'reward_discount'} eq 'flat')
	{
		# the value submitted should represent a currency value without decimal separator
		if ($merchant_information{'flat_fee_off'} !~ m/^\d+$/)
		{
			$missing_fields{'flat_fee_off'} = 1;
		}
		else
		{
			$merchant_information{'discount_subtype'} = 1;
			$merchant_information{'special'} = $merchant_information{'flat_fee_off'};
			delete $merchant_information{'flat_fee_off'};
		}
	}
	elsif ($merchant_information{'reward_discount'} eq 'pctoff')
	{
		$merchant_information{'discount_subtype'} = 3;
		$merchant_information{'special'} = $merchant_information{'percentage_of_sale'};
	}
	elsif ($merchant_information{'reward_discount'} eq 'bogo')
	{
		if ($merchant_information{'bogo_amt'} ne 'half' && $merchant_information{'bogo_amt'} ne 'free')
		{
			$missing_fields{'bogo_amt'} = "Invalid $translation->{'special'} selection";
		}
		else
		{
		    $merchant_information{'discount_subtype'} = 2;
		    $merchant_information{'special'} = $merchant_information{'bogo_amt'};
		    delete $merchant_information{'bogo_amt'};
		}
	}
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}

	my $returned_auth_info = "1";		
		
	my $credit_new_members_club_account = [];
		
	delete $merchant_information{'cc_type'} if exists $merchant_information{'cc_type'};
	delete $merchant_information{'cc_number'} if exists $merchant_information{'cc_number'};
	delete $merchant_information{'ccv_number'} if exists $merchant_information{'ccv_number'};
	delete $merchant_information{'cc_expiration_date'} if exists $merchant_information{'cc_expiration_date'};
	delete $merchant_information{'cc_name_on_card'} if exists $merchant_information{'cc_name_on_card'};
	delete $merchant_information{'cc_address'} if exists $merchant_information{'cc_name_on_card'};
	delete $merchant_information{'cc_city'} if exists $merchant_information{'cc_name_on_card'};
	delete $merchant_information{'cc_country'} if exists $merchant_information{'cc_country'};
	delete $merchant_information{'cc_state'} if exists $merchant_information{'cc_country'};
	delete $merchant_information{'ca_number'} if exists $merchant_information{'ca_number'};
	delete $merchant_information{'pay_iban'} if exists $merchant_information{'pay_iban'};
	delete $merchant_information{'pay_swift'} if exists $merchant_information{'pay_swift'};
	delete $merchant_information{'pay_bank_name'} if exists $merchant_information{'pay_swift'};
	delete $merchant_information{'pay_bank_city'} if exists $merchant_information{'pay_bank_city'};
	delete $merchant_information{'pay_bank_address'} if exists $merchant_information{'pay_bank_city'};
	delete $merchant_information{'pay_bank_state'} if exists $merchant_information{'pay_bank_city'};
	delete $merchant_information{'pay_bank_country'} if exists $merchant_information{'pay_bank_city'};
	delete $merchant_information{'pay_account_number'} if exists $merchant_information{'pay_account_number'};
	delete $merchant_information{'pay_payment_method'} if exists $merchant_information{'pay_payment_method'};
			
########################################################################
# If the member made it this far, all the required fields check out.
# Now we will see about making them a merchant, or putting
# them in the "merchant_pending" table.
########################################################################
#Create the Merchant Member, the Merchant Master record, the Merchant Location record, and the Shopping Member
			
				#sanitize some data
	if($merchant_information{'member_id'})
	{
		$merchant_information{'member_id'} =~ s/\D//g;
		$merchant_information{'member_id'} =~ s/^0*//;
	}
			
	if($merchant_information{'referral_id'})
	{
		$merchant_information{'referral_id'} =~ s/\D//g;
		$merchant_information{'referral_id'} =~ s/^0*//;
	}
				
	$merchant_information{'status'} = 1;
				
	$MerchantAffiliates = MerchantAffiliates->new($db);
					
					
	$merchant_information{'merchant_master_id'} = $MerchantAffiliates->createMerchant(\%merchant_information, $credit_new_members_club_account);
	$MerchantAffiliates->updateVendorStatusByMerchantID($merchant_information{'merchant_master_id'} , 1);
					
#	warn "short-reg:proc: 16" . Dumper(\%missing_fields) . Dumper(\%merchant_information);;
#	my $html = '';
#	$html = $gs->GetObject(10907);

#
# Convert Languages
#
#warn "Final Page Print: " . Dumper(\%merchant_information);

#	$html = translateLanguage($html, $translation);
#	$html =~ s/%%user%%/$merchant_information{'username'}/g;
#	$html =~ s/%%pass%%/$merchant_information{'password'}/g;
#	$html =~ s/%%merch%%/$merchant_information{'merchant_master_id'}/g;

#	print $CGI->header('-charset'=>'utf-8');	
#	print $html;
	print $CGI->header;
#	$TT->process(\$html, {
	$TT->process('short-registration-finish.tt', {
		'merchant_information'=>\%merchant_information,
		'lb'=>$translation
	}) || print $TT->error();
	exitScript();
}

__END__

=head1
CHANGE LOG
