#!/usr/bin/perl
#
# Given the link created for the opt_out, log the use in and redirect to the merchant cancel page
#
use strict;
use warnings;
use lib "/home/httpd/cgi-lib";
use CGI;
use DB;
use DB_Connect;
use G_S;
use Data::Serializer;
use Data::Dumper;
#
my $cgi = CGI->new();
my $G_S = G_S->new();
my ($db, @header_cookies);
#
# Set up the info for decrypting the url
#
my $obj = Data::Serializer->new();

$obj = Data::Serializer->new(
                         serializer => 'Storable',
                         digester   => 'MD5',
                         cipher     => 'DES',
                         secret     => 'PoHsBuLc1955',
                         compress   => 1,
		       );

my $serialized = 0;

if ($cgi->param('mystuff')) {
    $serialized = "^Storable|DES|MD5|hex|Compress::Zlib^" . $cgi->param('mystuff'); 
}

my $params = $obj->deserialize($serialized);

#warn Dumper($params);

if (! exists $params->{username}) {
    print
          $cgi->header(-expires=>'now'),
          $cgi->start_html(-bgcolor=>'#ffffff', -title=>'Error');
    print "<br />The Information in you link to this process is corrupted<br />";
    print $cgi->end_html();
    exit;
}

my $url="/mafLOGIN?credential_0=$params->{username}&credential_1=$params->{password}&credential_2=$params->{merchantid}&credential_3=maf&destination=/maf/cancel.shtml";
warn "URL: " . $url;
print $cgi->redirect(-URL=>$url);
#
# thats it in a nut shell.
#
