#!/usr/bin/perl -w
###### EmailTemplates.cgi
###### A simple interface for presenting templated emails, prepopulated with certain bits of data
###### created: 04/09/14	Bill MacArthur
###### last modified: 05/12/14	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::LibXML::Simple;
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;
use EmailTemplates::Master;
binmode STDOUT, ":encoding(utf8)";

my ($lb, $errstr, $rels) = ();
my %TMPL = (
	'language_pack' => 11077	# see EmailTemplates::Master for the "wrapper" template
);

my $db = DB_Connect('EmailTemplates.cgi') || exit;
$db->{'RaiseError'} = 1;
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});

LoadLB();
# for starters, we are going to expect that the user is a logged in Partner
my ($id, $mem_info) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
unless ( $id && $id !~ /\D/ )
{
	$errstr = $lb->{'badlogin'};
}
elsif ($mem_info->{'membertype'} ne 'v')
{
	$errstr = $lb->{'notpartner'};
}

if ($errstr) {
	Err($errstr);
	Exit();
}

# look to see if we have a template ID as our first path part, if not look for an 'e' parameter
# look to see if we have a recipient ID as our second path part, if not look for an 'r' parameter
# the first $tmp is always null as there is nothing preceding the first slash in the path
my ($tmp, $e, $r, $forcedRelationShip) = split('/', $q->path_info);
$e ||= $q->param('e');
$r ||= $q->param('r');
die "Empty email template ID" unless $e;
die "Invalid email template ID" if $e =~ m/\D/;
die "The recipient ID is empty" unless $r;
die "The recipient ID parameter is invalid" if $r =~ m/\D/;

my $emt = InstantiateEMT();

$forcedRelationShip = '' unless $gs->in_array($forcedRelationShip, [qw/exec coach sponsor/]);

if ($forcedRelationShip)
{
	$rels->{$forcedRelationShip} = 1;
}
else
{
	$rels = $db->selectrow_hashref("SELECT sponsor, coach, exec FROM my_sponsor_coach_exec WHERE id= ?", undef, $emt->r()->{'id'});
	foreach (keys %{$rels})
	{
		# if the user does not fill the particular relationship, then set that key to 0 so that we can recognize that the relationship does not exist
		$rels->{$_} = 0 if $rels->{$_} != $id;
	}
}

$emt->setRelationships($rels);

$emt->Display() || Err( $emt->errstr() );

Exit();

###### beginning of sub-routines

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub InstantiateEMT
{
	my $rv = ();

	my $className = 'EmailTemplates::emt' . $e;
	eval("use $className;");

	$className = 'EmailTemplates::Master' if $@;	# if we could not "use"... find/compile a related sub-class, we'll just use the Master class
	
	$rv = $className->new({'db'=>$db, 'gs'=>$gs, 'cgi'=>$q, 'lb'=>$lb, 'e'=>$e, 'r'=>$r, 'u'=>$id}) || Err( $className->errstr() );

	return $rv;
}

sub LoadLB
{
	$lb = $gs->GetObject($TMPL{'language_pack'}) || warn "Failed to load language pack template: $TMPL{'language_pack'}";
	$lb = XMLin($lb, 'NoAttr'=>1);
}

=head5 Overview

	The idea is that we are called and we identify the party by means of the full login cookie.

	A call to the script should look like this:
	EmailTemplates.cgi/email-ID-number/recipient-numeric-ID-number
	
	Alternatively we can also use parameterized values on the 'e' parameter amd the 'r' parameter
	EmailTemplates.cgi?e=email-ID-number;r=recipient-numeric-ID-number
	
=cut

__END__

05/12/15	Added the binmode line to compensate for changes introduced with the latest upgrade of DBD::Pg
