#!/usr/bin/perl -w
###### specials.cgi
######
###### The script to show a list of current SRW merchant specials.
######
###### created: 02/28/05	Karl Kohrt
######
###### modified: 04/04/05	Bill MacArthur
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DBI;
use CGI;
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);

###### GLOBALS
our $DOC_ROOT = '/wsp';
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our %TMPL = (	'list'	=> 10400 );
our $db = '';
our $oid = $q->param('oid') || '';
our $data = '';
our $msg = '';
our @result_list = ();
our ($member_id) = Who_Gets_Credit();
our $VIP = VIPactivity();
our $pk = ($VIP) ? $q->param('detail') : ();	# non-VIPs don't get this

################################
###### MAIN starts here.
################################

# Here is where we try logging into the DB.
unless ($db = DB_Connect( 'specials.cgi' )){exit}
$db->{RaiseError} = 1;

# print the list of specials.
Run_Query( $pk );

# If no records were found.
unless ( @result_list )
{
	Err($q->span({-style=>'color: #ff0000'}, 'No current specials were found.') );
}
elsif ($pk)
{
	###### HTML ize the offer details
	#$result_list[0]->{instructions} =~ s#\n#\n<br />#g;
				
	#$result_list[0]->{instructions} =~
	#	s#(http://www\.clubshop\.com.+)\s*
	#	#("$1<br />" . $q->a({-href=>$1}, $1))#xgse;
	###### display the offer detail
	print 	$q->header(), $q->start_html,
		$result_list[0]->{instructions}, $q->end_html;
}
# If a list of records were found.
else
{
	# Create list report
	Display_Results();
}

END:
$db->disconnect if $db;
exit;

################################
###### Subroutines start here.
################################

###### Display a list.
sub Display_Results
{
	my $html;
	my $results = '';
	my $replace = '';
	my $tmpl = G_S::Get_Object($db, $TMPL{list}) || die "Couldn't open $TMPL{list}";

	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		# Stop uninitialization errors.
		foreach ( keys %{$result} )
		{
			if ( $_ eq 'description' || $_ eq 'vendor_name' )
			{
				$replace = "None";
			}
			else { $replace = "&nbsp;" }
			$result->{$_} ||= $replace;
		}
		$results .= qq!
			<tr><td colspan="4"><hr></td></tr>
			<tr>
			<td>$result->{vendor_name}</TD>
			<td>$result->{description}</TD>
			<td>$result->{end_date}</TD>!;
		#	<td><a href="$DOC_ROOT/$result->{filename}?referer=$member_id"> Go </a>!;
		$results .= qq!<td><a href="$DOC_ROOT/$member_id/$result->{pk}"> Go </a>!;
		$results .= '&nbsp;| ' . $q->a({-href=>$q->script_name . "?detail=$result->{pk}"}, 'Detail')
			if $VIP;
		$results .= qq!</td></tr>!;
	}

	# Print the list to the browser.
	$tmpl =~ s/%%results%%/$results/g;
	$tmpl =~ s/%%message%%/$msg/;
	print $q->header(-expires=>'now'), $tmpl;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br />$_[0]<br />";
}

##### Runs the specified query, pushing the results onto a list.
sub Run_Query
{
	my $pk = shift;
	undef $pk if ($pk && $pk =~ /\D/);	# a simple security measure

	my ($qry, $sth) = ();
	$qry = "
		SELECT s.*, date_trunc('minutes', s.stamp) AS stamp, v.vendor_name
		FROM srw_specials s
		LEFT JOIN vendors v ON s.vendor_id = v.vendor_id 
		WHERE ";

	if ($pk)
	{
		$qry .= "pk = $pk";
	}
	else
	{
		$qry .= "NOW()::DATE BETWEEN s.start_date AND s.end_date
			ORDER BY s.end_date";
	}
	$sth = $db->prepare($qry);

	# All requires no bind variables.
	$sth->execute();
	
	while ( $data = $sth->fetchrow_hashref)
	{
		push (@result_list, $data);
 	}		
	$sth->finish();
}

sub VIPactivity
{
	###### we want to determine if a VIP is doing this
	my $ck = $q->cookie('AuthCustom_Generic');
	return unless $ck;

	###### if they have a good cookie and they are a VIP we'll return their info
	my ($id, $meminfo) = $gs->authen_ses_key( $ck );
	if (	$id &&
		$meminfo &&
		$id !~ /\D/ &&
		$meminfo->{membertype} eq 'v')
	{
		return $meminfo;
	}

	return;
}

sub Who_Gets_Credit
{
	(my $path = $q->path_info) =~ s#^/+##;
	($path) = split /\//, $path;
###### the logic is already handled in the SRWs, so why do it here too
###### we have to send something or the URL breaks
	return $path || 1;

# 	return	$q->param('id') ||
# 		$q->cookie('id') ||
# 		split /\//, $path ||
# 		$q->cookie('refspid') ||
# 		1;
}

###### 03/17/05 added handling for the offer detail to VIPs
###### 03/21/05 removed the Who_Gets_Credit logic
###### 04/04/05 removed the HTMLizer from the instructions