#!/usr/bin/perl -w
###### member_detail.cgi
###### display a member's details to the sponsor or upline Builder
###### Created: 06/01/2004	Bill MacArthur
###### Last modified: 03/12/09	Bill MacArthur

use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw(%URLS);
use DB_Connect;
use MemberDetail;

###### CONSTANTS
###### the min. team level to gain access unless they are the sponsor
my $PRIV_ACCESS_LVL = 40;

###### GLOBALS
our $q = new CGI;
our $ME = $q->url;
our ($db) = ();

################################
###### MAIN starts here.
################################
my $refid = $q->param('refid');
if ((! $refid) || $refid =~ /\D/ || length $refid > 8)
{
	Err("Bad refid received");
	exit;
}


my ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
if (!$id || $id =~ /\D/)
{
	RedirectLogin();
	exit;
}
elsif ($meminfo->{membertype} ne 'v')
{
	Err('These reports are available to VIPs only.');
	exit;
}

unless ($db = DB_Connect( 'member_detail.cgi' )){exit}
$db->{RaiseError} = 1;

Member_Detail() if Privileged();

$db->disconnect;
exit;

################################
###### Subroutines start here.
################################

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now', -charset=>'utf-8'),
		$q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<br />$_[0]<br />";
	print $q->end_html();
}

sub Member_Detail
{
	print $q->header(-expires=>'now', -charset=>'utf-8'),
		$q->start_html(
			-title=>'Member Detail',
			-encoding=>'utf-8',
			-style=>{src=>'/css/MemberDetail.css'}
		),
		MemberDetail::Generate($db, $refid),
		'<p><a style="font-size: 10px;" href="javascript:self.close()">Close window</a></p>',
		$q->end_html;
}
sub Privileged
{
	###### we need to check their qualifications for access
	###### currently they have to be a 'network builder' or higher
	###### or they have to be the direct sponsor

	return 1 if $meminfo->{id} eq (${\G_S::Get_Sponsor($db, $refid)}->{id} || '');

	my $sth = $db->prepare("
		SELECT	id FROM nbteam_vw
		WHERE id= $id AND level >= $PRIV_ACCESS_LVL");
	$sth->execute;
	my $member_id = $sth->fetchrow_array;
	$sth->finish;

	unless ($member_id)
	{
	###### if we don't qualify, we'll look up the current live label
	###### this is better than hard coding it and having to change it
	###### every so often
		$sth = $db->prepare("
			SELECT label FROM nbteam_levels
			WHERE level = $PRIV_ACCESS_LVL");
		$sth->execute;
		my $lbl = $sth->fetchrow_array || "nbteam_level $PRIV_ACCESS_LVL";
		$sth->finish;
		Err("You must be at least an active $lbl level to view a downline's report.");
		return;
	}
	###### now we need to verify that the logged in party is really the upline
	###### of the refid
	elsif (! G_S::Check_in_downline($db, 'members', $id, $refid) )
	{
		Err("$refid was not found in your downline.");
		return;
	}

	return 1;
}


###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff');
	print "Please login <a href=\"$URLS{LOGIN}?destination=$ME\">HERE</a>.<br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

# 08/17/04 reduced the level required for upline to view details 
###### 11/09/06 added the language preference to the detail
###### 09/14/07 added the member_origin to the mix
# 03/12/09 moved the table generation into a module for use elsewhere