#!/usr/bin/perl -w
# cgi_Object_Editor.cgi
# Main page to edit cgi_objects.
# they can then be edited or made inactive.
#
# Created: 08/20/02 Stephen Martin
#
# Last modified: 09/12/12	Bill MacArthur

=head1 cgi_Object_Editor.cgi

=for Commentary

cgi_Object_Editor.cgi
Main page to edit cgi_objects.
they can then be edited or made inactive.

Written by Stephen Martin
August 20th  2002

=cut

use strict;
use DBI;
use CGI;
use Carp;
use locale;
use URI::Escape;
#use Unicode::String;

use lib '/home/httpd/cgi-lib';
use ADMIN_DB;

#my $us = Unicode::String->new();
my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data, $obj_operator, $obj_stamp, $obj_type_description);
my $rv;
my $query;
my $public = '';
my $private = '';
my $spawn;

my $cgi = new CGI();

my $action          = $cgi->param('action') || '';
my $R               = $cgi->param('R');
my $obj_id          = $cgi->param('obj_id'); 
my $obj_name        = $cgi->param('$obj_name'); 
my $obj_description = $cgi->param('obj_description');
my $obj_value       = $cgi->param('obj_value');
my $public_view     = $cgi->param('public_view');
my $obj_type		= $cgi->param('obj_type');
my $obj_translation_key = $cgi->param('translation_key');

my $criteria1 = $cgi->param('criteria1') || '';
my $criteria2 = $cgi->param('criteria2') || '';
my $criteria3 = $cgi->param('criteria3') || '';

my $offset   = $cgi->param('offset') || '';

my $URLcriteria1 = uri_unescape($criteria1);
my $URLcriteria2 = uri_unescape($criteria2);
my $URLcriteria3 = uri_unescape($criteria3);

my $self = $cgi->script_name;
my $SEARCH   = "./HTMLcontent.cgi"  . "?offset=" . $offset . ";criteria1=" . $URLcriteria1  . ";criteria2=" .  $URLcriteria2 . ";criteria3=" . $URLcriteria3;

if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ )
{
 $spawn="\'.\/OEhelp1.cgi\',\'_OEH1_\',\'width=400,height=400,scrollbars=no\'";
} else {
 $spawn="\'.\/OEhelp1.cgi\',\'_OEH1_\',\'width=500,height=500,scrollbars=no\'";
}

# print the page header here so we can view Unicode.
#print "Content-type: text/html\n\n";

# Here is where we get our admin user and try logging into the DB

my $operator = $cgi->cookie('operator');
my $pwd = $cgi->cookie('pwd');
unless ($operator && $pwd)
{
	Err('You must be logged in to use this interface.');
	exit;
}

unless ($db = ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Catch the update request and action it before the record is re-displayed..
if ( $action eq "Update")
{
	$query = "UPDATE cgi_objects
          	  SET obj_description = ?,
                      obj_value       = ?,
                      public_view     = ?,
                      stamp           = now(),
			obj_type = $obj_type,
			translation_key = ${\($obj_translation_key ? 'TRUE' : 'FALSE')}
                  WHERE obj_id        = ?";


	$sth = $db->prepare($query);

# disabled this as of 08/28/14.... not quite sure why he did that, but we need to insert HTML entities sometimes and this reduces them to digits ;(
#	$obj_value=~ s/&#(\d*);/$1/g;	

	$rv = $sth->execute($obj_description,$obj_value,$public_view,$obj_id);
	defined $rv or die $sth->errstr;
        
	if ( $R eq "Archive" )
	{
 	 archive($obj_id);
	}

}

$query = "
	SELECT co.obj_id,
		co.obj_name,
		co.obj_description,
		co.obj_value,
		co.public_view,
		COALESCE(co.operator, '&nbsp;') AS operator,
		COALESCE(co.stamp::TIMESTAMP(0)::TEXT, '&nbsp;') AS stamp,
		co.obj_type,
		cot.type AS obj_type_description,
		co.translation_key
	FROM 	cgi_objects co, cgi_object_types cot
	WHERE 	co.obj_id = ?
	AND 	co.obj_type=cot.id";

$sth = $db->prepare($query);

$rv = $sth->execute($obj_id);
defined $rv or die $sth->errstr;

$sth->bind_columns(
	undef,
	\$obj_id,
	\$obj_name,
	\$obj_description,
	\$obj_value,
	\$public_view,
	\$obj_operator,
	\$obj_stamp,
	\$obj_type,
	\$obj_type_description,
	\$obj_translation_key);

$sth->fetch;
$sth->finish();
###### build a control for the object type	
my $list = $db->selectall_arrayref("
	SELECT id, type FROM cgi_object_types
	ORDER BY id
");

my (@vals, %lbls) = ();
foreach(@$list)
{
	push (@vals, $_->[0]);
	$lbls{$_->[0]} = $_->[1];
}
my $obj_type_block = $cgi->popup_menu(
	-name=>'obj_type',
	-id=>'obj_type',
	-values=>\@vals,
	-labels=>\%lbls,
	-default=>$obj_type,
	-force=>1,
	-class=>'in',
	-onchange=>"if (! confirm('Are you sure your want to convert this object to type: ' + obj_type_desc[this.selectedIndex] + '?')) this.selectedIndex=obj_type"
);
	
$obj_type_block .= qq|
	<script type="text/javascript">
	<!-- 
	var obj_type = document.getElementById('obj_type').selectedIndex;
	var obj_type_desc = new Array();\n|;

for (my $x=0; $x < scalar @$list; $x++)
{
	$obj_type_block .= "obj_type_desc[$x] = \"$list->[$x][1]\";\n";
}

$obj_type_block .= qq|
	-->
	</script>\n|;

         # $obj_name =~ s/[\W]/\&nbsp\;/g;

        if ( $public_view ) 
        {
         $public= 'selected="selected"';
        } else {
         $private= 'selected="selected"';
        }

# Escape the object value in case their are any html characters embedded.
$obj_value = $cgi->escapeHTML($obj_value);

# now get the callers/functions that use this template and build a list
my $callers = $db->selectall_arrayref('
	SELECT cof.process_name
	FROM cgi_objects_with_extended_function_mapping co
	JOIN cgi_object_functions cof ON cof.id=co.obj_caller
	WHERE co.obj_id=?
	ORDER BY 1', undef, $obj_id);
my $caller_list = '';
$caller_list .= "$_->[0]<br />" foreach @{$callers};
$caller_list =~ s#<br />$##;

print $cgi->header('-expires'=>'now', '-charset'=>'utf-8');
print qq~
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<link href="/css/memberinfo.cgi.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/yui/yahoo-min.js"></script>
<script type="text/javascript" src="/js/yui/event-min.js"></script>
<script type="text/javascript">

function spawn(URL,Name,features)
 { 
  window.open(URL,Name,features);
 }

function previewPage(template_number)
{ 
	nuwin = window.open('https://www.clubshop.com/cgi/admin/template_preview.cgi/' + template_number,'Preview');
}

function selectText()
{
	var ctl = document.getElementById('obj_value');
  ctl.focus();
  ctl.select(); 
 return false;
}


 function validate()
 {
	var ctl = document.getElementById('obj_description');
  if ( ctl.value == "" )
  {
    alert('Please enter a description for this template.');
    ctl.focus();
    return false;
  }
  return true;
 }

function init(){
// this doesn't work	document.getElementById('obj_value').wrap = 'off';	
}
</script>

<style type="text/css">
	
.lbl {
	font-size: 12px;
	color: #000000;
	font-weight: bold;
	font-family:  Arial, Helvetica, sans-serif;
	text-decoration: underline;
}

.lbl_data {
	font-size: 12px;
	color: #000000;
	font-weight: normal;
	font-family:  Arial, Helvetica, sans-serif;
	text-decoration: none;
}
						
</style>		

	<title>$obj_name - DHSC CGI Object Editor</title>
</head>
<body bgcolor="#eeffee" onload="init()">

<form action="$self" method="post" onsubmit="return validate();">

<input type="hidden" name="criteria1" value="$criteria1" />
<input type="hidden" name="criteria2" value="$criteria2" />
<input type="hidden" name="criteria3" value="$criteria3" />

<div align="center">
<b>DHSC CGI Object Editor</b><br />
</div>
<div align="right">
<a href='$SEARCH'><font style="font-family: Arial, Helvetica, sans-serif; font-size: 10px">Return to Search Results</font></a>
</div>

   <hr align="center" />
\n~;

print qq~
         <!-- DB Call -->
          <input type="hidden" name="obj_id" value="$obj_id" />
          <table border="0" align="center" cellpadding="3" style="width:100%">
          <tr>
          <td class="lbl">Object Name:</td>
          <td class="lbl">Used By:</td>

          <td class="lbl">Object ID</td>

          <td class="lbl" align="center">Last Changed:</td>

	<td class="lbl">By:</td>		

	<td nowrap="nowrap" align="right">
	</td>
          </tr>

          <tr>
          <td class="lbl_data" valign="top">$obj_name</td>
		<td>$caller_list</td>
          <td class="lbl_data" valign="top">$obj_id</td>

	<td class="lbl_data" align="center" valign="top">$obj_stamp</td>
	
	<td class="lbl_data" valign="top">$obj_operator</td>

          </tr>

<tr><td colspan="2">
<span class="lbl" style="padding-right:2em">Object Type: </span>$obj_type_block
</td>
<td colspan="3">
<span class="lbl" style="padding-right:1em">Translation Key?</span>
${\($cgi->checkbox(-name=>'translation_key', -label=>'', -value=>1, -checked=>$obj_translation_key, -force=>1))}
${\($cgi->a({	-style=>'padding-left:3em',
		-href=>"/cgi/admin/translation_templates.cgi?action=view&trans_type=1&obj_key=$obj_id",
		-target=>'_blank',
		-class=>'fp'}, 'View Translation List'))}
</td>
</tr>
        
         <tr>
          <td colspan="5" class="lbl">Description</td>
         </tr>

         <tr>
          <td colspan="5" align="center">
	<textarea name="obj_description" style="width: 100%;" cols="90" rows="10">$obj_description</textarea>
          </td>
          </tr>

	<tr>
          <td colspan="3" class="lbl">Content</td>
         <td valign="top" colspan="2">
          <input type="button" class="in" value="Highlight Content" onclick="selectText();" />&nbsp;&nbsp;&nbsp;&nbsp;

         <input type="button" class="in" value="Preview Page" onclick="previewPage($obj_id);" />&nbsp;&nbsp;&nbsp;

           <a href="#" onclick="spawn($spawn);">
          <img src="/admin/images/talkbubble3_query.gif" width="24" height="24" border="0" alt="" /></a>
         </td>
         </tr>

          <tr>
          <td colspan="5" align="center">
	<textarea name="obj_value" wrap="off" style="width: 100%;" cols="90" rows="25" id="obj_value">$obj_value</textarea>
          </td>
          </tr>

           <tr>
          <td colspan="5" class="lbl">Public or Private</td>
         </tr>

          <tr>
          <td colspan="3">
           <select name="public_view">
            <option value="1" $public>PUBLIC</option>
            <option value="0" $private>PRIVATE</option>
           </select>
         </td>
         <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <input type="radio" value="Archive" name="R" checked="checked" />Force Archive of this Template Definition.
         </td>
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
          <input type="radio" value="XArchive" name="R" />Do <b>NOT</b> Archive.
          </td>
         </tr>

          <tr>
          <td colspan="5">
           <hr align="center" />
         </td>
         </tr>
         <!-- DB Call -->
\n~;

print qq~
  <tr>
   <td colspan="5" align="right">
<input type="reset" style="color:#cc0000" class="in" />&nbsp;&nbsp;&nbsp;
<input type="submit" class="in" value="Update" name="action" style="color:#006600" />
&nbsp;&nbsp;&nbsp;
    <a href='$SEARCH'><font 
      style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Return to Search Results</font></a>
   </td>
  </tr>
</table>
</form>
</body>
</html>
\n~;


$db->disconnect;

exit;

sub archiveOLD{
    my $this_obj_id = $_[0];
    my $this_obj_name;
    my $this_obj_description;
    my $this_obj_value;
    my $this_obj_type;
    my $this_obj_caller;
    my $this_active;
    my $this_public_view;
    my $this_timestamp;

    my $psql = "	SELECT DISTINCT obj_name,
				obj_description,
				obj_value,
				obj_type,
				obj_caller,
				active,
				public_view,
				stamp
			FROM cgi_objects 
			WHERE obj_id = ?";
   
    $sth = $db->prepare($psql);

    $rv = $sth->execute($this_obj_id);
    defined $rv or die $sth->errstr;

    $sth->bind_columns(undef, \$this_obj_name,
                              \$this_obj_description, 
                              \$this_obj_value, 
                              \$this_obj_type,
                              \$this_obj_caller,
                              \$this_active,
                              \$this_public_view,
                              \$this_timestamp);

    $sth->fetch();
    
    $psql = "INSERT INTO cgi_objects_arc 
             ( obj_id, 
               obj_name, 
               obj_description, 
               obj_value,
               obj_type, 
               obj_caller, 
               stamp 
             ) VALUES ( ?,?,?,?,?,?,?)";


     $sth = $db->prepare($psql);

    $rv = $sth->execute($this_obj_id, 
                        $this_obj_name,
                        $this_obj_description,
                        $this_obj_value,
                        $this_obj_type,
                        $this_obj_caller,
                        $this_timestamp);
    defined $rv or die $sth->errstr;

}

sub archive
{
	###### let's just make this a SQL statement and keep it simple
	my $rv = $db->do("
		INSERT INTO cgi_objects_arc (
			obj_id, obj_name, obj_description, obj_value,
			obj_type, obj_caller, stamp)
		SELECT obj_id, obj_name, obj_description, obj_value,
			obj_type, obj_caller, stamp
		FROM cgi_objects WHERE obj_id= ?",
	undef, $_[0]);
	Err("Archiving failed") unless $rv eq '1';
}

sub Err
{
	print $cgi->header('-expires'=>'now', '-charset'=>'utf-8'), $cgi->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error', '-encoding'=>'utf-8');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $cgi->end_html();
}

#sub Unicode_Convert
#{
#	my $c=shift;
#	$us->pack($c);
#	return $us->utf8();
#}	

###### 05/29/03 Added the preview button.
###### 07/23/03 Added the upper 'Return to Search Results link.
###### 08/19/03 revised the preview window opener to not define the window parameters
###### 08/22/03 revised the size of the textarea for editting\viewing objects
###### 12/22/03 Commented out the first of two calls to the archive subroutine.
###### 02/10-11/04 fixed unit'd var errors and added functionality to display the last
###### modified date and operator
###### 04/30/04 explicitly defined the wrap for the object textarea to be 'off'
###### this makes it a bit awkward, but more accurately represents textual messages
###### 07/28/04 revised the javascript for selecting text to do it in every case
###### instead of after a browser test
###### 09/24/04 Added the Unicode conversion for foreign languages.
###### 04/22/05 made the archiving routine a simple SQL statement called *before* the update
###### 09/22/05 moved the printing of the top of the document below the queries and
###### control creation stuff so that the object name could be included in the HTML title
###### 01/25/06 Commented out utf8 conversion references and sub Unicode_Convert
