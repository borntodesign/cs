#!/usr/bin/perl -w
# pwp-admin.cgi
# admin search and administration for Personlized Web Pages (PWP)
# created Jan. 2008	Bill MacArthur
# last modified: 
#               02-09-2011 g.baker posgres 9.0 SUBSTR arg 1 must be text

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use G_S;

our (@alerts, @errors, @msg, $db, %params) = ();
###### the keys are the search types, the values are the labels for that option
###### and if search terms are required
###### the text to display if they are not submitted
my %search_types = (
	byid => {sort=>1, label=>'By ID', terms=>'An ID must be submitted'},
	waiting => {sort=>2, label=>'Awaiting approval (Complete)', terms=>''},
	disapproved => {sort=>3, label=>'Disapproved', terms=>''},
	keyword => {sort=>5, label=>'By keyword', terms=>'Keyword must be submitted'},
	# this indicates records recently changed/created
	recent => {sort=>10, label=>'Last changed within X days', terms=>'You must enter a digit/s for the number of days back you want to go'},
	by_page_name => {sort=>12, label=>'By Page Name', terms=>'You must enter a page name (case insensitive)'}
);

my %sort_types = (
	1 => {label=>'ID', sort=>'pwp.id'},
	3 => {label=>'Last Modified', sort=>'last_modified'},
	4 => {label=>'Approval Status', sort=>'approval_status'},
	5 => {label=>'Approval Coach', sort=>'pwp.coach'},
	7 => {label=>'Page Name', sort=>'pwp.page_name'}
);
my $q = new CGI;

unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}

exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;
LoadParams();
unless ($params{_action}) {
	# just the search interface
	Display();
}
elsif ($params{_action} eq 'update') {
	Update();
	Display();
}
else { push @errors, "Unrecognized action parameter: $params{_action}"; }

END:
Err() if @errors;
$db->disconnect;
exit;

sub cboAscDesc
{
	my %names = map {$_=>$search_types{$_}{label}} keys %search_types;
	my @vals = sort {$search_types{$a}{sort} <=> $search_types{$b}{sort}}keys %search_types;
	return $q->popup_menu(
		-name=>'ascdesc',
		-values=>['ASC','DESC'],
		-labels=>{ASC=>'Ascending', DESC=>'Descending'}
	);
}

sub cboSearchType
{
	my %names = map {$_=>$search_types{$_}{label}} keys %search_types;
	my @vals = sort {$search_types{$a}{sort} <=> $search_types{$b}{sort}}keys %search_types;
	return $q->popup_menu(
		-name=>'qtype',
		-values=>\@vals,
		-labels=>\%names
	);
}

sub cboSortType
{
	my %names = map {$_=>$sort_types{$_}{label}} keys %sort_types;
	my @vals = sort keys %sort_types;
	return $q->popup_menu(
		-name=>'sort',
		-values=>\@vals,
		-labels=>\%names
	);
}

sub cboStatus
{
	my $xclass = $_[0] == -1 ? 'disapproved' : $_[0] == 0 ? 'waiting' : 'approved';
	return $q->popup_menu(
		-class=>"status $xclass",
		-name=>'status',
		-values=>[0,1,-1],
		-labels=>{'0'=>'Waiting', '1'=>'Approved', '-1'=>'Disapproved'},
		-default=>$_[0],
		-force=>1
	);
}

sub Display
{
	print $q->header(-charset=>'utf-8', -expires=>'now');
	print $q->start_html(
			-encoding => 'utf-8',
			-title => 'PWP Administration',
			-style => {-code=>'
				@import url(/css/admin/mall-mgt.css);
				th { padding: 0.2em 1em 0.1em 0.3em; font:bold 75% sans-serif; text-align:left;}
				.thc { text-align:center; padding: 0 .3em .1em .3em; }
				td { vertical-align:top; font:normal 70% sans-serif; }
				td.tdnote { text-align:center; }
				table.report td { border:1px solid silver; padding:0.2em 0.3em;
					border-right-style:dotted; border-left-style:dotted; }
				table.report { border-collapse:collapse; }
				table#searchfrm td { padding-right:1em; }
				tr.a { background-color:rgb(255,255,240);}
				tr.b { background-color:#efefef;}
				tr.void { color:#999;}
				input,select { font-size:100%; }
				.fpnotes { font:normal smaller sans-serif; }
				table.report td.ctl { border-right:0; }
				select.status { margin:0 1em; }
				td.ctl form { display:inline; }
				.error { color:red; }
				select.approved { background-color:rgb(240,255,240); }
				select.disapproved { background-color:#fcc; }
				select.waiting { background-color:#eef; }
				textarea { margin-bottom:0.5em; vertical-align:top; font-family:sans-serif;
					font-size:100%; }
				textarea.txshow { display:inline; }
				textarea.txhide { display:none; }
				span.gr { color:green; }
			'},
			-script => {-src=>'/js/admin/pwp-admin.js'},
	);
	print q|<div id="instructions">Click the Notes link to toggle the Notes field.
		<br />Voided pages are unavailable / invisible to the VIP or their upline.
		<br />Voided pages are unavailable at the glocalgeneration web site.
		<br /><b>C</b> = 'Complete' - Incomplete pages are only available to the VIP.
		</div>|,
		$q->h4('Personalized Web Page Administration'),
#		$q->start_form(-method=>'post', -action=>$q->script_name, -enctype=>'application/x-www-form-urlencoded', -id=>'mainfrm'),
		q|<form method="get" action="|, $q->script_name, q|">|,
		$q->table({-id=>'searchfrm'},
			$q->Tr(
				$q->td('Search Type:<br />' . cboSearchType()) .
				$q->td('Search Values <span class="fpnotes">(as needed)</span><br />' .
					$q->textfield(-name=>'search_terms')
				) .
				$q->td('Order By:<br />' . cboSortType()) .
				$q->td({-style=>'vertical-align:bottom'}, cboAscDesc()) .
				$q->td({-style=>'vertical-align:bottom'}, $q->submit)
			)
		),
		$q->end_form, $q->hr;

	###### from here we can present a list
	SearchResults();

	###### now create a hidden form which will serve as our update form
	print q|<form method="get" action="|, $q->script_name, q|" id="mainfrm">|,
		$q->hidden(-force=>1, -name=>'_action', -value=>''),
		$q->hidden(-force=>1, -name=>'void', value=>''),
		$q->hidden(-force=>1, -name=>'status', value=>''),
		$q->hidden(-force=>1, -name=>'pk', value=>''),
		$q->hidden(-force=>1, -name=>'notes', value=>''),
	###### the next four serve to make the search values sticky to that the page will reload the same query
		$q->hidden('ascdesc'),
		$q->hidden('qtype'),
		$q->hidden('sort'),
		$q->hidden('search_terms'),
		$q->end_form, $q->end_html
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub FilterWhiteSpace {
	return unless defined $_[0];
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub LoadParams
{
	foreach('qtype', 'search_terms', '_action', 'sort', 'ascdesc', 'void', 'status', 'pk', 'notes')
	{
		$params{$_} = FilterWhiteSpace( $q->param($_) );
	}
}

sub printRow
{
	my ($tmp, $class) = @_;
	$class = 'void' if $tmp->{void};
	my $txclass = $tmp->{notes} ? 'txshow' : 'txhide';
	my $note = $tmp->{notes} ? '<b>Yes</b>' : 'Add';
	print $q->Tr({-class=>$class},
		$q->td( $q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$tmp->{id}"}, $tmp->{alias}) ) .
		$q->td( $q->a({-href=>"/cgi/pwp-preview.cgi?pk=$tmp->{pk}"}, $tmp->{page_name}) ) .
		$q->td( $tmp->{last_modified} ) .
		$q->td( $tmp->{operator} ) .
		$q->td( $tmp->{coach_alias} ?
			$q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$tmp->{coach_id}"}, $tmp->{coach_alias}) : '' ) .
		$q->td({-class=>'tdnote'}, $tmp->{complete} ? $q->span({-class=>'gr'}, 'Y') : '' ) .
		$q->td({-class=>'tdnote'},
			$q->a({-href=>'#', -class=>'note', -onclick=>"toggleNote($tmp->{pk})"}, $note) ) .
		$q->td({-class=>'ctl'}, $q->start_form(-action=>'#', -onsubmit=>'return subFrmSubmit(this)') .
			$q->textarea(-name=>'notes', -force=>1, -cols=>25, -rows=>2, -class=>$txclass,
				-id=>"tx$tmp->{pk}", -value=>$tmp->{notes}) .
			$q->checkbox(-label=>'', -name=>'void', -value=>1, -checked=>$tmp->{void}, -force=>1) .
			cboStatus($tmp->{approval_status}) . $q->submit(-value=>'Make Changes') . 
			$q->hidden(-name=>'pk', -value=>$tmp->{pk}, -force=>1)  . $q->end_form )
	);
}

sub SearchResults
{
	return '' unless $params{qtype};
	my ($error, @args, $Criteria) = ();
	my $Sort = $sort_types{$params{sort} || 1}{sort};
	my $Direction = $params{ascdesc} =~ /^ASC|DESC$/ ? $params{ascdesc} : 'ASC';
	unless ($search_types{$params{qtype}}){
		$error = "Invalid qtype: $params{qtype}";
	}
	elsif ($search_types{$params{qtype}}{terms} && ! $params{search_terms}){
		$error = $search_types{$params{qtype}}{terms};
	}
	elsif ($params{qtype} eq 'byid'){
		###### if they submitted an alias, convert it
		my $id = $params{search_terms};
		$id = G_S::Alias2ID($db, 'members', $id) if $id =~ /\D/;
		unless ($id){ $error = "Failed to find a membership matching: $params{search_terms}"; }
		else {
			$Criteria = "pwp.id=$id";
		}
	}
	elsif ($params{qtype} eq 'waiting'){
		$Criteria = q|m.membertype='v' AND pwp.complete=TRUE AND pwp.approved IS NULL|;
	}
	elsif ($params{qtype} eq 'disapproved'){
		$Criteria = q|m.membertype='v' AND pwp.approved = FALSE|;
	}
	elsif ($params{qtype} eq 'keyword'){
		$Criteria = 'pwp.profile_xml ~ ?';
		push @args, $params{search_terms};
	}
	elsif ($params{qtype} eq 'recent'){
		###### we should only have digits
		if ($params{search_terms} =~ /\D/){ $error = $search_types{$params{qtype}}{terms}; }
		else {
			$Criteria = "pwp.stamp >= NOW()::DATE - $params{search_terms}";
		}
	}
	elsif ($params{qtype} eq 'by_page_name'){
		$Criteria = 'pwp.page_name ~* ?';
		push @args, $params{search_terms};
	}
	if ($error){
		print $q->p({-class=>'error'}, $error);
		return;
	}

	my $sth = $db->prepare("
		SELECT pwp.pk, pwp.id, m.alias, pwp.void, pwp.notes, pwp.language_code, pwp.page_name,
			pwp.operator, pwp.complete,
			COALESCE(mc.alias, '') AS coach_alias, COALESCE(mc.id::TEXT, '') AS coach_id,
			SUBSTR((CASE WHEN pwp.stamp >= COALESCE(pwi.stamp, pwp.stamp) THEN pwp.stamp
				ELSE pwi.stamp
			END)::TEXT, 0, 20) AS last_modified,
			CASE WHEN pwp.approved IS NULL THEN 0::INT
				WHEN pwp.approved=FALSE THEN -1::INT
				ELSE 1::INT
				END AS approval_status

		FROM pwp_pages pwp
		INNER JOIN members m ON m.id=pwp.id
		LEFT JOIN members mc ON mc.id=pwp.coach
		LEFT JOIN pwp_images pwi ON pwi.pk=pwp.pk
		WHERE $Criteria
		ORDER BY $Sort $Direction
	");
	$sth->execute(@args);
	my $class = 'a';
	if (my $tmp = $sth->fetchrow_hashref){
		print q|<table class="report">|;
		print q|<tr><th>ID</th><th>Page Name</th><th>Last Modified</th>
			<th>By</th><th>Coach</th><th class="thc">C</th>
			<th class="thc">Notes</th><th>Void - Approval Status</th></tr>|;
		printRow($tmp, $class);
		$class = 'b';
		while ($tmp = $sth->fetchrow_hashref)
		{
			printRow($tmp, $class);
			$class = $class eq 'a' ? 'b':'a';
		}
		print "</table>\n";
	}
	else {
		print $q->h5('No matching records found');
	}
}

sub Update {
	die "Missing required pk parameter\n" unless $params{pk};
	die "Invalid pk param: $params{pk}\n" if $params{pk} =~ /\D/;
	my $status = {'0'=>'NULL', '1'=>'TRUE', '-1'=>'FALSE'}->{$params{status}} ||
		die "Invalid status param: $params{status}\n";
	my $void = {'0'=>'FALSE', '1'=>'TRUE'}->{$params{void}} ||
		die "Invalid void param: $params{void}\n";

	###### we are going to pass an undef instead of empty notes so that the field will go NULL
	my $rv = $db->do("UPDATE pwp_pages SET approved= $status, void= $void, notes= ? WHERE pk= $params{pk}",
		undef, ($params{notes} || undef));

	die "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return;
}








