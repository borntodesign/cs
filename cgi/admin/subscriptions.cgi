#!/usr/bin/perl -w
###### subscriptions.cgi
###### admin script for managing subscriptions
###### created July 2008		Bill MacArthur
###### last modified: 06/08/15	Bill MacArthur

use strict;
use Template;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw(%MEMBER_TYPES);
use ADMIN_DB;
require cs_admin;
use Subscriptions::Partner;
#use Data::Dumper;	# for debugging only
binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
my $q = new CGI;
$q->charset('utf-8');
my ($db, %params) = ();
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $SP = Subscriptions::Partner->new('db'=>\$db);
my $TT = Template->new({'ABSOLUTE' => 1});

#my %TMPL = (
#	'tt' => 10604
#);

if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
# 	if (&ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1){
# 		Err($q->h3('You must be a member of the accounting group to use this form'));
# 		goto 'END';
# 	}
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}
$db->{'RaiseError'} = 1;

my @req = 'id';
my @opt = qw/_action xml/;
LoadParams(\@req, \@opt);	# these are the initial parameters we might work with
							# additional params are loaded below on an as needed basis

unless ($params{'_action'})
{
#	Gather_Data();
}
elsif ($params{'_action'} eq 'cancel')
{
	@req = qw/pk ca/;
	@opt = 'notes';
	my $tbl = ();
	LoadParams(\@req, \@opt);
	
	# we need to determine if this pk is in the live or archive table
	my $subscr = $db->selectrow_hashref("SELECT pk, subscription_type, soa_trans_id, void FROM subscriptions WHERE pk= $params{'pk'}");

	if ( $subscr->{'pk'} )
	{
		$tbl = 'subscriptions';
	}
	else
	{
		$subscr = $db->selectrow_hashref("SELECT pk, subscription_type, soa_trans_id FROM subscriptions_archive WHERE pk= $params{'pk'}");
		$tbl = 'subscriptions_archive' if $subscr->{'pk'};
	}
	die "Failed to obtain subscription information\n" unless $tbl;
	die "The subscription with SID: $subscr->{'pk'} is already canceled" if $subscr->{'void'};

	my ($renewal) = $db->selectrow_array("SELECT renewal FROM subscription_types WHERE subscription_type= $subscr->{'subscription_type'}");
	
	$db->begin_work;
	$db->do("UPDATE $tbl SET void=TRUE, notes=? WHERE void=FALSE AND member_id= $params{'id'} AND pk= $params{'pk'}", undef, $params{'notes'});
	if ($params{'ca'} eq 'offset' && $subscr->{'soa_trans_id'})
	{
		my $soa_type = $renewal == 1 ? 11000 : 11001;
		$db->do("
			INSERT INTO soa (id, trans_type, amount, description, reconciled, memo)
			SELECT id, $soa_type, (amount * -1), ?, TRUE, ?
			FROM soa
			WHERE trans_id= $subscr->{'soa_trans_id'}", undef, "Cancellation of $params{'id'} : $params{'pk'}", $params{'notes'});
	}

	$db->commit;
}
elsif ($params{'_action'} eq 'extend')
{
	@req = qw/subscription_type expiration_date native_amount pp_value/;
	@opt = qw/ca_trans_id notes/;
	LoadParams(\@req, \@opt);

	ExtendSubscription();
}
elsif ($params{'_action'} eq 'create')
{
	@req = qw/subscription_type expiration_date native_amount pp_value/;
	@opt = qw/ca_trans_id notes/;
	LoadParams(\@req, \@opt);

	CreateSubscription();
}
elsif ($params{'_action'} eq 'update_expy')
{
	@req = qw/pk expiration_date/;
	@opt = qw/notes/;
	LoadParams(\@req, \@opt);
	my $rv = $db->do('UPDATE subscriptions SET end_date=?, notes=? || end_date::TEXT, operator=current_user WHERE pk=? AND member_id=?',
		undef, $params{'expiration_date'}, "$params{'notes'} - former expiration: ", $params{'pk'}, $params{'id'});
	if ($rv ne '1')
	{
		Err("Subscription: $params{'pk'} was not updated");
		Exit();
	}
}
elsif ($params{'_action'} eq 'UpDownGrade')
{
	@req = qw/pk subscription_type/;
	@opt = qw/notes/;
	LoadParams(\@req, \@opt);
	my ($rv) = $db->selectrow_array("SELECT alter_partner_subscription_in_place($params{'pk'}, $params{'subscription_type'}, ?)", undef, $params{'notes'});
	if (! $rv)
	{
		Err("Subscription: $params{'pk'} was not updated");
		Exit();
	}
}
else
{
	Err("Invalid _action parameter: $params{'_action'}");
	Exit();
}

Render_Page();

Exit();

################# start of subroutines ######################

sub _CreateSubscription
{
	my ($replacement, $member) = @_;
	unless ($replacement)
	{
		Err("Failed to obtain details for that subscription type: $params{'subscription_type'}");
		Exit();
	}
	
	# roll this into a transaction
	$db->{'AutoCommit'} = 0;
	
	# they could have a voided or expired subscription in place, so we will start by deleting the equivalent type of subscription that we are creating
	# the delete operation is actually part of the logic involved in the DB proc
	my ($sid) = $db->selectrow_array("
		SELECT create_member_subscription_by_date_range(?,?,?,?,?,?)", undef,
		$member->{'id'}, $replacement->{'subscription_type'}, $replacement->{'start_date'}, $params{'expiration_date'}, $params{'native_amount'}, $params{'pp_value'}) ||
			die "Failed to create a subscription with these values
				$member->{'id'}, $replacement->{'subscription_type'}, $replacement->{'start_date'}, $params{'expiration_date'}, $params{'native_amount'}, $params{'pp_value'}\n";

	my $scr = $db->selectrow_hashref("SELECT * FROM subscriptions WHERE pk=$sid");
	unless ($params{'ca_trans_id'})
	{
		($params{'ca_trans_id'}) = $db->selectrow_array(qq!
			SELECT debit_acct_create_trans($member->{'id'}, $replacement->{'soa_trans_type'}, ($scr->{'usd'} * -1), 'Subscription #$sid'::VARCHAR,
				NULL::INTEGER, "current_user"()::VARCHAR)!) || die "Failed to create a Club Account transaction for this entry\n";
	}

	$db->do("UPDATE subscriptions SET soa_trans_id=?, notes=? WHERE pk=$sid", undef, $params{'ca_trans_id'}, $params{'notes'});
	$db->do("UPDATE soa SET description= 'Member id: $member->{'id'}, Subscription ID: $sid\n' || description WHERE trans_id=?", undef, $params{'ca_trans_id'}) if $params{'ca_trans_id'};
	$db->{'AutoCommit'} = 1;
	
	# with a new subscription in place we need to flush our subscriptions cache
	$SP->FlushCache();
}

sub CreateSubscription
{
	my $member = GetMember();
	my $replacement = GetSubscriptionMatch( $SP->MonthlyReplacements($member) );
	$replacement ||= GetSubscriptionMatch( $SP->AnnualReplacements($member) );
	unless ($replacement)
	{
		Err("Failed to obtain details for that subscription type: $params{'subscription_type'}");
		Exit();
	}
	
	_CreateSubscription($replacement, $member);
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub Err
{
	print $q->header('-expires'=>'now'), $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub ExtendSubscription
{
	# monthlies first
	my ($renewal) = ();
	if (grep {$_ == $params{'subscription_type'}} (3,41,43,45,47,49))	# these are the monthly renewals
	{
		$renewal = GetMonthlyRenewal();
	}
	elsif ($params{'subscription_type'} == 4)							# this is the annual renewal
	{
		$renewal = GetAnnualRenewal();
	}
	else
	{
		Err("Programming for this subscription_type is not setup yet: $params{'subscription_type'}");
		Exit();
	}
	die "Failed to obtain renewal details\n" unless $renewal;
	
	my $member = GetMember();
	_CreateSubscription($renewal, $member);
}

sub Gather_Data
{
	my %rv = ();
	$rv{'member'} = GetMember();
	$SP->setMemberID($rv{'member'}->{'id'});
	
	$rv{'subscriptions'}->{'current'} = $SP->Live();
#		-- The idea behind 'frozen' in this context, is to make the interface NOT provide a means of canceling/voiding a subscription
#		-- The status_flag is NULL=expired, 1=Current, 0=Approaching Anniversary (for annual subscriptions

	$rv{'subscriptions'}->{'archive'} = $gs->select_array_of_hashrefs("
		SELECT s.*, p.frozen, t.void AS pp_transaction_voided, soa.id AS soa_id
		FROM subscriptions_archive s
		LEFT JOIN ta_live ta
			ON s.transactions_trans_id=ta.transaction_ref
			AND s.member_id=ta.id
		LEFT JOIN periods p ON ta.period=p.period
		LEFT JOIN soa ON soa.trans_id=s.soa_trans_id
		LEFT JOIN transactions t ON t.trans_id=s.transactions_trans_id
		WHERE s.member_id= $rv{'member'}->{'id'}
		ORDER BY s.pk DESC");

	$rv{'subscription_status'}->{'renewals'}->{'monthly'} = $SP->GetMonthlyRenewal();
		
	# see if any new monthly subscriptions are available (like when they do not have a monthly)
	$rv{'subscription_status'}->{'replacements'}->{'monthly'} = $SP->MonthlyReplacements($rv{'member'});
	
	$rv{'subscription_status'}->{'renewals'}->{'annual'} = $SP->GetAnnualRenewal();
	
	# go through the list of current subscriptions and see if one of them is an annual
	$rv{'subscription_status'}->{'replacements'}->{'annual'} = $SP->AnnualReplacements($rv{'member'});
	
	$rv{'dates'} = $db->selectrow_hashref('SELECT NOW()::DATE AS "now", first_of_month() AS "first_of_month", last_of_month() AS "last_of_month"');

	$rv{'club_account_balance'} = $db->selectrow_array("SELECT SUM(amount) FROM soa WHERE void=FALSE AND id= $rv{'member'}->{'id'}") || 0;

	return %rv;
}

sub GetAnnualRenewal
{
	return $db->selectrow_hashref("
		SELECT s.*
		FROM annual_vip_subscription_renewals s
		WHERE s.member_id= ?", undef, $params{'id'});
}

sub GetMember
{
	return $db->selectrow_hashref('
		SELECT m.id, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype, m.country AS country_code,
			cbc.currency_code AS default_currency
		FROM members m
		LEFT JOIN currency_by_country cbc
			ON cbc.country_code=m.country
		WHERE id=?', undef, $params{'id'}) || die "Failed to find a member matching: $params{'id'}\n";
}

sub GetMonthlyRenewal
{
	return $db->selectrow_hashref("
		SELECT s.*
		FROM monthly_vip_subscription_renewals s
		WHERE s.member_id= ?", undef, $params{'id'})
}

sub GetSubscriptionMatch
{
	my ($subs) = @_;
	
	foreach (@{$subs})
	{
		return $_ if $_->{'subscription_type'} == $params{'subscription_type'};
	}
	return undef;
}

sub LoadParams
{
	my ($req, $opt) = @_;
	foreach (@{$req}, @{$opt})
	{
		$params{$_} = $q->param($_);
	}
	foreach (@{$req})
	{
		die "$_ is a required parameter\n" unless defined $params{$_};
	}
	
	foreach (qw/id pk subscription_type/)
	{
		next unless defined $params{$_};
		die "The $_ parameter must be numeric\n" if $params{$_} =~ m/\D/;
	}
}

sub Render_Page
{
	my $subscription_types = {};
	my @monthly_stypes_sort = ();
	my $sth = $db->prepare('
		SELECT subscription_type, description, sub_class, COALESCE(weight,0) AS weight, renewal
		FROM subscription_types
		ORDER BY weight ASC');
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$subscription_types->{$tmp->{'subscription_type'}} = $tmp;
		push @monthly_stypes_sort, $tmp->{'subscription_type'} if $tmp->{'sub_class'} eq 'VM' && $tmp->{'renewal'};
	}

	my $data = {Gather_Data(), 'subscription_types'=>$subscription_types, 'monthly_stypes_sort'=>\@monthly_stypes_sort};
#	die Dumper($data);
	print $q->header('-charset'=>'utf-8');
	$TT->process('/home/httpd/cgi-templates/TT/cgi.admin.subscriptions.tt', $data) || print $TT->error;
}

__END__

# 08/19/08 added the differentiation of the cancellation refund types
# as well as pulling in addition soa & transaction data for enhancing the admin interface
###### 08/12/10 a slight tweak to the big SQL statement in Gather_Data to optimize it
###### 05/25/12 various cleanups and minor SQL changes to take into account the new subscription types
###### 10/09/13 had to break up the multiple statements from the transactional block in the cancellation section
# 06/06/14 heavy rewriting to port this over to a TT rendition from XML/XSL, also added in functionality for creating/replacing subscriptions instead of just extending existing ones
# 07/15/14 added type 49 to the list of subscriptions handled in ExtendSubscription()
04/27/15 Revised the logic in determining the soa trans_type when cancelling a subscription. The old logic was developed back when there was only one basic monthly subscription type
# 06/08/15 added binmode