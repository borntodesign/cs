#!/usr/bin/perl -w
###### regenerate_me.cgi
###### a simple script to validate admin access
###### and allow the user to regenerate specific file based resources
###### created: 05/10/06 Bill MacArthur
###### last modified: 03/17/15	Bill MacArthur

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
require cs_admin;

###### Globals
my (@cookie, $TEXT, $db);
my $q = new CGI;
my $script_name = $q->script_name;

unless ( $q->https )
{
	my $loc = $q->self_url;
	$loc =~ s/^http/https/;	
	print $q->redirect($loc);
	Exit();
}

die 'You must be logged in as staff' unless $q->cookie('cs_admin');
my $cs = cs_admin->new({'cgi'=>$q});
my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

#my $operator = $q->cookie('operator') || $q->param('operator');
#my $pwd = $q->cookie('pwd') || $q->param('pwd');
my $action = $q->param('action') || '';

#if ($action eq '1')
#{
#	###### currently this is the entire 'xmall'
#	###### I think only the specials are being used, however, as of 05/10/06
#	my $rv = RunningCK('xmlgen_mall.pl');
##	$rv ||= qx(/home/admin/tools/xmlgen_mall.pl);
#	$rv ||= qx(/home/admin/tools/xmlgen_mall.pl all);
#	###### if we were successful, then nothing was returned
#	$rv ||= 'Xmall and specials pages regenerated';
#	Err($rv);
#}
if ($action eq '2')
{
	###### currently these are the translated country drop-downs used in the malls
	my $rv = RunningCK('create-mall-countries.pl');
	if ($rv)
	{
		Err('Apparently we are already/still running the command');
	}
	else
	{		
		$rv = qx(/home/admin/tools/mall/create-mall-countries.pl);
		
		$rv = "There was an error running the necessary command: $?" if $?;
	}
	
	###### if we were successful, then nothing was returned
	$rv ||= 'Mall drop-down countries lists regenerated';
	Err($rv);
}
else
{
	Err('Invalid or empty selection.<br />Hint: be sure to select an item.');
}

Exit();

################

sub Err
{
	my $msg = shift;
	$msg =~ s#\n#<br />#g;
	print $q->header(-expires=>'-1d'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$msg,
		$q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if defined $db;
}

sub RunningCK
{
	###### there is no guarantee that this will work
	###### it is just a preliminary attempt to avoid concurrent runs which
	###### may cause some nasty overwrites
	###### once we have a longer running cron to test against, then we will know
	my $me = shift;
	my $res = qx(ps -Af --width 180 | grep $me | grep -v -c grep) || 0;
#print "Content-type: text/html\n\nres=$res\n";
#	my $res = qx(ps -Af --width 180 | grep $me) || '';
#	return "Already running $res" if  $res > 1;
	return "Already running $res" if  $res > 0;
	return;
}
#sub Validate
#{
#	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
#	$db = ADMIN_DB::DB_Connect( 'db.cgi', $operator, $pwd ) || return ();
#	return 1;
#}


