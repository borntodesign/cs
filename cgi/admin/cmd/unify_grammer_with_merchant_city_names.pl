#!/usr/bin/perl -w

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use DB;
use DB_Connect;
use G_S;
use Lingua::EN::NameCase;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $query = <<EOT;

	SELECT
		id,
		city
	FROM
		merchant_affiliates_master
	WHERE
		status IN (1,2)
		AND
		(
			city <> ''
			OR
			city IS NOT NULL
		)

EOT


my $update_query = <<EOT;

	UPDATE
		merchant_affiliates_master
	SET
		city = ?
	WHERE
		id = ?

EOT


my $location_query = <<EOT;

SELECT
	mal.id, mal.city
FROM
	merchant_affiliate_locations mal
	JOIN
	merchant_affiliates_master mam
	ON
	mal.merchant_id = mam.id
WHERE
	mam.status IN (1,2)
	AND
	(
		mal.city <> ''
		OR
		mal.city IS NOT NULL
	)
	
EOT


my $update_location_query = <<EOT;

	UPDATE
		merchant_affiliate_locations
	SET
		city = ?
	WHERE
		id = ?

EOT

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################

unless ($db = DB_Connect('cancellation.cgi')){exit}
$db->{RaiseError} = 1;

eval{
	
	my $sth_update = $db->prepare( $update_query );
	
	my $sth = $db->prepare( $query );
	$sth->execute();
	
	my $counter = 0;
	print "Merchant_Master_Record\n";
	while (my $row = $sth->fetchrow_hashref)
	{
		#last if ($counter > 1);
		
		if($row->{city})
		{
			my $updated_city_name = Lingua::EN::NameCase::NameCase($row->{city});
			
			if ($row->{city} ne $updated_city_name)
			{
				eval{
					$sth_update->execute($updated_city_name,$row->{id});
				};
				if($@)
				{
					print 'Error: ' . $@;
				}
				
				print "Merchant ID: $row->{id}\t\tOriginal City Name: $row->{city}\t\tUpdated City Name: $updated_city_name\n";
				$counter++;
			}
		}
	}
	
	$sth->finish();
	$sth_update->finish();
	
	
	$sth_update = $db->prepare( $update_location_query );
	
	$sth = $db->prepare( $location_query );
	$sth->execute();
	
	$counter = 0;
	print "\n\nMerchant_Location_Record\n";
	while (my $row = $sth->fetchrow_hashref)
	{
		
		#last if ($counter > 1);
		
		if($row->{city})
		{
			my $updated_city_name = Lingua::EN::NameCase::NameCase($row->{city});
			
			if ($row->{city} ne $updated_city_name)
			{
				eval{
					$sth_update->execute($updated_city_name,$row->{id});
				};
				if($@)
				{
					print 'Error: ' . $@;
				}
				
				print "Location ID: $row->{id}\t\tOriginal City Name: $row->{city}\t\tUpdated City Name: $updated_city_name\n";
				$counter++;
			}
		}
	}
	
	$sth->finish();
	$sth_update->finish();
	
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


