#!/usr/bin/perl

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use DB;
use DB_Connect;

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $query = <<EOT;

		SELECT
			mal.id,
			mal.location_name,
			mal.city,
			mal.country
		FROM
			merchant_affiliate_locations mal
		ORDER BY
			mal.id
		--LIMIT 20

EOT

my $query_location_name_count =<<EOT;

 SELECT
 	COUNT(mal.location_name_url) AS url_name_count
 FROM
 	merchant_affiliate_locations mal
 WHERE 
 	mal.location_name_url = ?
 	AND
 	mal.city = ?
 	AND
 	mal.country = ?

EOT


my $update_query =<<EOT;

	UPDATE
		merchant_affiliate_locations
	SET
		location_name_url = ?
	WHERE
		id = ?

EOT

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################

unless ($db = DB_Connect('cancellation.cgi')){exit}
$db->{RaiseError} = 1;

eval{
		my $sth = $db->prepare($query);
		my $sth_name_count = $db->prepare($query_location_name_count);

		$sth->execute();
		
		
		while (my $outermost_rows = $sth->fetchrow_hashref)
		{
			
			my $location_name_url = lc($outermost_rows->{'location_name'});
			
			$location_name_url =~ s/\W//g;
			
			my $flag = 0;
			my $name_counter = '';
			
			my $location_name_url_modified = '';
			
			while($flag == 0)
			{
				
				$location_name_url_modified = $location_name_url . $name_counter;
				
				$sth_name_count->execute(($location_name_url_modified,$outermost_rows->{'city'},$outermost_rows->{'country'}));
				
				my $rows = $sth_name_count->fetchrow_hashref;
				
				$flag = 1 if ($rows->{'url_name_count'} == 0);
				
				$name_counter = $name_counter ? $name_counter +1 : 2 ;
				
				
			}
			
			
			eval{
				print "Pre-insert: $outermost_rows->{id}: $location_name_url_modified \n";
				
				my $response = $db->do($update_query, undef, ($location_name_url_modified, $outermost_rows->{id}));
				
				if($response == 0 || $response eq '0E0')
				{
					print "Error with $outermost_rows->{id}: $location_name_url_modified \n rows updated: $response \n\n";
				}
				elsif($response > 1)
				{
					print " $outermost_rows->{id}: $location_name_url_modified \n rows updated: $response \n\n";
				}
			};
			if($@)
			{
				
				print "Error: $@ ";
				print "Error with $outermost_rows->{id}: $location_name_url_modified \n\n";
				
			}
			
			
		}
		
		$sth->finish();
		$sth_name_count->finish();

};
if($@)
{
	print "Error: $@ ";
}
			
exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


