#!/usr/bin/perl -w

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use DB;
use DB_Connect;
use G_S;
use MerchantAffiliates;
#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
our $MerchantAffiliates = {};


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $query = <<EOT;
	SELECT
		*
	FROM
		some_table
EOT

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################

unless ($db = DB_Connect('cancellation.cgi')){exit}
$db->{RaiseError} = 1;

eval{
	
	
	
	my %merchant_information = (
								merchant_type=>'offline',
								#member_id=>'',
								referral_id	=>'1',
								business_name=>'MARITIM Park Hotel Riga',
								firstname1=>'Liene',
								lastname1=>'Freimane',
								address1=>'Slokas 1',
								city=>'Riga',	
								country=>'LV',
								state=>'Riga',
								postalcode=>'LV-1048',
								phone=>'0037126308566',
#								home_phone=>'',
								fax=>'0037167069001',
								language=>'lv',
								emailaddress1=>'liene@maritim.lv',
								username=>'Maritim',
								password=>'maritim_',
								business_type=>'1658',
								percentage_of_sale=>0.10,
								url=>'www.maritim.lv',
								url_lang_pref=>'en',
								payment_method=>'CA',
#								ca_number=>,
#								cc_type=>,	
#								cc_number=>'',	
#								ccv_number=>'',
#								cc_expiration_date=>'',	
#								cc_name_on_card=>'',	
#								cc_address=>'',
#								cc_city=>'',
#								cc_country=>'',
#								cc_state=>'',
								#cc_postalcode=>,
								merchant_package=>3,
								tin=>40003428241,
								);
	
	
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	my $merchant_info = $MerchantAffiliates->createMerchant(\%merchant_information);
	print "merchant_info: $merchant_info \n\n";
	
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


