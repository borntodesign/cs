#!/usr/bin/perl -w
###### srwspec_admin.cgi
######
###### The admin script for manipulating the SRW merchant specials.
######
###### created: 02/24/05	Karl Kohrt
######
###### modified: 
	
use strict;
use lib ('/home/httpd/cgi-lib');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
use G_S;
use DBI;
use CGI;
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our %TMPL = (	'search'	=> 10396,
		'display'	=> 10397 );
our $db = '';
our $member_id = '';
our $action = ( $q->param('action') || '' );
our $search_field = $q->param('search_field');
our $search_value = $q->param('search_value');
$search_value = G_S::PreProcess_Input($search_value) if $search_value;
our $alias = '';
our $membertype = '';
our $data = '';
our $msg = '';
our $vendor_input = '';
our $vendor_link =  '';
our @field_names = qw/filename start_date end_date description
				pk instructions notes vendor_id vendor_name stamp operator/;
our @char_fields = qw/=filename
			=date
			=description
			=instructions
			=vendor_name
			=notes
			=all/;
our @int_fields = qw/=pk =vendor_id/;
###### %field has the form of 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %field = (	filename 		=> {value=>'', class=>'n', req=>1},
		start_date		=> {value=>'', class=>'n', req=>1},
		end_date		=> {value=>'', class=>'n', req=>1},
		description		=> {value=>'', class=>'n', req=>0},
		pk			=> {value=>'', class=>'n', req=>0},
		instructions 		=> {value=>'', class=>'n', req=>0},
		notes			=> {value=>'', class=>'n', req=>0},
		vendor_id		=> {value=>'', class=>'n', req=>0},
		vendor_name		=> {value=>'', class=>'n', req=>0},
		stamp			=> {value=>'', class=>'n', req=>0},
		operator		=> {value=>'', class=>'n', req=>0}
		);
our @result_list = ();

################################
###### MAIN starts here.
################################

# Here is where we get our admin user and try logging into the DB.
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'srwspec_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# If this is the first time through, just print the Search page.
unless ( $action )
{
	Search_Form()	;
}
# If we are searching for records.
elsif ($action eq 'search')
{
	# Check the validity of the field and the value provided by the user.
	if ( Check_Validity() )
	{
		Run_Query();
		# If no records were found.
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		# If a list of records were found.
		elsif ( @result_list > 1 )
		{
			###### create list report
			Display_Results();
		}
		# If one record was found.
		else
		{
			$data = shift @result_list;
			Load_Data();
			Do_Page('display');
		}
	}
	# If there were invalid parameters passed.
	else
	{
		Search_Form($msg);
	}
}
# We should have one row of data with this action.
elsif ($action eq 'show')
{
	if ( Check_Validity() )
	{
		Run_Query();

		# Just in case we don't have one row of data.
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		else
		{		
			$data = shift @result_list;
			Load_Data();
			Do_Page('display');
		}
	}
	# If there were invalid parameters passed.
	else
	{
		Search_Form($msg);
	}
}
# Request for the form to create a new record.
elsif ( $action eq 'new' )
{
	$msg .= $q->span({-class=>'r'}, "Click the Submit button to Create a new Merchant Special.<br>\n");
	# Change the action to create for the next display page.
	Do_Page('new');
}
# Creating or updating a record.
elsif ( $action eq 'create' || $action eq 'update' )
{
	Load_Params();
	Check_Required();

	# If we already have an error.
	if ($msg)
	{
		# Reload the page with the problems flagged if there are any errors
		Do_Page('display');
	}
	# Otherwise, create/update the record, then display it.
	else
	{
		if ( $action eq 'create' ) { Create() }
		elsif ( $action eq 'update' ) 	{ Update() }

		$search_field = 'pk';
		$search_value = $field{pk}{value};
		Run_Query();
		$data = shift @result_list;
		Load_Data();
		Do_Page('display');
	}
}
else { Err('Undefined action.') }

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

sub Check_Required
{
	my $found_one = 0;
	# We'll check required fields and assign new html classes as necessary
	foreach (keys %field)
	{
		# If a required field is empty, we'll flag it.
		if ( !$field{$_}{value} && $field{$_}{req} )
		{
			$field{$_}{class} = 'r';
			unless ( $found_one )
			{
				$msg .= $q->span({-class=>'r'},
					"<br>Please fill in the required fields marked in <b>RED</b><br>\n");
				$found_one = 1;
			}
		}
	}
}

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			$msg .= $q->span({-style=>'color: #ff0000'},
				"Invalid search value for $search_field - Use only whole numbers with this field.");
			return;	
		}
	}
	else
	{
		$msg .= $q->span({-style=>'color: #ff0000'},
				"Invalid field specification - Try again.");
		return;				
	}
	return 1;						
}

###### Create a record.
sub Create
{
	my $value_list = '';
	my @values = ();
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my $vendor_id_field = '';
	my $vendor_id_value = '';

	# Create the query.
	if ( $field{vendor_id}{value} )
	{
		$vendor_id_field = "vendor_id,\n";
		$vendor_id_value = "?,";
		push (@values, $field{vendor_id}{value});
	}

	$qry .= "	INSERT INTO srw_specials (
				$vendor_id_field
				pk,
				filename,
				start_date,
				end_date,
				description,
				instructions,
				notes,
				operator,
				stamp
				)
			VALUES	( $vendor_id_value ?, ?, ?, ?, ?, ?, ?, current_user, NOW() );";

	# Get the next pk in the sequence.
	$field{pk}{value} = $db->selectrow_array("SELECT NEXTVAL('srw_specials_pk_seq');");

	push (@values, $field{pk}{value} || '');
	push (@values, $field{filename}{value} || '');
	push (@values, $field{start_date}{value} || '');
	push (@values, $field{end_date}{value} || '');
	push (@values, $field{description}{value} || '');
	push (@values, $field{instructions}{value} || '');
	push (@values, $field{notes}{value} || '');
	$sth = $db->prepare($qry);

	# If we were able to create the record.
	if ( $field{pk}{value} && ($rv = $sth->execute(@values)) )
	{					
		$msg .= qq#\n	<span class="b">
					The record was successfully created.
					</span><br/>\n#;
		# Change the action to update for the next display page.
		$action = 'update';
	}
	# If no creation was performed.
	else
	{					
		$msg .= qq#\n	<span class="r">
					The record was not created.
					</span><br/>\n#;
	}
	$sth->finish;
	return $rv;
}

###### Display a list if more than one result is returned from the query.
sub Display_Results
{
	my $html;

	# print a title for the browser window.
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#ffffee', -title=>'Merchant Specials Search Results');

	# print a simple page header.
	print qq!
		<CENTER><H1>Merchant Specials Search Results</H1></CENTER><p>
		<TABLE align="center" border="0" cellpadding="2">
 			<TR>
			<TD><b> Merchant Special ID </b></TD>
			<TD><b> Vendor Name </b></TD>
			<TD><b> Vendor ID </b></TD>
			<TD><b> Filename </b></TD>
			<TD><b> Start Date </b></TD>
 			<TD><b> End Date </b></TD>
			</TR>!;

	my $row_color = "#ECFFF9";
	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		# Alternate row color.
		$row_color =  ($row_color eq '#E6F2FF') ? '#ECFFF9' : '#E6F2FF';

		# Stop uninitialization errors.
		foreach ( keys %{$result} ) { $result->{$_} ||= '&nbsp;' }
		print qq!
			<TR bgcolor="$row_color">
			<TD ALIGN="center"><A href="$ME?action=show&search_field=pk&search_value=$result->{pk}"> $result->{pk} </a></TD>
			<TD>$result->{vendor_name}</TD>  
			<TD>$result->{vendor_id}</TD>
			<TD>$result->{filename}</TD>
			<TD>$result->{start_date}</TD>
			<TD>$result->{end_date}</TD>
			</TR>!;
	}

	print qq!
	</TABLE>
	<CENTER><p><a href="$ME">Click Here for New Search</a></CENTER>!;

	print $q->end_html();
}

###### Display a single record to the browser.
sub Do_Page
{
	my $tmpl_name = shift;
	$action = ($tmpl_name eq 'new') ? 'create' : 'update';

	# If there is no value for the vendor ID.
	if ( $tmpl_name eq 'new' || $field{vendor_id}{value} eq '' )
	{
		# Provide a link to the vendor record search interface.
		$vendor_link = qq!<a href="/cgi/admin/vms/List_Vendors.cgi" class="q" target="_blank">Search Vendor List</a>!;
		# The new and display/update pages are the same template from this point forward.
		$tmpl_name = 'display';
	}
	else
	{
		# Provide a link to the specific vendor record.
		$vendor_link = qq!<a href="/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=%%vendor_id%%&offset=&criteria1=%%vendor_id%%" class="q" target="_blank">View / Edit Vendor Details</a>!;
	}

	my $hidden = '';# = &Create_hidden();
	my $tmpl = G_S::Get_Object($db, $TMPL{$tmpl_name}) || die "Couldn't open $TMPL{$tmpl_name}";
	$tmpl =~ s/%%message%%/$msg/;
	$tmpl =~ s/%%hidden%%/$hidden/;
	$tmpl =~ s/%%ME%%/$ME/g;
	$tmpl =~ s/%%action%%/$action/g;
	$tmpl =~ s/%%vendor_link%%/$vendor_link/g;

	foreach (keys %field)
	{
		$tmpl =~ s/%%$_%%/$field{$_}{value}/g;
		$tmpl =~ s/%%class_$_%%/$field{$_}{class}/;
	}
	print $q->header(-expires=>'now'), $tmpl;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br>$_[0]<br>";
}

###### Loads the data from the query into the global hash variable.
sub Load_Data
{
	###### this procedure presumes that the field keys are identical to the passed parameters
	foreach my $key (keys %field)
	{
		$field{$key}{value} = $data->{$key};
		$field{$key}{value} = '' unless defined $field{$key}{value};
	}
}

###### Load the params into a local hash.
###### Populating our field array hash values
sub Load_Params
{
	###### this loop depends on the %field hash keys being identical to the passed parameter names
	foreach (keys %field)
	{
		# Check for a defined value
		$field{$_}{value} = ( defined $q->param($_) ) ? $q->param($_) : '';

		# Unless it is a free-form field, clean it up.
		unless ( grep { /$_/ } qw/notes instructions filename/ )
		{
			$field{$_}{value} =~ s/\t|\r|\n|\f|\[M|"|;|:|\+|=//g;
		}
	}
}

##### Runs the specified query, pushing the results onto a list (array).
sub Run_Query
{
	my ($qry, $sth) = ();
	$qry = "	SELECT s.*, date_trunc('minutes', s.stamp) AS stamp, v.vendor_name
			FROM srw_specials s
				LEFT JOIN vendors v ON s.vendor_id = v.vendor_id 
			";

	# All search fields except 'vendor_name' for are in the srw_specials table.
	my $table = 's';
	if ( $search_field eq 'vendor_name' ) { $table = 'v' }
	if ( $search_field eq 'all' )
	{ 
		$qry .= " ORDER BY s.start_date DESC LIMIT 50;";
	}
	elsif ( $search_field eq 'date' )
	{ 
		$qry .= " WHERE ? BETWEEN s.start_date AND s.end_date";
	}
	elsif ( grep { /=$search_field/ } @char_fields )
	{ 
		$qry .= " WHERE $table.$search_field ~* ?";
	}
	else
	{
		$qry .= " WHERE $table.$search_field = ?";
	}

	# Order by the vendor name unless we get them all, then we're already ordering by date desc.
	unless ( $search_field eq 'all' )
	{
		$qry .= " ORDER BY v.vendor_name;";
	}
	$sth = $db->prepare($qry);

	# All requires no bind variables.
	if ( $search_field eq 'all' ) 	{ $sth->execute() }
	else 					{ $sth->execute( $search_value ) }
	
	while ( $data = $sth->fetchrow_hashref)
	{
		push (@result_list, $data);
 	}		
	$sth->finish();
}

###### print our main search form with any pertinent messages
sub Search_Form
{
	my $msg = shift || '';
	# Open the search template.
	my $tmpl = G_S::Get_Object($db, $TMPL{'search'}) || die "Failed to load template: $TMPL{'search'}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$msg/;
	print $q->header(), $tmpl;	
}

###### Update a record.
sub Update
{
	my $value_list = '';
	my @values = ();
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my $vendor_id_field = '';
	my $vendor_id_value = '';

	# Create the query.
	# Create the query.
	if ( $field{vendor_id}{value} )
	{
		$vendor_id_field = "vendor_id= ?,\n";
		push (@values, $field{vendor_id}{value});
	}
	else { $vendor_id_field = "vendor_id= NULL,\n" }

	$qry .= "	UPDATE srw_specials
			SET	$vendor_id_field
				filename= ?,
				start_date= ?,
				end_date= ?,
				description= ?,
				instructions= ?,
				notes= ?,
				operator= current_user,
				stamp= NOW()
			WHERE 	pk = ?;";
	push (@values, $field{filename}{value} || '');
	push (@values, $field{start_date}{value} || '');
	push (@values, $field{end_date}{value} || '');
	push (@values, $field{description}{value} || '');
	push (@values, $field{instructions}{value} || '');
	push (@values, $field{notes}{value} || '');
	push (@values, $field{pk}{value} || '');

	$sth = $db->prepare($qry);
	# If we were able to update the information.
	if ( $rv = $sth->execute(@values) )
	{					
		$msg .= qq#\n	<span class="b">
					The update was successful.
					</span><br/>\n#;
	}
	# If no update was performed.
	else
	{					
		$msg .= qq#\n	<span class="r">
					The update failed.
					</span><br/>\n#;
	}
	$sth->finish;
	return $rv;
}






