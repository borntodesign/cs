#!/usr/bin/perl
###### reinstate.cgi
###### handle in-house reinstatements
###### revision history at the end
######
###### Last modified: 10/16/2006	Bill MacArthur

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;

$| = 1;

###### cancellation/termination status values:
###### cp=cancel pending p=processed cd=cancel dropped tp=termination pending tr=termination rescinded
###### sp=suspension pending tw=termination waiting

###### Globals
my %TEMPLATES = (
	page1 =>	"$G_S::HTML_TMPL_DIR/admin/reinstatement.tmpl.html",
	page2 =>	"$G_S::HTML_TMPL_DIR/admin/reinstatement.tmpl2.html"
);
my ($dat, %not_xfers, %xfers) = ();

my $GOOD_MEM = 'm|ag|agu|s|v';	###### these are desirable members to get back
my $BAD_MEM = 'r|t|c|d';		###### these are ......

my $q = new CGI;
my $id = $q->param('id');
my $action = $q->param('action') || '';
my $list = $q->param('list');

###### these numbers will keep track of the members to be transferred and not to be transferred
my $total = my $xgood_total = my $xbad_total = my $good_total = my $bad_total = 0;

my $rein;	###### the reinstatement person's data hash

my ($key, $val, $myflg, $dbflag, $dbserver, $db);

###### here is where we get our user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
exit unless $db = ADMIN_DB::DB_Connect( 'reinstate.cgi', $operator, $pwd );
$db->{RaiseError} = 1;

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
unless (CheckGroup())
{
	Err($q->h3('You must be a member of the accounting group to use this form'));
	goto 'END';
}

unless ($action){ Page1() }
###### if we have an action and no ID we've got no place to go

elsif (not $id){ Err( $q->h4('Missing the ID') ) }

elsif ($action eq 'Submit')
{

###### these vars will hold the data that we will display on step 2
	my ($members, $xmembers) = ();

###### we'll first get some info on our reinstatee and a count of currently sponsored members, if any
	my $qry = "
		SELECT firstname, lastname, alias, spid, membertype,
			(SELECT COUNT(id) FROM members WHERE spid = $id) AS cnt
		FROM members WHERE id = $id";

	my $sth = $db->prepare($qry);
	$sth->execute;
	$rein = $sth->fetchrow_hashref;
	$sth->finish;
	if ($rein->{cnt} > 0)
	{
		Page2( $q->h4("This party already has members sponsored.<br>This reinstatement needs closer inspection.") );
		goto 'END';
	}

###### now we'll check his sponsor to be sure that the sponsor is still a valid membertype
	$sth = $db->prepare("SELECT membertype FROM members WHERE id= $rein->{spid}");
	$sth->execute;
	my $sp_mtype = $sth->fetchrow_array;
	$sth->finish;
	unless ($sp_mtype eq 'v' || $sp_mtype eq 'm' || $sp_mtype =~ /ag/)
	{
		my $msg = "This sponsor of this party no longer qualifies to be the sponsor.<br>
Membertype: $G_S::MEMBER_TYPES{$sp_mtype}<br>
This reinstatement needs closer inspection.<br>
You may need to change this party's sponsor and then perform reinstatement.";
		Page2( $q->b($msg) );
		goto 'END';
	}

	$qry = qq/
		SELECT DISTINCT m.id,
			m.alias,
			m.firstname,
			m.lastname,
			m.emailaddress,
			m.membertype,
			m.spid
		FROM 	members m,
		(	SELECT id FROM spid_arc WHERE spid= $id
			UNION
			SELECT id FROM members_arc WHERE spid= $id
		) AS ma
		WHERE 	m.id = ma.id/;
				
	$sth = $db->prepare($qry);
	$sth->execute;
	while ($dat = $sth->fetchrow_hashref)
	{
		$total++;

###### if the ones to be rolled back are no longer directly under the sponsor of the reinstatee, then we won't 
###### move them back because they have probably been reassigned by that sponsor
###### we will also keep track of the types of members in each group (useful for spotting potential problems)
		if ($dat->{spid} == $rein->{spid})
		{
			if ($dat->{membertype} =~ /$GOOD_MEM/)
			{
				$xfers{$dat->{id}} = $dat;
				$xgood_total++;
			}
			else
			{
				$xbad_total++;
			}
		}
		else
		{
			if ($dat->{membertype} =~ /$GOOD_MEM/)
			{
				$not_xfers{$dat->{id}} = $dat;
				$good_total++;
			}
			else
			{
				$bad_total++;
			}
		}
	}
	$sth->finish;

###### here we'll create a table of members that will NOT be transferred
	foreach (keys %not_xfers)
	{
		$members .= $q->Tr(
			$q->td({-class=>'b'}, $not_xfers{$_}{alias}).
			$q->td({-class=>'b'}, "$not_xfers{$_}{firstname} $not_xfers{$_}{lastname}").
			$q->td({-class=>'b'}, $not_xfers{$_}{emailaddress}).
			$q->td({-class=>'b', -align=>'center'}, $not_xfers{$_}{membertype}).
			$q->td({-class=>'b'}, $not_xfers{$_}{spid})
		) . "\n";
	}

###### if we have 
	if ($members)
	{
		$members = <<TBL;
<table bgcolor="#ffff00" cellspacing="0" cellpadding="4" border="0">
<tr>
    <td colspan="5" style="color: #ff0000; font-weight: bold;">
	Members who will not be rolled back.</td>
</tr>
<tr>
<td class="h">ID</td>
<td class="h">Name</td>
<td class="h">Email Address</td>
<td class="h">Membertype</td>
<td class="h">Current Sponsor</td>
</tr>
$members
</table>
<br><br>
TBL
	}

###### now we'll create a table of members to be transferred
	foreach (keys %xfers)
	{
		$xmembers .= $q->Tr(
			$q->td({-class=>'g'}, $xfers{$_}{alias}).
			$q->td({-class=>'g'}, "$xfers{$_}{firstname} $xfers{$_}{lastname}").
			$q->td({-class=>'g'}, $xfers{$_}{emailaddress}).
			$q->td({-class=>'g', -align=>'center'}, $xfers{$_}{membertype}).
			$q->td({-class=>'g'}, $xfers{$_}{spid})
		) . "\n";
	}
 
	if ($xmembers)
	{
		$xmembers = <<TBL2;
<table bgcolor="#eeffee" cellspacing="0" cellpadding="4" border="0">
<tr>
    <td colspan="5" style="color: #009900; font-weight: bold;">
	Members who will be rolled back.</td>
</tr>
<tr>
<td class="h">ID</td>
<td class="h">Name</td>
<td class="h">Email Address</td>
<td class="h">Membertype</td>
<td class="h">Current Sponsor</td>
</tr>
$xmembers
</table>
<br><div align="center">
<form action="/cgi/admin/reinstate.cgi">
<input type="submit" name="action" value="Reinstate">
<input type="hidden" name="id" value="$id">
<input type="hidden" name="spid" value="$rein->{spid}">
</form></div>
TBL2
	}
	
	my $message = <<CNT;
<div>
Total members previously sponsored: $total<br>
Total 'Good' members to be transferred: $xgood_total<br>
Total 'Other' members to be transferred: $xbad_total<br>
Total 'Good' members that will not be transferred: $good_total<br>
Total 'Other' members that will not be transferred: $bad_total<br></div>
CNT
	Page2($message, $members, $xmembers);
}
elsif ($action eq 'Reinstate')
{
	my $spid = $q->param('spid');
	my $qry = "
		UPDATE members SET
			membertype = 'v', operator = '$operator', stamp = NOW(),
			memo = 'reinstated ' || NOW()
		WHERE id = $id";

	my $res = $db->do($qry);
	unless ($res == 1)
	{
		Err( $q->("There has been a problem updating $id") );
		goto 'END';
	}

###### we'll start printing status messages here
	print $q->header(-charset=>'utf-8'), $q->start_html();
	print "$id has been switched to VIP.<br>Beginning transfer of members.<br><br>\n";

	$qry = "
		UPDATE members SET
			spid = $id, operator = '$operator', stamp = NOW(),
			memo = 'rolled back down to previous sponsor $id due to reinstatement'
		WHERE spid = $spid AND id IN
			(SELECT id FROM members_arc WHERE spid = $id
			UNION
			SELECT id FROM spid_arc WHERE spid = $id)";

	$res = $db->do($qry);
	print "$res total members transferred. This includes all member types.<br>\n";
	print "Generating a list of Members, Affinity Groups and VIPs who have been transferred.<br><br>\n";
	print "<div style=\"font-family: Arial, Helvetica, sans-serif; font-size: 9pt;\">\n";
											
	$qry = "
		SELECT alias, firstname, lastname, emailaddress, membertype
		FROM members
		WHERE spid = $id AND membertype ~ 'm|ag|v' ORDER BY membertype, id";

	my $sth = $db->prepare($qry);
	$sth->execute;
	while ( my $dat = $sth->fetchrow_hashref)
	{
		print "$dat->{alias}, $dat->{firstname} $dat->{lastname}, $dat->{emailaddress}, $dat->{membertype}<br>\n";
	}
	$sth->finish;
	print "</div>\n";
	print "<br><br>Job complete.<br>Print this out for your report, or copy and paste for your email.\n";
	print "<br><br><a href=\"/cgi/admin/reinstate.cgi\">Click here</a> to do another, otherwise, close this window.\n";
	print $q->end_html();
}
else
{
	Err("<b>There has been an undefined error</b>");
}
END:
$db->disconnect;
exit;

sub CheckGroup
{
	my $sth = $db->prepare("SELECT usesysid FROM pg_user WHERE usename = '$operator'"); $sth->execute;
	my $sid = $sth->fetchrow_array;
	$sth->finish;
	$sth = $db->prepare("SELECT grolist FROM pg_group WHERE groname = 'accounting'"); $sth->execute;
	my $res = $sth->fetchrow_array;
	$sth->finish;
	return ($res =~ /$sid/) ? 1 : undef;
}

sub Err{
	print $q->header(-charset=>'utf-8'), $q->start_html();
	print $_[0];
	print $q->end_html();
}

sub Page1
{
	open (PAGE, "$TEMPLATES{page1}") || die "Couldn't open $TEMPLATES{page1}\n";
	print $q->header(-charset=>'utf-8');
	foreach (<PAGE>){ print }
	close PAGE;
}

sub Page2
{
	my ($message, $message2, $message3) = @_;
	open (PAGE, "$TEMPLATES{page2}") || die "Couldn't open $TEMPLATES{page2}\n";
	print $q->header(-charset=>'utf-8');
	foreach (<PAGE>){
		$_ =~ s/%%alias%%/$rein->{alias}/g;
		$_ =~ s/%%firstname%%/$rein->{firstname}/g;
		$_ =~ s/%%lastname%%/$rein->{lastname}/g;
		$_ =~ s/%%membertype%%/$rein->{membertype}/g;
		$_ =~ s/%%message%%/$message/;
		$_ =~ s/%%message2%%/$message2/;
		$_ =~ s/%%message3%%/$message3/;
		print;
	}
	close PAGE;
}

###### 03/20/02 went to the DB_Connect lib for our DB connection
###### 06/18/02 put reporting if a reinstatement would place the party under a person who was no longer a VIP
###### also changed the template locations
###### 05/01/03 removed the ' - reinstate.cgi' reference from the operator entry in the UPDATE queries.
###### 06/30/04 rewrote for a bit more clarity, added handling for the spid_arc table
###### removed references to Partners
###### 06/01/05 changed the sponsor check to allow 'member' and 'ag' sponsors too
# 10/16/06 added use lib