#!/usr/bin/perl -w
###### administrative_suspensions.cgi
###### tool for handling administrative suspensions
###### last modified: 12/27/14	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
require ADMIN_DB;
require cs_admin;

$| = 1;   # do not buffer error messages

###### GLOBALS
my $Accounting_Contact = 'acf1@dhs-club.com';
my ($db, $row, $table, $id) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $cs = cs_admin->new({'cgi'=>$q});

my $order = $q->param('order');
my $action = $q->param('action');
my $me = $q->script_name();
my $prompt = "<p class=\"b_alert\">This interface is for non accounting suspensions only.</p>";

unless ($q->cookie('cs_admin'))
{
	Err('You must be logged in to use this tool');
	exit;
}

my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

unless ($action)
{
	Do_Page();
}
elsif ($action eq 'suspend')
{
	if (Suspend())
	{
		Do_Page();
#		NotifyAccounting($action);
	}
}
elsif ($action eq 'unsuspend')
{
	if (UnSuspend())
	{
		Do_Page();
#		NotifyAccounting($action);
	}
}
else
{
	Err('undefined action');
}

$db->disconnect;
exit;

sub Do_Page
{
	my $rowcolor = '#ffffee';
	my $orderBY = 'm.id';
	if ($order eq 'dateDN'){ $orderBY = 'm.stamp DESC' }
	elsif($order eq 'dateUP'){ $orderBY = 'm.stamp' }

	my $tmpl = $gs->GetObject(10060) || die "Failed to retrieve template";

###### get my current suspensions
	my $sth = $db->prepare("	SELECT m.id,
						m.alias,
						m.membertype,
						m.memo,
						c.stamp
					FROM 	members m,
						cancellations c
					WHERE m.id=c.id
					AND c.status ~ 'sa'
					ORDER BY $orderBY");
	$sth->execute;
	while ($row = $sth->fetchrow_hashref)
	{
		$table .= $q->Tr({'-bgcolor'=>$rowcolor},
					$q->td({'-class'=>'id'}, $row->{'alias'}) . "\n" .
					$q->td({'-class'=>'mtype'}, uc $row->{'membertype'}) . "\n" .
					$q->td({'-class'=>'date'}, $row->{'stamp'}) . "\n" .
					$q->td({'-class'=>'memo'}, $row->{'memo'}) . "\n" .
					$q->td({'-class'=>'btn', -valign=>'center'},
									$q->start_form('-action'=>$me, '-method'=>'get') .
									$q->hidden('-name'=>'action', '-value'=>'unsuspend', '-force'=>1) .
									$q->hidden('-name'=>'id', '-value'=>$row->{'id'}, '-force'=>1) .
									$q->submit('-value'=>'Drop', '-class'=>'btn') .
									$q->end_form)) . "\n";
		$rowcolor = ($rowcolor eq '#ffffee') ? '#eeeeee' : 'eeeeff';
	}
	$sth->finish;

	print $q->header('-expires'=>'now', '-charset'=>'utf-8');
	$tmpl =~ s/%%table%%/$table/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	$tmpl =~ s/%%me%%/$me/g;
	$tmpl =~ s/%%prompt%%/$prompt/;
	print $tmpl;
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub NotifyAccounting
{
	my $type = shift;
	
	my %Content = (	'suspend'	=> "This is to inform you that the following party has been Administratively Suspended:\n",
				'unsuspend'	=> "This is to inform you that the Administrative Suspension on following party has been dropped:\n"
			);

	my %Subject = (	'suspend'	=> 'Administrative Suspension',
				'unsuspend'	=> 'Dropped Administrative Suspension'
			);

	open (MAIL, "| /usr/sbin/sendmail -t") || die "Failed to open sendmail\n";
	print MAIL "To: $Accounting_Contact\n";
	print MAIL "Subject: $Subject{$type}\n";
	print MAIL <<_mailpiece_;
$Content{$type}
Alias:		$id->{'alias'}
Name:		$id->{'firstname'} $id->{'lastname'}
Email:		$id->{'emailaddress'}
Membertype:	$id->{'membertype'}
_mailpiece_
	close MAIL;
}

sub Suspend
{
	my $pid = uc $q->param('id');
	my $memo = $q->param('memo') || '';
	unless ($pid)
	{
		Err('No ID received');
		return;
	}

	my $qry = "
		SELECT m.id,
				m.alias,
				m.firstname,
				m.lastname,
				m.emailaddress,
				m.membertype,
				c.status
		FROM members m
		LEFT JOIN cancellations c
			ON m.id=c.id
		WHERE ";
			
	$qry .= ($pid =~ m/\D/) ? "m.alias= ?" : "m.id= ?";
	my $sth = $db->prepare($qry);
	$sth->execute($pid);
	$id = $sth->fetchrow_hashref;
	$sth->finish;
	
	unless ($id)
	{
		Err("No match on ID: $pid");
		return;
	}

###### if they have any kind of cancellation on file except for a processed one, we cannot proceed
	if ($id->{'status'} && $id->{'status'} ne 'p')
	{
		Err("$pid has a cancellation/suspension/termination in progress. That action must be negated before this suspension can be processed.");
		return;
	}

	$db->begin_work;
	
	$db->do("UPDATE members SET status= 0, operator= ?, memo= ? WHERE id= $id->{'id'}", undef, $creds->{'username'}, $memo);
	
	if ($id->{'status'})
	{
		$db->do("DELETE FROM cancellations WHERE id= $id->{'id'}");
	}
	
	$db->do("INSERT INTO CANCELLATIONS (id, status, memo) VALUES ($id->{'id'}, 'sa', ?)", undef, $memo);
	
	$db->commit;

	$order = 'dateDN';
	$prompt = "<p class=\"r_alert\">Suspension for $pid successfully established</p>";
	return 1;
}

sub UnSuspend
{
	my $pid = uc $q->param('id');
	unless ($pid)
	{
		Err('No ID received');
		return;
	}

	my $qry = "
		SELECT m.id,
				m.alias,
				m.firstname,
				m.lastname,
				m.emailaddress,
				m.membertype,
				c.status
		FROM members m
		LEFT JOIN cancellations c
		ON m.id=c.id
		WHERE ";
			
	$qry .= ($pid =~ m/\D/) ? "m.alias= ?" : "m.id= ?";
	my $sth = $db->prepare($qry);
	$sth->execute($pid);
	$id = $sth->fetchrow_hashref;
	$sth->finish;
	
	unless ($id)
	{
		Err("No match on ID: $pid");
		return;
	}

	unless ($id->{'status'} && $id->{'status'} eq 'sa')
	{
		Err("$pid does not have an administrative suspension in place.");
		return;
	}

	$db->begin_work;
	$db->do("
		UPDATE members
		SET status= 1,
			operator= ?
		WHERE id= $id->{'id'}", undef, $creds->{'username'});
			
	$db->do("DELETE FROM cancellations WHERE id= $id->{'id'}");

	$db->commit;

	$prompt = "<p class=\"g_alert\">Suspension for $pid successfully dropped</p>";
	return 1;
}

###### 08/06/03 added the NotifyAccounting routine
###### 11/18/03 Commented out the two calls to the NotifyAccounting routine.
###### 02/01/06 made the delete SQL execute in a 'do' instead of a prepare/execute
# 10/03/06 added the use lib pragma
# 12/27/14 the reason for diving into this relic was to break up the SQL into separate statements to keep DBI happy, but will in here put cs_admin to work and also did code beautification
