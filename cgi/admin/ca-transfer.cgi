#!/usr/bin/perl -w
###### originally SOInsert.cgi
###### originally created by: Stephen Martin
###### rewrite to do CA->CA transfers: 09/09 Bill MacArthur
###### last modified: 02/19/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use ADMIN_DB;

$| = 1;

###### Globals

my $sth = ();
my $rv;
our $db        = ();
our $q         = new CGI;
our $firstname;
our $lastname;
our $trans_type;
our $amount;
our $description;
our $memo;
our $batch;
our $o;
my $qry;
my $self = $q->url();
my $des;
my $des_in;
my $class;
my $editor_link = "./SOEditor.cgi";
my $trans_menu;
my $transaction_hash;
my $MESSAGE = '';
my %TMPL = (
	'fund' => 10123
);
my ($src, $destination) = ();

unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
	exit;
}

my $operator = $q->cookie('operator');
my $pwd      = $q->cookie('pwd');

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
	###### I don't want empty strings in the DB, so I'll undef any empties so they go in a NULLs
	$param{$_} = undef if length $param{$_} == 0;
	next unless $param{$_};
	###### remove leading and trailing whitespace
	$param{$_} =~ s/^\s*|\s*$//g;
}

exit unless $db = ADMIN_DB::DB_Connect( 'SOAreport.cgi', $operator, $pwd );
$db->{RaiseError} = 1;

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
if ( CheckGroup() != 1 ) {
    Err(
        $q->h3('You must be a member of the accounting group to use this form')
    );
    End();
}

$param{'action'} ||= '';

if ( $param{'action'} eq 'confirm' )
{
	# present the information back so that they can eyeball it with a few more details
	###### Ensure we've got the basic parameters
	foreach('src_id','dest_id','src_trans_type','amount','dest_trans_type'){
		next if $param{$_};
		Err($q->h4("The $_ parameter is missing"));
		End();
	}

	$src = $db->selectrow_hashref("
		SELECT id, alias, firstname ||' '|| lastname AS name, UPPER(membertype) AS mtype
		FROM members WHERE ${\($param{id} =~ /\D/ ? 'alias' : 'id')} = ?", undef, (uc $param{'src_id'})) ||
		die "Failed to find a matching membership for: $param{'src_id'}\n";

	$destination = $db->selectrow_hashref("
		SELECT id, alias, firstname ||' '|| lastname AS name, UPPER(membertype) AS mtype
		FROM members WHERE ${\($param{id} =~ /\D/ ? 'alias' : 'id')} = ?", undef, (uc $param{'dest_id'})) ||
		die "Failed to find a matching membership for: $param{'dest_id'}\n";
	
	GenerateForm();
}
elsif ($param{'action'} eq 'confirmed')
{
	my ($trans_id) = $db->selectrow_array("
		SELECT create_ca2ca_xfer(
			$param{'src_id'},
			$param{'dest_id'},
			$param{src_trans_type},
			$param{dest_trans_type},
			$param{amount},
			?,
			?,
			?,
			?,
			NULL, NULL)
	", undef, $param{'src_description'}, $param{'dest_description'}, $param{'src_memo'}, $param{'dest_memo'}) ||
		die "Failed to create a transfer";

	# Bill - whenever Accounting needs to make a transfer using this form - https://www.clubshop.com/cgi/admin/ca-transfer.cgi
	# automatically assess a "Transfer Fee" of -$3.00 to the Funding/Source Member and include "Transfer Fee" in the Transaction Type list.
	my $rv = $db->do("INSERT INTO soa (trans_type, amount, id, description) VALUES (15032, -3.00, $param{'src_id'}, ?)", undef, "references: $trans_id");
	$MESSAGE = "$trans_id successfully created -and- fee applied" if $trans_id;
	GenerateForm();
}
else
{
	GenerateForm();
}

End();

###### ###### ######

sub CheckGroup
{
    my $sth = $db->prepare("SELECT usesysid FROM pg_user WHERE usename = '$operator'");
    $sth->execute;
    my $sid = $sth->fetchrow_array;
    $sth->finish;
    $sth = $db->prepare("SELECT grolist FROM pg_group WHERE groname = 'accounting' AND $sid= ANY(grolist)");
    $sth->execute;
    my $res = $sth->fetchrow_array;
    $sth->finish;
    ( $res ) ? ( return 1 ) : (return);
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub Err {
    print $q->header(), $q->start_html();
    print $_[0];
    print $q->end_html();
}

sub gen_trans_menu {
	my $name = shift;
    my ( $sth, $qry, $data, $rv, $html );    ###### database variables

    $html = qq|<select name="$name"><option value="">- - -</option>|;

    $qry = "select
            id,name 
            from soa_transaction_types
	WHERE admin_interface_available=TRUE
	order by name asc";

    $sth = $db->prepare($qry);

    $rv = $sth->execute;

    while ( $data = $sth->fetchrow_hashref ) {
        $html .= qq|<option value="$data->{id}">$data->{name}</option>|;
    }

    $rv = $sth->finish();

    return $html . '</select>';
}

sub GenerateForm
{
	print $q->header(-charset=>'utf-8');
	print qq|<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>Create a Club Account Transaction</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="/css/SOAadmin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
fieldset
{
	display:inline;
}

legend
{
	font-weight: bold;
}

table
{
	text-align:left;
}

.message { color:blue; }
</style>

<script type="text/javascript">
 function _V()
 {
  if ( document.f.src_id.value == "")
  {
   alert("Please enter a Source member ID");
   document.f.src_id.focus();
   return false;
  }
 
  if ( document.f.dest_id.value == "")
  {
   alert("Please enter a Recipient member ID");
   document.f.dest_id.focus();
   return false;
  }
  
  if ( document.f.amount.value == "")
  {
   alert("Please enter an amount");
   document.f.amount.focus();
   return false;
  }
  
  if ( isNaN(document.f.amount.value))
  {
   alert("Please enter a number for the amount!");
   document.f.amount.focus();
   return false;
  }

  if ( document.f.amount.value < 0)
  {
   alert("Please enter an amount greater than zero");
   document.f.amount.focus();
   return false;
  }
  
  if ( document.f.src_trans_type.selectedIndex == 0)
  {
   alert("Please select a Source transaction type");
   document.f.src_trans_type.focus();
   return false;
  }
 
  if ( document.f.dest_trans_id.selectedIndex == 0)
  {
   alert("Please select a Destination transaction type");
   document.f.dest_trans_type.focus();
   return false;
  }

  return true;
 }

 function adjustColor(ctl){
if (isNaN(ctl.value)){ ctl.style.color = 'inherit'; }
else if (ctl.value < 0){ ctl.style.color = '#f00'; }
else { ctl.style.color = '#0c0'; }
return;
}
</script>
</head>

<body>
|;
	print qq|<form action="$self" method="get" name="f" onsubmit="return _V();">

<h4>Create a Club Account Transfer</h4>
|;

	print qq|<p class="message">$MESSAGE</p>| if $MESSAGE;
	
	print q|
<fieldset style="background-color:#ffe;">
<legend>Funding/Source Member</legend>
<table class="def">

<tr>
 <td class="lbs2">Member ID</td>

 <td class="lbs2">
|;

	if ($param{'action'} eq 'confirm')
	{
		print $q->hidden('src_id');
		print "$src->{alias}, $src->{name}, $src->{mtype}";
	}
	else
	{
		print $q->textfield('-name'=>'src_id', '-value'=>'', -force=>1);
	}

	print q|</td>
</tr>
<tr>
 <td class="lbs1">Trans. Type</td>
 <td class="lbs1">
|;

	if ($param{'action'} ne 'confirm')
	{
		print gen_trans_menu('src_trans_type');
	}
	else
	{
		my ($tt) = $db->selectrow_array('SELECT "name" FROM soa_transaction_types WHERE id=?', undef, $param{src_trans_type});
		print $tt, $q->hidden('src_trans_type');
	}

	print q|
</td>
</tr>     
<tr>
 <td class="lbs2">Amount</td>
 <td class="lbs2">
|;

	if ($param{'action'} ne 'confirm')
	{
  		print qq|<input type="text" name="amount" onchange="adjustColor(this)" />|;
	}
	else
	{
		printf("%.2f", $param{amount});
		print $q->hidden('amount');
	}

	print '</td></tr>';

	if ($param{'action'} ne 'confirm')
	{
		print q|<tr>
 <td class="lbs1">Description</td>
 <td class="lbs1">
 <textarea rows="3" cols="50" name="src_description"></textarea>
</td>
</tr>

<tr>
 <td class="lbs2">Memo</td>
 <td class="lbs2">
 <textarea rows="3" cols="50" name="src_memo"></textarea>
</td>
</tr>
|;
	}

	print q|
</table>
</fieldset>
<br /><br />
<fieldset style="background-color:#eef;">
<legend>Recipient Member</legend>
<table class="def">

<tr>
 <td class="lbs2">Member ID</td>

 <td class="lbs2">
|;

	if ($param{'action'} eq 'confirm')
	{
		print "$destination->{alias}, $destination->{name}, $destination->{mtype}";
		print $q->hidden('dest_id');
	}
	else
	{
		print $q->textfield('-name'=>'dest_id', '-value'=>'', -force=>1);
	}


	print q|
</td>
</tr>
<tr>
 <td class="lbs1">Trans. Type</td>
 <td class="lbs1">
|;

	if ($param{'action'} ne 'confirm')
	{
		print gen_trans_menu('dest_trans_type');
	}
	else
	{
		my ($tt) = $db->selectrow_array('SELECT "name" FROM soa_transaction_types WHERE id=?', undef, $param{src_trans_type});
		print $tt, $q->hidden('dest_trans_type');
	}

	print '</td></tr>';     

	if ($param{'action'} ne 'confirm')
	{
		print q|<tr>
 <td class="lbs1">Description</td>
 <td class="lbs1">
  <textarea rows="3" cols="50" name="dest_description"></textarea>
</td>
</tr>

<tr>
 <td class="lbs2">Memo</td>
 <td class="lbs2">
<textarea rows="3" cols="50" name="dest_memo"></textarea>
</td>
</tr>

<tr>
 <td colspan="2">
  <hr />
 </td>
</tr>
|;
	}
	
	print q|</table></fieldset><br /><br />|;

	if ($param{'action'} eq '' || $param{'action'} eq 'confirmed')
	{
		print q|
  <input type="submit" value="Submit" class="in" style="margin-right:1em;" />
  <input type="reset" class="in" />
  <input type="hidden" name="action" value="confirm" />
|;
	}
	else
	{
		print $q->submit('Perform Transfer'), $q->hidden('-name'=>'action', '-value'=>'confirmed', '-force'=>1);
	}

	if ($param{'action'} eq '' || $param{'action'} eq 'confirm')
	{
		print $q->hidden('src_description'), $q->hidden('src_memo');
		print $q->hidden('dest_description'), $q->hidden('dest_memo');
	}
	print '</form></body></html>';
}