#!/usr/bin/perl

print "content-type: text/html\n\n";
print "\n";

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC CGI Help</title>
</head>
<body bgcolor="#eeffee">
<table border="0" width="100%" align="center">
  <tr>
  <th align="left"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top" nowrap>
 <b>DHSC Template Help</b>
  </th>
 </tr>
  <tr>
  <td align="left" nowrap
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top">
  This screen will allow you to '<u>view</u>' a template to verify its content<br>
  prior to recovering it,<br>
  <br>
  <u>Once</u> you have <b><u>verified</u></b> that this is the correct template
  you may<br>
  '<u>recover</u>' the template by pressing the '<i>Recover</i>' button.<br>
  <br>
  Once the recovery process has completed you will be returned<br>
  to the <u>Template Management Screen</u> and the search results.
  <br>
  </td>
 </tr>
<tr>
 <td height="15">&nbsp;
 </td>
</tr>
 <tr>
  <td align="left">
   <form>
   <input type="button" value="Close" name="execB"
    onClick="javascript:window.close();" class="in">
   </form>
  </td>
 </tr>
</table>
</body>
</html>
\n~;

exit;


