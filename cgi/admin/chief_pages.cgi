#!/usr/bin/perl -w

##
## chief_pages.cgi
##
## 03/02/2007 Shane Partain

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my @th = ('Alias', 'URL', 'Name');
my @rows = ();
my @columns = ();

our $q = new CGI;
our $db = '';

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'profile_questions_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

my $chiefs = $db->selectall_hashref("	SELECT alias,
						url,
						firstname,
						lastname
					FROM members m, leaders l
					where l.id=m.id
					AND active=true", "alias");

push @rows, $q->th( \@th );
foreach (keys %{$chiefs}) {
	@columns = ($chiefs->{$_}->{alias}, $q->a({-href=>"$chiefs->{$_}->{url}"}, $chiefs->{$_}->{url}),
			"$chiefs->{$_}->{firstname} $chiefs->{$_}->{lastname}");
	push @rows, $q->td(\@columns);
}

print   $q->header(),
	$q->start_html(),
	$q->center(
		$q->h3('Chief Pages'),
		$q->table( $q->Tr( \@rows ) )
		),	
	$q->end_html;

