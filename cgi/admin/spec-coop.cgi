#!/usr/bin/perl -w
###### spec-coop.cgi
###### a custom 'coop' interface for use by trusted Chiefs... and now 01 :)
###### created: 11/07/07	Bill MacArthur
###### last modified: 05/20/18	Bill MacArthur

$| = 1;

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S qw($LOGIN_URL);
use DB_Connect;
use Mail::Sendmail;
require MailingUtils;
require cs_admin;
#use Data::Dumper;	# only needed for debugging
binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
my $NOMAIL = 0;	###### set to 1 to not send mail (testing environment only)
# the activity types we will create for member transfers of this membertype
my %MTYPE_CODES = ('s' => 1, 'm' => 2, 'v' => 12);

my ($db, @List, $PartnerRecipient, %AdditionalMembershipDetailsCache) = ();
my $num_processed = 0;

my $q = new CGI;
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my %TMPL =
(
	'subject' => 10640
);
my $error_flag = ();

# although this is not specifically an admin aware script in the sense that we have to be logged in as admin or that it will act differently as admin
# we want to record the admin operator in certain records if admin is logged, particularly as 01, and is performing management
#my $operator = $q->cookie('operator');
my $operator = ();
if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new();
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	$operator = $creds->{'username'};
}

my $params = LoadParams();

my $container = $params->{'c'};			# this will end being a partial hashref of the coop member record
unless ($container)
{
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html('-title'=>'Enter a Coop ID'),
		$q->start_form('-method'=>'get'),
		'Enter a numeric Coop ID<br />',
		$q->textfield('-name'=>'c'),
		$q->submit('Submit'),
		$q->end_form,
		$q->end_html;
	exit;
}
elsif($container =~ /\D/)
{
	die "Invalid 'c' parameter\n";
}

my $recipient = ();	# this will become a hashref of recipient details, which will start as the ID submitted, but can change if we are doing a stacking operation
my $num_to_transfer = $params->{'num_to_transfer'} || '';
$num_to_transfer = 0 unless $num_to_transfer && $num_to_transfer !~ m/\D/;	# ensure that we have a positive digit

my $Processed_List = '';
my $Errors = '';
my $inv = ();			###### inventory hash
my $auction = ();		###### for admin use, this may end up containing a hashref of the auction record
my $no_mail_flag = 0;	# This is globally manipulated. If there is an error in processing, we don't want the list to go out.

my ( $id, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . ${\$q->script_name()} . "\">$LOGIN_URL</a>");
	exit;
}

exit unless $db = DB_Connect('spec-coop');
$db->{'RaiseError'} = 1;

$container = $db->selectrow_hashref('
	SELECT m.id, m.spid, m.alias, c.house_acct
	FROM members m
	JOIN coop.coops c
		ON c.coop_id=m.id
	WHERE m.id= ?', undef, $container);
die "Container lookup failed: $container\n" unless $container;

###### if they are not in the MAP, they don't have access to this interface
unless ( $id == 1 ||
	( $db->selectrow_array('
		SELECT coop.coop_admins.member_id
		FROM coop.coop_admins
		WHERE member_id=? AND coop_id=?', undef, $id, $container->{'id'}) ) )

{
	Err('Access Denied');
	exit;
}

# see if the coop has a merchant/vendor relationship for sales integration
# if not the key will be undefined
$container->{'merchant'} = GetMerchantRelation();

if ($params->{'action'} eq 'process')
{
	if ($recipient = CheckRecipient())
	{
		if ($recipient->{'membertype'} ne 'v')
		{
			# we need to send the upline Partner the normal transfer recipient's email (per Dick 12/05/14)
			$PartnerRecipient = $db->selectrow_hashref("SELECT id, alias, spid, firstname, lastname, membertype, emailaddress, language_pref FROM my_upline_vip($recipient->{'id'})");
		}
		else
		{
			$PartnerRecipient = $recipient;
		}
	
		if ($params->{'group'})
		{
			ProcessGroup();
		}
		else
		{
			Process();
		}
	}
}
Check_Inventory();
Display();

END:
$db->disconnect;
exit;

###### ###### Start of Sub-Routines

sub AdditionalMembershipDetails
{
	my $r = shift;
	
	# no need to do this over and over again
	unless ($AdditionalMembershipDetailsCache{$r->{'id'}})
	{
		# the following two values will be used to ensure a TP transfer that aligns to business rules
		# if we cannot find a depth, we will set it artificially high so as to refuse the transfer
		$AdditionalMembershipDetailsCache{$r->{'id'}}->{'network_depth'} = $db->selectrow_array("SELECT depth FROM network.depths WHERE id= $r->{'id'}") || 1000000;
	
		$AdditionalMembershipDetailsCache{$r->{'id'}}->{'pnsid'} = $db->selectrow_array("SELECT network.my_pnsid($r->{'id'})") || 0;
	}

	$r->{'network_depth'} = $AdditionalMembershipDetailsCache{$r->{'id'}}->{'network_depth'};
	$r->{'pnsid'} = $AdditionalMembershipDetailsCache{$r->{'id'}}->{'pnsid'};
	return;
}

sub cboPurchaseType
{
	my $sth = $db->prepare("SELECT transfer_type, label FROM coop.transfer_types ORDER BY transfer_type");
	$sth->execute;
	my (@vals, %labels) = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @vals, $tmp->{'transfer_type'};
		$labels{$tmp->{'transfer_type'}} = $tmp->{'label'};
	}
	return $q->popup_menu(
		'-name' => 'transfer_type',
		'-default' => 1,
		'-id' => 'cbo_transfer_type',
		'-values' => \@vals,
		'-labels' => \%labels
	);
}

sub cboSalePriceCurrency
{
	my $frmId = shift;
	my @vals = $container->{'merchant'}->{'default_currency_code'};
	push @vals, 'USD' if $container->{'merchant'}->{'default_currency_code'} ne 'USD';
	
	return $q->popup_menu(
		'-id'=>"sale_price_currency_$frmId",
		'-name'=>'sale_price_currency',
		'-force'=>1,
		'-onchange'=>"setNativeCommission($frmId)",
		'-values'=>\@vals,
		'-default'=>$container->{'merchant'}->{'default_currency_code'}
	);
}

sub Check_Inventory
{
	$inv = $db->selectrow_hashref("
		SELECT COUNT(members.*) AS total,
			SUM(CASE WHEN members.membertype='ma' THEN 1 ELSE 0 END) AS merchants,
			SUM(CASE WHEN members.membertype='s' THEN 1 ELSE 0 END) AS shoppers,
			SUM(CASE WHEN members.membertype='m' THEN 1 ELSE 0 END) AS associates,
			SUM(CASE WHEN members.membertype IN ('m','s','ma') AND COALESCE(ps.cnt,0) = 0 THEN 1 ELSE 0 END) AS transferrable,
			SUM(CASE WHEN members.membertype='v' THEN 1 ELSE 0 END) AS vips,
			SUM(CASE WHEN members.membertype='tp' THEN 1 ELSE 0 END) AS tps
		FROM members
		LEFT JOIN ps_cnt ps
			ON members.id=ps.id
		-- allow them to exclude memberships for transfer for say an auction or whatever
		LEFT JOIN opt2003_exclusions ox
			ON ox.id=members.id AND ox.spid=members.spid
		WHERE ox.id IS NULL
		AND members.spid = $container->{'id'}");
}

sub CheckRecipient
{
	###### is the recipient valid (a VIP and is the operator in the upline)
	unless ($params->{'recipient'})
	{
		$Errors .= "No recipient received\n";
		return;
	}

	my $transfer2 = $db->selectrow_hashref("
		SELECT id, alias, spid, firstname, lastname, membertype, emailaddress, language_pref
		FROM members
		WHERE ${\($params->{'recipient'} =~ /\D/ ? 'alias' : 'id')} = ?
		", undef, uc( $params->{'recipient'} ));

	unless ($transfer2)
	{
		$Errors .= "Cannot find a membership to match: $params->{'recipient'}\n";
		return;
	}
	elsif ($transfer2->{'membertype'} ne 'v' && ! $operator)	# if they are not a house account, they can only transfer to Partners
	{
		$Errors .= "$params->{'recipient'} is not a Partner\n";
		return;
	}
	elsif ($operator && $transfer2->{'membertype'} ne 'v' && $transfer2->{'membertype'} ne 'tp')
	{
		$Errors .= "$params->{'recipient'} is not a Partner -or- Trial Partner\n";
		return;
	}

	# 01/18/12 Dick said to allow Coop operators to cherry pick from their coops
	if ($transfer2->{'id'} == $container->{'spid'})
	{
		# let 'em through
	}
	# for admin usage, we can ignore the following check
	# per Dick on 04/13/11 we are also allowing Fab the ability to sell anywhere in the network
	# also if the parent of the coop is making a purchase, that is OK
	elsif ($id > 1 && $container->{'id'} != 4031135 && $id != $container->{'spid'})
	{
		my ($rv) = $db->selectrow_array("SELECT id FROM relationships WHERE id= $transfer2->{'id'} AND upline= $container->{'spid'}");
		unless ($rv)
		{
			$Errors .= "$params->{'recipient'} is not in Coop's parent organization\n";
			return;
		}
	}
	
	return $transfer2;
}

sub Display
{
	my $merch_js = '';
	if ($container->{'merchant'})
	{
		$merch_js = "
			var commission_percentage = $container->{'merchant'}->{'commission_percentage'};
			var pp_multiplier = $container->{'merchant'}->{'pp_multiplier'};
			var xrates = new Array();
			xrates['USD'] = 1;";
		if ($container->{'merchant'}->{'default_currency_code'} ne 'USD')
		{
			my ($rate) = $db->selectrow_array('
				SELECT "rate" FROM exchange_rates_now WHERE "code"=?',
				undef, $container->{'merchant'}->{'default_currency_code'});
			$merch_js .= "xrates['$container->{'merchant'}->{'default_currency_code'}'] = $rate;";
		}
	}
	print $q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>'Spec-Coop',
			'-encoding'=>'utf-8',
			'-style'=>{
				'-type'=>'text/css',
				'-code'=>'
					input.w7em{margin:0.1em; margin-right: 0.5em; width:7em;}
					div, p{font-size:90%;}
					textarea{font-size:85%; margin-right:0.5em;}
					table {border-collapse:collapse;}
					table#mainTbl {margin:1em 0;}
					td {vertical-align:top; padding: 0.2em 0.5em 0.2em 0.2em;}
					#mainTbl td { border:1px solid silver; }
					#groupFrmTbl td { border:0; padding:0.2em;}
					#mainFrmTbl td { border:0; padding:0.2em;}
					.txerror {color:red;}
					.txsuccess {color:green;}'
			},
			'-script'=>[{
					'-type'=>'text/javascript',
					'-src'=>"/js/spec-coop.js"
				},
				{
					'-type'=>'text/javascript',
					'-code'=>$merch_js
				},
				{
					'-type'=>'text/javascript',
					'-src'=>'/js/sprintf.js'
				}
			]
		);

	print $q->div({'-style'=>'float:right'}, "
			Sales associated with this Coop will be reflected under:<br />
			Vendor ID: $container->{'merchant'}->{'vendor_id'}<br />
			Merchant Name: $container->{'merchant'}->{'merchant_name'}<br />
			Merchant Location ID: " . ($container->{'merchant'}->{'location_id'} || '') . "<br />
			Minimum Commission %: " . (100 * $container->{'merchant'}->{'commission_percentage'})
		) if $container->{'merchant'};

	print $q->div("
		Inventory for: <b>$container->{'id'} - $container->{'alias'}</b><br />
		Affiliates (m): $inv->{'associates'}<br />
		Members (s): $inv->{'shoppers'}<br />
		Merchants: $inv->{'merchants'}<br />
		Transferrable: $inv->{'transferrable'}<br />
		Partners: $inv->{'vips'}<br />
		Trial Partners: $inv->{'tps'}
		<!--Note: Members that are on the container's exclusion list are not reflected in these figures.-->");


	print $q->p({'-style'=>'color:red'}, $Errors) if $Errors;

	print '<table id="mainTbl"><tr><td>',
		$q->start_form('-method'=>'get', '-id'=>'mainfrm'),
		$q->span({'-style'=>'font-weight:bold;font-size:smaller;'}, 'Members'),
		$q->p('This interface will transfer the newest memberships regardless of membertype'),
		$q->hidden('c'),
		$q->hidden('-name'=>'action', '-value'=>'process');

	print
		'<div><table id="mainFrmTbl">',
		$q->Tr( $q->td(	$q->textfield('-name'=>'recipient', '-force'=>1, '-class'=>'w7em') ) .
				$q->td('Enter the transfer recipient')
		),
		$q->Tr( $q->td(	$q->textfield('-name'=>'num_to_transfer', '-force'=>1, '-class'=>'w7em') ) .
				$q->td('Enter the number to transfer')
		),
		$q->Tr( $q->td( cboPurchaseType() ) .
				$q->td('Purchase Type')
		),
		$q->Tr( $q->td( $q->textfield(
						'-name'=>'transfer_notes',
						'-force'=>1,
						'-value'=>'',
						'-style'=>'width:13em',
						'-class'=>'w7em') ) .
				$q->td('Transfer Notes')
		);
	print SalesFormElements(1) if $container->{'merchant'};
	print
		'</table>',
#		$q->button('-onclick'=>'specrst()', '-value'=>'Reset', '-style'=>'color:red'),
		$q->reset('-style'=>'color:red', '-class'=>'w7em'),
		$q->submit('-value'=>'Process', '-style'=>'color:green', '-class'=>'w7em'),
		'</div>',
		$q->end_form, '</td><td>';

	print $q->start_form('-method'=>'get', '-id'=>'groupfrm'),
		$q->span({'-style'=>'font-weight:bold;font-size:smaller;'}, 'Partners, Trial Partners, Affiliates, Merchants or Members'),
		$q->p('This interface will perform any transfers you specify unless they are on the Exclusion List'),
		$q->hidden('c'),
		$q->hidden('-name'=>'action', '-value'=>'process'),
		$q->hidden('-name'=>'group', '-value'=>1),
		'<table id="groupFrmTbl">',
		$q->Tr(
			$q->td($q->textfield('-name'=>'recipient', '-force'=>1, '-class'=>'w7em')) .
			$q->td('Enter the transfer recipient')
		),
		$q->Tr( $q->td($q->textarea('-name'=>'grouplist', '-force'=>1, '-cols'=>18, '-rows'=>6, '-class'=>'w7em')) .
				$q->td('Enter the memberships to transfer')
		),
		$q->Tr( $q->td( cboPurchaseType() ) .
				$q->td('Purchase Type')
		),
		$q->Tr( $q->td( $q->textfield(
						'-name'=>'transfer_notes',
						'-id'=>'transfer_notes',
						'-force'=>1,
						'-value'=>'',
						'-style'=>'width:13em',
						'-class'=>'w7em') . HouseConditional() ) .
				$q->td({'-style'=>'vertical-align:middle'}, 'Transfer Notes')
		);

	print SalesFormElements(2) if $container->{'merchant'};

	print $q->Tr(
		$q->td( $q->textfield('-name'=>'auction', '-value'=>'', '-force'=>1, '-class'=>'w7em')) .
		$q->td('Auction ID') )
		if $container->{'house_acct'};
#		if $id == 1;

	print '</table>',
		$q->reset('-style'=>'color:red', '-class'=>'w7em'),
		$q->submit('-value'=>'Process', '-style'=>'color:green', '-class'=>'w7em');

	print '&nbsp;&nbsp;&nbsp;' . $q->checkbox('-name'=>'stacktp', '-value'=>1, '-label'=>'Stack Trial Partners') if $operator;
	
	print '</form></td></tr>';
	
	print '</table>';
	print $q->start_form('-onsubmit'=>'return false', '-id'=>'proclistfrm');
	print $Processed_List ?
		$q->submit('-value'=>'Highlight List', '-onclick'=>'document.forms.proclistfrm.proclist.select()') :
		'Your list of transferees will appear in this box when the transfer is complete';
	
	my $txclass = $error_flag ? 'txerror' : 'txok';
	print  '<br />',
		$q->textarea(
			'-name'=>'Processed_List',
			'-value'=>$Processed_List,
			'-id'=>'proclist',
			'-rows'=>($Processed_List ? ($num_to_transfer >= 20 ? 20 : $num_to_transfer) : 1),
			'-cols'=>120,
			'-class'=>$txclass
		),

		$q->end_form;

	if ($Processed_List && ! $error_flag)
	{
		print "<p>Sending list to: $PartnerRecipient->{'emailaddress'}<br />";
		my $rv = SendEmail();
		print $rv ? $q->span({'-class'=>'txerror'}, "Mail send failed: $rv") : $q->span({'-class'=>'txsuccess'}, 'Success'), '</p>';
	}
		print $q->end_html;
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub GetMerchantRelation
{
	# determine the merchant type and get the discount particulars
	my $rv = $db->selectrow_hashref("
		SELECT
			mr.coop_id,
			mr.vendor_id,
			mr.default_currency_code,
			v.cv_mult AS pp_multiplier,
			v.reb_mult,
			v.comm_mult,
			v.vendor_group AS trans_type,
			COALESCE(mad.discount,v.discount) AS commission_percentage,
			mal.id AS location_id,
			COALESCE(mal.location_name,v.vendor_name) AS merchant_name
		FROM coop.merchant_relationships mr
		JOIN vendors v
			ON v.vendor_id=mr.vendor_id
		LEFT JOIN merchant_affiliate_locations mal
			ON mal.vendor_id=v.vendor_id
		LEFT JOIN merchant_affiliate_discounts mad
			ON mad.location_id=mal.id
			AND mad.start_date <= NOW()::DATE
			AND COALESCE(mad.end_date,NOW()::DATE) >= NOW()::DATE
		WHERE mr.active=TRUE
		AND v.status > 0
		AND mr.coop_id= $container->{'id'}");
	
	return $rv;
}

sub HouseConditional
{
	return '' unless $meminfo->{'id'} == 1;
	return $q->br . $q->popup_menu(
		'-name'=>'transfer_notes_choices',
		'-force'=>1,
		'-onchange'=>"document.getElementById('transfer_notes').value = this.options[this.selectedIndex].value",
		'-values'=>[
			'',
			'CO-OP Transfer',
			'GPS BASIC PLUS',
			'GPS PRO PLUS',
			'GPS PREMIER PLUS'
		]
	);
}

sub LoadParams
{
	my %rv = ();
	my @expected = qw/
		num_to_transfer
		action
		c
		transfer_type
		transfer_notes
		recipient
		group
		grouplist
		sale_price
		sale_price_currency
		usd_comm/;
	
	if ($operator)
	{
		push @expected, 'stacktp';
	}
		
	# these are the only parameters we expect
	foreach (@expected)
	{
#		$rv{$_} = G_S::PreProcess_Input($q->param($_) || '');
		$rv{$_} = $q->param($_) || '';
		
		###### remove carriage returns that may come in from windows
		###### and leading/trailing whitespace
		if ($_ eq 'recipient')
		{
			# remove all whitespace from this one
			$rv{$_} =~ s/\s*//g;
		}
		else
		{
			$rv{$_} =~ s/\r+|^\s*//g;
			$rv{$_} =~ s/\n\s*|\s*\n/\n/g;
		}
	}
	

	# now preprocess them for sanity
	# the transfer type should be in the range of 1 - 5
	die "Incorrect transfer type" if $rv{'transfer_type'} && $rv{'transfer_type'} !~ m/^[1-5]$/;
	return \%rv;
}

# this routine transfers a user defined number of memberships without regard to membership specifics
sub Process
{
#	return unless CheckRecipient();
	if (! $num_to_transfer || $num_to_transfer < 0)
	{
		$Errors .= "Invalid or missing 'number to transfer': $num_to_transfer\n";
		return;
	}

	Check_Inventory();

	if ($num_to_transfer > ($inv->{'associates'} + $inv->{'shoppers'}))
	{
		$Errors .= "There is insufficient inventory to complete a transfer of: $num_to_transfer\n";
		return;
	}

	my $sth = $db->prepare("
		SELECT
			members.id,
			members.alias,
			members.firstname,
			members.lastname,
			members.emailaddress,
			members.membertype
		FROM members
		LEFT JOIN ps_cnt
			ON ps_cnt.id=members.id
		LEFT JOIN opt2003_exclusions ox
			ON ox.id=members.id
			AND ox.spid=members.spid
		WHERE ox.id IS NULL
		AND members.spid= $container->{'id'}
		AND members.membertype IN ('m','s')
		AND COALESCE(ps_cnt.cnt, 0) = 0
		ORDER BY members.join_date DESC
		LIMIT $num_to_transfer");
	$sth->execute;

	my ($tx_log_id ) = $db->selectrow_array("SELECT NEXTVAL('coop.transfer_log_transfer_id_seq')") ||
		die "Failed to obtain NEXTVAL coop.transfer_log_transfer_id_seq";

	###### we don't want script failure halfway through
	$db->{'RaiseError'} = 0;	
	
	# start a transaction to ensure we either succeed or fail completely
	$db->begin_work;
	Record_Sale($tx_log_id);	# we will die if the sale transaction fails

	my $errflag = '';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$errflag = '';
		
		# if we do not have an activity defined for transferring this membertype, then skip it
		# also per Dick, do not create activities for transfers to 01
		if ($MTYPE_CODES{$tmp->{'membertype'}} && $recipient->{'id'} > 1)
		{
			$db->do("
				-- this statement creates a new signup activity for the new sponsor
				INSERT INTO activity_rpt_data (id, spid, activity_type, val_text)
				VALUES ($tmp->{'id'}, $recipient->{'id'}, $MTYPE_CODES{$tmp->{'membertype'}}, '$id Coop Purchase');
			");
		}
		
		$db->do("
			UPDATE members SET
				spid= $recipient->{'id'},
				memo= 'coop transfer initiated by $id',
				operator='spec-coop.cgi'
			WHERE id= $tmp->{'id'}");

		$db->do("
		-- transfer the nspid... the nspid business rules regarding new members are likely to be complex and this is no place to take care of them
			UPDATE network.spids SET spid= $recipient->{'id'}, memo='transferred nspid credit' WHERE id= $tmp->{'id'}");

		$db->do("
		-- the next statement takes care of transferring activities of this
		-- party to the new upline
		-- by deleting the keys that go with the data, a new set of keys will be created
 			DELETE FROM activity_rpt_key
			WHERE pk IN
				(SELECT pk FROM activity_rpt_data WHERE id = $tmp->{'id'}
		-- don`t reset the original signup activity for the original sponsor
				AND activity_type !=1
				AND activity_type !=2)");
				
		$db->do("
		-- this statement creates a new signup activity for the new sponsor
			INSERT INTO activity_rpt_data (id, spid, activity_type, val_text)
			VALUES ($tmp->{'id'}, $recipient->{'id'}, $MTYPE_CODES{$tmp->{'membertype'}}, '$id Coop Purchase')");

		$db->do("
		-- log the transfer
			INSERT INTO coop.transfer_log (
				coop_id,
				user_id,
				member_id,
				membertype,
				recipient_id,
				transfer_type,
				notes,
				transfer_id,
				operator
			) VALUES (
				$container->{'id'},
				$id,
				$tmp->{'id'},
				'$tmp->{'membertype'}',
				$recipient->{'id'},
				$params->{'transfer_type'},
				?,
				$tx_log_id,
				?
			);
		", undef, $params->{'transfer_notes'}, $operator);

		$errflag ||= $db->errstr;
		$Processed_List .= $error_flag || "$tmp->{'alias'}, $tmp->{'membertype'}, $tmp->{'firstname'}, $tmp->{'lastname'}, $tmp->{'emailaddress'}\n";
		last if ($errflag);
	}
	unless ($errflag)
	{
		$db->commit;
	}
	else
	{
		$db->rollback;
	}
	return 1;
}

sub ProcessGroup
{
	# transfer a VIP or a member with members under them
#	return unless CheckRecipient();

	# for admin use, we may have received an auction ID... let's make sure we can find it
	if (my $a = $params->{'auction'})
	{
		$a =~ s/\s//g;
		die "Auction ID should be numeric only" if $a =~ /\D/;
		
		$auction = $db->selectrow_hashref('SELECT * FROM auctions WHERE auction_id=?', undef, $a);
		die "Cannot identify auction with ID: $a" unless $auction;
	}
	
	my $list = $params->{'grouplist'};

	###### get rid of extraneous newlines
	$list =~ s/^\n+|\n+$//;
	$list =~ s/\n+/\n/g;

	my @list = split(/\n/, $list);
	@list = ReorganizeListByNetworkDepth(@list) if $params->{'stacktp'};
	unless (@list)
	{
		$Errors .= 'The list received was empty';
		return;
	}
	
	my $qry = "
		SELECT
			m.id,
			m.alias,
			m.membertype,
			m.firstname,
			m.lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			ox.id AS excluded
		FROM members m
		LEFT JOIN opt2003_exclusions ox
			ON ox.id=m.id
			AND ox.spid=m.spid
		WHERE m.spid=? AND ";
	my $sth_spck_a = $db->prepare("$qry m.alias=?");
	my $sth_spck_id = $db->prepare("$qry m.id=?");

	my ($tx_log_id ) = $db->selectrow_array("SELECT NEXTVAL('coop.transfer_log_transfer_id_seq')") || die "Failed to obtain NEXTVAL coop.transfer_log_transfer_id_seq";

	# start a transaction to ensure we either succeed or fail completely
	$db->begin_work;
	Record_Sale($tx_log_id);	# we will die if the sale transaction fails

	my $errflag = '';
	foreach my $item (@list)
	{
		$errflag = '';
		my $sth = $item =~ /\D/ ? $sth_spck_a : $sth_spck_id;
		$sth->execute($container->{'id'}, $item);
		my $tmp = $sth->fetchrow_hashref;
		if (! $tmp)
		{
			$Processed_List .= "$item not found on Coop frontline\n";
			$errflag = 1;
			$no_mail_flag = 1;
			#next;
			last;
		}
		elsif ($tmp->{'excluded'})
		{
			$Processed_List .= "$item is on the Coop exclusion list\n";
			$errflag = 1;
			$no_mail_flag = 1;
			#next;
			last;
		}
		elsif (! grep $_ eq $tmp->{'membertype'}, (qw/m s v tp ma/))
		{
			$Processed_List .= "$item is not a transferrable membertype: $tmp->{'membertype'}\n";
			$errflag = 1;
			$no_mail_flag = 1;
			#next;
			last;
		}
		elsif ($tmp->{'id'} == $recipient->{'id'})
		{
			$Processed_List .= "$item is the same as the recipient!\n";
			$no_mail_flag = 1;
			#next;
			last;
		}
		
		if ($tmp->{'membertype'} eq 'v')
		{
			$errflag = TransferVIP($tmp, $tx_log_id);	# there should be no return value in normal operation
		}
		elsif ($tmp->{'membertype'} eq 'm' || $tmp->{'membertype'} eq 's' || $tmp->{'membertype'} eq 'ma')
		{
			TransferNonVIP($tmp, $tx_log_id);
		}
		else
		{
			$errflag = TransferTP($tmp, $tx_log_id);
		}

		$errflag ||= $db->errstr;
		$Processed_List .= $errflag || "$tmp->{'alias'}, $tmp->{'membertype'}, $tmp->{'firstname'}, $tmp->{'lastname'}, $tmp->{'emailaddress'}\n";

		last if $errflag;
		$recipient = $tmp if $params->{'stacktp'};
	}

	unless ($errflag)
	{
		$db->commit;
	}
	else
	{
		$db->rollback;
		$Processed_List .= 'Transfer operation aborted. Try again by omitting the IDs causing the error.';
		$no_mail_flag = 1;
	}

	$sth_spck_a->finish;
	$sth_spck_id->finish;

	return 1;
}

sub Record_Sale
{
	my $tx_log_id = shift;
	return unless $params->{'sale_price'};
	die "A positive commission amount must be entered" unless $params->{'usd_comm'};
	
	# if the transfer type indicates this is a sale, but there is no sale amount, then bail out
	die "You have indicated that this is a Regular Purchase" if ($params->{'transfer_type'} == 1 && ! $params->{'sale_price'});
	
	# validate the sale_price and USD commission
	foreach (qw/usd_comm sale_price/)
	{
		die "Invalid $_. $params->{'usd_comm'} must be stictly numeric in the format xx -or- xx.yy"
			unless $params->{$_} =~ m/^\d*$/ || $params->{$_} =~ m/^\d*\.\d*$/;
	
		die "$_ must be a positive amount" unless $params->{$_} >= 0;
	}

	my ($xrate) = $db->selectrow_array('
		SELECT "rate" FROM exchange_rates_now WHERE "code"=?', undef, $params->{'sale_price_currency'}) ||
		die "Failed to obtain the current exchange rate for: $params->{'sale_price_currency'}";
	
	my $usd_sale_price = ($params->{'sale_price'} || 0) / $xrate;

	die "Failed to obtain an active vendor relationship for this Coop" unless $container->{'merchant'};
	
	# calculate our minimum expected commission and see if what the user has submitted at least matches that
	my $mincomm = sprintf "%.2f", ($usd_sale_price * $container->{'merchant'}->{'commission_percentage'});
	die "Minimum allowable commission for: $params->{'sale_price'} is $mincomm"	unless ($params->{'usd_comm'} || 0) >= $mincomm;

	my $description = $params->{'auction'} ? "Auction: $params->{'auction'}" : '';
	if ($params->{'transfer_notes'})
	{
		$description .= " - " if $description;
		$description .= $params->{'transfer_notes'};
	}

	# now we will either create a merchant transaction or a simple PP transaction
	# depending on if we have a merchant location ID
	if ($container->{'merchant'}->{'location_id'})
	{
		$db->do("
			INSERT INTO merchant_affiliates_transactions (
				mal_id,
				trans_amt,
				trans_date,
				notes,
				native_currency_amt,
				native_currency_code,
				alias,
				usd_commission
			) VALUES (
				$container->{'merchant'}->{'location_id'},
				$usd_sale_price,
				NOW()::DATE,
				?,
				$params->{'sale_price'},
				?,
				'$recipient->{'alias'}',
				?
			)", undef, $description, $params->{'sale_price_currency'}, $params->{'usd_comm'});
		
		$db->do("
			INSERT INTO coop.purchase_log (transfer_id, mat_record_id)
			SELECT $tx_log_id, CURRVAL('maf_trans_seq');
		");
	}
	else
	{
		# we are assuming that this is a regular mall type transaction
		# AS OF 12/18/12 Dick wants the transactions labeled differently... so we have created a new transaction type
		die "Don't know how to create a trans_type: $container->{'merchant'}->{'trans_type'} coop sale transaction" if $container->{'merchant'}->{'trans_type'} != 22;

		$db->do("
			INSERT INTO transactions (
				id,
				spid,
				membertype,
				trans_type,
				vendor_id,
				amount,
				description,
				cv_mult,
				reb_mult,
				comm_mult,
				receivable,
				discount_amt,
				currency_code,
				native_currency_amount
			) VALUES (
				$recipient->{'id'},
				$recipient->{'spid'},
				'$recipient->{'membertype'}',
				$container->{'merchant'}->{'trans_type'},
				$container->{'merchant'}->{'vendor_id'},
				$usd_sale_price,
				?,
				$container->{'merchant'}->{'pp_multiplier'},
				$container->{'merchant'}->{'reb_mult'},
				$container->{'merchant'}->{'comm_mult'},
				$params->{'usd_comm'},
				$params->{'usd_comm'},
				?,
				$params->{'sale_price'}
			)", undef, $description, $params->{'sale_price_currency'});
		
		$db->do("			
			INSERT INTO coop.purchase_log (transfer_id, transactions_trans_id)
			SELECT $tx_log_id, CURRVAL('transactions_trans_id_seq')			
		");
	}
}

sub ReorganizeListByNetworkDepth
{
	my (@rv, @tmp) = ();
	foreach (@_)
	{
		$_ = $db->selectrow_array("SELECT id FROM members WHERE alias = ?", undef, uc($_)) if m/\D/;
		next unless $_;
		my $hr = {'id'=>$_};
		AdditionalMembershipDetails($hr);
		push @tmp, $hr;
	}
	
	for my $x (sort {$a->{'network_depth'} <=> $b->{'network_depth'}} @tmp)
	{
		push @rv, $x->{'id'};
	}
#die Dumper(\@rv);
	return @rv;
}

sub SalesFormElements
{
	my $frmId = shift;
	my $rv = qq|<tr><td>
<input type="text" class="w7em" id="sale_price_$frmId" name="sale_price" value="" />| .
#<input type="text" class="w7em" id="sale_price_$frmId" name="sale_price" onchange="setNativeCommission($frmId)" value="" />| .
	cboSalePriceCurrency($frmId) .

	qq|
</td><td>Sale Price</td></tr>
<tr><td>
	<input type="text" class="w7em" id="native_comm_$frmId" name="native_comm" value="0" disabled="disabled" /></td>
	<td>Native Commission</td></tr>
<tr><td>
	<input type="text" class="w7em" id="xr_display_$frmId" value="1" disabled="disabled" /></td>
	<td>Exchange Rate</td></tr>
<tr><td>
	<input type="text" class="w7em" id="usd_comm_$frmId" name="usd_comm" onchange="ensureMinimumCommission(this, $frmId)" value="0" /></td>
	<td>PP Value<!--USD Commission--></td></tr>
<!--tr><td>
	<input type="text" class="w7em" id="pp_value_$frmId" name="pp_value" value="0" disabled="disabled" /></td>
	<td>PP Value</td></tr-->|;

	return $rv;
}

sub SendEmail
{
	return 'Not sending mail as NOMAIL flag is set' if $NOMAIL;
	return 'Not sending mail due to errors while processing' if $no_mail_flag;
	 
	my $subject = $gs->GetObject($TMPL{'subject'}, $PartnerRecipient->{'language_pref'});
	return "Failed to retrieve template: $TMPL{'subject'}" unless $subject;
	
	# for auction related transfers, we'll doctor the email body a little
	if ($auction)
	{
		$Processed_List = _AuctionEmail();
	}

	my $upline = $db->selectrow_hashref('SELECT firstname, lastname, emailaddress FROM my_coach(?)', undef, $PartnerRecipient->{'id'});
	
	my %mail = (
		'Subject'			=> MailingUtils::DrHeaders($subject),
		'To'				=> MailingUtils::DrHeaders( qq|"$PartnerRecipient->{'firstname'} $PartnerRecipient->{'lastname'}"<$PartnerRecipient->{'emailaddress'}>| ),
		'Reply-To'			=> $meminfo->{'emailaddress'},
		'From'				=> 'no-reply@clubshop.com',
		'Message'			=> $gs->Prepare_UTF8($Processed_List,'encode'),
		'Content-type'		=> 'text/plain; charset="utf-8"'
	);
	
	$mail{'CC'} = MailingUtils::DrHeaders( qq|"$upline->{'firstname'} $upline->{'lastname'}"<$upline->{'emailaddress'}>| ) if $upline;
#	warn 'CC: ' . MailingUtils::DrHeaders( qq|"$upline->{'firstname'} $upline->{'lastname'}"<$upline->{'emailaddress'}>| ) if $upline;
	# for auction related transfers, we won't use the default Coop subject line or From address
	if ($auction)
	{
		$mail{'Subject'} = 'DHS Coop';
		$mail{'From'} = '"Coop Dept."<Coop@clubshop.com>';
	}
	
	# copy the coop manager in on these
#	$mail{'BCC'} = 'webmaster@dhs-club.com';# if $meminfo->{'id'} == 1;

#	print "$_:$mail{$_}\n" foreach keys %mail;
	my $rv = sendmail(%mail);
	return $rv ? 0 : $Mail::Sendmail::error;
}

sub TransferNonVIP
{
	my $tmp = shift;
	my $tx_log_id = shift;
	
	my $activity_sql = '';
	# if we do not have an activity defined for transferring this membertype, then skip it
	# also per Dick, do not create activities for transfers to 01
	if ($MTYPE_CODES{$tmp->{'membertype'}} && $recipient->{'id'} > 1)
	{
		$db->do("
			-- this statement creates a new signup activity for the new sponsor
			INSERT INTO activity_rpt_data (id, spid, activity_type, val_text)
			VALUES ($tmp->{'id'}, $recipient->{'id'}, $MTYPE_CODES{$tmp->{'membertype'}}, '$id Coop Purchase');
		");
	}

	$db->do("
		UPDATE members
		SET spid= $recipient->{'id'},
			memo= 'coop transfer initiated by $id',
			operator= ?
		WHERE id= $tmp->{'id'}", undef, ($operator || 'spec-coop.cgi'));

#	$db->do("
#	-- transfer the nspid... the nspid business rules regarding new members are likely to be complex and this is no place to take care of them
#		UPDATE network.spids SET spid= $recipient->{'id'}, memo='transferred nspid credit' WHERE id= $tmp->{'id'}");

# the activity report no longer uses the keys
# 12/05/14... yes it does, for the date selector drop-down
	$db->do("
	-- the next statement takes care of transferring activities of this
	-- party to the new upline
	-- by deleting the keys that go with the data, a new set of keys will be created
 		DELETE FROM activity_rpt_key
		WHERE pk IN
			(SELECT pk FROM activity_rpt_data WHERE id = $tmp->{'id'}
	-- don`t reset the original signup activity for the original sponsor
			AND activity_type !=1
			AND activity_type !=2)");
			
	$db->do("
	-- log the transfer
		INSERT INTO coop.transfer_log (
			coop_id,
			user_id,
			member_id,
			membertype,
			recipient_id,
			transfer_type,
			notes,
			transfer_id,
			operator
		) VALUES (
			$container->{'id'},
			$id,
			$tmp->{'id'},
			'$tmp->{'membertype'}',
			$recipient->{'id'},
			$params->{'transfer_type'},
			?,
			$tx_log_id,
			?
		);
	", undef, $params->{'transfer_notes'}, $operator);

}

sub TransferVIP
{
	my $tmp = shift;
	my $tx_log_id = shift;

	my $rv = ValidateTransfer($tmp);
	return $rv if $rv;

	$db->do("
		UPDATE members
		SET spid= $recipient->{'id'}, memo= 'coop transfer initiated by $id', operator= ?
		WHERE id= $tmp->{'id'}", undef, ($operator || 'spec-coop.cgi'));

# the keys are no longer used in the activity reports
# 12/05/14... yes it does, for the date selector drop-down
	$db->do("
	-- the next statement takes care of transferring activities of this
	-- party to the new upline
	-- by deleting the keys that go with the data, a new set of keys will be created
 		DELETE FROM activity_rpt_key
		WHERE pk IN
			(SELECT pk FROM activity_rpt_data WHERE id = $tmp->{'id'}
	-- don`t reset the original signup activity NOR the upgrade for the original sponsor
			AND activity_type NOT IN (1, 2, 12))");
	
	$db->do("
	-- this statement creates a new VIP upgrade activity for the new sponsor
		INSERT INTO activity_rpt_data (id, spid, activity_type, val_text)
		VALUES ($tmp->{'id'}, $recipient->{'id'}, $MTYPE_CODES{$tmp->{'membertype'}}, '$id Coop Purchase')");

# new Partners being in the TP line, we cannot be mucking around with nspids
#	$db->do("
#	-- transfer the nspid... the nspid business rules regarding upgrades are likely to be complex and this is no place to take care of them
#		UPDATE network.spids SET spid= $recipient->{'id'}, memo='transferred nspid credit' WHERE id= $tmp->{'id'}");
	
	$db->do("
	-- log the transfer
		INSERT INTO coop.transfer_log (
			coop_id,
			user_id,
			member_id,
			membertype,
			recipient_id,
			transfer_type,
			notes,
			transfer_id,
			operator
		) VALUES (
			$container->{'id'},
			$id,
			$tmp->{'id'},
			'$tmp->{'membertype'}',
			$recipient->{'id'},
			$params->{'transfer_type'},
			?,
			$tx_log_id,
			?
		);
	", undef, $params->{'transfer_notes'}, $operator);
	
	return undef;
}

sub TransferTP
{
	my $tmp = shift;
	my $tx_log_id = shift;

	my $rv = ValidateTransfer($tmp);
	return $rv if $rv;
	
	$db->do("
		UPDATE members
		SET spid= $recipient->{'id'}, memo= 'coop transfer initiated by $id', operator= ?
		WHERE id= $tmp->{'id'}", undef, ($operator || 'spec-coop.cgi'));

# the keys are no longer used at the activity reports
# 12/05/14... yes it does, for the date selector drop-down
	$db->do("
	-- the next statement takes care of transferring activities of this
	-- party to the new upline
	-- by deleting the keys that go with the data, a new set of keys will be created
 		DELETE FROM activity_rpt_key
		WHERE pk IN
			(SELECT pk FROM activity_rpt_data WHERE id = $tmp->{'id'}
	-- don`t reset the original signup activity NOR the upgrade for the original sponsor
			AND activity_type NOT IN (1, 2, 12))");
			
#	-- transfer the nspid... the nspid business rules regarding upgrades are likely to be complex and this is no place to take care of them
#	-- actually they should have the correct nspid as part of being a Trial Partner ;)
	
	$db->do("
	-- log the transfer
		INSERT INTO coop.transfer_log (
			coop_id,
			user_id,
			member_id,
			membertype,
			recipient_id,
			transfer_type,
			notes,
			transfer_id,
			operator
		) VALUES (
			$container->{'id'},
			$id,
			$tmp->{'id'},
			'$tmp->{'membertype'}',
			$recipient->{'id'},
			$params->{'transfer_type'},
			?,
			$tx_log_id,
			?
		);
	", undef, $params->{'transfer_notes'}, $operator);
	
	return undef;
}

sub ValidateTransfer
{
	my $x = shift;
	AdditionalMembershipDetails($recipient);
	AdditionalMembershipDetails($x);
	
	return "Transferee: $x->{'id'} is at a higher network depth: $x->{'network_depth'} than the recipient: $recipient->{'id'} - $recipient->{'network_depth'}\n"
		unless $x->{'network_depth'} > $recipient->{'network_depth'};
	return "Transferee: $x->{'id'} is in a different taproot: $x->{'pnsid'} than the recipient: $recipient->{'id'} taproot: $recipient->{'pnsid'}\n"
		unless $x->{'pnsid'} > 0 && $x->{'pnsid'} == $recipient->{'pnsid'};

	return undef;
}

sub _AuctionEmail
{
	return qq|Hello $recipient->{'firstname'},

Congratulations on winning the Clubshop Co-Op Marketing auction for:
$auction->{'description'}


Your new member(s):
$Processed_List


Regards,
Clubshop
Co-Op Marketing Support|;
}

# 12/04/07 added checking and exclusion of memberships that have been marked excluded
# 02/13/08 added checking and exclusion of members with members (lines)
# 09/12/08 added checking and exclusion of members awaiting a 'Jump Start'
# 09/15/08 major revisions to allow transferring of JS members and VIPs
# also am allowing multiple IDs to access a given container
###### 09/22/08 a few tweaks to make this thing available to admin for any "container" membership
# 10/01/09 revised to use the coop_admins table instead of the hash map
###### 11/10/09 added handling for auction related transfers for 01
# 05/13/10 added a simple validation of $num_to_transfer at it's initialization
###### 06/04/10 made it possible to transfer a number of JS members using the same interface function as regular members
###### 04/13/11 hack to allow Fab to sell to the network just like admin
###### 04/19/11 added the update to the "active" flag for Builder Interns making a purchase
###### 06/30/11 added the SQL necessary to setup a backend processing record to move AP transfers into a pool
###### 10/04/11 made it so the container ID can receive a transfer out of the coop
###### 02/14/12 added logging of transfers
###### 02/20/12 added the ability to create transactions whilst performing transfers
###### 04/18/12 expanded the SQL in the transaction creation to explicitly include the reb_mult and comm_mult since the default (1) kicks in otherwise
###### 12/11/12 added the upline coach as a CC on the notification email
###### 03/25/13 added the transferral of nspid credit to the recipient
###### 07/31/13 disabled the normal creation of certain activities when doing transfers to 01
# 06/16/14 added handling for Trial Partners, also had to break out multiple SQL statements into individual $db->do calls
# 11/10/14 broke apart the multiple SQL statements in Record_Sale to appease DBI on the newer server
# 05/20/15 added the binmode to deal with utf-8 issues brought on by the recent update of DBD::Pg
