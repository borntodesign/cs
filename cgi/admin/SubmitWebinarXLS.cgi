#!/usr/bin/perl -w
##
## SubmitWebinarXLS.cgi	- an interface for submitting a spreadsheet of webinar attendees for entry into the DB
##
## Deployed Oct. 2012	Bill MacArthur
##
## last modified: 11/17/14	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use cs_admin;
use ADMIN_DB;
require ParseWebinarXLS;

my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
$q->charset('utf-8');
my $action = $q->param('_action');

##
## get the admin user and try logging into the DB
##
#my $operator = $q->cookie('operator');
#my $pwd = $q->cookie('pwd');
my $user = $cs->authenticate;
exit unless my $db = ADMIN_DB::DB_Connect('generic', $user->{'username'}, $user->{'password'});

$db->{'RaiseError'} = 0;	# we do not want to die in the middle of things... we will handle errors explicitly

unless ($action)
{
	XlsForm();
}
elsif ($action eq 'record')
{
	Record();
}
elsif ($action eq 'spreadsheet')	# we are submitting a spreadsheet
{
	# we must be searching for an ID
	Parse();
}
elsif ($action eq 'SubmitForm')
{
	SubmitForm();
}
else
{
	die "Unhandled action";
}

Exit();

###### start of subs

sub Err
{
	print $q->header('text/plain'), $_[0];
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub Nav
{
	return $q->p( $q->a({'-href'=>$q->url}, 'Start Over') . ' - ' . $q->a({'-href'=>$q->url . '?_action=SubmitForm'}, 'Individual Entry Form') );
}

sub Parse
{
	# we should have a filehandle available to us from the submission
	my $fh = $q->upload('xls') || die "Failed to obtain the uploaded file";
	
	my $s = ParseWebinarXLS->new($fh) || die "Failed to instantiate the parser";
	my $rv = $s->Parse;	# that is a hashref
	Err($rv->{'error'}) if $rv->{'error'};

	($rv->{'timestamp'}) = $db->selectrow_array('SELECT ?::TIMESTAMP', undef, $rv->{'webinar_date_time'});

	SubmitForm($rv);
}

sub Record
{
	my %params = ();
	my @vals = ();
	foreach (qw/webinar_id webinar_name webinar_timestamp/)
	{
		$params{$_} = $q->param($_) || die "$_ is a required field";
		push @vals, $db->quote($params{$_});
	}
	
	my @ids = $q->param('id');
	die "No IDs received" unless @ids;
	
	my $sth = $db->prepare('INSERT INTO webinar_records (id, webinar_id, webinar_date, webinar_name) VALUES (?,' .
		$db->quote($params{'webinar_id'}) . ',' . $db->quote($params{'webinar_timestamp'}) . ',' . $db->quote($params{'webinar_name'}) . ')'
	);
	my $rv = '<table cellspacing="3" style="margin-top:1em">';
	my $rowclass = 'a';
	my %counts = (
		'success'=>0,
		'notfound'=>0,
		'errors'=>0
	);

	foreach(@ids)
	{
		next unless $_;
		
		$rv .= qq#<tr class="$rowclass"><td>$_</td><td>=> #;
		my $id = $_;
		($id) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, uc($_)) if $_ =~ m/\D/;
		
		unless ($id)
		{
			$rv .= "</td><td>Not Found</td></tr>";
			$counts{'notfound'}++;
			next;
		}
		
		$rv .= "$id</td><td>";
		
		my $i = $sth->execute($id);
		if ($db->err)
		{
			$rv .= $db->errstr;
			$counts{'errors'}++;
		}
		else
		{
			$rv .= 'Success';
			$counts{'success'}++;
		}
		
		$rv .= '</td></tr>';
	}
	$rv .= '</table>';

	print
		$q->header,
		$q->start_html(
			'-title' => 'Webinar XLS Import Results',
			'-encoding'=>'utf-8',
		),
		$q->h4('Webinar XLS Import Results'),
		Nav(),
		$q->table({'-style'=>'border: 1px solid navy'},
			$q->Tr( $q->td('Successful Entries:') . $q->td($counts{'success'}) ) .
			$q->Tr( $q->td('Not Found:') . $q->td($counts{'notfound'}) ) .
			$q->Tr( $q->td('Errors:') . $q->td($counts{'errors'}) )
		),
		$rv,
		$q->end_html;
}

sub SubmitForm
{
	my $rv = shift || {};

	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>'Parsing XLS',
			'-encoding'=>'utf-8',
			'-style'=>{'code'=>'th,td {text-align:left; vertical-align:top;}'}
			),
		$q->h4('Spreadsheet Import Results'),
		$q->p('If any fields are incorrect or empty, simply correct them.<br />If an ID should not be submitted, just delete the value.');
	
	print Nav();
	
	print
		$q->start_form('-method'=>'post'),
		$q->hidden('-name'=>'_action', '-value'=>'record', '-force'=>1),
		'<table id="mainTbl" cellspacing="5">',
		$q->Tr(
			$q->th('Webinar ID') .
			$q->td( $q->textfield('-name'=>'webinar_id', '-value'=>$rv->{'webinar_id'} || '') )
		),
		$q->Tr(
			$q->th('Extracted Date/Time') .
			$q->td($rv->{'webinar_date_time'} || '')
		),
		$q->Tr(
			$q->th('Timestamp to be entered into the DB<div style="font:8px normal sans-serif">A valid date/time should look like this: 2012-10-11 14:50:00</p>') .
			$q->td( $q->textfield('-name'=>'webinar_timestamp', '-value'=>$rv->{'timestamp'} || '') )
		),
		$q->Tr(
			$q->th('Webinar Name') .
			$q->td( $q->textfield('-name'=>'webinar_name', '-value'=>$rv->{'webinar_name'} || '') )
		),
		'<tr><th>ID/s to be submitted</th><td>';

	foreach (@{$rv->{'ids'}})
	{
		print $q->textfield('-name'=>'id', '-value'=>$_) , $q->br;
	}

	print $q->textfield('-name'=>'id', '-force'=>1),	# just a spare
		$q->br, $q->br,
		$q->submit,
		'</td></tr></table>',
		$q->end_form,
		$q->end_html;
}

sub XlsForm
{
	print
		$q->header,
		$q->start_html('-title'=>'Submit Webinar XLS', '-encoding'=>'utf-8'),
		$q->h4('Submit Webinar XLS'),
		$q->start_multipart_form,
		$q->hidden('-name'=>'_action', '-value'=>'spreadsheet'),
		'Select spreadsheet to upload: ',
		$q->filefield('-name'=>'xls'),
		$q->submit('-value'=>'Submit Spreadsheet', '-style'=>'margin-left:3em;'),
		$q->br, $q->br,
		$q->a({'-href'=>$q->url . '?_action=SubmitForm'}, 'Individual Entry Form'),
		$q->end_form.
		$q->end_html;
}
