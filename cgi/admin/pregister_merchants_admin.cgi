#!/usr/bin/perl -w
#########################################
# This script outputs the preregistered
# merchants from the "potential_merchant_affiliates"
# table.
#########################################
use lib('/home/httpd/cgi-lib');
use strict;
use CGI;
use G_S;
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use Members;
use PotentialMerchantAffiliates;
use URI::Escape;
use Data::Dumper;

##################################################################################
# Variable Declarations, and Object Instantiations
##################################################################################
my $CGI = CGI->new();
our $DB = {}; # used for the Database object later on.


my $operator = $CGI->cookie('operator');
my $pwd = $CGI->cookie('pwd');


##################################################################################
# Subroutines
##################################################################################

sub processTemplate
{
	my $params = shift;
	my $html_template = shift;

	foreach my $key (keys %$params)
	{
		$html_template =~ s/%%$key%%/$params->{$key}/g;
	}

	$html_template =~ s/%%.*%%//g;

	return $html_template;
}


sub displaySearchForm
{

	my $params = shift;

	$params->{title} = 'Pre-registered Merchant Affiliates Administration' if ! exists $params->{title};
	$params->{message} = 'Pre-registered Merchant Affiliates Administration' if ! exists $params->{message};

	my $html_form = <<EOT;
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<HTML>
		<HEAD>
		<TITLE>%%title%%</TITLE>
		</HEAD>
		<BODY bgcolor="#eeffee" onload="document.forms[0].search_value.focus()">
		%%error%%
		%%warning%%
		%%notice%%
		<H2 align="center">%%title%%</H2>
		<P>%%message%%</P>
		<br />
		<form method="post" action="%%action%%">
		<TABLE border="0">
		  <TBODY>
		    <TR>
			<TD>
			1) Select a Field to search by:
			</TD>
		       <TD>
		             <select size="1" name="search_field">
				        <option value="pk">Pre-registered Merchant Affiliate ID</option>
						<option value="id">Member Alias</option>
						<option value="referral">Sponsor ID</option>
						<option value="firstname">First Name</option>
						<option value="lastname">Last Name</option>
						<option value="company_name">Company Name</option>
						<option value="address1">Street Address</option>
						<option value="city">City</option>
						<option value="state">State/Province</option>
						<option value="country" selected="selected">Country</option>
						<option value="postalcode">Postal Code</option>
						<option value="phone">Phone Number</option>
						<option value="fax_num">Fax Number</option>
						<option value="emailaddress2">EMail Address</option>
						<option value="url">Website URL</option>
						<option value="biz_description">Business Description</option>
						<option value="contact_info">Contact Information</option>
						<option value="language_pref">Language Preference</option>
						<option value="accepted">Date Approved</option>
						<option value="member_id_assigned">Member ID</option>
						<option value="business_type">SIC Code</option>
			     </select>
		      </TD>
			 </TR>
			 <TR>
		      <TD>
		       2) Type your search string here:
			  </TD>
		      <TD>
			  	<INPUT size="50" type="text" name="search_value" maxlength="50">
			  </TD>
		      </TR>
		      <TR>
		      <TD align="right"><INPUT type="submit" value="Search"></TD>
		      <TD>
		      	<INPUT type="reset" value="Cancel">
		        <INPUT type="hidden" name="action" value="search">
		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>
		</FORM>
		</BODY>
		</HTML>

EOT


	print $CGI->header('text/html');

	print processTemplate($params, $html_form);
}

##########################################################
# displayPreregisteredMerchantList Displays a list of
# pre-registered merchants
#
# params array of hashrefs
#			The hashref keys should match the field names in the database for the "potential_merchant_affiliates" table.
#
# return string on error.
##########################################################
sub __displayPreregisteredMerchantList
{
	my $params = shift;

	my $content = {};

	$content->{title} = 'Pre-registered Merchant List' if ! exists $content->{title};
	$content->{message} = 'There were more than one result, choose from the Merchant below.' if ! exists $content->{message};

	my $html_form = <<EOT;
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<HTML>
		<HEAD>
		<TITLE>%%title%%</TITLE>
		<style type="text/css">
			<!--
				.accepted, .accepted a {
					color:#CCCCCC;
					list-style-image: url(/images/checkmark4.gif);
					list-style-type: none;
				}
			-->
        </style>
		</HEAD>
		<BODY bgcolor="#eeffee" onload="document.forms[0].search_value.focus()">
		%%error%%
		%%warning%%
		%%notice%%
		<H2 align="center">%%title%%</H2>
		<P>%%message%%</P>
		<br />
		<TABLE border="0">
		  <TBODY>
		    <TR>
	       	  <TD>
					%%content%%
		      </TD>
			 </TR>
			 <TR>
	       	  <TD>
					<br /><br />
		      </TD>
			 </TR>
			 <TR>
	       	  <TD>
					Potential Merchants with a check next to their name have been accepted as a Potential Merchant (they are not displayed in the merchant directory on the "www.clubbucks.com" site).
		      </TD>
			 </TR>
		  </TBODY>
		</TABLE>
		</BODY>
		</HTML>

EOT




	$content->{content} = '<ul>';

	foreach my $row (@$params)
	{
		my $url = $CGI->url;

		$url .= '?search_field=pk&search_value=' . uri_escape($row->{pk});

		if ($row->{accepted})
		{
			$content->{content} .= qq(<li class="accepted"><a href="$url">$row->{company_name}</a>; $row->{firstname} $row->{lastname}; $row->{address1}; $row->{city}, $row->{state} $row->{postalcode}</li>);
		} else {
			$content->{content} .= qq(<li><a href="$url">$row->{company_name}</a>; $row->{firstname} $row->{lastname}; $row->{address1}; $row->{city}, $row->{state} $row->{postalcode}</li>);
		}
	}

	$content->{content} .= '</ul>';

	print $CGI->header('text/html');
	print processTemplate($content, $html_form);

	return;

}

##########################################################
# displayPreregisteredMerchantForm Displays one
# pre-registered information so it is editable.
#
# params hash
#			The keys should match the field names in the database for the "potential_merchant_affiliates" table.
#			The country, and language dropdowns should be built before processing the template.
#
# return string on error.
##########################################################
sub __displayPreregisteredMerchantForm
{

	my $params = shift;

	eval{

		my $query_country_codes =<<EOT;
					SELECT
						country_name,
						country_code,
						restrictions
					FROM
						tbl_countrycodes

					ORDER BY country_name
EOT

		my $countries_hashref = {};
		my @countries = ();
		my $STH = $DB->prepare($query_country_codes);

		$STH->execute();

		while(my $row = $STH->fetchrow_hashref)
		{
			$countries_hashref->{$row->{country_code}} = $row->{country_name};
			push @countries, $row->{country_code};
		}

		$params->{country_dropdown} = $CGI->popup_menu(
				-name => 'country',
				-values => \@countries,
				-default => $params->{country},
				-labels => $countries_hashref
			);

	};
	if($@)
	{
			if (exists $params->{error})
			{
				$params->{error} .= '<br /> The country list was unavailable. ' . $@;
			} else {
				$params->{error} = 'The country list was unavailable. ' . $@;
			}
	}

	eval{

			my $query_languages = <<EOT;
						SELECT
							code2,
							description
						FROM
							language_codes
						WHERE
							code2 IS NOT NULL
						ORDER BY
							description
EOT

			my $language_hashref = {};
			my @languages = ();
			my $STH = $DB->prepare($query_languages);

			$STH->execute();

			while(my $row = $STH->fetchrow_hashref)
			{
				$language_hashref->{$row->{code2}} = $row->{description};
				push @languages, $row->{code2};
			}

			$params->{language_dropdown} = $CGI->popup_menu(
					-name => 'language_pref',
					-values => \@languages,
					-default => $params->{language_pref},
					-labels => $language_hashref
				);
	};
	if($@)
	{
			if (exists $params->{error})
			{
				$params->{error} .= '<br /> The language list was unavailable. ' . $@;
			} else {
				$params->{error} = 'The language list was unavailable. ' . $@;
			}
	}

	$params->{title} = 'Pre-registered Merchant Affiliate Application' if ! exists $params->{title};
	$params->{message} = $params->{title} if ! exists $params->{message};
	$params->{action} = '' if ! exists $params->{action};

	my $html_form =<<EOT;

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>%%title%%</title>
		<link rel="stylesheet" href="/css/memberapp_default.css" type="text/css" />
		<script src="/js/resource.js" type="text/javascript"></script>
		<script src="/js/state_list.js" type="text/javascript"></script>
		<script src="/js/getstates.js" type="text/javascript"></script>
	</head>
	<body onload="adjustInitialStateCtlFocus();setSelectPrompt(&quot;- Please select from this list -&quot;);">
		%%error%%
		%%warning%%
		%%notice%%
		<div style="width:700px; padding: 0 5px;">
		  <h3 style="text-align: center;">%%title%%</h3>
		  <p>%%message%%</p>
		  <p>
		  <form method="get" action="%%action%%" onsubmit="return sent('')" enctype="multipart/form-data">

		  	<input type="hidden" value="1" name="processMerchantForm" id="processMerchantForm" />
		  	<input type="hidden" value="%%pk%%" name="pk" id="pk" />

		    <table style="margin-right:auto; margin-left:auto;">
		      <tr>
		        <td class="field">Applicants Member ID, or Alias:<br />
		          <span class="ra">(If there is not one, one will be created on approval)</span></td>
		        <td class="input"><input type="text" class="main" name="id" onfocus="this.select()" value="%%id%%" />
		        </td>
		      </tr>

		      <tr>
		        <td class="field">Club Member ID:<br />
		          <span class="ra">(This will be created on approval if the member does not already have one.)</span></td>
		        <td class="input"><input type="text" class="main" name="member_id_assigned" onfocus="this.select()" value="%%member_id_assigned%%" /></td>
		      </tr>



		      <tr>
		        <td class="field">Referral ID Number:</td>
		        <td class="input"><input type="text" class="main" name="referral" onfocus="this.select()" value="%%referral%%" maxlength="200" /></td>
		      </tr>
		      <tr>
		        <td class="field">Personal First Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="firstname" onfocus="this.select()" value="%%firstname%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Personal Last Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="lastname" onfocus="this.select()" value="%%lastname%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Name of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="company_name" onfocus="this.select()" value="%%company_name%%" maxlength="100" /></td>
		      </tr>
		      <tr>
		        <td class="field">Address of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="address1" onfocus="this.select()" value="%%address1%%" maxlength="41" /></td>
		      </tr>
		      <tr>
		        <td class="field">City:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="city" onfocus="this.select()" value="%%city%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Country:<span class="ra">*</span></td>
		        <td class="input">%%country_dropdown%%</td>
		      </tr>
		      <tr>
		        <td class="field">State/Province:<span class="ra">*</span></td>
		        <td class="input"><select name="" class="main" style="display: none" id="state_select">
		            <option value=""></option>
		          </select>
		          <input type="text" class="main" name="state" onfocus="this.select();needs_country();" value="%%state%%" maxlength="50" style="display: inline" id="state_text" /></td>
		      </tr>
		      <tr>
		        <td class="field">Postal Code:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="postalcode" onfocus="this.select()" value="%%postalcode%%" maxlength="20" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Phone Number:</td>
		        <td class="input"><input type="text" class="main" name="phone" onfocus="this.select()" value="%%phone%%" maxlength="30" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Fax Number:</td>
		        <td class="input"><input type="text" class="main" name="fax_num" onfocus="this.select()" value="%%fax_num%%" maxlength="25" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Email Address:</td>
		        <td class="input"><input type="text" class="main" name="emailaddress2" onfocus="this.select()" value="%%emailaddress2%%" maxlength="100" /></td>
		      </tr>
		      <tr>
		        <td class="field">Prefered Language:<span class="ra">*</span></td>
		        <td class="input">%%language_dropdown%%</td>
		      </tr>
		      <tr>
		        <td class="field">Business Website Address (URL):</td>
		        <td class="input"><input type="text" class="main" name="url" onfocus="this.select()" value="%%url%%" /></td>
		      </tr>
		      <tr>
		        <td colspan="2" class="field">You must choose your Business Type from these categories. <img src="/images/arrow-right.gif" height="7" width="10" alt="-&gt;" /> <a href="javascript:void(0)" onClick="window.open('/cgi/biz_cat_search.cgi')">Search Business Categories</a> <span class="ra">*</span></td>
		      </tr>
		      <tr>
		        <td class="field">Type of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="biz_description" onfocus="this.select()" value="%%biz_description%%" maxlength="50" />
		          <input type="hidden" value="%%business_type%%" name="business_type" id="business_type" /></td>
		      </tr>
		      <tr>
		        <td class="field">Accepted:</td>
		        <td class="input">
		        	<input type="checkbox" name="accepted" value="1" %%accepted_checked%% /> %%accepted%%
		        </td>
		      </tr>
		      <tr>
		        <td class="field">Contact Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="contact_info" onfocus="this.select()" value="%%contact_info%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field" valign="top">Notes:</td>
		        <td class="input"><textarea name="notes" cols="40" rows="10" class="main" onfocus="this.select()">%%notes%%</textarea></td>
		      </tr>
		      <tr>
		        <td>
			        <input type="hidden" value="%%ipaddr%%" name="ipaddress" id="ipaddress" />
			        <input type="hidden" value="%%submitted%%" name="signup_date" id="signup_date" />
			        <input type="hidden" value="Pre-registered Merchant Application" name="landing_page" id="landing_page" />
		        <br />
		          <br />
		          <br />
		          <br /></td>
		        <td class="input">
		          <input type="submit" class="btn" value="Submit" />
		          &nbsp;&nbsp;
		          <input type="reset" class="btn" onclick="restoreStateFocus()" value="Reset" />
		        </td>
		        <tr>
			        <td class="field">Permanently Delete:</td>
			        <td class="input">
		        		<input type="checkbox" name="delete" value="1" />
		        	</td>
		      </tr>
		      </tr>
		    </table>
		  </form>
		  </p>
		  <p>&nbsp;</p>
		</div>
	</body>
</html>

EOT







	$params->{accepted_checked} = $params->{accepted} ? 'checked':'';

	print $CGI->header('text/html');
	print processTemplate($params, $html_form);

	return;
}

################################################
#
################################################
sub processPreregisteredMerchantForm
{
	use Members;
	use Encode;
	
	my $params = shift;


		my $search_field = 'pk';
		my $search_value = $CGI->param('pk');
		my $potential_merchant = {};

		my $submitted_data = {};


		$submitted_data->{pk} 					= $CGI->param('pk') ? decode_utf8($CGI->param('pk')) : '';
		$submitted_data->{id} 					= $CGI->param('id') ? decode_utf8($CGI->param('id')) : '';
		$submitted_data->{referral} 			= $CGI->param('referral') ? decode_utf8($CGI->param('referral')) : '';
		$submitted_data->{firstname}		 	= $CGI->param('firstname') ?  decode_utf8($CGI->param('firstname')) : '';
		$submitted_data->{lastname} 			= $CGI->param('lastname')  ?  decode_utf8($CGI->param('lastname')) : '';
		$submitted_data->{company_name} 		= $CGI->param('company_name') ?  decode_utf8($CGI->param('company_name')) : '';
		$submitted_data->{address1} 			= $CGI->param('address1')  ?  decode_utf8($CGI->param('address1')) : '';
		$submitted_data->{city} 				= $CGI->param('city') ? decode_utf8($CGI->param('city')) : '';
		$submitted_data->{state} 				= $CGI->param('state') ? decode_utf8($CGI->param('state')) : '';
		$submitted_data->{country} 				= $CGI->param('country') ? decode_utf8($CGI->param('country')) : '';
		$submitted_data->{postalcode} 			= $CGI->param('postalcode') ? decode_utf8($CGI->param('postalcode')) : '';
		$submitted_data->{phone} 				= $CGI->param('phone') ? decode_utf8($CGI->param('phone')) : '';
		$submitted_data->{fax_num} 				= $CGI->param('fax_num') ? decode_utf8($CGI->param('fax_num')) : '';
		$submitted_data->{emailaddress2} 		= $CGI->param('emailaddress2') ? decode_utf8($CGI->param('emailaddress2')) : '';
		$submitted_data->{url} 					= $CGI->param('url') ? decode_utf8($CGI->param('url')) : '';
		$submitted_data->{biz_description} 		= $CGI->param('biz_description') ? decode_utf8($CGI->param('biz_description')) : '';
		$submitted_data->{contact_info} 		= $CGI->param('contact_info') ? decode_utf8($CGI->param('contact_info')) : '';
		$submitted_data->{language_pref} 		= $CGI->param('language_pref') ? decode_utf8($CGI->param('language_pref')) : '';
		#$submitted_data->{submitted} 			= $CGI->param('submitted') ? $CGI->param('submitted') : 'NULL';
		$submitted_data->{accepted} 			= $CGI->param('accepted') ? decode_utf8($CGI->param('accepted')) : '';
		$submitted_data->{notes} 				= $CGI->param('notes') ? decode_utf8($CGI->param('notes')) : '';
		$submitted_data->{member_id_assigned} 	= $CGI->param('member_id_assigned') ? decode_utf8($CGI->param('member_id_assigned')) : 'NULL';
		$submitted_data->{business_type} 		= $CGI->param('business_type') ? decode_utf8($CGI->param('business_type')) : 'NULL';

		$submitted_data->{ipaddress}			= $CGI->param('ipaddress') ? decode_utf8($CGI->param('ipaddress')): '';
		$submitted_data->{signup_date}			= $CGI->param('signup_date') ? decode_utf8($CGI->param('signup_date')): '';
		$submitted_data->{landing_page}			= $CGI->param('landing_page') ? decode_utf8($CGI->param('landing_page')): '';


		$search_field =~ s/^\s+|\s+$//g;
		$search_value =~ s/^\s+|\s+$//g;

		if($CGI->param('delete'))
		{
			my $return_value = 0;
			my $notice = 'The Pre-registered Merchant record was removed. ';
			
			eval{
				my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);
				$return_value = $PotentialMerchantAffiliates->deleteMerchantRecord($submitted_data->{pk});
			};
			if($@)
			{
				displaySearchForm({error=>"There was an error processing the request.  Please try again, or contact the Internet Division. Error: " . $@});	
			}
			
			if($return_value)
			{
				$notice = 'There was an issue removing the requested Pre-registered Merchant.  Error: ' . $return_value;
			}
			
			displaySearchForm({notice=>$notice});
			
			return;
		}
		
		my $updated = {};

		eval{

			my $Members = Members->new($DB);
			my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);


			$updated = $PotentialMerchantAffiliates->updatePotentialMerchantInformation($submitted_data);

			$potential_merchant = $PotentialMerchantAffiliates->getPotentialMerchant({$search_field=>$search_value});


			if(defined $updated->{updated} && $updated->{updated})
			{
				$potential_merchant->[0]->{notice} = "The information was updated";
			} else {
				$potential_merchant->[0]->{notice} = "The information was not updated";
			}


			if(defined $updated->{new_member_info}->{member_id} && $updated->{new_member_info}->{member_id})
			{
				$potential_merchant->[0]->{notice} .= "<br />A new x membership was created for this member";

				if(! defined $PotentialMerchantAffiliates->{Members})
				{
					$PotentialMerchantAffiliates->setMembers();
				}

				if(!$Members->confirmMembership({id=>$updated->{new_member_info}->{member_id}}))
				{
					$potential_merchant->[0]->{notice} .= "<br />The member ship was confirmed.";
				} else {
					$potential_merchant->[0]->{warning} .= "<br />We were not able to confirm the membership.  <br />The members account is not active.<br />";
				}

				$PotentialMerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$updated->{new_member_info}->{member_id}, field=>'oid', force=>1});
			}



			__displayPreregisteredMerchantForm($potential_merchant->[0]);

	};
	if($@)
	{
	
		displaySearchForm({error=>"There was an error processing the request.  Please try again, or contact the Internet Division. $updated -- Error: " . $@});
	}




}

################################################
# displayPreregisteredMerchantInformation
# Params NONE the $CGI->params attritute is used.
#
#
################################################
sub displayPreregisteredMerchantInformation
{

	if(! $CGI->param('search_field') || ! $CGI->param('search_value'))
	{
		displaySearchForm({notice=>'No search criteria was specified.'});
		return;
	}

	eval{

		my $search_field = $CGI->param('search_field');
		my $search_value = $CGI->param('search_value');

		$search_field =~ s/^\s+|\s+$//g;
		$search_value =~ s/^\s+|\s+$//g;

		my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);

		my $potential_merchant = $PotentialMerchantAffiliates->getPotentialMerchant({$search_field=>$search_value});

		if(! $potential_merchant)
		{
			displaySearchForm({notice=>'There are not result, try another.'});
		}
		elsif(scalar @$potential_merchant == 1)
		{
			__displayPreregisteredMerchantForm($potential_merchant->[0]);
		}
		elsif (scalar @$potential_merchant > 1)
		{
			__displayPreregisteredMerchantList($potential_merchant);
		}
	};
	if($@)
	{
		displaySearchForm({'error'=>$@});
	}

}

################################################
# exitScript This came about because Bill
# was/is using goto statements.
################################################
sub exitScript
{
	$DB->disconnect if($DB);
	exit(0);
}

##################################################################################
# Main
##################################################################################

#########################################
# Connect to the DB
#########################################
unless ($DB = ADMIN_DB::DB_Connect( 'ma_real_admin.cgi', $operator, $pwd ))
{
	exit;
}

$DB->{RaiseError} = 1;

	if($CGI->param('processMerchantForm'))
	{
		processPreregisteredMerchantForm();
	}
	elsif($CGI->param('search_field') && $CGI->param('search_value'))
	{
		displayPreregisteredMerchantInformation();
	}
	else
	{
		displaySearchForm();
	}


exitScript();









