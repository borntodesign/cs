#!/usr/bin/perl -w
###### cbid_orders.cgi
######
###### This script allows the staff to submit a member order 
###### for "hard-stock" cards(i.e., brochures, hard cards, etc.) from our inventory.
######
###### Created: 05/09/2003	Karl Kohrt
###### Last modified: 05/05/14 Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
use ADMIN_DB;

###### GLOBALS

###### our html page templates
our %TMPL = (
	'order'	=> 10159,
	'display' => 10158);

###### the tables we'll work with
our %TBL = (
	'orders' => 'cbid_orders',
	'master' => 'clubbucks_master');

our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $dover_msg = '';

# Added an = before each fieldname so grep can identify the beginning of/and the whole word.
our @char_fields = qw/=member_id =layout =description =expiration/;
our @int_fields = qw/=order_id =cbid_start =cbid_end =num_sheets =class/;
our $cards_per_sheet = 0;
our $order_id = 0;
our $init_card1 = 0;
our $init_card2 = 0;
our $init_card3 = 0;
our $init_card4 = 0;
our $init_card5 = 0;
our $init_card6 = 0;
our $last_card = 0;
my $report = '';

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect( 'cbid_orders.cgi', $operator, $pwd );
$db->{'RaiseError'} = 1;

###### %the_order holds the values that have been submitted by the user. It has the form of 
###### 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'.
our %the_order = (
	'order_id' 	=> {'value'=>'', 'class'=>'n', 'req'=>0},
	'layout'	=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'member_id'	=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'num_sheets'=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'cbid_start'=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'cbid_end'	=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'description'=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'class'		=> {'value'=>'', 'class'=>'n', 'req'=>1},
	'expiration'=> {'value'=>'', 'class'=>'n', 'req'=>0}
);


###### If this is the first time through, just print the standard page.
unless ( $action )
{
	Load_Params();
	Do_Page();
}
elsif ($action eq 'order')
{
	# Load the submitted order data into the hash.	
	Load_Params();
	
	$cards_per_sheet = 0;
	Check_Layout();	# Check the layout of the sheet, assigning a value to $cards_per_sheet.
	Check_Required();	# Make sure all required fields are filled.
	Check_Validity();	# Check the validity of the field and the values provided by the user.

	# We'll do these only if the other checks are clean, since these checks require valid data. 
	unless ( $dover_msg )
	{
		Check_Full_Sheets();	# Check that the end - start represents a multiple of full sheets.
		Check_MemberID();	# Check to see if the member ID supplied is valid.
	}

	# If we have a 'do over' message at this point, redraw the order form with the error.
	if ($dover_msg)
	{
	}
	# No 'do over' message yet, so we will move on to database level checks.
	elsif ( Check_Avail() )		# Check that the requested CBID's are available.
	{
		# Set the first sheet's individual card ID's to the appropriate field in the database.
		Initialize_Sheet();

		# Update the ID references in the clubbucks_master table 
		#	and put order record in the cbid_orders table.
		if ( Update_Ids() )
		{
			# If successful, show the order.
			Show_Order();
			End();
		}
		# Otherwise, create an error message and display the original page.
		else
		{
			$dover_msg .= $q->span({-class=>'r'},
				"The previous order did not process normally, so it was aborted completely. 
				Please try again.<br />\n");
		}																					
 	}
	# If for any reason we have a $dover_msg, we will re-draw the page with the message.	
	Do_Page();
}

else { Err('Undefined URL action requested.') }

End();

###### ###### ###### ###### ######
###### Subroutines begin here
###### ###### ###### ###### ######

######	Check that the requested CBID's are available.
sub Check_Avail
{
	my $how_many = 0;
	my ($sth, $qry, $rv) = ();

	# Since cbid_end is the first card on the last sheet, we add a full sheet-1 to the number.
	$last_card = $the_order{cbid_end}{value} + ($cards_per_sheet-1);

	# Get count of available CBID's with class = 1 between cbid_start and cbid_end.
	$qry = "SELECT count(*)
		FROM $TBL{master}
		WHERE id BETWEEN $the_order{cbid_start}{value} AND $last_card
		AND class = 1";
	$sth = $db->prepare($qry);

	$rv = $sth->execute();
	defined $rv or die $sth->errstr;
	$how_many = $sth->fetchrow_array;

  	# Calculate how many CBID's were requested based upon the start and end CBID's provided.
	my $requested = 
	  ( $the_order{cbid_end}{value} - $the_order{cbid_start}{value} + $cards_per_sheet );

	# If the calculated CBID's doesn't match the number counted in the database. 
	if ( $requested != $how_many)
	{
		$dover_msg .= $q->span({-class=>'r'},
			"The Start and End numbers do not represent an available range of ClubBucks ID's. 
			Please correct your numbers.<br />\n");
		return 0;
	}
	$sth->finish();
	return 1;
}					

######	Check that the cbid_end - cbid_start represents a multiple of full sheets.
sub Check_Full_Sheets
{
	my $requested = 0;
		
	# If the user provided the values necessary.
	if ($cards_per_sheet && $the_order{cbid_end}{value} && $the_order{cbid_start}{value})
	{
		# Calculate how many sheets were requested based upon the CBID's provided.
		$requested = ((($the_order{cbid_end}{value} - $the_order{cbid_start}{value})
				/ $cards_per_sheet) + 1);
	}
 
	# If the calculated sheets doesn't match the actual number submitted. 
	if ( ($requested != $the_order{num_sheets}{value}) && $cards_per_sheet)
	{
		my $term = 'sheets';
		if ($cards_per_sheet == 1){
			$term = 'cards';
		} else {
			$requested = sprintf("%10.2f", $requested);
		}
		
		$dover_msg .= $q->span({-class=>'r'},
			"The Start and End numbers you entered represent $requested $term. 
			Please correct your numbers.<br />\n");
		return 0; 
	}
	return 1;
}					

######	Check which layout was selected.
sub Check_Layout
{
	if (($the_order{layout}{value} eq 'layout_3A') ||
		($the_order{layout}{value} eq 'layout_3B') ||
		($the_order{layout}{value} eq 'layout_3C'))
	{
		# One of the tri-fold layouts.
		$cards_per_sheet = 3;
	}
	elsif ($the_order{layout}{value} eq 'layout_6A')
	{
		# One of the tri-fold layouts.
		$cards_per_sheet = 6;
	}
	elsif ($the_order{layout}{value} eq 'single')
	{
		$cards_per_sheet = 1;
	}
}

######	Check to see if the Member ID is valid and the member is of a type that can order CB cards.
sub Check_MemberID
{
	my ($sth, $qry, $rv, $data) = ();
	my $search_field = '';
	my $id = $the_order{member_id}{value};		
	if ( $id =~ /\D/)
	{
		###### contains letters so it can't be an ID
		$id = uc $id;
		$search_field = 'alias = ';
	}
	elsif ( $id )
	{
		$search_field = 'id = ';
	}
	else { return 0; }	

	$search_field .= "\'$id\'";
	
	$qry = "SELECT count(id), id
		FROM members
		WHERE $search_field AND
		((membertype LIKE '%v%' ) OR
		membertype IN ('m','ma','ag','s','tp' ))
		GROUP BY id;";
	$sth = $db->prepare($qry);
	$sth->execute;
	$data = $sth->fetchrow_hashref;
	
	if ( $data->{count} == 0 )
	{
		 # The member ID is invalid.
		$the_order{member_id}{class} = 'r';
		$dover_msg .= $q->span({-class=>'r'}, "The member ID either does not exist or is not qualified to order cards.\n");
	}
	else
	{
		$the_order{member_id}{value} = $data->{id};			
	}				
}		
								
######	Check required fields and assign new html classes as necessary.
sub Check_Required
{
	foreach (keys %the_order)
	{
		if (($the_order{$_}{value} eq '') && ($the_order{$_}{req} == 1))
		{
		 	###### a required field is empty so we'll flag it.
			$the_order{$_}{class} = 'r';
			$dover_msg .= $q->span({-class=>'r'}, "Please fill in the required fields marked in <b>RED</b><br />\n");
		}
	}
	if ($dover_msg) { return 0; }
	else { return 1; }
}

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	my $hashkey = '';
	foreach $hashkey (keys %the_order)
	{
		if (grep { /=$hashkey/ } @char_fields)		
		{
			# Do nothing because all keyboard input is acceptable.
		}
		elsif (grep { /=$hashkey/ } @int_fields)
		{
			if ($the_order{$hashkey}{value} =~ /\D/)
			{
		 		# Contains letters so not valid for the number fields.
				$dover_msg .= $q->span({-class=>'r'}, "Invalid value in the $hashkey field - use only whole numbers.<br />");
			}
		}
		else 
		{
			$dover_msg .= $q->span({-class=>'r'}, "Undefined field $hashkey submitted.<br />\n");
		}
	}
}

sub Create_hidden
{
	my $hid;
###### we need to pass all our parameters that have values except the action param
	foreach ( $q->param() )
	{
		$hid .= $q->hidden($_) if ($_ ne 'action');
	}
	return $hid;
}

###### Print our main search form with any pertinent messages.
sub Do_Page
{
	my $hidden = '';# = &Create_hidden();
	my $tmpl = G_S::Get_Object($db, $TMPL{'order'}) || die "Couldn't open: $TMPL{'search'}";
	$tmpl =~ s/%%order_id%%/$order_id/g;
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$dover_msg/;
	$tmpl =~ s/%%hidden%%/$hidden/;
	
	foreach my $key (keys %the_order)
	{
		$tmpl =~ s/%%$key%%/$the_order{$key}{value}/g;
		$tmpl =~ s/%%class_$key%%/$the_order{$key}{class}/g;
	}
	my $ck = 'checked="checked"';
	# Since the layout field is a series of radio buttons, we have to handle it separately.
	if ( $the_order{layout}{value} eq 'layout_3A')
	{
		$tmpl =~ s/%%checked_3A%%/$ck/;
	}	
	elsif ( $the_order{layout}{value} eq 'layout_3B')
	{
		$tmpl =~ s/%%checked_3B%%/$ck/;
	}	
	elsif ( $the_order{layout}{value} eq 'layout_3C')
	{
		$tmpl =~ s/%%checked_3C%%/$ck/;
	}	
	elsif ( $the_order{layout}{value} eq 'layout_6A')
	{
		$tmpl =~ s/%%checked_6A%%/$ck/;
	}
	elsif ( $the_order{layout}{value} eq 'single')
	{
		$tmpl =~ s/%%checked_single%%/$ck/;
	}
	# whatever ones we have not touched are dealt with here, the placeholder we have touched no longer exists
	$tmpl =~ s/%%checked_3A%%//;
	$tmpl =~ s/%%checked_3B%%//;
	$tmpl =~ s/%%checked_3C%%//;
	$tmpl =~ s/%%checked_6A%%//;
	$tmpl =~ s/%%checked_single%%//;
	print $q->header(-expires=>'now'), $tmpl ;
}

sub End
{
	$db->disconnect;
	exit;
}
##### Prints an error message to the browser.
sub Err
{
	# -expires=>'now'  for the $q->header function.
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<br /><b>";
	print $_[0];
	print "</b><br />";
	print $q->end_html();
}

###### Set the first sheet's individual card ID's to the appropriate field in the database.
sub Initialize_Sheet
{
	# If these are trifold brochures.
   	if ($cards_per_sheet == 3)
	{			
		# Assign values to the cards on the first sheet depending upon the layout.
		if ( $the_order{layout}{value} eq 'layout_3A' )
		{
			$init_card1 = $the_order{cbid_start}{value};	
			$init_card2 = $the_order{cbid_start}{value} + 1;
			$init_card3 = $the_order{cbid_start}{value} + 2;
 		}
		elsif ( $the_order{layout}{value} eq 'layout_3B' )
		{
			$init_card1 = $the_order{cbid_start}{value} + 1;
			$init_card2 = $the_order{cbid_start}{value};
			$init_card3 = $the_order{cbid_start}{value} + 2;
		}
		elsif ( $the_order{layout}{value} eq 'layout_3C' )
		{
			$init_card1 = $the_order{cbid_start}{value} + 2;
			$init_card2 = $the_order{cbid_start}{value} + 1;
			$init_card3 = $the_order{cbid_start}{value};
		}
		else	# We've got a problem with the layout variable.
		{
				Err("I can't tell which layout you selected.");
				End();
		}
	}
   	elsif ($cards_per_sheet == 6)
	{			
		# Assign values to the cards on the first sheet depending upon the layout.
		if ( $the_order{layout}{value} eq 'layout_6A' )
		{
			$init_card1 = $the_order{cbid_start}{value};	
			$init_card2 = $the_order{cbid_start}{value} + 1;
			$init_card3 = $the_order{cbid_start}{value} + 2;
			$init_card4 = $the_order{cbid_start}{value} + 3;
			$init_card5 = $the_order{cbid_start}{value} + 4;
			$init_card6 = $the_order{cbid_start}{value} + 5;
		}
		else	# We've got a problem with the layout variable.
		{
				Err("I can't tell which layout you selected.");
				End();
		}
	}
	elsif ($cards_per_sheet == 1)
	{
		$init_card1 = $the_order{cbid_start}{value};
	}
	else	# We've got a problem with the $cards_per_sheet variable.
	{
		Err("I can't work with $cards_per_sheet cards per sheet. Call IT for help.");
		End();
	}
}

sub Load_Params
{
	###### populating our the_order array hash values
	###### this loop depends on the %the_order hash keys being identical to the passed parameter names
	foreach (keys %the_order)
	{
		$the_order{$_}->{'value'} = $q->param($_) || '';
	}
	
	# we should be able to trust admin submissions, but let's just make sure
	# validate the expiration value since it will be inserted directly into the SQL
	$the_order{'expiration'}->{'value'} = '' unless $the_order{'expiration'}->{'value'} eq "NOW() + INTERVAL '14 months'";
}

###### Call the Admin page to display the current order.
sub Show_Order
{
	my $path = "https://www.clubshop.com/cgi/admin/cbid_order_info.cgi?action=show&search_field=order_id&search_value=$order_id";
	print $q->redirect($path);
}

###### Build and run the update query for CBID numbers in the database.
sub Update_Ids
{
	# Get the order number.
	($order_id) = $db->selectrow_array("SELECT nextval('cbid_order_seq')") || die "Failed to obtain an order ID\n";

	# we used to create one biga** query and execute it at the end
	# instead, let's just execute each one inside of a transaction
	# if one statement fails, then the whole thing will automatically roll back anyway
	# this will make the execution much easier than trying one huge query... especially if we were to have hundreds of cards
	$db->begin_work;
	
	# Read the number of cards to be queried from the table.
	my $counter = 0;
	my ($qry, $rv) = ();
	my ($card1, $card2, $card3, $card4, $card5, $card6) = ();
	my $memo = "Order # $order_id";

	# If these are trifold brochures.
	if ($cards_per_sheet == 3)
	{		
		$report = "Primary #, secondary #, secondary #, etc...\n";
	}
	elsif ($cards_per_sheet == 6)
	{		
		$report = "Each line represents one sheet of cards.\n";
	}
	elsif ($cards_per_sheet == 1)
	{
		$report = "Individual cards. Ten cards to a line.\n"
	}

	$report .= "----------------------------------------\n";

	my $sscnt = 1;	# how we will keep track of how many cards to a line for single card orders
	
	for ($counter = 0;
		$counter < ($the_order{'num_sheets'}{'value'} * $cards_per_sheet);
		$counter += $cards_per_sheet)
	{
		# If these are trifold brochures.
		if ($cards_per_sheet == 3)
		{		
			# Query for the primary card on a sheet.
			$card1 = $init_card1 + $counter;
			$card2 = $init_card2 + $counter;
			$card3 = $init_card3 + $counter;
			$report .= "$card1, $card2, $card3\n";

			$db->do("
				UPDATE $TBL{'master'}
				SET referral_id = $the_order{'member_id'}{'value'},
					class = 2,
					stamp = NOW(),
					operator = CURRENT_USER,
					memo = ?			
				WHERE id = $card1;

				UPDATE $TBL{'master'}
				SET referral_clubbucks_id = $card1,
					class = 3,
					stamp = NOW(),
					operator = CURRENT_USER,
					memo = ?
				WHERE id = $card2
					OR id = $card3;", undef, $memo, $memo);
		}
		# If these are 6-card sheets.
		elsif ($cards_per_sheet == 6)
		{		
			$card1 = $init_card1 + $counter;
			$card2 = $init_card2 + $counter;
			$card3 = $init_card3 + $counter;
			$card4 = $init_card4 + $counter;
			$card5 = $init_card5 + $counter;
			$card6 = $init_card6 + $counter;
			$report .= "$card1, $card2, $card3, $card4, $card5, $card6\n";

			# Mark the first card's label field with a 1.
			$db->do("
				UPDATE $TBL{master}
				SET 	referral_id = $the_order{member_id}{value},
						class = $the_order{class}{value},
						label = '1',
						stamp = NOW(),
						operator = CURRENT_USER,
						memo = ?			
				WHERE id = $card1;

				UPDATE $TBL{master}
				SET 	referral_id = $the_order{member_id}{value},
						class = $the_order{class}{value},
						stamp = NOW(),
						operator = CURRENT_USER,
						memo = ?
				WHERE id = $card2
					OR id = $card3
					OR id = $card4
					OR id = $card5
					OR id = $card6;", undef, $memo, $memo);
		}
		elsif ($cards_per_sheet == 1)
		{
			$card1 = $init_card1 + $counter;
			my $qry = "
				UPDATE $TBL{'master'}
				SET 	referral_id = $the_order{'member_id'}{'value'},
						class = $the_order{'class'}{'value'},
						stamp = NOW(),
						operator = CURRENT_USER,
						memo = ?
			";
			$qry .= ", expiration= $the_order{'expiration'}->{'value'}" if $the_order{'expiration'}->{'value'};
			$db->do("
				$qry
				WHERE id = $card1", undef, $memo);

			$report .= "$card1,";

			if ($sscnt == 10)
			{
				$sscnt = 1;
				$report .= "\n";
			}
			else
			{
				$sscnt++;
			}
		}
		else	# We've got a problem with the $cards_per_sheet variable.
		{
				Err("I can't work with $cards_per_sheet cards per sheet. Call IT for help.");
				End();
		}
	}
	# remove trailing newline if present
	chomp $report;
	# remove trailing comma
	chop $report;

	# create our order record
	$db->do("INSERT INTO $TBL{'orders'}
		(	order_id,
			member_id,
			cbid_start,
			cbid_end,
			order_report,
			description)
		VALUES
		(	$order_id,
			$the_order{'member_id'}{'value'},
			$the_order{'cbid_start'}{'value'},
			$last_card,
			?,
			?)", undef, ($report, $the_order{'description'}->{'value'}));
	# if we haven't bombed by now, we should be good to go
	$db->commit;

	return 1;
}


###### 05/27/03 Added the explicit, bigint cast to the two queries in the Update_ID function.
######            Moved the member_id field into the @char_fields array so the aliases can be used.
###### 08/04/03 Added the functionality for ordering 6-card sheets.
###### 09/15/03 Added the ability to assign sponsorship instead of ownership to a set of cards.
###### 02/03/06 changed the master_qry execution to go inside a 'do' instead of a
###### prepare/execute
###### 05/14/09 some major revisions including the way the SQL is handled in Update_Ids,
###### and more notably for the inclusion of handling of single cards
# 11/10/11 added AMPs to the list of those who can get orders
###### 12/08/11 added the concept of an expiration date to the update query
# 05/05/14 replaced 'amp' with 'tp' in the membertype check list
