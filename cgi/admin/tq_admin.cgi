#!/usr/bin/perl -w
###### tq_admin.cgi
######
###### This script allows staff to move items to be translated into a working queue
###### and items that have been translated from the working queue into production.
######
###### Created: 11/04/2004	Karl Kohrt
######
###### Last modified: 05/22/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Encode::Guess;
use File::Copy;
use lib ('/home/httpd/cgi-lib');
use G_S;
use ADMIN_DB;
require cs_admin;

###### GLOBALS
my %TMPL = ( 'main'	=> 10347, 'language_list'=>10597 );
my $ARC_DIR = '/home/httpd/archives';
my $LOGFILE = "$ARC_DIR/log.txt";
my @db_result_fields = qw/pk full_lang stamp operator/;
my $db_result_header = qq#<tr>
				<td><span class="head">Resource Name</span><br />
					(Click to Populate Above)</td>
				<td class="head">View</td>
				%%edit%%
				<td class="head">Language</td>
				<td class="head">Last modified:</td>
				<td class="head">By:</td>
				%%complete-lock%%
				</tr>#;
my @file_result_fields = qw/language/;
my $file_result_header = qq#<tr>
				<td><span class="head">Resource Name</span> (Click to Populate Above)</td>
				<td class="head">View</td>
				<td class="head">Language</td>
				</tr>#;
# Tables.
my %TBL = (	'queue'	=> 'translation_edit_queue',
		'types'	=> 'translation_resource_types',
		'cgi_trans'	=> 'object_translations',
		'cgi_obj'	=> 'cgi_objects',
		'lang'		=> 'language_codes',
		'mallvx'	=> 'mall_vendor_extensions_for_translation');
# The resource types.
my %resource_types = (
 	1	=> { 	desc 		=> 'xml',
		 	location	=> '/home/httpd/html',
			preview_script=> '' },
	3	=> { 	desc 		=> 'translation template',
		 	location	=> $TBL{'cgi_trans'},
			value_column	=> 'value',
			preview_script=> '/cgi/admin/translation_templates.cgi?action=view;tid=' },
	4	=> { 	desc 		=> 'html',
		 	location	=> '/home/httpd/html',
			preview_script=> '' },
	5	=> {	desc		=> 'mall vendor extensions',
			location	=> $TBL{mallvx},
			value_column	=> 'value',
			preview_script=> '/cgi/admin/vms/mall-vendor-extensions.cgi?_action=detail;pk=' },
	6	=> { 	desc 		=> 'shtml',
		 	location	=> '/home/httpd/html',
			preview_script=> '' }
);
# Website home directories.
my %WEBDIR = ('/home/clubshop.com/html'	=> 'www.clubshop.com');
my $q = new CGI;
$q->charset('utf-8');
my ($db) = ();
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $pk = $q->param('pk');					# The pk for the queue table.
our $rid = ( $q->param('rid') || '' );			# Resource ID.
our $rtype = ( $q->param('rtype') || '' );		# Resource type.
our $rname = ( $q->param('rname') || '' );		# Resource name.
our $rlang = ( $q->param('rlang') || '' );		# Resource language.
#our $create_lang = ($rlang eq 'en') ? 'all' : $rlang;	# The language(s) to add/return.
our $create_lang = $q->param('create_lang') || $rlang;	# The language(s) to add/return.
our $list_lang = ( $q->param('list_lang') || '' );	# Previous listing language.
our $list_loc = ( $q->param('list_loc') 			# Previous listing location.
			|| $resource_types{$rtype}{'location'} 
			|| '' );
# No order parameter can be submitted at this time, but this is referenced below so keep it here for now.
my $order_field = ( $q->param('order') || '' );
our $message = '';
our $error = '';
our $add_list = '';
our $return_list = '';
our $lang_menu_search = '';
our $lang_menu_return = '';
our $lang_menu_add = '';
our $resource_types_menu = '';
my $db_resource = 0;
my $file_resource = 0;

##################################
###### MAIN starts here.
##################################

# Get our admin user and try logging into the DB.
my $cs = cs_admin->new({'cgi'=>$q});
my $ck = $q->cookie('cs_admin');
die "You must be logged in to use this resource\n" unless $ck;
my $creds = $cs->authenticate($ck);

exit unless $db = ADMIN_DB::DB_Connect( 'admin-rd', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $language_list = $gs->GetObject($TMPL{'language_list'}) || die "Couldn't open $TMPL{'language_list'}";

# If we have a defined type, flag the corresponding resource we are working with.
if ( $rtype )
{
	if ( $resource_types{$rtype}->{'location'} =~ m/\// )
	{
		$file_resource = 1;
	}
	else
	{
		$db_resource = 1;
	};
}
# Add an item to the translation queue.
if ($action eq 'add')
{
	# the global setting of the parameter is for legacy usage
	# we have subsequently allowed multiple languages to be selected for item creating
	my @rlang = $q->param('rlang');

	foreach my $rlang (@rlang)
	{
print STDERR "processing $rlang\n";
	# Unless it's already in the queue and this is not a request to add all languages.
		unless ( Exists( $rid, $rlang ) && $create_lang ne 'all' )
		{
	#		$message = '';
			my @languages = ();
			if ( $create_lang eq 'all' )
			{
	#			$language_list =~ s/en\|//;	# Remove English from the list to be added.
	#			@languages = split( /\|/, $language_list );
				foreach (split(/\|/, $language_list))
				{
					push @languages, $_ unless $_ eq 'en';
				}
			}
			else
			{
				$languages[0] = $rlang;
			}
			
			foreach my $language ( @languages )
			{
				# Put the item in the queue.								
				if ( Create( $language ) )
				{
					# Load success message.
					$message .= "<span class=\"b\">The '" . $resource_types{$rtype}{'desc'}. "' for the language '$language' was successfully added to the queue.<br/></span>\n";
				}
				else
				{
					$message .= "<span class=\"r\">Record NOT Created for language '$language'.<br /></span>\n";
				}
			}
		}
	}
}
# Return an item to production from the translation queue.
elsif ($action eq 'return')
{
	if ( Locked() )
	{
		$message .= "<span class=\"r\">The Return to production failed. Check to see if the record is locked.<br/></span>\n";
	}
	else
	{	
		if ( Return() )
		{
			# Load success message.
			$message .= "<span class=\"b\">The '" . $resource_types{$rtype}{'desc'}. "' for the language '$rlang' was successfully returned to production.<br/></span>\n";
			Delete_Queue();
		}
		else 
		{
			# Unless this has already been noted to be a file write error.
			unless ( $message =~ m/could not be written to/ )
			{
				$message .= "<span class=\"r\">The Return to production failed due to a database failure.<br/></span>\n";
			}
		}
	}
}
# List all the items requested.
elsif ($action eq 'list')
{
	# Overwrite the listing language from the previous page.
	$list_lang = $rlang;
	$list_loc = ( $q->param('rloc1') 
			|| $q->param('rloc2')
			||  $resource_types{$rtype}{'location'});
}
# Toggle a queue item's complete status.
elsif ($action eq 'complete')
{
	Toggle_Complete();
}
# Delete a queue item without returning it to production.
elsif ($action eq 'delete')
{
	if ( Delete_Queue() eq '0E0' )
	{
		$message .= "<span class=\"r\">The Delete request failed. Check to see if the record is locked.<br/></span>\n";
	}	
}
# Unlock a queue item to restore editing access.
elsif ($action eq 'unlock')
{
	Unlock();
}

# Build out the page unless we had an error.
unless ( $error )
{
	if ( $action )
	{
		###### we don't need to look at this list for completed translations
		$add_list = Get_List('add', $order_field) unless $action eq 'completed';

		$return_list = Get_List('return', $order_field);
	}
	
	Build_Menus();	
	Display_Page();
}
else
{
	Err($error);
}

$db->disconnect;
exit;

###### ###### ###### ###### ######
###### Subroutines start here.
##################################

###### Build out the list for display.		
sub Build_List
{
	my $list_type = shift;
	my @base_list = @_;
	my $list = '';
	my $result_fields = '';

	# If we have records to display.
	if ( scalar @base_list >= 1 )
	{
		$list .= '<table border="1" cellspacing="0" cellpadding="3" bgcolor="#ffffff">';
		if ( $db_resource || $list_type eq 'return' )
		{
			my $tmp_header = '';
			# If this is the return list, add the extra columns.				
			if ( $list_type eq 'return' )
			{
				($tmp_header = $db_result_header) =~ s/%%edit%%/<td><span class="head">
									Edit<\/span><\/td>/;
				$tmp_header =~ s#%%complete-lock%%#<td><span class="head">
										Complete<\/span><br />
										(Click to toggle)<\/td>
									<td><span class="head">
										Locked<\/span><br />
										(Click to unlock)<\/td>
									<td><span class="head">
										Delete<\/span><br />
										(Discard changes.)<\/td>#;
				push ( @db_result_fields, 'complete', 'locked' );
			}
			else			
			{
				($tmp_header = $db_result_header) =~ s/%%edit%%//;
				$tmp_header =~ s/%%complete-lock%%//;
			}
			$list .= $tmp_header;
			$result_fields = \@db_result_fields;
		}
		elsif ( $file_resource )
		{
			$list .= $file_result_header;
			$result_fields = \@file_result_fields;
		}

		# Display the various lines of data returned from the Get_List subroutine.
		foreach my $result (@base_list)
		{
			# Get the link value if this is the add list.			
			my $link_value = '';
			if ( $db_resource || $list_type eq 'return' )
			{
				$link_value = "$result->{'pk'}"
			}
			elsif ( $file_resource )
			{
				$link_value = "$result->{filename}";
			}
			
			# Find the language in which this item is written.
			my $this_lang = '&nbsp;';
			foreach ( keys %{$result} )
			{
				if ( $_ =~ m/language/ )
				{
					$this_lang = $result->{$_};
				}
			}
			
			# The Add/Return link for the item.
			my $action_link = qq#<td class="fp">#;
			if ( $result->{'complete'} || $list_type eq 'add' )
			{
				$action_link 	.= qq|<a href="#" onclick="return PopulateLine('$list_type','$rtype','$this_lang','|;
				if ( $list_type eq 'add' )
				{
					$action_link .= $link_value;
					if ( $db_resource )
					{						
						# Use the main object template's number & name.
						$action_link .= "','" . $q->escapeHTML("$result->{'obj_key'}: $result->{'obj_name'}") . qq#')">#;
						$action_link .= $q->escapeHTML("$result->{'obj_key'}: $result->{'obj_name'}");
					}
					elsif ( $file_resource )			
					{
						# Use the file's name.
						$action_link .= qq#','$link_value')">#;
						$action_link .= $link_value;
					}
					$action_link .= "</a>\n";
				}
				elsif ( $list_type eq 'return' )
				{
					# The original table's pk and queue table pk for value lookup later.
					$action_link .= $result->{'resource_identifier'};
					$action_link .= qq#','$result->{'name'}','$result->{'pk'}')">#;
					$action_link .= "$result->{'name'}</a>\n";
				}
			}
			else
			{			
				$action_link .= $result->{'name'};
			}
			$action_link .= "</td>\n";
												
			# The View/Edit link.
			my $view_link = '';
			my $edit_link = '';
			if ( $list_type eq 'add' )
			{
				$view_link = qq|<td class="fp">
						<a href="#" onclick="window.open('|
						. $resource_types{$rtype}->{'preview_script'};
				foreach ( keys %WEBDIR )
				{
					$link_value =~ s/($_)/http:\/\/$WEBDIR{$_}/;
				}
				
				$view_link .= $link_value;
				if ( $list_loc eq 'lexicons' )
				{
					$view_link .= ";lex";
				}
				
				$view_link .=  qq#', 'view'); return false;">View#;
				if ( $db_resource && ! $list_loc eq 'lexicons' )
				{
					$view_link .= "/Edit";
				}
				$view_link .=  qq#</a></td>#;
			}
			elsif ( $list_type eq 'return' )
			{
				$edit_link = qq|<td class="fp">
					<a href="#" onclick="window.open('|
						. "/cgi/t/translation_editor.cgi?action=edit;rid="
						. $link_value
						. qq#', 'edit'); return false;">Edit</a></td>#;
				$view_link = qq|<td class="fp">
					<a href="#" onclick="window.open('|
						. "/cgi/admin/template_preview.cgi/"
						. $link_value
						. qq#;tq', 'view'); return false;">View</a></td>#;
			}
			my $result_list = '';			
			# The results list in a row.	
			foreach ( @$result_fields )
			{
				unless ( $_ eq 'pk' || $_ =~ m/value$/ )
				{
					if ( $_ eq 'complete' )
					{
						if ( $result->{'complete'} eq '1' )
						{
							$result->{'complete'} = 'Yes';
						}
						else 
						{
							$result->{'complete'} = 'No';
						}
						$result->{'complete'} = qq#<a href="$ME?action=complete;pk=$result->{'pk'};rtype=$rtype;list_lang=$list_lang;list_loc=$list_loc">$result->{'complete'}</a>#;
					}
					elsif ( $_ eq 'locked')
					{
						if ( $result->{'locked'} )
						{
							$result->{'locked'} = qq#<a href="$ME?action=unlock;pk=$result->{'pk'};rtype=$rtype;list_lang=$list_lang;list_loc=$list_loc">Yes</a>#;
						}
						else
						{
							$result->{'locked'} = "No";
						}						
					}
					else
					{					
						$result->{$_} = CGI::escapeHTML($result->{$_}) || '&nbsp;';
					}
					$result_list .= qq#<td class="fp">$result->{$_}</td>\n#;
				}
			}
			# The delete link.
			my $delete_link = '';
			if ( $list_type eq 'return' )
			{
				$delete_link = qq|
					<td class="fp">
					<a href="#" onclick="DeleteResource('$result->{'pk'}','$rtype','$list_lang','$list_loc'); return false;">Delete Me</a>
					</td>\n|;
			}
			$list .= "<tr>\n"
				. $action_link
				. $view_link
				. $edit_link
				. $result_list
				. $delete_link
				. "</tr>\n";
		}
		$list .= "</table>";
	}
	else
	{
		unless ( $message eq '' )
		{
			$message .= "<br />";
		}
		$message .= "<span class=\"b\">No '"
					. $resource_types{$rtype}{'desc'}
					. "' resources";
		if ( $rlang )	{ $message .= " in the language '" . $rlang . "'"; }
		$message .= " were found in the ";
		if ( $list_type eq 'return' ) { $message .= "Edit Queue."}
		elsif ( $list_type eq 'add' )
		{
			if ( $file_resource ) { $message .= "directory " }
			elsif ( $db_resource ) { $message .= "table " }
			$message .= "'$list_loc'.";
		}
		$message .= "</span>";
	}
	return $list;
}

###### Build the different drop-down menus for the page.
sub Build_Menus
{
	$resource_types_menu = cboResources();
	$lang_menu_search = cboLanguage('');
	$lang_menu_return = cboLanguage('return');
	$lang_menu_add = cboLanguage('add');
}

###### Create a drop-down menu of language choices.
sub cboLanguage
{
	my $form_name = shift;
	my $default_value = $list_lang;
	my %labels = (''=>'Select a Language');
	my @values = '';
	my $tmp = '';
	my $sth = $db->prepare("
		SELECT code2, description
		FROM 	language_codes
		WHERE 	active = TRUE
		AND code2 ~ '$language_list'
		ORDER BY description;");
	$sth->execute();
	if ($form_name ne 'add')
	{
		$labels{'0'} = 'All';
		push @values, 'All';
	}

	while ( $tmp = $sth->fetchrow_hashref )
	{
		$labels{$tmp->{'code2'}} = $tmp->{'description'};
		push @values, $tmp->{'code2'};
	}
	my %attr = (
			-name=>'rlang',
			-size=>1,
			-values=>\@values,
			-labels=>\%labels,
			-default=>$default_value,
			-class=>'ctl',
			-force=>1,
			-onclick=>"LanguageChange('$form_name',this)"
	);

	if ($form_name eq 'add')
	{
		$attr{-multiple} = 'multiple';
		$attr{-style} = 'position:absolute; width:200px;top:-10px;';
		$attr{id} = 'addLangs';
		delete $attr{-onclick};
	}
	return $q->popup_menu(%attr);
}

###### Create a drop-down menu of resource types.
sub cboResources
{
	my $default_value = $rtype;
	my %labels = ();
	my @values = ();

	$labels{''} = 'Select a Resource blah';
	push @values, '';
	foreach ( sort {$a <=> $b} keys %resource_types )
	{
#Err("$_= $resource_types{$_}");
		unless ( $_ eq '' )
		{
			$labels{$_} = $resource_types{$_}{'desc'};
			push @values, $_;
		}
	}
	return $q->popup_menu(
		-name=>'rtype',
		-values=>\@values,
		-labels=>\%labels,
		-default=>$default_value,
		-class=>'ctl',
		-onchange=>'CheckClear()',
		-force=>1);
}

###### Create a new record with the information from the form.
sub Create
{
	my $language = shift;
	my $qry = '';
	my $qry_end = '';
	my $sth = '';
	my $rv = '';
	my @list = ();
	my $item_value = '';
	my $this_rid = '';		# This can be changed during multiple translations.
	my $base_file = '';
	my $lang_ext = '';
	my $new_marker = '-';	# Used to mark the $rid if this item has never existed before.

	# Just in case we don't have a name by this point.
	unless ( $rname ) { $rname = $resource_types{$rtype}{'desc'} . " " . $rid }

	# Get the language extension and base filename for any file resource.
	if ( $file_resource )
	{
		# Check for the language extension on the filename.
		$lang_ext = substr($rid, -2);

		# If in the allowed language list, remove the extension to get the base filename.
		if ( $lang_ext =~ m/$language_list/ ) { $base_file = substr($rid, 0, -3) }
		# Otherwise, it may have double extensions, so remove them.
		else
		{ 
			($base_file = $rid) =~ s/\.html\.html$/\.html/;
			$base_file =~ s/\.xml\.xml$/\.xml/;
			$base_file =~ s/\.shtml\.shtml$/\.shtml/;
		}
	}

	# In the case of multiple item creations,
	# we must check to see if this item already exists in each available language.
	if ( $create_lang eq 'all' )	
	{
		if ( $db_resource )	
		{
			$sth = $db-> prepare("
				SELECT r1.$resource_types{$rtype}{'value_column'}, r1.pk
				FROM 	$resource_types{$rtype}{'location'} r1,
					$resource_types{$rtype}{'location'} r2
				WHERE 	r1.language = ?
				AND 	r1.obj_key = r2.obj_key
				AND	r2.pk = ?");
			$sth->execute( $language, $rid );
			( $item_value, $this_rid ) = $sth->fetchrow_array();
			$sth->finish;

			# If we didn't find an rid for this one, this must be a new translation,
			#  so mark it with the negative id of the original,
			#  so we can reference it later to create a new item.
			unless ( $this_rid ) { $this_rid = $new_marker . $rid }
		}
		elsif ( $file_resource )
		{
			my $source_file = '';

			# Look for all files with the same base name as that submitted.
			# This way, a previously translated document can be used, saving a lot of
			#  translation time - as would be the case with an English document.
			while ( <$base_file*> )
			{
				# Check for the language extension on the filename.
				$lang_ext = substr($_, -2);
				# If it matches the language we are creating, make it the file to use.
				if ( $lang_ext eq $language )
				{
					$source_file = $_;
					last;
				}
			}
			# If we didn't find a matching language source file, default to the original.
			unless ( $source_file )
			{
				$source_file = $rid;
			}
			
			# Put the file's contents into a variable.
			$/ = undef;					
			open(DATA, $source_file);
			binmode DATA, ":encoding(utf8)";
			while ( <DATA> )
			{
				$item_value = $_ ;
			}
			close DATA;

			# Rename the file as needed.
			$this_rid = Rename_File($language, $base_file);
		}		
		# If we found something, we need to quote it properly for db insertion.
		if ( $item_value )
		{
			$item_value = $db->quote($item_value);
		}
	}

	# Unless we found an item value above.
	unless ( $item_value )
	{
		if ( $db_resource )
		{
			# We will use the value of the submitted item.
			($item_value, my $item_lang) = 
				$db->selectrow_array("
					SELECT $resource_types{$rtype}->{'value_column'}, language
					FROM $resource_types{$rtype}->{'location'}
					WHERE pk = $rid");
			# If the queried language doesn't match the submitted language,
			#  this must be a new translation, so mark it with the negative id
 			#  of the original, so we can reference it later to create a new item.
			if ( $item_lang ne $create_lang )
			{
				$this_rid = $new_marker . $rid;
			}
		}
		elsif ( $file_resource )
		{
			# Put the original file's contents into a variable.
			$/ = undef;
			open(DATA, $rid);
			binmode DATA, ":encoding(utf8)";
			while ( <DATA> )
			{
				$item_value = $_ ;
			}
			close DATA;

			# Rename the file as needed.
			$this_rid = Rename_File($language, $base_file);

		}
		# If we found something, we need to quote it properly for db insertion.
		if ( $item_value )
		{
			$item_value = $db->quote($item_value);
		}
		# In case we find/create an empty file.
		else
		{
			$item_value = $db->quote('');
		}

		# This will happen if it is an individual item submission.
		unless ( $this_rid )
		{
			$this_rid = $rid;
		}
	}
#	if ( $file_resource )
#	{
#		# Convert the object value to utf-8 if it is latin1.
#		my $enc = guess_encoding($item_value, 'latin1');
#		if (ref($enc)) 
#		{
#			$item_value = $gs->Conv2UTF8($item_value);
#			$message .= "<span class=\"b\">(UTF-8 conversion) - </span>\n";
#		}
#	}
	# Unless this item already exists in the queue.
	unless ( Exists( $this_rid, $language ) )	
	{
		# Insert the new record into the queue.
		$qry = "INSERT INTO $TBL{'queue'}
					( 	stamp,
						resource_identifier,
						resource_type,
						resource_value,
						language,
						name
					) VALUES (
						NOW(), ?, ?, $item_value, ?, ?
					);";
#Err("this_rid= $this_rid, rtype= $rtype, language= $language, rname= $rname");
#Err($qry);
		$sth = $db->prepare(	$qry );
		$rv = $sth->execute( $this_rid, $rtype, $language, $rname );
		$sth->finish;
	}	
	return $rv; 
}

###### Delete the requested from the database.
sub Delete_Queue
{
	my $sth = $db->prepare("DELETE FROM $TBL{'queue'} WHERE pk = ? AND locked IS NULL");
	my $rv = $sth->execute($pk);
	$sth->finish;
	return $rv;	
}

###### Display the list of records returned from the Get_List subroutine.
sub Display_Page
{
	my $tmpl = $gs->GetObject($TMPL{'main'}) || die "Couldn't open $TMPL{'main'}";

	$tmpl =~ s/%%me%%/$ME/g;
	$tmpl =~ s/%%message%%/$message/;
	$tmpl =~ s/%%add_select_list%%/$add_list/;	
	$tmpl =~ s/%%return_select_list%%/$return_list/;	
	$tmpl =~ s/%%lang_menu_search%%/$lang_menu_search/g;
	$tmpl =~ s/%%lang_menu_return%%/$lang_menu_return/g;
	$tmpl =~ s/%%lang_menu_add%%/$lang_menu_add/g;
	$tmpl =~ s/%%resource_types%%/$resource_types_menu/g;
	$tmpl =~ s/%%list_lang%%/$list_lang/g;
	$tmpl =~ s/%%list_loc%%/$list_loc/g;

	# print the page header here so we can view Unicode.
	print $q->header(	-expires=>'now',
				-type=>'text/html'), $tmpl;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $q->div($_[0]);
	print $q->end_html();
}

##### Checks to see if the item is already in the queue.
sub Exists
{
	my  $rid = shift;
	my $rlang  = shift;
	# Check to see if a translated version exists already.	
	my $sth = $db->prepare("
		SELECT pk
		FROM 	$TBL{'queue'}  
		WHERE 	resource_identifier = ?
		AND	language = ?;");
	my $rv = $sth->execute( $rid, $rlang );
	$rv = $sth->fetchrow_array();
	$sth->finish;

	if ( $rv ) {
		$message .= "	<span class=\"r\">
			The '$resource_types{$rtype}{'desc'}' 
			for the language '$rlang' is already in the queue.
			&nbsp;</span>\n";
	}
	return $rv; 
}

###### Gets the list of the requested resource type.
sub Get_List
{
	my $list_type = shift;
	my $order_field = shift;
	my $list = '';
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my $data = '';
	my @base_list = ();	
	my $link = '';

	# If this resource is kept in the database.
	if ( $db_resource || $list_type eq 'return' )
	{
		# Prepare the query to get the list data.
		if ( $list_type eq 'add' )
		{
# 			if ($rtype == 5)	###### handle mall vendor extensions than cgi templates
# 			{
# 				$qry = "SELECT lc.description AS full_lang, malls.language_code AS language,
# 						mvx.stamp, mvx.operator, mvx.val_text AS value,
# 						malls.label||':'||v.vendor_name||':'||mvxt.label||':'||mvx.pk AS obj_name
# 					FROM mall_vendor_extensions mvx
# 					INNER JOIN mall_vendor_extension_types mvxt ON mvxt.exttype=mvx.exttype
# 					INNER JOIN malls ON malls.mall_id=nvx.mall_id
# 					INNER JOIN vendors v ON v.vendor_id=mvx.vendor_id
# 					INNER JOIN language_codes lc ON lc.code2=malls.language_code
# 					WHERE mvx.translate=TRUE";
# 			}
# 			else
# 			{
				$qry = "	SELECT  %%add_fields%% r.*,
							lc.description AS full_lang							
						FROM $resource_types{$rtype}{'location'} r
							LEFT JOIN language_codes lc
							ON r.language::VARCHAR = lc.code2::VARCHAR";
				# If this is the object translations list, let's get the base template's name.
				if ( $resource_types{$rtype}{'location'} eq 'object_translations' )
				{			
					$qry =~ s/%%add_fields%%/obj_name, /;
					$qry .= " 	LEFT JOIN $TBL{'cgi_obj'} ON obj_key = obj_id ";
					$order_field = 'obj_name';
				}
				else {	$qry =~ s/%%add_fields%%//; }
				$qry .= " WHERE r.language IS NOT NULL and r.language != ''";
#			}
		}
		elsif ( $list_type eq 'return' )
		{
			$qry = "SELECT r.*,
					lc.description AS full_lang	 
				FROM 	$TBL{'queue'} r
					LEFT JOIN language_codes lc
					ON r.language::VARCHAR = lc.code2::VARCHAR
				WHERE	";
			###### we'll ignore the actual resource types when we want 'em all
			$qry .= ($action ne 'completed') ? "r.resource_type = '$rtype' " : 'complete=TRUE';
		}
		# If we have a language constraint.
		if ( $list_lang )
		{
			$qry .= " AND r.language = '$list_lang'";
		}
		# If we've been passed a field to order by. 
		if ( $order_field )
		{
			$qry .= " ORDER BY $order_field";
		}
		$sth = $db->prepare($qry);
#Err($qry);
		$rv = $sth->execute();
		while ( $data = $sth->fetchrow_hashref )
		{
			push (@base_list, $data);
		}		
		$sth->finish;
	}
	# If this resource is kept in a directory/file.	
	elsif ( $file_resource )
	{
		my $data = {};
		# The directory we are working in is either from the list, user provided, or the hash default.
		my $location_field = $list_loc; 

		# Look in $location_field for all files of type {desc}.
		while ( <$location_field/*.$resource_types{$rtype}{'desc'}*> )
		{
			$data->{'filename'} = $_;

			# Check for the language extension on the filename.
			my $lang_ext = substr($_, -2);
			# If it's in the list that we allow, use it, otherwise default to English.
			if ( $lang_ext =~ m/$language_list/ )
			{
				$data->{'language'} = $lang_ext;
			}
			else
			{
				$data->{'language'} = 'en';
			}
			
			# If the extension matches the requested language,
			#  or their was no language requested, put the doc on the list.			
			if ( ! $list_lang || $data->{'language'} eq $list_lang )
			{
				push (@base_list, $data);
			}
			$data = {};
		}		
	}
	# Now build out the list.
	$list = Build_List($list_type, @base_list);
	return $list;
}

###### Check a queue item to see if it is locked.
sub Locked
{
	my $sth = $db->prepare("SELECT locked FROM $TBL{queue} WHERE pk = $pk");
	$sth->execute();
	my $lock = $sth->fetchrow_array();
	$sth->finish;
	return $lock;
}

###### Rename a file based upon language information that was passed.
sub Rename_File
{
	my $new_lang = shift;
	my $base_file = shift;
	my $this_rid = '';

	# If this is the default, English file of the standard type, use the original filename.
	if ( ($rid =~ m/\.html\.html$/ || $rid =~ m/\.xml\.xml$/ || $rid =~ m/\.shtml\.shtml$/)
		&& $new_lang eq 'en' )
	{
		$this_rid = $rid;
	}
	# If this is the default, English html file of non-standard type, change it to the standard type.
	elsif ( ($rid =~ m/\.html$/ || $rid =~ m/\.html.en$/) && $new_lang eq 'en' )
	{
		$this_rid = $base_file . '.html';
	}
	elsif ( ($rid =~ m/\.shtml$/ || $rid =~ m/\.shtml.en$/) && $new_lang eq 'en' )
	{
		$this_rid = $base_file . '.shtml';
	}
	# If this is the default, English xml file of non-standard type, change it to the standard type.
	elsif ( ($rid =~ m/\.xml$/ || $rid =~ m/\.xml.en$/) && $new_lang eq 'en' )
	{
		$this_rid = $base_file . '.xml';
	}
	# Otherwise, add the language extension to the end.
	else
	{
		$this_rid = $base_file . "." . $new_lang;
	}

	return $this_rid;
}

###### Return the item to production.
sub Return
{
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my $cgi_object_key = '';

	# If no db record exists in production, we'll create it.
	if ( $db_resource && $rid < 0 )
	{
		# Table object_translations needs an obj_key value.
		if ( $resource_types{$rtype}->{'location'} eq $TBL{'cgi_trans'} )
		{
			$cgi_object_key = $db->selectrow_array("
				SELECT obj_key
				FROM	$TBL{'cgi_trans'}
				WHERE	pk = $rid * -1;");
		}						
		# Insert the new record into the original table.
		$qry = "INSERT INTO $resource_types{$rtype}->{'location'}
					( 	value,
						language";
		if ( $cgi_object_key )
		{
			$qry .= ", obj_key";
		}
		$qry .= ") VALUES (( SELECT resource_value 
					FROM 	$TBL{'queue'} 
					WHERE 	pk = $pk),
					'$rlang'";
		if ( $cgi_object_key )
		{
			$qry .= ", $cgi_object_key";
		}
		$qry .=  ");";
		$sth = $db->prepare($qry);
		$rv = $sth->execute();
		$sth->finish;
	}
	# If a db record exists, we'll update it.
	elsif ( $db_resource )
	{
		$qry = "UPDATE $resource_types{$rtype}{'location'} 
			SET value = ( SELECT resource_value 
						FROM 	$TBL{'queue'} 
						WHERE 	pk = $pk),
				language = '$rlang',
				stamp = NOW(),
				operator = '$creds->{'username'}'";
		if ( $cgi_object_key )
		{
			$qry .= ", obj_key = $cgi_object_key";
		}
		$qry .= " WHERE pk = $rid;";
		$sth = $db->prepare($qry);
		$rv = $sth->execute();
		$sth->finish;
	}
	# If this is a file.
	elsif 	( $file_resource )
	{
		# Get the translated content from the queue.
		my ($item_value) = $db->selectrow_array("
			SELECT resource_value 
			FROM 	$TBL{queue} 
			WHERE 	pk = $pk") ||
			die "Failed to obtain the resource_value for the pk: $pk\n";
		
		# Create a unique timestamp for the archive file.
		my ($sec, $min, $hour, $day, $mon, $year) = (localtime)[0,1,2,3,4,5];
		$mon++; $year += 1900;
		if ( length($sec) == 1 ) { $sec = '0' . $sec }	# Pad a single digit sec.
		if ( length($min) == 1 ) { $min = '0' . $min }	# Pad a single digit min.
		if ( length($hour) == 1 ) { $hour = '0' . $hour }	# Pad a single digit hour.
		if ( length($day) == 1 ) { $day = '0' . $day }	# Pad a single digit day.
		if ( length($mon) == 1 ) { $mon = '0' . $mon }	# Pad a single digit month.
		my $utime = "$year$mon$day" . "_" . "$hour$min$sec";

		# If the file $rid exists, backup it's contents to the archive directory.
		if (-e $rid)
		{
			# Remove the directory info from the filename and add a timestamp.
			(my $new_file = $rid) =~ s/.*\/(.*)/\/$1/;
			$new_file = $ARC_DIR . $new_file . "." . $utime;

			# Put the old file's contents in the new(archive) file.
			copy($rid, $new_file) || die "Failed to copy $rid to $new_file\n";
			chmod 0666, $new_file;

			# Delete the old file.
			#unlink $rid;	# not sure why Karl did this since the next step may fail
							# leaving us empty handed		
		}							

		# Put the contents of the variable into filename $rid.
		open(DATA, ">" . $rid ) || die "Failed to open $rid for writing\n";
		binmode DATA, ":encoding(utf8)";
		print DATA $item_value;
		if ( close DATA )
		{
			$rv = 1;
			chmod 0666, $rid;
			# Log the operator, filename, and timestamp.
			open(LOG, ">>" . $LOGFILE );
			print LOG scalar localtime() . ": " . $creds->{'username'} . ":\t $rid\n";
			close LOG;
		}
		else
		{
			$message .= "<span class=\"r\">The file '$rid' could not be written to.&nbsp;</span>\n";
		}
	}
	return $rv; 
}

###### Toggle a queue item's complete status.
sub Toggle_Complete
{
	my $sth = $db->prepare("
		UPDATE $TBL{queue} 
		SET 	complete = NOT complete
		WHERE pk = $pk;");
	$sth->execute();
	$sth->finish;
}

###### Unlock a queue item to restore editing access.
sub Unlock
{
	my $sth = $db->prepare("
		UPDATE $TBL{queue} 
		SET 	locked = NULL,
			lock_owner = NULL
		WHERE pk = $pk;");
	$sth->execute();
	$sth->finish;
}


###### 11/23/04 Added the $LOGFILE to keep track of the operator for the html and xml files.
###### 11/24/04 Added Unicode conversion for file resources in the Create subroutine.
###### 01/05/05 Added a check to see if the queue item is locked before attempting to return,
######		since a locked item cannot be deleted from the queue.
###### 01/07/05 Added the onClick=LanguageChange event to the cboLanguage menu to protect from 
######		changing an existing object to another language definition. Modified template to match.
###### 01/10/05 Added code so that a file of type .html or .html.en (english files)
######		will be returned to a file of type .html.html
###### 01/19/05 Modified the error message in the case of a failed Return() so that "...Database Failure"
######		wouldn't be added to $message if it was a file write error.
###### 02/22/05 Removed "use Test::utf8;" because it couldn't be found since the web server rebuild
######		and didn't appear to be used in the script after all.
###### 03/03/05 Added Icelandic (is) to the list of languages approved for translations.
###### 03/18/05 Modified the "All languages" logic so that it doesn't include the English document
######		and modified the template so that "All" is no longer the default for an English doc.
###### 03/03/05 Added Russian (ru) to the list of languages approved for translations.
###### 07/11/05 Added Slovenian (sl) to the list of languages approved for translations.
###### 07/22/05 Added Spanish (es) to the list of languages approved for translations.
###### 10/27/05 Added Welsh (cy) to the list of languages approved for translations.
###### 12/15/05 Added Portuguese (pt) to the list of languages approved for translations.
###### 01/25/06 Commented out utf8 conversion references. Deleted Unicode_Convert.
# 07/14/06 added Estonian to the language list
# 10/18/06 added Malay, Norwegian & Swedish
# 07/12/07 added Bulgarian and changed the require for ADMIN_DB to 'use'
# 12/04/07 added Afrikaans
# 01/02/08 removed a bunch of translations
###### 03/06/08 revised to pull in the language list from a template instead of being hardcoded in this script
# 02/24/10 added .shtml to the file handling options
# 11/11/14 removed the dependency on Unicode::String since it doesn't really seem to be needed anywhere
