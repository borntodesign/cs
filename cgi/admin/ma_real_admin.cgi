#!/usr/bin/perl -w
###### ma_real_admin.cgi
###### Lookup a real merchant affiliate record using a field selected by the user.
###### This script will also allow activations and updates to merchant affiliate records.
######
###### Created: 06/04/2003	Karl Kohrt
######
###### Last modified: 06/11/15	Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use State_List qw( %STATE_CODES );
use G_S;
use ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";	# added 05/12/15

###### GLOBALS

###### our page templates
our %TMPL = (
	'search'	=> 10174,
	'display'	=> 10173,
	'arc_list' 	=> 10200,
	'arc_display'	=> 10201 );

###### our tables
our %TBL = (
	'master'		=> 'merchant_affiliates_master',
	'master_arc'	=> 'merchant_affiliates_master_arc',
	'discounts'		=> 'merchant_affiliate_discounts',
	'vendors'		=> 'vendors',
	'members'		=> 'members',
	'notifications'	=> 'notifications');

my $q = new CGI;
$q->charset('utf-8');
my $ME = $q->url;

# make sure we are running a secure session
unless ($q->https)
{
	$ME =~ s/^http/https/;
	print $q->redirect($ME);
	exit;
}

our $COUPON_EDIT_SCRIPT = 'https://www.clubshop.com/cgi/maf/ma_coupons_edit.cgi';
our $action = ( $q->param('action') || '' );
our $search_field = $q->param('search_field');
our $search_value = $q->param('search_value');
$search_value = G_S::PreProcess_Input($search_value);
our $description = '';
our $SIAM = 0;

our ($db);	###### database variables
our ($StateList, $StateHash) = ();
our $data = '';
our $dover_msg = '';
our $status_message = '';
our $msg = '';			

our @char_fields = qw/=name
			=username
			=business_name
			=address1
			=city
			=state
			=country
			=postalcode
			=phone
			=fax
			=emailaddress1
			=url
			=blurb
			=archived_date/;
our @int_fields = qw/=id
			=id
			=member_id
			=business_type
			=tin
			=member_id
			=discount_type
			=pay_cardnumber
			=pay_cardexp
			=pay_cardexp_month
			=pay_cardexp_year
			=pay_cardcode
			=pay_account_number
			=pay_routing_number/;
our @result_list = ();
our $master_id = '';

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless $db = ADMIN_DB::DB_Connect( 'ma_real_admin.cgi', $operator, $pwd );
$db->{RaiseError} = 1;

###### %field has the form of 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %field = (
		id 			=> {value=>'', class=>'n', req=>1},
		status 		=> {value=>'', class=>'n', req=>0},
		member_id 		=> {value=>'', class=>'n', req=>0},
		referral_id 		=> {value=>'', class=>'n', req=>0},
		discount_type		=> {value=>'', class=>'n', req=>1},
		firstname1 		=> {value=>'', class=>'n', req=>1},
		lastname1 		=> {value=>'', class=>'n', req=>1},
		emailaddress1		=> {value=>'', class=>'n', req=>0},
		business_name 	=> {value=>'', class=>'n', req=>1},
		address1		=> {value=>'', class=>'n', req=>1},
		city 			=> {value=>'', class=>'n', req=>1},
		state_code		=> {value=>'', class=>'n', req=>0},
		state 			=> {value=>'', class=>'n', req=>0},
		country 		=> {value=>'', class=>'n', req=>1},
		postalcode		=> {value=>'', class=>'n', req=>1},
		phone 			=> {value=>'', class=>'n', req=>1},
		fax			=> {value=>'', class=>'n', req=>0},
		emailaddress2		=> {value=>'', class=>'n', req=>0},
		url			=> {value=>'', class=>'n', req=>0},
		business_type 	=> {value=>'', class=>'n', req=>1},
		username		=> {value=>'', class=>'n', req=>1},
		password		=> {value=>'', class=>'n', req=>1},
		pay_name 		=> {value=>'', class=>'n', req=>1},
		pay_address		=> {value=>'', class=>'n', req=>1},
		pay_city 		=> {value=>'', class=>'n', req=>1},
		pay_state_code	=> {value=>'', class=>'n', req=>0},
		pay_state 		=> {value=>'', class=>'n', req=>0},
		pay_country 		=> {value=>'', class=>'n', req=>1},
		pay_postalcode	=> {value=>'', class=>'n', req=>1},
		pay_cardnumber	=> {value=>'', class=>'n', req=>0},
		pay_cardexp		=> {value=>'', class=>'n', req=>0},
		pay_cardexp_month	=> {value=>'', class=>'n', req=>0},
		pay_cardexp_year	=> {value=>'', class=>'n', req=>0},
		pay_cardcode		=> {value=>'', class=>'n', req=>0},
		pay_payment_method	=> {value=>'', class=>'n', req=>1},
		pay_account_number	=> {value=>'', class=>'n', req=>0},
		pay_routing_number	=> {value=>'', class=>'n', req=>0},
		stamp			=> {value=>'', class=>'n', req=>0},
		operator		=> {value=>'', class=>'n', req=>0},
		notes			=> {value=>'', class=>'n', req=>0},
		contact_info		=> {value=>'', class=>'n', req=>0},
		rebate			=> {value=>0, class=>'n', req=>1},
		reb_com		=> {value=>0, class=>'n', req=>1},
		pp			=> {value=>0, class=>'n', req=>1},
		cap			=> {value=>'', class=>'n', req=>0},
		blurb			=> {value=>'', class=>'n', req=>0},
		discount		=> {value=>0, class=>'n', req=>1},
		tin			=> {value=>'', class=>'n', req=>0},
		archived_date	=> {value=>0, class=>'n', req=>0},
		'discount_blurb'	=> {'value'=>'', 'class'=>'n', 'req'=>0}
	);

###### If this is the first time through, just print the Search page.
unless ( $action )
{
	Search_Form()	;
}
elsif ($action eq 'search')
{
	# Check the validity of the field and the value provided by the user.
	if ( Check_Validity() )
	{
		Run_Query('master');
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		elsif ( @result_list > 1 )
		{
			###### create list report
			Display_Results();
		}
		else
		{
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
	}
	else
	{
		Search_Form($msg);
	}
}
elsif ($action eq 'show')
{
	if ( Check_Validity() )
	{
		Run_Query('master');

		###### at this point we should have one row of data
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		else
		{		
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
	}
	else
	{
		Search_Form($msg);
	}
}
elsif ($action eq 'arclist')
{
	if ( ArcList() == 0 )
	{
		Err("No matching records for Merchant ID: $master_id");
	}
}
elsif ($action eq 'arc')
{
#	$search_field = 'oid';
#	$search_value = $q->param('id');

	Run_Query('master_arc', {qry=>'id=? AND archived_date=?', list=>[$q->param('id'), $q->param('archived_date')]});

	###### at this point we should have one row of data
	unless ( @result_list )
	{
		Err("No matching records for Merchant ID: $master_id");
	}
	else
	{		
		$data = shift @result_list;
		Load_Data();
		Create_controls();
		$dover_msg = 'Data cannot be changed.';
		Do_Page('arc_display');
	}
}
elsif ( $action eq 'activate' )
{
	Load_Params();

	Check_Required();

	# If we already have an error.
	if ($dover_msg)
	{
		# Reload the page with the problems flagged if there are any errors
		Create_controls();
		Do_Page('display');
	}
	# If we don't have a member ID supplied.
	elsif ( $field{member_id}->{'value'} eq '' )
	{
		# Check the SPID and the E-mail to see if they are good.
		if ( Check_SPID() && Check_Email() )
		{
			# Create a new member record, then display the page with a message stating so.
			Create_Member();
		}
		Create_controls();
		Do_Page('display');
	}
	else
	{
		if ( Update(1) ){
			$dover_msg = "<br /><span class=\"b\">Merchant Record Successfully Updated/Activated</span>\n";

			my ($tmp, @list, $rv) = ();

			###### now we'll get all our locations and create activated 'vendor' records for each one as necessary
			my $sth = $db->prepare("	SELECT l.id,
								l.vendor_id,
								v.status
							FROM 	merchant_affiliate_locations l
							LEFT JOIN vendors v
							ON v.vendor_id = l.vendor_id
							WHERE l.merchant_id= $field{id}->{'value'}");
			$sth->execute;
			while ($tmp = $sth->fetchrow_hashref)
			{ 				push (@list, $tmp);
			}
			$sth->finish;

			if (scalar @list < 1)
			{
				$dover_msg .= "<br /><span class=\"r\">There are no Merchant Locations to activate.</span>\n";
			}
			else
			{
				foreach (@list)
				{
					$rv = Activate_Location($_);
					unless ($rv)
					{
						$dover_msg .= "<br /><span class=\r\">Merchant Location $tmp->{id} was NOT activated</span>\n";
					}
					else
					{
						$dover_msg .= "<br /><span class=\"b\">$rv</span>\n";
					}
				}
			}

			$search_field = 'id';
			$search_value = $field{id}->{'value'};
			Run_Query('master');
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
		else { Err('Merchant Record update failed') }
	}

}
elsif ( $action eq 'update' )
{
	Load_Params();
	Check_Required();

	# If we already have problems flagged.
	if ($dover_msg){
		# We'll reload the page. 
		Create_controls();
		Do_Page('display');
	}
	else{
		# If we succeed in updating the record ( 0 means no activation ).
		if ( Update(0) )
		{
			# Load message, get updated data, load it into variables, display the page.
			$dover_msg = "<br /><span class=\"b\">Record Successfully Updated</span>\n";
			$search_field = 'id';
			$search_value = $field{id}->{'value'};
			Run_Query('master');
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
		else { Err('Record update failed') }		
	}
}
else { Err('Undefined action.') }

$db->disconnect;
exit(0);


###### ###### ###### ###### ######
###### Subroutines start here.
##################################

sub Activate_Location
{
	my $dat = shift;

	###### this is the case where for some reason a vendor record exists and its status is greater than zero
	if ( $dat->{status} )
	{
		return "Vendor: $dat->{vendor_id} already exists and is active for Location: $dat->{id}";
	}

	###### this is the case where there is an existing vendor that is not active
	###### this will be the default case now
	elsif ( $dat->{vendor_id})
	{
#		return "Vendor: $dat->{vendor_id} already exists but is not active for Location: $dat->{id}. Manual activation will be required after further investigation.";
#		my $sth = $db->prepare("	UPDATE $TBL{vendors}
		my $rv = $db->do("	UPDATE $TBL{vendors}
						SET 	blurb = ?,
							status = 1
						WHERE 	vendor_id= $dat->{vendor_id};

					UPDATE $TBL{discounts}
						SET 	discount = $field{'discount'}->{'value'},
						--	rebate = $field{'rebate'}->{'value'},
						--	reb_com = $field{'reb_com'}->{'value'},
						--	pp = $field{'pp'}->{'value'},
							cap = $field{cap}->{'value'}
						WHERE 	location_id= $dat->{id}",
				undef, $field{blurb}->{'value'});

#		my $rv = $sth->execute($field{blurb}->{'value'});
		return unless $rv eq '1';

		return "Vendor: $dat->{vendor_id} successfully activated for Location: $dat->{id}";
	}

	###### this is our normal case where we'll create a vendor and activate it
	else
	{
		my $sth = $db->prepare("SELECT NEXTVAL('vendor_id_seq')");
		$sth->execute;
		my $vendor_id = $sth->fetchrow_array;
		$sth->finish;

		return unless ($vendor_id);

#		$sth = $db->prepare("	INSERT INTO vendors (
		my $rv = $db->do("	INSERT INTO vendors (
							vendor_id,
							vendor_name,
							vendor_group,
							status,
							notes,
							url,
							blurb)
						SELECT
							$vendor_id AS vendor_id,
							'MA Location: $dat->{id}' AS vendor_name,
							5 AS vendor_group,
							status,
							'activation' AS notes,
							url,
							blurb							
						FROM
							merchant_affiliates_master
						WHERE 	id= $field{id}->{'value'};

						UPDATE merchant_affiliate_locations
						SET 	vendor_id= $vendor_id,
							stamp= NOW(),
							operator= current_user,
							notes= 'activated'
						WHERE 	id= $dat->{id}
						AND 	merchant_id= $field{id}->{'value'};
			
						UPDATE $TBL{discounts}
						SET 	discount = $field{'discount'}->{'value'},
						--	rebate = $field{'rebate'}->{'value'},
						--	reb_com = $field{'reb_com'}->{'value'},
						--	pp = $field{'pp'}->{'value'},
							cap = $field{cap}->{'value'}
						WHERE 	location_id= $dat->{id}");

#		my $rv = $sth->execute;
#		$sth->finish;

		return unless $rv eq '1';

		return "Vendor: $vendor_id successfully created for Location: $dat->{id}";
	}
}

###### Show a list of archived records.
sub ArcList
{
	$master_id = $q->param('id');
	my $qry = "	SELECT id, archived_date,
				business_name,
				address1,
				emailaddress1,
				stamp,
				operator
			FROM 	$TBL{master_arc}
			WHERE id = ?
			ORDER BY stamp desc";

	my $bgcolor = '#ffffee';
	my $tblDATA = '';
	my $num_records = 0;
	my $sth = $db->prepare($qry);
	$sth->execute($master_id);

	while ( $data = $sth->fetchrow_hashref )
	{
		++$num_records;
				
		###### lets fill in the blank table cells
		foreach (keys %{$data}){
			unless ( $data->{$_} =~ /\w/ ){$data->{$_} = '&nbsp;'}
		}
		$data->{archived_date} = $q->escape($data->{archived_date});
		$tblDATA .= $q->Tr({-bgcolor=>$bgcolor},
			$q->td({-valign=>'top'},
				$q->a({-href=>$q->url()."?action=arc;id=$data->{id};archived_date=$data->{archived_date}", -target=>'_blank'}, '*') ) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{business_name}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{address1}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{emailaddress1}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{stamp}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{operator})
		);
		$bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	}

	if ( $num_records )
	{	
		$sth->finish;
		my $tmpl = G_S::Get_Object($db, $TMPL{arc_list}) || die "Couldn't open $TMPL{arc_list}";

		$tmpl =~ s/%%data%%/$tblDATA/;
		$tmpl =~ s/%%id%%/$master_id/g;         

		print $q->header(-expires=>'now'), $tmpl;
	}							
	return $num_records;
}

###### Creates combo boxes for the credit card expiration fields.
sub cboExp
{
	my $name = shift;
	my @values = ();
	if ($name =~ /month/){ @values = shift || qw/01 02 03 04 05 06 07 08 09 10 11 12/ }
	else {
		###### @values = shift || qw/03 04 05 06 07 08 09 10 11 12/
		my $s = ${[localtime]}[5];	# for 2007 this value = 107 (the number of years since 1900)
		###### if the stored real value is less than the current year,
		###### we'll add that year in just so it can properly appear in the list
		if ($field{$name}->{'value'} lt substr($s,1,2)){
			push @values, $field{$name}->{'value'};
		}
		while (scalar @values < 20){ push @values, substr($s++,1,2); }
	}

	###### pad our list with a space
	unshift (@values, '');

	return $q->popup_menu(	-name=>$name,
					-values=>\@values,
					-default=>$field{$name}->{'value'},
					-force=>'true');
}

###### Creates a combo box for the payment method.
sub cboPM
{
	my $name = shift;
	my $default_value = $field{$name}->{'value'} || '';
	my %lbls = (	'' 		=> '',
			'CC' 		=> 'Credit/Debit Card',
			'eCheck/ACH'	=> 'Checking Account',
			'MB'		=> 'Money Bookers');
	my @values = shift || ("CC", "eCheck/ACH", 'MB');

	###### pad our list with a space
	unshift (@values, '');

	return $q->popup_menu(	-name		=> $name,
					-values	=> \@values,
					-labels 	=> \%lbls,
					-default	=> $default_value,
					-force		=> 'true',
					-onchange	=> "Check_PM()");
}

###### Creates a combo box for the state selections.
sub cbo_State
{
	# Get the name of the field passed in.
	my $state_type = shift;
	my ($tmp1, $tmp2, $tmp3, $tmp4) = ();

	(
	$tmp1,	$tmp2,	$tmp3, $tmp4,
	$StateList,
	$StateHash
	)	= G_S::Standard_State(	$db,
						$field{'state_type'}->{'value'},
						$field{'state_type'}->{'value'},
						$field{'country'}->{'value'});
	###### if our list is not populated, then we'll put something in
	if ( ! defined $StateList || scalar @{$StateList} < 1)
	{
		$StateList = [''];
		$StateHash = {'' => 'Enter value below'};
	}

	return $q->popup_menu(	-name=>$state_type,
					-default=>$field{'state'}->{'value'},
					-values=>$StateList,
					-labels=>$StateHash,
					-class=>'in',
					-force=>'true');
}

###### Check if e-mail is unblocked, valid, unique, and not on a blocked domain.
sub Check_Email
{
	###### if there is no value submitted and it is not required, then return OK
	return 1 if (!$field{emailaddress1}->{'value'} && $field{emailaddress1}{req} != 1);

	my ($qry, $sth, $id) = ();
	my $bad_email = 0;

	# Check if the email address fits standard patterns.
	if ( G_S::Check_Email($field{emailaddress1}->{'value'}) == 0 )
	{
		$dover_msg .= $q->span({-class=>'r'}, "<br />Email address doesn't appear to be a valid address.</b>\n");
		$field{emailaddress1}->{'class'} = 'r';
		$bad_email = 1;
	}

	# Check for duplicate email addresses in both the primary and secondary columns
	my ( $match, $match2 ) = G_S::Duplicate_Email_CK( $db, 0, $field{emailaddress1}->{'value'}, '' );
	if ( $match || $match2 )
	{
		$qry = "SELECT id from  members where emailaddress = ? or emailaddress2 = ?";
		$sth = $db->prepare($qry);
		if ( $sth->execute($field{emailaddress1}->{'value'}, $field{emailaddress1}->{'value'}) )
		{
			$id = $sth->fetchrow_array();
		}
		$sth->finish;

		$dover_msg .= $q->span({-class=>'r'}, "<br />We found an existing Membership, # $id with email address, $field{emailaddress1}->{'value'}\n");
		$field{emailaddress1}->{'class'} = 'r';
		$bad_email = 1;
	}

###### 07/03/03 we are not checking for blocked domains due to all the problems
	# Check that they aren't using an email address that is in our mail refused list.
#	if ( G_S::Mail_Refused($db, $field{emailaddress1}->{'value'}) )
#	{
#		$dover_msg .= $q->span({-class=>'r'}, "<br>Email address appears in our 'Refused List'.</b>\n");
#		$field{emailaddress1}->{'class'} = 'r';
#		$bad_email = 1;
#	}

	if ( $bad_email ) { return 0; }
	else { return 1; }
}

###### Check for required fields.
sub Check_Required
{
	###### Mark the credit card or checking account fields required as needed.
	if ($field{pay_payment_method}->{'value'} eq 'CC')
	{
		$field{pay_cardnumber}{req} = 1 ;
		$field{pay_cardexp}{req} = 1 ;
		$field{pay_cardcode}{req} = 1 ;
		###### build our expiration date from the two fields
		$field{pay_cardexp}->{'value'} = $field{pay_cardexp_month}->{'value'} . $field{pay_cardexp_year}->{'value'};
		if (length $field{pay_cardexp}->{'value'} != 4)
		{
			$dover_msg .= qq|<div style="color: #ff0000">The card expiration date should be in MMYY format.</div>|;
			$field{pay_cardexp_month}->{'class'} = $field{pay_cardexp_year}->{'class'} = 'r';
		}
	}		
	elsif ($field{pay_payment_method}->{'value'} eq 'eCheck/ACH')
	{
		$field{pay_account_number}{req} = 1 ;
		$field{pay_routing_number}{req} = 1 ;
		if (length $field{pay_routing_number}->{'value'} != 9)
		{
			$dover_msg .= qq|<div style="color: #ff0000">The Bank Routing Number should be 9 digits long.</div>|;
			$field{pay_routing_number}->{'class'} = 'r';
		}
	}

	###### here we'll check required fields and assign new html classes as necessary
	foreach (keys %field)
	{
		# If a required field is empty, we'll flag it.
		if ( !$field{$_}->{'value'} && $field{$_}{req} )
		{	
			$field{$_}->{'class'} = 'r';
			$dover_msg = $q->span({-class=>'r'}, "<br>Please fill in the required fields marked in <b>RED</b><br>\n");
		}
	}
	foreach (keys %field)
	{
		if ( /state/ && ! /code/ )
		{
			###### check for a standardized state
			my ($success, $country_match) = ();
			my $pay = (/pay/) ? 'pay_' : '';

#Err("vals into stateLIB: field: $_ - |$field{$_}->{'value'}|, |$field{($_ . '_code')}->{'value'}|");
			
			($field{$_}->{'value'}, $field{($_ . '_code')}->{'value'}, $success, $country_match, $StateList, $StateHash) = G_S::Standard_State($db, $field{$_}->{'value'}, $field{($_ . '_code')}->{'value'}, $field{($pay . 'country')}->{'value'});

#Err("vals out: field: $_ - |$field{$_}->{'value'}|, |$field{($pay . $_ . '_code')}->{'value'}|, success= $success, match= $country_match");
		}
	}

	###### a state is required if the country is US (at the very least)
	if ( $field{'country'}->{'value'} eq 'US' )
	{
		my $match = 0;
		foreach (values %STATE_CODES)
		{
			if ( uc $field{'state'}->{'value'} eq $_)
			{
				###### we'll assign the match since they may have submitted a lower case value
				$field{'state'}->{'value'} = $_;
				$match = 1;
				last;
			}
		}

		unless ( $match )
		{
			$dover_msg .= $q->span({-class=>'r'}, "<br />Please select a state from the dropdown box.<br>\n");
		}
  	 }

	# a pay_state is required if the pay_country is US (at the very least)
	if ( $field{pay_country}->{'value'} eq 'US' )
	{
		my $match = 0;
		foreach (values %STATE_CODES)
		{
			if ( uc $field{pay_state}->{'value'} eq $_)
			{
				###### we'll assign the match since they may have submitted a lower case value
				$field{pay_state}->{'value'} = $_;
				$match = 1;
				last;
			}
		}
				
		unless ( $match )
		{
			$dover_msg = $q->span({-class=>'r'}, "<br>Please select a pay_state from the dropdown box.<br>\n");
		}
	}
}

######	Check to see if the Sponsor ID is valid and the sponsor is of a type that can sponsor others.
sub Check_SPID
{
	my ($sth, $qry) = ();
	my $search_field = '';
	my $id = $field{referral_id}->{'value'};

	unless ($id)
	{
		$field{referral_id}->{'class'} = 'r';
		$dover_msg .= $q->span({-class=>'r'}, "<br />The Referral ID is required to create a Club membership for this Merchant since they have no Club ID.\n");
		return 0;
	}
	elsif ( $id =~ /\D/)
	{
		###### contains letters so it can't be an ID
		$id = uc $id;
		$search_field = 'alias = ?';
	}
	else
	{
		$search_field = 'id = ?';
	}

	$qry = "SELECT id
		FROM members
		WHERE $search_field
		AND membertype ~ 'm|v'";

	$sth = $db->prepare($qry);
	$sth->execute($id);
	my $data = $sth->fetchrow_array;
	$sth->finish;

	unless ( $data )
	{
		 # The sponsor ID is invalid.
		$field{referral_id}->{'class'} = 'r';
		$dover_msg .= $q->span({-class=>'r'}, "The Referral ID either does not exist or is not qualified as a sponsor.\n");
		return 0;
	}
	else
	{
		# Assign the SPID to the form field, replacing the alias if that is what was passed in.
		$field{referral_id}->{'value'} = $data;			
		return 1;
	}				
}		

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			$msg= $q->span({-style=>'color: #ff0000'},
				"Invalid search value for $search_field - Use only whole numbers with this field.");
			return;	
		}
	}
	else
	{
		$msg= $q->span({-style=>'color: #ff0000'},
				"Invalid field specification - Try again.");
		return;				
	}
	return 1;						
}

# Create a special control for specific fields.
sub Create_controls
{
	foreach (keys %field)
	{
	###### now we'll convert the 'value' to the needed form element with the value included
		if (($_ eq 'country') || ($_ eq 'pay_country'))
		{	###### country is a special drop down box
			$field{$_}->{'control'} = G_S::cbo_Country($q, $_, 	$field{$_}->{'value'}, 'in');
		}
		elsif (($_ eq 'state_code') || ($_ eq 'pay_state_code'))
		{	###### our special US state drop-down
			$field{$_}->{'control'} = cbo_State($_);
		}
		elsif ( /payment_method/ )
		{
			$field{$_}->{'control'} = cboPM($_);
		}
		elsif ( /cardexp/ )
		{
			if ($_ eq 'pay_cardexp' && $field{pay_cardexp}->{'value'})
			{
				# Break it into month and year components.
				$field{pay_cardexp_month}->{'value'} = substr($field{pay_cardexp}->{'value'},0,2);
				$field{pay_cardexp_year}->{'value'} = substr($field{pay_cardexp}->{'value'},2,2);
			}
			else
			{
				$field{$_}->{'control'} = cboExp($_);
			}
		}
		elsif($_ eq 'status')
		{
			# If status is not NULL, then show the status via radio buttons
			#  and hide the SPID field.
			if ( $field{$_}->{'value'} ne '' )
			{
				$field{$_}->{'value'} = 
					$q->radio_group(	-name=>'status',
								-labels=>{0=>'Inactive', 1=>'Active'},
								-default=>$field{$_}->{'value'},
								-class=>'in',
								-values=>['0', '1'] )
					. $q->hidden(	-name=>'spid',
								-class=>'in',
								-size=>15,
								-value=>$field{referral_id}->{'value'},
								-force=>'true');
				$status_message = 'Changing a record to Inactive will remove it from the public listings.';
			}
			# Else show the activation button and SPID field (for member record).
			else
			{
				$field{$_}->{'value'} = $q->submit( 		-class=>'in',
										-value=>'Activate',
										-onclick=>"document.forms[0].action.value='activate'" )
							. "&nbsp;&nbsp;"
							. $q->span(		{-class=>$field{referral_id}->{'class'}},
										"Referral ID" )
							. "&nbsp;"
							. $q->textfield(	-name=>'referral_id',
										-class=>'in',
										-size=>15,
										-value=>$field{referral_id}->{'value'} );
				$status_message = '';
			}
		}			
		elsif ($_ eq 'discount_type')
		{
			my @rads = $q->radio_group(	-name=>$_,
							-nolabels=>1,
							-values=>[1,2,3,4,5],
							-default=>$field{discount_type}->{'value'});
			$field{$_}->{'control'} = \@rads;

			# Calulate the SIAM % value.
			if ( $field{'rebate'}->{'value'}
				&& $field{'reb_com'}->{'value'}
				&& $field{'pp'}->{'value'}
				&& $field{'discount'}->{'value'} )
			{
				$SIAM = $field{'discount'}->{'value'} - $field{'rebate'}->{'value'}
									- $field{'reb_com'}->{'value'}
									- $field{'pp'}->{'value'};
			}
			else { $SIAM = 'Unknown'; }
		}
	}
}

sub Create_hidden{
	my $hid;
	# we need to pass all our parameters that have values except the action param
	foreach ( $q->param() ){
		$hid .= $q->hidden($_) if ($_ ne 'action');
	}
	return $hid;
}

###### Create a new member record with the information from the form.
sub Create_Member
{
	my ($qry, $sth, $rv) = ();
	my $member_id = '';

	# Get the new member ID.
	$sth = $db->prepare("SELECT NEXTVAL('members_id_seq')");
	if ( $sth->execute )
	{
		$member_id = $sth->fetchrow_array;
	}
	$sth->finish;

	# Create the new member Alias.
	my $alias = 	uc substr($field{firstname1}->{'value'},0,1)
			. uc substr($field{lastname1}->{'value'},0,1)
			. $member_id;				

	$qry = "INSERT INTO members (
			id,
			spid,
			alias,
			membertype,
			mail_option_lvl,
			memo,
			firstname,
			lastname,
			emailaddress,
			tin,
			password,
			address1,
			city,
			state,
			country,
			postalcode )
		 VALUES (
			$member_id,
			$field{referral_id}->{'value'},
			?,
			'm',
			0,
			'Automatically created in conjunction with a Merchant Affiliation activation.',
			?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";		

	$sth = $db->prepare($qry);


	$rv = $sth->execute( 	$alias,
					$field{firstname1}->{'value'},
					$field{lastname1}->{'value'},
					$field{emailaddress1}->{'value'},
					$field{tin}->{'value'},
					$field{password}->{'value'},
					$field{pay_address}->{'value'},
					$field{pay_city}->{'value'},
					$field{pay_state}->{'value'},
					$field{pay_country}->{'value'},
					$field{pay_postalcode}->{'value'} );

	# If the query was successful, assign the new member ID to the form field.	
	if ( $rv ) 
	{
		$field{member_id}->{'value'} = $member_id;
		$dover_msg = $q->span({-class=>'r'}, "<br>A new Member Record has been created. 
				Please click the Activate button again to activate this Merchant.\n");
	}
	$sth->finish;
}

###### Display a list if more than one result is returned from the query.
sub Display_Results
{
	my $html;

	# print a title for the browser window.
	print $q->header(-expires=>'now', -charset=>'utf-8');
	print $q->start_html(-bgcolor=>'#ffffee', -title=>'Merchant Affiliate Search Results');

	# print a simple page header.
	print qq!
		<h1 align="center">Merchant Affiliate Search Results</h1>
		<table align="center" border="2" cellpadding="2">
 			<tr>
			<td><b> M.A. ID </b></td>
			<td><b> Member ID </b></td>
			<td><b> Username </b></td>
			<td><b> Business Name </b></td>
			<td><b> Business Type</b></td>
 			<td><b> City </b></td>
			<td><b> State </b></td>
			<td><b> Country </b></td>
			</tr>!;

	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		$result->{member_id} = $result->{signup_spid} || '&nbsp;';
		$result->{username} = $result->{linked_id} || '&nbsp;';
		print qq!
			<tr>
			<td><a href="$ME?action=show&search_field=id&search_value=$result->{id}" target="Resource Window $result->{id}"> $result->{id} </a></td>
			<td>$result->{member_id}</td>
			<td>$result->{username}</td>
			<td>$result->{business_name}</td>
			<td>$result->{business_type}</td>
			<td>$result->{city}</td>
			<td>$result->{'state'}</td>
			<td>$result->{'country'}</td>  
			</tr>!;
	}

	print qq!
	</table>
	<p align="center"><a href="$ME">Click Here for New Search</a></p>!;

	print $q->end_html();
}

###### Display a single record to the browser.
sub Do_Page
{
	my $tmpl_name = shift;

	my $hidden = '';# = &Create_hidden();
	my $tmpl = G_S::Get_Object($db, $TMPL{$tmpl_name}) || die "Couldn't open $TMPL{$tmpl_name}";
	$tmpl =~ s/%%message%%/$dover_msg/;
	$tmpl =~ s/%%status_message%%/$status_message/;
	$tmpl =~ s/%%hidden%%/$hidden/;
	$tmpl =~ s/%%SIAM%%/$SIAM/;
	$tmpl =~ s/%%ME%%/$ME/g;
	$tmpl =~ s/%%archives%%/$q->a({	-href=>$ME
							. "?id=$field{id}->{'value'}&action=arclist",
						-target=>'_blank',
						-class=>'q'},
						'Archived Records')/e;
	$tmpl =~ s/%%coupons%%/$q->a({	-href=>$COUPON_EDIT_SCRIPT
							. "?admin=1&merchant_id=$field{id}->{'value'}&membertype=maf",
						-target=>'_blank',
						-class=>'q'},
						'Coupons')/e;	
	$description = Get_Biz_Description();
	$tmpl =~ s/%%description%%/$description/ge;

	foreach (keys %field)
	{
		if ($field{$_}->{'control'})
		{
			if ($_ eq 'discount_type')
			{
				###### we'll have to iterate through the list of radio buttons
				my $x = 1;
				foreach my $rad (@{$field{$_}->{'control'}})
				{
					$tmpl =~ s/%%$_-control-$x%%/$rad/;
					$x++;
				}
			}
			else
			{
				# Display the combo box.
				$tmpl =~ s/%%$_-control%%/$field{$_}->{'control'}/;
				
				# If it is a state type field, display the value also.
				if ( ($_ eq 'state') || ($_ eq 'pay_state') )
				{
					$tmpl =~ s/%%$_%%/$field{$_}->{'value'}/;
				}				
			}
		}
		else
		{
			$tmpl =~ s/%%$_%%/$field{$_}->{'value'}/g;
		}

		$tmpl =~ s/%%class_$_%%/$field{$_}->{'class'}/;
	}

	print $q->header(-expires=>'now'), $tmpl;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR>$_[0]<BR>";
	print $q->end_html();
}

###### Get the Business description based upon the code in the Merchant record.
sub Get_Biz_Description
{
	my $data = '';
	
	if ( $field{business_type}->{'value'} )
	{ 
		my $sth = $db->prepare("
			SELECT yp_heading
			FROM ypcats2naics
			WHERE pk= ?");
		$sth->execute( $field{business_type}->{'value'} );
		$data = $sth->fetchrow_array;
		$sth->finish;
	}
	else
	{
		$data = 'No business code on record';
	}
	
	return $data;
}

###### Loads the data from the query into the global hash variable.
sub Load_Data
{
	###### this procedure presumes that the field keys are identical to the passed parameters
	foreach my $key (keys %field)
	{
		$field{$key}->{'value'} = $data->{$key};
		$field{$key}->{'value'} = '' unless defined $field{$key}->{'value'};
	}
}

###### Populating our field array hash values
sub Load_Params
{
	###### this loop depends on the %field hash keys being identical to the passed parameter names
	foreach (keys %field)
	{
		# Removed the 'or blank' at the end since it was blanking out zero value fields.
		###### instead am checking for a defined value
		$field{$_}->{'value'} = ( defined $q->param($_) ) ? $q->param($_) : '';

		###### let's filter for extraneous characters (this may pose a problem with other character sets)
		###### we'll be more restrictive on the name fields
		if ( /name/ )
		{
			$field{$_}->{'value'} =~ s/\t|\r|\n|\f|\[M|\+|=|_//g;
		}
		###### we can skip the memo and special exception verbiage fields
		elsif ( /notes|blurb/ )
		{
			next;
		}
		elsif ( /url/ )
		{
			$field{$_}->{'value'} =~ s/\t|\r|\n|\f|\[M|"//g;
		}
		# If any of the discount fields are blank or zero, default it to 0.							
		elsif ( /discount|rebate|reb_com|pp/ )
		{
			if ( ($field{$_}->{'value'} eq '0') || ($field{$_}->{'value'} eq '') )
			{
				$field{$_}->{'value'} = '0.0';
			}				
		}
		else
		{
			$field{$_}->{'value'} =~ s/\t|\r|\n|\f|\[M|"|;|:|\+|=//g;
		}
	}

	# handle the transfer of US_biz_state into the biz_state field if its blank
	#  as it probably would be upon an update for a US account
	#$field{biz_state}->{'value'} ||= $field{US_biz_state}->{'value'};
}

##### Runs the specified query, pushing the results onto a list (array).
sub Run_Query
{
	my $table_name = shift;
	my $args = shift;	# added after the fact to support a different query modifier than the default and an arrayref of DB args
	
	my $qry = "SELECT ma.* FROM $TBL{$table_name} ma WHERE ";
	unless ($args->{qry})
	{
		if (grep { /=$search_field/ } @char_fields)
		{ 
			$qry .= "ma.$search_field ~* ?";
		}
		else
		{
			$qry .= "ma.$search_field = ?";
		}
		# this is for compatibility with the old way of using the global var
		$args->{'list'} = [$search_value];
	}
	else
	{
		$qry .= $args->{qry};
	}

	my $sth = $db->prepare($qry);
	$sth->execute( @{$args->{'list'}} );
	
	while ( $data = $sth->fetchrow_hashref)
	{
		###### convert our decimal values into 'percentages'
		$data->{'rebate'} *= 100 if $data->{'rebate'};
		$data->{'pp'} *= 100 if $data->{'pp'};
		$data->{'reb_com'} *= 100 if $data->{'reb_com'};
		$data->{'discount'} *= 100 if $data->{'discount'};

		push (@result_list, $data);
 	}		
}

###### print our main search form with any pertinent messages
sub Search_Form
{
	my $this_msg = shift || '';
	# Open the search template.
	my $tmpl = G_S::Get_Object($db, $TMPL{'search'}) || die "Failed to load template: $TMPL{'search'}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$this_msg/;
	print $q->header(), $tmpl;	
}

###### Update the fields in the database.
sub Update
{
	my $activate_flag = shift;
	my $qry = '';

	# Unless this is a Special Exception, we will get the standard values from the 
	#  merchant_discounts table, otherwise we will leave the values from the form.
# we are not using any default values in the new system
#	if ( $field{discount_type}->{'value'} != 5 )
#	{
#		$qry = "SELECT 	rebate,
#					reb_com,
#					pp,
#					coalesce(cap_amount, 0) AS cap,
#					discount
#			FROM merchant_discounts
#			WHERE type = $field{discount_type}->{'value'}	";
#
#		my $sth = $db->prepare($qry);
#		$sth->execute();
#
#		( $field{'rebate'}->{'value'},
#		  $field{'reb_com'}->{'value'},
#		  $field{'pp'}->{'value'},
#		  $field{cap}->{'value'},
#		  $field{'discount'}->{'value'} ) = $sth->fetchrow_array();
#
#		$sth->finish;
#	}
#	else	###### we'll convert our 'percentages' into decimals for insertion into the DB
#	{
		$field{'discount'}->{'value'} /= 100;
		$field{'rebate'}->{'value'} /= 100;
		$field{'reb_com'}->{'value'} /= 100;
		$field{'pp'}->{'value'} /= 100;
#	}

	$qry = '';

	# If there is a value, then include the field and value in the query,
	#  otherwise we will blank them out of the query string completely since value is NULL.
	my $status_string = '';
	my $cap_string = '';
	$field{member_id}->{'value'} = $field{member_id}->{'value'} || '';

	# If we are not activating the record, but there is a value.
	if ( !($activate_flag) && $field{status}->{'value'} ne ''  )
	{
		# Set the status field to its current value.
		$status_string = "status= $field{status}->{'value'},";
	}
	# If we are activating the record, set status to 1.
	elsif ( $activate_flag )
	{
		$status_string = "status= 1,";

		# Send out email notification of activation.
		$qry = "INSERT INTO $TBL{notifications} (
				id,
				notification_type)
			VALUES (
				$field{id}->{'value'},
				69 );\n";					

# Replaced by a locations table trigger and notification_type 71.
#		$qry .= "INSERT INTO $TBL{notifications} (
#				id,
#				notification_type)
#			VALUES (
#				$field{id}->{'value'},
#				70 );\n";					
	}
	# Otherwise, the status must be NULL.
	else
	{
		$status_string = "status = NULL,";
	}

	if ( $field{cap}->{'value'} )
	{
		$cap_string = "cap= $field{cap}->{'value'},";
	}

	# Added to take care of updates before activation - i.e., no member id.
	$field{member_id}->{'value'} = $field{member_id}->{'value'} || 'NULL';

	$qry .= "UPDATE $TBL{master}
			SET
				$status_string
				discount_type= $field{discount_type}->{'value'},
				member_id= $field{member_id}->{'value'},
				firstname1= ?,
				lastname1= ?,
				emailaddress1= ?,
				business_name= ?,
				address1= ?,
				city= ?,
				state= ?,
				country= ?,
				postalcode= ?,
				phone= ?,
				fax= ?,
				emailaddress2= ?,
				tin= ?,
				url= ?,
				business_type= $field{business_type}->{'value'},
				username= ?,
				password= ?,
				pay_name= ?,
				pay_address= ?,
				pay_city= ?,
				pay_state= ?,
				pay_country= ?,
				pay_postalcode= ?,
				pay_payment_method= ?,
				pay_cardnumber= ?,
				pay_cardcode= ?,
				pay_cardexp= ?,
				pay_account_number= ?,
				pay_routing_number= ?,
				notes= ?,
				contact_info= ?,
			--	rebate= $field{'rebate'}->{'value'},
			--	reb_com= $field{'reb_com'}->{'value'},
			--	pp= $field{'pp'}->{'value'},
				$cap_string
				blurb= ?,
				discount= $field{'discount'}->{'value'},
				stamp= NOW(),
				operator= current_user
			WHERE id= $field{id}->{'value'}";

	my @list = ();

	foreach ( qw/firstname1
			lastname1
			emailaddress1
			business_name
			address1
			city
			state
			country
			postalcode
			phone
			fax
			emailaddress2
			tin
			url
			username
			password
			pay_name
			pay_address
			pay_city
			pay_state
			pay_country
			pay_postalcode
			pay_payment_method
			pay_cardnumber
			pay_cardcode
			pay_cardexp
			pay_account_number
			pay_routing_number
			notes
			contact_info
			blurb/ )
	{
		push (@list, $field{$_}->{'value'} || '');
	}	

#	$sth = $db->prepare($qry);
#	$rv = $sth->execute(@list);
	return $db->do($qry, undef, @list);
}

###### 06/10/03 added the activation routine
###### 06/18/03 Added the auto-creation of a member record upon activation with Create_Member.
######          Added the SPID field next to the Activate button.
######          Added the Check_SPID and Check_Email subroutines.
###### 06/24/03 Removed the 'or blank' default assignment from the Load_Params subroutine
######           since it was blanking out zero value fields, specifically the status field.
###### 06/25/03 Added the notification request entry to the Update routine - when activating.
###### 06/26/03 added handling for the pay_cardcode, also added explicit SQL to update the stamp and operator at an activation
###### 06/27/03 Added the notification request entry to all members in the newly activated merchant's local area.
###### 07/03/03 changed the SPID to a referral ID captured with the original application
###### 07/07/03 added true to the -force tags.
###### 07/17/03 Defaulted member id to NULL to take care of updates before activation.
###### 07/22/03 Added the archived records link and functionality.
###### 07/23/03 added handling for the TIN
###### 08/08/03 removed the requirement for email addresses and tin per Dick
###### 08/20/03 revised the activation procedure to simply activate a location/vendor if the
###### 		set was already present instead of creating a new vendor. Also fixed the 
###### 		state drop-down handling to work properly if the text field was empty.
###### 08/27/03 Added the 'Pay by checking account' functionality.
###### 09/04/03 fixed an empty var error in the expiration date components when there was no data to work with
###### 09/19/03 Commented out the insertion of a notification_type 70 into the notifications table. These 
######			notifications will now be handled by notification_type 71 created by a trigger on
######			the locations table.
###### 09/24/03 Added the Special Exception Verbiage handling.
###### 10/10/03 revised the source of the categories
###### 11/11/03 added the 'Money Bookers' pay method and added the on_change event to the drop-down
###### 11/12/03 Modified the activate sequence to include updating the discount record
######			percentages with the master record values.
###### 11/26/03 added setting of the new 'activated' date field in the master record
###### 12/11/03 removed the setting of the 'activated' field from the script since it was being
###### invoked for all updates (will move it to a database trigger/function)
###### 02/01/06 Changed the UPDATE/UPDATE combo in sub Activate_Location from a "prepare" to a "do",
######		changed the INSERT/UPDATE/UPDATE combo in sub Activate_Location from a "prepare" to a "do",
######		changed the INSERT/UPDATE combo in sub Update from a "prepare" to a "do",
###### 02/23/06 tweaked the $sth handling in Update()
###### 05/14/07 revised cboExp to generate a dynamic group of years instead of a hard-coded list
###### 11/17/14 made the charset utf-8 at the top since the new server does not have the CGI.pm hack to automatically make it so
###### 06/11/15 implemented -default in cbo_State