#!/usr/bin/perl -w
###### co_payout.cgi
######
###### A process to create Charitable Org. payout totals
######  for the Accounting Dept. to disperse donations.
######
###### created: 10/11/05	Karl Kohrt
######
###### last modified: 11/07/05	Karl Kohrt
	
use strict;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use MailingUtils;
use Mail::Sendmail;
use MIME::Lite;

###### CONSTANTS
our $US_CHECK_MIN = 5.00;		# Minimum amount for a US check to be created.
our $INTL_CHECK_MIN = 50.00;	# Minimum amount for an International check to be created.
our $UPLINE_ROLE = 120;		# Marketing Director to be carbon copied on email.
our $cgiini_script = 'co_payout.cgi';
our $ADMIN_ADDRESS = "webmaster\@dhs-club.com";
our $ACCOUNTING_ADDRESS = "acfspvr\@dhs-club.com";
our @ACCESS_GROUP = qw/cindy pat k_kohrt bill/;

###### GLOBALS
our $q = new CGI;
our $db = '';
our $mu = new MailingUtils;			
our $payouts = ();
our $co_id =  '';
our $count =  0;
our $grand_total =  0;
our $qbooks_file = '';
our $PAYFILE = "/home/httpd/cgi/admin/co_payout.txt";
our %EMAIL = ('co'	=> '10470',	# The email for the CO. We'll cc the Marketing Director.
		'md'	=> '10471' 	# For the Marketing Director in case the CO doesn't have email.
		);
# Today's date (mm-dd-yyyy).
my ($day, $mon, $year) = (localtime)[3,4,5];
$mon++;
$year += 1900;
if ( length($day) == 1 ) { $day = '0' . $day }	# Pad a single digit day.
if ( length($mon) == 1 ) { $mon = '0' . $mon }	# Pad a single digit month.
our $today = "$mon/$day/$year";

################################
###### MAIN starts here.
################################

# Get the admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
$db = ADMIN_DB::DB_Connect( $cgiini_script, $operator, $pwd ) || die "Couldn't connect to the database\n";

# The ADMIN_DB 'accounting' group now includes the entire staff, so I won't use it here.
#if (ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1)
###### If in the access group, continue processing, otherwise they're done.
if ( grep {/$operator/} @ACCESS_GROUP )
{
	# Stop buffering so we get realtime output to the browser.
	$|=1;
	print $q->header();
	print "<h3>Charitable Organization Payout Results</h3>";

	# Get all the donations that are not reconciled.
	my $donations = $db->selectall_hashref("SELECT c.*, m.firstname, m.lastname
							FROM 	co_trans c
								INNER JOIN members m ON c.member_id = m.id
							WHERE 	c.reconciled = 'f';", 'trans_id') || '';

	# Loop through all the un-reconclied transactions, combining them into the %payouts hash.
	foreach my $trans_id ( keys %{$donations} )
	{
		$co_id = $donations->{$trans_id}{type} . $donations->{$trans_id}{type_id};

		# Put some items into the hash only if this is the first occurence.
		unless ( $payouts->{$co_id}{list} )
		{
			$payouts->{$co_id}{list} = "Donation:\tFrom Donor:";
			$payouts->{$co_id}{type} = $donations->{$trans_id}{type};
			$payouts->{$co_id}{type_id} = $donations->{$trans_id}{type_id};

			# Get the country for check minimum purposes.
			my $qry = '';
			if ( $payouts->{$co_id}{type} eq 'ag' )
			{
				$qry = "SELECT 	country
					FROM 		members
					WHERE 		membertype = 'ag'
					AND 		id = ?;";
			}
			elsif ( $payouts->{$co_id}{type} eq 'co' )
			{
				$qry = "SELECT 	country
					FROM 		co_info
					WHERE 		id = ?;";
			}
			$payouts->{$co_id}{country} = 
					$db->selectrow_array($qry,
								undef,
								$payouts->{$co_id}{type_id}) || '';
		}

		# Add the data to the running email text and total.
		$payouts->{$co_id}{list} .= "\n\$$donations->{$trans_id}{amount}\t\t$donations->{$trans_id}{firstname} $donations->{$trans_id}{lastname}";
		$payouts->{$co_id}{total} += $donations->{$trans_id}{amount};

		# Keep track of the IDs so that we can mark them reconciled if necessary.
		$payouts->{$co_id}{trans_id}{$trans_id} = $trans_id;
	}

	open (PAY_FILE, ">$PAYFILE");
	print PAY_FILE "!TRNS,TRNSID,TRNSTYPE,DATE,ACCNT,NAME,CLASS,AMOUNT,DOCNUM,MEMO,CLEAR,TOPRINT,ADDR1,ADDR2,ADDR3,ADDR4,ADDR5,,0\n";
	print PAY_FILE "!SPL,SPLID,TRNSTYPE,DATE,ACCNT,NAME,CLASS,AMOUNT,DOCNUM,MEMO,CLEAR,QNTY,,0\n";
	print PAY_FILE "!ENDTRNS\n";

	# Process each of the CO's that we found in the transaction loop above.
	foreach $co_id ( keys %{$payouts} )
	{
		my $co_total = sprintf "%.2f", 0;
		my $status = "Not Paid";
		my $md_info = ();
		my $email_note = "No email sent.";

		# If the total meets the minimum to create a check.
		if ( ($payouts->{$co_id}{country} eq 'US' 
				&& $payouts->{$co_id}{total} >= $US_CHECK_MIN)
			|| $payouts->{$co_id}{total} >= $INTL_CHECK_MIN)
		{
			# Mark them as being paid.
			$status = "<b>Paid Out</b>";

			# Reconcile all the transactions that were collected/
			foreach ( keys %{$payouts->{$co_id}{trans_id}} )
			{
				# Update the donation transaction to show it as reconciled.
				my $rv = $db->do("	UPDATE	co_trans
							SET 	reconciled = 't',
								entered = NOW(),
								operator = '$cgiini_script'
							WHERE 	trans_id = ?;",
							undef, $_);
			}
			# Post an offsetting transaction for the CO that we are paying.
			my $rv = $db->do("	INSERT INTO	co_trans (	type,
									type_id,
									member_id,
									memo,
									amount,
									reconciled,
									trans_date,
									operator )
								VALUES( ?, ?, ?, ?, ?, ?, ?, ? );",

						undef,
						($payouts->{$co_id}{type},
						$payouts->{$co_id}{type_id},
						'-1',
						'Summary Payout',
						$payouts->{$co_id}{total}*-1,
						't',
						'NOW()',
						$cgiini_script));
			unless ( $rv == 1 ) 
			{ 
				my $tmp_amount = $payouts->{$co_id}{total}*-1;
				Email_Admin("INSERT INTO co_trans failed with the following:
				type= $payouts->{$co_id}{type}
				type_id= $payouts->{$co_id}{type_id}
				member_id= '-1'
				memo= 'Summary Payout'
				amount= $tmp_amount
				reconciled= 't'
				trans_date= '$today'
				operator= $cgiini_script");
			}
			# Get the CO's email info.
			my $co_info = Get_CO($payouts->{$co_id}{type}, $payouts->{$co_id}{type_id});
			# Get the upline MD's email info.

# commented out 04/25/12 as there is no MD anymore
#			$md_info = Get_Upline($UPLINE_ROLE, $co_info->{referral_id});

			$co_total = sprintf "%.2f", $payouts->{$co_id}{total};
			++$count;
			$grand_total += $co_total;

			# An item for the email list.
			$payouts->{$co_id}{list} .= "\n---------------\n\$$co_total Total\n";

			# An item for the QuickBooks list.
			print PAY_FILE qq#TRNS,,CHECKS,$today,CASH:Wachovia - Commissions 1333,"$co_info->{alias}",,-$co_total,,,N,Y,"$co_info->{alias} ($payouts->{$co_id}{type}-$payouts->{$co_id}{type_id})","$co_info->{address1}","$co_info->{address2}","$co_info->{city}, $co_info->{state}  $co_info->{postalcode}","$co_info->{country}",,0\n#;
			print PAY_FILE "SPL,,CHECKS,$today,MEMBER PAYMENTS:Donations,,,$co_total,,,N,1,,0\n";
			print PAY_FILE "ENDTRNS\n";

			# If the CO has an email address, email them and CC the Marketing Director.
			if ( $co_info->{emailaddress} )
			{
				Email_Someone('co', $co_info, $md_info, $payouts->{$co_id}{list});
				$email_note = " (Email sent to CO & MD.)";
			}
			# Otherwise, email the Marketing Director.
			else
			{
				Email_Someone('md', $md_info, $co_info, $payouts->{$co_id}{list});
				$email_note = " (Email sent to MD only.)";
			}
		}
		# An item for the browser list.
		print "$payouts->{$co_id}{type}-$payouts->{$co_id}{type_id}, ";
		print "MD ID= $md_info->{id}, " if $md_info->{id};
		print "\$" . (my $tmp = sprintf "%.2f", $payouts->{$co_id}{total});
		print " (country=$payouts->{$co_id}{country}), ";
		print "$status - ";
		print "$email_note";
		print " \n<br>";
	}
	print PAY_FILE "!ENDTRNS\n";
	$grand_total = sprintf "%.2f", $grand_total;
	print "<BR>$count CO's paid out for a total of \$$grand_total.\n";
	print "<h4>Email with the QuickBooks file attached has been sent to: $ACCOUNTING_ADDRESS.\n</h4>";

	close PAY_FILE;
	Email_Accounting();
}
else
{
	print $q->header();
	print "<h3>You must be a member of the \@ACCESS_GROUP (defined in the script) to run this process.</h3>";
}

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

###### Generate e-mail to the Accounting Dept.
sub Email_Accounting
{
	# Build the message
	my $message = MIME::Lite->new(
           	From     => 'co_payout.pl',
           	To       => $ACCOUNTING_ADDRESS,
           	Subject  => 'Charitable Organization Payouts',
           	Type     => "text/plain",
           	Encoding => '7bit',
           	Data     => "Attached is the QuickBooks upload file for the Charitable Organization payouts.");
	# Add the attachment
	$message->attach(
           	Type     	=> "application/text",
           	Encoding 	=> "base64",
      	     	Path 		=> $PAYFILE,
           	Filename 	=> "co_payout.iif");
	# Send the message
	$message->send;

	return 1;
}

###### Email errors to the admin.
sub Email_Admin
{
	my $errors = shift || '';
	open (MAIL, '|/usr/sbin/sendmail -t') || die "Couldn't open sendmail.\n\n$errors\n";
	print MAIL "To: $ADMIN_ADDRESS\n";
	print MAIL "Subject: $cgiini_script errors\n\n";
	print MAIL $errors;
	close MAIL;
}

###### Generate e-mail to the upline or downline - defined in $which.
sub Email_Someone{
	my ($which, $to, $other, $donations) = @_;
	my ($to_tmp, $from_tmp, $subject) = '';

	# Get the base email template(s).
	my $pc = $mu->Get_Object($db, $EMAIL{$which});

	# Select the template based upon the language preference.
	my $MSG = $mu->Select_Translation($pc, $to->{language_pref});

	###### remove any comment fields
	$MSG =~ s/(^|\n)(#.*)//g;
	###### remove line feeds (these can be caused by windows pasting)
	$MSG =~ s/\r//g;

	unless ($MSG){ die "Couldn't load Email Template: $EMAIL{$which}\n" }

	my $cc = '';
	my $bcc = $ADMIN_ADDRESS || '';
	$MSG =~ s/%%bccEmail%%/$bcc/;
	$MSG =~ s/%%donations%%/$donations/;
	$MSG =~ s/%%ag_id%%/$to->{ag_id}/;

	# These are our recipient member's info.
	$MSG =~ s/%%to_alias%%/$to->{alias}/g;
	$MSG =~ s/%%to_firstname%%/$to->{firstname}/g;
	$MSG =~ s/%%to_lastname%%/$to->{lastname}/g;
	$MSG =~ s/%%to_email%%/$to->{emailaddress}/g;

	# These are our other member's info.
	$MSG =~ s/%%other_alias%%/$other->{alias}/g;
	$MSG =~ s/%%other_firstname%%/$other->{firstname}/g;
	$MSG =~ s/%%other_lastname%%/$other->{lastname}/g;
	$MSG =~ s/%%other_email%%/$other->{emailaddress}/g;

	# extract our 'To' line from the template
	$MSG =~ s/(^|\n)(To:.*)//i;
	($to_tmp = $2) =~ s/^To:\s*//i;
	# extract our 'From'
	$MSG =~ s/(^|\n)(From:.*)//i;
	($from_tmp = $2) =~ s/^From:\s*//i;
	# extract our 'Subject'
	$MSG =~ s/(^|\n)(Subject:.*)//i;
	($subject = $2) =~ s/^Subject:\s*//i;
	# extract our 'Bcc'
	$MSG =~ s/(^|\n)(Bcc:.*)//i;
	($bcc = $2) =~ s/^Bcc:\s*//i if $2;
	# extract our 'Cc'
	$MSG =~ s/(^|\n)(Cc:.*)//i;
	($cc = $2) =~ s/^Cc:\s*//i if $2;

	# remove all remaining leading spaces and newlines
	$MSG =~ s/^(\s|\n)*//;

	# perform AOL link creation if we are mailing to an AOL address
	if ($to =~ m/\@aol\.com/){
		$MSG =~ s#(http\S*)#<a href="$1">$1</a>#g;
	}
		
	my %mail = (	To =>		$to_tmp,
			Cc =>		"$cc, $ACCOUNTING_ADDRESS",
			Subject =>	$subject,
			From =>	$from_tmp,
			Message =>	$MSG);

#	$mail{Bcc} = $bcc if $bcc;
#	&debug(\%mail);
	unless ( sendmail(%mail) )
	{
		print "An attempt to email your $which failed for the following reason - $Mail::Sendmail::error\n";
	}
	return 1;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}

# Get the CO's email info.
sub Get_CO
{
	my ($type, $id) = @_;
	my $info = ();
	$info = $db->selectrow_hashref("	SELECT id,
							co_name AS alias,
							address1,
							address2,
							city,
							state,
							postalcode,
							country,
							emailaddress,
							firstname,
							lastname,
							referral_id
						FROM 	co_list_view
						WHERE 	type = '$type'
						AND	type_id = $id");
	# If this is an AG, get their AG number for email display.
	if ( $type eq 'ag' && $info->{id} )
	{
		$info->{ag_id} = $db->selectrow_array("	SELECT alias
								FROM 	members
								WHERE 	id = $info->{id}");
	}
	else { $info->{ag_id} = "N/A" }
	return $info;
}

###### Find the upline Dream Team member.
sub Get_Upline
{
	my ($role, $spid) = @_;
	my $start = $spid;
	my $upline_role = '';
	my $tmp_sponsor = ();
	my %UplineList = ();
	my $SPONSOR_QRY = "SELECT 	m.id,
					m.spid,
					m.alias,
					m.firstname,
					m.lastname,
					m.emailaddress,
					COALESCE(m.language_pref, 'en') AS language_pref,
					m.membertype,
					COALESCE(v.level, 0) AS role
				FROM members m 
				LEFT JOIN nbteam_vw v
				ON v.id=m.id
				WHERE m.id= ?";
	my $sp_sth = $db->prepare($SPONSOR_QRY);

	# We won't break out of this loop until we have the appropriate role or we hit 01.
	while ((not ($upline_role)) && $spid > 1)
	{
		# if our upline is in the hash we'll use that, otherwise we'll look it up and load it in the hash
		unless ( $tmp_sponsor = $UplineList{$spid} )
		{
			###### Load another upline into the local hash.
			$sp_sth->execute($spid);
			$tmp_sponsor = $sp_sth->fetchrow_hashref;
			$sp_sth->finish();
			# Load this person into our hash so we don't have to look them up again if needed.
			if ( $tmp_sponsor ) { $UplineList{$spid} = $tmp_sponsor }
			else
			{
				Email_Admin("Couldn't retrieve record for $spid");
			}
		}

		last unless $tmp_sponsor;

		# this will capture any loops
		if ($tmp_sponsor->{spid} eq $start){
			Email_Admin("Loop detected at: $start\n");
			last;
		}	
		# Set the upline_role if this person is at the appropriate role.
		if ($tmp_sponsor->{role} >= $UPLINE_ROLE){ $upline_role = $tmp_sponsor }

		$spid = $tmp_sponsor->{spid};
	}
	return $tmp_sponsor;
}


###### 10/11/05 Removed the test email addresses that were still here after testing.
######		Changed the QBooks account to "CASH:Wachovia - Commissions 1333".
######		Added the AG-ID query to Get_CO for email display per Dick.
###### 11/07/05 Added code to check for COs with less than the defined minimum amount needed
######		to create a check and to leave their transactions un-reconciled in that case.
#2007-07-22  Keith  Added the ACOUNTING_EMAIL_ADDRESS to the Cc in the Email_Someone subroutine






