#!/usr/bin/perl -w
###### domains_blocked.cgi
###### a routine to view/manage? the mail_refused_domains table (domains that will not accept our mail)
###### last modified 03/05/04	Bill MacArthur

use strict;
use DBI;
use CGI;
#require '/home/httpd/cgi-lib/G_S.lib';
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
#require '/home/httpd/cgi-lib/DB_Connect.pl';
use CGI::Carp qw(fatalsToBrowser);

$| = 1;   # do not buffer error messages

###### GLOBALS
my $q = new CGI; 
my ($db);
my $mrd_cnt = 0;			###### the number of blocked domains
my $num_of_cols = 4;			###### the number of columns in our output table

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'domains_blocked', $operator, $pwd )){exit}

our $action = $q->param('action') || '';

unless ($action)
{
	Do_Page();
}


END:
$db->disconnect;
exit;

sub Do_Page{
###### output the page with our complete list of domains
	my ($x, $y, $tbl, $tmp);
	my $rowcolor = '#ffffee';
	my $time = scalar localtime();

	my $qry = "SELECT domain, 1 AS flagged FROM domains_blocked
UNION ALL
SELECT domain, 0 AS flagged FROM domains_bogus
ORDER BY domain";
	
	my $sth = $db->prepare($qry); $sth->execute;
	my $dat = $sth->fetchall_arrayref;
	$sth->finish;
	$mrd_cnt = scalar @$dat;

###### instead of handling what would be blank columns at the end of table creation, we'll push empty
###### vals onto the end of the stack to give an evenly divisible number with the number of columns
	my $remainder = $mrd_cnt % $num_of_cols;
	if ($remainder)
	{
		for ($x=0; $x < $num_of_cols - $remainder; $x++){ push (@$dat, ['&nbsp;']) }
	}

###### we'll divide the total lines of dat by the number of cols to tell us the number of table rows
	my $new_cnt = scalar @$dat;
	for ($x=0; $x < $new_cnt/$num_of_cols; $x++)
	{
		$tbl .= "<tr bgcolor=\"$rowcolor\">\n";
###### we'll insert the desired number of columns into each row
		for ($y=0; $y < $num_of_cols; $y++)
		{
			$tmp = shift @$dat;
			$tbl .= $q->td({-class=>"d$tmp->[1]"}, $tmp->[0]);
		}
		$tbl .= "</tr>\n";
		$rowcolor = ($rowcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	}

	$tbl = $q->table({-border=>1, -cellspacing=>0, -cellpadding=>4}, $tbl);

###### now let's output our page
	print $q->header(-expires=>'now');
	print <<PAGE;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache" /> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00" /> 
	<title>Mail Problem Domains</title>
<style type="text/css">
<!--
 td.d1{
 font-family: Arial, Helvetica, sans-serif;
 font-size: 12px;
 }
 td.d0{
 font-family: Arial, Helvetica, sans-serif;
 font-size: 12px;
 color: #0000aa;
 font-weight: bold;
 }

-->
</style>
</head>
<body bgcolor="#ffffff">

<div align="center" style="font-size: 9pt;"><b>This is the complete list of domains with known delivery issues.
</b><br />Most are explicit mail blocks. Domains in <span style="color: #0000aa; font-weight: bold;">blue</span> have other issues.
<br />Please direct additions/deletions to Bill<br /><br />
As of &nbsp;<b>$time PST</b> , the number of domains listed: <b>$mrd_cnt</b><br />
$tbl
</div>
</body></html>
PAGE
}

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print $q->end_html();
}

###### revamped the query to distinguish between the type of domain problem and format the report accordingly
###### removed the reference to DB_Connect
