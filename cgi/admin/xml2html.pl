#!/usr/bin/perl -w
###### xml2html.pl
###### process to transform an XML page into HTML using an XSL stylesheet.
###### 
###### Created: 10/26/2004 Karl Kohrt
######
###### last modified: 

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::LibXSLT;
use XML::LibXML;

###### GLOBALS
our $path = "/home/httpd/html/";
our $q = '';
my $xslt = '';
my $xsl = '';			# Our xsl stylesheet.
my $xml = '';			# Our xml file.
my $html = '';		# Our html file.
my $line_break = "\n";
my $badfile = 0;		# Marks whether or not a non-existent/bad file was submitted.
my $created = 0;		# Marks whether or not a new html file was created.

# Try to get the command line arguments first.
$xsl = $ARGV[0];		# Command line xsl stylesheet.
$xml = $ARGV[1];		# Command line xml file.

# If there are no command line arguments, we must be called from the browser.
unless ( $ARGV[0] && $ARGV[1] )
{
	$q = new CGI;
	$xml = $q->param('xmlfile');		# Our xml file.
	$xsl = $q->param('xslfile');		# Our xsl stylesheet.
#	$line_break = "<br>";
	print $q->header(), $q->start_html("XML to HTML Conversion Tool");
	print "<h2>XML to HTML Conversion Tool</h2>
		<b>Results:</b>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='/admin/xml2html.html'>Create Another</a><br><br>";
	print "<TEXTAREA COLS='80' ROWS='10'>";	
	open(STDERR, ">&STDOUT");
}
# Full path to the files.
$xsl = $path . $xsl;
$xml = $path . $xml;

# Check that the files provided exist.
if ( open(FILEX, "<$xml") ) { close FILEX }
else { print "File not found: $xml$line_break"; $badfile = 1; }
if ( open(FILEX, "<$xsl") ) { close FILEX }
else { print "File not found: $xsl$line_break"; $badfile = 1; }
unless ( $badfile )
{		
	# Create an html version.	
	($html = $xml) =~ s/\.xml/\.html/;
	
#print "$xml$line_break"; print "$xsl$line_break"; print "$html$line_break";

	# Transform the xml into html format.
#	$xslt = XML::LibXSLT->new ($xsl, warnings => 1);
#	$xslt->transform ($xml);

         my $parser = XML::LibXML->new();
         my $xslt = XML::LibXSLT->new();

         my $source = $parser->parse_file($xml);
         my $style_doc = $parser->parse_file($xsl);

         my $stylesheet = $xslt->parse_stylesheet($style_doc);

         my $results = $stylesheet->transform($source);

	# If the file opened successfully.
	if ( open(HTML, ">$html") )
	{
#		print HTML $q->header(-charset=>'utf-8');
		print HTML $stylesheet->output_string($results);		# Put the newly generated html in the file.
		close(HTML);
		chmod 0666, $html;			# Change permissions so 'nobody' can write to it later.
		print "$line_break$line_break Created file $html $line_break";
		$created = 1;
	}
	else { print "Could not open file $html $line_break"; }				
}

# If this was a web browser call, finish the html page.
if ( $q )
{
	print "</TEXTAREA>";
	if ( $created )
	{}
	$html =~ s/$path/\//;
	print "<br> See it here: <a href='$html' target='_blank'>$html</a>";
}
if ( $xslt ) { $xslt->dispose() }
exit;


