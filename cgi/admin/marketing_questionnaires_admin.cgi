#!/usr/bin/perl -w
################################################################
# marketing_questionnairs_admin.cgi
#
# A general report for the Marketing Questionnaires
#
# Deployed: 06/08
# Author: Keith Hasely
# Version: 0.1
################################################################

################################################################
# Includes
################################################################
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use ADMIN_DB;
use XML::Simple;
use XML::local_utils;
use CGI::Carp qw(fatalsToBrowser set_message);

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
our $q = CGI->new();
our $xs = XML::Simple->new(NoAttr=>1);
our $gs = new G_S;

our $ME = $q->url;

our	$userid = 0;
our $db = ();
my $sth = '';
################################################################
# Subroutine(s)
################################################################

###############################
# This sub came around because
# Bill was/is using a "goto" statement!
#
###############################
sub exitScript
{
	$db->disconnect if ($db);
	exit;
}
###############################

###############################
# Output an error message for the user
# then exit the script cleanly
#
# Param		hash
# 			string message			The mesage to output before exiting.
# Return	null
###############################
sub errorExitScript
{
	my $args = shift || {};
	print $q->header(-TYPE => 'text/html');

	print qq(<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>);
	print $args->{message};
	print qq(</title></head><body><p>);
	print $args->{message};
	print qq(</p></body></html>);
	exitScript();
}
###############################


################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exitScript() unless $db;
$db->{RaiseError} = 1;
$db->{pg_enable_utf8} = 1;


# Check if the user is admin
if ((!$q->cookie('operator') && !$q->cookie('pwd')) && !ADMIN_DB::DB_Connect( 'pwp', $q->cookie('operator'), $q->cookie('pwd')))
{
	exitScript();
}


print $q->header(-TYPE => 'text/html');
# @import "all.css"; /* just some basic formatting, no layout stuff */

print <<EOS;
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

				<title>Questionnaires Reports</title>

<style type="text/css">
	\@import "all.css"; /* just some basic formatting, no layout stuff */

	body {
		font:	12px/1.2 Verdana, Arial, Helvetica, sans-serif;
		background:#ddd;
		margin:10px 10px 0px 10px;
		padding:0px;
		}

	#leftcontent {
		position: absolute;
		left:10px;
		top:50px;
		width:250px;
		background:#fff;
		border:1px solid #000;
		}

	#centercontent {
		background:#fff;
   		margin-left: 299px;
   		margin-right:299px;
		border:1px solid #000;
		voice-family: "\\"}\\"";
		voice-family: inherit;
   		margin-left: 249px;
   		margin-right:249px;
		}
	html>body #centercontent {
   		margin-left: 249px;
   		margin-right:249px;
		}

	#rightcontent {
		position: absolute;
		right:10px;
		top:50px;
		width:250px;
		background:#fff;
		border:1px solid #000;
		}

	#header {
		background:#fff;
		height:39px;
		border-top:1px solid #000;
		border-right:1px solid #000;
		border-left:1px solid #000;
		voice-family: "\\"}\\"";
		voice-family: inherit;
		height:39px;
		}
	html>body #header {
		height:39px;
		}

	p,h1,pre {
		margin:0px 10px 10px 10px;
		}

	h1 {
		font-size:14px;
		padding-top:10px;
		}

	#header h1 {
		font-size:14px;
		padding:10px 10px 0px 10px;
		margin:0px;
		}

</style>
</head><body>

<div id="header"><h1>Questionnaires Usage Report</h1></div>
<div id="leftcontent">
<h1>Completed Surveys</h1>
<p>
EOS

##################################################
# Completed surveys count.
##################################################
$sth = $db->prepare("
SELECT
count(qus.id) AS completed_surveys,
q.id,
q.questionnaires_name
FROM
questionnaires_user_status AS qus
JOIN
questionnaires AS q
ON
q.id = qus.questionnaires_id
WHERE
qus.completed IS NOT NULL
GROUP BY
q.id,
q.questionnaires_name
");

$sth->execute();


while (my $row = $sth->fetchrow_hashref)
{

	print $row->{questionnaires_name};
	print q(: );
	print $row->{completed_surveys};
	print q(<br />);

}

if (!$sth->rows())
{
	print q(No surveys have been completed. <br />);
}

print <<EOS;
<br />
</p>
</div>
<div id="centercontent">
<h1>Incompleted Surveys</h1>
<p>
EOS

##################################################
# Uncompleted surveys count.
##################################################
$sth = $db->prepare("
SELECT
count(qus.id) AS noncompleted_surveys,
q.id,
q.questionnaires_name
FROM
questionnaires_user_status AS qus
JOIN
questionnaires AS q
ON
q.id = qus.questionnaires_id
WHERE
qus.completed IS NULL
GROUP BY
q.id,
q.questionnaires_name
");

$sth->execute();

while (my $row = $sth->fetchrow_hashref)
{
	print $row->{questionnaires_name};
	print q(: );
	print $row->{noncompleted_surveys};
	print q(<br />);
}

if (!$sth->rows())
{
	print q(No surveys are incomplete. <br />);
}
print <<EOS;
<br />
</p>
</div>
	<div id="rightcontent">
	<h1>Updated Surveys</h1>
	<p>
EOS

##################################################
# Updated surveys count.
##################################################
$sth = $db->prepare("
SELECT
count(qus.id) AS updated_surveys,
q.id,
q.questionnaires_name
FROM
questionnaires_user_status AS qus
JOIN
questionnaires AS q
ON
q.id = qus.questionnaires_id
WHERE
qus.completed <> qus.updated
GROUP BY
q.id,
q.questionnaires_name
");

$sth->execute();

while (my $row = $sth->fetchrow_hashref)
{
	print $row->{questionnaires_name};
	print q(: );
	print $row->{updated_surveys};
	print q(<br />);
}

if (!$sth->rows())
{
	print q(No surveys have been updated.  <br />);
}

print <<EOS
<br />
</p>
</div>
	</body></html>
EOS
