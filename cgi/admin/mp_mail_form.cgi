#!/usr/bin/perl -w
###### mp_mail_form.cgi
######
###### This script allows the staff to create and submit an email that will be  
###### sent out to all the former members who are now in the members_purge table.
######
###### Created: 12/17/2003	Karl Kohrt
######
###### Last modified: 02/05/2004	Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS
our %TMPL = ( 'run_flag'	=> 10247,
		'email'	=> 10248,
		'create'	=> 10251 );
our %TBL = 	( 'objs',	=> 'cgi_objects');

# The minimum fields that we expect to find in the email template.
our @the_email = qw/<%%emailaddress%%> To: From: Subject: remove/;

our $q = new CGI;
our $action = ( $q->param('action') || '' );
our ($db);
our $job_status = '';
our $dover_msg = '';
our $messages = '';	
our $current_email = ( $q->param('current_email') || '' );

###### restrict usage to after the 5th of the month
if (${[localtime]}[3] <= 5)
{
	Err("This system cannot be used until after the 5th.");
	exit;
}

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = &ADMIN_DB::DB_Connect( 'mp_mail_form.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# If this is the first time through, just print the standard page.
unless ( $action )
{
	Check_Background();	# Get the background process' status for display purposes.
	Get_Email_Template($TMPL{email});
	Do_Page();
}
# If we're creating a new email job.
elsif ($action eq 'create')
{
	Check_Background();	# Check that the background process is available to accept a new job.
	Check_Required();	# Make sure the minimum required fields are included.

	# If the other checks are clean. 
	unless ( $dover_msg )
	{
		# If the Update of the email template was successful.
		if ( Update_Email() )
		{
			# Update the background process so it is no longer available.
			Update_Background();
		}					
	}
	else
	{
		# Move the error messages to the placeholder substitution variable.
		$messages = $dover_msg;
	}		
	Do_Page();
}
# If we're viewing a sample of a new email.
elsif ($action eq 'view')
{
	Do_Sample_Page();
}
else
{
	Err('Undefined URL action requested.');
}

$db->disconnect;
exit(0);


###### ###### ###### ###### ######
###### Subroutines begin here
###### ###### ###### ###### ######

######	Check to see if the background process is available to accept a new job.
sub Check_Background
{
	my $qry = "	SELECT obj_value
			FROM $TBL{objs}
			WHERE obj_id = ?;";
	my $sth = $db->prepare($qry);
	$sth->execute($TMPL{run_flag});
	$job_status = $sth->fetchrow_array;

	if ( $job_status ne 'AVAILABLE')
	{
		$dover_msg .= $q->span({-class=>'r'},
			"The Email template is not available due to a previously submitted mailing.<br>\n");
	}
	$sth->finish();
	return 1;
}					
							
######	Rudimentary check of the minimum fields required for a successful email.
sub Check_Required
{
	foreach (@the_email)
	{
		unless( $current_email =~ m/$_/ )
		{
			$dover_msg .= $q->span({-class=>'r'}, "The required field '$_' appears to be missing.<br>\n");
		}
	}
	return 1;
}

###### Print our main creation form with any pertinent messages.
sub Do_Page
{
	my $tmpl = &G_S::Get_Object($db, $TMPL{'create'}) || die "Couldn't open: $TMPL{'create'}";
	$tmpl =~ s/%%current_email%%/$current_email/g;
	$tmpl =~ s/%%messages%%/$messages/g; 
	$tmpl =~ s/%%job_status%%/$job_status/g; 
	print $q->header(-expires=>'now'), $tmpl ;
}

###### Print a sample version of the new email.
sub Do_Sample_Page
{
	# remove any comment fields
	$current_email =~ s/(^|\n)(#.*)//g;

	# remove line feeds (these can be caused by windows pasting)
	$current_email =~ s/\r//g;

	# Substitute arbitrary values.
	$current_email =~ s/%%alias%%/AB123456789/g;
	$current_email =~ s/%%firstname%%/Asample/g;
	$current_email =~ s/%%lastname%%/Beforewego/g;
	$current_email =~ s/%%emailaddress%%/ab\@clubshop.com/g;

	# Put it all in a textarea box so that it displays properly.

	$current_email = "<h3>Close this window when you are ready to 'Create Email' 
			<br>or to make further template modifications.</h3>
			<form><textarea name='current_email' rows='25' cols='80'>$current_email</textarea>					<br><input type='reset' value='Close Window' onClick='self.close()'></form>";
	print $q->header(-expires=>'now'), $current_email ;
}

##### Prints an error message to the browser.
sub Err
{
	# -expires=>'now'  for the $q->header function.
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>";
	print $_[0];
	print "</B><BR>";
	print $q->end_html();
}

###### Get the current email template.
sub Get_Email_Template
{
	my $email_id = shift;
	my $qry = "	SELECT obj_value
			FROM $TBL{objs}
			WHERE obj_id = ?;";
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($TMPL{email});
	defined $rv or return;

	$current_email = $sth->fetchrow_array;
	return 1;
}

###### Update the background process' status.
sub Update_Background
{
	$job_status = 'WAITING';
	my $qry = "	UPDATE $TBL{objs}
			SET obj_value = ?
			WHERE obj_id = ?";
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($job_status, $TMPL{run_flag});
	if ( defined $rv )
	{
		$messages .= $q->span({-class=>'b'}, "The email job has been submitted.<br>\n");
		return 1;
	}
	else
	{
		$messages .= $q->span({-class=>'r'}, "The email job was NOT submitted.<br>\n");
		return 0;
	}			
}

###### Update the email template.
sub Update_Email
{
	my $qry = "	UPDATE $TBL{objs}
			SET obj_value = ?
			WHERE obj_id = ?";
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($current_email, $TMPL{email});
	if ( defined $rv )
	{
		$messages .= $q->span({-class=>'b'}, "The email template has been updated.<br>\n");
		return 1;
	}
	else
	{
		$messages .= $q->span({-class=>'r'}, "The email template was NOT updated.<br>\n");
		return 0;
	}			
}

###### 12/22/03 Added the Do_Sample_Page subroutine and the 'view' case in the main program.
###### 02/05/04 added the restriction on usage
