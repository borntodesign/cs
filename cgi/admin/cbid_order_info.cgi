#!/usr/bin/perl -w
###### cbid_order_info.cgi
######
###### Lookup ClubBucks ID Order records using a field and a value supplied by the user.
###### This script gives the option to export the order_report to a file that can
######  be e-mailed to a member. It does not allow updates to ClubBucks ID records.
###### Created: 05/09/2003	Karl Kohrt
######
###### Last modified: 07/01/11 Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

###### GLOBALS

###### our page template
our %TMPL = (	'search'	=> 10157,
		'display'	=> 10158);

###### the table we'll get data from
our $TBL = 'cbid_orders';
our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $search_field = $q->param('search_field');
our $search_value = $q->param('search_value');


our ($db);	###### database variables
our $data = '';
our $dover_msg = '';

# Added an = before each fieldname so grep can identify the beginning of/and the whole word.
our @char_fields = qw/=operator =stamp =order_report =description/;
our @int_fields = qw/=order_id =member_id =cbid_start =cbid_end =cbid_number/;
our @result_list = ();

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'cbid_order_info.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

###### %current holds the values that are currently in the database. It has the form of 
###### 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'. Class and req are not currently used in this script.
my %current = (order_id 	=> {value=>'', class=>'n', req=>1},
		member_id	=> {value=>'', class=>'n', req=>0},
		cbid_start	=> {value=>'', class=>'n', req=>0},
		cbid_end	=> {value=>'', class=>'n', req=>0},
		operator	=> {value=>'', class=>'n', req=>1},
		stamp		=> {value=>'', class=>'n', req=>1},
		order_report	=> {value=>'', class=>'n', req=>0},
		description	=> {value=>'', class=>'n', req=>0}
	);


###### If this is the first time through, just print the Search page.
unless ( $action )
{
	Search_Form()	;
}
elsif ($action eq 'search')
{
	# Check the validity of the field and the value provided by the user.
	if ( validity_check() )
	{
		Run_Search($search_field, $search_value);
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		elsif ( @result_list > 1 )
		{
			###### create list report
			Display_List();
		}

		else
		{
			$data = shift @result_list;
			Load_Data();
			Do_Page();
		}
	}
}
elsif ($action eq 'show')
{
	if ( validity_check() ){
		Run_Search($search_field, $search_value);
		###### at this point we should have one row of data
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		else
		{		
			$data = shift @result_list;
			Load_Data();
			Do_Page();
		}
	}
}
elsif ($action eq 'void')
{
	# Load the passed parameters.
	foreach (keys %current) { $current{$_}{value} = $q->param($_) }
	
	# If none of the associated cards are activated.
	if ( Cards_Inactive() )
	{
		# Assign the reason for voiding to the description field.
		$current{description}{value} = "*VOIDED* - " . $q->param('reason');

		# Void the order and reset all associated cards to unassigned status.
		Void_Order();
	}
	else
	{
		# Let the user know why the order couldn't be voided.
		$dover_msg = $q->span({-class=>'r'},
			"This order cannot be voided because at least one of the associated cards<br>
			has been activated or voided, or this order has already been voided in the past.<br>");
	}
						
	# Get the updated record and display it to the browser.
	Run_Search('order_id', $current{order_id}{value});
	$data = shift @result_list;
	Load_Data();
	Do_Page();
}

else { Err('Undefined URL action requested.') }

$db->disconnect;
exit(0);


###### ###### ###### ###### ######
###### Subroutines begin here
###### ###### ###### ###### ######

###### Check to see that all associated cards are still not activated.
sub Cards_Inactive
{
	my ($qry, $sth, $rv) = ();
	my $how_many = 0;
	my $expected = $current{cbid_end}{value} - $current{cbid_start}{value} + 1;
	
	$qry = "	SELECT count(*)
			FROM 	clubbucks_master 
			WHERE 	id between ? and ?
			AND 	(class = 2 OR class = 3 OR class = 7)
			AND 	current_member_id IS NULL;";
	$sth = $db->prepare($qry);
	$sth->execute($current{cbid_start}{value}, $current{cbid_end}{value});
	$how_many = $sth->fetchrow_array();

	if ( $how_many == $expected ) { return 1 }	# Return true if we found all inactive,
	else { return 0 }					# otherwise, return false.
}

sub Create_hidden
{
	my $hid;
	###### we need to pass all our parameters that have values except the action param
	foreach ( $q->param() )
	{
		unless ( ($_ eq 'action') || ($_ eq 'reason') )
		{
			$hid .= $q->hidden($_);
		}
	}
	return $hid;
}

###### Displays the results if the search returned multiple records.
sub Display_List
{
	my $html;

	# print a title for the browser window.
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#ffffee', -title=>'ClubBucks Card Order Search Results');

	# print a simple page header.
	print qq!
		<div align="center"><h1>ClubBucks Card Order Search Results</h1>
		<TABLE align="center" border="2" cellpadding="2">
 			<TR>
			<TH> Order # </TH>
			<TH> Member ID </TH>
			<TH> CBID Start </TH>
			<TH> CBID End </TH>
			<TH> Operator </TH>
			<TH> Timestamp </TH>
			<TH> Description </TH>
			</TR>!;

	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		print qq!
			<TR>
			<TD><A href="$ME?action=show;search_field=order_id;search_value=$result->{order_id}" target="Resource Window $result->{order_id}"> $result->{order_id} </a></TD>
			<TD>$result->{member_id}</TD>
			<TD>$result->{cbid_start}</TD>
			<TD>$result->{cbid_end}</TD>
			<TD>$result->{operator}</TD>
			<TD>$result->{stamp}</TD>
			<TD>$result->{description}&nbsp;</TD>
			</TR>!;
	}

	print qq!
	</TABLE>
	<p><a href="$ME">Click Here for New Search</a></p></div>!;

	print $q->end_html();
}

###### Display an individual member record.
sub Do_Page
{
	my $hidden = Create_hidden();
	my $line = G_S::Get_Object($db, $TMPL{display}) || die "Couldn't open $TMPL{display}";
	$line =~ s/%%message%%/$dover_msg/;
	$line =~ s/%%hidden%%/$hidden/;
	$line =~ s/%%url%%/$q->url()/ge;

	$current{order_report}{value} =~ s/\n/<BR>/g;
	
	foreach my $key (keys %current)
	{
		$line =~ s/%%$key%%/$current{$key}{value}/g;
		$line =~ s/%%class_$key%%/$current{$key}{class}/g;
	}
	$line =~ s/%%quick_links%%/Quick_Links()/e;
	print $q->header(-expires=>'now'), $line;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>";
	print $_[0];
	print "</B><BR>";
	print $q->end_html();
}

###### this procedure presumes that the current keys are identical to the queried data
sub Load_Data
{
	foreach (keys %current){
		$current{$_}{value} = $data->{$_} || '';
	}
}

###### Creates links that display information related to this record.
sub Quick_Links
{
	my $links = $q->a({-href=>$ME, -class=>'q'}, 'New Search');
	$links .= "&nbsp;&nbsp;&nbsp;\n";
	$links .= $q->a({-href=>"/cgi/admin/cbid_orders.cgi", -class=>'q'}, 'New CBID Order');
	my @list = ();
	
	if ($current{member_id}{value}){
		push (@list, $q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$current{member_id}{value}", -class=>'q', -target=>'_blank'}, 'Member record') );
	}

	foreach (@list){
		$links .= "&nbsp;&nbsp;&nbsp;$_\n";
	}

	$links .= "&nbsp;&nbsp;&nbsp;";
	$links .= $q->a({-href=>"/cgi/admin/cbid_info.cgi", -class=>'q', -target=>'_blank'}, 'Individual CBID Search');

	unless ( $current{description}{value} =~ m/\*VOIDED\*/i ){
		$links .= "&nbsp;&nbsp;&nbsp;";
		$links .= $q->a({	-href=>'javascript:document.forms[0].submit()',
					-class=>'q',
					-onClick=>'Void_Order()'},
					'Void This Order');
	}

	return $links;
}

##### Runs the specified search, pushing the results onto a list (array).
sub Run_Search
{
	my $search_field = shift;
	my $search_value = shift || 'NULL';
	my $sth;

	if (grep { /=$search_field/ } @char_fields)
	{ 
		$sth = $db->prepare("SELECT * FROM $TBL WHERE $search_field ~* '$search_value'");
	}
	else
	{
		if ($search_field eq 'cbid_number')
		{
			$sth = $db->prepare("
				SELECT * FROM $TBL 
				WHERE $search_value BETWEEN cbid_start and cbid_end");
		}
		else
		{			
			$sth = $db->prepare("
				SELECT * FROM $TBL
				WHERE $search_field = $search_value");
		}
	}
	$sth->execute();
	
	while ( $data = $sth->fetchrow_hashref)
	{
		push (@result_list, $data);
	}		
}

sub Search_Form
{
###### print our main search form with any pertinent messages
	my $msg = shift || '';
	# Open the search template.
	my $tmpl = G_S::Get_Object($db, $TMPL{'search'}) ||
		die "Failed to load template: $TMPL{'search'}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$msg/;
	print $q->header(), $tmpl;	
}

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub validity_check
{
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'Invalid search value - Use only whole numbers with this field.') );
			return;	
		}
	}
	else
	{
		Search_Form( $q->span({-style=>'color: #ff0000'}, 'Invalid field specification - Try again.') );
		return;				
	}
	return 1;						
}

###### Void the order and reset all associated cards to unassigned status.
sub Void_Order
{
	my $qry = "
		UPDATE cbid_orders 
		SET	description = ${\$db->quote($current{description}{value})},
			operator = CURRENT_USER,
			stamp = NOW(),
			void=TRUE
		WHERE order_id = $current{order_id}{value};

		UPDATE clubbucks_master 
		SET	class = 1,
			referral_id = NULL,
			label = NULL,
			referral_clubbucks_id = NULL,
			operator = CURRENT_USER,
			stamp = NOW(),
			memo = 'Returned to inventory'
		WHERE id BETWEEN $current{cbid_start}{value} AND $current{cbid_end}{value}
		AND (class = 2 OR class = 3 OR class = 7)
		AND current_member_id IS NULL;";

	# Troubleshooting without query execution. Uncomment the four lines below.
	#Err("$qry<br><br>reason= $current{description}{value}<br>
	#			order_id= $current{order_id}{value}<br>
	#			cbid_start= $current{cbid_start}{value}<BR>
	#			cbid_end= $current{cbid_end}{value}"); return;

	$db->do($qry);
}

###### primarily changed the DBI stuff in the VOID_ORDER routine to eliminate
###### statement preparation with placeholders
# 07/01/11 added void=TRUE to the cbid_orders update in Void_Order
