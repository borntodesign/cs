#!/usr/bin/perl -w
##
## SubmitClubHouseXLS.cgi	- an interface for submitting a spreadsheet of ClubHouse attendees for entry into the activity records
##
## Deployed Aug. 2013	Bill MacArthur
##
## last modified: 

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;
use Spreadsheet::ParseExcel;

my $q = new CGI;
my $action = $q->param('_action');

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect('generic', $operator, $pwd);

$db->{'RaiseError'} = 0;	# we do not want to die in the middle of things... we will handle errors explicitly

unless ($action)
{
	XlsForm();
}
elsif ($action eq 'record')
{
	Record();
}
elsif ($action eq 'spreadsheet')	# we are submitting a spreadsheet
{
	# we must be searching for an ID
	Parse();
}
elsif ($action eq 'SubmitForm')
{
	SubmitForm();
}
else
{
	die "Unhandled action";
}

Exit();

###### start of subs

sub Err
{
	print $q->header('text/plain'), $_[0];
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub Nav
{
	return $q->p( $q->a({'-href'=>$q->url}, 'Start Over') . ' - ' . $q->a({'-href'=>$q->url . '?_action=SubmitForm'}, 'Individual Entry Form') );
}

sub Parse
{
	my $rv = ParseXLS();	# that is a hashref
	Err($rv->{'error'}) if $rv->{'error'};

	($rv->{'timestamp'}) = $db->selectrow_array('SELECT ?::TIMESTAMP', undef, $rv->{'webinar_date_time'});

	SubmitForm($rv);
}

sub ParseXLS
{
	# we should have a filehandle available to us from the submission
	my $fh = $q->upload('xls') || die "Failed to obtain the uploaded file";
	
	my $parser = Spreadsheet::ParseExcel->new();
	my $workbook = $parser->parse($fh);

	my $ws = $workbook->worksheet(0);
	my %rv = ();
	
	###### rows/cells start at -0-
	my ( $row_min, $row_max ) = $ws->row_range();
	if ($row_max < $row_min)
	{
		$rv{'error'} = "Either parsing failed to see any data or the sheet is empty";
		return \%rv;
	}
	
	my $cell = $ws->get_cell(4,1);
#	$rv{'webinar_date_time'} = $cell->unformatted(8,1);
	$rv{'webinar_date_time'} = $cell->unformatted();
	
#	$cell = $ws->get_cell(3,0);
#	warn $cell->unformatted();
	# remember we start at -0- so col_min should =0 and col_max will be 1 less than a visual inspection would indicate
	my ( $col_min, $col_max ) = $ws->col_range();
	if ($col_max < 8)
	{
		$col_max++;
		$rv{'error'} = "There seems to be a problem with the format of this document. We are expecting 13 columns, but we seem to have $col_max";
		return \%rv;
	}
	
	for (my $x=8; $x <= $row_max; $x++)
	{
		# first we want to verify that they actually were present
		$cell = $ws->get_cell($x, 5);
		my $present = $cell->unformatted;
		
		next unless $present;
		
		# now get the ID column
		$cell = $ws->get_cell($x, 7);
		
		# if there is no value in the cell, the call to ->unformated will fail... so let's test for that and try looking up the ID by the email address
		my $id = '';
		unless ($cell)
		{
			$cell = $ws->get_cell($x, 3);
			my $email = $cell->unformatted;
			($id) = $db->selectrow_array('SELECT id FROM members WHERE emailaddress= ?', undef, lc($email)) || '';
		}
		else
		{
			$id = $cell->unformatted;
			
			# get rid of leading and trailing whitespace
			$id =~ s/^\s*|\s*?//g;
		}

		next unless $id gt '-';
		
		# try to reduce what we are looking for to plain number IDs or aliases
		$id =~ m/^(\d+)|([a-z|A-Z]{2}\d+)$/;
		$id = $1 || $2;
		next unless $id;
		push @{$rv{'ids'}}, $id;
	}
	
	return \%rv;
}

sub Record
{
	my %params = ();
	my @vals = ();
	foreach (qw/webinar_timestamp/)
	{
		$params{$_} = $q->param($_) || die "$_ is a required field";
		push @vals, $db->quote($params{$_});
	}
	
	my @ids = $q->param('id');
	die "No IDs received" unless @ids;
	
	my $sth = $db->prepare('INSERT INTO activity_tracking (id, class, stamp) VALUES (?, 63,' . $db->quote($params{'webinar_timestamp'}) . ')');
	my $rv = '<table cellspacing="3" style="margin-top:1em">';
	my $rowclass = 'a';
	my %counts = (
		'success'=>0,
		'notfound'=>0,
		'errors'=>0
	);

	foreach(@ids)
	{
		next unless $_;

		
		$rv .= qq#<tr class="$rowclass"><td>$_</td><td>=> #;
		my $id = $_;
		($id) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, uc($_)) if $_ =~ m/\D/;
		
		unless ($id)
		{
			$rv .= "</td><td>Not Found</td></tr>";
			$counts{'notfound'}++;
			next;
		}
		
		$rv .= "$id</td><td>";
		
		my $i = $sth->execute($id);
		if ($db->err)
		{
			$rv .= $db->errstr;
			$counts{'errors'}++;
		}
		else
		{
			$rv .= 'Success';
			$counts{'success'}++;
		}
		
		$rv .= '</td></tr>';
	}
	$rv .= '</table>';

	print
		$q->header,
		$q->start_html(
			'-title' => 'ClubHouse XLS Import Results'
		),
		$q->h4('ClubHouse XLS Import Results'),
		Nav(),
		$q->table({'-style'=>'border: 1px solid navy'},
			$q->Tr( $q->td('Successful Entries:') . $q->td($counts{'success'}) ) .
			$q->Tr( $q->td('Not Found:') . $q->td($counts{'notfound'}) ) .
			$q->Tr( $q->td('Errors:') . $q->td($counts{'errors'}) )
		),
		$rv,
		$q->end_html;
}

sub SubmitForm
{
	my $rv = shift || {};

	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>'Parsing XLS',
			'-encoding'=>'utf-8',
			'-style'=>{'code'=>'th,td {text-align:left; vertical-align:top;}'}
			),
		$q->h4('Spreadsheet Import Results'),
		$q->p('If any fields are incorrect or empty, simply correct them.<br />If an ID should not be submitted, just delete the value.');
	
	print Nav();
	
	print
		$q->start_form('-method'=>'post'),
		$q->hidden('-name'=>'_action', '-value'=>'record', '-force'=>1),
		'<table id="mainTbl" cellspacing="5">',
		$q->Tr(
			$q->th('Extracted Date/Time') .
			$q->td($rv->{'webinar_date_time'} || '')
		),
		$q->Tr(
			$q->th('Timestamp to be entered into the DB<div style="font:8px normal sans-serif">A valid date/time should look like this: 2012-10-11 14:50:00</p>') .
			$q->td( $q->textfield('-name'=>'webinar_timestamp', '-value'=>$rv->{'timestamp'} || '') )
		),
		'<tr><th>ID/s to be submitted</th><td>';

	foreach (@{$rv->{'ids'}})
	{
		print $q->textfield('-name'=>'id', '-value'=>$_) , $q->br;
	}

	print $q->textfield('-name'=>'id', '-force'=>1),	# just a spare
		$q->br, $q->br,
		$q->submit,
		'</td></tr></table>',
		$q->end_form,
		$q->end_html;
}

sub XlsForm
{
	print
		$q->header,
		$q->start_html('-title'=>'Submit ClubHouse XLS'),
		$q->h4('Submit ClubHouse XLS'),
		$q->start_multipart_form,
		$q->hidden('-name'=>'_action', '-value'=>'spreadsheet'),
		'Select spreadsheet to upload: ',
		$q->filefield('-name'=>'xls'),
		$q->submit('-value'=>'Submit Spreadsheet', '-style'=>'margin-left:3em;'),
		$q->br, $q->br,
		$q->a({'-href'=>$q->url . '?_action=SubmitForm'}, 'Individual Entry Form'),
		$q->end_form.
		$q->end_html;
}
