#!/usr/bin/perl

######  msg_sub_sys_by_id0.cgi
######  11/19/2002	Stephen Martin
######  This utility displays the mesage queue for id = 0 (System Wide Messages)  

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use POSIX qw(locale_h);
use locale;
use Digest::MD5 qw(md5 md5_hex md5_base64);

setlocale( LC_CTYPE, "en_US.ISO8859-1" );

push ( @INC, '/home/httpd/cgi-lib' );

require '/home/httpd/cgi-lib/ADMIN_DB.lib';
require '/home/httpd/cgi-lib/G_S.lib';

###### Globals
my ( $db, $qry, $sth, $rv, $data, $HTML );
my ( @cookie, $TEXT );
my $q = new CGI;
unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
}
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd      = $q->cookie('pwd')      || $q->param('pwd');

my $self   = $q->url();
my $action = $q->param('action');
my $id     = 0;
my $marc   = $q->param('marc');
my $execA  = $q->param('execA');
my @R;
my $r_hash;
my $IMG;
my $TXTIMG;
my $ARCH_FLAG;
my $ARCREQ;
my $RECREQ;
my $moid;

my $results = "";

unless ( $db = &ADMIN_DB::DB_Connect( 'msg_sub_sys.cgi', $operator, $pwd ) ) {
    $db->{RaiseError} = 1;
    exit(1);
}

###### Catch the Archive Request

if ( $execA eq " ARCHIVE " )
{

 $moid = $q->param('moid');
 
 unless ( $moid ) 
 {
  &Err("Missing oid?? for archive request!");
  exit;
 }

 archive_msg($moid);

}

###### Catch the Recovery Request

if ( $execA eq " RECOVER " )
{

 $moid = $q->param('moid');

 unless ( $moid )
 {
  &Err("Missing oid?? for recovery request!");
  exit;
 }

 recover_msg($moid);
 xm_arc($moid);

}

###### Catch the Search Request 

if ( $action eq " Search " ) {

    ##### Generate the Result Set

    &gen_results($id);

    $results = $results . <<TH;
<td>&nbsp;</td>
<td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">HTML?</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Text?</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Create Date</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">View Date</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Expired Date</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Archive?</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Archive Date</font></td>
<td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Recover?</font></td>
TH

    if (@R) {
        foreach $r_hash (@R) {

            $r_hash->{msg_entered}  = substr( $r_hash->{msg_entered},  0, 10 );
            $r_hash->{msg_viewed}   = substr( $r_hash->{msg_viewed},   0, 10 );
            $r_hash->{msg_expires}  = substr( $r_hash->{msg_expires},  0, 10 );
            $r_hash->{msg_archived} = substr( $r_hash->{msg_archived}, 0, 10 );

            if ( $r_hash->{msg_archived} ) {
                $ARCH_FLAG = "1";
            }
            else {
                $ARCH_FLAG = "0";
            }


            if ( $ARCH_FLAG == "0" )
            {
             $ARCREQ = <<AR;
<input type="submit" name="execA" value=" ARCHIVE " class="tin">
AR
$RECREQ="";
            }
             else 
            {
             $ARCREQ = "";
$RECREQ = <<RR;
<input type="submit" name="execA" value=" RECOVER " class="bluein">
RR
            }
    
            if ( $r_hash->{msg_id} ) {
                $IMG = <<I1;
<A HREF="./msg_sub_sys_open_template.cgi?oid=$r_hash->{oid}&a=$ARCH_FLAG" 
   target="_open_"><IMG SRC="/images/msg_tick.gif" BORDER=0 ALT="Message is a template"></A>
I1
            }
            else {
                $IMG = "";
            }

            if ( $r_hash->{msg} ) {
                $TXTIMG = <<I2;
<A HREF="./msg_sub_sys_open_txtmsg.cgi?oid=$r_hash->{oid}&a=$ARCH_FLAG"
   target="_open_"><IMG SRC="/images/msg_tick.gif" BORDER=0 ALT="Message is textual"></A>
I2
            }
            else {
                $TXTIMG = "";
            }

            $results = $results . <<H
<tr>
<form method="POST" action="%%SELF%%">
 <input type="hidden" name="moid" value="$r_hash->{oid}">
 <input type="hidden" name="id" value="$id">
 <input type="hidden" name="marc" value="$marc">
 <input type="hidden" name="action" value=" Search ">
 <td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">$r_hash->{id}</font></td>
 <td>$IMG</td>
 <td>$TXTIMG</td>
 <td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">$r_hash->{msg_entered}</font></td>
 <td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">$r_hash->{msg_viewed}</font></td>
 <td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">$r_hash->{msg_expires}</font></td>
 <td nowrap>$ARCREQ</td>
 <td nowrap><font size="1" face="Verdana, Arial, Helvetica, sans-serif">$r_hash->{msg_archived}</font></td>
 <td nowrap>$RECREQ</td>
</form>
</tr>
H
        }
    }
    else {
        $results = <<E
<tr>
 <td colspan="7">
 <font size="1" face="Verdana, Arial, Helvetica, sans-serif">
  No Results Returned for <b>$id</b>.
 </font>
 </td>
</tr>
E
    }
}

###### Load the Default Template Template 

my $TMPL = &G_S::Get_Object( $db, 10058 );
unless ($TMPL) { &Err("Couldn't retrieve the template object."); exit; }

###### display Existing Vendors

$TMPL =~ s/%%RESULTS%%/$results/g;
$TMPL =~ s/%%ID%%/$id/g;
$TMPL =~ s/%%SELF%%/$self/g;

if ( $marc eq "_ARC_" ) {
    $TMPL =~ s/%%CHECK%%/checked/g;
}

print $q->header();

print $TMPL;

$db->disconnect;

1;

sub Err {
    my ($msg) = @_;
    print $q->header(), $q->start_html();
    print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
    print $q->end_html();
}

sub gen_results {
    my ($id) = @_;

    my ( $sth, $qry, $data, $rv );    ###### database variables

    $id =~ s/\D//g;

    $qry = "SELECT 
          messages.oid, 
          messages.id, 
          messages.msg_id, 
          messages.msg, 
          messages.msg_entered, 
          messages.msg_viewed, 
          messages.msg_expires,
          null as msg_archived
         FROM 
          messages
        WHERE messages.id = ? 
        UNION  
        SELECT
         messages_arc.oid, 
         messages_arc.id, 
         messages_arc.msg_id, 
         messages_arc.msg, 
         messages_arc.msg_entered, 
         messages_arc.msg_viewed, 
         messages_arc.msg_expires, 
         messages_arc.msg_archived
        FROM 
         messages_arc
        WHERE messages_arc.id = ?
        ORDER BY 5";

    if ( $marc ne "_ARC_" ) {
        ( $qry, undef ) = split ( /UNION/, $qry );
        $qry = $qry . " ORDER BY 5";
    }

    $sth = $db->prepare($qry);

    if ( $marc ne "_ARC_" ) {
        $rv = $sth->execute($id);
        defined $rv or die $sth->errstr;
    }
    else {
        $rv = $sth->execute( $id, $id );
        defined $rv or die $sth->errstr;
    }

    while ( $data = $sth->fetchrow_hashref ) {
        push @R, $data;
    }

    $rv = $sth->finish();

}

sub archive_msg
{
 my ($oid) = @_;

 my ( $sth, $qry, $data, $rv );    ###### database variables

 $qry = "DELETE 
          FROM
           messages 
          WHERE oid = ?";

 
 $sth = $db->prepare($qry);

 $rv = $sth->execute($oid);
  defined $rv or die $sth->errstr;

 $rv = $sth->finish();

}

sub xm_arc
{
 my ($oid) = @_;

 my ( $sth, $qry, $data, $rv );    ###### database variables

 $qry = "DELETE
          FROM
           messages_arc
          WHERE messages_arc.oid = ?";

  $sth = $db->prepare($qry);

 $rv = $sth->execute($oid);
  defined $rv or die $sth->errstr;

 $rv = $sth->finish();

}

sub recover_msg
{
 my ($oid) = @_;

 my ( $sth, $qry, $data, $rv );    ###### database variables

 $qry = "SELECT * 
          FROM 
           messages_arc 
          WHERE messages_arc.oid = ?";

 $sth = $db->prepare($qry);

 $rv = $sth->execute($oid);
  defined $rv or die $sth->errstr;

 $data = $sth->fetchrow_hashref();

 $rv = $sth->finish();

 ###### Data now contains the elements 

 ###### Now insert the elements back into messages

 $qry = "INSERT 
          INTO
           messages (id,msg_id,msg, msg_expires) VALUES ( ?,?,?,?)";

 $sth = $db->prepare($qry);

 $rv = $sth->execute($data->{id},$data->{msg_id},$data->{msg}, $data->{msg_expires});
  defined $rv or die $sth->errstr;

 $rv = $sth->finish();

}

