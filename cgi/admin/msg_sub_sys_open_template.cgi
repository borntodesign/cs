#!/usr/bin/perl

######  msg_sub_sys_open_template.cgi
######  11/20/2002	Stephen Martin
######  This utility displays a TEMPLATE assigned to a message 

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use POSIX qw(locale_h);
use locale;

setlocale( LC_CTYPE, "en_US.ISO8859-1" );

push ( @INC, '/home/httpd/cgi-lib' );

require '/home/httpd/cgi-lib/ADMIN_DB.lib';
require '/home/httpd/cgi-lib/G_S.lib';

###### Globals
my ( $db, $qry, $sth, $rv, $data);
my ( @cookie, $TEXT );
my $q = new CGI;
unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
}
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd      = $q->cookie('pwd')      || $q->param('pwd');

my $self   = $q->url();
my $action = $q->param('action');
my $oid     = $q->param('oid');
my $a   = $q->param('a');
my $msg_id;

unless ( $db = &ADMIN_DB::DB_Connect( 'msg_sub_sys.cgi', $operator, $pwd ) ) {
    $db->{RaiseError} = 1;
    exit(1);
}

unless( $oid )
{
 &Err("Missing oid?");
 exit;
}

$msg_id = obtain_message($oid,$a);

###### Load the Default Template Template 

my $TMPL = &G_S::Get_Object( $db, $msg_id );
unless ($TMPL) { &NotFoundErr($msg_id); exit; }

###### display Existing Vendors

print $q->header();

print $TMPL;

$db->disconnect;

1;

sub Err {
    my ($msg) = @_;
    print $q->header(), $q->start_html();
    print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
    print $q->end_html();
}

sub NotFoundErr
{
 my ($msg_id) = @_;

 my $X_TMPL = &G_S::Get_Object( $db, 10056 );
  unless ($X_TMPL) { &Err("Could not find Error Template for $0"); exit; }

 print $q->header();

 print $X_TMPL;
 
}

sub obtain_message
{
 my ($oid) = @_;
 shift;
 my ($a) = @_;

 my ( $sth, $qry, $data, $rv, $table );    ###### database variables

 if ( $a eq "1" )
 {
  $table = "messages_arc";
 }
  else 
 {
  $table = "messages";
 }


 $qry = "SELECT msg_id from $table where oid = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($oid);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data->{msg_id}

}




