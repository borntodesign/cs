#!/usr/bin/perl -w
###### assign-pnsid.cgi
###### manage Trial Partner line designations
# last modified 01/22/15	Bill MacArthur

use strict;

use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
require ADMIN_DB;
require cs_admin;

my ($errors) = ();
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my $ME = $q->script_name;
my $title = 'Assign a PNSID (Partner Network Separator ID)';

die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $rid = $q->param('rid') || '';
$rid =~ s/^\s*|\s*$//g;

my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $action = $q->param('action');
unless ($action)
{
	SearchForm();
}
elsif ($action eq 'search')
{
	DoForm();
}
else	# we must be performing an update
{
	Update();
}

Exit();

######

sub cboPnsIds
{
	my @vals = '';
	my %lbls = (''=>'');
	my $pnsids = $db->selectall_hashref("SELECT id, spid, company_name FROM members WHERE membertype='pns' AND company_name IS NOT NULL", 'id');
	foreach (sort {$a<=>$b} keys %{$pnsids})
	{
		push @vals, $pnsids->{$_}->{'id'};
		$lbls{ $pnsids->{$_}->{'id'} } = "$pnsids->{$_}->{'company_name'} - $pnsids->{$_}->{'id'}";
	}
	
	# create an entry to do a deletion
	push @vals, -1;
	$lbls{'-1'} = 'DELETE PNSID';
	return $q->popup_menu(
		'-name'=>'pnsid',
		'-values'=>\@vals,
		'-labels'=>\%lbls,
		'-default'=>$_[0],
		'-force'=>1
	);
}

sub DoForm
{
	die "Cannot proceed without rid parameter" unless $rid;
	my $m = $db->selectrow_hashref('SELECT id, alias, firstname, lastname, membertype FROM members WHERE alias=? OR id= ?', undef, uc($rid), $rid);
	unless ($m)
	{
		$errors = "Could not find a membership matching id: $rid";
		SearchForm();
	}
	elsif (! ($m->{'membertype'} eq 'tp' || $m->{'membertype'} eq 'v'))
	{
		$errors = "$m->{'alias'} cannot be placed into a Trial Partner line. Only Trial Partners and Partners can be placed. Their membertype: $m->{'membertype'}";
		SearchForm();
	}
	
	my ($pnsid) = $db->selectrow_hashref('
		SELECT
			pnsid,
			seq,
			enter_date::TIMESTAMP(0) AS enter_date,
			deadline::TIMESTAMP(0) AS deadline
		FROM od.pool WHERE id=?', undef, $rid);
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-encoding' => 'utf-8',
			'-title' => $title,
			'-style' => {'-code' => 'th {text-align:right;} caption {text-align:right; font-weight:bold;}'},
			'-script' => [
				{'-src'=>'/js/jquery-min.js', '-type'=>'text/javascript'},
				{'-src'=>'/js/admin/assign-pnsid.js', '-type'=>'text/javascript'}
			]
		);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	my $submitLbl = $pnsid->{'pnsid'} ? 'Update' : 'Set PNSID';
	print	
		$q->start_form('-method'=>'get'),
		$q->h4("Assign a PNSID (Trial Partner line identifier) for:"),
		$q->table(
			$q->Tr(
				$q->th('Name') . $q->td("$m->{'firstname'} $m->{'lastname'}")
			) .
			$q->Tr(
				$q->th('ID:') . $q->td($m->{'alias'})
			) .
			$q->Tr(
				$q->th('PNSID:') . $q->td( cboPnsIds($pnsid->{'pnsid'}) )
			) .
			$q->Tr(
				$q->th('Entered:') . $q->td( $pnsid->{'enter_date'} || '' )
			) .
			$q->Tr(
				$q->th('Deadline:') . $q->td( $pnsid->{'deadline'} || '' )
			) .
			$q->Tr(
				$q->th( $q->a({'-href'=>'#', '-title'=>'Unlock Sequence', '-onclick'=>'unlockSeq(); return false;', '-style'=>'text-decoration:none'}, 'Sequence:')) .
				$q->td(
					$q->textfield(
						'-id' => 'input_seq',
						'-name' => 'seq',
						'-value' => ($pnsid->{'seq'} || '' ),
						'-disabled' => 'disabled'
					)
				)
			) .
			$q->Tr(
				$q->th() . $q->td({'-style'=>'text-align:right'}, $q->submit($submitLbl) . $q->reset({'-style'=>'margin-left:1em'}) )
			) .
			$q->Tr(
				$q->th() . $q->td({'-style'=>'text-align:right; padding-top:2em;'}, $q->a({'-href'=>$q->url}, 'Start Over') )
			)
		),
		
		$q->hidden('-value'=>$m->{'id'}, '-name'=>'rid'),
		$q->hidden('-value'=>'update', '-name'=>'action', '-force'=>1),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html($title);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->h4("Member Search - $title"),
		$q->start_form('-method'=>'get'),
		$q->textfield('-value'=>'', '-name'=>'rid'),
		' '.
		$q->submit('Search'),
		$q->hidden('-value'=>'search', '-name'=>'action'),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub Update
{
	die "Cannot proceed without rid parameter" unless $rid;
	my $pns = ();
	my $tmp = $q->param('pnsid');
	my $seq = $q->param('seq');
	die 'Sequence must be a simple numeric value, no letters or spaces.' if ($seq && $seq =~ m/\D/);
	
	if ($tmp)
	{
		if ($tmp > 0)	# a value of -1 means that we want to DELETE the pnsid for the party
		{
			$pns = $db->selectrow_hashref('SELECT id, alias, membertype FROM members WHERE id=?', undef, $tmp);
			unless ($pns)
			{
				$errors = "Cannot find a membership matching $tmp";
				DoForm();
			}
			elsif ($pns->{'membertype'} ne 'pns')
			{
				$errors = "Membership: $pns->{'alias'} is not a PNS: $pns->{'membertype'}";
				DoForm();
			}
		}
	}
	else
	{
		$errors = "No pnsid received. Don't know what to do.";
		DoForm();
	}

	# am I currently in a TP line? if so then update to preserve the dates, otherwise create a new record
	my ($pnsid) = $db->selectrow_array("SELECT pnsid FROM od.pool WHERE id=?", undef, $rid);
	
	$db->begin_work;
	if ($pnsid)
	{
		if ($tmp == -1)	# we are deleting their record
		{
			$db->do("DELETE FROM od.pool WHERE id= ?", undef, $rid);
		}
		else
		{
			if ($seq)
			{
				$db->do("UPDATE od.pool SET pnsid= $pns->{'id'}, seq= $seq WHERE id= ?", undef, $rid);
			}
			else
			{
				$db->do("UPDATE od.pool SET pnsid= $pns->{'id'} WHERE id= ?", undef, $rid);
			}
		}
	}
	else
	{
		if ($tmp > 0)
		{
			if ($seq)
			{
				$db->do(qq#INSERT INTO od.pool (id, pnsid, seq) VALUES (?, $pns->{'id'}, $seq)#, undef, $rid);
			}
			else
			{
				$db->do(qq#INSERT INTO od.pool (id, pnsid) VALUES (?, $pns->{'id'})#, undef, $rid);
			}
		}
	}
	$db->commit;
	DoForm();
}

__END__

01/22/15	Add display and handling for the od.pool.seq