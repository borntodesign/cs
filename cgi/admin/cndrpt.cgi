#!/usr/bin/perl -w
###### cndrpt.cgi
###### reporting on email campaign delivery stats
###### created: 08/09/06	Bill MacArthur
###### Last modified:

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $q = new CGI;
my $action = $q->param('action') || '';
my $cn = $q->param('cn');
my (@th) = ();

###### here is where we get our user and try logging into the DB
my $operator = $q->cookie('operator') || '';
my $pwd = $q->cookie('pwd') || '';
unless ($operator && $pwd)
{
	Err("DB login required for report access.<br />Received: $operator : $pwd");
	exit;
}
my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd ) || exit;
$db->{RaiseError} = 1;

my $qry = '';
my $title = 'Email Campaign Delivery Reporting';
print $q->header, $q->start_html(
	-encoding=> 'utf-8',
	-title=> $title,
	-style=> {-code=>styles()}
	), $q->h4($title);

unless ($action)
{
	$qry = '
		SELECT cn, COUNT(DISTINCT id) AS "count"
		FROM mail_tracking GROUP BY cn ORDER BY cn';
	@th = ('Campaign', 'Count');
}
elsif ($action eq 'cn_by_date')
{
	$qry = "
		SELECT cn, COUNT(DISTINCT id) AS \"count\", stamp::DATE as click_day
		FROM mail_tracking
		WHERE cn= $cn
		GROUP BY cn, stamp::DATE ORDER BY stamp::DATE";
	@th = ('Campaign', 'Count', 'Date');
}
else
{
	Err('Invalid action');
	goto 'END';
}
print "<table>\n";
print $q->Tr($q->th(\@th));
my $sth = $db->prepare($qry);
$sth->execute();
my $class = 'a';
while (my @row = $sth->fetchrow_array)
{
	print "<tr class=\"$class\">\n";
	$class = $class eq 'a' ? 'b' : 'a';
	my $cn = shift @row;
	print "<td>";
	print ((! $action) ? $q->a({-href=>$q->script_name . "?action=cn_by_date;cn=$cn"}, $cn) : $cn);
	print "</td>";
# 	foreach (@row)
# 	{
# 		print $q->td($_);
# 	}
	print $q->td(\@row);
	print "</tr>\n";
}
print "</table>", $q->end_html;

END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub styles
{
	return <<END
body {background-color: #fff}
table{border-collapse:collapse}
th{ padding-right:0.2em; padding-left:1em;}
td{border:1px solid silver; text-align:right; padding:0.2em;}
tr.a {background-color: #ffe}
tr.b {background-color: #eee}
END
}
