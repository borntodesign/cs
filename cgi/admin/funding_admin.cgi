#!/usr/bin/perl -w
###### funding_admin.cgi
######
###### A script that allows admins to place funds into a VIPs pre-pay account. 
###### (soa table) which will be used later to pay for other VIPs' fees, etc. 
######
###### created: 11/08/05	Karl Kohrt
######
###### modified: 01/09/07	Shane Partain
	
use strict;
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
use G_S qw ($LOGIN_URL);
use CGI;
require XML::Simple;
require XML::local_utils;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser);
use State_List;
use Business::CreditCard;
use MailingUtils;
use Mail::Sendmail;

###### CONSTANTS
our $MALL_TRANS_TYPES = ' AND trans_type BETWEEN 4000 AND 4999 ';
our $FIRST_MENU_YEAR = 2012;
our $MAX_MENU_YEAR = $FIRST_MENU_YEAR + 5;	
our @numerics = qw/id quantity price order_type/;
our $TRACKING = 1;			# Change to 0 for no logging.
our $PATH = "/var/log";	# Where the logs are kept.
our $ACCOUNTING_ADDRESS = "acfspvr\@dhs-club.com";
our $ADMIN_ADDRESS = "webmaster\@dhs-club.com";
our $cgiini = "funding_admin.cgi";
our %TMPL = (	'fund'		=> 10504,	# The funding form.
		'report'	=> 10509,	# The reporting interface.
		'email'	=> 10505	# The email to the depositing VIP.
		);
# Today's date in US date format (mm/dd/yyyy).
my ($day, $mon, $year) = (localtime)[3,4,5];
$mon++; $year += 1900;
if ( length($day) == 1 ) { $day = '0' . $day }  # Pad a single digit day.
if ( length($mon) == 1 ) { $mon = '0' . $mon }  # Pad a single digit month.
our $today = "$mon/$day/$year";

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $mu = new MailingUtils;			
our $ME = $q->script_name;
our $db = '';
our $mem_info = '';
our $login_error = 0;
our $messages = '';
our $trans_list = '';
our $summary = '';
our $next_action = '';
our $params_hash = ();
our $page = '';
our ($start, $stop) = '';

################################
###### MAIN starts here.
################################

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'funding_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Get the transaction codes and descriptions.
our $TYPES = $db->selectall_hashref("	SELECT *
						FROM 	soa_transaction_types", 'id', undef);						

# Load the params.
Load_Params();

# If this is the first time through, just print the Search page.
unless ( $params_hash->{action} )
{
	# Do nothing except display the form below.
	$next_action = "confirm";
	$page = 'fund';
}
elsif ($params_hash->{action} eq 'confirm')
{
	# Get the submitted member's info.
	if ( $mem_info = Get_Member() )
	{
		$params_hash->{id} = $mem_info->{id};
		$next_action = "deposit";
	}
	else
	{
		$messages = qq!<span class="bad">
				The member # $params_hash->{id} was not be found or is not a VIP.</span>!;
		$next_action = "confirm";
	}
	$page = 'fund';
}
elsif ($params_hash->{action} eq 'deposit')
{
	# Place the funds in the table.
	if ( Insert_Funds() )
	{
		$messages = qq#<span class="good">
				The funds were deposited successfully.
				</span>#;
		Email_VIP();
	}
	else
	{
		$messages = qq#<span class="bad">
				The funds deposit failed.</span>#;		
	}
	$next_action = "confirm";
	$page = 'fund';
}
elsif ($params_hash->{action} eq 'report')
{
	# Get the submitted member's info.
	if ( ($trans_list, $summary) = Get_Transactions() )
	{
	}
	else
	{
		$messages = qq!<span class="instructions">
				There are no transactions for the requested period.</span>!;
	}
	$page = 'report';
}
elsif ($params_hash->{action} eq 'reportbyid')
{
	my $rv = $db->selectall_arrayref('
		SELECT id, SUM(amount) AS balance
		FROM soa
		WHERE void=FALSE
		GROUP BY id HAVING SUM(amount) <> 0
		ORDER BY balance DESC; 
	');
	print $q->header('text/plain');
	foreach(@{$rv}){
		print "$_->[0]\t $_->[1]\n";
	}
	End();
}
else { Err('Undefined action.') }

# Now build out the page.
Display_Page();

End();

################################
###### Subroutines start here.
################################

sub End
{
	$db->disconnect if $db;
	exit;
}

sub BuildNewURL
{
	my $arg = shift;
	my $url = $q->self_url;
	if ($url =~ /show-details=/)
	{
		$url =~ s/show-details=\d*/show-details=$arg/;
	}
	else
	{
		$url .= ";show-details=$arg";
	}
	return $url;
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	# Get the template.
	my $template = $gs->Get_Object($db, $TMPL{$page}) ||
			die "Failed to load the template: $TMPL{$page}\n";

	$template =~ s/%%ME%%/$ME/g;
	if ( $messages && $params_hash->{action} ne 'report' )
	{
		$messages = qq#<tr><td colspan="3" align="center">
				<hr width="30%"/>$messages<hr width="30%"/>
				</td></tr>#;
	}

	# it's very much of a waste, but rather than hacking the actual data generation part
	# we'll simply change our list to a simple option that will be shown unless they *really* want the whole enchilada
	unless ($q->param('show-details')){

		$trans_list = $q->Tr(
			$q->td({-colspan=>7, style=>'background-color:yellow;'},
				$q->a({-href=>BuildNewURL(1)}, 'Show Transaction Details')
			)
		);
	}
	else
	{
		# create a hide details link
		$trans_list = $q->Tr(
			$q->td({-colspan=>7, style=>'background-color:#99ff66;'},
				$q->a({-href=>BuildNewURL(0)}, 'Hide Transaction Details')
			)
		) . $trans_list;
	}
	$template =~ s/%%messages%%/$messages/g;
	$template =~ s/%%trans_list%%/$trans_list/;
	$template =~ s/%%summary_data%%/$summary/;

	if ( $page eq 'fund' )
	{
		$template =~ s/%%next_action%%/$next_action/g;
		if ( ! $next_action || $next_action eq 'confirm' )
		{
			$template =~ s/conditional-deposit-start.+?conditional-deposit-stop//sg;
		}
		elsif ( $next_action eq 'deposit' )
		{
			$template =~ s/conditional-confirm-start.+?conditional-confirm-stop//sg;
			$template =~ s/%%id%%/$params_hash->{id}/g;
			$template =~ s/%%type%%/$params_hash->{type}/g;
			$template =~ s/%%amount%%/$params_hash->{amount}/g;
			$template =~ s/%%firstname%%/$mem_info->{firstname}/g;
			$template =~ s/%%lastname%%/$mem_info->{lastname}/g;
			$template =~ s/%%alias%%/$mem_info->{alias}/g;
			$template =~ s/%%emailaddress%%/$mem_info->{emailaddress}/g;
			$template =~ s/%%language_pref%%/$mem_info->{language_pref}/g;
			$template =~ s/%%type_description%%/$TYPES->{$params_hash->{type}}{name}/g;
			$template =~ s/%%othertext%%/$params_hash->{othertext}/g;
		}
	}
	elsif ( $page eq 'report' )
	{
		# Do whatever is unique to the report.
		$template =~ s/%%today%%/$today/g;
		$template =~ s/%%start_date%%/$start/g;
		$template =~ s/%%stop_date%%/$stop/g;
		$template =~ s/%%show-details%%/($q->param('show-details')?1:0)/e;
	}
	print $q->header(-charset=>'utf-8'), $template;
}

###### Generate e-mail to the depositing VIP.
sub Email_VIP
{
	my ($to_tmp, $from_tmp, $cc, $bcc, $subject) = '';
	# For troubleshooting email.
#	$bcc =$ADMIN_ADDRESS;

	# Get the base email template(s).
	my $pc = $mu->Get_Object($db, $TMPL{email});

	# Select the template based upon the language preference.
	my $MSG = $mu->Select_Translation($pc, $params_hash->{language_pref});

	###### remove any comment fields
	$MSG =~ s/(^|\n)(#.*)//g;
	###### remove line feeds (these can be caused by windows pasting)
	$MSG =~ s/\r//g;

	unless ($MSG){ die "Couldn't load Email Template: $TMPL{email}\n" }

	$MSG =~ s/%%bcc_email%%/$bcc/;

	# The transaction data.
	$MSG =~ s/%%trans_id%%/$params_hash->{trans_id}/;
	$MSG =~ s/%%trans_amount%%/$params_hash->{amount}/;
	my $trans_desc = $TYPES->{$params_hash->{type}}{name};
	# If this is the Other-Accounting Action choice.
	if ( $params_hash->{type} == 1200 )
	{
		# Tack on the text entered by the user.
		$trans_desc .= " - " . $params_hash->{othertext};
	}
	# If this is a moneybooker deposit.
	if ( $params_hash->{type} == 10000 )
	{
		# Blank out the placeholders that mark the moneybooker paragraph.
		$MSG =~ s/%%conditional-moneybookers-start%%/\n/g;
		$MSG =~ s/%%conditional-moneybookers-stop%%/\n/g;
	}
	else
	{
		# Blank out the new moneybooker form paragraph.
		$MSG =~ s/%%conditional-moneybookers-start.+?conditional-moneybookers-stop%%//sg;
	}
	$MSG =~ s/%%trans_desc%%/$trans_desc/;

	# These are our recipient member's info.
	$MSG =~ s/%%to_alias%%/$params_hash->{alias}/g;
	$MSG =~ s/%%to_firstname%%/$params_hash->{firstname}/g;
	$MSG =~ s/%%to_lastname%%/$params_hash->{lastname}/g;
# This next line's email entry is for testing only.
#	$MSG =~ s/%%to_email%%/$ADMIN_ADDRESS/g;
	$MSG =~ s/%%to_email%%/$params_hash->{emailaddress}/g;

	# extract our 'To' line from the template
	$MSG =~ s/(^|\n)(To:.*)//i;
	($to_tmp = $2) =~ s/^To:\s*//i;
	# extract our 'From'
	$MSG =~ s/(^|\n)(From:.*)//i;
	($from_tmp = $2) =~ s/^From:\s*//i;
	# extract our 'Subject'
	$MSG =~ s/(^|\n)(Subject:.*)//i;
	($subject = $2) =~ s/^Subject:\s*//i;
	# extract our 'Bcc'
	$MSG =~ s/(^|\n)(Bcc:.*)//i;
	($bcc = $2) =~ s/^Bcc:\s*//i if $2;
	# extract our 'Cc'
	$MSG =~ s/(^|\n)(Cc:.*)//i;
	($cc = $2) =~ s/^Cc:\s*//i if $2;

	# remove all remaining leading spaces and newlines
	$MSG =~ s/^(\s|\n)*//;

	# perform AOL link creation if we are mailing to an AOL address
	if ($to_tmp =~ m/\@aol\.com/){
		$MSG =~ s#(http\S*)#<a href="$1">$1</a>#g;
	}
		
	my %mail = (	To 		=> $to_tmp,
			Bcc	 	=> $bcc,
			Subject 	=> $subject,
			From 		=> $from_tmp,
			'Content-Type'=> 'text/plain; charset="utf-8"',
                	Message 	=> $MSG);

#	$mail{Bcc} = $bcc if $bcc;
#	&debug(\%mail);
	unless ( sendmail(%mail) )
	{
		$messages = "An attempt to email $params_hash->{emailaddress} failed for the following reason - $Mail::Sendmail::error\n";
	}
	return 1;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}

###### Get the member's details.
sub Get_Member
{
	my $where_clause = '';
	# If the ID passed is an alias, get the actual ID.
	if ( $params_hash->{id} && $params_hash->{id} =~ m/\D/ )
	{
		$where_clause = 'alias =';
	}
	else { $where_clause = 'id =' }
	my $info = $db->selectrow_hashref("SELECT *
						FROM 	members_cancel_pending
						WHERE 	$where_clause ?
						AND 	membertype = 'v'",
						undef, $params_hash->{id});
	# utf8'ize the names.
#	$gs->Prepare_UTF8($info);
	return $info;
}

###### Get the transactions for the specified timeframe.
sub Get_Transactions
{
	my $start_day = $q->param('period') || 'oneday';
	my $trans_data = '';
	my %totals = ();

	# If a range or special date was requested, 
	#  build out the start and stop dates from the passed params.
	if ( $start_day eq 'range' )
	{
		$start = "'" . $q->param('start_year') . "-" . $q->param('start_mon') . "-" . $q->param('start_day') . "'";
		# Set as the first of the month, letting the SQL figure out the last day of the month.
		$stop = "'" . $q->param('stop_year') . "-" . $q->param('stop_mon') . "-" . $q->param('stop_day') . "'";
	}
	# Otherwise, we'll just subtract the interval from NOW().
	else
	{
		my %DAYS = (	'zeroday'	=> '0 days',
				'oneday'	=> '1 day',
				'twoday'	=> '2 days',
				'threeday'	=> '3 days' );
		$start = $stop = qq#(NOW() - INTERVAL '$DAYS{$start_day}')::DATE#;
	}
	# Add mall transaction sql if that is what was requested.
	my $mall_only_sql = '';
	if ( $q->param('mall') == 1 )
	{
		$mall_only_sql = $MALL_TRANS_TYPES;
		$messages = qq#<span class="good">
				Reporting Mall Transactions Only
				</span>#;
	}

	# Get the transactions.
	my $transactions = $db->selectall_hashref("
		SELECT id,
			trans_id,
			trans_type,
			amount,
			description,
			entered::DATE,
			void,
			$start AS start_date,
			$stop AS stop_date
		FROM 	soa
		WHERE 	entered::DATE BETWEEN $start AND $stop
		$mall_only_sql
		ORDER BY trans_id", 'trans_id', undef) || '';
	foreach ( sort keys %{$transactions} )
	{
		my $void_string = ($transactions->{$_}{void}) ? "<b>VOID</b> - " : '';
		$trans_data .= qq#	<tr>
					<td>$transactions->{$_}{entered}</td>
					<td align="center">$transactions->{$_}{trans_id}</td>
					<td align="left">
					<a href="/cgi/funding.cgi?action=report;admin=1;id=$transactions->{$_}{id}" target="_blank">
						$transactions->{$_}{id}</a></td>
					<td>$TYPES->{$transactions->{$_}{trans_type}}{name}</td>
					<td>$void_string$transactions->{$_}{description}</td>
					<td align="right">$transactions->{$_}{amount}</td>
					</tr>#;
		$totals{$TYPES->{$transactions->{$_}{trans_type}}{name}} += $transactions->{$_}{amount};
	#	$totals{$transactions->{$_}{trans_type}} += $transactions->{$_}{amount};
	}
	# Get the actual start and stop dates if they are calculated from an interval.
	if ( $start =~ m/INTERVAL/ )
	{
		($start, $stop) = $db->selectrow_array("
			SELECT $start AS start,
				$stop AS stop
			FROM 	soa
			LIMIT 1");
	}
	# Otherwise, strip the quotes from the dates.
	else { $start =~ s/\'//g; $stop =~ s/\'//g; }

	# Create the totals report section.
	my $summary_data = qq#<tr><td colspan="5">
				<table border="0"><tr>
				<th>Category</th><th>Total in \$US</th></tr>#;
	my $summary_total = 0.00;
	foreach ( sort keys %totals )
	{
		my $pretty = sprintf "%.2f", $totals{$_};
		$summary_total += $totals{$_};
		$summary_data .= qq#<tr><td class="data">$_</td>
					<td class="amount">$pretty</td></tr>#;
	}
	$summary_total = sprintf "%.2f", $summary_total;
	$summary_data .= qq#<tr><td class="total">Total:</td>
				<td class="total">$summary_total</td></tr>#;
	$summary_data .= qq#</table></td></tr>#;
	return $trans_data, $summary_data;
}

###### Insert funds into their pre-pay account - soa table and return the transaction number.
sub Insert_Funds
{
	my $description = '';
	if ( $params_hash->{type} == 1200 )	# The Other-Accounting Action choice.
	{
		$description = $params_hash->{othertext};
	}
	else
	{
		$description = 'Pre-Pay Funding (admin)';
	}

	# Build the query if we can get a transaction number from the soa table.
	if ( my $trans_num = $db->selectrow_array("SELECT nextval('soa_trans_id_seq');") )
	{
		my $qry = "INSERT INTO soa ( trans_id,
						id,
						trans_type,
						amount,
						description,
						operator )
				VALUES ( ?, ?, ?, ?, ?, ? )";
		my $sth = $db->prepare($qry);
		my $rv = $sth->execute(	$trans_num,
						$params_hash->{id},
						$params_hash->{type},
						$params_hash->{amount},
						$description,
						$cgiini);
		if ( $rv )
		{
			$params_hash->{trans_id} = $trans_num;
			return 1;
		}
		else { return 0; }
	}
	else { return 0; }
}

###### Load the params into a local hash.
sub Load_Params
{
	# Get the submitted parameters.
	foreach ( $q->param() )
	{
		$params_hash->{$_} = $q->param($_) || '';
	}
	$params_hash->{amount} = sprintf "%.2f", $params_hash->{amount} if $params_hash->{amount};
	$params_hash->{id} = uc($params_hash->{id}) if $params_hash->{id};
}


###### 12/06/05 Added the Other selection to Email_VIP, Display_Page, and Insert_Funds.
###### 12/22/05 Added the moneybookers conditional paragraph to the email.
###### 01/25/06 Commented out utf8 conversion references
###### 03/16/06 Added the Today and Mall Transactions selections.
###### 04/10/06 Added a "VOID" label to voided transactions in the $trans_list.
###### 01/09/07 Added Type column to report.

