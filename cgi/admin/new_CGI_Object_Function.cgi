#!/usr/bin/perl
# new_CGI_Object_Function.cgi
# This utility will create a new CGI Object Function
#
# Written by Stephen Martin
# August 29th  2002
#

=head1 new_CGI_Object_Function.cgi

=for Commentary

new_CGI_Object.cgi
This utility will create a new CGI Object Function

Written by Stephen Martin
August 29th  2002

=cut

use strict;
use DBI;
use CGI qw(:cgi);
use Carp;
use POSIX qw(locale_h);
use locale;
use URI::Escape;

setlocale(LC_CTYPE, "en_US.ISO8859-1");

# use HTML::Template;

require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data);    ###### database variables
my $rv;
my $operator;
my $query;
my $public;
my $private;
my $spawn;

my $cgi = new CGI();

my $action          = $cgi->param('action');
my $id;
my $process_name    = $cgi->param('process_name');
my $process_type    = $cgi->param('process_type');
my $process_detail  = $cgi->param('process_detail');
my $active          = $cgi->param('active');

my $self = "./new_CGI_Object_Function.cgi";

# Here is where we get our admin user and try logging into the DB

$operator = $cgi->cookie('operator'); my $pwd = $cgi->cookie('pwd');
unless ($db = &ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Catch the add request

if ( $action eq "Add this Function")
{

        # Find a new id to use as an insert!

        $query = "select coalesce(max(id),0) + 1 as id from cgi_object_functions";

        $sth = $db->prepare($query);

        $rv = $sth->execute();
        defined $rv or die $sth->errstr;

        $sth->bind_columns(undef, \$id);

        $sth->fetch();

	$rv = $sth->finish();

        $query  = "INSERT INTO cgi_object_functions (
                   id,
                   process_name,
                   process_type,
                   process_detail,
                   active ) VALUES (
                   ?,?,?,?,?)";

	$sth = $db->prepare($query);

	$rv = $sth->execute($id,
                            $process_name,
                            $process_type,
                            $process_detail,
                            $active);
	defined $rv or die $sth->errstr;

        $rv = $sth->finish();

        $db->disconnect;

        # redirect to Template management system print $cgi->header();

        print $cgi->header();

        print qq~
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
        <html>
        <head>
        <META HTTP-EQUIV="pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00">
        <META HTTP-EQUIV="Expires" CONTENT="NOW">
        <META HTTP-EQUIV="last modified" CONTENT="NOW">

         <LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
         <title>DHSC CGI Insert a new CGI Object Function Success!!</title>
         </head>
         <body bgcolor="#eeffee">
          <script language="JavaScript">
           if (confirm("Do you wish to add another CGI Object Function?")) 
           {
            location.href = "https://www.clubshop.com/cgi/admin/new_CGI_Object_Function.cgi";
           } else {
            window.close();
           }
          </script>
           <noscript>
             <center>Your record ID=$id was inserted successfully!</center>
           </noscript>
         </body>
         </html>
        
        \n~;

exit;
}

print $cgi->header();

print qq~

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC CGI Insert a new CGI Object Function</title>
<script language="JavaScript">
 function validate()
 {
  if ( document.vform1.process_name.value == "" )
  {
   alert('Please enter a process name');
   document.vform1.process_name.focus();
   return false;
  }
  
  if ( document.vform1.process_type.value == "" )
  {
   alert('Please enter a process type');
   document.vform1.process_type.focus();
   return false;
  }

  if ( document.vform1.process_detail.value == "" )
  {
   alert('Please enter some information regarding this process');
   document.vform1.process_detail.focus();
   return false;
  }

  return true;
 }
</script>
</head>

<body bgcolor="#eeffee">
<form action="$self" method="POST" name="vform1"
 onSubmit="return validate();">

<center>
<b>DHSC CGI Insert a new CGI Object Function</b><br><br>
</center>
<table border="0" width="635" align="center">
  <tr>
  <td align="center" colspan="4" height="10">
  &nbsp;
  </td>
 </tr>
 <tr>
  <td align="center" colspan="4">
   <hr noshade align="center">
  </td>
 </tr>
</table>
<table border="0" width="635" align="center">
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Process Name <font size="1">(for example someapp.cgi )</font></u>
          </td>
          </tr>

          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
           <input type="text" name="process_name" size="40" maxsize="64"
           style="width : 620px;">
          </td>
          </tr>
        
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Process Type</u>
          </td>
          </tr>

          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
          <input type="text" name="process_type" size="40" maxsize="64"
           style="width : 620px;">
          </td>
          </tr>

         <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Process Detail</u>
         </td>
         </tr>

         <tr>
          <td colspan="4" align="center"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
           <textarea name="process_detail" style="width : 620px;"
                        cols="50" rows="5"></textarea>
          </td>
          </tr>
                  <tr>
          <td colspan="2"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Active?</u>         
          </td>
         <td colspan="2"
          style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
         </td>
         </tr>
         
         <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
          <select name="active" size="1">
           <option selected value="true">YES</option>
           <option value="false">NO</option>          
          </select>
          </td>
          </tr>
          <tr>
          <td colspan="4">
           <hr noshade align="center">
         </td>
         </tr>
        <tr>
   <td colspan="4" align="right">
    <input type="submit" class="in" value="Add this Function" name="action">&nbsp;&nbsp;&nbsp;
    <input type="button" class="in" value="Cancel"
     onClick="window.close();">
   </td>
  </tr>

 <tr>
    <td width="615" colspan="4"></td>
  </tr>
</table>
</form>
</body>
</html>
\n~;



1;

