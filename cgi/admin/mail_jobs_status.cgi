#!/usr/bin/perl
###### mail_jobs_status.cgi
###### This utility displays a list of the currently processed and 
######   running mail jobs, allows you to view details of a 
######   selected job, and drop a selected job table from the database.
###### Created by Stephen Martin 12/12/2002
###### last modified: 10/12/06	Bill MacArthur
######                02/09/2011 g.bajer  postgres 9.0 port SUBSTR arg 1
######                                    must be text

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
use ADMIN_DB;

$| = 1;

###### Globals

my $q = new CGI;
my $self   = $q->url();

my $sth;
my $rv;
my $qry;
my $data;

my $TMPL;
my $TABLE;
my @JOB_LIST;
my @TABLE;

###### here is where we get our user and try logging into the DB

my $operator = $q->cookie('operator');
my $pwd      = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect( 'mail_jobs_status.cgi', $operator, $pwd );
$db->{RaiseError} = 1;

###### If we are still here we must be good.

###### Find out what action we will be doing
my $action   = $q->param("action");
my $job_num  = $q->param("job_number");

unless ($action)
{
	###### Print the default template showing all of the mail jobs.

	$TMPL = G_S::Get_Object( $db, 10091 );
	unless ($TMPL) { Err("Couldn't retrieve the template object."); exit; }

	obtain_job_list();

	foreach my $job_hash (@JOB_LIST) 
	{
	 display_stats( $job_hash); 
	}

	print $q->header();

	$TMPL =~ s/%%SELF%%/$self/g;
	$TMPL =~ s/%%TABLE%%/@TABLE/g;

	print $TMPL;
}

###### Display the details of a selected job_number.
elsif ($action eq 'details')
{
	$TMPL = G_S::Get_Object( $db, 10139 );
	unless ($TMPL)
	{
		Err("Couldn't retrieve the template object.");
		exit;
	}

	display_details($job_num);

	print $q->header();

	$TMPL =~ s/%%TABLE%%/@TABLE/g;

	print $TMPL;
}

###### Delete the selected job_number's table.
elsif ($action eq 'delete')
{
	# Delete the Job table.
	delete_job_table($job_num);

	# Print the remaining jobs to the browser. Same as the unless portion of this statement.
	$TMPL = G_S::Get_Object( $db, 10091 );
	unless ($TMPL) { &Err("Couldn't retrieve the template object."); exit; }

	obtain_job_list();

	foreach my $job_hash (@JOB_LIST) 
	{
	 display_stats( $job_hash); 
	}

	print $q->header();

	$TMPL =~ s/%%SELF%%/$self/g;
	$TMPL =~ s/%%TABLE%%/@TABLE/g;

	print $TMPL;
}

###### Error if an action was selected that is not available.
else
{
	Err("The requested action, <B>$action</B>, is not allowed.");
}

$db->disconnect;

exit;



###############################################
#####
##### Display an error message.
#####
sub Err {
    print $q->header(), $q->start_html();
    print $_[0];
    print "<br />\n";
    print $q->end_html();
}

###############################################
#####
##### Collect all the 'z' tablenames from the system tables.
#####
sub obtain_job_list
{
 my ( $sth, $qry, $data, $rv );    ###### database variables

 $qry = "SELECT tablename
         FROM pg_tables
         WHERE tablename ~  'z[0-9]+\$'
	ORDER BY SUBSTR(tablename,2)::INT";

  $sth = $db->prepare($qry);

  $rv = $sth->execute;
   defined $rv or die $sth->errstr;

  while ( $data = $sth->fetchrow_hashref  )
  {
   push @JOB_LIST,$data;
  }

  $rv = $sth->finish();
}

###############################################
#####
##### Display the details of all mail jobs with a 'z' table in the database.
#####
sub display_stats
{
 my ($job_hash) = @_;

 my ( $sth, $qry, $data, $rv ,$html, $tablename);    ###### database variables

 $tablename = $job_hash->{tablename};
 $tablename =~ s/\D//g;

 $qry = "	SELECT owner,
			job_number,
			total_records,
			DATE_TRUNC('minutes', job_begun) AS job_begun,
			DATE_TRUNC('minutes', job_finished) AS job_finished,
			DATE_TRUNC('minutes', COALESCE ( job_finished - job_begun, NOW() - job_begun )) AS run_time,
			( COALESCE(job_finished - job_begun, NOW() - job_begun ) ) / total_records AS time_per_record,
			CASE
				WHEN job_finished >= job_begun THEN 'Completed'
				ELSE NULL
			END AS job_complete
		FROM 	mail_jobs
		WHERE 	job_number = ?";
 

$sth = $db->prepare($qry);

  $rv = $sth->execute($tablename);
   defined $rv or die $sth->errstr;

  while ( $data = $sth->fetchrow_hashref  )
  {

   # Provide defaults for fields with no data and/or highlight certain data.
   $data->{job_begun} = $data->{job_begun} || "Not Started";
   $data->{job_finished} = $data->{job_finished} || "\&nbsp;";
   $data->{run_time} = $data->{run_time}  || "\&nbsp;";
   $data->{time_per_record} = $data->{time_per_record}  || "\&nbsp;";

   if ( ! $data->{job_complete} && $data->{job_begun} ne "Not Started" )
   {
    $data->{job_complete} = "<b><font color=\"red\">Running</font></b>";
   }

   if ( ! $data->{job_complete} && $data->{job_begun} eq "Not Started" )
   {
    $data->{job_complete} = "<b><font color=\"orange\">Pending</font></b>";
   }

   # Load the html code into a variable.
   $html = <<EOTR;
	<tr bgcolor="#ffffee">
	<td align="left" nowrap class="fp">$data->{owner}</td>
	<td align="center" nowrap class="fp"><a href="./mail_jobs_status.cgi?job_number=$data->{job_number}&action=details" target="_blank">$data->{job_number}</a></td>
	<td align="left" nowrap class="fp">$data->{total_records}</td>
	<td align="center" nowrap class="fp">$data->{job_begun}</td>
	<td align="center" nowrap class="fp">$data->{job_finished}</td>
	<td align="center" nowrap class="fp">$data->{run_time}</td>
	<td align="right" nowrap class="fp">$data->{time_per_record}</td>
	<td align="center" nowrap class="fp">$data->{job_complete}</td>
	<td align="center" nowrap class="fp"><a href="/cgi/admin/mail_jobs_status.cgi?job_number=$data->{job_number}&action=delete" onclick="return confirm_delete($data->{job_number})">Delete $data->{job_number}</a></td>
	</tr>
EOTR

# Pass the variable's info into an array to be passed into the template in the main script.
push @TABLE,$html;

}

$rv = $sth->finish();
  
}


###############################################
#####
##### Display the details of a selected mail job.
#####
sub display_details
{
 my ($job_num) = @_;

 my ( $sth, $qry, $data, $rv ,$html);    ###### database variables

 # Select a specified mail job's info from the database.
 $qry = "SELECT owner,
		create_date,      
		sql_statement,    
		total_records,    
		from_address,     
		reply_to,         
		subject,          
		message_text,     
		start,            
		job_begun,        
		job_finished,     
		notifications_to, 
		CASE
			WHEN remove_link_flag= TRUE THEN 'YES'
			ELSE 'NO'
		END AS remove_link_flag 
	FROM mail_jobs
	WHERE job_number = $job_num";
 
$sth = $db->prepare($qry);

$rv = $sth->execute();
defined $rv or die $sth->errstr;

while ( $data = $sth->fetchrow_hashref  )
{
	$data->{$_} = $q->escapeHTML($data->{$_}) foreach keys %{$data};

# These will provide place holders in the html table structure if there is no data.
$data->{reply_to} = $data->{reply_to} || "\&nbsp;";
$data->{job_begun} = $data->{job_begun} || "\&nbsp;";
$data->{job_finished} = $data->{job_finished} || "\&nbsp;";

###### put in HTML breaks for simple newlines
$data->{message_text} =~ s#\n#<br />#g;

# Load the html code into a variable.
$html = <<EOKK;
	<table bgcolor="ffffee" border="1" cellpadding="3" >
	<tr ><td align="center" nowrap colspan="2"><B>Details for Job Number $job_num</B></td></tr>
	<tr class="fp"><td nowrap><B>Field Name</B></td><td><B>Field Value</B></td></tr>
	<tr class="fp"><td class="fp">Owner</td><td class="fp">$data->{owner}</td></tr>
	<tr ><td class="fp">Creation Date</td><td class="fp">$data->{create_date}</td></tr>
	<tr ><td class="fp">SQL</td><td class="fp">$data->{sql_statement}</td></tr>
	<tr ><td class="fp">Total Records</td><td class="fp">$data->{total_records}</td></tr>
	<tr ><td class="fp">From Address</td><td class="fp">$data->{from_address}</td></tr>
	<tr ><td class="fp">Reply To</td><td class="fp">$data->{reply_to}</td></tr>
	<tr ><td class="fp">Subject</td><td class="fp">$data->{subject}</td></tr>
	<tr ><td class="fp">Message</td><td class="fp">$data->{message_text}</td></tr>
	<tr ><td class="fp">Start Date</td><td class="fp">$data->{start}</td></tr>
	<tr ><td class="fp">Job Started</td><td class="fp">$data->{job_begun}</td></tr>
	<tr ><td class="fp">Job Finished</td><td class="fp">$data->{job_finished}</td></tr>
	<tr ><td class="fp">Notifications Recipient</td><td class="fp">$data->{notifications_to}</td></tr>
	<tr ><td class="fp">Remove Link</td><td class="fp">$data->{remove_link_flag}</td></tr>

EOKK

# Pass the variable's info into an array to be passed into the template in the main script.
push @TABLE,$html;
}


$rv = $sth->finish();
  
}


###############################################
#####
##### After receiving confirmation, DROP the table from the database.
#####
sub delete_job_table
{

my ($job_num) = @_;
my ($sth, $qry, $rv);    ###### database variables

$qry = "DROP TABLE z$job_num";
 
$sth = $db->prepare($qry);

$rv = $sth->execute();
defined $rv or die $sth->errstr;

$rv = $sth->finish();
  
}

##### 03/26/2003 - Added the functionality allowing display of a job's details 
#####               and the ability to drop a job table from the database.
#####               Changes were also made to the template 10091 by adding
#####               a java script popup confirmation. Template
#####               10139 was added to display the job details.
###### 07/18/03 added functionality to present the message with HTML breaks so it looks more like it was written
###### also put in the remove link field
###### 08/04/03 revised the query for existing mailjob tables to work with a regex rather than a simple like starting with 'z'
###### also revised the data presentaion query to truncate the microseconds info from the timestamps
###### 08/19/03 put a sort on the diplay of jobs
# 04/20/06 merely changed the link for deletions to remove the protocol and domain
# 09/06/06 ordered the output list
# 10/12/06 added use lib
