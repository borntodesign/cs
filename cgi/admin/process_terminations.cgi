#!/usr/bin/perl -w
###### process_terminations.cgi
###### process lists of terminations/suspensions
###### 
###### last modified 08/29/2014 Bill MacArthur

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
require ADMIN_DB;

###### cancellation/termination status values:
###### cp=cancel pending p=processed cd=cancel dropped Cp=Completely cancel
###### tp=termination pending tr=termination rescinded
###### sp=suspension pending tw=termination waiting

###### Globals
my $q = new CGI;
my $list = $q->param('list') || '';
my $c_type = ($q->param('c_type') || '');
my $c_reason = $q->param('c_reason');
$c_reason = $q->param('other_reason') unless $c_reason;
my ($db);
my $processed = my $fails = my $skipped = my $dupes = 0;
my $listprocessed = '';

###### get rid of leading and trailing whitespace and newlines and whatever else
$list =~ s/(^\D)(\D$)//;

###### check for necessary parameters
unless ($list && $c_reason && $c_type =~ /sp|tp/)
{
	Err( $q->h4('Missing at least one of the following:<br />List, Type of termination, reason for termination') );
	exit;
}

###### here is where we get our user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

exit unless $db = ADMIN_DB::DB_Connect( 'mopi.cgi', $operator, $pwd );

###### we will want to be informed of any DB errors by quickly aborting the process
$db->{RaiseError} = 1;

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
if (ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1)
{
	Err($q->h3('You must be a member of the accounting group to use this form'));
	goto 'END';
}

###### we will add the operator of this script to the memo which will be inserted into the cancellation record
$c_reason= $db->quote("$c_reason \n Batch process by: $operator");

###### prepare my query
my $qry = qq/
	SELECT m.membertype, c.status, firstname, lastname, ma.id, ma.status AS ma_status
	FROM members m LEFT JOIN cancellations c ON c.id=m.id 
	LEFT JOIN merchant_affiliates_master ma
		ON ma.member_id=m.id
		AND COALESCE(ma.status,1) > 0
	WHERE m.id= ?/;

my $sth = $db->prepare($qry);

print 	$q->header(-charset=>'utf-8'),
	$q->start_html(-bgcolor=>'#ffffff', -title=>'Processing Termination List ...'), $q->h4('Processing list');

foreach (split /\n/, $list)
{

###### get rid of newlines and windows carriage returns
	s/\n|\r//g;

	print "$_ - ";
	if ($_ =~ /^\D/){
		print "Skipped. Not a numeric ID.<br>\n";
		$listprocessed .= "$_\n";
		$skipped++;
		next;
	}
	elsif ($listprocessed =~ /$_/){
		print "Skipped. Duplicate list entry.<br>\n";
		$listprocessed .= "$_\n";
		$dupes++;
		next;
	}

	$sth->execute($_);
	my $member_info = $sth->fetchrow_hashref;

	# These will be the same for all records found.
	my $mtype = $member_info->{membertype};
	my $ctype = $member_info->{status};
	my $fname = $member_info->{firstname};
	my $lname = $member_info->{lastname};

	# We may have multiple merchant records,
	#  so we check them all or until we find one active.
	my $active_merchant = $member_info->{ma_status};
	while ($member_info = $sth->fetchrow_hashref)
	{
		$active_merchant = $member_info->{ma_status};
		last if $active_merchant;
	}

	# If we found an active merchant, skip this member ID.	
	if ($active_merchant){
		print "Skipped. ID associated with at least one active merchant account.<br>\n";
		$listprocessed .= "$_\n";
		$skipped++;
		next;
	}

	unless ($mtype){
		print "Skipped. Invalid ID number.<br>\n";
		$listprocessed .= "$_\n";
		$skipped++;
		next;
	}

###### if they are already termed or cancelled, why do it again :)
	if ($mtype eq 't'){ print "Skipped. Already terminated.<br>\n"; $skipped++; }
	elsif($mtype eq 'c'){ print "Skipped. Already cancelled.<br>\n"; $skipped++; }

###### if they are already termination, why start over :)
	elsif ($ctype =~ /tw|tp/){
		print "Skipped. Already awaiting termination. - $fname $lname<br>\n";
		$skipped++;
	}

###### if they have a cancellation pending we don't want to suspend them because it will take the whole
###### suspension period to get them out of the system
	elsif ($ctype =~ /cp/i && $c_type eq 'sp'){
		print "Skipped. Cancellation pending. - $fname $lname<br>\n";
		$skipped++;
	}else{
		$db->do("DELETE FROM cancellations WHERE id = $_");
		$qry = "INSERT INTO cancellations (id, status, memo) VALUES ($_, '$c_type', $c_reason)";
		my $res = $db->do( $qry );
		my $msg = '';
		if ($res eq '1'){
			$msg = "OK. Set for processing - $fname $lname<br>";
			$processed++;
		}else{
			my $error = $db->errstr;
			$msg = "Processing Error: $error<br>";
			$fails++;
		}
		print "$msg\n";
	}
	$listprocessed .= "$_\n";
}
$sth->finish;

my $total = $processed + $skipped + $dupes + $fails;
print "<br />Job Complete.<br />$total - items in the list.<br />\n";
print "$processed - scheduled for termination/suspension<br />\n";
print "$dupes - skipped as duplicate list entries.<br />$skipped - skipped for other reasons.<br>\n";
print "$fails unsuccessfully processed.<br /><br />print this page as a report if desired.\n", $q->end_html();

END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff');
	print $_[0];
	print $q->end_html();
}

###### 12/06/01 added a check to be sure that the user is a member of the Accounting group
###### 12/26/01 added handling for duplicate list entries, made DB connections work from the global module
###### 12/27/01 made the 'other reason' work!
###### 04/01/04 moved the RaiseError before the group checker
###### 04/27/04 Removed " || $ctype eq 'sp' " from the "...already termination..." test.
###### 07/21/04 Added the check for an associated active merchant account.
######		This involved modifying the query to include the merchant... table and
######		changing to $sth->fetchrow_hashref since there may be more than one merchant record.
###### 03/31/06 tweaked the loop evaluation for multiple merchant relationships
# 11/30/09 tweaked the evaluation to determine an "active" merchant
