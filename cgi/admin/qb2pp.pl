#!/usr/bin/perl -w
###### qb2pp.pl
###### convert a QuickBooks export file of a specific format to the format required for Wachovia 'Positive Pay'
###### refer to the Word document "Wachovia ARP - Input File Format (80 bytes).doc"
###### created: 06/05/07	Bill MacArthur
###### last modified: 09/20/11	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;

unless ($q->https)
{
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}

my $file = $q->param('upload');

###### if called without parameters, just display an input form
unless ($file)
{
	print $q->header, $q->start_html(
			'-title'=>'QuickBooks Export -> Positive Pay File',
			'-onload'=>'document.forms[0].default.focus(); document.forms[0].default.select();'),
		$q->p('Enter the name of the file you want to convert and hit Submit'),
		$q->start_multipart_form(),
		'<p>This is the default. Copy &amp; paste if you like.<br />',
		"Hint: 'Ctrl C' -&gt; Tab -&gt; 'Ctrl V' -&gt; Tab -&gt; Tab -&gt; Enter<br />",
		$q->textfield(
			'-name'=>'default',
			'-value'=>'\\\DHSPDC\D\Accounting\DHS-Club\acct_recon\commission.PRN',
			'-onclick'=>'this.select()',
			'-size'=>70),
		'</p><br />', $q->filefield('-name'=>'upload', '-size'=>70), ' ',
		$q->submit('Submit'), $q->end_form, $q->end_html;
	exit;
}


###### we'll be able to match the last four digits of the account to get the whole number
###### be sure to include the last four digit matches in the regex further down
my %ACCTS = (
	'1333' => {'acct' => 2000006261333, 'list' => '', 'ttl' => 0, 'cnt' => 0},
	'0415' => {'acct' => 2000036230415, 'list' => '', 'ttl' => 0, 'cnt' => 0}
);

###### the header line of our input needs to match this exactly
###### if it doesn't, then it is unlikely that the data will be correct
my $HEAD = q|"Date","Num","Name","Account","Memo","Amount","Type"|;

###### this should be line 2
my $LINE2 = ',,,,,,';

###### our data lines should look like this (less the preceding #)
#"5/25/2007","1005","2939310","Wachovia - Commissions 0415","Reward Pt Redemption",-50.00,"Check"
#"5/31/2007","1006","0022034","Wachovia - Commissions 0415","VOID:blah blah",-597.36,"Check"
#"5/31/2007","1006","blah blah","Wachovia - Commissions 1333","VOID:blub blub",-597.36,"Check"

#             MM -1     DD -2     YYYY -3  CK -4   ID -5                   Account suffix -6  Memo -7  Amt -8
my $GOOD = '"(\d{1,2})/(\d{1,2})/(\d{4})","(\d+)","(.*?)","WellsFargo -.+?Commissions (0415|1333)","*(.*?)"*,(.*?),"Check"';

foreach(<$file>){
	chomp $_;
	next unless $_;
	if (m#$GOOD#){
		Process();
	}
	elsif (/$HEAD/){ next; }
	elsif (/$LINE2/){ next; }
	else { die "Line format not recognized:\n$_\n"; }
}

print $q->header(), $q->start_html;
foreach (keys %ACCTS){
	next unless $ACCTS{$_}->{'cnt'};
	print "RECONCILIATIONHEADER0003", $ACCTS{$_}->{'acct'};
	printf '%012u', ($ACCTS{$_}->{ttl} * -100);	###### no decimal point and invert the value
	printf '%05u', $ACCTS{$_}->{'cnt'};
	print "<br />\n";
	print $ACCTS{$_}->{'list'};
#print "total: $ACCTS{$_}->{ttl}\n";
#print	"cnt: $ACCTS{$_}->{'cnt'}\n";
}
print $q->end_html;
exit;

sub Process{
	###### we have to preserve some match values which will get blown away after the match operation below
	my $name = $5;
	my $key = $6;
	my $amt = $8;

	###### we should have our needed values in the match variables
	my $line = $ACCTS{$6}->{'acct'};
	$line .= sprintf '%010u', $4;
	$line .= sprintf '%010u', ($amt * -100);	###### no decimal point & invert the value
	$line .= $3 . (sprintf '%02u', $1) . (sprintf '%02u', $2);
	$line .= ($7 =~ /VOID:/ ? 'V' : 'I');
	$line .= "$name<br />\n";
	$ACCTS{$key}->{'list'} .= $line;
	$ACCTS{$key}->{'cnt'}++;
	$ACCTS{$key}->{ttl} += $amt;
}



# 09/20/11 changed the bank name to WellsFargo
###### 08/22/12 no significant changes, only cosmetic ones
