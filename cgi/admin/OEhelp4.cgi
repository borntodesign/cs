#!/usr/bin/perl

print "content-type: text/html\n\n";
print "\n";

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC CGI Help</title>
</head>
<body bgcolor="#eeffee">
<table border="0" width="100%" align="center">
  <tr>
  <th align="left"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top" nowrap>
 <b>DHSC Template Help</b>
  </th>
 </tr>
  <tr>
  <td align="left" nowrap
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top">
  This page displays a list of the recoverable templates (if any )<br>
  that exist for this object.&nbsp;<br>
  You should note the '<u>Updated</u>' date and time to determine&nbsp;<br>
  the correct version to restore back to.<br>
  <br>
  By clicking on the '<u>View</u>' link you can see the contents of this<br>
  template version. Once you are satisfied that you have found&nbsp;<br>
  the correct version you can recover the template.
  </td>
 </tr>
<tr>
 <td height="15">&nbsp;
 </td>
</tr>
 <tr>
  <td align="left">
   <form>
   <input type="button" value="Close" name="execB"
    onClick="javascript:window.close();" class="in">
   </form>
  </td>
 </tr>
</table>
</body>
</html>
\n~;

exit;


