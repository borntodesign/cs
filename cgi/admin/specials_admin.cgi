#!/usr/bin/perl -w
###### specials_admin.cgi
######
###### An interface for admin to view and add Reward Points transactions.
######
###### created: 05/23/05	Karl Kohrt
######
###### last modified: 

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use ParseMe;

# GLOBALS
our $q = new CGI;
our $ME = $q->url;
our $db = '';
our $page_body = '';
our $action = '';
our $message = '';
our $vendor_id = '';				
our $link = '';
our $link_text = '';				
our $exp_date = '';				
our $page_type = 'main';

our %TMPL = (	'main'		=> '10450',
		'special'	=> '10451' );

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

if ($operator && $pwd)
{
	# our admin access
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'specials_admin.cgi', $operator, $pwd )){exit}

	$db->{RaiseError} = 1;
	$action = $q->param('action');

	# No action means we display the main menu only.
	unless ( $action )
	{
		Do_Page($page_type);
	}
	# Otherwise, there was some type of action submitted.
	else
	{
		$vendor_id = $q->param('vendor_id');
		my $search_param = ($vendor_id) ? 'vendor_id' : 'all';
		my $results = '';

		# A request to show the specials.
		if ( $action eq 'show' )
		{
			# Get the specials related to the vendor ID.
			$results = Build_List( Get_Specials($search_param, $vendor_id) );
		}
		# A request to create a new special.
		elsif ( $action eq 'create' )
		{
			# Get the parameters for the new special.
			$link = $q->param('link') ;				
			$link_text = $q->param('link_text') ;				
			$exp_date = $q->param('exp_date');				

			# Create a new special. 
			#  If unsuccessful, zero out the vendor_id for the next step.
			unless ( Create_Special() ) { $vendor_id = 0 }

			# Get the specials related to the vendor ID just created.
			$results = Build_List( Get_Specials('vendor_id', $vendor_id) );			
		}
		elsif ( $action eq 'special' )
		{
			# Get the individual special based upon the pk parameter.
			my @tmp = Get_Specials('pk', $q->param('pk'));
			$results = $tmp[0];
			$page_type = 'special';
		}
		elsif ( $action eq 'update' )
		{
			my $pk = $q->param('pk');
			if ( Update_Special($pk) )
			{
				$message = qq#<span class="b">
						The Special was successfully updated.<br>
						</span><br>#;
			}
			# Get the individual special based upon the pk parameter.
			my @tmp = Get_Specials('pk', $pk);			
			$results = $tmp[0];
			$page_type = 'special';
		}
		else
		{
			$message = qq#<span class="r">
					The action '$action' is not allowed.<br>
					</span><br>#;
		}
		# Display the results.
		Do_Page($page_type, $results);
	}
	$db->disconnect;
}
else
{
	Err('This interface is for Admin use only. Please log in, then try again.');
}

exit;

#####################################
###### Begin Subroutines Here. ######
#####################################

###### Build a list of specials for display.
sub Build_List
{
	my @results = @_;
	my $bg = '#ffffee';
	my @Sort = ('vendor_name', 'pk', 'exp_date', 'link_text', 'stamp', 'operator');
	my $tbl = $q->Tr({-bgcolor=>$bg},	$q->th(\@Sort));

	foreach my $tmp ( @results )
	{
		###### alternate our row color
		$bg = ($bg eq '#ffffee') ? '#eeeeee' : '#ffffee';
		$tbl .= qq|<tr bgcolor="$bg">\n|;
		foreach (@Sort)
		{
			$tbl .= qq#<td align="center">#;
			if ( $_ eq 'pk' )
			{
				$tbl .= qq#<a href="$ME?action=special;pk=$tmp->{pk}"
							target="_blank">
							$tmp->{pk} - Details</a>#;
			}
			elsif ( $_ eq 'vendor_name' )
			{
				$tbl .= qq#<a href="/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=$tmp->{vendor_id}&offset=&criteria1="
							target="_blank">
							$tmp->{vendor_name}</a>#;
			}
			elsif ( $_ eq 'link_text' )
			{
				$tbl .= qq#<a href="$tmp->{link}"
							target="_blank">
							$tmp->{link_text}</a>#;
			}
			else
			{
				$tbl .= $tmp->{$_} || '&nbsp;';
			}
			$tbl .= "</td>";
		}
	}
	$tbl = qq#<tr><td colspan="2" align="center">
			<table border="0" cellpadding="3" cellspacing="3">
			$tbl
			</table>
			</td></tr>#;
	return $tbl;
}

###### Create a special.
sub Create_Special
{
	my $sth = $db->prepare("	INSERT INTO vendor_specials
						(vendor_id, exp_date, link, link_text, operator, stamp)
					VALUES
						(?, ?, ?, ?, ?, NOW())");
	my $rv = $sth->execute($vendor_id, $exp_date, $link, $link_text, $operator);
	if ( $rv eq '0E0' )
	{
		$message .= qq#<span class="r">The new special was NOT added.</span><br>#;
	}
	else
	{
		$message .= qq#<span class="b">The new special was added.</span><br>#;
	}
}

###### Display the page to the browser.
sub Do_Page
{
	my ($type, $results) = @_;
	my $tmpl = G_S::Get_Object($db, $TMPL{$type}) || die "Couldn't open template: $TMPL{$type}";

	$tmpl =~ s/%%me%%/$ME/g;
	$tmpl =~ s/%%message%%/$message/;

	# If this is an update or individual display, we have to fill the individual data fields.
	if ( $type eq 'special' )
	{
		foreach ( keys %{$results} )
		{
			$tmpl =~ s/%%$_%%/$results->{$_}/g;
		}
	}
	# Otherwise, the output is already in an html format.
	else
	{
		$tmpl =~ s/%%results%%/$results/;
	}

	# print the page header here so we can view Unicode.
	print $q->header(	-expires=>'now',
				-type=>'text/html'), $tmpl;
}

###### Print an error message.
sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h4($_[0]), scalar localtime(),
		$q->end_html();
}

###### Get the requested specials and build a results list for display.
sub Get_Specials
{
	my $search_param = shift || 'pk';
	my $search_id = shift || $q->param('pk');
	my $where_clause = '';
	my $tbl = '';
	my @tmp_results = ();

	# If we have an ID.
	if ( $search_param eq 'vendor_id' )
	{
		$where_clause = " WHERE s.vendor_id = $search_id";
	}
	# No search ID, so we'll return all current specials as the default.
	elsif ( $search_param eq 'pk' )
	{
		$where_clause = " WHERE s.pk = $search_id";
	}
	# This is the 'all' case as well as the default.
	else
	{
		$where_clause = " WHERE s.exp_date >= NOW()";
	}
	my $sth = $db->prepare("	SELECT s.*, s.exp_date::DATE,
						v.vendor_name
					FROM 	vendor_specials s
						INNER JOIN vendors v ON s.vendor_id = v.vendor_id"
					. $where_clause
					. " ORDER BY v.vendor_name;"
					);
	my $rv = $sth->execute();
	if ( $rv eq '0E0' ) { $message .= qq#<span class="r">No specials were found.</span><br># }
	else
	{
		while ( my $tmp = $sth->fetchrow_hashref )
		{
			push @tmp_results, $tmp;
		}
	}
	return @tmp_results;
}

###### Update the special.
sub Update_Special
{
	my $pk = shift;
	my @excluded = qw/pk vendor_id stamp operator/;
	my @values = ();
	my $qry = "	UPDATE vendor_specials
			SET 	stamp = NOW(),
				operator = ?, ";
	push @values, $operator;

	foreach my $field ( ('exp_date', 'link', 'link_text') )
	{
		$qry .= " $field = ?,";
		push @values, $q->param($field);
	}
	chop $qry;
	$qry .= " WHERE pk = ?";
	push @values, $pk;

#Err("qry= $qry"); foreach (@values) {Err("$_");}; exit;
	# Update the special.
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@values);
	if ( $rv eq '0E0' )
	{
		$message .= qq#<span class="r">
				Database error - no changes were made.
				</span><br>#;
		return 0;
	}
	return $rv;
}




