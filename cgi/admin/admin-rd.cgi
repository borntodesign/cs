#!/usr/bin/perl -w
###### admin-rd.cgi
###### a simple redirector to the desired resource after looking up the member by the value provided
###### created: 06/07/07	Bill MacArthur
###### last modified: 06/05/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use ADMIN_DB;
require cs_admin;
use DHSGlobals;
no warnings 'once';
binmode STDOUT, ":encoding(utf8)";

my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my (@list, $err_msg) = ();

###### we should be in SSL mode
die "This resource should only be invoked in SSL mode\n" unless $q->https;

###### we should have our operator and password cookies
#my $operator = $q->cookie('operator');
#my $pwd = $q->cookie('pwd');
my $ck = $q->cookie('cs_admin');
die "You must be logged in to use this resource\n" unless $ck;
my $creds = $cs->authenticate($ck);

exit unless my $db = ADMIN_DB::DB_Connect( 'admin-rd', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

###### we're only expecting these params
# rc - Resource Code
# lookup_val - the value we are going to use to lookup records
# Ltype - the lookup type flag (this should say 'oid' for oid/unique number lookups)
my %param = map {$_, G_S::Prepare_UTF8($q->param($_) || '')} ('rc','lookup_val','Ltype');

###### we need these values for these params
foreach('rc','lookup_val'){
	die "Failed to receive a required parameter: $_\n" unless $param{$_};
}

$param{'lookup_val'} =~ s/^\s*|\s*$//g;	###### get rid of leading/trailing whitespace

my $sql = '
	SELECT m.id,
		m.alias,
		m.password,
		m.emailaddress,
		m.oid,
		m.membertype,
		m.firstname,
		m.lastname,
		COALESCE(p.seq,maxintval()) AS seq
	FROM members m
	LEFT JOIN mviews.positions_freshest p
		ON m.id=p.id
	WHERE ';

###### if we are strictly digits we are either an ID or an oid
if ($param{'lookup_val'} !~ /\D/)
{
	$sql .=
		$param{'Ltype'} eq 'oid' ? 'm.oid=' :
		$param{'Ltype'} eq 'seq' ? 'p.seq=' :
		'm.id = ';
}
###### if we match an @ we must be an email address
elsif ($param{'lookup_val'} =~ /@/)
{
	$param{'lookup_val'} = lc $param{'lookup_val'};
	$sql .= 'm.emailaddress = ';
}
###### if we are alpha numeric we must be an alias
elsif ($param{'lookup_val'} =~ /\d/)
{
	$param{'lookup_val'} = uc $param{'lookup_val'};
	$sql .= 'm.alias = ';
}
###### otherwise we'll try by lastname
else
{
	$sql .= 'm.lastname ILIKE ';
}
$sql .= '? ORDER BY m.id';

#warn $sql;
#warn $param{'lookup_val'};

my $sth = $db->prepare($sql);
$sth->execute($param{'lookup_val'});
my $cnt = 0;
while (my $tmp = $sth->fetchrow_hashref)
{
	###### limit search results to ...
	if ($cnt++ > 500){
		$err_msg = 'Your search results would exceed 500. The list has been truncated.';
		last;
	}
	push @list, $tmp;
}
$sth->finish;

unless (@list)
{
	$err_msg = "No matches found for: $param{'lookup_val'}";
	ShowList();
}
elsif (scalar @list > 1)
{
	ShowList();
}
else {
	my $url = CreateURL($list[0]);
	if ($url =~ m/www\.glocalincome\.com/){
		
	}
	else {
		$url = 'https://' . DHSGlobals::CLUBSHOP_DOT_COM  . $url;
	}
	print $q->redirect($url);
}
END:
$db->disconnect;
exit;

sub CreateURL
{
	my $url = $ADMIN_DB::AppMap{$param{'rc'}}->{'url'};
	my $data = shift;
	# once Dick changed the default login destination for all member types, the previous functionality of going to their business centers was lost
	# of course he still wants admin to go to their business centers instead of the default they would receive
	# we will deal with that by coercing the final destination here
	$url = LoginAsDest($data) if $param{'rc'} == 3;

	###### this little dealey converts every %%placeholder%% that matches a data key in the data key/value
	$url =~ s/%%(.+?)%%/(defined $data->{$1} ? $data->{$1} : '')/eg;
	return $url;
}

sub LoginAsDest
{
	my $data = shift;
	my $rv = ();
	if ($data->{'membertype'} eq 'm')
	{
		$rv = '/cgi/LoginMembers.cgi?_action=login;username=%%alias%%;password=%%password%%';
	}
	elsif ($data->{'membertype'} eq 'v')
	{
		$rv = '/cgi/LoginMembers.cgi?destination=/p/index.shtml;_action=login;username=%%alias%%;password=%%password%%';
	}
	else
	{
		$rv = '/cgi/LoginMembers.cgi?_action=login;username=%%alias%%;password=%%password%%';
	}
	return $rv;
}

sub ShowList
{
	my $bgc = 'g';
	print $q->header(-charset=>'utf8'), $q->start_html(
		'-title' => "Search Results for: $param{'lookup_val'}",
		'-style' => {'-code' =>'
			body { font-family: sans-serif; }
			tr.g {background-color:#eee;}
			tr.y {background-color:#efe;}
			td {
				border: none;
				font: normal 80% sans-serif;
				padding: 0.2em 0.3em;
			}'}
	);
	print $q->h5({'-style'=>'text-align:center'}, "Search Results for: $param{'lookup_val'}");
	print $q->p({'-style'=>'text-align:center'}, $err_msg) if $err_msg;
	print q|<table style="margin:auto">|;
	foreach (@list){
		$bgc = $bgc eq 'g' ? 'y' : 'g';
		print qq|<tr class="$bgc">|;
		print $q->td($q->a({'-href'=>CreateURL($_)}, $_->{'id'})),
			$q->td($_->{'alias'}), $q->td($_->{'firstname'}), $q->td($_->{'lastname'}),
			$q->td($_->{'emailaddress'}), $q->td($_->{'membertype'});
		print '</tr>';
	}
	print q|</table>|, $q->end_html;
}

# 06/20/07 tweaked the SQL for the lastname search to use ILIKE instead of a case-insensitive match
# (this gives a much tighter result set)
# 08/17/10 increased the search results limit from 50 to 300 ;)
# 05/14/15 added the ability to search by mviews.positions.seq
# 06/05/15 made the seq portion of the join by members data a left join to deal with those who do not have a real seq, like terminated