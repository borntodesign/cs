#!/usr/bin/perl -w
###### ChangeVlinePosition.cgi
###### Move a membership within the vertical line
# created 06/04/14	Bill MacArthur
# last updated: 06/08/15	Bill MacArthur

use strict;

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
require ADMIN_DB;
require cs_admin;

my $NOCOMMIT = 0;	# set to 1 to rollback all changes for debugging
my ($errors, $alerts) = ();
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my $ME = $q->script_name;

die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $action = $q->param('action');
unless ($action)
{
	SearchForm();
}

my $tid = $q->param('tid') || '';
$tid =~ s/^\s*|\s*$//g;
my $did = $q->param('did') || '';
$did =~ s/^\s*|\s*$//g;

die "Both a Transferee ID and a Destination ID must be provided" unless $tid && ($did || $q->param('taproot_flag'));

# since this will be a rarely used admin script, I will not bother building in checks on the first pass and parameter verification on the second
# checks will be good enough in both cases
my $t = GetMember($tid);
unless ($t)
{
	$errors = "Could not find a membership matching id: $tid";
	SearchForm();
}

my $d = ();
if ($q->param('taproot_flag') && $action eq 'examine')
{
	$did = GetDestinationIdBasedOnPool($t->{'id'});
	SearchForm() if $errors;

	$alerts = 'Final placement is not guaranteed as more Trial Partners could be added to the bottom of the line before this transaction completes.';
	$d = GetMember($did);
}
elsif ($did)
{
	$d = GetMember($did);
	
	unless ($d)
	{
		$errors = "Could not find a membership matching id: $did";
		SearchForm();
	}
}
else	# we should never get here but...
{
	unless ($d)
	{
		$errors = "Could not find a membership matching id: $did";
		SearchForm();
	}
}

if ($action eq 'examine')
{
	Examine();
}
elsif ($action eq 'transfer')
{
	Transfer();
}
else
{
	die "Unknown 'action' parameter: $action";
}

Exit();

######

sub Examine
{
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title' => 'Confirm Placement',
			'-style' => {'-code' => '
				th {text-align:left; padding-right:1em; padding-left: 0.3em;}
				td {text-align:left; border: 1px solid grey; padding: 0.2em 0.4em;}
				.bbme {font-weight:bold; color:blue;}
				.rbme {font-weight:bold; color:red;}
				.gbme {font-weight:bold; color:green;}
				.ime {font-style: italic; padding: .2em;}
			'}
		);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;
	print $q->p({'-style'=>'color:blue'}, $alerts) if $alerts;
	print	
		$q->start_form('-method'=>'get'),
		$q->hidden('tid'),
		$q->hidden('-name'=>'did', '-value'=>$d->{'id'}, '-force'=>1),
		$q->hidden('taproot_flag'),
		$q->h4(qq|Confirm the placement of: <span class="bbme">$t->{'alias'} - $t->{'firstname'} $t->{'lastname'}  ($t->{'display_code'})</span> <i>under</i>
			<span class="gbme">$d->{'alias'} - $d->{'firstname'} $d->{'lastname'} ($d->{'display_code'})</span>|),
		'<table style="border-collapse: collapse; margin-top:1em;">',
		$q->Tr(
			$q->th('Current Position') . $q->th('Proposed Position')
		);
	
	print '<tr><td>';
	foreach my $row (GetSegment($t))
	{
		my $class = ($row->{'id'} == $t->{'id'} ? 'bbme': $row->{'id'} == $d->{'id'} ? 'rbme' : 'normal');
		print $q->div({'-class'=>$class}, "$row->{'alias'} - $row->{'firstname'} $row->{'lastname'} ($row->{'display_code'})");
	}

	print '</td><td>';
	foreach my $row (GetSegment($d, $t->{'id'}))
	{
		my $class = ($row->{'id'} == $d->{'id'} ? 'gbme': $row->{'id'} == $t->{'id'} ? 'bbme ime' : 'normal');
		print $q->div({'-class'=>$class}, "$row->{'alias'} - $row->{'firstname'} $row->{'lastname'} ($row->{'display_code'})");
		
		# in order to show our transferee "in position", we will put them into place now
		if ($row->{'id'} == $d->{'id'})
		{
			$row = $t;
			redo;
		}
	}
	print '</td></tr>';
	
	print
		'</table>',
		$q->submit('Perform Transfer'),
		$q->hidden('-value'=>'transfer', '-name'=>'action', '-force'=>1),
		$q->hidden('-value'=>$t->{'nspid'}, '-name'=>'top', '-force'=>1),
		$q->end_form,
		$q->p({'-style'=>'margin-top:3em'}, $q->a({'-href'=>$q->url}, 'Start Over'));
		$q->end_html;
	Exit();
}

sub Exit
{
	$db->rollback if $NOCOMMIT;
	$db->disconnect;
	exit;
}

sub GetDestinationIdBasedOnPool
{
	my $id = shift;
	my ($di) = $db->selectrow_array("
		SELECT ns.spid
		FROM network.spids ns
		WHERE id= network.my_pool_id($id)");
	die "Failed to identify the pool or the pool sponsor for $id" unless $di;
	$errors .= "$id is already the sponsor of the taproot pool" if $id == $di;
	return $di;
}

sub GetMember
{
	my $id = shift;
	my $qry = qq/
		SELECT m.id, m.alias, m.firstname, m.lastname, m.membertype, mt.display_code, m.nspid, m.rspid
		FROM network.members m
		JOIN member_types mt
			ON mt."code" = m.membertype
		WHERE m.membertype IN ('v','tp','pns') AND /;
	$qry .= ($id =~ m/\D/) ? 'm.alias=?' : 'm.id= ?';
	return $db->selectrow_hashref($qry, undef, uc($id));
}

sub GetMemberByNspid
{
	my $id = shift;
	my $ignore = shift || 0;
	return $db->selectrow_hashref(qq/
		SELECT m.id, m.alias, m.firstname, m.lastname, m.membertype, mt.display_code, ns.spid AS nspid, m.spid AS rspid
		FROM members m
		JOIN member_types mt
			ON mt."code" = m.membertype
		JOIN network.spids ns
			ON ns.id=m.id
		WHERE ns.spid= $id
		AND ns.id != $ignore
		AND m.membertype IN ('v','tp','pns','mp')/);
}

# build an array of memberships above and below the ID argument
sub GetSegment
{
	my $m = shift;
	my $ignore = shift;
	my @RV = $m;
	
	my $tmp = $m;
	for (my $x=0; $x < 3; $x++)
	{
		my $rv = GetMember($tmp->{'nspid'});
		die "Failed to find qualifying nspid upline $tmp->{'nspid'} for $tmp->{'alias'}" unless $rv;
		
		# ensure there is no loop
		if (grep {$rv->{'id'} == $_->{'id'}} @RV)
		{
			$errors .= "<br />There is a problem with looping going on here: $rv->{'id'}. Check the existing position of your Transferee and/or your Destination ID.";
			last;
		}
		unshift @RV, $rv;

		$tmp = $rv;
	}

	$tmp = $m;
	for (my $x=0; $x < 3; $x++)
	{
		my $rv = GetMemberByNspid($tmp->{'id'}, $ignore);
		last unless $rv;
		# ensure there is no loop
		if (grep {$rv->{'id'} == $_->{'id'}} @RV)
		{
			$errors .= "<br />There is a problem with looping going on here: $rv->{'id'}. Check the existing position of your Transferee and/or your Destination ID.";
			last;
		}
		
		push @RV, $rv;
		$tmp = $rv;
	}
	
	return @RV;
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html('Move Member Within Vertical Line');

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->h4('Move Member Within Vertical Line'),
		
		$q->start_form('-method'=>'get'),
		'Transferee ID: ',
		$q->textfield('-value'=>'', '-name'=>'tid'),
		$q->br,
		'Place under ID: ',
		$q->textfield('-value'=>'', '-name'=>'did', '-id'=>'didtext'),
		$q->checkbox('-style'=>'margin-left:3em', '-name'=>'taproot_flag', '-value'=>1, '-label'=>' Place at bottom of taproot'),
		$q->br,
		$q->submit('Search'),
		$q->hidden('-value'=>'examine', '-name'=>'action'),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub ShowTransferStatus
{
	my $ptop = $q->param('top');
	die "Failed to receive 'top' parameter" unless $ptop;
	my $top = GetMember($ptop) || die "Failed to identify a qualifying membership for $ptop";
	
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title' => 'Transfer Status',
			'-style' => {'-code' => '
				th {text-align:left; padding-right:1em; padding-left: 0.3em;}
				td {text-align:left; border: 1px solid grey; padding: 0.2em 0.4em;}
				.bme {font-weight:bold;}
				.rbme {font-weight:bold; color:red;}
				.bbme {font-weight:bold; color:blue;}
				.gbme {font-weight:bold; color:green;}
			'}
		);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->h4(qq|Transfer Status of: <span class="bbme">$t->{'alias'} - $t->{'firstname'} $t->{'lastname'} ($t->{'display_code'})</span> <i>under</i>
			<span class="gbme">$d->{'alias'} - $d->{'firstname'} $d->{'lastname'} ($d->{'display_code'})</span>|),
		'<table style="border-collapse: collapse; margin-top:1em;">',
		$q->Tr(
			$q->th('Old Position') . $q->th('New Position')
		);
	
	print '<tr><td>';
	foreach my $row (GetSegment($top))
	{
		my $class = ($row->{'id'} == $t->{'id'} ? 'bbme': $row->{'id'} == $d->{'id'} ? 'rbme' : 'normal');
		print $q->div({'-class'=>$class}, "$row->{'alias'} - $row->{'firstname'} $row->{'lastname'} ($row->{'display_code'})");
	}

	print '</td><td>';
	foreach my $row (GetSegment($d))
	{
		my $class = ($row->{'id'} == $d->{'id'} ? 'gbme': ($row->{'id'} == $t->{'id'}) ? 'bbme' : 'normal');
		print $q->div({'-class'=>$class}, "$row->{'alias'} - $row->{'firstname'} $row->{'lastname'} ($row->{'display_code'})");
	}
	print '</td></tr>';
	
	print
		'</table>',
		$q->p({'-style'=>'margin-top:3em'}, $q->a({'-href'=>$q->url}, 'Start Over'));
		$q->end_html;
	
	Exit();
}

sub Transfer
{
	my $memo = 'line move by: ' . $q->script_name;
	
	$db->begin_work; # we don't need to check for errors as RaiseError is set... the transaction is just to ensure we don't get half way through successfully and then bomb on error
	
	# first rollup any Partners and such under the transferee in the vertical line, to the transferee's nspid
	$db->do("
		UPDATE network.spids ns SET spid= $t->{'nspid'}, memo=?, operator=?
		FROM members m
		WHERE m.id=ns.id
		AND m.membertype IN ('v','tp','mp','pns')
		AND ns.spid= $t->{'id'}", undef, $memo, $creds->{'username'});
	
	# now get all the Partners and such in the vertical line under the destination Partner (there really should only be one, but just to be sure)
	# there is a case where a downgraded Partner is moved out of the line but remains under the sponsor and if reinstated, the sponsor has -2-
	# so in order to splice the Partner right back into the line, we need to ignore the transferee
	$db->do("
		SELECT ns.id INTO TEMP TABLE nst
		FROM network.spids ns
		JOIN members m
			ON m.id=ns.id
		WHERE m.membertype IN ('v','tp','mp','pns')
		AND ns.spid= $d->{'id'}
		AND ns.id != $t->{'id'}");
	
	# I suppose if there is more than one, we have an unexpected condition that warrants further investigation and so we will bomb out
	my ($cnt) = $db->selectrow_array("SELECT COUNT(*) FROM nst");
	if ($cnt > 1)
	{
		$db->rollback;
		$errors = 'The destination party actually has more than one v,tp,mp member directly beneath them in the vertical line, so aborting';
		SearchForm();	# that routine Exits
	}
	
	# we need to manage the seq "position" of the transferee to fit them into the spot
	# so first we will look up the value
	my ($dseq) = $db->selectrow_array("SELECT seq FROM mviews.positions_freshest WHERE id= $d->{'id'}");
	if (! $dseq)
	{
		$db->rollback;
		$errors = 'Could not obtain an seq "position" on the destination party, so aborting';
		SearchForm();	# that routine Exits
	}
	else
	{
		# if we are placing them in the taproot, then that next function will return null as there is no "next_highest_available_seq"...
		# our transferee is going to become the next highest
		my $seq = ();
		if ($q->param('taproot_flag'))
		{
			($seq) = $db->selectrow_array("SELECT nextval('od.pool_seq_seq')");
		}
		else
		{
			($seq) = $db->selectrow_array("SELECT network.next_highest_available_seq($dseq)");
		}

		if (! $seq)
		{
			$db->rollback;
			$errors = 'Could not obtain an available seq "position" for the transferee to fit into the line, so aborting';
			SearchForm();	# that routine Exits
		}
		else
		{
			my $rv = $db->do("UPDATE od.pool SET seq= $seq WHERE id= $t->{'id'}");
			$db->do("UPDATE network.positions SET seq= $seq WHERE id= $t->{'id'}") unless $rv eq '1';	# if they don't have a pool record, they must have a partner record
		}
	}
	
	# now move the transferee into place
	my $rv = $db->do("UPDATE network.spids SET spid= $d->{'id'}, memo=?, operator= ? WHERE id= $t->{'id'}", undef, $memo, $creds->{'username'});
	unless ($rv eq '1')
	{
		$db->do("INSERT INTO network.spids (id, spid, memo, operator) VALUES ($t->{'id'}, $d->{'id'}, ?, ?)", undef, $memo, $creds->{'username'});
	}
	
	# now move the parties formerly under the destination partner down under the transferee
	$db->do("UPDATE network.spids ns SET spid= $t->{'id'}, memo=?, operator=? FROM nst WHERE nst.id=ns.id", undef, $memo, $creds->{'username'});
	
	my $odpool = $db->selectrow_hashref("SELECT seq, pnsid FROM od.pool WHERE id= $d->{'id'}");
	if (! $odpool)
	{
		$db->do("DELETE FROM od.pool WHERE id= $t->{'id'}");	# just in case I am moving out
	}
	else
	{
		# if I have been moved into the Trial Partner line, my sponsor (the destination ID), will have an od.pool record
		# if he does, then I need to create/update my own to reflect that I am in the TP line
		$rv = $db->do("UPDATE od.pool SET seq= $odpool->{'seq'} +1 WHERE id= $t->{'id'}");
		$db->do("INSERT INTO od.pool (id, pnsid) VALUES ($t->{'id'}, $odpool->{'pnsid'})") unless $rv eq '1';
	}
	
	$db->commit unless $NOCOMMIT;
	
	ShowTransferStatus();
}

__END__

10/29/14 Added elements into the insert/update statements to always insert the current user
		also made the od.pool record deletion conditional so that a party retains his original deadline if he changes position in the TP line

04/15/15 added handling for changing the seq "position" when moving within the line

06/08/15 changed the acquisition of the seq for a transferee into the bottom of the taproot