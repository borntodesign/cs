#!/usr/bin/perl -w
##
## ipn-reporting.cgi
## present sales aggregates and vendors without any sales
## released: 08/06/07	Bill MacArthur
## modified: 

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
#use G_S;
use ADMIN_DB;
use PayPal;

my $ManualPayPalFee = 0;
my $errormsg = my $message = '';
my $REPORT = '';
my (%Config, $TakeAction) = ();
_init_config_();

#my @DateRanges = ('Y-T-D', 'M-T-D', 'Last Month', 'Custom - enter below');
###### the following values need to exactly match the 'columns' returned by the SQL
my @Global_Order_By = ('stamp','payer_email');
my $q = new CGI;
my $qt = $q->param('query_type') || '';
my @date = localtime;
$date[5] += 1900;
my @year_options = (2007);
for (my $x = 2007; $x < $date[5]; $x++){ push @year_options, $x; }	###### plan ahead ;)

## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my $db = ADMIN_DB::DB_Connect('generic', $operator, $pwd) || exit;
$db->{RaiseError} = 1;

if ($q->param) {
	die "Invalid query type parameter\n" unless grep $qt eq $_, keys %Config;
	my $action = $q->param('action') || '';
	if ($action eq 'update'){ DoUpdates(); }

	my $sql = $Config{$qt}->{sql};
	$sql =~ s/-- EXTRA COLS/,$Config{$qt}->{extra_cols}/ if $Config{$qt}->{extra_cols};

#	if ($q->param('date_range') =~ /Custom/){
		$sql =~ s/<STARTDATE>/$db->quote(
			$q->param('start_year') . '-' .
			$q->param('start_month') . '-' .
			$q->param('start_day'))/ex;

		$sql =~ s/<ENDDATE>/$db->quote(
			$q->param('end_year') . '-' .
			$q->param('end_month') . '-' .
			$q->param('end_day'))/ex;
# 	}
# 	elsif ($q->param('date_range') eq 'M-T-D'){
# 		$sql =~ s/<STARTDATE>/first_of_month()/;
# 		$sql =~ s/<ENDDATE>/NOW()/;
# 	}
# 	elsif ($q->param('date_range') eq 'Y-T-D'){
# 		$sql =~ s/<STARTDATE>/(SUBSTR(NOW(), 1,4)||'-01-01')::DATE/;
# 		$sql =~ s/<ENDDATE>/NOW()/;
# 	}
# 	elsif ($q->param('date_range') eq 'Last Month'){
# 		$sql =~ s/<STARTDATE>/first_of_another_month(first_of_month() - 1)/;
# 		$sql =~ s/<ENDDATE>/first_of_month() - 1/;
# 	}
# 	else { die "Invalid date range parameter received: " . $q->param('date_range'); }
	$sql .= "\nGROUP BY $Config{$qt}->{group_by_SQL}" if $Config{$qt}->{group_by_SQL};
	if ($Config{$qt}->{force_orderby}){
		$sql .= "\nORDER BY $Config{$qt}->{force_orderby}";
	}
	elsif ($q->param('order_by')) {
		die "Invalid order_by parameter\n" unless grep $q->param('order_by') eq $_, @{$Config{$qt}->{order_by}};
		$sql .= qq!\nORDER BY "! . $q->param('order_by') . '"';
		# the default will be the SQL default
		$sql .= ' DESC' if $q->param('order_by_direction') eq 'DESC';
	}
#print "Content-type: text/plain\n\n$sql"; exit;

	my $sth = $db->prepare($sql);
	$sth->execute;
	my $tmp = $sth->fetchrow_hashref;
	unless ($tmp){
		$errormsg = 'No Data Returned';
	} else {
		BuildReportTable($tmp, $sth);
	}
}
Display();

$db->disconnect;
exit;

sub BuildReportTable
{
	my ($first, $sth) = @_;
	$TakeAction = 1 unless $first->{'Cleared'};	###### a flag to decide if we present an update button
	my $class = 'a';
	$REPORT = '<table class="report">';
	$first->{$_} = &{$Config{$qt}->{special}->{$_}}($first) foreach keys %{$Config{$qt}->{special}};
	$REPORT .= $q->Tr( $q->th( $Config{$qt}->{display_cols} ));
	$REPORT .= $q->Tr({-class=>$class}, $q->td( [map $first->{$_}, @{$Config{$qt}->{display_cols}}] ));
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$TakeAction = 1 unless $tmp->{'Cleared'};
		$tmp->{$_} = &{$Config{$qt}->{special}->{$_}}($tmp) foreach keys %{$Config{$qt}->{special}};
		$class = $class eq 'a' ? 'b':'a';
		$REPORT .= $q->Tr({-class=>$class}, $q->td( [map $tmp->{$_}, @{$Config{$qt}->{display_cols}}] ));
	}
	$REPORT .= "</table>\n";
}

sub Create_CA_Trans
{
	my ($ipn_id, $assign_to) = @_;
	###### in here we will create a Club Account transaction out of the ipn amount
	###### and then update the ipn
	# let's wrap this all in a transaction block
	$db->{AutoCommit} = 0;
	if ( my $ipn = $db->selectrow_hashref('
		SELECT * FROM ipn WHERE ipn_id = ? AND COALESCE(cleared, FALSE) != TRUE FOR UPDATE', undef, $ipn_id) )
	{
		###### assign this ipn to the correct member
		$ipn->{id} = $assign_to;

		my $soa = PayPal::Create_SOA_Record($db, $ipn);
		$message .= $q->div('Creating Club Account transaction: ' . ($soa || 'Failed'));
		unless ($soa){
			$db->rollback;
			$db->{AutoCommit} = 1;
			return undef;
		}

		###### create a manual processing fee transaction
		$db->do("INSERT INTO soa (trans_type, amount, id, description)
			VALUES (9310, $ManualPayPalFee, ?, ?)",
			undef, $assign_to, "ref: $soa");

		my $soa_memo = "Assigned to: $assign_to, Club Account transaction ($soa) created\n";
		my $rv = $db->do("
			UPDATE ipn SET memo = ? || COALESCE(memo, ''), cleared = TRUE
			WHERE COALESCE(cleared, FALSE) != TRUE AND ipn_id = ?", undef, $soa_memo, $ipn_id);

		$message .= $q->div("Result of update to ipn record: $rv");
		if ($rv eq '1'){
			$db->{AutoCommit} = 1;
			return 1;
		}
		else {
			$db->rollback;
			$db->{AutoCommit} = 1;
			return undef;
		}
	}
	$db->{AutoCommit} = 1;
	return undef;
}

sub Display
{
	print $q->header(-charset=>'utf-8'),
	$q->start_html(
		-title=>'PayPal IPN Report',
		-encoding=>'utf-8',
		-style=>'/css/admin/generic-report.css'
	);
	###### our instructional block
	print $q->div({
		-style=>'float:right; width:40%; border:1px solid navy; padding: 0.5em;',
		-class=>'fpnotes'}, _instructions_());
       print $q->p('<b>PayPal IPN Report</b>');
	print $q->p({-class=>'message'}, $message) if $message;
	print $q->p({-class=>'error'}, $errormsg) if $errormsg;
	SearchForm();
	print $q->start_form();
	foreach ($q->param)
	{
		next if /cleared_|ipn_id_/;
		print $q->hidden($_);
	}
	print $REPORT;
	if ($TakeAction)
	{
		print $q->p({-style=>'text-align: center'}, $q->submit('Apply Updates'));
		print $q->hidden(-name=>'action', -value=>'update');
	}
	print $q->end_form;
	print $q->end_html();

}

sub DoUpdates
{
	###### we are looking for params matching cleared_xxx and ipn_id_xxx
	###### where xxx = an ipn_id record identifier
	# by reverse sorting, we are going to process ipn_id's before clears
	foreach (reverse sort $q->param)
	{
		next unless $q->param($_);	# ignore empty params
		next unless /^(ipn_id_|cleared_)(\d+)$/;
		my $ipn_id = $2;
		my $val = $q->param($_);
		if (/cleared_/ && $q->param($_) eq 'TRUE'){ MarkCleared($ipn_id); }
		elsif (/ipn_id_/){
			($val) = $db->selectrow_array('SELECT id FROM members WHERE alias = ?',
				undef, (uc $val)) if $val =~ /\D/;	###### an alpha value must be an alias
			Create_CA_Trans($ipn_id, $val);
		}
	}
}

sub InputCleared
{
	###### create a drop-down control for the cleared value if necessary
	my $tmp = shift;
	return 'Cleared' if $tmp->{'Cleared'};
	return $q->popup_menu(
		-name => "cleared_$tmp->{ipn_id}",
		-values => ['', 'TRUE'],
		-labels => {'TRUE'=>'Clear'},
		-default => $tmp->{'Cleared'},
		-class => 'action_ctl'
	);
}

sub InputID
{
	###### create an input field for the member ID as long as there isn't already a value
	###### we don't really want them changing the value do we?
	my $tmp = shift;
	return $tmp->{'Member ID'} if $tmp->{'Member ID'};
	return '' if $tmp->{'Cleared'};
	return $q->textfield(
		-name=>"ipn_id_$tmp->{ipn_id}",
		-class => 'action_ctl'
	);
}

sub MarkCleared
{
	my $ipn_id = shift;
	my $rv = $db->do('
		UPDATE ipn SET cleared = TRUE WHERE COALESCE(cleared, FALSE) != TRUE AND ipn_id = ?', undef, $ipn_id);
	$message .= $q->div("Update of record ($ipn_id) to 'Cleared' returned: $rv\n");
}

sub MungeMemo
{
	###### convert newlines to HTML breaks and also HTML escape other stuff
	my $tmp = shift;
	my $memo = $q->escapeHTML($tmp->{memo});
	$memo =~ s#\n#<br />#g;
	return $memo;
}

sub SearchForm
{
	##
	## print out the search form.
	##

       print $q->start_form,
	'<table>',
	$q->Tr( $q->td('Query Type'),
		$q->td(
			$q->popup_menu(
				-name => 'query_type',
				-values => [sort {$a<=>$b} keys %Config],
				-labels => {map {$_ , $Config{$_}->{label}} keys %Config}
	))),
# 	$q->Tr( $q->td('Date Range'), $q->td(
# 			$q->popup_menu(
# 				-name => 'date_range',
# 				-values => \@DateRanges
# 			)
# 		)
# 	),
	$q->Tr( $q->td('Start Date'), $q->td(
		$q->popup_menu(
			-name => 'start_month',
			-values => [map {sprintf '%02s', $_} (1..12)],
			-labels => {'01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'April', '05'=>'May', '06'=>'June',
				'07'=>'July', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec'},
			-default => sprintf '%02s', ($date[4] + 1)
			) .
		$q->popup_menu(
			-name => 'start_day',
			-values => [map {sprintf '%02s', $_} (1..31)],
			-default => ($date[3] -1)
			).
		$q->popup_menu(
			-name   => 'start_year',
			-values => \@year_options,
			-default => $date[5]
			))),
	$q->Tr( $q->td('End Date'), $q->td(
		$q->popup_menu(
			-name	=> 'end_month',
			-values => [map {sprintf '%02s', $_} (1..12)],
			-labels => {'01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'April', '05'=>'May', '06'=>'June',
				'07'=>'July', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec'},
			-default => sprintf '%02s', ($date[4] + 1)
			) .
		$q->popup_menu(
			-name => 'end_day',
			-values => [map {sprintf '%02s', $_} (1..31)],
			-default => ($date[3])
			).
		$q->popup_menu(
			-name => 'end_year',
			-values => \@year_options,
			-default => $date[5]
			)));
	print $q->Tr($q->td('Order by:') . $q->td(
		$q->popup_menu(
			-name=>'order_by',
			-values=>\@Global_Order_By
		) . 
		$q->popup_menu(
			-name=>'order_by_direction',
			-values=>['ASC','DESC'],
			-labels=>{'ASC'=>'Ascending', 'DESC'=>'Descending'}
		) .
		$q->div({class=>'fpnotes'}, $q->span({-class=>'lybg'},
			'"Totals for Date Range" queries will always be ordered by Date')) .
		$q->div({class=>'fpnotes'}, $q->span({-class=>'lybg'},
			'"Uncleared" queries ignore dates.'))
	)), '</table>',
	$q->submit,
      $q->end_form(), $q->hr;
}

sub _base_SQL
{
return q|
Select mc_gross AS "Gross Total", mc_fee AS "Fee Total",
	cleared AS "Cleared",
	first_name AS "First Name", last_name AS "Last Name",
	ipn_id, txn_id AS "PayPal txn_id", txn_type, memo, id AS "Member ID",
	DATE_TRUNC('second', stamp::TIMESTAMP) AS "Timestamp", payer_email, payer_id AS "PayPal ID"
FROM ipn
|;
}

sub _base_SQL_aggregates
{
return q|
SELECT SUM(ipn.mc_gross) AS "Gross Total", SUM(ipn.mc_fee) AS "Fee Total", stamp::DATE AS "Date"
FROM ipn WHERE stamp::DATE BETWEEN <STARTDATE> AND <ENDDATE>
GROUP BY stamp::DATE
|;
}

sub _init_config_
{
	###### the display_cols should exactly match the names of the columns 
	###### the array order will be the display order
%Config = (
	1 => {
		sql => _base_SQL . 'WHERE cleared IS NULL OR cleared = FALSE',
		label => 'All Uncleared',
		display_cols => ['Gross Total', "Fee Total", "First Name", "Last Name",
			'Memo', "Member ID", "Clear",'Timestamp', 'payer_email'
		#	,"PayPal ID", "PayPal txn_id"
		]
		,order_by => ['stamp', 'payer_email']
#		,force_orderby => ['stamp']
		,special => {
			'Member ID' => \&InputID,
			'Clear' => \&InputCleared,
			'Memo' =>\&MungeMemo
		}
	},
	20 => {
		sql => _base_SQL_aggregates,
		label => 'Totals for Date Range',
		display_cols => ['Gross Total', "Fee Total", "Date"],
		force_orderby => 'stamp::DATE'
	}
# 	,3 => {
# 		sql => _base_SQL_noSales,
# 		label => 'Vendors with no Sales',
# 		display_cols => ['Vendor', 'Last Changed by', 'Last Change Date', 'Notes'],
# 		order_by => ['Vendor', 'Last Change Date']
# 	}
);
	###### now we basically want clones of some keys with the exception of a query tweak
	$Config{2} = _clone_config('1');
	$Config{2}->{sql} = _base_SQL . 'WHERE cleared = TRUE AND stamp::DATE BETWEEN <STARTDATE> AND <ENDDATE>';
	$Config{2}->{label} = 'Cleared (by date range)';

	$Config{3} = _clone_config('1');
	$Config{3}->{sql} = _base_SQL . 'WHERE stamp::DATE BETWEEN <STARTDATE> AND <ENDDATE>';
	$Config{3}->{label} = 'All (by date range)';
}

sub _clone_config
{
	my %h = %{$Config{$_[0]}};
	return \%h;
}

sub _instructions_
{
return 'To apply a PayPal payment to a member`s Club Account,
enter the ID number in the text field and hit the "Apply Updates" button.
<p>To simply "Clear" an item without creating a Club Account entry,
select "Clear" from the drop-down and then update.</p>
One or more records can be processed simultaneously using these methods.
';
}

