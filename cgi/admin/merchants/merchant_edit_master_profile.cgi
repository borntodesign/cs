#!/usr/bin/perl

=head1 NAME:
merchant_edit_master_profile.cgi

	This script is used by DHS Employees to update the Merchants Master Profile information.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use ADMIN_DB;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI;
use CGI::Cookie;
use File::Copy;
use Image::Size;
use MerchantAffiliates;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
	
	$db, $CGI, $MerchantAffiliates
	
=cut

#our $ObjectName = SomeClass->new();
our $db;
our $CGI = CGI->new();
our $G_S = G_S->new();
our $MerchantAffiliates = {};

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $xml = '';
our $xsl = '';
our $configuration_xml = '';
our $langauges_xml = '';
our $languages_ref = {};
our $countries_xml = '';
our $operator = '';


	
=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


sub displayApplication
{
	my $merchant_master_record_hashref = shift;
	my $errors = shift;
	
	my $errors_xml = '';
	my $notices_xml = '';
	my $merchant_master_vendor_information_hashref = {};
	
	my $application_configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	
	$countries_xml = G_S::Get_Object($db, 10407);
	
	$languages_ref = $db->selectall_arrayref("SELECT '<' || code2 || '>' ||  description || '</' || code2 || '>'  FROM language_codes WHERE active = true ORDER BY description");
	
	foreach (@$languages_ref)
	{
	   $langauges_xml .= $_->[0];
	}
	
	$langauges_xml = "<language>$langauges_xml</language>";
	
	
	my $params = '';

	my $information_hashref_arrayref = {};
	
	
	

	
	# If there was no information passed as a parameter into this sub for "merchant_master_record_hashref"
	# retrieve the merchant master record, and member information.
	if (!$merchant_master_record_hashref)
	{
		my $merchant_member_info = {};

	
		eval{
			$MerchantAffiliates = MerchantAffiliates->new($db);
			
			$merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($CGI->param('id'));
			$merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($CGI->param('id'));
			$MerchantAffiliates->setMembers() if ! $MerchantAffiliates->{Members};
			
			$merchant_member_info = $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$merchant_master_record_hashref->{member_id}});
			
		};
		if($@)
		{
			#TODO Change this to something useful, and do it in the MAF site as well!
			print $CGI->header('text/plain');
			print $@;
			exitScript();
		}

	


		####################################################################################################################
		# Here we process field specific information, so it works with the common.xsl Style Sheet.
		####################################################################################################################
		$information_hashref_arrayref->{url_language_pref} = lc($merchant_master_record_hashref->{url_language_pref});
		$information_hashref_arrayref->{url_language_pref} = [$information_hashref_arrayref->{url_language_pref}];
		
		$merchant_master_record_hashref->{discount} =~ s/0*$//; 
		
		foreach (keys %{$application_configuration->{admin_exception_percentage_of_sale}})
		{
			
			if ($merchant_master_record_hashref->{discount} eq $application_configuration->{admin_exception_percentage_of_sale}->{$_}->{val})
			{
				$information_hashref_arrayref->{exception_percentage_of_sale} = [$merchant_master_record_hashref->{discount}];
				last;
			}
			
		}
		
		$information_hashref_arrayref->{percentage_of_sale} = [$merchant_master_record_hashref->{discount}] if (! defined $information_hashref_arrayref->{exception_percentage_of_sale});
		

		$information_hashref_arrayref->{language} = lc($merchant_member_info->{language_pref});
			
		$information_hashref_arrayref->{language} = [$information_hashref_arrayref->{language}];
			
		$information_hashref_arrayref->{home_phone} = [$merchant_member_info->{phone}];
		
		$information_hashref_arrayref->{spid} = [$merchant_member_info->{spid}];
		
		my $lp = G_S::Get_LangPref();
#		$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?', undef, ($merchant_master_record_hashref->{business_type}));
		$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($lp, $merchant_master_record_hashref->{business_type}));

		
		
		####################################################################################################################
		
		$merchant_master_vendor_information_hashref->{coupon} =~ s/>/\/>/ if (exists $merchant_master_vendor_information_hashref->{coupon} && $merchant_master_vendor_information_hashref->{coupon} && $merchant_master_vendor_information_hashref->{coupon} !~ /\/\s*>/);
		
	}
	
	

		
	#Make the information XML friendly
	foreach (keys %$merchant_master_record_hashref)
	{
		next if (! defined $merchant_master_record_hashref->{$_} || !$_);
			
		$information_hashref_arrayref->{$_} = [$merchant_master_record_hashref->{$_}];
	}
		
	my $lp = G_S::Get_LangPref();
#	$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?', undef, ($merchant_master_record_hashref->{business_type}));
	$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($lp, $merchant_master_record_hashref->{business_type}));

	$params = XML::Simple::XMLout($information_hashref_arrayref, RootName => "params" );
	
	
	if ($errors)
	{
		$errors_xml = XML::Simple::XMLout($errors, RootName => "errors" );
	}


	if ($CGI->param('updated'))
	{
		$notices_xml = XML::Simple::XMLout({updated=>['updated']}, RootName => "notices" );
	}
	
	
	#TODO: Remove this code, and comments before pushing to production.
#	use Data::Dumper;
#	print $CGI->header('text/plain');
#	print "test\n\n";
#	
#	eval{
#		
#		my $config = XML::Simple::XMLin("<base>$configuration_xml</base>");
#		
#		
#		
#		foreach (keys %{$config->{exception_percentage_of_sale}})
#		{
#			$config->{percentage_of_sale}->{$_} = $config->{exception_percentage_of_sale}->{$_};
#		}
#		
#		delete $config->{exception_percentage_of_sale};
#		
#		
#		my $config_new_xml = XML::Simple::XMLout($config);
#		
#		print "\n\n";
#		print Dumper($config) . "\n\n";
#		
#		print $config_new_xml . "\n\n";
#		
#		
#	};
#	if($@)
#	{
#		print $@;
#	}
#	
#	exitScript();
	
		
	my	$output_xml = <<EOT;
		<base>
			
			<menus>
				$countries_xml
				
				$configuration_xml
				
				$langauges_xml
				
				<status>
					<null val="0">Inactive</null>
					<two val="2">Approved</two>
					<one val="1">Active</one>
				</status>
				
			</menus>
			
			<data>
				$errors_xml
				$notices_xml
			</data>
			<banner>
				$merchant_master_vendor_information_hashref->{coupon}
			</banner>
			
			$params
			
			$xml
			
		</base>
		
EOT
	
	
	
	
	
	
	eval{
		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $output_xml);
		
#		print $CGI->header('text/plain');
#		print $output_xml;
		
	};
	if($@)
	{
		print $CGI->header('text/plain');
		print $@;
		warn $FindBin::RealScript . ' - ' . $@;
		
#		print "\n\n";
#		
#		print $xsl;
#		print "\n\n";
#		print $output_xml;
		
		
	}
	
	exitScript();
}

sub processApplication
{
	
	my %merchant_information = ();
	
	my %missing_fields = ();
	my $MerchantAffiliates = {};
	
	my $cookie = {};
	
	my $application_configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	my %merchant_banner_info = ('location' => $application_configuration->{banner}->{location});
	
	my %required_fields = 
	(
			1	=>	[
					'merchant_type',
					
					'business_type',
					'business_name',
					
					'firstname1',
					'lastname1',
					
					'address1',
					'city',
					'state',
					'country',
					
					'phone',
					
					'language',
					
					'emailaddress1',
					#'emailaddress2',
					
					#'username',
					'password',
					'status'
			],
	);




			# Assign the information the merchant submitted to the "$merchant_information" hash.
			foreach my $key ($CGI->param())
			{
				next if $key =~ /submit|biz_description/i;
						
																	#$CGI->param($key) &&
				$merchant_information{$key} = $CGI->param($key) if $key !~ /^_|^x$|^y$/;
				
			}


		eval{


	
			
			$MerchantAffiliates = MerchantAffiliates->new($db);
			
			my $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($CGI->param('id'));
			
			
			$MerchantAffiliates->setMembers()  if ! $MerchantAffiliates->{Members};
			%merchant_banner_info = ('location' => $application_configuration->{banner}->{location});
			
			#																			Y										N																																																																																																																																														Y																																																																																																																																													
			$missing_fields{emailaddress1_preexisting_update} = ['Missing Field'] if ($merchant_information{emailaddress1} && ! $MerchantAffiliates->{Members}->checkEmailDuplicates($merchant_information{emailaddress1}, $merchant_master_record_hashref->{member_id}) && ! $MerchantAffiliates->{Members}->checkEmailDomain($merchant_information{emailaddress1}));
			
			$missing_fields{emailaddress2_preexisting_update} = ['Missing Field'] if ($merchant_information{emailaddress2} && ! $MerchantAffiliates->{Members}->checkEmailDuplicates($merchant_information{emailaddress2}, $merchant_master_record_hashref->{member_id}) && ! $MerchantAffiliates->{Members}->checkEmailDomain($merchant_information{emailaddress2}));
			
			if ($merchant_information{emailaddress1})
			{
	 			$missing_fields{emailaddress1_preexisting_update} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress1}, 1));
			}
			
			if($merchant_information{emailaddress2})
			{
 				$missing_fields{emailaddress2_preexisting_update} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress2}, 1));
			}
			
			$missing_fields{emailaddress_should_not_match} = ['Missing Field'] if ($merchant_information{emailaddress1} && $merchant_information{emailaddress2} && $merchant_information{emailaddress1} eq $merchant_information{emailaddress2});
			
			if (! $merchant_information{exception_percentage_of_sale} && ! $merchant_information{percentage_of_sale})
			{
				#$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
				$missing_fields{percentage_of_sale} = ['Missing Field'];
			}
#			elsif($merchant_information{exception_percentage_of_sale} && $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale_not_both} = ['Missing Field'];
#			}
			
			#TODO: Make an exception for the current individual
			if (defined $merchant_information{username})
			{
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingUsernameDuplicate($merchant_information{username}));
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkMasterUsernameDuplicates($merchant_information{username}));
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkLocationUsernameDuplicates($merchant_information{username}));
			}
			
			if (exists $merchant_information{cap} && $merchant_information{cap} =~ /\D/)
			{
				$merchant_information{cap} =~ s/\.\d*$//;
				$merchant_information{cap} =~ s/\D//g;
			}
			
			if (exists $merchant_information{banner} && $merchant_information{banner})
			{
				
				$merchant_banner_info{temp_banner_file} = $CGI->tmpFileName($merchant_information{banner});
				
				$merchant_banner_info{light_weitht_file_handler} = $CGI->upload('banner');
				
				($merchant_banner_info{width}, $merchant_banner_info{height}, $merchant_banner_info{mime_type}) = Image::Size::imgsize($merchant_banner_info{light_weitht_file_handler});
				
				$missing_fields{banner_wrong_file_type} = ['Missing Field'] if $merchant_banner_info{mime_type} !~ /GIF|JPEG|JPG|PNG/i;
				$missing_fields{banner_wrong_file_width} = ['Missing Field'] if $merchant_banner_info{width} > $application_configuration->{'banner'}->{'max_width'};
				$missing_fields{banner_wrong_file_height} = ['Missing Field'] if $merchant_banner_info{height} > $application_configuration->{'banner'}->{'max_height'};
				$missing_fields{banner_size_to_large} = ['Missing Field'] if (-s $merchant_banner_info{temp_banner_file} > (1024 * $application_configuration->{'banner'}->{'max_size'}));
				
			}
			
			
			$missing_fields{status} = ['Missing Field'] if $merchant_information{status} !~ /^\d$/;
			
		};
		if($@)
		{
			warn "Error: $FindBin::RealBin / $FindBin::RealScript - There was an error instanciating the Members Class. " . $@;
			
			$missing_fields{script_error} = ['Script Error'];
		}	
	
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
#		use Data::Dumper;
#		print $CGI->header('text/plain');
#		print Dumper(%missing_fields);
#		exitScript();
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}
	
	
	eval{
		
		#processing for the merchant banner.
		if($merchant_information{banner})
		{
			$merchant_banner_info{mime_type} = lc($merchant_banner_info{mime_type});
			
			die "We can not write to the folder the merchant banners are stored in. $application_configuration->{banner}->{location} " if(! -W $application_configuration->{banner}->{location} );
		
			$merchant_banner_info{io_handler} = $merchant_banner_info{light_weitht_file_handler}->handle;
			die 'The banner could not be coppied.' if (! File::Copy::copy($merchant_banner_info{io_handler}, "$application_configuration->{banner}->{location}ma_" . $CGI->param('id') . ".$merchant_banner_info{mime_type}"));
				
			$merchant_information{banner} = '<img src="' . $application_configuration->{banner}->{web_location} . 'ma_' . $CGI->param('id') . '.' . $merchant_banner_info{mime_type} . '" width="' . $merchant_banner_info{width} . '" height="' . $merchant_banner_info{height} . '" />';
		}
		
		
		$merchant_information{delete_banner} = "$application_configuration->{banner}->{location}ma_" . $CGI->param('id') . ".*" if ($merchant_information{delete_banner});
		
		
		
		$merchant_information{operator} = $operator;
		
		die ' There was an error updating the Master Information ' if ! $MerchantAffiliates->updateMerchantMasterInformation($CGI->param('id'), \%merchant_information);
		
		
		#Remove this, it causes an error when the XML XSL translation, the use needs to reupload the banner if there is an error.
		delete $merchant_information{banner} if $merchant_information{banner};
		
	};
	if($@)
	{
		delete $merchant_information{banner} if $merchant_information{banner};
		$missing_fields{script_error} = ['Script Error'];
		
		warn $FindBin::RealScript . ' : ' . $@;
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
		
	}
	
	my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated') . '&' . URI::Escape::uri_escape('edit') . '=' . URI::Escape::uri_escape('edit') . '&' . URI::Escape::uri_escape('id') . '=' . URI::Escape::uri_escape($CGI->param('id'));
	
	print $CGI->redirect(-uri=>$url);
	exitScript();
}

sub displayUnavailable
{
	
	
	print $CGI->redirect(-uri=>'/maf/profile/unavailable');
	exitScript();
	
	
	
	
}
####################################################
# Main
####################################################

$operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 11112009)
{
	
	$xml = G_S::Get_Object($db, 10694);
	$xsl = G_S::Get_Object($db, 10723);
	
	$configuration_xml = G_S::Get_Object($db, 10681);

} else {
	
	$xml = G_S::Get_Object($db, 10679);
	$xsl = G_S::Get_Object($db, 10711);
	
	$configuration_xml = G_S::Get_Object($db, 10677);

}

if(! $CGI->param('updated') && $CGI->param && ! $CGI->param('edit'))
{
	processApplication();
	
} else {
	displayApplication();
}


exitScript();

__END__

=head1
CHANGE LOG

	YYYY-MM-DD	PERSONS NAME	DESCRIPTION OF CHANGES
	2010-07-26	Bill MacArthur	Changed query using ypcats2naics to use business_codes() for translatable business types

=cut


