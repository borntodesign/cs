#!/usr/bin/perl

=head1 NAME:
pending_merchant_admin.cgi

	This displays a search form that allows one to search for pending merchants, 
	display the specified pending merchant(s), accept pending merchants into our program,
	and links are also provided to edit pending.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, FindBin, DHSGlobals, CGI, DB, DB_Connect, G_S, URI::Escape

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use G_S;
use MailTools;
use MerchantAffiliates;
use Payment;
use RewardCards;
use XML::Simple;
use Data::Dumper;

#use URI::Escape;
use Lingua::EN::NameCase 'NameCase';

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my @notices = ();
my @warnings = ();
my @errors = ();
our $url = $CGI->url();
#$url =~ s/\.cgi$/_submit\.cgi/;

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}


sub caseInsensitive { "\L$a" cmp "\L$b" }


=head2
processTemplate

=head3
Description:

	Process a template, replace the place holders with content.

=head3
Params:

	params	hashref	The keys should match the placeholders (ex. %%PLACE_HOLDER%%, $params->{PLACE_HOLDER}='The content that replaces the %%PLACE_HOLDER%%.')
	
	template	string	The document that is to be processed.

=head3
Returns:

	string	The HTML Document 

=cut

sub processTemplate
{
	my $params = shift;
	my $template = shift;

	foreach my $key (keys %$params)
	{
		$template =~ s/%%$key%%/$params->{$key}/g;
	}

	$template =~ s/%%.*%%//g;

	return $template;
}

=head2
displaySearchForm

=head3
Description:

	Display the form we use to search for pending Merchants

=head3
Params:

	$params	hashref	I'm not sure what I'm going to do with this yet.

=head3
Returns:

	string	The HTML Document 

=cut

sub displaySearchForm
{

	my $params = shift;
	
	my %field_names = ();
	my $select_box_values = [];
	
	my $MerchantAffiliates = {};

	my @countries = ();
	my @provinces = ();
	my @business_types = ();
	my @languages = ();
	my @packages = ();
	
	$params->{ERRORS} = '' if ! defined $params->{ERRORS};
	$params->{WARNINGS} = '' if ! defined $params->{WARNINGS};
	
	
	$params->{NOTICES} = '' if ! defined $params->{NOTICES};
	
	
	
	
	eval{
		
		$MerchantAffiliates = MerchantAffiliates->new($db);
		@countries = $MerchantAffiliates->retrievePendingMerchantCountries();
		@provinces = $MerchantAffiliates->retrievePendingMerchantProvinces();
		@packages = $MerchantAffiliates->retrievePendingMerchantPackages();
	};
	if($@)
	{
		
		my $error_message = $@;
		$params->{ERRORS} .= "There was a fatal error instanciating the MerchantAffiliates class, or retriving ...";
		
	}
	
	
	
	
	my $search_value = ($CGI->param('search_field') =~ /country|state|merchant_package/) ? $CGI->param('search_field'): 'search_value';
	
	$params->{WARNINGS} .= q(<span class="warnings">A Search String is required to fulfill your request.</span><br />) if ($CGI->param('search_field') && ! $CGI->param($search_value));

	
	$params->{WARNINGS} .= q(<span class="warnings">A Search Field is required to fulfill your request.</span><br />) if($CGI->param($search_value) && ! $CGI->param('search_field'));
	
	$params->{SEARCH_VALUE} = q( value=") . $CGI->param($search_value) . q(" );
	
	$params->{TITLE} = 'Pending Merchant Affiliates Administration' if ! exists $params->{TITLE};
	$params->{MESSAGE} = 'Pending Merchant Affiliates Administration' if ! exists $params->{MESSAGE};
	$params->{ACTION} = $url if ! exists $params->{ACTION};
	
	$params->{ERRORS} .= q(<span class="errors">) . $CGI->param('errors') . '</span>' if $CGI->param('errors');
	$params->{WARNINGS} .= q(<span class="warnings">) . $CGI->param('warnings') . '</span>' if $CGI->param('warnings');
	
	$params->{NOTICES} = q(<span class="notices">) . $params->{NOTICES} . '</span><br />' if $params->{NOTICES};
	$params->{NOTICES} .= q(<span class="notices">) . $CGI->param('notices') . '</span>' if $CGI->param('notices');
	
	
	#TODO: Extract the field list from the "pending_merchant" table, adjust the names for display, then build a select box.
	
	my $sth = $db->column_info( undef, 'public', 'merchant_pending', "%");
	
	my @columns = ();
	while(my $rows = $sth->fetchrow_hashref())
	{
		next if $rows->{COLUMN_NAME} =~ /^status$|^stamp$/;
		$rows->{COLUMN_NAME} =~ s/"//g;
		push @columns, $rows->{COLUMN_NAME};
	}

	
	my @sorted_columns = sort caseInsensitive @columns;
	
	unshift @sorted_columns, '', 'business_name', 'id', 'referral_id', '';
	
	foreach (@sorted_columns)
	{
		my $human_readable_name = $_;
		
		$human_readable_name =~ s/pay_/CC_/g;
		
		$human_readable_name =~ s/_/\ /g;
		
		$field_names{$_} = Lingua::EN::NameCase::NameCase($human_readable_name);
		
		push @$select_box_values, $_;
	
	}
	
	$field_names{''} = '';
	
	my @country_values = ();
	my %country_names = ();
	foreach (@countries)
	{
		$country_names{$_->{'country'}} = $_->{'country_label'};
		push @country_values, $_->{'country'};
	}
	
	my @province_values = ();
	my %province_name = ();
	foreach (@provinces)
	{
		my $state = $_->{'state'};
		
		$state =~ s/\s*$//;
		$state =~ s/^\s*//;
		$province_name{$_->{'state'}} = " $state ($_->{'state_label'})";
		push @province_values, $_->{'state'};
	}
	
	
	my @package_values = ();
	my %package_name = ();
	foreach (@packages)
	{
		next if ! $_->{'name'};
		$package_name{$_->{'merchant_package'}} = " $_->{'name'} ($_->{'package_type'})";
		push @package_values, $_->{'merchant_package'};
	}
	
	my @status_values = (0,1,'both');
	my %status_name = (
						'both'=>'Both',
						'0'=>'Pending',
						'1'=>'Approved'
					);

#	warn "\n\n \@columns: ";
#	warn Dumper(@columns);
#	
#	warn "\n\n \@sorted_columns: ";
#	warn Dumper(@sorted_columns);
#	
#	warn "\n\n field_name: ";
#	warn Dumper(%field_names);
	
	
	$params->{SEARCH_FIELDS} = $CGI->popup_menu(
											-name=>'search_field', 
											-id=>'search_field', 
											-values=>$select_box_values,
											-labels=>\%field_names,
											-default=>$CGI->param('search_field')
										);
	
	$params->{COUNTRY_LIST} = $CGI->popup_menu(
											-name=>'country', 
											-values=>\@country_values,
											-labels=>\%country_names,
											-style=>'display: none;',
											-default=>$CGI->param('county')
										);
	
	$params->{PROVINCE_LIST} = $CGI->popup_menu(
											-name=>'state', 
											-values=>\@province_values,
											-labels=>\%province_name,
											-style=>'display: none;',
											-default=>$CGI->param('state')
										);
	
	$params->{MERCHANT_PACKAGE_LIST} = $CGI->popup_menu(
											-name=>'merchant_package', 
											-values=>\@package_values,
											-labels=>\%package_name,
											-style=>'display: none;',
											-default=>$CGI->param('merchant_package')
										);
	
	$params->{STATUS} = $CGI->popup_menu(
											-name=>'status', 
											-values=>\@status_values,
											-labels=>\%status_name,
											-default=>$CGI->param('status')
										);
											
	my $html_form = G_S::Get_Object($db, 10720); 
	
	print $CGI->header('text/html');
	
	print processTemplate($params, $html_form);
	
	exitScript();
	
}


=head2
displayPendingMerchantList

=head3
Description:

	Display the form we use to Approve Pending Merchants

=head3
Params:

	$params	hashref	{
						search_field	string	The field to search for the merchants with,
						search_value	string	The value to search the search_field with.
					}

=head3
Returns:

	string	The HTML Document 

=cut

sub displayPendingMerchantList
{
		my $param = shift;
		
		
		
		my $MerchantAffiliates = MerchantAffiliates->new($db);
		my $Payment = Payment->new($db);
		my $RewardCards = RewardCards->new($db);
		
		my $status = $param->{status};
		
		
		my $potential_merchant = $MerchantAffiliates->retrievePendingMerchant({$param->{search_field}=>$param->{search_value},status=>$status});
		my $reward_cards_by_country_province = $RewardCards->getRewardsCardCache();
		
		my $merchant_config_hashref = $MerchantAffiliates->retrieveMerchantConfigHashRef();
		
		displaySearchForm({NOTICES=> 'There were no results, try another Search String.'}) if (! $potential_merchant);
		
		my $payment_methods = $Payment->getPaymentMethods();
		
		$url =~ s/\.cgi$/_submit\.cgi/;
		
		my %template_values = (TITLE=>'Pending Merchant List',MESAGE=>'',ACTION=>$url);
		
		$potential_merchant = [$potential_merchant] if(ref($potential_merchant) eq 'HASH');
		
		$template_values{LIST} = '';
		
		foreach (@$potential_merchant)
		{
			my $merchant_checkbox_name = "merchant__$_->{id}";
			my $merchant_checkbox_delete_name = "delete__$_->{id}";
			my $merchant_checkbox_pay_ongoing_fees_name = "payongoing__$_->{id}";
			my $merchant_checkbox_pay_ongoing_annual_fees_name = "payongoingannual__$_->{id}";
			my $label = "row_$_->{id}";
			my $edit_label = $label . '_edit';
			my $headers_label = $label . '_headers';
			my $spacer_label = $label . '_spacer';
			my $information_label = $label . '_information';
			
			my $accepted = $_->{status} ? 'Accepted' : qq(<input type="checkbox" name="$merchant_checkbox_name" id="$merchant_checkbox_name" value="$_->{id}" >);
			
			my $package_percentage;
			
			if($_->{exception_percentage_of_sale} || $_->{percentage_of_sale})
			{
				$package_percentage = $_->{exception_percentage_of_sale} ? 'Exception ' . $_->{exception_percentage_of_sale} * 100 . '%' : $_->{percentage_of_sale} * 100 .'%';				
			}

			
			$package_percentage = $_->{percentage_off} * 100 . '%' if ($_->{percentage_off});
			
			$package_percentage = 'Set Discount ' . $_->{flat_fee_off} if ($_->{flat_fee_off});
			
			$package_percentage = 'BOGO ' . $_->{special} if ($_->{special});
			
			my $merchant_package_name = $MerchantAffiliates->getMerchantPackageNameById($_->{merchant_package},1);
			$merchant_package_name .= $MerchantAffiliates->getMerchantTypeNameById($_->{discount_type}) eq 'offline' ? ' (Tracked)':' (Direct)';
			
			$_->{payment_method} = uc($_->{payment_method});
			
			my $merchants_payment_information = '';
			
			
			
			if($_->{payment_method} ne 'CA')
			{
				$merchants_payment_information = $_->{payment_method}; #$payment_methods->{$_->{payment_method}};
			}
			else
			{
				
				my $account_balance = $db->selectcol_arrayref('SELECT account_balance FROM soa_account_balance_payable WHERE id= ?', undef, ($_->{ca_number}));
				
				
				
				#TODO: Figure the merchant package cost, and decide weather or not they can be approved.
				#TODO: Change the 50 to use the merchant seed price.
				my $merchants_package_price = $MerchantAffiliates->retrieveMerchantSeedCost($_->{merchant_type}) + $MerchantAffiliates->retrieveMerchantPackageCost($_->{merchant_type},$_->{merchant_package},$_->{country});
				
				my $able_to_pay = (defined $merchants_package_price && exists $account_balance->[0] && $merchants_package_price <= $account_balance->[0]) ? 'Yes': 'No';
				
				$merchants_payment_information = qq(
					$_->{payment_method}: 
					<a href='/cgi/admin/SOAadmin.cgi?id=$_->{ca_number}' target='#'>
						$_->{ca_number}
					</a> 
					(Able to pay: $able_to_pay) <br />
					Pay Ongoing Monthly Fees:
					<input type='checkbox' name='$merchant_checkbox_pay_ongoing_fees_name' id='$merchant_checkbox_pay_ongoing_fees_name' value='$_->{id}'>
					<br />
					Pay Ongoing Annual Fees:
					<input type='checkbox' name='$merchant_checkbox_pay_ongoing_annual_fees_name' id='$merchant_checkbox_pay_ongoing_annual_fees_name' value='$_->{id}'>
					
					); #"$payment_methods->{$_->{payment_method}}: <a href='/cgi/admin/SOAadmin.cgi?id=$_->{ca_number}' target='#'>$_->{ca_number}</a> (Able to pay: $able_to_pay)";
				
			}
			
			my $reward_cards = '';
			
			#Figure out how 
			{
				my $province = $_->{state} ? uc($_->{state}) : undef;
				my $country = $_->{country} ? uc($_->{country}) : undef;
				my $merchants_in_a_province = $_->{'number_of_merchants_in_a_province'} ? $_->{'number_of_merchants_in_a_province'}: 0;
				my $reward_cards_in_their_province = ($country && $province && $reward_cards_by_country_province->{$country}->{$province}) ? $reward_cards_by_country_province->{$country}->{$province} : 0;
				
				#warn "province: $province - country: $country - merchants_in_a_province: $merchants_in_a_province - merchant_config_hashref->{'reward_cards'}->{'cards_to_merchants'}: $merchant_config_hashref->{'reward_cards'}->{'cards_to_merchants'} - reward_cards_by_country_province->{country}->{province}: " . $reward_cards_by_country_province->{$country}->{$province} ;
				
				#warn Dumper($reward_cards_by_country_province->{$country});
				
				
					$reward_cards = $reward_cards_in_their_province - $merchants_in_a_province * $merchant_config_hashref->{'reward_cards'}->{'cards_to_merchants'};
				
					$reward_cards += -50 if $reward_cards <= 0;

					$reward_cards .= ' First Merchant' if ! $merchants_in_a_province;
				
				
			}	

			
			
			$template_values{LIST} .=<<EOT;
				<tr>
					<td width="13" valign="top">
						<a href="#" onClick="expandContract('$label', this);return false;" id="$label">+</a>
					</td>
					<td width="13" valign="top">
						<a href="/cgi/admin/merchants/update_pending_merchant.cgi?id=$_->{id}" target="#" id="$edit_label">EI</a>
						<a href="/cgi/admin/merchants/update_pending_merchant_payment_information.cgi?id=$_->{id}" target="#" id="$edit_label">EP</a>
					</td>
					<td width="117" valign="top">
						$_->{business_name}
					</td>
					<td valign="top">
						$_->{firstname1} $_->{lastname1}
					</td>
					<td valign="top">
						$merchants_payment_information
					</td>
					<td valign="top">
						$package_percentage
					</td>
					<td valign="top">
						$merchant_package_name
					</td>
					<td valign="top">
						$reward_cards
					</td>
					<td valign="top">
						$accepted
					</td>
					<td valign="top">
						<input type="checkbox" name="$merchant_checkbox_delete_name" id="$merchant_checkbox_delete_name" value="$_->{id}">
					</td>
				</tr>
				<tr id="$headers_label" style="display: none;">
					<th colspan="2"></th>
					<th colspan="3" align="left">Address</th>
					<th colspan="2" align="left">Business Phone</th>
					<th colspan="2" align="left">&nbsp;</th>
				</tr>
				<tr id="$information_label" style="display: none;">
					<td colspan="2"></td>
					<td colspan="3">
						$_->{address1}; $_->{city}, $_->{state} $_->{postalcode} $_->{country}
					</td>
					<td colspan="3">
						$_->{business_phone}
					</td>
					<td colspan="2"></td>
				</tr>
				<tr id="$spacer_label">
					<td colspan="9" style="height: 5px;"></td>
				</tr>
                     
EOT
 
                
                     
		}
		
		
		
	my $html_form = G_S::Get_Object($db, 10719);


	print $CGI->header('text/html');

	print processTemplate(\%template_values, $html_form);
	
#		print $CGI->header('text/plain');		
#		print Dumper($param) . "\n\n";
#		print Dumper($potential_merchant) . "\n\n";
		
		
		
		exitScript();
	
}

####################################################
# Main
####################################################
my $operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}

#unless ($db = DB_Connect('merchants_admin')){exit}

$db->{RaiseError} = 1;



eval{
	
	my $search_value = ($CGI->param('search_field') =~ /country|state|merchant_package/) ? $CGI->param('search_field'): 'search_value';
	
	if($CGI->param('search_field') && $CGI->param($search_value) )
	{
		
		
		
		displayPendingMerchantList(
			{
				search_field=>$CGI->param('search_field'),
				search_value=>$CGI->param($search_value),
				status=>$CGI->param('status')
			}
		);
		
	}
	else
	{
		displaySearchForm();
	}
	
	
	


};
if($@)
{
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


