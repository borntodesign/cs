#!/usr/bin/perl -w

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use MailTools;
use XML::local_utils;
use MerchantAffiliates;
use G_S;
use DHS::UrlSafe;
#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
my $MerchantAffiliates = {};
my $CGI = CGI->new();
our $db = {};
my $DHS_URLSafe = {};


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut


my $pending_id = $CGI->param('id') ? $CGI->param('id') : undef;

my $xml = '';

my $xsl = ''; 


my $configuration_xml = '';
	
my $data = '';

my $messages = {};
my $messages_xml = '';

my $langauges_xml = '';

my $params = '';
my $pricing_xml = '';
my $cc_expiration_dates_xml = G_S::buildCreditCardExpirationDate();
my $merchant_information = {};
my $countries_xml = '';
my $status_xml = '<status><zero>Pending</zero><one>Accepted</one></status>';

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################
my $operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

eval{
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	$configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
	$DHS_URLSafe = DHS::UrlSafe->new();
	
	$xml = G_S::Get_Object($db, 10680);
		
	$xsl = G_S::Get_Object($db, 10722); 
	
	$countries_xml = G_S::Get_Object($db, 10407);
	
	
	my $languages_ref = $db->selectall_arrayref("SELECT '<' || code2 || '>' ||  description || '</' || code2 || '>'  FROM language_codes WHERE active = true ORDER BY description");
	
	foreach (@$languages_ref)
	{
	   $langauges_xml .= $_->[0];
	}
	
	$langauges_xml = "<language>$langauges_xml</language>";
	
	
	$messages = $DHS_URLSafe->decodeHashOrArray($CGI->param('data')) if($CGI->param('data'));
		
	if(exists $messages->{params})
	{
		$merchant_information = $messages->{params};
		delete $messages->{params};
	}
	else
	{
		$merchant_information = $MerchantAffiliates->retrievePendingMerchant({id=>$pending_id});
		$merchant_information->{merchant_package} = 0 if ! $merchant_information->{merchant_package};
	}
	
	
	
	if(exists $merchant_information->{status} && $merchant_information->{status} =~ /\d|^$/ )
	{
		$merchant_information->{status} = $merchant_information->{status}?'one':'zero';
	}

	my $information_hashref_arrayref = {};
		
	foreach (keys %$merchant_information)
	{
		$information_hashref_arrayref->{$_} = [$merchant_information->{$_}];
	}
		
	$information_hashref_arrayref->{percentage_of_sale}->[0] =~ s/0*$// if $merchant_information->{percentage_of_sale};
	$information_hashref_arrayref->{exception_percentage_of_sale}->[0] =~ s/0*$// if $merchant_information->{exception_percentage_of_sale};
	
#	$information_hashref_arrayref->{language_pref}->[0] = uc($information_hashref_arrayref->{language_pref}->[0]) if $information_hashref_arrayref->{language_pref}->[0];
#	$information_hashref_arrayref->{website_language_pref}->[0] = uc($information_hashref_arrayref->{website_language_pref}->[0]) if $information_hashref_arrayref->{website_language_pref}->[0];	
	
	my $language_preference = G_S::Get_LangPref();
	$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($language_preference, $merchant_information->{business_type})) if $merchant_information->{business_type}; 

#	$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?',undef,($merchant_information->{business_type})) if $merchant_information->{business_type};
	
	$information_hashref_arrayref->{submit_location} = [$CGI->url()];
	
	$information_hashref_arrayref->{submit_location}->[0] =~ s/\.cgi$/_submit.cgi/; 
	
	
	
	my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
			
	my $country_code_lower_case = lc($merchant_information->{country});
	
#			#If it is a regular price country convert the regular pricing
#			if(exists $pricing->{regular_price_countries}->{$merchant_information->{merchant_type}}->{$merchant_information->{$country_code_lower_case}})
#			{
#				
#				$pricing->{prices}->{converted}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{bronze};
#				1 while $pricing->{prices}->{converted}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{silver};
#				1 while $pricing->{prices}->{converted}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{gold};
#				1 while $pricing->{prices}->{converted}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#			} else {
#				
#				$pricing->{prices}->{converted}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{bronze};
#				1 while $pricing->{prices}->{converted}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{silver};
#				1 while $pricing->{prices}->{converted}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{gold};
#				1 while $pricing->{prices}->{converted}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#			}
	
				#If it is a regular price country convert the regular pricing
			if(defined $country_code_lower_case && exists $merchant_information->{$country_code_lower_case} && exists $pricing->{regular_price_countries}->{offline}->{$merchant_information->{$country_code_lower_case}})
			{

						$pricing->{prices}->{converted}->{offline}->{free} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{regular}->{free};
						1 while $pricing->{prices}->{converted}->{offline}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{regular}->{bronze};
						1 while $pricing->{prices}->{converted}->{offline}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{regular}->{silver};
						1 while $pricing->{prices}->{converted}->{offline}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{regular}->{gold};
						1 while $pricing->{prices}->{converted}->{offline}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			} else {

						$pricing->{prices}->{converted}->{offline}->{free} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{sale}->{free};
						1 while $pricing->{prices}->{converted}->{offline}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{sale}->{bronze};
						1 while $pricing->{prices}->{converted}->{offline}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{sale}->{silver};
						1 while $pricing->{prices}->{converted}->{offline}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{sale}->{gold};
						1 while $pricing->{prices}->{converted}->{offline}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			}
			
			if(defined $country_code_lower_case && exists $merchant_information->{$country_code_lower_case} && exists $pricing->{regular_price_countries}->{offline_rewards}->{$merchant_information->{$country_code_lower_case}})
			{
						$pricing->{prices}->{converted}->{offline_rewards}->{free} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{free};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{basic} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{basic};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{bronze};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{silver};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{gold};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{platinum} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{regular}->{platinum};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			} else {
						$pricing->{prices}->{converted}->{offline_rewards}->{free} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{free};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{basic} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{basic};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
												
						$pricing->{prices}->{converted}->{offline_rewards}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{bronze};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{silver};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{gold};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{platinum} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{sale}->{platinum};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			}
			
			$pricing->{prices}->{converted}->{offline}->{monthly} = sprintf "%.2f USD", $pricing->{prices}->{offline}->{monthly};
			1 while $pricing->{prices}->{converted}->{offline}->{monthly} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{free} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{free};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{basic} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{basic};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{bronze} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{bronze};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{silver} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{silver};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{gold} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{gold};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{platinum} = sprintf "%.2f USD", $pricing->{prices}->{offline_rewards}->{monthly}->{platinum};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			
	
	$configuration_xml .= XML::Simple::XMLout($pricing->{prices}->{converted}, RootName => "converted", NoAttr => 1);
	
	$params = XML::Simple::XMLout($information_hashref_arrayref, RootName => "params" );
	
	$messages_xml = XML::Simple::XMLout($messages, RootName => 'data', NoAttr => 1) if (ref($messages) eq 'HASH');




};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}

	


	$xml = <<EOT;

	<base>
		
		<menus>
			$countries_xml
			
			$configuration_xml
			
			$langauges_xml
			
			$cc_expiration_dates_xml
			
			$status_xml
		
		</menus>
		
		$messages_xml
		
		$params
		
		$xml
		
	</base>
	
EOT


print $CGI->header(-charset=>'utf-8');
print XML::local_utils::xslt($xsl, $xml);

#print $CGI->header('text/plain');
#print $xml;

exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


