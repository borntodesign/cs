#!/usr/bin/perl

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use MailTools;
use G_S;
use MerchantAffiliates;
use DHS::UrlSafe;
use URI::Escape;
use CreditCard;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $MerchantAffiliates = {};
my $DHS_UrlSafe = DHS::UrlSafe->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $url;
my %url_query_strings = ();
my $messages = {};


=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################
my $operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

$url = $CGI->url();
$url =~ s/_submit\.cgi$/\.cgi/;
					
eval{
					
					$MerchantAffiliates = MerchantAffiliates->new($db);
					$MerchantAffiliates->setMembers();
					
					 my %pending_merchant_info = ();

					
					if($CGI->param('payment_method'))
					{
						$pending_merchant_info{payment_method} =  $CGI->param('payment_method');
					}
					else
					{
						$messages->{warnings}->{payment_method} = ['The Payment Method is a required field.'];
					}
					
					if($CGI->param('ca_number') && $pending_merchant_info{payment_method} eq 'ca')
					{
						$pending_merchant_info{ca_number} = $CGI->param('ca_number');
						$pending_merchant_info{ca_number} =~ s/\D//g;
						$pending_merchant_info{ca_number} =~ s/^0*//;
						$messages->{warnings}->{member_id_invalid} = ['The Member ID is invalid.'] if ($pending_merchant_info{ca_number} && ! $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$pending_merchant_info{ca_number}, field=>'id'}));
					}
					
					
					if($CGI->param('if_number') && $pending_merchant_info{payment_method} eq 'if')
					{
						$pending_merchant_info{if_number} = $CGI->param('if_number');
						$pending_merchant_info{if_number} =~ s/\D//g;
						$pending_merchant_info{if_number} =~ s/^0*//;
						
						$messages->{warnings}->{member_id_invalid} = ['The Member ID is invalid.'] if ($pending_merchant_info{if_number} && ! $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$pending_merchant_info{if_number}, field=>'id'}));
					}
					
	  				
	  				if($pending_merchant_info{payment_method} eq 'cc')
					{
						$pending_merchant_info{cc_type} = $CGI->param('cc_type') ? $CGI->param('cc_type'):undef;
		  				$pending_merchant_info{pay_name} = $CGI->param('pay_name') ? $CGI->param('pay_name'):undef;
		  				$pending_merchant_info{pay_address} = $CGI->param('pay_address') ? $CGI->param('pay_address'):undef;
		  				$pending_merchant_info{pay_city} = $CGI->param('pay_city') ? $CGI->param('pay_city'):undef;
		  				$pending_merchant_info{pay_state} = $CGI->param('pay_state') ? $CGI->param('pay_state'):undef;
		  				$pending_merchant_info{pay_country} = $CGI->param('pay_country') ? $CGI->param('pay_country'):undef;
		  				$pending_merchant_info{pay_postalcode} = $CGI->param('pay_postalcode') ? $CGI->param('pay_postalcode'):undef;
		  				$pending_merchant_info{pay_cardnumber} = $CGI->param('pay_cardnumber') ? $CGI->param('pay_cardnumber'):undef;
		  				$pending_merchant_info{pay_cardcode} = $CGI->param('pay_cardcode') ? $CGI->param('pay_cardcode'):undef;
		  				$pending_merchant_info{pay_cardexp} = $CGI->param('pay_cardexp') ? $CGI->param('pay_cardexp'):undef;
		  				
		  				$pending_merchant_info{pay_cardexp} =~ s/\D//g if $pending_merchant_info{pay_cardexp};
		  				
		  				
		  				
		  				$messages->{warnings}->{credit_pay_name_is_required} = ['Name On Card is a required field'] if (! $pending_merchant_info{pay_name});
		  				$messages->{warnings}->{credit_pay_address_is_required} = ['The billing address for the card is a required field'] if (! $pending_merchant_info{pay_address});
		  				$messages->{warnings}->{credit_pay_city_is_required} = ['The City is a required field'] if (! $pending_merchant_info{pay_city});
		  				$messages->{warnings}->{credit_pay_state_is_required} = ['The State/Province is a required field'] if (! $pending_merchant_info{pay_state});
		  				$messages->{warnings}->{credit_pay_country_is_required} = ['The Country is a required field'] if (! $pending_merchant_info{pay_country});
		  				$messages->{warnings}->{credit_pay_cardcode_is_required} = ['The CCV Number is a required field'] if (! $pending_merchant_info{pay_cardcode});
		  				$messages->{warnings}->{credit_pay_cardexp_is_required} = ['The Card Expiration Date is a required field'] if (! $pending_merchant_info{pay_cardexp});
		  				
		  				
		  				$messages->{warnings}->{credit_card_number_is_required} = ['The credit card number is a required field'] if (! $pending_merchant_info{pay_cardnumber});
		  				$messages->{warnings}->{invalid_credit_card_number} = ['The credit card number is invalid'] if ($pending_merchant_info{pay_cardnumber} && ! CreditCard::validate($pending_merchant_info{pay_cardnumber}));
		  				
		  				
					}
					
	  				if($CGI->param('ec_number') && $pending_merchant_info{payment_method} eq 'ec')
	  				{
		  				$pending_merchant_info{eco_card_number} = $CGI->param('ec_number') ? $CGI->param('ec_number'):undef;
	  				}  				
					
					
					$url_query_strings{id} = $CGI->param('id');
					
					if(! defined $messages->{warnings})
					{
						my $updated = $MerchantAffiliates->updatePendingMerchantPaymentInformation($CGI->param('id'),\%pending_merchant_info);
						
						if($updated)
						{
							$messages->{notices}->{notice} = ['The information was updated successfully.'];
						}
						else
						{
							$messages->{notices}->{notice} = ['The information was NOT updated.'];
							$pending_merchant_info{id} = $CGI->param('id');
							$messages->{params} = \%pending_merchant_info;
						}
					} 
					else
					{
						$messages->{notices}->{notice} = ['The information was NOT updated.'];
						$pending_merchant_info{id} = $CGI->param('id');
						$messages->{params} = \%pending_merchant_info;
					}
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>$@
	);
	
	$messages->{errors}->{error} = ['A fatal error occured, the information was not updated.  Contact IT.'];
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	
	
}

my $url_query_string = '';

$url_query_strings{data} = $DHS_UrlSafe->encodeHashOrArray($messages);

foreach (keys %url_query_strings)
{
	$url_query_string .= '&' if $url_query_string;
	$url_query_string .= "$_=" . URI::Escape::uri_escape($url_query_strings{$_});
}

#if($notices[0])
#{
#	$url_query_string .= $url_query_string ? '&notices=': 'notices=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@notices);
#}
#
#if($warnings[0])
#{
#	$url_query_string .= $url_query_string ? '&warnings=': 'warnings=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@warnings);
#}
#
#if($errors[0])
#{
#	$url_query_string .= $url_query_string ? '&errors=': 'errors=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@errors);
#}



$url .= "?$url_query_string" if $url_query_string;

print $CGI->redirect(-uri=>"$url");



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


