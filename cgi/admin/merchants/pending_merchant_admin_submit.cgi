#!/usr/bin/perl

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use MailTools;
use G_S;
use MerchantAffiliates;
use URI::Escape;
require 'ccauth.lib';
use Data::Dumper;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $MerchantAffiliates = {};
my $operator = 'cgi';

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $url;
my %url_query_strings = ();
my @notices = ();
my @warnings = ();
my @errors = ();


=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################
$operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

$url = $CGI->url();
$url =~ s/_submit\.cgi$/\.cgi/;
					

					
	my @approved_merchants = ();
	my @merchants_to_delete = ();
	my @pay_ongoing_charges = ();
	my @pay_ongoing_charges_annual = ();
	
	foreach ($CGI->param)
	{
		
		push @approved_merchants, $CGI->param($_) if ($_ =~ /^merchant__\d+$/ && $CGI->param($_) =~ /\d+/);
		
		push @merchants_to_delete, $CGI->param($_) if ($_ =~ /^delete__\d+$/ && $CGI->param($_) =~ /\d+/);
		
		$pay_ongoing_charges[$CGI->param($_)] = $CGI->param($_) if ($_ =~ /^payongoing__\d+$/ && $CGI->param($_) =~ /\d+/);
		
		$pay_ongoing_charges_annual[$CGI->param($_)] = $CGI->param($_) if ($_ =~ /^payongoingannual__\d+$/ && $CGI->param($_) =~ /\d+/);
		
	}
	
	
eval{

	$MerchantAffiliates = MerchantAffiliates->new($db);
	$operator = $CGI->cookie('operator')?$CGI->cookie('operator'):'cgi';
	
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>$@
	);
	
	push @errors, 'A fatal error occured, the information was not updated.  Contact IT.<br />';
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	
	
}

foreach (@merchants_to_delete)
{
	
	eval{
		
		if($MerchantAffiliates->deletePendingMerchant($_))
		{
			push @notices, "Merchant Pending ID $_ was sucessfully removed.<br />";
		}
		else
		{
			push @warnings, "We could not delete Pending Merchant ID $_ <br />";
		}
		
	};
	if($@)
	{
		push @errors, "We could not DELETE Pending Merchant ID $_ <br />";
		
		warn DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript - " . $@;
	}
	
}

foreach (@approved_merchants)
{

	eval{		
		my $club_account_transactions = undef;
		
		my $pending_merchant = $MerchantAffiliates->retrievePendingMerchant({id=>$_});
		
		my %potential_merchant_info = ();
		$potential_merchant_info{discount_subtype} = $pending_merchant->{discount_subtype} if $pending_merchant->{discount_subtype};	# added 01/05/16
		$potential_merchant_info{merchant_type} = $pending_merchant->{merchant_type} if $pending_merchant->{merchant_type};
					
		$potential_merchant_info{member_id} = $pending_merchant->{member_id} if $pending_merchant->{member_id};
		$potential_merchant_info{referral_id} = $pending_merchant->{referral_id} if $pending_merchant->{referral_id};
					
		$potential_merchant_info{business_name} = $pending_merchant->{business_name} if $pending_merchant->{business_name};
		
		$potential_merchant_info{tin} = $pending_merchant->{tin} if $pending_merchant->{tin};
					
		$potential_merchant_info{firstname1} = $pending_merchant->{firstname1} if $pending_merchant->{firstname1};
		$potential_merchant_info{lastname1} = $pending_merchant->{lastname1} if $pending_merchant->{lastname1};
					
		$potential_merchant_info{address1} = $pending_merchant->{address1} if $pending_merchant->{address1};
		$potential_merchant_info{city} = $pending_merchant->{city} if $pending_merchant->{city};
		$potential_merchant_info{country} = $pending_merchant->{country} if $pending_merchant->{country};
		$potential_merchant_info{state} = $pending_merchant->{state} if $pending_merchant->{state};
		$potential_merchant_info{postalcode} = $pending_merchant->{postalcode} if $pending_merchant->{postalcode};
					
		$potential_merchant_info{phone} = $pending_merchant->{business_phone} if $pending_merchant->{business_phone};
		$potential_merchant_info{home_phone} = $pending_merchant->{phone} if $pending_merchant->{phone};
		$potential_merchant_info{scanner_number} = $pending_merchant->{cell_phone} if $pending_merchant->{cell_phone};
		$potential_merchant_info{fax} = $pending_merchant->{fax} if $pending_merchant->{fax};
					
		$potential_merchant_info{language} = $pending_merchant->{language_pref} if $pending_merchant->{language_pref};
					
		$potential_merchant_info{emailaddress1} = $pending_merchant->{emailaddress1} if $pending_merchant->{emailaddress1};
		$potential_merchant_info{emailaddress2} = $pending_merchant->{emailaddress2} if $pending_merchant->{emailaddress2};
					
		$potential_merchant_info{username} = $pending_merchant->{username} if $pending_merchant->{username};
		$potential_merchant_info{password} = $pending_merchant->{password} if $pending_merchant->{password};
		
		$potential_merchant_info{url} = $pending_merchant->{url} if $pending_merchant->{url};
		$potential_merchant_info{website_language} = $pending_merchant->{website_language_pref} if $pending_merchant->{website_language_pref};
		
		$potential_merchant_info{blurb} = $pending_merchant->{blurb} if $pending_merchant->{blurb};
		
		$potential_merchant_info{discount_blurb} = $pending_merchant->{discount_blurb} if $pending_merchant->{discount_blurb};
		
		$potential_merchant_info{business_type} = $pending_merchant->{business_type} if $pending_merchant->{business_type};
		
		#TODO: Decide what to do here.
		$potential_merchant_info{percentage_of_sale} = $pending_merchant->{percentage_of_sale} if $pending_merchant->{percentage_of_sale};
		$potential_merchant_info{exception_percentage_of_sale} = $pending_merchant->{exception_percentage_of_sale} if $pending_merchant->{exception_percentage_of_sale};
		
		$potential_merchant_info{'special'} = $pending_merchant->{special} if $pending_merchant->{special};
		$potential_merchant_info{'flat_fee_off'} = $pending_merchant->{flat_fee_off} if $pending_merchant->{flat_fee_off};
		$potential_merchant_info{'percentage_off'} = $pending_merchant->{percentage_off} if $pending_merchant->{percentage_off};
		
		
		
		if (! exists $potential_merchant_info{'special'} || ! $potential_merchant_info{'special'})
		{
			if (exists $potential_merchant_info{'flat_fee_off'} || exists $potential_merchant_info{'percentage_off'})
			{
				$potential_merchant_info{'flat_fee_off'} =~ s/\D//g if (exists $potential_merchant_info{'flat_fee_off'} && $potential_merchant_info{'flat_fee_off'} =~ /\D/);
				
				$potential_merchant_info{'special'} = $potential_merchant_info{'flat_fee_off'} ? $potential_merchant_info{'flat_fee_off'} : $potential_merchant_info{'percentage_off'};							
			}
		}
					
		
		$potential_merchant_info{merchant_package} = $pending_merchant->{merchant_package} if $pending_merchant->{merchant_package};
		
		
		
		$potential_merchant_info{payment_method} = $pending_merchant->{payment_method} if $pending_merchant->{payment_method};
		
		
		
		$potential_merchant_info{'payment_ongoing'} = 1 if (defined $pay_ongoing_charges[$_] && $pay_ongoing_charges[$_]);
		$potential_merchant_info{'payment_ongoing_annual'} = 1 if (defined $pay_ongoing_charges_annual[$_] && $pay_ongoing_charges_annual[$_]);

		$potential_merchant_info{ca_number_annual} = $pending_merchant->{ca_number} if (defined $pay_ongoing_charges_annual[$_] && $pay_ongoing_charges_annual[$_]);
		
		#NOTE: The Country for the MerchantAffiliates::retrieveMerchantPackageCost is set to 'US' to the US amount will be retrieved.  We only use USD in the Club Account.
		my $merchant_package_fee = $MerchantAffiliates->retrieveMerchantPackageCost($potential_merchant_info{merchant_type},$potential_merchant_info{merchant_package},'US');
		my $seed_amount = $MerchantAffiliates->retrieveMerchantSeedCost($potential_merchant_info{merchant_type});
		#warn "seed_amount: $seed_amount - potential_merchant_info{merchant_type}: $potential_merchant_info{merchant_type} - MerchantAffiliates->retrieveMerchantSeedCost($potential_merchant_info{merchant_type}): " . $MerchantAffiliates->retrieveMerchantSeedCost($potential_merchant_info{merchant_type});
		if($potential_merchant_info{payment_method} eq 'ca')
		{
			
			#CA
			#	Merchant Deposit
			#	-50 from payee's account
			#	+50 Merchant Member Club Account
			#	
			#	Merchant Package Fee
			#	- $79 Payee's account
			#
			
			#TODO: I should check for the $potential_merchant_info{ca_number}, if it does not exist continue onto the next loop.  Ideally I shouldn't have to worry about this.
			
			$potential_merchant_info{ca_number} = $pending_merchant->{ca_number};
			

			
			$club_account_transactions = [];
			if($seed_amount > 0.00)
			{
				push @$club_account_transactions, {transaction_type=>1302, amount=>-$seed_amount, description=>"Seed money for Pending Merchant ID: $_", operator=>$operator,member_id=>$potential_merchant_info{ca_number}};
				push @$club_account_transactions, {transaction_type=>1302, amount=>$seed_amount, description=>"Seed money for a Merchant Affiliates Club Account paying by Club Account, from member $potential_merchant_info{ca_number}", operator=>$operator};
			}
			
			if($merchant_package_fee)
			{
				push @$club_account_transactions, {transaction_type=>2601, amount=>-$merchant_package_fee, description=>"The Merchant Package Fee for Pending Merchant ID: $_", operator=>$operator, member_id=>$potential_merchant_info{ca_number}};
				push @$club_account_transactions, {transaction_type=>2601, amount=>$merchant_package_fee, description=>"Funding for a Merchant Package Fee paying by Club Account, from member $potential_merchant_info{ca_number}", operator=>$operator};
			}
			
			delete $potential_merchant_info{ca_number} if (! $pay_ongoing_charges[$_]);
		}
		elsif($potential_merchant_info{payment_method} eq 'ec')
		{
			
			#TODO: When we have Eco Card, we need to add the transaction information here.
#			$club_account_transactions = [];
#			push @$club_account_transactions, {transaction_type=>0, amount=>0, description=>'', operator=>$operator};
			
			$potential_merchant_info{ec_number} = $pending_merchant->{eco_card_number} if $pending_merchant->{eco_card_number};
			
		}
		elsif($potential_merchant_info{payment_method} eq 'cc')
		{
			
			#CC
			#	Handle the same as in the registration form
			
			$club_account_transactions = [];
			push @$club_account_transactions, {transaction_type=>7000, amount=>$merchant_package_fee, description=>'CC Funding for a Merchant Package', operator=>$operator};
			#This was probably left out due to a transaction being created when a subscription is created.
			#push @$club_account_transactions, {transaction_type=>2601, amount=>-$merchant_package_fee, description=>'Payment of a Merchant Package', operator=>$operator};
			
			$potential_merchant_info{cc_type} = $pending_merchant->{cc_type} if $pending_merchant->{cc_type};
			$potential_merchant_info{cc_number} = $pending_merchant->{pay_cardnumber} if $pending_merchant->{pay_cardnumber};
			$potential_merchant_info{ccv_number} = $pending_merchant->{pay_cardcode} if $pending_merchant->{pay_cardcode};
			$potential_merchant_info{cc_expiration_date} = $pending_merchant->{pay_cardexp} if $pending_merchant->{pay_cardexp};
			$potential_merchant_info{cc_name_on_card} = $pending_merchant->{pay_name} if $pending_merchant->{pay_name};
			$potential_merchant_info{cc_address} = $pending_merchant->{pay_address} if $pending_merchant->{pay_address};
			$potential_merchant_info{cc_city} = $pending_merchant->{pay_city} if $pending_merchant->{pay_city};
			$potential_merchant_info{cc_country} = $pending_merchant->{pay_country} if $pending_merchant->{pay_country};
			$potential_merchant_info{cc_state} = $pending_merchant->{pay_state} if $pending_merchant->{pay_state};
			$potential_merchant_info{cc_postalcode} = $pending_merchant->{pay_postalcode} if $pending_merchant->{pay_postalcode};
			
		
			my %cc_parameters = ();

			$cc_parameters{cc_number} = $potential_merchant_info{cc_number};		
			$cc_parameters{card_code} = $potential_merchant_info{ccv_number};
			$cc_parameters{cc_expiration} = $potential_merchant_info{cc_expiration_date};
			$cc_parameters{cc_expiration} =~ s/^d//;
			
			$cc_parameters{account_holder_lastname} = $potential_merchant_info{cc_name_on_card};
			$cc_parameters{account_holder_address} = $potential_merchant_info{cc_address};
			$cc_parameters{account_holder_city} = $potential_merchant_info{cc_city};
			$cc_parameters{account_holder_state} = $potential_merchant_info{cc_state};
			$cc_parameters{account_holder_zip} = $potential_merchant_info{cc_postalcode} if $potential_merchant_info{cc_postalcode};
			$cc_parameters{account_holder_country} = $potential_merchant_info{cc_country};
			
			$cc_parameters{payment_method} = 'CC';
			$cc_parameters{invoice_number} = 'MA' . time;
			$cc_parameters{id} = 'MARegistration';
			
			my $authorize_charge_params = {};
			
			my $returned_auth_info = 0;
			
			if($merchant_package_fee)
			{
				
				$cc_parameters{amount} = $merchant_package_fee;
				$cc_parameters{order_description} = 'Merchant Registration Credit Card Charge ';
				
				$returned_auth_info = CCAuth::Authorize(\%cc_parameters, $authorize_charge_params);
				
			}
			else
			{
				
				#We changed this, if the merchant has made it to the pending area, their card is good so there is no need to double check it.
#				$cc_parameters{amount} = 1.50;
#				$cc_parameters{order_description} = 'Merchant Registration Credit Card Ping ';
#				
#				$authorize_charge_params = 	{auth_type=>'AUTH_ONLY'};
				
				$returned_auth_info = 1;
			}
			
#			my $returned_auth_info = CCAuth::Authorize(\%cc_parameters, $authorize_charge_params);
			
			
			
			if ($returned_auth_info ne '1')
			{

				my $error_code = $returned_auth_info;
				
				$returned_auth_info =~ s/^\d\s*|\d+:\s*|\<\/*b\>\s*|\<br\s\/\>//gi;
				
				push @warnings, "There was an issue charging the credit card for Pending Merchant ID $_.  $returned_auth_info<br />";
				
				next;
				
			}
			
			
		}
		elsif($potential_merchant_info{payment_method} eq 'if')
		{
			
			#Bank Transfer
			#	Bank Funding $50 Into their club account
			#
			$club_account_transactions = [];
			push @$club_account_transactions, {transaction_type=>3008, amount=>$seed_amount, description=>'Seed money for a Merchant Affiliates Club Account', operator=>$operator} if($seed_amount > 0.00);
			
		}
		
		
#		my $temp = \%potential_merchant_info;
#		warn "\n potential_merchant_info: \n" . Dumper($temp);
		
		my $merchant_id = $MerchantAffiliates->createMerchant(\%potential_merchant_info, $club_account_transactions);
		
		push @warnings, "We could not set Pending Merchant ID: $_ status to Pending<br />" if (! $MerchantAffiliates->updatePendingMerchantStatus($_));
		
		push @notices, "Merchant ID: $merchant_id was sucessfully created.<br />";
		
	};
	if($@)
	{
		
		my $fatal_error = $@;
		
		push @errors, "A fatal error occured creating Pending Merchant ID: $_ <br /> $fatal_error <br />";
		
		
		warn DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript - " . $fatal_error;
		
	}
		
}


my $url_query_string = '';

foreach (keys %url_query_strings)
{
	$url_query_string .= '&' if $url_query_string;
	$url_query_string .= "$_=" . URI::Escape::uri_escape($url_query_strings{$_});
}

if($notices[0])
{
	$url_query_string .= $url_query_string ? '&notices=': 'notices=';
	$url_query_string .= URI::Escape::uri_escape($_) foreach (@notices);
}

if($warnings[0])
{
	$url_query_string .= $url_query_string ? '&warnings=': 'warnings=';
	$url_query_string .= URI::Escape::uri_escape($_) foreach (@warnings);
}

if($errors[0])
{
	$url_query_string .= $url_query_string ? '&errors=': 'errors=';
	$url_query_string .= URI::Escape::uri_escape($_) foreach (@errors);
}



$url .= "?$url_query_string" if $url_query_string;

print $CGI->redirect(-uri=>"$url");



exitScript();

__END__

=head1
CHANGE LOG

	2010 Mar 10	Keith	Modified the approval process, so if a merchant has applied
						for a free package, their card is not pinged.

=cut


