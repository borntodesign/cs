#!/usr/bin/perl

=head1 NAME:
pending_merchant_admin.cgi

	This displays a search form that allows one to search for pending merchants, 
	display the specified pending merchant(s), accept pending merchants into our program,
	and links are also provided to edit pending.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, FindBin, DHSGlobals, CGI, DB, DB_Connect, G_S, URI::Escape

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use G_S;
use MailTools;
use MerchantAffiliates;
use Payment;
use URI::Escape;

#use URI::Escape;
use Lingua::EN::NameCase 'NameCase';

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my @notices = ();
my @warnings = ();
my @errors = ();
our $url = $CGI->url();

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub caseInsensitive { "\L$a" cmp "\L$b" }

=head2
processTemplate

=head3
Description:

	Process a template, replace the place holders with content.

=head3
Params:

	params	hashref	The keys should match the placeholders (ex. %%PLACE_HOLDER%%, $params->{PLACE_HOLDER}='The content that replaces the %%PLACE_HOLDER%%.')
	
	template	string	The document that is to be processed.

=head3
Returns:

	string	The HTML Document 

=cut

sub processTemplate
{
	my $params = shift;
	my $template = shift;

	foreach my $key (keys %$params)
	{
		next if ! $params->{$key};
		
		$template =~ s/%%$key%%/$params->{$key}/g;
	}

	$template =~ s/%%.*%%//g;

	return $template;
}

=head2
displaySearchForm

=head3
Description:

	Display the form we use to search for pending Merchants

=head3
Params:

	$params	hashref	I'm not sure what I'm going to do with this yet.

=head3
Returns:

	string	The HTML Document 

=cut

sub displaySearchForm
{

	my $params = shift;
	
	my %field_names = ();
	my $select_box_values = [];
	
	$params->{ERRORS} = '' if ! defined $params->{ERRORS};
	$params->{WARNINGS} = '' if ! defined $params->{WARNINGS};
	
	
	$params->{NOTICES} = '' if ! defined $params->{NOTICES};
	
	$params->{WARNINGS} .= q(<span class="warnings">A Search String is required to fulfill your request.</span><br />) if ($CGI->param('search_field') && ! $CGI->param('search_value'));

	$params->{WARNINGS} .= q(<span class="warnings">A Search Field is required to fulfill your request.</span><br />) if($CGI->param('search_value') && ! $CGI->param('search_field'));
	
	$params->{SEARCH_VALUE} = q( value=") . $CGI->param('search_value') . q(" );
	
	$params->{TITLE} = 'Merchant Affiliates Administration' if ! exists $params->{TITLE};
	$params->{MESSAGE} = 'Merchant Affiliates Administration' if ! exists $params->{MESSAGE};
	$params->{ACTION} = $url if ! exists $params->{ACTION};
	
	$params->{ERRORS} .= q(<span class="errors">) . $CGI->param('errors') . '</span>' if $CGI->param('errors');
	$params->{WARNINGS} .= q(<span class="warnings">) . $CGI->param('warnings') . '</span>' if $CGI->param('warnings');
	
	$params->{NOTICES} = q(<span class="notices">) . $params->{NOTICES} . '</span><br />' if $params->{NOTICES};
	$params->{NOTICES} .= q(<span class="notices">) . $CGI->param('notices') . '</span>' if $CGI->param('notices');
	
	
	my $sth = $db->column_info( undef, 'public', 'merchant_affiliates_master', "%");
	
	my @columns = ();
	while(my $rows = $sth->fetchrow_hashref())
	{
		next if $rows->{COLUMN_NAME} =~ /^status$|^stamp$/;
		$rows->{COLUMN_NAME} =~ s/"//g;
		push @columns, $rows->{COLUMN_NAME};
	}

	
	my @sorted_columns = sort caseInsensitive @columns;
	
	unshift @sorted_columns, '', 'business_name', 'id', 'referral_id', '';
	
	foreach (@sorted_columns)
	{
		my $human_readable_name = $_;
		
		$human_readable_name =~ s/pay_/CC_/g;
		
		$human_readable_name =~ s/_/\ /g;
		
		$field_names{$_} = Lingua::EN::NameCase::NameCase($human_readable_name);
		
		push @$select_box_values, $_;
	
	}
	
	$field_names{''} = '';
	
	
	
	while(my $rows = $sth->fetchrow_hashref())
	{
		$rows->{COLUMN_NAME} =~ s/"//g;
		
		my $human_readable_name = $rows->{COLUMN_NAME};
		
		$human_readable_name =~ s/pay_//g;
		
		$human_readable_name =~ s/_/\ /g;
		
		
		
		$field_names{$rows->{COLUMN_NAME}} = Lingua::EN::NameCase::NameCase($human_readable_name);
		
		push @$select_box_values, $rows->{COLUMN_NAME};
		
	}
	
	
#	my @sorted_columns = sort caseInsensitive @$select_box_values;
#	
#	unshift @sorted_columns, '', 'business_name', 'id', 'referral_id', '';
	
	my $MerchantAffiliates = MerchantAffiliates->new($db);
	my @countries = $MerchantAffiliates->retrieveMerchantCountries();
	my @provinces = $MerchantAffiliates->retrieveMerchantProvinces();
	my @packages = $MerchantAffiliates->retrieveMerchantPackages();
	
	
	my @country_values = ();
	my %country_names = ();
	foreach (@countries)
	{
		$country_names{$_->{'country'}} = $_->{'country_label'};
		push @country_values, $_->{'country'};
	}
	
	my @province_values = ();
	my %province_name = ();
	foreach (@provinces)
	{
		next if ! $_->{'state'};
		
		my $state =   $_->{'state'};
		
		$state =~ s/\s*$//;
		$state =~ s/^\s*//;
		$province_name{$_->{'state'}} = " $state ($_->{'state_label'})";
		push @province_values, $_->{'state'};
	}
	
	
	my @package_values = ();
	my %package_name = ();
	foreach (@packages)
	{
		next if (! $_->{'name'} || ! $_->{'package_type'} || ! $_->{'discount_type'});
		
		$package_name{$_->{'discount_type'}} = " $_->{'name'} ($_->{'package_type'})";
		push @package_values, $_->{'discount_type'};
	}
	
	my @status_values = ('-1,0',1,2,'1,2');
	my %status_name = (
						'1,2'=>'Active and Approved',
						'-1,0'=>'In-Active',
						1=>'Active',
						2=>'Approved'
					);
	
	
	
	$params->{SEARCH_FIELDS} = $CGI->popup_menu(
											-name=>'search_field', 
											-id=>'search_field', 
											-values=>$select_box_values,
											-labels=>\%field_names,
											-default=>$CGI->param('search_field')
										);
	
	$params->{COUNTRY_LIST} = $CGI->popup_menu(
											-name=>'country', 
											-values=>\@country_values,
											-labels=>\%country_names,
											-style=>'display: none;',
											-default=>$CGI->param('county')
										);
	
	$params->{PROVINCE_LIST} = $CGI->popup_menu(
											-name=>'state', 
											-values=>\@province_values,
											-labels=>\%province_name,
											-style=>'display: none;',
											-default=>$CGI->param('state')
										);
	
	$params->{MERCHANT_PACKAGE_LIST} = $CGI->popup_menu(
											-name=>'discount_type', 
											-values=>\@package_values,
											-labels=>\%package_name,
											-style=>'display: none;',
											-default=>$CGI->param('discount_type')
										);
	
	my $status_default = $CGI->param('status') ? $CGI->param('status') : '1';
	
	$params->{STATUS} = $CGI->popup_menu(
											-name=>'status', 
											-values=>\@status_values,
											-labels=>\%status_name,
											-default=>$status_default
										);
	
	my $html_form = (DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100804) ? G_S::Get_Object($db, 10812): G_S::Get_Object($db, 10808); 

	print $CGI->header('text/html');

	print processTemplate($params, $html_form);
	
	exitScript();
	
}


=head2
displayPendingMerchantList

=head3
Description:

	Display the form we use to Approve Pending Merchants

=head3
Params:

	$params	hashref	{
						search_field	string	The field to search for the merchants with,
						search_value	string	The value to search the search_field with.
					}

=head3
Returns:

	string	The HTML Document 

=cut

sub displayMerchantList
{
	eval{
		my $param = shift;
		
		
		my $MerchantAffiliates = MerchantAffiliates->new($db);
		
		$MerchantAffiliates->setMembers();

		my $Payment = Payment->new($db);
		
		my $search_field = $param->{search_field} ? $param->{search_field}: undef;
		my $search_value = $param->{search_value} ? $param->{search_value}: undef;
		my $status = $param->{status};
		
		chomp($search_field) if $search_field;
		chomp($search_value) if $search_value;
		chomp($status);
		
		my $merchant_search_parameters = {$search_field=>$search_value};
		
		#TODO: Wrap this in a eval.
		my $merchants = $MerchantAffiliates->getMerchantMaster({$search_field=>$search_value, status=>$status});
		my $application_configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef();
		
		displaySearchForm({NOTICES=> 'There were no results, try another Search String.'}) if (! $merchants || (ref($merchants) eq 'ARRAY' && ! defined $merchants->[0]));
		#If only one merchant was returned make it an arrayrefs of hashrefs
		$merchants = [$merchants] if(ref($merchants) eq 'HASH');
		
		my $payment_methods = $Payment->getPaymentMethods();
		
		
		#$url =~ s/\.cgi$/_submit\.cgi/;
		
		my %template_values = (TITLE=>'Merchant List',MESSAGE=>"Start a new <a href='/cgi/admin/merchants/merchant_admin.cgi'>Search</a>",LIST=>''); # ,ACTION=>$url
		
		my $listing_template = G_S::Get_Object($db, 10730); #(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100804) ? G_S::Get_Object($db, 10730) : G_S::Get_Object($db, 10723);
		
		foreach (@$merchants)
		{
			
			my $member_info = $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$_->{member_id}});
			my %merchant_list_template_values = ();
			
			#prep data.
			$merchant_list_template_values{SPID} = $member_info->{spid};
			
			if($_->{discount} > 0)
			{
				$merchant_list_template_values{PACKAGE_PERCENTAGE} =  $_->{discount} * 100;				
			}
			else
			{
				if($_->{special})
				{
					if ($_->{special} =~ /\W/g)
					{
						if ($_->{special} =~ /\W\W/g)
						{
							foreach my $other_off_key (keys %{$application_configuration->{other_off}})
							{
								if ($_->{special} eq $application_configuration->{other_off}->{$other_off_key}->{val})
								{
									$merchant_list_template_values{PACKAGE_PERCENTAGE} = $_->{special};
									last;
								}
							}
						}
						else
						{
							$_->{special} =~ s/0*$//;
							foreach my $percentage_off_key (keys %{$application_configuration->{percentage_off}})
							{
								if ($_->{special} eq $application_configuration->{percentage_off}->{$percentage_off_key}->{val})
								{
									$merchant_list_template_values{PACKAGE_PERCENTAGE} = $_->{special} * 100;
									last;
								}
							}
						}
					}
					else
					{
						foreach my $flat_fee_off_key (keys %{$application_configuration->{flat_fee_off}})
						{
							if ($_->{special} eq $application_configuration->{flat_fee_off}->{$flat_fee_off_key}->{val})
							{
								$merchant_list_template_values{PACKAGE_PERCENTAGE} = "\$ $_->{special}";
								last;
							}
						}
					}
				}
		
				
			}

			my $merchant_package_name = $MerchantAffiliates->getMerchantPackageNameById($_->{discount_type},1);
			my $merchant_type_name = $MerchantAffiliates->getMerchantTypeNameById($_->{discount_type}) eq 'offline' ? 'Tracked':'Direct';
			$merchant_list_template_values{MERCHANT_PACKAGE_NAME} =  "$merchant_package_name ($merchant_type_name)";
			
			$_->{pay_payment_method} = uc($_->{pay_payment_method}) if defined $_->{pay_payment_method};
			$merchant_list_template_values{MERCHANT_PAYMENT_INFORMATION} = (defined $_->{pay_payment_method} && defined $payment_methods->{$_->{pay_payment_method}}) ? $payment_methods->{$_->{pay_payment_method}}: '';
			
			$_->{pending_payment_method} = uc($_->{pending_payment_method}) if defined $_->{pending_payment_method};
			$merchant_list_template_values{PENDING_PAYMENT_INFORMATION} = defined $_->{pending_payment_method} ? 'X':'';
			
			
			
			
			#Prep the keys so they are all uppercase.
			foreach my $label (keys %$_)
			{
				
				$merchant_list_template_values{uc($label)} = $label =~ /username|password/i ? URI::Escape::uri_escape($_->{$label}) : $_->{$label};
			}
			
			$template_values{LIST} .= processTemplate(\%merchant_list_template_values, $listing_template);
   
                     
		}
		
		
		
	my $html_form = G_S::Get_Object($db, 10725); #(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100804) ?  : G_S::Get_Object($db, 10715);


	print $CGI->header('text/html');
	
	

	print processTemplate(\%template_values, $html_form);

#	print $CGI->header('text/plain');		
#	print Dumper($param) . "\n\n";
#	print Dumper($merchants) . "\n\n";
	
	};
	if($@)
	{
		print $CGI->header('text/plain');
		print $@;
	}

		
		
		
		exitScript();
	
}

####################################################
# Main
####################################################
my $operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}

#unless ($db = DB_Connect('merchants_admin')){exit}

$db->{RaiseError} = 1;


eval{
	
	my $search_value = ($CGI->param('search_field') && $CGI->param('search_field') =~ /country|state|discount_type/) ? $CGI->param('search_field'): 'search_value';
	
	if($CGI->param('search_field') && $CGI->param($search_value))
	{
		displayMerchantList(
										{
											search_field=>$CGI->param('search_field'),
											search_value=>$CGI->param($search_value),
											status=>$CGI->param('status')
										}
									);
	}
	else
	{
		displaySearchForm();
	}
	
	
	


};
if($@)
{
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	
	print $CGI->header('text/plain');
	print "An unrecoverable error has occured.  Contact IT.\n\n";
	
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


