#!/usr/bin/perl

=head1 NAME:
script_name.cgi

	Brief description of the script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use ADMIN_DB;
use MailTools;
use G_S;
use MerchantAffiliates;
use DHS::UrlSafe;
use URI::Escape;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $MerchantAffiliates = {};
my $DHS_UrlSafe = DHS::UrlSafe->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $url;
my %url_query_strings = ();
my $messages = {};

my $configuration = {};

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################
my $operator = $CGI->cookie('operator'); 
my $pwd = $CGI->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'merchants_admin', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

$url = $CGI->url();
$url =~ s/_submit\.cgi$/\.cgi/;
					
eval{
					
					$MerchantAffiliates = MerchantAffiliates->new($db);
					$MerchantAffiliates->setMembers();
					
					$configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef();
					 my %pending_merchant_info = ();
					
					if($CGI->param('member_id'))
					{
						$pending_merchant_info{member_id} = $CGI->param('member_id');
						$pending_merchant_info{member_id} =~ s/\D//g;
						$pending_merchant_info{member_id} =~ s/^0*//;
						$messages->{warnings}->{member_id_invalid} = ['The Member ID is invalid.'] if ($pending_merchant_info{member_id} && ! $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$pending_merchant_info{member_id}, field=>'id'}));
						
						
						
					}
					else
					{
						$pending_merchant_info{member_id} = undef;
					}
					
					
					
					
				if ($CGI->param('referral_id'))
				{
					
					$pending_merchant_info{referral_id} = $CGI->param('referral_id');
					$pending_merchant_info{referral_id} =~ s/\D//g;
					$pending_merchant_info{referral_id} =~ s/^0*//;
					
					my $referal_member_type = $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$pending_merchant_info{referral_id}, field=>'membertype'});
				
					$messages->{warnings}->{referral_id_wrong_membertype} = ['The reffering member must be at least a VIP.'] if (! $referal_member_type || $referal_member_type ne 'v');
					
					
				}
				
				$messages->{warnings}->{member_id_referral_id} = ['You can only specify a Member ID, or a Refferal ID NOT both.'] if($pending_merchant_info{member_id} && $pending_merchant_info{referral_id});	
				
					
					
					
					
					$pending_merchant_info{business_name} = $CGI->param('business_name') ? $CGI->param('business_name'):undef;
					$pending_merchant_info{firstname1} = $CGI->param('firstname1') ? $CGI->param('firstname1'):undef;
					$pending_merchant_info{lastname1} = $CGI->param('lastname1') ? $CGI->param('lastname1'):undef;
					$pending_merchant_info{address1} = $CGI->param('address1') ? $CGI->param('address1'):undef;
					$pending_merchant_info{city} = $CGI->param('city') ? $CGI->param('city'):undef;
					$pending_merchant_info{state} = $CGI->param('state') ? $CGI->param('state'):undef;
					$pending_merchant_info{country} = $CGI->param('country') ? $CGI->param('country'):undef;
					$pending_merchant_info{postalcode} = $CGI->param('postalcode') ? $CGI->param('postalcode'):undef;
					$pending_merchant_info{business_phone} = $CGI->param('business_phone') ? $CGI->param('business_phone'):undef;
					$pending_merchant_info{cell_phone} = $CGI->param('cell_phone') ? $CGI->param('cell_phone'):undef;
					$pending_merchant_info{fax} = $CGI->param('fax') ? $CGI->param('fax'):undef;
					
					$pending_merchant_info{language_pref} = $CGI->param('language_pref') ? $CGI->param('language_pref'):undef;
					$pending_merchant_info{cc_type} = $CGI->param('cc_type') ? $CGI->param('cc_type'):undef;
					
					$pending_merchant_info{'special'} = $CGI->param('special') ? $CGI->param('special'):undef;
					$pending_merchant_info{'percentage_off'} = $CGI->param('percentage_off') ? $CGI->param('percentage_off'):undef;
					$pending_merchant_info{'flat_fee_off'} = $CGI->param('flat_fee_off') ? $CGI->param('flat_fee_off'):undef;
					$pending_merchant_info{'percentage_of_sale'} = $CGI->param('percentage_of_sale') ? $CGI->param('percentage_of_sale'):undef;

					$pending_merchant_info{'discount_blurb'} = $CGI->param('discount_blurb') ? $CGI->param('discount_blurb'):undef;

					$pending_merchant_info{merchant_package} = $CGI->param('merchant_package') ? $CGI->param('merchant_package'):undef;
					
					$pending_merchant_info{emailaddress1} = $CGI->param('emailaddress1') ? $CGI->param('emailaddress1'):undef;
					
					$messages->{warnings}->{emailaddress1_preexisting} = ['The email address already exists, or is not from an exceptable domain.'] if ($pending_merchant_info{emailaddress1} && ! $MerchantAffiliates->{Members}->checkEmailDomain($pending_merchant_info{emailaddress1}));
					
#					$pending_merchant_info{emailaddress2} = $CGI->param('emailaddress2') ? $CGI->param('emailaddress2'):undef;
					$pending_merchant_info{username} = $CGI->param('username') ? $CGI->param('username'):undef;
					
					$messages->{warnings}->{username_preexisting} = ['The username already exists for another member.'] if ($pending_merchant_info{username} && ! $MerchantAffiliates->checkMasterUsernameDuplicates($pending_merchant_info{username}));
					$messages->{warnings}->{username_preexisting} = ['The username already exists for another member.'] if ($pending_merchant_info{username} && ! $MerchantAffiliates->checkLocationUsernameDuplicates($pending_merchant_info{username}));
					$messages->{warnings}->{username} = ['The username is a required field.'] if (! $pending_merchant_info{username});
					
					
					$pending_merchant_info{password} = $CGI->param('password') ? $CGI->param('password'):undef;


					$messages->{warnings}->{password} = ['A password must be specified.'] if ! $pending_merchant_info{password};
					
					
					
					$pending_merchant_info{url} = $CGI->param('url') ? $CGI->param('url'):undef;
					$pending_merchant_info{website_language_pref} = $CGI->param('website_language_pref') ? $CGI->param('website_language_pref'):undef;
					
					$pending_merchant_info{business_type} = $CGI->param('business_type') ? $CGI->param('business_type'):undef;
					
					$messages->{warnings}->{business_type} = ['A Business Type must be selected.'] if ! $pending_merchant_info{business_type};
					
					$pending_merchant_info{'merchant_type'} = $MerchantAffiliates->getMerchantTypeNameById($pending_merchant_info{'merchant_package'}) if(exists $pending_merchant_info{'merchant_package'} && defined $pending_merchant_info{'merchant_package'} && $pending_merchant_info{'merchant_package'} =~ /^\d+$/);
#					$pending_merchant_info{merchant_type} = $CGI->param('merchant_type') ? $CGI->param('merchant_type'):'offline';
					
					#This is a hack, but it is a quick fix for now.
					my %missing_fields = ();
					
					
					if(exists $pending_merchant_info{'merchant_type'} && $pending_merchant_info{'merchant_type'} eq 'offline_rewards')
					{
						#Make sure they selected one.
						if(! $pending_merchant_info{'special'} && ! $pending_merchant_info{'percentage_off'} && ! $pending_merchant_info{'flat_fee_off'})
						{
							$messages->{warnings}->{discount_option_is_required} = ['At least one Discount Type must be selected.'];
							$missing_fields{'discount_option_is_required'} = 'At least one Discount Type must be selected.';
						}

						
						if (exists $pending_merchant_info{'flat_fee_off'})
						{
							$pending_merchant_info{'flat_fee_off'} =~ s/\ +$//;	
							$pending_merchant_info{'flat_fee_off'} =~ s/^\ +//;	
							if($pending_merchant_info{'flat_fee_off'} =~ /\D/)
							{
								if (exists $pending_merchant_info{'flat_fee_off'} && $pending_merchant_info{'flat_fee_off'} =~ /\D/)
								{
									$messages->{warnings}->{flat_fee_off} = ['Only whole numbers are allowed in the Flat Fee field.'];
									$missing_fields{'flat_fee_off'} = 'Only whole numbers are allowed in the Flat Fee field.';
									
								}

							}
						}
						
						if(! exists $missing_fields{'discount_option_is_required'} && ! exists $missing_fields{'flat_fee_off'} && $pending_merchant_info{'percentage_off'})
						{
							my $package_name = $MerchantAffiliates->getMerchantPackageNameById($pending_merchant_info{'merchant_package'});
							#$missing_fields{'appropriate_discount_option_for_package'} = ['Missing Field'] if ($merchant_information{'percentage_off'} < $configuration->{'min_percentage_off_per_package'}->{$merchant_information{'merchant_type'}}->{$package_name});
							
							$messages->{warnings}->{discount_option_is_required} = ['At least one Discount Type must be selected.'] if ($pending_merchant_info{'percentage_off'} < $configuration->{'min_percentage_off_per_package'}->{$pending_merchant_info{'merchant_type'}}->{$package_name});
						}
					
					}
					elsif(exists $pending_merchant_info{'merchant_type'} && $pending_merchant_info{'merchant_type'} eq 'offline')
					{
						#Make sure they selected one.
						$messages->{warnings}->{discount_option_is_required} = ['A Discount Type must be selected.'] if(! $pending_merchant_info{'percentage_of_sale'});
					}
					
					
					
#					if($CGI->param('exception_percentage_of_sale') && $CGI->param('percentage_of_sale'))
#					{
#						$pending_merchant_info{percentage_of_sale} = undef;
#						$pending_merchant_info{exception_percentage_of_sale} = undef;
#						push @warnings, '<percentage_of_sale_not_both>Only one Discount Type can be selected, not both.</percentage_of_sale_not_both>  ';
#					}
#					elsif(!$CGI->param('exception_percentage_of_sale') && !$CGI->param('percentage_of_sale'))
#					{
#						$pending_merchant_info{percentage_of_sale} = undef;
#						$pending_merchant_info{exception_percentage_of_sale} = undef;
#						push @warnings, '<percentage_of_sale>At least one Discount Type must be selected.</percentage_of_sale>  ';
#						push @warnings, '<exception_percentage_of_sale>At least one Discount Type must be selected.</exception_percentage_of_sale>  ';
#					}
#					else
#					{
#						$pending_merchant_info{percentage_of_sale} = $CGI->param('percentage_of_sale') ? $CGI->param('percentage_of_sale'):undef;
#		  				$pending_merchant_info{exception_percentage_of_sale} = $CGI->param('exception_percentage_of_sale') ? $CGI->param('exception_percentage_of_sale'):undef;
#					}

#					$pending_merchant_info{payment_method} = $CGI->param('payment_method') ? $CGI->param('payment_method'):undef;
#	  				$pending_merchant_info{pay_name} = $CGI->param('cc_name_on_card') ? $CGI->param('cc_name_on_card'):undef;
#	  				$pending_merchant_info{pay_address} = $CGI->param('cc_address') ? $CGI->param('cc_address'):undef;
#	  				$pending_merchant_info{pay_city} = $CGI->param('cc_city') ? $CGI->param('cc_city'):undef;
#	  				$pending_merchant_info{pay_state} = $CGI->param('cc_state') ? $CGI->param('cc_state'):undef;
#	  				$pending_merchant_info{pay_country} = $CGI->param('cc_country') ? $CGI->param('cc_country'):undef;
#	  				$pending_merchant_info{pay_postalcode} = $CGI->param('cc_postalcode') ? $CGI->param('cc_postalcode'):undef;
#	  				$pending_merchant_info{pay_cardnumber} = $CGI->param('cc_number') ? $CGI->param('cc_number'):undef;
#	  				$pending_merchant_info{pay_cardcode} = $CGI->param('ccv_number') ? $CGI->param('ccv_number'):undef;
#	  				$pending_merchant_info{pay_cardexp} = $CGI->param('cc_expiration_date') ? $CGI->param('cc_expiration_date'):undef;
#	  				
#	  				$pending_merchant_info{eco_card_number} = $CGI->param('ec_number') ? $CGI->param('ec_number'):undef;
#	  				
#	  				$pending_merchant_info{ca_number} = $CGI->param('ca_number') ? $CGI->param('ca_number'):undef;
#	  				
#	  				$pending_merchant_info{if_number} = $CGI->param('if_number') ? $CGI->param('if_number'):undef;
					
					$pending_merchant_info{status} = ($CGI->param('status') eq 'one') ? 1:0;
					
					
					$url_query_strings{id} = $CGI->param('id');
					
					if(! defined $messages->{warnings})
					{
						
						my $updated = $MerchantAffiliates->updatePendingMerchantInformation($CGI->param('id'),\%pending_merchant_info);
						
						if($updated)
						{
							$messages->{notices}->{'notice'} = ['The information was updated successfully.'];
							delete $messages->{params};
						}
						else
						{
							$messages->{notices}->{'notice'} = ['The information was NOT updated.'];
							$pending_merchant_info{id} = $CGI->param('id');
							$messages->{params} = \%pending_merchant_info;
						}
					} else {
						$messages->{notices}->{'notice'} = ['The information was NOT updated.'];
						$pending_merchant_info{id} = $CGI->param('id');
						$messages->{params} = \%pending_merchant_info;
					}
					
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>$@
	);
	
	$messages->{errors}->{error} = ['A fatal error occured, the information was not updated.  Contact IT.'];
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	
	
}

my $url_query_string = '';

$url_query_strings{data} = $DHS_UrlSafe->encodeHashOrArray($messages);

foreach (keys %url_query_strings)
{
	$url_query_string .= '&' if $url_query_string;
	$url_query_string .= "$_=" . URI::Escape::uri_escape($url_query_strings{$_});
}



#if($notices[0])
#{
#	$url_query_string .= $url_query_string ? '&notices=': 'notices=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@notices);
#}
#
#if($warnings[0])
#{
#	$url_query_string .= $url_query_string ? '&warnings=': 'warnings=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@warnings);
#}
#
#if($errors[0])
#{
#	$url_query_string .= $url_query_string ? '&errors=': 'errors=';
#	$url_query_string .= URI::Escape::uri_escape($_) foreach (@errors);
#}



$url .= "?$url_query_string" if $url_query_string;

print $CGI->redirect(-uri=>"$url");



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


