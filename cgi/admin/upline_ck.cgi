#!/usr/bin/perl -w
###### upline_ck.cgi
###### a utility to get a list of live upline or end of month upline
###### last modified: 01/06/2015	Bill MacArthur

use DBI;
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
require ADMIN_DB;
require cs_admin;

my ($db, @List);
my $q = new CGI;
my $MEMBERS_TBL = 'members';
my $OUTPUT_STYLE = 'td{font-size: 9pt; font-family: sans-serif;}';
my $id = $q->param('id') || '';
$id =~ s/\s*//g;
my $ID = $id;
my $month = $q->param('month') || '';
my $nflag = $q->param('nflag');

print $q->header('-expires'=>'now');

unless ($ID)
{
	print "No ID submitted";
	exit;
}
	
if ($ID =~ /\D/)
{
	print "Only numeric IDs can be accepted for this check.";
	exit;
}

if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
}
else
{
	print "You must be logged into use this resource";
	exit;
}

$db->{'RaiseError'} = 1;

$month ? ($nflag ? Archive_Month_Network() : Archive_Month()) : ($nflag ? Current_Month_Network() : Current_Month());

$db->disconnect;
exit;

sub Archive_Month
{
	# check the sanity of this parameter before blindly inserting it into the SQL
	die "Invalid 'month' parameter: $month" unless $month =~ m/^cgi_\d{4}_\d{2}_\d{2}$/;

	$month =~ s/-/_/g;
	my ($spid, $membertype);
	my $sth = $db->prepare("
		SELECT id,
			spid,
			UPPER(membertype) AS membertype
		FROM $month.members
		WHERE id= ?");
			

	print $q->start_html('-bgcolor'=>'#ffffff', '-style'=>$OUTPUT_STYLE, '-title'=>"Upline Check for: $ID, month ending: $month");
	print "<b>Line of sponsorship for: <i>$ID</i>, month ending: <i>$month</i></b><br /><br />\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">\n";
	print "<tr><td style=\"font-weight: bold\">ID</td><td style=\"font-weight: bold\">Sponsor ID</td><td style=\"font-weight: bold\">Member Type</td></tr>\n";
	
	while ($ID > 1)
	{
		$sth->execute($ID);
		($ID, $spid, $membertype) = $sth->fetchrow_array;
		unless($ID)
		{
			print "<tr><td colspan=\"3\">Failed to retrieve record for $id</td></tr>\n";
			last;
		}
		else
		{
			print "<tr><td>$ID</td><td>$spid</td><td align=\"center\">$membertype</td></tr>\n";
			$ID = $spid;
		}
		if ( Dupe_Ck($ID) )
		{
			print qq#<tr><td colspan="3">Loop detected! Search terminated!</td></tr>#;
			last;
		}
		else
		{
			push (@List, $ID);
		}
	}
	$sth->finish;
	
	print '</table><br />', $q->a({'-href'=>'/admin/upline_checker.html'}, 'Search again.'), $q->end_html;
}

sub Archive_Month_Network
{
	# check the sanity of this parameter before blindly inserting it into the SQL
	die "Invalid 'month' parameter: $month" unless $month =~ m/^cgi_\d{4}_\d{2}_\d{2}$/;

	$month =~ s/-/_/g;
	my ($spid, $membertype, $op, $stamp, $memo);
	my $sth = $db->prepare("
		SELECT m.id,
			COALESCE(ns.spid, m.spid) AS spid,
			UPPER(m.membertype) AS membertype,
			ns.operator,
			ns.stamp::TIMESTAMP(0) AS stamp,
			ns.memo
		FROM $month.members m
		LEFT JOIN $month.network_spids ns
			ON ns.id=m.id
		WHERE m.id= ?");
			

	print $q->start_html('-bgcolor'=>'#ffffff', '-style'=>$OUTPUT_STYLE, '-title'=>"NSPID Upline Check for: $ID, month ending: $month");
	print "<b>NSPID Line of sponsorship for: <i>$ID</i>, month ending: <i>$month</i></b><br /><br />\n";
	print qq#<table border="1" cellspacing="0" cellpadding="4">\n#;
	print qq#<tr><td style="font-weight: bold">ID</td><td style="font-weight: bold">Sponsor ID</td><td style="font-weight: bold">Member Type</td>
		<td style="font-weight: bold">Operator</td><td style="font-weight: bold">Stamp</td><td style="font-weight: bold">Memo</td></tr>\n#;
	
	while ($ID > 1)
	{
		$sth->execute($ID);
		($ID, $spid, $membertype, $op, $stamp, $memo) = $sth->fetchrow_array;
		unless($ID)
		{
			print "<tr><td colspan=\"6\">Failed to retrieve record for $id</td></tr>\n";
			last;
		}
		else
		{
			print "<tr><td>$ID</td><td>$spid</td><td align=\"center\">$membertype</td><td>$op</td><td>$stamp</td><td>$memo</td></tr>\n";
			$ID = $spid;
		}
		if ( Dupe_Ck($ID) )
		{
			print "<tr><td colspan=\"6\">Loop detected! Search terminated!</td></tr>";
			last;
		}
		else { push (@List, $ID) }
	}
	$sth->finish;
	
	print '</table><br />', $q->a({'-href'=>'/admin/upline_checker.html'}, 'Search again.'), $q->end_html;

}

sub Current_Month
{
	my ($spid, $membertype, $operator, $stamp, $memo, $seq);

	my $sth = $db->prepare(qq#
		SELECT
			m.id,
			m.spid,
			upper(m.membertype) AS membertype,
			COALESCE(m.operator, '&nbsp;') AS operator,
			m.stamp::timestamp(0) AS stamp,
			SUBSTR( COALESCE(m.memo, '&nbsp;'), 0, 50) AS memo,
			COALESCE(op.seq, np.seq, mp.seq) AS seq
		FROM "$MEMBERS_TBL" m
		LEFT JOIN od.pool op
			ON op.id=m.id
		LEFT JOIN network.positions np
			ON np.id=m.id
		LEFT JOIN mviews.positions mp
			ON mp.id=m.id
		WHERE m.id= ?#);


	print $q->start_html('-bgcolor'=>'#ffffff', '-style'=>$OUTPUT_STYLE, '-title'=>"RSPID Upline Check for $ID");
	print "<b>RSPID Line of sponsorship for: $ID at the moment.<br /><br />\n";
	print qq#<table border="1" cellspacing="0" cellpadding="4">\n#;
	print qq#<tr>
		<td style="font-weight: bold">ID</td>
		<td style="font-weight: bold">Sponsor ID</td>
		<td style="font-weight: bold">M/T</td>
		<td style="font-weight: bold">PnsID</td>
		<td style="font-weight: bold">Operator</td>
		<td style="font-weight: bold">Stamp</td>
		<td style="font-weight: bold">Memo</td>
		</tr>\n#;

	while ($ID > 1)
	{
		$sth->execute($ID);
		($ID, $spid, $membertype, $operator, $stamp, $memo, $seq) = $sth->fetchrow_array;
		$operator = '&nbsp;' if $operator !~ /\w/;
		$memo = '&nbsp;' if $memo !~ /\w/;
		unless($ID)
		{
			print qq#<tr><td colspan="7">Failed to retrieve record for $id</td></tr>\n#;
			last;
		}
		else
		{
			print "<tr>
				<td>$ID</td>
				<td>$spid</td>
				<td align=\"center\">$membertype</td>
				<td>$seq</td>
				<td>$operator</td>
				<td>$stamp</td>
				<td>$memo</td>
				</tr>\n";
			$ID = $spid;
		}
	
		if ( Dupe_Ck($ID) )
		{
			print "<tr><td colspan=\"7\">Loop detected! Search terminated!</td></tr>";
			last;
		}
		else
		{
			push (@List, $ID);
		}
	}
	$sth->finish;
	
	print '</table><br />', $q->a({'-href'=>'/admin/upline_checker.html'}, 'Search again.'), $q->end_html;
}

sub Current_Month_Network
{
	my ($spid, $membertype, $operator, $stamp, $memo, $tpp, $pnsid, $seq);
	my $rowclass = 'a';
	
	my $sth = $db->prepare(qq#
		SELECT
			m.id,
			COALESCE(ns.spid, m.spid) AS spid,
			UPPER(m.membertype) AS membertype,
			COALESCE(ns.operator, '&nbsp;') AS operator,
			COALESCE(ns.stamp::TIMESTAMP(0)::TEXT, '&nbsp;') AS stamp,
			SUBSTR( COALESCE(ns.memo, '&nbsp;'), 0, 50) AS memo,
			COALESCE(r.opp,0) AS tpp,
			COALESCE(op.pnsid, np.pnsid, mp.pnsid) AS pnsid,
			COALESCE(op.seq, np.seq, mp.seq) AS seq
		FROM "$MEMBERS_TBL" m
		LEFT JOIN network.spids ns
			ON ns.id=m.id
		LEFT JOIN refcomp r
			ON r.id=m.id
		LEFT JOIN od.pool op
			ON op.id=m.id
		LEFT JOIN network.positions np
			ON np.id=m.id
		LEFT JOIN mviews.positions mp
			ON mp.id=m.id
		WHERE m.id= ?#);


	print $q->start_html('-style'=>{'-src'=>'/css/admin/generic-report.css'}, '-title'=>"NSPID Upline Check for $ID");
	print "<b>NSPID Line of sponsorship for: $ID at the moment.<br /><br />\n";
	print qq#<table class="report">\n#;
	print qq#<tr>
		<th>ID</th>
		<th>SpID</th>
		<th>M/T</th>
		<th>Seq</th>
		<th>PnsID</th>
		<th>TPP</th>
		<th>Operator</th>
		<th>Stamp</th>
		<th>Memo</th>
		</tr>\n#;

	my $lastSEQ = 0;
	my $lastPNS = 0;
	while ($ID > 1)
	{
		$sth->execute($ID);
		($ID, $spid, $membertype, $operator, $stamp, $memo, $tpp, $pnsid, $seq) = $sth->fetchrow_array;
		$operator = '&nbsp;' if $operator !~ /\w/;
		$memo = '&nbsp;' if $memo !~ /\w/;
		unless($ID)
		{
			print qq#<tr><td colspan="9">Failed to retrieve record for $id</td></tr>\n#;
			last;
		}
		else
		{
			print "<tr class=\"$rowclass\">
				<td>$ID</td>
				<td>$spid</td>
				<td align=\"center\">$membertype</td>";
			if ($lastSEQ && $lastSEQ < $seq)
			{
				print $q->td({'-class'=>'error'}, $seq);
			}
			else
			{
				print $q->td($seq);
			}
			
			if ($lastPNS && $lastPNS != $pnsid)
			{
				print $q->td({'-class'=>'error'}, $pnsid);
			}
			else
			{
				print $q->td($pnsid);
			}
			
			print "<td>$tpp</td>
				<td>$operator</td>
				<td>$stamp</td>
				<td>$memo</td>
				</tr>\n";
			$ID = $spid;
			$rowclass = $rowclass eq 'a' ? 'b':'a';
			
			$lastSEQ = $seq;
			$lastPNS = $pnsid;
		}
	
		if ( Dupe_Ck($ID) )
		{
			print qq#<tr><td colspan="8">Loop detected! Search terminated!</td></tr>#;
			last;
		}
		else
		{
			push (@List, $ID);
		}
	}
	$sth->finish;
	
	print '</table><br />', $q->a({'-href'=>'/admin/upline_checker.html'}, 'Search again.'), $q->end_html;
}

sub Dupe_Ck
{
	my $id = shift;
	foreach (@List)
	{
		return 1 if $id eq $_;
	}
	return 0;
}

###### 03/12/03 revised the dupe checking functionality to break out on dupes besides the submitted ID
###### changed the archive table name
###### 12/15/03 added the stamp to the output for current month
