#!/usr/bin/perl -w
###### masq.cgi
###### manage a CGI::Session that has a bit setup for replacement of a full member login cookie by admin
###### created: 09/12/14	Bill MacArthur
###### last modified: 

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
require ADMIN_DB;
require cs_admin;
use Admin::Masquerade;

my ($db, $creds, @alerts) = ();
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});

###### evaluate our user
###### we will first check for admin credentials
###### if present, we will go in as 01 with 'super credentials'
###### a regular login as 01 will permit the same operations
if (my $ck = $q->cookie('cs_admin'))
{
	$creds = $cs->authenticate($ck);
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
	$db->{'RaiseError'} = 1;
}
else
{
	die "This is an admin only interface. A DB login is required.";
}

my $gs = G_S->new({'db'=>$db, 'CGI'=>$q});
my $masq = Admin::Masquerade->new('dbh'=>$db);

my $action = $q->param('action');

if (! $action)
{
	_selectMasq();
}
elsif ($action eq 'setmasq')
{
	if ($q->param('clearmasq'))
	{
		$masq->Clear();
		push(@alerts, NoticeBlock());
		_selectMasq();
	}
	my $id = $gs->PreProcess_Input( $q->param('transferee'));
	unless ($id)
	{
		push(@alerts, 'No ID parameter received');
		push(@alerts, $masq->NoticeBlock());
		_selectMasq();
	}
	
	if (! $masq->MasqueradeAs($id, $creds->{'username'}))
	{
		push(@alerts, 'Failed to setup session - see error log');
		_selectMasq();
	}
	
	StatusPage();
}
elsif ($action eq 'status')
{
	StatusPage();
}
else
{
	die "Unrecognized action parameter";
}

Exit();

###### end of main

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub NoticeBlock
{
	$masq->NoticeBlock() || 'No masquerading session in place';
}

sub StatusPage
{
	my $rv = NoticeBlock();
	print $masq->Session->header('-charset'=>'utf-8'),
		$q->start_html('-encoding'=>'utf-8', '-title'=>'Masquerade Status'),
		$rv,
		$q->end_html;
		
	Exit();
}

sub _selectMasq
{
	print
		$q->header,
		$q->start_html('Choose party to masquerade as');
		
	foreach (@alerts)
	{
		print $q->div({'-style'=>'color:red'}, $_);
	}
	
	print
		$q->h4('Enter masquerade party ID'),
		$q->start_form('-style'=>'font-size:85%'),
		$q->textfield('-name'=>'transferee', '-style'=>'width:8em; font-size:12px;') . '&nbsp;',
		$q->checkbox('-name'=>'clearmasq', '-value'=>1, '-label'=>'Clear Masquerade', '-style'=>'margin-left:2em', '-force'=>1),
		$q->submit('-value'=>'Set/Clear Masq', '-style'=>'margin-left: 2em; font-size:12px;'),
		$q->hidden('-name'=>'action', '-value'=>'setmasq'),
		$q->end_form,
		$q->end_html;
		
	Exit();
}