#!/usr/bin/perl -w
# mall-vendor-extensions.cgi
# create/update vendor mall vendor extension records
# created Dec.. 2007	Bill MacArthur
# last modified: 02-18-2008		Bill MacArthur
# last modified: 07-25-2008		Keith Hasely
#                02-09-2010             g.baker  postgres 9.0 port SUBSTR
#                                                arg 1 must be text
# last modified: 02/26/16	Bill MacArthur

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
#use Data::Dumper;	# for debugging only
binmode STDOUT, ":encoding(utf8)";

our (@alerts, @errors, $db) = ();
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my @dbcols = qw/vendor_id void mall_id exttype val_text start_date end_date pk translate ignore_translate_for_display/;
my @extended_dbcols = qw//;
my $params = LoadParams();

exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;

my $action = $q->param('_action');
unless ($action) {
	die "Missing mall_id param" unless $params->{mall_id};
	die "Missing vendor_id param" unless $params->{vendor_id};
	ListExtensions();
}
elsif ($action eq 'add') {
	my $x = AddRecord();
	Detail($x) unless @errors;
}
elsif ($action eq 'detail') {
	die "Missing pk parameter\n" unless $params->{pk};
	my $x = GetExtensionDetail($params->{pk});
	Detail($x);
}
elsif ($action eq 'edit') {
	my $x = Update();
	Detail($x) unless @errors;
}
elsif ($action eq 'new'){
	die "Missing mall_id param" unless $params->{mall_id};
	die "Missing vendor_id param" unless $params->{vendor_id};
	Detail();
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect;
exit;



sub AddRecord
{

	my $pk = '';

#	print $q->header('text/plain');

	eval{
		$db->{AutoCommit} = 0;

		($pk) = $db->selectrow_array("SELECT NEXTVAL('mall_vendor_extensions_pk_seq')");

#		print $pk . "\n";

		my $qry = 'INSERT INTO mall_vendor_extensions (pk,';
		my $end = "VALUES ($pk,";
		my @args = ();
		foreach (@dbcols){
			# we don't want to insert NULLs or empties, that seems to end up disabling default column values
			next unless $params->{$_};
			$qry .= "$_,";
			$end .= '?,';
			push @args, $params->{$_};
		}
		chop $qry;
		chop $end;

#		print "$qry ) $end ), undef,\n";
#		print Dumper(@args) . "\n\n";

		$db->do("$qry ) $end )", undef, @args);


			if(defined $q->param('mallcats') &&  $q->param('exttype') =~ /3|4|5/)
			{
				$qry = "INSERT INTO mall_vendor_extensions_links (pk, category, incentive_mallcat_id) VALUES ($pk,?,?)";
				foreach my $incentive_mall_cat_id ($q->param('mallcats'))
				{
					@args = ();
					push @args, 'mallcat';

					# we don't want to insert NULLs or empties, that seems to end up disabling default column values
					next unless $incentive_mall_cat_id;
					push @args, $incentive_mall_cat_id;
		#			print "$qry, undef,\n";
		#			print Dumper(@args);

					$db->do($qry, undef, @args);
				}
			}

			if(defined $q->param('incentive') &&  $q->param('exttype') =~ /3|4|5/)
			{
				$qry = "INSERT INTO mall_vendor_extensions_incentive_links (pk, category, incentive_mallcat_id) VALUES ($pk,?,?)";
				foreach my $incentive_mall_cat_id ($q->param('incentive'))
				{
					@args = ();
					push @args, 'incentive';

					# we don't want to insert NULLs or empties, that seems to end up disabling default column values
					next unless $incentive_mall_cat_id;
					push @args, $incentive_mall_cat_id;
		#			print "$qry, undef,\n";
		#			print Dumper(@args);
					$db->do($qry, undef, @args);
				}


					$db->do("UPDATE mall_vendor_extensions SET ranking = (
						SELECT
							COALESCE(SUM(si.point_value), 0)
						FROM
							shopping_incentives AS si
						WHERE
							si.id IN (
										SELECT
											mveil.incentive_mallcat_id
										FROM
											mall_vendor_extensions_incentive_links AS mveil
										WHERE
											mveil.pk = ?
											AND
											mveil.category = 'incentive'
									)
					) WHERE pk= ?",undef,($pk,$pk));

			}

	};

	if($@)
	{
		push @errors, "Insert failed. DB reported: $DBI::errstr";
#		print 'Insert failed. DB reported: ' . $DBI::errstr;
		$db->rollback();
		$db->{AutoCommit} = 1;
		return undef;
	}

	$db->commit();
	$db->{AutoCommit} = 1;

	return GetExtensionDetail($pk);

}




sub cbo_XType
{
	my $cv = shift;
	my @opts = '';
	my %lbls = (''=>'-----');
	my $sth = $db->prepare('SELECT exttype, label FROM mall_vendor_extension_types ORDER BY exttype');
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref){
		push @opts, $tmp->{exttype};
		$lbls{$tmp->{exttype}} = $tmp->{label};
	}
	return $q->popup_menu(
		-name=>'exttype',
		-id=>'exttype',
		-values=>\@opts,
		-labels=>\%lbls,
		-default=>$cv,
		-onChange=>"enable_disable(this);"
	);
}

sub Detail
{
	###### show the data input/edit form
	my $data = shift || {};	###### a hashref
	my $mall = GetMallDetail($data);
	my $mr = GetMallRelationDetail($data);
	print $q->header(-charset=>'utf-8', -expires=>'now'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => ($data->{pk} ? "Extension Detail" : 'New Extension') . ": $mall->{label} : $mr->{vendor_name}",
			-style => [{-src=>'/css/admin/generic-report.css'},
					{-src=>'/css/admin/mall-mgt.css'},
					{-code=>'label { line-height: 50%}'}],
			-script => [
							{-src=>'/js/admin/vendor-mall-relations.cgi.js'},
							{-code=> "function enable_disable(element_obj){
								document.getElementById('mallcats').disabled = (element_obj.value.match(/3|4|5|8/))?false:true;
								document.getElementById('incentive').disabled = (element_obj.value.match(/3|4|5|8/))?false:true;
								}"
							}
						]
		),
		$q->div({-id=>'instructions'}, $q->a({-href=>'#',-onclick=>'return ToggleHelp()'}, 'Toggle Help')),
		$q->div({-id=>'help'}, q|<ul>
			<li>Dating is not necessary for the Vendor Overview Blurb.</li>
			<li>The default Start Date will be -NOW- if you do not enter it.</li>
			<li>The End Date can be left blank for items that have no expiration.</li>
			<li>Items with a Start Date in the future will show as pending in the list.</li>
			<li>Items with an End Date in the past will show as expired.</li>
			<li>Pending and Expired items will not be part of the vendor presence.</li>
			<li>Properly formed XHTML can be included in the Value.
				The Vendor Banner should be a complete HTML &lt;img&gt; tag, including an alt attribute.</li>
			<li>Remember that space constraints may exist for your content.</li>
			<li>The translation flag will automatically be checked when creating a new entry for a non-English mall store.
				It can be checked/unchecked as necessary for new or existing or modified entries.</li>
			<li>By default, an item flagged for translation will not be shown in a mall.
				Check the "Force Display" box to show the item regardless.</li>
			<li>Check the Void box to disable an item 'immediately'. Uncheck it to re-enable it.</li>
			</ul>
		|),
		$q->h5(($data->{pk} ? "Extension Detail" : 'New Extension') . ": $mall->{label} : $mr->{vendor_name}");
	print	$q->start_form(-action=>$q->script_name, -method=>'post'),
		$q->hidden('vendor_id'), $q->hidden('mall_id'), $q->hidden(-name=>'pk', -value=>$data->{pk} || '');

	print q|<table id="detail-tbl">|;

	print $q->Tr(
		$q->td({-class=>'label'}, 'Extension Type') .
		$q->td( cbo_XType($data->{exttype}) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Start Date') .
		$q->td( $q->textfield(-id=>'start_date', -name=>'start_date',
			-value=>EMD($data->{start_date}), -force=>1) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'End Date') .
		$q->td( $q->textfield(-id=>'end_date', -name=>'end_date', -value=>EMD($data->{end_date}), -force=>1) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Value') .
		$q->td( $q->textarea(-id=>'val_text', -name=>'val_text', -rows=>3, -cols=>50,
			-value=>EMD($data->{val_text})) ));

#	$data->{pk}


my $sth = '';
my $elements_enabled = $data->{exttype} =~ /3|4|5/?0:1;

if ($q->param('_action') eq 'new')
{
	$sth = $db->prepare("SELECT
		DISTINCT(mc.catid),
		mc.catname,
		vendor_categories_text_list(mc.catid) AS name,
		mc.catid AS selected
	FROM
		vendor_categories vc
		JOIN
		questionnaires_answers_links AS qal
		ON
		qal.incentive_mallcat_id = vc.catid
		AND
		qal.category = 'mallcat'
		INNER JOIN
		mallcats mc
		ON
		vc.catid = mc.catid
	WHERE
		vc.vendor_id = ?
	ORDER BY
		name");

	$sth->execute($mr->{vendor_id});
} else {

	$sth = $db->prepare("SELECT
		DISTINCT(mc.catid),
		mc.catname,
		vendor_categories_text_list(mc.catid) AS name,
		mvel.incentive_mallcat_id AS selected
	FROM
		vendor_categories vc
		JOIN
		questionnaires_answers_links AS qal
		ON
		qal.incentive_mallcat_id = vc.catid
		AND
		qal.category = 'mallcat'
		INNER JOIN
		mallcats mc
		ON
		vc.catid = mc.catid
		LEFT JOIN
		mall_vendor_extensions_links AS mvel
		ON
		mvel.incentive_mallcat_id = mc.catid
		AND
		mvel.category = 'mallcat'
		AND
		mvel.pk=?
	WHERE
		vc.vendor_id = ?
	ORDER BY
		name");

	$sth->execute($data->{pk}, $mr->{vendor_id});


}


	my $category_array = [];
	my $selected_category_array = [];
	my $category_hash = {};

	while (my $row = $sth->fetchrow_hashref)
	{
		$category_array->[scalar(@{$category_array})] = $row->{catid};
		$selected_category_array->[scalar(@{$selected_category_array})] = $row->{selected} if ($row->{selected});
		$category_hash->{$row->{catid}} = $row->{name};

	}

	print $q->Tr(
		$q->td({-class=>'label'}, 'Vendor Specific Mall Categories)') .
		$q->td( $q->scrolling_list(-id=>'mallcats', -name=>'mallcats', -size=>20,
			-value=>$category_array, -default=>$selected_category_array, -labels=>$category_hash, -multiple=>'multiple') ));

if ($q->param('_action') eq 'new')
{
	$sth = $db->prepare('SELECT si.id, si.name, 0 AS selected FROM shopping_incentives AS si');
	$sth->execute();
} else {
	$sth = $db->prepare("
				SELECT
					si.id,
					si.name,
					mvel.incentive_mallcat_id as selected
				FROM
					shopping_incentives AS si
					LEFT JOIN
						mall_vendor_extensions_incentive_links AS mvel
						ON
						mvel.incentive_mallcat_id = si.id
						AND
						mvel.category = 'incentive'
						AND
						mvel.pk=?
		");

	$sth->execute($data->{pk});

}

	my $incentives_array = [''];
	my $selected_incentives_array = [];
	my $incentives_hash = {''=>''};


	while (my $row = $sth->fetchrow_hashref)
	{
		$incentives_array->[scalar(@{$incentives_array})] = $row->{id};
		$incentives_hash->{$row->{id}} = $row->{name};

		$selected_incentives_array->[scalar(@{$selected_incentives_array})] = $row->{selected} if ($row->{selected});
	}



	print $q->Tr(
		$q->td({-class=>'label'}, 'Shopping Incentives') .
		$q->td(
				$q->scrolling_list(
									-name=>'incentive',
									-id=>'incentive',
									-value=>$incentives_array,
									-labels=>$incentives_hash,
									-default=>$selected_incentives_array,
									-multiple=>'multiple'
								)
				)
	);



	if($elements_enabled)
	{
		print "<script type='text/javascript'>enable_disable(document.getElementById('exttype'))</script>";
	}


#	print $q->Tr(
#		$q->td({-class=>'label'}, 'Dump') .
#		$q->td( Dumper($mr))
#	);

	###### set our default translation flag for a new entry, based on the mall language
	$data->{translate} = 1 if $mall->{language_code} ne 'en' and not $data->{pk};
	print $q->Tr(
		$q->td({-class=>'label'}, 'Flag for translation') .
		$q->td( $q->checkbox(-id=>'translate', -name=>'translate', -label=>'', -class=>'ckbx1',
				-checked=>$data->{translate}, -value=>'TRUE') ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Force Display') .
		$q->td( $q->checkbox(-name=>'ignore_translate_for_display', -class=>'ckbx1',
				-value=>'TRUE', -label=>'') ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Void') .
		$q->td( $q->checkbox(-id=>'void', -name=>'void', -label=>'', -class=>'ckbx1',
				-checked=>$data->{void}, -value=>'TRUE') ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Timestamp') .
		$q->td($q->textfield(-id=>'stamp', -name=>'stamp', -force=>1,
				-disabled=>'disabled', -value=>$data->{stamp})	)) if $data->{stamp};
	print $q->Tr(
		$q->td({-class=>'label'}, 'Last Modified by') .
		$q->td($q->textfield(-id=>'operator', -name=>'operator', -force=>1,
				-disabled=>'disabled', -value=>$data->{operator}) )) if $data->{operator};

	print 	$q->Tr( $q->td({-colspan=>2, -align=>'right'},
			$q->submit($data->{pk} ? 'Submit' : 'Create New Extension') .
			$q->reset(-id=>'reset') ));
	print '</table>';
	print	$q->hidden(-name=>'_action', -value=>($data->{pk} ? 'edit' : 'add'), -force=>1),
		$q->end_form, $q->p({-class=>'fpnotes'},
			$q->a({-href=>'#',
			-onclick=>'if(window.opener){window.opener.location.reload();self.close();}'}, 'Close Window') .
			$q->a({-href=>$q->url . "?_action=new;mall_id=$mr->{mall_id};vendor_id=$mr->{vendor_id}",
				-style=>'margin-left:2em'}, 'Create New Extension')),
		$q->end_html;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	return undef unless defined $_[0];
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub GetExtensionDetail {
	return $db->selectrow_hashref('SELECT * FROM mall_vendor_extensions WHERE pk= ?', undef, $_[0]) ||
		die "Failed to pull an exception record with that pk: $_[0]\n"
}

sub GetMallDetail {
	$params->{mall_id} ||= $_[0]->{mall_id};
	return $db->selectrow_hashref('SELECT * FROM malls WHERE mall_id= ?', undef, $params->{mall_id}) ||
		die "Failed to get mall details for: $params->{mall_id}\n";

}

sub GetMallRelationDetail {
	$params->{vendor_id} ||= $_[0]->{vendor_id};
	$params->{mall_id} ||= $_[0]->{mall_id};
	my $mr = $db->selectrow_hashref('
		SELECT v.vendor_name, mv.vendor_id, mv.mall_id
		FROM mall_vendors mv, vendors v
		WHERE mv.vendor_id=v.vendor_id AND mv.mall_id= ? AND mv.vendor_id= ?',
		undef, $params->{mall_id}, $params->{vendor_id}) ||
		die "Failed to get mall relation details using submitted mall_id and vendor_id\n";
	$mr->{vendor_name} = $q->escapeHTML($mr->{vendor_name});
	return $mr;
}

sub ListExtensions
{
	my $mall = GetMallDetail();
	my $mr = GetMallRelationDetail();
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => "Vendor Extensions: $mall->{label} : $mr->{vendor_name}",
			-style => [{-src=>'/css/admin/generic-report.css'}, {-src=>'/css/admin/mall-mgt.css'}, {-src=>'/css/admin/vendor-mall-extensions.css'}],
			-script => {-src=>'/js/admin/vendor-mall-relations.cgi.js'}
		),
		$q->div({-id=>'instructions'}, $q->a({-href=>'#',-onclick=>'return ToggleHelp()'}, 'Toggle Help')),
		$q->div({-id=>'help'}, q|<ul>
			<li class="active">Active items are hilighted like this.</li>
			<li class="pending">Pending items like this.</li>
			<li class="expired">Expired items like this.</li>
			<li class="voided">Voided items like this.</li>
			<li>The "Vendor Overview Blurb" and the "Vendor Banner" will show as active regardless of the end date.</li>
			</ul>
		|),
		$q->h5("Vendor Extensions: $mall->{label} : $mr->{vendor_name}");
	print $q->p({-class=>'fpnotes'},
		$q->a({-href=>$q->url . "?_action=new;mall_id=$mr->{mall_id};vendor_id=$mr->{vendor_id}",
			-onclick=>'window.open(this.href);return false;'}, 'Create New Extension'));
	print q|<table class="report"><thead><tr><th></th><th>Extension Type</th><th>Start Date</th><th>End Date</th>
		<th>Value</th><th>Void</th><th>Last Modified</th><th>By:</th></tr></thead><tbody>|;
	my $sth = $db->prepare(q|
		SELECT mre.exttype,
			mre.val_text,
			mre.start_date,
			COALESCE(mre.end_date::TEXT,'') AS end_date,
			SUBSTR(mre.stamp::TEXT,0,20) AS stamp,
			mre.pk,
			mre.operator,
			CASE WHEN void=TRUE THEN 'x'::TEXT ELSE ''::TEXT END AS void,
			mx.label,
			CASE WHEN mre.exttype IN (1,2) THEN 2::INT	-- our blurb and our banner never expires
				WHEN start_date > NOW() THEN 1::INT
				WHEN end_date < NOW()::DATE THEN 0::INT
				ELSE 2::INT
			END AS active
		FROM mall_vendor_extensions mre, mall_vendor_extension_types mx
		WHERE mre.exttype=mx.exttype AND mre.mall_id= ? AND mre.vendor_id= ?
		ORDER BY active DESC, mre.end_date DESC NULLS FIRST, mre.exttype|);
	$sth->execute($params->{mall_id}, $params->{vendor_id});
	my $class = 'a';
	my %xclass = (0=>'expired', 1=>'pending', 2=>'active');	###### additional HTML classes
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$tmp->{val_text} = $q->escapeHTML($tmp->{val_text});
		$class = 'voided' if $tmp->{void};
		print qq|<tr class="$class $xclass{$tmp->{active}}">|;
		print $q->td({-class=>'edit'}, $q->a({-href=>$q->script_name . "?pk=$tmp->{pk};_action=detail",
			-onclick=>'window.open(this.href);return false;'}, 'Edit'));
		print qq|<td class="exttype">$tmp->{label}</td>
			<td class="date">$tmp->{start_date}</td><td class="date">$tmp->{end_date}</td>
			<td class="xvalue">$tmp->{val_text}</td><td class="void_flg">$tmp->{void}</td>
			<td class="stamp">$tmp->{stamp}</td><td>$tmp->{operator}</td></tr>|;
		$class = $class eq 'a' ? 'b':'a';
	}
	print '</tbody></table>',
		$q->p({-class=>'fpnotes'}, $q->a({-href=>'#', -onclick=>'self.close()'}, 'Close Window'));
	print $q->end_html;
}

sub LoadParams
{
	my $rv = {};
	foreach(@dbcols)
	{
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	# since we are using a checkbox on the interface, with a value of TRUE when checked
	# we don't get FALSE when not checked :)
	foreach (qw/void translate ignore_translate_for_display/){
		$rv->{$_} = $rv->{$_} ? 'TRUE' : 'FALSE';
	}
	return $rv;
}

sub Update {

	eval{
		$db->{AutoCommit} = 0;


		$db->do("DELETE FROM mall_vendor_extensions_links WHERE pk = ?", undef, $params->{pk});

		$db->do("DELETE FROM mall_vendor_extensions_incentive_links WHERE pk = ?", undef, $params->{pk});

		my $qry = "INSERT INTO mall_vendor_extensions_links (pk, category, incentive_mallcat_id) VALUES (?,?,?)";
		foreach my $incentive_mall_cat_id ($q->param('mallcats'))
		{
			my @args = ($params->{pk}, 'mallcat');

			# we don't want to insert NULLs or empties, that seems to end up disabling default column values
			next unless $incentive_mall_cat_id;
			push @args, $incentive_mall_cat_id;
#			print "$qry, undef,\n";
#			print Dumper(@args);

			$db->do($qry, undef, @args);
		}

		foreach my $incentive_mall_cat_id ($q->param('incentive'))
		{
			$qry = "INSERT INTO mall_vendor_extensions_incentive_links (pk, category, incentive_mallcat_id) VALUES (?,?,?)";
			my @args = ($params->{pk}, 'incentive');

			# we don't want to insert NULLs or empties, that seems to end up disabling default column values
			next unless $incentive_mall_cat_id;
			push @args, $incentive_mall_cat_id;
#			print "$qry, undef,\n";
#			print Dumper(@args);
			$db->do($qry, undef, @args);
		}


		$qry = 'UPDATE mall_vendor_extensions SET ';
		foreach my $p (@dbcols){
			next if grep $p eq $_, ('vendor_id','mall_id','pk');
			$qry .= "$p=";
			$qry .= length ($params->{$p} ||'') > 0 ? $db->quote($params->{$p}) : 'NULL';
			$qry .= ',';
		}
		$qry .= 'stamp=NOW(), operator=current_user, ';
		$qry .= " ranking = (
								SELECT
									COALESCE(SUM(si.point_value), 0)
								FROM
									shopping_incentives AS si
								WHERE
									si.id IN (
												SELECT
													mvel.incentive_mallcat_id
												FROM
													mall_vendor_extensions_incentive_links AS mvel
												WHERE
													mvel.pk = ?
													AND
													mvel.category = 'incentive'
											)
								) ";
		$qry .= ' WHERE pk= ?';

		$db->do($qry, undef, ($params->{pk}, $params->{pk}));







	};

	if($@)
	{
#		print 'Update failed. DB reported: ' . $DBI::errstr;
		$db->rollback();
		$db->{AutoCommit} = 1;
		die "Update failed. DB reported: $DBI::errstr\n"
	}

	$db->commit();
	$db->{AutoCommit} = 1;


	return GetExtensionDetail($params->{pk});
}

###### 02/15/08 added handling for the 'translate' flag in the extensions
###### 02/18/08 added handling for the ignore_translate_for_display flag
# 05/06/15 changed the ordering in ListExtensions()