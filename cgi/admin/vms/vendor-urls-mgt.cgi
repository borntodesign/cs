#!/usr/bin/perl -w
# vendor-urls-mgt.cgi
# 
# created Feb. 2007	Bill MacArthur
# last modified: 

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use G_S;

our (@alerts, @inst, @errors, @msg, $db) = ();
my %TMPL = (main=>10575);
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my @dbcols = qw/ad_id vendor_id url start_date end_date staff_notes active/; # the cols we will use params for

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
if ( $operator && $pwd )
{
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
	$db->{RaiseError} = 1;
}
else
{
	push @errors, 'You must be logged in to use this interface.';
	goto 'END';
}

#die $q->self_url;

my $action = $q->param('_action');
unless ($action) {
	# the initial load
	Display_Form();
}
elsif ($action eq 'add') {
	AddRecord();
}
elsif ($action eq 'lookup') {
	###### 
	PullRecords();
}
elsif ($action eq 'edit') {
	Update();
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect if $db;
exit;

sub AddRecord {
	my ($adid) = $db->selectrow_array("SELECT nextval('vendor_urls_advid_seq')") ||
		die "Failed to obtain a new ad_id";

	$q->param(-name=>'ad_id', -value=>$adid);
	my $qry = 'INSERT INTO vendor_urls (';
	my $end = 'VALUES (';
	foreach (@dbcols){
		# since we are using a checkbox on the interface, with a value of TRUE when checked
		# we don't get FALSE when not checked :)
		$q->param(-name=>$_, -value=>'FALSE') if $_ eq 'active' && not $q->param($_);

		# we don't want to insert NULLs, that seems to end up disabling default column values
		next unless $q->param($_);
		$qry .= "$_,";
		$end .= $db->quote(FilterWhiteSpace($q->param($_))) . ',';
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )");
	if ($rv eq '1'){
		print $q->header('text/xml'),
			CreateParamNode($db->selectrow_hashref("SELECT * FROM vendor_urls WHERE ad_id= $adid"));
	} else {
		push (@alerts, $DBI::errstr);
	}
}

sub CreateList {
	my $sth = $db->prepare("
		SELECT vendor_name, vendor_id
		FROM vendors WHERE status=1 AND vendor_group=2
		ORDER BY vendor_name");
	$sth->execute;
	my $rv = '';
	while (my $tmp = $sth->fetchrow_hashref){
		$rv .= qq|<option value="$tmp->{vendor_id}">| . $q->escapeHTML($tmp->{vendor_name}) . '</option>';
	}
	return $rv;
}

sub CreateParamNode {
	my $tmp = shift || return '';
	my $xml = '<params>';
	foreach (keys %{$tmp}){
		$xml .= "<$_>" . $q->escapeHTML($tmp->{$_}) . "</$_>";
	}
	$xml .= '</params>';
	return $xml;
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub Display_Form {
	my $page = G_S::Get_Object($db, $TMPL{'main'}) || die "Failed to retrieve page template $TMPL{'main'}";
	$page =~ s/%%url%%/$q->url/eg;
	$page =~ s/%%vendor_list%%/CreateList()/e;
	print $q->header(-expires=>'now', -type=>'text/html', -charset=>'utf-8'), $page;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub PullRecords {
	unless ($q->param('vendor_id')){
		push (@errors, 'No  vendor ID');
		return undef;
	}
	my $xml = '<root><vendor>';
	my $vendor = $db->selectrow_hashref("SELECT * FROM vendors WHERE vendor_id=?",undef,$q->param('vendor_id')) ||
		die "Failed to retrieve vendor record for " . $q->param('vendor_id');
	foreach (keys %{$vendor}){
		$xml .= "<$_>" . $q->escapeHTML($vendor->{$_}) . "</$_>";
	}
	$xml .= '</vendor>';
	my $sql = 'SELECT * from vendor_urls WHERE vendor_id=?';
	if ($q->param('active')){
		$sql .= (' AND active=' . ($q->param('active') eq 'true' ? 'TRUE' : 'FALSE'));
	}
	$sql .= ' ORDER BY ad_id';
	my $sth = $db->prepare($sql);
	$sth->execute($q->param('vendor_id'));
	$xml .= '<ads>';
	while (my $tmp = $sth->fetchrow_hashref){
		$xml .= CreateParamNode($tmp)
	}
	$xml .= '</ads></root>';
	print $q->header('text/xml'), $xml;
}

sub Update {
	foreach (qw/ad_id url staff_notes/){
		push (@errors, "Failed to receive $_ param") unless $q->param($_);
	}
	return if @errors;
	my $qry = 'UPDATE vendor_urls SET ';
	foreach my $p (@dbcols){
		next if grep $p eq $_, ('ad_id','vendor_id');
		# since we are using a checkbox on the interface, with a value of TRUE when checked
		# we don't get FALSE when not checked :)
		$q->param(-name=>$_, -value=>'FALSE') if $p eq 'active' && not $q->param($p);

		my $parm = FilterWhiteSpace($q->param($p));
		$qry .= "$p=";
		$qry .= length ($parm ||'') > 0 ? $db->quote($parm) : 'NULL';
		$qry .= ',';
	}
	$qry .= 'stamp=NOW(), operator=' . $db->quote($operator);
	$qry .= ' WHERE ad_id= ?';

	if ($db->do($qry, undef, $q->param('ad_id')) eq '1'){
		print $q->header('text/xml'),
			CreateParamNode($db->selectrow_hashref("SELECT * FROM vendor_urls WHERE ad_id= ?",undef,$q->param('ad_id')));
	}
	else { push (@alerts, 'Record edit failed:<br />' . $DBI::errstr); }
}

# 
