#!/usr/bin/perl -w
###### Add_New_Vendor.cgi
###### 09/16/2002	Stephen Martin
###### last modified: 05/09/08	Bill MacArthur

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
use G_S;

###### Globals
my ($db, $sth, $rv, $data, $HTML);
my (@cookie, $TEXT);
my $q = new CGI;
unless ( $q->https ){
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
	exit;
}
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd = $q->cookie('pwd') || $q->param('pwd');
my $action = $q->param("action") || '';

unless ($operator && $pwd)
{
 print $q->header(-expires=>'-1d', -charset=>'utf-8'), $q->start_html(),
 $q->h3("Incorrect login.<br />Back up and try again"), $q->end_html(); 
 exit;
}

exit unless $db = ADMIN_DB::DB_Connect( 'index.cgi', $operator, $pwd );

if ($action eq "Add New Vendor" )
{
	###### since this is an admin script, let's rely on the client side javascript to weed things out
#  unless ( $vendor_name && $url && $notes && is_numeric($discount) &&  is_numeric($payout))
#  {
#  	Err("You are missing a form input.<br />Or.. a number input is <b>NOT</b> Numeric.");
# 	goto 'END'; 
#  }
#  
#  unless ( $vendor_type && $vendor_group && $country )
#  {
#  	Err("You are missing a form input.");
# 	goto 'END';
#  }
 
	my @param_list = qw/
		vendor_name
		discount
		payout
		vendor_group
		status
		url
		country
		coupon
		blurb
		b2b_url
		notes cv_mult reb_mult comm_mult/;
	my $vendor_id = $db->selectrow_array("SELECT NEXTVAL('vendor_id_seq')");
	my @param_vals = ();
	my $qry = 'INSERT INTO vendors (vendor_id,';
	my $qry_end = ') VALUES(?,';
	foreach my $p (@param_list)
	{
		my $prm = TrimWhitespace($q->param($p));
		next unless $prm gt '';
		$qry .= "$p,";
		$qry_end .= '?,';
		push (@param_vals, $prm);
	}
	chop $qry;
	chop $qry_end;
	$qry .= $qry_end . ')';

 $sth = $db->prepare($qry);

 $rv = $sth->execute($vendor_id, @param_vals);

  defined $rv or die $sth->errstr;

 $rv = $sth->finish();

	###### for Mall vendors, send 'em off to create a vendor/affiliate relationship
	if ($q->param('vendor_group') == 2)
	{
#		print $q->redirect("https://www.clubshop.com/cgi/admin/vms/aff2vendor.cgi?vendor=$param_vals[0];flag=newvendor");
	###### let's send 'em straight to the vendor edit interface instead
	###### they can setup affiliate relationships and do their mall linkings from there
		print $q->redirect("https://www.clubshop.com/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=$vendor_id;flag=newvendor");
		goto 'END';
	}
}
###### Generate Vendor Groups

my $vendor_group_menu = gen_menu("vendor_group","transaction_types","trans_type","description");

###### Generate Status Menu 

my $status_menu = gen_menu("status","vendor_status_types","status_type","status");

###### Generate Country Menu 

my $country_menu = gen_menu("country","tbl_countrycodes","country_code","country_name");

###### Load the Template 
 
my $TMPL = G_S::Get_Object($db, 10018) || die "Failed to retrieve template #10018\n";

#$TMPL =~ s/%%vendor_type%%/$vendor_type_menu/g;
$TMPL =~ s/%%primary_mallcat%%/Primary_Mallcat()/e;
$TMPL =~ s/%%vendor_group%%/$vendor_group_menu/g;
$TMPL =~ s/%%status%%/$status_menu/g;
$TMPL =~ s/%%country%%/$country_menu/g;
$TMPL =~ s/%%form_action%%/$q->url/eg;
print $q->header(-expires=>'-1d', -charset=>'utf-8');

print $TMPL;


END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-expires=>'-1d'), $q->start_html();
	print $q->($_[0]);
	print $q->end_html();
}


sub getnum {
    use POSIX qw(strtod);
    my $str = shift;
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    $! = 0;
    my($num, $unparsed) = strtod($str);
    if (($str eq '') || ($unparsed != 0) || $!) {
        return;
    } else {
        return $num;
    } }


sub is_numeric { defined scalar &getnum(shift) }


sub gen_menu {
    my ( $menu_name, $table_name, $index_name, $desc_name ) = @_;
    my $dex;
    my $desc;

  my $qry = "SELECT $index_name, $desc_name from $table_name ORDER BY $index_name";

  $sth = $db->prepare($qry);

  $rv = $sth->execute();
  defined $rv or die $sth->errstr;

  $sth->bind_columns(undef, \$dex,\$desc);

  my $html = qq|<select name="$menu_name">\n|;

  while   ($sth->fetch() )
  {
	$html .= qq|<option value="$dex">| . $q->escapeHTML($desc) . "</option>\n";
  }

  $html .= "</select>\n";

  $rv = $sth->finish();

  return $html;
}

sub Primary_Mallcat
{
	return q|<select name="primary_mallcat" id="primary_mallcat">| .
	Primary_Mallcat_Options() . '</select>';
}

sub Primary_Mallcat_Options
{
	my ($loop, @unlinked) = ();
	my $sth = $db->prepare('SELECT catid, catname FROM mallcats WHERE parent=0 ORDER BY catname');
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$loop = qq|<option value="$tmp->{catid}">|;
		$loop .= $q->escapeHTML($tmp->{catname}) . '</option>';
		push @unlinked, $loop;
	}

	my $rv = '<option value="">Select a Primary Mall Category</option>';
	$rv .= $_ foreach @unlinked;
	return $rv;
}

sub TrimWhitespace {
	my $v = shift;
	return '' unless defined $v;
	$v =~ s/^\s*//;
	$v =~ s/\s*$//;
	return $v;
}
###### 07/03/03 made the vendor addition rely on a sequence rather than on the highest existing vendor ID
###### 08/25/05 Commented out the 'display Existing Vendors' section 
######		due to non-use and initialization errors.
###### 02/06/07 various cleanup and added handling for passing a successful addition of a Mall vendor to
###### aff2vendor
# 02/20/07 replaced the call to PreProcess to the local TrimWhitespace
###### 05/09/08 ripped out the vendor_type stuff and added the primary_mallcat
