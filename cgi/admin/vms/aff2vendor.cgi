#!/usr/bin/perl -w

##
## aff2vendor.cgi
##
## Admin interface for editing Linkshare Affiliate/Vendor Name mappings
##
## Deployed: 01/17/07	Shane Partain
##
## last modified: 02/08/10	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

our $q = new CGI;
our $db = '';

my %params = ();
foreach ($q->param()) {$params{$_} = G_S::PreProcess_Input($q->param($_));}

my @controls = ();
my @rows = ();
my %vendors = ();
my %affiliates = ();
my @existing_maps = ();
my $aff2vendors = '';
my $i=0;
my $hidden_inputs = '';
my $vendor_id = '';
my $edit = '';

### table headers
my @th = ('Vendor', 'Affiliate', 'Advertiser Name', 'Adv ID', '', 'Modified', 'By');
push @rows, $q->th(\@th);

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect('aff2vendor.cgi', $operator, $pwd)) {exit;}
$db->{RaiseError} = 1;

unless (%params) {

	printPage(getVendorList("no_vendor_param"));

}else{

	##
	## Find out which row of the form was submitted.
	##
	foreach (keys %params) { 
		if (/^edit(\d)$/) {
			$edit = $1;
			$vendor_id = $params{vend};
		}
	}
	unless ($edit) { $vendor_id = $params{vendor}; }
	
	$aff2vendors = $db->selectall_hashref("    	SELECT *
							FROM linkshare_affiliates_2vendors
							WHERE vendor_id=$vendor_id", "affiliate_code"
						);
	my $affiliates_rv = $db->selectall_hashref("    SELECT *
	                                                FROM linkshare_affiliates
	                                                WHERE active='true'", "code"
	                                        );

	##
	## Create the hash and sorted array for the affiliate dropdown.
	##
	foreach (keys %{$affiliates_rv}) {$affiliates{$_} = $affiliates_rv->{$_}->{name};}
	my @affiliate_names = (sort {$affiliates{$a} cmp $affiliates{$b}} keys %affiliates);
	
	##
	## Update/insert if this is an edit/add request.
	##
	foreach (keys %params){ if (/map/) { updateMap(); } }

	(my $vendor_dropdown, my $vendor_name) = getVendorList();
	
	##
	## Create the rows and hidden parameters for any existing mappings.
	##
	if ($aff2vendors) {
		foreach(keys %{$aff2vendors}) {
			$i++;
			push @existing_maps, $vendor_name;
			push @existing_maps, $affiliates_rv->{$_}->{name};
			push @existing_maps, $q->textfield(-name=>"map$i",
							-size=>25,
							-value=>$aff2vendors->{$_}->{vendor_name_map});
			push @existing_maps, $q->textfield(-name=>"adv_id$i",
                                                        -size=>10,
                                                        -value=>$aff2vendors->{$_}->{advertiser_id});
			push @existing_maps, $q->submit(-name=>"edit$i", -value=>'Edit');
			$aff2vendors->{$_}->{stamp} =~ s/^(.+)\..+$/$1/;
			push @existing_maps, $aff2vendors->{$_}->{stamp};
			push @existing_maps, $aff2vendors->{$_}->{operator};
			push @rows, $q->td(\@existing_maps);
			@existing_maps = ();
			$hidden_inputs .= $q->hidden(-name=>"aff$i", -value=>$affiliates_rv->{$_}->{code});
		}
	}

	##
	## Create the "add" row.
	##
	push @controls, $vendor_dropdown;	
	push @controls, $q->popup_menu( -name=>'affiliate',
	                                -values=>['', @affiliate_names],
	                                -default=>'',
	                                -labels=>\%affiliates,
	                                'id="affiliate"');
	push @controls, $q->textfield(-name=>'map', -size=>25);
	push @controls, $q->textfield(-name=>"adv_id", -size=>10);
	push @controls, $q->submit(-name=>"Add");
	push @rows, $q->td(\@controls);
	
	$hidden_inputs .= $q->hidden(-name=>'vend', -value=>$vendor_id);
	printPage( $q->table({-id=>'main_tbl'}, $q->Tr(\@rows)), $hidden_inputs );

}

$db->disconnect;

sub Err
{
	print $q->header(), $q->start_html();
	print $_[0];
	print "<br />\n";
	print $q->end_html();
}

sub getVendorList
{

	##
	## Anything passed in causes the dropdown to have an onchange event, since it will
	## be used alone on the page, in a case where no params are passed to the script.
	##
	my $onchange = shift || '';
	if ($onchange) { $onchange ='aff2vendor.submit()';}
	my $vendors_rv = $db->selectall_hashref("
	       SELECT v.vendor_id, COALESCE(mal.location_name, v.vendor_name) AS vendor_name
		FROM vendors v
		LEFT JOIN merchant_affiliate_locations mal
			ON v.vendor_id=mal.vendor_id
		WHERE v.status=1
		AND v.vendor_group IN (2,3,6)", "vendor_id");
	##
	## Creates a hash and sorted array for the dropdown.
	##
        foreach (keys %{$vendors_rv}) {$vendors{$_} = $vendors_rv->{$_}->{vendor_name};}
        my @vendor_names = (sort {$vendors{$a} cmp $vendors{$b}} keys %vendors);

        my $vendor_list = $q->popup_menu( -name=>'vendor',
                                        -values=>['', @vendor_names],
                                        -default=>'',
                                        -labels=>\%vendors,
					-onChange=>$onchange,
					'id="vendor"'
                                        );

	##
	## Return the dropdown and the vendor name.
	##
	return $vendor_list, $vendors{$vendor_id};

}

sub NewVendorAlert
{
	return '' unless $params{flag} && $params{flag} eq 'newvendor';
	return $q->p({-id=>'newvendoralert'},'
		It is highly recommended that you create the affiliate/vendor mapping now.<br />
		If you choose not to do so, any sales attributed to this vendor will get error flagged
		during any automated data import process.
	');
}

sub printPage{

	my $content = shift;
	my $hidden = shift;

	print $q->header(-charset=>'utf-8'),
       $q->start_html(-title=>'Affiliate/Vendor Name Mappings',
			 -style=>{-src=>'/css/aff2vendor.css'},
			-encoding=>'utf-8'),
	$q->h3('Affiliate/Vendor Name Mappings'),
	NewVendorAlert(),
	$q->start_form(-name=>'aff2vendor', -method=>'GET'),
	$content,
	$hidden,
	$q->end_form();
	print "<ul id=\"instructions\">
		<li>The 'Advertiser Name' should reflect the exact spelling that the affiliate will use in a downloaded report.</li>
		<li>If the affiliate uses advertiser IDs, then fill in the 'Adv ID' field with that value.</li></ul>\n";

	print $q->end_html();
}
sub updateMap{
	
	my $param_num = '';
	my $vendor = $vendor_id;
	my $aff_code = '';
	my $map = '';
	my $adv_id = '';
	my @existing = '';

	##
	## If this is a request to edit an existing record, we need to identify
	## the parameters that are in the row of the form being submitted.
	##
	if ($edit) {
		$param_num = $edit;
		foreach ($params{ ("map" . $param_num) }, $params{ ("adv_id" . $param_num) }) {
			 s/^\s+//;
			 s/\s+$//;
		}	
		$aff_code = $db->quote($params{ ("aff" . $param_num) });
		$map = $params{ ("map" . $param_num) } ? $db->quote($params{ ("map" . $param_num) }) : 'NULL';
#		$map = $db->quote($params{ ("map" . $param_num) });
		$adv_id = $params{ ("adv_id" . $param_num) } ? $db->quote($params{ ("adv_id" . $param_num) }) : 'NULL';
#		$adv_id = $db->quote($params{ ("adv_id" . $param_num) });

		$db->do("UPDATE linkshare_affiliates_2vendors
			SET vendor_name_map=$map, advertiser_id=$adv_id
			WHERE vendor_id=$vendor
			AND affiliate_code=$aff_code
			");
	##
	## Otherwise, just use the parameters from the "Add" row in the form.
	##
	}else{
		foreach ( $params{map}, $params{adv_id} ) {
                	s/^\s+//;
                	s/\s+$//;
                }
		$aff_code = $db->quote($params{affiliate});
		$map = $params{map} ? $db->quote($params{map}) : 'NULL';
#		$map = $db->quote($params{map});
		$adv_id = $params{adv_id} ? $db->quote($params{adv_id}) : 'NULL';
#		$adv_id = $db->quote($params{adv_id});
		
		@existing = $db->selectrow_array(	"SELECT *
							FROM linkshare_affiliates_2vendors
							WHERE vendor_id=$vendor
                                			AND affiliate_code=$aff_code");
		unless (@existing) {
			$db->do("INSERT INTO linkshare_affiliates_2vendors(affiliate_code, vendor_name_map, vendor_id, advertiser_id)
				values($aff_code, $map, $vendor, $adv_id)");
		}
	}
	$db->disconnect;
	
	##
	## Print out a redirect to avoid confusing the script with the existing parameters.
	##
	my $redurl = ($q->url . '?vendor=' . $vendor);
	print $q->redirect($redurl);
	exit;

}

###### 02/06/07 added NewVendorAlert
# 04/19/07 tweak to also pull in outlet center 'vendors'
# 02/08/10 tweak to also pull in "marketplace" vendors
