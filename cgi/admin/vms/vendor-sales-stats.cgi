#!/usr/bin/perl -w
##
## vendor-sales-stats.cgi
## present sales aggregates and vendors without any sales
## released: 08/06/07	Bill MacArthur
## modified: 
##           02/09/2011 g.baker postgres 9.0 SUBSTR arg 1 must be TEXT

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
use ADMIN_DB;

my $errormsg = '';
my $REPORT = '';
my %Config = _config_();
my @DateRanges = ('Y-T-D', 'M-T-D', 'Last Month', 'Custom - enter below');
###### the following values need to exactly match the 'columns' returned by the SQL
my @Global_Order_By = ('Vendor', 'Sales Total', '# of Sales', 'Commissions', 'Average Comm. %', 'Last Change Date');
my $q = new CGI;
my $qt = $q->param('query_type') || '';
my @date = localtime;
$date[5] += 1900;
my @year_options = ();
for (my $x = 2007; $x <= $date[5]; $x++){ push @year_options, $x; }	###### plan ahead ;)

## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my $db = ADMIN_DB::DB_Connect('tracking_search.cgi', $operator, $pwd) || exit;
$db->{'RaiseError'} = 1;

if ($q->param)
{
	die "Invalid query type parameter\n" unless grep $qt eq $_, keys %Config;

	my $sql = $Config{$qt}->{'sql'};
	$sql =~ s/-- EXTRA COLS/,$Config{$qt}->{'extra_cols'}/ if $Config{$qt}->{'extra_cols'};
	
	if ($q->param('date_range') =~ /Custom/)
	{
		$sql =~ s/<STARTDATE>/$db->quote($q->param('start_year') . '-' . $q->param('start_month') . '-01')/e;
		$sql =~ s/<ENDDATE>/'last_of_another_month(' . $db->quote($q->param('end_year') . '-' . $q->param('end_month') . '-01') . ')'/e;
	}
	elsif ($q->param('date_range') eq 'M-T-D')
	{
		$sql =~ s/<STARTDATE>/first_of_month()/;
		$sql =~ s/<ENDDATE>/NOW()/;
	}
	elsif ($q->param('date_range') eq 'Y-T-D')
	{
		$sql =~ s/<STARTDATE>/(SUBSTR(NOW()::TEXT, 1,4)||'-01-01')::DATE/;
		$sql =~ s/<ENDDATE>/NOW()/;
	}
	elsif ($q->param('date_range') eq 'Last Month')
	{
		$sql =~ s/<STARTDATE>/first_of_another_month(first_of_month() - 1)/;
		$sql =~ s/<ENDDATE>/first_of_month() - 1/;
	}
	else
	{
		die "Invalid date range parameter received: " . $q->param('date_range');
	}
	
	$sql .= "\nGROUP BY $Config{$qt}->{'group_by_SQL'}" if $Config{$qt}->{'group_by_SQL'};
	
	if ($Config{$qt}->{'force_orderby'})
	{
		$sql .= "\nORDER BY $Config{$qt}->{'force_orderby'}";
	}
	elsif ($q->param('order_by'))
	{
		die "Invalid order_by parameter\n" unless grep $q->param('order_by') eq $_, @{$Config{$qt}->{order_by}};
		$sql .= qq!\nORDER BY "! . $q->param('order_by') . '"';
		# the default will be the SQL default
		$sql .= ' DESC' if $q->param('order_by_direction') eq 'DESC';
	}
#print "Content-type: text/plain\n\n$sql"; exit;

	my $sth = $db->prepare($sql);
	$sth->execute;
	my $tmp = $sth->fetchrow_hashref;
	unless ($tmp)
	{
		$errormsg = 'No Data Returned';
	}
	else
	{
		BuildReportTable($tmp, $sth);
	}
}
Display();

$db->disconnect;
exit;

sub BuildReportTable
{
	my ($first, $sth) = @_;
	$first->{'Vendor'} = VendorURL($first);
	my $class = 'a';
	$REPORT = '<table class="report">';
	$REPORT .= $q->Tr( $q->th( $Config{$qt}->{'display_cols'} ));
	$REPORT .= $q->Tr({-'class'=>$class}, $q->td( [map $first->{$_}, @{$Config{$qt}->{display_cols}}] ));
	
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$tmp->{'Vendor'} = VendorURL($tmp);
		$class = $class eq 'a' ? 'b':'a';
		$REPORT .= $q->Tr({'-class'=>$class}, $q->td( [map $tmp->{$_}, @{$Config{$qt}->{display_cols}}] ));
	}
	$REPORT .= "</table>\n";
}

sub Display
{
	print $q->header(-charset=>'utf-8'),
	$q->start_html(
		-title=>'Vendor Sales Stats',
		-encoding=>'utf-8',
		-style=>'/css/admin/generic-report.css'
	),
        $q->p('<b>Vendor Sales Stats</b>');
	print $q->p($errormsg) if $errormsg;
	SearchForm();
	print $REPORT;
      print $q->end_html();

}

sub SearchForm
{
	##
	## print out the search form.
	##

	my $lbls = {'01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec'};
	my $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
	
	print $q->start_form('-method'=>'get'),
	'<table>',
	$q->Tr( $q->td('Query Type'),
		$q->td(
			$q->popup_menu(
				'-name' => 'query_type',
				'-values' => [sort keys %Config],
				'-labels' => {map {$_ , $Config{$_}->{'label'}} keys %Config}
	))),
	$q->Tr(
		$q->td('Date Range'),
		$q->td(
			$q->popup_menu(
				'-name' => 'date_range',
				'-values' => \@DateRanges
			) .
			$q->span({'-style'=>'padding-left:1em', '-class'=>'fpnotes', '-class'=>'lybg'}, 'Be sure to set this to "Custom" if you intend to specify dates below')
		)
	),
	$q->Tr( $q->td('Start Date'), $q->td(
		$q->popup_menu(
			'-name'=> 'start_month',
			'-values' => $months,
			'-labels' => $lbls,
			'-default' => ($date[4] + 1)
			) .
		$q->popup_menu(
			'-name'=> 'start_year',
			'-values' => \@year_options,
			'-default' => $date[5]
			))),
	$q->Tr( $q->td('End Date'), $q->td(
		$q->popup_menu(
			'-name'=> 'end_month',
			'-values' => $months,
			'-labels' => $lbls,
			'-default' => ($date[4] + 1)
			) .
		$q->popup_menu(
			'-name' => 'end_year',
			'-values' => \@year_options,
			'-default' => $date[5]
			)));
	print $q->Tr($q->td('Order by:') . $q->td(
		$q->popup_menu(
			'-name' =>'order_by',
			'-values'=>\@Global_Order_By
		) . 
		$q->popup_menu(
			'-name' =>'order_by_direction',
			'-values'=>['ASC','DESC'],
			'-labels'=>{'ASC'=>'Ascending', 'DESC'=>'Descending'}
		) .
		$q->div({'-class'=>'fpnotes'}, $q->span({'-class'=>'lybg'}, 'Group by Month queries will always be ordered by Vendor/Month')) .
		$q->div({'-class'=>'fpnotes'}, $q->span({'-class'=>'lybg'},	'No Sale Vendor queries can only be ordered by Vendor or Last Change Date'))
	)), '</table>',
	$q->submit,
      $q->end_form(),
}

sub VendorURL
{
	return $q->a({
		'-href'=>"/cgi/admin/vms/Edit_Vendor.cgi?vendor_id=$_[0]->{vendor_id}",
		'-onclick'=>'window.open(this.href); return false;'
		}, $q->escapeHTML($_[0]->{'Vendor'}) );
}

sub _base_SQL
{
return q|
SELECT v.vendor_name AS "Vendor", v.vendor_id,
	SUM(t.amount) AS "Sales Total",
	COUNT(t.id) AS "# of Sales",
	SUM(t.receivable) AS "Commissions"
	,  ((
	CASE
		WHEN SUM(t.amount) = 0 THEN 1
		ELSE SUM(t.receivable) / SUM(t.amount)
	END
	) * 100)::NUMERIC(7,1) AS "Average Comm. %"
	-- EXTRA COLS
FROM transactions t
INNER JOIN vendors v ON t.vendor_id=v.vendor_id
WHERE t.trans_type = 2 AND t.void != TRUE
AND t.trans_date BETWEEN <STARTDATE> AND <ENDDATE> 
|;
}

sub _base_SQL_noSales
{
return q|
SELECT v.vendor_name AS "Vendor", v.vendor_id, v."operator" AS "Last Changed by",
	v.stamp::DATE AS "Last Change Date", v.notes AS "Notes"
FROM vendors v
LEFT JOIN transactions t ON t.vendor_id=v.vendor_id
AND t.trans_date BETWEEN <STARTDATE> AND <ENDDATE> 
WHERE t.vendor_id ISNULL
AND v.vendor_group = 2
AND v.status = 1
|;
}

sub _base_groupby_SQL
{
	return q|v.vendor_name, v.vendor_id|;
}

sub _config_
{
	###### the display_cols should exactly match the names of the columns 
	###### the array order will be the display order
return (
	1 => {
		'sql' => _base_SQL,
		'label' => 'Group by Interval',
		'display_cols' => ['Vendor', 'Sales Total', '# of Sales', 'Commissions', 'Average Comm. %'],
		'order_by' => ['Vendor', 'Sales Total', '# of Sales', 'Commissions', 'Average Comm. %'],
		'group_by_SQL' => _base_groupby_SQL
	},
	2 => {
		'sql' => _base_SQL,
		'label' => 'Group by Month',
		'extra_cols' => q!SUBSTR((DATE_TRUNC('month',t.trans_date))::TEXT,1,7) AS "Month"!,
		'display_cols' => ['Vendor', 'Month', 'Sales Total', '# of Sales', 'Commissions', 'Average Comm. %'],
		'group_by_SQL' => _base_groupby_SQL . q!, SUBSTR((DATE_TRUNC('month',t.trans_date))::TEXT,1,7) !,
		'force_orderby' => '"Vendor","Month"'
	},
	3 => {
		'sql' => _base_SQL_noSales,
		'label' => 'Vendors with no Sales',
		'display_cols' => ['Vendor', 'Last Changed by', 'Last Change Date', 'Notes'],
		'order_by' => ['Vendor', 'Last Change Date']
	}
);
}
