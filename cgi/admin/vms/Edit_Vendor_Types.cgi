#!/usr/bin/perl
###### Edit_Vendor_Types.cgi
###### 09/16/2002	Stephen Martin

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use POSIX qw(locale_h);
use locale;
setlocale(LC_CTYPE, "en_US.ISO8859-1");


push (@INC, '/home/httpd/cgi-lib');

require '/home/httpd/cgi-lib/ADMIN_DB.lib';
require '/home/httpd/cgi-lib/G_S.lib';

###### Globals
my ($db, $qry, $sth, $rv, $data, $HTML);
my (@cookie, $TEXT);
my $q = new CGI;
unless ( $q->https ){ print $q->redirect('https://www.clubshop.com' . $q->script_name()) }
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd = $q->cookie('pwd') || $q->param('pwd');


my $vendor_type = $q->param('vendor_type');
my $description = $q->param('description');
my $notes = $q->param('notes');
my $action = $q->param("action");

if (!(defined $operator))
{
 print $q->header(-expires=>'-1d'), $q->start_html(),
 $q->h3("Incorrect login.<br>Back up and try again"), $q->end_html(); 
 exit(0);
}

unless ( $db = &ADMIN_DB::DB_Connect( 'index.cgi', $operator, $pwd ))
{
 $db->{RaiseError} = 1;
 exit(1);
}

if ( $action eq "Update Vendor Type" )
{
 unless ( $vendor_type && $description && $notes )
 {
 &Err("You are missing a form input."); exit; 
 }
  
 $qry = "UPDATE vendor_types 
         SET description = ?,
             notes       = ?
         WHERE vendor_type = ?";

 $sth = $db->prepare($qry);

 $rv = $sth->execute($description, $notes, $vendor_type);
  defined $rv or die $sth->errstr;

  $rv = $sth->finish();
 
}

###### Default show the values for this template

###### Load in the appropriate Template 

my $TMPL = &G_S::Get_Object($db, 10019);
        unless ($TMPL){ &Err("Couldn't retrieve the template object."); exit; }

$qry = "SELECT vendor_type, description, notes FROM vendor_types WHERE vendor_type= ?";

$sth = $db->prepare($qry);

 $rv = $sth->execute($vendor_type);
  defined $rv or die $sth->errstr;

  $data = $sth->fetchrow_hashref();

 $rv = $sth->finish();

$db->disconnect;

$TMPL =~ s/%%vendor_type%%/$data->{vendor_type}/g;
$TMPL =~ s/%%description%%/$data->{description}/g;
$TMPL =~ s/%%notes%%/$data->{notes}/g;

print $q->header(-expires=>'-1d');

print $TMPL;

1;

sub Err{
  my ( $msg ) = @_;
print $q->header(-expires=>'-1d'), $q->start_html();
print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
print $q->end_html();
}


