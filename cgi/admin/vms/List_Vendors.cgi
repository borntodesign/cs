#!/usr/bin/perl
# List_Vendors.cgi
# Main Search page to edit vendors.
# they can then be edited or made inactive.
#
# Written by Stephen Martin
# September 16th 2002
# last modified: 12/17/15	Bill MacArthur

=head1 List_Vendors.cgi

=for Commentary

List_Vendors.cgi
Main Search page to edit vendors.
they can then be edited or made inactive.

Written by Stephen Martin
September 16th 2002

=cut

use strict;
use CGI;
use Carp;
use URI::Escape;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use G_S;

binmode STDOUT, ":encoding(utf8)";

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $qry, $data);    ###### database variables
my ($rv, $query, $action, $counter, $newoffset, $newlink, $prevlink, $forward, $back, %vh) = ();
my $editor   = "./Edit_Vendor.cgi";
my $self     = "./List_Vendors.cgi";
my $affiliate_link = '/cgi/admin/vms/aff2vendor.cgi?vendor=';
my $DISPLAYMAX = 100;
my $class;
my $cgi = new CGI();
my $criteria1 = $cgi->param('criteria1');
my $allmp = $cgi->param('allmp');

# Here is where we get our admin user and try logging into the DB

my $operator = $cgi->cookie('operator');
my $pwd = $cgi->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'index.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

###### print the search form part of the page
print $cgi->header(-expires=>'now', -charset=>'utf-8');
PageHead();

if ($query = determine_query())
{
#print "<br><b>Debug $query </b><br>";

	my $sth = $db->prepare($query);

	my $rv = $sth->execute();
	defined $rv or die $sth->errstr;
#	my ($vendor_id, $vendor_name, $vendor_description, $affcode, $advname, $advid) = ();
#	$sth->bind_columns(undef, \$vendor_id,\$vendor_name,\$vendor_description,\$vendor_status);

	$counter = 0;

print qq~<table id="maintbl"><thead>
<tr>
<th><img src="/images/icon_mp.gif" width="20" height="20" alt="MarketPlace Merchant Flag" title="Click for MarketPlace Merchant records"/></th>
<th></th><th><span class="vname_inactive">Vendors</span> are inactive.<br />
<span class="vname_alert">Vendors</span> have issues.</th>
<th>Vendor Notes</th><th title="Linked to a Mall" style="color:blue">ML</th>
<th class="affcode">Affiliate Code</th><th class="advname">Advertiser Name</th><th class="advid">Adv. ID</th>
</tr></thead><tbody>
\n~;

while (my $tmp = $sth->fetchrow_hashref)
{
	G_S::Null_to_empty($tmp);
	$tmp->{$_} = $cgi->escapeHTML($tmp->{$_}) foreach keys %{$tmp};
	###### keep the same row color if we are doing the same vendor with a different affiliate
	$counter-- if $vh{$tmp->{vendor_id}} && $counter > 0;
	$class = ($counter % 2  == 0) ? 'lbs1' : 'lbs2';
	$tmp->{vendor_description} =~ s#\n#<br />#g;
	my $vname_class =
		($tmp->{status} == 0) ? 'vname_inactive' : 
		($tmp->{status} == 1) ? 'vname_active' : 'vname_alert';

		print qq|<tr class="$class"><td class="${\($vh{$tmp->{vendor_id}} ? 'nb':'')}">|;

		print $cgi->a({
			-href=>"/cgi/admin/ma_location_admin.cgi?search_field=id;action=search;search_value=$tmp->{mal_id}",
			-onclick=>'window.open(this.href);return false;'
			},
			$cgi->img({
				-src=>'/images/icon_mp.gif',
				-width=>20,
				-height=>20,
				-style=>'border:0',
				-alt=>'Click for Merchant Records'})
		) if $tmp->{mal_id};
		print '</td>';
		unless ($vh{$tmp->{vendor_id}}){
			print qq|<td class="vid">|;
			print qq|<a href="$editor\?vendor_id=$tmp->{vendor_id};criteria1=$criteria1" onclick="openWin(this); return false;">$tmp->{vendor_id}</a>
			</td>
			<td class="$vname_class">$tmp->{vendor_name}</td>
			<td class="desc">$tmp->{vendor_description}</td>|;
		} else {
			print qq|<td class="nb" colspan="3"></td>|;
		}
		print qq~<td class="ml${\($vh{$tmp->{vendor_id}} ? ' nb':'')}">~;
		print ${\($tmp->{mall_relations} > 0 ? '*':'')} unless $vh{$tmp->{vendor_id}};
		print '</td>';
		print qq~
	<td class="affcode">${\AffiliateLink($tmp)}</td>
	<td class="advname">$tmp->{advname}</td>
	<td class="advid">$tmp->{advid}</td>
         </tr>
         <!-- DB Call -->
\n~;

	if ($counter++ > $DISPLAYMAX){
		print qq~<tr><td colspan="6" style="color:red">Display truncated at $DISPLAYMAX rows.
			Modify your query to optain fewer results.</td></tr>~;
		last;
	}
	$vh{$tmp->{vendor_id}} = 1;
}

	print '</tbody></table>';

	###### end of 'if criteria1'
}

print '</div></body></html>';

$db->disconnect;

exit;

sub AffiliateLink
{
	my $tmp = shift;
	return $cgi->a({
		-href =>	$affiliate_link . $tmp->{vendor_id},
		-onclick =>	'openWin(this); return false;',
		-class =>	$tmp->{affiliate_code} ? 'affcode_normal' : 'affcode_none'
	}, $tmp->{affiliate_code} ? $tmp->{affiliate_code} : '* None');
}

sub determine_query
{
	my $allMP = $cgi->param('allMP');
	return undef unless $criteria1 || $allMP;

    my $op1 = lc $criteria1;
     my $numop1 = getnum($op1);
     if ( $numop1 == undef ) 
     {
      $numop1 = '-1';
     }
	$op1 = $db->quote($op1);
	
	my $rv = "SELECT v.vendor_id, COALESCE(mal.location_name, v.vendor_name) AS vendor_name,
			v.notes AS vendor_description, v.status,
			la.affiliate_code, la.vendor_name_map AS advname, la.advertiser_id AS advid,
			COALESCE(mal.id::TEXT,'') AS mal_id,
			COUNT(mr.vendor_id) AS mall_relations
		FROM vendors v
		LEFT JOIN mall_vendors mr ON mr.vendor_id=v.vendor_id
		LEFT JOIN linkshare_affiliates_2vendors la ON v.vendor_id=la.vendor_id
		LEFT JOIN merchant_affiliate_locations mal ON mal.vendor_id=v.vendor_id
		WHERE ";
	if ($allMP){
		$rv .= 'v.vendor_group=6 AND v.status=1';
	} else {
		$rv .=	"(v.vendor_name ~* $op1 OR
			v.notes ~* $op1 OR
			v.vendor_id = $numop1 OR
			mal.location_name ~* $op1
			)
--			AND v.vendor_group IN (2,3,6,14)
		";
	}
	return "$rv GROUP BY v.vendor_id, mal.location_name, v.vendor_name, v.notes, v.status, la.affiliate_code, la.vendor_name_map, la.advertiser_id, mal.id, mr.vendor_id
		ORDER BY v.vendor_name, v.vendor_id";
}

sub getnum{
 use POSIX qw(strtod);
 my $str = shift;
 $str =~ s/^\s+//;
 $str =~ s/\s+$//;

 $! = 0;
 
 my ($num, $unparsed) = strtod($str);

 if (( $str eq '' ) || ($unparsed != 0) || $! )
 {
 return undef;
 } else {
 return $num;
 } 
}

sub PageHead
{
print qq~<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<style type="text/css">
table {
	margin: 0.2em auto 0 auto;
	border-collapse:collapse;
}

th {
	text-align: left;
	font-weight: normal;
	font-size: 75%;
	padding: 0.3em 1em;
	vertical-align: bottom;
}

td {
	text-align:left;
	font-size: 85%;
	vertical-align: top;
	white-space:normal;
	color:black;
	padding: 0.3em 1em 0.3em 0.5em;
	border: 1px solid silver;
	border-bottom: none;
}

td.desc{ font-size:75%; }

tr.lbs1{ background-color : #F5F5F5; }
tr.lbs2{ background-color: #fff; }

a { text-decoration: none; }

td.vid{
	padding-left: 0.3em;
	white-space: nowrap;
}

td.nb { border: none; }
td.vname_active {}
td.affcode { text-align: center; }
.vname_inactive { color: #999; }
.vname_alert {color:red;}
input.in{
	font-size: 12px;
}

body{ 
	background-color: #BEE0FA;
	font-size: 90%;
	font-family: Arial, Helvetica, sans-serif;
}

table#maintbl tbody{
	border-left: 1px solid silver;
	border-right: 1px solid silver;
}
a.affcode_normal{}
a.affcode_none{
	/*color: red;*/
	background-color: yellow;
}
.ml		/* our mall link container */
{
	text-align:center;
	padding:0;
	font-weight:bold;
	font-size:120%;
	color:#090;
	vertical-align:middle;
}
</style>
<script type="text/javascript">
function openWin(me){ window.open(me.href); }
</script>

<title>Vendor Search</title>
</head>
<body onload="document.forms[0].criteria1.focus();">

<div style="text-align:center">
<h4>DHSC Vendor Search</h4>
<table>
 <tr>
  <td class="lbs1 desc" style="">
Enter the vendor name or portion thereof, or part of the vendor description ( a keyword )
  </td>
  <td class="lbs1"><form action="$self" method="get">
     <input type="text" name="criteria1" maxlength="128" />
	&nbsp;&nbsp;&nbsp;<input type="submit" name="action" value="Search" class="in" />
<br />
<input type="checkbox" name="allMP" value="1" />
Show all Active MarketPlace Merchants
</form>

  </td>
 </tr>
</table>
   <hr style="width:50%" />
\n~;
}

###### 02/25/04 increased the number of search results per page
###### 08/21/06 various SQL fine tunings and HTML revisions
###### 04/18/07 a wide range of changes to improve the HTML, and the presentation
###### extra functionalities also added like denoting vendor status
###### 05/06/08 added MarketPlace merchants to the list
# 01/17/15 just added binmode STDOUT, ":encoding(utf8)";