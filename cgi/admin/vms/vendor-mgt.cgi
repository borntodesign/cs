#!/usr/bin/perl -w
# vendor-mgt.cgi
# create/update master vendor records
# this replaces the old Edit_Vendor and Add_Vendor interfaces
# created Dec. 2007	Bill MacArthur
# last modified: 

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);

our (@alerts, @errors, $db, @taxonomy, $taxflg, $NEW) = ();
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}

# the cols we will use params for
# there are a number of columns that we are not providing update capabilities for at this time
my @dbcols = qw/vendor_id vendor_name discount payout vendor_group  status url
	active pp_range_lower pp_range_upper mrp_range_lower mrp_range_upper vrp_range_lower vrp_range_upper
	country notes cv_mult reb_mult comm_mult primary_mallcat/;

my $params = LoadParams();

exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;
GetTaxonomy();

my $action = $q->param('_action');
unless ($action)	###### our default action will be to bring up a detail of the vendor
{
	die "Missing vendor_id parameter\n" unless $params->{vendor_id};
	my $relation = GetRelationDetail($params->{vendor_id});
	die "No relation found - vendor_id: $params->{vendor_id}\n" unless $relation->{vendor_id};
	Detail($relation);
}
elsif ($action eq 'add')	###### we are going to add a vendor
{
	my $mall = AddRecord( LoadParams() );
	Detail($mall) unless @errors;
}
elsif ($action eq 'new')	###### we want to bring up an interface for creating a new vendor
{
	$NEW = 1;
	Detail({});
}
elsif ($action eq 'edit')	###### we want to record the changes we have just made
{
	Update($params);
	my $relation = GetRelationDetail($params->{mall_id});
	Detail($relation) unless @errors;
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect;
exit;

sub AddRecord {
	my $params = shift;
	my $qry = 'INSERT INTO mall_vendors (';
	my $end = 'VALUES (';
	my @args = ();
	foreach (@dbcols){
		# we don't want to insert NULLs or empties, that seems to end up disabling default column values
		next unless $params->{$_};
		$qry .= "$_,";
		$end .= '?,';
		push @args, $params->{$_};
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )", undef, @args);
	if ($rv eq '1'){ return GetRelationDetail($params->{mall_id}); }
	push @errors, "Insert failed. DB reported: $DBI::errstr";
	return undef;
}

sub AffLink {
	my $data = shift;
	my $msg = 'Affiliate Relationship';
	my $class = 'ok';
	unless ($data->{l2vid}){
		$class = 'warning';
		$msg = 'No Affiliate Relationship - Click to Add';
	}
	return $q->a({
		-class=>$class,
		-href=>'/cgi/admin/vms/aff2vendor.cgi?vendor=' . $data->{vendor_id},
		-onclick=>'window.open(this.href); return false;'
	}, $msg);
}

sub cboCountry
{
        ###### we could have used the G_S version,
        ###### but it relies on a static list and doesn't apply an id to the control
        my $sth = $db->prepare('SELECT country_name, country_code FROM tbl_countrycodes ORDER BY country_name');
        $sth->execute;
        my @codes = '';
        my %names = (''=>'-----');
        while (my $tmp = $sth->fetchrow_hashref)
        {
                push @codes, $tmp->{country_code};
                $names{$tmp->{country_code}} = $tmp->{country_name};
        }
        return $q->popup_menu(
                -name=>'country_code',
                -id=>'country_code',
                -values=>\@codes,
                -labels=>\%names,
                -default=>$_[0]
        );
}

sub cboVendorGroup
{
	my $sth = $db->prepare('SELECT * FROM transaction_types WHERE active=TRUE ORDER BY description');
	$sth->execute;
	my (@vals, %lbls) = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @vals, $tmp->{trans_type};
		$lbls{$tmp->{trans_type}} = $tmp->{description};
	}
	return $q->popup_menu(
		-name=>'vendor_group',
		-values=>\@vals,
		-labels=>\%lbls,
		-default=> ($_[0] || 2)
	);
}

sub Detail
{
	###### show the data input/edit form
	my $data = shift;	###### a hashref
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => ($NEW ? 'Create New Vendor' : "Vendor Detail: $data->{vendor_name}"),
			-style => [{-src=>'/css/admin/generic-report.css'}, {-src=>'/css/admin/mall-mgt.css'}]
		#	-script => {-src=>'/js/admin/vendor-mall-relations.cgi.js'}
		#	-onload => ($data->{mall_id} ? "init()" : "init('new')")
		);
	print qq|<div id="link-container"><div id="mall-relations">
		<span style="font-size:larger;font-weight:bold">Mall Relationships</span>
		<p><a href="/cgi/admin/vms/vendor-mall-relations.cgi?vendor_id=$data->{vendor_id};_action=new" onclick="window.open(this.href);return false;">Create New Relation</a></p>
		<ul id="ul-mall-relations">|,
		MallRelations(),
		'</ul></div><p>',
		AffLink($data), '</p><p>', Taxonomy_Link(),
		'</p></div>' unless $NEW;

# 	print $q->p({-class=>'fpnotes', -style=>'float:right'},
# 			$q->a({-href=>"/cgi/admin/vms/mall-vendor-extensions.cgi?mall_id=$data->{mall_id};vendor_id=$data->{vendor_id}",
# 				-onclick=>'window.open(this.href); return false;'}, 'Open the Extension List'))
# 		if $data->{mall_id} && $data->{vendor_id};

# 	print $q->p({-class=>'fpnotes', -style=>'float:right; clear:right;'},
# 			$q->a({-href=>'#',
# 			-onclick=>'if(window.opener){window.opener.location.reload();};self.close();'}, 'Close Window') ),
	print $q->h5($NEW ? 'Create New Vendor' : 'Vendor Detail');
	print	$q->start_form(-action=>$q->script_name, -method=>'post',
			-onsubmit=>'return ' .($data->{vendor_id} ? 'validate_frm(this)' : 'validate_new(this)')),
		$q->hidden(-name=>'vendor_id', -value=>$data->{vendor_id});

	print $q->hidden(-name=>'mall_id', -value=>$data->{mall_id}) if $data->{mall_id};
	print q|<table id="detail-tbl">|;
	print $q->Tr(
		$q->td({-class=>'label'}, 'Vendor') .
		$q->td( $q->textfield(-id=>'vendor_name', -name=>'vendor_name',
			-value=>EMD($data->{vendor_name})) ));

	if ($data->{vendor_id}){
		print $q->Tr(
			$q->td({-class=>'label'}, 'Vendor ID') .
			$q->td( $q->textfield(-name=>'display_vendor_id',
				-value=>EMD($data->{vendor_id}), -disabled=>'disabled') )
		);
	}
	print $q->Tr(
		$q->td({-class=>'label'}, 'Vendor Group') .
		$q->td( cboVendorGroup($data->{vendor_group}) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Primary Mall Category') .
		$q->td( Primary_Mallcat() ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Status') .
		$q->td( $q->popup_menu(
			-name=>'status',
			-values=>[1,0,-1],
			-labels=>{1=>'Active', 0=>'Inactive', -1=>'Inactive NP', ''=>'Unset'},
			-default=> ($data->{status} || 1)
		)));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Country') .
		$q->td( cboCountry($data->{country}) ));

	print $q->Tr(
		$q->td({-class=>'label'}, 'URL') .
		$q->td( $q->textfield(-id=>'url', -name=>'url', -value=>EMD($data->{url}))	));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Commission %') .
		$q->td( $q->textfield(-id=>'discount', -name=>'discount', -value=>EMD($data->{discount})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Payout %') .
		$q->td( $q->textfield(-id=>'payout', -name=>'payout', -value=>EMD($data->{payout})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Ship From') .
		$q->td( $q->textfield(-id=>'ship_from', -name=>'ship_from', -value=>EMD($data->{ship_from})) ));

	if ($data->{vendor_group} == 2)
      {
	print $q->Tr({-class=>'ranges'},
		$q->td({-class=>'label'}, 'PP Range Lower') .
		$q->td($q->textfield(-id=>'pp_range_lower', -name=>'pp_range_lower', -class=>'ranges') .
			$q->textfield(-name=>'default_pp_range_lower', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{pp_range_lower})) .
			$q->button(-value=>'Clear Ranges', onclick=>'clearRanges()',
				-style=>'margin-left:10em')
		));
	print $q->Tr({-class=>'ranges'},
		$q->td({-class=>'label'}, 'PP Range Upper') .
		$q->td($q->textfield(-id=>'pp_range_upper', -name=>'pp_range_upper', -class=>'ranges') .
			$q->textfield(-name=>'default_pp_range_upper', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{pp_range_upper})) .
			$q->span({-style=>'color:gray; background-color: rgb(255,255,230); padding: 0.2em 0.5em;', -class=>'fpnotes'},
				'Gray boxes are the defaults')
		));

	print $q->Tr({-class=>'ranges'},
		$q->td({-class=>'label'}, 'MRP Range Lower') .
		$q->td(
			$q->textfield(-id=>'mrp_range_lower', -name=>'mrp_range_lower', -class=>'ranges') .
			$q->textfield(-name=>'default_mrp_range_lower', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{mrp_range_lower})) .
			'VRP Range Lower' .
			$q->textfield(-id=>'vrp_range_lower', -name=>'vrp_range_lower', -class=>'ranges') .
			$q->textfield(-name=>'default_vrp_range_lower', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{vrp_range_lower}))
	));

	print $q->Tr({-class=>'ranges'},
		$q->td({-class=>'label'}, 'MRP Range Upper') .
		$q->td(
			$q->textfield(-id=>'mrp_range_upper', -name=>'mrp_range_upper', -class=>'ranges') .
			$q->textfield(-name=>'default_mrp_range_upper', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{mrp_range_upper})) .
			'VRP Range Upper' .
			$q->textfield(-id=>'vrp_range_upper', -name=>'vrp_range_upper', -class=>'ranges') .
			$q->textfield(-name=>'default_vrp_range_upper', -disabled=>'disabled',
				-class=>'default-range', -value=>EMD($data->{vrp_range_upper}))
	));
      }
	print $q->Tr(
		$q->td({-class=>'label'}, 'Notes') .
		$q->td( $q->textarea(
			-id=>'notes', -name=>'notes', -cols=>50, -rows=>4,
			-value=>EMD($data->{notes})) ));

	unless ($NEW){
		print $q->Tr(
			$q->td({-class=>'label'}, 'Timestamp') .
			$q->td( $q->textfield(-name=>'stamp', -value=>EMD($data->{stamp}), -disabled=>'disabled') ));

		print $q->Tr(
			$q->td({-class=>'label'}, 'Operator') .
			$q->td( $q->textfield(-name=>'operator',	-value=>EMD($data->{operator}), -disabled=>'disabled') ));

	}
	print 	$q->Tr( $q->td({-colspan=>2, -align=>'right'},
			$q->submit($data->{vendor_id} ? 'Submit' : 'Create New Vendor') .
			$q->reset(-id=>'reset') ));
	print '</table>';
	print	$q->hidden(-name=>'_action', -value=>($data->{mall_id} ? 'edit' : 'add'), -force=>1), $q->end_form;
	print unless $NEW;
	print $q->end_html;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub GetRelationDetail {
	my ($vendor_id) = @_;
	return $db->selectrow_hashref(q|
		SELECT v.*, l2v.vendor_id AS l2vid
		FROM vendors_plus_ranges v
		LEFT JOIN linkshare_affiliates_2vendors l2v ON v.vendor_id=l2v.vendor_id
		WHERE v.vendor_id= ?|, undef, $vendor_id);
}

sub GetTaxonomy
{
	my $sth = $db->prepare('
		SELECT mc.catid, mc.catname, vc.vendor_id, v.primary_mallcat
		FROM mallcats mc
		LEFT JOIN vendors v ON v.primary_mallcat=mc.catid AND v.vendor_id= ?
		LEFT JOIN vendor_categories vc ON vc.catid=mc.catid AND vc.vendor_id= ?
		WHERE mc.parent=0 AND mc.catid > 0 ORDER BY mc.catname');
	$sth->execute(($params->{vendor_id} || -1), ($params->{vendor_id} || -1));
	while (my $tmp = $sth->fetchrow_hashref){
		push @taxonomy, $tmp;
		$taxflg ||= 1 if $tmp->{vendor_id};
	}
}

sub LoadParams
{
	my $rv = {};
	foreach(@dbcols)
	{
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	return $rv;
}

sub MallRelations
{
	my $rv = '';
	my $sth = $db->prepare("
		SELECT malls.label, COALESCE(cc.country_name,'') AS country_name, mv.active, mv.mall_id
		FROM mall_vendors mv
		INNER JOIN vendors v ON v.vendor_id=mv.vendor_id
		INNER JOIN malls ON malls.mall_id=mv.mall_id
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=malls.country_code
		WHERE mv.vendor_id= ?");
	$sth->execute($params->{vendor_id});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$rv .= $q->li({-class=>($tmp->{active} ? '' : 'inactive')},
			$q->a({-href=>"/cgi/admin/vms/vendor-mall-relations.cgi?vendor_id=$params->{vendor_id};_action=detail;mall_id=$tmp->{mall_id}",
				-class=>'mall-url', -onclick=>'window.open(this.href);return false;'},
				"$tmp->{label} - $tmp->{country_name}"));
	}
	return $rv ? $rv : $q->li('No Mall Relations');
}

sub Primary_Mallcat
{
	return q|<select name="primary_mallcat" id="primary_mallcat">| .
	Primary_Mallcat_Options() . '</select>';
}

sub Primary_Mallcat_Options
{
	my (@linked, @unlinked, $loop, $flg) = ();

	foreach my $tmp(@taxonomy)
	{
		$loop = qq|<option value="$tmp->{catid}"|;
		if ($tmp->{primary_mallcat}){
			$loop .= q| selected="selected"|;
			$flg = 1;
		}
		$loop .= '>' . $q->escapeHTML($tmp->{catname}) . '</option>';

		if ($tmp->{vendor_id}){ push @linked, $loop; }
		else { push @unlinked, $loop; }
	}

	my $rv = '';
	$rv .= q|<optgroup label="Linked" class="linked-cat-options">|;
	###### if we are not linked to anything, insert an empty option
	$rv .= '<option value="" selected="selected">-----</option>' unless $flg;
	$rv .= $_ foreach @linked;
	$rv .= q|</optgroup><optgroup label="Unlinked" class="unlinked-cat-options">|;
	$rv .= $_ foreach @unlinked;
	return $rv . '</optgroup>';
}

sub Taxonomy_Link
{
	my $msg = 'Taxonomy';
	my $class = 'ok';
	unless ($taxflg){
		$class = 'warning';
		$msg = 'Taxonomy is Empty - Click to Add';
	}
	return $q->a({
		-class=>$class,
		-href=>"/cgi/admin/vms/vendor-mallcat-admin.cgi?vid=$params->{vendor_id};_action=basepage",
		-onclick=>'window.open(this.href); return false;'
	}, $msg);
}

sub Update {
	my $params = shift;
	my $qry = 'UPDATE vendors SET ';
	foreach my $p (@dbcols){
		next if grep $p eq $_, ('vendor_id','mall_id');
		$qry .= "$p=";
		$qry .= length ($params->{$p} ||'') > 0 ? $db->quote($params->{$p}) : 'NULL';
		$qry .= ',';
	}
	$qry .= 'stamp=NOW()';
	$qry .= ' WHERE mall_id= ? AND vendor_id= ?';
#die $qry;
	my $rv = $db->do($qry, undef, $params->{mall_id}, $params->{vendor_id});
	die "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return;
}

# 









