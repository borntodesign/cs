#!/usr/bin/perl -w
###### Edit_Vendor.cgi
###### last modified: 11/27/15	Bill MacArthur
###### 01/07/2002	Stephen Martin

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use lib('/home/httpd/cgi-lib');
use XML::local_utils;
use XML::Simple;
use MailTools;
use Merch_Tools;
use MerchantAffiliates;
use ADMIN_DB;
use G_S;
require cs_admin;

binmode STDOUT, ":encoding(utf8)";

###### Globals
my ($qry, $rv, $data, $HTML, @taxonomy, $taxflg);
my (@cookie, $TEXT);
my $q = new CGI;
unless ( $q->https ){
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
	exit;
}
die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $cs = cs_admin->new({'cgi'=>$q});
my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;
my $TT = Template->new({'INCLUDE_PATH' => '/home/httpd/cgi-templates/TT/admin/Edit_Vendor'});

my %tmpl = (
	'default' => 'default.tt'
);

my $vendor_id   = $q->param('vendor_id') || die "Failed to receive vendor_id parameter\n";
my $vendor_name = process_param($q->param('vendor_name'));
my $discount    = process_param($q->param('discount'));
#my $payout      = process_param($q->param('payout'));
my $vendor_group = $q->param('vendor_group');
my $status      = process_param($q->param('status'));
my $old_status  = process_param($q->param('old_status'));
my $url         = $q->param('url');
$url =~ s/\s//g if $url;
my $country     = $q->param('country');
my $coupon      = process_param($q->param('coupon'));
my $blurb       = process_param($q->param('blurb'));
my $b2b_url     = process_param($q->param('b2b_url'));
my $pp_mult	= process_param($q->param('pp_mult'));
my $reb_mult	= process_param($q->param('reb_mult'));
my $comm_mult	= process_param($q->param('comm_mult'));
my $notes 	= process_param($q->param('notes'));
my $action 	= process_param($q->param("action"));
my $mal_id      = process_param($q->param('mal_id'));

my $gs = G_S->new({'db'=>$db, 'CGI'=>$q});

if ( $action eq "Update Vendor" )
{
#
# If we are updating an online mall merchant, update the merchant master status first
#
	# we will do this in a transaction
	$db->begin_work;

    my $merch_data = {};
    my $data2 = {};
    if ($vendor_group == 6 && $old_status == 3 && $status == 1)
    {
        my $qry = "
        	SELECT
				m.id,
				m.spid,
				m.firstname,
				mal.location_name,
				mam.id as mam_id,
				mam.emailaddress1,
				mal.vendor_id,
				mam.status AS mam_status,
				ms.firstname||' '||ms.lastname AS spid_name,
				ms.emailaddress AS spid_emailaddress
				FROM merchant_affiliate_locations mal
				JOIN merchant_affiliates_master mam
					ON mam.id = mal.merchant_id
				JOIN members m 
					ON mam.member_id = m.id 
				JOIN members ms
					ON ms.id=m.spid
				WHERE mal.vendor_id = $vendor_id;";
        my $stmt = $db->prepare($qry);
        $stmt->execute();
        $data2 = $stmt->fetchrow_hashref || die "This query failed to return a result:\n$qry";

		unless ($data2->{'mam_status'} == 1)
		{
			# Bill... I am not sure there is any good reason to use the MerchantAffiliates class to do this instead of simply updating the status field directly
			# especially since we could obtain the mam ID from the query above
			# but in case there is any other business logic involved in using the class method, we will just go with it
			my $MA = MerchantAffiliates->new($db);
			$merch_data = $MA->retrieveMerchantMasterRecordByID($data2->{'mam_id'});
			$merch_data->{'status'} = 1;
			$MA->updateMerchantMasterInformation($data2->{'mam_id'}, $merch_data);
		}
        $qry = "
        	UPDATE mall_vendors SET active=TRUE, notes= notes ||'
        		made active via Edit_Vendor.cgi by: ' || current_user
        	WHERE vendor_id = $data2->{'vendor_id'}
        	AND COALESCE(active,FALSE) <> TRUE;";
		$db->do($qry);
    }

    if (! $vendor_name && ! $mal_id)
    {
       	Err('Vendor Name is required');
    }

#    foreach (qw(vendor_group country vendor_id discount payout))
    foreach (qw(vendor_group country vendor_id discount))
    {
       	unless (eval("defined \$$_ && \$$_ gt ''"))
       	{
       		Err("You are missing a form input: $_");
       	}
    }

#    foreach (qw(discount payout))
    foreach (qw(discount))
    {
       	unless ( eval("is_numeric(\$$_)") )
       	{
                Err("This field must be numeric only: $_");
        }
    }
 
    $qry = "UPDATE vendors SET 
	discount          = ?,		
--	vendor_group      = ?,
	status            = ?,
	url               = ?,
	country           = ?,
	coupon            = ?,
	blurb             = ?,
	notes             = ?,
	cv_mult 	  = ?,
	reb_mult	  = ?,
	comm_mult         = ?";

    my @extra_args = ();

    foreach (qw(vendor_name primary_mallcat))
    {
       	if ($q->param($_)){
       		push @extra_args, $q->param($_);
       		$qry .= ", $_ = ?";
       	}
    }

    $rv = $db->do("$qry WHERE vendor_id= ?", undef, (
		$discount,		
#		$vendor_group,
		$status,
		$url,
		$country,
		$coupon,
		$blurb,
		$notes,
		$pp_mult,
		$reb_mult,
		$comm_mult,
		@extra_args, $vendor_id));

#    defined $rv or die $db->errstr;
    $db->commit;
    
#
# put out email to merchant if vendor_group is 6 and status changes to active
#
    if ($vendor_group == 6 && $status == 1 && $old_status != 1)
    {
		my $html = '';
		my $translation_xml = $gs->Get_Object($db, 10933);
		my $translation = XML::Simple::XMLin($translation_xml);
        $html = $gs->Get_Object($db, 10936);
		$html = Merch_Tools::addLanguage($html, $translation->{'email5'});
        $html =~ s/%%params_firstname%%/$data2->{'location_name'}/g;
        $html =~ s/%%p7%%//g;
        my %email = (
              'from'=>'info@ClubShop.com',
              'to'=>qq#"$data2->{'location_name'}" <$data2->{emailaddress1}>#,
              'subject'=>$translation->{'email5'}->{'subject'},
              'text'=>$html,
              'cc'=>qq!"$data2->{'spid_name'}" <$data2->{'spid_emailaddress'}>!
	   );

		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
#        if ($data2->{'spid'})
#        {
#			$html = $gs->Get_Object($db, 10936);
#			$html = Merch_Tools::addLanguage($html, $translation->{'email5'});
#			$html =~ s/%%params_firstname%%/$data2->{spid_firstname}/g;
#            $html =~ s/%%p7%%//g;
#            %email = (
#				'from'=>'info@ClubShop.com',
#				'to'=>"$data2->{spid_emailaddress}",
#				'subject'=>$translation->{'email5'}->{'subject'},
#				'text'=>$html,
#			);
#	    $MailTools->sendTextEmail(\%email);
#		}
    }
}

###### Load the Template 
 
#my $TMPL = $gs->Get_Object($db, 10020) || die "Couldn't retrieve the template object.\n";
my $TMPL = $tmpl{'default'};

###### display Existing Vendors

$qry = "
	SELECT v.vendor_id,
		v.vendor_name,
		v.discount,
		v.vendor_group,
		v.status,
		v.url,
		v.country,
		v.coupon,
		v.blurb,
		v.notes,
		v.cv_mult,
		v.reb_mult,
		v.comm_mult,
		v.stamp::TIMESTAMP(0) AS stamp,
		v.operator,
		v.primary_mallcat,
		l2v.vendor_id AS l2vid,
		mal.id AS mal_id,
		tt.description AS vendor_group_description
	FROM vendors v
	JOIN transaction_types tt
		ON tt.trans_type=v.vendor_group
	LEFT JOIN linkshare_affiliates_2vendors l2v ON v.vendor_id=l2v.vendor_id
	LEFT JOIN merchant_affiliate_locations mal ON mal.vendor_id=v.vendor_id
	WHERE v.vendor_id = ?";

my $sth = $db->prepare($qry);
$sth->execute($vendor_id);
$data =  $sth->fetchrow_hashref();
$sth->finish();

GetTaxonomy();

my @countries = ();
$sth = $db->prepare('SELECT country_code, country_name FROM tbl_countrycodes ORDER BY country_name');
$sth->execute;
while (my $tmp = $sth->fetchrow_hashref)
{
	$tmp->{'country_name'} = $gs->Prepare_UTF8($tmp->{'country_name'}, 'encode');
	push @countries, $tmp;
}


$TMPL =~ s/%%affiliate_link%%/AffLink()/e;
#$TMPL =~ s/%%primary_mallcat%%/Primary_Mallcat()/e;
$TMPL =~ s/%%taxonomy_link%%/Taxonomy_Link()/e;


print $q->header('-expires'=>'now', '-charset'=>'utf-8');

$TT->process($TMPL, {
	'form_action' => $q->script_name,
	'data' => $data,
	'countries' => \@countries,
	'mall_relations' => MallRelations(),
	'status_menu' => gen_menu($data->{'status'},"status","vendor_status_types","status_type","status"),
	'primary_mallcat' => Primary_Mallcat(),
	'affiliate_link' => AffLink(),
	'taxonomy_link' => Taxonomy_Link(),
	'payout_percentages' => GetPayoutPercentages($data->{'vendor_group'})
}) || print $TT->error();

Exit();

#############################################

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub AffLink
{
	my $msg = 'Affiliate Relationship';
	my $class = 'ok';
	unless ($data->{'l2vid'})
	{
		$class = 'warning';
		$msg = 'No Affiliate Relationship - Click to Add';
	}
	return $q->a({
		'-class'=>$class,
		'-href'=>'/cgi/admin/vms/aff2vendor.cgi?vendor=' . $data->{'vendor_id'},
		'-onclick'=>'window.open(this.href); return false;'
	}, $msg);
}

sub Err
{
	print $q->header('-expires'=>'-1d', '-charset'=>'utf-8'), $q->start_html('-encoding'=>'utf-8');
	print $q->h3($_[0]), $q->end_html();
	Exit();
}

sub GetPayoutPercentages
{
	my $trans_type = shift;
	return $db->selectrow_hashref("
		SELECT
			pp_pct,
			(pp_pct * 100)::NUMERIC(5,2) AS pp_pct_as_pct,
			rebate_pct,
			(rebate_pct * 100)::NUMERIC(5,2) AS rebate_pct_as_pct,
			commission_pct AS refcomm_pct,
			(commission_pct * 100)::NUMERIC(5,2) AS refcomm_pct_as_pct
		FROM configurations.tp_configuration_overview WHERE trans_type= $trans_type");
}

sub GetTaxonomy
{
	my $sth = $db->prepare('
		SELECT mc.catid, mc.catname, vc.vendor_id, v.primary_mallcat
		FROM mallcats mc
		LEFT JOIN vendors v ON v.primary_mallcat=mc.catid AND v.vendor_id= ?
		LEFT JOIN vendor_categories vc ON vc.catid=mc.catid AND vc.vendor_id= ?
		WHERE mc.parent=0 AND mc.catid > 0 ORDER BY mc.catname');
	$sth->execute($vendor_id, $vendor_id);
	while (my $tmp = $sth->fetchrow_hashref){
		push @taxonomy, $tmp;
		$taxflg ||= 1 if $tmp->{'vendor_id'};
	}
}

sub is_numeric
{
	my $v = shift;
	$v =~ s/\.//;
	return (defined $v && $v gt '' && $v !~ /\D/) ? 1 : undef;
}

sub gen_menu
{
    my ($selected, $menu_name, $table_name, $index_name, $desc_name ) = @_;

    my $dex;
    my $desc;
    my $html;
    my $sflag;
    my $seen = 0;

  $qry = "SELECT $index_name, $desc_name from $table_name ORDER BY $index_name";

  my $sth = $db->prepare($qry);

  $sth->execute();

  $sth->bind_columns(undef, \$dex,\$desc);

  $html = "<select name='$menu_name' style='width: 350px;'>\n";

  while   ($sth->fetch() )
  {
   if ( $dex eq $selected ) 
   { 
    $sflag = " selected=\"selected\"";
    $seen = 1;
   }
    else 
   {
    $sflag = '';
   }

   $html .= qq|<option value="$dex"| . $sflag . '>' . CGI::escapeHTML($desc) . "</option>\n";
  }

	$sth->finish();

  if ( ! $seen ) 
  {
   $html .= "<option selected='selected'></option>\n";
  }

  $html .= "</select>\n";

  return $html;
}

sub MallRelations
{
	my $rv = '';
	my $sth = $db->prepare("
		SELECT malls.label, COALESCE(cc.country_name,'') AS country_name, mv.active, mv.mall_id
		FROM mall_vendors mv
		INNER JOIN vendors v ON v.vendor_id=mv.vendor_id
		INNER JOIN malls ON malls.mall_id=mv.mall_id
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=malls.country_code
		WHERE mv.vendor_id= ?
		ORDER BY country_name");
	$sth->execute($vendor_id);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$rv .= $q->li({'-class'=>($tmp->{'active'} ? '' : 'inactive')},
			$q->a({
				'-href'=>"/cgi/admin/vms/vendor-mall-relations.cgi?vendor_id=$vendor_id;_action=detail;mall_id=$tmp->{'mall_id'}",
				'-class'=>'mall-url', '-onclick'=>'window.open(this.href);return false;'
				},
				"$tmp->{'country_name'} - $tmp->{'label'}"));
	}
	return $rv ? $rv : $q->li('No Mall Relations');
}

sub Primary_Mallcat
{
	return q|<select name="primary_mallcat" id="primary_mallcat">| . Primary_Mallcat_Options() . '</select>';
}

sub Primary_Mallcat_Options
{
	my (@linked, @unlinked, $loop, $flg) = ();

	foreach my $tmp(@taxonomy)
	{
		$loop = qq|<option value="$tmp->{'catid'}"|;
		if ($tmp->{'primary_mallcat'})
		{
			$loop .= q| selected="selected"|;
			$flg = 1;
		}
		$loop .= '>' . $q->escapeHTML($tmp->{'catname'}) . '</option>';

		if ($tmp->{'vendor_id'}){
			push @linked, $loop;
		}
		else {
			push @unlinked, $loop;
		}
	}

	my $rv = '';
	$rv .= q|<optgroup label="Linked" class="linked-cat-options">|;
	###### if we are not linked to anything, insert an empty option
	$rv .= '<option value="" selected="selected">-----</option>' unless $flg;
	$rv .= $_ foreach @linked;
	$rv .= q|</optgroup><optgroup label="Unlinked" class="unlinked-cat-options">|;
	$rv .= $_ foreach @unlinked;
	return $rv . '</optgroup>';
}

sub process_param
{
	my $p = shift;
	return '' unless defined $p;
	
###### filter out leading & trailing whitespace
		$p =~ s/^\s*//;
		$p =~ s/\s*$//;

###### convert other duplicate whitespace into one space
#		$p =~ s/\s{2,}/ /g;	# that catches newlines and such
		$p =~ s/ {2,}/ /g;

	return $p;		
}

sub Taxonomy_Link
{
	my $msg = 'Taxonomy';
	my $class = 'ok';
	unless ($taxflg){
		$class = 'warning';
		$msg = 'Taxonomy is Empty - Click to Add';
	}
	return $q->a({
		'-class'=>$class,
		'-href'=>"/cgi/admin/vms/vendor-mallcat-admin.cgi?vid=$data->{'vendor_id'};_action=basepage",
		'-onclick'=>'window.open(this.href); return false;'
	}, $msg);
}

####### Added the ability to update the new 3 extra fields for cv_mult, reb_mult and comm_mult
###### 08/21/06 various code cleanups
# 08/24/06 added processing of params to manage whitespace
###### 02/23/07 added CGI escaping of the textual values inserted into the form
###### 11/28/07 added handling for the vendor_catgories (taxonomy)
###### 05/08/08 removed the vendor_type stuff
###### heavy revisions to the recent code introduced by George to handle the automatic placement of a new online merchant in the mall along with the attendant emails
