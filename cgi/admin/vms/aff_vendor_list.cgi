#!/usr/bin/perl -w

## aff_vendor_list.cgi
##
## Displays a list of vendor/affiliate mappings for a particular (or no) affiliate.
##
## Deployed: 01/22/07	Shane Partain
##
##
##

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

our $q = new CGI;
our $db = '';

my %params = ();
foreach ($q->param()) {$params{$_} = $q->param($_);}
my @headers = ('Vendor ID', 'Vendor Name', 'Advertiser Name', 'Advertiser ID', 'URL');
my $i = 0;
my @columns;
my @rows;

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect('aff2vendor.cgi', $operator, $pwd)) {exit;}
$db->{RaiseError} = 1;

unless (%params) {

        my $affiliates_rv = $db->selectall_hashref("    SELECT *
                                                        FROM linkshare_affiliates
                                                        WHERE active='true'", "code"
                                                );

        ##
        ## Create the hash and sorted array for the affiliate dropdown.
        ##
	my %affiliates = ();
        foreach (keys %{$affiliates_rv}) {$affiliates{$_} = $affiliates_rv->{$_}->{name};}
        my @affiliate_names = (sort {$affiliates{$a} cmp $affiliates{$b}} keys %affiliates);

	##
	## Add an option to find vendors not connected to an affilitate.
	##
	push @affiliate_names, 'none';
	$affiliates{none} = 'No Affiliate';
	
	my $affilate_dropdown = $q->popup_menu( -name=>'affiliate',
                                        -values=>['', @affiliate_names],
                                        -default=>'',
					-onChange=>'av_search.submit()',
                                        -labels=>\%affiliates,
                                        'id="affiliate"');

	printPage($affilate_dropdown, 'List Vendors by Affiliate:', "uses_form");

}elsif ($params{affiliate} eq 'none') {

	my $vendors_no_affiliates = $db->selectall_hashref("SELECT v.vendor_name,
								v.vendor_id,
								url,
								affiliate_code
							FROM vendors v LEFT JOIN linkshare_affiliates_2vendors l
							ON v.vendor_id=l.vendor_id
							WHERE affiliate_code IS null
							AND status=1
							AND vendor_group=2", "vendor_id");

	my @heads = ('Vendor ID', 'Vendor Name', 'URL');
	push @rows, $q->Tr({'class="gray"'}, $q->th( \@heads));

	foreach (sort {$vendors_no_affiliates->{$a}->{vendor_name} cmp $vendors_no_affiliates->{$b}->{vendor_name}}
			 keys %{$vendors_no_affiliates}) {

		push @columns, $q->a({-href=>'Edit_Vendor.cgi?vendor_id=' . $_}, $_);
                push @columns, $q->a({-href=>'aff2vendor.cgi?vendor=' . $_}, "$vendors_no_affiliates->{$_}->{vendor_name}");
                push @columns, $vendors_no_affiliates->{$_}->{url};

                if (++$i % 2 == 0) {
                        push @rows, $q->Tr({'class="gray"'}, $q->td( \@columns));
                }else{
                        push @rows, $q->Tr($q->td( \@columns));
                }
                @columns = ();

        }
        printPage($q->table(@rows), "Vendors Without Affiliates");

}else{

	my $aff2vendors = $db->selectall_hashref("      SELECT *
                                                        FROM linkshare_affiliates_2vendors l,
								vendors v
                                                        WHERE l.affiliate_code=\'$params{affiliate}\'
							AND l.vendor_id=v.vendor_id", "vendor_id"
                                                );
	my $aff_name = $db->selectrow_array("SELECT name FROM linkshare_affiliates WHERE code=\'$params{affiliate}\'");
	
	push @rows, $q->Tr({'class="gray"'}, $q->th( \@headers));
	
	foreach (sort {$aff2vendors->{$a}->{vendor_name} cmp $aff2vendors->{$b}->{vendor_name}} keys %{$aff2vendors}) {
		
		push @columns, $q->a({-href=>'Edit_Vendor.cgi?vendor_id=' . $_}, $_);
		push @columns, $q->a({-href=>'aff2vendor.cgi?vendor=' . $_}, "$aff2vendors->{$_}->{vendor_name}");
		push @columns, $aff2vendors->{$_}->{vendor_name_map};
		push @columns, $aff2vendors->{$_}->{advertiser_id};
		push @columns, $aff2vendors->{$_}->{url};
		
		if (++$i % 2 == 0) { 
			push @rows, $q->Tr({'class="gray"'}, $q->td( \@columns)); 
		}else{
			push @rows, $q->Tr($q->td( \@columns));
		}
		@columns = ();

	}
	printPage($q->table(@rows), "Vendors for $aff_name");

}

$db->disconnect;

sub printPage{

        my $content = shift;
	my $heading = shift;

	$content = $q->h3($heading) . $content;

	if (shift) {

		$content = $q->start_form(-name=>'av_search', -method=>'GET', -action=>'aff_vendor_list.cgi') .
                                $content .
			$q->end_form();

	}

        print $q->header(),
        $q->start_html(-title=>'Affiliate/Vendor Search',
                         -style=>{-code=>'body{font-size: 90%}
                                        select#affiliate{width:10em}
					tr.gray{background-color:#CCCCCC;}
                                '}),
        $q->center(
                                $content,
                ),
        $q->end_html();

}
