#!/usr/bin/perl -w
# vendor-mall-relations.cgi
# create/update vendor mall relation records
# created Nov. 2007	Bill MacArthur
# last modified: 05/08/08	Bill MacArthur

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);

our (@alerts, @errors, $db) = ();
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my @dbcols = qw/vendor_id url active languages pp_range_lower pp_range_upper mrp_range_lower
	mrp_range_upper vrp_range_lower vrp_range_upper gift_cert_url
	rp_increase_expiration notes mall_id/; # the cols we will use params for

exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;

my $vendor_id = $q->param('vendor_id') || die "Failed to receive a vendor_id param\n";
my $action = $q->param('_action');
unless ($action) {
	# there is no default at this time
	push(@errors, 'Missing _action parameter');
}
elsif ($action eq 'add') {
	my $mall = AddRecord( LoadParams() );
	Detail($mall) unless @errors;
}
elsif ($action eq 'new') {
	my $vn = $db->selectrow_hashref('
		SELECT vendor_id, vendor_name, url AS def_url, vendor_group,
			url as imported_url, mrp_range_lower AS def_mrp_range_lower,
			mrp_range_upper AS def_mrp_range_upper, vrp_range_lower AS def_vrp_range_lower,
			vrp_range_upper AS def_vrp_range_upper, pp_range_lower AS def_pp_range_lower,
			pp_range_upper AS def_pp_range_upper
		FROM vendors_plus_ranges_plus_mp WHERE vendor_id= ?', undef, $vendor_id);
	die "Failed to identify a vendor match for vendor_id: $vendor_id\n" unless $vn;
	Detail($vn);
}
elsif ($action eq 'detail') {
	die "Missing mall_id parameter\n" unless my $mall_id = $q->param('mall_id');
	my $relation = GetRelationDetail($mall_id);
	die "No relation found - mall_id: $mall_id - vendor_id: $vendor_id\n" unless $relation->{mall_id};
	Detail($relation);
}
elsif ($action eq 'edit') {
	my $params = LoadParams();
	Update($params);
	my $relation = GetRelationDetail($params->{mall_id});
	Detail($relation) unless @errors;
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect;
exit;

sub AddRecord {
	my $params = shift;
	my $qry = 'INSERT INTO mall_vendors (';
	my $end = 'VALUES (';
	my @args = ();
	foreach (@dbcols){
		# we don't want to insert NULLs or empties, that seems to end up disabling default column values
		next unless $params->{$_};
		$qry .= "$_,";
		$end .= '?,';
		push @args, $params->{$_};
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )", undef, @args);
	if ($rv eq '1'){ return GetRelationDetail($params->{mall_id}); }
	push @errors, "Insert failed. DB reported: $DBI::errstr";
	return undef;
}

sub cboMalls
{
	my $sth = $db->prepare("
		SELECT malls.mall_id, malls.label,
			COALESCE(malls.country_code,'') AS country_code,
			COALESCE(cc.country_name,'') AS country_name
		FROM malls
		LEFT JOIN mall_vendors mv ON malls.mall_id=mv.mall_id AND mv.vendor_id= ?
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=malls.country_code
		ORDER BY cc.country_name");
	$sth->execute($vendor_id);
	my @codes = '';
	my %names = (''=>'-----');
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @codes, $tmp->{mall_id};
		$names{$tmp->{mall_id}} = "$tmp->{label} - $tmp->{country_name}";
	}
	return $q->popup_menu(
		-name=>'mall_id',
		-id=>'mall_id',
		-values=>\@codes,
		-labels=>\%names,
		-default=>$_[0]
	);
}

sub Detail
{
	###### show the data input/edit form
	my $data = shift;	###### a hashref
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => ($data->{mall_id} ? "Mall Relation Detail: $data->{label}" : 'New Mall Relation'),
			-style => [{-src=>'/css/admin/generic-report.css'}, {-src=>'/css/admin/mall-mgt.css'}],
			-script => {-src=>'/js/admin/vendor-mall-relations.cgi.js'}
		#	-onload => ($data->{mall_id} ? "init()" : "init('new')")
		);
	print $q->p({-class=>'fpnotes', -style=>'float:right'},
			$q->a({-href=>"/cgi/admin/vms/mall-vendor-extensions.cgi?mall_id=$data->{mall_id};vendor_id=$data->{vendor_id}",
				-onclick=>'window.open(this.href); return false;'}, 'Open the Extension List'))
		if $data->{mall_id} && $data->{vendor_id};

	print $q->p({-class=>'fpnotes', -style=>'float:right; clear:right;'},
			$q->a({-href=>"/cgi/admin/product_payout_details.cgi?mall_id=$data->{mall_id};vendor_id=$data->{vendor_id}",
				-onclick=>'window.open(this.href); return false;'}, 'Open the PPoDs List'))
		if $data->{mall_id} && $data->{vendor_id};

	print $q->p({-class=>'fpnotes', -style=>'float:right; clear:right;'},
			$q->a({-href=>'#',
			-onclick=>'if(window.opener){window.opener.location.reload();};self.close();'}, 'Close Window') ),
		$q->h5($data->{mall_id} ? "Mall Relation Detail" : 'New Mall Relation');
	print	$q->start_form(-action=>$q->script_name, -method=>'post',
			-onsubmit=>'return ' .($data->{mall_id} ? 'validate_frm(this)' : 'validate_new(this)')),
		$q->hidden(-name=>'vendor_id', -value=>$data->{vendor_id});

	print $q->hidden(-name=>'mall_id', -value=>$data->{mall_id}) if $data->{mall_id};
#	unless ($data->{mall_id}){
#		print qq|<script type="text/javascript">var imported_url = '${\EMD($data->{imported_url})}';</script>|;
#		print $q->span({-class=>'hilite-imports label'}, 'Hilighted values have been imported from the vendor record')
#	}
	print q|<table id="detail-tbl">|;
	print $q->Tr(
		$q->td({-class=>'label'}, 'Vendor') .
		$q->td( $q->textfield(-id=>'vendor_name', -name=>'vendor_name',
			-value=>EMD($data->{vendor_name}), -disabled=>'disabled') ));

	if ($data->{mall_id}){
		print $q->Tr(
			$q->td({-class=>'label'}, 'Mall ID') .
			$q->td( $q->textfield(
				-id=>'mall_id', -name=>'mall_id',
				-value=>EMD($data->{mall_id}), -disabled=>'disabled') )
		);
		print $q->Tr(
			$q->td({-class=>'label'}, 'Mall Label') .
			$q->td( $q->textfield(
				-id=>'mall_label', -name=>'mall_label',
				-value=>EMD($data->{label}) . ' - ' . EMD($data->{country_name}),
				-disabled=>'disabled') )
		);
		print $q->Tr(
			$q->td({-class=>'label'}, 'Orig. Link Date') .
			$q->td( $q->textfield(
				-id=>'entered', -name=>'entered',
				-value=>$data->{entered},
				-disabled=>'disabled') )
		);
	} else {
		print $q->Tr(
			$q->td({-class=>'label'}, 'Mall Relation') .
			$q->td( cboMalls() ));
	}

	print $q->Tr(
		$q->td({-class=>'label'}, 'Default URL') .
		$q->td( $q->textfield(-class=>'url', -name=>'def_url', -disabled=>'disabled',
			-value=>EMD($data->{def_url}))
	));

	print $q->Tr(
		$q->td({-class=>'label'}, 'URL') .
		$q->td( $q->textfield(-id=>'url', -name=>'url', -value=>EMD($data->{url}))
	));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Languages') .
		$q->td( $q->textfield(-id=>'languages', -name=>'languages', -value=>EMD($data->{languages})) ));

#	print $q->Tr(
#		$q->td({-class=>'label'}, 'Ship From') .
#		$q->td( $q->textfield(-id=>'ship_from', -name=>'ship_from', -value=>EMD($data->{ship_from})) ));

	print q|<tr class="ranges">|,
		$q->td({-class=>'label'}, 'PP Range Lower');

	print '<td>';
	###### this item is conditional... it is not available for MarketPlace merchants
	print $q->textfield(-id=>'pp_range_lower', -name=>'pp_range_lower', -class=>'ranges',
			-value=>EMD($data->{pp_range_lower}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_pp_range_lower', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_pp_range_lower})) .
		$q->button(-value=>'Clear Ranges', onclick=>'clearRanges()',
			-style=>'margin-left:10em');
	print '</td></tr>';

	print q|<tr class="ranges">|,
		$q->td({-class=>'label'}, 'PP Range Upper');
	print '<td>';
	###### this item is conditional... it is not available for MarketPlace merchants
	print $q->textfield(-id=>'pp_range_upper', -name=>'pp_range_upper', -class=>'ranges',
			-value=>EMD($data->{pp_range_upper}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_pp_range_upper', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_pp_range_upper})) .
		$q->span({-style=>'color:gray; background-color: rgb(255,255,230); padding: 0.2em 0.5em;', -class=>'fpnotes'},
			'Gray boxes are the defaults');
	print '</td></tr>';

	print q|<tr class="ranges">|,
		$q->td({-class=>'label'}, 'MRP Range Lower');
	print '<td>';
	###### this item is conditional... it is not available for MarketPlace merchants
	print $q->textfield(-id=>'mrp_range_lower', -name=>'mrp_range_lower', -class=>'ranges',
			-value=>EMD($data->{mrp_range_lower}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_mrp_range_lower', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_mrp_range_lower})) .
		'VRP Range Lower ';

	###### this item is conditional... it is not available for MarketPlace merchants
	print $q->textfield(-id=>'vrp_range_lower', -name=>'vrp_range_lower', -class=>'ranges',
			-value=>EMD($data->{vrp_range_lower}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_vrp_range_lower', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_vrp_range_lower}));
	print '</td></tr>';

	print q|<tr class="ranges">|,
		$q->td({-class=>'label'}, 'MRP Range Upper');
	print '<td>';
	###### this item is conditional... it is not available for MarketPlace merchants
	print  $q->textfield(-id=>'mrp_range_upper', -name=>'mrp_range_upper', -class=>'ranges',
			-value=>EMD($data->{mrp_range_upper}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_mrp_range_upper', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_mrp_range_upper})) .
		'VRP Range Upper ';
	###### this item is conditional... it is not available for MarketPlace merchants
	print $q->textfield(-id=>'vrp_range_upper', -name=>'vrp_range_upper', -class=>'ranges',
			-value=>EMD($data->{vrp_range_upper}))
		unless $data->{vendor_group} == 6;

	print $q->textfield(-name=>'default_vrp_range_upper', -disabled=>'disabled',
			-class=>'default-range', -value=>EMD($data->{def_vrp_range_upper}));
	print '</td></tr>';

	print $q->Tr(
		$q->td({-class=>'label'}, 'Gift Certificate URL') .
		$q->td( $q->textfield(-id=>'gift_cert_url', -name=>'gift_cert_url',
			-value=>EMD($data->{gift_cert_url})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'RP Increase Expiry') .
		$q->td( $q->textfield(-id=>'rp_increase_expiration', -name=>'rp_increase_expiration',
			-value=>EMD($data->{rp_increase_expiration})) .
			q| <span class="fpnotes">mm-dd-yyyy</span>| ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Notes') .
		$q->td( $q->textarea(
			-id=>'notes', -name=>'notes', -cols=>50, -rows=>4,
			-value=>EMD($data->{notes})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Timestamp') .
		$q->td( $q->textfield(
			-id=>'stamp', -name=>'stamp', -force=>1,
			-value=>EMD($data->{stamp}), -disabled=>'disabled') )
	) if $data->{mall_id};

	print $q->Tr(
		$q->td({-class=>'label'}, 'Active') .
		$q->td( $q->checkbox(
			-id=>'active', -name=>'active', -label=>'',
			-checked=>$data->{active}, -value=>'TRUE') ));

	if ($data->{mall_id}){
		print $q->Tr(
			$q->td({-class=>'label', -colspan=>2},
				$q->p('These options are managed programmatically') .
				'Sale Flag' .
				$q->checkbox(
				-id=>'flag_sale', -name=>'flag_sale', -label=>'', -class=>'ckbx1',
				-checked=>$data->{flag_sale}, -value=>'TRUE', -disabled=>'disabled') .
				'Coupon Flag' .
				$q->checkbox(
				-id=>'flag_coupon', -name=>'flag_coupon', -label=>'', -class=>'ckbx1',
				-checked=>$data->{flag_coupon}, -value=>'TRUE', -disabled=>'disabled') .
				'Shipping Incentive Flag' .
				$q->checkbox(
				-id=>'flag_shipping', -name=>'flag_shipping', -label=>'', -class=>'ckbx1',
				-checked=>$data->{flag_shipping}, -value=>'TRUE', -disabled=>'disabled') ));
	}
	print 	$q->Tr( $q->td({-colspan=>2, -align=>'right'},
			$q->submit($data->{mall_id} ? 'Submit' : 'Create New Mall Relation') .
			$q->reset(-id=>'reset') ));
	print '</table>';
	print	$q->hidden(-name=>'_action', -value=>($data->{mall_id} ? 'edit' : 'add'), -force=>1),
		$q->end_form, $q->p({-class=>'fpnotes'},
			$q->a({-href=>'#',
			-onclick=>'if(window.opener){window.opener.location.reload();self.close();}'}, 'Close Window') ),
		$q->end_html;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub GetRelationDetail
{
	my ($mall_id) = @_;
	return $db->selectrow_hashref(q|
		SELECT mv.*, v.vendor_name, malls.label, v.vendor_group,
			COALESCE(cc.country_name,'') AS country_name,
			v.mrp_range_lower AS def_mrp_range_lower,
			v.mrp_range_upper AS def_mrp_range_upper,
			v.vrp_range_lower AS def_vrp_range_lower,
			v.vrp_range_upper AS def_vrp_range_upper,
			v.pp_range_lower AS def_pp_range_lower,
			v.pp_range_upper AS def_pp_range_upper,
			v.url AS def_url
		FROM mall_vendors mv
		INNER JOIN vendors_plus_ranges_plus_mp v ON v.vendor_id=mv.vendor_id
		INNER JOIN malls ON malls.mall_id=mv.mall_id
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=malls.country_code
		WHERE mv.mall_id= ? AND mv.vendor_id= ?|, undef, $mall_id, $vendor_id);
}

sub LoadParams
{
	my $rv = {};
	foreach(@dbcols)
	{
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	# since we are using a checkbox on the interface, with a value of TRUE when checked
	# we don't get FALSE when not checked :)
	$rv->{active} = $rv->{active} ? 'TRUE' : 'FALSE';
	return $rv;
}

sub Update {
	my $params = shift;
	my $qry = 'UPDATE mall_vendors SET ';
	foreach my $p (@dbcols){
		next if grep $p eq $_, ('vendor_id','mall_id');
		$qry .= "$p=";
		$qry .= length ($params->{$p} ||'') > 0 ? $db->quote($params->{$p}) : 'NULL';
		$qry .= ',';
	}
	$qry .= 'stamp=NOW()';
	$qry .= ' WHERE mall_id= ? AND vendor_id= ?';
#die $qry;
	my $rv = $db->do($qry, undef, $params->{mall_id}, $params->{vendor_id});
	die "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return;
}

# 04/23/08 added link to PPoDs
###### 05/01/08 removed handling for the ship_from and text_slots


