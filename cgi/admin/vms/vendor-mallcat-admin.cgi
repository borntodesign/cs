#!/usr/bin/perl -w
###### vendor-mallcat-admin.cgi
###### the script for administrating the vendor/mall categories linkings
###### created: November '07	Bill MacArthur
###### last modified: 05/28/08	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use ADMIN_DB;
#no warnings 'once';

my $q = new CGI;
my (@list, $err_msg) = ();
my $Title = 'Vendor Category Linking';

###### we should be in SSL mode
die "This resource should only be invoked in SSL mode\n" unless $q->https;

###### we should have our operator and password cookies
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged in to use this resource\n" unless $operator && $pwd;

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
$db->{RaiseError} = 1;

###### we're only expecting these params
# vid - vendor ID
# catid - category ID
# _action - what we are going to do (default is show a table of categories under a certain parent)
my %param = map {$_, G_S::PreProcess_Input(defined $q->param($_) ? $q->param($_) : '')} ('catid', '_action', 'vid', 'parent');

###### we need these values for these params
#foreach('vid'){ die "Failed to receive a required parameter: $_\n" unless $param{$_}; }
unless ($param{vid}){
	ShowVendorList();
	goto 'END';
}

my $vid = $db->selectrow_hashref("SELECT * FROM vendors WHERE vendor_id= ?", undef, $param{vid}) ||
		die "Invalid vendor_id parameter\n";
if (! $param{'_action'}){
	ReturnCategoryTable();
}
elsif ($param{'_action'} eq 'basepage'){
	BasePage();
}
elsif ($param{'_action'} eq 'linkcat')
{
	###### link only one category from the text search interface
	die "catid parameter missing\n" unless $param{catid};
	my $rv = $db->do("INSERT INTO vendor_categories (vendor_id,catid)
		VALUES (?,?)", undef, $param{vid}, $param{catid});

	print $q->header(-charset=>'utf-8', -type=>'text/plain');
	if ($rv eq '1'){
		print $db->selectrow_array("SELECT vendor_categories_text_list(?)", undef, $param{catid});
	} else {
		print "Link failure? DB reports: $DBI::errstr\n";
	}
}
elsif ($param{'_action'} eq 'linkcats')
{
	# cats2add - the category IDs we want to link to a particular vendor
	my @list = $q->param('cats2add');
#die "scalar=". scalar @list;
	unless (scalar @list > 0){
		Err('No categories selected');
	} else {
		foreach (@list){
			next unless $_;	###### we want to skip the placeholder option if it was selected
			$db->do("INSERT INTO vendor_categories (vendor_id,catid) VALUES (?,?)", undef, $param{vid}, $_);
		}
	}
	ReturnCategoryTable();
}
elsif ($param{'_action'} eq 'tax'){
	ShowTaxonomy();
}
elsif ($param{'_action'} eq 'textsearch'){
	TextSearch();
}
elsif ($param{'_action'} eq 'unlink'){
	foreach('catid','parent'){ die "Failed to receive a required parameter: $_\n" unless defined $param{$_}; }
	$db->do('DELETE FROM vendor_categories WHERE catid=? AND vendor_id=?', undef, $param{catid}, $param{vid});
	###### we now need to have the parent cat as the 'active' catid
	$q->param(-name=>'catid', -value=>$param{parent});
	$param{catid} = $param{parent};
	ReturnCategoryTable();
}
else {
	die "Undefined _action: $param{'_action'}\n";
}

END:
$db->disconnect;
exit;

sub BasePage
{
	###### generate the base page for a given vendor
	print $q->header(-charset=>'utf-8');
	print qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>$Title</title>
<link rel="stylesheet" href="/css/admin/mallcat-admin.css" type="text/css" />
<script src="/js/yui/yahoo-min.js" type="text/javascript"></script>
<script src="/js/yui/event-min.js" type="text/javascript"></script>
<script src="/js/yui/connection-min.js" type="text/javascript"></script>
<script src="/js/admin/mallcat-admin.js" type="text/javascript"></script>

<script type="text/javascript">
var Mallcat_Admin_URL = '${\$q->script_name}?';
function init(){
	VENDOR_ID=$vid->{vendor_id};
	getTableByCatid('0');
}
</script>
</head><body onload="init()">
|;
	print Instructions();
	print $q->div({-id=>'divtitle'}, $	q->h4({-id=>'h4title'}, $Title) . VendorListFrm());

	if ($vid->{status} != 1){
		print $q->p({-style=>'color:red'}, 'This vendor is not active');
	}
	print qq|<div id="vendorname">
		<a id="alt-interface" href="${\$q->script_name}?vid=$param{vid};_action=tax">List Interface</a>
		$vid->{vendor_name}</div>|,
		$q->div({-id=>'flipflop'}, 'FLIPFLOP DIV'), $q->div({-id=>'td_0'}), $q->end_html;
}

sub Cats2Link_Ctl
{
	###### a control containing the categories under a given parent that are not linked to the vendor
	my $catid = $param{'catid'} || 0;
	my $sth = $db->prepare("
		SELECT catid, catname FROM mallcats WHERE catid > 0 AND parent= ?
		EXCEPT
		SELECT mc.catid, mc.catname FROM mallcats mc, vendor_categories vc
		WHERE mc.catid=vc.catid AND mc.parent= ? AND vc.vendor_id= ?
		ORDER BY catname");
	$sth->execute($catid, $catid, $param{vid});

	my $options = '';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$options .= qq|<option value="$tmp->{catid}">| . $q->escapeHTML($tmp->{catname}) . "</option>\n";
	}
	# a descriptive label for option -0-, our empty option... +'s mean there are more to add -'s means there aren't
	my $opt0 = $options ? '+++++' : '-----';

	return qq|<select name="cats2add" multiple="multiple" size="1" id="cats2add_$catid" class="cats2add" onclick="raiseCats2AddCtl(this);">| .
		qq|<option value="">$opt0</option>| . $options . '</select>';
}

sub CreateCategoryRow
{
	my ($cat, $rowclass) = @_;
	$cat->{catname} = $q->escapeHTML($cat->{catname});
	return qq|
<tr id="tr_$cat->{catid}" class="$rowclass">
	<td class="catname"><a href="#" title="Cat ID: $cat->{catid}" onclick="VendorCatAlert(this, $cat->{catid});">$cat->{catname}</a></td>
	<td class="ctl"><p class="expandcat" id="p_$cat->{catid}">$cat->{children}</p></td>
	<td id="td_$cat->{catid}" class="container"></td>
</tr>
|;
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub GetCategoryRow
{
	###### pull one row from the DB
	my $catid = shift;
	return $db->selectrow_hashref('
		SELECT mc.catid, mc.catname, 0 AS children
		FROM mallcats mc
		WHERE mc.catid > 0 AND mc.catid = ? ORDER BY mc.catname', undef, $catid);
}

sub Instructions
{
	return q|<ul id="instructions">
		<li>+++++ indicates there are additional unlinked categories.</li>
		<li>----- indicates there are no additional unlinked categories.</li>
		<li>Click the Add button or drop-down to expand it.</li><li>Click Reset to collapse it.</li>
		<li>To link categories, select one or more and click Add.</li>
		<li>To unlink categories, click the category and click OK</li></ul>|;
}

sub ReturnCategoryTable
{
	my $catid = $param{'catid'} || 0;
	my $rv = qq|<table id="table_$catid"><thead>|;
	$rv .= $q->Tr({-class=>'newcatname'}, $q->td({-colspan=>3, -class=>'newcatname'},
		$q->start_form(-method=>'GET', -onsubmit=>'linkVendor2Cat(this); return false;',
			-id=>"form_$catid", -action=>$q->url) .
	#	$q->textfield(-name=>'catname', -class=>'newcatname') .
		Cats2Link_Ctl() .
		$q->button(-class=>'submitnew cats2add', -value=>'Add', -onclick=>"toggleCats2Add($catid);") .
		$q->reset(-style=>'margin-left: 0.2em;', -class=>'submitnew cats2add',
			-onclick=>"lowerCats2Add($catid);") .
		$q->hidden(-name=>'_action', -value=>'linkcats', -force=>1) .
		$q->hidden(-name=>'catid', -value=>$catid) . 
		$q->hidden(-name=>'vid', -value=>$param{vid}) .
		$q->end_form));
	$rv .= '</thead><tbody>';
	my $sth = $db->prepare("
		SELECT mc.catid, mc.catname, COALESCE(tmp.children, 0) AS children
		FROM vendors v
		INNER JOIN vendor_categories vc ON vc.vendor_id=v.vendor_id
		INNER JOIN mallcats mc ON vc.catid=mc.catid
		LEFT JOIN (
			SELECT mallcats.parent AS id, COUNT(mallcats.*) AS children
			FROM mallcats
			INNER JOIN vendor_categories ivc ON ivc.catid = mallcats.catid AND ivc.vendor_id= ?
			GROUP BY parent
			) AS tmp ON tmp.id=mc.catid
		WHERE v.vendor_id = ? AND mc.parent= ?
		ORDER BY mc.catname");
	$sth->execute($param{vid}, $param{vid}, $catid);
	my $rowclass = 'a';
	while (my $cat = $sth->fetchrow_hashref){
		$rv .= CreateCategoryRow($cat, $rowclass);
		$rowclass = $rowclass eq 'a' ? 'b':'a';
	}
	$rv .= '</tbody></table>';
	print $q->header('text/xml'), $rv;
}

sub ShowTaxonomy
{
	my $sth = $db->prepare("
		SELECT vendor_categories_text_list(vc.catid) AS tax
		FROM vendor_categories vc WHERE vendor_id= ? ORDER BY tax");
	$sth->execute($param{vid});

	print $q->header(-charset=>'utf-8'),
		$q->start_html(-encoding=>'utf-8', -title=>$Title, -style=>{-src=>'/css/admin/mallcat-admin.css'});
	print q|<ul id="instructions"><li><a href="#" onclick="location.reload(); return false;">Reload</a></li></ul>|;
	print $q->div({-id=>'divtitle'}, $	q->h4({-id=>'h4title'}, $Title) . VendorListFrm());

	if ($vid->{status} != 1){
		print $q->p({-style=>'color:red'}, 'This vendor is not active');
	}
	print qq|<div id="vendorname"><p id="alt-interface">
		<a href="${\$q->script_name}?vid=$param{vid};_action=basepage">Editing Interface</a><br />|,
		$q->a({-href=>"${\$q->script_name}?vid=$param{vid};_action=textsearch",
			-onclick=>"window.open(this.href,'textsearch','width=700,height=200,scrollbars=yes,resizable=yes');return false;"},
			'Link by Text Search'),
		qq|</p>$vid->{vendor_name}</div><ul id="taxonomy">|;

	my $cnt = 0;
	my $rowclass = 'a';
	while (my ($tmp) = $sth->fetchrow_array)
	{
		print $q->li({-class=>$rowclass}, $q->escapeHTML($tmp));
		$rowclass = $rowclass eq 'a' ? 'b':'a';
		$cnt++;
	}
	print '</ul>', $q->p("$cnt categories"), $q->end_html;
}

sub ShowVendorList
{
	print $q->header(-charset=>'utf-8'),
		$q->start_html(-encoding=>'utf-8', -title=>$Title, -style=>{-src=>'/css/admin/mallcat-admin.css'});
	print $q->div({-id=>'divtitle'}, $q->h4({-id=>'h4title'}, $Title) . VendorListFrm()),
		$q->end_html;
}

sub TextSearch
{
	my $msg = '';
	$param{'text_search_phrase'} = $q->param('text_search_phrase') || '';
	$param{'text_search_phrase'} =~ s/^\s*|\s$//g;	###### remove leading and trailing whitespace
	unless ($param{'text_search_phrase'}){
#		$msg = 'No search phrase received';
		goto 'FINISH';
	}
	my $rv = '';
	my @list = ();
	###### first we'll search on the complete phrase
	###### this SQL is tweaked to exclude categories already linked to the vendor
	my $sth = $db->prepare("
		SELECT mc.catid, vendor_categories_text_list(mc.catid) AS item
		FROM mallcats mc
		LEFT JOIN vendor_categories vc ON mc.catid=vc.catid AND vc.vendor_id= ?
		WHERE vc.vendor_id IS NULL AND mc.catname ~* ? ORDER BY item");
	$sth->execute($param{vid}, $param{'text_search_phrase'});
	while (my $tmp = $sth->fetchrow_hashref){
		###### highlight our search phrase
		$tmp->{item} =~ s#($param{'text_search_phrase'})#<span class="hilite">$1</span>#ig;
		push @list, $tmp;
	}
	unless (@list){
		###### since we apparently didn't find anything on the complete phrase, we'll do an -OR- on the words
		my @words = split /\s/, $param{'text_search_phrase'};
		my $OR = '';
		for (my $x=0; $x < scalar @words; $x++){
			$words[$x] =~ s/^\s*|\s$//g;
			$OR .= "$words[$x]|";
		}
		chop $OR;
		$sth->execute($param{vid}, $OR);
		while (my $tmp = $sth->fetchrow_hashref){
			$tmp->{item} =~ s#($OR)#<span class="hilite">$1</span>#ig;
			push @list, $tmp;
		}
	}
	unless (@list){
		$msg = "No matches for: $param{'text_search_phrase'}";
	}
FINISH:
	print $q->header(-charset=>'utf-8');
	print qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Category Results for: $param{'text_search_phrase'}</title>
<link rel="stylesheet" href="/css/admin/mallcat-admin.css" type="text/css" />
<script src="/js/yui/yahoo-min.js" type="text/javascript"></script>
<script src="/js/yui/event-min.js" type="text/javascript"></script>
<script src="/js/yui/connection-min.js" type="text/javascript"></script>
<script src="/js/admin/mallcat-admin.js" type="text/javascript"></script>

<script type="text/javascript">
var TEXTLINK_URL = '${\$q->script_name}?_action=linkcat;vid=$param{vid};catid=';
function init(){
	VENDOR_ID=$vid->{vendor_id};
	CreateCatLinkEvents();
	document.forms[0].text_search_phrase.focus();
}
</script>
</head><body onload="init()">
|;
	print q|<ul id="instructions">
		<li>The list shown here only includes category matches <strong>not</strong> already linked to the vendor.</li>
		<li>Clicking these links will automatically add all ancestor categories.</li>
		<li style="text-align:right"><a href="#" onclick="self.close(); return false;">Close Window</a></li>
		</ul>|;
	print $q->div({-id=>'flipflop'}, 'FLIPFLOP DIV');
	print $q->p('<b>Category Text Search</b>');

	print $q->start_form(-method=>'GET', -action=>$q->script_name, -style=>'display:block'),
		$q->hidden(-name=>'_action', -value=>'textsearch'), $q->hidden('vid'),
		$q->textfield(-name=>'text_search_phrase', -onclick=>'this.select()', -class=>'newcatname'),
		$q->submit(-value=>'Search', -class=>'submitnew'), $q->end_form;
	
	if (@list){
		print '<ul id="taxonomy">';
		my $rowclass = 'a';
		foreach my $tmp (@list){
			$tmp->{item} =~ s/&/&amp;/g;
			print $q->li({-class=>$rowclass}, $q->a({-href=>'#', -id=>"tax$tmp->{catid}"}, $tmp->{item}));
			$rowclass = $rowclass eq 'a' ? 'b':'a';
		}
		print '</ul>';
	}
	elsif ($msg){ print $q->p($msg); }
	print $q->end_html;
}

sub VendorListFrm
{
	my $sth = $db->prepare("
		SELECT vendor_id, vendor_name FROM vendors WHERE status=1 AND vendor_group=2 ORDER BY vendor_name");
	$sth->execute;
	my $options = '';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$options .= qq|<option value="$tmp->{vendor_id}">| . $q->escapeHTML($tmp->{vendor_name}) . "</option>\n";
	}

	return $q->start_form(-method=>'GET', -action=>$q->script_name, -id=>'vendorlist') .
		$q->hidden(-name=>'_action', -value=>'basepage') .
		qq|<select name="vid" class="cats2add" onchange="document.getElementById('vendorlist').submit();">| .
		qq|<option value="">- Select a Vendor -</option>| . $options . '</select></form>';
}

###### 12/07/07 tweaked the SQL in ReturnCategoryTable to include the vendor_id in the inner SQL
# 05/28/08 revised the order of the yui javascripts in the text search page