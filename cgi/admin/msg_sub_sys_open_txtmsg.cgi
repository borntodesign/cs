#!/usr/bin/perl
######  msg_sub_sys_open_template.cgi
######  11/20/2002	Stephen Martin
######  This utility displays a TEMPLATE assigned to a message 
###### last modified: 09/25/07	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';

use ADMIN_DB;
use G_S;

###### Globals
my ( $db, $qry, $sth, $rv, $data);
my ( @cookie, $TEXT );
my $q = new CGI;
unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
}
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd      = $q->cookie('pwd')      || $q->param('pwd');

my $self   = $q->url();
my $action = $q->param('action');
my $oid     = $q->param('oid');
my $a   = $q->param('a');
my $MSG;

unless( $oid )
{
 Err("Missing oid?");
 exit;
}

$db = ADMIN_DB::DB_Connect( 'msg_sub_sys.cgi', $operator, $pwd ) || exit;
$db->{RaiseError} = 1;

unless ($MSG = obtain_message($oid,$a)){ NotFoundErr(); }
else {
	$MSG = $q->escapeHTML($MSG);
	###### convert newlines to HTML newlines :)
	$MSG =~ s#\n|\r#<br />#g;
	print $q->header(-charset=>'utf-8'), $q->start_html;

	print $q->div($MSG), $q->end_html;
}

$db->disconnect;
exit;

sub Err {
    my ($msg) = @_;
    print $q->header(), $q->start_html();
    print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
    print $q->end_html();
}

sub NotFoundErr
{

 my $X_TMPL = G_S::Get_Object( $db, 10057);
  unless ($X_TMPL) { Err("Could not find Error Template for $0"); exit; }

 print $q->header();

 print $X_TMPL;
 
}

sub obtain_message
{
 my ($oid) = @_;
 shift;
 my ($a) = @_;

 my ( $sth, $qry, $data, $rv, $table );    ###### database variables

 if ( $a eq "1" )
 {
  $table = "messages_arc";
 }
  else 
 {
  $table = "messages";
 }


 $qry = "SELECT msg from $table where oid = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($oid);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data->{msg}

}

###### 09/25/07 made the text handling render on an HTML page
