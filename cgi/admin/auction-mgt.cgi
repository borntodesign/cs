#!/usr/bin/perl -w
# auction-mgt.cgi
# create/update auction records
# created Nov. 2009	Bill MacArthur
# last modified: 08/22/11	Bill MacArthur

# this started off as a copy of mall-mgt.cgi

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
#use G_S;

my (@errors, $db, %dates) = ();
my $messages = '';
my $q = new CGI;
my $QRY_START = "
	SELECT auctions.auction_id, auctions.description, auctions.notes,
		COALESCE(auctions.high_bid_id::TEXT, ''::TEXT) AS high_bid_id, 
		auctions.high_bidder_alias, auctions.auction_end_text, auctions.high_bid,
		SUBSTR(auctions.auction_start::TEXT,1,19) AS auction_start,
		SUBSTR(auctions.auction_end::TEXT,1,19) AS auction_end,
		SUBSTR(auctions.high_bid_stamp::TEXT,1,19) AS high_bid_stamp,
		auctions.last_changed_by, auctions.pay_trans_id, auctions.finalized_by,
		auctions.setup_by, auctions.void, auction_priority,
		CASE WHEN auctions.auction_end <= NOW() THEN TRUE::BOOLEAN ELSE FALSE::BOOLEAN END AS closed";
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}

exit unless $db = ADMIN_DB::DB_Connect( 'auction-mgt.cgi', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;
my $params = LoadParams();

my $action = $params->{_action};
unless ($action) {
	# the initial load
	ShowList();
}
elsif ($action eq 'add') {
	my $auction = AddRecord();
#	die "$auction\n";
	AuctionDetail($auction) unless @errors;
}
elsif ($action eq 'bid_detail') {
	BidDetail();
}
elsif ($action eq 'bid_list') {
	BidList();
}
elsif ($action eq 'collect') {
	Collect();
	AuctionDetail();
}
elsif ($action eq 'detail') {
	AuctionDetail();
}
elsif ($action eq 'edit') {
	Update($params);
	AuctionDetail() unless @errors;
}
elsif ($action eq 'finalize') {
	Finalize();
	AuctionDetail();
}
elsif ($action eq 'new') {
	AuctionDetail();
}
elsif ($action eq 'void') {
	VoidAuction();
	AuctionDetail();
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

Err() if @errors;
End();

###### ###### ######

sub AddRecord {
	foreach (qw/description notes auction_start auction_end_date auction_end_time/)
	{
		push @errors, "Missing $_" unless $params->{$_};
	}
	if ($params->{auction_start} eq $params->{auction_end_date})
	{
		push @errors, 'Auction start date and end date cannot be the same';
	}
	return undef if @errors;
	
	my $qry = "INSERT INTO auctions (description, notes, auction_start, auction_end, auction_priority,";
	my $start = $db->selectrow_array('SELECT CASE WHEN ?::DATE = NOW()::DATE THEN NOW() ELSE ? END',
		undef, $params->{auction_start}, $params->{auction_start});
#die "start=$start param=$params->{auction_start}\n";
	my $end = "VALUES (?,?,?,?,?,";
	my @args = (
		$params->{description},
		$params->{notes},
		$start,
		"$params->{auction_end_date} $params->{auction_end_time}",
		$params->{auction_priority}
	);

	foreach (qw/organization_id auction_end_text high_bid/){
		# we don't want to insert NULLs or empties, that seems to end up disabling default column values
		next unless $params->{$_};
		$qry .= "$_,";
		$end .= '?,';
		push @args, $params->{$_};
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )", undef, @args);
	if ($rv eq '1'){
		return $db->selectrow_hashref("$QRY_START FROM auctions WHERE auction_id= CURRVAL('auctions_auction_id_seq')");
	}
	push @errors, "Insert failed. DB reported: $DBI::errstr";
	return undef;
}

sub AuctionDetail
{
	###### show the data input/edit form
	my $data = shift || ($params->{auction_id} ?
		$db->selectrow_hashref("
			$QRY_START, m.id AS member_id
			FROM auctions
			LEFT JOIN members m
				ON m.alias=auctions.high_bidder_alias
			WHERE auction_id=?", undef, $params->{auction_id}) : {});
	$data->{ca_balance} = $data->{member_id} ? CA_Balance( $data->{member_id} ) : '';
#	my @collist = qw|mall_id country_code language_code label notes active stamp url|;
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => ($data->{auction_id} ? "Auction Detail: $params->{auction_id}" : 'New Auction Entry'),
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/auction-mgt.css'}],
			-script => [
				{-code=>"var high_bid_id = $data->{high_bid_id};\nvar bid_id = $data->{high_bid_id};"},
				{-src=>'/js/admin/auction-mgt.js'}]
		),
		$q->h5($data->{auction_id} ? 'Auction Detail' : 'New Auction Entry');
	print $messages;
	print $q->start_form(-action=>$q->script_name, -method=>'get'),
		$q->hidden('auction_id');
	print q|<table class="report">|;
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Auction ID') .
		$q->td({-class=>'data'}, EMD($data->{auction_id}) )) unless $params->{_action} eq 'new';
	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Description<br />(for the public eye)') .
		$q->td( $q->textarea(-cols=>60, -rows=>1,
			-id=>'description', -name=>'description', -value=>EMD($data->{description})) ));
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Notes<br />(for staff use)') .
		$q->td( $q->textarea(-cols=>60, -rows=>1,
			-id=>'notes', -name=>'notes', -value=>EMD($data->{notes})) ));

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Auction Start') .
		$q->td({-class=>'data'}, $data->{auction_start} ?
			"$data->{auction_start} PST" . $q->hidden(-name=>'auction_start', -value=>$data->{auction_start}) :
			cboStartDate()
		));
		
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Auction End') .
		$q->td({-class=>'data'}, $data->{closed} ?
			$q->textfield(-name=>'auction_end', -value=>$data->{auction_end}) :
			cboEndDate($data->{auction_end} ? substr($data->{auction_end},0,10) : undef) .
			cboTimes($data->{auction_end} ? substr($data->{auction_end},11) : undef)
		));

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Organization ID<br />(restricts who can purchase)') .
		$q->td( $q->textfield(-name=>'organization_id', -value=>EMD($data->{organization_id})) ));

	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Auction End Text') .
		$q->td( $q->textfield(-style=>'width:20em', -force=>1,
			-id=>'auction_end_text', -name=>'auction_end_text', -value=>EMD($q->escapeHTML($data->{auction_end_text}))) .
			'<br />(use this only if you do not want the default date and time displayed'
		 ));

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Priority') .
		$q->td( $q->popup_menu(
			-force=>1,
			-id=>'auction_priority', -name=>'auction_priority',
			-values=>[0,1,2,3,4,5],
			-default=>$data->{auction_priority}) .
			'<br />(leave at -0- for normal date sorting)'
		 ));

	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'High Bid') .
		$q->td({-class=>'data'}, defined $data->{high_bid} ? $data->{high_bid} :
			$q->textfield( -id=>'high_bid', -name=>'high_bid')));
		 
	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'High Bid ID') .
		$q->td({-class=>'data'}, $data->{high_bid_id} ?
			$q->a({-href=>"?_action=bid_detail;bid_id=$data->{high_bid_id}"}, $data->{high_bid_id}) : '' ))
		 unless $params->{_action} eq 'new';
		 
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'High Bidder Alias') .
		$q->td({-class=>'data'}, $data->{high_bidder_alias} ?
			$q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$data->{high_bidder_alias}",
				-onclick=>'window.open(this.href); return false;'}, $data->{high_bidder_alias}) : ''
		)) unless $params->{_action} eq 'new';

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'High Bidder CA Balance') .
		$q->td({-class=>'data'}, (sprintf '%.2f', ($data->{ca_balance} || 0) ) ))
		 unless $params->{_action} eq 'new';
		 

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'High Bid Timestamp') .
		$q->td({-class=>'data'}, ($data->{high_bid_stamp} ? "$data->{high_bid_stamp} PST" : '')) )
		 unless $params->{_action} eq 'new';

	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Payment Trans ID') .
		$q->td({-class=>'data'}, $data->{pay_trans_id} ?
		
			LinkPayTransID($data->{pay_trans_id}) :
				
			$q->textfield(-name=>'pay_trans_id') . CollectButton($data, $data->{high_bid_id})
		)) unless $params->{_action} eq 'new';
		
	unless ($params->{_action} eq 'new')
	{
		print $q->Tr({-class=>'a'},
			$q->th({-class=>'label'}, 'Void') .
			$q->td( $data->{void} ?
				$q->span({-class=>'color:red; font-weight:bold;'}, 'Yes') : VoidButton($data) ));
	
		print $q->Tr({-class=>'b'},
			$q->th({-class=>'label'}, 'Setup by') .
			$q->td( EMD($data->{setup_by}) ));
		print $q->Tr({-class=>'a'},
			$q->th({-class=>'label'}, 'Finalized by') .
			$q->td( $data->{finalized_by} ? $data->{finalized_by} : FinalizeButton($data) ));
		print $q->Tr({-class=>'b'},
			$q->th({-class=>'label'}, 'Last Changed By') .
			$q->td( EMD($data->{last_changed_by}) ));
	}
	
	my $_action = '';
	unless (defined $data->{closed}){
		$_action = $q->submit(-value=>'Create Auction', -style=>'margin-right:1em') .
			$q->hidden(-name=>'_action', -value=>'add', -force=>1) . $q->reset;
	}
#	elsif ($data->{closed}){
#		# the record cannot be adjusted
#	}
	elsif ($data->{void} || $data->{finalized_by} || $data->{pay_trans_id}) {
		# do nothing... we don't want to display anything
	}
	else {
		$_action = $q->submit(-value=>'Edit Auction', -style=>'margin-right:1em') .
			$q->hidden(-name=>'_action', -value=>'edit', -force=>1) . $q->reset;
	}
	print $q->Tr({-class=>'a'}, $q->td({-colspan=>2, -align=>'right'}, $_action)) if $_action;
	print '</table>';
	print $q->end_form, $q->end_html;
}

sub BidDetail
{
	my $data = $db->selectrow_hashref('
		SELECT ab.*
		FROM auction_bids ab
		WHERE ab.bid_id=?', undef, $params->{bid_id}) ||
		die "Failed to obtain the bid details for bid: $params->{bid_id}";
	my $a = LoadAuction($data->{auction_id});
	$data->{account_balance} = CA_Balance($data->{bidder_id});
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => 'Bid Detail',
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/auction-mgt.css'}],
			-script => [
				{-code=>"var high_bid_id = $a->{high_bid_id};\nvar bid_id = $data->{bid_id};"},
				{-src=>'/js/admin/auction-mgt.js'}]
		);
	print $q->p({-class=>'fpnotes'},
		$q->a({-href=>$q->script_name . "?_action=bid_list;auction_id=$data->{auction_id}"}, 'All Bids') );
	print q|<table class="report">|;

	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Bid ID') .
		$q->td({-class=>'data'}, EMD($data->{bid_id}) ));
	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Auction ID') .
		$q->td({-class=>'data'}, $q->a({-href=>"?_action=detail;auction_id=$data->{auction_id}"}, $data->{auction_id}) ));
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Bidder ID') .
		$q->td({-class=>'data'}, EMD($data->{bidder_id}) ));
	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Bid Amount') .
		$q->td({-class=>'data'}, EMD($data->{bid_amount}) ));
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, "Bidder's CA Balance") .
		$q->td({-class=>'data'}, $data->{account_balance} ));

	# convert the newlines in the text to linebreaks
	$data->{bidder_info} =~ s#\n#<br />#g;
	print $q->Tr({-class=>'a'},
		$q->th({-class=>'label'}, 'Payment Info') .
		$q->td({-class=>'data'}, $data->{bidder_info} ));
	print $q->Tr({-class=>'b'},
		$q->th({-class=>'label'}, 'Timestamp') .
		$q->td({-class=>'data'}, $data->{stamp} ));

	my $btn = CollectButton($a, $data->{bid_id});
	print qq|<tr><td colspan=2" align="right">$btn</td></tr>| if $btn;

	print '</table>';
	print $q->end_form, $q->end_html;
}

sub BidList
{
	die "auction_id parameter not received" unless $params->{auction_id};
	my $sth = $db->prepare('
		SELECT bid_id, bidder_id, bid_amount, bidder_info, SUBSTR(stamp::TEXT,1,19) AS stamp
		FROM auction_bids WHERE auction_id=? ORDER BY bid_id');
	$sth->execute($params->{auction_id});
	
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => 'Auction Management - Bid List',
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/auction-mgt.css'}
			]
		);
	print "<h5>Reporting on auction: ";
	print $q->a({-href=>"?_action=detail;auction_id=$params->{auction_id}"}, $params->{auction_id});
	print '</h5>';
	print '<table class="report">';
	print '<tr>
			<th>Bid ID</th><th>Bidder ID</th><th>Bid Amount</th><th>Bid Info (last line)</th><th>Timestamp</th>
			</tr>';
	my $class = 'a';
	while (my $data = $sth->fetchrow_hashref)
	{
		print '<tr class="$class">';
		print $q->td( $q->a({-href=>"?_action=bid_detail;bid_id=$data->{bid_id}"}, $data->{bid_id}) );
		print $q->td( $q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$data->{bidder_id}",
				-onclick=>'window.open(this.href);return false;'}, $data->{bidder_id}) );
		print $q->td( $data->{bid_amount} );
		$data->{bidder_info} =~ /\n(.*)$/;
		$data->{bidder_info} = $1;
		print $q->td( $data->{bidder_info} );
		print $q->td( $data->{stamp} );
	}
	print '</table>', $q->end_html;
}

sub cboDates
{
	my ($name, $type) = @_;
	my $vals = GetDates($type);
	# place an empty entry at the beginning
	unshift @{$vals}, '';
	return $q->popup_menu(
		-name=>$name,
		-values=>$vals
	);
}

sub cboEndDate
{
	my $default = shift;
	my $dates = DateListOptions('NOW()::DATE');
	# get rid of today
#	my $x = shift @{$dates};
# sometimes we need today
	return $q->popup_menu(
		-name=>'auction_end_date',
		-id=>'auction_end_date',
		-values=>$dates,
		-default=>($default || $dates->[1])
	);
}

sub cboOrderBy
{
	my %labels = (
		'auctions.auction_id'=>'Auction ID',
		'auctions.auction_start'=>'Auction Start',
		'auctions.auction_end'=>'Auction End',
		'auctions.description'=>'Description',
		'auctions.high_bid DESC'=>'High Bid (Descending)',
		'auctions.high_bidder_alias'=>'High Bidder'		 
	);

	return $q->popup_menu(
		-name=>'order_by',
		-values=>['auctions.auction_id','auctions.auction_start','auctions.auction_end',
			'auctions.description','auctions.high_bid DESC','auctions.high_bidder_alias'],
		-labels=>\%labels
	);
}

sub cboStartDate
{
	my $dates = DateListOptions();
	return $q->popup_menu(
		-name=>'auction_start',
		-id=>'auction_start',
		-values=>$dates
	);
}

sub cboStatus
{
	return $q->popup_menu(
		-name=>'auction_status',
		-values=>[2,0,1,3],
		-labels=>{0=>'Open', 1=>'Closed', 2=>'All', 3=>'Paid but NOT Finalized'},
		-default=>$_[0]
	);
}
sub cboTimes
{
	my $default = shift;
	my %times = (
		'03:00:00' => '6:00 AM EST',
		'04:00:00' => '7:00 AM EST',
		'07:30:00' => '10:30 AM EST',
		'10:00:00' => '1:00 PM EST'
	);
	my @t = sort keys %times;
	return $q->popup_menu(
		-name=>'auction_end_time',
		-values=>\@t,
		-labels=>\%times,
		-default=>($default || '03:00:00'),
		-style=>'margin-left:1em'
	);
}

sub CA_Balance
{
	my $id = shift;
	return $db->selectrow_array("
		SELECT SUM(amount) AS account_balance FROM soa WHERE void=FALSE AND id= $id") || 0;
}

sub Collect
{
	require GI_ccauth;
	require 'ccauth_parse.pm';
	
	my $bid = $db->selectrow_hashref('SELECT * FROM auction_bids WHERE auction_id=? AND bid_id= ?',
		undef, $params->{auction_id}, $params->{bid_id}) ||
		die "Failed to find the bid with bid ID: $params->{bid_id} and auction ID: $params->{auction_id}\n";
	my $auction = LoadAuction($bid->{auction_id});
	foreach (qw/void finalized_by pay_trans_id/)
	{
		if ($auction->{$_}){
			$messages .= $q->p({-class=>'red'}, "You cannot collect on this auction. It is marked $_: $auction->{$_}");
			return;
		}
	}
	
	# determine payment type based on the last line (it looks like we are either going to have a CA debit or a CC payment)
	if ($bid->{bidder_info} !~ /Payment Type: Club Account$/)
	{
		$bid->{bidder_info} =~ m#Credit Card Name: (.+?)\n.*Card Number: (\d+).*CVV2: (\d*).*Card Expiry: (\d{2})/\d{0,2}(\d{2})#s;
		my ($name, $ccnumber, $cvv, $expy) = ($1,$2,$3, $4 . $5);
#print $q->header('text/plain'); print "$_\n" foreach ($name, $ccnumber, $cvv, $expy); exit;
		my %DATA = ();
	###### get the next auto increment number
		$DATA{invoice_number} = $db->selectrow_array("SELECT nextval('inv_num_invoice_seq')") ||
			die "Failed to generate an invoice number\n";
		$DATA{invoice_number} = "AUCTION-$DATA{invoice_number}";
		$DATA{order_description} = "Auction ID: $auction->{auction_id}";
		$DATA{id} = $auction->{high_bidder_alias};
		$DATA{amount} = $bid->{bid_amount};
#		$DATA{emailaddress} = $m->{emailaddress};
#		$DATA{ship_address} = $m->{address1};
#		$DATA{ship_city} = $m->{city};
#		$DATA{ship_state} = $m->{state};
#		$DATA{ship_zip} = $m->{postalcode};
#		$DATA{ship_country} = $m->{country};
#		$DATA{ship_firstname} =$m->{firstname};
#		$DATA{ship_lastname} = $m->{lastname};
		$DATA{account_holder_firstname} = '';
		$DATA{account_holder_lastname} = $name;
#		$DATA{account_holder_address} = $p->{mop_address};
#		$DATA{account_holder_city} = $p->{mop_city};
#		$DATA{account_holder_state} = $p->{mop_state};
#		$DATA{account_holder_zip} = $p->{mop_postal_code};
#		$DATA{account_holder_country} = $p->{mop_country};
		$DATA{payment_method} = 'CC';
		$DATA{cc_number} = $ccnumber;
		$DATA{cc_expiration} = $expy;
		$DATA{card_code} = $cvv;
		my ($auth) = CCAuth::Authorize(\%DATA, {raw=>1});
		unless ($auth)
		{
			$messages .= $q->p({-class=>'red'}, "CC authorization failed for an unknown reason. Try again.");
			return;
		}

		my $rv = ccauth_parse::Parse($auth);
		unless ( $rv->{resp_code} == 1)
		{
			$messages .= $q->p({-class=>'red'},
				"CC authorization failed<br />
				Transaction ID: $rv->{transaction_id}<br />$rv->{resp_reason_code} : $rv->{resp_reason_text}");
			return;
		}
		$messages .= $q->p({-class=>'green'}, "CC authorization success!<br />Authnet transaction: $rv->{transaction_id}");

		###### since they authorized, we'll create a Club Account credit reflecting the amount received
		my $result = $db->do("INSERT INTO soa (trans_type, id, amount, description, memo) VALUES (7000, ?, ?, ?, ?)",
			undef, $bid->{bidder_id}, $bid->{bid_amount},
			"Approval Code: $rv->{approval_code}", "Transaction ID: $rv->{transaction_id}");
		unless ($result eq '1')
		{
			$messages .= $q->p({-class=>'red'}, 'Failed to create a CA transation reflecting the CC collection');
			return;
		}
	}
	$db->{AutoCommit} = 0;
	my $trans_id = $db->selectrow_array(qq|
		SELECT debit_acct_create_trans(
			$bid->{bidder_id},
			12500,
			($bid->{bid_amount} * -1)::NUMERIC,
			'Auction ID: $bid->{auction_id}'::CHARACTER VARYING,
			NULL::INTEGER,
			"current_user"()::CHARACTER VARYING)|);
	die "Failed to create a CA debit transaction. Check their balance." unless $trans_id;
	
	# now place the trans_id in the auction record
	my $qry = "UPDATE auctions SET pay_trans_id= $trans_id";
	my @args = ();
	if ($auction->{high_bid_id} != $bid->{bid_id})
	{
		my $mbr = $db->selectrow_hashref("SELECT alias, firstname, lastname FROM members WHERE id= $bid->{bidder_id}");
		push @args, "\n***** The high bidder did not pay for this auction.\n***** Be sure to make the transfer to $mbr->{alias}, $mbr->{firstname} $mbr->{lastname}";
		$qry .= ',notes = notes || ?';
	}
	my $rv = $db->do("$qry WHERE auction_id= $bid->{auction_id}", undef, @args);
	die "Failed to update auction record with the payment trans id. Rolling back." unless $rv eq '1';
	$db->{AutoCommit} = 1;
	$messages .= $q->p({-class=>'green'}, "Success payment debited. Trans ID: $trans_id");
	return $auction;
}

sub CollectButton
{
	my ($auction, $bid_id) = @_;
	return '' if $auction->{pay_trans_id} || $auction->{void} || $auction->{finalized_by};
	return '' unless $auction->{high_bid_id};
	return $q->button(
		-value=>'Collect Payment',
		-style=>'margin-left:5em; color:green;',
		-onclick=>"if (confirmBid()){ document.location='" . $q->url . "?_action=collect;auction_id=$auction->{auction_id};bid_id=$bid_id';}")
}

sub DateListOptions
{
	my $first_item = shift || "'Now()'::TEXT";	# this allows us to get an actual date for today instead of the text
	my @dates = $db->selectrow_array("
		SELECT $first_item, NOW()::DATE +1, NOW()::DATE +2, NOW()::DATE +3, NOW()::DATE +4, NOW()::DATE +5, NOW()::DATE +6");
	return \@dates;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub End
{
	$db->disconnect;
	exit;
}

sub Err {
	print $q->header, $q->start_html;
	print $q->p($_) foreach @errors;
	print $q->end_html;
}

sub EscapeAMP {
	my $rv = shift;
	$rv =~ s/&/&amp;/g;
	return $rv;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	return $_[0] unless defined $_[0];
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub Finalize
{
	my $a = LoadAuction($params->{auction_id});
	if (! $a->{pay_trans_id}){
		$messages .= $q->p({-class=>'red'}, 'Auctions cannot be finalized until payment is recorded');
	}
	elsif ($a->{void}){
		$messages .= $q->p({-class=>'red'}, 'Voided auctions cannot be finalized.');
	}
	$db->do(qq|
		UPDATE auctions SET finalized_by= "current_user"() WHERE auction_id= $a->{auction_id};
		UPDATE auction_bids SET bidder_info='CC' WHERE auction_id=$a->{auction_id} AND bidder_info ~ 'Credit Card';
	|);
	$messages .= $q->p({-class=>'green'}, 'The auction has been finalized');
	return;
}

sub FinalizeButton
{
	my $a = shift;
	# present a button to finalize the auction... conditionally
	return '' if $a->{finalized_by} || $a->{void} || ! $a->{pay_trans_id};
	
	return $q->button(
		-value=>'Finalize Auction',
		-style=>'color:green; margin-right:1em',
		-onclick=>"document.location='" . $q->url . "?_action=finalize;auction_id=$a->{auction_id}'")
}

sub GetDates
{
	my $type = shift;
	return $dates{$type} if $dates{$type};
	my $sth = $db->prepare("SELECT DISTINCT SUBSTR(($type)::TEXT, 1, 10) AS $type FROM auctions WHERE $type IS NOT NULL ORDER BY $type DESC");

	my $rv = $sth->execute;
	while (my $tmp = $sth->fetchrow_array){
		push @{$dates{$type}}, $tmp;
	}
	return $dates{$type};
}

sub LinkPayTransID
{
	my $trans_id = shift;
	return $q->a({-href=>"/cgi/admin/SOViewer.cgi?trans_id=$trans_id",
				-onclick=>'window.open(this.href); return false;'}, $trans_id);
}

sub LoadAuction
{
	my $auction_id = shift;
	return $db->selectrow_hashref("$QRY_START FROM auctions WHERE auction_id=?", undef, $auction_id) ||
		die "Failed to find auction with ID: $auction_id\n";
}

sub LoadParams
{
	my $rv = {};
	foreach(qw/
		_action
		order_by
		auction_start
		auction_end_date
		auction_end_time
		auction_end
		auction_priority
		auction_id
		auction_status
		description notes
		end_time
		high_bid
		high_bid_id
		bid_id
		open_range_begin
		open_range_end
		close_range_begin
		close_range_end
		organization_id
		auction_end_text
		pay_trans_id/)
	{
		# don't want a null "param" unless there was really an empty parameter
		next unless defined $q->param($_);
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	# since we are using a checkbox on the interface, with a value of TRUE when checked
	# we don't get FALSE when not checked :)
#	$rv->{finalized} = $rv->{finalized} ? 'TRUE' : 'FALSE';
#die "keys: ", keys %{$rv};
	return $rv;
}

# this was created an never used, delete if it turns out unneeded later on
sub RedirectToDetail
{
	# initially we just performed the AuctionDetail routine after performing an action
	# unfortunately, when reloading the main window to quickly, the href hadn't been refreshed yet in the iframe
	# this caused some actions, most notably auction creation, to be executed again
	# we will mitigate the problem by issuing a redirect after a successful action, that way
	print $q->redirect($q->url . "?_action=detail;auction_id=$params->{auction_id}");
}

sub ShowList
{
	# if we receive no parameters, like when we simply call up the script
	# let's default to a list of all open auctions
	# 0 = open, 1 = closed, 2 = both, 3 = paid but NOT finalized
	my $auction_status = $q->param('auction_status') || 0;
	if($auction_status == 0){ $auction_status =	"auctions.auction_end > NOW()"; }
	elsif($auction_status == 2){ $auction_status =	"1=1"; }
	elsif($auction_status == 3){
		$auction_status = '(pay_trans_id IS NOT NULL AND finalized_by IS NULL)';
		# negate any dates as they will only serve to confuse
		$params->{open_range_begin} = ();
		$params->{open_range_begin} = ();
	}
	else{ $auction_status =	"auctions.auction_end <= NOW()"; }
	
	my $auction_begin = '';
	if ($params->{open_range_begin}){
		$auction_begin = "AND auctions.auction_start::DATE BETWEEN ${\$db->quote($params->{open_range_begin})} AND ";
		$auction_begin .= $params->{open_range_end} ? $db->quote($params->{open_range_end}) : 'NOW()';
	}

	my $auction_end = '';
	if ($params->{close_range_begin}){
		$auction_end = "AND auctions.auction_end::DATE BETWEEN ${\$db->quote($params->{close_range_begin})} AND ";
		$auction_end .= $params->{close_range_end} ? $db->quote($params->{close_range_end}) : 'NOW()';
	}

	my $order_by = 'ORDER BY ' . ($params->{order_by} ? $params->{order_by} : 'auctions.auction_end, auctions.auction_id');
	
	###### spit out a list of Auctions
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => 'Auction Management',
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/auction-mgt.css'}
			],
			-script => [
				{-type=>"text/javascript", -src=>"/js/yui/yahoo-min.js"},
				{-type=>"text/javascript", -src=>"/js/yui/event-min.js"},
				{-type=>"text/javascript", -src=>"/js/yui/dom-min.js"},
				{-type=>"text/javascript", -src=>"/js/IframeBox.js"}
			]
		), qq#
		<div id="instructions">
		<!-- -->
		</div>
		<p style="float:right" class="fpnotes">
		<a href="${\$q->url}?_action=new" onclick="page_reload_flag=1;showIframeBox()" target="rpt_iframe">Create New Auction</a>
		| <a href="/admin/auction-mgt.html" onclick="window.open(this.href); return false;">Auction Mgt. Manual</a>
		</p>

		<!--h5>Auction List for:</h5--><form method="get" action="${\$q->script_name}">
		<table class="report">
		<tr><th>Auction status: </th><td>${\cboStatus($q->param('auction_status') || 0)}</td></tr>
		<tr><th>Auctions opening between: </th><td>
				${\cboDates('open_range_begin', 'auction_start')} and ${\cboDates('open_range_end', 'auction_start')}
		</td></tr>
		<tr><th>Auctions closing between: </th><td>
				${\cboDates('close_range_begin', 'auction_end')} and ${\cboDates('close_range_end', 'auction_end')}
		</td></tr>
		<tr><th>Order by: </th><td>${\cboOrderBy()}</td></tr>
		<tr><td colspan="2" align="right"><input type="submit" /> <input type="reset" /></td></tr>
		</table></form>
		<hr align="left" width="50%" />
		<table class="report" id="table-list"><thead><tr>
		<th>ID</th><th>Start</th><th>End</th><th>Description</th><th>Notes</th>
		<th>High Bidder</th><th>High Bid</th><th>Payment</th>
		<th style="text-align:center" title="Finalized">Fn</th>
		</tr></thead><tbody id="auction-mgt">#;

	my $qry = "
		$QRY_START
		, ab.bid_id, (COALESCE(soa.amount,0) * -1) AS soa_amount
		FROM auctions
		LEFT JOIN auction_bids ab
			ON ab.bid_id=auctions.high_bid_id
		LEFT JOIN soa
			ON soa.trans_id=auctions.pay_trans_id
		WHERE $auction_status $auction_begin $auction_end
		$order_by
		";
#print STDERR $qry;
	my $sth = $db->prepare($qry);
	$sth->execute;
	my $bid_total = 0;
	my $total_sales = 0;
	my $cnt = 0;
	my $class = 'a';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		my $_class = $tmp->{void} ? 'voided' : $class;
		print qq|<tr class="$_class">|,
			$q->td($q->a({
					-class=>'id',
					-href=>$q->url . "?_action=detail;auction_id=$tmp->{auction_id}",
					-target=>'rpt_iframe',
					-onclick=>'showIframeBox()'}, $tmp->{auction_id}) ),
			$q->td({-class=>'astart'}, $tmp->{auction_start}),
			$q->td( $tmp->{auction_end}),
			$q->td({-class=>'description'}, $tmp->{description}),
			$q->td( EMD($tmp->{notes}) );
			
		print $q->td( $tmp->{high_bidder_alias} ?
			$q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$tmp->{high_bidder_alias}",
				-onclick=>'window.open(this.href);return false;'}, $tmp->{high_bidder_alias}) : ''
		);
			
		print $q->td({-class=>'high_bid'}, $tmp->{bid_id} ?
			$q->a({-href=>$q->script_name . "?_action=bid_detail;bid_id=$tmp->{bid_id}",
				-onclick=>'showIframeBox()', -target=>'rpt_iframe'}, $tmp->{high_bid}) : $tmp->{high_bid}
			),
			$q->td({-style=>'text-align:center'}, $tmp->{pay_trans_id} ? LinkPayTransID($tmp->{pay_trans_id}) : ''),
			$q->td({-class=>'fn'}, $tmp->{finalized_by} ? '&#10004;' : '' );
		print '</tr>';
		$class = $class eq 'a' ? 'b':'a';
		$bid_total += $tmp->{high_bid} unless $tmp->{void};
		$total_sales += $tmp->{soa_amount};
		$cnt++;
	}
	print qq|<tr style="background-color:#efe"><td align="right" colspan="6">Total Bids: </td>
		<td style="text-align:right;">|;
	printf '%.2f', $bid_total;
	print '</td><td style="text-align:right;">';
	printf '%.2f', $total_sales;
	print qq|</td><td style="text-align:center">$cnt</td></tr>|;
	
	my $links =
		$q->div({-class=>'close_me fpnotes'}, 
			$q->a({-href=>$q->script_name . '?_action=new', -target=>'rpt_iframe'}, 'Create New Auction') .
			$q->a({-href=>'#',
				-onclick=>"hideIframeBox(); setInterval(location.reload(), 1000); return false;",
				-style=>'margin-left:2em',
				-title=>'Close and reload the master list window'}, 'Close &amp; Reload') .
			$q->a({-href=>'#', -onclick=>'hideIframeBox(); return false;', -style=>'margin-left:2em'}, 'Close')
		);

	print '</tbody></table>';
	print $q->div({-id=>'iframe_box'},
			$links .
			'<iframe id="rpt_iframe" name="rpt_iframe" frameborder="0" height="580" src="https://www.clubshop.com/_empty_" width="750"></iframe>' .
			$links
		),
		$q->end_html;
}

sub Update {
	my $params = shift;
	# we have to build our auction_end from the 2 parts unless we are getting a complete value
	$params->{auction_end} ||= "$params->{auction_end_date} $params->{auction_end_time}";
	if ($params->{auction_start} eq $params->{auction_end_date})
	{
		push @errors, 'Auction start date and end date cannot be the same';
	}
	return undef if @errors;

	my $qry = 'UPDATE auctions SET ';
	foreach my $p (qw/
		description
		notes
		auction_start
		auction_end
		auction_priority
		organization_id
		auction_end_text
		pay_trans_id/){
		# if we did not recieve a parameter at all, we don't want to mess with the DB column at all
		next unless exists $params->{$p};
#		next unless length ($params->{$p} ||'') > 0;
		$qry .= "$p=";
		$qry .= (length ($params->{$p}) || 0) > 0 ? $db->quote($params->{$p}) : 'NULL';
#		$qry .= $db->quote($params->{$p});
		$qry .= ',';
	}
	$qry .= 'last_changed_by = current_user';
	$qry .= ' WHERE auction_id= ?';
#die $qry;
	my $rv = $db->do($qry, undef, $params->{auction_id});
	die "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return;
}

sub VoidAuction
{
	my $a = LoadAuction($params->{auction_id});
	my $rv = $db->do('UPDATE auctions SET void=TRUE WHERE auction_id=?', undef, $a->{auction_id});
	$messages .= $q->p({-class=>'red'}, 'For some reason, the auction failed to void') unless $rv eq '1';
	return $a;
}

sub VoidButton
{
	my $a = shift;
	# present a button to void the auction... conditionally
	return '' if $a->{finalized_by} || $a->{void} || $a->{pay_trans_id};
	
	# we'll allow them to void an auction with bids... at least to start with
	# they will have to do the cleanup with the VIPs
	return $q->button(
		-value=>'VOID Auction',
		-style=>'color:red; margin-right:1em',
		-onclick=>"document.location='" . $q->url . "?_action=void;auction_id=$a->{auction_id}'")
}

# 11/24/09 tweaked the CC regexp to take into account expy dates with either 2 or 4 digit years
# 09/24/10 made to use the "new" GI_ccauth instead of the ccauth.lib which is now ClubShop centric
