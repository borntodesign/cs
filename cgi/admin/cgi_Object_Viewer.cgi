#!/usr/bin/perl
# cgi_cgi_Object_Viewer.cgi
# Main page to view cgi_objects.
#
# Written by Stephen Martin
# August 20th  2002
#

use strict;
use DBI;
use CGI qw(:cgi);
use Carp;
use POSIX qw(locale_h);
use locale;
use URI::Escape;

setlocale(LC_CTYPE, "en_US.ISO8859-1");

# use HTML::Template;

require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data);    ###### database variables
my $rv;
my $operator;
my $query;
my $public;
my $private;
my $spawn;
my $cgi = new CGI();

my $action          = $cgi->param('action');
my $pk              = $cgi->param('pk');
my $obj_id          = $cgi->param('obj_id'); 
my $obj_name        = $cgi->param('$obj_name'); 
my $obj_description = $cgi->param('obj_description');
my $obj_value       = $cgi->param('obj_value');

my $criteria1 = $cgi->param('criteria1');
my $criteria2 = $cgi->param('criteria2');
my $criteria3 = $cgi->param('criteria3');

my $offset   = $cgi->param('offset');

my $URLcriteria1=uri_unescape($criteria1);
my $URLcriteria2=uri_unescape($criteria2);
my $URLcriteria3=uri_unescape($criteria3);

my $self     = "./cgi_Object_Viewer.cgi";
my $target   = "./HTMLcontent.cgi";
my $SEARCH   = $target  . "?offset=" . $offset . "\&criteria1=" . $URLcriteria1  . "\&criteria2=" .  $URLcriteria2 .  "\&criteria3=" . $URLcriteria3;
my $redirect;

# Here is where we get our admin user and try logging into the DB

$operator = $cgi->cookie('operator'); my $pwd = $cgi->cookie('pwd');
unless ($db = &ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Trap Recovery

if ( $action eq "Recover") 
{
 recover_template($obj_id,$pk); 
 $redirect = "https:\/\/" . $ENV{'SERVER_NAME'}. $ENV{'SCRIPT_NAME'};
 $redirect =~ s/cgi_Object_Viewer.cgi/HTMLcontent.cgi/g;

 print $cgi->redirect(-location=>$redirect);
}

if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ )
{
 $spawn="\'.\/OEhelp2.cgi\',\'_OEH1_\',\'width=400,height=220,scrollbars=no\'";
} else {
 $spawn="\'.\/OEhelp2.cgi\',\'_OEH1_\',\'width=500,height=250,scrollbars=no\'";
}

print $cgi->header();

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">


<title>DHSC CGI Object Viewer</title>

<script language="JavaScript">

  function spawn(URL,Name,features)
 {
  window.open(URL,Name,features);
 }

</script>
</head>
<body bgcolor="#eeffee">
<form action="$self" method="POST" name="vform1">

<input type="hidden" name="obj_id"    value="$obj_id">
<input type="hidden" name="pk"        value="$pk">
<input type="hidden" name="criteria1" value="$criteria1">
<input type="hidden" name="criteria2" value="$criteria2">
<input type="hidden" name="criteria3" value="$criteria3">

<center>
<b>DHSC CGI Object Viewer</b><br><br>
</center>
<table border="0" width="635" align="center">
  <tr>
  <td align="center" colspan="4" height="10">
  &nbsp;
  </td>
 </tr>
 <tr>
  <td align="center" colspan="4">
   <hr noshade align="center">
  </td>
 </tr>
</table>
\n~;


$query = "SELECT obj_id, obj_name, obj_description, obj_value FROM cgi_objects_arc
          WHERE obj_id = \'$obj_id\' AND 
                pk     = \'$pk\'";

# print "<br><b>Debug $query </b><br>";

$sth = $db->prepare($query);

$rv = $sth->execute();
defined $rv or die $sth->errstr;

$sth->bind_columns(undef, \$obj_id,\$obj_name,\$obj_description, \$obj_value);

while   ($sth->fetch() )
        {
         # $obj_name =~ s/[\W]/\&nbsp\;/g;

         print qq~
         <!-- DB Call -->
          <input type="hidden" name="obj_id" value="$obj_id">
          <table border="0" width="635" align="center">
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Object Name</u>
          </td>
          </tr>

          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">$obj_name
          </td>
          </tr>
        
         <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Description</u>
         </td>
         </tr>

         <tr>
          <td colspan="4" align="center"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><textarea name="obj_description" style="width : 620px;"
                        cols="50" rows="5">$obj_description</textarea>
          </td>
          </tr>
                  <tr>
          <td colspan="2"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Content</u>&nbsp;&nbsp;&nbsp;
              <a href="#" onClick="spawn($spawn);"> 
               <img src="https://www.clubshop.com/admin/images/talkbubble3_query.gif"
               width="24" height="24" border="0" alt="Help! Click Me!"></a>
         </td>
         <td colspan="2"
          style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
         </td>
         </tr>
          <tr>
          <td colspan="4" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" width="627">
<textarea name="obj_value" style="width:100%" cols="50" rows="15">$obj_value</textarea>
          </td>
          </tr>
          <tr>
          <td colspan="4">
           <hr noshade align="center">
         </td>
         </tr>
         <!-- DB Call -->
\n~;
        }

$rv = $sth->finish();

print qq~
  <tr>
   <td colspan="4" align="right">
    <input type="submit" class="in" value="Recover" name="action">&nbsp;&nbsp;&nbsp;
    <a href='$SEARCH'><font 
      style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Return to Search Results</font></a>
   </td>
  </tr>

 <tr>
    <td width="615" colspan="4"></td>
  </tr>
</table>
</form>
</body>
</html>
\n~;


$db->disconnect;

exit(0);


sub recover_template{
    my $obj_id = $_[0];
    shift;
    my $pk     = $_[0];

    my $rec_obj_name;  
    my $rec_obj_description; 
    my $rec_obj_value; 
    my $rec_obj_type; 
    my $rec_obj_caller; 
    my $rec_stamp;
 
    my $psql = "SELECT DISTINCT obj_name,obj_description,obj_value,obj_type,obj_caller,stamp
                FROM cgi_objects_arc 
                WHERE obj_id = ? AND pk = ?";

    
    $sth = $db->prepare($psql);

    $rv = $sth->execute($obj_id,$pk);
    defined $rv or die $sth->errstr;

    $sth->bind_columns(undef, \$rec_obj_name,
                              \$rec_obj_description,
                              \$rec_obj_value,
                              \$rec_obj_type,
                              \$rec_obj_caller,
                              \$rec_stamp  );

    $sth->fetch();

    $psql = "UPDATE cgi_objects 
             SET
                obj_description = ?,
                obj_value       = ?
             WHERE 
                obj_id          = ?";

   $sth = $db->prepare($psql);

   $rv = $sth->execute($rec_obj_description,$rec_obj_value,$obj_id);
   defined $rv or die $sth->errstr;                
}
