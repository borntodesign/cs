#!/usr/bin/perl -w
###### refcomp-plan-compare.cgi
###### compare the regular network_calcs with the referral compensation version for a given ID
###### created: 08/08/11	 Bill MacArthur
###### last modified: 08/17/11	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use ADMIN_DB;
use G_S;
use XML::local_utils;

my $q = new CGI;
my $script = $q->script_name;
my ($new, $old) = ();

my $newTBL = 'refcomp_me';
my $oldTBL = 'reference.me_final';

###### we should be in SSL mode
die "This resource should only be invoked in SSL mode\n" unless $q->https;

###### we should have our operator and password cookies
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged in to use this resource\n" unless $operator && $pwd;

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
$db->{'RaiseError'} = 1;

###### we're only expecting these params
my %param = map {$_, G_S::PreProcess_Input(defined $q->param($_) ? $q->param($_) : '')} ('id','list','orderby');

###### we need these values for these params
#foreach('id'){ die "Failed to receive a required parameter: $_\n" unless $param{$_}; }
unless (defined $param{'id'} || $param{'list'})
{
	SubmitIDFrm();
}
elsif ($param{'list'})
{
	ShowList();
}
else
{
	my $id = ($param{'id'} =~ /\D/ ?
		$db->selectrow_array('SELECT id FROM members WHERE alias= UPPER(?)', undef, $param{'id'}) :
		$param{'id'});
	
	die "Failed to identify a membership matching: $param{'id'}" unless defined $id;

	my $xslt = XML::local_utils->new();
	my $xsl = _xsl();
	my ($xml) = $db->selectrow_array("SELECT analysis FROM $newTBL WHERE id=?", undef, $id);
	
	print
		$q->header('-charset'=>'utf-8'),
		$xslt->xslt($xsl, $xml);
}

$db->disconnect;
exit;

sub Diffs
{
	my $col = shift;
	my $diff = $new->{$col} - $old->{$col};
	$diff = sprintf("%.2f", $diff) if $diff != int $diff;
	my $class = $diff > 0 ? 'increase' : $diff < 0 ? 'decrease' : 'same';
	print $q->td( $q->span({'-class'=>$class}, $diff) );
}

sub DisplayRecordset
{
	my $rec = shift;
	$rec->{'total'} = sprintf("%.2f", $rec->{'calculated_pay'} + $rec->{'bonus'});
	$rec->{'real_npp'} = sprintf("%.2f", $rec->{'npp'} - $rec->{'exec_breakaway_volume'});

	my $class = shift;
	print qq|<tr class="$class"><td><b>|, uc($class), '</b></td>';
	foreach(qw/ppp npp exec_breakaway_volume real_npp payout_percentage paid_out mgt_mult
		calculated_pay bonus a_level active_lines total/)
	{
		print $q->td($rec->{$_});
	}
	print qq|</tr><tr class="$class">|;
	print $q->td({'-class'=>'wt'}, '');
	$rec->{'exec_report'} =~ s#\n#<br />#g;
	print $q->td({'-class'=>'report', '-colspan'=>12}, ($rec->{'exec_report'} ? $q->h5('Exec Report') : '') . $rec->{'exec_report'});
	print '</tr>';
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Header
{
	return qq#
		<tr><th><a href="$script?list=1;orderby=id">ID</a></th>
		<th>Current<br />mType</th>
		<th>Name</th>
		<th>PPP</th>
		<th>Real NPP</th>
		<th>Old Org Inc</th>
		<th>Old X<br /> Bonus</th>
		<th>Live<br />Comp</th>
		<th>New<br />Comp</th>
		<th>Frontline PPP</th>
		<th>50% Inc</th>
		<th>25% Inc</th>
		<th>Dividends</th>
		<th><a href="$script?list=1;orderby=newlvl%20DESC">New Pay<br />Level</a></th>
		<th>Old Pay<br /> Level</th>
		<th><a href="$script?list=1;orderby=prvs%20DESC">PRVs</a></th>
		<th><a href="$script?list=1;orderby=prv_handicap%20DESC">PRV Handicap</a></th>
		<th><a href="$script?list=1">Difference</a></th>
		</tr>#;
}

sub Legend
{
	return qq|<p style="margin-top: 0; float:right; width:10em; font:normal small sans-serif; text-align:left;">
ExBV: Exec Breakaway Volume<br />
Level: Network Level<br />
AL: Active Lines<br />
<a href="${\ $q->url}?list=1">Show complete list</a><br />
<a href="${\ $q->url}">New Search</a>
</p>|;
}

sub ShowList
{
	$param{'orderby'} ||= 'total_diff';
	print $q->header;
print <<"header";
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Referral Comp Plan comparisons List</title>
<style type="text/css">
table {	border-collapse:collapse; font:normal small sans-serif;}
td, th {padding:0.3em; border:1px solid silver;}
td {  text-align:right;}
th { background-color: #eee; font-size:90%; vertical-align:top;}
tr.a {background-color:#ffe;}
tr.b {background-color:#eee;}
a {text-decoration:none;}
td.r, a.r {color:red;}
td.g, a.g {color:green;}

</style>
</head>
<body>
<table><thead>${\(Header())}
</thead><tbody class="scrollContent">
header

	my %ttls = ();
	my $sth = $db->prepare(qq#
		SELECT
			m.id,
			cn.ppp,
			(cn.npp - cn.exec_breakaway_volume) AS realnpp,
			cn.calculated_pay,
			cn.referral_commissions,
			cn.bonus,
			(cn.calculated_pay + cn.bonus) AS live_comp,
			cn.a_level AS oldlvl,
			m.firstname ||' '||m.lastname AS name,
--			COALESCE(nbtl.code,'') AS role,
			m.membertype,
			(rfc.my50 + rfc.my25 + rfc.dividends) AS new_comp,
			rfc.frontline_ppp,
			rfc.my50,
			rfc.my25,
			rfc.dividends,
			COALESCE(rfc.a_level, 0) AS newlvl,
			(COALESCE((rfc.my50 + rfc.my25 + rfc.dividends),0) - COALESCE((cn.calculated_pay + cn.bonus),0)) AS total_diff,
			rfc.prvs,
			rfc.prv_handicap

		FROM members m
		LEFT JOIN $oldTBL cn
			ON cn.id=m.id
--		LEFT JOIN nbteam_vw nbt
--			ON nbt.id=m.id
--		LEFT JOIN nbteam_levels nbtl
--			ON nbtl."level"=nbt."level"
		LEFT JOIN $newTBL rfc
			ON rfc.id=m.id
		WHERE (cn.id IS NOT NULL OR rfc.id IS NOT NULL)
		AND (COALESCE((rfc.my50 + rfc.my25 + rfc.dividends),0) - COALESCE((cn.calculated_pay + cn.bonus),0)) <> 0
		ORDER BY $param{'orderby'}#);
	$sth->execute;
	my $class = 'a';
	my $diffTTL = 0;
	my $diff_class = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$ttls{'my25'} += $tmp->{'my25'};
		$ttls{'my50'} += $tmp->{'my50'};
		$ttls{'dividends'} += $tmp->{'dividends'};
		$ttls{'frontline_ppp'} += $tmp->{'frontline_ppp'};
		$ttls{'new_comp'} += $tmp->{'new_comp'};
		$ttls{'prvs'} += $tmp->{'prvs'};
		$ttls{'calculated_pay'} += $tmp->{'calculated_pay'};
		$ttls{'bonus'} += $tmp->{'bonus'};
		$ttls{'live_comp'} += $tmp->{'live_comp'};
		
		$diffTTL += $tmp->{'total_diff'};
		my $id = $tmp->{'id'};
		$diff_class = $tmp->{'total_diff'} > 0 ? 'g' : $tmp->{'total_diff'} < 0 ? 'r' : 'default';
		print qq|<tr class="$class">|;
		foreach (qw/id membertype name ppp realnpp calculated_pay bonus live_comp new_comp frontline_ppp my50 my25 dividends newlvl oldlvl prvs prv_handicap total_diff/)
		{
			$tmp->{$_} = $q->a({'-href'=>$q->url . "?id=$id"}, $tmp->{$_}) if $_ eq 'id';
			$tmp->{$_} = $q->a({'-href'=>$q->url . "?id=$id", '-class'=>$diff_class}, $tmp->{$_}) if $_ eq 'total_diff';
			print $q->td($tmp->{$_});
		}
		print '</tr>';
		$class = $class eq 'a' ? 'b' : 'a';
	}
	$diff_class = $diffTTL >= 0 ? 'g' : 'r';
	print qq|</tbody><tfoot>${\(Header())}
		<tr>
		<td colspan="5">Totals</td>
		<td>${\(sprintf '%.2f', $ttls{'calculated_pay'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'bonus'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'live_comp'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'new_comp'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'frontline_ppp'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'my50'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'my25'})}</td>
		<td>${\(sprintf '%.2f', $ttls{'dividends'})}</td>
		<td></td>
		<td></td>
		<td>$ttls{'prvs'}</td>
		<td></td>
		<td class="$diff_class">|;
	printf '%.2f', $diffTTL;
	print "</td></tr>";
	print '<tr><td colspan="10">Percentages</td><td>';
	printf '%.2f', (($ttls{'my50'}/$ttls{'frontline_ppp'}) * 100);
	print '</td><td>';
	printf '%.2f', (($ttls{'my25'}/$ttls{'frontline_ppp'}) * 100);
	print '</td><td>';
	printf '%.2f', (($ttls{'dividends'}/$ttls{'frontline_ppp'}) * 100);
	print '</td><td colspan="4"></td></tr>';
	print '</tfoot></table></body></html>';
}

sub SubmitIDFrm
{
	print $q->header,
		$q->start_html(
			'-onload'=>"document.getElementById('id').focus()",
			'-title'=>'Comp Plan Comparisons'),
		Legend(),
		$q->h4('Enter an ID'),
		$q->start_form,
		$q->textfield('-name'=>'id', '-id'=>'id'),
		$q->submit,
		$q->end_form,
		$q->p( $q->a({'-href'=>$q->url . '?list=1'},'Show List') ),
		$q->end_html;
}

sub _xsl
{
	return q|
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>



	
	<xsl:template match="/">	
		<html>
			<title></title>
			<style type="text/css">
				.numeric { text-align:right; }
				tr.even { background-color:#eee; }
				tr.odd { background-color:#ffe; }
				.ltgr { background-color:#efe; }
				#wrapper td { vertical-align:top; }
				td.mme { padding-left:80px; }
			</style>
			<body>
			<h4>Referral Compensation Plan Details for <xsl:value-of select="*/@id" /></h4>
			<table>
			<tr><td>Frontline PP: </td><td class="numeric"><xsl:value-of select="me/@frontline_ppp" /></td></tr>
			<tr><td>Frontline Income: </td><td class="numeric ltgr"><xsl:value-of select="me/@frontline_income" /></td></tr>
			<tr><td>25% Income: </td><td class="numeric ltgr"><xsl:value-of select="me/@my25" /></td></tr>
			<tr><td>Dividends: </td><td class="numeric ltgr"><xsl:value-of select="format-number(sum(me/dividends/@*), '0.00')" /></td></tr>
			<tr><td>Total: </td><td class="numeric ltgr"><xsl:value-of select="format-number( (sum(me/dividends/@*) + me/@frontline_income + me/@my25), '0.00')" /></td></tr>
			</table>
			<br />
			
			<table id="wrapper"><tr><td>
			
			25% Details:<br /><br />
			<table>
			<tr><th>ID</th><th>Original PP</th><th>Potential Pay</th><th>Allowed Pay</th></tr>
			
			<xsl:for-each select = "me/dn">
			<tr><xsl:attribute name="class">
				<xsl:choose>
				<xsl:when test="position() mod 2 = 0">even</xsl:when>
				<xsl:otherwise>odd</xsl:otherwise>
				</xsl:choose>
				</xsl:attribute>
			<td><xsl:value-of select="@id" /></td>
			<td class="numeric"><xsl:value-of select="@orig_pp" /></td>
			<td class="numeric"><xsl:value-of select="@potential_pay" /></td>
			<td class="numeric"><xsl:value-of select="@commission" /></td>
			</tr>
			</xsl:for-each>
			
			<tr><td colspan="3" align="right">Total: </td><td class="numeric ltgr"><xsl:value-of select="*/@my25" /></td></tr>
			</table>
			
			</td><td class="mme">

			Dividends:<br /><br />
			
			<table>
			<tr><th>Pay Level</th><th>Dividend</th></tr>
			<tr class="odd"><td>TP</td><td class="numeric"><xsl:value-of select="*/dividends/@_2" /></td></tr>
			<tr class="even"><td>TL</td><td class="numeric"><xsl:value-of select="*/dividends/@_3" /></td></tr>
			<tr class="odd"><td>TC</td><td class="numeric"><xsl:value-of select="*/dividends/@_4" /></td></tr>
			<tr class="even"><td>BM</td><td class="numeric"><xsl:value-of select="*/dividends/@_5" /></td></tr>
			<tr class="odd"><td>SM</td><td class="numeric"><xsl:value-of select="*/dividends/@_6" /></td></tr>
			<tr class="even"><td>GM</td><td class="numeric"><xsl:value-of select="*/dividends/@_7" /></td></tr>
			<tr class="odd"><td>RD</td><td class="numeric"><xsl:value-of select="*/dividends/@_8" /></td></tr>
			<tr class="even"><td>ED</td><td class="numeric"><xsl:value-of select="*/dividends/@_9" /></td></tr>
			<tr class="odd"><td>DD</td><td class="numeric"><xsl:value-of select="*/dividends/@_10" /></td></tr>
			<tr class="even"><td>Exec</td><td class="numeric"><xsl:value-of select="*/dividends/@_11" /></td></tr>
			<tr class="odd"><td>Exec 1</td><td class="numeric"><xsl:value-of select="*/dividends/@_12" /></td></tr>
			<tr class="even"><td>Exec 2</td><td class="numeric"><xsl:value-of select="*/dividends/@_13" /></td></tr>
			<tr class="odd"><td>Exec 3</td><td class="numeric"><xsl:value-of select="*/dividends/@_14" /></td></tr>
			<tr class="even"><td>Exec 4</td><td class="numeric"><xsl:value-of select="*/dividends/@_15" /></td></tr>
			<tr class="odd"><td>Exec 5</td><td class="numeric"><xsl:value-of select="*/dividends/@_16" /></td></tr>
			<tr class="even"><td>Exec 6</td><td class="numeric"><xsl:value-of select="*/dividends/@_17" /></td></tr>
			<tr><td align="right">Total:</td><td class="numeric ltgr"><xsl:value-of select="format-number(sum(*/dividends/@*), '0.00')" /></td></tr>
			</table>
			
			</td>
			<td class="mme">
			Execs:<br /><br />
			<xsl:for-each select = "me/exec">
				<xsl:value-of select="." /><br />
			</xsl:for-each>
			
			</td>
			
			</tr></table>
			
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
	|;
}
