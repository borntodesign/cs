#!/usr/bin/perl -w
###### reporting.cgi
###### admin script to provide various reports
###### created: 02/28/08	Bill MacArthur
###### last modified: 05/12/15	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use ADMIN_DB;
require cs_admin;
binmode STDOUT, ":encoding(utf8)";
$| = 1;

###### GLOBALS
my $start_run = time();
my $q = new CGI;
$q->charset('utf-8');
my $def_results_limit = 100;	# how many 'rows' we`ll return before warning them

# value is a required flag
my ($db) = ();
my $rowclass = 'b';			# a global we can keep reusing to maintain 'state' between row calls

###### here is where we get our admin user and try logging into the DB
if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}

my %params = map {$_, $q->param($_)} $q->param;

unless ($params{'query_id'})
{
	Err('No query ID received');
}
else
{
	die "Invalid query_id: $params{'query_id'}\n" if $params{'query_id'} =~ /\D/;
	eval('_Query'.$params{'query_id'});
	die $@ if $@;
}
$db->disconnect;
exit;

sub DumpParams
{
	foreach (sort keys %params)
	{
		next if /submit/i || $params{$_} =~ /submit/i;
		print $q->div("$_ : " . (defined $params{$_} ? $params{$_} : ''));
	}
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print $_[0];
	DumpParams();
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Header
{
	print $q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-encoding'=>'utf-8',
			'-title'=>'Report',
			'-style'=>{'-src'=>'/css/admin/generic-report.css'},
			'-script'=>{'-src'=>'/js/jquery.min.js'}
		), $q->div({'-style'=>'font:bold smaller sans-serif'}, $_[0]);
}

sub MegaResults
{
	print $q->header,
		$q->start_html('Large Result Set'),
		$q->h4("$_[0] results returned using these criteria");
		
	DumpParams();
	
	print $q->p( $q->a({'-href'=>$q->self_url . ';ignore_limit=1'}, 'Pull the report ignoring the limit'));
	
	my $url = $q->self_url;
	###### if we already have a limit param in place, we'll modify the value, otherwise we break the URL
	unless ($url =~ s/limit=\d*/limit=$def_results_limit/)
	{
		$url .= ";limit=$def_results_limit"
	}
	print $q->p( $q->a({'-href'=>$url }, "Pull the report limiting results to: $def_results_limit"));
	print $q->end_html;

}
sub NoResults
{
	print $q->header, $q->start_html('No Results'),
		$q->h4('No results returned using these criteria');
	DumpParams();
	print $q->end_html;
}

sub PatienceIn
{
	print $q->p({'-id'=>'patienceBox', '-style'=>'color:blue'}, "Be patient, this could take a few moments");
}

sub PatienceOut
{
	print q|<script type="text/javascript">
		$( document ).ready(function() {
			if ( $('#patienceBox').length ) {
    			$('#patienceBox').remove();
			}
  		});
  	</script>|; 
}

sub RowClass
{
	$rowclass = $rowclass eq 'a' ? 'b':'a';
	return $rowclass;
}

sub _Query1
{
	###### all appx referers based on date

	###### if they only submitted one value, fill in the other with the same
	$params{'date_start'} ||= $params{'date_end'};
	$params{'date_end'} ||= $params{'date_start'};
	die "Missing a start or end date\n" unless $params{'date_start'} || $params{'date_end'};

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'}){
		undef $limit;
	}

	my @args = ($params{'date_start'}, $params{'date_end'});
	
	my $qry = '
		SELECT stamp, referer
		FROM tracking_appx_referers
		WHERE stamp BETWEEN ? AND ?';
	$qry .= ' ORDER BY pk DESC';
	if ($limit){
		$qry .= " LIMIT ?";
		push @args, $limit;
	}

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@args);
	if ($rv > $def_results_limit && ! ($params{'limit'} || $params{'ignore_limit'}))
	{
		MegaResults($rv);
		return undef;
	}
	Header('Grouped referers by date range');
	print '<table class="report"><tr><th>Date</th><th>Referer</th></tr>';
	while (my $tmp = $sth->fetchrow_hashref){
		print '<tr class="', RowClass(), '">';
		print "<td>$tmp->{stamp}</td>\n";
		print '<td>',
			$q->a({-href=>$tmp->{referer}, -onclick=>"window.open(this.href); return false;"},
				$q->escapeHTML($tmp->{referer})),
			'</td>';
		print '</tr>';
	}
	print '</table>', $q->end_html;
}

sub _Query2
{
	###### total counts for all appx referers

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'}){
		undef $limit;
	}

	my @args = ();
	
	my $qry = '
		SELECT COUNT(*) AS cnt, referer
		FROM tracking_appx_referers
		GROUP BY referer';
	if ($params{'hit_cnt'}){
		$qry .= " HAVING COUNT(*) > ?";
		push @args, $params{'hit_cnt'};
	}
	$qry .= ' ORDER BY cnt DESC';
	if ($limit)
	{
		$qry .= " LIMIT ?";
		push @args, $limit;
	}

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@args);
	if ($rv > $def_results_limit && ! ($params{'limit'} || $params{'ignore_limit'}))
	{
		MegaResults($rv);
		return undef;
	}
	Header('Total hit counts grouped by referer');
	print '<table class="report"><tr><th>Hits</th><th>Referer</th></tr>';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		print '<tr class="', RowClass(), '">';
		print "<td>$tmp->{cnt}</td>\n";
		print '<td>',
			$q->a({'-href'=>$tmp->{'referer'}, '-onclick'=>"window.open(this.href); return false;"},
				$q->escapeHTML($tmp->{'referer'})),
			'</td>';
		print '</tr>';
	}
	print '</table>', $q->end_html;
}

sub _Query3
{
	###### all appx referers based on date

	###### if they only submitted one value, fill in the other with the same
	$params{'date_start'} ||= $params{'date_end'};
	$params{'date_end'} ||= $params{'date_start'};
	die "Missing a start or end date\n" unless $params{'date_start'} || $params{'date_end'};

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'})
	{
		undef $limit;
	}

	my @args = ($params{'date_start'}, $params{'date_end'});
	
	my $qry = '
		SELECT stamp, COUNT(*) AS cnt, referer
		FROM tracking_appx_referers
		WHERE stamp BETWEEN ? AND ?
		GROUP BY stamp, referer';
	if ($params{'hit_cnt'})
	{
		$qry .= " HAVING COUNT(*) > ?";
		push @args, $params{'hit_cnt'};
	}
	
	$qry .= ' ORDER BY stamp DESC, cnt DESC';
	if ($limit)
	{
		$qry .= " LIMIT ?";
		push @args, $limit;
	}

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@args);
	if ($rv > $def_results_limit && ! ($params{'limit'} || $params{'ignore_limit'}))
	{
		MegaResults($rv);
		return undef;
	}
	Header('Grouped referers by date range');
	print '<table class="report"><tr><th>Date</th><th>Hits</th><th>Referer</th></tr>';
	while (my $tmp = $sth->fetchrow_hashref){
		print '<tr class="', RowClass(), '">';
		print "<td>$tmp->{stamp}</td>\n";
		print "<td>$tmp->{cnt}</td>\n";
		print '<td>',
			$q->a({'-href'=>$tmp->{'referer'}, '-onclick'=>"window.open(this.href); return false;"},
				$q->escapeHTML($tmp->{'referer'})),
			'</td>';
		print '</tr>';
	}
	print '</table>', $q->end_html;
}

sub _Query4
{
	###### count totals for appx referers based on textual match

	###### we need a term to search for
	die "Invalid or empty text/keyword parameter\n" unless $params{'keyword'};

	###### if they only submitted one value, fill in the other with the same
	$params{'date_start'} ||= $params{'date_end'};
	$params{'date_end'} ||= $params{'date_start'};

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'})
	{
		undef $limit;
	}

	my @args = $params{'keyword'};
	my $qry = 'SELECT ' . ($params{'group_by'} =~ /stamp/ ? 'stamp,' : '');
	$qry .= '
		COUNT(*) AS cnt, referer
		FROM tracking_appx_referers WHERE referer ~* ?';
		
	if ($params{'date_start'} && $params{'date_end'})
	{
		push @args, ($params{'date_start'}, $params{'date_end'}) if $params{'date_start'} && $params{'date_end'};
		$qry .= ' AND stamp BETWEEN ? AND ?'
	}
	
	die "Invalid group_by paramater: $params{group_by}\n" unless grep $params{'group_by'} eq $_, ('referer','stamp,referer');
	$qry .= " GROUP BY $params{'group_by'}";

	if ($params{'hit_cnt'})
	{
		$qry .= " HAVING COUNT(*) > ?";
		push @args, $params{'hit_cnt'};
	}
	$qry .= ' ORDER BY' . ($params{'group_by'} =~ /stamp/ ? ' stamp DESC,' : '') .  ' cnt DESC';
	if ($limit){
		$qry .= " LIMIT ?";
		push @args, $limit;
	}

	my $sth = $db->prepare($qry);
	die $db->errstr . "\n$qry\n" if $db->errstr;
	
	my $rv = $sth->execute(@args);
	die $db->errstr . "\n$qry\n" if $db->errstr;
	
	if ($rv == 0)
	{
		NoResults();
		return undef;
	}
	elsif ($rv > $def_results_limit && ! ($params{'limit'} || $params{'ignore_limit'})){
		MegaResults($rv);
		return undef;
	}
	Header('Grouped referers by date range');
	print '<table class="report"><tr><th>Date</th><th>Hits</th><th>Referer</th></tr>';
	while (my $tmp = $sth->fetchrow_hashref){
		print '<tr class="', RowClass(), '">';
		print $q->td($tmp->{stamp} || '');
		print "<td>$tmp->{cnt}</td>\n";
		print '<td>',
			$q->a({'-href'=>$tmp->{'referer'}, '-onclick'=>"window.open(this.href); return false;"},
				$q->escapeHTML($tmp->{'referer'})),
			'</td>';
		print '</tr>';
	}
	print '</table>', $q->end_html;
	return undef;
}

sub _Query5
{
	###### membership counts grouped by country, membership type

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'})
	{
		undef $limit;
	}

	my @args = ();
	
	my $qry = q|
		SELECT COUNT(m.*) AS cnt, COALESCE(m.country, '') AS country, COALESCE(cc.country_name, ' country undefined') AS country_name, m.membertype
		FROM members m
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=m.country
		LEFT JOIN configurations.placeholder_partners cpp ON cpp.id=m.id
		WHERE m.membertype IN ('ag','m','s','v','tp','ma')
		AND cpp.id IS NULL
		GROUP BY COALESCE(m.country, ''), COALESCE(cc.country_name, ' country undefined'), m.membertype
		ORDER BY COALESCE(cc.country_name, ' country undefined'), m.membertype|;

	my $sth = $db->prepare($qry);

	Header('Memberships per Country');
	
	Table_1($sth);

	print $q->end_html;
}

sub _Query6
{
	###### membership counts grouped by country, membership type

	my $limit = $params{'limit'};
	###### if they have chosen to ignore the limit let 'em have it
	if ($params{'ignore_limit'})
	{
		undef $limit;
	}

	my @args = ();
	
	my $qry = q|
		WITH holders AS (SELECT DISTINCT current_member_id FROM clubbucks_master)
		SELECT COUNT(m.*) AS cnt, COALESCE(m.country, '') AS country, COALESCE(cc.country_name, ' country undefined') AS country_name, m.membertype
		FROM members m
		JOIN holders
			ON holders.current_member_id=m.id
		LEFT JOIN tbl_countrycodes cc ON cc.country_code=m.country
		WHERE m.membertype IN ('ag','m','s','v','tp','ma')
		GROUP BY COALESCE(m.country, ''), COALESCE(cc.country_name, ' country undefined'), m.membertype
		ORDER BY COALESCE(cc.country_name, ' country undefined'), m.membertype|;

	my $sth = $db->prepare($qry);

	Header('Club Rewards Card Holders per Country');
	
	Table_1($sth);

	print $q->end_html;
}

sub _Query7
{
	# "New VIPs Report" tp replace the old newvips.xsp from the old server
	my $sth = $db->prepare("
		SELECT
			m.id AS nv_id,
			m.alias AS nv_alias,
			m.firstname ||' '|| m.lastname AS nv_name,
			m.country AS nv_country,
			m.vip_date AS nv_vip_date,
			msp.alias AS sp_alias, msp.firstname ||' '|| msp.lastname AS sp_name,
			mc.alias AS c_alias, mc.firstname ||' '|| mc.lastname AS c_name,
			mx.alias AS x_alias, mx.firstname ||' '|| mx.lastname AS x_name
		FROM members m
		JOIN members msp ON msp.id=m.spid
		LEFT JOIN my_coach ON my_coach.id=m.id
		LEFT JOIN members mc ON mc.id=my_coach.upline
		LEFT JOIN relationships r ON r.id=m.id AND r.x1=TRUE
		LEFT JOIN members mx ON mx.id = r.upline
		WHERE m.vip_date >= first_of_month()
		AND m.membertype='v'
		ORDER BY m.vip_date
	");
	
	my $rowcnt = $sth->execute;
	
	print $q->header(),
		$q->start_html(
			'-encoding'=>'utf-8',
			'-title'=>'Report',
			'-style'=>{'-src'=>'/css/admin/generic-report.css', '-code'=>'table.report {font-size:110%}'},
			'-script'=>{'-src'=>'/js/jquery.min.js'}
		), $q->div({'-style'=>'font:bold smaller sans-serif'}, "New Partners ($rowcnt)");
	
	unless ($rowcnt)
	{
		print $q->h3('No new Partners yet');
	}
	else
	{
		print $q->div({'-style'=>'font-size:75%'}, 'Click headings to sort');
		print '<table class="report"><thead><tr>
			<th><a href="#" onclick="sort(this)">ID</a></th>
			<th>New Partner</th>
			<th><a href="#" onclick="sort(this)">Country</a></th>
			<th><a href="#" onclick="sort(this)">Upgrade Date</a></th>
			<th><a href="#" onclick="sort(this)">Sponsor</a></th>
			<th><a href="#" onclick="sort(this)">Coach</a></th>
			<th><a href="#" onclick="sort(this)">Exec</a></th>
			</tr></thead><tbody id="data_table">';
		
		while (my $t = $sth->fetchrow_hashref)
		{
			print $q->Tr(
				$q->td($q->a({'-href'=>"/cgi/vip/member-followup.cgi?rid=$t->{'nv_id'}", '-onclick'=>'window.open(this.href);return false;'}, $t->{'nv_alias'})) .
				$q->td($t->{'nv_name'}) .
				$q->td($t->{'nv_country'}) .
				$q->td($t->{'nv_vip_date'}) .
				$q->td($t->{'sp_name'}) .
				$q->td($t->{'c_name'} || '') .
				$q->td($t->{'x_name'} || '')
			);
		}
		
		print '</tbody></table><script type="text/javascript" src="/admin/reporting/sortq5.js"></script>';
	}

	print $q->end_html;
}

sub _Query8
{
	# PP differentials report
	$params{'differential'} ||= 5;	# default value
	my $diff = $params{'differential'} !~ m/\D/ ? $params{'differential'} : 5;
	
	my $pdiff = $diff/100;
	
	my $sth = $db->prepare(qq#
		WITH mya AS (
			SELECT COUNT(*) AS cnt, subscription_type, currency_code
			FROM partner_subscription_annual psa
			WHERE psa.paid=TRUE
			GROUP BY subscription_type, currency_code
			UNION
			SELECT COUNT(*) AS cnt, subscription_type, currency_code
			FROM partner_subscription_monthly psm
			WHERE psm.paid=TRUE
			GROUP BY subscription_type, currency_code
		)
		SELECT spc.subscription_type AS stype,
			st.description,
			spc.amount,
			spc.currency_code,
			spc.usd,
		--	spc.notes,
			spc.effective_date,
			spc.pp AS curr_pp,
			COALESCE(spp.pp_cap,spp_def.pp_cap) AS pp_cap,
			(spc.usd * COALESCE(spp.percentage,spp_def.percentage))::NUMERIC(6,2) AS calc_pp,
			((spc.usd * COALESCE(spp.percentage,spp_def.percentage)) - COALESCE(spp.pp_cap,spp_def.pp_cap))::NUMERIC(6,2) AS pp_overage,
			(((spc.usd * COALESCE(spp.percentage,spp_def.percentage)) - COALESCE(spp.pp_cap,spp_def.pp_cap)) / COALESCE(spp.pp_cap,spp_def.pp_cap) * 100)::NUMERIC(4,1) AS pct_over,
			CASE WHEN mya.currency_code IS NULL THEN NULL::BOOLEAN ELSE TRUE END AS active,
			mya.cnt
		FROM subscription_pricing_current spc
		JOIN subscription_types st
			ON st.subscription_type=spc.subscription_type
		LEFT JOIN mya
			ON mya.subscription_type=spc.subscription_type
			AND mya.currency_code=spc.currency_code
		LEFT JOIN subscription_point_percentages spp
			ON spp.subscription_type=spc.subscription_type
			AND spp.currency_code=spc.currency_code
		LEFT JOIN subscription_point_percentages spp_def
			ON spp_def.subscription_type=spc.subscription_type
			AND spp_def.currency_code='def'
		WHERE ((spc.usd * COALESCE(spp.percentage,spp_def.percentage)) > (COALESCE(spp.pp_cap,spp_def.pp_cap) * (1 + $pdiff)))
		OR ((spc.usd * COALESCE(spp.percentage,spp_def.percentage)) < (COALESCE(spp.pp_cap,spp_def.pp_cap) * (1 - $pdiff)))
		ORDER BY "active", spc.currency_code, spc.subscription_type
	#);
	
	my $rowcnt = $sth->execute;
	
	print $q->header(),
		$q->start_html(
			'-encoding'=>'utf-8',
			'-title'=>'PP differentials Report',
			'-style'=>{'-src'=>'/css/admin/generic-report.css', '-code'=>'table.report {font-size:110%}'},
			'-script'=>{'-src'=>'/js/jquery.min.js'}
		), $q->div({'-style'=>'font:bold smaller sans-serif'}, "PP differentials &lt;=&gt; $diff\% ($rowcnt)");
	
	unless ($rowcnt)
	{
		print $q->h3("No differentials over $diff\%");
	}
	else
	{
		print $q->div({'-style'=>'font-size:75%'}, 'Click headings to sort. <span class="fpnotes">Be patient if there are hundreds of rows.</span>');
		print $q->start_form('-style'=>'font-size:75%; margin-top:0.5em;', '-method'=>'get'),
			'Change the differential: ',
			$q->popup_menu(
				'-name'=>'differential',
				'-onchange'=>'this.parentNode.submit()',
				'-values'=>[5,10,15,20]
			),
			$q->hidden('query_id'),
			$q->end_form;
		print '<table class="report"><thead><tr>
			<th><a href="#" onclick="sort(this)">stype</a></th>
			<th>Description</th>
			<th><a href="#" onclick="sort(this)">Native Price</a></th>
			<th><a href="#" onclick="sort(this)">Ccode</a></th>
			<th><a href="#" onclick="sort(this)">USD</a></th>
			<th>Effective Date</th>
			<th><a href="#" onclick="sort(this)">Current PP Val</a></th>
			<th>PP Cap</th>
			<th>Calc PP Val</th>
			<th><a href="#" onclick="sort(this)">PP overage</a></th>
			<th><a href="#" onclick="sort(this)">% over</a></th>
			<th><a href="#" onclick="sort(this)" title="Number of active subscriptions matching this currency/subscription type">Active</a></th>
			</tr></thead><tbody id="data_table">';
		
		while (my $t = $sth->fetchrow_hashref)
		{
			my $class = $t->{'pp_overage'} > 0 ? '' : 'error';
			print $q->Tr(
				$q->td($t->{'stype'}) .
				$q->td($t->{'description'}) .
				$q->td($t->{'amount'}) .
				$q->td( $q->a({'-target'=>'_blank', '-href'=>"/admin/fee-pp-adjust.php?currency=$t->{'currency_code'}&frum=live&action=lookup_currency"}, $t->{'currency_code'})) .
				$q->td($t->{'usd'}) .
				$q->td($t->{'effective_date'}) .
				$q->td({'-class'=>$class}, $t->{'curr_pp'}) .
				$q->td($t->{'pp_cap'}) .
				$q->td({'-class'=>$class}, $t->{'calc_pp'}) .
				$q->td({'-class'=>$class}, $t->{'pp_overage'}) .
				$q->td({'-class'=>$class}, $t->{'pct_over'}) .
				$q->td($t->{'cnt'} || '')
			);
		}
		
		print '</tbody></table><script type="text/javascript" src="/admin/reporting/sortq5.js"></script>';
	}

	print $q->end_html;
}

sub Table_1
{
	my $sth = shift;
	
	# the query takes a while to complete and the server times it out if we don't start outputting something, so we will do the header and create a notice that we can remove after
	PatienceIn();
	my $rv = $sth->execute();
	
	my $xseconds = time() - $start_run;
	
	print $q->p("That query took: $xseconds seconds");
	
	print $q->div({'-style'=>'font-size:75%'}, 'Click headings to sort');
	print '<table class="report"><thead><tr><th><a href="#" onclick="sort(this)">Country</a></th>';
	print '<th><a href="#" onclick="sort(this)">' . uc($_) . '</a></th>' foreach ('ag','m','s','v','tp','ma');
	print '<th><a href="#" onclick="sort(this)">Total</a></th></tr></thead><tbody id="data_table">';
	
	my $wc = ();
	my %list = ();
	my %grand_ttls = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$wc = $tmp->{'country_name'} || 0;	###### there are a number of memberships without a designated country
		$list{$wc}->{'country_name'} = $tmp->{'country_name'} || '';
		$list{$wc}->{$tmp->{'membertype'}} = $tmp->{'cnt'};
		$grand_ttls{$tmp->{'membertype'}} += $tmp->{'cnt'};
		$list{$wc}->{'total'} += $tmp->{'cnt'};
	}
	
	foreach my $cc (sort keys %list)
	{
		print '<tr>';
		print "<td>$list{$cc}->{'country_name'}</td>\n";
		foreach ('ag','m','s','v','tp','ma')
		{
			print $q->td($list{$cc}->{$_} || '');
		}
		print $q->td($list{$cc}->{'total'} || ''), "</tr>\n";
	}
	print '</tbody><tbody><tr><th></th>';
	print '<th>' . $grand_ttls{$_} . '</th>' foreach ('ag','m','s','v','tp','ma');
	
	my $grand_total = 0;
	$grand_total += $grand_ttls{$_} foreach keys %grand_ttls;
	print "<th>$grand_total</th></tr>";
 
	print '</tbody></table><script type="text/javascript" src="/admin/reporting/sortq5.js"></script>';
	
	PatienceOut();
}

__END__

11/11/14	added _Query7()
05/12/15	added the binmode to compensate for the recent upgrade of DBD::Pg