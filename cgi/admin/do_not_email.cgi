#!/usr/bin/perl
###### do_not_email.cgi
###### Add to the do_not_email database table.
###### Created by Stephen Martin 11/26/2002
###### revision history at the end

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Email::Valid;

require '/home/httpd/cgi-lib/G_S.lib';

$| = 1;

###### Globals

my $q       = new CGI;
my $MESSAGE = $q->param('MESSAGE');
my $action  = $q->param('action');
my $dat	    = $q->param('dat');
my $self   = $q->url();

my $db;
my $sth;
my $rv;
my $qry;
my $data;

my $valid;
my $TMPL;

###### here is where we get our user and try logging into the DB

my $operator = $q->cookie('operator');
my $pwd      = $q->cookie('pwd');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
unless ( $db = &ADMIN_DB::DB_Connect( 'do_not_email.cgi', $operator, $pwd ) ) {
    exit;
}
$db->{RaiseError} = 1;

###### If we are still here we must be good..
###### Print the inital template

###### Catch Add Request ######

if ( $action eq " Add Email " ) {

    unless ( $dat )
    {
     &Err("Missing Email Address!");
     exit;
    }

    $valid = 0;

    ( my $u, my $d ) = split(/\@/,$dat);

    if ( $u eq "%" )
    {
     if ( $d =~ /^\w+([\.-]?\w+)*(\.\w{2,3})+$/ )
     {
      $valid = 1;
     }
    }    
     else 
    {
     $valid = Email::Valid->address($dat);
    }
    
    if ( $valid )
    {
     add_e($dat);
     $MESSAGE = "$dat added to do_not_email table.";
    }
     else 
    {
     $MESSAGE = "$dat is <b>NOT</b> a well formed email address!";
    }

}

###### Print the default template

$TMPL = &G_S::Get_Object( $db, 10067 );
unless ($TMPL) { &Err("Couldn't retrieve the template object."); exit; }

print $q->header();

$TMPL =~ s/%%SELF%%/$self/g;
$TMPL =~ s/%%MESSAGE%%/$MESSAGE/g;

print $TMPL;

$db->disconnect;

exit;

sub Err {
    print $q->header(), $q->start_html();
    print $_[0];
    print "<br>\n";
    print $q->end_html();
}

sub add_e
{
 my ($e) = @_;

 my ( $sth, $qry, $data, $rv );    ###### database variables

$qry = "INSERT INTO
         do_not_email
        ( emailaddress, entered_by ) VALUES ( ?,?)";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($e,$operator);
     defined $rv or die $sth->errstr;

    $rv = $sth->finish();
}
