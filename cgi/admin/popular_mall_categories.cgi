#!/usr/bin/perl -w
################################################################
# marketing_questionnairs_admin.cgi
#
# A general report for the Marketing Questionnaires
#
# Deployed: 06/08
# Author: Keith Hasely
# Version: 0.1
################################################################

################################################################
# Includes
################################################################
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser set_message);

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
our $q = CGI->new();
our $gs = new G_S;
our $ME = $q->url;
our $db = ();
my $sth = '';

my $country = $q->param('country')?$q->param('country'):'US';
################################################################
# Subroutine(s)
################################################################

###############################
# This sub came around because
# Bill was/is using a "goto" statement!
#
###############################
sub exitScript
{
	$db->disconnect if ($db);
	exit;
}
###############################

###############################
# Output an error message for the user
# then exit the script cleanly
#
# Param		hash
# 			string message			The mesage to output before exiting.
# Return	null
###############################
sub errorExitScript
{
	my $args = shift || {};
	print $q->header(-TYPE => 'text/html');

	print qq(<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>);
	print $args->{message};
	print qq(</title></head><body><p>);
	print $args->{message};
	print qq(</p></body></html>);
	exitScript();
}
###############################


################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exitScript() unless $db;
$db->{RaiseError} = 1;


# Check if the user is admin
if ((!$q->cookie('operator') && !$q->cookie('pwd')) && !ADMIN_DB::DB_Connect( 'pwp', $q->cookie('operator'), $q->cookie('pwd')))
{
	exitScript();
}


print $q->header(-TYPE => 'text/html');
# @import "all.css"; /* just some basic formatting, no layout stuff */

print <<EOS;
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

				<title>Questionnaires Reports</title>

<style type="text/css">
	\@import "all.css"; /* just some basic formatting, no layout stuff */

	body {
		font:	12px/1.2 Verdana, Arial, Helvetica, sans-serif;
		background:#ddd;
		margin:10px 10px 0px 10px;
		padding:0px;
		}

	#leftcontent {
		position: absolute;
		left:10px;
		top:50px;
		width:10px;
		background:#fff;
		border:1px solid #000;
		}

	#centercontent {
		background:#fff;
   		margin-left: 0px;
   		margin-right: 0px;
		border:1px solid #000;
		voice-family: "\\"}\\"";
		voice-family: inherit;
   		margin-left: 0px;
   		margin-right: 0px;
		}
	html>body #centercontent {
   		margin-left: 0px;
   		margin-right: 0px;
		}

	#rightcontent {
		position: absolute;
		right:10px;
		top:50px;
		width: 10px;
		background:#fff;
		border:1px solid #000;
		}

	#header {
		background:#fff;
		height:39px;
		border-top:1px solid #000;
		border-right:1px solid #000;
		border-left:1px solid #000;
		voice-family: "\\"}\\"";
		voice-family: inherit;
		height:39px;
		}
	html>body #header {
		height:39px;
		}

	p,h1,pre {
		margin:0px 10px 10px 10px;
		}

	h1 {
		font-size:14px;
		padding-top:10px;
		}

	#header h1 {
		font-size:14px;
		padding:10px 10px 0px 10px;
		margin:0px;
		}

</style>
</head><body>

<div id="header"><h1>Category Popularity</h1></div>
<div id="centercontent">
<p>
<form>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<td colspan="4" align="center">

		<select name="country">
EOS

$sth = $db->prepare("
SELECT
DISTINCT(country_code) AS country
FROM
malls
WHERE
country_code IS NOT NULL
");

$sth->execute();

while (my $row = $sth->fetchrow_hashref)
{
	print qq(<option value="$row->{country}");
	print q( selected ) if $country eq $row->{country};
	print qq(>$row->{country}</option>);
}


print <<EOS;
	</select>
	<input type="submit" value="Send">
</td>
</tr>
<tr>
	<td colspan="4" align="center"> &nbsp; </td>
</tr>
EOS

##################################################
# Popularity of categories
##################################################
$sth = $db->prepare("
SELECT
	COUNT(msp.id) AS total_sum,
	vendor_categories_text_list(msp.incentive_mallcat_id) AS mall_category_path
FROM
	member_shopping_preferences AS msp
	JOIN
	members AS m
	ON
	m.id = msp.member_id
	WHERE
	msp.category = 'mallcat'
	AND
	m.country = ?
GROUP BY
	msp.incentive_mallcat_id
ORDER BY
	total_sum DESC
");

$sth->execute($country);

while (my $row = $sth->fetchrow_hashref)
{
	print <<EOT;
		<tr>
			<td style="width:10px;"></td>
			<td>
				$row->{mall_category_path}:
			</td>
			<td style="width:20px:" align="right">
				$row->{total_sum}
			</td>
			<td style="width:10px;"></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2" style="height:1px; background-color:#C0C0C0"></td>
			<td></td>
		</tr>
EOT

}

print "</table><p>&nbsp;</p><p>&nbsp;</p>";
if (!$sth->rows())
{
	print q(No categories are popular. <br />);
}

print <<EOS;
<br />
</p>
</div>

	</body></html>
EOS
