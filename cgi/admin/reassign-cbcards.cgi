#!/usr/bin/perl -w
###### reassign-cbcards.cgi
###### 02/22/14: Dick: Bill - We have quite a few cards that have been returned to us over the years that we need to be able to put back in circulation.
###### Rther than asking you to change the ownership each time, can you create an interface for Pat to use to do this?
###### created 02/22/14	Bill MacArthur

use strict;
use Template;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
require cs_admin;
require ADMIN_DB;

my %TMPL = (
	'good2go' => 0,
	'activated_card_present' => 0
);

my $q = new CGI;
my $ck = $q->cookie('cs_admin');
die "You must be logged into the DB to use this interface" unless $ck;
my $Path = $q->path_info || '';
my $ME = $q->script_name;
$Path =~ s#^/##;

my (%params, @errors) = ();
my $cs_admin = cs_admin->new({'cgi'=>$q});
my $operator = $cs_admin->authenticate($ck);
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator->{'username'}, $operator->{'password'} );
$db->{'RaiseError'} = 1;

my $gs = G_S->new({'cgi'=>$q, 'db'=>$db});

# if we made it this far we must be good, otherwise we would have thrown some sort of errors

LoadParams();

unless ($params{'action'})
{
	DoForm();
}
elsif ($params{'action'} eq 'inspect')
{
	Inspect();
}
elsif ($params{'action'} eq 'update')
{
	Update();
}

Exit();

###### ######

sub DoForm
{
	print
		$q->header,
		$q->start_html('Inspect/Reassign Club Rewards Cards'),
		$q->h4('Inspect/Reassign Club Rewards Cards');
	
	foreach (@errors)
	{
		print $q->div({'style'=>'color:red'}, $_);
	}
	
	print
		$q->start_form('-method'=>'get'),
		$q->hidden('-value'=>'inspect', '-name'=>'action'),
		'Enter the Card range (plain numbers only):<br />Start: ',
		$q->textfield('-name'=>'startNum', '-force'=>1),
		' End: ',
		$q->textfield('-name'=>'endNum', '-force'=>1),
		$q->submit('Inspect'),
		$q->end_form,
		$q->end_html;
		
	Exit();
}

sub DoTT
{
	my ($tmpl, $data) = @_;
	my $tmp = $gs->GetObject($TMPL{$tmpl}) || "Failed to load template: $TMPL{$tmpl}, $tmpl";
	my $TT = Template->new();
	
	print $q->header('-charset'=>'utf-8');
	$TT->process(\$tmp, $data) || print $TT->error;
	
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub Inspect
{
	# put all the cards in the range into a temp table for further analysis
	if ($params{'endNum'} =~ m/\D/ || $params{'startNum'} =~ m/\D/)
	{
		push @errors, 'The card numbers must be strictly numeric... no letters';
	}
	elsif ($params{'endNum'} < $params{'startNum'})
	{
		push @errors, 'The starting number is higher than the ending number';
	}
	
	DoForm() if @errors;
	
	$db->do('
		SELECT id, "class", referral_id, current_member_id, operator, stamp::TIMESTAMP(0) AS stamp, memo INTO TEMP TABLE cbc
		FROM clubbucks_master WHERE id BETWEEN ? AND ?', undef, $params{'startNum'}, $params{'endNum'});

	my $data = {};
	
	# ensure that there are no active cards in the bunch and if there are bail out with a print out of the batch
	my $data->{'active_cards'} = $gs->select_array_of_hashrefs('SELECT * FROM cbc WHERE current_member_id IS NOT NULL');
	DoTT('activated_card_present', $data) if $data->{'active_cards'};
	
	$data->{'num_of_cards'} = $db->selectrow_array('SELECT COUNT(*) FROM cbc');
	
	# determine if we are dealing with 1 batch or more than 1
	# if 1, then determine if we are dealing with the whole batch... why bother reinitializing part when we may have to do the entire thing at some point anyway
	$data->{'distinct_memos'} = [ $gs->selectall_array('SELECT DISTINCT memo FROM cbc') ];
	$data->{'total_cards_in_batch'} = $db->selectrow_array("SELECT COUNT(*) FROM clubbucks_master WHERE memo= ${$data->{'distinct_memos'}}->[0]") if scalar @{$data->{'distinct_memos'}} == 1;
	
	$data->{'distinct_rows'} = $gs->select_array_of_hashrefs('SELECT DISTINCT "class", referral_id, stamp, memo FROM cbc');
	
	DoTT('good2go', $data);
}

sub LoadParams
{
	foreach (qw/action startNum endNum/)
	{
		$params{$_} = $gs->PreProcess_Input($q->param($_));
	}
}

