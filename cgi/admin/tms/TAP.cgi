#!/usr/bin/perl -w
###### TAP.cgi
###### 09/23/2002	Stephen Martin
###### last modified 11/20/15	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);

use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
use G_S qw( %MEMBER_TYPES );
require AOE;

binmode STDOUT, ":encoding(utf8)";

###### Globals
my ($db, $qry, $sth, $rv, $data, $HTML);
my (@cookie, $TEXT);
my $q = new CGI;

unless ( $q->https )
{
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
}

my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd = $q->cookie('pwd') || $q->param('pwd');

my $transaction_ref  = $q->param('transaction_ref');
my $action           = $q->param('action');
my $self             = $q->script_name();
my $class = 'lbs1';

$transaction_ref =~ s/\D//g;

if (!(defined $operator))
{
	print $q->header(), $q->start_html();
	print $q->h3("Incorrect login.<br>Back up and try again"), $q->end_html(); 
	exit;
}

unless ( $transaction_ref )
{
	Err("transaction_ref is missing??");
	exit;
}

$db = ADMIN_DB::DB_Connect( 'trans-index.cgi', $operator, $pwd ) || die "Failed to connect to the DB.";

$db->{'RaiseError'} = 1;

$sth = $db->prepare("SELECT get_working_period(1, NOW())");
$sth->execute;
my $CurrentPeriod = $sth->fetchrow_array;
$sth->finish;

###### Assign some HEADER HTML

$HTML = <<EOHTML1;
<tr>
 <td class="lbsh">ID</td>
 <td nowrap="nowrap" class="lbsh">Member Details</td>
 <td nowrap="nowrap" class="lbsh">Transaction Type</td>
 <td nowrap="nowrap" class="lbsh">Club Cash (USD)</td>
 <td nowrap="nowrap" class="lbsh">Commissions</td>
 <td nowrap="nowrap" class="lbsh">NPP</td>
 <td nowrap="nowrap" class="lbsh">PPP</td>
 <td nowrap="nowrap" class="lbsh">RPP</td>
 <td nowrap="nowrap" class="lbsh">QPP</td>
 <td nowrap="nowrap" class="lbsh">FPP</td>
 <td nowrap="nowrap" class="lbsh">PLPP</td>
 <td nowrap="nowrap" class="lbsh">Pay Status</td>
 <td nowrap="nowrap" class="lbsh">Commission Period</td>
 <!--td nowrap class="lbsh">CV Spread?</td-->
</tr>
EOHTML1

###### Run the query to extract all the required apportionment info 

$qry = "SELECT 
	m.id,
        m.firstname || ' ' || m.lastname AS name,
        m.membertype,
        a.rebate, 
        t.description,
        a.commission, 
        a.ppp, 
        a.npp,
	a.rpp,
	a.qpp,
	a.fpp,
	a.plpp,
	p.period,
        p.open,
	p.close
/*        CASE WHEN a.cv_spread = TRUE THEN 'Yes'
         ELSE 'No'
        END 
           AS cv_spread */
        FROM 
  periods p, ta_live a
  LEFT OUTER JOIN members m ON a.id = m.id 
  LEFT OUTER JOIN transaction_types t ON a.transaction_type = t.trans_type
WHERE 
  a.transaction_ref = ?
AND p.period = a.period
ORDER BY 
  a.pk";

$sth = $db->prepare($qry);

$rv = $sth->execute($transaction_ref);
defined $rv or die $sth->errstr;

while( $data = $sth->fetchrow_hashref() )
{
	$class= ($class eq 'lbs1') ? 'lbs2' : 'lbs1';

	$data->{paid} = ($CurrentPeriod == $data->{period}) ? 'Open' : 'Closed';
	$data->{commission_month} = "$data->{open} - " . ($data->{close} || '');

	$HTML .= <<EOHTML2;
<tr>
 <td class="$class">$data->{id}</td>
 <td nowrap="nowrap" class="$class">$data->{name}&nbsp;$G_S::MEMBER_TYPES{$data->{membertype}}</td>
 <td nowrap="nowrap" class="$class">$data->{description}</td>
 <td nowrap="nowrap" class="$class">${\($data->{rebate} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{commission} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{npp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{ppp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{rpp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{qpp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{fpp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">${\($data->{plpp} || '&nbsp;')}</td>
 <td nowrap="nowrap" class="$class">$data->{paid}</td>
 <td nowrap="nowrap" class="$class">$data->{commission_month}</td>
 <!--td nowrap="nowrap" class="$class">$data->{cv_spread}</td-->
</tr>
EOHTML2

}

$sth->finish;

###### Print a message if no results are returned

if ( ! $rv ) 
{
 $HTML = $HTML . <<EOHTML3;
<tr>
 <td nowrap colspan="9"
     class="lbs1"
     align="center">There are no apportionment details for Transaction $transaction_ref
 </td>
</tr>
EOHTML3
}  
###### Load the Template showing the apportionment

my $TMPL = G_S::Get_Object($db, 10026) || die "Couldn't retrieve the template object: 10026";

$TMPL =~ s/%%self%%/$self/g;
$TMPL =~ s/%%HTML%%/$HTML/g;

print $q->header(-charset=>'utf-8');

print $TMPL;

$db->disconnect;

exit;

sub Err
{
	my ( $msg ) = @_;
	print $q->header(), $q->start_html();
	print qq~
	<center>
	<h1>$msg</h1>
	</center>
\n~;
	print $q->end_html();
}

###### 09/26/02 corrected column name spelling in the outputting variable for commission
###### 09/30/03 revised to use the new apportionment format
###### also cleaned up a bunch of uninitialized var errors after turning on the -w switch
# 01/24/12 removed CV Spread
# 04/16/12 added rpp qpp plpp and the real fpp instead of the rename of npp
