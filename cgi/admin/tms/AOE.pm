package AOE;

use Exporter;
@ISA = ('Exporter');

@EXPORT  = qw($transactions_table  $transactions_sequence $vendors_table $apportionment_table);

# Globally exported variables

$transactions_table	=	"transactions";
$transactions_sequence 	=	"transactions_trans_id_seq";
$vendors_table		=	"vendors";
$apportionment_table	=	"transactions_apportionment";
1;


