#!/usr/bin/perl -w
###### AOE.cgi
###### 09/17/2002	Stephen Martin
###### this script takes care of order entry for Mall purchases
###### last modified 06/11/08	Bill MacArthur

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5  qw(md5 md5_hex md5_base64);
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
use G_S qw( %MEMBER_TYPES );

use AOE;

###### Globals
my ($db, $qry, $sth, $rv, $data, $HTML);
my (@cookie, $TEXT);
my $q = new CGI;
unless ( $q->https )
{
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
	exit;
}

my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd = $q->cookie('pwd') || $q->param('pwd');

my $affiliate_code = $q->param('affiliate_code');
my $vendor  	     = $q->param('vendor') || '';
my $vendor_untainted = $vendor;
my $amount 	     = $q->param('amount');
$amount = '' unless defined $amount;
my $amount_untainted = $amount;
my $alias	     = $q->param('alias');
my $alias_untainted  = $alias;
my $action           = $q->param('action') || '';
my $transaction_date = $q->param('transaction_date');
my $discount         = $q->param('discount');
my $notes            = $q->param('notes');
my $self             = $q->script_name();
my $o		     = $q->param('o');
my $commission = $q->param('commission');
my $id;
my $spid;
my $payout;
my $member;
my $membertype;
my $vendor_info;
my $trans_type;
my $discount_amount;
my $YEAR;
my $MON;
my $DAY;
my $js1;
#my $js2;
my $js3;
my $note_tag;
#my $transaction_hash;
my $sys_message;
my @dup_trans_id;
my $dup_trans_id;
my $des;
my $des_in;
my $db_row;

my $cv_mult;
my $reb_mult;
my $comm_mult;

my %MEMBERS = %MEMBER_TYPES;

##### 1st step calculate and encrypted MD5 of the remote IP #####

$des = md5_hex($ENV{'REMOTE_ADDR'});

if (!(defined $operator))
{
	print $q->header(), $q->start_html(),
	$q->h3("Incorrect login.<br />Back up and try again"), $q->end_html(); 
	exit;
}

unless ( $db = ADMIN_DB::DB_Connect( 'trans-index.cgi', $operator, $pwd ))
{
	$db->{RaiseError} = 1;
	exit;
}
###### ###### Add Transaction block ###### ######
if ( $action eq " Add Transaction " )
{
	###### how many times are we going to execute this transaction
	my $how_many_times = $q->param('how_many_times') || 1;
	my @trans_id_list = ();

 # In this block retreive id from a hidden field

 $id         = $q->param('id');
 $spid       =  $q->param('spid');
 $membertype = $q->param('membertype');
 $trans_type = $q->param('trans_type');
 $notes      = $q->param('notes');
 
 $cv_mult    = $q->param('cv_mult');
 $cv_mult    = sprintf( "%.2f",$cv_mult);

 $reb_mult   = $q->param('reb_mult');
 $reb_mult   =  sprintf( "%.2f",$reb_mult);

 $comm_mult  = $q->param('comm_mult');
 $comm_mult  =  sprintf( "%.2f",$comm_mult);

 unless ($trans_type ) { $trans_type = "2"; }
 
 $discount_amount = $q->param('discount_amount');
 $discount_amount = sprintf( "%.2f",$discount_amount);

 # remove any leading dollar signs
 $amount =~ s/^\$//;
 $discount =~ s/^$//;

 # remove any non digits save for -,+,.
 $amount =~ s/(?![\-\+\.])\W//g;

 # Look to see if we have a percent value 
 if ( $discount =~ /\%/ )
 {
	$discount  =~ s/(?![\-\+\.])\W//g;
	unless ( $discount == 0 )
	{
		# Change percentage to a decimal format.
		$discount = $discount / 100;
	}			
 } else {
	$discount  =~ s/(?![\-\+\.])\W//g;
 }	

 unless ( $vendor && $id && (length $amount > 0) && $spid && $membertype && $trans_type)
 {
	Err("You are missing a form input.");
	goto 'END';
 }

	###### we need the rebate, reb_com and pp col values and since they were not originally
	###### programmed into the template, we'll have to gather them explicitly
	$sth = $db->prepare("SELECT rebate,
					reb_com,
					pp,
					cap
				FROM vendors
				WHERE vendor_id= $vendor");
	$sth->execute;
	my $vh = $sth->fetchrow_hashref;
	$sth->finish;

	###### keep out initialized var errors
	foreach (keys %{$vh})
	{
		unless ( defined $vh->{$_} )
		{
			$vh->{$_} = 'NULL';
		}
		else
		{
			$vh->{$_} = "'$vh->{$_}'";
		}
	}

	die "Failed to gather extra vendor details." unless $vh;

	$qry = "INSERT INTO $AOE::transactions_table (
			currency_code,
			receivable,
			native_receivable_amount,
			receivable_currency_code,
			affiliate_code,
			trans_id,
			id, 
			spid,
			membertype,
			trans_type,
			vendor_id,
			amount,
			native_currency_amount,
			discount,
			description,
			trans_date,
			discount_amt,
			cv_mult,
			reb_mult,
			comm_mult,
			rebate,
			reb_com,
			pp,
			cap
		 ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                   ?, ?, ?, ?, ?,
                                   ?, ?, ?,
			$vh->{rebate},
			$vh->{reb_com},
			$vh->{pp},
			$vh->{cap})";

 $sth = $db->prepare($qry);

foreach (my $x=0; $x < $how_many_times; $x++)
{
	my ($trans_id) = $db->selectrow_array("
	SELECT NEXTVAL(\'\"$AOE::transactions_sequence\"\'::text)");

	$rv = $sth->execute(
			$q->param('amount_cc'),
			$commission,
			$q->param('native_commission'),
			$q->param('commission_cc'),
			$affiliate_code,
			$trans_id,
			$id,
			$spid,
			$membertype,
			$trans_type,
			$vendor, 
			$amount,
			$q->param('native_amount'),
			$discount,
			$notes,
			$transaction_date,
			$discount_amount,
			$cv_mult,
			$reb_mult,
			$comm_mult);
			
	defined $rv or Err($sth->errstr);
	push (@trans_id_list, $trans_id);
}

	###### create a value to pass as an argument
	my $list = '';
	$list .= "$_," foreach @trans_id_list;
	chop $list;

#	print $q->redirect($q->url . '?action=new;affiliate_code=$affiliate_code;LR=' . $list);
	InitialPage($list);
#goto 'END';
}
###### ###### Create Order Section ###### ######
elsif ( $action eq " Create Order Entry " ){ Create_Order_Entry(); }

###### ###### Initial Page Load ###### ######
else { InitialPage(); }

END:	
$db->disconnect;

exit;

###### BEGINNING OF SUBROUTINES		

sub Create_Order_Entry
{
	unless ( $vendor && $alias && $affiliate_code && length $amount > 0)
	{
		Err("You are missing a form input.");
		goto 'END';
	}
 
 # Untaint our input to remove things we dont want..

 # An alias or ID can be input an id is an alias minus the two letter prefix.
 # but there shouldn't be any spaces
	$alias =~ s/\s*//g;

 # remove any dollar signs or spaces
	$amount =~ s/\$*|\s//g;
 
 # remove any non digits save for -,+,.
	$amount =~ s/(?![\-\+\.])\W//g;

 # remove any dollar signs or spaces
	$commission =~ s/\$*|\s//g;
 
 # remove any non digits save for -,+,.
	$commission =~ s/(?![\-\+\.])\W//g;

 # Retreive data about our member..

	$qry = "SELECT id,
			alias,
			spid,
			firstname,
			lastname,
			membertype,
			emailaddress
		FROM members WHERE ";

	$qry .= ($alias =~ /\D/) ? 'alias= ?' :	'id = ?';

	$sth = $db->prepare($qry);
	$rv = $sth->execute(uc $alias);
	defined $rv or die $sth->errstr;
 
	$member = $sth->fetchrow_hashref;
	$sth->finish();

	unless ( $member->{id} ) 
	{
		Err("That was not a valid ID.");
		goto 'END';
	}

	$id = $member->{id};
	
###### This section flags any duplicate transactions unless
###### the override has been set and verified

# Added 'or blank' to alleviate uninitialized errors.
	$des_in = $o || '';

# Retreive vendor information
	$qry = "SELECT COALESCE(payout, 0) AS np, v.*
		FROM $AOE::vendors_table v
		WHERE v.vendor_id = ?";

	$sth = $db->prepare($qry);
	$rv = $sth->execute($vendor);
	defined $rv or die $sth->errstr;

	$vendor_info = $sth->fetchrow_hashref;
	$sth->finish();
 
	unless ( $vendor_info->{vendor_id} ) 
	{
		Err("Vendor Information not retreived for vendor_id: <b>$vendor</b>.");
		goto 'END';
	}
	$vendor_info->{vendor_name} = CGI::escapeHTML($vendor_info->{vendor_name});

	###### make sure the vendor and the affiliate are linked
	my ($ck, $names) = ckAffiliateCode($vendor_info->{vendor_id});
	unless ($names)
	{
		Err("The vendor: <b>$vendor_info->{vendor_name}</b>, is not linked to any Affiliate Programs<br />
			The vendor will have to be linked before proceeding with this transaction.");
		goto 'END';
	}
	elsif (! $ck)
	{
		my ($name) = $db->selectrow_array("SELECT name FROM linkshare_affiliates WHERE code = ?", undef, $affiliate_code);
		Err("The vendor: <b>$vendor_info->{vendor_name}</b>, is not linked to the selected Affiliate Program: <b>$name</b><br />
			It is linked to these Affiliates: <b>$names</b><br />
			If you have selected the wrong Affiliate Program, then back up and change your selection.<br />
			If your Affiliate selection is correct, then this vendor will have to be linked to the Affiliate before proceeding with this transaction.");
		goto 'END';
	}
	
	###### convert non USD entries to USD
	my $amount_cc = $q->param('amount_cc') || 'USD';
	my $native_amount = $amount;
	if ($amount_cc ne 'USD')
	{
		($amount) = $db->selectrow_array('SELECT calculate_usd_from_native_currency(?,?,?)', undef,
			$amount, $amount_cc, $transaction_date);
	}

	my $commission_cc = $q->param('commission_cc') || 'USD';
	my $native_commission = $commission;
	if ($commission_cc ne 'USD')
	{
		($commission) = $db->selectrow_array('SELECT calculate_usd_from_native_currency(?,?,?)', undef,
			$commission, $commission_cc, $transaction_date);
	}
 # Load the Order Template

	my $TMPL = G_S::Get_Object($db, 10023) || die "Couldn't retrieve the template object: 10023.";
	


 # Populate form 

#	my $THIS_YEAR = (localtime) [5];
#	$MON       = (localtime) [4];
#	$DAY       = (localtime) [3];
#
#
#	$THIS_YEAR = $THIS_YEAR + 1900;
#	$MON++;
#
#	$MON = sprintf( "%0.2d", $MON);
#	$DAY = sprintf( "%0.2d", $DAY);
#
#	my $DATESTAMP = $THIS_YEAR . "-" . $MON . "-" . $DAY;

	my $vendor_menu = $vendor_info->{vendor_name} . $q->hidden('vendor');

###### since we are going to be calculating off the commission, we don't care what the vendor record says
###### unless is is Outlet Center :)
# 	if ( $vendor_info->{np} == 0 && $vendor_info->{vendor_group} == 3 )
# 	{
# 		$payout = "";  
# 		$js1=<<EOJS1;
# 		<script type="text/javascript">alert("Please enter a fixed amount for the discount");
# 		//      document.forms.aoe.discount.value = '0';
# 		      document.forms.aoe.discount_amount.value = '';
# 		      document.forms.aoe.discount_amount.focus();
# 		</script>
# EOJS1
# 		$js3 = "var pc = false;";
# 	}
#   else 
# 	{
# 	###### this is actually the payout percentage as entered in the DB
	###### in fact, it is for display purposes only as it is not used except for Outlet Center stuff
		$payout = $vendor_info->{'payout'};
		$payout = $payout * 100;
		$payout = sprintf( "%.2f", $payout );
		$payout = $payout . "\%";
		$js1="";
		$js3="var pc = true;";
#	}

	my $disc_amt = 0;
	###### for regular mall sales, we payout 2/3 of our commission
	if ($vendor_info->{vendor_group} == 2){
#		$disc_amt = sprintf('%.2f', (($commission / 3) * 2));
# let's make it easy on ourselves... and also mimic the automated download system
		$disc_amt = sprintf('%.2f', ($commission * .8));
	}
	###### for outlet center sales we have them enter the 
	elsif ($vendor_info->{vendor_group} == 3) {
		$disc_amt = sprintf('%.2f', ($vendor_info->{payout} * $amount));
	}

 print $q->header(-charset=>'utf-8');

# $TMPL =~ s/%%transaction_date%%/$DATESTAMP/g;
 $TMPL =~ s/%%transaction_date%%/$transaction_date/g;
 $TMPL =~ s/%%memberid%%/$member->{alias}/g;
 $TMPL =~ s/%%spid%%/$member->{spid}/g;
 $TMPL =~ s/%%hiddenid%%/$member->{id}/g;
 $TMPL =~ s/%%hiddenmembertype%%/$member->{membertype}/g;
 $TMPL =~ s/%%trans_type%%/$vendor_info->{'vendor_group'}/g;
 $TMPL =~ s/%%firstname%%/$member->{firstname}/g;
 $TMPL =~ s/%%lastname%%/$member->{lastname}/g;
 $TMPL =~ s/%%membertype%%/$MEMBERS{$member->{membertype}}/g;
 $TMPL =~ s/%%emailaddress%%/$member->{emailaddress}/g;
 $TMPL =~ s/%%vendor_menu_selected%%/$vendor_menu/;
 $TMPL =~ s/%%vendor_group%%/$vendor_info->{vendor_group}/;
 $TMPL =~ s/%%payout%%/$payout/;
 $TMPL =~ s/%%amount%%/sprintf('%.2f', $amount)/e;
 $TMPL =~ s/%%native_amount%%/sprintf('%.2f', $native_amount)/e;
 $TMPL =~ s/%%discount_amount%%/$disc_amt/;
 $TMPL =~ s/%%commission%%/sprintf('%.2f', $commission)/e;
 $TMPL =~ s/%%native_commission%%/sprintf('%.2f', $native_commission)/e;
 $TMPL =~ s/%%affiliate_code%%/$affiliate_code/;
 $TMPL =~ s/%%js1%%/$js1/g;
 $TMPL =~ s/%%js3%%/$js3/g;
 $TMPL =~ s/%%self%%/$self/g;
 $TMPL =~ s/%%cv_mult%%/$vendor_info->{cv_mult}/g;
 $TMPL =~ s/%%reb_mult%%/$vendor_info->{reb_mult}/g;
 $TMPL =~ s/%%comm_mult%%/$vendor_info->{comm_mult}/g;
 $TMPL =~ s/%%amount_cc%%/$amount_cc/g;
 $TMPL =~ s/%%commission_cc%%/$commission_cc/g;

 print $TMPL;

}

sub cboAffiliates
{
	###### generate a drop-down list of affiliates
	###### seed our list with an instruction and a 'false' value that will still allow error trapping to work
	my %lbl = (0=>'Select the Affiliate');
	my @vals = 0;
	my $sth = $db->prepare("SELECT name, code FROM linkshare_affiliates WHERE active=TRUE ORDER BY name");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref){
		push @vals, $tmp->{code};
		$lbl{$tmp->{code}} = $tmp->{name};
	}
	###### make the default affiliate be the last one selected
	###### this will default to the instruction item when no code is present
#		-default=> $affiliate_code || 0,
	return $q->popup_menu(
		-name=>'affiliate_code',
		-values=>\@vals,
		-labels=>\%lbl
	);
}

sub ckAffiliateCode
{
	my $vid = shift;
	my $sth = $db->prepare("
		SELECT lav.affiliate_code, la.name
		FROM linkshare_affiliates_2vendors lav, linkshare_affiliates la
		WHERE lav.affiliate_code = la.code
		AND lav.vendor_id = $vid");
	$sth->execute;
	my ($match, $names) = ();
	while (my $tmp = $sth->fetchrow_hashref){
		$names .= " $tmp->{name},";
		$match = 1 if $affiliate_code eq $tmp->{affiliate_code};
	}
	chop $names;
	return ($match, $names);
}

sub Err
{
	print $q->header(-charset=>'utf-8'), $q->start_html(), $q->p($_[0]), $q->end_html();
}

sub getnum
{
    use POSIX qw(strtod);
    my $str = shift;
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    $! = 0;
    my($num, $unparsed) = strtod($str);
    if (($str eq '') || ($unparsed != 0) || $!)
	{
		return;
	} else {
		return $num;
	}
}

sub gen_menu
{
    my ( $menu_name, $table_name, $index_name, $desc_name ) = @_;

    my ($dex, $desc, $status) = ();
#    my $sflag;
#    my $seen = 0;

	$qry = "SELECT $index_name, $desc_name, status
		FROM $table_name
		WHERE vendor_group IN (1,2,3,4,7,8,14)
	--	AND status=1
		AND status IN (0,1)
		ORDER BY vendor_name";

  $sth = $db->prepare($qry);
 
  $rv = $sth->execute();
  defined $rv or die $sth->errstr;

  $sth->bind_columns(undef, \$dex,\$desc,\$status);

#   $html = qq|<select name="$menu_name" style="width: 350px; font-family: Arial, Helvetica, sans-serif; font-size: 10px;" 
#             onChange="return _validate();document.forms.aoe.submit();"
#             size="10">\n|;
	my $html = qq|<select name="$menu_name" id="vendors">\n|;
	$html .= '<option value="">Select a Vendor</option>';
	my ($active, $inactive) = ();
#	$active = '<option value="">Select a Vendor</option>';
  while   ($sth->fetch() )
  {
	my $opt .= qq|<option value="$dex"|;
	$opt .= qq| selected="selected"| if $dex eq $vendor;
#	$html .= qq| class="inactive"| if $status != 1;
	$opt .= '>' . CGI::escapeHTML($desc). "</option>\n";
	$status != 1 ? ($inactive .= $opt) : ($active .= $opt);
  }

  $rv = $sth->finish();

	$html .= qq|<optgroup label="Active Vendors" id="og_active">$active</optgroup>|;
	$html .= qq|<optgroup label="Inactive Vendors" id="og_inactive">$inactive</optgroup>|;
	$html .= "</select>\n";

  return $html;
}

sub duplicate
{
   my ($id) = @_;
   shift;
   my ($vendor ) = @_;
   shift;
   my ($amount ) = @_;

   my @stack;
   my $ary_ref = \@stack;

   my %hash;

   my $qry = "SELECT 
               $AOE::transactions_table.trans_id, 
               transaction_types.description  as trans_type_desc, 
               $AOE::vendors_table.vendor_name, 
               $AOE::transactions_table.amount, 
               $AOE::transactions_table.discount, 
               $AOE::transactions_table.description, 
               $AOE::transactions_table.trans_date, 
               $AOE::transactions_table.entered, 
               $AOE::transactions_table.discount_amt
              FROM  $AOE::transactions_table
               LEFT OUTER JOIN transaction_types ON $AOE::transactions_table.trans_type = transaction_types.trans_type
               LEFT OUTER JOIN $AOE::vendors_table ON $AOE::transactions_table.vendor_id = $AOE::vendors_table.vendor_id
               WHERE
                ( $AOE::transactions_table.id        = ?  )   AND
                ( $AOE::transactions_table.vendor_id = ?  )   AND 
                ( $AOE::transactions_table.amount    = ?  )   AND
                ( now() - $AOE::transactions_table.trans_date  < \'30 days\')";

  $sth = $db->prepare($qry);
  
  $rv = $sth->execute($id, $vendor, $amount);
  defined $rv or die $sth->errstr;

  $ary_ref  = $sth->fetchall_arrayref();


  $rv = $sth->finish();

  return ( $ary_ref );

}

sub dump_duplicates
{

    my @ROWS;
    my @data;

    my ($data) = @_;
    shift;

    my ($id)   = @_;

    my $member_info;
    my $TRANS_HTML_TABLE;
    my $HTML2;

    my $class;
    my $rowc;

    $member_info = getmemberdetails($id); 

   @ROWS = @$data;

    $TRANS_HTML_TABLE =<<EOHTML1;
<tr>
 <td nowrap class="lbsh">&nbsp</td>
 <td nowrap class="lbsh">Transaction ID</td>
 <td nowrap class="lbsh">Transaction Type</td>
 <td nowrap class="lbsh">Vendor Name</td>
 <td nowrap class="lbsh">Amount</td>
 <td nowrap class="lbsh">Payout</td>
 <td nowrap class="lbsh">Discount</td>
 <td nowrap class="lbsh">Description</td>
 <td nowrap class="lbsh">Transaction Date</td>
 <td nowrap class="lbsh">Transaction Entered</td>
 <td nowrap class="lbsh">&nbsp</td>
</tr>
EOHTML1
    
   $rowc=0;

 foreach my $row (@ROWS)
   {
    
   if ( $rowc % 2 == 0 )
   {
    $class="class=\"lbs1\"";
   } else {
    $class="class=\"lbs2\"";
   }

   @$row[3] = sprintf( "%.2f",@$row[3]);

   if ( @$row[4] == 0 )
   {
    @$row[4] = "Flat Rate";
   } else {
    @$row[4] = @$row[4] * 100;
    @$row[4] = sprintf( "%.2f",@$row[4]);
    @$row[4] = @$row[4] . "\%";
   }


   @$row[5] = substr(@$row[5],0,15);

   $TRANS_HTML_TABLE = $TRANS_HTML_TABLE . <<EOHTML2;
<tr>
 <td nowrap $class>&nbsp;</td>
 <td nowrap $class>@$row[0]</td>
 <td nowrap $class>@$row[1]</td>
 <td nowrap $class>@$row[2]</td>
 <td nowrap $class>@$row[3]</td>
 <td nowrap $class>@$row[4]</td>
 <td nowrap $class>\$@$row[8]</td>
 <td nowrap $class>@$row[5]</td>
 <td nowrap $class>@$row[6]</td>
 <td nowrap $class>@$row[7]</td>
 <td nowrap $class>&nbsp;</td>
</tr>
EOHTML2

  $rowc++;
  }

$HTML2=<<EOHTML1;
<tr>
 <td class="lbsh">&nbsp</td>
 <td class="lbsh">ID #</td>
 <td class="lbsh">SPID #</td>
 <td class="lbsh">Member Name</td>
 <td class="lbsh">Member Type</td>
 <td class="lbsh">Email</td>
 <td class="lbsh">&nbsp</td>
</tr>
<tr>
 <td class="lbs1">&nbsp</td>
 <td class="lbs2">$member_info->{id}</td>
 <td class="lbs1">$member_info->{spid}</td>
 <td class="lbs2">$member_info->{prefix}&nbsp;$member_info->{firstname}&nbsp;$member_info->{lastname}</td>
 <td class="lbs1">$MEMBERS{$member_info->{membertype}}</td>
 <td class="lbs2">$member_info->{emailaddress}</td>
 <td class="lbs1">&nbsp</td>
</tr>
EOHTML1

if ( $rowc == 0 )
{
 $TRANS_HTML_TABLE =<<EOHTML3;
 <td nowrap class="lbs2">There are no transactions</td>
EOHTML3
 $HTML2 = "<tr><td></td></tr>";
}


###### Load the Template To show the results
my $TMPL = G_S::Get_Object($db, 10027);
	unless ($TMPL)
	{
		Err("Couldn't retrieve the template object.");
		exit;
	}

$TMPL =~ s/%%self%%/$self/g;
$TMPL =~ s/%%HTML1%%/$TRANS_HTML_TABLE/g;
$TMPL =~ s/%%HTML2%%/$HTML2/g;
$TMPL =~ s/%%alias%%/$alias_untainted/g;
$TMPL =~ s/%%vendor%%/$vendor_untainted/g;
$TMPL =~ s/%%amount%%/$amount_untainted/g;
$TMPL =~ s/%%des%%/$des/g;

print $q->header(-charset=>'utf-8');

print $TMPL;

$rv = $sth->finish();

}

sub getmemberdetails
{
 my ($id) = @_;
 my ($sth, $qry, $data, $rv);    ###### database variables

    $qry = "	SELECT *, 
			COALESCE(prefix,\'\') || \' '\ || firstname || \' \' ||  lastname AS memname
		FROM members
		WHERE id =  ?";

 $sth = $db->prepare($qry);

 $rv = $sth->execute($id);

 $data = $sth->fetchrow_hashref;
 $rv = $sth->finish();

 return $data;
}

sub InitialPage
{
	###### last record/s inserted will come in as an arg
	my $LR = shift;
	$sys_message ="";
	
	if ( $LR ) 
	{
		###### we may receive a 'list'
	 $sys_message = "Transaction Number/s $LR inserted successfully.";
	}
	 
	my $TMPL = G_S::Get_Object($db, 10022) || die "Couldn't retrieve the template object: 10022";
	
	###### display Existing Vendors
	
	$TMPL =~ s/%%self%%/$self/g;
	$TMPL =~ s/%%message%%/$sys_message/g;
	$TMPL =~ s/%%affiliates%%/cboAffiliates()/e;
	$TMPL =~ s/%%vendor_menu%%/gen_menu("vendor",$AOE::vendors_table,"vendor_id","vendor_name")/e;
	$TMPL =~ s/%%transaction_date%%/$db->selectrow_array('SELECT NOW()::DATE -1')/e;
	print $q->header(), $TMPL;
}

sub is_numeric { defined scalar getnum(shift) }


##### 09/27/02 linked the membertypes hash to the global var
##### 10/01/02 Potential Duplicate Entry Search Added           S.Martin
##### 01/08/03 Void statement added 
###### 06/05/03 fixed some INTERVAL problems that arose due to the DB upgrade
###### 08/14/03 cleaned up the code for better readability in some areas
###### also removed reference to the partner date in the getmemberdetail routine
###### 09/05/03 revised to exclude mall-type vendors from the list (the other types use a different payout structure)
###### also did some more cleaning up
###### 09/08/03 added carrying of the rebate, reb_com, pp and cap from the vendor record
###### 10/02/03 just added a description so we could tell what this script does at a glance
###### 11/21/03 Modifications to suppress errors to the log:
######		Changed == to eq in 'unless ( $des_in eq $des )' near line 315.
###### 	Added 'or blank' to $des_in = $o || '' near line 313.
######		Added $vendor_id = $vendor_id || '', $payout = $payout || '',
######		and $vendor_group = $vendor_group || '' near line 600.
######		Modified the structure of 'if ( $discount =~ /\%/ )' by moving the test for
######		$discount != 0 down into the if structure and changing it to 
######		'unless ($discount == 0)' near line 130.
###### 12/15/03 added the vendor group '8' to the menu list for entering CA subscriptions
###### 06/28/06 revised param checking logic to allow a zero value in the amount
###### 12/15/06 added handling for multiple transaction inserts per 'action'
###### 04/07 added many extra functionalities - major template revisions as well
###### old templates are not forward compatible
# 04/24/07 imposed a sort order on the affiliate list and also specified a status criteria for the vendor list
###### 09/18/07 changed the way the discount amount is calculated
###### 06/11/08 a number of minor changes have been made recently