#!/usr/bin/perl -w
###### rewards_cards_order_entry.cgi
###### 01/11/12	Bill MacArthur
###### last modified: 12/02/14	Bill MacArthur

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
use G_S qw( %MEMBER_TYPES );

###### Globals
my ($output, $error, $alert) = ();
my $q = new CGI;

unless ( $q->https )
{
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
}

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

my $action = $q->param('action');
my $self = $q->script_name();

unless ($operator && $pwd)
{
	print $q->header(), $q->start_html();
	print $q->h3("Incorrect login.<br />Back up and try again"), $q->end_html(); 
	exit;
}

my $db = ADMIN_DB::DB_Connect( 'trans-index.cgi', $operator, $pwd ) || die "Failed to connect to the DB.";
$db->{'RaiseError'} = 1;

# if the interface is called without an 'action' param, then we will create a transaction entry screen
unless ($action)
{
	InputInterface();
}
elsif ($action eq 'confirm')
{
	InputInterface() unless ConfirmInterface();
}
elsif ($action eq 'doit')
{
	CreateTransaction();
	InputInterface();
}
else
{
	die "Unexpected action type: $action";
}

print $q->header('-charset'=>'utf-8');
print $q->start_html(
	'-encoding'=>'utf-8',
	'-title'=>'Club Rewards Cards Transaction Entry',
	'-style'=>{'-code'=> '
		body {font-size:larger;}
		.g {font-weight:bold; color: green;}
		.tfe {font-weight:bold; color: red;}
		.error {color: red;}'}
);
print $output;
print $q->end_html;

Exit();

sub ConfirmInterface
{
	my $id = $q->param('id');
	my $amount = $q->param('amount') || 0;
	my $pp = $q->param('pp') || 0;
	
	unless ($id)
	{
		$error = 'No ID supplied';
		return undef;
	}
	elsif ((! $amount) && (! $pp))
	{
		$error = 'Did you really want to create a $0 transaction with -0- PP?';
		return undef;
	}

	$id = uc($id);	# in case it is an alias with lower case letters
	my $qry = "
		SELECT id, firstname ||' '|| lastname AS name, alias, admin_membertype_label(membertype) AS mtype
		FROM members WHERE ";
	$qry .= $id =~ m/\D/ ? 'alias=?' : 'id=?';
	my $member = $db->selectrow_hashref($qry, undef, $id);
	unless ($member)
	{
		$error = "A matching membership was not found for: $id";
		return undef;
	}
	
	$output = $q->h4('Confirm this Transaction Entry');
	$output .=
		$q->start_form .
		'<table><tr>' .
		"<td>Purchaser:</td><td>" .
		$q->span({'-class'=>'g'}, "$member->{'name'} - $member->{'alias'} - $member->{'mtype'}") .
		$q->hidden('-name'=>'id', '-force'=>1, '-value'=>$member->{'id'}) .
		'</td></tr>';

	my $class = $amount > 0 ? 'g' : 'tfe';
	$output .=
		'<tr><td>Purchase Amount (USD):</td><td>' .
		$q->hidden('-name'=>'amount') .
		$q->span({'-class'=>$class}, sprintf('%.2f', $amount)) .
		'</td></tr>';

	$class = $pp > 0 ? 'g' : 'tfe';
	$output .=
		'<tr><td>PP Value:</td><td>' .
		$q->hidden('-name'=>'pp') .
		$q->span({'-class'=>$class}, $pp) .
		'</td></tr>';

	$output .=
		'<tr><td>Description:</td><td>' .
		$q->hidden('-name'=>'description') .
		$q->span({'-class'=>$class}, $q->param('description')) .
		'</td></tr><tr><td></td><td>' .
		$q->p( $q->submit('Confirm Transaction') ) .
		'</td></tr></table>' .
		$q->hidden('-name'=>'action', '-value'=>'doit', '-force'=>1) .
		$q->end_form;
	return 1;
}

sub CreateTransaction
{
	my $id = $q->param('id');
	my $amount = $q->param('amount') || 0;
	my $pp = $q->param('pp') || 0;
	my ($transID) = $db->selectrow_array(q|SELECT nextval('transactions_trans_id_seq')|) || die "Failed to obtain a trans_id";
	$db->begin_work;
	$db->do("
		INSERT INTO transactions (trans_id, id, trans_type, vendor_id, amount, description)
		VALUES ($transID, ?, 15, 11719, ?, ?)", undef, $id, $amount, $q->param('description'));
	$db->do("
		INSERT INTO transactions_extensions (trans_id, pp_value)
		VALUES ($transID, ?)", undef, $pp);
	$db->commit;
	# if we did not barf on that last insert, then we are all set
	$alert = "Transaction created: $transID";
}

sub Err
{
	my ( $msg ) = @_;
	print $q->header(), $q->start_html();
	print $q->h3($msg);
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub InputInterface
{
	$output = $q->h4({'-style'=>'margin-bottom:0.5em;'}, 'Club Rewards Cards PP Transaction Entry');
	$output .= $q->p({'-style'=>'font-size:9px; color:brown;'},
		'This interface will create transactions that will only generate FPP (Frontline Pay Points).
		If the purchaser is an Affiliate or Partner, they will get the credit,
		otherwise the credit will flow up to the first upline Affiliate or Partner.');

	$output.= $q->p({'-class'=>'error'}, $error) if $error;
	$output.= $q->p({'-class'=>'g'}, $alert) if $alert;
	$output .=
		$q->start_form .
		'<table><tr>' .
		'<td>Purchaser ID:</td><td>' .
		$q->textfield('-name'=>'id', '-style'=>'margin-right:2em;', '-value'=>'', '-force'=>1) .
		'</td></tr><tr><td>Purchase Amount (USD):</td><td>' .
		$q->textfield('-name'=>'amount', '-style'=>'margin-right:2em;', '-value'=>'', '-force'=>1) .
		'</td></tr><tr><td>PP Value:</td><td>' .
		$q->textfield('-name'=>'pp', '-style'=>'margin-right:2em;', '-value'=>'', '-force'=>1) .
		'</td></tr><tr><td>Description:</td><td>' .
		$q->textfield('-name'=>'description', '-size'=>50, '-value'=>'', '-force'=>1) .
		'</td></tr><td></td><td>' .
		$q->submit('-style'=>'margin-left: 1em;', '-value'=>'Place Order') .
		'</td></tr></table>' .
		$q->hidden('-name'=>'action', '-value'=>'confirm', '-force'=>1) .
		$q->end_form;
}

__END__

12/02/14	Revised the SQL in CreateTransaction() to split the statements into two calls to keep the newer version of DBI happy
