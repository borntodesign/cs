#!/usr/bin/perl -w
###### AOL.cgi
###### created: 09/20/2002	Stephen Martin
###### this is the Mall affiliate order lookup script

###### last modified 11/20/15	Bill MacArthur

use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5  qw(md5 md5_hex md5_base64);
use URI::Escape;
use lib ('/home/httpd/cgi-lib');
use G_S;
use ADMIN_DB;
require AOE;

binmode STDOUT, ":encoding(utf8)";

###### Globals
my $apportionment_table = 'ta_live';
				
my ($db, $qry, $sth, $rv, $data, $HTML);
my (@cookie, $TEXT);
my $q = new CGI;
unless ( $q->https ){
	print $q->redirect('https://www.clubshop.com' . $q->script_name());
	exit;
}
		 
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

my $alias = G_S::PreProcess_Input($q->param('alias')) || '';
my $alias_untainted  = $alias;
my $TID = G_S::PreProcess_Input($q->param('TID')) || '';
my $TID_untainted    = $TID;
   $TID              =~ s/\D//g;
   $TID_untainted    =~ s/\D//g;
my $action           = $q->param('action') || '';
my $self             = $q->script_name();
my $date_type = $q->param('date_type');
my $date_range = $q->param('date_range');

my $member_info;
my $id;
my $transaction_hash;
my $TRANS_HTML_TABLE;
my $HTML2;

my $class;
my $rowc;
my $trans_id;
my $match;
my $redirect;

my $LINK = "./TAP.cgi";

my %MEMBERS = %G_S::MEMBER_TYPES;

unless ( $operator && $pwd )
{
 print $q->header(-charset=>'utf-8'), $q->start_html(),
 $q->h3("Incorrect login.<br />Back up and try again"), $q->end_html(); 
 exit;
}

$db = ADMIN_DB::DB_Connect( 'trans-index.cgi', $operator, $pwd ) || die "Couldn't connect to the DB";
$db->{RaiseError} = 1;

unless ($action)
{
	DoFrames();
}
elsif ($action eq 'search-form')
{
	my $TMPL = G_S::Get_Object($db, 10024) || die "Failed to load template: 10024";
	
	$TMPL =~ s/%%self%%/$self/g;
	$TMPL =~ s/%%id%%/$alias/g;
	$TMPL =~ s/%%tid%%/$TID/g;
	print $q->header(-charset=>'utf-8'), $TMPL;
}
if ( $action eq " VOID " )
{

	unless ( $alias || $TID )
	{
		Err("That was not a valid alias.");
		goto 'END';
	}

	$trans_id = $q->param('trans_id');
	$trans_id =~ s/\D//g;

	unless ( $trans_id )
	{
		Err("No transaction id supplied to VOID function?");
		goto 'END';
	}

	# Scan through the transactions_apportionment looking for any sign of transaction 'x'

	if ( apportionment_scan($trans_id) )
	{
		voiding_error($trans_id);
		goto 'END';
	}      
	else 
	{
		void($trans_id);
	}

	my $new_action = "LO";

	$redirect = "$self " . "\?action=" . $new_action . "\&" . "alias=" . $alias_untainted . "\&" . "TID=" . $TID_untainted;
	$redirect = 'https://www.clubshop.com' . $redirect;
	$redirect =~ s/\s//g;

	print $q->redirect($redirect);
}
elsif ( $action eq " Lookup Order " || $action eq "LO" )
{
	unless ( $alias || $TID ) 
	{
		Err("That was not a valid alias.");
		goto 'END';
	}


	$qry = "SELECT 
			t.trans_id, 
			transaction_types.description AS trans_type_desc, 
			vendors.vendor_name, 
			t.amount, 
			t.discount, 
			COALESCE(t.description, '') AS description,
			t.trans_date, 
			t.entered::TIMESTAMP(0), 
			t.discount_amt,
			t.void,
			t.cv_mult,
			t.reb_mult,
			t.comm_mult,
			CASE WHEN t.receivable IS NOT NULL THEN t.receivable::TEXT
			ELSE ''::TEXT END AS receivable
		FROM 
			transaction_types,
			transactions t
		LEFT JOIN vendors
		ON 	vendors.vendor_id=t.vendor_id
		WHERE
			t.trans_type=transaction_types.trans_type
		";


	if ($alias && $date_range && $date_range ne '*'){
		die "Invalid date_type" unless grep $_ eq $date_type, (qw/entered trans_date/);
		$qry .= "AND $date_type >= (NOW() - INTERVAL " . $db->quote("$date_range days") . ")::DATE\n";
	}

	if ( $alias )
	{
		$alias =~ s/\D//g;
		$id = $alias;
		$member_info = getmemberdetails($id);  
		$qry .= "AND t.id = ? ORDER BY t.trans_date, t.entered";
		$sth = $db->prepare($qry);
		$rv = $sth->execute($id);
	}
	elsif ( $TID )
	{
		$TID =~ s/\D//g;
		$id = get_id($TID);
		$member_info = getmemberdetails($id);
		$qry .= "AND t.trans_id = ?";

		$sth = $db->prepare($qry);
		$rv = $sth->execute($TID);
	}


	$TRANS_HTML_TABLE =<<EOHTML1;
<tr>
 <td class="lbsh">Trans. ID</td>
 <td class="lbsh">Trans. Type</td>
 <td class="lbsh">Vendor Name</td>
 <td class="lbsh">Amount</td>
 <td class="lbsh">Payout</td>
 <td class="lbsh">Recv.</td>
 <td class="lbsh">Discount</td>
 <td class="lbsh">V</td>
 <td class="lbsh">R</td>
 <td class="lbsh">C</td>
 <td class="lbsh">Description</td>
 <td class="lbsh">Trans. Date</td>
 <td class="lbsh">Trans. Entered</td>
</tr>
EOHTML1

	$class = '';

	while ( $transaction_hash = $sth->fetchrow_hashref ) 
	{
		$class = ($class eq 'lbs1') ? 'lbs2' : 'lbs1';

		# Added to alleviate uninitialized errors due to NULL value.
		$transaction_hash->{discount_amt} = $transaction_hash->{discount_amt} || 0;
		$transaction_hash->{discount_amt} = sprintf( "%.2f",$transaction_hash->{discount_amt});

   unless ( $transaction_hash->{discount} )
   {
    $transaction_hash->{discount} = "Flat Rate";
   } else {
    $transaction_hash->{discount} *= 100;
    $transaction_hash->{discount} =  sprintf( "%.2f",$transaction_hash->{discount});
    $transaction_hash->{discount} .= "\%";
   }


   $transaction_hash->{description} = substr($transaction_hash->{description},0,40);
   $transaction_hash->{description} ||= '&nbsp;';

	$TRANS_HTML_TABLE .= $transaction_hash->{void} eq '1' ? '<tr class="strike">' : '<tr class="data">';
   $TRANS_HTML_TABLE .= <<EOHTML2;
 <td class="$class">
	<a href="$LINK?transaction_ref=$transaction_hash->{trans_id}" target="_blank">$transaction_hash->{trans_id}</a></td>
 <td class="$class">$transaction_hash->{trans_type_desc}</td>
 <td class="$class">$transaction_hash->{vendor_name}</td>
 <td class="$class">$transaction_hash->{amount}</td>
 <td class="$class">$transaction_hash->{discount}</td>
 <td class="$class">$transaction_hash->{receivable}</td>
 <td class="$class">$transaction_hash->{discount_amt}</td>
 
 <td class="$class">$transaction_hash->{cv_mult}</td>
 <td class="$class">$transaction_hash->{reb_mult}</td>
 <td class="$class">$transaction_hash->{comm_mult}</td>

 <td class="$class">$transaction_hash->{description}</td>
 <td class="$class">$transaction_hash->{trans_date}</td>
 <td class="$class">$transaction_hash->{entered}</td>
EOHTML2

	###### turn on the 'VOID' button only if it's an option
	unless ($transaction_hash->{void})
	{
		$TRANS_HTML_TABLE .= qq|
 <td>
<form action="" style="display:inline">
 <input type="hidden" name="trans_id" value="$transaction_hash->{trans_id}" />
 <input type="hidden" name="action_stored" value=" Lookup Order " />
 <input type="hidden" name="alias" value="$alias_untainted" />
 <input type="hidden" name="TID" value="$TID_untainted" />
<input type="submit" name="action" value=" VOID " class="tin"
  onClick="return _c('$transaction_hash->{trans_id}', '$transaction_hash->{vendor_name}', '$transaction_hash->{amount}', '$transaction_hash->{trans_date}');">
</form></td>

</tr>
					|;
	}
	else
	{
		$TRANS_HTML_TABLE .= qq|<td class="$class">&nbsp;</td></tr>\n|;
	}
}

	$HTML2=<<EOHTML1;
<tr>
 <td class="lbsh">ID #</td>
 <td class="lbsh">SPID #</td>
 <td class="lbsh">Member Name</td>
 <td class="lbsh">Member Type</td>
 <td class="lbsh">Email</td>
</tr>
<tr>
 <td class="lbs1">$member_info->{id}</td>
 <td class="lbs1">$member_info->{spid}</td>
 <td class="lbs1">$member_info->{memname}</td>
 <td class="lbs1">$MEMBERS{$member_info->{membertype}}</td>
 <td class="lbs1">$member_info->{emailaddress}</td>
</tr>
EOHTML1

unless ( $class )
{
	$TRANS_HTML_TABLE = qq|<td class="lbs2">There are no transactions</td>\n|;
	$HTML2 = "<tr><td></td></tr>";
}


	my $TMPL = G_S::Get_Object($db, 10025) || die "Couldn't retrieve template: 10025.";

	$TMPL =~ s/%%self%%/$self/g;
	$TMPL =~ s/%%HTML1%%/$TRANS_HTML_TABLE/;
	$TMPL =~ s/%%HTML2%%/$HTML2/;

	print $q->header(-charset=>'utf-8'), $TMPL;
}

END:
$db->disconnect;

exit;

###### ###### End of Main ###### ######						

sub DoFrames
{
	my $rpt_url = '/_empty_';
	# we could receive an search parameters but no action parameter
	# in that case, we should direct the search to the report frame right off
	if ($alias){
		$date_type ||= 'entered';
		$date_range ||= '30';
		$rpt_url = "$self?action=LO;alias=$alias;date_type=$date_type;date_range=$date_range";
	}
	
	print $q->header, qq#<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd"><html><head>
<title>Transaction Lookup</title></head>
<frameset rows="120,*">
<frame src="$self?action=search-form;alias=$alias;tid=$TID" />
<frame src="$rpt_url" name="results" />
</frameset>
</html>#;
}

sub Err
{
	print $q->header(-charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff', -encoding=>'utf-8');
	print qq~<h1>$_[0]</h1>\n~;
	print $q->end_html();
}

sub getmemberdetails
{
	my ($id) = @_;

	my $sth = $db->prepare("
		SELECT id,
			spid,
			alias,
			membertype,
			COALESCE(emailaddress, '') AS emailaddress,
            		COALESCE(prefix, '') || ' ' || firstname || ' ' ||  lastname AS memname
            from members where id =  ?"); 

	$sth->execute($id);

 my $data = $sth->fetchrow_hashref;
 $sth->finish();

 return $data;
}


sub get_id
{
   my ($tid) = @_;

   my ($sth, $qry, $data, $rv);    ###### database variables

   $qry = "SELECT id FROM transactions WHERE trans_id = ?";

   $sth = $db->prepare($qry);

   $rv = $sth->execute($tid);

   $data = $sth->fetchrow_hashref;
   $rv = $sth->finish();
   
   return $data->{id};
}

sub apportionment_scan
{
	my ( $trans_id ) = @_;

#	my $qry = "SELECT ta.pk
#		FROM 	$apportionment_table ta,
#			periods p
#		WHERE 	ta.period=p.period
#		AND 	p.close IS NOT NULL
#		AND 	transaction_ref= ?
#		LIMIT 1";
	my $qry = "SELECT ta.pk
		FROM 	$apportionment_table ta,
			periods p
		WHERE 	ta.period=p.period
		AND 	p.frozen= FALSE
		AND 	transaction_ref= ?";
		
	my $sth = $db->prepare($qry);

	my $rv = $sth->execute($trans_id);
	defined $rv or die "Apportionment scan query failed";

	my $data = $sth->fetchrow_array;
	$sth->finish();

	return $data;  
}

sub void
{

	my ( $trans_id ) = @_;

	my $qry = "	UPDATE $AOE::transactions_table
           		SET 	void= TRUE,
           			description= 'Void ' || NOW() || COALESCE(description, ''),
				operator= current_user
           			WHERE trans_id = ?";

	# Now delete all entries for this transaction from transaction_apportionment
# this will be handled by a trigger on the transactions table
#	$qry .= ";\nDELETE FROM $apportionment_table where transaction_ref= ?";

	my $rv = $db->do($qry, undef, ($trans_id));
	defined $rv or die;
}

sub voiding_error
{
	my ( $transaction_ref ) = @_;

	my $hidden;

 ###### Assign some HEADER HTML

	$HTML=<<EOHTML1;
<tr>
 <td class="lbsh">Member Details</td>
 <td class="lbsh">Transaction Type</td>
 <td class="lbsh">Rebate</td>
 <td class="lbsh">Commision</td>
 <td class="lbsh">PPP</td>
 <td class="lbsh">NPP</td>
 <td class="lbsh">Status</td>
 <td class="lbsh">Commission Period</td>
 <!--td class="lbsh">CV Spread?</td-->
</tr>
EOHTML1
###### Run the query to extract all the required apportionment info 

	$qry = "SELECT 
			m.firstname || ' ' || m.lastname AS name,
			m.membertype,
			a.rebate, 
			COALESCE(t.description, '') AS description,
			a.commission, 
			a.ppp, 
			a.npp, 
			CASE WHEN p.close ISNULL THEN 'Open'
			ELSE 'Closed'
			END AS paid, 
			(p.open || ' - ' || COALESCE(p.close::TEXT, '')) AS commission_month
			/*
			CASE WHEN a.cv_spread = true THEN 'Yes'
			ELSE 'No'
			END AS cv_spread
			*/
		FROM 	members m,
			$apportionment_table a,
			transaction_types t,
			periods p
		WHERE 	a.transaction_type = t.trans_type
		AND 	a.id = m.id
		AND 	p.period = a.period
		AND 	a.transaction_ref = ?
		ORDER BY a.pk";

	$sth = $db->prepare($qry);

	$rv = $sth->execute($transaction_ref);
	defined $rv or die $sth->errstr;

	$class = '';

	while( $data = $sth->fetchrow_hashref() )
	{
		$class = ($class eq 'lbs1') ? 'lbs2' : 'lbs1';

		$HTML .= <<EOHTML2;
<tr>
 <td class="$class">$data->{name}&nbsp;$MEMBERS{$data->{membertype}}</td>
 <td class="$class">$data->{description}</td>
 <td class="$class">$data->{rebate}</td>
 <td class="$class">$data->{commission}</td>
 <td class="$class">$data->{ppp}</td>
 <td class="$class">$data->{npp}</td>
 <td class="$class">$data->{paid}</td>
 <td class="$class">$data->{commission_month}</td>
 <!--td class="$class">$data->{cv_spread}</td-->
</tr>
EOHTML2

}

$rv = $sth->finish();

###### Print a message if no results are returned

if ( ! $class ) 
{
 $HTML .= <<EOHTML3;
<tr>
 <td colspan="9" class="lbs1" align="center">
	There are no apportionment details for Transaction $transaction_ref??
 </td>
</tr>
EOHTML3
}  

$hidden =<<EOHTML4;
<input type="hidden" name="action" value="LO" />
<input type="hidden" name="alias" value="$alias_untainted" />
<input type="hidden" name="TID" value="$TID_untainted" />
EOHTML4

###### Load the Template showing the apportionment

	my $TMPL = G_S::Get_Object($db, 10029) || die "Couldn't retrieve template: 10029";

	$TMPL =~ s/%%self%%/$self/g;
	$TMPL =~ s/%%HTML%%/$HTML/g;
	$TMPL =~ s/%%HIDDEN%%/$hidden/g;


	print $q->header(-charset=>'utf-8'), $TMPL;
}

###### 09/27/02 assigned the global member types var to %MEMBER
###### 10/02/02 Created voiding section
###### 10/04/02 made the transaction description an HTML break if it was empty
###### 01/03/03 Added Datastamp to voided transaction Descriptions
###### 01/08/03 Altered screen layout to accommodate new columns
###### 03/04/03 added -w switch, also changed to the .pm style for G_S, rolled the transaction voiding
###### and the apportionment deletion into a transaction to avoid getting one without the other,
###### set RaiseError to TRUE, fixed incorrect SQL test for NULL in apportionment_test
###### 04/09/03 fixed so time SQL errors caused by the upgrade to PostgreSQL 7.3.2
###### 05/12/03 fixed some unit'd vars, removed the param options from admin login,
###### also fixed a few poorly placed 'exits' that would cause DB handles not to be closed properly
###### 10/02/03 reworked for easier readability, ripped out the hash parameter checking,
###### also removed some more 'exit's
###### ended up making many revisions too numerous to mention
###### 10/13/03 fixed the broken HTML when removing the VOID button from a voided row
###### 10/16/03 changed the straight join on the vendors table to a LEFT to improve
###### results when there is no vendor (like CA subscriptions)
###### 11/21/03 Added  $member_info->{emailaddress} = $member_info->{emailaddress} || '';
######			at ~ line 266 to alleviate uninitialized errors due to NULL value.
######		Added  $transaction_hash->{discount_amt} = $transaction_hash->{discount_amt} || 0;
######			at ~ line 194 to alleviate uninitialized errors due to NULL value.
###### 01/30/04 further uninit'd var fixes and SQL refinements
###### 07/07/04 lengthened the transaction description limit from 25 to 40
###### 02/01/06 revised the delete qry to work from a do instead of a select
###### 07/14/06 many HTML revisions as well as a few perl ones
###### 10/26/07 more HTML revisions as well as adding the receivable amount into the mix
###### 11/09 introduced the frames based interface with history selection instead of the whole enchilada by default
###### 09/11/12 removed references to cv_spread