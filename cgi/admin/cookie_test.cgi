#!/usr/bin/perl

use CGI ':all';
print header,start_html(-bgcolor=>'#ffffff');
unless (cookie()){
	print h1('No cookies received');
}
else{
	print h4('These are the individual cookies we received from you.');
	foreach (cookie()){
		print "<b>$_ </b>= ".cookie($_).br;
	}
	print br.h4('This is the cookie string as a whole.');
	print "<b>raw cookie string </b>= " , raw_cookie();
}
print end_html;
exit;



