#!/usr/bin/perl -w
###### co_admin.cgi
######
###### The admin script for manipulating Charitable Organization records.
######
###### created: 02/15/05	Karl Kohrt
######
###### modified: 03/01/06 Karl Kohrt
	
use strict;
use lib ('/home/httpd/cgi-lib');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
use G_S;
use DBI;
use CGI;
use DB_Connect;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser);
use Solicit;
use State_List qw( %STATE_CODES );
use MailingUtils;
use Mail::Sendmail;

###### CONSTANTS
our $TOP_EXEC_ROLE = 120;	# Marketing Director

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our %TMPL = (	'search'	=> 10389,
		'display'	=> 10390 );
our %EMAIL = ('co'		=> 10477 );
our %STATUS = ('p'	=> 'Pending',
		'1'	=> 'Active',
		'0'	=> 'De-Activated' );
our $db = '';
our $member_id = '';
our $action = ( $q->param('action') || '' );
our $search_field = $q->param('search_field') || '';
our $valid = 0;
$valid = 1 if $search_field eq 'pending';		# pending is a special case, so set valid here.
our $search_value = $q->param('search_value');
$search_value = G_S::PreProcess_Input($search_value) if $search_value;
our $search_type = $q->param('search_type') if $q->param('search_type');
$search_type = $q->param('type') unless $search_type;
our $nom_details = '';
our $alias = '';
our $membertype = '';
our $access_type = '';
our $login_error = 0;
our %co_info = ();
our ($StateList, $StateHash) = ();
our $data = '';
our $status_message = '';
our $msg = '';			
our @field_names = qw/type type_id firstname lastname co_name emailaddress
			url address1 address2 city state country postalcode
			phone fax stamp username password active co_notes
			category referral_id fein confirm_ip/;
# Create a string to match the array above for use in queries.
our $field_names_string = join ', ', @field_names;
our @char_fields = qw/=type
			=firstname
			=lastname
			=co_name
			=emailaddress
			=url
			=address1
			=address2
			=city
			=state
			=country
			=postalcode
			=phone
			=fax
			=username
			=password
			=active
			=co_notes
			=fein
			=confirm_ip/;
our @int_fields = qw/=id
			=type_id
			=referral_id
			=category
			=member_id/;
###### %field has the form of 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %field = (	type_id 		=> {value=>'', class=>'n', req=>1},
		type 			=> {value=>'', class=>'n', req=>1},
		active 		=> {value=>'', class=>'n', req=>0},
		category		=> {value=>'', class=>'n', req=>1},
		referral_id 		=> {value=>'', class=>'n', req=>0},
		firstname 		=> {value=>'', class=>'n', req=>1},
		lastname 		=> {value=>'', class=>'n', req=>0},
		co_name 		=> {value=>'', class=>'n', req=>1},
		emailaddress		=> {value=>'', class=>'n', req=>0},
		url			=> {value=>'', class=>'n', req=>0},
		address1		=> {value=>'', class=>'n', req=>0},
		address2		=> {value=>'', class=>'n', req=>0},
		city 			=> {value=>'', class=>'n', req=>1},
		state_code		=> {value=>'', class=>'n', req=>0},
		state 			=> {value=>'', class=>'n', req=>0},
		country 		=> {value=>'', class=>'n', req=>1},
		postalcode		=> {value=>'', class=>'n', req=>0},
		phone 			=> {value=>'', class=>'n', req=>0},
		fax			=> {value=>'', class=>'n', req=>0},
		username		=> {value=>'', class=>'n', req=>0},
		password		=> {value=>'', class=>'n', req=>0},
		fein 			=> {value=>'', class=>'n', req=>0},
		confirm_ip		=> {value=>'', class=>'n', req=>0},
		stamp			=> {value=>'', class=>'n', req=>0},
		operator		=> {value=>'', class=>'n', req=>0},
		notes			=> {value=>'', class=>'n', req=>0}
		);
our @result_list = ();

################################
###### MAIN starts here.
################################

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'co_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

###### If this is the first time through, just print the Search page.
unless ( $action )
{
	Search_Form()	;
}
elsif ($action eq 'search')
{
	# Check the validity of the field and the value provided by the user.
	$valid ||= Check_Validity();
	if ( $valid )
	{
		Run_Query();
		# If no records were found.
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		# If a list of records were found.
		elsif ( @result_list > 1 )
		{
			###### create list report
			Display_Results();
		}
		# If one record was found.
		else
		{
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
	}
	else
	{
		Search_Form($msg);
	}
}
elsif ($action eq 'show')
{
	$valid ||= Check_Validity();
	if ( $valid )
	{
		Run_Query();

		###### at this point we should have one row of data
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		else
		{		
			$data = shift @result_list;
			Load_Data();
			Create_controls();
			Do_Page('display');
		}
	}
	else
	{
		Search_Form($msg);
	}
}
elsif ( $action eq 'activate' || $action eq 'update' )
{
	my $activate_flag = 0;	# 0 means no need to activate. 
	my $activate_message = "";
	Load_Params();
	Check_Required();

	# If we already have an error.
	if ($msg)
	{
		# Reload the page with the problems flagged if there are any errors
		Create_controls();
		Do_Page('display');
	}
	else
	{
		# If we're activating for the first time, set the flag and message addition.
		if ( $action eq 'activate' )
		{
			$activate_flag = 1;
			$activate_message = "/Activated";
		}
		# Get the current status before we update, in case this is a de-activation.
		my $pre_active = $db->selectrow_array("	SELECT active
								FROM 	co_list
								WHERE 	type = ?
								AND 	type_id = ?;",
								undef,
								$field{type}{value},
								$field{type_id}{value});
		Update($activate_flag);

		$search_field = 'type_id';
		$search_value = $field{type_id}{value};
		$search_type = $field{type_id}{type} || $search_type;

		# Get the newly updated data.
		Run_Query();
		$data = shift @result_list;
#Err("type= $field{type}{value}, value= $field{type_id}{value}, pre= $pre_active, data= $data->{active}");
		# Do all de-activation related tasks if we de-activated.
		Process_Deactivation() if ($pre_active eq '1' && $data->{active} eq '0');
		Load_Data();
		Create_controls();
		Do_Page('display');
	}

}
else { Err('Undefined action.') }

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

###### A CO categories drop-down menu.
sub cbo_Categories
{
	my @list = ('');
	my %cats = ( '' => 'Select One' );

	my $sth = $db->prepare("SELECT type, description FROM co_cats ORDER BY description;");
	$sth->execute;
	while ( my( $type, $description) = $sth->fetchrow_array )
	{
		$cats{$type} = $description;
		push (@list, $type);
	}
	$sth->finish;
	return $q->popup_menu(	-name=>'category',
					-class=>'in',
					-values=>\@list,
					-default=>$field{category}{value},
					-labels=>\%cats,
					-force=>'true');
}

###### A country drop-down menu.
sub cbo_Country
{
	require '/home/httpd/cgi-lib/CC.list';
	return $q->popup_menu(	-name=>'country',
					-class=>'in',
					-values=>\@CC::COUNTRY_CODES,
					-default=>$field{country}{value},
					-labels=>\%CC::COUNTRIES,
					-force=>'true');
}

###### A state drop-down menu.
sub cbo_State
{
	my ($tmp1, $tmp2, $tmp3, $tmp4) = ();

	(
	$tmp1,	$tmp2,	$tmp3, $tmp4,
	$StateList,
	$StateHash
	)	= G_S::Standard_State(	$db,
						$field{state}{value},
						$field{state}{value},
						$field{country}{value});
	###### if our list is not populated, then we'll put something in
	if ( ! defined $StateList || scalar @{$StateList} < 1)
	{
		$StateList = [''];
		$StateHash = {'' => 'Enter value below'};
	}

	return $q->popup_menu(	-name=>'state_code',
					-default=>'',
					-values=>$StateList,
					-labels=>$StateHash,
					-class=>'in',
					-force=>'true');
}

sub Check_Required
{
	###### here we'll check required fields and assign new html classes as necessary
	foreach (keys %field)
	{
		# If a required field is empty, we'll flag it.
		if ( !$field{$_}{value} && $field{$_}{req} )
		{	
			$field{$_}{class} = 'r';
			$msg = $q->span({-class=>'r'}, "<br>Please fill in the required fields marked in <b>RED</b><br>\n");
		}
	}
	foreach (keys %field)
	{
		if ( /state/ && ! /code/ )
		{
			###### check for a standardized state
			my ($success, $country_match) = ();	
			($field{$_}{value}, $field{($_ . '_code')}{value}, $success, $country_match, $StateList, $StateHash) = G_S::Standard_State($db, $field{$_}{value}, $field{($_ . '_code')}{value}, $field{('country')}{value});
		}
	}

	###### a state is required if the country is US (at the very least)
	if ( $field{country}{value} eq 'US' )
	{
		my $match = 0;
		foreach (values %STATE_CODES)
		{
			if ( uc $field{state}{value} eq $_)
			{
				###### we'll assign the match since they may have submitted a lower case value
				$field{state}{value} = $_;
				$match = 1;
				last;
			}
		}

#		unless ( $match )
#		{
#			$msg .= $q->span({-class=>'r'}, "<br>Please select a state from the dropdown box.<br>\n");
#		}
  	 }
}

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			$msg= $q->span({-style=>'color: #ff0000'},
				"Invalid search value for $search_field - Use only whole numbers with this field.");
			return;	
		}
	}
	else
	{
		$msg= $q->span({-style=>'color: #ff0000'},
				"Invalid field specification - Try again.");
		return;				
	}
	return 1;						
}

sub Create_controls
{
	foreach (keys %field)
	{
		###### now we'll convert the 'value' to the needed form element with the value included
		if ($_ eq 'country')
		{	###### country is a special drop down box
			$field{$_}{control} = cbo_Country();
		}
		elsif ($_ eq 'state_code')
		{	###### our special US state drop-down
			$field{$_}{control} = cbo_State($_);
		}
		elsif ($_ eq 'category')
		{	###### our CO categories drop-down
			$field{$_}{control} = cbo_Categories($_);
		}
		elsif($_ eq 'active')
		{
			# If active is not NULL, then show the status via radio buttons.
			if ( $field{$_}{value} ne '' )
			{
				$field{$_}{value} = 
					$q->radio_group(	-name=>'active',
								-labels=>{0=>'Inactive', 1=>'Active'},
								-default=>$field{$_}{value},
								-class=>'in',
								-values=>['0', '1'] );
				$status_message = 'Changing a record to Inactive will remove it from the public listings.';
			}
			# Else show the activation button.
			else
			{
				$field{$_}{value} = $q->submit( 		-class=>'in',
										-value=>'Activate',
										-onclick=>"document.forms[0].action.value='activate'" );
				$status_message = '';
			}
		}			
	}
}

###### Display a list if more than one result is returned from the query.
sub Display_Results
{
	my $html;

	# print a title for the browser window.
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#ffffee', -title=>'Charitable Organization Search Results');

	# print a simple page header.
	print qq!
		<CENTER><H1>Charitable Organization Search Results</H1></CENTER><p>
		<TABLE align="center" border="0" cellpadding="2">
 			<TR>
			<TD><b> C.O. Name </b></TD>
			<TD><b> C.O. Type </b></TD>
			<TD><b> C.O. ID </b></TD>
 			<TD><b> City </b></TD>
			<TD><b> State </b></TD>
			<TD><b> Country </b></TD>
			<TD><b> Referral ID </b></TD>
			<TD><b> Status </b></TD>
			<TD><b> Confirmed </b></TD>
			</TR>!;

	my $row_color = "#ECFFF9";
	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		# Mark the NULL active field pending.
		$result->{active} = "p" unless defined $result->{active};
		# Alternate row color.
		$row_color =  ($row_color eq '#E6F2FF') ? '#ECFFF9' : '#E6F2FF';
		print qq!
			<TR bgcolor="$row_color">
			<TD><A href="$ME?action=show&search_field=id;search_value=$result->{id};search_type=$result->{type}"> $result->{co_name} </a></TD>
			<TD>$result->{type}</TD>
			<TD>$result->{id}</TD>
			<TD>$result->{city}</TD>
			<TD>$result->{state}</TD>
			<TD>$result->{country}</TD>  
			<TD>$result->{referral_id}</TD>
			<TD>$STATUS{$result->{active}}</TD>  
			<TD>$result->{confirm_ip}</TD>  
			</TR>!;
	}

	print qq!
	</TABLE>
	<CENTER><p><a href="$ME">Click Here for New Search</a></CENTER>!;

	print $q->end_html();
}

###### Send an email to the $mail_who party.
sub Do_Mail
{
	my $mail_who = shift;
	my $exec = {};

	# At this point we have an ID, we can look upline
	#  and select the necessary parties to email.
	my $sth = $db->prepare("
		SELECT m.id, m.spid, m.firstname, m.lastname, m.emailaddress,
			m.alias, m.membertype,
			COALESCE(nbt.level, 0) AS role,
			COALESCE(a.a_level, 0) AS a_level
		FROM 	members m
		LEFT JOIN nbteam_vw nbt
		ON 	m.id=nbt.id
		LEFT JOIN a_level a
		ON 	a.id=m.id
		WHERE 	m.id= ?");
	my $tmp = {spid => $data->{referral_id}};
	while ($tmp->{spid} > 1)
	{
		$sth->execute($tmp->{spid});
		$tmp = $sth->fetchrow_hashref;

		if ($tmp->{role} >= $TOP_EXEC_ROLE && $tmp->{membertype} eq 'v')
		{
			$gs->ConvHashref('utf8', $tmp);
			$exec = $tmp;
		}
		last if $tmp->{role} >= $TOP_EXEC_ROLE;
	}
	my $tmpl = $gs->Get_Object($db, $EMAIL{$mail_who}) ||
	die "Failed to retrieve the email template #$EMAIL{'$mail_who'}\n";

	# If we don't have a CO email address, sub in the exec's info in the To: field.
	if ( ! $data->{emailaddress} && $mail_who eq 'co' )
	{
		$tmpl =~ s/%%co:firstname%%/$exec->{firstname}/;
		$tmpl =~ s/%%co:lastname%%/$exec->{lastname}/;
		$tmpl =~ s/%%co:emailaddress%%/$exec->{emailaddress}/;
	}
	# Now sub in all the remaining fields.
	ParseMe::ParseAll(\$tmpl,{
		exec	=> $exec,
		co	=> $data});
	my $hdr = ParseMe::Grab_Headers(\$tmpl);
	MailingUtils::DrHeaders($hdr);
	my %mail = (
		'Content-Type'=> 'text/plain; charset="utf-8"',
		'Message'	=> $tmpl,
		%{$hdr}
	);

	sendmail(%mail) || die $Mail::Sendmail::error;
}

###### Display a single record to the browser.
sub Do_Page
{
	my $tmpl_name = shift;

	my $hidden = '';# = &Create_hidden();
	my $tmpl = G_S::Get_Object($db, $TMPL{$tmpl_name}) || die "Couldn't open $TMPL{$tmpl_name}";
	$tmpl =~ s/%%message%%/$msg/;
	$tmpl =~ s/%%status_message%%/$status_message/;
	$tmpl =~ s/%%hidden%%/$hidden/;
	$tmpl =~ s/%%ME%%/$ME/g;
	# Set the color of the asterisk for certain uneditable fields if it's an AG.
	my $class_noedit_id = ( $field{type}{value} eq 'ag' ) ? 'b' : 'wt';
	$tmpl =~ s/%%class_noedit_id%%/$class_noedit_id/;

	foreach (keys %field)
	{
		if ($field{$_}{control})
		{
			# Display the combo box.
			$tmpl =~ s/%%$_-control%%/$field{$_}{control}/;
				
			# If it is a state type field, display the value also.
			if ( ($_ eq 'state') || ($_ eq 'pay_state') )
			{
				$tmpl =~ s/%%$_%%/$field{$_}{value}/;
			}
		}
		else
		{
			$tmpl =~ s/%%$_%%/$field{$_}{value}/g;
		}

		$tmpl =~ s/%%class_$_%%/$field{$_}{class}/;
	}
	# This is the link to the mem_info record for AGs only.
	if ( $field{type}{value} eq 'ag' ) { $tmpl =~ s/%%ag_link%%/$field{username}{value}/ }
	elsif ( $field{type}{value} eq 'co' ) { $tmpl =~ s/%%ag_link%%/co/ }

	print $q->header(-expires=>'now'), $tmpl;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br>$_[0]<br>";
}

###### Loads the data from the query into the global hash variable.
sub Load_Data
{
	###### this procedure presumes that the field keys are identical to the passed parameters
	foreach my $key (keys %field)
	{
		$field{$key}{value} = $data->{$key};
		$field{$key}{value} = '' unless defined $field{$key}{value};
	}
}

###### Load the params into a local hash.
###### Populating our field array hash values
sub Load_Params
{
	###### this loop depends on the %field hash keys being identical to the passed parameter names
	foreach (keys %field)
	{
		# Removed the 'or blank' at the end since it was blanking out zero value fields.
		###### instead am checking for a defined value
		$field{$_}{value} = ( defined $q->param($_) ) ? $q->param($_) : '';

		###### let's filter for extraneous characters (this may pose a problem with other character sets)
		###### we'll be more restrictive on the name fields
		if ( /co_name/ )
		{
			$field{$_}{value} =~ s/\t|\r|\n|\f|\[M|\+|=|_//g;
		}
		###### we can skip the memo and special exception verbiage fields
		elsif ( /notes/ )
		{
			next;
		}
		else
		{
			$field{$_}{value} =~ s/\t|\r|\n|\f|\[M|"|;|:|\+|=//g;
		}
	}
}

##### Checks for a de-activation and runs the special processes for that case.
sub Process_Deactivation
{
	# Add the email address to the Do Not Solicit list.
	my $notes = "$operator: " if $operator;
	$notes .= " via co_admin.cgi de-activation.";
	Solicit::Do_Not_Solicit($db, $data->{emailaddress}, $notes);

	# Email the CO and cc: the MD that this CO has be de-activated.
	Do_Mail('co');

	# De-activate the individual member donation selections for this CO.
	$db->do("	UPDATE co_selections
			SET 	active = 'f'
			WHERE 	type_id= $field{type_id}{value}
			AND	type= '$field{type}{value}';");
}

##### Runs the specified query, pushing the results onto a list (array).
sub Run_Query
{
	my ($qry, $sth, @values) = ();
	$qry = "SELECT * FROM co_list_view WHERE ";

	if (grep { /=$search_field/ } @char_fields)
	{ 
		$qry .= "$search_field ~* ?";
		push (@values, $search_value);
	}
	elsif ( $search_field eq 'pending' )
	{
		$qry .= "active IS NULL ";
	}
	else
	{
		$qry .= "$search_field = ?";
		$qry .= " AND type = '$search_type'" if (($search_field eq 'id' || $search_field eq 'type_id')
									&& $search_type);
		push (@values, $search_value);
	}
	$qry .= " ORDER BY co_name;";
	$sth = $db->prepare($qry);
	$sth->execute( @values );
	
	while ( $data = $sth->fetchrow_hashref)
	{
		foreach ( keys %{$data} ) { $data->{$_} ||= '' unless $_ eq 'active' }
		push (@result_list, $data);
 	}		
	$sth->finish();
}

###### print our main search form with any pertinent messages
sub Search_Form
{
	my $msg = shift || '';
	# Open the search template.
	my $tmpl = G_S::Get_Object($db, $TMPL{'search'}) || die "Failed to load template: $TMPL{'search'}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$msg/;
	print $q->header(), $tmpl;	
}

###### Update a nomination record.
sub Update
{
	my $activate_flag = shift;
	my $value_list = '';
	my @values = ();
	my $qry = '';
	my $sth = '';
	my $rv = '';

	my $status_string = '';

	# If we are not activating the record, but there is a value.
	if ( !($activate_flag) && $field{active}{value} ne ''  )
	{
		# Set the status field to its current value.
		$status_string = "active= '$field{active}{value}',";
	}
	# If we are activating the record, set status to 1.
	elsif ( $activate_flag )
	{
		$status_string = "active= '1',";

	}
	# Otherwise, the status must be NULL.
	else
	{
		$status_string = "active = NULL,";
	}

	# Update the general list info in the co_list table.
	$qry .= "	UPDATE co_list
			SET
				$status_string
				category= ?,
				stamp= NOW(),
				operator= current_user,
				notes= ?,
				url= ?
			WHERE 	type_id= $field{type_id}{value}
			AND	type= '$field{type}{value}';";
	push (@values, $field{category}{value} || '');
	push (@values, $field{notes}{value} || '');
	push (@values, $field{url}{value} || '');

	# Set the unique field names for the appropriate table.
	my $tablename = '';
	my $co_name_string = '';
	my $fax_string = '';
	my $username_string = '';
	my $referral_id_string = '';
	my $notes_string = '';
	my $tin_string = '';
	my $phone_string = '';
	if ( $field{type}{value} eq 'co' )
	{
		$tablename = 'co_info';
		$co_name_string = 'co_name';
		$fax_string = 'fax';
		$username_string = 'username';
		$referral_id_string = 'referral_id= ?,';
		$notes_string = 'notes';
		$tin_string = 'fein';
		$phone_string = 'phone= ?,';
	}
	elsif ( $field{type}{value} eq 'ag' )
	{
		$tablename = 'members';
		$co_name_string = "company_name";
		$fax_string = 'fax_num';
		$username_string = 'alias';
		$notes_string = 'memo';
		$tin_string = 'tin';
		$phone_string = '';	# Phone is a combo of areacode and phone, so no updates for ag.
	}
	# Update the detail info in the appropriate table.
	$qry .= "	UPDATE $tablename
			SET
				firstname= ?,
				lastname= ?,
				$co_name_string= ?,
				emailaddress= ?,
				address1= ?,
				address2= ?,
				city= ?,
				state= ?,
				country= ?,
				postalcode= ?,
				$phone_string
				$fax_string= ?,
				$username_string= ?,
				password= ?,
				stamp= NOW(),
				$referral_id_string
				$tin_string= ?,
				operator= current_user,
				$notes_string= 'Updated via the CO admin.'
			WHERE id= $field{type_id}{value}";

	foreach ( @field_names )
	{
		push (@values, $field{$_}{value} || '') 
			unless (	$_ eq 'category' ||
					$_ eq 'co_notes' ||
					$_ eq 'url' ||
					$_ eq 'type' ||
					$_ eq 'type_id' ||
					$_ eq 'stamp' ||
					$_ eq 'active' ||
					$_ eq 'confirm_ip' ||
					($_ eq 'referral_id' && $tablename eq 'members') ||
					($_ eq 'phone' && $tablename eq 'members') );
	}	
#Err($qry);
#foreach ( @values ) { Err($_); }
#$msg .= qq#<br/>\n	<span class="r">TEST MODE - NO UPDATE PERFORMED</span><br/>\n#;
#return 1;
#	$sth = $db->prepare($qry);
	# If we were able to update the co's information.
#	if ( $rv = $sth->execute(@values) )
	if ( $rv = $db->do($qry, undef, @values) )
	{					
		$msg .= qq#\n	<br/><span class="b">
					The update was successful.
					</span><br/>\n#;
	}
	# If no update was performed.
	else
	{
		$msg = "<br /><span class=\"r\">CO Record update failed.</span><br/>\n";
	}		
	return $rv;
}


###### 08/23/05 Added the de-activation code.
###### 03/01/06 Modified the Update sub from a "prepare" qry to a "do" qry due to 8.1 DB upgrade.








