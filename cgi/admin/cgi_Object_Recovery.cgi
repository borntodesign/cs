#!/usr/bin/perl
# cgi_Object_Recovery.cgi
# Show list of possible recovery points.
#
# Written by Stephen Martin
# August 21st  2002
#

=head1 cgi_Object_Recovery.cgi

=for Commentary

cgi_Object_Recovery.cgi
Show list of possible recovery points.

Written by Stephen Martin
August 21st  2002

=cut

use strict;
use DBI;
use CGI qw(:cgi);
use Carp;
use POSIX qw(locale_h);
use locale;
use URI::Escape;

setlocale(LC_CTYPE, "en_US.ISO8859-1");

# use HTML::Template;

require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data);    ###### database variables
my $rv;
my $operator;
my $query;
my $action;
my $counter;
my $newoffset;
my $newlink;
my $arc_pk;
my $arc_obj_id; 
my $arc_obj_name;  
my $arc_obj_description; 
my $arc_obj_value; 
my $arc_obj_type; 
my $arc_obj_caller; 
my $arc_stamp;  
my $arc_entered_archive;
my $spawn;
my $button1;
my $backoffset;
my $prevlink;
my $forward;
my $back;

my $MENU     = "./HTMLcontent.cgi";
my $self     = "./cgi_Object_Recovery.cgi";
my $viewer   = "./cgi_Object_Viewer.cgi";

my $DISPLAY= 4;
my $button = 0;

my $cgi = new CGI();

my $criteria1 = $cgi->param('criteria1');
my $criteria2 = $cgi->param('criteria2');
my $criteria3 = $cgi->param('criteria3');

my $Xoffset   = $cgi->param('Xoffset');

my $URLcriteria1=uri_unescape($criteria1);
my $URLcriteria2=uri_unescape($criteria2);
my $URLcriteria3=uri_unescape($criteria3);

my $obj_id  = $cgi->param('obj_id'); 

if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ )
{
 $spawn="\'.\/OEhelp4.cgi\',\'_OEH4_\',\'width=400,height=230,scrollbars=no\'";
} else {
 $spawn="\'.\/OEhelp4.cgi\',\'_OEH4_\',\'width=500,height=250,scrollbars=no\'";
}

print $cgi->header();

# Here is where we get our admin user and try logging into the DB

$operator = $cgi->cookie('operator'); my $pwd = $cgi->cookie('pwd');
unless ($db = &ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">

<script language="JavaScript">
<!--
function spawn(URL,Name,features)
 {
  window.open(URL,Name,features);
 }
//-->
</script>

<title>DHSC cgi Object Recovery</title>
</head>
<body bgcolor="#eeffee">
<form action="$self" method="POST" name="sform1">
<center>
<b>DHSC Template Recovery</b><br><br>
</center>
<table border="0" width="635" align="center">
 <tr>
  <td align="center" colspan="4">
   <hr noshade align="center">
  </td>
 </tr>
</table>
<table border="0" width="635" align="center">
  <tr>
    <td colspan="9" 
        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
        width="627">Below is a list of recoverable templates for this object.&nbsp;&nbsp;&nbsp;
    <a href="#" onClick="spawn($spawn);">
           <img src="https://www.clubshop.com/admin/images/talkbubble3_query.gif"
           width="24" height="24" border="0"></a>
    </td>
  </tr>
  <tr>
   <td colspan="9">
    <hr noshade align="center">
   </td>
  </tr>
  <tr>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Template</td>
   <td>&nbsp;</td>
   <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Description</td>
   <td>&nbsp;</td>
   <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Updated</td>
  </tr>
\n~;


$query = "SELECT pk,
                 obj_id,
                 obj_name,  
                 obj_description, 
                 obj_value, 
                 obj_type, 
                 obj_caller, 
                 stamp,  
                 entered_archive 
         FROM cgi_objects_arc
         WHERE  obj_id = ?
         ORDER by entered_archive DESC";

# print "<br><b>Debug $query </b><br>";

$sth = $db->prepare($query);

$rv = $sth->execute($obj_id);
defined $rv or die $sth->errstr;

$sth->bind_columns(undef, \$arc_pk,
                          \$arc_obj_id, 
                          \$arc_obj_name,  
                          \$arc_obj_description, 
                          \$arc_obj_value,
                          \$arc_obj_type,
                          \$arc_obj_caller, 
                          \$arc_stamp,
                          \$arc_entered_archive);

$counter = 0;

while   ($sth->fetch() )
        {

         # $obj_name =~ s/[\W]/\&nbsp\;/g;

         $arc_obj_description = substr($arc_obj_description,0,120);

         if (($counter >= $Xoffset) && ($counter <= ($Xoffset+$DISPLAY-1)))
         {
         print qq~
         <!-- DB Call -->
          <tr>
           <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
             <a href="$viewer\?obj_id=$arc_obj_id&pk=$arc_pk&Xoffset=$Xoffset">
              <font style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">View</font></a>
           </td>
           <td>&nbsp;</td>
           <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           </td>
           <td>&nbsp;</td>
           <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">$arc_obj_name</td>
           <td>&nbsp;</td>
           <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">$arc_obj_description</td>
           <td>&nbsp;</td>
           <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">$arc_entered_archive</td>

         </tr>
         <!-- DB Call -->
\n~;

         }

         $counter++;


        }

        $button=$button||( $counter > ($Xoffset+$DISPLAY));
        $button1= $Xoffset;
        $backoffset = $Xoffset - $DISPLAY;

         $newoffset = $Xoffset+$DISPLAY;

         $newlink=$self . "?obj_id=" . $obj_id . "\&Xoffset=" . $newoffset . "\&criteria1=" . $URLcriteria1  . "\&criteria2=" . $URLcriteria2 . "\&criteria3=" . $URLcriteria3;
         $prevlink=$self . "?obj_id=" . $obj_id . "\&Xoffset=" . $backoffset . "\&criteria1=" . $URLcriteria1  . "\&criteria2=" . $URLcriteria2 . "\&criteria3=" . $URLcriteria3; 

         if ( $button )
         {
          $forward = "<a href=\"$newlink\"><img src=\"https:\/\/www.clubshop.com\/admin\/images\/right2.gif\" border=\"0\" alt=\"More Results\"></a>";
         }

         if ( $button1 )
         {
          $back    = "<a href=\"$prevlink\"><img src=\"https:\/\/www.clubshop.com\/admin\/images\/left2.gif\" border=\"0\" alt=\"Previous Results\"></a>";
         }

        if ( $counter == 0 ) 
        {
         print qq~
         <tr>
           <td colspan="9" height="10">&nbsp;</td>
          </tr>
         <tr>
          <td colspan="9"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           No results were returned..
          </td>
         </tr>
         <tr>
          <td colspan=9"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <a href="https://www.clubshop.com/cgi/admin/HTMLcontent.cgi"><img src="https://www.clubshop.com/admin/images/TMPhome.gif" border="0" alt="Template Management Page"></a>
          </td>
         </tr>

         \n~;
        }
$rv = $sth->finish();

         if ($button || $button1) 
         {

          print qq~
          <tr>
           <td colspan="9" height="10">&nbsp;</td>
          </tr>
          <tr>
           <td colspan="9" align="center" nowrap
            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            $back&nbsp;&nbsp;$forward&nbsp;&nbsp;<a href="https://www.clubshop.com/cgi/admin/HTMLcontent.cgi"><img 
             src="https://www.clubshop.com/admin/images/TMPhome.gif" border="0"
              alt="Template Management Page"></a>
           </td>
          </tr>
          <tr>
           <td colspan="9" height="10">&nbsp;</td>
          </tr>
          \n~;
         } else {
         print qq~
          <tr>
           <td colspan="9" height="10">&nbsp;</td>
          </tr>
          <tr>
           <td colspan="9" align="center" nowrap
            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            <a href="https://www.clubshop.com/cgi/admin/HTMLcontent.cgi"><img
              src="https://www.clubshop.com/admin/images/TMPhome.gif" border="0"
              alt="Template Management Page"></a>
           </td>
          </tr>
         
          \n~;
         }

print qq~

          <tr>
           <td width="615" colspan="9"></td>
          </tr>
         </table>
</form>
</body>
</html>
\n~;


$db->disconnect;

exit(0);

