#!/usr/bin/perl -w
###### SOViewer.cgi
###### Statement of Accounts Viewer
###### last modified 05/18/2015	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use DB_Connect;
use Digest::MD5 qw(md5 md5_hex md5_base64);

require '/home/httpd/cgi-lib/ADMIN_DB.lib';
binmode STDOUT, ":encoding(utf8)";

$| = 1;

###### Globals

our($meminfo) = ();
my $sth = ();
our $db        = ();
our $q         = new CGI;
our $id        = our $firstname = our $lastname = '';
our $data;
our $o;
our $trans_id;

my $qry;
my $self = $q->url();
my $des;
my $des_in;
my $class;
my $editor_link = "./SOEditor.cgi";

my %MEMBERS = %G_S::MEMBER_TYPES;

unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
}

my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd      = $q->cookie('pwd')      || $q->param('pwd');

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
}

##### 1st step calculate and encrypted MD5 of the remote IP #####

$des = md5_hex( $ENV{'REMOTE_ADDR'} );

if ( !( defined $operator ) ) {
    print $q->header(-charset=>'UTF-8'), $q->start_html(),
      $q->h3("Incorrect login.<br />Back up and try again"), $q->end_html();
    exit(0);
}

unless ( $db = ADMIN_DB::DB_Connect( 'SOAreport.cgi', $operator, $pwd ) ) {
    exit;
}
$db->{RaiseError} = 1;

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
# July 2008, is this really necessary?
#if ( CheckGroup() != 1 ) {
#    Err(
#        $q->h3('You must be a member of the accounting group to use this form')
#    );
#    goto 'END';
#}

###### At this point all validation has been completed 

$o = $param{o};

$id = $param{id};
$id =~ s/\D//g;

$trans_id = $param{trans_id};

###### Ensure that this program is entered with an id !
# actually, since we are going by trans_id what do we care the id is?
#unless ($id) {
#    Err( $q->h3('The id parameter is missing?') );
#    goto 'END';
#}

#$meminfo = getmemberdetails($id);

###### Validate IP address entry into program 
# I guess Stephen was really paranoid or something, shutting this off July 2998
#    $des_in = $o;

#    if ( $des_in ne $des ) {
#        Err( $q->h3('Internal Security Violation 1') );
#        goto 'END';
#    }

###### OK we are verified pull out the details using a query

$qry = "SELECT 
        soa.trans_id, 
        soa.id, 
        soa.amount, 
        soa.description as tdesc, 
        soa.memo, 
        soa.entered, 
        COALESCE(soa.batch, '') AS batch,
        soa.reconciled,
        soa.void,
        CASE 
         WHEN soa.reconciled = true THEN 'Reconciled'
         ELSE 'No'
        END  as reconmsg, 
        soa.operator, 
        soa_transaction_types.name as trans_type_desc
      FROM 
        soa
      LEFT OUTER JOIN soa_transaction_types ON soa.trans_type = soa_transaction_types.id
      WHERE
--       ( soa.id = ? ) AND 
       ( soa.trans_id = ? )";

$sth = $db->prepare($qry);

#$sth->execute( $id, $trans_id );
$sth->execute( $trans_id );

$data = $sth->fetchrow_hashref();

$sth->finish;

$meminfo = getmemberdetails($data->{id});

my $TMPL = G_S::Get_Object( $db, 10121 );
unless ($TMPL) { Err("Couldn't retrieve the template object."); return }

print $q->header( -expires => 'now', -charset=>'UTF-8' );

$TMPL =~ s/%%FIRSTNAME%%/$meminfo->{firstname}/g;
$TMPL =~ s/%%LASTNAME%%/$meminfo->{lastname}/g;
$TMPL =~ s/%%SELF%%/$self/g;
$TMPL =~ s/%%ID%%/$meminfo->{id}/g;
$TMPL =~ s/%%O%%/$des/g;
$TMPL =~ s/%%MEMTYPE%%/$MEMBERS{$meminfo->{membertype}}/g;
$TMPL =~ s/%%ALIAS%%/$meminfo->{alias}/g;
$TMPL =~ s/%%TRANS_ID%%/$trans_id/g;

$TMPL =~ s/%%TRANS_TYPE%%/$data->{trans_type_desc}/g;
$TMPL =~ s/%%AMOUNT%%/$data->{amount}/g;
$TMPL =~ s/%%DESCRIPTION%%/$data->{tdesc}/g;
$TMPL =~ s/%%MEMO%%/$data->{memo}/g;
$TMPL =~ s/%%ENTERED%%/$data->{entered}/g;
$TMPL =~ s/%%BATCH%%/$data->{batch}/g;
$TMPL =~ s/%%RECON%%/$data->{reconmsg}/g;
$TMPL =~ s#%%VOID%%#($data->{void} ? '<b>YES</b>':'No')#e;
$TMPL =~ s/%%OPERATOR%%/$data->{operator}/g;

print $TMPL;

END:
$db->disconnect;
exit;

sub CheckGroup {
    my $sth =
      $db->prepare("SELECT usesysid FROM pg_user WHERE usename = '$operator'");
    $sth->execute;
    my $sid = $sth->fetchrow_array;
    $sth->finish;
    $sth =
      $db->prepare("SELECT grolist FROM pg_group WHERE groname = 'accounting'");
    $sth->execute;
    my $res = $sth->fetchrow_array;
    $sth->finish;
    ( $res =~ /$sid/ ) ? ( return 1 ) : (return);
}

sub Err {
    print $q->header(-charset=>'UTF-8'), $q->start_html();
    print $_[0];
    print $q->end_html();
}

sub ID_Ck {
    my $membertype;    ###### a temporary variable just to test with
    my $sth = $db->prepare( $_[0] );
    $sth->execute;

###### id, firstname, & lastname are globals
    ( $id, $firstname, $lastname, $membertype ) = $sth->fetchrow_array;
    $sth->finish;
    unless ($membertype) {
        Err('No match on the ID and Password received');
        return 0;
    }
    elsif ( $membertype ne 'v' ) {
        Err('According to our records, you are not a VIP');
        return 0;
    }
    return 1;
}

sub getmemberdetails
{
###### removed the stuff pertaining to the Partner date
###### I don't know what that was all about - Bill

    my ($id) = @_;
    my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = "select
            oid,*, 
            coalesce(prefix,\'\') || \' '\ || firstname || \' \' ||  lastname as memname,
            '' as paid_thru
             from members where id =  ?";


    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data;
}


##### 04/06/2003 - modified interval statements as noted below for postgres7.3:
#####  ...now() + interval('12 month') ) + 1 as paid_thru...
#####  became:
#####  ...cast(now() + interval '12 month' + interval '1 day' AS date) as paid_thru...
###### 11/04/03 removed the Partner date stuff from getmemberdetails
###### 02/14/06 revised the SQL to actually pull up the matching transaction
###### performed other cursory cleanups
# 08/22/08 added the void field to the display mix