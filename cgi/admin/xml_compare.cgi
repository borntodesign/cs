#!/usr/bin/perl -w
###### xml_compare.cgi
######
###### A script to compare one xml document to another xml document. 
######
###### created: 06/28/05	Karl Kohrt
######
###### modified: 
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use DBI;
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use XML::SemanticDiff;

###### CONSTANTS
our $cgiini_script = 'xml_compare.cgi';
our $FORM = 10459;
our %DOCUMENTS = (	't' =>	{	'desc' => 'translation_object',
					'sql' 	=> '	SELECT value
							FROM object_translations 
							WHERE pk = ?;'},
			'c' =>	{	'desc' => 'cgi_object',
					'sql' 	=> '	SELECT obj_value AS value
							FROM cgi_objects 
							WHERE obj_id = ?;'},
			'l' =>	{	'desc' => 'lexicon',
					'sql' 	=> '	SELECT value
							FROM lexicons 
							WHERE pk = ?;'}
		);

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $diff = XML::SemanticDiff->new(keepdata => 1);
# This one will let you know what line numbers the changes are on.
#our $diff = XML::SemanticDiff->new(keeplinenums => 1);
our $ME = $q->script_name;
our $xs = new XML::Simple(NoAttr=>1);
our $db = '';
our $params = ();
our $message = '';
our $results = '';
our $form = '';

################################
###### MAIN starts here.
################################

# Get the admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
# Login into the db.
if ($db = ADMIN_DB::DB_Connect( $cgiini_script, $operator, $pwd ))
{
	# Get the parameters.
	Load_Params();

	$form = $gs->Get_Object($db, $FORM) ||
			die "Failed to load template: $FORM\n";
	if ( $params->{action} eq 'report' )
	{
		# If we don't have a baseline provided for a translation object, find the baseline.
		if ( ! $params->{baseline} && $params->{i_type} eq 't' )
		{
			$params->{baseline} = Get_Baseline($params->{interest});
			$params->{b_type} = 't';
		}
		if ( $params->{baseline} )
		{
			$results .= qq#<br><span class="btn"><u>Comparison: $DOCUMENTS{$params->{i_type}}{desc} $params->{interest} VS. $DOCUMENTS{$params->{b_type}}{desc} $params->{baseline} (Baseline)</u></span><br><br>#;

			my $baseline_xml = Get_XML($params->{baseline}, $params->{b_type});
			my $interest_xml = Get_XML($params->{interest}, $params->{i_type});
			my $baseline_xml_hash = $xs->XMLin($baseline_xml);
			my $interest_xml_hash = $xs->XMLin($interest_xml);
			my $suspects = '';

			# Find the differences between the document in question and its baseline document.
			if ( $params->{d_report}
				|| ! ( $q->{d_report} || $q->{c_report} || $q->{n_report} ) )
			{
				$suspects = Semantic_Diff($baseline_xml, $interest_xml);
				$results .= qq#<span class="btn">Differences between the document in question and its baseline document:</span><br/><textarea cols='80' rows='10'>$suspects</textarea><br/><br/>#;
			}

			# Find nodes with equivalent contents.
			if ( $params->{c_report} )
			{
				my $suspects = Node_Contents_Equal($baseline_xml_hash, $interest_xml_hash);
				$results .= qq#<span class="btn">Node Contents Equal:</span><br/><textarea cols='80' rows='10'>$suspects</textarea><br/><br/>#;
			}

			# Find the nodes with the same names.
			if ( $params->{n_report} )
			{
				$suspects = Node_Names_Equal($baseline_xml_hash, $interest_xml_hash);
				$results .= qq#<span class="btn">Node Names Equal:</span><br/><textarea cols='80' rows='10'>$suspects</textarea><br/><br/>#;
			}
		}
		else
		{
			$message = qq#<span class="bad">No default baseline document code was found for this particular Document of Interest.</span><br/>#;
		}
	}
}
else
{
	$message = qq#<span class="bad">Login at the Main DB page.</span><br/>#;
}

# Put the results and any messages onto the page.
$form =~ s/%%ME%%/$ME/g;
$form =~ s/%%results%%/$results/g;
$form =~ s/%%message%%/$message/g;

# Put the parameters back on the page, with the exception of the report selections.
foreach ( qw/baseline interest/ )
{
	my $tmp = $params->{$_} || '';
	$form =~ s/%%$_%%/$tmp/g;
}
foreach ( qw/b_type i_type/ )
{
	my $tmp = "checked_" . $params->{$_} || '';
	$form =~ s/%%$tmp%%/selected/g;
}
# Blank out all remaining substitution fields.
$form =~ s/%%.*%%//g;
print $q->header(-expires=>'now'), $form;

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>" . $_[0] . "<br/>";
}

# Get the baseline document code if possible.
sub Get_Baseline
{
	my $icode = shift;

	my ($bcode ) = $db->selectrow_array("	SELECT o1.pk
							FROM 	object_translations o1,
								object_translations o2
							WHERE 	o1.obj_key = o2.obj_key
							AND 	o1.language = 'en'
							AND 	o2.pk = ?",
							undef, $icode
						);
	return $bcode;
}

###### Retrieve the xml data.
sub Get_XML
{
	my ( $object, $type ) = @_;
	my ( $data ) = $db->selectrow_hashref($DOCUMENTS{$type}{'sql'},
						undef, $object);
	# utf-8ize the data - specifically the name.
	$gs->ConvHashref('utf8', $data) if $data;

	return $data->{value};
}

###### Load the params into a local hash.
sub Load_Params
{
	foreach ( $q->param() )
	{
		$params->{$_} = $q->param($_) || '';
	}
}

sub Node_Contents_Equal
{
	my ($baseline, $interest) = @_;
	my $suspects = '';

	# Let's pick out equivalent node names.
	foreach my $baseline_node ( keys %{$baseline} )
	{
		foreach my $interest_node ( keys %{$interest} )
		{
			if ( lc($interest->{$interest_node}) eq lc($baseline->{$baseline_node}) )
			{
				$suspects .= "\n$params->{interest}: '$interest_node' = $params->{baseline}: '$baseline_node'\nnode content= $interest->{$interest_node}\n";
			}
		}
	}
	$suspects ||= "None";
	return $suspects;
}

sub Node_Names_Equal
{
	my ($baseline, $interest) = @_;
	my $suspects = '';

	# Let's pick out equivalent node names.
	foreach my $baseline_node ( keys %{$baseline} )
	{
		foreach my $interest_node ( keys %{$interest} )
		{
			if ( $interest_node eq $baseline_node )
			{
				$suspects .= "\n$interest_node\n$params->{interest}: $interest->{$interest_node}\n$params->{baseline}: $baseline->{$baseline_node}\n";
			}
		}
	}
	$suspects ||= "None";
	return $suspects;
}

sub Semantic_Diff
{
	my ($baseline, $interest) = @_;
	my $suspects = '';
	foreach my $change ($diff->compare($baseline, $interest)) 
	{
      		$suspects .= "\n$change->{message} in context $change->{context}\n$params->{interest}: $change->{old_value}\n$params->{baseline}: $change->{new_value}\n";
 	}
	$suspects ||= "None";
	return $suspects;
}


