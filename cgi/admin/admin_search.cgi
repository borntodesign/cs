#!/usr/bin/perl
# new 'admin_search.cgi'
# last modified 05/04/05	Karl Kohrt
###### originally written by Chris Handley
###############################################################
### This script has been written to process the parameters from the HTML page
### that drives the search function.  This script should not need to change if we want
### to search different tables or new fields.  The HTML page should pass all of the
### infomation regarding the view to use and the fields to process.
###############################################################
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
$| = 1;

my $mailprog = '/usr/sbin/sendmail';
my $TBL_STYLE = "td{font-family: Arial, Helvetica, sans-serif; font-size: 8pt;}";
my $q = new CGI; 

my $columns = "*";
my $getcol = $q->param('getcol');
my $pred = $q->param('where_value');
my $like = $q->param('like');
my $order = $q->param('order');
my $view = $q->param('view');
if ($view eq "")
{
	$view = "members";
	$columns = "id, spid, firstname, lastname, membertype, company_name, emailaddress, emailaddress2";
}
else
{
	 $view = "admin_search_" . $view;
}

if ($pred eq "") {
	&Err("<h2>Please enter a value to search for - Thank You!</h2>
<p>Use the Back button to search again or <A href=\"/admin/Adminsearch.html\">Click Here for new search</a>");
exit(0); }

my ($db);

###### here is where we get our user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
unless ($db = &ADMIN_DB::DB_Connect( 'admin_search.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;
&print_head;

$pred =~tr/a-z/A-Z/;

my $op;
if ($like eq "equal") 	{  $op= "="; }
elsif ($like eq "begin") 	{  $pred = $pred . "\%";  $op="like"; }
elsif ($like eq "ends") 	{  $pred =  "\%" . $pred; $op="like"; }

my $sth = $db->prepare("SELECT $columns FROM $view WHERE UPPER($getcol) $op ? $order");	
$sth->execute($pred);

&doheader;
my ($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k) = ();
while (($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k) =  $sth->fetchrow_array) 
{
&format_output;
}

print qq!
</TABLE>
<p>Use the Back button to search again or <a href="/admin/Adminsearch.html">Click Here for new search</a>!;
print $q->end_html();

$sth->finish;
$db->disconnect;
exit(0);

sub print_head
{
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#F3F3F3', -title=>'Admin Search', -style=>{-code=>$TBL_STYLE});
print qq!<center><img src="/admin/images/searchresults.gif"></center><p>!;
}

sub doheader
{
print qq!

<TABLE border="2">
      <TR>
      <TD nowrap><b> Member ID </b></TD>
      <TD nowrap><b> Sponsor ID </b></TD>
      <TD><b> MemberName </b></TD>
      <TD><b> Type </b></TD>
      <TD><b> CompanyName </b></TD>
      <TD><b> Email </b></TD>
      <TD><b> AlternateEmail </b></TD>
     <TD><b> Account # </b></TD>      
     <TD><b> CreditCard # </b></TD>
     </TR>
     <TR>
      <TD> </TD>
      <TD> </TD>
      <TD> </TD>
      <TD> </TD>
      <TD> </TD>
      <TD> </TD>
      <TD> </TD>
       <TD> </TD>
      <TD> </TD>
     </TR>!;
}

sub Err{
	print $q->header(), $q->start_html();
	print $_[0];
	print $q->end_html();
}

sub format_output
{

	if ($a eq "") { $a='N/A';}
	if ($b eq "") { $b='N/A';}
	if ($c eq "") { $c='N/A';}
	if ($d eq "") { $d='N/A';}
	if ($e eq "") { $e='N/A';}
	if ($f eq "") { $f='N/A';}
	if ($g eq "") { $g='N/A';}
	if ($h eq "") { $h='N/A';}
	if ($i eq "") { $i='N/A';}
	if ($j eq "") { $j='N/A';}
	if ($k eq "") { $k='N/A';}

	if ($e eq 'v') { $e='VIP';} 
	if ($e eq 'p') { $e='Partner';} 
	if ($e eq 't') { $e='Terminated';} 
	if ($e eq 'c') { $e='Cancelled';} 
	if ($e eq 'r') { $e='Removed';} 
	if ($e eq 'm') { $e='Member';} 
	if ($e eq 's') { $e='Shopper';} 
	if ($e eq 'd') { $e='Duplicate';} 
	if ($e eq 'x') { $e='Unconfirmed';} 

	my $acct = 'Restricted'; 
	my $card = 'Restricted'; 

	if ($operator eq 'bill')		{$acct = $i; $card = $j; }
	if ($operator eq 'k_kohrt') 	{$acct = $i; $card = $j; }
	if ($operator eq 'cindy')		{$acct = $i; $card = $j; }

	my $fullname = $c . " " . $d;

	print "<TR>";
	if ($a eq "N/A") 
	{
	     	print "<TD>$a</a></TD>";
	}
	else
	{
		print qq!<TD><A href="/cgi/admin/memberinfo.cgi?id=$a" target="Resource Window $a"> $a </a></TD>!;
	}
	print "<TD>$b</TD>
     		<TD>$fullname</TD>
     		<TD>$e</TD>
     		<TD>$f</TD>  
     		<TD>$g</TD>  	   
     		<TD>$h</TD>
     		<TD>$acct</TD>
     		<TD>$card</TD>
     		</TR>";
}

###### 11/21/01 incorporated my standard in-house DB connection routine & removed the warn_email notification
###### 12/11/01 added CGI::Carp to catch errors
###### 03/20/02 disabled -w because of the multitude of errors it generated due to uninitialised vars
###### 04/12/02 made the search value pass through dbi execute for proper quoting
###### 05/04/05 Added the sponsor ID to the fields and the ability to search for potential members.

