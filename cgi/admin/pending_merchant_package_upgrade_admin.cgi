#!/usr/bin/perl -w

=head1 NAME:
pending_merchant_package_upgrade_admin.cgi

	This script allows the approval of Merchant Package Upgrades VIA Bank Draft.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Once Accounting has collected the funds from a Merchant Bank Draft request,
	they use this interface to push a Merchants upgrade through.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, G_S, ADMIN_DB, CGI::Carp, Members, MerchantAffiliates, URI::Escape

=cut
use FindBin;
use lib "$FindBin::Bin/../../cgi-lib";
use strict;
use CGI;
use G_S;
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use Members;
use MerchantAffiliates;
use URI::Escape;
use Data::Dumper;

=head1

=head2
CLASSES:

	Classes that are instanciated

	$CGI
	$DB

=cut

my $CGI = CGI->new();
our $DB = {}; # used for the Database object later on.


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used throughout the script.

	$operator
	$pwd

=cut

my $operator = $CGI->cookie('operator');
my $pwd = $CGI->cookie('pwd');


=head1
SUBROUTINES

=head2
processTemplate

=head3
Description:

	This does stuff


=head3
Params:

	params	hashref	{
						
						
						
					}
	
	html_template	string	The template
	
	

=head3
Returns:

	none

=cut

sub processTemplate
{
	my $params = shift;
	my $html_template = shift;

	foreach my $key (keys %$params)
	{
		$html_template =~ s/%%$key%%/$params->{$key}/g;
	}

	$html_template =~ s/%%.*%%//g;

	return $html_template;
}


=head2
displaySearchForm

=head3
Description:

	This does stuff


=head3
Params:

	params	hashref	{
						
						
						
					}

=head3
Returns:

	none

=cut

sub displaySearchForm
{

	my $params = shift;

	$params->{title} = 'Merchant Bank Draft Package Upgrade Administration' if ! exists $params->{title};
	$params->{message} = 'Merchant Bank Draft Package Upgrade Administration' if ! exists $params->{message};

	my $html_form = <<EOT;
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<HTML>
		<HEAD>
		<TITLE>%%title%%</TITLE>
		</HEAD>
		<BODY bgcolor="#eeffee" onload="document.forms[0].search_value.focus()">
		%%error%%
		%%warning%%
		%%notice%%
		<H2 align="center">%%title%%</H2>
		<P>%%message%%</P>
		<br />
		<form method="post" action="%%action%%">
		<TABLE border="0">
		  <TBODY>
		    <TR>
			<TD>
			1) Select a Field to search by:
			</TD>
		       <TD>
		             <select size="1" name="search_field">
						<option value="id">Merchant ID</option>
						<option value="all">All Pending Bank Drafts</option>
			     </select>
		      </TD>
			 </TR>
			 <TR>
		      <TD>
		       2) Type your search string here:
			  </TD>
		      <TD>
			  	<INPUT size="50" type="text" name="search_value" maxlength="50">
			  </TD>
		      </TR>
		      <TR>
		      <TD align="right"><INPUT type="submit" value="Search"></TD>
		      <TD>
		      	<INPUT type="reset" value="Cancel">
		        <INPUT type="hidden" name="action" value="search">
		      </TD>
		    </TR>
		  </TBODY>
		</TABLE>
		</FORM>
		</BODY>
		</HTML>

EOT


	print $CGI->header('text/html');

	print processTemplate($params, $html_form);
}



=head2
__displayPendingMerchantList

=head3
Description:

	Displays a list of pre-registered merchants


=head3
Params:

params array of hashrefs	The hashref keys should match the field names in the database for the "potential_merchant_affiliates" table.

=head3
Returns:

	string on error

=cut

sub __displayPendingMerchantList
{
	my $params = shift;

	my $content = {};

	$content->{title} = 'Merchant  Bank Draft List' if ! exists $content->{title};
	$content->{message} = 'Your results are below.' if ! exists $content->{message};

	my $html_form = <<EOT;
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<HTML>
		<HEAD>
		<TITLE>%%title%%</TITLE>
		<style type="text/css">
			<!--
				.accepted, .accepted a {
					color:#CCCCCC;
					list-style-image: url(/images/checkmark4.gif);
					list-style-type: none;
				}
			-->
        </style>
		</HEAD>
		<BODY bgcolor="#eeffee" onload="document.forms[0].search_value.focus()">
		%%error%%
		%%warning%%
		%%notice%%
		<H2 align="center">%%title%%</H2>
		<P>%%message%%</P>
		<br />
		<TABLE border="0">
		  <TBODY>
		    <TR>
	       	  <TD>
				<form>
					<input type="hidden" name="approve" value="approve">
					%%content%%
					
					<br />
					<input type="submit" name="submit" id="submit" value="Submit" />
				</form>
		      </TD>
			 </TR>
		  </TBODY>
		</TABLE>
		</BODY>
		</HTML>

EOT




	#$content->{content} = '<ul>';
	$content->{error} = '';

	foreach my $row (@$params)
	{
		my $url = $CGI->url;

		$url .= '?search_field=pk&search_value=' . uri_escape($row->{pk});

		$row->{merchant_package} = '0' if (!$row->{merchant_package});
		
		
		#$content->{content} .= qq(<li>$row->{business_name}; $row->{firstname1} $row->{lastname1} <br />Routing Number: $row->{mpp_pay_routing_number}<br />Account Number: $row->{mpp_pay_account_number} <br />IBAN Number: $row->{mpp_pay_iban}<br />SWIFT Code: $row->{mpp_pay_swift}<br />Bank Name: $row->{mpp_pay_bank_name}<br />Bank Address: $row->{mpp_pay_bank_address}<br />Bank City: $row->{mpp_pay_bank_city}<br />Bank State/Province: $row->{mpp_pay_bank_state}<br />Bank Postal Code: $row->{mpp_pay_bank_postalcode}<br />Bank Country: $row->{mpp_pay_bank_country}<br />Merchant Package:  $row->{mpp_discount_type} <input type="checkbox" name="$row->{id}__approved" id="approved" value="$row->{id}"/>Approve</li>);
		
		my $package_upgrade_cost = 0;
		
		eval{
			
			my $MerchantAffiliates = MerchantAffiliates->new($DB);
			
			$package_upgrade_cost = $MerchantAffiliates->retrieveMerchantPackageCost('Offline',$row->{mpp_discount_type},$row->{country}); #retrieveMerchantPackageCost($merchant_type, $discount_type, $merchants_country)
			
		};
		if($@)
		{
			
			$content->{error} .= $@;
			
		}
		
		$content->{content} .= qq($row->{business_name}; $row->{firstname1} $row->{lastname1} Merchant Package:  $row->{mpp_discount_type}; Package Cost: $package_upgrade_cost <input type="checkbox" name="$row->{id}__approved" id="approved" value="$row->{id}"/>Approve<br /><br />);
		
	}
	
	#$content->{content} .= '</ul>';
	
	print $CGI->header('text/html');
	print processTemplate($content, $html_form);

	return;

}

=head2
__displayPendingMerchantForm

=head3
Description:

	Displays one pre-registered information so it is editable.


=head3
Params:

params hash	The keys should match the field names in the database for the "potential_merchant_affiliates" table.
			The country, and language dropdowns should be built before processing the template.

=head3
Returns:

	string on error

=cut

sub __displayPendingMerchantForm
{

	my $params = shift;

	eval{

		my $query_country_codes =<<EOT;
					SELECT
						country_name,
						country_code,
						restrictions
					FROM
						tbl_countrycodes

					ORDER BY country_name
EOT

		my $countries_hashref = {};
		my @countries = ();
		my $STH = $DB->prepare($query_country_codes);

		$STH->execute();

		while(my $row = $STH->fetchrow_hashref)
		{
			$countries_hashref->{$row->{country_code}} = $row->{country_name};
			push @countries, $row->{country_code};
		}

		$params->{country_dropdown} = $CGI->popup_menu(
				-name => 'country',
				-values => \@countries,
				-default => $params->{country},
				-labels => $countries_hashref
			);

	};
	if($@)
	{
			if (exists $params->{error})
			{
				$params->{error} .= '<br /> The country list was unavailable. ' . $@;
			} else {
				$params->{error} = 'The country list was unavailable. ' . $@;
			}
	}

	eval{

			my $query_languages = <<EOT;
						SELECT
							code2,
							description
						FROM
							language_codes
						WHERE
							code2 IS NOT NULL
						ORDER BY
							description
EOT

			my $language_hashref = {};
			my @languages = ();
			my $STH = $DB->prepare($query_languages);

			$STH->execute();

			while(my $row = $STH->fetchrow_hashref)
			{
				$language_hashref->{$row->{code2}} = $row->{description};
				push @languages, $row->{code2};
			}

			$params->{language_dropdown} = $CGI->popup_menu(
					-name => 'language_pref',
					-values => \@languages,
					-default => $params->{language_pref},
					-labels => $language_hashref
				);
	};
	if($@)
	{
			if (exists $params->{error})
			{
				$params->{error} .= '<br /> The language list was unavailable. ' . $@;
			} else {
				$params->{error} = 'The language list was unavailable. ' . $@;
			}
	}

	$params->{title} = 'Pending Merchant Affiliate Application' if ! exists $params->{title};
	$params->{message} = $params->{title} if ! exists $params->{message};
	$params->{action} = '' if ! exists $params->{action};

	my $html_form =<<EOT;

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>%%title%%</title>
		<link rel="stylesheet" href="/css/memberapp_default.css" type="text/css" />
		<script src="/js/resource.js" type="text/javascript"></script>
		<script src="/js/state_list.js" type="text/javascript"></script>
		<script src="/js/getstates.js" type="text/javascript"></script>
	</head>
	<body onload="adjustInitialStateCtlFocus();setSelectPrompt(&quot;- Please select from this list -&quot;);">
		%%error%%
		%%warning%%
		%%notice%%
		<div style="width:700px; padding: 0 5px;">
		  <h3 style="text-align: center;">%%title%%</h3>
		  <p>%%message%%</p>
		  <p>
		  <form method="get" action="%%action%%" onsubmit="return sent('')" enctype="multipart/form-data">

		  	<input type="hidden" value="1" name="processMerchantForm" id="processMerchantForm" />
		  	<input type="hidden" value="%%id%%" name="id" id="id" />

		    <table style="margin-right:auto; margin-left:auto;">
		      <tr>
		        <td class="field">Applicants Member ID, or Alias:<br />
		          <span class="ra">(If there is not one, one will be created on approval)</span></td>
		        <td class="input"><input type="text" class="main" name="id" onfocus="this.select()" value="%%id%%" />
		        </td>
		      </tr>

		      <tr>
		        <td class="field">Club Member ID:<br />
		          <span class="ra">(This will be created on approval if the member does not already have one.)</span></td>
		        <td class="input"><input type="text" class="main" name="member_id_assigned" onfocus="this.select()" value="%%member_id_assigned%%" /></td>
		      </tr>



		      <tr>
		        <td class="field">Referral ID Number:</td>
		        <td class="input"><input type="text" class="main" name="referral" onfocus="this.select()" value="%%referral%%" maxlength="200" /></td>
		      </tr>
		      <tr>
		        <td class="field">Personal First Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="firstname" onfocus="this.select()" value="%%firstname%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Personal Last Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="lastname" onfocus="this.select()" value="%%lastname%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Name of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="company_name" onfocus="this.select()" value="%%company_name%%" maxlength="100" /></td>
		      </tr>
		      <tr>
		        <td class="field">Address of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="address1" onfocus="this.select()" value="%%address1%%" maxlength="41" /></td>
		      </tr>
		      <tr>
		        <td class="field">City:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="city" onfocus="this.select()" value="%%city%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field">Country:<span class="ra">*</span></td>
		        <td class="input">%%country_dropdown%%</td>
		      </tr>
		      <tr>
		        <td class="field">State/Province:<span class="ra">*</span></td>
		        <td class="input"><select name="" class="main" style="display: none" id="state_select">
		            <option value=""></option>
		          </select>
		          <input type="text" class="main" name="state" onfocus="this.select();needs_country();" value="%%state%%" maxlength="50" style="display: inline" id="state_text" /></td>
		      </tr>
		      <tr>
		        <td class="field">Postal Code:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="postalcode" onfocus="this.select()" value="%%postalcode%%" maxlength="20" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Phone Number:</td>
		        <td class="input"><input type="text" class="main" name="phone" onfocus="this.select()" value="%%phone%%" maxlength="30" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Fax Number:</td>
		        <td class="input"><input type="text" class="main" name="fax_num" onfocus="this.select()" value="%%fax_num%%" maxlength="25" /></td>
		      </tr>
		      <tr>
		        <td class="field">Business Email Address:</td>
		        <td class="input"><input type="text" class="main" name="emailaddress2" onfocus="this.select()" value="%%emailaddress2%%" maxlength="100" /></td>
		      </tr>
		      <tr>
		        <td class="field">Prefered Language:<span class="ra">*</span></td>
		        <td class="input">%%language_dropdown%%</td>
		      </tr>
		      <tr>
		        <td class="field">Business Website Address (URL):</td>
		        <td class="input"><input type="text" class="main" name="url" onfocus="this.select()" value="%%url%%" /></td>
		      </tr>
		      <tr>
		        <td colspan="2" class="field">You must choose your Business Type from these categories. <img src="/images/arrow-right.gif" height="7" width="10" alt="-&gt;" /> <a href="javascript:void(0)" onClick="window.open('/cgi/biz_cat_search.cgi')">Search Business Categories</a> <span class="ra">*</span></td>
		      </tr>
		      <tr>
		        <td class="field">Type of Business:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="biz_description" onfocus="this.select()" value="%%biz_description%%" maxlength="50" />
		          <input type="hidden" value="%%business_type%%" name="business_type" id="business_type" /></td>
		      </tr>
		      <tr>
		        <td class="field">Accepted:</td>
		        <td class="input">
		        	<input type="checkbox" name="accepted" value="1" %%accepted_checked%% /> %%accepted%%
		        </td>
		      </tr>
		      <tr>
		        <td class="field">Contact Name:<span class="ra">*</span></td>
		        <td class="input"><input type="text" class="main" name="contact_info" onfocus="this.select()" value="%%contact_info%%" maxlength="50" /></td>
		      </tr>
		      <tr>
		        <td class="field" valign="top">Notes:</td>
		        <td class="input"><textarea name="notes" cols="40" rows="10" class="main" onfocus="this.select()">%%notes%%</textarea></td>
		      </tr>
		      <tr>
		        <td>
			        <input type="hidden" value="%%ipaddr%%" name="ipaddress" id="ipaddress" />
			        <input type="hidden" value="%%submitted%%" name="signup_date" id="signup_date" />
			        <input type="hidden" value="Pre-registered Merchant Application" name="landing_page" id="landing_page" />
		        <br />
		          <br />
		          <br />
		          <br /></td>
		        <td class="input">
		          <input type="submit" class="btn" value="Submit" />
		          &nbsp;&nbsp;
		          <input type="reset" class="btn" onclick="restoreStateFocus()" value="Reset" />
		        </td>
		        <tr>
			        <td class="field">Permanently Delete:</td>
			        <td class="input">
		        		<input type="checkbox" name="delete" value="1" />
		        	</td>
		      </tr>
		      </tr>
		    </table>
		  </form>
		  </p>
		  <p>&nbsp;</p>
		</div>
	</body>
</html>

EOT







	$params->{accepted_checked} = $params->{accepted} ? 'checked':'';

	print $CGI->header('text/html');
	print processTemplate($params, $html_form);

	return;
}

=head2
processPendingMerchantForm

=head3
Description:

	


=head3
Params:

	

=head3
Returns:

	

=cut

sub processPendingMerchantForm
{
	use Members;
	use Encode;
	
	my $params = shift;


		my $search_field = 'pk';
		my $search_value = $CGI->param('pk');
		my $potential_merchant = {};

		my $submitted_data = {};


		$submitted_data->{pk} 					= $CGI->param('pk') ? decode_utf8($CGI->param('pk')) : '';
		$submitted_data->{id} 					= $CGI->param('id') ? decode_utf8($CGI->param('id')) : '';
		$submitted_data->{referral} 			= $CGI->param('referral') ? decode_utf8($CGI->param('referral')) : '';
		$submitted_data->{firstname}		 	= $CGI->param('firstname') ?  decode_utf8($CGI->param('firstname')) : '';
		$submitted_data->{lastname} 			= $CGI->param('lastname')  ?  decode_utf8($CGI->param('lastname')) : '';
		$submitted_data->{company_name} 		= $CGI->param('company_name') ?  decode_utf8($CGI->param('company_name')) : '';
		$submitted_data->{address1} 			= $CGI->param('address1')  ?  decode_utf8($CGI->param('address1')) : '';
		$submitted_data->{city} 				= $CGI->param('city') ? decode_utf8($CGI->param('city')) : '';
		$submitted_data->{state} 				= $CGI->param('state') ? decode_utf8($CGI->param('state')) : '';
		$submitted_data->{country} 				= $CGI->param('country') ? decode_utf8($CGI->param('country')) : '';
		$submitted_data->{postalcode} 			= $CGI->param('postalcode') ? decode_utf8($CGI->param('postalcode')) : '';
		$submitted_data->{phone} 				= $CGI->param('phone') ? decode_utf8($CGI->param('phone')) : '';
		$submitted_data->{fax_num} 				= $CGI->param('fax_num') ? decode_utf8($CGI->param('fax_num')) : '';
		$submitted_data->{emailaddress2} 		= $CGI->param('emailaddress2') ? decode_utf8($CGI->param('emailaddress2')) : '';
		$submitted_data->{url} 					= $CGI->param('url') ? decode_utf8($CGI->param('url')) : '';
		$submitted_data->{biz_description} 		= $CGI->param('biz_description') ? decode_utf8($CGI->param('biz_description')) : '';
		$submitted_data->{contact_info} 		= $CGI->param('contact_info') ? decode_utf8($CGI->param('contact_info')) : '';
		$submitted_data->{language_pref} 		= $CGI->param('language_pref') ? decode_utf8($CGI->param('language_pref')) : '';
		#$submitted_data->{submitted} 			= $CGI->param('submitted') ? $CGI->param('submitted') : 'NULL';
		$submitted_data->{accepted} 			= $CGI->param('accepted') ? decode_utf8($CGI->param('accepted')) : '';
		$submitted_data->{notes} 				= $CGI->param('notes') ? decode_utf8($CGI->param('notes')) : '';
		$submitted_data->{member_id_assigned} 	= $CGI->param('member_id_assigned') ? decode_utf8($CGI->param('member_id_assigned')) : 'NULL';
		$submitted_data->{business_type} 		= $CGI->param('business_type') ? decode_utf8($CGI->param('business_type')) : 'NULL';

		$submitted_data->{ipaddress}			= $CGI->param('ipaddress') ? decode_utf8($CGI->param('ipaddress')): '';
		$submitted_data->{signup_date}			= $CGI->param('signup_date') ? decode_utf8($CGI->param('signup_date')): '';
		$submitted_data->{landing_page}			= $CGI->param('landing_page') ? decode_utf8($CGI->param('landing_page')): '';


		$search_field =~ s/^\s+|\s+$//g;
		$search_value =~ s/^\s+|\s+$//g;

		if($CGI->param('delete'))
		{
			my $return_value = 0;
			my $notice = 'The Pre-registered Merchant record was removed. ';
			
			eval{
				my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);
				$return_value = $PotentialMerchantAffiliates->deleteMerchantRecord($submitted_data->{pk});
			};
			if($@)
			{
				displaySearchForm({error=>"There was an error processing the request.  Please try again, or contact the Internet Division. Error: " . $@});	
			}
			
			if($return_value)
			{
				$notice = 'There was an issue removing the requested Pre-registered Merchant.  Error: ' . $return_value;
			}
			
			displaySearchForm({notice=>$notice});
			
			return;
		}
		
		my $updated = {};

		eval{

			my $Members = Members->new($DB);
			my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);


			$updated = $PotentialMerchantAffiliates->updatePotentialMerchantInformation($submitted_data);

			$potential_merchant = $PotentialMerchantAffiliates->getPotentialMerchant({$search_field=>$search_value});


			if(defined $updated->{updated} && $updated->{updated})
			{
				$potential_merchant->[0]->{notice} = "The information was updated";
			} else {
				$potential_merchant->[0]->{notice} = "The information was not updated";
			}


			if(defined $updated->{new_member_info}->{member_id} && $updated->{new_member_info}->{member_id})
			{
				$potential_merchant->[0]->{notice} .= "<br />A new x membership was created for this member";

				if(! defined $PotentialMerchantAffiliates->{Members})
				{
					$PotentialMerchantAffiliates->setMembers();
				}

				if(!$Members->confirmMembership({id=>$updated->{new_member_info}->{member_id}}))
				{
					$potential_merchant->[0]->{notice} .= "<br />The member ship was confirmed.";
				} else {
					$potential_merchant->[0]->{warning} .= "<br />We were not able to confirm the membership.  <br />The members account is not active.<br />";
				}

				$PotentialMerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$updated->{new_member_info}->{member_id}, field=>'oid', force=>1});
			}



			__displayPotentialMerchantForm($potential_merchant->[0]);

	};
	if($@)
	{
	
		displaySearchForm({error=>"There was an error processing the request.  Please try again, or contact the Internet Division. $updated -- Error: " . $@});
	}




}


sub processApprovedMerchants
{
	
	print $CGI->header('text/html');
	
	
	print "<h1>Your Results are Below.</h1><br />\n";
	print "<a href = '/cgi/admin/$FindBin::Script'>Start a new Serch</a><br /><br />\n";
	
	
	my %approved_merchants = ();
	
	
	foreach ($CGI->param)
	{
		
		my ($merchant_id, $field_name) = split(/__/, $_);
		
		next if ! $field_name;
		
		$approved_merchants{$merchant_id}{$field_name} = $CGI->param($_);
		
	}
	
	
	
	
	
	#print Dumper(%approved_merchants) . "\n";
	
	my $MerchantAffiliates = {};

	eval{
		
		$MerchantAffiliates = MerchantAffiliates->new($DB);
	};
	if($@)
	{
		print $@;
		
	}
		# The key is the pending ID.
		foreach (keys %approved_merchants)
		{
			next if $_ !~ /^\d+$/;
			
			if (defined $approved_merchants{$_}{approved} && $approved_merchants{$_}{approved})
			{
				
				my $merchant_info = $MerchantAffiliates->retrieveMerchantMasterRecordByID($_, {with_pending_payment_information=>1});
				
				my %merchant_hash = ();
				
				
				#print Dumper($merchant_info) . "\n\n";
				
				
				foreach (keys %$merchant_info)
				{
					delete 	$merchant_info->{$_} if ! defined $merchant_info->{$_};
				}
				
				
				#$merchant_hash{merchant_package} = $merchant_info->{merchant_package} if defined $merchant_info->{merchant_package};
				$merchant_hash{pay_payment_method} = uc($merchant_info->{mpp_pay_payment_method}) if $merchant_info->{mpp_pay_payment_method};
				
				$merchant_hash{pay_account_number} = $merchant_info->{mpp_pay_account_number} if defined $merchant_info->{mpp_pay_account_number};
				$merchant_hash{pay_iban} = $merchant_info->{mpp_pay_iban} if defined $merchant_info->{mpp_pay_iban};
				$merchant_hash{pay_swift} = $merchant_info->{mpp_pay_swift} if defined $merchant_info->{mpp_pay_swift};
				$merchant_hash{pay_routing_number} = $merchant_info->{mpp_pay_routing_number} if defined $merchant_info->{mpp_pay_routing_number};
				
				$merchant_hash{discount_type} = $merchant_info->{mpp_discount_type} if defined $merchant_info->{mpp_discount_type};
				
				$merchant_hash{pay_bank_name} = $merchant_info->{mpp_pay_bank_name} if defined $merchant_info->{mpp_pay_bank_name};
				$merchant_hash{pay_bank_address} = $merchant_info->{mpp_pay_bank_address} if defined $merchant_info->{mpp_pay_bank_address};
				$merchant_hash{pay_bank_city} = $merchant_info->{mpp_pay_bank_city} if defined $merchant_info->{mpp_pay_bank_city};
				$merchant_hash{pay_bank_state} = $merchant_info->{mpp_pay_bank_state} if defined $merchant_info->{mpp_pay_bank_state};
				$merchant_hash{pay_bank_postalcode} = $merchant_info->{mpp_pay_bank_postalcode} if defined $merchant_info->{mpp_pay_bank_postalcode};
				$merchant_hash{pay_bank_country} = $merchant_info->{mpp_pay_bank_country} if defined $merchant_info->{mpp_pay_bank_country};
				
				#Clear unused fields.
				$merchant_hash{ca_number} = '';
				$merchant_hash{ec_number} = '';
				$merchant_hash{cc_type} = '';
				$merchant_hash{pay_cardnumber} = '';
				$merchant_hash{pay_cardcode} = '';
				$merchant_hash{pay_cardexp} = '';
				$merchant_hash{pay_name} = '';
				$merchant_hash{pay_address} = '';
				$merchant_hash{pay_city} = '';
				$merchant_hash{pay_country} = '';
				$merchant_hash{pay_state} = '';
				$merchant_hash{pay_postalcode} = '';
				
				
				eval{
					
					$MerchantAffiliates->{db}->begin_work();
					
					$MerchantAffiliates->updateMerchantMasterInformation($_,\%merchant_hash);
					$MerchantAffiliates->deleteMerchantPaymentPendingRecord($_);
					
					$MerchantAffiliates->{db}->commit();
					
					print "$merchant_info->{business_name}; $merchant_info->{firstname1} $merchant_info->{lastname1}<br /><br />\n\n";
					
				};
				if($@)
				{
					print "There was an error upgrading Merchant ID $_<br />\nError: $@ <br /><br />\n\n";
					$MerchantAffiliates->{db}->rollback();
				}
				
			}
		}

	
	
	

	
	
}


################################################
# displayPreregisteredMerchantInformation
# Params NONE the $CGI->params attritute is used.
#
#
################################################
sub displayPendingMerchantInformation
{
	
	if((! $CGI->param('search_field') || ! $CGI->param('search_value')) && $CGI->param('search_field') ne 'all')
	{
		displaySearchForm({notice=>'No search criteria was specified.'});
		return;
	}
	
	eval{
		
		my $search_field = $CGI->param('search_field');
		my $search_value = $CGI->param('search_value');
		
		$search_field =~ s/^\s+|\s+$//g;
		$search_value =~ s/^\s+|\s+$//g;
		
		my $MerchantAffiliates = MerchantAffiliates->new($DB);
		
		my $potential_merchant = [];
		
		if($search_field eq 'all')
		{
			my $query =<<EOT;
				SELECT
					mam.*,
					mpp.id AS mpp_id,
					mpp.ca_number AS mpp_ca_number,
					mpp.status AS mpp_status,
					mpp.discount_type AS mpp_discount_type
				FROM
					merchant_affiliates_master mam
					JOIN
					merchant_payment_pending mpp
					ON
					mam.id = mpp.merchant_id
					AND
					mpp.status = 0
EOT

			my $sth = $DB->prepare($query);
			$sth->execute();
			
			while (my $row = $sth->fetchrow_hashref)
			{
				push @$potential_merchant, $row;
			}
			
		}
		else 
		{	
			$potential_merchant = $MerchantAffiliates->retrieveMerchantMasterRecordByID($search_value,{with_pending_payment_information=>1});
		}
		
#		use Data::Dumper;
#		displaySearchForm({'error'=>$potential_merchant->[0]->{stuff}});
		
		if(ref $potential_merchant eq 'HASH' && defined $potential_merchant->{mpp_id})
		{
			__displayPendingMerchantList([$potential_merchant]);	
		}
		elsif(ref $potential_merchant eq 'ARRAY' && defined $potential_merchant->[0]->{mpp_id})
		{
			__displayPendingMerchantList($potential_merchant);
		} else {
			
			displaySearchForm({notice=>'There are no result, try again.'});
			
		}
	};
	if($@)
	{
		displaySearchForm({'error'=>$@});
	}

}

################################################
# exitScript This came about because Bill
# was/is using goto statements.
################################################
sub exitScript
{
	$DB->disconnect if($DB);
	exit(0);
}

##################################################################################
# Main
##################################################################################

#########################################
# Connect to the DB
#########################################
unless ($DB = ADMIN_DB::DB_Connect( 'ma_real_admin.cgi', $operator, $pwd ))
{
	exit;
}

$DB->{RaiseError} = 1;

	if($CGI->param('processMerchantForm'))
	{
		processPendingMerchantForm();
	}
	elsif($CGI->param('search_field') || $CGI->param('search_value'))
	{
		displayPendingMerchantInformation();
	}
	elsif($CGI->param('approve'))
	{
		processApprovedMerchants();
	}
	else
	{
		displaySearchForm();
	}


exitScript();









