#!/usr/bin/perl -w
# HTMLcontent.cgi
# Main Search page to edit cgi_objects.
# they can then be edited or made inactive.
#
# Written by Stephen Martin
# August 20th  2002
#
###### last modified 09/19/12	Bill MacArthur

=head1 HTMLcontent.cgi

=for Commentary

Main Search page to edit cgi_objects.
they can then be edited or made inactive.

Written by Stephen Martin
August 20th  2002

=cut

use strict;
use DBI;
use CGI qw(:cgi);
use Carp;
use URI::Escape;
use CGI::Carp qw/fatalsToBrowser/;
		
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data);    ###### database variables
my $rv;
my $operator;
my $query;
my $action;
my $counter;
my $newoffset;
my $backoffset;
my $lastoffset = 0;
my $last_template = 0;
my $newlink;
my $prevlink;
my $lastlink;
my $menu1 = '';
my $menu2 = '';
my $inputwidth = '';
my $inputstyle = '';
my $obj_id = ''; 
my $obj_name = ''; 
my $obj_description = '';
my $obj_type = '';
my $obj_translation_key = '';
my $spawn = '';
my $forward = '';
my $back = '';
my $last = '';
my $editor = "./cgi_Object_Editor.cgi";
my $self = "./HTMLcontent.cgi";
my $recovery = "./cgi_Object_Recovery.cgi";
my $DISPLAY = 100;
my $button = 0;
my $button1 = 0;

my $cgi = new CGI();

my $criteria1 = $cgi->param('criteria1') || '';
my $criteria2 = $cgi->param('criteria2') || '';
my $criteria3 = $cgi->param('criteria3') || '';
my $offset = $cgi->param('offset') || 0;

my $URLcriteria1=uri_unescape($criteria1);
my $URLcriteria2=uri_unescape($criteria2);
my $URLcriteria3=uri_unescape($criteria3);

if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ )
{
 $spawn="\'.\/OEhelp3.cgi\',\'_OEH3_\',\'width=400,height=230,scrollbars=no\'";
} else {
 $spawn="\'.\/OEhelp3.cgi\',\'_OEH3_\',\'width=500,height=250,scrollbars=no\'";
}

print $cgi->header();

# Here is where we get our admin user and try logging into the DB

$operator = $cgi->cookie('operator');
my $pwd = $cgi->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

$menu1 = gen_menu("criteria2","cgi_object_types","id","type"); 
$menu2 = gen_menu("criteria3","cgi_object_functions","id","process_name");


if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ )
{
 $inputwidth=32;
 $inputstyle=" style=\"width:180px;\"";
} else {
 $inputwidth=20;
}

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
<meta http-equiv="pragma" content="no-cache"> 
<meta http-equiv="Expires" content="Tue, 26-Oct-1965 12:00:00"> 
<meta http-equiv="Expires" content="NOW"> 
<!--meta http-equiv="last modified" content="NOW"--> 

<link href="/css/memberinfo.cgi.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/js/jquery-min.js"></script>

<script type="text/javascript">
<!--
function spawn(URL,Name,features)
 { 
  window.open(URL,Name,features);
 }
//-->
</script>

<title>DHSC Template Management</title>
</head>
<body bgcolor="#eeffee" onLoad="document.forms[0].criteria1.focus()">
<form action="$self" method="post" name="sform1">
<div style="text-align:center">
<b>DHSC Template Management</b><br /><br />
</div>
<table border="0">
 <tr>
  <td align="left" colspan="2" valign="top">
   <font style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
Please enter the name (or part of the name), part of the description ( a keyword ) or the obj_id number of the template to edit.
   </font>
  </td>
  <td align="left" colspan="2"  valign="top" nowrap>
     <input type="text" name="criteria1" size="$inputwidth" maxlength="128" 
      $inputstyle>&nbsp;&nbsp;&nbsp;<input type="submit" name="action" value="Search"
      class="in"> 
  </td>
 </tr>
  <tr>
  <td align="center" colspan="4" height="10">&nbsp;</td>
 </tr>
 <tr>
  <td nowrap colspan="2" width="50%"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Search by Template Type<br />
  $menu1</td>
  <td nowrap colspan="2" width="50%"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">Search by Program Name<br />
  $menu2</td>
 </tr>
 <tr>
  <td align="center" colspan="4">
   <hr />
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
Please select a template to edit from the list, by clicking on its hyper-link'd ID number.&nbsp;&nbsp;&nbsp;
          <a href="#" onClick="spawn($spawn);">
<img src="/images/talkbubble3_query.gif" width="24" height="24" border="0" alt="HELP! Click Me!" /></a>
    </div>
    <hr />

  </td>
 </tr>
</table>
<p class="fp" style="margin:0 0 1em 12em">TK = Translation Key</p>
<table border="0" cellpadding="3" cellspacing="1" bgcolor="#eeeeee" id="mainTbl">
\n~;

# Get the number of the last template for the 'End of List' button.
$qry = "SELECT count(*)
	FROM cgi_objects
	WHERE active = true";
$sth = $db->prepare($qry);
$rv = $sth->execute();
$last_template = $sth->fetchrow_array;

$query = determine_query($criteria1,$criteria2,$criteria3);

# print "<br><b>Debug $query </b><br>";

$sth = $db->prepare($query);

$rv = $sth->execute();
defined $rv or die $sth->errstr;

my ($lastmodified, $bywhom) = ();
$sth->bind_columns(undef, \$obj_id,\$obj_name,\$obj_description,\$lastmodified,\$bywhom,\$obj_type,\$obj_translation_key);

$counter = 0;
my $row_color = '#ffffee';
while   ($sth->fetch() )
{

	# $obj_name =~ s/[\W]/\&nbsp\;/g;

# nowadays we have wide, high rez monitors, so let's show it all
#	$obj_description = substr($obj_description,0,120);

	$obj_description = CGI::escapeHTML($obj_description);

	if (($counter >= $offset) && ($counter <= ($offset + $DISPLAY - 1)))
	{
		print qq~
		<!-- DB Call -->
		<tr bgcolor="$row_color">
		<td valign="top" class="objid">
		<a href="$editor\?obj_id=$obj_id;offset=$offset;criteria1=$URLcriteria1;criteria2=$URLcriteria2;criteria3=$URLcriteria3">$obj_id</a>
		<br /><i>$obj_type</i>
		</td>
		<td class="fp">${\($obj_translation_key ? qq|<a href="/cgi/admin/translation_templates.cgi?action=view;trans_type=1;obj_key=$obj_id">TK</a>| : '')}</td>
		<td valign="top" class="fp">$obj_name</td>
		<td valign="top" class="fp">$obj_description</td>
		<td class="fp" nowrap>$bywhom<br />$lastmodified</td>
		<td class="fp">
		<a title="Restore" href="$recovery\?obj_id=$obj_id;offset=$offset;criteria1=$URLcriteria1;criteria2=$URLcriteria2;criteria3=$URLcriteria3"><img src="/images/undo.gif" height="15" width="15" border="0" alt="" /></a>
		</td>
		</tr>
		<!-- DB Call -->
		\n~;

	}
	$counter++;
	$row_color = ($row_color eq '#ffffee') ? '#eeeeee' : '#ffffee';
}

$rv = $sth->finish();

print "</table></form>\n";

$button=$button||( $counter > ($offset+$DISPLAY));
$button1= $offset;
$backoffset = $offset - $DISPLAY;		# The previous page of templates.
$newoffset = $offset + $DISPLAY;		# The next page of templates.
$lastoffset = $last_template - $DISPLAY;	# The last template number in the list.

$newlink=$self . "?offset=" . $newoffset . "\;criteria1=" . $URLcriteria1  . "\;criteria2=" . $URLcriteria2 . "\;criteria3=" . $URLcriteria3;
$prevlink=$self . "?offset=" . $backoffset . "\;criteria1=" . $URLcriteria1  . "\;criteria2=" . $URLcriteria2 . "\;criteria3=" . $URLcriteria3;
$lastlink=$self . "?offset=" . $lastoffset . "\;criteria1=" . $URLcriteria1  . "\;criteria2=" . $URLcriteria2 . "\;criteria3=" . $URLcriteria3;
         
if ( $button )
{
          $forward = "<a href=\"$newlink\"><img src=\"https:\/\/www.clubshop.com\/admin\/images\/right2.gif\" border=\"0\" alt=\"More Results\"></a>";
          $last = "<a href=\"$lastlink\"><img src=\"https:\/\/www.clubshop.com\/admin\/images\/last2.gif\" border=\"0\" alt=\"Last Page of Results\"></a>";
}

if ( $button1 ) 
{
          $back    = "<a href=\"$prevlink\"><img src=\"/admin/images/left2.gif\" border=\"0\" alt=\"Previous Results\" /></a>";
}

if ($button || $button1) 
{
	print qq~
            $back&nbsp;&nbsp;&nbsp;&nbsp;$forward&nbsp;&nbsp;&nbsp;&nbsp;$last&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/cgi/admin/HTMLcontent.cgi"><img src="/admin/images/TMPhome.gif" border="0" alt="Template Management Page" /></a>
          \n~;
}
else
{
	print qq~
            <a href="/cgi/admin/HTMLcontent.cgi"><img src="/admin/images/TMPhome.gif" border="0" alt="" /></a>
          \n~;
}
print q|
<script type="text/javascript">
 
$("#mainTbl tr").hover(
  function () {
    $(this).css("background","yellow");
  }, 
  function () {
    $(this).css("background","");
  }
);
 
</script>
|;

print "</body></html>";


$db->disconnect;

exit;

sub Err
{
	my $q = new CGI;
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub gen_menu
{
    my ( $menu_name, $table_name, $index_name, $desc_name ) = @_;
   
    my $dex;
    my $desc;
    my $html;

	$query = "	SELECT $index_name,
				$desc_name
			FROM 	$table_name\n";


	###### exclude inactive 'functions'
	if ($menu_name eq 'criteria3')
	{
		$query .= 'WHERE active= TRUE';
	}

  $sth = $db->prepare("$query ORDER BY $desc_name");

  $rv = $sth->execute();
  defined $rv or die $sth->errstr;

  $sth->bind_columns(undef, \$dex,\$desc);

  $html = "<SELECT name='$menu_name'>\n";
  $html = $html . "<OPTION></OPTION>";
   
  while   ($sth->fetch() )
  {

   $html = $html . "<OPTION value=\'$dex\'>$desc</OPTION>\n";
  }    

  $html = $html . "</SELECT>\n";

  $rv = $sth->finish();

  return $html;  
}

###### Determine which query to run based on the criteria entered for the search.
sub determine_query
{
	# Criteria 1 - ID number , name, or description.
	my ( $op1 ) = $_[0];
	shift;
	# Criteria 2 - Template type.
	my ( $op2 ) = $_[0];
	shift;
	# Criteria 3 - Program name.
	my ( $op3 ) = $_[0];
 
	my $sql; 
	my $numop1 = 0;		###### Karl added the initialization here on 05/28/03

	$op1 = lc $op1;

	$numop1 = getnum($op1);
######  Returning -1 instead of undef from getnum(), since the return value was being immediately 
######  tested for undef and assigned -1 if true.
#    if ( $numop1 == undef ) 
#    {
#     $numop1 = '-1';
#    }

	my $SQL_base = "
		SELECT co.obj_id,
			co.obj_name,
			obj_description,
			COALESCE(co.stamp::TIMESTAMP(0)::TEXT, '&nbsp;') AS last_modified,
			COALESCE(co.operator, '&nbsp;') AS operator,
			cot.\"type\",
			co.translation_key\n";
	
	# If all three criteria were defined.
	if ( $op1 && $op2 && $op3 ) 
	{
		$sql = "
			$SQL_base
			FROM cgi_objects_with_extended_function_mapping co, cgi_object_types cot
			WHERE  (
				LOWER(co.obj_name) LIKE '\%$op1\%'
				OR LOWER(co.obj_description) LIKE '\%$op1\%'
				OR co.obj_id = $numop1
				)
			AND    co.obj_caller = \'$op3\'
			AND 	co.obj_type = \'$op2\'
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";

		return $sql;
	}

	if ( $op1 && $op2 )
	{
		$sql = "
			$SQL_base
			FROM cgi_objects co, cgi_object_types cot
			WHERE  (
				LOWER(co.obj_name) LIKE '\%$op1\%'
				OR LOWER(co.obj_description) LIKE '\%$op1\%'
				OR co.obj_id= $numop1
			)
			AND    co.obj_type = '$op2'
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";
   
		return $sql;
	}

	if ( $op1 && $op3 )
	{

		$sql = "
			$SQL_base

			FROM cgi_objects_with_extended_function_mapping co, cgi_object_types cot
			WHERE  (
				LOWER(co.obj_name) LIKE '\%$op1\%'
				OR LOWER(co.obj_description) LIKE '\%$op1\%'
				OR co.obj_id = $numop1
				)
			AND    co.obj_caller = '$op3'
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";

		return $sql;
	}

	if ( $op2 )
	{
		$sql = "
			$SQL_base

			FROM cgi_objects co, cgi_object_types cot     
			WHERE 	co.obj_type = '$op2'
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";

		return $sql;
	}

	if ( $op3 )
	{
		$sql = "
			$SQL_base

			FROM cgi_objects_with_extended_function_mapping co, cgi_object_types cot
			WHERE 	co.obj_caller = '$op3'
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";

		return $sql;
	}
 
	if ( $op1 )
	{
		$sql = "
			$SQL_base

			FROM cgi_objects co, cgi_object_types cot
			WHERE  (
				LOWER(co.obj_name) LIKE '\%$op1\%'
				OR LOWER(co.obj_description) LIKE '\%$op1\%'
				OR co.obj_id = $numop1
				)
			AND 	co.active= TRUE
			AND 	co.obj_type=cot.id
			ORDER BY co.obj_id";

		return $sql;
	}

	# If no criteria were specified, get all the objects for display.
	$sql = "
		$SQL_base

		FROM cgi_objects co, cgi_object_types cot
		WHERE 	co.active= TRUE
		AND 	co.obj_type=cot.id
		ORDER BY co.obj_id";
	return $sql;
}

		
sub getnum{
 use POSIX qw(strtod);
 my $str = shift;
 $str =~ s/^\s+//;
 $str =~ s/\s+$//;

 $! = 0;
 
 my ($num, $unparsed) = strtod($str);

 if (( $str eq '' ) || ($unparsed != 0) || $! )
 {
###### Replaced the undef with the -1 return, since the return value was being immediately 
######  tested for undef and assigned -1 if true.
# return undef;
 return '-1';
 } else {
 return $num;
 } 
}

###### 10/03/02 added a focus action on page load
###### 01/08/02 Exclude Void Templates when extracting for a know application name.
###### 03/14/03 excluded inactive templates from the opening screen
###### also added the -w switch
###### 05/28/03 Added several variable initializations to suppress errors to the web log.
###### 06/10/03 revised the SQL statements so that inactive templates would not show up in the results except for a direct object ID query
###### 08/27/03 revised yet another SQL statement, also added the Err() routine for debugging
###### 09/15/03 revised to exclude inactive functions from the drop-down list
###### 08/20/04 many formatting tweaks including inclusion of the operator and 
###### last modified date
###### 10/25/04 added the object type to the output
# 05/19/10 added a hyperlink to the translations interfac on the TK label for translations in list mode
# 09/19/12 changed to a VIEW of cgi_objects to gain the value of the one-to-many relationships table mapping objects to callers (functions)
