#!/usr/bin/perl -w
###### member_comm_status.cgi
######
###### This script allows staff to set a VIP's commission to be suspended or forfeited.
######
###### Created: 04/26/2004	Karl Kohrt
######
###### Last modified: 

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS

###### our page templates
our %TMPL = ( 'main'	=> 10288 );

###### our tables
our %TBL = (	'status'	=> 'member_comm_status' );

our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $id = ( $q->param('id') || '' );
our $status = ( $q->param('status') || '' );
our $notes = ( $q->param('notes') || '' );
our @base_list = ();
our $db = '';
our $message = '';

# here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'member_comm_status.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

##################################
###### MAIN starts here.
##################################

# If a member ID was passed into the script.
if ( $id )
{
	if ($action eq 'create')
	{
		if ( Create() )
		{
			$message = "<span class=\"b\">Status Successfully Changed.<BR></span>\n";
		}
		else { Err('Status Change Failed. Try again.') }
	}
	Get_Data();
	Display_Results();
}
else { Err('No member ID was passed. Go back to the Member Info page and try again.') }

$db->disconnect;
exit(0);

###### ###### ###### ###### ######
###### Subroutines start here.
##################################

###### Create a new record with the information from the form.
sub Create
{
	my $qry = '';
	my $qry_end = '';
	my $sth = '';
	my $rv = '';
	my @list = ();
	my $first_field = 1;

	# Change the current status' flag to 0 (not current). 							
#	$qry = "UPDATE $TBL{status}
#		SET 	active = 0
#		WHERE 	current = 1
#		AND 	member_id = ?;";
#	$sth = $db->prepare($qry);
#	$rv = $sth->execute( $id );
#	$sth->finish;

	# Return if we were unable to update the current status record.
#	unless ( $rv ) { return $rv }
	
	# Insert the new, current status.
	$qry = "DELETE FROM $TBL{status} WHERE member_id= ?;

		INSERT INTO $TBL{status}
			(	member_id,
				comm_status,
				stamp,
				operator,
--				current,
				notes
			) VALUES (
				?,
				?,
				NOW(),
				CURRENT_USER,
--				1,
				?
			);";

#	$sth = $db->prepare($qry);
#Err("qry= $qry<BR>notes= $notes");  
#	$rv = $sth->execute( $id, $id, $status, $notes );
	$db->do($qry, undef, $id, $id, $status, $notes);
#	$sth->finish;
#	return $rv; 
	return 1 unless $db->errstr;
}

###### Display the list of records returned from the Get_Data subroutine.
sub Display_Results
{
	my $list = '';
	my $first_one = 1;
	my $header = '';
	my $class = 'current';

	my $tmpl = G_S::Get_Object($db, $TMPL{main}) || die "Couldn't open $TMPL{main}";

	# If we have records to display.
	if ( scalar @base_list >= 1 )
	{
		$message .= "<span class=fp>Current status is shown in bold.<br /></span>\n";
		$header .= "<tr>";

		# Display the various lines of data returned from the query.
		foreach my $result (@base_list)
		{
			$list .= "<tr>";
			# Display all the fields in a row.				
			foreach my $field_name ( keys %{$result} )
			{
				$result->{$field_name} = $result->{$field_name} || '&nbsp;';
				$list .= "<td class=\"$class\">$result->{$field_name}</td>";
				if ( $first_one ) { $header .= "<td><b>$field_name</b></td>" }
			}
			$list .= "</tr>\n";
			$first_one = 0;
			$class = "fp";
		}
		$header .= "</tr>\n";
	}
	else 
	{
		unless ( $message eq '' )
		{
			$message .= "<br />";
		}						
		$message = "<span class=\"b\">No records found. Therefore, member #$id is set to be paid.<br /></span>\n";
	}

	$tmpl =~ s/%%ID%%/$id/g;
	$tmpl =~ s/%%HEADER%%/$header/;
	$tmpl =~ s/%%HISTORY%%/$list/;
	$tmpl =~ s/%%MESSAGE%%/$message/;
	$tmpl =~ s/%%ME%%/$ME/g;
	print $q->header(-expires=>'now'), $tmpl;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<br />$_[0]<br />";
	print $q->end_html();
}

###### Gets the data from the table and loads it into an array.
sub Get_Data
{
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my @result_list = ();	

	$qry = "	SELECT *
			FROM $TBL{status}
			WHERE member_id = ?

			UNION ALL
			SELECT * FROM member_comm_status_arc WHERE member_id= ?
			ORDER BY stamp DESC";

	$sth = $db->prepare($qry);

	$rv = $sth->execute($id, $id);
	while ( my $data = $sth->fetchrow_hashref )
	{
		push (@base_list, $data);
	}		

	$sth->finish;
	return scalar @base_list;
}

