#!/usr/bin/perl -w
###### cbid_batch_mgt.cgi
###### create/reconcile CBID batches
###### last modified 05/15/03	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS
my $rptScript = 'https://www.clubshop.com/cgi/admin/cbid_batch_rpt.cgi?search_field=id&action=search&search_value=';
my $CBIDrptScript = 'https://www.clubshop.com/cgi/admin/cbid_info.cgi?search_field=id&action=search&search_value=';

my %TMPL = (	front			=> 10161,
		reconcile_errors	=> 10162);

###### these are generic messages that can be customized with added info later as necessary	
my %err_msgs = (	batch_not_found	=> 'That batch was not found',
			batch_start_mismatch	=> 'The start CBID of the batch doesn`t match your submitted start #.',
			batch_end_mismatch	=> 'The end CBID of the batch doesn`t match your submitted end #.',
			existing_cbid_match	=> 'Existing CBIDs were found that fall within the range of this batch.',
			batch_overlap		=> 'There appears to be a CBID overlap with another Batch.',
			already_reconciled	=> 'This Batch has already been reconciled.',
			batch_voided		=> 'The selected Batch has been VOIDED'
		);

###### the table we'll get data from
my $TBL = 'cbid_batches';

my $q = new CGI;
my $ME = $q->url;
my $action = $q->param('action') || '';
my $start = $q->param('start');
my $end = $q->param('end');
my $range = $q->param('range');
my $batch_ID = $q->param('batchID');
my $description = $q->param('description');
my $notes = $q->param('notes') || '';
my ($db, $match, $diff, @errors) = ();

my @char_fields = '=description';
my @int_fields = qw/start end/; 

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'cbid_batch_mgt.cgi', $operator, $pwd )){ exit }
$db->{RaiseError} = 1;

unless ($action)
{
	Do_Page();
}

elsif ($action eq 'submit')
{
	unless ( $description )	{ Err('<b>You must provide a job description</b>') }
	elsif (! $range )		{ Err('You must provide the number of CBIDs for this batch.') }
	elsif ( $range =~ /\D/)	{ Err('The number of CBIDs must be a numeric only value') }
	elsif ( $range <= 1 )	{ Err('The desired number of CBIDs must be greater than ONE') }
	elsif ( Check_Start_End() )	{}	###### our errors are handled already
	else 
	{

	###### we'll generate our start and end numbers based on the number of CBIDs they want
	###### unless they have explicitly set start & end values
		unless ($start && $end)
		{		
			my $sth = $db->prepare("SELECT NEXTVAL('cbid_batches_start_id_seq')");
			$sth->execute;
			$start = $sth->fetchrow_array;
			$sth->finish;
			$end = $start + $range -1;
		}

		my $conflict = Check_Conflicts();

		if ($conflict)
		{
			eval('Show_' . $conflict . '_Conflicts');
		}

		else
		{								
			my $batchID = Create_Batch();
			if ( $batchID )
			{
				print $q->header, $q->start_html(-bgcolor=>'#ffffff');
				print $q->h4("Batch <i>$batchID</i> successfully setup.");
				print $q->a({-href=>($rptScript . $batchID), -target=>'_blank'}, 'Click here for the details.');
				print ' | ', $q->a({-href=>$ME}, 'Batch Management');
				print ' | ', $q->a({-href=>'javascript::void()', -onclick=>'self.close()'}, 'Close this Window');
				print $q->end_html;
			}

			else { Err("Failed to setup batch") }
		}
	}
}

elsif ($action eq 'reconcile')
{
	
	unless ($start && $end && $batch_ID)
	{	
		Err('Parameters missing. A Batch ID, start CBID and end CBID are all required.');
	}

	elsif ( $start =~ /\D/ || $end =~ /\D/ || $batch_ID =~ /\D/)
	{
		Err('CBID and Batch ID values can only be numeric and must be greater than zero.');
	}
	
	elsif ( $start >= $end )
	{
		Err('The ending CBID must be greater than the starting CBID');
	}

	elsif ( $start < 1 || $end < 1 || $batch_ID < 1)
	{
		Err('CBID and Batch ID values must be greater than zero.');
	}
	
	else
	{		
		unless ( Verify_Batch() ){ Show_Reconcile_Errs() }
	
		else
		{
			my $conflict = Check_Conflicts();

			if (! $conflict)
			{
				if ( Reconcile() )
				{
					print $q->header, $q->start_html(-bgcolor=>'#ffffff');
					print $q->h4("Batch: $batch_ID successfully reconciled");
					print $q->a({-href=>$ME}, 'Batch Management.'), $q->end_html;
				}

				else
				{
					Err('<b>Reconciliation failed.</b>');
				}
			}

			elsif ($conflict eq 'Batch')
			{
				push (@errors, 'batch_overlap');

				$err_msgs{batch_overlap} .= '<br />The following Batchs overlap.';

				foreach (@{$match})
				{
					$err_msgs{batch_overlap} .= "<br />$_[0]\n";
				}
				
				Show_Reconcile_Errs();
			}

			elsif ($conflict eq 'CBID')
			{
				push (@errors, 'existing_cbid_match');

				$err_msgs{existing_cbid_match} .= '<br />The following CBIDs overlap.<br />';
				my $x = 0;
				foreach (@{$match})
				{
					$x++;
					if ($x > 20)
					{
						$err_msgs{existing_cbid_match} .= "<p>List truncated for length. There are <b> ${\(scalar @{$match})} total CBIDs </b>that overlap.</p>";
						last;
					}

					else
					{
						$err_msgs{existing_cbid_match} .= "<br />$_->[0]\n";
					}
				}
				
				Show_Reconcile_Errs();
			}
		}
	}
}

else { Err("Undefined 'action'") }

END:

$db->disconnect();
exit;

###### ###### THE END OF 'MAIN' ###### ######

sub Check_Conflicts
{
###### we have to make sure that there is not a conflict in batches
	my $qry = "	SELECT id FROM $TBL
			WHERE ((start_cbid <= $start AND end_cbid >= $start)
			OR (start_cbid <= $end AND end_cbid >= $end))";

###### if we are doing a reconciliation we will have the match of the batch we are
###### working on, so we'll exclude it here
	$qry .= " AND id != $batch_ID" if $batch_ID;

	$qry .= "\nAND voided= FALSE ORDER BY id";
	
	my $sth = $db->prepare( $qry );
	$sth->execute;
	$match = $sth->fetchall_arrayref;
	$sth->finish;
	if (scalar @{$match} > 0)
	{
		return 'Batch';
	}

###### next make sure there is no conflict in previously issued CBIDs
	$sth = $db->prepare("SELECT id
				FROM clubbucks_master
				WHERE id>= $start AND id<= $end
				ORDER BY id");
	$sth->execute;
	$match = $sth->fetchall_arrayref;
	$sth->finish;
	if (scalar @{$match} > 0)
	{
		return 'CBID';
	}
	
	return;
}

sub Check_Start_End
{
###### if we have one value but not the other we cannot proceed
	if (($start && ! $end) || ($end && ! $start))
	{
		Err('Start and End values must be used together or not at all.');
		return 1;
	}

###### if we have no submitted values, we are all set
	return unless ($start && $end);

###### if our values are not strictly numeric, we cannot go on
	if (($start =~ /\D/ || $end =~ /\D/))
	{
		Err('Start and End values must be only numbers.');
		return 1;
	}


	if ($end <= $start)
	{
		Err('The End value must be greater than the Start value.');
		return 1;
	}

	if (($end - $range + 1) != $start)
	{
		Err('Check you numbers! The number of CBIDs you selected does not match the Start and End values');
		return 1;
	}

	return;
}

sub Create_Batch
{
	my $sth = $db->prepare("SELECT NEXTVAL('cbid_batches_id_seq')");
	$sth->execute;
	my $batchID = $sth->fetchrow_array;
	$sth->finish;
	die "Failed to retrieve the next Batch ID" unless $batchID;

	my $rv = $db->do("INSERT INTO $TBL (id, start_cbid, end_cbid, description)
		VALUES ($batchID, $start, $end, ${\$db->quote($description)});
		SELECT SETVAL('cbid_batches_start_id_seq', $end)");

	return ($rv) ? $batchID : 0;	
}

sub Do_Page
{
	my $tmpl = shift || 'front';

	$tmpl = G_S::Get_Object($db, $TMPL{$tmpl}) || die "Failed to load template: $TMPL{$tmpl}";
	
	$tmpl =~ s/%%action%%/$ME/g;
	print $q->header, $tmpl;
}

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Reconcile
{
	###### since ordinary users are not granted update privileges on the batches table
	###### we'll have to create a special DB connection for this purpose
	use DB_Connect;
	my $dbh = DB_Connect::DB_Connect('cbid_batch_mgt.cgi') || exit;
	$dbh->{RaiseError} = 1;
	
	my $op = $dbh->quote($operator);

	my $qry = "	UPDATE $TBL
			SET 	reconciled_date= NOW(),
				reconciled_by= $op,
				notes= ${\($dbh->quote($notes))}
			WHERE id= $batch_ID;\n";

	for (my $x = $start; $x < ($end +1); $x++)
	{
		$qry .= "	INSERT INTO clubbucks_master (id, class, operator, memo)
				VALUES ( $x, 1, $op, 'Batch $batch_ID load');\n";
	}

	my $rv = $dbh->do($qry);
	$dbh->disconnect;
	return $rv;
}

sub Show_Batch_Conflicts
{
	print $q->header, $q->start_html(-bgcolor=>'#ffffff'), $q->h4("Batch conflict");
	print "Calculated Start CBID: <em>$start</em><br />Calculated End CBID: <em>$end</em>\n<p>";

	foreach (@{$match})
	{
		print $q->a({-href=>$rptScript . $_->[0], -target=>'_blank'}, "Batch: $_->[0]"), '<br />';
	}

	print "</p><p>Please contact the IT Department for further assistance.</p>";
	print $q->end_html;	
}

sub Show_CBID_Conflicts
{
	print $q->header, $q->start_html(-bgcolor=>'#ffffff'), $q->h4("Existing CBID conflict");

	if (scalar @{$match} > 20)
	{
		print $q->h4('There are more than 20 existing CBIDs in place within the range of this batch.');
	}

	else
	{
		print $q->h4('The following CBIDs have been found within the range of this batch')	;

		foreach (@{$match})
		{
			print $q->a({-href=>$CBIDrptScript . $_->[0], -target=>'_blank'}, "CBID: $_->[0]"), '<br />';
		}
	}
	
	print "<p>Please contact the IT Department for further assistance.</p>";
	print $q->end_html;	
}

sub Show_Reconcile_Errs
{
	unless (@errors)
	{
		Err('Show_Reconcile_Errs called without any errors flagged!');
		return;
	}
		
	my $tmpl = G_S::Get_Object($db, $TMPL{reconcile_errors}) || die "Failed to load template: $TMPL{reconcile_errors}";
	my $errs = '';

	foreach (@errors)
	{
		$errs .= "$err_msgs{$_}<br />\n";
	}

	$tmpl =~ s/%%message%%/$errs/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header, $tmpl;
}

sub Verify_Batch
{
	my $sth = $db->prepare("SELECT * FROM $TBL WHERE id= $batch_ID");
	$sth->execute;
	my $batch = $sth->fetchrow_hashref;
	$sth->finish;

	unless ($batch)
	{
		push (@errors, 'batch_not_found');
		return;
	}

	elsif ($batch->{voided})
	{
		push (@errors, 'batch_voided');
		return;
	}
	
	if ($batch->{start_cbid} != $start)
	{
		push (@errors, 'batch_start_mismatch');
	}

	if ($batch->{end_cbid} != $end)
	{
		push (@errors, 'batch_end_mismatch');
	}

	if ($batch->{reconciled_by} || $batch->{reconciled_date})
	{
		push (@errors, 'already_reconciled');
	}

	return (@errors) ? 0 : 1;
}

###### initial release 05/15/03








