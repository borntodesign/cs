#!/usr/bin/perl -w
################################################################
# questionnaires_mall_categories_interface.cgi
#
# The interface for Marketing Questionnairs
# This script started as a copy of "pwp-edit.cgi" and was stripped
# and modified to handle Marketing Questionnairs (survey questions).
#
# Deployed: 07/08
# Author: Keith Hasely
# Version: 0.1
################################################################

################################################################
# Includes
################################################################
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use URI::Escape;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use CGI::Carp qw(fatalsToBrowser set_message);
use Data::Dumper;

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
our $CGI = CGI->new();

our $ME = $CGI->url;

our $db = {};

our $questionnaires_id = defined $CGI->param('questionnaires_id')?$CGI->param('questionnaires_id'):'';
our $incentives = defined $CGI->param('incentives')?$CGI->param('incentives'):0;


################################################################
# Subroutine(s)
################################################################
###############################
# Display the page
# then exit the script cleanly
#
# Param		hash
# 			db	obj		The database object
# Return	null
###############################
sub displayPage
{
	my $args = shift || {};

	if(!exists $args->{db})
	{
		errorExitScript("A Database connection was not specified.");
	}


my $incentives_query = <<EOT;
	SELECT
		si.id,
		si.name,
		qasil.id AS selected
	FROM
		shopping_incentives AS si
		LEFT JOIN
		questionnaires_answers_links AS qasil
		ON
			qasil.incentive_mallcat_id = si.id
		AND
			qasil.questionnaires_id = ?
		AND
			qasil.category = 'incentive'
		AND
			qasil.questionnaires_answers_id = ?
EOT

my $category_query = <<EOT;
	SELECT
		mc.catid AS id,
		vendor_categories_text_list(mc.catid) AS name,
		qaml.id AS selected
	FROM
		mallcats AS mc
		LEFT JOIN
		questionnaires_answers_links AS qaml
		ON
			qaml.incentive_mallcat_id = mc.catid
		AND
			qaml.questionnaires_id = ?
		AND
			qaml.questionnaires_answers_id = ?
		AND
			qaml.category = 'mallcat'
	WHERE
		mc.catname ~* ?
		OR
		qaml.questionnaires_answers_id = ?
EOT

my $questions_and_answers = {};

my $mallcats_incentives_query = $incentives?$incentives_query:$category_query;

my $html = '';

my $extra_divs = <<EOT;

	<div id="fadeDiv" class="fadeDiv">
	</div>

	<div id="addCategoryDiv" class="addCategoryDiv">
		<span id="addCategoryClose" style="position:absolute; top:0px; width:395px; text-align:right">
			<a href="#" onclick="closeDivs(); return false;"><strong>X</strong></a>
		</span>

		<span id="addCategoryForm" style="position:absolute; top:20px; background-color:#FFFFFF; border: 1px solid #666666; width:380px; height:300px;">
			<table id="addCategoryTable" width="390" height="275" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td>&nbsp;</td>
				<td id="question_comment_area" valign="top" align="center">
					<form name="frmAddCategory" id="frmAddCategory" action="" method="get">
					<table width="350" border="0" cellspacing="5" cellpadding="0">
					  <tr>
						<td width="19%"></td>
						<td width="81%"></td>
					  </tr>

					  <tr>
						<td class="question" align="left">
							Search Category:
							<br />
							<input editable name="search_text" type="text" class="msgBox" id="search_text" style="width:300px"/>
							<input type="button" value="Search" onClick="searchCategory(document.getElementById('search_text').value, document.getElementById('additional_categories')); return false;" />
						</td>
					  </tr>
					  <tr>
						<td colspan="2" align="left" class="question" >
							Additional Categories:
							<br />
							<div style="width:350px; overflow-x: auto; border: 1px solid; margin-bottom: 1px;">
						  		<select multiple name="additional_categories" id="additional_categories" style="border: 0px none; min-width: 350px; overflow: auto;" size="13"></select>
						  	</div>
						  	<input type="button" value="Add" onClick="addCategory(document.getElementById('additional_categories')); return false;" />
						</td>
					  </tr>

					</table>
				  </form></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>

				<td height="10" class="blogBackground"></td>
				<td class="blogBackground"></td>
				<td class="blogBackground"></td>
			  </tr>

			</table>
		</span>
	</div>
EOT



my $questionnaires_query = <<EOT;
	SELECT
		id, questionnaires_name
	FROM
		questionnaires
	ORDER BY
		questionnaires_name
EOT

my $questionnaires = $args->{db}->selectall_hashref($questionnaires_query, "id",);



my $questionnaires_select_box = qq(<select name="questionnaires_id" onChange="submit();">);

$questionnaires_select_box .= qq(<option value=""></option>);

my $questionnaires_category_links = q(<span class="question">Update:</span> );

$questionnaires_category_links .= $incentives?qq(<a href="$ME?questionnaires_id=$questionnaires_id">Categories</a>):"Categories";

$questionnaires_category_links .= "&nbsp; &nbsp;";
$questionnaires_category_links .= $incentives?"Incentives":qq(<a href="$ME?incentives=1&questionnaires_id=$questionnaires_id">Incentives</a>);

$questionnaires_category_links = $questionnaires_id?$questionnaires_category_links:'';

foreach my $questionnaires_key (sort keys %{$questionnaires})
{
       $questionnaires_select_box .= qq(<option value="$questionnaires->{$questionnaires_key}->{id}");
       $questionnaires_select_box .= ($questionnaires->{$questionnaires_key}->{id} == $questionnaires_id)?' selected ':'';
       $questionnaires_select_box .= qq(>$questionnaires->{$questionnaires_key}->{questionnaires_name}</option>);
}

$questionnaires_select_box .= qq(</select>);





if($questionnaires_id =~ /^\d+$/)
{
	my $sth = $args->{db}->prepare("
										SELECT
											qq.id AS qq_id,
											qq.question,
											qa.id AS qa_id,
											COALESCE(mc.catname,qa.answer) AS answer
										FROM
											questionnaires_questions AS qq
											JOIN
											questionnaires_answers AS qa
											ON
											qq.id = qa.questionnaires_questions_id
											LEFT JOIN
											mallcats AS mc
											ON
											mc.catid = qa.mallcats_catid
											AND
											qa.mallcats_catid <> 0
										WHERE
											qq.questionnaires_id = ?
											AND
											qq.active = TRUE
										ORDER BY
											qq.id,
											qa.id
	");

	my $sth_options = $args->{db}->prepare($mallcats_incentives_query);



	$sth->execute($questionnaires_id);
	while (my $row = $sth->fetchrow_hashref)
	{
		$questions_and_answers->{$row->{qq_id}}->{question} = $row->{question} if (!exists $questions_and_answers->{$row->{qq_id}}->{question});
		$questions_and_answers->{$row->{qq_id}}->{answers}->{$row->{qa_id}}->{answer}= $row->{answer};

		next if ($row->{answer} =~ /^no$|^None$/i || !defined $row->{answer});

		if($incentives){
			$sth_options->execute($questionnaires_id,$row->{qa_id});
		} else {
			$sth_options->execute($questionnaires_id, $row->{qa_id}, $row->{answer}, $row->{qa_id});
		}
		while (my $category_row = $sth_options->fetchrow_hashref)
		{
			$questions_and_answers->{$row->{qq_id}}->{answers}->{$row->{qa_id}}->{mallcats}->{$category_row->{id}}->{category}= $category_row->{name};
			$questions_and_answers->{$row->{qq_id}}->{answers}->{$row->{qa_id}}->{mallcats}->{$category_row->{id}}->{selected}= $category_row->{selected};
		}


	}


}

$html .= q(<form method="POST"><table>);
$html .= q(<tr><td width="15"></td><td width="150"></td><td></td><td></td><td></td></tr>);

if(keys %{$questions_and_answers})
{

foreach my $question_keys (keys %{$questions_and_answers})
{
	#Question
	$html .= qq(<tr><td colspan="5" class="question" >$questions_and_answers->{$question_keys}->{question}</td></tr>);


	foreach my $answer_keys (keys %{$questions_and_answers->{$question_keys}->{answers}})
	{
		#Answer
		$html .= qq(<tr><td width="10"></td><td class="answer" colspan="5">$questions_and_answers->{$question_keys}->{answers}->{$answer_keys}->{answer}<br />);

		#Mall Categories
		#<div style="border:1px solid; height:100px; width:500px; overflow: -moz-scrollbars-horizontal; overflow-x: auto;">
		# border:0px hidden transparent;
		$html .= qq(<select size="5" multiple name="$answer_keys" id="$answer_keys" style="min-width: 350px;">);
		if(exists $questions_and_answers->{$question_keys}->{answers}->{$answer_keys}->{mallcats})
		{


			foreach my $mallcats_keys (keys %{$questions_and_answers->{$question_keys}->{answers}->{$answer_keys}->{mallcats}})
			{
				my $selected = (defined $questions_and_answers->{$question_keys}->{answers}->{$answer_keys}->{mallcats}->{$mallcats_keys}->{selected})?q(selected="selected"):'';

				$html .= qq(<option value="$mallcats_keys" $selected>$questions_and_answers->{$question_keys}->{answers}->{$answer_keys}->{mallcats}->{$mallcats_keys}->{category}</option>);

			}


		}
		$html .= qq(</select>);

		if(!$incentives)
		{
			$html .= qq(<br /><input name="add" value="Add Additional Mall Categories" type="button" onclick="answer_key = '$answer_keys'; showDivs(0); return false" /></td>);
		}
		#</select></div>
		$html .= qq(<td></td>);
		$html .= qq(<td></td>);

		$html .= qq(</tr>);
		$html .= qq(<tr><td colspan="5">&nbsp;</td></tr>);
	}



}

$html .= qq(<tr><td colspan="5"><input type="hidden" name="incentives" value="$incentives"><input type="hidden" name="questionnaires_id" value="$questionnaires_id"><input type="submit" name="submit" value="Submit"></td></tr>);
}

$html .= q(</table></form>);



	print $CGI->header(-TYPE => 'text/html', -CHARSET=>'utf-8');
	print <<EOT;
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Questionnaire Answer Mall Category Relationships</title>
			<link rel="stylesheet" href="/css/questionnaires_admin.css" type="text/css" />
			<script src="/js/questionnaires_admin.js"></script>
			<script src="/js/yui/yahoo-min.js"></script>
			<script src="/js/yui/event-min.js"></script>
			<script src="/js/yui/connection-min.js"></script>
		</head>
		<body>
			<div id="content">
			<table>
				<tr>
					<td>
						<form>
							<span class="question">
							Select a Questionnaire
							</span>
							<br />
							$questionnaires_select_box
						</form>
					</td>
				</tr>
				<tr>
					<td>
						$questionnaires_category_links
					</td>
				</tr>
				<tr>
					<td>
						$html
					</td>
				</tr>
			</table>
			</div>


			$extra_divs
		</body>
	</html>
EOT



}
###############################

sub encodeURIData
{
	my $args = shift || {};

	my $return_string = '';

	foreach my $key (keys %$args)
	{
		$return_string .= "&$key=" . uri_escape($args->{$key});
	}

	return $return_string;
}
###############################
# Update
# then exit the script cleanly
#
# Param		hash
# 			db	obj		The database object
# Return	null
###############################
sub insertUpdateUserAnswers
{
	my $args = shift || {};

	if(!exists $args->{db} || !defined $args->{db})
	{
		errorExitScript("A Database connection was not specified.");
	}

	if($questionnaires_id !~/\d+/)
	{
		errorExitScript("The Questionnaires ID is not valid.");
	}



	my $http_query_string = encodeURIData({questionnaires_id=>$questionnaires_id,incentives=>$incentives});
	my $category = $incentives?'incentive':'mallcat';

	my $delete_query = <<EOT;
						DELETE
						FROM
							questionnaires_answers_links
						WHERE
							questionnaires_id = $questionnaires_id
							AND
							category = '$category'
EOT

	my $insert_query = <<EOT;
		INSERT
		INTO
			questionnaires_answers_links
		(questionnaires_id, questionnaires_answers_id, incentive_mallcat_id, category)
		VALUES
		(?,?,?,?)
EOT


	$args->{db}->{AutoCommit} = 0;

	eval{
		$args->{db}->do($delete_query);

		my $sth = $args->{db}->prepare($insert_query);

		#print $CGI->header(-type => 'text/plain');

		foreach my $key ($CGI->param())
		{
			next if $key =~/\D+/;

			#print qq($key\n);

			foreach my $name ($CGI->param($key))
			{
				#print qq($questionnaires_id, $key, $name, $category\n);
				$sth->execute($questionnaires_id, $key, $name, $category);
			}
		}

		$args->{db}->commit();
	};

	if ($@)
	{
		$args->{db}->rollback();



		$http_query_string .= encodeURIData({error=>'The information was not saved, please try again.'});
	}

	$args->{db}->{AutoCommit} = 1;

	print $CGI->redirect(-URL =>"$ME?$http_query_string");
}
###############################

###############################
# Output an error message for the user
# then exit the script cleanly
#
# Param		hash
# 			string message			The mesage to output before exiting.
# Return	null
###############################
sub errorExitScript
{
	my $message = shift || {};

	print $CGI->header(-TYPE => 'text/html');

	print qq(<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />);
	print q(<title>test</title>);
	print qq(</head><body>);
	print $message;
	print qq(</body></html>);
	exitScript();
}
###############################

###############################
# This sub came around because
# Bill was/is using a "goto" statement!
#
###############################
sub exitScript
{
	$db->disconnect if ($db);
	exit;
}
###############################

###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exitScript() unless $db;
$db->{RaiseError} = 1;


if($CGI->param('submit'))
{
	insertUpdateUserAnswers({db=>$db});

} else {
	displayPage({db=>$db});
}

exitScript();
