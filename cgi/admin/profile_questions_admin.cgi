#!/usr/bin/perl -w
#### profile_questions_admin.cgi
####
#### Web interface for modification of questions and answers for profile
#### surveys.
####
#### Deployed: 01/02/07 Shane Partain

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

our $q = new CGI;
our $db = '';

my %params = ();
foreach ( $q->param() ) { $params{$_} = $q->param($_); }
my @rows = ();
my @columns = ();

### db columns being used in the questions html table
my @question_columns = qw/question_key question stamp operator/;

### table headers for the questions table
my @question_th = qw/Select Question Modified By/;

### table headers for the questions in the html table on the editing screen
my @qa_question_th = ('Question', 'Control Type', 'Modified', 'By');

### db columns being used in the questions html table on the editing screen
my @qa_question_columns = qw/question control_type stamp operator/;

### table headers for the answers in the html table on the editing screen
my @qa_answers_th = qw/Answer Sorting Modified By/;

### db columns being used in the answers html table on the editing screen
my @qa_answers_columns  = qw/answer sorting stamp operator/;


##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'profile_questions_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

### if there's no parameters, just show all the questions
unless ( %params ) {

	my $questions = $db->selectall_hashref("SELECT *
						FROM profile_questions", 'question_key');

	my ($max_qk) = $db->selectrow_array("SELECT max(question_key)
						FROM profile_questions");

	### Forces the "Add a question" row to be created.
	$questions->{ ($max_qk+1) }->{question_key} = $max_qk+1;

	my @radio = $q->radio_group(-name=>'qk', -values=>[(sort keys %$questions)], -nolabels=>1);
	push @rows, $q->th( \@question_th );

	foreach my $question_key ( sort keys %$questions ) {
        	foreach ( @question_columns ) {
			if ( /question_key/ ) {
				 push @columns, shift @radio;
				 next;
			}
			if ( /question/ && !$questions->{$question_key}->{$_} ){
				push @columns, $q->h4('Add a new question.');
				next;
			}
			push @columns, $questions->{$question_key}->{$_} ;
		}
		push @rows, $q->td(\@columns);
		@columns = ();
	}
	
	print 	$q->header(),
		$q->start_html(-title=>'Profile Questions Admin', -style=>'/css/profile.css'),
		$q->center(
			$q->h3('Profile Questions Admin'),
			$q->a({-href=>'javascript:void(0)',
				-onClick=>"window.open('/pq_admin.html', 'helpWindow', 'resizable=1,scrollbars=1,width=420,height=400')",
				-target=>'helpWindow'}, 'Help'),
			$q->br, $q->br,
			$q->start_form(-method=>'GET', -action=>'profile_questions_admin.cgi'),
				$q->table( $q->Tr( \@rows ) ),
				$q->submit('edit'),
			),
			$q->end_form(),
		$q->end_html;

### Commit the changes
} elsif ( $params{update} ) {

	### Update the question, if needed.
	my $questions = $db->selectrow_hashref("SELECT *
                                                FROM profile_questions
                                                WHERE question_key=$params{qk}");

	unless ( ( $questions->{question} eq $params{question} ) &&
			( $questions->{control_type} == $params{control_type} ) ) {
		$params{question} = $db->quote( $params{question} );
		if ( $questions->{question} ){ ### Modify the question.
			$db->do("UPDATE profile_questions
				SET question=$params{question},
					control_type=$params{control_type},
					stamp=now(),
					operator=\'$operator\'
				WHERE question_key=$params{qk}");
		} elsif ( $params{question} ) { ### Add a new question.
			$db->do("INSERT into profile_questions (question, control_type)
				VALUES ( $params{question}, $params{control_type} )");
		}
	}

	### Update the answers, if needed.
	my $answer_key = '';
	my $answer = '';
	my $sorting = '';
	my $key = 'answer_key';
        my $question_answers = $db->selectall_hashref("SELECT *
                                                        FROM profile_question_answers
                                                        WHERE question_key=$params{qk}", $key);

	foreach ( keys %params ) {
		if ( /^answer(\d+)$/ && $params{$_} ) {
			$answer_key = $1;
			$answer = $params{$_};
			$sorting = $params{("sorting" . $1)};
			
			### Check to see if the user made changes to this question.
			unless ( ( $question_answers->{$1}->{answer} eq $answer ) &&
					 ( $question_answers->{$1}->{sorting} == $sorting) ) {
				$answer = $db->quote( $answer );
				if ( $question_answers->{$1}->{answer} ){
					if ( $answer ) { ### Modify an existing answer.
						$db->do("UPDATE profile_question_answers
							SET answer=$answer,
								sorting=$sorting,
								stamp=now(),
								operator=\'$operator\'
							WHERE pk=$question_answers->{$1}->{pk}");
					} 
				} elsif ( $answer ) { ### Add a new answer.
					$db->do("INSERT into profile_question_answers
							( answer_key, question_key, answer, sorting )
						VALUES ( $answer_key, $params{qk}, $answer, $sorting )");
				}
			}
		}
	}

print $q->redirect( ( 'profile_questions_admin.cgi?qk=' . $params{qk} ) );

### If there's a question_key parameter, load the edit screen for that question
} elsif ( $params{qk} ) {
	
	my $questions = $db->selectrow_hashref("SELECT *
        	                                FROM profile_questions 
						WHERE question_key=$params{qk}");
	
	my $question_answers = $db->selectall_hashref("SELECT *
							FROM profile_question_answers
							WHERE question_key=$params{qk}", 'pk');
	
	### This block forces an additional row with an empty answer field to be created in the
	### html table, for adding a new answer.
	my $max = $db->selectrow_hashref("SELECT max(pk) as pk, 
						max(sorting) as sorting,
						max(answer_key) as answer_key
						FROM profile_question_answers
						WHERE question_key=$params{qk}");
	$question_answers->{ ($max->{pk}+1) }->{sorting} = $max->{sorting}+1;
	$question_answers->{ ($max->{pk}+1) }->{answer_key} = $max->{answer_key} + 1;

	### labels for control_type dropdown	
	my %controls = ();
	my $controls = $db->selectall_hashref("SELECT *
						FROM profile_questions_control_types", 'pk');
	foreach ( keys %$controls ) { $controls{$_} = $controls->{$_}->{description} ; }

	### print out the question
	push @rows, $q->th( \@qa_question_th );
	foreach ( @qa_question_columns ) {
		if ( /question/ ) {
			push @columns, $q->textarea( -name=>'question',
							-value=>$questions->{$_},
							-cols=>35,
							-rows=>4,
                                                        -wrap=>'virtual'
							);
			next;
		}
		if ( /control_type/ ) {
			push @columns, $q->popup_menu( -name=>'control_type',
							-values=>[ (sort keys %$controls) ], 
							-default=>$questions->{$_},
							-labels=>\%controls
							);
			next;

		} 
		push @columns, $questions->{$_};
	}
	push @rows, $q->td(\@columns);
	@columns = ();

	### print out the answers for this question
	push @rows, $q->th( \@qa_answers_th );
	foreach my $pk ( sort { $question_answers->{$a}->{sorting} <=> $question_answers->{$b}->{sorting} }
				keys %$question_answers ) { 
		foreach ( @qa_answers_columns ) {
			if ( /answer/ ){
				my $field_name = $_ . $question_answers->{$pk}->{answer_key};
				push @columns, $q->textarea( -name=>$field_name,
								-value=>$question_answers->{$pk}->{$_},
								-cols=>35,
								-rows=>4,
								-wrap=>'virtual' );
				next;
			}
			if ( /sorting/ ){
				my $field_name = $_ . $question_answers->{$pk}->{answer_key};
				push @columns, $q->textfield( -name=>$field_name,
								-value=>$question_answers->{$pk}->{$_},
								-size=>2 );
				next;
			}
			push @columns, $question_answers->{$pk}->{$_};
		}
		push @rows, $q->td( \@columns );
		@columns = ();
	}

	print   $q->header(),
                $q->start_html( -title=>'Profile Questions Admin - Edit',
				-style=>'/css/profile.css',
				 ),
		$q->center(
                        $q->h3('Profile Questions Admin - Edit'),
			$q->a({-href=>'profile_questions_admin.cgi'}, 'View All Questions'),
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
			$q->a({-href=>'javascript:void(0)',
				-onClick=>"window.open('/pq_admin.html', 'helpWindow', 'resizable=1,scrollbars=1,width=420,height=400')",
				-target=>'helpWindow'}, 'Help'),
			$q->br, $q->br,
                        $q->start_form(-method=>'POST', -action=>'profile_questions_admin.cgi'),
				$q->table( $q->Tr( \@rows ) ),
                                $q->submit('save changes'),
                        ),
			$q->hidden( -name=>'qk', -value=>$questions->{question_key} ),
			$q->hidden( -name=>'update', -value=>1 ),
                        $q->end_form(),
                $q->end_html;

} else {}

sub Err
{

        print $q->header(), $q->start_html();
        print $_[0];
        print "<br />\n";
        print $q->end_html();
}


$db->disconnect;
