#!/usr/bin/perl -w
###### cbid_info.cgi
###### Lookup a member's ClubBucks ID records using a field and a value supplied by the user.
###### This script will also allow updates to ClubBucks ID records.
###### Created: 04/28/2003	Karl Kohrt
######
###### Last modified: 12/05/03 Karl Kohrt

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
#use POSIX qw(locale_h);
#use locale;
#setlocale(LC_CTYPE, "en_US.ISO8859-1");
use State_List qw( %STATE_CODES );

use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS

###### our page template
our %TMPL = (	'search'	=> 10152,
		'liv'		=> 10153,
		'arc_list' 	=> 10218,
		'arc_display'	=> 10219);

###### the table we'll get data from
our %TBL = (	'cbids'	=>	'clubbucks_master',
		'cbids_arc'	=> 'clubbucks_master_arc');

our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $search_field = $q->param('search_field');
our $search_value = G_S::PreProcess_Input($q->param('search_value'));

our ($db);	###### database variables
our ($dover_flag, $data) = ();
our $dover_msg = '';
our $master_id = 0;

# Added an = before each fieldname so grep can identify the beginning of/and the whole word.
our @char_fields = qw/=operator =stamp =label =memo/;
our @int_fields = qw/=id =class =referral_id =referral_clubbucks_id =current_member_id/;
our @result_list = ();

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'memberinfo.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

###### %current holds the values that are currently in the database. It has the form of 
###### 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %current = (id 				=> {value=>'', class=>'n', req=>1},
		class				=> {value=>'', class=>'n', req=>1},
		referral_id 			=> {value=>'', class=>'n', req=>0},
		referral_clubbucks_id	=> {value=>'', class=>'n', req=>0},
		current_member_id		=> {value=>'', class=>'n', req=>0},
		operator			=> {value=>'', class=>'n', req=>1},
		stamp				=> {value=>'', class=>'n', req=>1},
		label		 		=> {value=>'', class=>'n', req=>0},
		emailaddress	 		=> {value=>'', class=>'n', req=>0},
		memo		 		=> {value=>'', class=>'n', req=>0}
	);

###### %new holds the newly submitted values.
my %new = (id 				=> '',
		class				=> '',
		referral_id 			=> '',
		referral_clubbucks_id	=> '',
		current_member_id		=> '',
		operator			=> '',
		stamp				=> '',
		label		 		=> '',
		memo		 		=> ''
	);

###### If this is the first time through, just print the Search page.
unless ( $action )
{
	Search_Form()	;
}
elsif ($action eq 'arclist')
{
	if ( ArcList() == 0 )
	{
		Err("No matching records for ClubBucks ID: $master_id");
	}
}
elsif ($action eq 'arc')
{
	$search_field = 'oid';
	$search_value = $q->param('id');

	Run_Search('cbids_arc', $search_field, $search_value);

	###### at this point we should have one row of data
	unless ( @result_list )
	{
		Err("No matching records for ClubBucks Card ID: $master_id");
	}
	else
	{		
		$data = shift @result_list;
		Load_Data();
		$dover_msg = 'Data cannot be changed.';
		Do_Page('arc_display');
	}
}
elsif ($action eq 'search')
{
	# Check the validity of the field and the value provided by the user.
	if ( validity_check() )
	{
		Run_Search('cbids', $search_field, $search_value);
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		elsif ( @result_list > 1 )
		{
			###### create list report
			Display_List();
		}

		else
		{
			$data = shift @result_list;
			Load_Data();
			Class_Menu();
			Do_Page('liv');
		}
	}
}
elsif ($action eq 'show'){
	if ( validity_check() ){
		Run_Search('cbids', $search_field, $search_value);
		###### at this point we should have one row of data
		unless ( @result_list )
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'No matching records') );
		}
		else
		{		
			Check_Membertype();
			$data = shift @result_list;
			Load_Data();
			Class_Menu();
			Do_Page('liv');
		}
	}
}
elsif ( $action eq 'update' )
{
	# Load the submitted values into the %new hash.
	Load_Params();

 	# Query the current data from the database, and load it into the %current hash.
	Run_Search('cbids', 'id', $new{id});
	$data = shift @result_list;
	Load_Data();

	# Do the initial checks common to all attempts.
	Check_Required();
	Check_Membertype();

	# If we have a 'do over' message at this point, no more testing is necessary.
	if ($dover_msg)
	{
	}
	# No 'do over' message, so we will move on to 'Class' checks.
	elsif ($new{class} == 1)
	{
		$dover_msg = $q->span({-class=>'r'},
			"<br>You can't change to a Class 1 - Unassigned.<br>\n");
	}
	elsif (($new{class} == 2) && (($current{class}{value} <= 2) || ($current{class}{value} == 6)))
	{
		if (Check_Id_Exists($new{referral_id}, 'referral_id', 'clubbucks_master') >= 1)
		{
			Update();
		}
	}
	elsif (($new{class} == 3) && (($current{class}{value} == 3) || ($current{class}{value} == 6)))			
	{
		if (Check_Id_Exists($new{referral_clubbucks_id}, 'referral_clubbucks_id',
			 'clubbucks_master') >= 1)
		{
			# If the referring clubbucks ID's class is 2, 4, or 5.
			my ($sth, $rv, $how_many) = ();
			my $qry = 
			"SELECT count(*)
			FROM clubbucks_master		
			WHERE referral_clubbucks_id = $new{referral_clubbucks_id} AND 
				(class = 2 OR
				 class = 4 OR
				 class = 5)";

			$sth = $db->prepare($qry);
			$rv = $sth->execute();
			$how_many = $sth->fetchrow_array;
			$sth->finish;

			if ($how_many == 0){
				$dover_msg = $q->span({-class=>'r'},
					"<br>The referral_clubbucks_id is either not valid or not of the proper class.<br>\n");
			}
			else{				
				Update();
			}
		}
	}
	elsif (($new{class} == 4) && (($current{class}{value} <= 4) || ($current{class}{value} == 6)))
	{
		if (Check_Id_Exists($new{current_member_id}, 'id', 'members') >= 1)
		{
			Update();
		}
	}
	elsif (($new{class} == 5) && ($current{class}{value} >= 5))
	{
		if (Check_Id_Exists($new{current_member_id}, 'id', 'members') >= 1)
		{
		Update();
		}
	}
	elsif ($new{class} == 6){
		Update();
	}
	else {
		$dover_msg = $q->span({-class=>'r'},
			"<br>You can't change from a Class $current{class}{value} to a Class $new{class}.<br>\n");
	}	
	# Reload the page with the problems flagged if there are any errors.
	#   The %new hash holds the hidden value of the current CBID.
	Run_Search('cbids', 'id', $new{id});
	$data = shift @result_list;
	Load_Data();
	Class_Menu();
	Do_Page('liv');
}
else { &Err('Undefined URL action requested.') }

$db->disconnect;
exit(0);


###### ###### ###### ###### ######
###### Subroutines begin here
###### ###### ###### ###### ######

###### Show a list of archived records.
sub ArcList
{
	$master_id = $q->param('id');
	my $qry = "	SELECT oid,
				referral_clubbucks_id,
				referral_id,
				current_member_id,
				class,
				label,											
				memo,
				stamp,
				operator
			FROM 	$TBL{cbids_arc}
			WHERE id = ?
			ORDER BY stamp desc";

	my $bgcolor = '#ffffee';
	my $tblDATA = '';
	my $num_records = 0;
	my $sth = $db->prepare($qry);
	$sth->execute($master_id);

	while ( $data = $sth->fetchrow_hashref )
	{
		++$num_records;
				
		###### lets fill in the blank table cells
		foreach (keys %{$data}){
			unless ( defined $data->{$_} ) { $data->{$_} = '&nbsp;' }
			unless ( $data->{$_} =~ /\w/ ){$data->{$_} = '&nbsp;'}
		}
		$tblDATA .= $q->Tr({-bgcolor=>$bgcolor},
			$q->td({-valign=>'top'}, $q->a({-href=>$q->url()."?action=arc&id=$data->{oid}", -target=>'_blank'}, '*') ) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{referral_clubbucks_id}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{referral_id}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{current_member_id}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{class}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{label}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{stamp}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{operator}) .
			$q->td({-valign=>'top', -class=>'data'}, $data->{memo})
		);
		$bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	}

	if ( $num_records )
	{	
		$sth->finish;
		my $tmpl = G_S::Get_Object($db, $TMPL{arc_list}) || die "Couldn't open $TMPL{arc_list}";

		$tmpl =~ s/%%data%%/$tblDATA/;
		$tmpl =~ s/%%id%%/$master_id/g;         

		print $q->header(-expires=>'now'), $tmpl;
	}							
	return $num_records;
}

###### Check to see if an ID number exists in a table.
###### 	Pass the ID number, field name, and the table name to the subroutine.
sub Check_Id_Exists{
	my $id_num = shift || 'NULL';
	my $id_field = shift;
	my $table = shift;
	my ($sth, $rv, $how_many) = ();
	my $qry = 
	"SELECT count(*)
	FROM $table		
	WHERE $id_field = $id_num";

	$sth = $db->prepare($qry);
	$rv = $sth->execute();
	$how_many = $sth->fetchrow_array;
	$sth->finish;

	if ($how_many == 0){
		$dover_msg = $q->span({-class=>'r'},
			"<br>The $table:$id_field # $id_num does not exist.<br>\n");
	}

	return $how_many;
}

###### Check to see if this member is of a valid member type in order to make changes.
sub Check_Membertype{
	# Run this only if a new member ID has been submitted. 
	#   If not, no $dover_msg is returned allowing the flow to continue.
	if ($new{current_member_id}){
		my ($sth, $rv, $how_many) = ();

		# If the member ID doesn't exist.
		if (Check_Id_Exists($new{current_member_id}, 'id', 'members') >= 1){
			# Find out if the membertype will allow changes.
			my $qry = 
			"SELECT count(*)
			FROM members		
			WHERE id = $new{current_member_id} AND				(membertype = 'm' OR
				 membertype = 'v' OR
				 membertype = 's' OR
				 membertype = 'ag' OR
				 membertype = 'agu')";

			$sth = $db->prepare($qry);
			$rv = $sth->execute();
			$how_many = $sth->fetchrow_array;
			$sth->finish;

			# If not a member type that is allowed to make changes.
			if ($how_many == 0){
				$dover_msg = $q->span({-class=>'r'},
				"<br>You can't change to member ID #$new{current_member_id} due to membertype limitation.<br>\n");
			}
		}
	}
}


sub Check_Required{
	######	here we'll check required fields and assign new html classes as necessary
	foreach (keys %new){
		if ($new{$_} eq '' && $current{$_}{req} == 1){	###### a required field is empty
			$current{$_}{class} = 'r';				###### so we'll flag it
			$dover_flag=1;
			$dover_msg = $q->span({-class=>'r'}, "<br>Please fill in the required fields marked in <b>RED</b><br>\n");
		}
	}
}

###### Loads up a drop down menu of Class values for the new page.
sub Class_Menu{
	my @values = (1, 2, 3, 4, 5, 6, 7);
	my %labels = (1 => '1 - Unassigned',
			2 => '2 - Assigned',
			3 => '3 - Assigned-secondary',
			4 => '4 - Temporary',
			5 => '5 - Fixed',
			6 => '6 - Void',
			7 => '7 - Owned');
	$current{class}{value} = $q->popup_menu(	-name=>'class',
							-class=>'in',
							-values=>\@values,
							-default=>$current{class}{value},
							-labels=>\%labels,
							-override=>1);
}

sub Create_hidden{
	my $hid;
###### we need to pass all our parameters that have values except the action param
	foreach ( $q->param() ){
		$hid .= $q->hidden($_) if ($_ ne 'action');
	}
	return $hid;
}

###### Displays the results if the search returned multiple records.
sub Display_List{
	my $html;

	# print a title for the browser window.
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#ffffee', -title=>'ClubBucks Card Search Results');

	# print a simple page header.
	print qq!
		<CENTER><H1>ClubBucks Card Search Results</H1></CENTER><p>
		<TABLE align="center" border="2" cellpadding="2">
 			<TR>
			<TH> CBID </TH>
			<TH> Member ID </TH>
			<TH> Ref. CBID </TH>
			<TH> Referral ID </TH>
			<TH> Class </TH>
			<TH> Description/Label </TH>
			</TR>!;

	# Display the various lines of data returned from the query.
	foreach my $result (@result_list)
	{
		$result->{class} = $result->{class} || '&nbsp;';
		$result->{current_member_id} = $result->{current_member_id} || '&nbsp;';
		$result->{referral_clubbucks_id} = $result->{referral_clubbucks_id} || '&nbsp;';
		$result->{referral_id} = $result->{referral_id} || '&nbsp;';
		$result->{label} = $result->{label} || '&nbsp;';
		print qq!
			<TR>
			<TD><A href="$ME?action=show&search_field=id&search_value=$result->{id}" target="Resource Window $result->{id}"> $result->{id} </a></TD>
			<TD>$result->{current_member_id}</TD>
			<TD>$result->{referral_clubbucks_id}</TD>
			<TD>$result->{referral_id}</TD>
			<TD>$result->{class}</TD>
			<TD>$result->{label}</TD>
			</TR>!;
	}

	print qq!
	</TABLE>
	<CENTER><p><a href="$ME">Click Here for New Search</a></CENTER>!;

	print $q->end_html();
}

###### Display an individual member record.
sub Do_Page
{
	my $tmpl_name = shift;
	my $hidden = '';# = &Create_hidden();
	my $line = G_S::Get_Object($db, $TMPL{$tmpl_name}) || die "Couldn't open $TMPL{$tmpl_name}";
	$line =~ s/%%message%%/$dover_msg/;
	$line =~ s/%%hidden%%/$hidden/;
	$line =~ s/%%url%%/$q->url()/ge;
	
	foreach my $key (keys %current)
	{
		$line =~ s/%%$key%%/$current{$key}{value}/g;
		$line =~ s/%%class_$key%%/$current{$key}{class}/g;
	}
	$line =~ s/%%quick_links%%/Quick_Links()/e;
	print $q->header(-expires=>'now'), $line;
}

##### Prints an error message to the browser.
sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>";
	print $_[0];
	print "</B><BR>";
	print $q->end_html();
}

sub Load_Data{
###### this procedure presumes that the current keys are identical to the queried data
	foreach (keys %current){
		$current{$_}{value} = $data->{$_} || '';
	}
}

sub Load_Params{
###### populating our new array hash values
###### this loop depends on the %new hash keys being identical to the passed parameter names
	foreach (keys %new){
		$new{$_} = $q->param($_) || '';
		###### let's filter for extraneous characters (this may pose a problem with other
 		###### character sets) - we'll be more restrictive on the name fields
		if ( /name/ ){
			$new{$_} =~ s/\t|\r|\n|\f|\[M|\+|=|_//g;
		}
		###### we can skip the memo field
		elsif ( /memo/ ){next}
		elsif ( /url/ ){
			$new{$_} =~ s/\t|\r|\n|\f|\[M|"//g;
		}
		elsif ( /current_member_id/ ){
			$new{$_} =~ s//NULL/g;
		}
		else{
			$new{$_} =~ s/\t|\r|\n|\f|\[M|"|;|:|\+|=//g;
		}
	}
}

sub Quick_Links{
	my $links = $q->a({-href=>$ME, -class=>'q'}, 'New Search');
	my @list = ();
	
	if ($current{current_member_id}{value}){
		push (@list, $q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$current{current_member_id}{value}", -class=>'q', -target=>'_blank'}, 'Member record') );
	}
#	if ($current{emailaddress}{value}){
#		push (@list, $q->a({-href=>"mailTo:$current{emailaddress}{value}?Subject=ClubBucks Card # $current{id}{value} Info", -class=>'q'}, "Email $current{emailaddress}{value}") );
#	}

	if ($current{referral_id}{value}){
		push (@list, $q->a({-href=>"/cgi/admin/memberinfo.cgi?id=$current{referral_id}{value}", -class=>'q', -target=>'_blank'}, 'Referring Member record') );
	}

	foreach (@list){
		$links .= "&nbsp;&nbsp;&nbsp;$_\n";
	}

	$links .= "&nbsp;&nbsp;&nbsp;" . $q->a({-href=>"$ME?id=$current{id}{value}&action=arclist", -class=>'q'}, 'Archived Records');
	$links .= "\n&nbsp;&nbsp;&nbsp;";
	
	return $links;
}

##### Runs the specified search, pushing the results onto a list (array).
sub Run_Search
{
	my $table_name = shift;
	my $search_field = shift;
	my $search_value = shift || 'NULL';
	my $sth = '';

	if (grep { /=$search_field/ } @char_fields)
	{ 
		$sth = $db->prepare("SELECT c.*,
					COALESCE(m.emailaddress, '') AS emailaddress
					FROM 	$TBL{$table_name} c
					LEFT JOIN members m
						ON c.current_member_id = m.id
					WHERE 	c.$search_field ~* ?
					ORDER BY c.id");
	}
	else
	{
		$sth = $db->prepare("SELECT c.*,
					COALESCE(m.emailaddress, '') AS emailaddress
					FROM 	$TBL{$table_name} c
					LEFT JOIN members m
						ON c.current_member_id = m.id
					WHERE 	c.$search_field = ?
					ORDER BY c.id");
	}
	$sth->execute( $search_value );

	while ( $data = $sth->fetchrow_hashref)
	{
		push (@result_list, $data);
	}		

	$sth->finish();
}

sub Search_Form{
###### print our main search form with any pertinent messages
	my $msg = shift || '';
	# Open the search template.
	my $tmpl = &G_S::Get_Object($db, $TMPL{'search'}) || die "Failed to load template: $TMPL{'search'}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$msg/;
	print $q->header(), $tmpl;	
}

sub Update{
	my ($sth, $rv) = ();
 
	# Set value to NULL if no value was passed. Note, query will fail if id is NULL.
	$new{id} ||= 'NULL';
	$new{current_member_id} ||= 'NULL';
	$new{label} ||= '';
	$new{memo} ||= '';
	
	my $qry = "
	UPDATE $TBL{cbids} SET class= ?, current_member_id = $new{current_member_id},
		stamp= NOW(), label= ?, memo= ?, referral_id= ?, operator= ?
	WHERE id= $new{id}";

	my @list = ();
	foreach ( qw/class label memo referral_id/ )
	{
		push (@list, $new{$_});
	}	
	
	$sth = $db->prepare($qry);
	if ($rv = $sth->execute(@list, $operator)){
		$dover_msg = "<br /><span class=\"b\">Record Successfully Updated</span>\n";
	}
	else {
		$dover_msg = "<br /><span class=\"r\">Record Update Failed</span>\n";
	}

	$sth->finish;
	return $rv;	
}

sub validity_check{
##### Checks the validity of the Search Field and the Search Value before executing a query.
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			Search_Form( $q->span({-style=>'color: #ff0000'}, 'Invalid search value - Use only whole numbers with this field.') );
			return;	
		}
	}
	else
	{
		Search_Form( $q->span({-style=>'color: #ff0000'}, 'Invalid field specification - Try again.') );
		return;				
	}
	return 1;						
}

###### 04/30/03 Removed all references to the void and activated columns in the database.
###### 05/01/03 Added the rules for changing a 'clubbucks_master:current_member_id� value.
###### 06/10/03 removed the cbid_order_info quicklink
###### 08/14/03 Added the '7-Owned' option to the Class_Menu.
###### 08/26/03 Added the 'Archived Records' functionality.
###### 10/13/03 Added the email address to the page for Ted's use when forwarding order confirmations.
###### 12/05/03 Added 'unless ( defined $data->{$_} ) { $data->{$_} = '&nbsp;' }' to fill NULL values
######			in order to avoid uninitialized errors.



