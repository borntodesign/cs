#!/usr/bin/perl -w
# reverse-chargebacks.cgi
# create a chargeback reversal in the current period
# created: 05/13/09	Bill MacArthur
# this was originally created as a CLI script and then moved to a CGI
# last modified: 12/08/09	Bill MacArthur

use strict;
use DBI;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;
my ($errors, $db) = ();
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

if ( $operator && $pwd )
{
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
	$db->{RaiseError} = 1;
}
else
{
	die 'You must be logged in to use this interface.';
}

my %params = ();
foreach(qw/trans_id proceed/){
	$params{$_} = $q->param($_),
}

unless ($params{trans_id})
{
	InputForm();
	End();
}


my $orig_tid = $params{trans_id};
if ($orig_tid =~ m/\D/)
{
	$errors .= "The transaction ID should be digits only: $orig_tid";
	InputForm();
	End();
}

my $orig_member = $db->selectrow_hashref("
	SELECT m.id, m.alias, m.spid, m.membertype, m.emailaddress, m.firstname ||' '|| m.lastname AS name,
		tt.description, t.trans_type, t.amount, t.vendor_id, ta.npp, ta.ppp
	FROM members m
	JOIN transactions t
		ON t.id=m.id
	JOIN transaction_types tt
		ON tt.trans_type=t.trans_type
	JOIN ta_live ta
		ON ta.transaction_ref=t.trans_id AND ta.id=t.id
	WHERE t.trans_id= $orig_tid");

unless ($orig_member)
{
	$errors .= "No matching transaction found";
	InputForm();
	End(); 
}

print $q->header(-charset=>'utf-8'), $q->start_html(-encoding=>'utf-8'),
	$q->div("original: $orig_member->{alias} $orig_member->{name}, $orig_member->{emailaddress}"),
	$q->div({-style=>'margin-left:5em'}, "amount: $orig_member->{amount}, trans_type: $orig_member->{description}");

my $ta_list = $db->selectall_hashref("
	SELECT m.alias, m.emailaddress, m.firstname ||' '||m.lastname AS name,
		ta.pk, ta.npp, COALESCE(nbt.level,0) AS role, ta.id
	FROM transactions t
	JOIN ta_live ta
		ON ta.transaction_ref=t.trans_id
	JOIN members m
		ON ta.id=m.id
	LEFT JOIN nbteam_vw nbt
		ON nbt.id=ta.id
	WHERE t.trans_id= $orig_tid
	AND m.membertype='v'
	AND ta.npp > 0
	ORDER BY ta.pk", 'pk');

unless ($ta_list)
{
	print $q->p("No matching ta live records over 0 NPP");
	End();
}

my $vip_sponsor = ();
my $email_list = ();
my $chief = ();

print $q->start_form;
my $upline_list = '';
my $upline_rows = 0;
foreach (sort keys %{$ta_list})
{
	unless ($vip_sponsor){
		$vip_sponsor = $ta_list->{$_};
		print $q->p("VIP sponsor: $ta_list->{$_}->{alias} $ta_list->{$_}->{name}, $ta_list->{$_}->{emailaddress}");
		$email_list .= "$ta_list->{$_}->{emailaddress},";
	}
	elsif (! $chief){
		$email_list .= "$ta_list->{$_}->{emailaddress},";
		$upline_list .= "$ta_list->{$_}->{alias} $ta_list->{$_}->{name}, $ta_list->{$_}->{emailaddress}\n<br />";
		$upline_rows++;
	}
	$chief = 1 if $ta_list->{$_}->{role} >= 140;
}

print 'Upline:<br />';#, $q->textarea(-name=>'upline_list', value=>$upline_list, -cols=>80, -rows=>$upline_rows);
print $upline_list;
#chop $email_list;	# remove the trailing newline
chop $email_list;	# remove the trailing comma
print $q->p(
	'Email list:<br />' .
	$q->textarea(-value=>$email_list, -cols=>120, -rows=>2, -name=>'email_list', -onclick=>'this.select()')),
	$q->end_form;

if (! $params{proceed})
{
	print $q->start_form(-id=>'workfrm', style=>'margin-top:1em;'),
		$q->hidden(-name=>'trans_id', -id=>'trans_id'), "Proceed? ";
	print $q->submit(-name=>'proceed', -value=>'Yes'), ' ',
		$q->button(
			-value=>'No',
			-onclick=>"document.getElementById('trans_id').value=''; document.getElementById('workfrm').submit();");
	print $q->end_form, $q->end_html;
	End();
}

# I guess we have the OK to proceed
my ($trans_id) = $db->selectrow_array("SELECT NEXTVAL('transactions_trans_id_seq')") ||
	die "Failed to obtain a transaction ID to setup the reversal\n"; 
$db->begin_work;
$db->do("INSERT INTO transactions (trans_id, id, spid, membertype, trans_type, description, apportioned, vendor_id, amount, static)
	VALUES ($trans_id, $orig_member->{id}, $orig_member->{spid}, '$orig_member->{membertype}', $orig_member->{trans_type},
		'reversal of original transaction: $orig_tid', NOW(), ?, ($orig_member->{amount} * -1), TRUE );
", undef, $orig_member->{vendor_id});

# create the apportionment specifically for the original party
$db->do("INSERT INTO ta_live (id, ppp, npp, cv_spread, transaction_type, transaction_ref)
		VALUES ( $orig_member->{id}, ($orig_member->{ppp} * -1),
			($orig_member->{npp} * -1), TRUE, $orig_member->{trans_type}, $trans_id )
");
	
foreach (sort keys %{$ta_list})
{
	$db->do("INSERT INTO ta_live (id, npp, cv_spread, transaction_type, transaction_ref)
		VALUES ( $ta_list->{$_}->{id}, ($ta_list->{$_}->{npp} * -1), TRUE, $orig_member->{trans_type}, $trans_id )
	");
}
$db->commit;

print $q->p(
	'View the transaction: ' .
	$q->a({
			-href=>"/cgi/admin/tms/AOL.cgi?action=LO;TID=$trans_id",
			-onclick=>"window.open(this.href);return false;"
		}, $trans_id) ),
	$q->p( $q->a({-href=>$q->url}, 'Start Over') ),
	$q->end_html;
End();

######

sub End
{
	$db->disconnect();
	exit;

}

sub InputForm
{
	print $q->header,
		$q->start_html(-title=>'Reverse a Chargeback', -onload=>"document.getElementById('trans_id').focus()"),
		$q->start_form;
	print $q->p({-style=>'color:red'}, $errors) if $errors;
	print "Enter the original transaction ID that is to be reversed in the current period: ",
		$q->textfield(
			-name=>'trans_id',
			-onclick=>'this.select()',
			-style=>'margin-left:0.5em; margin-right:0.5em;',
			-id=>'trans_id'),
		$q->submit, $q->end_form, $q->end_html;
}

# 12/08/09 introduced the 'static' column in the transactions table for use when creating these reversals... we do not want them reapportioned when a line of sponsorship changes
