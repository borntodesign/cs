#!/usr/bin/perl -w
###### 01-mv-members.cgi
###### the member, partner, VIP, what have you transfer interface for use by Dick
###### newly created as a direct copy of the existing mv-members.cgi script: Feb. 2014	Bill MacArthur
###### last modified: 05/12/15	Bill MacArthur

use strict;
use Digest::MD5 qw( md5_hex );
use Encode;
use Template;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
require ADMIN_DB;
require cs_admin;
use G_S qw(%MEMBER_TYPES $EXEC_BASE $LOGIN_URL);
use GI;
use XML::local_utils;
#binmode STDOUT, ":encoding(utf8)";
#use Data::Dumper;	# only for debugging

my $lu = XML::local_utils->new;
my $TT = Template->new;
my $gs = new G_S;
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my $Path = $q->path_info || '';
my $ME = $q->script_name;# . $Path;
$Path =~ s#^/##;

my $key = 'onykey!23#';
my $LIMIT = 25;	###### the default number of results we will return for a list

my $defActivityType = 22;		# when we create an activity record for this transfer, this will be it's type
my $tpXferActivityType = 70;	# the type we will use for transfers of Trial Partners
my $smXferActivityType = 71;	# the type we will use for transfers of Members/Affiliates

my %TMPL = (
	'no-pay'	=> 1319,
	'Error'		=> 1307,
	'screen1'	=> 1322,
	'XML'		=> 1323,
	'screen2'	=> 1324,
	'screen3-partner'	=> 1327,
	'screen3'	=> 1325,
	'screen4'	=> 1326
);

my ($admin, $db, $realOP, %param, @mvid, $lang_blocks, @alerts, @lp, @countries) = ();

###### these globals pertain to an actual transfer
my ($firstID, $lastID, $transfer_list, $proc_status) = '';

my $recipient = {};
my $activity = $q->param('activity') || '';
my $formlist = \'';

Load_Params();
my $transferee = $param{'transferee'};

###### evaluate our user
###### we will first check for admin credentials
###### if present, we will go in as 01 with 'super credentials'
###### a regular login as 01 will permit the same operations
if (my $ck = $q->cookie('cs_admin'))
{
	my $creds = $cs->authenticate($ck);
	exit unless $db = ADMIN_DB::DB_Connect( 'mv-members', $creds->{'username'}, $creds->{'password'} );
	$db->{'RaiseError'} = 1;
	$db->{'pg_enable_utf8'} = 0;
	$realOP = {
		'id'		=> 1,
		'alias'		=> $creds->{'username'},
		'firstname'	=> $creds->{'username'},
		'lastname'	=> 'The DHS Club',
		'emailaddress'	=> '',
		'spid'		=> 1,
		'membertype'=> 'v'
	};
	$admin = 1;
#	$db->{'AutoCommit'} = 0;	# for testing set to -0-
}
else
{
	die "This is an admin only interface. A DB login is required.";
}

Load_lang_blocks();

unless ($transferee)
{
	_selectTransferee();
}
else
{
	my $c = $transferee =~ m/\D/ ? 'alias=?' : 'id=?';
	$transferee = $db->selectrow_hashref('SELECT id, spid, alias, firstname, lastname, language_pref FROM members WHERE ' . $c, undef, $transferee);
	$realOP->{'firstname'} = "$realOP->{'alias'} acting on behalf of $transferee->{'alias'} - $transferee->{'firstname'} $transferee->{'lastname'}";
	# put any checks in here that end up being required
}

unless ($activity)
{
	push @alerts, $q->div({'-class'=>'y_alert'}, "ADMIN - $lang_blocks->{'recipient_prompt'}");
	Do_Page( $TMPL{'screen1'} );
}

###### the same goes here
###### unless we need different content based on transfer type
###### we will just present our form with the proper block showing for this step						
unless ($param{'pd'})
{
	push @alerts, $q->div({'-class'=>'note_nop'}, "ADMIN - $lang_blocks->{'select_recipient'}");
	Do_Page($TMPL{'screen1'});
}
elsif (! $param{'pd_hash'})
{
	###### verify the recipient
	unless (Validate_Recipient())
	{
		Do_Page( $TMPL{'screen1'} );
	}
}

###### we should have a pd_hash for the activities beyond this point
unless (Validate_Pid($param{'pd_hash'}))
{
	Err('PD hash failed to validate');
}
else
{
	$recipient = Generate_Pid_Hash($param{'pd_hash'});
	if ( $param{'pd'} !~ /$recipient->{'id'}|$recipient->{'alias'}/ )
	{
		Err('PD hash doesn`t match PD');
	}
}

if ($activity eq 'start')
{
	Do_Page(
		$TMPL{'screen2'},
		{'title'=>'title2', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'cboLP'=>cboLP(), 'cboCountries'=>cboCountries()}}
	);
}
elsif ($activity eq 'getlist')
{
	Do_Page(
		($param{'transfer_type'} eq 'v' ? $TMPL{'screen3-partner'} : $TMPL{'screen3'}),
		{'title'=>'title3', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'list' => Create_List($param{'transfer_type'})}}
	);
}
elsif ($activity eq 'transfer')
{
	my $rv = ();
	if (Pre_Transfer_Checks())
	{
		$rv = Transfer_Group();
	}
	else
	{
		Exit();
	}
	
	if ($rv)
	{
		Update_Transfer_SEQ();
	}

	$proc_status .= 'Job finished.';
	Do_Page(
		$TMPL{'screen4'},
		{'title'=>'title4', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'proc_status'=>$proc_status}}
	);
}
else
{
	Err('Invalid activity received.');
}

Exit();

###### start of subroutines

sub _selectTransferee
{
	print
		$q->header,
		$q->start_html('Select donor party');
		
	foreach (@alerts)
	{
		print $q->div({'-style'=>'color:red'}, $_);
	}
	
	print
		$q->h4('Enter donor party ID'),
		$q->start_form,
		$q->textfield('-name'=>'transferee') . '&nbsp;' . $q->submit,
		$q->end_form,
		$q->end_html;
		
	Exit();
}

sub CanSponsor
{
	my $mtype = shift;
	my %a = ();

# per Dick 02/12/14: Bill - we really need to update the Transfer Function to allow for the transfer of Trial Partners to downline TPs or Partners.
	my $labels = $db->selectall_hashref('
		SELECT mt."code", mt.label
		FROM member_types mt', 'code');
	my %perms = (
		'v'=>[ qw/v ma ag m s tp/ ],
		'm'=>[ qw/s/ ],
		'tp'=>[ qw/m s tp v/ ],
		'ag'=>[ qw/s/ ]
	);

	foreach my $tmp (@{$perms{$mtype}})
	{
		$a{$tmp} = {'can_sponsor'=>$tmp, 'label'=>$labels->{$tmp}->{'label'}};
	}
	return \%a;
}

sub cboCountries
{
	my $list = $db->selectall_arrayref('SELECT country_code, country_name FROM country_names_translated(?)', undef, $realOP->{'lang_pref'});
	my @vals = '';
	my %labels = ();
	foreach (@{$list})
	{
		push @vals, $_->[0];
		$labels{$_->[0]} = $_->[1];
	}
	return $q->popup_menu(
		'-name'=>'countries',
		'-values'=>\@vals,
		'-labels'=>\%labels,
		'-multiple'=>1
	);
}

sub cboLP
{
	my $list = $db->selectall_arrayref('SELECT code2, description FROM language_codes(?) WHERE active=TRUE', undef, $transferee->{'language_pref'});
	my @vals = '';
	my %labels = ();
	foreach (@{$list})
	{
		push @vals, $_->[0];
		$labels{$_->[0]} = $_->[1];
	}
	return $q->popup_menu(
		'-name'=>'lp',
		'-values'=>\@vals,
		'-labels'=>\%labels,
		'-multiple'=>1
	);
}

sub Check_Transfer_SEQ
{
	unless ($param{'seq'})
	{
		Err('Missing seq parameter.');
	}

	my ($seq, $complete) = $db->selectrow_array("
		SELECT seq, complete
		FROM vip_transfers_log
		WHERE 	transferer= $transferee->{'id'}
		AND 	seq= $param{'seq'}
	");

	unless ($seq)
	{
		Err('Seq not found or does not match.');
	}
	elsif ($complete)
	{
		Err($lang_blocks->{'already_done'});
	}

	return 1;
}

sub Create_List
{
	my $mtype = shift;
	
	# Dick wants both m's and s's to be included in the "Members" lists
# as of 09/24/12 Dick says "Break them out please."
	# and since we cannot simply insert other bare user submitted data directly into the SQL, we will create a mask
	# the value of a given hash key will be directly inserted into the SQL to provide a membertype IN (xxx)
	my %mtype_map = (
		'm'=>"'m'",
		'v'=>"'v'",
		'ag'=>"'ag'",
		'ma'=>"'ma'",
		's'=>"'s'",
		'tp'=>"'tp'"
	);
#	my $count = 0;

###### develop a query of potential transferees

	my $limit = $param{'limit'} || $LIMIT;
	# ignore values that are not strictly numeric
	$limit = $LIMIT if $limit =~ m/\D/;

	my $orderBy = $param{'sortOrder'} || 'DESC';
	# ignore values that are outside of what we expect
	$orderBy = 'DESC' unless $orderBy =~ m/^asc$|^desc$/i;
	
	my $AND = '';

	if (@lp)
	{
		my @tmp = ();
		push @tmp, $db->quote($_) foreach @lp;
		$AND = 'AND m.language_pref IN (' . join(',', @tmp) . ')';
	}

	if (@countries)
	{
		my @tmp = ();
		push @tmp, $db->quote($_) foreach @countries;
		$AND = ' AND m.country IN (' . join(',', @tmp) . ')';
	}

# with the advent of TNT 2.0 and the transferral of PRP credit, we have to guard against sponsor inversion in our nspid line
# this can occur if we transfer someone on our frontline to somebody else who is actually at a lower position in the nspid line...
# making it impossible for the new sponsor to see their newly transferred Partner
# since actually filtering each and every potential transferee against the recipient would exert an enormous cost,
# we will instead compare network.depths and should be almost as effective with almost no cost
# so let's get the network.depth for our recipient and we will exclude anybody whose network depth is less
#	my ($flp_depth) = $db->selectrow_array('SELECT depth FROM network.depths WHERE id= ?', undef, $param{'flp'}) || 0;
	my ($recipient_depth) = $db->selectrow_array('SELECT depth FROM network.depths WHERE id= ?', undef, $recipient->{'id'}) || 0;
	
###### this SQL is used in all cases
###### we'll add things onto the end as necessary for the individual list type
	my $qry = qq#
		SELECT DISTINCT
			m.id,
			m.spid,
			m.alias,
			m.mail_option_lvl AS ms,
			d.depth,
			CASE
				WHEN m.membertype= 'ag' THEN m.company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			CASE
				WHEN m.membertype <> 'ma' THEN '&nbsp;'
				ELSE 'Merchant Affiliate'
			END AS flags,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.mail_option_lvl,
			membertype,
			mt.display_code,
			COALESCE(lc.description,'') AS language_pref_name,
			m.language_pref,
			cnt.country_name
	
		FROM country_names_translated(?) cnt, depths d
		JOIN network.depths nd
			ON nd.id=d.id
		JOIN members m
			ON m.id=d.id
		JOIN member_types mt
			ON mt.code=m.membertype
		LEFT JOIN language_codes(?) lc
			ON lc.code2=m.language_pref			
		WHERE 	cnt.country_code=m.country
		AND		m.spid= ? --rid->{id}

	-- exclude 'Front Line Point' which may or may not be our recipient
		AND 	m.id != ?

	-- exclude BEMs... but only when they are NOT VIPs
		AND 	(m.membertype IN ('v') OR m.mail_option_lvl > 0)
		
	-- exclude parties who are at a higher network depth so we can mitigate nspid sponsor inversion..
		AND nd.depth >= $recipient_depth
	#;

	$qry .= "$AND AND m.membertype IN ($mtype_map{$mtype})";

	$qry .= "\nORDER BY m.id $orderBy LIMIT $limit";
#die "id: $realOP->{'id'}\nflp: $param{'flp'}\nmtype: $mtype\n$qry";
	my $sth = $db->prepare($qry);
	$sth->execute($transferee->{'language_pref'}, $transferee->{'language_pref'}, $transferee->{'id'}, $param{'flp'});

	my $bg = '#ffffee';
	my $datclass = ();
	my $list = '';

	while (my $row = $sth->fetchrow_hashref)
	{
		$list .= qq|<tr bgcolor="$bg">\n|;
		$list .= $q->td({'-class'=>'data'}, 
				"$row->{'alias'},
				$row->{'firstname'} $row->{'lastname'}") . "\n" .

			$q->td({'-class'=>'data', '-align'=>'center'}, $row->{'display_code'}) . "\n";

		$list .= $q->td({'-class'=>'data', '-align'=>'center'}, $row->{'ms'}) . "\n";

		$list .= $q->td({'-class'=>'data'}, $row->{'language_pref_name'}) . "\n";
		
		$list .= $q->td({'-class'=>'data'}, $row->{'country_name'}) . "\n";

		$list .= $q->td({'-class'=>'confirm'}, $row->{'flags'}) . "\n";

		$list .= $q->td( ctl_mvid($row) ) . "\n";

		$list .= "</tr>\n";
		$bg = ($bg eq '#ffffff') ? '#ffffee' : '#ffffff';
	}
	$sth->finish;

	if ($list)
	{
		my $hdr = $q->Tr({'-class'=>'h5', '-bgcolor'=>'#eeffee'}, $q->th(
			[
			'Name',
			'Type',
			$q->a({'-href'=>'#',
				'-class'=>'hdr',
				'-title'=>$lang_blocks->{'click4expl'},
				'-onclick'=>"whatIsThis('ms'); return false;"}, 'MS'),
			$q->a({'-href'=>'#',
				'-class'=>'hdr',
				'-title'=>$lang_blocks->{'lp'}}, 'LP'),
				'Country',
			'&nbsp;',
			'&nbsp;'
			]
		)) . "\n";
		$list = $q->table(
			{
			'-border'=>1,
			'-cellspacing'=>0,
			'-cellpadding'=>4
			},
			$hdr . $list) ."\n";

#		$list = "<table><tr><td align=\"right\" style=\"font-size:3px\">
#			${\Create_List_Form(\$list)}
#			</td></tr></table>\n";
		$list = Create_List_Form(\$list);
 	}
	else
	{
		$list = $q->p({'-class'=>'blu_alert'}, $lang_blocks->{'no_transferees'});
	}
#warn $list;
	return $list;
}

sub Create_List_Form
{
	my $list = shift;
	my $form = $q->start_form(
		'-action'=>$ME,
		'-name'=>'list',
		'-method'=>'post',
		'-onsubmit'=>'return ListFrmSubmit(this)'
	) . "\n";
	$form .= $q->hidden('-name'=>'activity', '-value'=>'transfer', '-force'=>1) . "\n";
	$form .= $q->hidden('transfer_type') . "\n";
	$form .= $q->hidden('transferee');
	
	###### in order to keep them from submitting multiple times
	###### we'll give them a variable number 'key' to go with this transfer
	###### if they later try to submit more than once we can stop 'em
	my ($seq) = $db->selectrow_array("SELECT NEXTVAL('generic_pk_seq')");
	unless ( $db->do("
		INSERT INTO vip_transfers_log (seq, recipient_id, transferer)
		VALUES ($seq, $recipient->{'id'}, $transferee->{'id'})") )
	{
		return $q->p({'-class'=>'confirm'}, 'Failed to setup transfer sequence.');
	}
	$form .= $q->hidden('-name'=>'seq', '-value'=>$seq);

	###### if this is an AP or CSR list
	###### we'll provide buttons at the top & bottom
	###### since it could be a lengthy list
	$form .= ctl_buttons() if ($param{'transfer_type'} eq 'm' || $param{'transfer_type'} eq 's');
	
	foreach('pd','pd_hash','rid','rid_hash')
	{
		$form .= "\n" . $q->hidden($_);
	}

	$form .= "<br /><br />\n$$list\n<br /> ";
	###### lets put in our buttons if this is not a Partner transfer... we will be providing specially labeled buttons directly in the template in that case
	$form .= ctl_buttons('x') unless $param{'transfer_type'} eq 'v';
	$form .= "\n</form>\n";
	return $form;
}

sub ctl_buttons
{
	my $arg = shift;

	###### if I am going to have a duplicate set of buttons, I'll need
	###### to create distinct names
	$arg = $arg ? ($arg . '_') : '';
	my $buttons = ();

	###### we will have an additional 'select all' button
	###### only for simple stack transfers
	if ($Path eq 'simple' || $Path eq 'autostack')
	{
		$buttons = $q->button(
			'-value'=>$lang_blocks->{'select_all'},
			'-onClick'=>"for(x=0; x<document.forms.list.mvid.length; x++) document.forms.list.mvid[x].checked=true;",
			'-class'=>'btn_select_all',
			'-onmouseout'=>'resetBtn(this)',
			'-onmouseover'=>"setBtn(this, 'select_all')",
			'-name'=>$arg . 'select_all'
		) . "\n";
	}

	$buttons .= $q->button(
		'-value'=>$lang_blocks->{'cancel'},
		'-onClick'=>"location.href='$ME}'",
		'-class'=>'btn_cancel',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'cancel')",
		'-name'=>$arg . 'cancel'
	) . "\n";

	$buttons .= $q->reset(
		'-class'=>'btn_reset',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'reset')",
		'-value'=>$lang_blocks->{'reset'}
	) . "\n";

	$buttons .= $q->submit(
		'-value'=>$lang_blocks->{'perform_xfer'},
		'-class'=>'btn_transfer',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'transfer')"
	)  . "\n";

	return $buttons;
}

sub ctl_mvid
{
	###### since we want to provide radio buttons instead of checkboxes sometimes
	###### we'll make the determination here
	my $row = shift;

	# Dick wants them to only be able to transfer 1 CSR at a time (really only one per day)
	# for admin use Dick wants to have a big gun, so checkboxes to do it all in one fell swoop
#	if ((grep $_ eq $param{'transfer_type'}, (qw/m s/)) && $recipient->{'membertype'} ne 'mp')
#	{
		return $q->checkbox(
			'-class'=>'input',
			'-name'=>'mvid',
			'-value'=>Generate_Pid($row),
			'-label'=>''
			);
#	}
#	else
#	{
#		return $q->radio_group(
#			'-class'=>'input',
#			'-name'=>'mvid',
#			'-value'=>Generate_Pid($row),
#			'-nolabels'=>1,
#			'-default'=>0
#			);
#	}
}

sub Do_Page
{
	my ($TMPL, $args) = @_;

	###### if I receive a digit only, I know I have to retrieve the template
	if ($TMPL !~ /\D/)
	{
		$TMPL = $gs->Get_Object($db, $TMPL) || die "Failed to retrieve template: $TMPL";
	}
	
	my $rv = '';
	# take care of our language nodes
	Parse_Lang_Blocks({'realOP' => $transferee, 'user' => $realOP, 'recipient' => $recipient});
#warn Dumper($args);
	my %data_hash = (
			'lang_blocks' => $lang_blocks,
			'realOP' => $realOP,
			'user' => $realOP,
			'alerts' => \@alerts,
			'script_name' => $ME,
			'param' => \%param,
			'hidden_lp' => Hidden_LP(),
	, %{ $args->{'data'} || {} } );

#warn Dumper(\%data_hash);
	$TT->process(\$TMPL, \%data_hash, \$rv);
		
	my $GI = GI->new();
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>($lang_blocks->{ $args->{'title'} || '' } || $args->{'title'} || $lang_blocks->{'title1'}),
			'style'=>$GI->StyleLink('/css/mv-members.css'),
			'content'=>\$rv,
			'script'=>'<script type="text/javascript" src="/js/mv-members.js"></script>'
		});
		
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	my $msg = shift || '';
	my $tmpl = shift || $TMPL{'Error'};
	if ($db)
	{
		$tmpl = $gs->Get_Object($db, $tmpl) ||
			die "Failed to load template: $tmpl\n\nPlus you have this 'error':\n$msg\n";
	}

###### if we don't have a DB connection, then we'll setup a vanilla template
	else
	{
		$tmpl = $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Transfer error');
		$tmpl .= '%%error_msg%%<br /><br />';
		$tmpl .= '%%timestamp%%';
		$tmpl .= $q->end_html();
	}

	$tmpl =~ s/%%error_msg%%/$msg/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print 	$q->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
	Exit();
}

sub Generate_Pid
{
###### my pid will consist of 7 characters of a hash + the transfer ID
###### & a pipe delimited list of personal data
	my $row = shift;
	return unless $row;

	my $pid = "$row->{'id'}|$row->{'spid'}|$row->{'alias'}|$row->{'firstname'}|";
	$pid .= "$row->{'lastname'}|$row->{'emailaddress'}|$row->{'membertype'}|";
	$pid .= "$row->{'depth'}|$row->{'ms'}";
	$pid = md5_hex($pid . $key) . "|$pid";
}

sub Generate_Pid_Hash
{
###### generate a hash based on the data in the passed pid and pass it back as a ref
###### refer to Generate_Pid for the values contained in the pid
	my $pid = shift;
	return unless $pid;
	my @vals = split(/\|/, $pid);
	shift @vals;
	my %r = (
		'id'		=> $vals[0],
		'spid'		=> $vals[1],
		'alias'		=> $vals[2],
		'firstname'	=> $vals[3],
		'lastname'	=> $vals[4],
		'emailaddress'	=> $vals[5],
		'membertype'	=> $vals[6],
		'depth'		=> $vals[7],
		'ms'		=> $vals[8]
	);
	return \%r;
}

sub Get_OID
{
	return $db->selectrow_array("SELECT oid FROM members WHERE id= $_[0]");
}

sub Get_Member_Record
{
	my $id = shift;
	return unless $id;

	my $rv = $db->selectrow_hashref(qq#
		SELECT m.id,
			m.spid,
			m.alias,
			CASE
				WHEN m.membertype= 'ag' THEN company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.membertype,
			mt.display_code,
			d.depth,
			m.mail_option_lvl AS ms,
--			CASE
--				WHEN m.language_pref ISNULL OR m.language_pref='' THEN 'en'
--				ELSE m.language_pref
--			END AS language_pref,
			COALESCE(m.language_pref,'') AS language_pref,	-- is defaulting to English really the best option?
			m.oid
		FROM members m
		JOIN depths d
			ON m.id=d.id
		JOIN member_types mt
			ON mt."code"=m.membertype
		WHERE 	${\($id =~ /\D/ ? 'm.alias' : 'm.id')} = ?
	#, undef, $id);
	
	($rv->{'language_pref_name'}) = $db->selectrow_array('SELECT description FROM language_codes(?, ?)', undef, $rv->{'language_pref'}, $realOP->{'lang_pref'}) || '';
	return $rv;
}

sub Hidden_LP
{
	my $rv = '';
	foreach (@lp)
	{
		$rv .= $q->hidden('-name'=>'lp', '-force'=>1, '-value'=>$_);		
	}
	return $rv;
}

sub Load_lang_blocks
{
	my $xml = $gs->Get_Object( $db, $TMPL{'XML'} ) || die "Failed to load lang_blocks\n";
	$lang_blocks = $lu->flatXMLtoHashref($xml);
}

sub Load_Params
{
	my ($tmp) = ();

	foreach ('pd_hash', 'rid_hash', 'transfer_type', 'transferee')
	{
		$param{$_} = $q->param($_) || '';
	}

	$param{'pd'} = $gs->PreProcess_Input(uc $q->param('pd')) || '';

	###### load specific parameters that we want to do bounds checking on
	foreach ('flp','limit','seq')
	{
		if ($tmp = $q->param($_) || '')
		{
			die 'Invalid $_ parameter received' if $tmp =~ /\D/;
			$param{$_} = $tmp;
		}
	}

	if ($tmp = $q->param('sortOrder'))
	{
		unless ($tmp =~ /^asc$|^desc$/)
		{
			die 'Invalid orderBy parameter received';
		}
		$param{'sortOrder'} = $tmp;
	}

	@mvid = $q->param('mvid');
	foreach($q->param('lp'))
	{
		next unless $_;
		push @lp, $_;
	}

	foreach($q->param('countries'))
	{
		next unless $_ && m/[A-Z]{2}/;	# this param should be a 2 uppercase letter country code
		push @countries, $_;
	}
}

sub Parse_Lang_Blocks
{
	# the reason for doing this way is that we don't have to worry about breaking the XML
	# *prior* to parsing it out into lang_blocks
	# we can do our conversion to a hash and then take care of each node individually
	my $data = shift;

	foreach my $k (keys %{$lang_blocks})
	{
		$lang_blocks->{$k} = Parse_Lang_Blocks_Node($k, $data);
	}
}

=head3 Parse_Lang_Blocks_Node

While we will want to perform substitutions on the general set of nodes prior to finally rendering a page,
there are cases, such as during error generation where we will stuff a node into the @alerts array
and we need a particular node to have substitution performed on it before the whole is finally done.

This routine performs that one-off substitution and is used by Parse_Lang_Blocks to boot

=cut

sub Parse_Lang_Blocks_Node
{
	my ($node_name, $data) = @_;
	my $rv;
	$TT->process(\$lang_blocks->{$node_name}, $data, \$rv);
	return $rv;
}

sub Pre_Transfer_Checks
{
	if (@mvid)
	{
		return unless Check_Transfer_SEQ();

		return 1;
	}
	else
	{
		###### evidently there were no transferees selected
	#	$alert .= $q->p({-class=>'confirm'}, 'You failed to make your selections.');
	#	$formlist = Create_List();
	#	Do_Page($TMPL{main}, 'list');
	###### we don't have the parameters to to create a list so we'll error
	###### out and let them press the back button
		Err("You failed to make your selections.<br />
			Please back up and check one or more parties to transfer.");
		return;
	}
}

sub Transfer_Group
{
###### we'll transfer a group of members to another party
###### we'll build our transfer LIST as we go
###### we have our IDs as the keys of the hash and our matching data as the value
	my ($qry, $row) = ();
	my $memo = "Transferred by $realOP->{'alias'}";

	($recipient->{'pspid'}) = $db->selectrow_array("SELECT pid FROM network.pspids WHERE id= $recipient->{'id'}");
	
	foreach(@mvid)
	{
		unless (Validate_Pid($_))
		{
#			print "Skipping - Error validating pid: $_ <br />\n";
			$proc_status .= "Skipping - Error validating pid: $_ <br />\n";
			next;
		}
		$row = Generate_Pid_Hash($_);
		
#		print "Transferring $row->{'alias'} <br />\n";
		$proc_status .= "Transferring $row->{'alias'} <br />\n";
		$qry = "
			UPDATE members SET
				spid= $recipient->{'id'},
				stamp= NOW(),
				memo= '$memo',
				operator= 'mv-members.cgi'
			WHERE id = $row->{'id'}
			AND spid= $transferee->{'id'};	-- just to be sure for admin use that we don't run into any BAAAd things

			UPDATE depths SET
				depth= ($recipient->{'depth'} +1)
			WHERE id= $row->{'id'};
			
			INSERT INTO backend_processing (
				val_int1,
				val_int2,
				proctype
			) VALUES (
				$row->{'id'},
				$recipient->{'id'},
				7
			);";

	###### our activity report entry (if we are not transferring CSR members to a pool)
	###### credit goes to the operator
#		my $acttype = $row->{'membertype'} eq 'tp' ? 70 : 22;
		my $acttype = $row->{'membertype'} eq 'tp' ? $tpXferActivityType :
				$row->{'membertype'} eq 'm' ? $smXferActivityType :
				$row->{'membertype'} eq 's' ? $smXferActivityType :
				$defActivityType;
		$qry .= "
			INSERT INTO activity_rpt_data (
				id,
				spid,
				activity_type,
				val_text
			) VALUES (
				$row->{'id'},
				$recipient->{'id'},
				$acttype,
		-- put in our realOP rather than the one being managed if applicable
				${\$db->quote($realOP->{'alias'})}
			);";

		if ($recipient->{'membertype'} eq 'v')
		{
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}, $row->{'emailaddress'}\n";
		}
		else
		{
	###### if it is not a VIP receiving the transfer, we use a different format
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}\n";
		}

#print "qry = $qry\n"; Exit();

		$db->do($qry);
		$firstID ||= $row->{'id'};
	}

	# if it was a pool receiving the transfer
#	print " - Done.<br />\n";
	$proc_status .= " - Done.<br />\n";

	if ($transfer_list)
	{
		return 1;
	}

	return undef;
}

sub Update_Transfer_SEQ
{
	$db->do("
		UPDATE vip_transfers_log
		SET complete= TRUE
		WHERE transferer= $transferee->{'id'} AND seq= $param{'seq'}
	");
}

sub TransferTypeOptions
{
	my @options = ();

	# generate our membertype options that this recipient can receive
	$recipient->{'can_sponsor'} ||= CanSponsor($recipient->{'membertype'});
	
	# we must assume that "somebody" is going to specify the order of the options
	# and since I am not going to add more data to the DB for doing this
	# I will do a sort in here even though it means if another membertype is added that should be transferrable
	# this code will have to be embellished
#	foreach my $mt (qw/v ag ma m s/)
	foreach my $mt (qw/v ag ma m s tp/)	# 01/21/13 the option to transfer Partners is being dropped whilst Dick is at work with reorganization for TNT 2.0
	{
		push @options, $recipient->{'can_sponsor'}->{$mt} if $recipient->{'can_sponsor'}->{$mt};
	}
	return \@options;	
}

sub Validate_Pid
{
###### my pid will consist of a hash & a pipe delimited list of 
###### personal data, the first field of which is the numeric ID

	my $var = shift;
	return unless $var;
	my @val = split(/\|/, $var, 2);

	$val[1] =~ /(^\d*)\|/;

	return ( md5_hex( $val[1] . $key) eq $val[0] ) ? 1 : 0;
}

sub Validate_Recipient
{
	$recipient = Get_Member_Record($param{'pd'});

	unless ( $recipient )
	{
		# the specified recipient was either empty or has no match in the DB
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'invalid_recipient'});
		return;
	}
	elsif ( $recipient->{'id'} == $transferee->{'id'})
	{
		# you cannot be the recipient... duh
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'ucannot'});
		return;
	}
	
	# CanSponsor will return an empty hashref if nothing else
	$recipient->{'can_sponsor'} = CanSponsor($recipient->{'membertype'});
	unless ( keys %{$recipient->{'can_sponsor'}} )
	{
		push @alerts, $q->div({'-class'=>'y_alert'}, Parse_Lang_Blocks_Node('cannot_receive_transfers', {'recipient'=>$recipient}) );
		return;
	}
	
	my ($rv) = ();
	my $tmp = $recipient;
	my $startID = $recipient->{'spid'};
	my $sth = $db->prepare("
		SELECT id,
			spid,
			alias,
			CASE
				WHEN membertype= 'ag' THEN company_name
				ELSE firstname
			END AS firstname,
			CASE
				WHEN membertype= 'ag' THEN ''
				ELSE lastname
			END AS lastname,
			emailaddress,
			membertype,
			0 AS depth,
			mail_option_lvl AS ms
		FROM members
		WHERE id= ?");

	###### flp will be our Front Line Point of connection to the recipient
	###### we'll need this value later so we can exclude them from any lists
	my $flp = $tmp->{'id'};

	while ($tmp->{'spid'} != $transferee->{'id'} && $tmp->{'spid'} > 1)
	{
		$rv = $sth->execute($tmp->{'spid'});
		die "Failed to retrieve sponsor record for: $tmp->{'spid'}" unless $rv;

		$tmp = $sth->fetchrow_hashref;
		die "Loop detected!" if $tmp->{'spid'} == $startID;

		$flp = $tmp->{'id'};
	}

	if ($tmp->{'spid'} == 1 && $transferee->{'id'} != 1)
	{
		# OK, we may be in our nspid organization, that's why we made it all the way to the top
		($rv) = $db->selectrow_array("SELECT network.is_nspid_above_you($recipient->{'id'}, $transferee->{'id'})");
		unless ($rv)
		{
			push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'not_in_organization'});
			return;
		}
	}

	# if the recipient is only in our nspid organization, then this value will end up being 01 or somewhere above us,
	# but it won't matter for what we are using the flp for in the next step, namely that we do not select somebody on my frontline who is upline of the recipient
	$param{'flp'} = $flp;

###### we'll assign the numeric ID to pd just in case we don't have it
###### and also set the pd param
	$param{'pd'} = $recipient->{'id'};
	$param{'pd_hash'} = Generate_Pid($recipient);
	$param{'recipient'} = $recipient;	# we will need this data when we Do_Page()
	return $recipient;
}

###### 11/24/04 changed the list form's method to an explicit 'post' since some VIPs
###### were having trouble submitting more than 13 members for transfer
###### also fixed up the utf8 mailing to properly encode the data and doctor the headers
###### 12/20/04 fixed the broken checks following the VIP under member check towards the top
###### also added a restriction on the amount of PP a party can have before being
###### disqualified from being transferred
###### 01/12/05 fixed the cases where $q->param was embedded in error messages bare
###### 01/14/05 added a coalesce to the language_pref col in getting the member rec.
###### 01/17/05 revised the %%script_name%% placeholder to get q->url instead so that
###### the path information would not be embedded (this was causing problems in jump to)
###### 01/18/05 made the emailing alert conditional on the recipient's membertype
###### 01/27/05 added a hack to allow 01 to perform transfers over 250 PP
###### added another hack to allow 01 to override an explicit denial of mgt. rights
###### 02/11/05 put the actual network calcs table into the mix rather than
###### the view which was killing us
###### 05/10/05 just a little javascript tweak and a little preprocessing of the pd param
###### 05/12/05 changed the error handler in Pre_Transfer_Checks to simply abort
###### 09/08/05 revised to allow managing members with the exclusion of parties
###### originally sponsored by the member
###### 01/25/06 Added -charset=utf-8 to the $q->header statement.
###### to pull in the necessary level and role information for real and rid
# 07/06/06 revised the SQL for list generation to show AG company names
###### 02/08/08 further tweakage of the BEM exclusion implemented in Jan. '08
# 09/16/08 added an insert for notification type 81 on a VIP transfer
###### 07/15/09 disabled mailings to recipients with bad mail setting
###### just quickie revisions to move this to the GI domain
###### 11/18/10 integration of the transfer of AMPs with VIPs
# 02/24/11 disabled the email to VIP recipients of a transfer
###### 01/10/12 lot's of changes involving many things, like who can sponsor whom and a lot of code cleanup
###### 04/09/12 ripped out things related to 'amp's
###### 09/24/12 added 's' membertype to the list in TransferTypeOptions per Dick's request to be able to transfer "Members"
###### 10/27/12 added functionality to enable the generated lists to match the language pref of the recipient or to select from a language list
###### these changes also involved displaying language prefs of the recipient and the potential transferees
###### 03/14/13 weaving the nspid network into this thing as far as being able to transfer to recipients who may be only in the nspid network
###### 03/20/13 implemented a primitive hack to filter out potential transferees when their network depth is less than the recipients to try and mitigate nspid sponsor inversion
###### 04/09/13 made the previous really work as it was relying on the regular depth of the recipient rather than the network.depth
###### 04/11/13 we'll get this right yet... it was excluding those of greater depth instead of those of lesser depth
###### 04/18/13 yet another tweak to the previous logic and also introduced the reassignment to a different pool if the party being transferred is not a Partner
###### 07/10/13 added Affiliates (m) to the list is CanSponsor() for Shoppers
###### 11/08/13 added the ability to transfer certain membertypes to Trial Partners
###### 02/12/14 added the ability to transfer Trial Partners themselves, also reinstituted the nspid check for all membertypes during the transfer list development to ensure we do not get sponsor inversion
###### 02/13/14 disabled the network.spids management integration (updating network.spids) as we no longer need to much around with that
###### 08/29/14	added in the backend_processing record creation
###### 01/21/15 made the activity type, created in Transfer_Group(), dependent on the membertype of the transferee
###### 03/24/15 further extended the variety of activity types to mimic the current behavior of the real partner interface
# 05/08/15 added binmode to deal with encoding issues that have arisen from the latest update of DBD::Pg
