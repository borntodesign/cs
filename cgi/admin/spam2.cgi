#!/usr/bin/perl -w
###### spam2.cgi
###### generates the spam2.html page in the clubshop root directory
###### last modified 12/19/01	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
$| = 1;

###### GLOBALS
my $MBR = '/cgi/members';	###### our member website script
my $COLS = 5;			###### the number of columns in our output table
my $db;
my $q = new CGI;

###### our redirect location
my $RED = 'http://www.clubshop.com/spam2.html';

###### our output file
my $OUT = '/home/httpd/html/spam2.html';

###### this is our template
open (TMPL, '/home/httpd/cgi-templates/Html/spam2.tmpl.html') || die "Couldn't open the spam2 template.\n";

require '/home/httpd/cgi-lib/DB_Connect.pl';
unless ($db = &DB_Connect::DB_Connect('spam2.cgi')){exit}
$db->{RaiseError} = 1;

my $qry = qq+SELECT alias, c.status FROM members m LEFT JOIN cancellations c ON c.id = m.id
WHERE m.status = 0 AND m.membertype ~ 'p|v' AND
(c.status != 'tw' OR c.status ISNULL) AND m.id != 1 ORDER BY m.id+;

my ($alias, $status, $data) = ();
my $cnt = 0;
my $sth = $db->prepare($qry); $sth->execute;

while (($alias, $status)= $sth->fetchrow_array){
	$cnt = 0 if $cnt == 5;
	$data .= '<tr>' if $cnt == 0;
	$data .= $q->td( $q->a({-href=>"$MBR/$alias"}, $alias));
	$cnt++; $data .= "</tr>\n" if $cnt == $COLS;
}
$sth->finish;
$db->disconnect;

###### since we will probably run out of data before the table row is finished
###### we need to fill in the blanks, so we'll test where we are at and do so
while($cnt != $COLS){
	$data .= $q->td('&nbsp;');
	$cnt++; $data .= "</tr>\n" if $cnt == $COLS;
}

###### this is our output page
open (PAGE, ">$OUT") || die "Couldn't open $OUT\n";
foreach (<TMPL>){
	$_ =~ s/%%data%%/$data/;
	$_ =~ s/%%stamp%%/scalar localtime()/e;
	print PAGE;
}

close PAGE; close TMPL;

print $q->header(-Refresh=>"2; URL=$RED"), $q->start_html();
print "The page, $RED, has been refreshed.<br>You should be redirected there shortly.<br>\n";
print "Click <a href=\"$RED\">here</a> if not.\n";
print $q->end_html();

exit;

