#!/usr/bin/perl -w
##
## MemberFollowUpAdmin.cgi	- an interface for viewing/deleting followup records
##
## Deployed Oct. 2012	Bill MacArthur
##
## last modified: 

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

my (@errors) = ();

my $q = new CGI;
my $action = $q->param('_action');
my $id = $q->param('id');
##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect('generic', $operator, $pwd);
$db->{'RaiseError'} = 1;
$db->{'AutoCommit'} = 0;
my $gs = G_S->new({'db'=>$db});

unless ($id)
{
	SearchForm();
}
elsif ($action eq 'report')
{
	Report();
}
elsif ($action eq 'delete')
{
	Delete();
	Report();
}
else
{
	die "Unhandled action";
}

Exit();

###### start of subs

sub Err
{
	print $q->header('text/plain'), $_[0];
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub Report
{
	unless ($id)
	{
		push @errors, 'Member ID not received';
		SearchForm();
		return;
	}
	elsif ($id =~ m/\D/)
	{
		my ($_id) = $db->fetchrow_array('SELECT id FROM members WHERE alias=?', undef, uc($id));
		unless ($_id)
		{
			push @errors, 'Member ID could not be found: $id';
			SearchForm();
			return;
		}
		$id = $_id;
	}
	
	my @rv = $gs->select_array_of_hashrefs("
		SELECT
			mft.pk,
			mft.upline_id,
			mft.entered::timestamp(0) AS entered,
			mft.val_text,
			COALESCE(at.label,'') AS activity_label
		FROM member_followup_text mft
		LEFT JOIN member_followup_key mfk ON mfk.mft_pk=mft.pk
		LEFT JOIN activity_rpt_data ard ON ard.pk = mfk.ard_pk
		LEFT JOIN activity_types at ON at.activity_type=ard.activity_type
		WHERE mft.id= $id
		ORDER BY entered DESC");
	
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>"Member Followup Report On: $id",
			'-encoding'=>'utf-8',
			'-style'=>{'code'=>'
				th,td {text-align:left; vertical-align:top;}
				tr.a {background-color:#eee;}
				tr.b {background-color:#ffe;}
				td {border: 1px solid #666; border-right: 1px solid #999; border-left: 1px solid #999;}
				'}
			),
		$q->h4("Member Followup Report On: $id");
	
	unless (@rv)
	{
		print
			$q->p("No records to report on"),
			$q->end_html;
		return;
	}
	
	print
		$q->start_form,
		'<table id="mainTbl" cellpadding="3" cellspacing="0"><tr>',
		$q->th('Entered'),
		$q->th('Upline ID'),
		$q->th('Related Activity'),
		$q->th('Followup Text'),
		$q->th(''),
		'</tr>';
		
	my $rowclass = 'a';
	foreach my $row (@rv)
	{
		print
			qq#<tr class="$rowclass">#,
			$q->td($row->{'entered'}),
			$q->td($row->{'upline_id'}),
			$q->td($row->{'activity_label'}),
			$q->td($row->{'val_text'}),
			$q->td( $q->checkbox('-name'=>'pk', '-value'=>$row->{'pk'}, '-label'=>'') ),
			'</tr>';
		$rowclass = $rowclass eq 'a' ? 'b':'a';
	}
	
	print
		$q->Tr( $q->td({'-colspan'=>5, '-style'=>'text-align:right; border: 0;'}, $q->br . $q->submit('-value'=>'Delete Selected Items') ) ),
		'</table>',
		$q->end_form,
		$q->end_html;
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html('-title'=>'Search Member Followup Records');
	
	print
		$q->h4('Search Member Followup Records');

	print $q->div({'-class'=>'errors'}, @errors) if @errors;
	
	print
		$q->start_form,
		$q->hidden('-name'=>'_action', '-value'=>'report'),
		'Member ID: ',
		$q->textfield('-name'=>'id'),
		$q->submit('-value'=>'Search', '-style'=>'margin-left:3em;'),
		$q->end_form.
		$q->end_html;
}