#!/usr/bin/perl -w
###### mallcat-admin.cgi
###### the script for administrating the mall categories
###### created: October '07	 Bill MacArthur
###### last modified: 10/19/07	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use ADMIN_DB;
#no warnings 'once';

my $q = new CGI;
my (@list, $err_msg) = ();

###### we should be in SSL mode
die "This resource should only be invoked in SSL mode\n" unless $q->https;

###### we should have our operator and password cookies
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged in to use this resource\n" unless $operator && $pwd;

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
$db->{RaiseError} = 1;

###### we're only expecting these params
# catid - category ID
# _action - what we are going to do (default is show a table of categories under a certain parent)
# catname - a category name
my %param = map {$_, G_S::PreProcess_Input(defined $q->param($_) ? $q->param($_) : '')} ('catid', '_action', 'catname');

###### we need these values for these params
#foreach('rc','lookup_val'){ die "Failed to receive a required parameter: $_\n" unless $param{$_}; }
my $catQry = "
	SELECT mc.catid, mc.catname, COALESCE(tmp.children, 0) AS children
	FROM mallcats mc LEFT JOIN
	(SELECT parent AS id, COUNT(*) AS children FROM mallcats GROUP BY parent) AS tmp ON tmp.id=mc.catid
	WHERE mc.catid > 0 AND mc.parent = ? ORDER BY mc.catname";

unless ($param{'_action'}){
	ReturnCategoryTable();
}
elsif ($param{'_action'} eq 'newcatname'){
	CreateNewCat();
}
elsif ($param{'_action'} eq 'textsearch'){
	TextSearch();
}
else { die "Undefined _action\n"; }

END:
$db->disconnect;
exit;

sub CreateCategoryRow
{
	my ($cat, $rowclass) = @_;
	$cat->{catname} = $q->escapeHTML($cat->{catname});
	return qq|
<tr id="tr_$cat->{catid}" class="$rowclass">
	<td class="catname"><a href="#" title="Cat ID: $cat->{catid}">$cat->{catname}</a></td>
	<td class="ctl"><p class="expandcat" id="p_$cat->{catid}">$cat->{children}</p></td>
	<td id="td_$cat->{catid}" class="container"></td>
</tr>
|;
}

sub CreateNewCat
{
	###### enter a new category into the DB and return a standard category row for insertion into the interface
	die "Failed to receive the catname parameter\n" unless $param{catname};
	die "Failed to receive a numeric catid parameter\n" unless
		defined $param{catid} && $param{catid} =~ /^\d+$/;
	my ($newcatid) = $db->selectrow_array("SELECT NEXTVAL('mallcats_catid_seq')");
	my $rv = $db->do("
		INSERT INTO mallcats (catid, catname, parent)
		VALUES ($newcatid, ?, ?)", undef, $param{catname}, $param{catid});
	die "Category creation failure\n" unless $rv eq '1';
	print $q->header('text/xml'), CreateCategoryRow( GetCategoryRow($newcatid), 'newcategory' );
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub GetCategoryRow
{
	###### pull one row from the DB
	my $catid = shift;
	return $db->selectrow_hashref('
		SELECT mc.catid, mc.catname, 0 AS children
		FROM mallcats mc
		WHERE mc.catid > 0 AND mc.catid = ? ORDER BY mc.catname', undef, $catid);
}

sub ReturnCategoryTable
{
	my $catid = $param{'catid'} || 0;
	my $rv = qq|<table id="table_$catid"><thead>|;
	$rv .= $q->Tr({-class=>'newcatname'}, $q->td({-colspan=>3, -class=>'newcatname'},
		$q->start_form(-method=>'get', -onsubmit=>'submitNewCatName(this); return false;') .
		$q->textfield(-name=>'catname', -class=>'newcatname') .
		$q->submit(-class=>'submitnew', -value=>'Add') . $q->hidden(-name=>'_action', -value=>'newcatname') .
		$q->hidden(-name=>'catid', -value=>$catid) . $q->end_form));
	$rv .= '</thead><tbody>';
	my $sth = $db->prepare($catQry);
	$sth->execute($catid);
	my $rowclass = 'a';
	while (my $cat = $sth->fetchrow_hashref){
		$rv .= CreateCategoryRow($cat, $rowclass);
		$rowclass = $rowclass eq 'a' ? 'b':'a';
	}
	$rv .= '</tbody></table>';
	print $q->header('text/xml'), $rv;
}

sub TextSearch
{
	my $msg = '';
	$param{'text_search_phrase'} = $q->param('text_search_phrase') || '';
	$param{'text_search_phrase'} =~ s/^\s*|\s$//g;	###### remove leading and trailing whitespace
	unless ($param{'text_search_phrase'}){
#		$msg = 'No search phrase received';
		goto 'FINISH';
	}
	my $rv = '';
	my @list = ();
	###### first we'll search on the complete phrase
	my $sth = $db->prepare(
		"SELECT vendor_categories_text_list(catid) AS item FROM mallcats WHERE catname ~* ? ORDER BY item");
	$sth->execute($param{'text_search_phrase'});
	while (my ($tmp) = $sth->fetchrow_array){
		###### highlight our search phrase
		$tmp =~ s#($param{'text_search_phrase'})#<span class="hilite">$1</span>#ig;
		push @list, $tmp;
	}
	unless (@list){
		###### since we apparently didn't find anything on the complete phrase, we'll do an -OR- on the words
		my @words = split /\s/, $param{'text_search_phrase'};
		my $OR = '';
		for (my $x=0; $x < scalar @words; $x++){
			$words[$x] =~ s/^\s*|\s$//g;
			$OR .= "$words[$x]|";
		}
		chop $OR;
		$sth->execute($OR);
		while (my ($tmp) = $sth->fetchrow_array){
			$tmp =~ s#($OR)#<span class="hilite">$1</span>#ig;
			push @list, $tmp;
		}
	}
	unless (@list){
		$msg = "No matches for: $param{'text_search_phrase'}";
	}
FINISH:
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding=>'utf-8',
			-title=>"Category Results for: $param{'text_search_phrase'}",
			-style=>{-src=>'/css/admin/mallcat-admin.css'},
			-onload=>"document.forms[0].text_search_phrase.focus()"
		), $q->p('<b>Category Text Search</b>');

	print $q->start_form(-method=>'get', -style=>'display:block'), $q->hidden(-name=>'_action', -value=>'textsearch'),
		$q->textfield(-name=>'text_search_phrase', -onclick=>'this.select()', -class=>'newcatname'),
		$q->submit(-value=>'Search', -class=>'submitnew'), $q->end_form;
	
	if (@list){
		print '<div id="SEARCH-RESULTS">';
		my $rowclass = 'a';
		foreach (@list){
			s/&/&amp;/g;
			print $q->div({-class=>$rowclass}, $_);
			$rowclass = $rowclass eq 'a' ? 'b':'a';
		}
		print '</div>';
	}
	elsif ($msg){ print $q->p($msg); }
	print $q->end_html;
}

###### 10/12/07 revised the catid parameter check in CreateNewCat
###### 10/19/07 added TextSearch

