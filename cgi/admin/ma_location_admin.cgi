#!/usr/bin/perl -w
###### ma_location_admin.cgi
######
###### admin interface for managing Merchant Affiliate locations
######
###### last modified: 09/18/14	Bill MacArthur

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw( $COACH_LEVEL $SALT %URLS );
use ADMIN_DB;

###### GLOBALS
# Countries that require zip codes.
our @zip_countries = qw/US/;

my %TBL = (
	'locations' => 'merchant_affiliate_locations',
	'locations_arc'	=> 'merchant_affiliate_locations_arc',
	'vendors' => 'vendors',
	'discounts' => 'merchant_affiliate_discounts');
my $q = new CGI;
my $ME = $q->url;

# make sure we are running a secure session
unless ($q->https)
{
	$ME =~ s/^http/https/;
	print $q->redirect($ME);
	exit;
}

my $COUPON_EDIT_SCRIPT = 'https://www.clubshop.com/cgi/admin/ma_coupons_edit.cgi';
my %TMPL = (
	MAIN		=> 10176,
	search 	=> 10175,
	arc_list	=> 10202,
	arc_display	=> 10203,
	discount_row	=> 10234,
	discount_rpt	=> 10235);
		
our ($db, $StateList, $StateHash, @locations, $search_field, $search_value) = ();
our $future_discount = ();		
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

unless ($pwd && $operator)
{
	Err('You must be logged in to use this application');
	exit;
}

# Today's date in postgres date format (yyyy-mm-dd).
my ($day, $mon, $year) = (localtime)[3,4,5];
$mon++; $year += 1900;
if ( length($day) == 1 ) { $day = '0' . $day }	# Pad a single digit day.
if ( length($mon) == 1 ) { $mon = '0' . $mon }	# Pad a single digit month.
our $today = "$year-$mon-$day";

# our default prompt
my $default_prompt= qq||;
my $prompts = '';

###### %field has the form of 'label','field name', 'value', 'html class type'
###### 'required flag (set to 1 if required)'.
###### class would be used for the label.
###### keys should match DB column names.
our %field = (
	id			=> {value=>'', class=>'n', req=>0, nodb=>1},
	merchant_id		=> {value=>'', class=>'n', req=>0, nodb=>1},
	vendor_id		=> {value=>'', class=>'n', req=>0, nodb=>1},
	location_name		=> {value=>'', class=>'n', req=>1},
	address1		=> {value=>'', class=>'n', req=>1},
	address2		=> {value=>'', class=>'n', req=>0},
	address3		=> {value=>'', class=>'n', req=>0},
	city			=> {value=>'', class=>'n', req=>1},
	state			=> {value=>'', class=>'n', req=>0},
	state_code		=> {value=>'', class=>'n', req=>0, nodb=>1},
	country		=> {value=>'', class=>'n', req=>1},
	postalcode		=> {value=>'', class=>'n', req=>0},
	phone			=> {value=>'', class=>'n', req=>1},
	username		=> {value=>'', class=>'n', req=>1},
	password		=> {value=>'', class=>'n', req=>1},
	notes			=> {value=>'', class=>'n', req=>0},
	stamp			=> {value=>'', class=>'n', req=>0, nodb=>1},
	operator		=> {value=>'', class=>'n', req=>0, nodb=>1},
	'vendor-status'	=> {value=>'', class=>'n', req=>1, numeric=>1},
	'vendor-url'		=> {value=>'', class=>'n', req=>0},
	'vendor-coupon'	=> {value=>'', class=>'n', req=>0},
	'vendor-notes'	=> {value=>'', class=>'n', req=>0},
	'vendor-cv_mult'	=> {value=>'', class=>'n', req=>0, numeric=>1, nodb=>1},
	'vendor-reb_mult'	=> {value=>'', class=>'n', req=>0, numeric=>1, nodb=>1},
	'vendor-comm_mult'	=> {value=>'', class=>'n', req=>0, numeric=>1, nodb=>1},
	'vendor-blurb'	=> {value=>'', class=>'n', req=>0},
	'vendor-stamp'	=> {value=>'', class=>'n', req=>0, nodb=>1},
	'vendor-operator'	=> {value=>'', class=>'n', req=>0, nodb=>1},
	'vendor-group'	=> {value=>'', class=>'n', req=>0, nodb=>1},
	'discount-discount_type'	=> {value=>'', class=>'n', req=>1},
	'discount-discount'	=> {value=>'', class=>'n', req=>1, numeric=>1},
	'discount-rebate'	=> {value=>'', class=>'n', req=>1, numeric=>1},
	'discount-reb_com'	=> {value=>'', class=>'n', req=>1, numeric=>1},
	'discount-pp'		=> {value=>'', class=>'n', req=>1, numeric=>1},
	'discount-cap'	=> {value=>'', class=>'n', req=>0, numeric=>1},
	'discount-start_date'=> {value=>'', class=>'n', req=>1},
	'discount-end_date'	=> {value=>'', class=>'n', req=>0, nodb=>1},
	'discount-stamp'	=> {value=>'', class=>'n', req=>0, nodb=>1},
	'discount-operator'	=> {value=>'', class=>'n', req=>0, nodb=>1}
);

###### the following two are the fields that we'll allow searches by
my @char_fields = qw/location_name
			address1
			address2
			address3
			city
			state
			country
			postalcode
			phone
			username
			password
			notes/;
my @int_fields = qw/	id
			oid
			merchant_id
			vendor_id/;

###### these are flags for presenting or hiding blocks of the template that we want
###### to handle conditionally :)
our %conditionals = (1=>0, 2=>1, 3=>0);

our $location_id = '';
our $data = '';
our $last_change = '';

##########################################
###### MAIN program begins here.
##########################################

unless ($db = ADMIN_DB::DB_Connect( 'ma_location_admin.cgi', $operator, $pwd )){exit}
###### we are not giving everyone in the organization access to this function
###### only those in the ? group -- leaving this in in case we do want to restrict access later
#	if (ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1){
#		Err($q->h3('You must be a member of the accounting group to use this form'));
#		goto 'END';
#	}

$db->{RaiseError} = 1;

our $action = $q->param('action');

###### we enter the application for the first time
###### we should have a merchant ID to work with at this point
unless ($action)
{
	Search_Form();
}
elsif ($action eq 'search')
{
	$search_field = $q->param('search_field');
	$search_value = $q->param('search_value');
	unless ( $search_field && $search_value )
	{
		Err('Both a search field and a search value are required');
		goto 'END';
	}

	Run_Query('locations');
	if (scalar @locations == 0)
	{
		Search_Form( $q->span({-class=>'r'}, 'No Match Found'));
	}
	elsif (scalar @locations == 1)
	{
		###### put our data set into our hash
		Load_Data();

		if ($locations[0]->{'vendor-group'} eq '6')
		{
			###### 'MarketPlace' locations do not require all the fields that a 'bricks & mortar' store does
			foreach ('address1','city','state','country','phone')
			{
				$field{$_}{req} = 0;
			}

			###### and we want to explicitly indicate that they are 'MarketPlace' locations
			$conditionals{3} = 1;
		}

		# Check to see if there is already a future discount record on file.
		$future_discount = Future_Discount();

		###### by doing this we'll flag any missing fields and populate our state dropdown
		Check_Required();

		###### but we don't want any error prompts
		$prompts = '';

		Do_Page();		
	}
	else
	{
		Search_Form('', Create_List() );
	}
}
elsif ($action eq 'submit')
{
	my $successes = '';
	Load_Params();

	my @list = ();
	foreach (keys %field)
	{
		push (@list, $_) unless /^vendor-/;
	}

	if ($field{'vendor-group'}{value} eq '6')
	{
		###### 'MarketPlace' locations do not require all the fields that a 'bricks & mortar' store does
		foreach ('address1','city','state','country','phone')
		{
			$field{$_}{req} = 0;
		}

		###### and we want to explicitly indicate that they are 'MarketPlace' locations
		$conditionals{3} = 1;
	}

	###### If we return true, then we'll redo the page with errors flagged				
	if ( Check_Required() )
	{
		Do_Page();
	}
	else
	{
		my $rv = Update_Location();
		unless (! $rv || $rv ne '1' )
		{
			$successes .= "<span class=\"b\">The Location details were updated.</span><br />";
		}
		else
		{	# Really an error message, but we want them to know if the others succeeded.
			$successes.= "<span class=\"r\">The Location details were NOT updated.</span><br />";
		}
		$rv = Update_Vendor();
		unless (! $rv || $rv ne '1' )
		{
			$successes .= "<span class=\"b\">The Vendor details were updated.</span><br />";
		}
		else
		{	# Really an error message, but we want them to know if the others succeeded.
			$successes.= "<span class=\"r\">The Vendor details were NOT updated.</span><br />";
		}
		if ( $field{'discount-start_date'}{value} gt $today )
		{		
			$rv = Update_Discount();
			unless (! $rv || $rv ne '1' )
			{
				$successes .= "<span class=\"b\">The Discount details were updated.</span><br />";
			}
		}
		else
		{	# Really an error message, but we want them to know if the others succeeded.
			$successes.= "<span class=\"r\">The Discount details were NOT updated.</span><br />";
		}
		$search_field = 'id';
		$search_value = $field{id}{value};

		# Unless there are error messages.
		unless ( $prompts )
		{
			###### rather than redisplaying the page with submitted data, we'll actually load the record
			###### that way we can be sure that what we see is what we have
			Run_Query('locations');
			
			###### put our data set into our hash
			Load_Data();

			###### by doing this we'll flag any missing fields and populate our state dropdown
			Check_Required();

			###### we want a success message if they are no errors.
			if ( $successes )
			{			
				$prompts .= $successes;
			}
			else
			{
				$prompts .= "<span class=\"r\">No changes were recorded.</span><br>";
			}
		}
		# Check to see if there is already a future discount record on file.
		$future_discount = Future_Discount();

		Do_Page();
	}
}
elsif ($action eq 'arclist')
{
	if ( ArcList() == 0 )
	{
		Err("No matching records for Merchant ID: $location_id");
	}
}
elsif ($action eq 'all_discounts')
{
	if ( All_Discounts() == 0 )
	{
		Err("No matching Discount records for Merchant ID: $location_id");
	}
}
elsif ($action eq 'arc')
{
	$search_field = 'oid';
	$search_value = $q->param('id');

	Run_Query('locations_arc');

	###### at this point we should have one row of data
	unless ( @locations )
	{
		Err("No matching records for Merchant ID: $location_id");
	}
	else
	{
		Load_Data();
		Create_controls();
		$prompts = 'Data cannot be changed.';
		$last_change = $field{'stamp'}{value};
##### Until we begin archiving the vendor info, we will only use the location stamp above.
#		# Find the most recent change date between the vendors and locations tables.
# 		$last_change = $field{'stamp'}{value} gt $field{'vendor-stamp'}{value}
#				? $field{'stamp'}{value}
#				: $field{'vendor-stamp'}{value};
# 		if ( $field{stamp}{value} gt $field{vendor-stamp}{value} )
#		{
#			$last_change = $field{stamp}{value};
#		}
#		else
#		{							
#			$last_change = $field{'vendor-stamp'}{value};
#		}							
		Do_Page('arc_display');
	}
}

###### our last catchall case		
else { Err('Undefined action') }

END:
$db->disconnect;
exit;

###### ###### ###### ###### ######
###### Subroutines start here.
##################################

###### Show all discount records for this location.
sub All_Discounts
{
	$location_id = $q->param('id');
	my $one_row = G_S::Get_Object($db, $TMPL{discount_row}) || die "Couldn't open $TMPL{discount_row}";
	my $qry = qq|	SELECT d.discount_type,				
				m.name,				
				(d.rebate * 100)::NUMERIC(5,2) AS rebate,				
				(d.reb_com * 100)::NUMERIC(5,2) AS reb_com,				
				(d.pp * 100)::NUMERIC(6,3) AS pp,				
				(d.discount * 100)::NUMERIC(5,2) AS discount,				
				d.cap,				
				d.start_date,				
				d.end_date,				
				d.stamp,
				d.operator
			FROM 	$TBL{discounts} d,
				merchant_discounts m
			WHERE 	location_id = ?
			AND 	d.discount_type = m.type
			ORDER BY start_date DESC|;

	my $bgcolor = '#ffffee';
	my $tblDATA = '';
	my $num_records = 0;
	my $sth = $db->prepare($qry);
	$sth->execute($location_id);

	while ( $data = $sth->fetchrow_hashref )
	{
		++$num_records;
		my $this_row = $one_row;
		
		# lets fill in the data
		foreach (keys %{$data})
		{
			$data->{$_} = '&nbsp;' unless ($data->{$_} && $data->{$_} =~ /\w/);
			$this_row =~ s/%%$_%%/$data->{$_}/;
		}
		$tblDATA .= $this_row;
	}
	$sth->finish;

	if ( $num_records )
	{	
		my $tmpl = G_S::Get_Object($db, $TMPL{discount_rpt}) || die "Couldn't open $TMPL{discount_rpt}";

		$tmpl =~ s/%%data%%/$tblDATA/;
		$tmpl =~ s/%%id%%/$location_id/g;         

		print $q->header(-expires=>'now'), $tmpl;
	}							
	return $num_records;
}

###### Show a list of archived records.
sub ArcList
{
	$location_id = $q->param('id');
	my $qry = "	SELECT id,
				location_name,
				address1,
				city,
				state,
				stamp,
				operator
			FROM 	$TBL{locations_arc}
			WHERE id = ?
			ORDER BY stamp desc";

	my $bgcolor = '#ffffee';
	my $tblDATA = '';
	my $num_records = 0;
	my $sth = $db->prepare($qry);
	$sth->execute($location_id);

	while ( $data = $sth->fetchrow_hashref )
	{
		++$num_records;
				
		###### lets fill in the blank table cells
		foreach (keys %{$data}){
			unless ( $data->{$_} =~ /\w/ ){$data->{$_} = '&nbsp;'}
		}
		$tblDATA .= $q->Tr({-bgcolor=>$bgcolor},
			$q->td({-valign=>'top'}, $q->a({-href=>$q->url()."?action=arc;id=$data->{id};stamp=$data->{stamp}", -target=>'_blank'}, '*') ) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{location_name}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{address1}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{city}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{state}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{stamp}) .
			$q->td({-valign=>'top', -class=>'data', -nowrap=>''}, $data->{operator})
		);
		$bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	}
	if ( $num_records )
	{	
		my $tmpl = G_S::Get_Object($db, $TMPL{arc_list}) || die "Couldn't open $TMPL{arc_list}";

		$tmpl =~ s/%%data%%/$tblDATA/;
		$tmpl =~ s/%%id%%/$location_id/g;         

		print $q->header(-expires=>'now'), $tmpl;
	}							
	return $num_records;
}

###### Creates a combo box from which a discount type may be selected.
sub cboDiscount
{
	my $name = shift;
	my (%hash) = ();
	my @list = '';		
	my $sth = $db->prepare("	SELECT type,
						name
					FROM merchant_discounts
					WHERE void = 'f'
					ORDER BY type;");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$hash{$tmp->{type}} = $tmp->{name};
		push (@list, $tmp->{type})
	}

	return $q->popup_menu(	-name=>$name,
					-default=>$field{$name}{value},
					-values=>\@list,
					-labels=>\%hash,
					-force=>'true')						
}

###### Creates a combo box from which a state may be selected.
sub cboState
{
	###### if our list is not populated, then we'll put something in
	if ( ! defined $StateList || scalar @{$StateList} < 1)
	{
		$StateList = [''];
		$StateHash = {'' => 'Enter value below'};
	}

	my $name = shift;
	return $q->popup_menu(	-name=>$name,
					-default=>'',
					-values=>$StateList,
					-labels=>$StateHash);
}

sub cboStatus
{
	my $name = shift;
	my (%hash) = ();
	my @list = '';		
	my $sth = $db->prepare("	SELECT status_type,
						status
					FROM vendor_status_types");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$hash{$tmp->{status_type}} = $tmp->{status};
		push (@list, $tmp->{status_type})
	}

	return $q->popup_menu(	-name=>$name,
					-default=>$field{$name}{value},
					-values=>\@list,
					-labels=>\%hash)						
}

###### Checks to see if all required fields have been filled out.
sub Check_Required
{
	my $list = shift || [keys %field];

	my $missing = 0;

	###### check our numeric only fields
	foreach (@$list)
	{
		if (	$field{$_}{numeric} &&
			defined $field{$_}{value} &&
			$field{$_}{value} gt '' &&
			$field{$_}{value} !~ /^-?\d*\.?\d+?$/
		   )				
		{
			$missing = 1;
			$field{$_}{class} = 'r';
		}
	}

	if ($missing == 1)
	{
		$prompts = qq|<div style="color: #ff0000">The information marked in Red must be numeric only.</div>|;
		return $prompts;
	}

	###### are we required			
	foreach (@$list)
	{
		#Err("current param= $_ : $field{$_}{value}");

		if ( ($_ =~ /postalcode/) )
		{
			$field{$_}{req} = Check_Zips();
		}						
		if ($field{$_}{req} == 1 && $field{$_}{value} eq '')
		{
		#Err("MISSING - current param= $_ : $field{$_}{value}");
			$missing = 1;
			$field{$_}{class} = 'r';
		}
	}

	if ($missing == 1)
	{
		$prompts = qq|<div style="color: #ff0000">Please enter the required information marked in Red.</div>|;
	}
	
	foreach (@$list)
	{
		if (/state/ && ! /code/)
		{
			###### check for a standardized state
			my ($success, $country_match) = ();

			(
			$field{$_}{value},
			$field{($_ . '_code')}{value},
			$success,
			$country_match,
			$StateList,
			$StateHash
			) = G_S::Standard_State(	$db,
							$field{$_}{value},
							$field{($_ . '_code')}{value},
							$field{country}{value}
							);
		}
	}
	return $prompts;
}

###### Check to see if the merchant record requires a zip code.
sub Check_Zips
{
	# If this is not an Online Merchant Affiliate.
	if ( $field{'vendor-group'}{value} ne '6' )
	{
		# Look at all countries in a list.		
		foreach ( @zip_countries )
		{
			# If an item in the list equals the country submitted, require zip codes.
			if ( $_ eq $field{country}{value} ) { return 1 }
		}
	}							
	# If we made it this far, this merchant doesn't require zip codes.
	return 0;	
}

###### Create special control menus for the page to be displayed.
sub Create_controls
{
	my @list = @_;

	foreach (@list){
	###### now we'll convert the 'value' to the needed form element with the value included
		if ( /country/ )
		{
			$field{$_}{control} = G_S::cbo_Country($q, $_, $field{$_}{value}, '');
		}
		elsif ( /state/ )
		{
			$field{$_}{control} = cboState($_);
		}
		elsif ( /status/ )
		{
			$field{$_}{control} = cboStatus($_);
		}
		elsif ( /discount_type/ )
		{
			$field{$_}{control} = cboDiscount($_);
		}

	###### this part used only for script generated forms
		else
		{
			$field{$_}{control} = $q->textfield(	-name=>$_,
									-size=>34,
									-maxlength=>50);
		}
	}
}

sub Create_List
{
	my $bgcolor = '#ffffee';
	my $tbl = $q->p({-class=>'b'}, "${\(scalar @locations)} records found.");
	$tbl .= $q->Tr({-bgcolor=>'#eeffee'},
				$q->th( 'Location ID' ) .
				$q->th( 'Merchant ID' ) .
				$q->th( 'Location Name' ) .
				$q->th( 'Address1' ) .
				$q->th( 'City' )
			);
	
	foreach (@locations)
	{
		$tbl .= $q->Tr({-bgcolor=>$bgcolor},
				$q->td( {-class=>'fp'},
					$q->a({-href=>"$ME?search_value=$_->{id}&search_field=id&action=search",
						-target=>'_blank'},
						$_->{id})) .
				$q->td( {-class=>'fp'}, ${\($_->{merchant_id} || '&nbsp;')}) .
				$q->td( {-class=>'fp'}, ${\($_->{location_name} || '&nbsp;')}) .
				$q->td( {-class=>'fp'}, ${\($_->{address1} || '&nbsp;')}) .
				$q->td( {-class=>'fp'}, ${\($_->{city} || '&nbsp;')})
			);
		$bgcolor = ($bgcolor eq '#ffffee') ? '#eeeeee' : '#ffffee';
	}

	$tbl = $q->table({-border=>1, -cellspacing=>0, -cellpadding=>4}, $tbl);
	return $tbl;
}

###### Display the page to the browser.
sub Do_Page
{
	my $tmpl = shift || 'MAIN';
	my $TMPL = G_S::Get_Object($db, $TMPL{$tmpl}) || die "Cannot open Template: $TMPL{$tmpl}";

	Create_controls('state_code', 'country', 'vendor-status', 'discount-discount_type');
	
	###### If our conditional flag is set we'll leave that block alone.
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$TMPL =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	###### Go through our list and parse out the placeholders.
	foreach (keys %field)
	{
		if ($field{$_}{control})
		{
			$TMPL =~ s/%%$_-control%%/$field{$_}{control}/;
		}
		else
		{
			$TMPL =~ s/%%$_%%/$field{$_}{value}/g;
		}

		$TMPL =~ s/%%$_-class%%/$field{$_}{class}/;
	}

	# If we found a future discount record we'll fill it out here.		
	if ( $future_discount )
	{
		foreach ( keys %{$future_discount} )
		{
			$future_discount->{$_} ||= '';
			$TMPL =~ s/%%$_%%/$future_discount->{$_}/g;
		}
	}
	# Otherwise we'll delete it.	
	else
	{
		$TMPL =~ s/conditional-future-open.+?conditional-future-close//sg;
	}
	
	###### since $ME will be empty after the app is accepted, we need to fill our hyperlink with a real value
	$TMPL =~ s/%%my_url%%/$q->url/e;

	$TMPL =~ s/%%action%%/$ME/g;
	$TMPL =~ s/%%prompts%%/$prompts/;
	$TMPL =~ s/%%last_change%%/$last_change/;

	$TMPL =~ s/%%archives%%/$q->a({	-href=>$ME
							. "?id=$field{id}{value}&action=arclist",
						-target=>'_blank',
						-class=>'q'},
						'Location Archives')/e;
	$TMPL =~ s/%%all_discounts%%/$q->a({	-href=>$ME
							. "?id=$field{id}{value}&action=all_discounts",
						-target=>'_blank',
						-class=>'q'},
						'Discount Archives')/ge;
	$TMPL =~ s/%%coupons%%/$q->a({	-href=>$COUPON_EDIT_SCRIPT
							. "?admin=1&merchant_id=$field{id}{value}&membertype=mal",
						-target=>'_blank',
						-class=>'q'},
						'Coupons')/e;
	print $q->header('-charset'=>'utf-8'), G_S::Prepare_UTF8($TMPL,'encode');
}

sub Err
{
###### if we want to use a 'pretty' page as a holder for our message we'll try to
###### pull up the template
	my $tmpl = '';
	my $error = shift;
			
#	if ($db){ G_S::Get_Object($db, 'TEMPLATE_NUMBER') }
	
	unless ($tmpl)
	{
		$tmpl = qq|
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
			<html><head>
			<title>Error</title>
			</head>
			<body bgcolor="#ffffff">
			<br><br>
			%%message%%
			<p>Current server time: <tt>%%timestamp%% PST</tt>
			</body></html>|;
	}

	$tmpl =~ s/%%message%%/$error/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header, $tmpl;
}


##### Looks to see if we have a future discount record in the table.
sub Future_Discount
{
	my ($data) = ();
	my $qry = qq|	SELECT d.discount_type AS "future-discount_type",				
				m.name AS "future-discount_name",				
				(d.rebate * 100)::NUMERIC(5,2) AS "future-rebate",				
				(d.reb_com * 100)::NUMERIC(5,2) AS "future-reb_com",				
				(d.pp * 100)::NUMERIC(6,3) AS "future-pp",				
				(d.discount * 100)::NUMERIC(5,2) AS "future-discount",				
				d.cap AS "future-cap",				
				d.start_date AS "future-start_date",				
				d.stamp AS "future-stamp",
				d.operator AS "future-operator"
			FROM 	$TBL{locations} l,
				$TBL{discounts} d,
				merchant_discounts m
			WHERE 	l.id = d.location_id
			AND 	d.discount_type = m.type
			AND 	d.start_date > NOW()::date
			AND 	d.end_date IS NULL
			AND	l.|;

	if (grep { /$search_field/ } @char_fields)
	{ 
		$qry .= "$search_field ~* ?\n";
	}
	else
	{
		$qry .= "$search_field = ?\n";
	}
	my $sth = $db->prepare("$qry");
	$sth->execute( $search_value );
	
	my $future_discount = $sth->fetchrow_hashref;
#foreach ( keys %{$future_discount} ) { Err("$_= $future_discount->{$_}") };
	$sth->finish();
	return $future_discount;
}

sub Load_Data
{
	foreach (keys %field)
	{
		###### because a zero in the data will fall through if we do || ''
		$field{$_}{value} = $locations[0]->{$_};
		$field{$_}{value} = '' unless defined $field{$_}{value};
	}
}
				
###### Load the data from the form into the script variables.
sub Load_Params
{
	###### Populating our field array hash values.
	###### This loop depends on the %field hash keys being identical to the passed parameter names.
	foreach (keys %field){

		###### since our usual param || '' will lose a value equal to zero
		###### we are going to this two part version
		$field{$_}{value} = $q->param($_);
		$field{$_}{value} = '' unless defined $field{$_}{value};

		next unless $field{$_}{value};

		###### Filter out leading & trailing whitespace.
		$field{$_}{value} =~ s/^\s*//;
		$field{$_}{value} =~ s/\s*$//;

		###### we won't do any filtering on our 'notes'
		next if /notes$|blurb$/;

		###### this is our default filtering										
		$field{$_}{value} =~ s/('?)( ?)(,?)(\.?)(-)(\W*)(_*)/$1$2$3$4$5/g;
			
		if ( /city|state/ )
		{
			###### we will upper case all the first letters of the name fields, city, state
			$field{$_}{value} = ucfirst($field{$_}{value});
		}
		elsif ( /submitted_id/ )
		{
			$field{$_}{value} = uc $field{$_}{value};
		}
	}
}

##### Runs the specified query, pushing the results onto a list (array).
sub Run_Query
{
	my $table_name = shift;
	my ($data) = ();
	my $qry = qq|	SELECT l.*,
				v.status AS "vendor-status",
				COALESCE(v.url, '') AS "vendor-url",
				COALESCE(v.coupon, '') AS "vendor-coupon",
				COALESCE(v.notes, '') AS "vendor-notes",
				v.cv_mult * 100 AS "vendor-cv_mult",
				v.reb_mult * 100 AS "vendor-reb_mult",
				v.comm_mult * 100 AS "vendor-comm_mult",
				v.blurb AS "vendor-blurb",
				v.stamp AS "vendor-stamp",
				v.operator AS "vendor-operator",
				v.vendor_group AS "vendor-group",
				d.discount_type AS "discount-discount_type",				
				(d.rebate * 100)::NUMERIC(5,2) AS "discount-rebate",				
				(d.reb_com * 100)::NUMERIC(5,2) AS "discount-reb_com",				
				(d.pp * 100)::NUMERIC(6,3) AS "discount-pp",				
				(d.discount * 100)::NUMERIC(5,2) AS "discount-discount",				
				d.cap AS "discount-cap",				
				d.start_date AS "discount-start_date",				
				d.end_date AS "discount-end_date",				
				d.stamp AS "discount-stamp",
				d.operator AS "discount-operator"
			FROM 	$TBL{$table_name} l,
				$TBL{discounts} d,
				$TBL{vendors} v
			WHERE 	l.vendor_id = v.vendor_id\n|;
	if ( $table_name eq 'locations' )
	{
		$qry .= "	AND 	l.id = d.location_id
				AND 	d.start_date <= NOW()::date
				AND 	(d.end_date >= NOW()::date OR d.end_date IS NULL)\n";
	}
	else	# This is the archive table.			
	{
		$qry .= "	AND 	l.id = d.location_id
				AND 	d.start_date <= l.stamp::date
				AND 	(d.end_date >= l.stamp::date OR d.end_date IS NULL)\n";
	}
	$qry .= "	AND	l.";

	if (grep { /$search_field/ } @char_fields)
	{ 
		$qry .= "$search_field ~* ?\n";
	}
	else
	{
		$qry .= "$search_field = ?\n";
	}

	my $sth = $db->prepare("$qry ORDER BY l.id");
#Err($qry); Err($search_value) ; return;
	$sth->execute( $search_value );
	
	while ( $data = $sth->fetchrow_hashref)
	{
		push (@locations, $data);
	}		
	$sth->finish();
}

###### print our main search form with any pertinent messages
sub Search_Form
{
	my $msg = shift || '';
	my $results = shift || '';
	
	# Open the search template.
	my $tmpl = G_S::Get_Object($db, $TMPL{'search'}) || die "Failed to load template: $TMPL{'search'}";
	
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$msg/;
	$tmpl =~ s/%%results%%/$results/;
	print $q->header(), $tmpl;	
}

###### Update the discount record if changes were made.
sub Update_Discount
{
	my $qry = '';
	my $sth = '';
	my $rv = '';	
	my $qry_args = '';
	my (@list, $tmp, $fld) = ();
	my $first_one = 1;
	my $how_many = 0;
	my $data = '';	

	# Let's see how many current/future records we have.
	$qry .= "	SELECT count(*)
			FROM 	$TBL{discounts}
			WHERE 	location_id = $field{id}{value}
			AND 	(end_date >= NOW()::date
				OR end_date IS NULL)";		
	$sth = $db->prepare($qry);
	$rv = $sth->execute(@list);
	$how_many = $sth->fetchrow_array();
	$sth->finish;

	@list = ();

	# If there is only a current record, we'll insert a future discount record.
	if ( $how_many == 1 )
	{
		# First update the end date on the current discount record.
		$qry = "	UPDATE $TBL{discounts}
				SET 	end_date = ?::timestamp - INTERVAL '1 day'
				WHERE 	location_id = $field{id}{value}
				AND	end_date IS NULL;";
		$sth = $db->prepare($qry);
		$rv = $sth->execute($field{'discount-start_date'}{value});
		$sth->finish;

		# Let's create and run the insert query for the new discount record.
		$qry = "	INSERT INTO $TBL{discounts} ( ";
		foreach $fld (keys %field)
		{
			###### if this value is flagged as nodb it doesn't belong in the DB
			next if $field{$fld}{nodb};

			###### skip over non discount fields
			next unless $fld =~ /^discount/;
					
			###### chop the discount tag off the field
			($tmp = $fld) =~ s/(^discount-)//;

			$qry .= " $tmp, ";

			###### we'll insert numeric values directly so that we can convert to NULL as necessary
			if ($field{$fld}{numeric})
			{
				if ( $field{$fld}{value} eq '' )
				{
					$qry_args .= " NULL,";
				}
				# we need to convert our percentages into decimal equivalents
				elsif ( $fld =~ /(discount-)discount|rebate|reb_com|pp/ )
				{
					$qry_args .= " $field{$fld}{value}::numeric/100,";
				}
				else
				{			
					$qry_args .= " $field{$fld}{value},";
				}
			}
			else
			{
				$qry_args .= " ?,";
				push (@list, $field{$fld}{value});
			}
		}
		$qry .= "	location_id,
				operator,
				stamp";
		$qry_args .= " ?, ?, ?)";
		push (@list, $field{id}{value}, $operator, 'NOW()');
		$qry .= " ) VALUES (";
		$qry .= $qry_args;
		$sth = $db->prepare($qry);

		$rv = $sth->execute(@list);
		$sth->finish;
		return $rv;
	}
	# If there has been a change and we have a future discount record, we'll update it.
	elsif ( $how_many == 2 )
	{
		# Set the end date on the current discount record.
		$qry = "	UPDATE $TBL{discounts}
				SET 	end_date = ?::timestamp - INTERVAL '1 day'
				WHERE 	location_id = $field{id}{value}
				AND	end_date >= NOW()::date;";
		$sth = $db->prepare($qry);
		$rv = $sth->execute($field{'discount-start_date'}{value});
		$sth->finish;
		unless ( $rv ) { return 0 } 

		$qry = "	UPDATE $TBL{discounts} SET";
		foreach $fld (keys %field)
		{
			###### if this value is flagged as nodb it doesn't belong in the DB
			next if $field{$fld}{nodb};

			###### skip over non discount fields
			next unless $fld =~ /^discount/;
						
			###### chop the discount tag off the field
			($tmp = $fld) =~ s/(^discount-)//;

			###### we'll insert numeric values directly so that we can convert to NULL as necessary
			if ($field{$fld}{numeric})
			{
				if ( $field{$fld}{value} eq '' )
				{
					$field{$fld}{value} = 'NULL';
				}
				# we need to convert our percentages into decimal equivalents
				elsif ( $fld =~ /(discount-)discount|rebate|reb_com|pp/ )
				{
					$field{$fld}{value} = $field{$fld}{value}/100;
				}
				$qry .= " $tmp= $field{$fld}{value},";
			}
			else
			{
				$qry .= " $tmp= ?,";
				push (@list, $field{$fld}{value});
			}
		}
		$qry .= "	operator= ?,
				stamp= NOW()
				WHERE 	location_id = $field{id}{value}
				AND	end_date IS NULL";

		push (@list, $operator);
		$sth = $db->prepare($qry);
		$rv = $sth->execute(@list);
		$sth->finish;
		return $rv;
	}
	else
	{
		Err("More than two current/future discount records found for this merchant. Call IT.");
	}			
}

###### Create the Location record and insert it into the database.
sub Update_Location
{
	# The notes for the location registration.
	my @list = ();
	my $fld = '';
	my $first_one = 1;	

	my $qry = "UPDATE $TBL{locations} SET";

	###### now we'll build out
	foreach $fld (keys %field)
	{
		###### skip over vendor  fields
		next if $fld =~ /^vendor-/;
		###### skip over discount fields
		next if $fld =~ /^discount-/;

		###### if this value is flagged as nodb it doesn't belong in the DB
		next if $field{$fld}{nodb};

		# If this is a blank field.
		if ( $field{$fld}{value} eq '' )
		{
			# We need this to deal with potential referential integrity rules.		
			$qry .= " $fld= NULL,";
		}
		else
		{
			$qry .= " $fld= ?,";
			push (@list, $field{$fld}{value});
		}
	}

	$qry .= "	operator= ?,
			stamp= NOW()
			WHERE id= $field{id}{value}
			AND merchant_id= $field{merchant_id}{value}
			AND\n";
	push (@list, $operator);

	# Now add the check to see if anything has changed.
	foreach $fld (keys %field)
	{

		###### if this value is flagged as nodb it doesn't belong in the DB
		next if $field{$fld}{nodb};

		###### skip over vendor  fields
		next if $fld =~ /^vendor-/;
		###### skip over discount fields
		next if $fld =~ /^discount-/;

		if ( $first_one ) { $first_one = 0; $qry .= " ( " }
		else { $qry .= " OR " };

		###### we'll insert numeric values directly so that we can convert to NULL as necessary
		if ($field{$fld}{numeric})
		{
			if ( $field{$fld}{value} eq '' )
			{
				$qry .= " $fld IS NOT NULL ";
			}
			else
			{
				$qry .= " $fld != $field{$fld}{value} ";
			}
		}
		else
		{
			if ( $field{$fld}{value} eq '' )
			{
				$qry .= " $fld IS NOT NULL ";
			}
			else
			{
				$qry .= " ($fld != ? OR $fld IS NULL)";
				push (@list, $field{$fld}{value});
			}
		}
	}

	$qry .= " )";
			
#Err($qry); #foreach ( @list ) { Err($_) };
			
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@list);
	$sth->finish;
	return $rv;
}

sub Update_Vendor
{
	my $qry = "	UPDATE $TBL{vendors} SET";

	my (@list, $tmp, $fld) = ();
	my $first_one = 1;

	foreach $fld (keys %field)
	{

		###### if this value is flagged as nodb it doesn't belong in the DB
		next if $field{$fld}{nodb};

		###### skip over non vendor fields
		next unless $fld =~ /^vendor/;
						
		###### chop the vendor tag off the field
		($tmp = $fld) =~ s/(^vendor-)//;

		###### we'll insert numeric values directly so that we can convert to NULL as necessary
		if ($field{$fld}{numeric})
		{
			if ( $field{$fld}{value} eq '' )
			{
				$field{$fld}{value} = 'NULL';
			}
			$qry .= " $tmp= $field{$fld}{value},";
		}
		else
		{
			if ( $field{$fld}{value} eq '' )
			{
				$qry .= " $tmp= NULL,";
			}
			else
			{			
				$qry .= " $tmp= ?,";
				push (@list, $field{$fld}{value});
			}								
		}
	}

	$qry .= "	operator= ?,
			stamp= NOW()
			WHERE 	vendor_id = $field{vendor_id}{value}
			AND\n";

	push (@list, $operator);

	# Now add the check to see if anything has changed.
	foreach $fld (keys %field)
	{
		###### if this value is flagged as nodb it doesn't belong in the DB
		next if $field{$fld}{nodb};

		###### skip over non vendor fields
		next unless $fld =~ /^vendor/;
						
		###### chop the vendor tag off the field
		($tmp = $fld) =~ s/(^vendor-)//;

		if ( $first_one ) { $first_one = 0; $qry .= " ( " }
		else { $qry .= " OR " };

		if ( $field{$fld}{value} eq '' )
		{
			$qry .= " $tmp IS NOT NULL ";
		}
		else
		{
			$qry .= " ($tmp != ? OR $tmp IS NULL)";
			push (@list, $field{$fld}{value});
		}
	}

	$qry .= " )";

#Err($qry); foreach ( @list ) { Err($_) }; #return;
			
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@list);
	$sth->finish;
	return $rv;
}

###### 08/20/03 added a sort order to the list generation
###### and handling for NULL fields since we removed some NULL constraints in the DB
###### 09/17/03 Added handling for the postalcode field.
###### 09/24/03 Added handling for the Special Exception Verbiage.
###### 10/08/03 revised the format of the vendor query to accept the new 3\8% pp value
###### 10/21/03 Added handling for the separate 'discount structure' record.
######			Modified all update queries to update only if data changed.
###### 11/11/03 Added 'OR $tmp IS NULL' to the fields in the Update_Vendors query
######			to allow updates to NULL fields.
###### 11/19/03 Modified all queries involving discount percentage valuess to show
######			values as percentages and to store values as decimals.
###### 02/10/04 took care of an init'd var error
###### 08/23/04 Added '::numeric' to the query build of the discount values that are being 
######		divided by 100, because interger values were evaluating to zero.
###### 01/23/08 a tiny tweak in Check_Required to allow negative numbers to fly on the numeric test
