#!/usr/bin/perl -w
###### admin-rd-pwl.cgi (an admin script)
###### accept a numeric ID, lookup the personal website and go on your way (as long as they have 1 and 1 only)
###### created: 06/27/07	Bill MacArthur
###### last modified:

use strict;
use CGI;
use lib ('/home/httpd/cgi-lib');
use ADMIN_DB;
my $q = new CGI;
my $id = $q->param('id') || '';
die "Invalid or missing id parameter: $id\n" unless $id;
###### we should have our operator and password cookies
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged in to use this resource\n" unless $operator && $pwd;

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
$db->{RaiseError} = 1;
my $urls = $db->selectall_arrayref('SELECT val_text FROM member_options WHERE option_type = 2 AND id = ?', undef, $id);

if (scalar @{$urls} == 1){ print $q->redirect($urls->[0][0]); }
elsif (scalar @{$urls} == 0){ print $q->header('text/plain'), "No personal website record was found for: $id"; }
else {
	print $q->header(-charset=>'utf-8'), $q->start_html;
	foreach (@{$urls}){ print $q->p( $q->a({-href=>$_->[0][0]}, $_->[0][0])); }
	print $q->end_html;
}
$db->disconnect;
exit;


