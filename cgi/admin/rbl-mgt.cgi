#!/usr/bin/perl -w
# rbl-mgt.cgi
# present an empty form to accept values for a new rbl record
# create a record after form submission
# or pull a record and accept modifications
# created Jan. 2007	Bill MacArthur
# last modified: 01/29/07	Bill MacArthur

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use G_S;

our (@alerts, @inst, @errors, @msg, $db) = ();
our %fields = (
	network => {sort=>10, descr=>'IP Address or Segment'},
	domain => {sort=>20, descr=>'Domain', ctl=>\&Domain_List},
	country => {sort=>30, descr=>'Country', ctl=>\&Country_List},
	code => {sort=>40, descr=>'Reason', ctl=>\&Reasons},
	active => {sort=>50, descr=>'Active', ctl=>\&Active_btn},
	notes => {sort=>60, descr=>'Notes'},
	stamp => {sort=>70, descr=>'Stamp'}
);
my $q = new CGI;

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

###### we will not allow admin access for update use (we have an in-house interface for that)
if ( $operator && $pwd )
{
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
	$db->{RaiseError} = 1;
}
else
{
	push @errors, 'You must be logged in to use this interface.';
	goto 'END';
}

my $action = $q->param('action');
unless ($action) {
	# this corresponds to an 'Add'
#	push (@inst, 'To add donor records, simply fill out the form and click the Add button.');
	Display_Form();
}
elsif ($action eq 'add') {
	if (AddRecord() eq '1'){ $q->delete_all }
	Display_Form();
}
elsif ($action eq 'search') {
	###### if we get multiple records, PullRecords will handle the call to generate a list page
	Display_Form() unless PullRecords();
}
elsif ($action eq 'edit') {
	Update();
	PullRecords();
	Display_Form();
}
else { push (@errors, 'Unrecognized action parameter: ' .$q->param('action')); goto 'END'; }

END:
Err() if @errors;
$db->disconnect if $db;
exit;

sub Active_btn {
	return $q->checkbox(
		-name=> 'active',
		-value=>1,
		-checked=>1,
		-label=>''
	);
}

sub AddRecord {
	my $qry = 'INSERT INTO rbl (';
	my $end = 'VALUES (';
	foreach (keys %fields){
		# we don't want to insert NULLs, that seems to end up disabling default column values
		next unless $q->param($_);
		$qry .= "$_,";
		$end .= $db->quote(G_S::PreProcess_Input($q->param($_))) . ',';
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )");
	if ($rv eq '1'){
		push (@msg, 'Record creation was successful');
	} else {
		push (@alerts, $DBI::errstr) unless $rv eq '1';
	}
	return $rv;
}

sub CtlStamp {
	
}

sub Country_List {
	my %list = ();
	my @list = '';
	my $sth = $db->prepare("SELECT country_code, country_name FROM tbl_countrycodes ORDER BY country_name");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref){
		$list{$tmp->{country_code}} = "$tmp->{country_name} ($tmp->{country_code})";
		push @list, $tmp->{country_code};
	}
	return CGI::popup_menu(
		-name=>'country',
		-values=> \@list,
		-labels=> \%list,
		-default=> ($q->param('country') || '')
	);
}

sub Domain_List {
	my @list = '';
	my $sth = $db->prepare("SELECT domain FROM rbl_domains ORDER BY domain");
	$sth->execute;
	while (my ($tmp) = $sth->fetchrow_array){push @list, $tmp}
	return CGI::popup_menu(
		-name=>'domain',
		-values=> \@list,
		-default=> ($q->param('domain') || '')
	);
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub Display_Form {
	print $q->header(-expires=>'now', -type=>'text/html', -charset=>'utf-8'), $q->start_html(
		-encoding=>'utf-8',
		-style=>{-src=>'/css/rbl-mgt.css', -type=>'text/css'},
		-script=>{-src=>'/js/admin/rbl-mgt.js', -type=>'text/javascript'},
		-title=>'Add Records',
		-onload=>'init()');
	print $q->h3('Add/Edit Records');
	print $q->p({-class=>'error'}, $_) foreach @alerts;
	print $q->p({-class=>'msg'}, $_) foreach @msg;
	print $q->p({-class=>'inst'}, $_) foreach @inst;
	print $q->start_form(-method=>'GET', -action=>$q->url, -id=>'main');
	if ($action eq 'search' || $action eq 'edit'){
		print $q->hidden(-name=>'action', -value=>'edit', -force=>1);
	} else { print $q->hidden(-name=>'action', -value=>'add', -force=>1) }

	print '<table>';

	foreach (sort {$fields{$a}->{sort} <=> $fields{$b}->{sort}} keys %fields){
		###### of course we will never have a real stamp parameter, only a value stuffed in from a search
		next if $_ eq 'stamp' && ! $q->param('stamp');
		print "<tr><td>$fields{$_}->{descr}</td><td>\n";
		print $fields{$_}->{ctl} ? &{$fields{$_}->{ctl}} : $q->textfield(-name=>$_, -class=>'tf', -onclick=>'this.select()');
		print "</td></tr>\n";
	}
	print $q->Tr($q->td({-colspan=>2,-align=>'right'},
		$q->submit(-class=>'btn', -id=>'actionBtn',
			-value=>($action eq 'search' || $action eq 'edit') ? 'Edit' : 'Add') . ' ' .
		$q->reset({-class=>'btn'}, 'Reset')));
	print '</table>', $q->end_form;

	###### the search form
	print $q->h3('Search Records');
	print $q->start_form(-method=>'GET', -action=>$q->url, -id=>'search');
	print $q->hidden(-name=>'action', -value=>'search', -force=>1);
	print 'Enter an IP address: ', $q->textfield(-name=>'network', -class=>'tf', -onclick=>'this.select()', -force=>1);
	print ' ', $q->submit, $q->end_form;
	print $q->p({-align=>'right'}, $q->a({-href=>$q->url}, 'Start Over')), $q->end_html;
}

sub Display_List {
	my @list = shift;
	print $q->header(-expires=>'now', -type=>'text/html', -charset=>'utf-8'), $q->start_html(
		-style=>{'src'=>'/css/rbl-mgt.css'},
		-title=>'List RBL Records');
	print $q->h3('RBL records for ' . $q->param('network'));
	print "<table><tr>\n";
	foreach(SortedKeys()){
		print $q->th($fields{$_}->{descr});
	}
	print "</tr>\n";
	foreach my $row (@list){
		print "<tr>\n";
		foreach(SortedKeys()){
			$row->{$_} = $q->a({-href=>$q->url . "?network=$row->{$_}"}, $row->{$_}) if $_ eq 'network';
			print $q->td($row->{$_});
		}
		print "</tr>\n";
	}
	print "</table>\n", $q->end_html;
}

sub PullRecords {
	unless ($q->param('network')){
		push @alerts, 'No IP address received';
		return undef;
	}
	my $sth = $db->prepare("SELECT * FROM rbl WHERE network >>= ?");
	$sth->execute(G_S::PreProcess_Input($q->param('network')));
	my @list = ();
	while (my $tmp = $sth->fetchrow_hashref){ push @list, $tmp }
	if (scalar @list > 1){
		Display_List(@list);
		return 1;
	}
	elsif (scalar @list == 1){
		push @msg, '1 match found';
		# load the recordset into the params var
		# when CGI.pm creates the form fields, it will use the values automatically
		foreach (keys %{$list[0]}){ $q->param(-name=>$_, -value=>$list[0]->{$_}) }
	}
	else {
		push @alerts, 'No matches found for: ' . $q->param('network');
		$q->delete_all;
	}
	return undef;
}

sub Reasons {
	my %list = ();
	my @list = '';
	my $sth = $db->prepare("SELECT code, value FROM rbl_codes ORDER BY code");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref){
		$list{$tmp->{code}} = $tmp->{value};
		push @list, $tmp->{code};
	}
	return CGI::popup_menu(
		-name=>'code',
		-values=> \@list,
		-labels=> \%list,
		-default=> ($q->param('code') || '')
	);
}

sub SortedKeys {
	return sort {$fields{$a}->{sort} <=> $fields{$b}->{sort}} keys %fields;
}

sub Update {
	my $id = $q->param('network') || die "Failed to receive network param";
	my $qry = 'UPDATE rbl SET ';
	foreach my $p (keys %fields){
		next if grep $p eq $_, ('network','stamp');
		my $parm = G_S::PreProcess_Input($q->param($p));
		$qry .= "$p=";
		###### the active flag has a NOT NULL constraint
		$qry .= $p ne 'active' ? (length ($parm ||'') > 0 ? $db->quote($parm) : 'NULL') :
			($parm > 0 ? 'TRUE' : 'FALSE');
		$qry .= ',';
	}
	$qry .= 'stamp=NOW()';
	$qry .= ' WHERE network= ?';
	if ($db->do($qry, undef, $q->param('network')) eq '1'){ push (@msg, 'Record edit successful.'); }
	else { push (@alerts, 'Record edit failed'); }
}

# 01/29/07 revised the add record SQL to not insert explicit NULLs