#!/usr/bin/perl -w
use strict;
use Net::Nslookup;
use Time::HiRes  qw( time );
use CGI;
use CGI::Carp qw(fatalsToBrowser);

$| = 1;

my $q = new CGI;
die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $classC = $q->param('classC');
$classC =~ s/^\s*|\s*$//g;

Interface() unless $classC;

die "Invalid class C network: $classC" if $classC !~ m/^\d+\.\d+\.\d+$/;
print $q->header('text/plain');

for (my $x=1; $x < 256; $x++)
{
	my $ip = "$classC.$x";
	my $start = time();
	my $rv = nslookup('host'=>$ip, 'type'=>'PTR') || '';
	my $end = time();
	printf("%.2f", $end - $start);
	print " .. $ip : $rv\n";
}

exit;

sub Interface
{
	print $q->header,
		$q->start_html('Check Net Range'),
		$q->start_form,
		$q->div(
			'Enter a class C network (the first 3 octets): ' . $q->textfield('-name'=>'classC') . $q->submit({'style: margin-left: 1em'})
		),
		$q->end_form,
		$q->end_html;
	
	exit;
}