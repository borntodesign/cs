#!/usr/bin/perl -w
=head2 ContactMerchSponsorAboutBounce.cgi

=head3 Basically, this does what the name suggests.

	Requires a merchant ID, presents an admin interface with a few merchant details for confirmation
	The user must provide a "From" address.
	The user can/should also provide the email that bounced (editted to exclude sensitive data if necessary)
	
	With the correct data, an email will be sent to the sponsoring VIP in their language preference and the sender will be CC'd

=cut

use strict;
use CGI;
use CGI::Carp 'fatalsToBrowser';
use MIME::Lite;
use Template;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use ParseMe;
use G_S;

my $cgi = new CGI;
my (%params, @cookies) = ();
my $SENDNONE = 0;	# set this to -0- to actually send the mail, otherwise do everything but

unless ($cgi->https){
	my $url = $cgi->url;
	$url =~ s/^http/https/;
	print $cgi->redirect($url);
	exit;
}

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $cgi->cookie('operator'), $cgi->cookie('pwd') );
$db->{RaiseError} = 1;
my $action = $cgi->param('_action');

unless ($action)
{
	LoadParams('required'=>[qw/merchant_id/]);
	Interface();
}
elsif ($action eq 'send')
{
	LoadParams('required'=>[qw/to merchant_id from email subject message/]);
	push @cookies, $cgi->cookie(
		-name=>'ContactMerchSponsorAboutBounce_from',
		-value=>$params{'from'},
		-secure=>1,
		-path=>$cgi->script_name
		);
	my $rv = SendMail();
	Results($rv);
}

$db->disconnect;
exit;

sub FilterWhiteSpace
{
###### filter out leading & trailing whitespace
	return $_[0] unless defined $_[0];
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub GetMessage
{
	my $args = shift;
	my $tmp = G_S::Get_Object($db, $args->{'template'}, $args->{'sponsor'}->{'language_pref'});

	my $tt = Template->new();
	my $msg = ();
	$tt->process(\$tmp, $args, \$msg);
	my $rv = ParseMe::Grab_Headers(\$msg);
#warn keys %{$rv};
	$rv->{'message'} = $msg;
	return $rv;
}

sub Interface
{
	my $merchant = $db->selectrow_hashref('
		SELECT id, member_id, business_name, emailaddress1
		FROM merchant_affiliates_master WHERE id=?', undef, $params{'merchant_id'}) || 
		die "No merchant found with ID: $params{'merchant_id'}";
	
	my $sponsor = $db->selectrow_hashref("
		SELECT ms.id, ms.alias, ms.emailaddress, ms.firstname, ms.lastname,
			ms.firstname || ' ' || ms.lastname AS name,
			ms.language_pref
		FROM members m
		JOIN members ms
			ON ms.id=m.spid
		WHERE m.id= $merchant->{'member_id'}") ||
		die "Failed to obtain sponsor for merchant ID: $params{'merchant_id'}";

	my $msg = GetMessage({'merchant'=>$merchant, 'sponsor'=>$sponsor, 'template'=>1530});
	
	print
		$cgi->header(-charset=>'utf-8'),
		$cgi->start_html(
			-encoding=>'utf-8',
			-style=>{
				-code=>'
				body {
					background-color: #efffef;
					font:normal 80% sans-serif;
				}
				table {
					border-collapse:collapse;
					background-color:white;
				}
				td {
					border:1px solid silver;
					padding: .2em .3em;
					font:normal 85% sans-serif;
				}
				input.textfield {
					width: 20em;
					font:normal inherit sans-serif;
				}
				textarea { font:normal 85% sans-serif; }
			'},
			-title=> 'Contact Merchant Sponsor About Merchant Email Bounce'
		);
	print
		$cgi->start_form(),
		$cgi->hidden('merchant_id'),
		$cgi->hidden(-name=>'_action', -value=>'send'),
		'<table style="background-color:inherit"><tr><td>',
		$cgi->p('Send mail about this merchant:'),
		qq|<table>
			<tr><td>Merchant ID</td><td>$merchant->{'id'}</td></tr>
			<tr><td>Merchant Name</td><td>$merchant->{'business_name'}</td></tr>
			<tr><td>Merchant Email</td><td>$merchant->{'emailaddress1'}</td></tr>
		   </table></td>|;
	print
		'<td>',
		$cgi->p('To this VIP sponsor:'),
		qq|<table>
			<tr><td>Sponsor ID</td><td>$sponsor->{'alias'}</td></tr>
			<tr><td>Sponsor Name</td><td>$sponsor->{'name'}</td></tr>
			<tr><td>Sponsor Email</td><td>|,
		$cgi->textfield(-name=>'to', -value=>$sponsor->{'emailaddress'}, -class=>'textfield'),
		qq|</td></tr></table></td></tr></table>|;
	print
		'<br /><br />',
		'<table><tr><td>From (your emailaddress): </td><td>',
		$cgi->textfield(-name=>'from', -class=>'textfield', -value=>$cgi->cookie('ContactMerchSponsorAboutBounce_from')),
		'</td></tr><tr><td>Subject: </td><td>',
		$cgi->textfield(-name=>'subject', -class=>'textfield', -value=>$msg->{'SUBJECT'}),
		'</td></tr></table><br />',
		'<br />Paste Bounced email: (remember to remove sensitive information)<br />',
		$cgi->textarea(
			-name=>'email',
			-rows=>25,
			-cols=>120,
			-wrap=>'virtual'
			);
	print
		'<br />', $cgi->submit(-value=>'Send', -style=>'color:#0a0; font-weight:bold;'),
		' ', $cgi->reset(-style=>'color:red'), '<br /><br />',
		'The message that will be sent:<br />',
		$cgi->textarea(
			-name=>'message',
			-rows=>25,
			-cols=>120,
			-wrap=>'virtual',
			-value=>$msg->{'message'}
			);
		$cgi->end_form,
		$cgi->end_html;
}

sub LoadParams
{
	my %args = @_;
	my $rv = {};
	foreach(qw/
		_action
		merchant_id
		from
		email
		message
		subject
		to/)
	{
		# don't want a null "param" unless there was really an empty parameter
		next unless defined $cgi->param($_);
		$params{$_} = FilterWhiteSpace( $cgi->param($_) );
	}
	
	# these are required
	foreach my $req (@{$args{'required'}})
	{
		die "$req is a required parameter" unless $params{$req};
	}
}

sub Results
{
	my $rv = shift;
	print $cgi->header(-type=>'text/plain', -cookie=>\@cookies);
	print $SENDNONE ? "In testing mode. Mail NOT sent\n\n" : "Mail sent:\n\n";
	print $rv;
}

sub SendMail
{
	my $msg = MIME::Lite->new(
		'From'    => $params{'from'},
		'To'      => $params{'to'},
		'Cc'      => $params{'from'},
		'Subject' => $params{'subject'},
		'Data'    => $params{'message'}
    );

    $msg->attach(
        Type     => 'TEXT',
        Data     => $params{'email'}
    ) if $params{'email'};

	unless ($SENDNONE){
	    ### use Net:SMTP to do the sending
    	$msg->send('smtp', 'mail.dhs-club.com', Debug=>0 );
    	die "Message send failed" unless $msg->last_send_successful();
	}
#    print $cgi->header('text/plain');
#    $msg->print(\*STDOUT);

	return $msg->as_string;
}