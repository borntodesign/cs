#!/usr/bin/perl -w
###### mail_job.cgi
###### 10/27/2003	Bill MacArthur
######
###### Last modified: 12/18/00 Bill MacArthur

###### ###### If you get a 'complaint' of an extraneous > in the mail body of a test message
###### ignore it. There is something in the innards of sendmail that places the character before a line that begins
###### with 'From '
###### It is not in this script nor in Mail::Sendmail as the problem manifests even if piping directly to sendmail.

use strict;
use Mail::Sendmail;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
use ADMIN_DB;

###### GLOBALS
my $q = new CGI;
my $QRY = ''; # will hold our query that will form the basis for the job
my %param = ();
my $job_type = $q->param('job_type');
my (@domains, @country, @join_date, @vip_date, @append_freeform) = ();

###### initialize our default result set
my $default_fields =  'members_cancel_pending.id,firstname,lastname,alias,
	emailaddress,members_cancel_pending.oid as moid';

###### the result set is the column names that will be returned
my %HTML_TEMPLATES = (
	'step2' =>	"/home/httpd/cgi-templates/Html/mail_job/mail_job_cgi_step2.html",
	'step3' =>	"/home/httpd/cgi-templates/Html/mail_job/mail_job_cgi_step3.html");

my ($key, $val, $myflg, $dbflag, $dbserver, $db);

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

unless ($db = ADMIN_DB::DB_Connect( 'mail_job.cgi', $operator, $pwd )){exit}

###### we want to see our errors
$db->{RaiseError} = 1;
Load_Params();

###### we have a new job to consider
if ( $param{action} eq 'new' ){
	$QRY = '
		FROM members_cancel_pending
		JOIN ps_cnt
			ON ps_cnt.id=members_cancel_pending.id';

	if ( $q->param('inc_vip_tables') ){
		$QRY .= '
			LEFT JOIN network_calcs
				ON 	network_calcs.id=members_cancel_pending.id
			LEFT JOIN level_values
				ON level_values.lvalue = network_calcs.a_level';
	}
	
	if ( $q->param('inc_roles_tables')){
		$QRY .= '
			LEFT JOIN nbteam_vw
				ON nbteam_vw.id=members_cancel_pending.id
			LEFT JOIN nbteam_levels
				ON nbteam_levels."level"=nbteam_vw."level"';
	}

	$QRY .= '
			WHERE (';


###### we must have a job type specified for this section to work
	unless ( $job_type )
	{
		Err( $q->h4('improper parameter received<br>job_type') );
		goto 'END';
	}

###### STRUCTURED FORM JOB STARTS HERE
	if ( $job_type eq 'structured' ){
###### load our arrays for a structured job
		@country = $q->param('country');
		foreach ( split /\n/, $q->param('domains') ){
			push ( @domains, $_ );
		}
		foreach ( split /\n/, $q->param('join_date') ){
			push ( @join_date, $_ );
		}
		foreach ( split /\n/, $q->param('vip_date') ){
			push ( @vip_date, $_ );
		}
		foreach ( split /\n/, $q->param('append_freeform') ){
			push ( @append_freeform, $_ );
		}
		my @membertype = $q->param('membertype');
###### if we don't have a member type we must quit since we don't want to select across all member types
		unless ( @membertype ){
			Err( $q->h4('improper parameter received<br />membertype') );
			goto 'END';
		}
		foreach ( @membertype ){
			$QRY .= "membertype = '$_' OR ";
		}
###### truncate the last 'OR ' and sustitute a closing parenthesis
		$QRY =~ s/.{3}$/)\n/;

		my @mail_option_lvl = $q->param('mail_option_lvl');
###### if we don't have a mail option level we must quit since we don't want to select types
		unless ( @mail_option_lvl ){
			Err( $q->h4('improper parameter received<br />mail option level') );
			goto 'END';
		}
		$QRY .= " AND (";
		foreach ( @mail_option_lvl ){
			$QRY .= "mail_option_lvl = '$_' OR ";
		}
###### truncate the last 'OR ' and sustitute a closing parenthesis
		$QRY =~ s/.{3}$/)\n/;

###### add our email domain modifiers
		if ( @domains ){
			$QRY .= "AND (";
			foreach ( @domains ){
				$QRY .= "emailaddress ~ '\@$_' OR ";
			}
###### truncate the last 'OR' and substitute a closing parenthesis
			$QRY =~ s/.{3}$/)\n/;
		}

###### add our join date modifiers
		if ( @join_date ){
			$QRY .= "AND (";
			foreach ( @join_date ){
				$QRY .= "join_date $_ AND ";
			}
###### truncate the last 'AND' and substitute a closing parenthesis
			$QRY =~ s/.{4}$/)\n/;
		}
###### add our vip date modifiers
		if ( @vip_date ){
			$QRY .= "AND (";
			foreach ( @vip_date ){
				$QRY .= "vip_date $_ AND ";
			}
###### truncate the last 'AND' and substitute a closing parenthesis
			$QRY =~ s/.{4}$/)\n/;
		}
###### add our country modifiers 
		if ( @country ){
			unless ( scalar @country == 1 && $country[0] eq '' ){
###### the country parameter will be present even if nothing is selected, so we need to escape the NULL
				$QRY .= "AND (";
				foreach ( @country ){
					$QRY .= "country = '$_' OR ";
				}
###### truncate the last 'AND' and substitute a closing parenthesis
				$QRY =~ s/.{3}$/)\n/;
			}
		}

###### add our 'FreeForm' modifiers
		if ( @append_freeform ){
			$QRY .= "AND (";
			foreach ( @append_freeform ){
				$QRY .= "$_ AND ";
			}
###### truncate the last 'AND' and substitute a closing parenthesis
			$QRY =~ s/.{4}$/)\n/;
		}
	}

###### FORM OUR FREEFORM SECTION HERE
	elsif ( $job_type eq 'freeform' ){
###### load our arrays for a freeform job
		Append_Default_results();
		my $qry_modifier = $param{'freeform_query_modifier'};
		unless ( $qry_modifier ){
			Err( $q->h4('improper parameter received<br>no freeform_query_modifier') );
			goto 'END';
		}
		else{ $QRY .= "$qry_modifier)\n" }
	}
	else{ Err( $q->h4('improper parameter received<br>job_type') )}

###### create our query beginning
	my $QRY_START = "SELECT $default_fields\n";

###### in order to present query to read, we will substitute the newlines for html line breaks
	my $QRYdisplay = $QRY;
	$QRYdisplay =~ s#\n#<br />#g;

###### our query construction processing is now complete
###### now lets get an aniticipated count of the result set
	$db->{RaiseError} = 0;
	my $sth = $db->prepare("SELECT COUNT(*) $QRY"); $sth->execute;
	if ($DBI::err){
		Err("Database error # $DBI::err<br />Error message: $DBI::errstr<br />SELECT COUNT(*) The query: $QRY");
		$sth->finish;
		goto 'END';
	}
	$db->{RaiseError} = 1;

	my $acount = $sth->fetchrow_array; $sth->finish;
	my $hidden = $q->hidden(-name=>'query', -value=>"$QRY_START $QRY");
	$hidden .= $q->hidden(-name=>'acount', -value=>$acount);

###### we need to pass these on for our testing of the message in step two
	$hidden .= $q->hidden(-name=>'extra_fields');
	print $q->header();
	open (PAGE, "$HTML_TEMPLATES{'step2'}") || die "Couldn't open step 2 template\n";
	foreach (<PAGE>){
		$_ =~ s/%%query%%/$QRY_START<br>$QRYdisplay/;
		$_ =~ s/%%acount%%/$acount/;
		$_ =~ s/%%hidden%%/$hidden/;
		$_ =~ s/%%script_name%%/$q->url()/e;
		print "$_\n";
	}
	close PAGE;
}
elsif ( $param{action} eq 'test' )
{
###### make sure we have required fields
	foreach ( qw/from_address notifications2 mail_txt subject query/ )
	{
		unless ( $param{$_} )
		{
			Err("No parameter received.<br />$_");
			goto 'END';
		}
	}

	my $hidden = '';
###### get a row of data to test
	my $sth = $db->prepare("$param{'query'} LIMIT 1");
	$sth->execute;
	my $results = $sth->fetchrow_hashref;
	$sth->finish;

	my %MAIL  = (
		'Content-Type' => 'text/plain; charset="utf-8"',
		To => $param{'notifications2'},
		From => $param{'from_address'},
		'Reply-To' => $param{'reply2_address'},
		Message => $param{'mail_txt'}
	);

	foreach ( keys %{$results} )
	{
###### we need to reduce the column renaming to what the database will return
		$_ =~ s/members_cancel_pending.oid as moid/moid/g;

###### if they are used to using Gammadyne, they will be familiar with enclosing placeholders in double [[ ]]
		$MAIL{Message} =~ s/\[\[$_\]\]/$results->{$_}/g;
		$param{'subject'} =~ s/\[\[$_\]\]/$results->{$_}/g;
	}
	$MAIL{Subject} = $param{'subject'};

###### process the removal link if checked
	if ( $param{'remove_link_flag'} eq 't' )
	{
		my $remove_link = G_S::Get_Object($db, 99, 'en') || die "Couldn't retrieve removal link object";
		$remove_link =~ s/%%emailaddress%%/$results->{emailaddress}/g;
		$remove_link =~ s/%%oid%%/$results->{moid}/g;
		$MAIL{Message} .= "\n\n\n$remove_link\n";
	}

	sendmail(%MAIL) ||
		die "Failed to send test email: $Mail::Sendmail::Error\n\nPlease press the Back button and try again\n";

	open (PAGE, "$HTML_TEMPLATES{'step3'}") || die "Couldn't open step 3 template\n";
	print $q->header(-charset=>'utf-8');
	foreach (<PAGE>)
	{
		$_ =~ s/%%script_name%%/$q->url()/e;
		$_ =~ s/%%notifications2%%/$param{'notifications2'}/g;
		$_ =~ s/%%hidden%%/Create_Hidden()/e;
		print $_;
	}
	close PAGE;
}		
elsif ( $param{'action'} eq 'process' )
{
	my $qry = '';
	my $subject = $db->quote($param{'subject'});
	$param{'job_start'} = $param{'job_start'} ? $db->quote($param{'job_start'}) : 'NOW()';

###### first get a job number
	my $sth = $db->prepare("SELECT nextval('mail_jobs_job_number_seq')"); $sth->execute;
	my $jobnum = $sth->fetchrow_array; $sth->finish;

	$qry = "
INSERT INTO mail_jobs (job_number, owner, sql_statement, total_records, from_address, 
	reply_to, subject, message_text, start, notifications_to, remove_link_flag)
VALUES
	($jobnum, '$operator', ?, $param{'acount'}, '$param{'from_address'}', 
	'$param{'reply2_address'}',	 $subject, ?, $param{'job_start'}, '$param{'notifications2'}',
	'$param{'remove_link_flag'}');
";

###### now we'll add the job table creation statement
	my $query = "$param{'query'} ORDER BY emailaddress";
	$query =~ m/(.+?)(FROM.+)/s;
	$qry .= "\n$1 INTO z$jobnum $2;\n";
	$qry .= "GRANT select ON z$jobnum TO public";

	$db->{RaiseError} = 0;
	$sth = $db->do( $qry, undef, "$1 INTO z$jobnum $2", $param{'mail_txt'});
#	$sth = $db->prepare( $qry ); $sth->execute( "$1 INTO z$jobnum $2", $param{'mail_txt'});
	if ($DBI::err){
		$qry =~ s#\n#<br />#g; 
		&Err("Database error # $DBI::err<br />Error message: $DBI::errstr<br />The query:<br />$qry");
#		$sth->finish;
		goto 'END';
	}
#	$sth->finish;

	print $q->header(), $q->start_html(-bgcolor=>'#ffffff');
	print "Job # <b>$jobnum</b> successfully set up.<br> Please make note of the number for future reference.";
	print $q->end_html();
}
else{ Err( $q->h4('improper parameters received<br>action') )}


END:
$db->disconnect;
exit;

sub Append_Default_results
{
	foreach ( split /\n/, $param{'extra_fields'} ){
		$_ =~ s/^\s*//; $_ =~ s/\s*$//;
		$default_fields .= ",$_";
	}
}

sub Create_Hidden
{
	my $hidden = '';
	foreach ( keys %param ){
###### we want to exclude the action parameter because we will be sending a new one
		$hidden .= $q->hidden($_) if ( $_ ne 'action' );
	}
	return $hidden;
}

sub Err
{
	my ( $msg ) = @_;
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff');
	print "$msg\n";
	print $q->end_html();
}

sub Load_Params
{
	foreach ( $q->param() ){
		$param{$_} = $q->param($_);
	}
}

###### 10/26/01 added escaping for the mail body and the subject before insertion into the database
###### 01/03/02 removed DB raise error for the initial query and opted for a more complete look at the query
###### 	removed db connect failure logging, also turned DBI RaiseError off so we could explicitly trap errors
###### 	and display the offending query, also went with the global admin DB connect lib,
###### 	dropped the EXCLUDED_DOMAINS query exclusion
###### 02/26/03 added the ps_cnt table into the base query
###### 07/25/03 revised the looping for the columns parsing to use the actual columns
###### 	returned rather than those extrapolated from the query
###### 	this fixes things like COALESCE(...), etc.
###### 	pointed to network_calcs instead of vipcalcs.
###### 07/28/04 Changed members table to members_cancel_pending view in the $default_fields, in the
###### 	'if ( $param{action} eq 'new' ) section, and in the 'reduce to column name' section
######		in order to weed out cancels from this month still marked as VIP.
###### 08/07/06 revised the main query to pull in the id too
# 09/21/06 coercing the removal link to 'en'
# 10/12/06 added use lib
###### 06/20/07 employing Mail::Sendmail
