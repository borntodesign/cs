#!/usr/bin/perl -w
##
## img_tag_tracking_search.cgi
## derived from Shane Partain's tracking_search
##
## released: 07/26/07	Bill MacArthur
## modified: 02/18/11	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

my $MAX_RESULTS = 300;	###### the maximum result set we will display before telling 'em to refine their query
#my @query_params = ('id', 'v.vendor_id', 'ip_addr');
#my %query_params = ();
my %params = ();
my $start = '';
my $end = '';
my $i = '';
my @row = ();
my @table = ();
my %colmap = (
	referer => {selected=>1, sort=>200, optlbl=>'referer (typically the Vendor`s site)'},
	ip => {selected=>1, sort=>30, optlbl=>'IP address'},
	id => {selected=>1, sort=>40, optlbl=>'ID (user`s ID cookie)'},
	cid => {selected=>1, sort=>50, optlbl=>'CID (the ID as returned by the vendor)'},
	vid => {selected=>1, sort=>60, optlbl=>'VID visitor ID'},
	xid => {selected=>1, sort=>70, optlbl=>'Transaction ID from vendor'},
	stamp => {selected=>1, sort=>80, optlbl=>'Timestamp'},
	pk => {sort=>1, optlbl=>'Record #'},
	currency => {sort=>100, optlbl=>'Sale Currency'},
	description => {sort=>110, optlbl=>'A description returned by the vendor'},
	reconciled => {sort=>120, optlbl=>'Reconciled Flag'},
	comamt => {sort=>130, optlbl=>'Commission Amount'},
	cbid => {sort=>140, optlbl=>'ClubBucks Card #'},
	vendor_id => {sort=>150, optlbl=>'Vendor ID'}
);

my $q = new CGI;
my @date = localtime;
$date[5] += 1900;
my @year_options = ();
#for (my $x = 2010; $x < $date[5] +1; $x++){ push @year_options, $x; }	###### plan ahead ;)
for (my $x = $date[5]; $x > $date[5] -2; $x--){ push @year_options, $x; }	###### plan ahead ;)

## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my $db = ADMIN_DB::DB_Connect('tracking_search.cgi', $operator, $pwd) || exit;
$db->{RaiseError} = 1;

my @display_cols = sort {$colmap{$a}->{sort} <=> $colmap{$b}->{sort}} $q->param('display_cols');	###### this is a multi-valued parameter
die "Invalid column name received" if grep /\W/, @display_cols;	###### simple sanity check
my $sort = $q->param('sort');
die "Invalid column name received" if $sort =~ /\W/;	###### simple sanity check
foreach my $p ($q->param()){
	next if grep $_ eq $p, ('display_cols','sort');
	$params{$p} = G_S::PreProcess_Input($q->param($p));
}
die "Invalid column name received" if grep /\W/, keys %params;	###### simple sanity check

unless (%params) {
	SearchForm();
}else{

	##
	## Print out the results.
	##

	if($params{start_month}) {
		$params{start_month} = sprintf("%02d", $params{start_month});
		$params{start_day} = sprintf("%02d", $params{start_day});
		$start = $params{start_year} . "-" . $params{start_month} . "-" . $params{start_day};
#		$start = $db->quote($start);
	}
	if($params{end_month}) {
		$params{end_month} = sprintf("%02d", $params{end_month});
		$params{end_day} = sprintf("%02d", $params{end_day});
		$end = $params{end_year} . "-" . $params{end_month} . "-" . $params{end_day};
#		$end = $db->quote($end);
	}

	my $query = 'SELECT pk,';
	foreach(@display_cols){
		next if $_ eq 'pk';	###### we include the column by default the data, display is optional
		$query .= "$_,";
	}
	chop $query;
	$query .= ' FROM img_tag_tracking WHERE ';
# 	foreach (@query_params) {
# 		if ($params{$_}) {$query_params{$_} = $db->quote($params{$_}) };
# 	}
	my @args = ();
	foreach (keys %params) {
		next if (/^start/ || /^end/);	## Everything but the dates gets appended to the query here.
		next unless $params{$_};	###### we don't want to query on empty vals
		if ($_ eq 'id')			# we'll query both id and cid on the submitted id value
		{
			if ($params{$_} =~ /null/i)	# make a way to use NULL
			{
				$query .= '(id IS NULL AND cid IS NULL) AND ';
			} else {
				$query .= '(id = ? OR cid = ?) AND ';
				push @args, ($params{$_}, $params{$_});
			}
		} else {
			$query .= "$_ = ? AND ";
			push @args, $params{$_};
		}
	}
	$query .= "stamp::DATE BETWEEN ? AND ? ORDER BY $sort LIMIT $MAX_RESULTS";
	push @args, ($start, $end);

	my $table = '<table><tr>';
	$table .= $q->th(\@display_cols) . "</tr>\n";
	my $class = 'a';
	my $sth = $db->prepare($query);
	$sth->execute(@args);
	if (my $row = $sth->fetchrow_hashref){
		$table .= $q->Tr({-class=>$class}, $q->td([map $q->escapeHTML($row->{$_}), @display_cols]) );
		my $rows = 1;
		while ($row = $sth->fetchrow_hashref){
			$class = $class eq 'a' ? 'b' : 'a';
			$table .= $q->Tr({-class=>$class}, $q->td([map $q->escapeHTML($row->{$_}), @display_cols]) );
			$rows++;
		}

		print $q->header(-charset=>'utf-8'),
			$q->start_html(
			-title=>'Direct Sales Tracking Search Results',
			-encoding=>'utf-8',
			-style=>{
				-code=>'table{font-size:75%; border-collapse:collapse;}
					td{white-space:nowrap; padding: 0.2em 0.3em; border:1px solid #009;}
					th{background-color:#cccccc; border:1px solid #009;}
					tr.a {background-color:rgb(255,255,240);}
					tr.b {background-color:#efefef;}
				'
		});
		print $q->div("Your query has been limited to $MAX_RESULTS. Please go back and narrow your search.")
			if $rows == $MAX_RESULTS;

		print $q->h4("Direct Sales Tracking Search Results ($rows)"), $table, '</table>', $q->end_html;
	}
	else { SearchForm($q->span({-style=>'color:red'}, 'No records found')) }
}

$db->disconnect;
exit;

sub err
{
	print $q->header(-charset=>'utf-8'), $q->start_html('Image Tag Tracking Error'), $_[0], $q->end_html;
}

sub SearchForm
{
	my $msg = shift;
	##
	## print out the search form.
	##
	print $q->header(-charset=>'utf-8'),
	$q->start_html(
		-title=>'Direct Sales Tracking Search',
		-encoding=>'utf-8',
		-style=>{
			-code=>'td{vertical-align:top;} td,input,select{font-size:80%;}'
		}),
        $q->h4('Direct Sales Tracking Search');
	print $q->p($msg) if $msg;

       print $q->start_form(-method=>'get'),
	'<table>',
	$q->Tr( $q->td('Member ID'), $q->td($q->textfield(-name => 'id') .
		$q->span({-style=>'font-size:90%; padding-left:1em;'}, '(Enter the word "NULL" to get entries with no id <b>and</b> no cid)'))),
	$q->Tr( $q->td('Vendor ID'), $q->td($q->textfield(-name => 'vendor'))),
	$q->Tr( $q->td('IP Address'), $q->td($q->textfield(-name => 'ip'))),
	$q->Tr( $q->td('Start Date'), $q->td(
		$q->popup_menu(
			-name	=> 'start_month',
			-values => [1,2,3,4,5,6,7,8,9,10,11,12],
			-labels =>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'April',5=>'May',6=>'June',
				7=>'July',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'},
			-default => ($date[4] + 1)
			) .
		$q->popup_menu( -name   => 'start_day',
			-values => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
				21,22,23,24,25,26,27,28,29,30,31],
			-default => $date[3]
			) .
		$q->popup_menu( -name   => 'start_year',
			-values => \@year_options,
			-default => $date[5]
			))),
	$q->Tr( $q->td('End Date'), $q->td(
		$q->popup_menu(-name	=> 'end_month',
			-values => [1,2,3,4,5,6,7,8,9,10,11,12],
			-labels =>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'April',5=>'May',6=>'June',
				7=>'July',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'},
			-default => ($date[4] + 1)
			) .
		$q->popup_menu(-name => 'end_day',
			-values => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                                21,22,23,24,25,26,27,28,29,30,31],
			-default => $date[3]
                	) .
		$q->popup_menu(-name => 'end_year',
			-values => \@year_options,
			-default => $date[5]
			))),
	'<tr><td>Display Columns</td><td>
	<select name="display_cols" multiple="multiple" size="5">
		<optgroup label="Common">';
	foreach (sort {$colmap{$a}->{sort} <=> $colmap{$b}->{sort}} keys %colmap)
	{
		next unless $colmap{$_}->{selected};
		print $q->option({-selected=>'selected', -value=>$_}, $colmap{$_}->{optlbl});
	}
		print '</optgroup><optgroup label="Other">';
	foreach (sort {$colmap{$a}->{sort} <=> $colmap{$b}->{sort}} keys %colmap)
	{
		next if $colmap{$_}->{selected};
		print $q->option({-value=>$_}, $colmap{$_}->{optlbl});
	}
	print '</optgroup>
	</select></td></tr>
	<tr><td>Sort by:</td><td><select name="sort">
		<option selected="selected" value="stamp">Timestamp</option>
		<option value="ip">IP address</option>
		<option value="id">ID (user`s ID cookie)</option>
		<option value="cid">CID (the ID as returned by the vendor)</option>
	</select></td></tr>
	</table>',
	$q->submit,
	'<br /><br />',
	$q->a({-href => "/admin/ts_help.html",
		-onclick => "window.open(this.href, 'helpWindow', 'resizable=1,scrollbars=1,width=420,height=400')",
		-target => 'helpWindow'},
		'Help'),
        $q->end_form(),
        $q->end_html();
}

# 08/06/07 fixed up a few HTML errors and added HTML escaping for the referer URLs
# 02/18/11 revised the year drop-down list generation
