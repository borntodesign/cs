#!/usr/bin/perl -w
###### memberinfo.cgi
###### lookup a member record by id, alias or email address
###### also search the member archives by OID
###### last modified 06/08/2015	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Mail::RFC822::Address;		
use Template;
use lib ('/home/httpd/cgi-lib');
use State_List;
use G_S;
require cs_admin;
use ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
###### our page template
my %TMPL = (
	'livlist' => '5000',
	'liv' => '5010',
	'oid' => '5010',
	'arclist' => '5020',
	'arc' => '5030');

###### the table we'll get data from
my %TBL = (
	'arc' => 'members_arc',
	'liv' => 'members',
	'oid' => 'members');

my $q = new CGI;
$q->charset('utf-8');
my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $qry, $data, $dover_flag, %StateCodes, @StateCodes, $creds);

my $dover_msg = '';
my $action = ( $q->param('action') || '' );
my $id = $q->param('id');
my $Ltype = $q->param('Ltype') || 'liv';

my $override1 = $q->param('override1') || "0";
my $override2 = $q->param('override2') || "0";

my $o1 = '';
my $o2 = '';

my $badformat = 0;

my $CURRENT_MEMBERTYPE = '';

###### remove leading and trailing whitespace
$id =~ s/^\s*//; $id =~ s/\s*$//;

###### here is where we get our admin user and try logging into the DB
#my $operator = $q->cookie('operator');
#my $pwd = $q->cookie('pwd');
#exit unless $db = ADMIN_DB::DB_Connect( 'memberinfo.cgi', $operator, $pwd );
###### here is where we get our admin user and try logging into the DB
if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	$creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}
$db->{'RaiseError'} = 1;

my $gs = G_S->new({'db'=>$db, 'CGI'=>$q});

###### %field has the form of 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %field = (
	'id' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'Alias' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'membertype' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'spid' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'Prefix' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'secPrefix' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'FirstName' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'_FirstName' => {},
	'secFirstName' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'MDName' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'secMDName' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'LastName' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'_LastName' => {},
	'secLastName' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Suffix' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'secSuffix' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Company_Name' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Company_Name' => {},
	'Address1' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Address1' => {},
	'Address2' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'City' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_City' => {},
	'State' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_State' => {},
	'state_name' => {},
	'PostalCode' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_PostalCode' => {},
	'Country' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Country' => {},
	'cellphone' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'other_contact_info' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Phone' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Phone' => {},
	'cellphone' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Fax_Num' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Fax_Num' => {},
	'skype_name' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'TIN' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Password' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_Password' => {},
	'EMailAddress' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_EMailAddress' => {},
	'EMailAddress2' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'_EMailAddress2' => {},
	'language_pref' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Join_Date' => {'value'=>'', 'class'=>'n', 'req'=>1},
	'Vip_Date' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'status' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'level' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'stamp' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'operator' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'mail_option_lvl' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'Memo' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'oid' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'nspid' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'noperator' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'nstamp' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'nnotes' => {'value'=>'', 'class'=>'n', 'req'=>0},
	'ncoach' => {'value'=>'', 'class'=>'n', 'req'=>0}
);

###### If this is the first time through, there won't be any action parameter.
###### So, we'll just print the page.
unless ( $action )
{
	unless ($id)
	{
		Abort("The necessary parameters were not received.<br />Please close this window and try again.\n");
		Exit();
	}

	my $qry_end = '';
		
	if ($Ltype eq 'arclist')
	{
		if ( ArcList() == 0 )
		{
			Abort("No matching records on:<br />ID: $id");
		}
		Exit();
	}

###### the query end is the same, however the table the query works on is different
	elsif ($Ltype eq 'arc' || $Ltype eq 'oid')
	{
		$qry_end = "m.oid= ";
	}

	elsif ($id =~ /@/)
	{	###### looks like an email address
		$id = lc $id;
		$qry_end = "m.EMailAddress= ";
	}
	elsif ($id =~ /\D/)
	{		###### contains letters so it can't be an ID
		$id = uc $id;
		$qry_end = "m.alias= ";
	}
	else
	{
		$qry_end = "m.id= ";
	}

	$qry_end .= $db->quote($id);
	
	if ( Get_Data($qry_end) )
	{
		Create_controls();
		Do_Page();
	}

	Exit();
}

###### We'll only go past this point if there was an action param
Load_Params();

if ($action eq 'updatenspid')
{
	UpdateNspid();
	Exit();
}

Check_Required();

if ( $dover_flag )
{###### all required info was not submitted, so we'll redo the page with ommissions flagged
	Create_controls();
	Do_Page();
	Exit();
}

if ( $field{'EMailAddress'}->{'value'} && ! Mail::RFC822::Address::valid($field{'EMailAddress'}->{'value'}) )
{	###### email address doesn't fit standard patterns
	$dover_flag = 1;
	$dover_msg = $q->span({'-class'=>'r'}, "<br />Email address doesn't appear to be a valid address.<br />Please check it.</b><br />\n");
	$field{'EMailAddress'}->{'class'} = 'r';
}

if ( $field{'EMailAddress2'}->{'value'} )
{
	unless ( Mail::RFC822::Address::valid($field{'EMailAddress2'}->{'value'}) )
	{
		$dover_flag = 1;
		$dover_msg = $q->span({'-class'=>'r'}, "<br />Alternate email address doesn't appear to be a valid address.<br />Please check it.</b><br />\n");
		$field{'EMailAddress2'}->{'class'} = 'r';
	}
}

my ($match, $match2) = ();

###### if we have empty email address fields we won't bother checking for 'dupes'
#if ( defined $field{EMailAddress}{'value'} || defined $field{EMailAddress2}{'value'} )
if ( $field{'EMailAddress'}->{'value'} || $field{'EMailAddress2'}->{'value'} )
{
	($match, $match2) = $gs->Duplicate_Email_CK( $db, $id, $field{'EMailAddress'}->{'value'}, $field{'EMailAddress2'}->{'value'} );
}

if ( $match  && ! $override1)
{
	#Debug("id= $id<br />match= $match");

	$dover_flag = 1;
	$dover_msg .= $q->span({'-class'=>'r'}, "<br />$match is in use by another member.<br />
                                       Submit the form again to confirm that you wish to still use it.</b><br />\n");
	$o1 = qq#<input type="hidden" name="override1" value="1" />\n#;
}
else 
{
	$override1 = 1;
}

if ( $match2 && ! $override2)
{
	#Debug("match2= $match2");
	
	$dover_flag = 1;
	$dover_msg .= $q->span({'-class'=>'r'}, "<br />$match2 is in use by another member.<br />
                                       Submit the form again to confirm that you wish to still use it.</b><br />\n");
	$o2 = qq#<input type="hidden" name="override2" value="1" />\n#;
}
else
{
	$override2 = 1;
}

###### US State/Territory Verification Check if Country = US

my %S  = %State_List::STATE_CODES;

$badformat = 1;

# The state code is bad by default 

# warn $field{Country}{'value'},"\n";

if ( $field{'Country'}->{'value'} eq "US" )
{
  # warn "State Comparision Entered\n";

  while ( my $svalue = each(%S))
  {
   # warn "Value = ",$S{$svalue},"\n";

   if ( $S{$svalue} eq  $field{'State'}->{'value'} )
   {
    # warn "Hit! ",$S{$svalue}," = ",$field{State}{'value'},"\n";

    $badformat = 0;  # If we have a match then the format is Good!  
   }
  }
}

# warn "badformat is now ",$badformat,"\n";

if ( $badformat == 1 && $field{'Country'}->{'value'} eq "US" && $field{'State'}->{'value'})
{
 # warn "Setting dover flag\n";

 $dover_flag = 1;
 $dover_msg = $q->span({'-class'=>'r'}, "<br />",$q->param('State')," is not in the correct 2 character format. </b><br />\n");
}

###### email address problems we'll redo the page
if ( $dover_flag )
{
	Create_controls();
	Do_Page();
}
elsif ($action eq 'accept')
{
	DisallowCoopToMemberTransfers();
	my @vals;
###### lets upper case the first letter of name fields
	push (@vals, ucfirst $field{'secFirstName'}->{'value'});
	push (@vals, (ucfirst $field{'MDName'}->{'value'}, ucfirst $field{'secMDName'}->{'value'}));
	push (@vals, ucfirst $field{'secLastName'}->{'value'});

###### we need to lower case all our email addresses
###### and we don't want empties for values if it should be blank
	$field{'EMailAddress'}->{'value'} = lc( $field{'EMailAddress'}->{'value'} ) if $field{'EMailAddress'}->{'value'};
	$field{'EMailAddress2'}->{'value'} = lc( $field{'EMailAddress2'}->{'value'} ) if $field{'EMailAddress2'}->{'value'};

###### no empties here either
	push @vals, NullNotEmpty( $field{'language_pref'}->{'value'});

	push @vals, NullNotEmpty($field{'Address2'}->{'value'});

	push @vals, NullNotEmpty($field{'cellphone'}->{'value'});
	push @vals, NullNotEmpty($field{'other_contact_info'}->{'value'});

	push @vals, NullNotEmpty($field{'TIN'}->{'value'});
	push @vals, $field{'Memo'}->{'value'};

	$qry = "
UPDATE members
SET 
	join_date = '$field{Join_Date}->{'value'}',
	spid = '$field{spid}->{'value'}',
	membertype = '$field{'membertype'}->{'value'}',
	prefix = '$field{Prefix}->{'value'}',
	secprefix = '$field{secPrefix}->{'value'}',
	secfirstname = ?,
	mdname = ?,
	secmdname = ?,
	seclastname = ?,
	suffix = '$field{Suffix}->{'value'}',
	secsuffix = '$field{secSuffix}->{'value'}',
	language_pref= ?,
	address2 = ?,
	cellphone = ?,
	other_contact_info = ?,
	tin = ?,
	alias = '$field{Alias}->{'value'}',
	operator = '$creds->{'username'}',
	stamp = NOW(), 
	status = '$field{'status'}->{'value'}',
	mail_option_lvl = '$field{'mail_option_lvl'}->{'value'}',
	memo = ?";

	###### we can only pass dates if there are values, else the update fails
	if ($field{'Vip_Date'}->{'value'} gt '')
	{
		$qry .= ", VIP_Date = " . $db->quote($field{'Vip_Date'}->{'value'});
	}
	
	foreach (keys %field)
	{
		# for certain fields, we are carrying the original value in a like named parameter
		# ie. FirstName is carried in _FirstName
		# if both values are the same, then we do not need to update the value
		# this will help when trying to change the spid of 'ma' memberships in particular
		next unless /^_(.*)/;
#warn "1: $1 , _: $_";
		next if EmptyNotNull($field{$1}) eq EmptyNotNull($field{$_});
		$qry .= ", $1 = " . $db->quote($field{$1}->{'value'});
	}

	$qry .= " WHERE id = '$id'";
	my $sth = $db->prepare($qry);

	if ( $override1 && $override2 )
	{
		eval { $sth->execute(@vals) };
		if ($@)
		{
			$qry =~ s#\n#<br />#g;
			$qry .= "<br />Query vals:<br />-&gt; " . join('<br />-&gt; ', @vals) . "<br />";
			DB_error($qry);
			Exit()
		}
		else
		{
			$sth->finish;
		}
	}

	if (defined $DBI::errstr)
	{
		DB_error("There was an error updating this record.");
		Exit();
	}
	else
	{
###### Reprint the form
		if ( Get_Data("m.id = '$id'") > 0 )
		{
			Create_controls();
			$dover_msg = $q->span({'-class'=>'b'},"<br />Record successfully updated.");
			Do_Page()
		}
###### If Get_Data returns a zero it has already taken care of its error reporting so nothing else needs done
	}
}

$db->disconnect;
exit;

###### ###### ###### ###### ######

sub Abort
{
###### prints out a simple error message
	print
		$q->header,
		$q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8');
	print "<b>$_[0]</b>\n";
	print $q->end_html();
	Exit();
}

sub ArcList
{
	my $qry = "
		SELECT m.oid::TEXT,
			id,
			spid,
			admin_membertype_label(m.membertype) AS membertype,
			emailaddress,
			m.stamp,
			operator,
			memo
		FROM members_arc m
		WHERE ";

	###### modify our query depending on the type of input we are looking for
	if ($id =~ /\D/)
	{
		if ($id =~ /@/)
		{	###### looks like an email address
			$id = lc $id;
			$qry .= 'm.emailaddress = ?';
		}
		else
		{			###### must be an alias
			$id = uc $id;
			$qry .= 'm.alias = ?';
		}
	}else{				###### just digits must mean we're looking for an ID
		$qry .= "	m.id = ?
				UNION 
				SELECT '' AS oid,
					id,
					spid, 
					'N/A' AS membertype, 
					'' AS emailaddress, 
					stamp, 
					operator, 
					memo 
				FROM 	spid_arc
				WHERE 	id = $id ";
	}
	$qry .= " ORDER BY stamp DESC";

	my $sth = $db->prepare($qry);
	$sth->execute($id);
	$data = $sth->fetchrow_hashref;
	
	unless ($data)
	{
		$sth->finish;
		return 0;
	}
	
	my $bgcolor = '#ffffee';
	my $tblDATA = '';
	my $messages = '<br />';
	my $arc_link = '';
	my $id_link = '';

	# If it's a non-ID search, provide instruction for the links to the complete archive lists.
	if ($id =~ /\D/)
	{
		$messages = qq|<div class="message">Click the <span class="message" style="color: #0000ff;">ID</span> to pull up a possibly more complete list including any "SPID only" changes.</div><br />|;
	}
	do{
	###### lets fill in the blank table cells
		foreach (keys %{$data})
		{
			next if $_ eq 'oid';
			$data->{$_} = '&nbsp;' unless (defined $data->{$_} && $data->{$_} =~ '\w');
		}
		
		###### lets truncate the memo if it's too long
		###### they can bring up the whole record if necessary anyway
		if (length $data->{'memo'} > 100)
		{
			$data->{'memo'} = substr($data->{'memo'}, 0, 100);
				
			###### now we'll let 'em know it's been cut off
			$data->{'memo'} .= '<br /><i>MEMO TRUNCATED</i>';
		}

		###### since newlines in the data will not render properly in the html table
		###### we'll change them to html linebreaks
		$data->{'memo'} =~ s#\n#<br />#g;

		# When no oid is present, don't provide a link.
		$arc_link = (! $data->{'oid'}) ? 
					$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, 'N/A') :
					$q->td({'-valign'=>'top'}, $q->a({'-href'=>$q->url()."?Ltype=arc&id=$data->{'oid'}", '-target'=>'_blank'}, '*') );
		# When it's a non-ID search, provide a link to the complete archive list.
		$id_link = ($id =~ /\D/) ?
					$q->td({'-valign'=>'top'}, $q->a({'-href'=>$q->url()."?Ltype=arclist&id=$data->{'id'}", '-target'=>'_blank', '-class'=>'message'}, $data->{'id'}) ) :
					$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, $data->{'id'});

		$tblDATA .= $q->Tr({-bgcolor=>$bgcolor},
			$arc_link .
			$id_link .
			$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, $data->{'spid'}) .
			$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, $data->{'membertype'}) .
			$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, $data->{'emailaddress'}) .
			$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>'nowrap'}, $data->{'stamp'}) .
			$q->td({'-valign'=>'top', '-class'=>'data'}, $data->{'operator'}) .
			$q->td({'-valign'=>'top', '-class'=>'data'}, $data->{'memo'})
		);
		$bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	} while $data = $sth->fetchrow_hashref;
	$sth->finish;

	my $tmpl = $gs->GetObject($TMPL{$Ltype}) || die "Failed to load template: $TMPL{$Ltype}";

	$tmpl =~ s/%%data%%/$tblDATA/;
	$tmpl =~ s/%%messages%%/$messages/;
	$tmpl =~ s/%%id%%/$id/g;
	$tmpl =~ s/%%override1%%/$o1/g;
	$tmpl =~ s/%%override2%%/$o2/g;

	print $q->header, $tmpl;
	return 1;
}

sub cbo_Country
{
	my $sth = $db->prepare("SELECT * FROM tbl_countrycodes ORDER BY country_name ASC");
	$sth->execute;
	my (%labels, @values);

	###### we want 'US' to be at the top of the list so we'll push an extra one on after we push on a blank
	push (@values, '');
	push (@values, 'US');
	while (my($cntry, $code) = $sth->fetchrow_array)
	{
		$labels{$code} = $cntry;
		push (@values, $code);
	}
	$sth->finish;
	return $q->popup_menu(
		'-name'=>'Country',
		'-id'=>'country',
		'-class'=>'in',
		'-values'=>\@values,
		'-default'=>$data->{'country'},
		'-labels'=>\%labels,
		'-onchange'=>'getStates()');
}

sub cboLanguage
{
	my $default_value = $field{'language_pref'}{'value'} || '';
	my %labels = ();
	my @values = ();
	my $tmp = '';
	my $sth = $db->prepare("
		SELECT code2, description
		FROM 	language_codes
		WHERE 	active = 'true'
		ORDER BY description;");
	$sth->execute();
	$labels{''} = 'Select a Language';
	push @values, '';
	while ( $tmp = $sth->fetchrow_hashref )
	{
		$labels{$tmp->{'code2'}} = $tmp->{'description'};
		push @values, $tmp->{'code2'};
	}
	return $q->popup_menu(
		'-name'=>'language_pref',
		'-values'=>\@values,
		'-labels'=>\%labels,
		'-default'=>$default_value,
		'-force'=>1);
}

sub cbo_Mail_Option_Lvl
{
	my ($code, $label, %labels, @values);

	push (@values , '');
	my $sth = $db->prepare('SELECT code, label from "Mail_Options" ORDER BY code ASC');
	$sth->execute;
	while (($code, $label) = $sth->fetchrow_array)
	{
		$labels{$code} = $label;
		push (@values, $code);
	}
	$sth->finish;
	return $q->popup_menu(
		'-name'=>'mail_option_lvl',
		'-class'=>'in',
		'-values'=>\@values,
		'-default'=>$field{'mail_option_lvl'}->{'value'},
		'-force'=>1,
		'-labels'=>\%labels);
}

sub cbo_Membertype
{
	my ($code, $label, $staff_flag, %labels, @values);

	push (@values , '');
	my $sth = $db->prepare('SELECT code, label, staff_flag from "Member_Types" ORDER BY code ASC');
	$sth->execute;
	while (($code, $label, $staff_flag) = $sth->fetchrow_array)
	{
		if ($staff_flag || $field{'membertype'}->{'value'} eq $code)	# for items that should always be present, we will include them as well as if we match the membertype
		{
			$labels{$code} = "$label ($code)";
			push (@values, $code);
		}
	}
	$sth->finish;

	$CURRENT_MEMBERTYPE = $field{'membertype'}->{'value'};

	return $q->popup_menu(
		'-name'=>'membertype',
		'-class'=>'in',
		'-force'=>1,
		'-values'=>\@values,
		'-default'=>$field{'membertype'}->{'value'},
		'-onChange'=>"return _M(this);",
		'-labels'=>\%labels);
}

sub cboStateCodes
{
	# stuff an empty on the top just in case there is no default
	unshift(@StateCodes, '') if @StateCodes;
	
	return $q->popup_menu(
		'-id'=>'state_select',
		'-name'=>'State',
		'-class'=>'in',
		'-force'=>1,
		'-values'=>\@StateCodes,
		'-default'=>$field{'State'}->{'value'},
		'-labels'=>\%StateCodes,
		'-onchange'=>'turnOFF("state_text")');
}

sub Check_Required
{
	######	here we'll check required fields and assign new html classes as necessary
	foreach (keys %field)
	{
		if ( ($field{$_}->{'value'} || '') eq '' && $field{$_}->{'req'} == 1 )
		{	###### a required field is empty

		###### we have to treat the TIN differently depending on the country
			next if ($_ eq 'TIN' && $field{'Country'}->{'value'} ne 'US' && $field{'Country'}->{'value'} ne '');

			$field{$_}->{'class'} = 'r';	###### so we'll flag it
			$dover_flag=1;
			$dover_msg = $q->span({'-class'=>'r'}, "<br />Please fill in the required fields marked in <b>RED</b><br />\n");
		}
	}
}

		
sub Create_controls
{
	foreach (keys %field)
	{
		###### now we'll convert the 'value' to the needed form element with the value included
		if ($_ eq 'mail_option_lvl')
		{
			$field{$_}->{'value'} = cbo_Mail_Option_Lvl();
		}
		elsif ($_ eq 'language_pref')
		{
			$field{$_}->{'value'} = cboLanguage();
		}
		elsif ($_ =~ /Prefix/)
		{
			$field{$_}->{'value'} = $q->popup_menu(
				'-name'=>$_,
				'-class'=>'in',
				'-default'=> $field{$_}->{'value'},
				'-values'=>['', 'Mr.', 'Mrs.', 'Ms.', 'Miss', 'Dr.']);
		}
		elsif ($_ =~ /Suffix/)
		{
			$field{$_}->{'value'} = $q->popup_menu(
				'-name'=>$_,
				'-class'=>'in',
				'-default'=> $field{$_}->{'value'},
				'-values'=>['', 'Sr', 'Jr', 'III', 'IV', 'V', 'Phd.']);
		}
		elsif ($_ =~ /status/)
		{	###### not to be confused with any other labels which include 'status'
			$field{$_}->{'value'} = $q->popup_menu(
				'-name'=>$_,
				'-class'=>'in',
				'-default'=> $field{$_}->{'value'},
				'-values'=>['0', '1'],
				'-labels'=>{'0'=>'OFF', '1'=>'Normal'});
		}
	}
	
	# Ok not exactly a control creation, but a data collection creation for the states control
	if ($field{'Country'}->{'value'})
	{
		my $sth = $db->prepare('SELECT code, name FROM state_codes WHERE country= ? ORDER BY name');
		$sth->execute($field{'Country'}->{'value'});
		while (my $tmp = $sth->fetchrow_hashref)
		{
			$StateCodes{$tmp->{'code'}} = $tmp->{'name'};
			push(@StateCodes, $tmp->{'code'});
		}
	}
}

sub Create_hidden
{
	my $hid;

	###### we need to pass all our parameters that have values except the action param
	foreach ( $q->param() )
	{
		$hid .= $q->hidden($_) if ($_ ne 'action');
	}
	return $hid;
}

sub DB_error
{
	print $q->header,
		$q->start_html('-title'=>'Database Error!', '-bgcolor'=>'#ffffff');
	print "There has been a database error. The problem and the specific error are shown below.<br />\n";
	print "<b>Problem:</b> $_[0]<br /><b>Error:</b> $DBI::errstr\n";
	print $q->end_html();
}

sub Debug
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff');
	print "$_[0]\n";

	print $q->end_html;
}

sub DisallowCoopToMemberTransfers
{
	my ($coop_id, $mtype) = $db->selectrow_array('
		SELECT m.spid, m.membertype
		FROM members m
		WHERE m.id= ?', undef, $field{'id'}->{'value'});
	die "Failed to obtain the spid for: $field{'id'}->{'value'}" unless $coop_id;

	# they are always wanting to transfer merchants who come under 01 when they should be under some VIP
	return if $mtype eq 'ma';

	$coop_id = IsCoop($coop_id);
	
	# if we are not sponsored by a coop, we are done here
	return unless $coop_id;

	# if the recipient is a coop, then we are OK
	$coop_id = IsCoop( $field{'spid'}->{'value'} );
	return if $coop_id;

	# is the transferee a shopper and the recipient a pool under the sponsorship of a Coop?
	my $rec = $db->selectrow_hashref('SELECT spid, membertype FROM members WHERE id= ?', undef, $field{'spid'}->{'value'}) ||
		die "Failed to obtain member record for: $field{'spid'}->{'value'}";
	return if $rec->{'membertype'} eq 'mp' && IsCoop($rec->{'spid'}) && $field{'membertype'}->{'value'} eq 's';

#	die "You must use the Co-Op Member Transfer interface to perform a Coop transfer\n" unless $coop_id;
}

sub Do_Page
{
	my $TT = Template->new();
	my $hidden = '';	# = Create_hidden();
	my $tmpl = $gs->GetObject($TMPL{$Ltype}) || die "Failed to load template: $TMPL{$Ltype}";

	$tmpl =~ s/%%message%%/$dover_msg/;
	$tmpl =~ s/%%hidden%%/$hidden/;
	$tmpl =~ s/%%url%%/$q->url()/ge;
	foreach my $key (keys %field)
	{
		###### since we seem to get unit'd errs frequently
		###### we'll do this
		$tmpl =~ s/%%$key%%/((defined $field{$key}->{'value'} && $field{$key}->{'value'} gt '') ? $field{$key}->{'value'} : '')/eg;
		
		$tmpl =~ s/%%class_$key%%/$field{$key}->{'class'}/g;
	}

	###### this one covers the email address inserted into the removal link
	$tmpl =~ s#%%escaped_emailaddress%%#CGI::escape($field{'EMailAddress'}->{'value'})#e;

	$tmpl =~ s#%%oid%%#$data->{'oid'}#;
	$tmpl =~ s/%%current_membertype%%/$CURRENT_MEMBERTYPE/g;

	$tmpl =~ s/%%override1%%/$o1/g;
	$tmpl =~ s/%%override2%%/$o2/g;

	my $xag = xag_membertype();
	$tmpl =~ s/%%affinity%%/$xag/;

	print $q->header('-expires'=>'now', '-charset'=>'utf-8');
	$TT->process(\$tmpl, {
		'field'=>\%field,
		'cbo_Membertype' => cbo_Membertype(),
		'cbo_Country' => cbo_Country(),
		'cboStateCodes' => cboStateCodes(),
		'StateCodes'=>\%StateCodes,
		'G_S' => $gs
	}) || print $TT->error;
}

sub EmptyNotNull
{
	return defined $_[0] ? $_[0] : '';
}

sub Err
{
        print $q->header, $q->start_html;
        print $q->p($_[0]);
        print $q->end_html;
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}
	
sub Get_Data
{
	$qry = "
		SELECT lpad(m.id::TEXT,7,'0') AS id,
			lpad(m.spid::TEXT,7,'0') AS spid,
			membertype,
			Prefix,
			FirstName,
			MDName,
			LastName,
			Suffix,
			secPrefix,
			secFirstName,
			secMDName,
			secLastName,
			secSuffix,
			Company_Name,
			EMailAddress,
			EMailAddress2,
			language_pref,
			Alias,
			Join_date,
			Address1,
			Address2,
			City,
			m.state,
			sc.name AS state_name,
			PostalCode,
			m.country,
			cellphone,
			other_contact_info,
			Phone,
			Fax_Num,
			VIP_Date,
			Password,
			stamp::TIMESTAMP(0) AS Stamp,
			m.Memo,
			m.status,
			mail_option_lvl,
			TIN,
			m.operator,
			m.oid
		FROM $TBL{$Ltype} m
		LEFT JOIN state_codes sc
			ON sc.code=m.state
			AND sc.country=m.country
		WHERE $_[0]";

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute;

	if ($rv eq '0E0' )
	{	###### there was no matching ID
		my $msg = "No matching record on:<br />";
		$msg .= ($Ltype eq 'oid') ? ("Unique Number: $id") : ("ID: $id");
		Abort($msg);
		return 0;
	}
	elsif ($rv > 1)
	{
		return Live_List($sth);
	}
	elsif ( Load_Data($sth) )
	{
		###### there was a DB error
		Abort("There was a database error: $DBI::errstr\n");
		return 0;
	}

	return 1;
}

sub IsCoop
{
	return $db->selectrow_array("
		SELECT coop_id FROM coop.coops WHERE house_acct=TRUE AND coop_id= ?", undef, $_[0]) || '';
}

sub Live_List
{
	###### this is not the best way to handle this, but its a hack that will work for now
	my $sth = shift;

	###### we already have a global statement handle waiting with data
	$data = $sth->fetchrow_hashref;
	
	my $bgcolor = '#ffffee';
	my $tblDATA = '';

	do{
	###### lets fill in the blank table cells
		foreach (keys %{$data})
		{
			unless ( $data->{$_} )
			{
				$data->{$_} = '&nbsp;';
			}			
		}
		
		###### lets truncate the memo if it's too long
		###### they can bring up the whole record if necessary anyway
		if (length $data->{'memo'} > 100)
		{
			$data->{'memo'} = substr($data->{'memo'}, 0, 100);
				
			###### now we'll let 'em know it's been cut off
			$data->{'memo'} .= '<br /><i>MEMO TRUNCATED</i>';
		}

		###### since newlines in the data will not render porperly in the html table
		###### we'll change them to html linebreaks
		$data->{memo} =~ s#\n#<br />#g;

		$tblDATA .= $q->Tr({-bgcolor=>$bgcolor},
			$q->td({'-valign'=>'top', '-class'=>'q'}, $q->a({'-href'=>$q->url()."?Ltype=liv&id=$data->{'id'}", '-target'=>'_blank'}, $data->{'id'}) ) .
			$q->td({'-valign'=>'top', '-class'=>'q'}, $data->{'alias'}) .
			$q->td({'-valign'=>'top', '-class'=>'q'}, $data->{'spid'}) .
			$q->td({'-valign'=>'top', '-class'=>'q'}, $data->{'membertype'}) .
			$q->td({'-valign'=>'top', '-class'=>'q', '-nowrap'=>'nowrap'}, $data->{'emailaddress'}) .
			$q->td({'-valign'=>'top', '-class'=>'q', '-nowrap'=>'nowrap'}, $data->{'stamp'}) .
			$q->td({'-valign'=>'top', '-class'=>'q', '-nowrap'=>'nowrap'}, $data->{'operator'}) .
			$q->td({'-valign'=>'top', '-class'=>'q'}, $data->{'memo'})
		);

		$bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');

	}while $data = $sth->fetchrow_hashref;

	$sth->finish;

	my $tmpl = $gs->GetObject($TMPL{'livlist'}) || die "Failed to load template: $TMPL{'livlist'}";
	$tmpl =~ s/%%data%%/$tblDATA/;

	print $q->header(-charset=>'utf-8'), $tmpl;
	return 0;
}
		
sub Load_Data
{
	my $sth = shift;
	$data = $sth->fetchrow_hashref;

	###### this procedure presumes that the field keys are identical to the passed parameters
	foreach my $key (keys %field)
	{
		$field{$key}->{'value'} = defined $data->{lc($key)} ? $q->escapeHTML( $data->{lc($key)} ) : '';
	}

	###### eliminate the whitespace from data importation
#	$field{Phone_AC}{'value'} =~ s/\s//g;

	###### Now we need to get the level for this member if they are a VIP and we are not searching archives
	if ( $field{'membertype'}->{'value'} =~ /v/i && $Ltype ne 'arc' )
	{
		my $qry = "	SELECT	
				--	nc.a_level,
					lv.Lname
				FROM 	a_level nc,
					level_values lv
				WHERE 	nc.A_Level = lv.Lvalue
				AND 	nc.Id = '$field{'id'}{'value'}'";

		my $sth = $db->prepare($qry);
		$sth->execute;
		$field{'level'}->{'value'} = $sth->fetchrow_array;
		$sth->finish;
	}
	$field{'level'}->{'value'} ||= '';
	
	# get the network spid and network if we are not searching archives.. also some Trial Partner into if available
	if ( $Ltype ne 'arc' )
	{
		($field{'nspid'}->{'value'}, $field{'noperator'}->{'value'}, $field{'nstamp'}->{'value'}, $field{'nnotes'}->{'value'}) = $db->selectrow_array("
			SELECT LPAD(COALESCE(ns.spid,m.spid)::TEXT,7,'0') AS spid, ns.operator, ns.stamp, ns.memo
			FROM members m
			LEFT JOIN network.spids ns
				ON ns.id=m.id
			WHERE m.id= ?
		", undef, $field{'id'}->{'value'});
		
		($field{'ncoach'}->{'value'}) = $db->selectrow_array('SELECT upline FROM my_coach WHERE id= ?', undef, $field{'id'}->{'value'}) || '';
		($field{'pnsid'}->{'value'}) = $db->selectrow_array('SELECT pnsid FROM od.pool WHERE id= ?', undef, $field{'id'}->{'value'});
		
		$field{'position'} = $db->selectrow_hashref('SELECT seq, pnsid FROM mviews.positions_freshest WHERE id= ?', undef, $field{'id'}->{'value'});
		
		# do they have any sort of upgrade_tmp record in place?
		($field{'upgrade_tmp'})= $db->selectrow_array('SELECT id FROM upgrade_tmp WHERE id= ?', undef, $field{'id'}->{'value'});
	}
	
	# we need to know if this member is in a Coop so that we can disable spid changes using this interface
	$field{'coop_id'}->{'value'} = IsCoop($field{'spid'}->{'value'});
	($DBI::err && $DBI::err > 0) ? return 1 : return;
}

sub Load_Params
{
	###### populating our field array hash values
	###### this loop depends on the %field hash keys being identical to the passed parameter names
	foreach (keys %field)
	{
		$field{$_}->{'value'} = $gs->Prepare_UTF8(NullNotEmpty( $q->param($_) || $q->param(lc $_) ));

		###### let's filter for extraneous characters (this may pose a problem with other character sets)
		###### we'll be more restrictive on the name fields
		if ($_ eq 'Company_Name')
		{
			# leave quotes and commas
			$field{$_}->{'value'} =~ s/\t|\r|\n|\f|\[M|;|:|\+|=|_//g;
		}
		elsif ($_ =~ m/name/)
		{
			$field{$_}->{'value'} =~ s/,|\t|\r|\n|\f|\[M|'|"|;|:|\+|=|_//g;
		}

		###### we can skip the memo field
		elsif ($_ =~ /memo/i)
		{
			next;
		}
		elsif ($field{$_}->{'value'})
		{
			$field{$_}->{'value'} =~ s/\t|\r|\n|\f|\[M|"|;|:|\+|=//g;
		}
	}
}

sub NullNotEmpty
{
		return (defined $_[0] && $_[0] gt '') ? $_[0] : undef;
}

sub UpdateNspid
{
	# an empty parameter means they must mean to delete the value
	unless ($field{'nspid'}->{'value'})
	{
		$db->do('DELETE FROM network.spids WHERE id= ?', undef, $field{'id'}->{'value'});
	}
	else
	{
		# convert an alias to an ID
		my $spid = $field{'nspid'}->{'value'};
		($spid) = $db->selectrow_array('SELECT id FROM members WHERE alias= ?', undef, uc($spid)) if $spid =~ /\D/;
		die "Cannot identify a membership with ID: $field{'nspid'}->{'value'}" unless $spid;
		
		my $rv = $db->do("UPDATE network.spids SET spid= $spid, memo= ? WHERE id= ?", undef, $field{'nnotes'}->{'value'}, $field{'id'}->{'value'});
		$db->do("INSERT INTO network.spids (id, spid, memo) VALUES (?, $spid, ?)", undef, $field{'id'}->{'value'}, $field{'nnotes'}->{'value'}) if $rv ne '1';
	}
	
	if ( Get_Data('m.id=' . $db->quote($field{'id'}->{'value'})) )
	{
		Create_controls();
		Do_Page();
	}
}

sub xag_membertype()
{
	if ($data->{'membertype'} eq 'xag'){
		return '<a onclick="window.open(this.href);return false;" href="https://www.clubshop.com/cgi/appx.cgi//admin_cfrm?next_step=step77;id=' . $data->{'id'} .
			'"><b>Confirm</b></a>&nbsp;&nbsp;' .
			'<a onclick="window.open(this.href);return false;" href="https://www.clubshop.com/cgi/appx.cgi//admin_cfrm?next_step=step66;id=' . $data->{'id'} .
                	'"><b>Delete</b></a>&nbsp;&nbsp;';
	}else{ return ''; }
}

###### 11/08/01 added letter case adjustments for certain fields (like email address to keep them lower case)
###### 12/27/01 modified to handle record lookups from members_arc, made the database connection stuff
###### work from the global module
###### added handling for W. European characters
###### added searching by members 'Unique' number, oid
###### 06/19/02 altered wording on a no match condition for a unique number
###### 06/20/02 made the Partner date update work even when a VIP date is present
###### 06/24/02 fixed a minor CGI oo syntax error & added pull-in for oid
###### 08/12/12 added functionality for searching the archives directly by ID or alias
###### (for cases where the original record has been deleted from the live)
###### 08/13/02 revised the query end formation to use DB quote
###### 10/03/02 removed the current_membertype stuff from the arclist report
###### Stephen added email dupe checking in Dec. 02
###### 01/15/03 removed two script generated links and went to hardcoded ones in the template
###### 01/24/03 Added US State / Territory verification process to enforce normalization of US State names
###### 04/07/03 revamped some queries for easier readability
###### 05/01/03 changed the behaviour of Stephen's logic to fail a US record update
###### on the basis of an improperly formatted State value when a value wasn't even present
###### 05/27/03 Added QuickLink for ClubBucks Card Info to related file:
######			'/home/httpd/cgi-templates/Html/admin/minfo.html'
###### 08/12/03 Commented out the %%sponsor_record%% and %%archive_records%% template substitutions.
###### 09/03/03 removed the emailaddress requirement and put the -w flag in
###### started fixing uninitialized var errors
###### decided to remove the -w flag for now to squelch the errors
###### 09/05/03 added handling for empty email addresses since now they are not required
###### also dropped some old routines and incorporated some global ones (email check & dupe email check)
###### also removed the Partner date update portion of the query
###### 10/27/03 revised to look at network calcs instead of vipcalcs
###### 12/09/03 added escaping of the email address for use in the removal link
###### 12/22/03 added handling for multiple matches on live records
###### 12/24/03 fixed some uninit'd vars uncovered by the -w switch
###### 02/04/04 employed a remedy for unit'd var errs in Do_Page
###### also commented out the selection of the Partner date in one query in anticipation
###### of dropping that column
###### 02/11/04 still fixing unit'd var errors on oid - it seems that on an incompleted
###### update, the only data available to fill out the form comes from the submitted
###### form, so the oid needed to be carried
###### 04/29/04 went to the Mail::RFC822::Address::valid email address check
###### it seems awfully loose, but I guess they are technically valid :-?
###### 06/24/04 Changed the template handling from opening files in the
######		 cgi-templates directory to the Template Management System.
######		Modified the ArcList subroutine to display the spid_arc archives in addition to 
######		 the members_arc archives.
###### 06/29/04 Added the member ID to the ArcList in order to provide a link to a possibly
######		more complete list which may include spid change archives.
###### 09/01/04 Added the language_pref references to the code, including cboLanguage.
# 01/26/06 added the charset to the header call
# 02/07/06 adjusted the substitution of field vals to recognize a value 'zero' as a legit value
# 11/27/06 Added controls to confirm/delete pending AGs
###### 12/17/07 phased out partner_date and phone_ac, began phase in of cellphone and skype_name
# 04/14/09 functionally, changed the behavior to display illegal characters in the interface
###### introduced some code to eliminate having some fields set to empty if the interface field is empty
###### instead, we will set the DB field to NULL
###### 11/04/10 fixed the broken update of the language_pref value
###### 03/11 various adjustments to the updating code to allow changes of sponsor on "ma" memberships to not invoke the DB level constraint on data changes to these types of memberships
###### basically, we have made it so some fields are simply not touched in the update statement unless there is actually a data change
###### 08/03/12 added the use of Template Toolkit so that the drop down menu at the main interface could be dynamically altered at the template level
###### 08/07/13 added the integration of the network coach
###### 11/13/13 added pulling the Trial Partner pnsid
# 05/12/14 broke the multi-statement apart in UpdateNspid() so that it works with more recent versions of DBD::Pg
# 06/17/14 revised the SQL in UpdateNspid to not delete since we now have a foreign key constraint on that table
# 10/23/14 revised cbo_Membertype to not show certain entries that are flagged appropriately in the DB
# 04/22/15 added cboStateCodes and stuff to support that in here and in the templates
# 06/05/15 wove in the use of the cs_admin class for authentication instead of the older cookies - also pulled in an upgrade_tmp id if available
