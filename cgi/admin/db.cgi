#!/usr/bin/perl -w
###### db.cgi
###### last modified: 12/17/2015	Bill MacArthur

no warnings 'once';
use CGI;;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
use ADMIN_DB;
require cs_admin;

###### Globals
my (@cookie, $TEXT, $db, $operator, $pwd, $creds);
my $q = new CGI;
$q->charset('utf-8');
my $gs = new G_S;
my $cs = cs_admin->new({'cgi'=>$q});
my $script_name = $q->script_name;
my $main = 10472;	# The CGI object template.

unless ( $q->https )
{
	my $loc = $q->self_url;
	$loc =~ s/^http/https/;	
	print $q->redirect($loc);
}

if ($q->cookie('cs_admin'))
{
	$creds = $cs->authenticate($q->cookie('cs_admin'), 1);
}

# $creds will come back undef on authentication failure
if ($creds)
{
	$operator = $creds->{'username'};
	$pwd = $creds->{'password'}
}
else
{
	# we must be logging in
	$operator = $q->param('operator');
	$pwd = $q->param('pwd');
}
#my $operator = $q->cookie('operator') || $q->param('operator');
#my $pwd = $q->cookie('pwd') || $q->param('pwd');

unless ($operator)	###### there are no cookies or params so lets just do the login screen
{
	Login();
}
elsif ($q->param('logout'))
{
	Login('logout');
}
elsif (Validate() < 1)
{
	###### we don't have the right cookies or login information so lets leave
	print
		$q->header('-expires'=>'-1d'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("Incorrect login.<br />Back up and try again"),
		$q->end_html();
}
elsif (! Authorize())
{
	print $q->header('-expires'=>'-1d'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("You are not authorized for access from your current IP address"),
		$q->end_html();
}
elsif ($q->path_info && $q->path_info eq '/login_as')
{
	my $id = uc $q->param('id');
	if ((! defined $id) || $id eq '')
	{
		print $q->header,
			$q->start_html,
			qq|<html><head><title>Login As</title></head><body>
			<script type="text/javascript">
			{self.close()}
			</script></body></html>|;
	}
	elsif ($id == 0)
	{
		my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
		$meminfo->{'alias'} ||= 'not logged in';

		print $q->header,
			$q->start_html,
			qq|<script type="text/javascript">
			{
			var id=window.prompt('Current member login: $meminfo->{'alias'}\\n\\nLogin as: ', '1');
			if (id == null){ self.close() }
			else{location.replace('$script_name/login_as?id=' + id)}
			}
			</script></body></html>|;
	}
	else
	{

		my $qry = "
			SELECT alias,
				CASE
					WHEN password ISNULL OR password <= ' ' THEN oid::VARCHAR
					ELSE password
				END AS password
			FROM members WHERE ";
		$qry .= ($id =~ /\D/) ? 'alias= ?' : 'id= ?';
		my $sth = $db->prepare($qry);
		$sth->execute($id);
		my $m = $sth->fetchrow_hashref;
		$sth->finish;
		unless ($m->{'alias'})
		{
			print $q->header('text/plain'), "$id not found";
		}
		else
		{
			my $loc = "/LOGIN?credential_0=$m->{'alias'}&credential_1=$m->{'password'}";
			$loc .= "&destination=$script_name/login_as";

			print $q->header, $q->start_html,
			qq|<script type="text/javascript">
			javascript:location.replace('$loc')</script></body></html>|;
		}
	}
}
else
{	###### lets create the necessary cookies in case they don't have 'em
#	$cookie[0] = $q->cookie('-name'=>'operator', '-value'=>$operator, '-path'=>'/', '-secure'=>1, '-expires'=>'+12h');
#	$cookie[1] = $q->cookie('-name'=>'pwd', '-value'=>$pwd, '-path'=>'/', '-secure'=>1, '-expires'=>'+12h');
# ideally we would have a methodology where the cookie is a session cookie with an expiration mechanism for limited shelf life, but at this moment we do not
	$cookie[0] = $q->cookie('-name'=>'operator', '-value'=>$operator, '-path'=>'/', '-secure'=>1);
	$cookie[1] = $q->cookie('-name'=>'pwd', '-value'=>$pwd, '-path'=>'/', '-secure'=>1);
	$cookie[2] = $cs->CreateCookie($operator, $pwd);
	print $q->header('-cookie'=>\@cookie, '-expires'=>'-1d');
	my $TEXT = $gs->Get_Object($db, $main) || die "Couldn't open template $main";
	$TEXT =~ s/%%AppList%%/AppList()/e;
	$TEXT =~ s/%%username%%/$operator/;
	$TEXT =~ s/%%password%%/$pwd/;
	print $TEXT;
}

$db->disconnect if defined $db;
exit;

sub AppList
{
	my %map = %ADMIN_DB::AppMap;
	my $ctl = $q->popup_menu(
		'-id'=>'applist',
		'-name'=> 'rc',
		'-class'=> 'in',
		'-values'=> [AppMapSort()],
		'-labels'=> {map {$_, $map{$_}->{'label'}} AppMapSort()}
	);
}

sub AppMapSort
{
	my %map = %ADMIN_DB::AppMap;
#	return sort {($map{$a}->{'label'} cmp $map{$b}->{'label'}) || N2Z($map{$a}->{sort}) <=> N2Z($map{$b}->{sort})} keys %map;
	###### first sort alphabetically on the labels
	my @list = sort {$map{$a}->{'label'} cmp $map{$b}->{'label'}} keys %map;
	###### then on the sort
	return sort {N2Z($map{$b}->{sort}) <=> N2Z($map{$a}->{sort})} @list;
}

sub Authorize
{
	return 1 if $q->remote_addr eq '208.78.129.189';
	my ($rv) = $db->selectrow_array('
		SELECT remote_access
		FROM odb.odb_users
		WHERE odb.odb_users.rolname= ?', undef, $operator);
	return $rv;
}

sub Login
{
	my $flag = shift || '';
	if ($flag eq 'logout')
	{
		$cookie[0] = $q->cookie('-name'=>'operator', '-value'=>'', '-path'=>'/', '-expires'=>'-1d', '-secure'=>1);
		$cookie[1] = $q->cookie('-name'=>'pwd', '-value'=>'', '-path'=>'/', '-expires'=>'-1d', '-secure'=>1);
		$cookie[2] = $q->cookie('-name'=>'cs_admin', '-value'=>'', '-path'=>'/', '-expires'=>'-1d', '-secure'=>1);
		$cookie[3] = $q->cookie('-name'=>'PHPASESS', '-value'=>'', '-path'=>'/', '-expires'=>'-1d', '-secure'=>1);
		print $q->header('-cookie'=>\@cookie, '-expires'=>'-1d');
	}
	else
	{
		print $q->header('-expires'=>'-1d');
	}
	
	print $q->start_html('-bgcolor'=>'#eeffee',
		'-onload'=>'document.forms[0].operator.focus()',
		'-title'=>'DHSC Database Login');
	print $q->h3("DHSC Database Login");
	print $q->start_form('-action'=>'/cgi/admin/db.cgi');
	print $q->textfield('-name'=>'operator'), $q->b(' Username<br />');
	print $q->password_field('-name'=>'pwd'), $q->b(' Password<br />'), $q->submit();
	print $q->end_form(), $q->end_html();
}

sub N2Z
{
	###### Null to Zero
	return $_[0] ? $_[0] : 0;
}

sub Validate
{
	$db = ADMIN_DB::DB_Connect( 'db.cgi', $operator, $pwd ) || return 0;
	$db->{'RaiseError'} = 1;
	return 1;
}

###### 03/22/2001	added .ini support
###### 05/03/2001	added expanded cookie parameters so that other admin scripts could use them
###### 07/27/01	converted to OO CGI
###### 09/12/01	made the script aware & require SSL
###### 03/19/02 removed the domain specification in the admin cookie and implemented the global DB_Connect mech.
###### 10/14/02 made the admin cookies only passable in a secure session
###### the previous entry was negated sometime probably due to accessing insecure scripts
###### we should change that asap
###### 11/13/03 added the logout option
###### 06/09/04 added the login_as feature
###### 06/23/04 Moved the html template definition into variable $main for easier access.
###### 07/28/05 Moved the html template into the CGI objects table for even easier access.
###### 04/17/06 made the cookies valid only in an SSL session
# 07/10/06 added the Authorize function
###### 07/06/10 added the substitution of the username/password in the main template
# 04/02/15 simply added an id attribute to the AppList dropdown, also added charset to our CGI object
# 12/17/15 made this thing actually use the cs_admin authentication