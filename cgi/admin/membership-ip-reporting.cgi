#!/usr/bin/perl -w
# membership-ip-reporting.cgi
# a interface for performing fraud analysis of logins and upgrades
###### 
use strict;
use Socket;
use CGI;
use Date::Simple ('date', 'today');
use Date::Range;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my ($db) = ();
###### we will not allow admin access for update use (we have an in-house interface for that)
if ( $operator && $pwd )
{
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
	$db->{RaiseError} = 1;
}
else
{
	die 'You must be logged in to use this interface.';
}

my $search_type = $q->param('search_type');
my $group_min_size = $q->param('minimum_group_size') || 2;

my $Errors = '';	# we'll put errors that we want displayed at the top of the interface in here
my $Results = ();	# where we will put the DB result set from a query
my $end_date = '';	# this will be assigned the value of today

my @dates = GetDates();
#print $q->header('text/plain'), @dates;
unless ($search_type){
	###### our basic interface
	Report();
}
elsif ($search_type eq 'all'){
	LookupAll();
	Report();
}
elsif ($search_type eq 'id'){
	LookupById();
	Report();
}
elsif ($search_type eq 'ip'){
	LookupByIpAddr();
	Report();
}
elsif ($search_type eq 'host'){
	my $sv = LoadSearch_Value();
	print $q->header('text/plain');
	unless ($sv =~ /^\d+\.\d+\.\d+\.\d+$/){
		print "Invalid IP address submitted: $sv";
	}
	my $s = gethostbyaddr(inet_aton($sv),AF_INET);
	print $s || 'No domain info returned';
}
else {
	die "Invalid search_type parameter: $search_type";
}

$db->disconnect;
exit;

sub GetDates
{
	$end_date = today();
	my $start_date = today() - 120;
	my $range = Date::Range->new($start_date, $end_date);
	return reverse $range->dates;
}

sub LoadSearch_Value
{
	my $sv = $q->param('search_value') || die "Failed to receive a search_value parameter";
	$sv =~ s/^\s*|\s*$//g;
	return $sv;
}

sub LookupAll
{
#	my $sth = $db->prepare(q#
#		SELECT DISTINCT mlo.ipaddr, mlo.id, '' AS stamp, m.firstname ||' '|| m.lastname AS name
#		FROM members m, membership_logins mlo,
#		(
#			SELECT COUNT(*) AS CNT, ipaddr
#			FROM (
#				SELECT DISTINCT ml.ipaddr, ml.id
#				FROM membership_logins ml
#				LEFT JOIN ip_reporting_exclusions irx
#					ON irx.ipaddr=ml.ipaddr
#				WHERE ml.stamp::DATE BETWEEN ? AND ?
#				AND irx.ipaddr IS NULL
#				AND ml.id > 1
#			) AS itmp
#			GROUP BY ipaddr
#			HAVING COUNT(*) >= 1
#		) AS otmp
#		WHERE m.id=mlo.id 
#		AND mlo.stamp::DATE BETWEEN ? AND ?
#		AND otmp.ipaddr=mlo.ipaddr
#		AND mlo.id > 1;
#		
#	#);
	my $sth = $db->prepare(q#
SELECT DISTINCT tmp2.ipaddr, '' AS stamp, m.id, m.firstname ||' '|| m.lastname AS name, tmp2.login_type
FROM members m,
(	SELECT otmp.ipaddr,
		CASE WHEN mlo.id IS NOT NULL THEN mlo.id WHEN ut.id IS NOT NULL THEN ut.id ELSE uta.id END AS id,
		CASE WHEN mlo.id IS NOT NULL THEN 'ml'::TEXT ELSE 'ut'::TEXT END AS login_type
	FROM

	(
		SELECT COUNT(*) AS CNT, ipaddr
		FROM (
			SELECT DISTINCT tmp0.ipaddr, tmp0.id FROM
			( 	SELECT ipaddr,id FROM membership_logins WHERE stamp::DATE BETWEEN ? AND ?
				UNION
				SELECT ipaddr, id FROM upgrade_tmp WHERE stamp::DATE BETWEEN ? AND ?
				UNION
				SELECT ipaddr, id FROM upgrade_tmp_arc WHERE stamp::DATE BETWEEN ? AND ?
			) AS tmp0
			LEFT JOIN ip_reporting_exclusions irx
				ON irx.ipaddr=tmp0.ipaddr
			WHERE irx.ipaddr IS NULL
		) AS itmp
		GROUP BY ipaddr
		HAVING COUNT(*) >= ?
	) AS otmp
	LEFT JOIN membership_logins mlo
		ON otmp.ipaddr=mlo.ipaddr
		AND mlo.stamp::DATE BETWEEN ? AND ?
	LEFT JOIN upgrade_tmp ut
		ON otmp.ipaddr=ut.ipaddr
		AND ut.stamp::DATE BETWEEN ? AND ?
	LEFT JOIN upgrade_tmp_arc uta
		ON otmp.ipaddr=uta.ipaddr
		AND uta.stamp::DATE BETWEEN ? AND ?
) AS tmp2
WHERE m.id=tmp2.id

	#);
	$sth->execute(
		$q->param('start_date'), $q->param('end_date'),
		$q->param('start_date'), $q->param('end_date'),
		$q->param('start_date'), $q->param('end_date'),
		$group_min_size,
		$q->param('start_date'), $q->param('end_date'),
		$q->param('start_date'), $q->param('end_date'),
		$q->param('start_date'), $q->param('end_date')
	);
	RunQuery($sth);
}

sub LookupById
{
	my $sv = LoadSearch_Value();
	($sv) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, uc($sv)) if $sv =~ m/\D/;
	die "Failed to find a membership matching: " . $q->param('search_value') unless $sv;

	my $sth = $db->prepare(q#
		SELECT mlo.ipaddr, mlo.id, substr(mlo.stamp::TEXT,0,20) AS stamp, m.firstname ||' '|| m.lastname AS name, mlo.login_type
		FROM members m,
		(
			SELECT id, stamp, ipaddr, 'ml'::TEXT AS login_type FROM membership_logins WHERE id=? AND stamp::DATE BETWEEN ? AND ?
			UNION
			SELECT id, stamp, ipaddr, 'ug'::TEXT AS login_type FROM upgrade_tmp WHERE id=? AND stamp::DATE BETWEEN ? AND ?
			UNION
			SELECT id, stamp, ipaddr, 'ug'::TEXT AS login_type FROM upgrade_tmp_arc WHERE id=? AND stamp::DATE BETWEEN ? AND ?
		) AS mlo
		WHERE m.id=mlo.id 
		ORDER BY mlo.stamp
	#);
	$sth->execute(
		$sv, $q->param('start_date'), $q->param('end_date'),
		$sv, $q->param('start_date'), $q->param('end_date'),
		$sv, $q->param('start_date'), $q->param('end_date')
	);
	RunQuery($sth);
}

sub LookupByIpAddr
{
	my $sv = LoadSearch_Value();
	die "Invalid IP address submitted: $sv" unless $sv =~ /^\d+\.\d+\.\d+\.\d+$/;
	my $sth = $db->prepare(q#
		SELECT mlo.ipaddr, mlo.id, substr(mlo.stamp::TEXT,0,20) AS stamp, m.firstname ||' '|| m.lastname AS name, mlo.login_type
		FROM members m,
		(
			SELECT id, stamp, ipaddr, 'ml'::TEXT AS login_type FROM membership_logins WHERE ipaddr=? AND stamp::DATE BETWEEN ? AND ?
			UNION
			SELECT id, stamp, ipaddr, 'ug'::TEXT AS login_type FROM upgrade_tmp WHERE ipaddr=? AND stamp::DATE BETWEEN ? AND ?
			UNION
			SELECT id, stamp, ipaddr, 'ug'::TEXT AS login_type FROM upgrade_tmp_arc WHERE ipaddr=? AND stamp::DATE BETWEEN ? AND ?
		) AS mlo
		WHERE m.id=mlo.id 
		ORDER BY mlo.stamp
	#);
	$sth->execute(
		$sv, $q->param('start_date'), $q->param('end_date'),
		$sv, $q->param('start_date'), $q->param('end_date'),
		$sv, $q->param('start_date'), $q->param('end_date')
	);
	RunQuery($sth);
}

sub Report 
{
	print $q->header;
	print $q->start_html(
		-title=>'Membership IP Reporting',
		-encoding=>'utf-8',
		-onload=>'init()',
		-style=>{-src=>'/css/admin/generic-report.css',
			-code=>'
				body { margin-top:0; }
				a { text-decoration:none; }
				td { white-space:nowrap; }
				td.id,td.ip { color: blue; }
				#mainFrm { position:fixed; top:0; left:0; background-color:#efffef; }
				#mainTbl, #errors, #iframe-div { margin-top:6em; }
				#iframe-div {  position:fixed; right:0; font:normal 75% sans-serif; padding:0.3em; border:1px solid silver; }
				td.lt { text-align:center; }
			'
		},
		-script=>{-src=>'/js/admin/membership-ip-reporting.js'}
	);
	
	print $q->start_form(-method=>'get', -onsubmit=>'frmSubmit(this)', -id=>'mainFrm');
	print '<table id="frmTbl"><tr><th>Start Date</th><th>End Date</th><th>Value</th><th>Search Type</th>',
		$q->th(
			$q->a({-href=>'#', -title=>'Minimum Grouping Size'}, 'MGS')
		),
		'<th></th><th>Window Type</th></tr><tr>',
		$q->td(
			$q->popup_menu(
				-name=>'start_date',
				-values=>\@dates,
				-default=>$end_date
			)
		),
		$q->td(
			$q->popup_menu(
				-name=>'end_date',
				-values=>\@dates,
				-default=>$end_date
			)
		),
		$q->td( $q->textfield(-name=>'search_value', -onclick=>'this.select()') ),
		$q->td(
			$q->popup_menu(
				-onchange=>'changeWindowType(this)',
				-name=>'search_type',
				-default=>'all',
				-values=>[qw/all id ip host/],
				-labels=>{all=>'All', id=>'ID', ip=>'IP Addr.', host=>'Host'}
			)
		),
		$q->td(
			$q->popup_menu(
				-name=>'minimum_group_size',
				-values=>[2..10]
			)
		),
		$q->td(	$q->submit ),
		$q->td(
			$q->radio_group(
				-name=>'window_type',
				-values=>['same','new','host'],
				-force=>1,
				-labels=>{same=>'Same', new=>'New', host=>'Host'}
			)
		),
		'</tr></table>';

	print $q->end_form;
	print '<div id="iframe-div">
		Click/Double Click ID or IP to set search value and window type.<br />
		Set search type to "Host" to lookup a hostname for a given IP.<br />
		Click the member name to open a new member detail window.<br />
		Host name lookups will appear in this box:<br />
		<iframe src="" frameborder="1" height="45" width="300" name="iframe"></iframe></div>';
	print $q->p({-id=>'errors'}, $Errors) if $Errors;
	if ($Results){
		ReportTable();
	}
	print $q->end_html;
}

sub ReportTable
{
	my $class = 'a';
	print '<table id="mainTbl" class="report">';
	print '<tr><th>IP Address</th><th>Member ID</th><th>Name</th>';
	print '<th>Timestamp</th>' unless $search_type eq 'all';
	print $q->th(
		$q->a({-href=>'#', -title=>'ml=Member Login , ug=Upgrade'}, 'ML or UG')
	);
	print '</tr>';
	foreach my $row (@{$Results})
	{
		print qq|<tr class="$class">|;
		print $q->td({-class=>'ip'}, $row->{ipaddr});
		print $q->td({-class=>'id'}, $row->{id});
		print $q->td( $q->a({-href=>"memberinfo.cgi?id=$row->{id}", -class=>'name'}, $row->{name}) );
		print $q->td($row->{stamp}) unless $search_type eq 'all';
		print $q->td({-class=>'lt'}, $row->{login_type});
		print '</tr>';
	}
	print '</table>';
}

sub RunQuery
{
	my $sth = shift;
	my @list = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @list, $tmp;
	}
	$Results = \@list;
}
