#!/usr/bin/perl -w
###### ebiz_course_admin.cgi
######
###### This script allows staff to modify E-Business Courses and related lessons, tests,
###### and access control logic(acl), as well as to create and delete any of these items.
######
###### Created: 04/15/2004	Karl Kohrt
######
###### Last modified: 11/22/04	Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS
###### our page templates
our %TMPL = (
		'question' 	=> 10571,
		'answer' 	=> 10571
		);	

###### our tables
our %TBL = (	
		'question'	=> 'profile_questions',
		'answer'	=> 'profile_question_answers',
		);

our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $table_name = ( $q->param('table_name') || 'course' );
our $course_key = ( $q->param('course_key') || $q->param('key') || '' );
our $question_key = '';
our @base_list = ();
our $buttons = '';
our $db = '';
our $message = '';
our $header = '';
our $footer = '';

# here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'profile_questions_admin.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Lower case the table name so that it matches the code references.
$table_name = "\L$table_name";

# The different record structures of our tables. The array defines the order that the items
# appear in each row.
our @course = qw/key seq active pass_grade stamp name pass_handler notes course_root/;
our %course = (
	key		=> {value => '', req=>0, english => 'Course Key', numeric => 1, class => 'fp'},
	seq 		=> {value => '', req=>1, english => 'Seq', numeric => 1, class => 'fp', fld_size=>4},
	active		=> {value => '', req=>0, english => 'Active (yes/no)', class => 'fp'},
	pass_grade 	=> {value => '', req=>1, english => 'Pass Grade', numeric => 1, class => 'fp', fld_size=>4},
	stamp		=> {value => '', req=>0, english => 'Last Modified', class => 'fp'},
	name 		=> {value => '', req=>1, english => 'Course Name', class => 'fp'},
	pass_handler	=> {value => '', req=>0, english => 'pass_handler (IT determined)', class => 'fp', fld_size=>4},
	notes 		=> {value => '', req=>0, english => 'Notes', class => 'fp'},
	course_root	=> {value => '', req=>1, english => 'Course Directory', class => 'fp'}
);

our @lesson = qw/key course_key lesson_id description content/;

our %lesson = (
	key		=> {value => '', req=>0, english => 'Lesson Key', numeric => 1, class => 'fp'},
	course_key	=> {value => '', req=>1, english => 'Course Key', numeric => 1, class => 'fp'},
	lesson_id	=> {value => '', req=>1, english => 'Lesson ID', numeric => 1, class => 'fp'},
	description 	=> {value => '', req=>0, english => 'Description', class => 'fp'},
	content	=> {value => '', req=>1, english => 'Lesson Content(filename)', class => 'fp'}
);

our @question = qw/question_key question stamp operator/;

our %question = (
	question_key	=> {value => '', req=>1, english => 'The Question', class => 'fp'},
	question	=> {value => '', req=>1, english => 'The Question', class => 'fp'},
	stamp		=> {value => '', req=>0, english => 'Last Modified', class => 'fp'},
	operator	=> {value => '', req=>0, english => 'By Who', class => 'fp'}
);

our @answer = qw/pk answer_key question_key answer sorting stamp operator/;

our %answer = (
	pk		=> {value => '', req=>0, english => 'Answer Key', numeric => 1, class => 'fp'},
	answer_key	=> {value => '', req=>1, english => 'Answer #', numeric => 1, class => 'fp'},
	question_key	=> {value => '', req=>1, english => 'Question Key', numeric => 1, class => 'fp'},
	answer		=> {value => '', req=>1, english => 'Answer Text', class => 'fp'},
	sorting		=> {value => '', req=>0, english => 'Correct Answer? (1=yes)', class => 'fp'},
	stamp		=> {value => '', req=>0, english => 'Display Type', class => 'fp'},
	operator	=> {value => '', req=>0, english => 'Alignment', class => 'fp'}
);

our @acl = 	qw/key course_key seq control_item label type/;

our %acl = (
	key		=> {value => '', req=>0, english => 'ACL Key', numeric => 1, class => 'fp'},
	course_key	=> {value => '', req=>1, english => 'Course Key', numeric => 1, class => 'fp'},
	seq		=> {value => '', req=>1, english => 'Sequence', numeric => 1, class => 'fp'},
	control_item	=> {value => '', req=>1, english => 'Control Item', class => 'fp'},
	label		=> {value => '', req=>1, english => 'Label/Message', class => 'fp'},
	type		=> {value => '', req=>1, english => 'Type', class => 'fp'}
);

##################################
###### MAIN starts here.
##################################

# Assign the array name and the hash name based upon the table parameter that was passed in.
our $table_array = eval '\@' . $table_name;
our $table_hash = eval '\%' . $table_name;

if ($action eq 'Create')
{
	Load_Params();

	# If we return true, then we'll redo the page with errors flagged				
	if ( Check_Required() )
	{
		@base_list = Get_Data($table_name);
		Display_Results();
	}
	else
	{	
		if ( Create() )
		{
			# Load message, get updated data, load it into variables, display the page.
			$message = "<span class=\"b\">Record Successfully Created.<BR></span>\n";
			@base_list = Get_Data($table_name);
			Display_Results();
		}
		else { Err('Record Creation Failed.') }
	}
}
elsif ($action eq 'Delete')
{
	Load_Params();
	if ( Delete() )
	{
		# Load message, get updated data, load it into variables, display the page.
		$message = "<span class=\"b\">Record Successfully Deleted.<BR></span>\n";
		@base_list = Get_Data($table_name);
		Display_Results();
	}
	else { Err('Record Deletion Failed.') }		
}
elsif ($action eq 'Update')
{
	Load_Params();
	if ( Update() )
	{
		# Load message, get updated data, load it into variables, display the page.
		$message = "<span class=\"b\">Record Successfully Updated.<BR></span>\n";
		@base_list = Get_Data($table_name);
		Display_Results();
	}
	else { Err('Record Update Failed.') }
}
# For all other actions just show the base page.
else 
{
	@base_list = Get_Data($table_name);	
	Display_Results();
}

$db->disconnect;
exit(0);


###### ###### ###### ###### ######
###### Subroutines start here.
##################################

sub cboControlType
{
	###### place a friendly control on the interface for easy setup
	my ($name) = @_;
	return $q->popup_menu(
		-name => $name,
		-values => ['','radio','checkbox','text'],
		-default => '',
		-labels => {radio=>'Radio', checkbox=>'Checkbox', text=>'Text Box'}
	);
}

###### Checks to see if all required fields have been filled out.
sub Check_Required
{
	my $missing = 0;

	# check our numeric only fields
	foreach (keys %{$table_hash})
	{
		if (${$table_hash}{$_}{numeric} &&
			defined ${$table_hash}{$_}{value} &&
			${$table_hash}{$_}{value} gt '' &&
			${$table_hash}{$_}{value} !~ /^\d*?\.??\d+?$/
		   )				
		{
			$missing = 1;
			${$table_hash}{$_}{class} = 'r';
		}
	}
	if ($missing == 1)
	{
		$message = qq|<span class="r">The information marked in Red must be numeric only.<BR></span>|;
	}
	else
	{	
		# check our required fields
		foreach (keys %{$table_hash})
		{
			if (${$table_hash}{$_}{req} &&
				(! defined ${$table_hash}{$_}{value} ||
				${$table_hash}{$_}{value} eq '')
			   )				
			{
#Err("$_= " . ${$table_hash}{$_}{value});
				$missing = 1;
				${$table_hash}{$_}{class} = 'r';
			}
		}
		if ($missing == 1)
		{
			$message = qq|<span class="r">The information marked in Red is required.<BR></span>|;
		}
	}																			
	return($missing);
}

###### Create a new record with the information from the form.
sub Create
{
	my $qry = '';
	my $qry_end = '';
	my $sth = '';
	my $rv = '';
	my @list = ();
	my $first_field = 1;

	$qry = "INSERT INTO $TBL{$table_name} ( ";

	foreach (keys %{$table_hash})
	{
		# We won't include the record key, stamp, or operator since the DB has defaults.
		unless ( $_ eq 'key' || $_ eq 'stamp' || $_ eq 'operator' )
		{
			# Put a comma before the next field.
			if ( ! $first_field ) { $qry .= ", "; $qry_end .= ", "; }

			# Add the next field to the query.
			$qry .= "$_";
			$qry_end .= "?";
			push (@list, ${$table_hash}{$_}{value} || '');
			$first_field = 0;
		}
	}
	$qry .= ") VALUES (" . $qry_end . ")";

	$sth = $db->prepare($qry);
#Err($qry); foreach (@list) { Err("$_") };  
	$rv = $sth->execute( @list );

	$sth->finish;
	return $rv; 
}

###### Create the line of buttons appropriate for the page to be displayed.
sub Create_Buttons
{
	# For courses, show buttons for the related items.
	if ( $table_name eq 'course' )
	{
		$buttons .= qq|&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="action" VALUE='Show Lessons' ONCLICK='document.forms[0].table_name.value="lesson"; return CheckBox()'>
				&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="action" VALUE='Show Questions & Answers' ONCLICK='document.forms[0].table_name.value="question"; return CheckBox()'>
				&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="action" VALUE='Show ACL' ONCLICK='document.forms[0].table_name.value="acl"; return CheckBox()'>
		|;
	}
	# For all others, show button back to courses.
	else
	{
		if ( $table_name eq 'question' )
		{
			$buttons .= qq|&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="action" VALUE='Show Answers' ONCLICK='document.forms[0].table_name.value=\"answer\"; return CheckBox()'>\n|;
		}
		elsif ( $table_name eq 'answer' )
		{
			$buttons .= qq|&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="action" VALUE='Back to Questions' ONCLICK='document.forms[0].table_name.value=\"question\"'>\n|;
		}
		$buttons .= qq|&nbsp;&nbsp;&nbsp;<INPUT TYPE='submit' NAME='action' VALUE=Delete ONCLICK='document.forms[0].table_name.value=\"$table_name\"; return CheckBox()'>
				&nbsp;&nbsp;&nbsp;<INPUT TYPE='submit' NAME='action' VALUE='Back to Courses' ONCLICK='document.forms[0].table_name.value=\"course\"'>\n|;
	}
}

###### Create the header and footer of the page.
sub Create_Header_Footer
{	
	$header = "<TR><TD CLASS=fp><b> Select One </b></TD>";
	$footer = "<TR><TD ALIGN='center' CLASS='fp'>
			<INPUT TYPE='submit' NAME='action' VALUE='Update' ONCLICK='document.forms[0].table_name.value=\"$table_name\"; return CheckBox()'>						
			<BR>-or-<BR>
			<INPUT TYPE='submit' NAME='action' VALUE='Create' ONCLICK='document.forms[0].table_name.value=\"$table_name\"'>
			</TD>";
	### create the table rows
	foreach ( @{$table_array} )
	{
		### skip the key, since it's only used for the radio button
		next if $_ eq "question_key";

		$header .= $q->td({-class=> ${$table_hash}{$_}{class}}, 
			"<b>${$table_hash}{$_}{english}</b>");

		# Pre-fill the course_key field.
		if ( $_ eq "course_key" )
		{
			$footer .= qq#<TD CLASS="fp"><INPUT type="text" name="$_" SIZE="6" VALUE="$course_key" READONLY></TD>\n#;
		}
		# Pre-fill the question_key field.
#		elsif ( $_ eq "question_key" )
#		{
#			$footer .= qq#<TD CLASS="fp"><INPUT type="text" name="$_" SIZE="6" VALUE="$question_key" READONLY></TD>\n#;
#		}
		# Don't show a data entry field for stamp or operator.
		elsif ( $_ eq "stamp" || $_ eq "operator")
		{
			$footer .= qq#<TD CLASS="fp">&nbsp;</TD>\n#;
		}
		elsif ( $_ eq 'control_type')
		{
			$footer .= $q->td({-class=>'fp'}, cboControlType($_));
		}
		# Show a data entry field for all others.
		else
		{
			# adjust sizes based on type of anticipated input
			# to minimize page width bloat
			my $size = $course{$_}{fld_size} || 12;
			$footer .= qq#<TD CLASS="fp"><INPUT type="text" name="$_" SIZE="$size"></TD>\n#;
		}
	}						
	$header .= "</tr>\n";
	$footer .= "</tr>\n";
}
		
###### Delete the requested from the database.
sub Delete
{
	my $qry = '';
	my $sth = '';
	my $rv = '';

	$qry = "DELETE FROM $TBL{$table_name} WHERE key = ?";

	$sth = $db->prepare($qry);
#Err($qry); Err(${$table_hash}{key}{value});  
	$rv = $sth->execute(${$table_hash}{key}{value});
	$sth->finish;
	return $rv;	
}

###### Display the list of records returned from the Get_Data subroutine.
sub Display_Results
{
	my $list = '';
	my @answers_list = ();	

	my $tmpl = G_S::Get_Object($db, $TMPL{$table_name}) || die "Couldn't open $TMPL{$table_name}";

	# Display the various lines of data returned from the query.
	foreach my $result (@base_list)
	{
		$list .= "<tr>\n";

		# Display all the fields in a row.				
		foreach my $field_name ( @{$table_array} )
		{
			if ( $field_name eq "question_key" || $field_name eq "answer_key" )
			{
				$list .= qq#<td align="center" class="fp"><input type="radio" name="question_key" value="$result->{question_key}"></td>\n#;
			}
			else
			{				
				$result->{$field_name} = CGI::escapeHTML($result->{$field_name}) || '&nbsp;';
				$list .= qq#<td class="fp">$result->{$field_name}</td>\n#;
			}				
		}
		$list .= "</tr>\n";

##### This is the beginning of displaying answers below each question on the questions page.
##### Halted development due to the complexity of it within the model already created.
#			if ( $table_name eq 'question' )
#			{
#				# Get the 'answer' data for the current question.			
# 				@answers_list = Get_Data('answer', $result->{key});
#
#				# Display the various lines of data returned from the query.
#				foreach my $result (@answers_list)
#				{
#					$list .= "<TR>";
#					$list .= "<TD ALIGN=center><INPUT TYPE=radio NAME=key VALUE=$result->{key}></TD>";
#		
#					# Display all the fields in a row.				
#					foreach my $field_name ( @{$table_array} )
#					{
#						unless ( $field_name eq "key" )
#						{				
#							$result->{$field_name} = $result->{$field_name} || '&nbsp;';
#							$list .= "<td>$result->{$field_name}</td>";
#						}				
#					}
#					$list .= "</tr>\n";
#				}
#			}
		}

	unless ($list)
	{
		unless ( $message eq '' )
		{
			$message .= "<br>";
		}						
		$message .= "<span class=\"b\">No " . $table_name . "s were found.</span><br>";
	}

	# Create the column headings and the 'Create' record footer fields.	
	Create_Header_Footer();

	# Put the appropriate buttons on the page.
	Create_Buttons();

	my $title = "Profile \u$table_name" . "s - Administration";
	$tmpl =~ s/%%TITLE%%/$title/g;
	$tmpl =~ s/%%ME%%/$ME/;
	$tmpl =~ s/%%MESSAGE%%/$message/;
	$tmpl =~ s/%%TABLE_NAME%%/$table_name/;
	$tmpl =~ s/%%HEADER%%/$header/;
	$tmpl =~ s/%%LIST%%/$list/;
	$tmpl =~ s/%%FOOTER%%/$footer/;
	$tmpl =~ s/%%BUTTONS%%/$buttons/;

	print $q->header(-expires=>'now'), $tmpl;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR>$_[0]<BR>";
	print $q->end_html();
}

###### Gets the relative data for the header display.
sub Get_Relative_Data
{
	my $the_table = shift;
	my $the_key = shift;
	my $qry = '';
	my $sth = '';
	my $rv = '';
	
	$qry .= "	SELECT * 
			FROM $TBL{$the_table}
			WHERE question_key = $the_key";

	$sth = $db->prepare($qry);
	$rv = $sth->execute();

	my $data = $sth->fetchrow_hashref;

	# Show the related course/lesson name or question at the top of the requested page.
	my $headline = 	$data->{name}
				|| $data->{question_number} . " - " . $data->{question}
 				|| 'Unknown';
	$message .= "In relation to: <U>$headline</U>";

	$sth->finish;
}


###### Gets the data from the requested table and loads it into the global hash variable.
sub Get_Data
{
	my $the_table_name = shift;
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my @result_list = ();	
	my $the_key = '';

	$qry = "	SELECT * 
			FROM $TBL{$the_table_name}\n";

	# When returning from an answers page to the questions page we need to get the course_key.
	if ( $the_table_name eq 'question' && $q->param('question_key') )
	{
		$the_key = $q->param('question_key');
		$sth = $db->prepare("
			SELECT course_key
			FROM $TBL{question} WHERE key = $the_key");
		$sth->execute();
	 	$course_key = $sth->fetchrow_array;
	}

	if ( $the_table_name eq 'answer' )
	{
		$the_key = $question_key = $q->param('question_key') || $q->param('key');
	 	$qry .= "WHERE question_key = $the_key ORDER BY answer_number";
	}
	elsif ( $the_table_name eq 'acl')
	{
		$the_key = $course_key;
	 	$qry .= "WHERE course_key = $the_key ORDER BY seq";	
	}
	elsif ( $the_table_name eq 'lesson' )
	{
		$the_key = $course_key;
	 	$qry .= "WHERE course_key = $the_key ORDER BY lesson_id";
	}
#	elsif ( $the_table_name ne 'course' )
#	{
#		$the_key = $course_key;
#	 	$qry .= "WHERE course_key = $the_key ORDER BY question_number";
#	}
#	else
#	{
#		$qry .= "ORDER BY seq";
#	}
	$sth = $db->prepare($qry);

	$rv = $sth->execute();
	while ( my $data = $sth->fetchrow_hashref )
	{
		push (@result_list, $data);
	}		

	$sth->finish;

	# Get the relative data for the list we have just retrieved.
	if ( $the_table_name eq 'answer' ) 
	{
		Get_Relative_Data('question', $the_key);
	}
#	elsif ( $the_table_name ne 'course' ) 
#	{
#		Get_Relative_Data('course', $the_key);
#	}

	return @result_list
}

###### Populating our field array hash values
sub Load_Params
{
	# this loop depends on the hash keys being identical to the passed parameter names
	foreach (keys %{$table_hash})
	{
		${$table_hash}{$_}{value} = ( defined $q->param($_) ) ? $q->param($_) : '';

		# Default the active flag to no if no value is given.
		if ( /active/ && ${$table_hash}{$_}{value} eq '' )
		{
			${$table_hash}{$_}{value} = 'no';
		}
	}
}

###### Update the fields in the database.
sub Update
{
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my @list = ();
	my $first_field = 1;

	$qry = "UPDATE $TBL{$table_name} SET ";

	foreach (keys %{$table_hash})
	{
		# We include all non-blank fields except the record key.
		if ( ( $_ ne 'key' 
			&& ${$table_hash}{$_}{value} )
			|| $_ eq 'stamp'
			|| $_ eq 'operator' )
		{
			# Put a comma before the next field.
			if ( ! $first_field ) { $qry .= ", " }

			# Add the next field to the query.
			if ( $_ eq 'stamp' ) { $qry .= "$_ = NOW()" }
			elsif ( $_ eq 'operator' ) { $qry .= "$_ = CURRENT_USER" }
			else
			{
				$qry .= "$_ = ?";
				push (@list, ${$table_hash}{$_}{value} || '');
			}
			$first_field = 0;
		}
	}
	$qry .= " WHERE question_key = " . ${$table_hash}{key}{value};

	$sth = $db->prepare($qry);
#Err($qry); foreach (@list) { Err($_) };  
	$rv = $sth->execute(@list);
	$sth->finish;
	return $rv;	
}

###### added various formatting elements and added sort orders to the page results
###### 05/11/04 fixed a broken sort order for ACLs
###### 05/25/04 Added "elsif ( $the_table_name eq 'lesson' )" to the sort section
######		to accommodate the lesson table selection.
###### 11/22/04 updated all references to var values beginning with %{$table_hash}->
###### to begin with ${$table_hash} to clear deprecation error warnings
###### also revised the array and hash assignments to evals instead of if/elsifs
###### added a drop-down control for the answer control type input
