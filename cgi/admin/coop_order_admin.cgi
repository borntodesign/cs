#!/usr/bin/perl -w
###### coop_order_admin.cgi
######
###### An interface for admin to view and modify Co-Op orders.
######
###### created: 05/02/05	Karl Kohrt
######
###### last modified: 01/02/06	Karl Kohrt

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use ParseMe;
use State_List;
use Business::CreditCard;

###### CONSTANTS
# An SQL Interval representing how long they've been a member before we can transfer them.
our $member_age = '16 hours';	 
our $LOG_PATH = "/home/admin/tools";	# Where the logs are kept.
our %TMPL = (	'config'	=> '10409',	# The xml config file containing the donor account numbers.
		'main'		=> '10415',
		'order'	=> '10417' );
our @numerics = qw/id quantity price order_type cc_cvv2 cc_exp/;
our @fieldnames = qw/pk
			id
			quantity
			price
			pay_type
			order_type
			acct_holder
			addr
			city
			state
			postalcode
			country
			cc_number
			cc_cvv2
			cc_exp
			status
			stamp
			operator
			/;

###### GLOBALS
our $q = new CGI;
our $ME = $q->url;
our $db = '';
our $page_body = '';
our $id = '';		# The member id of the transaction.			
our $action = '';
our $message = '';
our $type = '';				
our $stamp = '';
our $params = ();
our $order = ();
our $page_type = 'main';


my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

if ($operator && $pwd)
{
	# Stop buffering so we get realtime output to the browser.
	$|=1;
	# our admin access
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'coop_order_admin.cgi', $operator, $pwd )){exit}

	$db->{RaiseError} = 1;
	$action = $q->param('action');

	# No action means we display the main menu only.
	unless ( $action )
	{
		Do_Page($page_type);
	}
	# Otherwise, there was some type of action submitted.
	else
	{
		# Get the parameters passed in.
		Load_Params();
		my $results = '';

		if ( $action eq 'show' )
		{
			# Get the orders and build a list based upon the params passed in.
			$results = Build_List( Get_Orders() );
		}
		elsif ( $action eq 'order' )
		{
			# Get the order's log entry if requested.
			$message = Get_Log_Entry($params->{pk}) if $params->{viewlog};
			# Get the individual order based upon the pk parameter.
			my @tmp = Get_Orders('pk');
			$results = $tmp[0];
			$page_type = 'order';
		}
		elsif ( $action eq 'update' )
		{
			if ( Update_Order() )
			{
				$message = qq#<span class="b">
						The order was successfully updated.<br>
						</span><br>#;
			}
			# Get the individual order based upon the pk parameter.
			my @tmp = Get_Orders('pk');			
			$results = $tmp[0];
			$page_type = 'order';
		}
		elsif ( $action eq 'count' )
		{
			$message = qq#<span class="b"># . Count_Members() . qq#</span><br>#;
		}
		else
		{
			$message = qq#<span class="r">
					The action '$action' is not allowed.<br>
					</span><br>#;
		}
		# Display the results along with the menu.
		Do_Page($page_type, $results);
	}
	$db->disconnect;
}
else
{
	Err('This interface is for Admin use only. Please log in, then try again.');
}

exit;

#####################################
###### Begin Subroutines Here. ######
#####################################

###### Build a list of orders for display.
sub Build_List
{
	my @results = @_;
	my $bg = '#ffffee';
	my $pend_ord_ttl = 0;
	my $pend_qty_ttl = 0;
	my $pend_dol_ttl = 0;
	my $fill_ord_ttl = 0;
	my $fill_qty_ttl = 0;
	my $fill_dol_ttl = 0;
	my $reject_ord_ttl = 0;
	my $reject_qty_ttl = 0;
	my $reject_dol_ttl = 0;
	my $void_ord_ttl = 0;
	my $void_qty_ttl = 0;
	my $void_dol_ttl = 0;

	my @Sort = ('pk', 'id', 'quantity', 'price', 'pay_type', 'order_type', 'status', 'stamp');
	my $tbl = $q->Tr({-bgcolor=>$bg}, $q->th(\@Sort));

	foreach my $tmp ( @results )
	{
		###### alternate our row color
		$bg = ($bg eq '#ffffee') ? '#eeeeee' : '#ffffee';
		$tbl .= qq|<tr bgcolor="$bg">\n|;
		foreach (@Sort)
		{
			$tbl .= qq#<td align="center">#;
			if ( $_ eq 'pk' )
			{
				$tbl .= qq#<a href="$ME?action=order;pk=$tmp->{$_}" target="_blank">
							$tmp->{$_}</a>#;
			}
			elsif ( $_ eq 'id' )
			{
				$tbl .= qq#<a href="/cgi/admin/memberinfo.cgi?id=$tmp->{$_}" target="_blank">
							$tmp->{$_}</a>#;
			}
			elsif ( $_ eq 'status' )
			{
				if ( $tmp->{$_} eq 'p' )
				{
					$tbl .= "Pending";
					$pend_ord_ttl++;
					$pend_qty_ttl += $tmp->{quantity};
					$pend_dol_ttl += $tmp->{price};
				}
				elsif ( $tmp->{$_} eq 'f' )
				{
					$tbl .= "Filled";
					$fill_ord_ttl++;
					$fill_qty_ttl += $tmp->{quantity};
					$fill_dol_ttl += $tmp->{price};
				}
				elsif ( $tmp->{$_} eq 'r' )
				{
					$tbl .= "Rejected";
					$reject_ord_ttl++;
					$reject_qty_ttl += $tmp->{quantity};
					$reject_dol_ttl += $tmp->{price};
				}
				elsif ( $tmp->{$_} eq 'v' )
				{
					$tbl .= "Void";
					$void_ord_ttl++;
					$void_qty_ttl += $tmp->{quantity};
					$void_dol_ttl += $tmp->{price};
				}
			}
			elsif ( $_ eq 'pay_type' && $tmp->{pay_type} eq 'FR' )
			{
				# Put the replaced ID on the page if this was a replacement.
				my $replace_id = $db->selectrow_array("	SELECT replace_id
										FROM 	coop_replace
										WHERE order_num = ?", undef, $tmp->{pk});
				$tbl .= qq#<a href="/cgi/admin/memberinfo.cgi?id=$replace_id;Ltype=arclist" target="_blank">
						$tmp->{$_}</a>#;
			}
			else
			{
				$tbl .= $tmp->{$_} || '&nbsp;';
			}
			$tbl .= "</td>";
		}
	}
	# Calculate the overall totals for the output.
	my $over_ord_ttl = $fill_ord_ttl + $reject_ord_ttl + $pend_ord_ttl + $void_ord_ttl;
	my $over_qty_ttl = $fill_qty_ttl + $reject_qty_ttl + $pend_qty_ttl + $void_qty_ttl;
	my $over_dol_ttl = $fill_dol_ttl + $reject_dol_ttl + $pend_dol_ttl + $void_dol_ttl;

	# Blank out all zero value fields.
	$pend_ord_ttl ||= "&nbsp;";
	$pend_qty_ttl ||= "&nbsp;";
	$pend_dol_ttl = ${\sprintf "%.2f", $pend_dol_ttl};
	$fill_ord_ttl ||= "&nbsp;";
	$fill_qty_ttl ||= "&nbsp;";
	$fill_dol_ttl = ${\sprintf "%.2f", $fill_dol_ttl};
	$reject_ord_ttl ||= "&nbsp;";
	$reject_qty_ttl ||= "&nbsp;";
	$reject_dol_ttl = ${\sprintf "%.2f", $reject_dol_ttl};
	$void_ord_ttl ||= "&nbsp;";
	$void_qty_ttl ||= "&nbsp;";
	$void_dol_ttl = ${\sprintf "%.2f", $void_dol_ttl};
	
	$tbl = qq#<tr><td align="center" colspan="2">
			<table border="0" cellpadding="3" cellsapcing="3">
			$tbl
			</table>
			</td></tr>#;
	$tbl .= qq#<tr><td align="center" colspan="2">
			<table border="0" cellpadding="3" cellsapcing="3">
			<tr>
			<td colspan="4" align="center"><h2><u>Search Totals</u></h2></td>
			</tr>
			<tr>
			<td><h3>&nbsp;</h3></td>
			<td align="center"><h3><u>Orders</u></h3></td>
			<td align="center"><h3><u>Members</u></h3></td>
			<td align="right"><h3><u>Dollars</u></h3></td>
			</tr>
			<tr>
			<td align="right"><h3>Pending: </h3></td>
			<td align="center"><h3>$pend_ord_ttl</h3></td>
			<td align="center"><h3>$pend_qty_ttl</h3></td>
			<td align="right"><h3>\$$pend_dol_ttl</h3></td></b>
			</tr>
			<tr>
			<td align="right"><h3>Filled: </h3></td>
			<td align="center"><h3>$fill_ord_ttl</h3></td>
			<td align="center"><h3>$fill_qty_ttl</h3></td>
			<td align="right"><h3>\$$fill_dol_ttl</h3></td></b>
			</tr>
			<tr>
			<td align="right">Rejected: </td>
			<td align="center">$reject_ord_ttl</td>
			<td align="center">$reject_qty_ttl</td>
			<td align="right">\$$reject_dol_ttl</td>
			</tr>
			<tr>
			<tr>
			<td align="right">Void: </td>
			<td align="center">$void_ord_ttl</td>
			<td align="center">$void_qty_ttl</td>
			<td align="right">\$$void_dol_ttl</td></b>
			</tr>
			<td align="right">Combined Totals: </td>
			<td align="center">$over_ord_ttl</td>
			<td align="center">$over_qty_ttl</td>
			<td align="right">\$${\sprintf "%.2f", $over_dol_ttl}</td>
			</tr>
			</table>
			</td></tr>#;
	return $tbl;
}

###### Build a drop-down menu of credit card expiration dates.
sub Build_Menu_CCExp
{
	my $default = shift || '';
	my @cc_exp_order = ();
	my $cc_exp = ();
	my $shortyear = '';
	my ( $month, $year ) = (localtime)[ 4, 5 ];
	$month += 1; 
	$year += 1900;
	my $maxyear = $year + 8;	# 8 years of expiration dates.

	while ( $year <= $maxyear )
	{
		($shortyear = $year) =~ s/^20//;
		while ( $month <= 12 )
		{
			$month = ( length($month) == 1 ) ? "0" . $month : $month;
			my $key = $month . $shortyear;
			$cc_exp->{$key} = $month . "/" . $year;
			# Build an array for ordering the menu.
			push @cc_exp_order, $key;
			$month += 1;
		}
		# It's a new year, so start with month 1 - January.
		$month = 1;
		$year += 1;
	}
	return $q->popup_menu(	-name=>'cc_exp',
					-class=>'in',
					-values=>\@cc_exp_order,
					-default=>$default,
					-labels=>$cc_exp,
					-force=>'true');
}

###### Build a drop-down menu of the countries.
sub Build_Menu_Country
{
	my $default = shift || '';
	require '/home/httpd/cgi-lib/CC.list';
	return $q->popup_menu(	-name=>'country',
					-class=>'in',
					-values=>\@CC::COUNTRY_CODES,
					-default=>$default,
					-labels=>\%CC::COUNTRIES,
					-force=>'true');
}

sub Build_Menu_OrderType
{
	my $default = shift || '';
	my %Oh = ( 	''	=> 'Select One',
			'1'	=> 'VIP Direct (1)',
			'5'	=> 'Auto Pilot (5)' );
	my @ORDERTYPES = ("", "1", "5");

	return $q->popup_menu(	-name=>'order_type',
					-values=>\@ORDERTYPES,
					-default=>$default,
					-labels=>\%Oh,
					-class=>'in',
					-force=>'true',
					-onClick=>"document.forms[0].which[1].checked=true");
}

###### Build a drop-down menu of the states.
sub Build_Menu_States
{
	# Get the name of the field passed in.
	my ($default_state, $default_country) = @_;
	my ($tmp1, $tmp2, $tmp3, $tmp4) = ();
	my ($StateList, $StateHash) = ();

	(
	$tmp1,	$tmp2,	$tmp3, $tmp4,
	$StateList,
	$StateHash
	)	= G_S::Standard_State(	$db,
						$default_state,
						$default_state,
						$default_country);
	###### if our list is not populated, then we'll put something in
	if ( ! defined $StateList || scalar @{$StateList} < 1)
	{
		$StateList = [''];
		$StateHash = {'' => 'Enter value below'};
	}

	return $q->popup_menu(	-name=>'state',
					-default=>$default_state,
					-values=>$StateList,
					-labels=>$StateHash,
					-class=>'in',
					-force=>'true');
}

###### Build a drop-down menu of the order status types.
sub Build_Menu_Status
{
	my $default = shift || '';
	my %S = ( 	''	=> 'Select One',
			'p'	=> 'Pending',
			'f'	=> 'Filled',
			'r'	=> 'Rejected',
			'v'	=> 'Void' );
	my @STATUS_CODES = ("", "p", "f", "r", "v");

	return $q->popup_menu(	-name=>'status',
					-values=>\@STATUS_CODES,
					-default=>$default,
					-labels=>\%S,
					-class=>'in',
					-force=>'true',
					-onClick=>"document.forms[0].which[1].checked=true");
}

###### Count how many members 
sub Count_Members
{
	# Get the donor accounts from the config file.
	my $donor_accounts = G_S::Get_Object($db, $TMPL{'config'})
				|| die "Failed to load XML template: $TMPL{'config'}\n";
	# Remove everything except the info between the donor_accounts tags.
	$donor_accounts =~ s/^(.*?)donor_accounts>|<\/donor_accounts(.*?)$//gs;

	my $avail = $db->selectrow_array("
		SELECT count(*)
		FROM 	members
		WHERE 	spid IN ( $donor_accounts )
		AND 	membertype IN ('m','s')
		AND 	mail_option_lvl > 0
		AND 	stamp < NOW() - INTERVAL '$member_age'");

	my $total = $db->selectrow_array("
		SELECT count(*)
		FROM 	members
		WHERE 	spid IN ( $donor_accounts )
		AND 	membertype IN ('m','s')
		AND 	mail_option_lvl > 0");
	return "Co-Op Members Available For Transfer: $avail<br />Co-Op Members Total Up to the Minute: $total";
}

###### Display the page to the browser.
sub Do_Page
{
	my ($type, $results) = @_;
	$results ||= '';
	my $tmpl = G_S::Get_Object($db, $TMPL{$type}) || die "Couldn't open template: $TMPL{$type}";
	$tmpl =~ s/%%me%%/$ME/g;
	$tmpl =~ s/%%message%%/$message/;

	# If this is an individual order, we need the menus.
	if ( $type eq 'order' )
	{
		$tmpl =~ s/%%cc_exp_menu%%/Build_Menu_CCExp($results->{cc_exp})/e;
		$tmpl =~ s/%%country_menu%%/Build_Menu_Country(uc($results->{country}))/e;
		$tmpl =~ s/%%state_menu%%/Build_Menu_States(uc($results->{state}), uc($results->{country}))/e;
		$tmpl =~ s/%%ordertype_menu%%/Build_Menu_OrderType($results->{order_type})/e;
		$tmpl =~ s/%%status_menu%%/Build_Menu_Status($results->{status})/e;

		# Blank out all but the last 3 credit card numbers.
		$results->{cc_number} = Obfuscate($results->{cc_number}) if $results->{cc_number};

		# Put the standard fields on the page.
		foreach ( @fieldnames )
		{
			$results->{$_} ||= '';
			$tmpl =~ s/%%$_%%/$results->{$_}/g;
		}
		# Put the member name on the page for easier of access.
		my $member_name = $db->selectrow_array("	SELECT firstname || ' ' || lastname
								FROM members
								WHERE id = ?", undef, $results->{id});
		$tmpl =~ s/%%member_name%%/$member_name/g;
		# Put the replaced ID on the page if this was a replacement.
		my $replace_id = '';
		if ( $results->{pay_type} eq 'FR' )
		{		
			$replace_id = $db->selectrow_array("	SELECT replace_id
									FROM 	coop_replace
									WHERE order_num = ?", undef, $results->{pk});
			$replace_id = qq#<a href="/cgi/admin/memberinfo.cgi?id=$replace_id;Ltype=arclist" target="_blank">
						Replaces Member \#$replace_id</a>#;
		}
		$tmpl =~ s/%%replace_id%%/$replace_id/;
	}
	else
	{
		$tmpl =~ s/%%ordertype_menu%%/Build_Menu_OrderType()/e;
		$tmpl =~ s/%%status_menu%%/Build_Menu_Status()/e;
		$tmpl =~ s/%%results%%/$results/;
	}

	# print the page header here so we can view Unicode.
	print $q->header(	-expires=>'now',
				-type=>'text/html'), $tmpl;
}

###### Print an error message.
sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h4($_[0]), scalar localtime(),
		$q->end_html();
}

###### Get the pertinent info out of the log file.
sub Get_Log_Entry
{
	my $order_num = shift;
	my $data = '';

	# Open the tracking log file.
	open (TRACK_LOG, "<$LOG_PATH/co_fill_tracking.log");
	while (<TRACK_LOG>)
	{
		if ( $_ =~ m/order#$order_num/ )
		{
			$data .= "$_<br>";
		}
	}
	close TRACK_LOG;
	$data = "Log data for this order could not be found." unless $data;
	return qq#<span class="b">$data</span>#;
}

###### Get the orders based upon the search params passed in.
sub Get_Orders
{
	my @search_params = shift || qw/id pay_type order_type status stamp range/;
	my $where_clause = '';
	my $tbl = '';
	my @values = ();
	my @tmp_results = ();

	foreach ( @search_params )
	{
		# If we have a complete date.
		if ( $_ eq 'stamp' && $params->{$_} )
		{
			$where_clause .= " AND " if $where_clause;		
			$where_clause .= " ($_)::TEXT ~ ?::TEXT ";
			push @values, $params->{$_};
		}
		# If we have a partial date.
		elsif ( $_ eq 'range' && $params->{$_} )
		{
			$where_clause .= " AND " if $where_clause;		
			$where_clause .= " stamp::TEXT ~ ? ";
			push @values, $params->{$_};
		}
		# If we have this type of parameter passed in.
		elsif ( $params->{$_} )
		{
			$where_clause .= " AND " if $where_clause;		
			$where_clause .= " $_ = ? ";
			push @values, $params->{$_};
		}
	}
	# Unless we already have search parameters, we'll return all pending orders as the default.
	$where_clause = "status = 'p'" unless $where_clause;
	my $sth = $db->prepare("	SELECT * 
					FROM coop_orders WHERE "
					. $where_clause
					. " ORDER BY pk;"
					);
	my $rv = $sth->execute(@values);
	if ( $rv eq '0E0' ) { $message .= qq#<span class="r">No orders were found.</span><br># }
	else
	{
		while ( my $tmp = $sth->fetchrow_hashref )
		{
			push @tmp_results, $tmp;
		}
	}
	return @tmp_results;
}

###### Load the params into a local hash.
sub Load_Params
{
	# Get the submitted parameters.
	foreach ( @fieldnames )
	{
		$params->{$_} = $q->param($_) || '';
	}
	# Get any un-documented parameters.
	foreach ( $q->param() )
	{
		$params->{$_} = $q->param($_) || '' unless  $params->{$_};
	}
	# Use the full state field value if the combo box doesn't have a value. 
	$params->{'state'} = $params->{'fullstate'} unless $params->{'state'};
	# Get the full CC number if this is the obfuscated version.
	if ( $params->{'cc_number'} =~ m/\*/ )
	{
		$params->{'cc_number'} = $db->selectrow_array("	SELECT cc_number
									FROM 	coop_orders
									WHERE 	pk = ?",
									undef, $params->{pk});
	}
}

###### Blank out all but the last 3 characters.
sub Obfuscate
{
	my $n = shift;
	my $new = '';
	my $digits = length($n) - 3;
	$new .= substr($n, $digits, 3);

	while ( $digits ) { 	$new = "*" . $new; $digits--; }

	return $new;
}

###### Update the order.
sub Update_Order
{
	my $pk = shift;
	my @excluded = qw/pk id stamp operator/;
	my @values = ();
	my $qry = "	UPDATE coop_orders
			SET 	stamp = NOW(),
				operator = ?, ";
	push @values, $operator;
	$params->{'state'} = lc($params->{'state'}) if $params->{'state'};
	
	foreach my $field ( @fieldnames )
	{
		# Include the fields in the query, unless we've excluded the field.
		unless ( grep /$field/, @excluded )
		{
			$qry .= " $field = ?,";
			push @values, $params->{$field};
		}
	}
	chop $qry;
	$qry .= " WHERE pk = ? AND status != 'f'";
	push @values, $params->{'pk'};

#Err("qry= $qry"); foreach (@values) {Err("$_");}; exit;
	# Update an order unless it is already filled.
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute(@values);
	if ( $rv eq '0E0' )
	{
		$message .= qq#<span class="r">
				The order appears to have already been filled. Therefore, no changes were made.
				</span><br>#;
		return 0;
	}
	return $rv;
}


###### 04/29/05 Added ordertype menu and search capabilities.
###### 05/02/05 Added pending and void orders totals to the bottom of the report.
###### 05/16/05 Removed the @pay_types array since it wasn't being used.
###### 05/26/05 Added the replaced ID to the individual order form when free replacement.
###### 01/02/06 Added "mail_option_lvl > 0" to the Count_Members queries.








