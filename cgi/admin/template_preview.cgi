#!/usr/bin/perl -w
###### template_preview.cgi
######
###### The application for previewing a resource value in a browser window.
######  
###### 05/29/2003	Karl Kohrt
###### last modified: 08/12/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
require cs_admin;
use ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
our $q = new CGI;
my ($db, $TMPL) = ();

if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'template_preview.cgi', $creds->{'username'}, $creds->{'password'} );
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}
$db->{'RaiseError'} = 1;

##########################################
###### MAIN program begins here.
##########################################

our $params = $q->path_info();				# Get the path info.
$params =~ s/\///;						# Remove the leading slash.
our ($id_number, $location) = split(/;/, $params);	# Get the id number and location.
$location = 'cgi_obj' unless $location;			# Default is the cgi_object table.

#unless ( $db = DB_Connect('template_preview.cgi') ){ exit }
#$db->{RaiseError} = 1;

if ( $location eq 'cgi_obj' )
{
	# Retrieve the current value from the cgi_object table.
	$TMPL = G_S::Get_Object($db, $id_number) ||
		die "Cannot open Template: $id_number";
}
elsif ( $location eq 'tq' )
{
	# Retrieve the current value from the translation queue.
	$TMPL = $db->selectrow_array("	SELECT resource_value
						FROM 	translation_edit_queue
						WHERE 	pk = $id_number") ||
		die "Cannot open Translation Resource: $id_number";
}
elsif ( $location eq 'lex' )
{
	# Retrieve the current value from the lexicons table.
	$TMPL = $db->selectrow_array("	SELECT value
						FROM 	lexicons
						WHERE 	pk = $id_number") ||
		die "Cannot open Lexicon: $id_number";
}

###### if we are showing a text page, we'll change some common textual items
###### to their HTML equivalent
if ($TMPL !~ m#/<br|<p|<body#i)
{
	$TMPL =~ s/  /&nbsp;&nbsp;/g;
	$TMPL =~ s/</&lt;/g;
	$TMPL =~ s/>/&gt;/g;
	$TMPL =~ s#\n#<br />#g;
}

if ($TMPL !~ m#<body#i)
{
	$TMPL = '<body>' . $TMPL .'</body>';
}

if ($TMPL !~ m#<html#i)
{
	$TMPL = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>' . $TMPL .'</html>';
}

print $q->header('-charset'=>'utf-8'), $TMPL;

$db->disconnect;

exit;

sub Err
{
        print $q->header, $q->start_html;
        print $q->p($_[0]);
        print $q->end_html;
}

###### added a simple check for the presence of HTML, if not present, added HTML linebreaks
###### to make the template more accurately readable in an HTML page
###### 05/18/04 added handling for multiple textual spaces into repeating &nbsp;
###### 08/20/04 revised the check to overlook '</weblink>'
###### 11/19/04 Added handling for the Translation Queue resources, including the addition of
######		second parameter, $location.
# 02/17/16 implemented cs_admin to make sure that only staff is using this interface