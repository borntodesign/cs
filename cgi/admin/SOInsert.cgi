#!/usr/bin/perl -w
###### SOInsert.cgi
###### Statement of Accounts Viewer
###### originally created by: Stephen Martin
###### last modified: 05/11/2015 Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;
use lib '/home/httpd/cgi-lib/';
use G_S;
require cs_admin;
use ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

###### Globals

my $sth = ();
my $rv;
our $db        = ();
our $q         = new CGI;
$q->charset('utf-8');
my $cs = cs_admin->new({'cgi'=>$q});
our $firstname;
our $lastname;
our $trans_type;
our $amount;
our $description;
our $memo;
our $batch;
our $o;
my $qry;
my $self = $q->url();
my $des;
my $des_in;
my $class;
my $editor_link = "./SOEditor.cgi";
my $trans_menu;
my $transaction_hash;
my $MESSAGE = '';
my %TMPL = (
	'fund' => 10123,	# The funding form.
	'email' => 10505	# The email to the recipient of the action
);


unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
	exit;
}

my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});

my $ck = $q->cookie('cs_admin');
die "You must be logged in to use this resource\n" unless $ck;
my $creds = $cs->authenticate($ck);
exit unless $db = ADMIN_DB::DB_Connect( 'admin-rd', $creds->{'username'}, $creds->{'password'} );
$db->{RaiseError} = 1;

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
#if ( CheckGroup() != 1 ) {
if (! ADMIN_DB::CheckGroup($db, $creds->{'username'}, 'accounting'))
{
    Err(
        $q->h3('You must be a member of the accounting group to use this form')
    );
    Exit();
}

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

foreach ( $q->param() )
{
    $param{$_} = $gs->Prepare_UTF8( $q->param($_) );
    
	###### I don't want empty strings in the DB, so I'll undef any empties so they go in as NULLs
	$param{$_} = undef if length $param{$_} == 0;
	next unless $param{$_};
	
	###### remove leading and trailing whitespace
	$param{$_} =~ s/^\s*|\s*$//g;
}

$param{'action'} ||= '';

if ( $param{'action'} eq 'insert' or $param{'action'} eq 'confirm' )
{
	###### Ensure we've got the basic parameters
	foreach('id','trans_type','amount')
	{
		next if $param{$_};
		Err($q->h4("The $_ parameter is missing"));
		Exit();
	}

	my $m = $db->selectrow_hashref("
		SELECT id, alias, firstname, lastname, COALESCE(emailaddress,'') AS emailaddress, membertype,
			COALESCE(language_pref, 'en') AS language_pref
		FROM members
		WHERE ${\($param{'id'} =~ /\D/ ? 'alias' : 'id')} = ?", undef, (uc $param{'id'})) || die "Failed to find a matching membership for: $param{'id'}\n";

	if ($param{'action'} eq 'confirm')
	{
		print $q->header(),
			$q->start_html(
				'-title'=>'Confirm Transaction Insertion',
				'-style'=>{'-code'=>'
					body, td, input, button, div{font:normal 85% sans-serif}
					td{border:1px solid silver; padding: 0.2em 0.3em; }
					'},
				'-encoding'=>'utf8');
		print $q->start_form('-action'=>$q->url);
		print q|<table style="margin-left:auto; margin-right:auto;">|;
		print $q->Tr($q->td('Alias:') . $q->td($m->{'alias'}));
		print $q->Tr($q->td('Name:') . $q->td("$m->{firstname} $m->{lastname}"));
		print $q->Tr($q->td('Email') . $q->td($m->{'emailaddress'}));
		print $q->Tr($q->td('Member Type') . $q->td($m->{membertype}));
		
		my $fc = $param{amount} < 0 ? 'red' : 'green';
		print $q->Tr($q->td('Amount') .
			$q->td({-style=>"color:$fc; font-weight:bold; font-size:100%"}, sprintf '%6.2f', $param{amount}));
		print $q->Tr($q->td('Send Email?') . $q->td($param{sendmail} ? 'YES' : 'NO'));
		print q|</table><div style="text-align:center">|;
		print $q->submit(-value=>'Create Transaction', -onclick=>'window.opener.document.f.reset();');
		print $q->button(
			-style=>'margin-left: 3em;',
			-onclick=>'self.close();',
			-value=>'Cancel Transaction');
			
		foreach (keys %param)
		{
			next if $_ eq 'action';
			print $q->hidden('-name'=>$_, '-value'=>$param{$_}, '-force'=>1) if defined $param{$_};
		}
		
		print '</div>',
			$q->hidden('-name'=>'action', '-value'=>'insert', '-force'=>1);
			$q->end_form,
			$q->end_html;
	}
	else
	{

###### At this point all parameters are verified
###### Select the next ID so we can insert it directly
###### Transaction ID Lock Begins

		my ($trans_id) = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')");

###### Now insert the record 

		$db->do("INSERT INTO soa (
			trans_id, 
			id, 
			trans_type, 
			amount, 
            description, 
			memo, 
			batch
		) VALUES ( ?,?,?,?,?,?,?)", undef,
		$trans_id,
		$param{'id'},
		$param{trans_type},
		$param{amount},
		$param{description},
		$param{memo},
		$param{batch});

		print $q->header(),
			$q->start_html(-encoding=>'utf8'),
			$q->p("Transaction id: $trans_id Inserted"),
			$q->p( $q->a({-href=>'#', -onclick=>'self.close(); return false;'}, 'Close Window') .
				$q->a({-href=>"/cgi/admin/SOAadmin.cgi?id=$m->{id}",
				-onclick=>"window.open(this.href,'$m->{'alias'}'); self.close();",
				-style=>'margin-left:2em'}, 'View Report') );

		###### send an email if necessary
		SendMail($m, $trans_id) if $param{sendmail};

		print $q->end_html;
	}
}
else {

	$trans_menu = gen_trans_menu();

	my $TMPL = $gs->GetObject( $TMPL{fund} );
	Err("Couldn't retrieve the template object.") unless $TMPL;

	$TMPL =~ s/%%TRANS_MENU%%/$trans_menu/g;
	$TMPL =~ s/%%MESSAGE%%/$MESSAGE/g;
	$TMPL =~ s/%%SELF%%/$self/g;
	print $q->header( -expires => 'now' ), $TMPL;
}

Exit();

##############################3

sub Err {
    print $q->header(), $q->start_html();
    print $_[0];
    print $q->end_html();
    
    Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub gen_trans_menu {
    my ( $sth, $qry, $data, $rv, $html );    ###### database variables

    $html = "";

    $qry = "select
            id,name 
            from soa_transaction_types
	WHERE admin_interface_available=TRUE
	order by name asc";

    $sth = $db->prepare($qry);

    $rv = $sth->execute;

    while ( $data = $sth->fetchrow_hashref ) {
        $html .= "<option value=\"$data->{id}\">$data->{name}</option>\n";
    }

    $rv = $sth->finish();

    return $html;
}

sub SendMail
{
	my ($m, $trans_id) = @_;

	###### this routine copied directly from funding_admin.cgi to start with

	my ($to_tmp, $from_tmp, $cc, $subject) = '';
	# For troubleshooting email.
	my $bcc = ''; #$ADMIN_ADDRESS;

	# Get the base email template(s).
	my $MSG = $gs->GetObject($TMPL{email}, $m->{language_pref}) || die "Email template retrieve failure\n";

	###### remove any comment fields
	$MSG =~ s/(^|\n)(#.*)//g;
	###### remove line feeds (these can be caused by windows pasting)
	$MSG =~ s/\r//g;
	$MSG =~ s/%%bcc_email%%/$bcc/;

	# The transaction data.
	my $trans = $db->selectrow_hashref("
		SELECT soa.*, stt.name
		FROM soa
		JOIN soa_transaction_types stt
			ON soa.trans_type=stt.id
		WHERE soa.trans_id = $trans_id");
	$MSG =~ s/%%trans_id%%/$trans_id/;
	$MSG =~ s/%%trans_amount%%/$trans->{'amount'}/;
	my $trans_desc = $trans->{name};
	# If this is the Other-Accounting Action choice.
#	if ( $param{trans_type} == 1200 )
#	{
		# Tack on the text entered by the operator
		$trans_desc .= " - " . $gs->Prepare_UTF8($param{'description'}, 'encode');
#	}
	# If this is a moneybooker deposit.
	if ( $param{trans_type} == 10000 )
	{
		# Blank out the placeholders that mark the moneybooker paragraph.
		$MSG =~ s/%%conditional-moneybookers-start%%/\n/g;
		$MSG =~ s/%%conditional-moneybookers-stop%%/\n/g;
	}
	else
	{
		# Blank out the new moneybooker form paragraph.
		$MSG =~ s/%%conditional-moneybookers-start.+?conditional-moneybookers-stop%%//sg;
	}
	$MSG =~ s/%%trans_desc%%/$trans_desc/;

	# These are our recipient member's info.
	$MSG =~ s/%%to_alias%%/$m->{'alias'}/g;
	$MSG =~ s/%%to_firstname%%/$gs->Prepare_UTF8($m->{'firstname'}, 'encode')/eg;
	$MSG =~ s/%%to_lastname%%/$gs->Prepare_UTF8($m->{'lastname'}, 'encode')/eg;
# This next line's email entry is for testing only.
#	$MSG =~ s/%%to_email%%/$ADMIN_ADDRESS/g;
	$MSG =~ s/%%to_email%%/$m->{'emailaddress'}/g;

	# extract our 'To' line from the template
	$MSG =~ s/(^|\n)(To:.*)//i;
	($to_tmp = $2) =~ s/^To:\s*//i;
	# extract our 'From'
	$MSG =~ s/(^|\n)(From:.*)//i;
	($from_tmp = $2) =~ s/^From:\s*//i;
	# extract our 'Subject'
	$MSG =~ s/(^|\n)(Subject:.*)//i;
	($subject = $2) =~ s/^Subject:\s*//i;
	# extract our 'Bcc'
	$MSG =~ s/(^|\n)(Bcc:.*)//i;
	($bcc = $2) =~ s/^Bcc:\s*//i if $2;
	# extract our 'Cc'
	$MSG =~ s/(^|\n)(Cc:.*)//i;
	($cc = $2) =~ s/^Cc:\s*//i if $2;

	# remove all remaining leading spaces and newlines
	$MSG =~ s/^(\s|\n)*//;

	my %mail = (
		To 		=> $to_tmp,
		Bcc	 	=> $bcc,
		Subject => $subject,
		From 	=> $from_tmp,
		'Content-Type'=> 'text/plain; charset="utf-8"',
		'Message' 	=> $MSG);

#	$mail{Bcc} = $bcc if $bcc;
#	&debug(\%mail);
	unless ( sendmail(%mail) )
	{
		print "An attempt to email $m->{'emailaddress'} failed for the following reason - $Mail::Sendmail::error\n";
	}
}

__END__

##### 04/06/2003 - modified interval statements as noted below for postgres7.3:
#####  ...now() + interval('12 month') ) + 1 as paid_thru...
#####  became:
#####  ...cast(now() + interval '12 month' + interval '1 day' AS date) as paid_thru...
###### 06/08/07 put into production after ripping out some unnecessary stuff
###### 08/31/07 introduced a little javascript to reset the main form when the confirm form is submitted
###### 10/01/13 revised to use ADMIN_DB::CheckGroup() instead of the local version
05/11/15
	Several code changes to deal with the recent changes made in DBD::Pg regarding the utf8 flag
	Also a lot of general code cleanup