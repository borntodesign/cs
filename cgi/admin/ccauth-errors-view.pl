#!/usr/bin/perl -w
###### ccauth-errors-view.pl
###### a simple script to view the output of the authorizations errors processing module
###### created: 12/20/05	Bill MacArthur
###### last modified:

use strict;
use CGI;

use lib '/home/httpd/cgi-lib';
use G_S;
require 'ccauth_parse.pm';
require XML::local_utils;

my $gs = new G_S;
my $q = new CGI;

(my $auth = $q->path_info) =~ s#^/##;

my ($auth_errors, $rv) = ccauth_parse::Report($auth);

my $xml = "<base>$auth_errors</base>\n";
print $q->header;
print XML::local_utils::xslt(xsl(), $xml);

exit;

sub xsl
{
	return qq|<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	omit-xml-declaration="yes"
	media-type="text/html"/>
	
	<xsl:template match="/">	
		<html>
<link rel="stylesheet" href="/css/appx.upg.css" type="text/css" />
<link rel="stylesheet" href="/css/memberapp_default.css" type="text/css" />
			<title></title>
			<body>
<div class="error" style="text-align: left; padding: 3px 5px; margin-right:auto; margin-left:auto; width:70%; border: 1px inset #33a">
			<xsl:copy-of select = "/base/*"/>
</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
	|;
}
