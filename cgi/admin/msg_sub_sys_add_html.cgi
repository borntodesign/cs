#!/usr/bin/perl

######  msg_sub_sys_add_html.cgi
######  11/18/2002	Stephen Martin
######  This utility adds either a HTML message or a template reference 
######  to the message_sub_system queue.
######  A specific message can be directed to a specific ID or sent globally.
######
###### Last modified: 12/30/04 Karl Kohrt

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use POSIX qw(locale_h);
use locale;
use Digest::MD5 qw(md5 md5_hex md5_base64);

setlocale( LC_CTYPE, "en_US.ISO8859-1" );

push ( @INC, '/home/httpd/cgi-lib' );

require '/home/httpd/cgi-lib/ADMIN_DB.lib';
require '/home/httpd/cgi-lib/G_S.lib';

###### Globals
my ( $db, $qry, $sth, $rv, $data, $HTML );
my ( @cookie, $TEXT );
my $q = new CGI;
unless ( $q->https ) {
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
}
my $operator = $q->cookie('operator') || $q->param('operator');
my $pwd      = $q->cookie('pwd')      || $q->param('pwd');

my $self   = $q->url();
my $action = $q->param('action');
my $txtmsg = $q->param('txtmsg');
my $ids    = $q->param('ids');
my $obj_id = $q->param('obj_id');
my $MM     = $q->param('MM');
my $DD     = $q->param('DD');
my $YYYY   = $q->param('YYYY');
my $hour   = $q->param('hour');
my $mins   = $q->param('mins');
my $median = $q->param('median');

my @I;
my $ts;
my $sys_message;

unless ( $db = &ADMIN_DB::DB_Connect( 'msg_sub_sys.cgi', $operator, $pwd ) ) {
    $db->{RaiseError} = 1;
    exit(1);
}

###### Catch the insert ID/message request here

if ( $action eq " Send Message " ) {
    unless ($txtmsg || $obj_id ) {
        &Err("Please provide a text message!");
        exit;
    }

    unless ($ids) {
        &Err(
"Please provide an ID or a list of comma spearated ids as message recipients.!"
        );
        exit;
    }

    unless ( $MM && $DD && $YYYY && $hour && $mins ) {
        &Err("Please provide an expiration date MM/DD/YYYY/HH:MM");
        exit;
    }

    ###### Whilst HTML may be placed into a message.. 
    ###### Remove Server Side Includes.

    $txtmsg =~ s/<!--(.|\n)*-->//g;

    ##### Correct hours with median

    if ( $median eq "PM" && $hour != 12 ) {
        $hour = $hour += 12;

        if ( $hour eq "24" )
        {
         $hour = "00";
        }
    }

    ###### Build the TimeStamp

    $ts = $YYYY . "-" . $MM . "-" . $DD . " " . $hour . ":" . $mins;

    ###### Extract target(s) for message

    $sys_message = "";

    if ( $ids =~ /.*\*/ ) {
        insert_message( 0, $obj_id, $txtmsg, $ts );
        $sys_message = <<S1;
          <tr align="center" bgcolor="#FF0000"> 
           <td colspan="3" nowrap><font color="#FFFF00" size="2" face="Verdana, Arial, Helvetica, sans-serif">
            <strong>Global Message Inserted Successfully.</strong></font>
           </td>
          </tr>
S1
    }
    else {
        @I = split ( /\,/, $ids );

        foreach (@I) {
            s/\D//g;
            insert_message( $_, $obj_id, $txtmsg, $ts );
        }

        $sys_message = <<S2;
          <tr align="center" bgcolor="#FF0000">
           <td colspan="3" nowrap><font color="#FFFF00" size="2" face="Verdana, Arial, Helvetica, sans-serif">
            <strong>Message for $ids Inserted Successfully.</strong></font>
           </td>
          </tr>
S2
    }
}

###### Load the Default Template Template 

my $TMPL = &G_S::Get_Object( $db, 10053 );
unless ($TMPL) { &Err("Couldn't retrieve the template object."); exit; }

###### display Existing Vendors

$TMPL =~ s/%%SYS_MESSAGE%%/$sys_message/g;
$TMPL =~ s/%%SELF%%/$self/g;

print $q->header();

print $TMPL;

$db->disconnect;

1;

sub Err {
    my ($msg) = @_;
    print $q->header(), $q->start_html();
    print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
    print $q->end_html();
}

sub insert_message {
    my ($id) = @_;
    shift;
    my ($msg_id) = @_;
    shift;
    my ($msg) = @_;
    shift;
    my ($msg_expires) = @_;

    my ( $sth, $qry, $data, $rv );    ###### database variables

    if ( ! $msg_id ) { $msg_id = 'null'; }

    $qry = "INSERT INTO
          messages 
         ( 
          id,
          msg_id, 
          msg, 
          msg_expires
         ) VALUES ( \'$id\', $msg_id, \'$msg\', \'$msg_expires\' )";

    $sth = $db->prepare($qry)
     or die "Cannot prepare query $!\n";

    $rv = $sth->execute();
    defined $rv or die "Cannot execute $qry $!\n";

    $rv = $sth->finish();

}


###### 12/30/04 Added a check for the hour of 12PM, 
######		since 12 + 12 = 24 = 00 hours, according to the previous logic.
