#!/usr/bin/perl
# new_CGI_Object.cgi
# This utility will create a new CGI Object
#
# Written by Stephen Martin
# August 29th  2002
#
# Last modified:	Bill MacArthur
# Date:		03/09/16

=head1 new_CGI_Object.cgi

=for Commentary

new_CGI_Object.cgi
This utility will create a new CGI Object

Written by Stephen Martin
August 29th  2002

=cut

use strict;
use FindBin;
use lib "$FindBin::Bin/../../cgi-lib";
use DBI;
use CGI qw(:cgi);
use Carp;
#use POSIX qw(locale_h);
#use locale;
use URI::Escape;
use DHSGlobals;

require '/home/httpd/cgi-lib/ADMIN_DB.lib';

my $DB_error_contact = 'webmaster@dhs-club.com';
my ($db, $sth, $qry, $data);    ###### database variables
my $rv;
my $operator;
my $query;
my $public;
my $private;
my $spawn;

my $cgi = new CGI();

my $action          	= $cgi->param('action');
my $obj_id;
my $obj_name    	= $cgi->param('obj_name');
my $obj_description    	= $cgi->param('obj_description');
my $obj_value  		= $cgi->param('obj_value');
my $obj_type  		= $cgi->param('obj_type');
my $obj_translation_key = $cgi->param('translation_key');
my $obj_caller  	= $cgi->param('obj_caller');
my $active          	= $cgi->param('active');
my $public_view         = $cgi->param('public_view');
my $TMENU;
my $FMENU;
my $o_id;
my $o_type;
my $f_id;
my $f_process_name;
my $self = "./new_CGI_Object.cgi";
my $dhsglobalsclubshop_dot_com = DHSGlobals::CLUBSHOP_DOT_COM;

# Here is where we get our admin user and try logging into the DB

$operator = $cgi->cookie('operator'); my $pwd = $cgi->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'HTMLcontent.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

# Catch the add request

if ( $action eq "Add this Template")
{

        # Find a new id to use as an insert!

        $query = "select coalesce(max(obj_id),0) + 1 as obj_id from cgi_objects";

        $sth = $db->prepare($query);

        $rv = $sth->execute();
        defined $rv or die $sth->errstr;

        $sth->bind_columns(undef, \$obj_id);

        $sth->fetch();

	$rv = $sth->finish();

        $query   = "INSERT INTO cgi_objects (
                    obj_id,
                    obj_name,
                    obj_description,
                    obj_value,
                    obj_type,
                    obj_caller,
                    active,
                    public_view,
			translation_key ) VALUES (
                    ?,?,?,?,?,?,?,?,?)";

	$sth = $db->prepare($query);

	$rv = $sth->execute($obj_id,
                            $obj_name,
                            $obj_description,
                            $obj_value,
                            $obj_type,
                            $obj_caller,
                            $active,
                            $public_view,
				($obj_translation_key ? 'TRUE' : 'FALSE')
	);

	defined $rv or die $sth->errstr;

        $rv = $sth->finish();

        $db->disconnect;

        # redirect to Template management system print $cgi->header();

        print $cgi->header();
	
	
	
        print qq~
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
        <html>
        <head>
        <META HTTP-EQUIV="pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00">
        <META HTTP-EQUIV="Expires" CONTENT="NOW">
        <META HTTP-EQUIV="last modified" CONTENT="NOW">

         <LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
         <title>DHSC Insert a new CGI Object Success!!</title>

         </head>
         <body bgcolor="#eeffee">

          <script language="JavaScript" type="text/javascript">
		       if (confirm("Your newly created template is $obj_id. - Do you wish to add another CGI Object?")) 
       	    	{
       	   		location.href = "https://$dhsglobalsclubshop_dot_com/cgi/admin/new_CGI_Object.cgi";
       	   	}
			else
			{
       	   		location.href = "https://$dhsglobalsclubshop_dot_com/cgi/admin/HTMLcontent.cgi";
          		}
         </script>

           <noscript>
             <center>Your record ID=$obj_id was inserted successfully!</center>
           </noscript>
         </body>
         </html>
        
        \n~;

exit;
}


	$query = "SELECT id as o_id,
                	 type as o_type 
        	  FROM cgi_object_types WHERE id <> 10";
 
	$sth = $db->prepare($query);

        $rv = $sth->execute();
        defined $rv or die $sth->errstr;

        $sth->bind_columns(undef, \$o_id, \$o_type);

        while ( $sth->fetch()) 
        {
         $TMENU = $TMENU . "<option value=\"" . $o_id."\">" . $o_type ."<\/option>";
        }

        $rv = $sth->finish();


	$query = "SELECT id as f_id,
                	 process_name as f_process_name 
        	  FROM cgi_object_functions
        	  WHERE active=TRUE
        	  ORDER BY LOWER(process_name)";
 
	$sth = $db->prepare($query);

        $rv = $sth->execute();
        defined $rv or die $sth->errstr;

        $sth->bind_columns(undef, \$f_id, \$f_process_name);

        while ( $sth->fetch()) 
        {
         $FMENU = $FMENU . "<option value=\"" . $f_id."\">" . $f_process_name ."<\/option>";
        }

        $rv = $sth->finish();

print $cgi->header();

print qq~


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC Insert a new CGI Object (Template)</title>
<script language="JavaScript">
 function validate()
 {
  if ( document.vform1.obj_name.value == "" )
  {
   alert('Please enter a name for this template');
   document.vform1.obj_name.focus();
   return false;
  }
  
  if ( document.vform1.obj_description.value == "" )
  {
   alert('Please enter a description for this template');
   document.vform1.obj_description.focus();
   return false;
  }

  if ( document.vform1.obj_value.value == "" )
  {
   alert('Please enter some XML/HTML content for this template');
   document.vform1.obj_value.focus();
   return false;
  }

  if ( document.vform1.obj_type.selectedIndex == -1 )
  {
   alert('Please select an Object Type');
   document.vform1.obj_type.focus();
   return false;
  }

  if ( document.vform1.obj_caller.selectedIndex == -1 )
  {
   alert('Please select a calling process that uses this template');
   document.vform1.obj_caller.focus();
   return false;
  }
  
  

  return true;
 }
</script>
</head>

<body bgcolor="#eeffee">
<form action="$self" method="POST" name="vform1"
 onSubmit="return validate();">

<center>
<b>DHSC Insert a new CGI Object (Template)</b><br><br>
</center>
<table border="0" width="635" align="center">
  <tr>
  <td align="center" colspan="4" height="10">
  &nbsp;
  </td>
 </tr>
 <tr>
  <td align="center" colspan="4">
   <hr noshade align="center">
  </td>
 </tr>
</table>
<table border="0" width="635" align="center">
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Template Name</u>
          </td>
          </tr>

          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
           <input type="text" name="obj_name" size="40" maxsize="64"
           style="width : 620px;">
          </td>
          </tr>
        
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Description</u>
          </td>
          </tr>

          <tr>
          <td colspan="4" align="center"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><textarea name="obj_description" style="width : 620px;"
                        cols="50" rows="5"></textarea>
          </td>
          </tr>
          
          <tr>
          <td colspan="2"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Content</u>&nbsp;&nbsp;&nbsp;
         </td>
         <td colspan="2"
          style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
         </td>
         </tr>
          <tr>
          <td colspan="4" align="center"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><textarea name="obj_value" style="width : 620px;"
                        cols="50" rows="15"></textarea>
          </td>
          </tr>
          
          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627"><u>Type of Object</u>
          </td>
          </tr>

          <tr>
          <td colspan="4" 
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"
           width="627">
          <select name="obj_type" size="1">
           $TMENU         
          </select>
          </td>
          </tr>

          <tr>
          <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" width="627"><u>Calling Process</u>
          </td>
          </tr>


          <tr>
          <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
          <select name="obj_caller" size="1">
           $FMENU         
          </select>
          </td>
          </tr>

          <tr>
          <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height:180%;"><u>Translation Key?</u><br />
	<select name="translation_key"><option value="">No</option><option value="1">Yes</option></select>
          </td>
          </tr>

          <tr>
          <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px"><u>Active?</u>         
          </td>
         <td colspan="2"
          style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
         </td>
         </tr>
         
         <tr>
          <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" width="627">
          <select name="active" size="1">
           <option selected="selected" value="true">YES</option>
           <option value="false">NO</option>          
          </select>
          </td>
          </tr>
          
           <tr>
          <td colspan="4"
           style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
           <u>Public or Private</u>
         </td>
         </tr>

          <tr>
          <td colspan="4">
           <select name="public_view">
            <option value="1" >PUBLIC</option>
            <option value="0" selected="selected">PRIVATE</option>
           </select>
          </td>
         </tr>
         
          <tr>
          <td colspan="4">
           <hr noshade align="center">
         </td>
         </tr>
        <tr>
   <td colspan="4" align="right">
    <input type="submit" class="in" value="Add this Template" name="action">&nbsp;&nbsp;&nbsp;
    <input type="button" class="in" value="Cancel"
     onClick="window.close();">
   </td>
  </tr>

 <tr>
    <td width="615" colspan="4"></td>
  </tr>
</table>
</form>
<Script Language="JavaScript">
 document.vform1.obj_type.selectedIndex = -1;
 document.vform1.obj_caller.selectedIndex = -1;

</Script>
</body>
</html>
\n~;



1;

###### 05/14/03 - Added the template number to the JavaScript box that prompts
###### 		for another template creation. Stopped window from closing on Cancel.
# 03/09/16	added a criteria to the SQL that generates the functions list so as to not pull inactive items