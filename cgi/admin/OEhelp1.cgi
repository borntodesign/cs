#!/usr/bin/perl

print "content-type: text/html\n\n";
print "\n";

print qq~
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC CGI Help</title>
</head>
<body bgcolor="#eeffee">
<table border="0" width="100%" align="center">
  <tr>
  <th align="left"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top" nowrap>
 <b>DHSC Template Help</b>
  </th>
 </tr>
  <tr>
  <td align="left" nowrap
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top">
  You may make alterations to both the template description<br> and the template
  content.<br>
  <br>
  You can '<i>highlight</i>' the content section by pressing the '<i>highlight</i>'<br>
  button. On Microsoft. browsers this will also copy the content into<br>
  the paste buffer.<br>
  <br>
  A template may be made PUBLIC or PRIVATE by selecting the&nbsp;<br>
  appropriate value from the <u>Public or Private</u> dropdown menu.<br>
  <br>
  By default an 'archive' copy of the unaltered template is made<br>
  before the changes are written to the database. Subsequent versions<br>
  of a template may be recovered if this option is enabled.<br>
  <br>
  The '<i>Update</i>' button will commit your changes to the database.<br>
  <br>
  Once you have made your alterations, clicking on the&nbsp;<br>
  <u>Return to Search Results</u> will return you to the search results page.<br>
  </td>
 </tr>
<tr>
 <td height="15">&nbsp;
 </td>
</tr>
 <tr>
  <td align="left">
   <form>
   <input type="button" value="Close" name="execB"
    onClick="javascript:window.close();" class="in">
   </form>
  </td>
 </tr>
</table>
</body>
</html>
\n~;

exit;


