#!/usr/bin/perl -w
###### cancel_db.cgi
###### lookup a member cancellation records by id, alias or cancellation number
######
###### last modified 01/27/06 	Karl Kohrt
######               02/09/2011 g.baker  postgres 9.0 port SUBSTR arg 1
######                                   must be text

###### the variable HTML stuff is below in Action_Blocks, Block_1, etc.

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';
use lib ('/home/httpd/cgi-lib');
use G_S;
	
###### GLOBALS
my $MAIN_TMPL = './html/cancel_db.html';
my $q = new CGI;
my $action = ( $q->param('action') || '' );
my $id = ( uc $q->param('id') || '' );
my $memo = $q->param('memo') || '';
my $script_name = $q->script_name;

###### remove leading and trailing whitespace
$id =~ s/^\s*//; $id =~ s/\s*$//;

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($operator && $pwd)
{
	Err('No login detected. You must be logged in to use this interface.');
	exit;
}

my $db = ();
unless ($db = ADMIN_DB::DB_Connect( 'cancel_db.cgi', $operator, $pwd )){exit}
$db->{RaiseError} = 1;

###### START of ACTION TESTS
unless ($action)
{
	my $sth = $db->prepare("SELECT NOW()::DATE -1, NOW()::DATE");
	$sth->execute;
	my ($yesterday, $today) = $sth->fetchrow_array;
	$sth->finish;
	
	my $tmpl = G_S::Get_Object($db, 650) || die 'Failed to load template: 650';
	$tmpl =~ s/%%yesterday%%/$yesterday/;
	$tmpl =~ s/%%today%%/$today/;
	$tmpl =~ s/%%action%%/$script_name/g;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
	goto 'END';
}
elsif ($action eq 'date_range')
{
	my $start = G_S::PreProcess_Input($q->param('start'));
	my $end = G_S::PreProcess_Input($q->param('end'));
	my $status = G_S::PreProcess_Input($q->param('status'));

	unless ($start && $end)
	{
		Err('Start and End dates must be supplied even if they are the same.');
		goto 'END';
	}
	elsif (! $status || $status !~ /^\(.*\)$/ )
	{
		Err('Incorrect/empty status parameter received.');
		goto 'END';
	}

	my $sth = $db->prepare("
		SELECT c.id,
			m.alias,
			m.firstname || ' ' || m.lastname AS name,
			c.status,
			c.stamp::TIMESTAMP(0),
			c.init_stamp::TIMESTAMP(0),
			SUBSTR(c.memo,0,80) AS memo
		FROM 	members m,
			cancellations c
		WHERE 	m.id=c.id
		AND 	c.status IN $status
		AND 	c.stamp BETWEEN ? AND ?
		ORDER BY c.status, c.stamp, c.id");

	my $rv = $sth->execute($start, $end);

	print $q->header(-expires=>'now', -charset=>'utf-8');
	print $q->start_html(-bgcolor=>'#ffffff',
				-title=>'Cancellations by date range',
				-style=>'td{	font-family: arial,helvetica,sans-serif;
						font-size: 8pt;}
					th{	font-family: arial,helvetica,sans-serif;
						font-size: 9pt;}');
	print $q->h4("$status cancellations between $start &amp; $end");
	print qq|<p style="font-size: 9pt;"><a href=\"$script_name\">New Search</a>
		<br />Click on the alias to bring up the cancellation records.
		<br />Click on the name to bring up the member record.</p>\n|;

	if ($rv eq '0E0')
	{
		print "No matching cancellations found.", $q->end_html;
	}
	else
	{
		print qq|<p style="font-size: 10pt;"><b>$rv</b> records found.</p>\n|;
		print "<table><tr>
			<th>Alias/ID</th>
			<th>Status</th>
			<th>Name</th>
			<th>Initiated</th>
			<th>Last Change</th>
			<th>Memo</th></tr>\n";

		my $bgcolor = '#ffffee';
		while ( my $tmp = $sth->fetchrow_hashref )
		{
			$tmp->{memo} =~ s/\n/<br>/g;

			print qq|<tr bgcolor="$bgcolor">
				<td><a href="$script_name?id=$tmp->{id}&action=idsearch" target="_blank">$tmp->{alias}</a></td>
				<td>$tmp->{status}</td>
				<td><a href="/cgi/admin/memberinfo.cgi?id=$tmp->{id}" target="_blank">$tmp->{name}</a></td>
				<td>$tmp->{init_stamp}</td>
				<td>$tmp->{stamp}</td>
				<td>$tmp->{memo}</td>
				</tr>\n|;

			$bgcolor = ($bgcolor eq '#ffffee') ? '#eeeeee' : '#ffffee';
		}
		print "</table>\n";
		print "<p><a href=\"$script_name\">New Search</a></p>\n";
		print $q->end_html;
	}

	goto 'END';
}

unless ($id)
{
	Err("Missing ID");
	goto 'END';
}

###### since we'll need this whenever we need the memo, we may as well type it only once
$memo = $db->quote("Action performed by: $operator\r\n$memo");

if ($action eq 'idsearch' || $action eq 'pksearch')
{
	Display_Records($action);
}
elsif ($action eq 'new term')
{
	Term();
	Display_Records('idsearch');
}
elsif ($action eq 'update')
{
	Update();
	Display_Records('idsearch');
}
else{ Err('Missing parameters') }

END:

$db->disconnect;
exit;

sub Action_Block($$)
{
###### we are passed membertype and status
###### Block_1: termination
###### Block_2: cancellation
###### Block_3: cancellation/termination adjustments
	my ($mtype, $status) = @_;
	my $block = '';
	if ($mtype !~ /v/)
	{
		return '';
	}
	elsif (not $status)
	{
		$block = Block_1() . Block_2();
	}

###### suspension pending / termination waiting (suspended)
	elsif ($status =~/tp|sp|tw/)
	{
		$block = Block_3($status);
	}

###### if they have a pending cancellation we can allow terms or changes
###### since we will only allow drops through the members cancellation form (because of email notifications
###### we will need all 3 blocks
	elsif ($status =~ /cp/)
	{
		$block = Block_1() . Block_2() . Block_3($status);
	}

	elsif ($status =~ /Cp|cd|p/)
	{
		$block = Block_1() . Block_2();
	}
	else
	{
		return;
	}
	
	my $hdr = qq/
<!-- BORDER TABLE -->
<table border="1" cellspacing="0" cellpadding="4"><tr><td bgcolor="#ff3366">
/;
	my $ftr = "</td></tr></table>\n";
	return $hdr . $block . $ftr
}

sub Block_1
{
###### this would be our termination html block
###### putting these definitions in separate blocks makes modification/debugging easier (I think ;> )
	return <<BLK1;
<!-- TERMINATION ROW -->
<form action="/cgi/admin/cancel_db.cgi">
<input type="hidden" name="id" value="%%id%%">
<input type="hidden" name="action" value="new term">
<b>Terminate %%alias%%:</b>
<br>
Memo:<br>
<textarea cols="40" rows="2" name="memo" wrap="soft" style="font-size: 8pt;"></textarea>
&nbsp;&nbsp;<input type="submit" value="Terminate" style="font-size: 8pt; color: Red;">
</form>
</td></tr>
BLK1
}

sub Block_2
{
###### this would be our cancellation block
	return <<BLK2;
<!-- CANCELLATION ROW -->
<tr><td bgcolor="#ffffcc">
Cancellations must now be entered by logging in as the VIP and going through the Glocal Income cancellation interface.
<!--
<form action="/cgi/cancellation.cgi" target="_blank">
<input type="hidden" name="id" value="%%alias%%">
<input type="hidden" name="password" value="%%password%%">
<b>Start/Drop Cancellation for: %%alias%%</b>
<br>
Memo:<br>
<textarea cols="40" rows="2" name="memo" wrap="soft" style="font-size: 8pt;"></textarea>
&nbsp;&nbsp;
<input type="submit" value="Submit" style="font-size: 8pt;">
</form><i>Cancellation starts/drops here will go through the members cancellation app.</i>
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/admin/cancel_db_help.html#StartDrop" onclick="window.open(this.href,'help', 'width=400,height=350,scrollbars'); return false;">Help</a>
-->
</td></tr>
BLK2
}

sub Block_3
{
###### this would be our cancellation/termination adjustment block
	my $status = $_[0];
	my $txt = <<BLK3;
<!-- ADJUSTMENT ROW -->
<tr><td bgcolor="#ffffff">
<form action="/cgi/admin/cancel_db.cgi"><input type="hidden" name="action" value="update">
<input type="hidden" name="id" value="%%id%%">
<b>Adjust Cancellation/Termination for %%alias%%</b>
<br>
Memo:<br>
<textarea cols="40" rows="2" name="memo" wrap="soft" style="font-size: 8pt;"></textarea>
<br><br>Change status to: <select name="status" style="font-size: 8pt;">
BLK3
	
#	<option value=""></option> ###### eliminated this first empty line
###### we want to be selective about options since terms apply only to pending terminations
###### and cancellation options apply only to pending cancellations
	if ($status =~ /t|sp/)
	{
		$txt .= qq#<option value="tr">Rescind Termination</option>#;
	}
	else
	{
		$txt .= qq#<option value="Cp">Cancel Completely</option># if $status ne 'Cp';
###### leaving the following out because we want them to use the members app for these actions
###### doing so will preserve the email notifications that are integral with it
#		if ($status eq 'cd'){ $txt .= qq#<option value="cp">Restart Cancellation</option># }
#		else{ $txt .= qq#<option value="cd">Drop Cancellation</option># }
	}
		$txt .= <<XTRA;
</select>
&nbsp;&nbsp;&nbsp;
<input type="submit" value="Modify" style="font-size: 8pt;">
</form>
XTRA
	$txt .= "<i>Rescinded 'Terminations' will actually be deleted.</i>" if $status =~ /t/;
	return $txt;
}

sub Display_Records
{
	my $action = $_[0];
	my $columnDef = "c.pk = '$id'";
		
	if ($action eq 'pksearch' && $id =~ /\D/)
	{
		Err("Invalid cancellation ID: $id<br>letters not permitted");
		return;
	}
		
	###### if we're searching by id and it starts with letters we'll assume an alias
	elsif ($action eq 'idsearch' && $id =~ /^\D/)
	{
		$columnDef = "alias = '$id'";
	}

	###### else we'll assume a numeric ID
	elsif ($action eq 'idsearch')
	{
		$columnDef = "m.id = '$id'";
	}

	my ($qry, $qryArc) = ();

	###### we'll use a straight join if searching by cancellation.pk 
	######  because it will use the pk index
	if ($columnDef =~ /pk/)
	{
		$qry = "	SELECT c.*,
					m.firstname,
					m.lastname,
					m.alias,
					m.password,
					m.membertype
				FROM 	cancellations c,
					members m
				WHERE 	m.id = c.id
				AND 	$columnDef";

		$qryArc = "	SELECT c.*,
					m.firstname,
					m.lastname,
					m.alias,
					m.password,
					m.membertype
				FROM 	cancellations_arc c,
					members m
				WHERE 	c.id = m.id
				AND 	$columnDef
				ORDER BY c.stamp DESC";


	###### otherwise we'll do a left join to return some kind of info if there 
	###### is at least a match in the members table
	}else{
		$qry = "	SELECT COALESCE(c.pk::TEXT, '') AS pk,
					COALESCE(c.status, '') AS status,
					COALESCE(c.init_stamp::TEXT, '') AS init_stamp,
					COALESCE(c.stamp::TEXT, '') AS stamp,
					COALESCE(c.memo, '') AS memo,
					m.firstname,
					m.lastname,
					m.alias,
					m.password,
					m.membertype,
					m.id
				FROM 	members m
				LEFT JOIN cancellations c
				ON m.id = c.id
				WHERE 	$columnDef";

		$qryArc = "	SELECT COALESCE(c.pk::TEXT, '') AS pk,
					COALESCE(c.status, '') AS status,
					COALESCE(c.init_stamp::TEXT, '') AS init_stamp,
					COALESCE(c.stamp::TEXT, '') AS stamp,
					COALESCE(c.memo, '') AS memo,
					m.firstname,
					m.lastname,
					m.alias,
					m.password,
					m.membertype,
					m.id
				FROM 	members m
				LEFT JOIN cancellations_arc c
				ON 	m.id = c.id
				WHERE 	$columnDef
				ORDER BY c.stamp DESC";
	}
						
	my $sth = $db->prepare( $qry );
	$sth->execute;
	my $data = $sth->fetchrow_hashref;
	$sth->finish;

	$sth = $db->prepare( $qryArc );
	$sth->execute;
	my $arcdata = $sth->fetchrow_hashref;
	
	unless ($data || $arcdata)
	{
		Err("No match found for $id");
		$sth->finish;
		return;
	}

	###### since we'll abort if we have no data of any kind, we can safely give some values to $data from $arcdata
	###### this will allow identity placeholders to be filled
	unless ($data)
	{
# 		my %DATA = (	firstname =>	$arcdata->{firstname},
# 				lastname =>	$arcdata->{lastname},
# 				alias =>	$arcdata->{alias},
# 				id =>		$arcdata->{id},
# 				membertype => $arcdata->{membertype},
# 				status =>	$arcdata->{status},
# 				memo =>	$arcdate->{memo});
# 		$data = \%DATA;
		$data = $arcdata;
	}
	
	###### since we want line breaks in our memos to render correctly in an html table
	###### we'll substitute <br> for them
	$data->{memo} =~ s/\n/<br>/g;
	
	###### this keeps the cell looking nice if memo is null
	###### since we want all 'cells' to look the same for a void record though
	###### we put the nb in only if there is data in the row
	$data->{memo} = '&nbsp;' if ($data->{status} && not $data->{memo});

	my $arctable = '';

	if ($arcdata)
	{
		my $bgc = '#ffffff';
		do
		{
			$arcdata->{memo} =~ s/\n/<br>/g;
			$arcdata->{memo} = '&nbsp;' if ($arcdata->{status} && ! $arcdata->{memo});
			$arctable .= $q->Tr({-bgcolor=>$bgc},
				$q->td($arcdata->{pk}).
				$q->td({-align=>'center'}, $arcdata->{status}).
				$q->td($arcdata->{stamp}).
				$q->td($arcdata->{init_stamp}).
				$q->td($arcdata->{memo}));
			$bgc = ($bgc eq '#ffffff') ? ('#eeeeee') : ('#ffffff');
		} while ( $arcdata = $sth->fetchrow_hashref );
	}
	$sth->finish;
	
	my $refresh = $q->url() . "?id=$data->{id}&action=idsearch";
	
	open(PAGE, "$MAIN_TMPL") || die "Couldn't open the main template $MAIN_TMPL";
	print $q->header(-expires=>'now', -charset=>'utf-8');
	foreach (<PAGE>){
		$_ =~ s/%%Action Block%%/Action_Block($data->{membertype}, $data->{status})/e;
		$_ =~ s/%%refresh%%/$refresh/;
		$_ =~ s/%%id%%/$data->{id}/g;
		$_ =~ s/%%pk%%/$data->{pk}/g;
		$_ =~ s/%%status%%/$data->{status}/g;
		$_ =~ s/%%stamp%%/$data->{stamp}/g;
		$_ =~ s/%%init_stamp%%/$data->{init_stamp}/g;
		$_ =~ s/%%memo%%/$data->{memo}/g;
		$_ =~ s/%%password%%/$data->{password}/g;
		$_ =~ s/%%alias%%/$data->{alias}/g;
		$_ =~ s/%%firstname%%/$data->{firstname}/g;
		$_ =~ s/%%lastname%%/$data->{lastname}/g;
		$_ =~ s/%%membertype%%/uc $data->{membertype}/eg;
		$_ =~ s/%%archives%%/$arctable/;
		print $_;
	}
}

sub Err
{
	print $q->header(-charset=>'utf-8'), $q->start_html(), $q->h3($_[0]), $q->end_html();
}

sub Term
{
	my $active_merchant = '';
	my $merchants_status = $db->selectall_arrayref("	SELECT status
							FROM merchant_affiliates_master 
							WHERE member_id= $id;");
	# We may have multiple merchant records,
	#  so we check them all or until we find one active.
	foreach ( @$merchants_status )
	{
		# If we found an active merchant, mark it and exit the for loop.
		if ( @$_[0] == 1 ) { $active_merchant = @$_; last; }
	}
	# If we found an active merchant, skip this cancellation.	
	if ($active_merchant)
	{
		Err("Skipped - This ID is associated with at least one active merchant account.<br>\n");
	}
	# Otherwise, process the cancellation.
	else
	{
		# first we'll delete any existing cancellation record to prevent conflict with the unique ID index
		$db->do("DELETE FROM cancellations WHERE id = '$id'");
		$db->do("INSERT INTO cancellations (id, status, memo) VALUES ($id, 'tp', $memo)");
	}
}

sub Update
{
	my $status = $q->param('status');
	unless ($status)
	{
		Err("Missing 'status' parameter");
	}
	else
	{
		$db->do("UPDATE cancellations
			SET status = '$status',
			stamp = NOW(),
			memo = $memo
			WHERE id = $id");
	}
	
	if ($status eq 'tr')
	{
###### since we'll want to record any comments passed and the fact that we're rescinding the termination
###### we first did an update and now we'll delete the record so it will move to the archive
###### this is so that if the member decides to cancel, a notification of existing status won't come up
		$db->do("DELETE FROM cancellations WHERE id = $id");
	}
}

###### 12/18/01 added handling for suspended memberships
###### 02/25/02 reordered the elsifs in the block selection because tp were being matched on the p and not
###### including block 3
###### Changed the location of cgi.ini from ../Admin_files/cgi.ini to ../../cgi-lib/cgi.ini
###### 08/21/03 changed to the Admin_DB connector, reformatted for easier readability
###### and fixed the condition where the report would fail if there were no archived records
###### 09/02/03 fixed uninitialized var errors when working with empty cancellation recordsets
###### 01/14/04 added handling for searching cancellations by date
###### 07/22/04 Added the check for an associated active merchant account to the Term subroutine.
###### 01/27/06 Added -charset=>'utf-8' to the header statements for the 8.1 DB upgrade.

