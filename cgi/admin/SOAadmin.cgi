#!/usr/bin/perl -w
###### SOAadmin.cgi
###### Club Accounts Administration Engine
###### originally written by Steven Martin as Statement of Accounts admin
###### last modified 06/03/2015	Bill MacArthur

use strict;
require Time::DaysInMonth;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use G_S;
use Digest::MD5 qw(md5 md5_hex md5_base64);
use ADMIN_DB;
require cs_admin;

binmode STDOUT, ":encoding(utf8)";

###### Globals
my $TmplID = 10120; 
my($meminfo, $db, $sth, $data, $html, %CsiClasses) = ();
my $q = new CGI;
$q->charset('utf-8');
my $id = my $firstname = my $lastname = '';
my $qry;
my $qualifyingSQL = '';
my $self = $q->url();
my $MON;
my $NOW;
my $MENU;
my $STARTMENU;
my $ENDMENU;
my $PERIOD;
my $TOTAL;
#my $des;
#my $des_in;
#my $o;
my $editor_link = "/cgi/admin/SOViewer.cgi";
my $csiclass = 1;	# just a global for maintaining an HTML class for CSI

my @MONTHS = (
    '_UNDEF_', 'January', 'February', 'March',     'April',   'May',
    'June',    'July',    'August',   'September', 'October', 'November',
    'December'
);

my %MEMBERS = %G_S::MEMBER_TYPES;

unless ( $q->https )
{
    print $q->redirect( 'https://www.clubshop.com' . $q->script_name() );
    exit;
}

#my $operator = $q->cookie('operator');
#my $pwd      = $q->cookie('pwd');

###### my parameters alias, pwd
my %param = ();    ###### parameter hash

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
}
$param{'action'} ||= '';
#$o = $param{o};
$id = $param{id};
die "Invalid id parameter: $id\n" if $id =~ m/\D/;

##### 1st step calculate and encrypted MD5 of the remote IP #####

#$des = md5_hex( $ENV{'REMOTE_ADDR'} );

#if ( !( defined $operator ) ) {
#    print $q->header(-charset=>'utf-8'), $q->start_html(),
#      $q->h3("Incorrect login.<br />Back up and try again"), $q->end_html();
#    exit(0);
#}
#
#exit unless $db = ADMIN_DB::DB_Connect( 'SOAreport.cgi', $operator, $pwd );
if ($q->cookie('cs_admin'))
{
	my $cs = cs_admin->new({'cgi'=>$q});
	my $creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
# 	if (&ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1){
# 		Err($q->h3('You must be a member of the accounting group to use this form'));
# 		goto 'END';
# 	}
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}
$db->{'RaiseError'} = 1;

###### Ensure that this program is entered with an id !

unless ($id)
{
    Err( $q->h3('The id parameter is missing?') );
    goto 'END';
}

$meminfo = getmemberdetails($id);

###### Validate re-entry into program 

#if ( $param{'action'} )
#{
#    $des_in = $o;
#
#    if ( $des_in ne $des )
#	{
#        Err( $q->h3('Internal Security Violation 1') );
#        goto 'END';
#    }
#
#}

###### Trap action and amend SQL as appropriate 

$NOW = get_slice();

if ( $param{'action'} eq "SMON" ) {

    ###### build query extension to retrieve within this date range

    $MON           = $param{ym};
    $qualifyingSQL = gen_SQL($MON) . " ORDER BY soa.entered, soa.trans_id";
    $html           = single_month( $id, $qualifyingSQL );
    $PERIOD         = get_period($MON);
    $MENU           = gen_dropdown($MON);
}
elsif ( $param{'action'} eq "FMON" )
{
    unless ( $param{'bm'} )
	{
        Err( $q->h3('The begin month parameter is missing?') );
    }

    unless ( $param{'em'} )
	{
        Err( $q->h3('The end month parameter is missing?') );
    }

    $MON    = $NOW;
    $PERIOD = $param{'bm'} . " to " . $param{'em'};
    $MENU   = gen_dropdown($MON);

    $html  = slice_month( $param{'bm'}, $param{'em'} );
}
else
{
    ###### by default use the current month and the prior
	$MON = $NOW;
	my ($bm, $em) = $db->selectrow_array("
		SELECT SUBSTR((first_of_another_month((first_of_month() - INTERVAL '1 day')::DATE))::TEXT,1,7),
		SUBSTR((last_of_month())::TEXT,1,7)");
	$PERIOD = "$bm to $em";
#    $qualifyingSQL = gen_SQL($NOW) . " ORDER BY soa.entered ASC";
#    $html           = single_month( $id, $qualifyingSQL );
#    $PERIOD         = get_period($MON);
    $MENU           = gen_dropdown($MON);
	$html  = slice_month( $bm, $em );
}

###### Get Template 

$STARTMENU = gen_start_menu();
$ENDMENU   = gen_end_menu();

my $TMPL = G_S::Get_Object( $db, $TmplID );
unless ($TMPL)
{
	Err("Couldn't retrieve the template object: $TmplID");
	goto 'END';
}

print $q->header( '-charset'=>'utf-8', '-expires' => 'now' );

#$data->{total} = sprintf( "%.2f", $data->{total} );

$TMPL =~ s/%%SOA_ROWS%%/$html/g;
$TMPL =~ s/%%FIRSTNAME%%/$meminfo->{firstname}/g;
$TMPL =~ s/%%LASTNAME%%/$meminfo->{lastname}/g;
$TMPL =~ s/%%ACCOUNT_BALANCE%%/$meminfo->{'account_balance'}/g;
$TMPL =~ s/%%TRANSFERRABLE_BALANCE%%/$meminfo->{'transferable'}/;
$TMPL =~ s/%%WITHDRAWABLE_BALANCE%%/$meminfo->{'withdrawable'}/;
$TMPL =~ s/%%transferrable_items_total%%/RedIfNeg($meminfo->{'transferrable_items_total'})/e;
$TMPL =~ s/%%TOTAL%%/RedIfNeg(sprintf '%8.2f', $TOTAL)/eg;
$TMPL =~ s/%%SELF%%/$self/g;
$TMPL =~ s/%%MONTHS%%/$MENU/;
$TMPL =~ s/%%PERIOD%%/$PERIOD/g;
$TMPL =~ s/%%ID%%/$id/g;
#$TMPL =~ s/%%O%%/$des/g;
$TMPL =~ s/%%O%%//g;
$TMPL =~ s/%%MEMTYPE%%/$MEMBERS{$meminfo->{membertype}}/g;
$TMPL =~ s/%%ALIAS%%/$meminfo->{alias}/g;
$TMPL =~ s/%%BEGINMONTHS%%/$STARTMENU/g;
$TMPL =~ s/%%ENDMONTHS%%/$ENDMENU/g;
$TMPL =~ s/%%ACCT_TOTAL%%/RedIfNeg($db->selectrow_array("
	SELECT SUM(amount)::NUMERIC(8,2) FROM soa
	WHERE void=FALSE AND id=$id"))/ex;

my $tin_info = $meminfo->{'tin'} ? qq#<tr><td class="lbs1">Tax ID: </td><td class="lbs1">$meminfo->{'tin'}</td></tr># : '';

$TMPL =~ s/%%TIN%%/$tin_info/g;

print $TMPL;

END:
$db->disconnect;
exit;

sub CsiClass
{
	my $csd = shift;

	unless ($CsiClasses{$csd})
	{
		$csiclass = 1 if $csiclass > 4;
		$CsiClasses{$csd} = $csiclass;
		$csiclass++;
	}
	
	return $CsiClasses{$csd}
}

sub Err
{
    print $q->header(-charset=>'utf-8'), $q->start_html();
    print $_[0];
    print $q->end_html();
}

sub get_slice
{
    my $mday;
    my $mon;
    my $year;

    ( $mday, $mon, $year ) = (localtime)[ 3, 4, 5 ];

    $mon++;

    $year = $year + 1900;

    return $year . "-" . sprintf( "%02d", $mon );
}

sub gen_dropdown
{
    ###### Generate a dropdown of -12 months to now defaulted to whatever was selected in $MON

    my ($m) = @_;    # MON selected value

    my $cy;
    my $cm;

    my $py;
    my $pm;

    my $i;

    my $html;

    my $selected;

    ####### Find now -12 months 

    ( $cy, $cm ) = split ( /-/, $NOW );

    $html = "";

    for ( $i = 0 ; $i < 12 ; $i++ ) {
        $pm = $cm - $i;
        $py = $cy;

        if ( $pm <= 0 ) {
            $pm = $pm + 12;
            $py = $py - 1;
        }

        $pm = sprintf( "%02d", $pm );

        if ( $py . "-" . $pm eq $m ) { $selected = " SELECTED "; }
        else { $selected = ""; }

        $html = $html
          . "<option value=\""
          . $py . "-"
          . $pm
          . "\" $selected >"
          . $MONTHS[$pm] . " - "
          . $py
          . "</option>\n";
    }
    return $html;
}

sub get_period
{

    my ($m) = @_;

    my $cy;
    my $cm;

    ( $cy, $cm ) = split ( /-/, $m );

    return $MONTHS[$cm] . " - " . $cy;
}

sub gen_SQL
{
    my ($m) = @_;

    my $cy;
    my $cm;

    ( $cy, $cm ) = split ( /-/, $m );

	return " AND ( date_part('years',entered) = $cy )  AND ( date_part('months',entered) = $cm ) ";
}

sub getmemberdetails
{
    my ($id) = @_;

    my $sth = $db->prepare("
		SELECT
            		--oid,
			*, 
			coalesce(prefix,\'\') || \' '\ || firstname || \' \' ||  lastname as memname,
			'' AS paid_thru
		FROM members
		WHERE id =  ?");

    $sth->execute($id);

    my ($data) = $sth->fetchrow_hashref;
    $sth->finish();

	($data->{'account_balance'}, $data->{'transferable'}, $data->{'withdrawable'}, $data->{'transferrable_items_total'}) = $db->selectrow_array("
		SELECT account_balance, transferrable_balance, withdrawable_balance, transferrable_items_total
		FROM soa_account_balance_payable WHERE id= $data->{'id'}");
    return $data;
}

sub gen_start_menu {
    my ( $sth, $qry, $data, $rv );    ###### database variables
    my $html;
    my $y;
    my $m;

    $qry = qq!
select 
 DISTINCT date_part('years',entered ) || '-' || lpad((date_part('months',entered ))::TEXT,2,'0') as start_date
from 
 soa
 WHERE id= ?
order by date_part('years',entered ) || '-' || lpad((date_part('months',entered ))::TEXT,2,'0') asc!;

    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);

    $html = "";
    while ( $data = $sth->fetchrow_hashref ) {
        ( $y, $m ) = split ( /-/, $data->{'start_date'} );

        $html .= qq|
<option value="$data->{'start_date'}">$MONTHS[$m] - $y</option>
|;
    }
    $rv = $sth->finish();

    return $html;

}

sub gen_end_menu
{
    my ( $sth, $qry, $data, $rv );    ###### database variables
    my $html;
    my $y;
    my $m;

    $qry = qq!
select 
 DISTINCT date_part('years',entered ) || '-' || lpad((date_part('months',entered ))::TEXT,2,'0') as start_date
from 
 soa
 WHERE id= ?
order by date_part('years',entered ) || '-' || lpad((date_part('months',entered ))::TEXT,2,'0') desc!;

    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);

    $html = "";
    while ( $data = $sth->fetchrow_hashref )
    {
        ( $y, $m ) = split ( /-/, $data->{'start_date'} );

        $html .= qq|<option value="$data->{'start_date'}">$MONTHS[$m] - $y</option>|;
    }
    $rv = $sth->finish();

    return $html;
}

sub PrintRow
{
	my ($data, $class) = @_;
	
	###### update my class if this transaction is voided
	$class .= ' tr-void' if $data->{'void'};
	
	# if this is a statement item, add a class for that
	my $csi = $data->{'statement_date'} ? ' csi' . CsiClass($data->{'statement_date'}) : '';
#<a href="$editor_link?id=$id;trans_id=$data->{'trans_id'};o=$des" target="_blank">$data->{'trans_id'}</a>
	return qq#<tr class="$class $csi">
		<td>
		<a href="$editor_link?id=$id;trans_id=$data->{'trans_id'}" target="_blank">$data->{'trans_id'}</a>
		</td>
		<td>$data->{'trans_type_desc'}</td>
		<td class="amt${\(($data->{'amount'} || 0) < 0 ? ' neg':'')}">$data->{'amount'}</td>
		<td class="amt${\(($data->{'display_amount'} || 0) < 0 ? ' neg':'')}">$data->{'display_amount'}</td>
		<td>$data->{tdesc}</td>
		<td>$data->{memo}</td>
		<td>$data->{entered}</td>
		<td>$data->{batch}</td>
		<td>#
		.
#		($data->{'ref_trans_id'} ? qq|<a href="$editor_link?id=$id;trans_id=$data->{'ref_trans_id'};o=$des" target="_blank">$data->{'ref_trans_id'}</a>| : '')
		($data->{'ref_trans_id'} ? qq|<a href="$editor_link?id=$id;trans_id=$data->{'ref_trans_id'}" target="_blank">$data->{'ref_trans_id'}</a>| : '')
		.
		qq#</td>
		<td>$data->{reconciled}</td>
		<td>$data->{'void'}</td>
		<td class="neg">$data->{'frozen'}</td>
		<td>$data->{'transferable'}</td>
		<td>$data->{'withdrawable'}</td>
		<td class="$csi">$data->{'comm_statement_item'}</td>
		<td class="amt${\(($data->{'csi_display_amount'} || 0) < 0 ? ' neg':'')} $csi">$data->{'csi_display_amount'}</td>
		<td class="ac $csi">$data->{'comm_month_ending'}</td>
		<td class="ac $csi">$data->{'statement_date'}</td>
		</tr>\n#;
}

sub RedIfNeg
{
	###### a complete hack to make the values appear Red if they are negative
	###### ... it was either this or some other hack ;)
	my $v = shift;
	return $v unless $v < 0;
	return $q->span({'-style'=>'color:red'}, $v);
}

sub single_month
{
    my ($id, $qualifyingSQL) = @_;

    my $html = '';
    my $rc = 0;
	
    ###### get current statement of accounts for the id for this single month # 

#    $qry = _baseQry() . "( soa.id = ? ) ORDER BY soa.entered, soa.trans_id";

#    $qry .= $qualifyingSQL;
	$qry = _baseQry() . "( soa.id = ? )" . $qualifyingSQL;
#die $qry;
    $sth = $db->prepare($qry);
    $sth->execute($id);

	my $class = '';
    while ( $data = $sth->fetchrow_hashref )
    {
		G_S::Null_to_empty($data);
		$class = ($rc % 2 == 0) ?  'lbs1' : 'lbs2';
		$html .= PrintRow($data, $class);
		$rc++;
		$TOTAL += $data->{'amount'} unless $data->{'void'};
    }

    $sth->finish;

    if ( $rc == 0 )
    {
        $html = '<tr><td colspan="18" align="center" class="lbs2">*** There are no transactions for this month ***</td></tr>';
    }

    return $html;

}

sub slice_month
{
    my ($bm, $em) = @_;
    my $y;
    my $m;
    my $d;

    my $start;
    my $end;
    my $temp;

    my $html;
    my $rc;

    # Now re-arrange the dates if they are backward so we go from
    # low to high

    if ( $em lt $bm )
    {
        my $temp = $em;
        $em = $bm;
        $bm = $temp;
    }

    # construct a fully qualified start and end date.
    # YYYY-MM  -->  YYYY-MM-(start date or end date)

    ( $y, $m ) = split ( /-/, $em );

    $d = Time::DaysInMonth::days_in( $y, $m );

    $d = sprintf( "%02d", $d );

    $end = $y . "-" . $m . "-" . $d;

    ( $y, $m ) = split ( /-/, $bm );

    $start = $y . "-" . $m . "-" . "01";

    $qry = _baseQry() . "( soa.id = ? ) AND 
		soa.entered::DATE BETWEEN ? AND ?
      ORDER BY soa.entered, soa.trans_id";

    $sth = $db->prepare($qry);
    $sth->execute( $id, $start, $end );

    $html = "";

    $rc = 0;

	my $class = '';
    while ( $data = $sth->fetchrow_hashref )
    {
		G_S::Null_to_empty($data);
		$class = ($rc % 2 == 0) ?  'lbs1' : 'lbs2';
		$html .= PrintRow($data, $class);
		$rc++;
		$TOTAL += $data->{'amount'} unless $data->{'void'};
    }

    $sth->finish;

    if ( $rc == 0 )
    {
        $html = '<tr><td colspan="18" align="center" class="lbs2">*** There are no transactions for this month ***</td></tr>';
    }

    return $html;
}

sub _baseQry
{
	return "SELECT 
        soa.trans_id, 
        soa.id, 
        COALESCE(soa.amount::TEXT, '') AS amount,
        CASE WHEN soa.comm_statement_item IS NULL THEN COALESCE(soa.display_amount::TEXT, '') ELSE '' END AS display_amount, 
        SUBSTR(soa.description,0,35) as tdesc, 
        SUBSTR(soa.memo,0,35) AS memo,
        DATE_TRUNC('second',soa.entered) AS entered,
        soa.batch,
		CASE WHEN soa.void=TRUE THEN 'x' ELSE '' END AS void,
        CASE
         WHEN soa.reconciled = true THEN 'x'
         ELSE ''
        END  as reconciled,
        soa.operator, 
        soa_transaction_types.name AS trans_type_desc,
		soa.ref_trans_id,
        CASE
         WHEN soa.frozen = TRUE THEN 'x'
         ELSE ''
        END AS frozen,
        CASE
         WHEN soa_transaction_types.transferrable = TRUE THEN 'x'
         ELSE ''
        END AS transferable,
        CASE
         WHEN soa_transaction_types.can_withdraw = TRUE THEN 'x'
         ELSE ''
        END AS withdrawable,
        CASE
         WHEN soa.comm_statement_item = TRUE THEN 'x'
         ELSE ''
        END AS comm_statement_item,
        CASE WHEN soa.comm_statement_item=TRUE AND soa.comm_statement_period IS NOT NULL THEN COALESCE(soa.display_amount, soa.amount)::TEXT ELSE '' END AS csi_display_amount,
        COALESCE(p.close::TEXT,'') AS comm_month_ending,
        COALESCE(p.comm_statement_date::TEXT,'') AS statement_date

      FROM 
        soa
      INNER JOIN soa_transaction_types
      	ON soa.trans_type = soa_transaction_types.id
      LEFT JOIN periods p
      	ON p.period=soa.comm_statement_period
      	AND p.comm_statement_date IS NOT NULL
      WHERE 
      ";
}

##### 04/06/2003 - modified interval statements as noted below for postgres7.3:
#####  ...now() + interval('12 month') ) + 1 as paid_thru...
#####  became:
#####  ...cast(now() + interval '12 month' + interval '1 day' AS date) as paid_thru...
###### 11/04/03 removed the stuff about the Partner date in the getmemberdetails SQL
###### also commented out the use Time::... module
###### 06/07/07 put into production after making sub for the row printout function
###### also compliled the $TOTAL from the list instead of a separate aggregate
###### disabled the *_total routines and employed an account totaling routine (that value is more valuable)
###### also removed the accounting group check since this is simply a report with no sensitive info
###### 09/17/07 changed the sort order in the SQL
###### 11/13/08 revised the gen_*_menu routines to query by ID instead of a full table scan
###### also commented out the oid in the getmemberdetails SQL
###### 12/24/08 fine tuned the SQL for looking up by date to make it really DATE instead of testing timestamp against dates
###### 09/07/11 added handling for the 'frozen' flag
###### 03/02/15 added certain balance summaries to getmemberdetails()
# 05/12/15 added the binmode to deal with the changes in the recent upgrade of DBD::Pg
# 06/03/15 Removed the requirement for the "o" parameter. Also integrated cs_admin for authentication instead of the operator/password cookies