#!/usr/bin/perl

print "content-type: text/html\n\n";
print "\n";

print qq~


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="Tue, 26-Oct-1965 12:00:00"> 
<META HTTP-EQUIV="Expires" CONTENT="NOW"> 
<META HTTP-EQUIV="last modified" CONTENT="NOW"> 

<LINK HREF="/css/memberinfo.cgi.css" REL="stylesheet" TYPE="text/css">
<title>DHSC CGI Help</title>
</head>
<body bgcolor="#eeffee">
<table border="0" width="100%" align="center">
  <tr>
  <th align="left"
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top" nowrap>
 <b>DHSC Template Help</b>
  </th>
 </tr>
  <tr>
  <td align="left" nowrap
   style="font-family: Arial, Helvetica, sans-serif; font-size: 12px" valign="top">
  This is the main template management page. From this page<br>
  you can search for a specific template. by entering either a<br>
  keyword, an id number, or part of the object name.&nbsp; Searching<br>
  by template type or program name is also possible.<br>
  <br>
  by clicking on the id# hyperlink, you can access the&nbsp;<br>
  '<u>Template Editor</u>'&nbsp; , whilst clicking on the '<u>RESTORE</u>' link
  will<br>
  present you with a list of recoverable templates for that object.
  </td>
 </tr>
<tr>
 <td height="15">&nbsp;
 </td>
</tr>
 <tr>
  <td align="left">
   <form>
   <input type="button" value="Close" name="execB"
    onClick="javascript:window.close();" class="in">
   </form>
  </td>
 </tr>
</table>
</body>
</html>


\n~;

exit;


