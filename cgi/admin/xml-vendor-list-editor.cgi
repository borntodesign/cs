#!/usr/bin/perl -w
###### xml-vendor-list-editor.cgi
###### admin script to vendor XML docs in the imall
###### created: 01/03/07	Bill MacArthur
###### last modified: 04/25/07	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use ADMIN_DB;

###### GLOBALS
my $q = new CGI;
my $work = 'vendors.xml';	# the file we will be working on

# value is a required flag
my %collist = (name=>1, vendor_id=>1, ad_id=>0, vrp=>1, mrp=>1, pp=>1, mp=>0, id=>0, gift_card=>0);
my (%cols, $db, $file) = ();

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

###### we could have tested for the admin flag, but just in case we have someone trying to access things
###### without going through the admin interface, we may be able to stop 'em here
if ($operator && $pwd)
{
	unless ($db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd )){exit}

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
# 	if (&ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1){
# 		Err($q->h3('You must be a member of the accounting group to use this form'));
# 		goto 'END';
# 	}
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}

my $caller = $q->param('filename') || die "Failed to receive a filename parameter\n";
$caller =~ m#(/.*/)#;
my $cwd = $1;
my @catids = $q->param('catid');
my $action = $q->param('action') || 'update';
my $backup_filename = $q->param('backup_file');
foreach (keys %collist)
{
	$cols{$_} = $q->escapeHTML($q->param($_)) || '';
}

if ($action eq 'update')
{
	CheckParamCols();
	Open();
	if ($cols{id}){
		die "Update failed to find the node" unless $file =~ s#<vendor.*?id="$cols{id}".*?</vendor>#UpdateNode()#e;
	} else {
		die "Update failed to find the node" unless $file =~ s#<vendor.*?vendor_id="$cols{vendor_id}".*?</vendor>#UpdateNode()#e;
	}
       open (OUT, '>' . $cwd . $work) || die 'Failed to open ' . $cwd . $work . " for writing\n";
	print OUT $file;
	close OUT;
	my $loc = ();
	($loc = $caller) =~ s#/home/httpd/html##s;
	$loc .= '?'. ($cols{id} ? "node_id=$cols{id}" : "vendor_id=$cols{vendor_id}");
	print $q->redirect($loc);
}
elsif ($action eq 'Add Vendor')
{
        CheckParamCols();
        Open();
        die "Update failed to add the node" unless $file =~ s#</vendors>#NewNode() . '</vendors>'#e;
        open (OUT, '>' . $cwd . $work) || die 'Failed to open ' . $cwd . $work . " for writing\n";
        print OUT $file;
        close OUT;
        my $loc = ();
        ($loc = $caller) =~ s#/home/httpd/html##s;
	$loc .= '?'. ($cols{id} ? "node_id=$cols{id}" : "vendor_id=$cols{vendor_id}");
	print $q->redirect($loc);
}
elsif ($action eq 'Delete')
{
	Open();
	CheckParamCols();
	if ($cols{id}){
		die "Delete failed to find the node with id: $cols{id}" unless $file =~ s#(<vendor.*?id="$cols{id}".*?</vendor>)#DeleteNode($1)#e;
	} else {
		die "Delete failed to find the node with vendor_id: $cols{vendor_id}" unless $file =~ s#(<vendor.*?vendor_id="$cols{vendor_id}".*?</vendor>)#DeleteNode($1)#e;
	}
	open (OUT, '>' . $cwd . $work) || die 'Failed to open ' . $cwd . $work . " for writing\n";
	print OUT $file;
	close OUT;
	print $q->header, $q->start_html, $q->p('Deletion successful.'),
		$q->p($q->a({-href=>'#', -onclick=>'self.close();return false;'}, 'Close Window')),
		$q->end_html;
}
else { Err("Undefined Action: $action") }
$db->disconnect;
exit;

sub CheckParamCols
{
	foreach (keys %collist)
	{
        	die "The $_ is required" if $collist{$_} && length $cols{$_} == 0;
	}
}
sub DeleteNode
{
	my $node = shift;
	$node =~ s#^<#("<!-- 'deleted' by $operator on ") . scalar localtime() . ' '#e;
	$node =~ s#>$#-->#;
	return $node;
}
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Open
{
        ###### slurp in whole files in a single bound
        undef $/;
        open (IN, $cwd . $work) || die 'Failed to open ' . $cwd . $work . " for reading\n";
        $file = <IN>;
        close IN;
        if ($backup_filename)
        {
                open (BACK, '>' . $cwd . $backup_filename . ".$work") || die 'Failed to open backup file: ' . $cwd . $backup_filename . ".$work\n";
                print BACK $file;
                close BACK;
        }
}

sub NewNode
{
	return UpdateNode(time) . "\n";
}

sub UpdateNode
{
	my $id = shift;
	my $rv = '<vendor ';
	if ($id){
		# we will receive an 'id' value on a new node creation
		$rv .= qq|id="$id" |;
	} elsif ($cols{id}){
		$rv .= qq|id="$cols{id}" |;
	} else {
		# we'll assign an id attribute on every update where the attribute does not exist
		$rv .= qq|id="| . time . qq|" |;
	}

	$rv .= qq|name="$cols{name}" vendor_id="$cols{vendor_id}" vrp="$cols{vrp}" mrp="$cols{mrp}" pp="$cols{pp}"|;
	$rv .= " ad_id=\"$cols{ad_id}\"" if $cols{ad_id};
	$rv .= ' mp="1"' if $cols{mp};
	$rv .= '>';
	$rv .= "<catid>$_</catid>" foreach @catids;
	$rv .= "<gift_card>$cols{gift_card}</gift_card>" if $cols{gift_card};
	$rv .= '</vendor>';
	return $rv;
}

# 02/13/07 added handling for the ad_id parameter
# 03/08/07 added handling for 'id' attributes
# 03/19/07 instituted id attribute creation even for updates
###### 04/13/07 added handling for either node id or vendor id based editting
# 04/25/07 tweaked the error messages in the node deletion block to help identify a stumbling block
###### 07/26/07 added handling for the gift_card node
