#!/usr/bin/perl -w
###### SQLExec.cgi
###### a simple interface to allow staff to perform freeform queries without having to use Access
###### it is only good for simple queries as it will render a textual list as if they executed the query within psql
###### since that is exactly what we are going to do
###### created: 07/13/10	Bill MacArthur
###### last modified: 02/29/16	Bill MacArthur

use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;

my $QryRecipient = 'b.macarthur@dhs-club.com';
my $q = new CGI;
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged into the DB to use this interface" unless $operator && $pwd;

my $sql = $q->param('sql');
###### filter out leading whitespace
$sql =~ s/^\s*//;
unless ($sql){
	Interface();
}
else {
	my $rv = CkSQL();
	die $rv if $rv;
	# and just to try being a little more safe
#	$sql = "\\set AUTOCOMMIT off\n$sql";	# tried various methods, but the result is the same... no results displayed
	print $q->header('-type'=>'text/plain','-charset'=>'utf-8');
	$rv = qx/PGPASSWORD=$pwd\nexport PGPASSWORD\npsql network -h 192.168.17.2 -U $operator -c "$sql" 2>&1/;
	$rv ||= $!;
	print $rv;
}
exit;

sub CkSQL
{
	# we will add the trailing ; if it is not already present
	$sql .= ';' unless $sql =~ /;\s*$/m;
	return 'The word "delete" cannot be used in the query' if $sql =~ /delete/i;
	return 'The word "update" cannot be used in the query' if $sql =~ /update/i;
	return 'The query must begin with "SELECT|WITH|CREATE TEMP TABLE"' if $sql !~ /^(SELECT|WITH|CREATE TEMP TABLE)/i;
	my @c = $sql =~ /;/g;
	return 'You can only execute one statement within your query. A statement is defined by a trailing semicolon' if scalar @c > 1 && $sql !~ /CREATE TEMP TABLE/;
	
	sendmail(
		'Subject'=>'Query executed',
		'To'=>$QryRecipient,
		'From'=>'"SQLExec.cgi"<clubshop@www.clubshop.com>',
		'Message'=>"Executed by: $operator\n\n$sql");
	return undef;
}

sub Interface
{
	print
		$q->header(-charset=>'utf-8'),
		$q->start_html(
			-title=>'SQL Executer',
			-style=>{code=>'body {font-size:90%;}'}
		),
		$q->h4('SQL Executer'),
		'<div>The rules of the game<ul><li>SELECT queries only</li><li>No UPDATE or DELETE queries :)</li>
		<li>If you really booger something up, let IT know</li><li>Simple queries with small result sets only</li>
		<li>Your query results will open in a new window</li>
		<li>All queries run are logged... don`t be going where you shouldn`t</li>
		</ul></div>',
		$q->start_form(-target=>'_blank'),
		$q->textarea(
			-name=>'sql',
			-rows=>30,
			-cols=>100,
			-style=>'width:100%; background-color:rgb(240,255,240);'),
		$q->reset, ' ', $q->submit,
		$q->end_form, $q->end_html;
}

# 05/28/14 added charset to the results header
# 02/29/16 removed white-space:nowrap from the CSS for the textarea
