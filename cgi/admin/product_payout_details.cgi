#!/usr/bin/perl -w
# product_payout_details.cgi
# create/update mall_vendor_product_payout_details records
# created April. 2008	Bill MacArthur
# last modified: 12-23-15	Bill MacArthur

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
#use G_S;
require cs_admin;

binmode STDOUT, ":encoding(utf8)";

our (@alerts, @inst, @errors, @msg) = ();
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my @dbcols = qw/description rp pp void/; # the cols we will use params for
my @addl_params = qw/pk vendor_id mall_id/; # params that don't belong in update/insert SQL

#exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
my $cs = cs_admin->new({'cgi'=>$q});
my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $params = LoadParams();
die "mall_id and vendor_id paramters are required\n" unless $params->{'mall_id'} and $params->{'vendor_id'};

my $vendor = $db->selectrow_hashref('
	SELECT vendor_name
	FROM vendors
	WHERE status > 0
	AND vendor_id=?', undef, $params->{'vendor_id'}) ||	die "Failed to find an active vendor matching vendor_id: $params->{'vendor_id'}\n";

my $action = $q->param('_action');
unless ($action) {
	# load the template page
	_template();
}
elsif ($action eq 'list') {
	ShowList();
}
elsif ($action eq 'add') {
	my $rv = AddRecord();
#die "rv=$rv\n";
	if ($rv =~ /\D/){ Err("Failed to add record\n" . $rv); }
	else {
		ShowList($rv);
	}
}
elsif ($action eq 'update') {
	my $rv = Update();
	if ($rv =~ /\D/){ Err("Failed to update record\n" . $rv); }
	else {
		ShowList($rv);
	}
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect;
exit;

sub AddRecord
{
	return 'mall_id and vendor_id are required'	unless $params->{'mall_id'} && $params->{'vendor_id'};
	return 'A description and either an rp or a pp are required' unless $params->{description} && ($params->{rp} || $params->{pp});

	my $qry = 'INSERT INTO mall_vendor_product_payout_details (vendor_id,mall_id,pk,';
	my $end = 'VALUES (?,?,?,';
	my ($pk) = $db->selectrow_array("SELECT nextval('mall_vendor_product_payout_details_pk_seq')") ||
		die "Failed to obtain nextval('mall_vendor_product_payout_details_pk_seq')\n";
	my @args = ($params->{'vendor_id'}, $params->{'mall_id'}, $pk);
	foreach (@dbcols){
		# we don't want to insert NULLs or empties, that seems to end up disabling default column values
		next unless $params->{$_};
		$qry .= "$_,";
		$end .= '?,';
		push @args, $params->{$_};
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )", undef, @args);
	return "Insert failed.\nQuery: $qry\nDB reported: $DBI::errstr" unless $rv eq '1';
	return $pk;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub Err {
	print $q->header, $q->start_html, $q->p($_[0]), $q->end_html;
}

sub FilterWhiteSpace
{
	return $_[0] unless $_[0];
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub LoadParams
{
	my $rv = {};
	foreach(@dbcols, @addl_params)
	{
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	# since we are using a checkbox on the interface, with a value of TRUE when checked
	# we don't get FALSE when not checked :)
#	$rv->{active} = $rv->{active} ? 'TRUE' : 'FALSE';

	foreach (qw/vendor_id mall_id/)
	{
		die "$_ should be numeric only" if defined $rv->{$_} && $rv->{$_} =~ m/\D/;
	}
	return $rv;
}

sub MallCBO
{
	my (@mallIDs, %rows) = ();
	my $sth = $db->prepare('
		SELECT malls.mall_id, malls.label
		FROM malls
		JOIN mall_vendors mv
			ON mv.mall_id=malls.mall_id
		WHERE mv.vendor_id= ?
		AND mv.active=TRUE
		AND malls.active=TRUE
		ORDER BY malls.label');
	$sth->execute($params->{'vendor_id'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @mallIDs, $tmp->{'mall_id'};
		$rows{$tmp->{'mall_id'}} = $tmp->{'label'};
	}
	return $q->popup_menu(
		'-name' => 'mall_id',
		'-id'	=> 'mall_id',
		'-default'	=> $params->{'mall_id'},
		'-labels'	=> \%rows,
		'-values'	=> \@mallIDs,
		'-onchange'	=> "window.location.assign('" . $q->url() . "?vendor_id=$params->{'vendor_id'}&mall_id= ' + this.options[this.selectedIndex].value)",
		'-override'	=> 1
	);
}

sub ShowList
{
	###### spit out a list of records for a given vendor/mall ID -and- pk if we want a detail
	unless ($params->{'mall_id'} && $params->{'vendor_id'}){
		Err('mall_id and vendor_id are required');
		return;
	}

	my $qry = "SELECT pk, description, rp, pp, SUBSTR(stamp::TEXT,0,20) AS stamp, operator, void
		FROM mall_vendor_product_payout_details
		WHERE mall_id=? AND vendor_id=?\n";
	if (my $pk = shift){
		$qry .= "AND pk= $pk";
	}
	my $sth = $db->prepare("$qry ORDER BY pk");
	$sth->execute($params->{'mall_id'}, $params->{'vendor_id'});
	if ($db->errstr){
		Err('Database error:' . $db->errstr);
		return;
	}
	print $q->header('-charset'=>'utf-8', '-type'=>'text/xml'), '<root>';
	while (my $tmp = $sth->fetchrow_hashref){ ShowRow($tmp); }
	print '</root>';
}

sub ShowRow {
	my $tmp = shift;
	print '<tr>';
	foreach (qw/pk description rp pp stamp operator void/){
		$tmp->{$_} = $q->escapeHTML($tmp->{$_}) if $_ eq 'description';
		print "<$_>", EMD($tmp->{$_}), "</$_>";
	}
	print '</tr>';
}
sub Update {
	return 'pk parameter is required' unless $params->{'pk'} && $params->{'pk'} !~ /\D/;
	my $qry = 'UPDATE mall_vendor_product_payout_details SET ';
	foreach my $p (@dbcols){
		$qry .= "$p=";
		if ($p eq 'void'){
			$qry .= $params->{$p} ? 'TRUE' : 'FALSE';
		}
		else {
			$qry .= length ($params->{$p} ||'') > 0 ? $db->quote($params->{$p}) : 'NULL';
		}
		$qry .= ',';
	}
	$qry .= 'stamp=NOW()';
	$qry .= ' WHERE pk=?';
#die $qry;
	my $rv = $db->do($qry, undef, $params->{'pk'});
	return "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return $params->{'pk'};
}

sub _template {
	unless ($params->{'mall_id'} && $params->{'vendor_id'}){
		Err('mall_id and vendor_id are required');
		return;
	}
	my $MALL_ID = $params->{'mall_id'};
	my $VENDOR_ID = $params->{'vendor_id'};
	my $mallCBO = MallCBO();
	
	print $q->header('-charset'=>'utf-8'),
qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Product Payout Details</title>

<script type="text/javascript">
//<![CDATA[
// this section will be embedded in the head of the document
	var \$MALL_ID = $MALL_ID;
	var \$VENDOR_ID = $VENDOR_ID;
//]]>
</script>

<script type="text/javascript" src="/js/yui/yahoo-min.js"></script>
<script src="/js/yui/dom-min.js" type="text/javascript"></script>
<script src="/js/yui/event-min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/yui/connection-min.js"></script>
<script type="text/javascript" src="/js/admin/product_payout_details.js"></script>

<link rel="stylesheet" href="/css/admin/product_payout_details.css" type="text/css" />
</head>

<body onload="init()">
<div id="heading"><b>Product Payout Details:</b> Vendor ID: <b>$VENDOR_ID</b>,<b>$vendor->{'vendor_name'}</b>&nbsp;&nbsp;$mallCBO</div>
<form action="#" id="containerFrm">
<table id="dataTbl"><thead>
<tr><th>Pk</th><th>Description</th><th style="padding-right: 1em">ClubCash</th><th>PP</th><th>Stamp</th><th>Operator</th><th>Void</th><th></th></tr>
</thead>
<tbody id="dataTblBody"><tr><td></td></tr></tbody>
</table>
</form>
<form id="workingFrm" method="get" action="/cgi/admin/product_payout_details.cgi">
<input id="h_pk" type="hidden" value="" name="pk" />
<input id="h_description" type="hidden" value="" name="description" />
<input id="h_rp" type="hidden" value="" name="rp" />
<input id="h_pp" type="hidden" value="" name="pp" />
<input id="h_void" type="hidden" value="" name="void" />
<input type="hidden" value="$MALL_ID" name="mall_id" />
<input type="hidden" value="$VENDOR_ID" name="vendor_id" />
<input type="hidden" value="" name="_action" />
</form>
<div id="flipflop"></div>
<!--p>If you assign both RP and PP values, they will both show for VIPs, but only the RP value will show for Shoppers.</p-->
</body>
</html>|;
}

__END__

12/23/15	Just some minor verbiage changes.