#!/usr/bin/perl -w

=head1 NAME:
co-op_import.cgi

	This script handles the importing of new members from our Co-ops

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	strict, FindBin, CGI, ADMIN_DB, G_S, Coop::Factory, Encode

=cut

use strict;
use FindBin;
use lib "$FindBin::Bin/../../cgi-lib";
use CGI;
use ADMIN_DB;
#use DB;
#use DB_Connect;
use G_S;
use Coop::Factory;
use Encode;


=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $SomeClass = SomeClass->new();
our $db = {};
our $CGI = {};

eval{
	
	$CGI = CGI->new();
	my $operator = $CGI->cookie('operator');
	my $pwd = $CGI->cookie('pwd');

	die 'Permission Denied' if (! $operator && ! $pwd);

	unless ($db = ADMIN_DB::DB_Connect( 'coop-import', $operator, $pwd )){die 'Permission Denied';}
	$db->{RaiseError} = 1;
	

};
if ($@)
{
	print $@;
	exitScript();
}

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
processList

=head3
Description:

	This function does the bulk of the work.  It takes it's input from 
	$CGI->param, instanciates $Coop class, sends the coop list to be 
	processed by "Coop->processList" (passing in an arrayref 
	"@invalid_potential_members" for the potential members that don't 
	meet our criteria), then the number of members that were created are 
	displayed, and the members that could'nt be created are returned 
	in the text area.

=head3
Params:

	none it uses global variables for it's input.

=head3
Returns:

	none it out puts a web page.

=cut

sub processList
{
	

	
	my $Coop = {};
	my @invalid_potential_members = ();
	my $members_created = 0;
	my $errors = '';
	my $warnings = '';
	my $notice = '';
	my $list = '';
	
	my $unacceptable_members_csv = '';
	
	#get the interface name
	my $class_name = decode_utf8($CGI->param('co-ops'));

	eval{
		#Instanciate the Coop class
		$Coop = Coop::Factory->new($class_name, $db);
		
		my $headers = $CGI->param('headers') ? decode_utf8($CGI->param('headers')):0;
		$list = $CGI->param('list') ? decode_utf8($CGI->param('list')): '';
		
		# Send the coop list to the Coop class for processing, and return the
		# number of users created.
		$members_created = $Coop->processList($list, $headers, \@invalid_potential_members);
		
	};
	if ($@)
	{
		$errors .= 'There was a fatal error: ' . $@;
	}
	

	# If there were members that could'nt be created
	# turn their information into a CSV list
	if(scalar @invalid_potential_members)
	{
		my $headers_to_print = '';
		my @headers = ();
		$warnings .= 'These members did not meet the requirements for membership.  There should be an error message for each potential applicant at the end of each line.<br />';
		
		my $original_headers = $Coop->getOriginalHeaderMapping();
		my $db_fields = $Coop->getDbFields();
		
		foreach my $header_name (@{$original_headers})
		{
			$headers_to_print .= $header_name . ',';
		}
		
		$headers_to_print =~ s/,$//;
		$unacceptable_members_csv .= "$headers_to_print\n";
		
		# Loop through "@invalid_potential_members" and
		# turn them back into a CSV string.
		foreach my $member_hashref (@invalid_potential_members)
		{
		 	my $members_info = '';
		 	foreach my $header (@{$db_fields})
		 	{
				$members_info .= $member_hashref->{$header} . ",";		 		
		 	}
			
			if (exists $member_hashref->{error})
			{
				$members_info .= $member_hashref->{error};
			} 
			elsif(exists $member_hashref->{'warn'} )
			{
				$members_info .= $member_hashref->{'warn'};
			}else {
				$members_info =~ s/,$//;
			}
				
			$unacceptable_members_csv .= "$members_info\n";
		}
	} 
	
	# Check for errors when creating new members
	if (ref($members_created) ne 'HASH')
	{
		$notice .= "Number of Members Created: $members_created";
	} else {
		if (exists $members_created->{error})
		{
			$errors .= $members_created->{error};
			$unacceptable_members_csv = $list;
		}
		
	}
	
	
	
	# Build the coop list for the dropdown menu.
	my @coops = $Coop->getImportCoops();
	my $new_coop = [];
	my $new_coop_labels = {};
	
	foreach my $row (@coops)
	{
		push @$new_coop, $row->{provider_name};
		$new_coop_labels->{$row->{provider_name}} = $row->{provider_name};
	}
	
	my $default = $CGI->param('co-ops') ? decode_utf8($CGI->param('co-ops')): '';
		
	my $provider_select_list = $CGI->popup_menu(-name=>'co-ops',
	                            -values=>$new_coop,
	                            -labels=>$new_coop_labels,
	                            -default=>$default
								);
								
	
	# Get the page template
	my $template = G_S::Get_Object($db, 10639);
	
	$errors = qq(<div class="error">) . $errors . qq(</div>) if $errors;
	$warnings = qq(<div class="warning">) . $warnings . qq(</div>) if $warnings;
	$notice = qq(<div class="notice">) . $notice . qq(</div>) if $notice;
	
	print $CGI->header('text/html');
	$template =~ s/%%ERROR%%/$errors/;
	$template =~ s/%%WARNING%%/$warnings/;
	$template =~ s/%%NOTICE%%/$notice/;
	$template =~ s/%%COOPS_SELECT_LIST%%/$provider_select_list/;
	$template =~ s/%%LIST%%/$unacceptable_members_csv/;
	
	$template =~ s/%%\w+%%//g;
	
#	use Data::Dumper;
#	print Dumper(@invalid_potential_members);
	
	print $template;	
}


=head2
importScreen

=head3
Description:

	This is the first screen the user comes to.
	It allows the user to select a Co-op, specify
	if their CSV list contains headers, and a text
	area for pasting a CSV list.

=head3
Params:

	none it uses global variables for it's input.

=head3
Returns:

	none it out puts a web page.

=cut

sub importScreen
{
	my $Coop = {};
	my $new_coop = [0];
	my $new_coop_labels = {0=>''};
	my $provider_select_list = '';
	my @coops = ();
	
	my $template = G_S::Get_Object($db, 10639);
	
	eval{
		
		$Coop = Coop::Factory->new('', $db);
		
		@coops = $Coop->getImportCoops();
		
		foreach my $row (@coops)
		{
			push @$new_coop, $row->{provider_name};
			$new_coop_labels->{$row->{provider_name}} = $row->{provider_name};
		}
		
		my $default = decode_utf8($CGI->param('co-ops')) if $CGI->param('co-ops');
		
		$provider_select_list = $CGI->popup_menu(-name=>'co-ops',
	                             -values=>$new_coop,
	                             -labels=>$new_coop_labels,
	                             -default=>$default
	                        );
	   
	};
	if ($@)
	{
		print $CGI->header('text/html');
		$template =~ s/%%ERROR%%/$@/;
		
		$template =~ s/%%\w+%%//g;
		
		print $template;
		
		exitScript();
	}
	
	if ($CGI->param('list'))
	{
		my $list = decode_utf8($CGI->param('list'));
		
		$template =~ s/%%LIST%%/$list/;
	}
	
	$template =~ s/%%COOPS_SELECT_LIST%%/$provider_select_list/;
	$template =~ s/%%\w+%%//g;
	
	print $CGI->header('text/html');
	
	print $template;
	
	exitScript();
	
}

####################################################
# Main
####################################################

if ($CGI->param('co-ops') && $CGI->param('list'))
{
	processList();
} else {
	importScreen();
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


