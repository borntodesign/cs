#!/usr/bin/perl -w
##
## InactiveCoachAdmin.cgi
##
## Deployed Nov, 2012	Bill MacArthur
##
## last modified: 

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;
use Data::Dumper;	# for debugging only

my $title = 'Inactive Coach Administration';

my $q = new CGI;
my $action = $q->param('_action');
my $ME = $q->script_name;

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect('generic', $operator, $pwd);
$db->{'RaiseError'} = 1;
my $gs = G_S->new({'db'=>$db});

unless ($action)
{
	SearchForm();
}
elsif ($action eq 'update')
{
	Update();
}
else
{
	# we must be searching for an ID
	Search();
}

Exit();

###### start of subs

#sub DiffClass
#{
#	my $row = shift;
#	my ($diff, $diffclass) = ();
#	if ($row->{'diff'} < 0)
#	{
#		$diff = "$row->{'diff'}";
#		$diffclass = 'tmppd';
#	}
#	else
#	{
#		$diff = "+ $row->{'diff'}";
#		$diffclass = 'tmppi';
#	}
#	
#	return ($diff, $diffclass);
#}

sub err
{
	my $msg = shift;

	print $q->header,
        $q->start_html,
		$msg,
		$q->end_html;
	exit;
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub GetRecords
{
	my $qry = "
		SELECT
			ic.id AS ic_id,
			ic.inactive,
			COALESCE(ic.stamp::TIMESTAMP(0)::TEXT,'') AS stamp,
			COALESCE(ic.notes, '') AS notes,
			COALESCE(ic.operator,'') AS operator,
			m.id,
			m.alias,
			(m.firstname||' '||m.lastname) AS name,
			r.a_level,
			r.tmpp,
			lv.lname
--			COALESCE(th.tmpp::TEXT,'') AS last_month_tmpp,
--			(r.tmpp - COALESCE(th.tmpp,0)) AS diff

		FROM members m
		JOIN refcomp_col_remap r
			ON r.id=m.id
		JOIN level_values lv
			ON lv.lvalue=r.a_level
		LEFT JOIN house_accounts hc
			ON hc.id=m.id
		LEFT JOIN inactive_coach ic
			ON m.id=ic.id
--		LEFT JOIN tmpp_history th
--			ON th.id=m.id
--			AND th.period= get_period_by_date(1, (first_of_month() -1))
		WHERE
	";

	my $id = $q->param('member_id') || '';
	$id =~ s/\s//g;
	if ($id)
	{
		$id = uc($id);
		$id = $db->selectrow_hashref('SELECT id, alias, firstname, lastname FROM members WHERE ' . ($id =~ /\D/ ? 'alias=?': 'id=?'), undef, $id) ||
			die "Could not identify a member with id: " . $q->param('member_id');
		
		$qry .= "m.id= $id->{'id'}";
	}
	else
	{
		$qry .= "hc.id IS NULL AND m.membertype= 'v' AND ((ic.id IS NULL AND r.a_level >= 5) OR ic.id IS NOT NULL) ORDER BY m.id ASC";
	}
	
	my @rv = ();
	
	my $sth = $db->prepare($qry);
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	
	return ($id, \@rv);
}

sub ListInterface
{
	my $list = shift;
#	$title .= ": Incomplete Orders";
	
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>$title,
			'-style'=>{
				'src'=>'/css/admin/generic-report.css',
				'code'=> LocalStyles()
			},
			'-encoding'=>'utf-8'
		),
		$q->h4($title),
		$q->p( $q->a({'-href'=>$ME}, 'New Search') ),
		'<table class="report">',
		$q->Tr(
			$q->th('Member ID') .
			$q->th('Name') .
			$q->th('Pay Level') .
			$q->th('Coach Status') .
			$q->th('Last Changed') .
			$q->th('Notes') .
			$q->th('Operator') 
#			$q->th('Last Month TMPP') .
#			$q->th('Current TMPP') .
#			$q->th({'-style'=>'text-align:center'}, '+ / -') .
#			$q->th('% Change')
		);
	
	my $rowclass = 'a';
	foreach my $row (@{$list})
	{
		my ($status, $tdclass) = Status($row);
		
#		my ($diff, $diffclass) = DiffClass($row);
#		my $pct_chg = ();

#die Dumper($row);
#		if ($row->{'last_month_tmpp'} eq '' || $row->{'last_month_tmpp'} == 0)
#		{
#			$pct_chg = '';
#		}
#		else
#		{
#			$pct_chg = (($row->{'tmpp'} - $row->{'last_month_tmpp'}) / $row->{'last_month_tmpp'}) * 100;
#			$pct_chg = sprintf('%3.2f', $pct_chg);
#			$pct_chg = ( $row->{'diff'} < 0 ? '':'+ ' ) . $pct_chg;
#		}
		
		print $q->Tr({'-class'=>$rowclass},
			$q->td(
				$q->a({'-href'=>"$ME?_action=search;member_id=$row->{'id'}"}, $row->{'alias'})
			) .
			$q->td($row->{'name'}) .
			$q->td($row->{'lname'}) .
			$q->td({'-class'=>$tdclass}, $status) .
			$q->td($row->{'stamp'}) .
			$q->td($row->{'notes'}) .
			$q->td($row->{'operator'})
#			$q->td({'-class'=>'alr'}, $row->{'last_month_tmpp'}) .
#			$q->td({'-class'=>'alr'}, $row->{'tmpp'})
#			$q->td({'-class'=>"alr $diffclass"}, $diff)
#			$q->td({'-class'=>"alr $diffclass"}, $pct_chg)
		);
		
		$rowclass = $rowclass eq 'a' ? 'b':'a';
	}
	
	print
		'</table>',
		$q->end_html;
}

sub LocalStyles
{
	return '
				td.txc { text-align:center; }
				table caption
				{
					font-family: arial,helvetica,sans-serif;
					font-weight:bold;
					text-align:left;
				}
				td.active,option.active,select.active { background-color:#efe; }
				td.danger,option.danger,select.danger { background-color:#eea; }
				td.deactivated,option.deactivated,select.deactivated { background-color:#fcc; }
				td.alr { text-align:right; }
				td.tmppd { color:red; }
				td.tmppi { color:green; }
				table.mgt td
				{
					font-family: arial,helvetica,sans-serif;
					font-weight:normal;
					text-align:left;
					font-size: 100%;
					border: 1px solid #ccc;
				}';
}

sub ManagementForm
{
	my $rid = shift;
	my $list = shift;
	my $row = $$list[0];
#	my @tmpp = $gs->select_array_of_hashrefs("
#		SELECT
#			th.tmpp,
#			(th.change_percentage * 100)::NUMERIC(5,2) AS diff,
#			ic.id AS ic_id,
#			ic.inactive,
#			p.open,
#			p.close
#		FROM tmpp_history th
#		JOIN periods p
#			ON p.period=th.period
#		LEFT JOIN inactive_coach ic
#			ON ic.id=th.id
#			AND ic.stamp BETWEEN p.open AND p.close
#		WHERE th.id= $rid->{'id'}
#		ORDER BY th.period DESC
#	");
#	my @tmpp = $gs->select_array_of_hashrefs("
#		SELECT
#--			th.tmpp,
#--			(th.change_percentage * 100)::NUMERIC(5,2) AS diff,
#			ic.id AS ic_id,
#			ic.inactive,
#			p.open,
#			p.close
#		FROM inactive_coach ic
#		JOIN periods p
#			ON ic.stamp BETWEEN p.open AND p.close
#		WHERE ic.id= $rid->{'id'}
#		ORDER BY p.period DESC
#	");
#	
#	my $tmppTbl = '<table class="report"><!--caption>TMPP History</caption--><tr><th>Commission Period</th><!--th>TMPP</th><th>Change Percentage</th--><th>Coach Status</th></tr>';
#	my $rowclass = 'a';
#	foreach my $tmp (@tmpp)
#	{
#		my ($status, $tdclass) = Status($tmp);
##		my ($diff, $diffclass) = DiffClass($tmp);
#		$tmppTbl .= $q->Tr({'-class'=>$rowclass},
#			$q->td("$tmp->{'open'} - $tmp->{'close'}") .
##			$q->td($tmp->{'tmpp'}) .
##			$q->td({'-class'=>$diffclass}, $diff) .
#			$q->td({'-class'=>$tdclass}, $status)
#		);
#		$rowclass = $rowclass eq 'a' ? 'b':'a';
#	}
#	$tmppTbl .= '</table>';

	my ($status, $tdclass) = Status($row);
	
	$title .= ": $rid->{'alias'} $rid->{'firstname'} $rid->{'lastname'}";
	my $option_classes = {
		'ACTIVE' => {'class'=>'active'},
		'IN DANGER' => {'class'=>'danger'},
		'DEACTIVATED' => {'class'=>'deactivated'}
	};

	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>$title,
			'-style'=>'/css/admin/generic-report.css',
			'-encoding'=>'utf-8',
			'-style'=>{
				'src'=>'/css/admin/generic-report.css',
				'code'=> LocalStyles()
			}
		),
		$q->h4($title),
		$q->p( $q->a({'-href'=>$ME}, 'New Search') ),
		$q->p( $q->a({'-href'=>"$ME?_action=search"}, 'List all relevant statuses') ),
		$q->start_form('-method'=>'get'),
		$q->hidden('-name'=>'member_id', '-value'=>$rid->{'id'}, '-force'=>1),
		$q->hidden('-name'=>'_action', '-value'=>'update', '-force'=>1),
		'<table class="mgt">',
		$q->Tr(
			$q->td($row->{'alias'}) .
			$q->td($row->{'name'}) .
			$q->td($row->{'lname'}) .
			$q->td(
				$q->popup_menu(
					'-class'=> $option_classes->{$status}->{'class'},
					'-onchange'=>'this.className=this.options[this.selectedIndex].className',
					'-name'=>'status',
					'-default'=>$status,
					'-values'=>['ACTIVE','IN DANGER', 'DEACTIVATED'],
					'-attributes'=> $option_classes,
					'-force'=>1
				)
			) .
			$q->td($row->{'stamp'}) .
			$q->td(
				$q->textfield(
					'-name'=>'notes',
					'-value'=> $row->{'notes'},
					'-style'=>'width:20em',
					'-onclick'=>'this.select()'
				)
			) .
			$q->td(
				$q->submit('Change Status') . ' ' . $q->reset
			)
		),
		'</table>',
		$q->end_form,
#		$q->div({'-style'=>'margin-top:2em'}, $tmppTbl),
		$q->end_html;
}

sub Search
{
	my ($id, $rv) = GetRecords();
	if ($id)
	{
		ManagementForm($id, $rv);
	}
	else
	{
		ListInterface($rv);
	}
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html('-title'=>$title),
		$q->h4($title),
		$q->start_form('-method'=>'get'),
		$q->hidden('-name'=>'_action', '-value'=>'search'),
		'Enter member ID: ',
		$q->textfield('-name'=>'member_id'),
		' ',
		$q->submit,
		$q->p('Leave the ID field blank if you want a list of all coach statuses'),
		$q->end_form.
		$q->end_html;
}

sub Status
{
	my $row = shift;
	my $status = 'ACTIVE';
	my $tdclass = 'active';
	if ($row->{'ic_id'})
	{
		if (! defined $row->{'inactive'})
		{
			# keep the default
		}
		elsif (! $row->{'inactive'})
		{
			$status = 'IN DANGER';
			$tdclass = 'danger';
		}
		elsif ($row->{'inactive'})
		{
			$status = 'DEACTIVATED';
			$tdclass = 'deactivated';
		}
		# otherwise the default prevails
	}
	
	return ($status, $tdclass);
}

sub Update
{
	my $status = $q->param('status') || die "No status parameter received";
	my $id = $q->param('member_id') || die "No member_id parameter received";
	my $ic = $db->selectrow_hashref('SELECT * FROM inactive_coach WHERE id= ?', undef, $id);
	
	$status = $status eq 'ACTIVE' ? 'NULL' : $status eq 'DEACTIVATED' ? 'TRUE' : 'FALSE';	# IN DANGER equals a FALSE in the inactive field of the recordset
	
	if ($ic)
	{
		$db->do("UPDATE inactive_coach SET inactive= $status, notes= ? WHERE id= $ic->{'id'}", undef, $q->param('notes'));
	}
	else
	{
		$db->do("INSERT INTO inactive_coach (id, inactive, notes) VALUES (?, $status, ?)", undef, $id, $q->param('notes'));
	}
	
	Search();
}