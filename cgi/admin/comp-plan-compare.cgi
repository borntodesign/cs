#!/usr/bin/perl -w
###### comp-plan-compare.cgi
###### compare the regular network_calcs with the revised version for a given ID
###### created: September '08	 Bill MacArthur
###### last modified: 05/17/11	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib/';
use ADMIN_DB;
use G_S;
my $q = new CGI;
my ($new, $old) = ();

# this table and the next one should have the same schema as network_calcs
my $newTBL = 'nctest';
my $oldTBL = 'ncreference';

###### we should be in SSL mode
die "This resource should only be invoked in SSL mode\n" unless $q->https;

###### we should have our operator and password cookies
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "You must be logged in to use this resource\n" unless $operator && $pwd;

exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
$db->{'RaiseError'} = 1;

###### we're only expecting these params
# catid - category ID
# _action - what we are going to do (default is show a table of categories under a certain parent)
# catname - a category name
my %param = map {$_, G_S::PreProcess_Input(defined $q->param($_) ? $q->param($_) : '')} ('id','list');
###### we need these values for these params
#foreach('id'){ die "Failed to receive a required parameter: $_\n" unless $param{$_}; }
unless ($param{id} || $param{list})
{
	SubmitIDFrm();
}
elsif ($param{list}){ ShowList(); }
else {
my $id = ($param{id} =~ /\D/ ?
	$db->selectrow_array('SELECT id FROM members WHERE alias= UPPER(?)', undef, $param{id}) :
	$param{id}) ||
	die "Failed to identify a membership matching: $param{id}";

$new = $db->selectrow_hashref("
	SELECT c.*, m.alias, m.firstname, m.lastname FROM $newTBL c, members m WHERE m.id=c.id AND c.id= $id");
die "Failed to find a $newTBL comparison record for: $id" unless $new;

$old = $db->selectrow_hashref("
	SELECT c.* FROM $oldTBL c WHERE c.id= $id") ||
	die "Failed to find a $oldTBL record for: $id";


print $q->header(-expires=>'now');
print <<"header";
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comp Plan comparisons for $id</title>
<style type="text/css">
table {	border-collapse:collapse; font:normal smaller sans-serif; float:left;}
td, th {padding:0.3em; border:1px solid silver;}
td {  text-align:right;}
th { background-color: #eee; font-size:90%;}
td.report { padding-bottom:2em; background-color: white; text-align:left;}
tr.new {background-color:#efe;}
tr.old {background-color:#ffe;}
td.wt {background-color:silver;}
td.diff{ background-color:#eee; text-align:right; font-weight:bold;}
span.increase {color: #090;}
span.decrease {color: red;}
span.same {display:none;}
h5 {margin-top: 0.2em;}
tr.changes td {font-weight:bold;}
</style>
</head>
<body>
${\ Legend()}
<h4>$new->{alias}, $new->{firstname} $new->{lastname}</h4>

<table>
header
Header();
DisplayRecordset($new, 'new');
Header();
DisplayRecordset($old, 'old');
Header();
print qq|<tr class="changes"><td>Changes</td>|;
Diffs($_) foreach (qw/ppp npp exec_breakaway_volume real_npp payout_percentage paid_out/);
print '<td></td>';
Diffs($_) foreach (qw/calculated_pay bonus a_level active_lines total/);
print '</tr>';
print '</table>
	<p style="clear:both; padding-top:1em;">
		* If there are PP changes, then the calculations are not in sync and will not provide accurate comparisons.
	</p>
	</body></html>';

}
$db->disconnect;
exit;

sub Diffs
{
	my $col = shift;
	my $diff = $new->{$col} - $old->{$col};
	$diff = sprintf("%.2f", $diff) if $diff != int $diff;
	my $class = $diff > 0 ? 'increase' : $diff < 0 ? 'decrease' : 'same';
	print $q->td( $q->span({-class=>$class}, $diff) );
}

sub DisplayRecordset
{
	my $rec = shift;
	$rec->{'total'} = sprintf("%.2f", $rec->{'calculated_pay'} + $rec->{bonus});
	$rec->{'real_npp'} = sprintf("%.2f", $rec->{'npp'} - $rec->{'exec_breakaway_volume'});
#	$rec->{analysis} =~ /mgt_bonus="(.*?)"/;
#	$rec->{mgt_bonus} = $1 || '';
	my $class = shift;
	print qq|<tr class="$class"><td><b>|, uc($class), '</b></td>';
	foreach(qw/ppp npp exec_breakaway_volume real_npp payout_percentage paid_out mgt_mult
		calculated_pay bonus a_level active_lines total/)
	{
		print $q->td($rec->{$_});
	}
	print qq|</tr><tr class="$class">|;
	print $q->td({-class=>'wt'}, '');
	$rec->{'exec_report'} =~ s#\n#<br />#g;
	print $q->td({-class=>'report', -colspan=>12}, ($rec->{'exec_report'} ? $q->h5('Exec Report') : '') . $rec->{exec_report});
	print '</tr>';
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Header
{
	print '<tr><th></th>
<th>PPP</th>
<th>Gross NPP</th>
<th>ExBV</th>
<th>Real NPP</th>
<th>Pay %</th>
<th>Paid Out</th>
<th>Mgt. Mult.</th>
<th>Calc. Pay</th>
<th>Exec Bonus</th>
<th>Level</th>
<th>AL</th>
<th>Total Comp</th>
</tr>';
}

sub Legend
{
	return qq|<p style="margin-top: 0; float:right; width:10em; font:normal small sans-serif; text-align:left;">
ExBV: Exec Breakaway Volume<br />
Level: Network Level<br />
AL: Active Lines<br />
<a href="${\ $q->url}?list=1">Show complete list</a><br />
<a href="${\ $q->url}">New Search</a>
</p>|;
}

sub ShowList
{
	print $q->header;
print <<"header";
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comp Plan comparisons List</title>
<style type="text/css">
table {	border-collapse:collapse; font:normal small sans-serif;}
td, th {padding:0.3em; border:1px solid silver;}
td {  text-align:right;}
th { background-color: #eee; font-size:90%; vertical-align:top;}
tr.a {background-color:#ffe;}
tr.b {background-color:#eee;}
a {text-decoration:none;}
td.r, a.r {color:red;}
td.g, a.g {color:green;}
</style>
</head>
<body>
<table>
<tr><th>ID</th>
<th>Current<br />mType</th>
<th>Name</th>
<th>Current<br />Role</th>
<th>PPP</th>
<th>Real NPP</th>
<th>New Org Inc</th>
<th>Old Org Inc</th>
<th>New X<br />Bonus</th>
<th>Old X<br /> Bonus</th>
<th>New Pay<br />Level</th>
<th>Old Pay<br /> Level</th>
<th>Active<br />Lines New</th>
<th>Active<br />Lines Old</th>
<th>Difference</th>
</tr>
header

	my $sth = $db->prepare(q#
		SELECT cn.*
			,m.firstname ||' '||m.lastname AS name
			,COALESCE(nbtl.code,'') AS role
			,m.membertype
		FROM compare_netcalcs cn
		JOIN members m
			ON cn.id=m.id
		LEFT JOIN nbteam_vw nbt
			ON nbt.id=cn.id
		LEFT JOIN nbteam_levels nbtl
			ON nbtl."level"=nbt."level"
		ORDER BY cn.total_diff#);
	$sth->execute;
	my $class = 'a';
	my $diffTTL = 0;
	my $diff_class = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$diffTTL += $tmp->{'total_diff'};
		my $id = $tmp->{'id'};
		$diff_class = $tmp->{'total_diff'} > 0 ? 'g' : $tmp->{'total_diff'} < 0 ? 'r' : 'default';
		print qq|<tr class="$class">|;
		foreach (qw/id membertype name role newppp newrealnpp new old newb oldb newlvl oldlvl ALnew ALold total_diff/){
			$tmp->{$_} = $q->a({-href=>$q->url . "?id=$id"}, $tmp->{$_}) if $_ eq 'id';
			$tmp->{$_} = $q->a({-href=>$q->url . "?id=$id", -class=>$diff_class}, $tmp->{$_}) if $_ eq 'total_diff';
			print $q->td($tmp->{$_});
		}
		print '</tr>';
		$class = $class eq 'a' ? 'b' : 'a';
	}
	$diff_class = $diffTTL >= 0 ? 'g' : 'r';
	print qq|<tr><td colspan="14">Total Difference</td><td class="$diff_class">|;
	printf '%.2f', $diffTTL;
	print "</td></tr>";
	
	print '</table></body></html>';
}

sub SubmitIDFrm
{
	print $q->header,
		$q->start_html(
			-onload=>"document.getElementById('id').focus()",
			-title=>'Comp Plan Comparisons'),
		Legend(),
		$q->h4('Enter an ID'),
		$q->start_form, $q->textfield(-name=>'id', -id=>'id'), $q->submit, $q->end_form, $q->end_html;
}
