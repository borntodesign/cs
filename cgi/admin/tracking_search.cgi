#!/usr/bin/perl -w
##
## tracking_search.cgi
##
## Deployed 03/12/07	Shane Partain
##
## last modified: 01/12/009	Keith 

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

my $SEARCH_RESULTS_LIMIT = 200;
my @query_params = ('id', 'v.vendor_id', 'ip_addr');
my %query_params = ();
my %params = ();
my $start = '';
my $end = '';
my $i = '';
my $sort = 't.stamp';
my $rows = '';
my @row = ();
my @table = ();
my $table = '';
my $color = '';

our $q = new CGI;
our $db = '';

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect('tracking_search.cgi', $operator, $pwd)) {exit;}
$db->{RaiseError} = 1;

foreach ($q->param()) {$params{$_} = G_S::PreProcess_Input($q->param($_));}

unless (%params) {

	##
	## Print out the search form.
	##
	
	print $q->header(),
	$q->start_html(-title=>'Tracking Search',
			-style=>{
				-code=>''
				}),
        $q->h3('Tracking Search'),
        $q->start_form(-method=>'get'),
	'<table>',
	'<tr><td>',
		'Member ID',
	'<td>',
		$q->textfield(	-name	=> 'id'),
	'<tr><td>',
		'Vendor ID',
	'<td>',
		$q->textfield(	-name	=> 'v.vendor_id'),
	'<tr><td>',
		'IP Address',
	'<td>',
		$q->textfield(  -name   => 'ip_addr'),
        '<tr><td>',
		'Start Date',
	'<td>',
		$q->popup_menu(	-name	=> 'start_month',
			-values	=> ['',1,2,3,4,5,6,7,8,9,10,11,12],
			-labels =>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'April',5=>'May',6=>'June',
				7=>'July',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'}
			),
		$q->popup_menu( -name   => 'start_day',
			-values => ['',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
				21,22,23,24,25,26,27,28,29,30,31]
			),
		$q->popup_menu( -name   => 'start_year',
			-values => ['',2018,2017,2016,2015,2014,2013,2012,2011]
			),
	'<tr><td>',
		'End Date',
	'<td>',
		$q->popup_menu(	-name	=> 'end_month',
			-values	=> ['',1,2,3,4,5,6,7,8,9,10,11,12],
			-labels =>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'April',5=>'May',6=>'June',
				7=>'July',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'}
			),
		$q->popup_menu( -name   => 'end_day',
                        -values => ['',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                                21,22,23,24,25,26,27,28,29,30,31]
                	),
		$q->popup_menu( -name   => 'end_year',
			-values => ['',2018,2017,2016,2015,2014,2013,2012,2011]
			),
	'</table>',
	$q->submit,
	'<br><br>',
	'<a href="javascript:void(0)" onclick="window.open(\'/admin/ts_help.html\', \'helpWindow\', \'resizable=1,scrollbars=1,width=420,height=400\')" target="helpWindow">Help</a>',
        $q->end_form(),
        $q->end_html();

}else{

	##
	## Print out the results.
	##

	if($params{start_month}) {
		$params{start_month} = sprintf("%02d", $params{start_month});
		$params{start_day} = sprintf("%02d", $params{start_day});
		$start = $params{start_year} . "-" . $params{start_month} . "-" . $params{start_day};
		$start = $db->quote($start);
	}
	if($params{end_month}) {
		$params{end_month} = sprintf("%02d", $params{end_month});
		$params{end_day} = sprintf("%02d", $params{end_day});
		$end = $params{end_year} . "-" . $params{end_month} . "-" . $params{end_day};
		$end = $db->quote($end);
	}

	my $query = 'SELECT date_trunc(\'second\', t.stamp),
				vendor_name,
				id,
				ip_addr,
				refspid,
				vid,
				t.url,
				referer,
				ad_id
			FROM tracking t, vendors v
			WHERE t.vendor_id=v.vendor_id';
	foreach (@query_params) {
		if ($params{$_}) {$query_params{$_} = $db->quote($params{$_}) };
	}
	foreach (keys %query_params) {
		if (/start/ || /end$/) {next;} ## Everything but the dates gets appended to the query here.
		$query .= " AND $_=$query_params{$_}";
	}
	## Look for a date range if both a start and end were submitted. Otherwise
	## just get stuff that matches the start date.
	if ($start || $end) {
		$query .= ' AND date_trunc(\'day\', t.stamp) BETWEEN ' . $start;
		if ($end) {
			$query .= " AND $end";
		}else{ $query .= " AND $start"; }
	}

	$query .= " ORDER BY $sort";

	## Table headers.
	$table = $q->th(["Date", "Vendor", "Member ID", "IP Address", "Refspid", "VID", "URL", "Referer", "Ad ID"]);

	my $sth = $db->prepare($query);
	unless ($sth->execute > 0) { err("No records found"); }
	while (@row = $sth->fetchrow_array) {
		if (++$rows == $SEARCH_RESULTS_LIMIT) {
			err("Your query returned > $rows records. Please go back and narrow your search.");
		}
		foreach (@row) { $_ = '&nbsp;' unless $_;}
		push @table, $q->td(\@row);
	}
	$sth->finish;
	$db->disconnect;

	foreach (@table) {
		if(++$i % 2 == 0) {
			$color = '#cccccc';
		}else{ $color = '#ffffff'; }
		$table .= $q->Tr({-bgcolor=>$color}, $_);
	}

	print $q->header(-charset=>'utf-8'),
	$q->start_html(
			-title=>'Tracking Search Results',
			-encoding=>'utf-8',
			-style=>{
				-code=>'table{font-size:75%; border-collapse:collapse;}
					td{white-space:nowrap; padding: 0.2em 0.3em; border:1px solid #009;}
					th{background-color:#cccccc; border:1px solid #009;}
					tr.a {background-color:rgb(255,255,240);}
					tr.b {background-color:#efefef;}
				'
		});
        print $q->h3('Tracking Search Results'),
	$q->start_html(-style=>{
                                -code=>'table{font-size:85%}td{white-space:nowrap}th{background-color:#cccccc}'
				}
		),
	$q->table({-cellspacing=>2, cellpadding=>2}, $table),
	$q->end_html;
}

sub err{
	my $msg = shift;

	print $q->header,
        $q->start_html,
	$msg,
	$q->end_html;
	exit;
}

# 07/02/08 added the $SEARCH_RESULTS_LIMIT global to make it easier to adjust this value later on (started at 100)

=head1 CHANGE LOG

    2009-01-12  Keith  Added 2009 to the start, and end dates.

=cut
