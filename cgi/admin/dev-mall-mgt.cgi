#!/usr/bin/perl -w
# mall-mgt.cgi
# create/update automated mall records
# created Nov. 2007	Bill MacArthur
# last modified: 04/23/08	Bill MacArthur

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use G_S;

our (@alerts, @inst, @errors, @msg, $db) = ();
my $q = new CGI;
unless ($q->https){
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my @dbcols = qw/label url active country_code language_code notes mall_id/; # the cols we will use params for
my @addl_params = qw/offset/; # params that don't belong in update/insert SQL

exit unless $db = ADMIN_DB::DB_Connect( 'generic', $q->cookie('operator'), $q->cookie('pwd') );
$db->{RaiseError} = 1;

my $action = $q->param('_action');
unless ($action) {
	# the initial load
	ShowList();
}
elsif ($action eq 'add') {
	my $mall = AddRecord( LoadParams() );
	Detail($mall) unless @errors;
}
elsif ($action eq 'new') {
	######
	Detail({});
}
elsif ($action eq 'detail') {
	die "No mall_id parameter received\n" unless my $mall_id = $q->param('mall_id');
	my $mall = $db->selectrow_hashref('SELECT * FROM malls WHERE mall_id=?', undef, $mall_id);
	die "No mall found matching: $mall_id\n" unless $mall->{mall_id};
	Detail($mall);
}
elsif ($action eq 'edit') {
	my $params = LoadParams();
	Update($params);
	my $mall = $db->selectrow_hashref('SELECT * FROM malls WHERE mall_id=?', undef, $params->{mall_id});
	Detail($mall) unless @errors;
}
elsif ($action eq 'vendor-list') {
	VendorList();
}
else { push (@errors, 'Unrecognized action parameter: ' . $q->param('action')); }

END:
Err() if @errors;
$db->disconnect;
exit;

sub AddRecord {
	my $params = shift;
	my $qry = 'INSERT INTO malls (';
	my $end = 'VALUES (';
	my @args = ();
	($params->{mall_id}) = $db->selectrow_array("SELECT nextval('malls_mall_id_seq')");
	foreach (@dbcols){
		# we don't want to insert NULLs or empties, that seems to end up disabling default column values
		next unless $params->{$_};
		$qry .= "$_,";
		$end .= '?,';
		push @args, $params->{$_};
	}
	chop $qry;
	chop $end;
	my $rv = $db->do("$qry ) $end )", undef, @args);
	if ($rv eq '1'){ return $db->selectrow_hashref("SELECT * FROM malls WHERE mall_id= $params->{mall_id}"); }
	push @errors, "Insert failed. DB reported: $DBI::errstr";
	return undef;
}

sub cboCountry
{
	###### we could have used the G_S version,
	###### but it relies on a static list and doesn't apply an id to the control
	my $sth = $db->prepare('SELECT country_name, country_code FROM tbl_countrycodes ORDER BY country_name');
	$sth->execute;
	my @codes = '';
	my %names = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @codes, $tmp->{country_code};
		$names{$tmp->{country_code}} = $tmp->{country_name};
	}
	return $q->popup_menu(
		-name=>'country_code',
		-id=>'country_code',
		-values=>\@codes,
		-labels=>\%names,
		-default=>$_[0]
	);
}

sub cboLanguage
{
	my $sth = $db->prepare('SELECT code2, description FROM language_codes WHERE active=TRUE ORDER BY description');
	$sth->execute;
	my @codes = '';
	my %names = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @codes, $tmp->{code2};
		$names{$tmp->{code2}} = $tmp->{description};
	}
	return $q->popup_menu(
		-name=>'language_code',
		-id=>'language_code',
		-values=>\@codes,
		-labels=>\%names,
		-default=>$_[0]
	);
}

sub Detail
{
	###### show the data input/edit form
	my $data = shift;	###### a hashref
	my @collist = qw|mall_id country_code language_code label notes active stamp url|;
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => ($data->{mall_id} ? "Mall Detail: $data->{label}" : 'New Mall Entry'),
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/mall-mgt.css'}]
		),
		$q->h5($data->{mall_id} ? "Mall Detail: $data->{label}" : 'New Mall Entry'),
		$q->start_form(-action=>$q->script_name);
	print q|<table id="detail-tbl">|;
	print $q->Tr(
		$q->td({-class=>'label'}, 'Mall ID') .
		$q->td( $q->textfield(
			-id=>'mall_id', -name=>'mall_id',
			-value=>EMD($data->{mall_id}), -readonly=>'readonly') )
	) if $data->{mall_id};
	print $q->Tr(
		$q->td({-class=>'label'}, 'Mall Label') .
		$q->td( $q->textfield(-id=>'label', -name=>'label', -value=>EMD($data->{label})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'URL') .
		$q->td( $q->textfield(-id=>'url', -name=>'url', -value=>EMD($data->{url})) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Country') .
		$q->td( cboCountry($data->{country_code} || 'US') ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Language') .
		$q->td( cboLanguage($data->{language_code} || 'en') ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Notes') .
		$q->td( $q->textarea(
			-id=>'notes', -name=>'notes', -cols=>50, -rows=>4,
			-value=>EMD($q->escapeHTML($data->{notes}))) ));
	print $q->Tr(
		$q->td({-class=>'label'}, 'Timestamp') .
		$q->td( $q->textfield(
			-id=>'stamp', -name=>'stamp', -force=>1,
			-value=>EMD($data->{stamp}), -readonly=>'readonly') )
	) if $data->{mall_id};
	print $q->Tr(
		$q->td({-class=>'label'}, 'Active') .
		$q->td( $q->checkbox(
			-id=>'active', -name=>'active', -label=>'',
			-checked=>$data->{active}, -value=>'TRUE') ));
	print 	$q->Tr( $q->td({-colspan=>2, -align=>'right'},
			$q->submit($data->{mall_id} ? 'Submit' : 'Create New Mall') .
			($data->{mall_id} ? $q->reset(-id=>'reset') : '') ));
	print '</table>';
	print	$q->hidden(-name=>'_action', -value=>($data->{mall_id} ? 'edit' : 'add'), -force=>1),
		$q->end_form, $q->p({-class=>'fpnotes'}, $q->a({-href=>'javascript:self.close()'}, 'Close Window') ),
		$q->end_html;
}

sub EMD {
	###### empty NOT undefined
	return defined $_[0] ? $_[0] : '';
}

sub Err {
	print $q->header, $q->start_html, $q->p(@errors), $q->end_html;
}

sub FilterWhiteSpace {
###### filter out leading & trailing whitespace
	$_[0] =~ s/^\s*//;
	$_[0] =~ s/\s*$//;
	return $_[0];
}

sub LoadParams
{
	my $rv = {};
	foreach(@dbcols)
	{
		$rv->{$_} = FilterWhiteSpace( $q->param($_) );
	}
	# since we are using a checkbox on the interface, with a value of TRUE when checked
	# we don't get FALSE when not checked :)
	$rv->{active} = $rv->{active} ? 'TRUE' : 'FALSE';
	return $rv;
}

sub ShowList
{
	###### spit out a list of Malls
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => 'Master Mall List',
			-onload => 'init()',
			-style => [
				{-src=>'/css/admin/generic-report.css'},
				{-src=>'/css/admin/mall-mgt.css'}
			],
			-script => [	{-src=>'/js/yui/yahoo-min.js'},
					{-src=>'/js/yui/event-min.js'},
					{-src=>'/js/admin/mall-mgt.js'}]
		), qq|
		<div id="instructions">
		<span style="color:#b20000">All automated malls will have to be listed here.</span>
		<br />'Label' represents what will be displayed in a 'Select Your Mall Drop-down'.
		<br />Italicized/greyed items are inactive.
		<br />Click the Label to bring up the vendor list for that Mall.
		</div>
		<h5>Master Mall List</h5>
		<div class="fpnotes"><a href="${\$q->url}?_action=new" onclick="page_reload_flag=1; window.open(this.href); return false;">Create New Mall</a></div>
		<hr align="left" width="50%" /><table class="report" id="table-list"><thead><tr>
		<th>Label</th><th>Country</th><th>URL</th><th>Language</th>
		<th></th></tr></thead><tbody id="tbody-mall-mgt">|;

	my $sth = $db->prepare("
		SELECT malls.*, COALESCE(cc.country_name,'') AS country_name
		FROM malls LEFT JOIN tbl_countrycodes cc ON malls.country_code=cc.country_code
		ORDER BY country_name, malls.label");
	$sth->execute;
	my $class = 'a';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		print $q->Tr({-class=> ($tmp->{active} ? $class : "$class inactive")},
			$q->td($q->a({-class=>'label', -href=>"#$tmp->{mall_id}"}, $tmp->{label})) .
			$q->td({-class=>'country_code'}, $tmp->{country_name}) .
			$q->td( $q->a({-href=>($tmp->{url} || '#'), class=>'mall-url'}, $tmp->{url})) .
			$q->td({-class=>'language_code'}, $tmp->{language_code}) .
			$q->td( $q->a({-href=>$q->url . "?_action=detail;mall_id=$tmp->{mall_id}", -class=>'edit-url'}, 'Edit') )
		);
		$class = $class eq 'a' ? 'b':'a';
	}
	print '</tbody></table>', $q->end_html;
}

sub Update {
	my $params = shift;
	my $qry = 'UPDATE malls SET ';
	foreach my $p (@dbcols){
		next if grep $p eq $_, ('mall_id');
		$qry .= "$p=";
		$qry .= length ($params->{$p} ||'') > 0 ? $db->quote($params->{$p}) : 'NULL';
		$qry .= ',';
	}
	$qry .= 'stamp=NOW()';
	$qry .= ' WHERE mall_id= ?';
#die $qry;
	my $rv = $db->do($qry, undef, $params->{mall_id});
	die "Update failed. Query returned: $rv\nDB reported: $DBI::errstr\n" unless $rv eq '1';
	return;
}

sub VendorList
{
	die "No mall_id parameter received\n" unless my $mall_id = $q->param('mall_id');
	my $mall = $db->selectrow_hashref(q|
		SELECT malls.*, COALESCE(cc.country_name,'') AS country_name
		FROM malls LEFT JOIN tbl_countrycodes cc ON malls.country_code=cc.country_code
		WHERE mall_id=?|, undef, $mall_id);
	die "No mall found matching: $mall_id\n" unless $mall->{mall_id};
	my %xt = (
		1=>'Store Description',
		2=>'Sstore Banner',
		3=>'Store Sales',
		4=>'Store Coupons',
		5=>'Shipping Incentives',
		6=>'Expanded RP/PP',
		7=>'Expanded PP',
		7=>'Expanded PP',
		8=>'Top Pick'
	);

	###### show the list of vendors for this particular mall
	my $page_title = "Mall Vendor List: $mall->{label} - $mall->{country_name}";
#	my @collist = qw|mall_id country_code language_code label notes active stamp url|;
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-encoding => 'utf-8',
			-title => $page_title,
			-style => [{-src=>'/css/admin/generic-report.css'}, {-src=>'/css/admin/mall-mgt.css'},
				{-code=>'a.v-url{display:block; overflow:hidden; width:20em; white-space:nowrap;}'}],
			-script => [	{-src=>'/js/yui/yahoo-min.js'},
					{-src=>'/js/yui/event-min.js'},
					{-src=>'/js/yui/dom-min.js'},
					{-src=>'/js/admin/mall-mgt.js'},
				{-code=>q|
					function tg(){
					var urls = YAHOO.util.Dom.getElementsByClassName('v-url', 'a', 'tbody-mall-mgt');
				// rather than if/else every element, we'll evaluate it one time and apply the result to all
					if (urls.length == 0){ return; }
					var width = urls[0].style.width == '' ? 'auto':'';
					for (var x=0; x < urls.length; x++){
						urls[x].style.width = width;
					  }
					}|
				}],
			-onload => "mall_id=$mall_id; init();"
		),

		$q->div({-id=>'instructions'}, $q->a({-href=>'#',-onclick=>'return ToggleHelp()'}, 'Toggle Help'));
	print q|<div id="help"><ul>
			<li>Select a vendor from the drop-down and click 'Link Vendor' to bring up an interface to
			add the vendor to the mall.</li>
			<li>Click the Vendor ID to open the vendor record in a new window.</li>
			<li>Clicking the URL heading will toggle the column to full width.</li>
			<li>The URL value will open in a new window.</li>
			<li>Usrc = the source of the URL. v = vendor record, mr = mall relation record.</li>
			<li>Vs = vendor status, MRs = mall relation status.
				Turning off a vendor disables the vendor in every mall.
				Turning off a mall relation disables the vendor only in the related mall.</li>
			<li>The 'Edit' link will open the mall relation editting interface in a new window.</li>
			<li>The 'VX' link will open the list of Vendor Extensions for the relation.</li></ul>
			<p>Extension Labels</p><ul>|;
	foreach (sort keys %xt){
		print qq|<li class="xt$_">$_  - $xt{$_}</li>|;
	}
	print '</ul></div>', $q->h5($page_title),
		$q->start_form(-action=>'/cgi/admin/vms/vendor-mall-relations.cgi',
				-onsubmit=>'return NewVendorLink(this)') .
		$q->hidden('mall_id');
	###### first pull a vendor list with a field indicating whether it is already linked
	my (@linked, @unlinked_c, @unlinked, %labels) = ();
#	push @unlinked, '';	# our first empty placeholder entry
	$labels{''} = '-----';
	my $qry = "SELECT v.vendor_id, v.vendor_name, mv.mall_id,
		COALESCE(mv.url,v.url) AS url, COALESCE(v.country,'') AS country,
		CASE WHEN mv.url IS NOT NULL THEN 'mr'::TEXT ELSE 'v'::TEXT END AS usrc, v.status, mv.active\n";

	foreach (sort keys %xt){
		$qry .= qq|,SUM(case when mvx.exttype= $_ then 1 else 0 end) AS "$xt{$_}"\n|;
	}

	$qry .= "FROM vendors v LEFT JOIN mall_vendors mv ON v.vendor_id=mv.vendor_id AND mv.mall_id=$mall_id
		LEFT JOIN mall_vendor_extensions mvx ON mvx.vendor_id=v.vendor_id AND mvx.mall_id=$mall_id
		AND mvx.void=FALSE AND NOW()::DATE BETWEEN mvx.start_date AND COALESCE(mvx.end_date, NOW()::DATE)

		WHERE v.vendor_group=2
	-- exclude unlinked/inactive vendors from the drop-down
	-- but include inactive members in the list
		AND CASE
			WHEN (mv.mall_id IS NULL AND v.status=1) OR mv.mall_id IS NOT NULL THEN TRUE
			ELSE FALSE
		END = TRUE
		GROUP BY v.vendor_id, v.vendor_name, mv.mall_id, mv.url, v.url, v.country, v.status, mv.active
		ORDER BY lower(vendor_name)";
	#$qry .= "\nOFFSET = " if $offset;
	#$qry .= "\nLIMIT = " if $limit;
	my $sth = $db->prepare($qry);
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref){
		if ($tmp->{mall_id}){ push @linked, $tmp; }
		else {
			if ($tmp->{country} eq $mall->{country_code}){ push @unlinked_c, $tmp->{vendor_id}; }
			else { push @unlinked, $tmp->{vendor_id}; }
		}
		$labels{$tmp->{vendor_id}} = "$tmp->{vendor_name} ($tmp->{vendor_id})";
	}

	###### now provide a list of vendors not already linked in this mall
	my @values = '';
	push @values, $q->optgroup(-name=>'In Country', -values=>\@unlinked_c,
		-class=>'incountry', -labels=>\%labels) if @unlinked_c;
	push @values, $q->optgroup(-name=>'Other Country', -values=>\@unlinked,
		-class=>'othercountry', -labels=>\%labels) if @unlinked;
	print $q->popup_menu(
			-id=>'vendor_id',
			-name=>'vendor_id',
			-force=>1,
			-values=>\@values,
			-labels=>\%labels) .
		'&nbsp;' . $q->submit('Link Vendor') .
		$q->hidden(-name=>'_action', -value=>'new', -force=>1) . $q->end_form;

	###### now display the rest
	my $class = 'a';
	print q|<table id="vendor-links-tbl" class="report">
	<thead><tr><th>Vendor ID</th><th>Vendor Name</th><th><a href="#" onclick="tg();return false;">URL</a></th>
		<th>Usrc</th>
		<th>VS</th><th>MRS</th><th></th>|;
	foreach (sort keys %xt){
		print "<th>$_</th>";
	}

	print q|</tr></thead><tbody id="tbody-mall-mgt">|;
	foreach (@linked){
		print qq|<tr class="| . $class . ($_->{status} == 1 && $_->{active} ? '' : ' inactive') . qq|">| .
			$q->td($q->a({-class=>'vid', -href=>'#'}, $_->{vendor_id})) .
			$q->td({-class=>'vn'}, $q->escapeHTML($_->{vendor_name})) .
			$q->td({-class=>'vurl'},
				$q->a({-href=>'#', -class=>'v-url'}, $q->escapeHTML($_->{url}))) .
			$q->td({-class=>'status'}, $_->{usrc}) .
			$q->td({-class=>($_->{status} ? 'status-on':'status-off')}, ($_->{status} ? 'ok' : 'x')) .
			$q->td({-class=>($_->{active} ? 'status-on':'status-off')}, ($_->{active} ? 'ok' : 'x')) .
			$q->td({-class=>'edit-vx'},
				$q->a({-class=>'edit-url', -href=>"#$_->{vendor_id}"}, 'Edit') .
				$q->a({-class=>'vx-url', -href=>"#$_->{vendor_id}"}, 'VX') .
				$q->a({-class=>'ppods-url', -href=>"#$_->{vendor_id}"}, 'PPoDs'));
		foreach my $k (sort keys %xt){
			print $q->td({-class=>"xt$k"}, $_->{$xt{$k}} || '');
		}
		print '</tr>';
		$class = $class eq 'a' ? 'b':'a';
	}
	print '</tbody></table>',
		$q->end_html;
}

###### 40/23/08 added the PPoDs link to the mix
