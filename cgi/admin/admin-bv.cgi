#!/usr/bin/perl -w
###### admin-bv.cgi Browser View
###### administrative viewing of HTML "mailings" as well as the textual versions
###### created: 11/09	Bill MacArthur
###### last modified: 

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S;
use ADMIN_DB;
use HtmlMailings;
use MailingUtils;
use Data::Dumper;

my $q = new CGI;

# run in SSL mode only
unless ($q->https){
	my $url = $q->self_url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my ($db, %params) = ();
my $member = {};

LoadParams();

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
die "DB login required" unless $operator && $pwd;
$db = ADMIN_DB::DB_Connect('bv', $operator, $pwd) || exit;
$db->{RaiseError} = 1;
my $HM = HtmlMailings->new({'db'=>$db});
$HM->{'BrowserView'} = 1 unless $params{'sendto'};	# if we are sending this an an email, we don't want browser view
$HM->{'NoTTprocess'} = 1 if $params{'NoTTprocess'};

unless ($params{type}){
	DoInterface();
	End();
}

die "A member ID is required" unless $params{'id'};
die "A document ID is required" unless $params{'docid'};

$HM->init({'docid'=>$params{'docid'}, 'lang_pref'=>($params{'lp'} || 'en')});

my $qry = "
	SELECT members.*, members.oid" .
	($HM->config()->{'sql'}->{'extra_cols'} || '') .
	" FROM members " .
	($HM->config()->{'sql'}->{'extra_tables'} || '') .
	" WHERE members.id=?" .
       ($HM->config()->{'sql'}->{'extra_where'} || '') .
	"\n" .
	($HM->config()->{'sql'}->{'finish'} || '');
#warn "$qry\n";
$member = $db->selectrow_hashref($qry, undef, $params{'id'}) || Err("No member match for: $params{'id'}");
		
# if we have not received a membership to plugin, they we will default to not interpolating the templates
$HM->{'NoTTprocess'} = 1 unless exists $member->{'id'};

my $rv = ();
if ($params{type} eq 'html'){
	$rv = $HM->GenerateHtmlDocument({'member'=>$member}, $params{'lp'});
}
elsif ($params{type} eq 'text') {
	$rv = $HM->GenerateTextDocument({'member'=>$member}, $params{'lp'});
}
else {
	Err('Unrecognized type');
}

if ($params{'sendto'}){
	SendMail();
}
else {
	if ($params{type} eq 'html'){
		print $q->header(-charset=>'utf-8'), $$rv;
	}
	else {
	#	print $q->header(-type=>'text-plain', -charset=>'utf-8'), $rv;
		print $q->header(-charset=>'utf-8'), $q->start_html($HM->Subject({'member'=>$member})), $q->start_form,
			$q->textarea(
				-name=>'_',
				-style=>'border:0; width:100%; height:100%; white-space:pre-wrap;',
				-value=>$$rv,
				-cols=>100,
				-rows=>60),
			$q->end_form, $q->end_html; 
	}
}

End();

###### ######

sub DoInterface
{
	# present an interface for the user to enter a document configuration template ID
	# an optional member ID and other items to determine what they want to see
	print $q->header, $q->start_html(
		-title=>'View HtmlMailings',
		-style=>{code=>'body,td,input,select {font:normal 11px sans-serif;}'}
		),
		$q->start_form(-method=>'get'),
		'<table><tr>
			<td>Select a document or enter a doc ID</td><td>';
			
		my ($vals, $labels) = _docids();
	print $q->popup_menu(
			-name=>'_docid',
			-values=>$vals,
			-labels=>$labels,
			-onchange=>"document.getElementById('docid').value=this.options[this.selectedIndex].value"
		),
		$q->textfield(-name=>'docid', -id=>'docid'),
		'</td></tr><tr><td>Select a member or enter a member ID</td><td>';
		
		($vals, $labels) = _testMembers();
	print $q->popup_menu(
			-name=>'_id',
			-values=>$vals,
			-labels=>$labels,
			-onchange=>"document.getElementById('id').value=this.options[this.selectedIndex].value"
		),
		$q->textfield(-name=>'id', -id=>'id'),
		'</td></tr><tr><td>Force email to this language</td><td>';
		
		($vals, $labels) = _langs();
	print $q->popup_menu(
			-name=>'lp',
			-values=>$vals,
			-labels=>$labels
		),
		'</td></tr><tr><td>Email Type</td><td>',
		$q->popup_menu(
			-name=>'type',
			-values=>[qw/html text/]
		),
		'</td></tr><tr><td>Send email to</td><td>',
		$q->textfield(-name=>'sendto', -style=>'width:20em'),
		$q->span({-style=>'margin-left:2em; color:blue'}, 'Leave this blank to just generate a page'),
		'</td></tr><tr><td>Do not interpolate placeholders</td><td>',
		$q->checkbox(-name=>'NoTTprocess', -value=>1, -label=>''),
		'</td></tr><tr><td></td><td>',
		$q->submit, $q->reset,
		'</td></tr></table>',
		$q->end_form,
		$q->p({-style=>'margin:2em'}, $q->span({-style=>'background-color:#ffc;'},
			'Remember that you will only see English images, even if you specify another language to view,
			unless you change your client (browser or email) settings to the other language.')),
		$q->end_html;
}

sub End
{
	$db->disconnect;
	exit;
}

sub Err
{
	print $q->header, $q->start_html('Error'), $q->div($_[0]), $q->end_html;
	End();
}

sub LoadParams
{
	foreach(qw/id docid lp type sendto NoTTprocess/)
	{
		$params{$_} = $q->param($_);
	}
}

sub SendMail
{
	my $mu = MailingUtils->new();
	my %mail = (subject=>$HM->Subject({member=>$member}, ($params{lp} || $member->{language_pref})));
	$mail{txt} = $rv if $params{type} eq 'text';
	$mail{html} = $rv if $params{type} eq 'html';
	$mail{from} = $HM->From();
	$mail{to} = $params{sendto};
#die Dumper($HM->{config});
	print $q->header('text/plain');
	my $_rv = $mu->Mail(%mail);
	if ($_rv eq '1'){
		print "Mail sent to $params{sendto}";
	}
	else {
		print $MailingUtils::error;
	}
}

sub _cboData
{
	my $qry = shift;
	my @vals = '';
	my %labels = ();
	my $sth = $db->prepare($qry);
	$sth->execute;
	while (my ($c, $d) = $sth->fetchrow_array)
	{
		push @vals, $c;
		$labels{$c} = $d;
	}
	return (\@vals, \%labels);
}


sub _docids
{
	# we can fill in some standard emails into this function later
	return ['', 10810, 10842, 10731, 10747, 1040, 10800, 10803, 1017, 1019, 1025, 1021, 10852, 1051],
		{
			10810=>'Affiliate Partner Welcome',
			10731=>'Associate Member Welcome (old)',
			10747=>'Shopper Member Welcome',
			1040=>'VIP Welcome',
			10800=>'Duplicate notice (AP)',
			10803=>'Duplicate notice (AMP/VIP)',
			1017=>'Newsletter AP',
			1019=>'Newsletter VIP',
			1025=>'Clubshop Newsletter',
			1021=>'Newsletter TEST Config',
			10852=>'ClubShop Survey taken by Pool Member',
			1051=>'One-off marketing push email'
		};
}

sub _langs
{
	return _cboData("
		SELECT code2, description FROM language_codes WHERE code2 IN (
			'en','it','fr','nl','be','es','ru','sk','de','sq','hi','gr','ro' )
		ORDER BY description
	");
}

sub _testMembers
{
	return _cboData("
		SELECT id, alias||' - '||firstname||' '||lastname||' ('||membertype||')' AS label
		FROM members WHERE spid=12 AND membertype IN ('s','m','v','amp')
		AND emailaddress ~ '\@'
		ORDER BY id
	");
}
