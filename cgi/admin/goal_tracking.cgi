#!/usr/bin/perl -w
###### goal_tracking.cgi
###### 
###### Calculate and display the monthly DHS Club marketing goals.
###### 	Pass the parameter static=1 if a static html page is desired.
###### 
###### Created: 01/13/05	Karl Kohrt
###### 
###### Last modified: 07/31/06 Bill MacArthur

use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use DB_Connect;
use XML::Simple;

###### CONSTANTS
our $REPORT = 10369;		# Main report template.
our $BASELINES = 10372;	# Baseline data to compare to.

###### GLOBALS
our $q = new CGI;
our $xml = new XML::Simple(NoAttr=>1);
our %months = ( 	'jan'	=> '01',
			'feb'	=> '02',
			'mar'	=> '03',
			'apr'	=> '04',
			'may'	=> '05',
			'jun'	=> '06',
			'jul'	=> '07',
			'aug'	=> '08',
			'sep'	=> '09',
			'oct'	=> '10',
			'nov'	=> '11',
			'dec'	=> '12' );
our $leap_year = 2000;          # A leap year.
our %MONTH_DAYS = (     '01'    => 31,
                        '02'    => 28,
                        '03'    => 31,
                        '04'    => 30,
                        '05'    => 31,
                        '06'    => 30,
                        '07'    => 31,
                        '08'    => 31,
                        '09'    => 30,
                        '10'    => 31,
                        '11'    => 30,
                        '12'    => 31,
                        '13'    => 29); # Leap year - February.
our $db = '';
our $admin = 0;
our $login_error = 0;
our $errors = '';
our $message = '';
our $current_month = '';	# This represents the month we are working with.
our $days_ratio = 0;		# The ratio of the part of the month that has passed.
our $static = '';		# The flag sent in to create a static html file.
our $operator = '';
our $pwd = '';

###### Member variables.
our $mem_net_growth = 0;
our $mem_net_growth_perc = 0;
our $mem_tot_goal = 0;
our $mem_tot_base = 0;
our $goal_mem_inc_this = 0;
our $goal_mem_tot_this = 0;
our $act_mtd_mem = 0;
our $goal_mtd_mem = 0;
our $diff_goal_mtd_mem = 0;
our $pro_mtd_mem = 0;
our $pro_mem_tot_this = 0;

###### VIP variables.
our $vip_net_growth = 0;
our $vip_net_growth_perc = 0;
our $vip_tot_goal = 0;
our $vip_tot_base = 0;
our $goal_vip_inc_this = 0;
our $goal_vip_tot_this = 0;
our $goal_mtd_vip = 0;
our $act_vip_ups = 0;
our $act_vip_downs = 0;
our $act_vip_terms = 0;
our $act_mtd_vip_inc = 0;
our $act_vip_tot_this = 0;
our $diff_goal_mtd_vip = 0;
our $pro_vip_tot_inc = 0;
our $pend_vip_terms = 0;
our $pro_mtd_vip = 0;
our $pro_vip_tot_this = 0;

###### Non-Fees PayPoints variables.
our $nfpp_mtd_tot = 0;
our $nfpp_net_growth = 0;
our $nfpp_net_growth_perc = 0;
our $nfpp_tot_goal = 0;
our $nfpp_tot_base = 0;
our $goal_nfpp_inc_this = 0;
our $goal_nfpp_tot_this = 0;
our $act_mtd_nfpp = 0;
our $goal_mtd_nfpp = 0;
our $diff_goal_mtd_nfpp = 0;
our $pro_mtd_nfpp = 0;
our $pro_nfpp_tot_this = 0;

################################
###### MAIN starts here.
################################

# Unless this is a cron job, get and test the cookie information for access.
unless ( $static = $q->param('static') )
{
	# Get the admin user cookie.
	$operator = $q->cookie('operator'); $pwd = $q->cookie('pwd');
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	$db = ADMIN_DB::DB_Connect( 'goal_tracking.cgi', $operator, $pwd )
}
else { $db = DB_Connect( 'goal_tracking.cgi' ) }

# Check for errors.
unless ( $db )
{
	$login_error = 1;
}
# Continue if we have a good login/cookie.
unless ( $login_error )
{
	# Get the baseline data from the XML template.
	Get_Baseline_Data();

	# Get the month-to-date data from the database.
	Get_MTD_Data();

	# Calculate the ratio equal to the part of the month that has passed.
	$days_ratio = Calc_Part_Month();

	# Calculate the data related to the VIPs.
	Calc_VIP_Data();

	# Calculate the data related to the Members.
	Calc_Member_Data();

	# Calculate the data related to the Non-Fee PayPoints.
	Calc_NFPP_Data();

	Display_Page();
}

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################


###### Calculate the part of the month that has passed.
sub Calc_Part_Month
{
	my $days = 0;
	# Today's date.
	my ($day, $month, $year) = (localtime)[3,4,5];
	$month++; $year += 1900;
	if ( length($month) == 1 ) { $month = '0' . $month };

	# Check for a leap year.
	if ( $month eq '02'
		&& (($year - $leap_year) / 4) == int(($year - $leap_year) / 4) )
	{
		$days = $MONTH_DAYS{13};
	}
	else { $days = $MONTH_DAYS{$month}; }

	# Return the ratio equal to the part of the month that has passed.
	return $day/$days;
}

###### Calculates the Member data.
sub Calc_Member_Data
{
	# If this is a percentage based goal.
	if ( $mem_tot_goal =~ m/%/ )
	{
		(my $mem_tot_goal_calc = $mem_tot_goal) =~ s/%//;
		$mem_tot_goal_calc /= 100;
		$goal_mem_inc_this = $mem_tot_goal_calc * $mem_tot_base;
		$goal_mem_inc_this = round($goal_mem_inc_this);
	}
	else
	{
		($goal_mem_inc_this = $mem_tot_goal) =~ s/^(\+)//;
	}
	$goal_mem_tot_this = $goal_mem_inc_this + $mem_tot_base;
	$goal_mtd_mem = round($goal_mem_tot_this * $days_ratio);

	# Calculate the difference of actual from goal month-to-date.
	$diff_goal_mtd_mem = $act_mtd_mem - $goal_mtd_mem;

	# Calculate the projected total for the month.
	$pro_mtd_mem = round($act_mtd_mem / $days_ratio);

	# Calculate the projected final difference of actual from goal.
	$pro_mem_tot_this = $pro_mtd_mem - $goal_mem_tot_this;

	# Calculate the % net growth.
	$mem_net_growth = $act_mtd_mem - round($mem_tot_base * $days_ratio);
	$mem_net_growth_perc = (($pro_mtd_mem / $mem_tot_base) - 1) * 100;
	$mem_net_growth_perc = sprintf("%.2f", $mem_net_growth_perc) . "%";
}

###### Calculates the Non-Fees PayPoints data.
sub Calc_NFPP_Data
{
	# If this is a percentage based goal.
	if ( $nfpp_tot_goal =~ m/%/ )
	{
		(my $nfpp_tot_goal_calc = $nfpp_tot_goal) =~ s/%//;
		$nfpp_tot_goal_calc /= 100;
		$goal_nfpp_inc_this = $nfpp_tot_goal_calc * $nfpp_tot_base;
		$goal_nfpp_inc_this = sprintf("%.2f", $goal_nfpp_inc_this);
	}
	else
	{
		($goal_nfpp_inc_this = $nfpp_tot_goal) =~ s/^(\+)//;
	}
	$goal_nfpp_tot_this = $goal_nfpp_inc_this + $nfpp_tot_base;
	$goal_mtd_nfpp = sprintf("%.2f", $goal_nfpp_tot_this * $days_ratio);

	# Calculate the difference of actual from goal month-to-date.
	$diff_goal_mtd_nfpp = sprintf("%.2f", $act_mtd_nfpp - $goal_mtd_nfpp);

	# Calculate the projected total for the month.
	$pro_mtd_nfpp = sprintf("%.2f", $act_mtd_nfpp / $days_ratio);

	# Calculate the projected final difference of actual from goal.
	$pro_nfpp_tot_this = sprintf("%.2f", $pro_mtd_nfpp - $goal_nfpp_tot_this);

	# Calculate the % net growth.
	$nfpp_net_growth = sprintf("%.2f", $act_mtd_nfpp - sprintf("%.2f", $nfpp_tot_base * $days_ratio));
	$nfpp_net_growth_perc = (($pro_mtd_nfpp / $nfpp_tot_base) - 1) * 100;
	$nfpp_net_growth_perc = sprintf("%.2f", $nfpp_net_growth_perc) . "%";
}

###### Calculates the VIP data.
sub Calc_VIP_Data
{
	# If this is a percentage based goal.
	if ( $vip_tot_goal =~ m/%/ )
	{
		(my $vip_tot_goal_calc = $vip_tot_goal) =~ s/%//;
		$vip_tot_goal_calc /= 100;
		$goal_vip_inc_this = $vip_tot_goal_calc * $vip_tot_base;
		$goal_vip_inc_this = round($goal_vip_inc_this);
	}
	else
	{
		($goal_vip_inc_this = $vip_tot_goal) =~ s/^(\+)//;
	}
	$goal_vip_tot_this = $goal_vip_inc_this + $vip_tot_base;
	$goal_mtd_vip = round($goal_vip_inc_this * $days_ratio);

	# Calculate the actual month-to-date increase/decrease and total.
	$act_mtd_vip_inc = $act_vip_ups - $act_vip_downs - $act_vip_terms;
	$act_vip_tot_this = $act_mtd_vip_inc + $vip_tot_base;

	# Calculate the difference of actual from goal month-to-date.
	$diff_goal_mtd_vip = $act_mtd_vip_inc - $goal_mtd_vip;

	# Calculate the projected total increase for the month.
	$pro_vip_tot_inc = round($act_mtd_vip_inc / $days_ratio);

	# Calculate the projected total for the month.
	$pro_vip_tot_this = $pro_vip_tot_inc + $vip_tot_base - $pend_vip_terms;

	# Calculate the projected final difference of actual from goal.
	$pro_mtd_vip = $pro_vip_tot_this - $goal_vip_tot_this;

	# Calculate the % net growth.
	$vip_net_growth = $act_vip_tot_this - $vip_tot_base;
	$vip_net_growth_perc = ($vip_net_growth / $vip_tot_base) * 100;
	$vip_net_growth_perc = sprintf("%.2f", $vip_net_growth_perc) . "%";
}

###### Display the report page.
sub Display_Page
{
	my $report = G_S::Get_Object($db, $REPORT) ||
		die "Failed to load template: $REPORT\n";

	my ($min, $hour, $day, $month, $year) = (localtime)[1,2,3,4,5];
	$month++; $year += 1900;
	if ( length($hour) == 1 ) { $hour = "0" . $hour }
	if ( length($min) == 1 ) { $min = "0" . $min }
	my $timestamp = "$month-$day-$year $hour:$min PT";
	$report =~ s/%%timestamp%%/$timestamp/g;

	# The baseline data.
	$current_month = ucfirst($current_month);
	$current_month =~ s/(\d{2}$)/ 20$1/;
	$report =~ s/%%current_month%%/$current_month/g;

	# The VIP data.
	$report =~ s/%%vip_tot_base%%/$vip_tot_base/g;
	$report =~ s/%%vip_tot_goal%%/$vip_tot_goal/g;
	$report =~ s/%%act_vip_ups%%/$act_vip_ups/g;
	$report =~ s/%%act_vip_downs%%/$act_vip_downs/g;
	$report =~ s/%%act_vip_terms%%/$act_vip_terms/g;
	$report =~ s/%%pend_vip_terms%%/$pend_vip_terms/g;
	$report =~ s/%%vip_net_growth%%/$vip_net_growth/g;
	$report =~ s/%%vip_net_growth_perc%%/$vip_net_growth_perc/g;
	$report =~ s/%%goal_vip_inc_this%%/$goal_vip_inc_this/g;
	$report =~ s/%%goal_vip_tot_this%%/$goal_vip_tot_this/g;
	$report =~ s/%%goal_mtd_vip%%/$goal_mtd_vip/g;
	$report =~ s/%%act_mtd_vip_inc%%/$act_mtd_vip_inc/g;
	$report =~ s/%%act_vip_tot_this%%/$act_vip_tot_this/g;
	$report =~ s/%%diff_goal_mtd_vip%%/$diff_goal_mtd_vip/g;
	$report =~ s/%%pro_vip_tot_inc%%/$pro_vip_tot_inc/g;
	$report =~ s/%%pro_mtd_vip%%/$pro_mtd_vip/g;
	$report =~ s/%%pro_vip_tot_this%%/$pro_vip_tot_this/g;

	# The Member data.
	$report =~ s/%%mem_tot_base%%/$mem_tot_base/g;
	$report =~ s/%%mem_tot_goal%%/$mem_tot_goal/g;
	$report =~ s/%%act_mtd_mem%%/$act_mtd_mem/g;
	$report =~ s/%%mem_net_growth%%/$mem_net_growth/g;
	$report =~ s/%%mem_net_growth_perc%%/$mem_net_growth_perc/g;
	$report =~ s/%%goal_mem_inc_this%%/$goal_mem_inc_this/g;
	$report =~ s/%%goal_mem_tot_this%%/$goal_mem_tot_this/g;
	$report =~ s/%%goal_mtd_mem%%/$goal_mtd_mem/g;
	$report =~ s/%%diff_goal_mtd_mem%%/$diff_goal_mtd_mem/g;
	$report =~ s/%%pro_mtd_mem%%/$pro_mtd_mem/g;
	$report =~ s/%%pro_mem_tot_this%%/$pro_mem_tot_this/g;

	# The Non-Fees PayPoints data.
	$report =~ s/%%nfpp_tot_base%%/$nfpp_tot_base/g;
	$report =~ s/%%nfpp_tot_goal%%/$nfpp_tot_goal/g;
	$report =~ s/%%act_mtd_nfpp%%/$act_mtd_nfpp/g;
	$report =~ s/%%nfpp_net_growth%%/$nfpp_net_growth/g;
	$report =~ s/%%nfpp_net_growth_perc%%/$nfpp_net_growth_perc/g;
	$report =~ s/%%goal_nfpp_inc_this%%/$goal_nfpp_inc_this/g;
	$report =~ s/%%goal_nfpp_tot_this%%/$goal_nfpp_tot_this/g;
	$report =~ s/%%goal_mtd_nfpp%%/$goal_mtd_nfpp/g;
	$report =~ s/%%diff_goal_mtd_nfpp%%/$diff_goal_mtd_nfpp/g;
	$report =~ s/%%pro_mtd_nfpp%%/$pro_mtd_nfpp/g;
	$report =~ s/%%pro_nfpp_tot_this%%/$pro_nfpp_tot_this/g;

	# Put out to a file for static display.
	if ( $static )
	{
		# Move the 3-day old file.
		rename "/home/httpd/html/admin/goal_tracking-2.html",
			"/home/httpd/html/admin/goal_tracking-3.html";
		# Move the 2-day old file.
		rename "/home/httpd/html/admin/goal_tracking-1.html",
			"/home/httpd/html/admin/goal_tracking-2.html";
		# Move the 1-day old file.
		rename "/home/httpd/html/admin/goal_tracking-0.html",
			"/home/httpd/html/admin/goal_tracking-1.html";
		# Open our static file and print to it for later viewing.
 		open (OUTFILE, ">/home/httpd/html/admin/goal_tracking-0.html") || die "Couldn't open the OUTPUT file.\n";
 		print OUTFILE $report;
 		close OUTFILE;
 	}
	# Put it out to the page as a real-time report.
	else
	{ 
		print $q->header(-expires=>'now'), $report;
	}
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br>$_[0]<br>";
}

###### Get the baseline data and assign it to local variables.
sub Get_Baseline_Data
{
	my $baselines = G_S::Get_Object($db, $BASELINES) ||
	die "Failed to load template: $BASELINES\n";

	# Load the baseline data from the XML template into a hashref.
	$baselines = $xml->XMLin($baselines);

	# Assign the baseline data to local variables.
	$current_month = $baselines->{current_month};
	$vip_tot_base = $baselines->{vips}{$current_month}{baseline};
	$mem_tot_base = $baselines->{new_mems}{$current_month}{baseline};
	$nfpp_tot_base = $baselines->{nfpp}{$current_month}{baseline};

	# Assign the goals.
	$vip_tot_goal = $baselines->{vips}{$current_month}{goal};
	$mem_tot_goal = $baselines->{new_mems}{$current_month}{goal};
	$nfpp_tot_goal = $baselines->{nfpp}{$current_month}{goal};
}

###### Get the month-to-date data from the database.
sub Get_MTD_Data
{
	# Create a postgres partial date string for database searches.
	my $year_month = "20" . substr($current_month,3,2)
				. "-"
				. $months{lc(substr($current_month,0,3))}
				. "-";
	my $first_of_month = $year_month . "01";

	# Get the number of new VIPs.
	$act_vip_ups = $db->selectrow_array("	SELECT count(id)
							FROM 	activity_rpt_data
							WHERE 	activity_type = 12
							AND 	rpt_date::TEXT  ~ '$year_month';");

	# Get the number of VIP downgrades and cancellations less any reversals.
	$act_vip_downs = $db->selectrow_array("	SELECT count(id)
							FROM  	activity_rpt_data
							WHERE 	(activity_type = 13
								OR 	activity_type = 14)
							AND 	rpt_date::TEXT  ~ '$year_month';");
	my $act_vip_down_reverse = $db->selectrow_array("SELECT count(id)
								FROM  	activity_rpt_data
								WHERE 	activity_type = 20
							AND 	rpt_date::TEXT ~ '$year_month';");
	$act_vip_downs -= $act_vip_down_reverse;

	# Get the number of VIP terminations.
	$act_vip_terms = $db->selectrow_array("	SELECT count(c.id)
							FROM 	cancellations c
								INNER JOIN cancellations_arc ca
								ON c.id = ca.id
							WHERE 	c.status = 'p'
							AND	ca.status ~ '^t'
							AND 	c.stamp::TEXT ~ '$year_month';");

	# Get the number of VIP pending terminations.
	$pend_vip_terms = $db->selectrow_array("	SELECT count(mop.id)
							FROM 	mop
								INNER JOIN members 
								ON mop.id = members.id
							WHERE 	membertype = 'v'
							AND 	paid_thru < '"
							. $first_of_month . 
							"'::date - interval '1 month';");

	# Get the number of new Members.
	$act_mtd_mem = $db->selectrow_array("	SELECT count(id)
							FROM 	activity_rpt_data
							WHERE 	rpt_date::TEXT ~ '$year_month'
							AND 	activity_type = 2
							AND 	(val_text = '' 
								OR val_text is null);");

	# Get the month-to-date amount of non-fee paypoints.
	$act_mtd_nfpp = $db->selectrow_array("	SELECT sum(npp) AS non_fee_paypoints
							FROM 	ta_live
							WHERE 	id = 3127951
							AND 	(transaction_type = 2
								OR transaction_type = 3
								OR transaction_type = 5
								OR transaction_type = 6)
							AND 	(period = get_working_period(1, '"
								. $first_of_month
								. "')
								OR period = get_working_period(2, '"
								. $first_of_month
								. "'));")
							|| 0;
}

###### Round a number to the nearest integer
sub round
{
    my $number = shift;
    return int($number + .5 * ($number <=> 0));	# This allows for negative and postive rounding.
}


###### 01/31/05 Added code to allow the script to create a static html page using the flag $static.
######		Also added the ability to bypass admin login if creating the static page.
###### 02/01/05 Modified the sub Get_MTD_Data so that it uses the baseline $current_month instead
######		of the first_of_month() function in Postgres. This keeps the data consistent after
######		we move into the next month and before we update the baseline data, though the data
######		is effectively stale until we change the baseline data. It runs faster too.
###### 02/03/05 Stopped figuring the Calc_Part_Month using yesterday per Mark.
######		We'll use today instead which gives good end-of-day results, but poorer early results.
###### 05/02/05 Added file rotation (4 days total) to the Display_Page sub for long weekends that
######		encompass the end of the month.
###### 01/03/06 Added leading zeros to the %MONTH_DAYS hash to correct a problem that surfaced
######		after the first of the year.
######		Stopped removing negative sign(s) from goal figures.
# 07/31/06 added an expiration header to the report output
# 02/09/2011 g.baker postgres 9.0 port date/text casts
