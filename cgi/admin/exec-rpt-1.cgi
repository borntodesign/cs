#!/usr/bin/perl -w
###### exec-rpt.cgi.cgi
###### admin script to Exec stats under the new comp plan
###### last modified: 11/17/03	Bill MacArthur

use strict;
use DBI;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Carp qw(fatalsToBrowser);
use G_S qw(%MEMBER_TYPES);

###### GLOBALS
our $q = new CGI;
my $AGGREGATES_TBL = 'network_calcs';
my $LEVELS_TBL = 'level_values';
my $me = $q->url;
my ($db, $dat, @list) = ();
my $id = $q->param('id');

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

###### we could have tested for the admin flag, but just in case we have someone trying to access things
###### without going through the admin interface, we may be able to stop 'em here
if ($operator && $pwd)
{
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'merch-trans_stats.cgi', $operator, $pwd )){exit}
	$db->{RaiseError} = 1;
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}

unless ($id)
{
	Get_List();
	Show_List();
}
else
{
	if ($id =~ /\D/)
	{
		Err('IDs must be numeric');
	}
	else
	{
		if (Get_Stats())
		{
			Render_Page();
		}
		else
		{
			Err('No match on that ID')
		}
	}
}
END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Get_List
{
	my $sth = $db->prepare("	SELECT m.id,
						m.spid,
						m.alias,
						m.firstname ||' '|| m.lastname AS name,
						COALESCE(lv.lname, 'n/a') AS a_level,
						COALESCE(nc.ppp::TEXT, 'n/a') AS ppp,
						(COALESCE(nc.npp, 0) - COALESCE(nc.exec_breakaway_volume, 0))::NUMERIC(8,2) AS npp,
						COALESCE(nc.calculated_pay::TEXT, 'n/a') AS calculated_pay,
						COALESCE(nc.bonus::TEXT, 'n/a') AS bonus,
						(COALESCE(nc.calculated_pay, 0) +  COALESCE(nc.bonus, 0))::NUMERIC(8,2) AS ttl_comp
					FROM 	members m,
						$AGGREGATES_TBL nc,
						$LEVELS_TBL lv
					WHERE m.id=nc.id
					AND 	lv.lvalue=nc.a_level
					AND 	nc.a_level >= 11
					AND 	m.membertype IN ('v','m','ag')
					ORDER BY ttl_comp DESC, nc.a_level DESC");

	$sth->execute();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$dat->{$tmp->{id}} = $tmp;
		push (@list, $tmp->{id});
	}
	$sth->finish;
}

sub Get_Stats
{
	my $sth = $db->prepare("	SELECT m.alias,
						m.firstname ||' '|| m.lastname AS name,
						COALESCE(lv.lname, 'n/a') AS a_level,
						COALESCE(nc.ppp::TEXT, 'n/a') AS ppp,
						(COALESCE(nc.npp, 0) - COALESCE(nc.exec_breakaway_volume, 0))::NUMERIC(8,2) AS npp,
						COALESCE(nc.calculated_pay::TEXT, 'n/a') AS calculated_pay,
						COALESCE(nc.bonus::TEXT, 'n/a') AS bonus,
						(nc.calculated_pay + nc.bonus) AS ttl_comp,
						COALESCE(nc.exec_report, '') AS exec_report
					FROM 	members m
					LEFT JOIN $AGGREGATES_TBL nc
					ON 	m.id=nc.id
					LEFT JOIN $LEVELS_TBL lv
					ON	lv.lvalue=nc.a_level
					WHERE 	m.id= ?");

	$sth->execute($id);
	$dat = $sth->fetchrow_hashref;
	$sth->finish;
}

sub Render_Page
{
	my $tmpl = G_S::Get_Object($db, 10232) || die "Failed to load template: 10232.";

	foreach (keys %{$dat})
	{
		$tmpl =~ s/%%$_%%/$dat->{$_}/;
	}
	print $q->header, $tmpl;
}

sub Show_List
{
	print $q->header, $q->start_html(-bgcolor=>'#ffffff', -style=>'td{font-size: 9pt}');
	print qq|<table border="1" cellspacing="0" cellpadding="3">\n|;
	print qq|	<tr><th>Alias</th><th>PPP</th><th>NPP</th>
			<th>Calculated Pay</th><th>Bonus</th>
			<th>Total Compensation</th><th>Level</th></tr>\n|;

	my $bg = '#ffffcc';
	foreach (@list)
	{
		print $q->Tr({-bgcolor=>$bg},
				$q->td($q->a({-href=>"$me?id=$_", -target=>'_blank'}, $dat->{$_}{alias})) .
				$q->td($dat->{$_}{ppp}) .
				$q->td($dat->{$_}{npp}) .
				$q->td($dat->{$_}{calculated_pay}) .
				$q->td($dat->{$_}{bonus}) .
				$q->td($dat->{$_}{ttl_comp}) . 
				$q->td($dat->{$_}{a_level})
			);
		$bg = ($bg eq '#ffffcc') ? '#ffffff' : '#ffffcc';
	}

	print "</table>", $q->end_html;
}

