#!/usr/bin/perl -w
# help-joyce.cgi.cgi
# a quickie interface to assist in setting up affiliate-vendor records

use strict;
use CGI;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my ($db) = ();
###### we will not allow admin access for update use (we have an in-house interface for that)
if ( $operator && $pwd )
{
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
	$db->{RaiseError} = 1;
}
else
{
	die 'You must be logged in to use this interface.';
}

my $pk = $q->param('pk') || die "Failed to receive pk param\n";
my $vid = $q->param('vendor_id') || die "Failed to receive vendor_id param\n";
my $qry = 'INSERT INTO linkshare_affiliates_2vendors (vendor_id, affiliate_code, vendor_name_map, advertiser_id)
	SELECT ?, affcode, advname, advid FROM affload WHERE pk=?;

	UPDATE affload SET processed=TRUE WHERE pk=?;
';
my $rv = $db->do($qry, undef, ($vid, $pk, $pk));
###### if we didn't die from a DBI error, I guess we are all set
print $q->header('text/plain'), $rv;
$db->disconnect;
exit;



