#!/usr/bin/perl -w
###### mv-members.cgi
###### the member, partner, VIP, what have you transfer interface
###### newly created: Dec. 2010	Bill MacArthur
###### last modified: 06/06/12	Bill MacArthur

###### ###### THIS SHOULD NOT BE USED IN PRODUCTION FOR PARTNERS... IT IS FOR ADMIN USE FOR COOP TRANSFERS ONLY ###### ######

use strict;
use Digest::MD5 qw( md5_hex );
use Encode;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/glocalincome.com/cgi-lib');
use G_S qw(%MEMBER_TYPES $EXEC_BASE);
use DB_Connect;
use Template;
#use Apache::GI_Authen;
#use GI_Authen_Constants qw($LoginScript);
use GI;
use XML::local_utils;
require ADMIN_DB;
#use Mail::Sendmail;	# only for recipient mailings if still being done from here
#use Data::Dumper;	# only for debugging

my $lu = XML::local_utils->new;
my $TT = Template->new;
my $gs = new G_S;
my $q = new CGI;
my $Path = $q->path_info || '';
my $ME = $q->script_name;# . $Path;
$Path =~ s#^/##;

my $key = 'onykey!23#';
my $LIMIT = 25;	###### the default number of results we will return for a list

my %TMPL = (
	'no-pay'	=> 1319,
	'Error'		=> 1307,
	'screen1'	=> 1322,
	'XML'		=> 1323,
	'screen2'	=> 1324,
	'screen3'	=> 1325,
	'screen4'	=> 1326
);

#my %EMAIL_TMPL = (
#	'vip-gets-members'	=> 10333,
#	'staff_bcc'			=> 10367,	# the template ID that holds the folowing val
#	'the_address'		=> ''		# the lucky dog to get copied in ;->
#);

my ($admin, $db, $realOP, %param, @mvid, $lang_blocks, @alerts) = ();

###### these globals pertain to an actual transfer
my ($firstID, $lastID, $transfer_list, $proc_status) = '';

my $recipient = {};
my $activity = $q->param('activity') || '';
my $formlist = \'';
my $operator = $q->cookie('operator');

Load_Params();

###### evaluate our user
###### we will first check for admin credentials
###### if present, we will go in as 01 with 'super credentials'
###### a regular login as 01 will permit the same operations
if ($operator && (my $pwd = $q->cookie('pwd')))
{
	unless ($db = ADMIN_DB::DB_Connect( 'mv-members', $operator, $pwd )){exit}
	$db->{'RaiseError'} = 1;

#	$realOP = {
#		'id'		=> 1,
#		'alias'		=> 'DC00001',
#		'firstname'	=> $operator,
#		'lastname'	=> 'The DHS Club',
#		'emailaddress'	=> '',
#		'spid'		=> 1,
#		'membertype'	=> 'v'
#	};
	$admin = 1;
}
else
{
	my $url = $q->url;
	$url =~ s/^http/https/;
	Err("You must be logged into the DB to use this interface. If you are logged in, you must access it in an SSL session: " . $q->a({'-href'=>$url}, $url));
	exit;
}
#else
#{
	( my $id, $realOP ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
	#GI_Authen::authen_ses_key( undef, undef, $q->cookie('GI_Authen_General') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ((! $id) || $id =~ /\D/ )
	{
#		Err("You need to login again at <a href=\"$LoginScript?destination=" . $q->script_name . "/$Path\">$LoginScript</a>");
		Err('The co-op login is invalid');
		exit;
	}

	###### since admin use is above, we will prohibit 01 use without it
	###### this will keep ex-employees who know the login credentials from
	###### being able to access this function
	if ($id == 1)
	{
		Err('This interface has not been converted for admin use since moving to GI');
#		Err('To use this interface as 01, you must be logged into the database.');
		exit;
	}

#	exit unless $db = DB_Connect('mv-members', 1, {'enable_lockout'=>1});
#	$db->{'RaiseError'} = 1;

	unless ($realOP->{'membertype'} eq 'v')
	{
		Err('This resource is for the exclusive use of Partners.');
		Exit();
	}

	###### exclude NOPs from the application
	my $sth = $db->prepare("SELECT id FROM nop WHERE id= $id");
	$sth->execute;
	$id = $sth->fetchrow_array;
	$sth->finish;
	if ($id)
	{
		Err('', $TMPL{'no-pay'});
		Exit();
	}
#}

# make sure this is a coop that admin is managing
my $coop = $db->selectrow_hashref("SELECT * FROM coop.coops WHERE coop_id=?", undef, $realOP->{'id'});
unless ($coop)
{
	Err("Could identify a coop with id: $realOP->{'id'}<p>You must be logged in as the Co-Op container as well as being logged into the DB</p>");
	Exit();
}

my $ADDL_TITLE = "Co-Op: $coop->{'coop_id'}";

Load_lang_blocks();

unless ($activity)
{
	push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'recipient_prompt'});
	Do_Page( $TMPL{'screen1'} );
	Exit();
}

###### the same goes here
###### unless we need different content based on transfer type
###### we will just present our form with the proper block showing for this step						
unless ($param{'pd'})
{
	push @alerts, $q->div({'-class'=>'note_nop'}, $lang_blocks->{'select_recipient'});
	Do_Page($TMPL{'screen1'});
	Exit();
}
elsif (! $param{'pd_hash'})
{
	###### verify the recipient
	unless (Validate_Recipient())
	{
		Do_Page( $TMPL{'screen1'} );
		Exit();
	}
}

###### we should have a pd_hash for the activities beyond this point
unless (Validate_Pid($param{'pd_hash'}))
{
	Err('PD hash failed to validate');
	Exit();
}
else
{
	$recipient = Generate_Pid_Hash($param{'pd_hash'});
	if ( $param{'pd'} !~ /$recipient->{'id'}|$recipient->{'alias'}/ )
	{
		Err('PD hash doesn`t match PD');
		Exit();
	}
}

if ($activity eq 'start')
{
	Do_Page(
		$TMPL{'screen2'},
		{'title'=>'title2', 'data'=>{'ctlOptions' => TransferTypeOptions()}}
	);
	Exit();
}
elsif ($activity eq 'getlist')
{
	Do_Page(
		$TMPL{'screen3'},
		{'title'=>'title3', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'list' => Create_List($param{'transfer_type'})}});
	Exit();
}
elsif ($activity eq 'transfer')
{
	my $rv = Transfer_Group() if Pre_Transfer_Checks();
	
	if ($rv)
	{
		Update_Transfer_SEQ();
	}

	$proc_status .= 'Job finished.';
	Do_Page(
		$TMPL{'screen4'},
		{'title'=>'title4', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'proc_status'=>$proc_status}});
}
else
{
	Err('Invalid activity received.');
}

Exit();

###### start of subroutines

sub CanSponsor
{
	my $mtype = shift;
	my %a = ();

	# determine if and who I can sponsor
	# the idea is that if I can login then I can see what membertypes I can sponsor
	# then those will be filtered on who can login of those
# the rules have changed... 01/12
# instead their is a precise ability defined that I prefer to not record in the DB

#	my $sth = $db->prepare(q#
#		SELECT ms.can_sponsor, mt2.label
#		FROM membertype_sponsors ms
#		JOIN member_types mt
#			ON mt."code"=ms.membertype
#		JOIN member_types mt2
#			ON mt2."code"=ms.can_sponsor
#		WHERE mt.can_login=TRUE
#		AND mt2.can_login=TRUE
#		AND ms.membertype = ?#);
#	$sth->execute($mtype);
	my $labels = $db->selectall_hashref('
		SELECT mt."code", mt.label
		FROM member_types mt', 'code');
	my %perms = (
		'v'=>[ qw/v ma ag m s/ ]
	);

	foreach my $tmp (@{$perms{$mtype}})
	{
		$a{$tmp} = {'can_sponsor'=>$tmp, 'label'=>$labels->{$tmp}->{'label'}};
	}
	return \%a;
}

sub Check_Transfer_SEQ
{
	unless ($param{'seq'})
	{
		Err('Missing seq parameter.');
		Exit();
	}

	my ($seq, $complete) = $db->selectrow_array("
		SELECT seq, complete
		FROM vip_transfers_log
		WHERE 	transferer= $realOP->{'id'}
		AND 	seq= $param{'seq'}
	");

	unless ($seq)
	{
		Err('Seq not found or does not match.');
		Exit();
	}
	elsif ($complete)
	{
		Err($lang_blocks->{'already_done'});
		Exit();
	}

	return 1;
}

sub Create_List
{
	my $mtype = shift;
	
	# Dick wants both m's and s's to be included in the "Members" lists
	# and since we cannot simply insert other bare user submitted data directly into the SQL, we will create a mask
	# the value of a given hash key will be directly inserted into the SQL to provide a membertype IN (xxx)
	my %mtype_map = (
		'm'=>"'m'",
		'v'=>"'v'",
		'ag'=>"'ag'",
		'ma'=>"'ma'"
	);
#	my $count = 0;

###### develop a query of potential transferees

	my $limit = $param{'limit'} || $LIMIT;
	# ignore values that are not strictly numeric
	$limit = $LIMIT if $limit =~ m/\D/;

	my $orderBy = $param{'sortOrder'} || 'DESC';
	# ignore values that are outside of what we expect
	$orderBy = 'DESC' unless $orderBy =~ m/^asc$|^desc$/i;

###### this SQL is used in all cases
###### we'll add things onto the end as necessary for the individual list type
	my $qry = q#
		SELECT DISTINCT
			m.id,
			m.spid,
			m.alias,
			m.mail_option_lvl AS ms,
			d.depth,
			m.language_pref,
			m.country,
			CASE
				WHEN m.membertype= 'ag' THEN m.company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			CASE
				WHEN m.membertype <> 'ma' THEN '&nbsp;'
				ELSE 'Merchant Affiliate'
			END AS flags,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.mail_option_lvl,
			membertype,
			mt.display_code
	
		FROM depths d
		JOIN members m
			ON m.id=d.id
		JOIN member_types mt
			ON mt.code=m.membertype
							
		WHERE 	m.spid= ? --rid->{id}

	-- exclude 'Front Line Point' which may or may not be our recipient
		AND 	m.id != ?

	-- exclude BEMs... but only when they are NOT VIPs or Pools
		AND 	(m.membertype IN ('v') OR m.mail_option_lvl > 0)
	#;

	$qry .= "AND m.membertype IN ($mtype_map{$mtype})";

	$qry .= "\nORDER BY m.id $orderBy LIMIT $limit";
#die "id: $realOP->{'id'}\nflp: $param{'flp'}\nmtype: $mtype\n$qry";
	my $sth = $db->prepare($qry);
	$sth->execute($realOP->{'id'}, $param{'flp'});

	my $bg = '#ffffee';
	my $datclass = ();
	my $list = '';

	while (my $row = $sth->fetchrow_hashref)
	{
		$list .= qq|<tr bgcolor="$bg">\n|;
		$list .= $q->td({'-class'=>'data'}, 
				"$row->{'alias'},
				$row->{'firstname'} $row->{'lastname'} ,
				$row->{'country'}, $row->{'language_pref'}") . "\n" .

			$q->td({'-class'=>'data', '-align'=>'center'}, $row->{'display_code'}) . "\n";

#		$list .= $q->td({'-class'=>'data'}, $row->{'npp'} || '&nbsp;') . "\n";

		$list .= $q->td({'-class'=>'data', '-align'=>'center'}, $row->{'ms'}) . "\n";

		$list .= $q->td({'-class'=>'confirm'}, $row->{'flags'}) . "\n";

#		if ($row->{'npp'} && $row->{'npp'} >= 250 && $realOP->{'id'} != 1)
#	###### another hack to allow 01 free access---------^^^^^^^^^^^^^^^^
#		{
#	###### we are not going to allow them to transfer organizations larger than this
#			$list .= $q->td({-class=>'note_nop'}, $lang_blocks->{'over_pp_limit'}); 
#		}
#		else
#		{
			$list .= $q->td( ctl_mvid($row) ) . "\n";
#		}

		$list .= "</tr>\n";
		$bg = ($bg eq '#ffffff') ? '#ffffee' : '#ffffff';
	}
	$sth->finish;

	if ($list)
	{
		my $hdr = $q->Tr({'-class'=>'h5', '-bgcolor'=>'#eeffee'}, $q->th(
			['Name',
			'Type',
#			$q->a({'-href'=>'javascript:void(0)',
#				'-class'=>'hdr',
#				'-title'=>$lang_blocks->{'click4expl'},
#				'-onclick'=>"whatIsThis('pp'); return false;"}, 'PP'),
			$q->a({'-href'=>'javascript:void(0)',
				'-class'=>'hdr',
				'-title'=>$lang_blocks->{'click4expl'},
				'-onclick'=>"whatIsThis('ms'); return false;"}, 'MS'),
			'&nbsp;', '&nbsp;']
		)) . "\n";
		$list = $q->table(
			{
			'-border'=>1,
			'-cellspacing'=>0,
			'-cellpadding'=>4
			},
			$hdr . $list) ."\n";

#		$list = "<table><tr><td align=\"right\" style=\"font-size:3px\">
#			${\Create_List_Form(\$list)}
#			</td></tr></table>\n";
		$list = Create_List_Form(\$list);
 	}
	else
	{
		$list = $q->p({'-class'=>'blu_alert'}, $lang_blocks->{'no_transferees'});
	}
#warn $list;
	return $list;
}

sub Create_List_Form
{
	my $list = shift;
	my $form = $q->start_form(
		'-action'=>$ME,
		'-name'=>'list',
		'-method'=>'post',
		'-onsubmit'=>'return ListFrmSubmit(this)'
	) . "\n";
	$form .= $q->hidden('-name'=>'activity', '-value'=>'transfer', '-force'=>1) . "\n";
	$form .= $q->hidden('transfer_type') . "\n";
	
	###### in order to keep them from submitting multiple times
	###### we'll give them a variable number 'key' to go with this transfer
	###### if they later try to submit more than once we can stop 'em
	my ($seq) = $db->selectrow_array("SELECT NEXTVAL('generic_pk_seq')");
	unless ( $db->do("
		INSERT INTO vip_transfers_log (seq, recipient_id, transferer)
		VALUES ($seq, $recipient->{'id'}, $realOP->{'id'})") )
	{
		return $q->p({'-class'=>'confirm'}, 'Failed to setup transfer sequence.');
	}
	$form .= $q->hidden('-name'=>'seq', '-value'=>$seq);

	###### if this is an AP or CSR list
	###### we'll provide buttons at the top & bottom
	###### since it could be a lengthy list
	$form .= ctl_buttons() if ($param{'transfer_type'} eq 'm' || $param{'transfer_type'} eq 's');
	
	foreach('pd','pd_hash','rid','rid_hash')
	{
		$form .= "\n" . $q->hidden($_);
	}

	###### lets put in our buttons
	###### we'll include
	$form .= "<br /><br />\n$$list\n<br /> ${\ctl_buttons('x')}\n</form>\n";
	return $form;
}

sub ctl_buttons
{
	my $arg = shift;

	###### if I am going to have a duplicate set of buttons, I'll need
	###### to create distinct names
	$arg = $arg ? ($arg . '_') : '';
	my $buttons = ();

	###### we will have an additional 'select all' button
	###### only for simple stack transfers
	if ($Path eq 'simple' || $Path eq 'autostack')
	{
		$buttons = $q->button(
			'-value'=>$lang_blocks->{'select_all'},
			'-onClick'=>"for(x=0; x<document.forms.list.mvid.length; x++) document.forms.list.mvid[x].checked=true;",
			'-class'=>'btn_select_all',
			'-onmouseout'=>'resetBtn(this)',
			'-onmouseover'=>"setBtn(this, 'select_all')",
			'-name'=>$arg . 'select_all'
		) . "\n";
	}

	$buttons .= $q->button(
		'-value'=>$lang_blocks->{'cancel'},
		'-onClick'=>"location.href='$ME}'",
		'-class'=>'btn_cancel',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'cancel')",
		'-name'=>$arg . 'cancel'
	) . "\n";

	$buttons .= $q->reset(
		'-class'=>'btn_reset',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'reset')",
		'-value'=>$lang_blocks->{'reset'}
	) . "\n";

	$buttons .= $q->submit(
		'-value'=>$lang_blocks->{'perform_xfer'},
		'-class'=>'btn_transfer',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'transfer')"
	)  . "\n";

	return $buttons;
}

sub ctl_mvid
{
	###### since we want to provide radio buttons instead of checkboxes sometimes
	###### we'll make the determination here
	my $row = shift;

	# Dick wants them to only be able to transfer 1 CSR at a time (really only one per day)
	if ((grep $_ eq $param{'transfer_type'}, (qw/m s/)) && $recipient->{'membertype'} ne 'mp')
	{
		return $q->checkbox(
			'-class'=>'input',
			'-name'=>'mvid',
			'-value'=>Generate_Pid($row),
			'-label'=>''
			);
	}
	else
	{
		return $q->radio_group(
			'-class'=>'input',
			'-name'=>'mvid',
			'-value'=>Generate_Pid($row),
			'-nolabels'=>1,
			'-default'=>0
			);
	}
}

sub Do_Page
{
	my ($TMPL, $args) = @_;

	###### if I receive a digit only, I know I have to retrieve the template
	if ($TMPL !~ /\D/)
	{
		$TMPL = $gs->Get_Object($db, $TMPL) || die "Failed to retrieve template: $TMPL";
	}
	
	my $rv = '';
	# take care of our language nodes
	Parse_Lang_Blocks({'realOP' => $realOP, 'user' => $realOP, 'recipient' => $recipient});
#warn Dumper($args);
	my %data_hash = (
			'lang_blocks' => $lang_blocks,
			'realOP' => $realOP,
			'user' => $realOP,
			'alerts' => \@alerts,
			'script_name' => $ME,
			'param' => \%param
	, %{ $args->{'data'} || {} } );

#warn Dumper(\%data_hash);
	$TT->process(\$TMPL, \%data_hash, \$rv);
		
	my $GI = GI->new();
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>($lang_blocks->{ $args->{'title'} || '' } || $args->{'title'} || $lang_blocks->{'title1'}) . " $ADDL_TITLE",
			'style'=>$GI->StyleLink('/css/mv-members.css'),
			'content'=>\$rv,
			'script'=>'<script type="text/javascript" src="/js/mv-members.js"></script>'
		});
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	my $msg = shift || '';
	my $tmpl = shift || $TMPL{'Error'};
	if ($db)
	{
		$tmpl = $gs->Get_Object($db, $tmpl) ||
			die "Failed to load template: $tmpl\n\nPlus you have this 'error':\n$msg\n";
	}

###### if we don't have a DB connection, then we'll setup a vanilla template
	else
	{
		$tmpl = $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Transfer error');
		$tmpl .= '%%error_msg%%<br /><br />';
		$tmpl .= '%%timestamp%%';
		$tmpl .= $q->end_html();
	}

	$tmpl =~ s/%%error_msg%%/$msg/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print 	$q->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
}

sub Generate_Pid
{
###### my pid will consist of 7 characters of a hash + the transfer ID
###### & a pipe delimited list of personal data
	my $row = shift;
	return unless $row;

	my $pid = "$row->{'id'}|$row->{'spid'}|$row->{'alias'}|$row->{'firstname'}|";
	$pid .= "$row->{'lastname'}|$row->{'emailaddress'}|$row->{'membertype'}|";
	$pid .= "$row->{'depth'}|$row->{'ms'}";
	$pid = md5_hex($pid . $key) . "|$pid";
}

sub Generate_Pid_Hash
{
###### generate a hash based on the data in the passed pid and pass it back as a ref
###### refer to Generate_Pid for the values contained in the pid
	my $pid = shift;
	return unless $pid;
	my @vals = split(/\|/, $pid);
	shift @vals;
	my %r = (
		'id'		=> $vals[0],
		'spid'		=> $vals[1],
		'alias'		=> $vals[2],
		'firstname'	=> $vals[3],
		'lastname'	=> $vals[4],
		'emailaddress'	=> $vals[5],
		'membertype'	=> $vals[6],
		'depth'		=> $vals[7],
		'ms'		=> $vals[8]
	);
	return \%r;
}

sub Get_OID
{
	return $db->selectrow_array("SELECT oid FROM members WHERE id= $_[0]");
}

sub Get_Member_Record
{
	my $id = shift;
	return unless $id;

	my $rv = $db->selectrow_hashref(qq#
		SELECT m.id,
			m.spid,
			m.alias,
			CASE
				WHEN m.membertype= 'ag' THEN company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.membertype,
			mt.display_code,
			d.depth,
			m.mail_option_lvl AS ms,
			CASE
				WHEN m.language_pref ISNULL OR m.language_pref='' THEN 'en'
				ELSE m.language_pref
			END AS language_pref,
			m.oid
		FROM members m
		JOIN depths d
			ON m.id=d.id
		JOIN member_types mt
			ON mt."code"=m.membertype
		WHERE 	${\($id =~ /\D/ ? 'm.alias' : 'm.id')} = ?
	#, undef, $id);
	return $rv;
}

sub Load_lang_blocks
{
	my $xml = $gs->Get_Object( $db, $TMPL{'XML'} ) || die "Failed to load lang_blocks\n";
	$lang_blocks = $lu->flatXMLtoHashref($xml);
}

sub Load_Params
{
	my ($tmp) = ();

	foreach ('pd_hash', 'rid_hash', 'transfer_type')
	{
		$param{$_} = $q->param($_) || '';
	}

	$param{'pd'} = $gs->PreProcess_Input(uc $q->param('pd')) || '';

	###### load specific parameters that we want to do bounds checking on
	foreach ('flp','limit','seq')
	{
		if ($tmp = $q->param($_) || '')
		{
			die 'Invalid $_ parameter received' if $tmp =~ /\D/;
			$param{$_} = $tmp;
		}
	}

	if ($tmp = $q->param('sortOrder'))
	{
		unless ($tmp =~ /^asc$|^desc$/)
		{
			die 'Invalid orderBy parameter received';
		}
		$param{'sortOrder'} = $tmp;
	}

	@mvid = $q->param('mvid');
}

sub Parse_Lang_Blocks
{
	# the reason for doing this way is that we don't have to worry about breaking the XML
	# *prior* to parsing it out into lang_blocks
	# we can do our conversion to a hash and then take care of each node individually
	my $data = shift;

	foreach my $k (keys %{$lang_blocks})
	{
		$lang_blocks->{$k} = Parse_Lang_Blocks_Node($k, $data);
	}
}

=head3 Parse_Lang_Blocks_Node

While we will want to perform substitutions on the general set of nodes prior to finally rendering a page,
there are cases, such as during error generation where we will stuff a node into the @alerts array
and we need a particular node to have substitution performed on it before the whole is finally done.

This routine performs that one-off substitution and is used by Parse_Lang_Blocks to boot

=cut

sub Parse_Lang_Blocks_Node
{
	my ($node_name, $data) = @_;
	my $rv;
	$TT->process(\$lang_blocks->{$node_name}, $data, \$rv);
	return $rv;
}

sub Pre_Transfer_Checks
{
	if (@mvid)
	{
		return unless Check_Transfer_SEQ();

		return 1;
	}
	else
	{
		###### evidently there were no transferees selected
	#	$alert .= $q->p({-class=>'confirm'}, 'You failed to make your selections.');
	#	$formlist = Create_List();
	#	Do_Page($TMPL{main}, 'list');
	###### we don't have the parameters to to create a list so we'll error
	###### out and let them press the back button
		Err("You failed to make your selections.<br />
			Please back up and check one or more parties to transfer.");
		return;
	}
}

sub Transfer_Group
{
###### we'll transfer a group of members to another party
###### we'll build our transfer LIST as we go
###### we have our IDs as the keys of the hash and our matching data as the value
	my ($qry, $row) = ();
	my $memo = "Co-Op Transfer by $operator";

	foreach(@mvid)
	{
		unless (Validate_Pid($_))
		{
#			print "Skipping - Error validating pid: $_ <br />\n";
			$proc_status .= "Skipping - Error validating pid: $_ <br />\n";
			next;
		}
		$row = Generate_Pid_Hash($_);
		
#		print "Transferring $row->{'alias'} <br />\n";
		$proc_status .= "Transferring $row->{'alias'} <br />\n";
		$qry = "
			UPDATE members SET
				spid= $recipient->{'id'},
				stamp= NOW(),
				memo= '$memo',
				operator= 'mv-members.cgi'
			WHERE id = $row->{'id'};

			UPDATE depths SET
				depth= ($recipient->{'depth'} +1)
			WHERE id= $row->{'id'};";

	###### our activity report entry (if we are not transferring CSR members to a pool)
	###### credit goes to the operator
#		$qry .= "
#			INSERT INTO activity_rpt_data (
#				id,
#				spid,
#				activity_type,
#				val_text
#			) VALUES (
#				$row->{'id'},
#				$recipient->{'id'},
#				22,
#		-- put in our realOP rather than the one being managed if applicable
#				${\$db->quote($realOP->{'alias'})}
#			);";	# if $param{'transfer_type'} ne 's' && ! isPoolTransfer();

		if ($recipient->{'membertype'} eq 'v')
		{
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}, $row->{'emailaddress'}\n";
		}
		else
		{
	###### if it is not a VIP receiving the transfer, we use a different format
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}\n";
		}

#print "qry = $qry\n"; Exit();

		$db->do($qry);
		$firstID ||= $row->{'id'};
	}

	# if it was a pool receiving the transfer
#	print " - Done.<br />\n";
	$proc_status .= " - Done.<br />\n";

	if ($transfer_list)
	{
		return 1;
	}

	return undef;
}

sub Update_Transfer_SEQ
{
	$db->do("
		UPDATE vip_transfers_log
		SET complete= TRUE
		WHERE transferer= $realOP->{'id'} AND seq= $param{'seq'}
	");
}

sub TransferTypeOptions
{
	my @options = ();

	# generate our membertype options that this recipient can receive
	$recipient->{'can_sponsor'} ||= CanSponsor($recipient->{'membertype'});
	
	# we must assume that "somebody" is going to specify the order of the options
	# and since I am not going to add more data to the DB for doing this
	# I will do a sort in here even though it means if another membertype is added that should be transferrable
	# this code will have to be embellished
	foreach my $mt (qw/v ag ma m/)
	{
		push @options, $recipient->{'can_sponsor'}->{$mt} if $recipient->{'can_sponsor'}->{$mt};
	}
	return \@options;	
}

sub Validate_Pid
{
###### my pid will consist of a hash & a pipe delimited list of 
###### personal data, the first field of which is the numeric ID

	my $var = shift;
	return unless $var;
	my @val = split(/\|/, $var, 2);

	$val[1] =~ /(^\d*)\|/;

	return ( md5_hex( $val[1] . $key) eq $val[0] ) ? 1 : 0;
}

sub Validate_Recipient
{
	$recipient = Get_Member_Record($param{'pd'});

	unless ( $recipient )
	{
		# the specified recipient was either empty or has no match in the DB
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'invalid_recipient'});
		return;
	}
	elsif ( $recipient->{'id'} == $realOP->{'id'})
	{
		# you cannot be the recipient... duh
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'ucannot'});
		return;
	}
	
	# CanSponsor will return an empty hashref if nothing else
	$recipient->{'can_sponsor'} = CanSponsor($recipient->{'membertype'});
	unless ( keys %{$recipient->{'can_sponsor'}} )
	{
		push @alerts, $q->div({'-class'=>'y_alert'},
			Parse_Lang_Blocks_Node('cannot_receive_transfers', {'recipient'=>$recipient}) );
		return;
	}
	
	my ($rv) = ();
	my $tmp = $recipient;
	my $startID = $recipient->{'spid'};
	my $sth = $db->prepare("
		SELECT id,
			spid,
			alias,
			CASE
				WHEN membertype= 'ag' THEN company_name
				ELSE firstname
			END AS firstname,
			CASE
				WHEN membertype= 'ag' THEN ''
				ELSE lastname
			END AS lastname,
			emailaddress,
			membertype,
			0 AS depth,
			mail_option_lvl AS ms
		FROM members
		WHERE id= ?");

	###### flp will be our Front Line Point of connection to the recipient
	###### we'll need this value later so we can exclude them from any lists
	my $flp = $tmp->{'id'};

	# a hack to allow admin to transfer outside of the organization of the Partner (for use specifically for coops)
	unless ($admin)
	{
		while ($tmp->{'spid'} != $realOP->{'id'} && $tmp->{'spid'} > 1)
		{
			$rv = $sth->execute($tmp->{'spid'});
			die "Failed to retrieve sponsor record for: $tmp->{'spid'}" unless $rv;
	
			$tmp = $sth->fetchrow_hashref;
			die "Loop detected!" if $tmp->{'spid'} == $startID;
	
			$flp = $tmp->{'id'};
		}
	
		if ($tmp->{'spid'} == 1 && $realOP->{'id'} != 1)
		{
			push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'not_in_organization'});
			return;
		}
	}

	$param{'flp'} = $flp;

###### we'll assign the numeric ID to pd just in case we don't have it
###### and also set the pd param
	$param{'pd'} = $recipient->{'id'};
	$param{'pd_hash'} = Generate_Pid($recipient);
	return $recipient;
}

###### 11/24/04 changed the list form's method to an explicit 'post' since some VIPs
###### were having trouble submitting more than 13 members for transfer
###### also fixed up the utf8 mailing to properly encode the data and doctor the headers
###### 12/20/04 fixed the broken checks following the VIP under member check towards the top
###### also added a restriction on the amount of PP a party can have before being
###### disqualified from being transferred
###### 01/12/05 fixed the cases where $q->param was embedded in error messages bare
###### 01/14/05 added a coalesce to the language_pref col in getting the member rec.
###### 01/17/05 revised the %%script_name%% placeholder to get q->url instead so that
###### the path information would not be embedded (this was causing problems in jump to)
###### 01/18/05 made the emailing alert conditional on the recipient's membertype
###### 01/27/05 added a hack to allow 01 to perform transfers over 250 PP
###### added another hack to allow 01 to override an explicit denial of mgt. rights
###### 02/11/05 put the actual network calcs table into the mix rather than
###### the view which was killing us
###### 05/10/05 just a little javascript tweak and a little preprocessing of the pd param
###### 05/12/05 changed the error handler in Pre_Transfer_Checks to simply abort
###### 09/08/05 revised to allow managing members with the exclusion of parties
###### originally sponsored by the member
###### 01/25/06 Added -charset=utf-8 to the $q->header statement.
###### to pull in the necessary level and role information for real and rid
# 07/06/06 revised the SQL for list generation to show AG company names
###### 02/08/08 further tweakage of the BEM exclusion implemented in Jan. '08
# 09/16/08 added an insert for notification type 81 on a VIP transfer
###### 07/15/09 disabled mailings to recipients with bad mail setting
###### just quickie revisions to move this to the GI domain
###### 11/18/10 integration of the transfer of AMPs with VIPs
# 02/24/11 disabled the email to VIP recipients of a transfer
###### 01/10/12 lot's of changes involving many things, like who can sponsor whom and a lot of code cleanup
###### 04/09/12 ripped out things related to 'amp's
