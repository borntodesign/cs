#!/usr/bin/perl -w
###### GrantFreePartnerStatus.cgi
###### 

use strict;

use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
require ADMIN_DB;
require cs_admin;
use FreePartnerStatus;

my ($errors) = ();
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my $ME = $q->script_name;

die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $rid = $q->param('rid') || '';
$rid =~ s/^\s*|\s*$//g;

my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $action = $q->param('action');
unless ($action)
{
	SearchForm();
}

my $m = $db->selectrow_hashref('SELECT id, alias, firstname, lastname FROM members WHERE alias=? OR id= ?', undef, uc($rid), $rid);
unless ($m)	{
	$errors = "Could not find a membership matching id: $rid";
	SearchForm();
}
my $fps = FreePartnerStatus->new({'db'=>$db, 'id'=>$m->{'id'}});

if ($action eq 'search')
{
	DoForm();
}
elsif ($action eq 'update')
{
	Update();
}
elsif ($action eq 'create')
{
	Create();
}
else {
	die "Unknown 'action' parameter: $action";
}

Exit();

######

sub Create
{
	my $exp = LoadExpirationParam();
	my $rv = $fps->createFreePartner({'expiration'=>$exp, 'notes'=>$q->param('notes')});
	$errors = 'There was an error: ' . $fps->{'errorStr'} unless $rv;
	DoForm();	
}

sub DoForm
{
	unless ($rid) {
		$errors = "Cannot proceed without ID parameter";
		SearchForm();
	}
	
	my $history = $fps->myHistory();
#	my $currentRec = $fps->myRecord();	# this does not mean their Free status is active, it just means this is the most recent record
	
	print
		$q->header,
		$q->start_html(
			'-title' => 'Manage Free Partner Status',
			'-style' => {'-code' => 'th {text-align:left; padding-right:1em; padding-left: 0.3em;} td {text-align:left; border: 1px solid grey; padding: 0.2em 0.4em;}'}
		);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->start_form('-method'=>'get'),
		$q->h4("Manage Free Partner Status for: $m->{'alias'} - $m->{'firstname'} $m->{'lastname'}"),
		'<table style="border-collapse: collapse; margin-top:1em;">' ,
		$q->Tr(
			$q->th('Free Status Ends') . $q->th('Last Action Time') . $q->th('By') . $q->th('Notes') .$q->th('')
		);
	
	foreach my $row (@{$history})
	{
		my $action = ProcessRow($row);	# as necessary, this function will change the appropriate columns to HTML fields and the action will become some HTML form element/s as well
		DoRow($row, $action);
	}

	# if they are not currently free, then we will present a row for them to be setup as free
	unless ($fps->isFree()) {
		my $row = {};
		my $action = ProcessRow($row);
		DoRow($row, $action);
	}
		
	print
		'</table>',
		$q->hidden('-value'=>$m->{'id'}, '-name'=>'rid'),
		$q->end_form,
		$q->p({'-style'=>'margin-top:3em'}, $q->a({'-href'=>$q->url}, 'Start Over'));
		$q->end_html;
	Exit();
}

sub DoRow
{
	my ($row, $action) = @_;
	print $q->Tr(
		$q->td($row->{'expiration'}) . $q->td($row->{'_stamp'}) . $q->td($row->{'operator'}) . $q->td($row->{'notes'}) . $q->td($action)
	);
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub LoadExpirationParam
{
	my $exp = $q->param('expiration');
	die ("Expiration date is empty or invalid: " . $exp || '') unless $exp && $exp =~ m/\d{4}-\d{2}-\d{2}/;
	return $exp;
}

sub ProcessRow
{
	my $row = shift;
	
	# initialize empty entries... particularly important for a null row
	# we must specify the columns because a null row will not have them
	foreach (qw/pk expiration _stamp operator notes/)
	{
		$row->{$_} = '' unless defined $row->{$_};
	}
	
	# the following are the columns we will create form fields for
	
	my $sth = $db->prepare('SELECT monthends_for_x_months(3)');
	$sth->execute;

	my @vals = $row->{'expiration'};
	while (my ($v) = $sth->fetchrow_array)
	{
		push(@vals, $v) unless $v eq $row->{'expiration'};	# we already have it on the stack from before the loop started
	}
	
	$row->{'expiration'} = $q->popup_menu(
		'-name' => 'expiration',
		'-values' => \@vals,
		'-default' => $row->{'expiration'},
		'-force' => 1
	) if (! $row->{'pk'}) || $row->{'_active'};	# if the row is null or the row is active

	$row->{'notes'} = $q->textfield(
		'-style' => 'width:20em',
		'-name' => 'notes',
		'-value' => $row->{'notes'},
		'-force' => 1
	) if (! $row->{'pk'}) || $row->{'_active'};
	
	my $rv = '';
	# there will be a pk value for every row... if there is not one, it is because we received a null row and need to create a new entry
	if ($row->{'pk'} && $row->{'_active'}) {
		$rv = $q->submit('Update') . $q->hidden('-name'=>'action', '-value'=>'update', '-force' => 1);
	} elsif (! $row->{'pk'}) {
		$rv = $q->submit('Make Free') . $q->hidden('-name'=>'action', '-value'=>'create', '-force' => 1);	
	}
	return $rv;
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html('Free Partner Status Member Search');

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->h4('Free Partner Status Member Search'),
		
		$q->start_form('-method'=>'get'),
		$q->textfield('-value'=>'', '-name'=>'rid'),
		' '.
		$q->submit('Search'),
		$q->hidden('-value'=>'search', '-name'=>'action'),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub Update
{
	die "Cannot proceed without rid parameter" unless $rid;
	my $exp = LoadExpirationParam();
	
	my $rv = $fps->_update({'expiration'=>$exp, 'notes'=>$q->param('notes')});
	$errors = 'There was a problem updating the record' unless $rv;
	DoForm();
}