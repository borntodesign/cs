#!/usr/bin/perl -w
###### merch-trans_stats.cgi
###### admin script to display Merchant Affiliate transaction aggregate statictics
###### last modified: 11/05/03	Bill MacArthur
######                02/09/2011 g.baker   postgres 9.0 port SUBSTR arg 1
######                                     must be text

use strict;
use DBI;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Carp qw(fatalsToBrowser);
use G_S qw(%MEMBER_TYPES);

###### GLOBALS
our $q = new CGI; 
my ($db, %mtd, %ttls, $month) = ();
my $factor = 1;

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

###### we could have tested for the admin flag, but just in case we have someone trying to access things
###### without going through the admin interface, we may be able to stop 'em here
if ($operator && $pwd)
{
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'merch-trans_stats.cgi', $operator, $pwd )){exit}

###### we are not giving everyone in the organization access to this function
###### only those in the accounting group
# 	if (&ADMIN_DB::CheckGroup($db, $operator, 'accounting') != 1){
# 		Err($q->h3('You must be a member of the accounting group to use this form'));
# 		goto 'END';
# 	}
}
else
{
	Err('Sorry, it does not appear that you are logged in.');
	exit;
}

Gather_MTD_Stats();

Render_Page();

END:
$db->disconnect;
exit;

sub Build_MTD_Table
{
	my $row1 = qq!<td class="ttls_h">Day</td>\n!;
	my $row2 = '<td>&nbsp;</td>';
	my $row3 = qq!<td class="ttls_h">\$</td>\n!;
	my $row4 = qq!<td class="ttls_h">#</td>\n!;

	foreach (sort {$a<=>$b} keys %mtd)
	{
		$row2 .= qq!<td align="center" valign="bottom">!;
		$row2 .= qq!<img src="/images/blueline.gif" width="10" alt="" height="!;
		$row2 .= int ($mtd{$_}{trans_ttls} / $factor) . "\" /></td>\n";

		$row1 .= qq!<td align="center" class="day">$mtd{$_}{day}</td>\n!;
		$row3 .= qq!<td align="center" class="ttl">${\int $mtd{$_}{trans_ttls}}</td>\n!;
		$row4 .= qq!<td align="center" class="day">$mtd{$_}{cnt}</td>\n!;
	}

	return qq|
<tr>$row1</tr>
<tr>$row2</tr>
<tr bgcolor="#eeffee">$row3</tr>
<tr bgcolor="#eeeeee">$row4</tr>
|;

}

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Gather_MTD_Stats
{
	my $high = 0;
	
	###### for now we are going to just do MTD
	###### since part of the data involves discounts which can change, going back
	######	to previous months would be problematical for discount calculations
	my $sth = $db->prepare("	SELECT SUM(t.trans_amt)::NUMERIC(8,2) AS trans_ttls,
						SUBSTR(t.trans_date::TEXT, 9,2) AS day,
						SUM(mad.discount) AS discount,
						COUNT(record_id) AS cnt
					FROM 	merchant_affiliates_transactions t,
						merchant_affiliate_discounts mad
					WHERE 	t.mal_id=mad.location_id
					AND 	mad.start_date <= t.trans_date
					AND 	(
						mad.end_date >= t.trans_date
						OR
						mad.end_date ISNULL
						)
					AND 	t.trans_date >= first_of_month()
					GROUP BY t.trans_date");

	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		###### populate our hash
		$mtd{$tmp->{day}} = $tmp;

		###### totalize data
		$ttls{transactions} += $tmp->{trans_ttls};
		$ttls{discount} += $tmp->{discount};
		$ttls{cnt} += $tmp->{cnt};

		###### keep my running high figure
		$high = $tmp->{trans_ttls} unless $high > $tmp->{trans_ttls};

		###### calculate our factor so we can keep the highest bar at 100
		$factor = $high / 100;

	}
	$sth->finish;

	if ($ttls{cnt} > 0)
	{
		$ttls{avg_disc} = $ttls{discount} / $ttls{cnt};
		$ttls{receivable} = $ttls{transactions} * $ttls{avg_disc};
		$ttls{avg_transaction} = $ttls{transactions} / $ttls{cnt};
	}
	else
	{
		$ttls{avg_disc} = 0;
		$ttls{receivable} = 0;
		$ttls{avg_transaction} = 0;
	}
}

sub Get_Month
{
	my $sth = $db->prepare("SELECT NOW()::DATE");
	$sth->execute;
	$month = $sth->fetchrow_array;
	$sth->finish;
	return $month;
}

sub Render_Page
{
	my $tmpl = G_S::Get_Object($db, 10194) || die "Failed to load template: 10194.";

	$tmpl =~ s/%%tbl%%/Build_MTD_Table()/e;
	$tmpl =~ s/%%month%%/Get_Month()/e;
	$tmpl =~ s/%%trans_ttls%%/sprintf("%.2f", $ttls{transactions})/e;
	$tmpl =~ s/%%trans_number%%/$ttls{cnt}/;
	$tmpl =~ s/%%avg_disc%%/sprintf("%.2g", ($ttls{avg_disc} * 100)) . '%'/e;
	$tmpl =~ s/%%receivable%%/sprintf("%.2f", $ttls{receivable})/e;
	$tmpl =~ s/%%avg_transaction%%/sprintf("%.2f", $ttls{avg_transaction})/e;

	print $q->header, $tmpl;
}

###### changed the printout for the number of transactions and changed the goto to exit on the login test failure
###### 10/09/03 added the average transaction value
###### 11/05/03 revised to look at the new discounts tbl instead of the vendors table
