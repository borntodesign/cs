#!/usr/bin/perl -w
###### rewards_admin.cgi
######
###### An interface for admin to view and add Reward Points transactions.
######
###### created: 01/24/05	Karl Kohrt
######
###### last modified: 09/25/07 Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use ParseMe;

# GLOBALS
our $q = new CGI;
our $ME = $q->url;
our $db = '';
our $page_body = '';
our $id = '';		# The member id of the transaction.			
our $action = '';
our $message = '';
our $type = '';				
our $amount = 0;				
our $memo = '';
our $staff_notes = '';				
our $trans_date = '';				
our @trans_types = ();
our $t_menu = '';

our %TMPL = (	'main'	=> '10375' );

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my $currency='USD';
my $date_redeem = '';
my $redeem_cash = 0;

if ($operator && $pwd)
{
	# our admin access
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'rewards_admin.cgi', $operator, $pwd )){exit}

	$db->{RaiseError} = 1;
	$action = $q->param('action');
	$t_menu = cboTransactions();	# The transaction types menu.

	# No action means we display the main menu only.
	unless ( $action )
	{
		Do_Page();
	}
	# Otherwise, there was some type of action submitted.
	else
	{
		if (my $search_id = G_S::PreProcess_Input($q->param('id')))
		{
			$id = ($search_id =~ /\D/) ? G_S::Alias2ID($db, 'members', $search_id) : $search_id;
			unless ($id){
				Err("Member lookup of $search_id failed");
				goto 'END';
			}
		}

		my $results = '';

		# A request to transfer some members.
		if ( $action eq 'show' )
		{
			# Get the transactions related to the member ID.
			$results = Get_Transactions($id);
		}
		# A request to show staff notes.
		elsif ( $action eq 'show_note' )
		{
			# Get the staff notes.
			my $title = 	qq!Staff Notes - Transaction #!
					. $q->param('trans_id');
			my $note = 	qq#<p><strong><u>$title</u></strong></p>#
					. $q->param('note');
			# print the staff notes to a new window.
			print $q->header(-expires=>'now', -charset=>'utf-8'),
				$q->start_html( -title=>$title), $note;
			goto 'END';
		}
		elsif ( $action eq 'create' )
		{
			# Get the parameters for the new transaction.
			$type = $q->param('type');				
			$amount = $q->param('amount') || 0;				
			$memo = $q->param('memo') ;				
			$staff_notes = $q->param('staff_notes') ;				
			$trans_date = $q->param('trans_date');
                        $currency = $q->param('currency');
                        $date_redeem = $q->param('date_redeem');
	
# we want to mark a referenced transaction processed as well if we are doing a reversal
			if ( ($type == 3 || $type == 7 || $type == 12) && (my $rtrans = $q->param('rtrans')) )
#			if ( $type == 3 && (my $rtrans = $q->param('rtrans')) )
			{
				# Mark the original transaction as processed.
				if ( Mark_Original($rtrans) )
				{
					# Now create a progress report transaction.
					Add_Transaction();
				}
			}
			# If the transaction type is not blank and exists in the DB.
			elsif ( $type != '' && grep { /$type/ } @trans_types )
			{
				# Create a new transaction.
				Add_Transaction();
			}
			# We should never get here since we provide the list, but just in case.
			else
			{
				$message = qq#<span class="r">
						The transaction type '$type' is not allowed.<br>
						</span><br />#;
			}
			# Get the transactions related to the member ID just used for display.
			$results = Get_Transactions($id);			
		}
		else
		{
			$message = qq#<span class="r">
					The action '$action' is not allowed.<br />
					</span><br />#;
		}
		# Display the results along with the menu.
		Do_Page($results);
		END:
	}
	$db->disconnect;
}
else
{
	Err('This interface is for Admin use only. Please log in, then try again.');
}

exit;

#####################################
###### Begin Subroutines Here. ######
#####################################

# Add a transaction.
sub Add_Transaction
{
	my $sth = $db->prepare("
		INSERT INTO reward_transactions
			(id, trans_id, reward_points, reward_type, cur_xchg_rate, currency, stamp, notes, created, operator, date_redeemable, date_reported, vendor_id, processed, total_pending_rewards, staff_notes)
		VALUES (?, NULL, ?, ?, 1.0, ?, NOW(), ?, NOW(), ?, ?, ?, NULL, TRUE, ?, ?)");
	my $rv = $sth->execute($id, $amount, $type, $currency, $memo,  $operator, $date_redeem, $trans_date, $redeem_cash, $staff_notes);
	if ( $rv eq '0E0' )
	{
		$message .= qq#<span class="r">The new transaction was NOT added.</span><br />#;
	}
	else
	{
		$message .= qq#<span class="b">The new transaction was added.</span><br />#;
	}
}

###### Create a drop-down menu of transaction types.
sub cboTransactions
{
	my %labels = ();
	my $tmp = '';
	my $sth = $db->prepare("	SELECT type, description
					FROM 	reward_trans_types
					WHERE 	active = 'true'
					ORDER BY description;");
	$sth->execute();
	$labels{''} = 'Select a Type';
	push @trans_types, '';
	while ( $tmp = $sth->fetchrow_hashref )
	{
		$labels{$tmp->{type}} = $tmp->{description};
		push @trans_types, $tmp->{type};
	}	
	return $q->popup_menu(
			-name=>'type',
			-values=>\@trans_types,
			-labels=>\%labels,
			-default=>'',
			-class=>'ctl',
			-force=>1);
}

###### Display the page to the browser.
sub Do_Page
{
	my $results = shift || '';
	my $tmpl = G_S::Get_Object($db, $TMPL{'main'}) || die "Couldn't open template: $TMPL{'main'}";

	$tmpl =~ s/%%me%%/$ME/g;
	$tmpl =~ s/%%message%%/$message/;
	$tmpl =~ s/%%results%%/$results/;
	$tmpl =~ s/%%transaction_types%%/$t_menu/;

	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

###### Print an error message.
sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h4($_[0]), scalar localtime(),
		$q->end_html();
}

###### Get the member's transactions and build a results list for display.
sub Get_Transactions
{
	my $search_id = shift || '';
	my $where_clause = '';
	my $tbl = '';

	# If we have an ID.
	if ( $search_id )
	{
		$where_clause = " WHERE r.id = $search_id";
	}
	# No search ID, so we'll return all un-processed transactions as the default.
	else
	{
		$where_clause = " WHERE r.processed IS NULL OR r.processed = FALSE";
	}
	my $sth = $db->prepare("SELECT r.*,
				       rt.description,
                                       rc.three 
				FROM 	reward_transactions r
				INNER JOIN reward_trans_types rt
				ON r.reward_type = rt.type
                                LEFT JOIN reward_currencies rc
                                ON rc.code = r.currency"
				. $where_clause
				. " ORDER BY r.stamp, r.pk;"
#				. " ORDER BY r.reward_type, r.pk;"
					);
	my $rv = $sth->execute();
	if ( $rv eq '0E0' ) { $message .= qq#<span class="r">No transactions were found.</span><br /># }
	else
	{
		my $bg = '#ffffee';
		my @Sort = ('trans_date', 'trans_id', 'id', 'description', 'notes', 'staff_notes', 'amount', 'fee', 'processed');
		$tbl = $q->Tr({-bgcolor=>$bg}, $q->th(\@Sort));

		while ( my $tmp = $sth->fetchrow_hashref )
		{
			###### alternate our row color
			$bg = ($bg eq '#ffffee') ? '#eeeeee' : '#ffffee';
			$tbl .= qq|<tr bgcolor="$bg">\n|;
			foreach (@Sort)
			{
				$tbl .= qq#<td align="center">#;
				if ( $_ eq 'processed' )
				{
				###### we have problems when a memo contains apostrophes
					$tmp->{notes} =~ s#'#\\'#g;
					$tbl .= $tmp->{$_} == 1 ? 'Yes' :
						qq|<a href="#" onclick="PopulateLine('$tmp->{id}','$tmp->{pk}','3','Redemption Processed - $tmp->{notes}','$tmp->{currency}'); return false;">No</a>|;
				}
				elsif ( $_ eq 'amount' )
				{
				    my $ptpt = sprintf("%.2f",$tmp->{reward_points}*$tmp->{cur_xchg_rate});
				    $tbl .= $ptpt . " " . $tmp->{currency} || '-';
				}
				elsif ( $_ eq 'staff_notes' && $tmp->{staff_notes} ne '' )
				{
					$tbl .= qq#<a href="$ME?action=show_note;note=$tmp->{staff_notes};trans_id=$tmp->{pk}" target="_blank">View Notes</a>#;
				}
                                elsif ( $_ eq 'fee') {
                                    $tbl .= $tmp->{three};
                                }
                                elsif ( $_ eq 'trans_date') {
				    $tbl .= $tmp->{date_reported};
                                }
				elsif ( $_ eq 'id' )
				{
					$tbl .= qq#<a href="/cgi/clubcash_monthly_detail.cgi?admin=1;report_id=$tmp->{id}" target="_blank">	$tmp->{id}</a>#;
				}
				else
				{
				    if ($_ eq 'trans_id') {
					$tbl .= $tmp->{pk};
				    }
				    else {
					$tbl .= $tmp->{$_} || '&nbsp;';
				    }
				}
				$tbl .= "</td>";
			}
		}
		$tbl = qq#<tr><td align="center">
				<table border="0" cellpadding="3" cellspacing="1">
				$tbl
				</table>
				</td></tr>#;
	}
	return $tbl;
}

###### Mark the original transaction as processed.
sub Mark_Original
{
	my $trans_id = shift;
	my $sth = $db->prepare("	UPDATE reward_transactions
					SET processed = 't'
					WHERE pk = ?
					AND (processed IS NULL
						OR processed = 'f');
					");
	my $rv = $sth->execute($trans_id);
	if ( $rv eq '0E0' )
	{
		$message .= qq#<span class="r">
				The original redemption request could not be processed.<br />
				Therefore, the new transaction was NOT added.
				</span><br />#;
		return 0;
	}
	return $rv;
}


###### 02/01/05 Added the staff_notes functionality for internal notes. Modified the template too.
###### 03/04/05 Added the link from the member IDs to their individual transaction reports.
###### 09/15/06 added escaping of the memo value inserted into the javascript call
###### 01/30/07 added handling for aliases and extraneous whitespace
