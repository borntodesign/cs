#!/usr/bin/perl -w
##
## subscription_member_order_tracking.cgi
##
## Deployed June, 2012	Bill MacArthur
##
## last modified: 05/08/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
use ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

my $title = 'Subscription Member Order Tracking';

my $q = new CGI;
$q->charset('utf-8');

my $action = $q->param('_action');
my $ME = $q->script_name;

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect('tracking_search.cgi', $operator, $pwd);
$db->{'RaiseError'} = 1;

unless ($action)
{
	SearchForm();
}
elsif ($action eq 'update')
{
	Update();
}
elsif ($action eq 'transfers_logged')
{
	CoopTransfersLogged();
}
else
{
	# we must be searching for an ID
	Search();
}

Exit();

###### start of subs

sub CoopTransfersLogged
{
	my $rid = $q->param('member_id') || die "Failed to receive an ID parameter";
	$rid = $db->selectrow_hashref('SELECT id, alias, firstname, lastname FROM members WHERE ' . ($rid =~ /\D/ ? 'alias=?': 'id=?'), undef, $rid) ||
			die "Could not identify a member with id: " . $q->param('member_id');
	my $start_date = $q->param('start_date') || die "Failed to receive a start_date parameter";
	my ($end_date) = $db->selectrow_array('SELECT ?::DATE + 30', undef, $start_date);
	die $db->errstr if $db->errstr;

	my $sth = $db->prepare("
		SELECT
			tl.coop_id, tl.member_id, tl.membertype, tl.transfer_type, tl.notes, tl.stamp::timestamp(0) AS stamp, tl.transfer_id, COALESCE(tl.operator,'') AS operator,
			tt.label AS transfer_type_label,
			m.firstname, m.lastname, m.alias, m.membertype AS current_membertype
		FROM coop.transfer_log tl
		JOIN coop.transfer_types TT ON tl.transfer_type=tt.transfer_type
		JOIN members m ON m.id=tl.member_id
		WHERE tl.stamp BETWEEN ? AND ?
		AND tl.recipient_id= $rid->{'id'}
		ORDER BY tl.stamp ASC, tl.member_id
	");
	$sth->execute($start_date, $end_date);
	die $db->errstr if $db->errstr;
	
	$title = "Manual Co-Op Transfers between $start_date and $end_date for: $rid->{'alias'} $rid->{'firstname'} $rid->{'lastname'}";
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>$title,
			'-style'=>'/css/admin/generic-report.css',
			'-encoding'=>'utf-8',
			'-style'=>{
				'src'=>'/css/admin/generic-report.css',
				'code'=>'
				td.txc { text-align:center; }
				table caption
				{
					font-family: arial,helvetica,sans-serif;
					font-weight:bold;
					text-align:left;
				}
				tr.voided { background-color: #ddd; }
				tr.spacer td { border:none }'
			}
		),
		$q->h4($title),
		$q->p( $q->a({'-href'=>$ME}, 'New Search') ),
		$q->p( $q->a({'-href'=>"$ME?_action=search"}, 'List all incomplete orders') ),
		'<table class="report"><caption></caption>',
		$q->Tr(
			$q->th('Transferee ID') .
			$q->th('Name') .
			$q->th('Transfer<br />mType') .
			$q->th('Live<br />mType') .
#			$q->th('LangPref') .
#			$q->th('Order Date') .
			$q->th('Transfer Type') .
			$q->th('Co-Op ID') .
			$q->th('Transfer Notes') .
			$q->th('Timestamp') .
			$q->th('Transfer<br />Batch') .
			$q->th('Operator')
		);
	
	my $flg = ();
	while (my $row = $sth->fetchrow_hashref)
	{
		# insert a blank row to separate distinct orders
		if ($flg && $flg != $row->{'transfer_id'})
		{
			print $q->Tr({'-class'=>'spacer'}, $q->td({'-colspan'=>9}, '&nbsp;') );
		}

		print $q->Tr(
			$q->td( $q->a({'-href'=>"https://www.clubshop.com/cgi/admin/memberinfo.cgi?id=$row->{'member_id'}", '-onclick'=>'window.open(this.href); return false;'}, $row->{'alias'}) ) .
			$q->td( "$row->{'firstname'} $row->{'lastname'}" ) .
			$q->td({'-align'=>'center'}, $row->{'membertype'}) .
			$q->td({'-align'=>'center'}, $row->{'current_membertype'}) .
			$q->td( $row->{'transfer_type_label'} ) .
			$q->td( $row->{'coop_id'} ) .
			$q->td( $row->{'notes'} ) .
			$q->td( $row->{'stamp' } ) .
			$q->td( $row->{'transfer_id'} ) .
			$q->td( $row->{'operator'} )
		);

		$flg = $row->{'transfer_id'};
	}
	
	print
		'</table>',
		$q->end_html;
}

sub err{
	my $msg = shift;

	print $q->header,
        $q->start_html,
	$msg,
	$q->end_html;
	exit;
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub ListInterface
{
	my $list = shift;
	$title .= ": Incomplete Orders";
	
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>$title,
			'-style'=>{
				'src'=>'/css/admin/generic-report.css',
				'code'=>'
				td.txc { text-align:center; }
				table caption
				{
					font-family: arial,helvetica,sans-serif;
					font-weight:bold;
					text-align:left;
				}'
			},
			'-encoding'=>'utf-8'
		),
		$q->h4($title),
		$q->p( $q->a({'-href'=>$ME}, 'New Search') ),
		'<table class="report">',
		$q->Tr(
			$q->th('Member ID') .
			$q->th('Name') .
			$q->th('Subscription ID') .
			$q->th('mType') .
			$q->th('Cnty') .
			$q->th('LangPref') .
			$q->th('Order Date') .
			$q->th('Order Amount') .
			$q->th('Filled So Far') .
			$q->th('Last Changed') .
			$q->th('Order History') .
			$q->th('Staff Notes')
		);
	
	foreach my $row (@{$list})
	{
		# don't show voided orders
		next if $row->{'void'};

		$row->{'history'} =~ s#\n#<br />#g;
		print $q->Tr(
			$q->td(
				$q->a({'-href'=>"$ME?_action=search;member_id=$row->{'member_id'}"}, $row->{'alias'})
			) .
			$q->td($row->{'name'}) .
			$q->td(
				$q->a({'-href'=>"/cgi/admin/subscriptions.cgi?id=$row->{'member_id'}", '-onclick'=>'window.open(this.href);return false;', '-title'=>'Subscriptions Report'}, $row->{'subscription_id'})
			) .
			$q->td({'-class'=>'txc'}, $row->{'membertype'}) .
			$q->td({'-class'=>'txc'}, $row->{'country'}) .
			$q->td({'-class'=>'txc'}, $row->{'language_pref'}) .
			$q->td($row->{'order_date'}) .
			$q->td($row->{'pkg_member_cnt'}) .
			$q->td($row->{'filled_so_far'}) .
			$q->td($row->{'last_modified'}) .
			$q->td($row->{'history'}) .
			$q->td($row->{'notes'})
		);
	}
	
	print
		'</table>',
		$q->end_html;
}

sub ManagementForm
{
	my $rid = shift;
	my $list = shift;

	my $unfilled_content = '';
	my $filled_content = '';

	foreach my $row (@{$list})
	{
		$row->{'history'} =~ s#\n#<br />#g;

		if ($row->{'remaining'} > 0 && ! $row->{'void'})
		{
			$unfilled_content .=
				$q->start_form('-method'=>'get') .
				$q->hidden('-name'=>'subscription_id', '-value'=>$row->{'subscription_id'}) .
				$q->hidden('-name'=>'member_id', '-value'=>$row->{'member_id'}) .
				$q->hidden('-name'=>'period', '-value'=>$row->{'period'}) .
				$q->hidden('-name'=>'_action', '-value'=>'update', '-force'=>1) .
				'<table class="report"><caption>Incomplete Order</caption>' .
				$q->Tr(
					$q->th('Member ID') .
					$q->th('Name') .
					$q->th(
						$q->a({'-href'=>"/cgi/admin/subscriptions.cgi?id=$row->{'member_id'}", '-onclick'=>'window.open(this.href);return false;', '-title'=>'Subscriptions Report'}, 'Subscription ID')
					) .
					$q->th('mType') .
					$q->th('Cnty') .
					$q->th('LangPref') .
					$q->th('Order Date') .
					$q->th('Order Amount') .
					$q->th('Filled So Far') .
					$q->th('Last Changed') .
					$q->th('Order History') .
					$q->th('Staff Notes') .
					$q->th('Filled Today') .
					$q->th('Void') .
					$q->th('')
				) .
				$q->Tr(
					$q->td($row->{'alias'}) .
					$q->td($row->{'name'}) .
					$q->td($row->{'subscription_id'}) .
					$q->td({'-class'=>'txc'}, $row->{'membertype'}) .
					$q->td({'-class'=>'txc'}, $row->{'country'}) .
					$q->td({'-class'=>'txc'}, $row->{'language_pref'}) .
					$q->td( $q->a({'-href'=>"$ME?_action=transfers_logged;member_id=$row->{'member_id'};start_date=$row->{'order_date'}"}, $row->{'order_date'}) )  .
					$q->td($row->{'pkg_member_cnt'}) .
					$q->td($row->{'filled_so_far'}) .
					$q->td($row->{'last_modified'}) .
					$q->td($row->{'history'}) .
					$q->td(
						$q->textfield(
							'-value'=>$row->{'notes'},
							'-name'=>'notes',
							'-style'=>'width: 15em;',
							'-force'=>1
						)
					) .
					$q->td( $q->textfield('-name'=>'filled') ) .
					$q->td( $q->checkbox('-name'=>'void', '-value'=>1, '-checked'=>$row->{'void'}, '-label'=>'') ) .
					$q->td( $q->submit('-value'=>'Update') )
				) .
				'</table>' .
				$q->end_form;
		}
		else
		{
			my $class = $row->{'void'} ? 'voided' : '';
			$filled_content .=
				$q->Tr({'-class'=>$class},
					$q->td($row->{'alias'}) .
					$q->td($row->{'name'}) .
					$q->td($row->{'subscription_id'}) .
					$q->td({'-class'=>'txc'}, $row->{'membertype'}) .
					$q->td({'-class'=>'txc'}, $row->{'country'}) .
					$q->td({'-class'=>'txc'}, $row->{'language_pref'}) .
					$q->td( $q->a({'-href'=>"$ME?_action=transfers_logged;member_id=$row->{'member_id'};start_date=$row->{'order_date'}"}, $row->{'order_date'}) ) .
					$q->td($row->{'pkg_member_cnt'}) .
					$q->td($row->{'filled_so_far'}) .
					$q->td($row->{'last_modified'}) .
					$q->td($row->{'history'}) .
					$q->td($row->{'notes'}) .
					$q->td({'-align'=>'center'}, $row->{'void'} ? 'X' : '' )
				);
		}
	}
	
	$title .= ": $rid->{'alias'} $rid->{'firstname'} $rid->{'lastname'}";
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-title'=>$title,
			'-style'=>'/css/admin/generic-report.css',
			'-encoding'=>'utf-8',
			'-style'=>{
				'src'=>'/css/admin/generic-report.css',
				'code'=>'
				td.txc { text-align:center; }
				table caption
				{
					font-family: arial,helvetica,sans-serif;
					font-weight:bold;
					text-align:left;
				}
				tr.voided { background-color: #ddd; }'
			}
		),
		$q->h4($title),
		$q->p( $q->a({'-href'=>$ME}, 'New Search') ),
		$q->p( $q->a({'-href'=>"$ME?_action=search"}, 'List all incomplete orders') ),
		$unfilled_content,

		'<table class="report"><caption>Completed Orders</caption>',
		$q->Tr(
			$q->th('Member ID') .
			$q->th('Name') .
			$q->th(
				$q->a({'-href'=>"/cgi/admin/subscriptions.cgi?id=$rid->{'id'}", '-onclick'=>'window.open(this.href);return false;', '-title'=>'Subscriptions Report'}, 'Subscription ID')
			) .
			$q->th('mType') .
			$q->th('Cnty') .
			$q->th('LangPref') .
			$q->th('Order Date') .
			$q->th('Order Amount') .
			$q->th('Filled So Far') .
			$q->th('Last Changed') .
			$q->th('Order History') .
			$q->th('Staff Notes') .
			$q->th('Void')
		),
		$filled_content,
		'</table>',
		$q->end_html;
}

sub Search
{
	my $qry = "
		SELECT
			s.member_id,
			s.subscription_id,
			s.order_date::DATE,
			s.pkg_member_cnt,
			s.filled_so_far,
			s.last_modified,
			(s.pkg_member_cnt - s.filled_so_far) AS remaining,
			s.history,
			s.notes,
			s.period,
			s.void,
			m.alias,
			m.membertype,
			(m.firstname||' '||m.lastname) AS name,
			m.country,
			m.language_pref
		FROM subscription_pkg_member_orders s
		JOIN members m
			ON m.id=s.member_id
	";

	my $id = $q->param('member_id') || '';
	$id =~ s/\s//g;
	if ($id)
	{
		$id = uc($id);
		$id = $db->selectrow_hashref('SELECT id, alias, firstname, lastname FROM members WHERE ' . ($id =~ /\D/ ? 'alias=?': 'id=?'), undef, $id) ||
			die "Could not identify a member with id: " . $q->param('member_id');
		
		$qry .= "WHERE s.member_id= $id->{'id'} ORDER BY s.order_date DESC";
	}
	else
	{
		$qry .= "WHERE s.filled_so_far < s.pkg_member_cnt ORDER BY order_date ASC, remaining DESC, s.last_modified ASC";
	}
	
	my @rv = ();
	
	my $sth = $db->prepare($qry);
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	
	if ($id)
	{
		ManagementForm($id, \@rv);
	}
	else
	{
		ListInterface(\@rv);
	}
}

sub SearchForm
{
	print
		$q->header,
		$q->start_html('-title'=>$title),
		$q->h4($title),
		$q->start_form('-method'=>'get'),
		$q->hidden('-name'=>'_action', '-value'=>'search'),
		'Enter member ID: ',
		$q->textfield('-name'=>'member_id'),
		' ',
		$q->submit,
		$q->p('Leave the ID field blank if you want a list of all unfilled orders'),
		$q->end_form.
		$q->end_html;
}

sub Update
{
	my $filled = $q->param('filled');
	die 'You did not submit a value in "Filled Today. Back up and try again.' unless defined $filled;
	die "Invalid for 'Filled Today'. This should be numeric only: $filled" if $filled =~ m/\D/;
	
	my $void = $q->param('void') ? 'TRUE' : 'FALSE';
	my $qry_start = "UPDATE subscription_pkg_member_orders SET void= $void, notes=?";
	$qry_start .= ", filled_so_far= filled_so_far + $filled" if defined $filled && $filled gt '';
	$db->do("
		$qry_start 
		WHERE subscription_id= ?
		AND period= ?", undef, $q->param('notes'), $q->param('subscription_id'), $q->param('period'));
	
	Search();
}

__END__

05/08/15	Just a few tweaks to correct an encoding issue that came up after updating to the latest DBD::Pg
