#!/usr/bin/perl -w
###### translation_templates.cgi
###### view/edit/create translation templates
###### created: 08/19/04	Bill MacArthur
###### last modified: 02/29/16	Bill MacArthur

use strict;
use CGI::Pretty;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;
require ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

my ($db, $prompt, %param) = ();
my $q = new CGI;
$q->charset('utf-8');
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my $ME = $q->script_name;
my %ObjectTypes = (1=>'OMS Template');

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator') || '';
my $pwd = $q->cookie('pwd') || '';
exit unless ($db = ADMIN_DB::DB_Connect( 'translation_templates', $operator, $pwd ));
$db->{RaiseError} = 1;

###### load all of our parameters
foreach ($q->param)
{
	$param{$_} = $gs->Prepare_UTF8($q->param($_));
}

unless ($param{action})
{
	###### present search interface
	Search_Interface();
}
elsif ($param{action} eq 'view')
{
	###### lookup the translation and display it
	unless ($param{'tid'} || ($param{trans_type} && $param{obj_key}))
	{
		Err('Insufficient translation identifiers to perform a lookup');
	}
	else
	{
		my $translation = Lookup();

		unless ($translation)
		{
			Search_Interface();
		}
		elsif (! exists $translation->{translations})
		{
			Display_Item($translation);
		}
		else
		{
			Display_List($translation);
		}
	}
}
elsif ($param{action} eq 'update')
{
	Display_Item( Lookup() ) if Update();
}
elsif ($param{action} eq 'create')
{
	if ( Create_Translation() )
	{
		Display_List(Lookup());
	}
}
elsif ($param{action} eq 'compose')
{
	Composition_Interface();
}
else
{
	Err('Invalid action parameter');
}

END:
$db->disconnect;

exit;

sub Composition_Interface
{
	###### we should have a trans_type parameter and and object identifier
	print $q->header(-charset=>'utf-8');
	print $q->start_html(
		'-encoding'=>'utf-8',
		'-bgcolor'=>'#eeffee',
		-title=>"Compose a new translation",
		-style=>{-code=>"
			td, p, div, ctl {font-family:sans-serif; font-size:10px;}
	"});
	print $q->div({-style=>'font-weight:bold'}, 'Compose a new translation for:');
	print $q->div("$ObjectTypes{$param{trans_type}}: $param{obj_key}");
	print $q->p({-style=>'color:#ff0000'}, $prompt) if $prompt;
	print $q->start_form(-name=>$q->script_name, -style=>"font-size:2px"); 
	print 	$q->hidden(-name=>'action', -value=>'create', -force=>1),
		$q->hidden('trans_type'),
		$q->hidden('obj_key'),
		_ctl_Language(),
		' ',
		 _ctl_ObjectType()
	;
	print $q->br, $q->br, $q->textarea(
		-name=>'value',
		-rows=>20,
		'-style'=>'width: 100%',
		-class=>'ctl'
	);
	print 	$q->br, $q->br, $q->submit('Create');
	print $q->end_form, $q->end_html;
}

sub Create_Translation
{
	my $redo = ();
	unless ($param{language})
	{
		$prompt .= 'Desired language must be selected.<br />';
		$redo = 1;
	}
	unless ($param{'value'})
	{
		$prompt .= 'The template is empty.<br />';
		$redo = 1;
	}

	my $rv = $db->selectrow_array("
		SELECT pk FROM object_translations
		WHERE 	obj_key= $param{obj_key}
		AND 	language= '$param{language}'
		AND 	key_type= $param{trans_type}");

	if ($rv)
	{
		$prompt = "There is already a translation created for language: $param{language}<br />\n";
		$prompt .= $q->a({-href=>"$ME?action=view&obj_key=$param{obj_key}&trans_type=$param{trans_type}"}, 'Return to the list.');
		$redo = 1;
	}
			
	if ($redo)
	{
		Composition_Interface();
		return;
	}

	$param{'value'}=~ s/&#(\d*);/$1/g;	
	# Convert the object value to Unicode to accommodate foreign language characters.
#	$param{'value'}=~ s/&#(\d*);/Unicode_Convert($1)/ge;	

	$rv = $db->do("
		INSERT INTO object_translations (
			obj_key,
			language,
			value,
			key_type
		) VALUES (
			$param{obj_key},
			'$param{language}',
			?,
			$param{trans_type}
		)
	", undef, $param{'value'});

	return 1 if $rv eq '1';

	Err("Failed to create translation: $DBI::errstr");
	return;
}

sub Display_Item
{
	my $translation = shift;

	# print the page header here so that we can view Unicode.
#	print "Content-type: text/html\n\n";
	print $q->header();
	print $q->start_html(
		'-encoding'=>'utf-8',
		'-bgcolor'=>'#eeffee',
		-title=>"Display translations: $param{'tid'}",
		-style=>{-code=>"
			td, div {font-family:sans-serif; font-size:10px;}
	"});
	print $q->h5('Translation Detail');
	print $q->p({-style=>'color:#ff0000'}, $prompt) if $prompt;
	print $q->start_form(-method=>'post', -action=>$ME); 
	print $q->hidden(-name=>'action', -value=>'update', -force=>1);
	print $q->hidden('tid');
	print $q->hidden(-name=>'obj_key', -value=>$translation->{obj_key});
	print $q->hidden(-name=>'trans_type', -value=>$translation->{key_type});
	print qq|<table border="1" cellspacing="0" cellpadding="3" bgcolor="#ffffff">\n|;

	###### these will have to be made conditional
	###### once we add additional translation types
	my $row = 
		$q->td('Template ID') .
		$q->td('Language') .
		$q->td("Linked to cgi_object:") .
		$q->td('Last modified:') .
		$q->td('By:');
	print $q->Tr($row);

	$row = 
		$q->td($param{'tid'}) .
		$q->td("$translation->{language} &gt; $translation->{language_name}") .
		$q->td("$translation->{obj_key}: $translation->{relation_name}") .
		$q->td($translation->{'last_modified'}) .
		$q->td($translation->{operator});
	print $q->Tr($row);

	print "</table>\n";
	print $q->div(
		$q->a({-href=>$ME, -style=>'margin-left:500px'}, 'New Search') . ' | ' .
		$q->a({-href=>"$ME?action=view&trans_type=$translation->{key_type}&obj_key=$translation->{obj_key}"},
			'Return to Translation List') .
		$q->br .
		$q->checkbox(
			-name=>'active',
			-checked=>$translation->{active},
			-label=>'Active',
			-value=>1
		)
	);
	print $q->br;
	print $q->textarea(
		'-force'=>1,
		-name=>'value',
		-value=>$translation->{'value'},
		-wrap=>'off',
		'-style'=>'width: 100%',
		-rows=>30);
	print $q->br, $q->submit('Update'), $q->end_form, $q->end_html;
}

sub Display_List
{
	my $dat = shift;

	###### there should be a key in the hashref called translations that is itself
	###### an array of hashrefs for each translation
	###### if the key is empty then there are no matching translations

	my $list = '';
	foreach my $trans (@{$dat->{translations}})
	{
		$list .= $q->Tr({-class=>($trans->{active} ? 'active' : 'inactive')},
			$q->td( $q->a({-href=>$q->script_name . "?action=view&tid=$trans->{pk}"}), $trans->{pk}) .
			$q->td($trans->{language}) .						
			$q->td($trans->{language_name}) .
			$q->td({-align=>'center'}, $trans->{active} ? 'Y' : '') .
			$q->td($trans->{'last_modified'}) .
			$q->td($trans->{operator})
		);
	}

	if ($list)
	{
		$list = $q->Tr($q->th(
			['Translation ID', 'Lang. Code', 'Language', 'Active', 'Last Modified', 'Operator'])) . $list;
		$list = $q->table({
			'-bgcolor'=>'#ffffff',
			-border=>1,
			-cellspacing=>0,
			-cellpadding=>3}, $list);
	}
	else
	{
		$list = $q->h4('No matching translations');
	}

	print $q->header(-charset=>'utf-8');
	print $q->start_html(
		'-encoding'=>'utf-8',
		'-bgcolor'=>'#eeffee',
		-title=>"Translation List: $dat->{obj_key}: $dat->{obj_name}",
		-style=>{-code=>"
			p, th {font-family:sans-serif; font-size:10px;}
			td {
				font-family:sans-serif;
				font-size:10px;
				padding-left: 4px;
				padding-right: 4px;
			}
			tr.inactive {background-color:#eee;}
			tr.inactive td {color:#999;}
	"});
	print $q->p(
		$q->b("Translation List: ").
		$q->span({-style=>'background-color:#eeeeee; padding:2px;'},
			"$dat->{obj_key}: $dat->{obj_name}")
	);
	print $q->p(
		$q->a({-href=>$q->script_name}, 'New Search') . ' | ' .
		$q->a({-href=>$q->script_name . "?action=compose&obj_key=$dat->{obj_key}&trans_type=$dat->{key_type}"}, 'Create a new translation')
	);
	print $q->p({-style=>'color:#ff0000'}, $prompt) if $prompt;
	print "$list\n";

	print $q->end_html;
}

sub Err
{

	print $q->header(), $q->start_html();
	print $_[0];
	print "<br />\n";
	print $q->end_html();
}

sub Lookup
{
	my ($qry, $rv) = ();

	###### if we receive an actual translation ID, then we will have to gather
	###### the related information after we know what it is linked to
	if ($param{'tid'})
	{
		$qry = "
			SELECT o.pk,
				o.obj_key,
				o.language,
				o.stamp::timestamp(0) AS last_modified,
				o.operator,
				o.key_type,
				o.value,
				o.active,
				lc.description AS language_name
			FROM 	object_translations o
			JOIN	language_codes lc
				ON o.language = lc.code2
			WHERE 	o.pk= $param{'tid'}";

		$rv = $db->selectrow_hashref($qry);
		unless ($rv)
		{
			$prompt = "No match on $param{'tid'}";
			return;
		}

		###### at some point we may have other types (ebiz tests & answers)
		###### so we'll prepare for it now
		unless ($rv->{key_type})
		{
			die "Translation type is undefined";
		}
		elsif ($rv->{key_type} == 1)
		{
			$qry = "
				SELECT obj_name AS relation_name
				FROM cgi_objects
				WHERE obj_id= $rv->{obj_key}";
		}
		else{die "Translation has unrecognized type"}

		my $addl = $db->selectrow_hashref($qry);
		die "Failed to retrieve related object: $rv->{obj_key}" unless $addl;

		###### merge the two into our return value
		%$rv = (%$rv, %$addl);		
	}
	else
	{
		###### I guess we are looking for translations for a particular object
		###### we should have a trans_type and obj identifier at this point
		###### since we only have the one type, I'll build to that for now
		if ($param{trans_type} == 1)
		{
			$rv = $db->selectrow_hashref("
				SELECT o.obj_name,
					o.obj_id AS obj_key,
					o.obj_type,
					o.translation_key,
					o.active,
					1 AS key_type
				FROM 	cgi_objects o
				WHERE 	obj_id= $param{obj_key}");
			unless ($rv)
			{
				$prompt .= "No matching master object found for: $param{obj_id}<br />\n";
				return;
			}

			unless ($rv->{translation_key})
			{
				###### if it is not a key, any translations connected to it
				###### will not be used
				$prompt .= "<i>The master object is not a translation key so any translations listed or created will not be used.</i><br />\n";
			}
		}

		###### now let's look up our translations
		my $sth = $db->prepare("
			SELECT o.pk,
				o.language,
				o.stamp::timestamp(0) AS last_modified,
				o.operator,
				o.active,
				lc.description AS language_name
			FROM 	object_translations o, language_codes lc
			WHERE 	o.obj_key= $rv->{obj_key}
			AND 	o.language = lc.code2
			ORDER BY lc.description");
		$sth->execute;
		while (my $tmp = $sth->fetchrow_hashref)
		{
			push (@{$rv->{translations}}, $tmp);
		}
		$sth->finish;
		$rv->{translations} ||= ();
	}
	return $rv;
}

sub Search_Interface
{
	print 	$q->header(-charset=>'utf-8'),
		$q->start_html(
			'-bgcolor'=>'#ffffff',
			-title=>'Search translations',
			-style=>{-code=>"
				td, p {font-family:sans-serif; font-size:10px;}"},
			-onload=>'document.forms[0].tid.focus()'
	);
	print $q->h4('Search translations');
	print $q->p({-style=>'color:#ff0000'}, $prompt) if $prompt;
	print $q->start_form(-action=>$ME), $q->hidden(-name=>'action', -value=>'view');
	print $q->table({-border=>0},
		$q->Tr(
			$q->td({-align=>'right'}, 'Enter translation ID: ') .
			$q->td( $q->textfield(-name=>'tid', -class=>'ctl') )
		) .						
		$q->Tr(
			$q->td({-align=>'right'}, 'Or Search by primary object ID: ') .
			$q->td( $q->textfield(-name=>'obj_key', -class=>'ctl') . ' ' .
			_ctl_ObjectType() )
		) .
		$q->Tr(
			$q->td({-colspan=>2, -align=>'center'},
				$q->br .
				$q->submit(-value=>'Search', -class=>'ctl'))
		)
	),
	$q->end_form, $q->end_html;
}

#sub Unicode_Convert
#{
#	my $c=shift;
#	$us->pack($c);
#	return $us->utf8();
#}	

sub Update
{
	my $redo = ();
	unless ($param{'tid'})
	{
		$prompt .= 'No translation ID parameter.<br />';
		$redo = 1;
	}
	unless ($param{'value'})
	{
		$prompt .= 'The template is empty.<br />';
		$redo = 1;
	}

	if ($redo)
	{
	#	Composition_Interface();
		return 1;
	}

	$param{'value'}=~ s/&#(\d*);/$1/g;	
	my $active = $param{'active'} ? 'TRUE' : 'FALSE';
	my $rv = $db->do("
		UPDATE object_translations SET
			value = ?,
			active= $active
		WHERE 	pk= $param{'tid'}", undef, $param{'value'});

	return 1 if $rv eq '1';

	Err("Failed to update translation: $DBI::errstr");
	return;
}

sub _ctl_Language
{
	my $dat = $db->selectall_arrayref("
		SELECT code2, description
		FROM language_codes WHERE active= TRUE
		ORDER BY description");

	my @list = '';
	my %lbls = (''=>'Select a Language');
	
	foreach(@$dat)
	{
		push (@list, $_->[0]);
		$lbls{$_->[0]} = $_->[1];
	}

	return $q->popup_menu(
		-class=>'ctl',
		-name=>'language',
		-values=>\@list,
		-labels=>\%lbls
	);
}

sub _ctl_ObjectType
{
	###### since we only have one type at this time, it'll be easy
	return $q->popup_menu(
		-name=>'trans_type',
		-class=>'ctl',
		-values=>[1],
		-labels=>{1=>'CGI Object Template'}
	);
}

__END__

###### 08/25/04 added the Update handling
###### 09/28/04 Added the Unicode conversion for foreign languages.
###### 10/04/04 added -charset=>'utf-8' to the appropriate header calls
###### 10/08/04 added the timestamp and operator to the list
###### 01/25/06 Commented out utf8 conversion references and sub Unicode_Convert
# tweaks to deal with the underlying DBD::Pg changes that have occurred
05/20/15 a few more tweaks, most notably adding '-force' to the textarea creation in Display_Item as it renders garble if merely digesting the raw parameter for review after submission
02/29/16 Went to a CSS style to define the textarea width