#!/usr/bin/perl
###### 11/04/2002	Stephen Martin
###### since this script will attempt a DB connection either as the ADMIN user or cgi,
###### last modified: 11/17/14: Bill MacArthur

use Digest::MD5 qw( md5_hex );
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);

use lib '/home/httpd/cgi-lib';
use G_S; ###### HOLDS OUR GLOBAL CONFIGURATION VARIABLES
use ADMIN_DB;

$| = 1;
my $q = new CGI;
$q->charset('utf-8');

###### GLOBALS

our $MYURL = $q->script_name();
our($db, $sth, $rv) = ();
our $qry;

###### a flag that allows special admin access (this is set below after a successful admin DB connection)
our $ADMINMODE = ();

###### Form Input Variables 

my $id;
my $alias           = $q->param('alias');
my $spid            = $q->param('spid');
my $membertype      = $q->param('membertype');
my $prefix          = $q->param('prefix');
my $firstname       = $q->param('firstname');
my $mdname          = $q->param('mdname');
my $lastname        = $q->param('lastname');
my $suffix          = $q->param('suffix');
my $secprefix       = $q->param('secprefix');
my $secfirstname    = $q->param('secfirstname');
my $secmdname       = $q->param('secmdname');
my $seclastname     = $q->param('seclastname');
my $secsuffix       = $q->param('secsuffix');
my $company_name    = $q->param('company_name');
my $emailaddress    = $q->param('emailaddress');
my $emailaddress2   = $q->param('emailaddress2');
my $join_date       = $q->param('join_date');
my $address1        = $q->param('address1');
my $address2        = $q->param('address2');
my $city            = $q->param('city');
my $state           = $q->param('state');
my $postalcode      = $q->param('postalcode');
my $country         = $q->param('country');
my $cellphone        = $q->param('cellphone');
my $other_contact_info = $q->param('other_contact_info');
my $phone           = $q->param('phone');
my $fax_num         = $q->param('fax_num');
my $password        = $q->param('password');
my $memo            = $q->param('memo');
my $tin             = $q->param('tin');
my $mail_option_lvl = $q->param('mail_option_lvl');

my $action = $q->param('action');

###### Form Input Variables 

our $DB_error_contact = 'webmaster@dhs-club.com';

my $TMPL;
my $country_menu;
my $MESSAGE;
my $status = "0";

###### we wait to connect to the DB until we are sure we've got a good ID
###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd      = $q->cookie('pwd');

###### we will not allow admin access for update use (we have an in-house interface for that)
if ( $operator && $pwd )
{
    unless ( $db = ADMIN_DB::DB_Connect( 'cag.cgi', $operator, $pwd ) ){
        exit;
    }
    $ADMINMODE = 1;
}
else
{
	Err('You must be logged in to use this interface.');
	exit;
}

###### Trap the insert request 

if ( $action eq " Insert " ) {
    ###### Perform Insertion actions and then present form for another input

    unless ( $membertype
        && $spid
        && $firstname
        && $address1
        && $city
        && $state
        && $country
        && $phone
        && $password
        && $emailaddress )
    {
        Err("You are missing a mandatory parameter from the form input.");
        goto 'END';
    }

    ##### Verify our sponsor is a VIP

    unless ( isSvip($spid) ){
        Err("$spid is NOT is not a valid sponsor membertype?");
        goto 'END';
    }

    ##### Lock a new sequence Number 

    $id = Get_Sequence();

    $qry = "INSERT INTO members 
         (
          id,
	  spid,
	  membertype,
	  prefix,
	  firstname,
	  mdname,
	  lastname,
	  suffix,
	  secprefix,
	  secfirstname,
	  secmdname,
	  seclastname,
	  secsuffix,
	  company_name,
	  emailaddress,
	  emailaddress2,
	  alias,
	  address1,
	  address2,
	  city,
	  state,
	  postalcode,
	  country,
	  cellphone,
	other_contact_info,
	  phone,
	  password,
	  memo,
	  status,
	  mail_option_lvl,
	  tin
         ) values (?,?,?,?,?,?,
	           ?,?,?,?,?,
	           ?,?,?,?,?,
	           ?,?,?,?,?,
	           ?,?,?,?,?,
	           ?,?,?,?,?)";

    $sth = $db->prepare($qry);

    $rv = $sth->execute(
        $id,              $spid,         $membertype,   $prefix,
        $firstname,       $mdname,       $lastname,     $suffix,
        $secprefix,       $secfirstname, $secmdname,    $seclastname,
        $secsuffix,       $company_name, $emailaddress, $emailaddress2,
        $alias,           $address1,     $address2,     $city,
        $state,           $postalcode,   $country,      $cellphone, $other_contact_info,
        $phone,           $password,     $memo,         "1",
        $mail_option_lvl, $tin
    );

    defined $rv or die $sth->errstr;

    $rv = $sth->finish();

    $MESSAGE = "<i>$id ($alias) Inserted Successfully </i>";

}

###### Obtain Enter Info  Template

$country_menu = Get_Country();

$TMPL = G_S::Get_Object( $db, 10041 );
unless ($TMPL)
{
	Err("Couldn't retrieve the template object.");
	exit;
}

$TMPL =~ s/%%SELF%%/$MYURL/g;
$TMPL =~ s/%%COUNTRY_MENU%%/$country_menu/g;
$TMPL =~ s/%%MESSAGE%%/$MESSAGE/g;

###### we need to put the header here since error handling for the DB connection will handle its own header
print $q->header();

###### if there is no action then this is the beginning of a renewal

print $TMPL;

END:
$db->disconnect;
exit;
###### ###### ###### ###### ######

sub Err {
    my ($msg) = @_;
    print $q->header();
    print $q->start_html();

    print qq~
<center>
<h1>$msg</h1>
</center>
\n~;
    print $q->end_html();
}

sub DB_error
{
###### if we have already begun outputting a page, we don't want html headers again
    my ( $msg, $flag ) = @_;

    print $q->header();

    if ($flag) { print '<br />' }
    else {
        print $q->start_html(
            -title   => 'Database Error!',
            -bgcolor => '#ffffff'
        );
    }

    print
"There has been a database error. The problem and the specific error are shown below.<br>\n";
    print "Please "
      . $q->a( { -href => "mailto:$DB_error_contact" }, 'contact' )
      . " us with this information.<br>";
    print "<b>Problem:</b> $msg<br><b>Error:</b> $DBI::errstr\n";
    print $q->end_html();
}

sub Error_G
{
    print $q->start_html( -bgcolor => '#ffffff' );
    print $q->h3( $_[0] ), scalar localtime(), $q->end_html;
}

sub ErrMsg
{
###### provides a simple error report from which they will have to back out to correct field input errors
    print
qq|<h3 style="color: #ff0000;">Some required information was missing from your submission.</h3>|;
    print
qq|<h4>Please note the message/s below and press your 'Back' button to provide the information.</h4>|;
    print qq|<ul style="font-weight: bold; color: #ff0000;">\n|;
    foreach (@_) { print "<li>$_</li>\n" }
    print '</ul>', scalar localtime(), '</body></html>';
}

sub Get_Country
{
    my ( $sth, $qry, $data, $rv );    ###### database variables
    my $html;

    $qry = "SELECT * from tbl_countrycodes order by country_name DESC";

    $sth = $db->prepare($qry);

    $rv = $sth->execute();
    defined $rv or die $sth->errstr;

    while ( $data = $sth->fetchrow_hashref ){
        $html = $html . <<HTML;
<option value="$data->{country_code}">$data->{country_name}</option>
HTML

    }

    $rv = $sth->finish();

    return $html;
}

sub Get_Sequence
{
    my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = " select lpad((nextval('members_id_seq'))::TEXT,7,'0') as your_id";

    $sth = $db->prepare($qry);

    $rv = $sth->execute();
    defined $rv or die $sth->errstr;

    $data = $sth->fetchrow_hashref;

    $rv = $sth->finish();

    return $data->{your_id}

}

sub isSvip
{
    my ($spid) = @_;

    my $sth = $db->prepare("SELECT id
				FROM members
				WHERE membertype ~ 'm|v|ag' AND id = ?");

    $sth->execute($spid);

    my $data = $sth->fetchrow_array  || 0;

    $sth->finish;

    return $data;

}

###### 10/01/03 revised to allow a member or a vip to sponsor an AG
###### 11/19/03 now AGs can sign up AGs as well
# 07/21/06 made the status on signup a 1 instead of 0
# 10/11/06 added use lib
###### 12/18/07 removed phone_ac, added cellphone & other_contact_info
# 11/06/08 removed the old system_status hook
# 02/09/2011 g.baker postgres 9.0 port lpad(text, int, text) casts
# 11/17/14 made the charset utf-8 at the top of the script since the now live server does not have the hacked version of CGI.pm
