#!/usr/bin/perl -w
##
## xferlist.cgi	- a temp interface for generating a list of IDs from a coop to transfer for a given Partner ID entered
##
## Deployed June, 2012	Bill MacArthur
##
## last modified: 12/04/14	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use ADMIN_DB;

my $title = 'Transfer List: Generate a List of IDs';

my $q = new CGI;
my $action = $q->param('_action');
my $ME = $q->script_name;
my $gs = G_S->new();

##
## get the admin user and try logging into the DB
##
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
exit unless my $db = ADMIN_DB::DB_Connect('tracking_search.cgi', $operator, $pwd);
$db->{'RaiseError'} = 1;

unless ($action)
{
	SearchForm();
}
else
{
	# we must be searching for an ID
	Search();
}

Exit();

###### start of subs

sub Exit
{
	$db->disconnect;
	exit;
}

sub Search
{
	my ($tmp) = ();
	my $JOIN = '';
	my $JOINX = '';
	my $id = $gs->PreProcess_Input($q->param('member_id')) || die 'The recipient ID is required. Back up and try again.';
	my $coop_id = $gs->PreProcess_Input($q->param('coop_id')) || die 'The coop ID is required. Back up and try again.';
	my $number_needed = $gs->PreProcess_Input($q->param('number_needed')) || die 'The number needed is required. Back up and try again.';
	my $active_flag = $q->param('active_flag');
	my $ranking = $gs->PreProcess_Input($q->param('ranking'));
	
	die 'Number needed must be numeric only' if $number_needed =~ m/\D/;
	die 'Ranking must be numeric only' if $ranking =~ m/\D/;
	
	$id =~ s/\s//g;

	if ($id =~ m/\D/)
	{
		# convert an alias to a numeric ID
		$id = uc($id);
		($tmp) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, $id) || die "Could not identify a member with id: $id";
		$id = $tmp;
	}

	my $recipient = $db->selectrow_hashref("
		SELECT m.id, m.membertype, m.firstname ||' '|| m.lastname AS name, m.country, m.language_pref,
			nd.depth AS network_depth
		FROM members m
		JOIN network.depths nd
			ON nd.id=m.id
		WHERE m.id= $id");

	if ($coop_id =~ m/\D/)
	{
		# convert an alias to a numeric ID
		$coop_id = uc($coop_id);
		($tmp) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, $id) || die "Could not identify a coop membership with id: $coop_id";
		$coop_id = $tmp;
	}

	# verify that they have entered a valid coop ID
	($tmp) = $db->selectrow_array("SELECT coop_id FROM coop.coops WHERE coop_id= $coop_id") || die "A Co-Op container cannot be identified with id: $coop_id";
	$coop_id = $tmp;

	my $country = uc($q->param('country')) || $recipient->{'country'};
	my $language_pref = lc($q->param('language_pref')) || $recipient->{'language_pref'};
	
	my $membertype = $q->param('membertype');
	if ($membertype eq "s,m")
	{
		$membertype = "'s','m'";
	}
	else
	{
		$membertype = $db->quote($membertype);
	}

	if ($active_flag || $ranking)
	{
		$JOIN = "JOIN member_ranking_summary mrs ON mrs.member_id=m.id";
		if ($active_flag)
		{
			$JOIN .= " AND mrs.current_flag=1";
			$JOINX = " - Active=YES";
		}
		
		 if ($ranking)
		 {
		 	$JOIN .= " AND mrs.ranking_summary >= $ranking";
		 	$JOINX .= " - Rating &gt;= $ranking";
		 }
	}
	
	my @qry_args = ();
	my $qry = "
		SELECT
			m.id, m.membertype, nd.depth AS network_depth
		FROM members m
		LEFT JOIN network.depths nd
			ON nd.id=m.id
		$JOIN
		WHERE m.spid= $coop_id
		AND m.mail_option_lvl > 0
		AND m.emailaddress ~ '\@'
		AND m.membertype IN ($membertype)
		AND m.id != $recipient->{'id'}
	";
	
	if ($q->param('ignore_country'))
	{
		# since we are ignoring the country, we will not insert a country criteria
		$country = $q->span({'-style'=>'color:red'}, 'Ignored');
	}
	elsif ($q->param('none_country'))
	{
		$qry .= "AND m.country != ?\n";
		push @qry_args, $country;
		$country = $q->span({'-style'=>'color:red'}, "None from: $country");
	}
	else
	{
		$qry .= "AND m.country = ?\n";
		push @qry_args, $country;
	}
	
	unless ($q->param('ignore_language_pref'))
	{
		$qry .= "AND m.language_pref= ?\n";
		push @qry_args, $language_pref;
	}
	else
	{
		$language_pref = $q->span({'-style'=>'color:red'}, 'Ignored');
	}

	my $IDsort = ($q->param('orderby') || '') eq 'DESC' ? 'DESC' : 'ASC';
	$qry .= "
		ORDER BY m.id $IDsort LIMIT $number_needed
	";

	my @rv = ();

	my $sth = $db->prepare($qry);
	$sth->execute(@qry_args);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		warn "proc $tmp->{'id'}";
		if (TpXferOK($tmp, $recipient))
		{
			push @rv, $tmp;
			warn "adding $tmp->{'id'} to the list";
		}
	}
	
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html('-title'=>$title, 'encoding'=>'utf-8'),
		$q->h4($title),
		$q->start_form('-method'=>'get'),
		$q->p("List for $recipient->{'name'} - Co-Op ID: $coop_id - Country: $country - Language Pref: $language_pref - Number Needed: $number_needed - Member Type/s: $membertype $JOINX - Sorted by Member ID: $IDsort");
	print $q->h4({'style'=>'color:red'}, "Alert, there is not the number available that you requested: $number_needed - available: " . scalar @rv)
		unless scalar @rv == $number_needed;

	print
		'<textarea rows="20">';
	
	foreach my $row (@rv)
	{
		print "$row->{'id'}\n";
	}

	print
		'</textarea>',
		$q->end_form,
		$q->end_html;
}

sub SearchForm
{
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html('-title'=>$title, 'encoding'=>'utf-8'),
		$q->h4($title),
		$q->start_form('-method'=>'get'),
		$q->hidden('-name'=>'_action', '-value'=>'search'),
		$q->p('Enter Co-Op ID: ' . $q->textfield('-name'=>'coop_id')),
		$q->p('Enter Recipient ID: ' . $q->textfield('-name'=>'member_id')),
		$q->p('Enter Number of IDs needed: ' . $q->textfield('-name'=>'number_needed')),
		$q->p(
			'Optional Country Code: ' . $q->textfield('-name'=>'country') .
			' -OR- ' . $q->checkbox('-name'=>'ignore_country', 'value'=>1, '-label'=>'Ignore the country altogether') .
			' -OR- ' . $q->checkbox('-name'=>'none_country', 'value'=>1, '-label'=>'None from this country')
			),
		$q->p('Optional Language Code: ' . $q->textfield('-name'=>'language_pref') . ' -OR- '. $q->checkbox('-name'=>'ignore_language_pref', 'value'=>1, '-label'=>'Ignore the Language altogether') ),
		$q->p('Active: ' . $q->checkbox('-name'=>'active_flag', 'value'=>1, '-label'=>'Y') . ' otherwise N/A'),
		$q->p('Rating: ' . $q->textfield('-name'=>'ranking', '-value'=>0)),
		$q->p(
			'Order by Member ID: ' .
			'<input type="radio" name="orderby" checked="checked" value="ASC" /> Ascending &nbsp;&nbsp;' .
			'<input type="radio" name="orderby" value="DESC" /> Descending &nbsp;&nbsp;'
		),
		q|Desired Member Type: <select name="membertype">
		<optgroup style="background-color:#cfc"><option value="m">Affiliates (m)</option></optgroup>
		<optgroup style="background-color:#ffc"><option value="s">Members (s)</option></optgroup>
		<option value="s,m" style="background-color:#faa">Members and Affiliates</option>
		<optgroup style="background-color:#cfc"><option value="tp">Trial Partners</option></optgroup>
		</select> |,
		$q->submit . ' ',
		$q->reset,
		$q->end_form.
		$q->end_html;
}

# we have to ensure that this potential TP transfer will be in the same taproot at the recipient
# and we have to ensure that they will be under them in the nspid line (we will merely compare network depth for that)
sub TpXferOK
{
	my $x = shift;
	my $recipient = shift;
	return 1 if $x->{'membertype'} ne 'tp';
	($x->{'pnsid'}) = $db->selectrow_array("SELECT pnsid FROM od.pool WHERE id= $x->{'id'}");
	
	# set this up the first time through
	unless ($recipient->{'pnsid'})
	{
		if ($recipient->{'membertype'} eq 'tp')
		{
			($recipient->{'pnsid'}) = $db->selectrow_array("SELECT pnsid FROM od.pool WHERE id= $recipient->{'id'}");
		}
		elsif ($recipient->{'membertype'} eq 'v')
		{
			($recipient->{'pnsid'}) = $db->selectrow_array("
				SELECT od.pool.pnsid
				FROM od.pool
				JOIN network.pspids
					ON network.pspids.pid=od.pool.id
				WHERE network.pspids.id= $recipient->{'id'}");
		}
	}

	return 0 unless $x->{'pnsid'} == $recipient->{'pnsid'};
	return 0 unless $x->{'network_depth'} > $recipient->{'network_depth'};
	return 1;
}

__END__

11/02/12	Added handling for an ignore_language_pref option
07/05/13	Filled this request: Bill - can you also create selection fields as follows:

    Active: Y or N/A

    Rating: I can enter a number and the list includes all the memberships that have a number that is equal to or greater than the number provided

08/22/13	Filled the request for a checkbox to exclude the particular country
12/04/14	per Dick Dec 2014: add TPs to the pull down menu... this of course entailed the necessary checks to ensure netork integrity... same taproot and no sponsor inversion