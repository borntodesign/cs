#!/usr/bin/perl -w
###### cbid_batch_rpt.cgi
###### Lookup CBID batch jobs
###### last modified 05/23/03	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use lib ('/home/httpd/cgi-lib');
use G_S;
require '/home/httpd/cgi-lib/ADMIN_DB.lib';

###### GLOBALS

###### the table we'll get data from
our $TBL = 'cbid_batches';
our $q = new CGI;
my $ME = $q->url;
my $action = $q->param('action') || '';
my $search_field = $q->param('search_field');
my $search_value = $q->param('search_value');
my %TMPL = (	report	=> 10160 );
my ($db, @result_list, $data) = ();
 
# Added an = before each fieldname so grep can identify the beginning of/and the whole word.
my @char_fields = qw/=description =operator =creation_date =reconciled_date =reconciled_by/;
my @int_fields = qw/=id =cbid/;

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
unless ($db = ADMIN_DB::DB_Connect( 'cbid_batch_rpt.cgi', $operator, $pwd )){ exit }
$db->{RaiseError} = 1;

unless ($action)
{
	Report();
}

elsif ($action eq 'search')
{
	if (! $search_field){ Err('No search field received') }
	elsif (! $search_value){ Err('No search value received') }
	else
	{
		if ( validity_check() )
		{
			Run_Search();
			Report();
		}
	}
}

else { Err("Undefined 'action'") }

END:

$db->disconnect();
exit;

sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

sub Report{
###### spit out our page with a very simple format built-in here
	my $tmpl = G_S::Get_Object($db, $TMPL{report}) || die "Failed to load template: $TMPL{report}";
	
###### if there are multiple records we'll list them down the page in the same format
###### rather than displaying a list of results which has to be selected from

	$tmpl =~ s#(<RECURSE>)(.*)(</RECURSE>)#%%TBL%%#s;
	my $tblformat = $2;
	die "Failed to parse the report format" unless $tblformat;		

	my $cnt = 0;
	my $tbl = '';
			
	foreach my $dat (@result_list)
	{
		my $fmt = $tblformat;

		foreach ( keys %{ $dat } )
		{
			###### fill out our nulls
			$dat->{$_} ||= '&nbsp;';

			$fmt =~ s/%%$_%%/$dat->{$_}/;
		}

		$tbl .= $fmt;
		$cnt++;
	}

###### if they are searching and got no results, we'll tell 'em so
###### if they are just loading the page to begin a search, we'll tell 'em nothing
	$tbl = "<b>No matches found.</b>"  if ( $action eq 'search' && ! $tbl );

###### provide a record count if there were some
	$cnt = ($cnt) ? "Matches found: $cnt" : '';

	$tmpl =~ s/%%record_count%%/$cnt/;
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%TBL%%/$tbl/;
	print $q->header, $tmpl;
}

##### Runs the specified search, pushing the results onto a list (array).
sub Run_Search{
	my $sth;

###### in this case we want to convert the 'date' timestamped column to a simple date
###### to facilitate matches on submitted date type values
	if ( $search_field =~ /_date/)
	{
		$sth = $db->prepare("SELECT * FROM $TBL WHERE ($search_field)::DATE = ?");
		$sth->execute( $search_value );
	}

###### the rest of our textual fields
	elsif (grep { /=$search_field/ } @char_fields)
	{
		$sth = $db->prepare("SELECT * FROM $TBL WHERE $search_field ~* ?");
		$sth->execute( $search_value );
	}

###### in this case we need to search a range		
	elsif ( $search_field eq 'cbid')
	{
		$sth = $db->prepare("SELECT * FROM $TBL
					WHERE start_cbid <= $search_value
					AND end_cbid >= $search_value");

		$sth->execute();
	}

	else
	{
		$sth = $db->prepare("SELECT * FROM $TBL WHERE $search_field = ?");
		$sth->execute( $search_value );
	}
	
	while ( $data = $sth->fetchrow_hashref )
	{
		###### calculate the total records ~ I could have modified all the queries, but its easier here
		$data->{total_cbids} = $data->{end_cbid} - $data->{start_cbid} +1;

		###### convert a boolean TRUE/FALSE to something readable
		$data->{voided} = ($data->{voided}) ? "<span style=\"color: #ff0000; font-weight: bold\">Yes</span>" : 'No';
		
		push (@result_list, $data);
	}		

	$sth->finish();
}

sub validity_check{
##### Checks the validity of the Search Field and the Search Value before executing a query.
	if (grep { /=$search_field/ } @char_fields) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{
		if ($search_value =~ /\D/)		# Contains letters so not valid for the integer field.
		{
			Err( $q->span({-style=>'color: #ff0000'}, 'Invalid search value - Use only whole numbers with this field.') );
			return;	
		}
	}
	else
	{
		Err( $q->span({-style=>'color: #ff0000'}, 'Invalid field specification - Try again.') );
		return;				
	}
	return 1;						
}

###### initial release 05/15/03
###### 05/23/03 made the YES on a void appear in RED
