#!/usr/bin/perl -w
################################################################
# marketing_questionnairs_admin.cgi
#
# A general report for the Marketing Questionnaires
#
# Deployed: 06/08
# Author: Keith Hasely
# Version: 0.1
################################################################

################################################################
# Includes
################################################################
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use ADMIN_DB;
use XML::Simple;
use XML::local_utils;
use CGI::Carp qw(fatalsToBrowser set_message);

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
our $q = CGI->new();
our $xs = XML::Simple->new(NoAttr=>1);
our $gs = new G_S;

our $ME = $q->url;

our	$userid = 0;
our $db = ();
my $sth = '';
################################################################
# Subroutine(s)
################################################################

###############################
# This sub came around because
# Bill was/is using a "goto" statement!
#
###############################
sub exitScript
{
	$db->disconnect if ($db);
	exit;
}
###############################

###############################
# Output an error message for the user
# then exit the script cleanly
#
# Param		hash
# 			string message			The mesage to output before exiting.
# Return	null
###############################
sub errorExitScript
{
	my $args = shift || {};
	print $q->header(-TYPE => 'text/html');

	print qq(<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>);
	print $args->{message};
	print qq(</title></head><body><p>);
	print $args->{message};
	print qq(</p></body></html>);
	exitScript();
}
###############################


################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exitScript() unless $db;
$db->{RaiseError} = 1;


# Check if the user is admin
if ((!$q->cookie('operator') && !$q->cookie('pwd')) && !ADMIN_DB::DB_Connect( 'pwp', $q->cookie('operator'), $q->cookie('pwd')))
{
	exitScript();
}


print $q->header(-TYPE => 'text/html');
# @import "all.css"; /* just some basic formatting, no layout stuff */

print <<EOS;
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

				<title>Questionnaires Reports</title>

<style type="text/css">
	\@import "all.css"; /* just some basic formatting, no layout stuff */

	body {
		font:	12px/1.2 Verdana, Arial, Helvetica, sans-serif;
		background:#ddd;
		margin:10px 10px 0px 10px;
		padding:0px;
		}

	#leftcontent {
		position: absolute;
		left:10px;
		top:50px;
		width:300px;
		background:#fff;
		border:1px solid #000;
		}

	#rightcontent {
		position: absolute;
		left:310px;
		top:50px;
		width:300px;
		background:#fff;
		border:1px solid #000;
		}

	#header {
		background:#fff;
		height:39px;
		width:600px;
		border-top:1px solid #000;
		border-right:1px solid #000;
		border-left:1px solid #000;
		voice-family: "\\"}\\"";
		voice-family: inherit;
		height:39px;
		}
	html>body #header {
		height:39px;
		}

	p,h1,pre {
		margin:0px 10px 10px 10px;
		}

	h1 {
		font-size:14px;
		padding-top:10px;
		}

	#header h1 {
		font-size:14px;
		padding:10px 10px 0px 10px;
		margin:0px;
		}

</style>
</head><body>

<div id="header"><h1>Questionnaires Usage Report</h1></div>
<div id="leftcontent">
<h1>Total Surveys by Country Sorted by Member Count</h1>
<p>
EOS

##################################################
# Uncompleted surveys count.
##################################################
$sth = $db->prepare("
SELECT
	COUNT(m.*) AS member_count,
	m.country AS country_code,
	tcc.country_name
FROM
	questionnaires_user_status AS q
	JOIN
	members AS m
	ON
	m.id=q.userid
	LEFT JOIN
	tbl_countrycodes AS tcc
	ON
	tcc.country_code = m.country
GROUP BY
	m.country,
	tcc.country_name
ORDER BY
	member_count
");

$sth->execute();

print '<table>';
	print '<tr><td>';
	print 'Country';
	print qq(</td><td align="right">);
	print 'Total Members';
	print '</td></tr>';

while (my $row = $sth->fetchrow_hashref)
{
	print '<tr><td>';
	print $row->{country_name}?$row->{country_name}:'Undeclared';
	print qq(:</td><td align="right">);
	print $row->{member_count};
	print qq(</td><td width="5" align="right">);
	print '</td></tr>';
}
print '</table>';
if (!$sth->rows())
{
	print q(No surveys are incomplete. <br />);
}
print <<EOS;
<br />
</p>
</div>
<div id="rightcontent">
<h1>Total Surveys by Country<br />&nbsp;</h1>
<p>
EOS

##################################################
# Uncompleted surveys count.
##################################################
$sth = $db->prepare("
SELECT
	COUNT(m.*) AS member_count,
	m.country AS country_code,
	tcc.country_name
FROM
	questionnaires_user_status AS q
	JOIN
	members AS m
	ON
	m.id=q.userid
	LEFT JOIN
	tbl_countrycodes AS tcc
	ON
	tcc.country_code = m.country
GROUP BY
	m.country,
	tcc.country_name
ORDER BY
	tcc.country_name
");

$sth->execute();

print '<table>';
	print '<tr><td>';
	print 'Country';
	print qq(</td><td align="right">);
	print 'Total Members';
	print '</td></tr>';

while (my $row = $sth->fetchrow_hashref)
{
	print '<tr><td>';
	print $row->{country_name}?$row->{country_name}:'Undeclared';
	print qq(:</td><td align="right">);
	print $row->{member_count};
	print qq(</td><td width="5" align="right">);
	print '</td></tr>';
}
print '</table>';
if (!$sth->rows())
{
	print q(No surveys are incomplete. <br />);
}

print <<EOS;
<br />
</p>
</div>
	</body></html>
EOS
