#!/usr/bin/perl -w
###### assign-pspid.cgi
###### manage pool spids

use strict;

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
require ADMIN_DB;
require cs_admin;

my ($errors) = ();
my $q = new CGI;
my $cs = cs_admin->new({'cgi'=>$q});
my $ME = $q->script_name;

die 'You must be logged in as staff' unless $q->cookie('cs_admin');

my $rid = $q->param('rid');
$rid =~ s/^\s*|\s*$//g;

my $creds = $cs->authenticate($q->cookie('cs_admin'));
exit unless my $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );
$db->{'RaiseError'} = 1;

my $action = $q->param('action');
unless ($action)
{
	SearchForm();
}
elsif ($action eq 'search')
{
	DoForm();
}
else	# we must be performing an update
{
	Update();
}

Exit();

######

sub DoForm
{
	die "Cannot proceed without rid parameter" unless $rid;
	my $m = $db->selectrow_hashref('SELECT id, alias, firstname, lastname FROM members WHERE alias=? OR id= ?', undef, uc($rid), $rid);
	unless ($m)
	{
		$errors = "Could not find a membership matching id: $rid";
		SearchForm();
	}
	
	my ($pspid) = $db->selectrow_hashref('SELECT pid, stamp::TIMESTAMP(0) AS stamp, operator, notes FROM network.pspids WHERE id=?', undef, $rid);
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html(
			'-encoding' => 'utf-8',
			'-title' => 'Assign a PSPID (Pool Spid)',
			'-style' => {'-code' => 'th {text-align:right;} caption {text-align:right; font-weight:bold;}'}
		);

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->start_form('-method'=>'get'),
		$q->table(
			$q->caption('Assign a PSPID for:') .
			$q->Tr(
				$q->th('Name') . $q->td("$m->{'firstname'} $m->{'lastname'}")
			) .
			$q->Tr(
				$q->th('ID:') . $q->td($m->{'alias'})
			) .
			$q->Tr(
				$q->th('PSPID:') . $q->td($q->textfield('-name'=>'pspid', '-force'=>1, '-value'=>($pspid->{'pid'} || '')))
			) .
			$q->Tr(
				$q->th('Timestamp:') . $q->td($pspid->{'stamp'})
			) .
			$q->Tr(
				$q->th('Operator:') . $q->td($pspid->{'operator'})
			) .
			$q->Tr(
				$q->th('Notes:') . $q->td( $q->textarea({'-name'=>'notes', '-cols'=>30, '-rows'=>3}, $pspid->{'operator'}) )
			) .
			$q->Tr(
				$q->th() . $q->td({'-style'=>'text-align:right'}, $q->submit('Set Pool Spid') )
			) .
			$q->Tr(
				$q->th() . $q->td({'-style'=>'text-align:right; padding-top:2em;'}, $q->a({'-href'=>$q->url}, 'Start Over') )
			)
		),
		
		$q->hidden('-value'=>$m->{'id'}, '-name'=>'rid'),
		$q->hidden('-value'=>'update', '-name'=>'action', '-force'=>1),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub Exit
{
	$db->disconnect;
	exit;
}

sub SearchForm
{
	print
		$q->header('-charset'=>'utf-8'),
		$q->start_html('Assign a PSPID (Pool Spid)');

	print $q->p({'-style'=>'color:red'}, $errors) if $errors;

	print	
		$q->h4('Member Search'),
		$q->start_form('-method'=>'get'),
		$q->textfield('-value'=>'', '-name'=>'rid'),
		' '.
		$q->submit('Search'),
		$q->hidden('-value'=>'search', '-name'=>'action'),
		$q->end_form,
		$q->end_html;
	Exit();
}

sub Update
{
	die "Cannot proceed without rid parameter" unless $rid;
	my $pspid = ();
	my $tmp = $q->param('pspid');
	if ($tmp)
	{
		$pspid = $db->selectrow_hashref('SELECT id, alias, membertype FROM members WHERE id=?', undef, $tmp);
		unless ($pspid)
		{
			$errors = "Cannot find a membership matching $tmp";
			DoForm();
		}
		elsif ($pspid->{'membertype'} ne 'mp')
		{
			$errors = "Membership: $pspid->{'alias'} is not a member pool: $pspid->{'membertype'}";
			DoForm();
		} 
	}

	$db->begin_work;
	$db->do("DELETE FROM network.pspids WHERE id= ?", undef, $rid);
	$db->do(qq#INSERT INTO network.pspids (id, pid, stamp, operator, notes) VALUES (?, $pspid->{'id'}, NOW(), "current_user"(), ?)#, undef, $rid, $q->param('notes')) if $pspid;
	$db->commit;
	DoForm();
}