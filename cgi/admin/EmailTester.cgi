#!/usr/bin/perl -w
# EmailTester.cgi
# an admin script for testing HTML emails
# created 08/06/09	Bill MacArthur

use strict;
use CGI;
use LWP::Simple;
use lib '/home/httpd/cgi-lib';
use ADMIN_DB;
use CGI::Carp qw(fatalsToBrowser);
use MailingUtils;

my $q = new CGI;

my $DEF_FROM = 'Info@ClubShop.com';
my $DEF_SUBJECT = 'Test Subject';

my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');
my ($db) = ();
if ( $operator && $pwd )
{
        exit unless $db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd );
        $db->{RaiseError} = 1;
}
else
{
        die 'You must be logged in to use this interface.';
}

my ($msg, $error, %Params) = ();
LoadParams();

if ($Params{'_action'}){
	# SendMailing will return 0 on success or error otherwise
	$msg = "The message was sent to $Params{recipient}" unless ($error = SendMailing());
}

ShowInterface();
End();

######

sub End
{
	$db->disconnect;
	exit;
}

sub GetURL_Content
{
	die "Invalid URL submitted" unless $Params{url} =~ m#^http(s)?://#;
	return get $Params{url};
}

sub LoadParams
{
	foreach(qw/_action recipient html text subject from url/){
		$Params{$_} = $q->param($_);
	}
	$Params{from} ||= $DEF_FROM;
	$Params{subject} ||= $DEF_SUBJECT;
}

sub SendMailing
{
	return 'Recipient field is missing' unless $Params{recipient};
	$Params{html} = GetURL_Content();
	return 'Neither an HTML nor a Text version of the email was submitted' unless $Params{html} || $Params{text};
	my $mu = new MailingUtils;
	my %mailing = (
		from => $Params{from},
		subject => $Params{subject},
		to => $Params{recipient}
	);
	$mailing{txt} = \$Params{text} if $Params{text};
	$mailing{html} = \$Params{html} if $Params{html};
	
	$mu->Mail(%mailing) || die "Mail sending failed";
	return 0;
}

sub ShowInterface
{
	print $q->header, $q->start_html('Email Tester'), 
		$q->h4('Test your emails here');
		
	print $q->p({-style=>'color:red'}, $error) if $error;
	print $q->p({-style=>'color:green'}, $msg) if $msg;
	print $q->div({-style=>'float:right; font-weight:bold;'}, $q->a({-href=>$q->url}, 'Start Over'));
	print $q->start_form, $q->hidden(-name=>'_action', -value=>1),
		'From:<br />', $q->textfield(-name=>'from', -value=>$DEF_FROM, -size=>60, -onclick=>'this.select()'), '<br />',
		'To:<br />', $q->textfield(-name=>'recipient', -size=>60, -onclick=>'this.select()'), '<br />',
		'Subject:<br />', $q->textfield(-name=>'subject', -value=>$DEF_SUBJECT, -size=>60, -onclick=>'this.select()'),
		'<br />',
		'<br />Text Mailpiece<br />', $q->div({-style=>'float:right'},$q->submit('Send Mail')),
		$q->textarea(-name=>'text', -cols=>120, -rows=>20, -wrap=>'off', -onclick=>'this.select()'), '<br />',
		'<br />URL (must begin with http:// or https://)<br />',
		$q->textfield(-name=>'url', -size=>60, -onclick=>'this.select()'), '<br />Or',
		'<br />HTML Mailpiece<br />', $q->div({-style=>'float:right'},$q->submit('Send Mail')),
		$q->textarea(-name=>'html', -cols=>120, -rows=>40, -wrap=>'off', -onclick=>'this.select()'),
		$q->end_form, $q->end_html;
}
