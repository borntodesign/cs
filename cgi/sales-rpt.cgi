#!/usr/bin/perl -w
###### sales-rpt.cgi
###### display aggegate sales stats for upper level VIPs
###### released: 01/21/05	Bill MacArthur
###### last modified: 01/14/10	Bill MacArthur

use strict;
use lib('/home/httpd/cgi-lib');
use ParseMe;				
use G_S qw/$LOGIN_URL $EXEC_BASE/;
use DB_Connect;
use DBI;
use CGI;			
use CGI::Carp('fatalsToBrowser');
use XML::local_utils;
use XML::Simple;
use Encode;
#use XML::Dumper;

###### GLOBALS
our $q = new CGI;
our %param = (
	xml => ($q->param('xml') || '')
);
# xml params: raw= the entire report doc
#		final= the stuff going into the XSL, xsl= you guessed it!

our $data_path = '/home/httpd/cgi-data/sales-rpt';
our $min_report_role = 115;		# what is the minimum role to qualify for this rpt.
our $exclude_me_role = 120;		# those closest to us whom we'll remove from our stats		
our ($id, $meminfo, $db, $rid, $data, $file, $reportee) = ();
our $gs = new G_S;
our $xmlu = new XML::local_utils;
our $xml = new XML::Simple;
our %TMPL = (
	xsl	=> 10365,
	cats	=> 10558,
	content	=> 10373,
	trans_types => 10772
);
our $report_type = 2;
our $stamp = '';
our $cats_xml = '';
my $REPORTS = 'reports';	# our report table

###### I predict that although this report is supposed to be geared towards upper
###### level VIPs, that soon, this will need to report on one's downline
###### and then the report will eventually be available to all VIPs
###### I will build to that model even though to support lower levels will
###### require revising the backend generator to develop a dataset for each party

###### we need some kind of ID to proceed
###### we'll look for an ID parameter, a VIP cookie or a member cookie in that order
our $ADMIN = AdminCK();
$id = $q->param('id');
if ($id)
{
	if (! $ADMIN)
	{
		Err('Your administrative login is invalid');
		exit;
	}
	else
	{
		$meminfo = GetMemInfo($id);
	}
}
elsif (($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic')) )
{
###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ( $id && $id !~ /\D/ )
	{
		###### do nothing - they're all set
	}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
	else
	{
		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
		exit;
	}

###### if they are not a VIP, they don't have access to the report
###### for now anyway
	if ( $meminfo->{membertype} !~ 'v' )
	{
		Err('Sales Reports are for VIP members only'),
		exit;
	}
	$gs->Prepare_UTF8($meminfo);
}
else
{
	print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
	exit;
}
$db ||= DB_Connect('sales-rpt.cgi') || exit;
$db->{RaiseError} = 1;
#$db->{pg_enable_utf8} = 1;

our $content = $gs->Load_Messages($db, $TMPL{content}) ||
	die "Failed to load messages and content.\n";

###### for now, we need to check the role of the party *IF* we are not in ADMIN mode
if (! $ADMIN && ! RoleCheck())
{
	$data = XMLerr($content->{msg1});
	goto 'OUTPUT';
}

$rid = $q->param('rid');
###### only MDs and above have access to downline reports
if ((! $ADMIN) && $rid && $meminfo->{role} < 120)
{
	Err("Your role is insufficient to view a downline report");
	goto 'END';
}
###### we'll handle 'errors' in the sub
goto 'END' unless ValidateRid();

###### ###### from here on, $id will refer to the report ID
$id = $rid if $rid;

###### Get the current category selections.
GetResearchCats();

###### get all the available report dates for this party
our $sth = $db->prepare("
	SELECT stamp::DATE FROM $REPORTS
	WHERE type= $report_type AND id= ?
	ORDER BY stamp DESC");
$sth->execute($id);
our @available = ();
while (my $tmp = $sth->fetchrow_array)
{
	push (@available, $tmp);
}
$sth->finish;

###### if we came up empty handed, then we may as well drop further reporting
unless (@available)
{
	$data = XMLerr($content->{msg3});
	goto 'OUTPUT';
}

my $date = GetDate();
($stamp, $data) = $db->selectrow_array("
	SELECT stamp, report
	FROM $REPORTS WHERE id= ? AND type= $report_type AND stamp::DATE = '$date'",
	undef, $id);

unless ($data)
{
	($data = XMLerr($content->{msg2})) =~ s/%%date%%/$date/;
}
elsif ($ADMIN && $param{xml} eq 'raw')
{
	EmitXML($data); goto 'END';
}


OUTPUT:
#die "utf8:".Encode::is_utf8($data);
#$data = encode_utf8($data);
	
$data .= $xml->XMLout($meminfo, RootName=>'user', NoAttr=>1);
$data .= $xml->XMLout($reportee, RootName=>'reportee', NoAttr=>1) if $reportee;
$data .= $xml->XMLout($content, RootName=>'content', NoAttr=>1);
$data .= Import_Available_XML();
$data .= $cats_xml;
$data .= $gs->Get_Object($db, $TMPL{trans_types});

$data = qq|<?xml version="1.0" ?><base>\n$data\n</base>\n|;
if ($param{xml} eq 'final')
{
	EmitXML($data);
}
else
{
	our $xsl = $gs->Get_Object($db, $TMPL{xsl}) ||
		die "Failed to retrieve main template: $TMPL{xsl}\n";
		
	$xsl =~ s/%%script_name%%/$q->script_name/ge;
	$xsl =~ s/%%last_updated%%/$stamp/g;
	$xsl =~ s/%%report_date%%/($date || '')/e;
	$xsl =~ s/%%timestamp%%/scalar localtime()/e;
	$xsl =~ s/%%admin%%/$ADMIN/;
	$xsl =~ s/%%id%%/$id/g;

	if ($ADMIN && $param{xml} eq 'xsl')
	{
		EmitXML($xsl);
	}
	else{	print $q->header(-charset=>'utf-8'), $xmlu->xslt($xsl, $data) }
}
					
END:
$db->disconnect;
exit;

###### ###### Start of Sub Routines ###### ######

sub AdminCK
{
	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return 0 unless $operator && $pwd;

	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	$db = ADMIN_DB::DB_Connect( 'sales-rpt.cgi', $operator, $pwd ) ||
		"Failed to connect to the database with staff login";
	return 1;
}

sub EmitXML
{
	print "Content-type: text/plain\n\n$_[0]\n";
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub GetMemInfo
{
	my $id = shift;
	my $mem = $db->selectrow_hashref("
		SELECT id,spid,alias,firstname,lastname
		FROM members WHERE id=?", undef, $id) ||
	die "Failed to retrieve a member record for $id\n";
	return $mem;
}

sub GetDate
{
	my $date = $q->param('date') || $available[0];
	$date = '' if ($date && $date !~ /^\d{4}-\d{2}-\d{2}$/);
	return $date;
		
}

###### Get the research categories that this member has selected.
sub GetResearchCats
{
#	my @categories = ();
	if ( my @categories = $db->selectrow_array("
			SELECT cats
			FROM 	md_research
			WHERE id= ?", undef, $id) )
	{
		# We retrieved a comma separated list, so split it.
		@categories = split ",", $categories[0];
		# Get all the categories from the md_research content template and put them in a hash.
		my $tmp_cats = $gs->Get_Object($db, $TMPL{cats}) || die "Failed to retrieve template $TMPL{cats}\n";

#		$tmp_cats = encode_utf8($tmp_cats);
#	print $q->header(), $q->start_html(-encoding=>'utf-8');
#	print "utf8:", Encode::is_utf8($tmp_cats), "#\n";
#	print "\n$tmp_cats\n";
#	exit;
		$tmp_cats =~ s/^(.*?)categories>|<\/categories(.*?)$//gs;
		$tmp_cats = $xml->XMLin("<base>$tmp_cats</base>");

		# Now build out our VIP's selected categories xml.
		$cats_xml = "<categories>";
		foreach ( @categories )
		{
			# this brings back bad memories
			# the XML libraries evidently change the utf8 flag somehow on multi-byte characters
			my $cat = decode_utf8( XML_Clean($tmp_cats->{$_}{item}) );
			$cats_xml .= "\n<category>$cat</category>";
		}
		$cats_xml .= "\n</categories>";
	}
}

sub GetRidDetails
{
	###### lets populate our rid record
	$reportee = $db->selectrow_hashref("
		SELECT m.id, m.spid, m.alias,
			m.firstname, m.lastname,
			COALESCE(nc.npp, 0) AS npp
		FROM members m, network_calcs nc
		WHERE m.id=nc.id
		AND 	m.id= ?", undef, $rid);
}

sub Import_Available_XML
{
	###### assign some default value
	return '' unless @available;
	return $xml->XMLout(\@available, RootName=>'available');
}

sub RoleCheck
{
	###### let's see if our party is high enough to qualify for this report
	my $role = $db->selectrow_hashref("
		SELECT nbt.level, nbtl.label
		FROM 	nbteam_vw nbt, nbteam_levels nbtl
		WHERE 	nbt.level=nbtl.level
		AND 	nbt.id= ?",
	undef, $id);
	return unless $role->{level} && $role->{level} >= $min_report_role;
	$meminfo->{role} = $role->{level};
	return 1;
}

sub ValidateRid
{
	return 1 unless $rid;
	###### we won't bother with elaborate checking
	if ($rid =~ /\D/){
		Err('Invalid report ID parameter received');
		return;
	}
	my $rv = $id if $rid == $id;
	###### the party should be in our downline if it ain't us
	$rv ||= $db->selectrow_array("
		SELECT upline FROM relationships WHERE id=? AND upline=?",
		undef, ($rid, $id));
	unless ($rv){
		Err("$rid not found in your downline");
		return;
	}
	GetRidDetails();
	return 1;
}

sub XMLerr
{
	return "<error>$_[0]</error>\n";
}

###### Clean up any xml entities that might break the page later.
sub XML_Clean
{
	my $string = shift;
	$string =~ s/\&/\&amp\;/g;
	$string =~ s/\</\&lt\;/g;
	$string =~ s/\>/\&gt\;/g;
	return $string;
}

###### 09/13/05 added handling for viewing of downline reports :) `told ya`
###### 04/03/06 added utf8 flagging of the cookie data so it wouldn't get double encoded
###### 04/07/06 Added the research category display handling. Also changed the 'messages' template
######		name to 'content' since it is now used for titles and other items beyond messages
######		and included the content in the final xml.
###### 12/15/08
###### 01/14/10 added the transaction types XML document to the mix