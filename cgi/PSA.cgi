#!/usr/bin/perl -w
# (P)artner(S)ubscription(A)ccess
# a script to work the the Partner Subscription Access system

# this script will only connect to the DB if necessary instead of always at the beginning as most CGI scripts do
# the reason for this is that this resource could be called frequently by static pages and will not always need to make a DB call to do it's job

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use PartnerSubscriptionAccess;

my $DEF_RETURN_TYPE = 'text/javascript';
my $cgi = new CGI;
my $psa = PartnerSubscriptionAccess->new($cgi, {'db'=>\&DB});
#$psa->db(\&DB);	# new cannot handle a reference to a function
my (@path, $db, $cookie) = ();

(undef, @path) = split('/', $cgi->path_info);
# $path[0] will be our primary action parameter
# $path[1] will be our resourceID

# if there is no action parameter -and- they do not already have a PartnerSubscription cookie, then we will simply issue a PartnerSubscription cookie with an empty content body
unless ($path[0])
{
	BakeCookie();
	
	my $type = $cgi->param('type') || $DEF_RETURN_TYPE;
	print $cgi->header('-cookies'=>$cookie, '-type'=>$type);
}
elsif ($path[0] eq 'debug')
{
	debug();
}
elsif ($path[0] eq 'reset')		# we need to reissue the cookie... maybe they upgraded their subscription???
{
	print $cgi->header('-cookies'=>$psa->CreateCookieFromLoginCookie(), '-type'=>$DEF_RETURN_TYPE);
}
elsif ($path[0] eq 'script')	# they want the authorization javascript
{
	Script();
}

Exit();

###### start of sub-routines

sub BakeCookie
{
	unless ($psa->GetRawCookieValue())
	{
		$cookie = $psa->CreateCookieFromLoginCookie();
	}
}

sub DB
{
	$db ||= DB_Connect::DB_Connect('PSA') || exit;
	return $db;
}

sub debug
{
	use Data::Dumper;
	die Dumper($psa->ExtractCookieValue());
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Script
{
	my $resourceID = $path[1];
	my $authorized = $psa->Authorized($resourceID);
	my $ck = $psa->GetRawCookieValue();
	
	# they could be authorized by virtue of the fact that we looked them up and determined they were, even if they did not already have the cookie
	# in that case we should have created one which we can set when we send the data back
	if ($authorized && ! $ck)
	{
		$cookie = $psa->GetCookedCookieObject();
	}
	
	# avoid an uninit'd var error in the print below
	$resourceID ||= '';
	print $cgi->header('-cookies'=>$cookie, '-type'=>'text/javascript');
	if ($authorized)
	{
		print qq#
			var psa_authorized = $authorized;
			var resourceID = '$resourceID';
		#;
	}
	else
	{
		print qq#
			window.location = '//www.clubshop.com/cgi/PSAx?resourceID=$resourceID';
		#;
	}
	
	Exit();
}