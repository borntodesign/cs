#!/usr/bin/perl -w
###### comm_prefs.cgi
######
###### A script to show/update commission preferences
######
###### created: 02/20/06	Karl Kohrt
######
###### modified: 09/15/15	Bill MacArthur
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use ParseMe
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use Email::Valid;
use ADMIN_DB;
require cs_admin;

###### CONSTANTS
my $PREFS_TBL = 'comm_prefs';
my $cgiini = "comm_prefs.cgi";
# Payment options that can be automatically changed - no paperwork needed by accounting.
my @AUTO_OPTIONS = (1,2,7,9);
our %TMPL = (
	'xsl'		=> 10549,
	'content'	=> 10550
);

###### GLOBALS
my $q = new CGI;
$q->charset('utf-8');

my $xs = new XML::Simple('NoAttr'=>1);
my ($db, $xml, $member_id, $mem_info, $member_details, $login_type, @messages, @errors) = ();
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});

################################
###### MAIN starts here.
################################

###### force SSL invocation
unless ($q->https)
{
	my $url = $q->self_url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;																	
}
# Get the id for a logged in member.
Get_Cookie();	

# If they are logged in with a password.
if ( $login_type )
{
	if ($login_type eq 'admin')
	{
		$member_id = $q->param('id');
		unless ($member_id)
		{
			print $q->header, $q->start_html('Error - Admin Mode');
			print qq|<h4 style="color:red">Admin Mode</h4><p>Failed to receive 'id' parameter</p>|;
			print $q->end_html;
			End();
		}
		$member_id = $gs->Alias2ID($member_id) if $member_id =~ /\D/;
	}
	$member_details = Get_Member_Details() || die "Failed to retrieve member record with id: $member_id\n";

	# If this is an update.
	if ( ($q->param('action') || '') eq 'update' )
	{
		Update_Preferences();
		$member_details = Get_Member_Details();
	}

}
# If they are not logged in.
else
{
	###### just redirect them ;)
	print $q->redirect("$LOGIN_URL?destination=${\$q->script_name}");
	End();
}

# Now build out the page.
Display_Page();

End();

################################
###### Subroutines start here.
################################

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $xsl = $gs->GetObject($TMPL{'xsl'}) || die "Failed to load XSL template: $TMPL{'xsl'}\n";

	# Establish the xml document.
	$xml = "<base>\n<script_name>" . $q->script_name . '</script_name><prompts><errors>';
	$xml .= "<error>$_</error>\n" foreach @errors;
	$xml .= '</errors><messages>';
	$xml .= "<message>$_</message>\n" foreach @messages;
	$xml .= '</messages></prompts>';
	$xml .= '<admin>1</admin>' if $login_type eq 'admin';

	# Put the member info in if they are logged in.
	if ( $login_type )
	{
		$xml .= $xs->XMLout($member_details, 'RootName'=>'member');
		ParseMe::ParseAll(\$xsl, {'user'=>$member_details}, 1); 
	}

	my $content = $gs->GetObject($TMPL{'content'}) || die "Failed to load template: $TMPL{'content'}\n";

	# Combine the remaining pieces of the xml document.
	$xml .= $content
		. GetDistributionMethods()
		. "\n</base>\n";

	if ($q->param('xml'))
	{
		print "Content-type: text/plain\n\n$xml";
	}
	else
	{
		print $q->header, XML::local_utils::xslt($xsl, $xml);
	}
}

sub End
{
	$db->disconnect if $db;
	exit;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), $q->start_html, $q->p($_[0]), $q->end_html;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
#	my $operator = $q->cookie('operator');
#	my $pwd = $q->cookie('pwd');
	my $ck = $q->cookie('cs_admin');
	# If this is an admin user.		 
#	if ( $operator && $pwd )
	if ($ck)
	{
		my $cs = cs_admin->new({'cgi'=>$q});
		# Get the admin user and try logging into the DB
		my $creds = $cs->authenticate($ck);
		
		# Check for errors.
#		$db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ) or die "Admin access failed. Please login again.\n";
		$db = ADMIN_DB::DB_Connect( $cgiini, $creds->{'username'}, $creds->{'password'} ) or die "Admin access failed. Please login again.\n";
		
		# Mark this as an admin user.
		$login_type = 'admin';
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect($cgiini);
		$db->{'RaiseError'} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id && $member_id !~ m/\D/ )
		{
			$login_type = 'auth';
		}
	}
	$db->{'RaiseError'} = 1;
}

sub GetDistributionMethods
{
	my $sth = $db->prepare("SELECT payment_option, payment_type, fee_label, active FROM comm_prefs_payment_options");
	$sth->execute;
	my ($opt, $type, $flbl, $active) = ();
	my $rv = '<commission_options>';
	while (($opt, $type, $flbl, $active) = $sth->fetchrow_array)
	{
		$rv .= qq|<option type="$type" label="$flbl" active="$active">$opt</option>|;
	}
	return $rv . '</commission_options>';
}

###### Get the member info to populate the form.
sub Get_Member_Details
{
	my $tmp = $db->selectrow_hashref("
		SELECT m.id,
			m.alias,
			m.firstname,
			m.lastname,
			m.country,
			c.payment_type,
			COALESCE(c.account,'') AS account,
			c.stamp,
			c.operator,
			c.notes AS comm_prefs_notes
		FROM members m
		LEFT JOIN $PREFS_TBL c
			ON m.id = c.id
		WHERE m.id = ?", undef, $member_id);
	$gs->Null_to_empty($tmp);
	$tmp->{'firstname'} = XML_Clean($tmp->{'firstname'});
	$tmp->{'lastname'} = XML_Clean($tmp->{'lastname'});
	($tmp->{'pay_agent_country'}) = $db->selectrow_array("SELECT country_code FROM pay_agent.active_countries WHERE country_code= '$tmp->{'country'}'");
	if ($tmp->{'pay_agent_country'})
	{
		push @messages, 'pac1', 'pac2';
		@AUTO_OPTIONS = ();	# per Dick Sept 2015, these parties have no options but are fixed at CA
	}
	return $tmp;
}

###### Update the commission preferences.
sub Update_Preferences
{
#	my $choice = $q->param('choice') || '';
	my $payment_option = $q->param('payment_option') || 0;

	# Prevent a hack of the URL to change to a payment option that requires paperwork.
	$payment_option = '' if $login_type ne 'admin' && ! grep { /$payment_option/ } @AUTO_OPTIONS;

	# option 1 (Check) is only available to US members
	if ($payment_option == 1 and $member_details->{'country'} ne 'US' && $login_type ne 'admin')
	{
		push @errors, 'ck_us_only';
		return 0;
	}

	# If no choices were made, error out.
	if ( $payment_option eq '' )
	{ 
		push @errors, 'message3';
		return 0;
	}
	# Otherwise, let's process the update request.
	my $account = '';;

	if ($payment_option == 7){ $account = $q->param('account_pp'); }
	elsif ($payment_option == 8){ $account = $q->param('account_ap'); }
	elsif ($payment_option == 9){ $account = $q->param('account_ep'); }

	# we shouldn't have any whitespace in either an emailaddress or number
	$account =~ s/\s*//g;

	# These payment options require an email address as the account.
	if ($payment_option == 8 || $payment_option == 7)
	{
		# Unless it's a valid email address.
		unless ( $account && Email::Valid->address($account) )
		{
			push @errors, 'bad_email';
			return 0;
		}
	}
	elsif ($payment_option == 9)	# EcoCard requires an integer account number for performing transfers
	{
		if ($account =~ m/\D/)
		{
			push @errors, 'bad_numeric_account_number';
			return 0;
		}
	}
	
	###### there shouldn't be any notes unless we are in Admin mode
	my $notes = ();
	$notes = $q->param('comm_prefs_notes') if $login_type eq 'admin';

	my $qry = '';
	# If we found an existing comm_prefs record.
	if ( $member_details->{'payment_type'} )
	{
		$qry = "UPDATE $PREFS_TBL
			SET	payment_type = ?,
				account = ?,
				stamp = NOW(),
				operator = ${\($login_type eq 'admin' ? 'current_user' : $db->quote($cgiini))},
				notes = ?
			WHERE 	id = ?";
	}
	else
	{
		# If we didn't find a record , insert one.
		$qry = qq|INSERT INTO $PREFS_TBL (
				payment_type,
				account,
				stamp,
				operator, notes,
				id )
			VALUES (
				?, ?,
				NOW(),
				${\($login_type eq 'admin' ? 'current_user' : $db->quote($cgiini))},
				?, ? )|;
	}

	my $sth = $db->prepare($qry);
	my $rv = 0;

	# If we were able to create the record.
	if ( $sth->execute(($payment_option || undef), ($account || undef), $notes, $member_id) )
	{					
		$rv = 1;
		push @messages, 'message1';
		Get_Member_Details();
	}
	# If no record was created.
	else
	{					
		push @errors, 'message2';
	}
	$sth->finish;
	return $rv;
}

###### Clean up any xml entities that might break the page later.
sub XML_Clean
{
	my $string = shift;
	$string =~ s/\&/\&amp\;/g;
	$string =~ s/\</\&lt\;/g;
	$string =~ s/\>/\&gt\;/g;
	return $string;
}

__END__

###### 05/09/07 released heavily revised version
# there are no more 'hold' options and the options are now contained in a foreign key table using int keys
# the templates are heavily revised as well
# there is no backwards compatibility with a previous version
# 03/02/09 added handling for Alert Pay (type 8)
###### 04/20/09 tweaked out to simply expect one parameter named 'account' for whatever payment type
###### 03/29/13 tweaked to recognize option #7 PayPal
###### 04/16/13 added the PayPal option number to the @AUTO_OPTIONS list so that it would actually work outside of admin mode
04/23/15	No real functional changes
###### 12/04/13 removed Alert Pay (Payza) from the @AUTO_OPTIONS as Payza is no longer being offered as of December 2013 per Dick
09/15/15
	Went to cs_admin for admin authentication.
	Also added the check for pay_agent_country in Get_Member_Details