#!/usr/local/bin/perl -w
#
# This script gets your current location by either ip address, a default address set by you earlier, or if you type
# in an address. Given that address, it locates all merchants within a given diameter (actually a box). From that list,
# all of the categories are collected and placed in a pull down menu.  
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use CGI;
use CGI::Cookie;
use G_S;
use ParseMe
require XML::Simple;
require XML::local_utils;
#use Data::Dumper;			# only needed for debugging
binmode STDOUT, ":encoding(utf8)";

my %TMPL = ('xml'=>10897);
my $PI = 4*atan2(1,1);    # pi to max digits of the system
my $earth_radius_mi = 3960.0;
my $earth_radius_km = 6371.0;
my $deg2rad = $PI/180.0;
my $rad2deg = 180/$PI;
my $cgi = new CGI;
my $gs = new G_S;
my @box = ();
my $cookiedata = "";
my $keyword_where = ";";
my $keyword_join = " ";
my $lng = 0;
my $lat = 0;
my $range = 0.0;
my %categories = ();
my $language_code = $gs->Get_LangPref();
#
# Check to see if the default cookie is there
#
my $cgiini = "ipaddress.cgi";
exit unless my $db = DB_Connect($cgiini);
#
# Get the translation hash
#
my $xmt = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}\n";
$xmt =~ s/\n|\r|^M//g;
my $lu = new XML::Simple;
my $trans = $lu->XMLin($xmt, 'NoAttr'=>1);
#warn Dumper($trans);
#
# determine if using the default  or current location
#
my @def_address = split ('\|', ($cgi->cookie('rewards_def_location') || ''));
my @cur_address = split ('\|', ($cgi->cookie('rewards_cur_location') || ''));
my $iplocat = " ";
#
# We have a current address use it.
#
my $usonly = 0;
if (@cur_address)
{
    $lng = $cur_address[1];
    $lat = $cur_address[2];
    $range = $cur_address[3];
    if ($cur_address[0] =~ m/US|us/)
    {
        $usonly = 1;
    }
}
#
# We have a default address use it.
#
elsif (@def_address)
{
    $lng = $def_address[1];
    $lat = $def_address[2];
    $range = $def_address[3];
    if ($def_address[0] =~ m/US|us/)
    {
        $usonly = 1;
    }
}
#
# no default or current found, use the ipaddress
#
else
{
    my $ip = $cgi->remote_host();
    my @tokes = split('\.',, $ip);
    my $nip = $tokes[3] + $tokes[2] * 256 + $tokes[1] * 256 * 256 + $tokes[0] * 256 * 256 * 256;
    my $qry = "SELECT *
        FROM ip_latlong_lookup
        WHERE $nip <= ipTO and $nip >= ipFROM
        LIMIT 1;";
#    warn $ip . "---" . $qry."\n";
    my $tdata = $db->prepare($qry);
    my $rv = $tdata->execute;
    my $tmp = '';
    my $units = 'KM';
    my $city = '';
    my $region = '';
    my $country = '';
    while ($tmp = $tdata->fetchrow_hashref)
    {
        $lng = $tmp->{iplongitude};
        $lat = $tmp->{iplatitude};
        if ($tmp->{countryshort} eq 'US')
        {
            $usonly = 1;
            $units = "Miles";
        }
		$country = $tmp->{countryshort};
        $city = $tmp->{ipcity};
        $region = $tmp->{ipregion};
    }
    $range = 10.0;
    $iplocat = $city . "," . $region . "," . $country . "|" . $lng . "|" . $lat ."|" . $range . "|" . $units . "|By IP";
}
#
# loccookie is the current address. It is needed by rewardsdirectory.shtml to display certain default information
#
my $loccookie = "";
if ($iplocat ne " ")
{
    $loccookie = $cgi->cookie('-name'=>"rewards_cur_location", '-expires'=>"+30m", '-domain'=>"clubshop.com", '-value'=>$iplocat);
}
#
# start loading the data for the merchants located within the desired box around the location
#
$cookiedata = "\"$trans->{'here'}^ $lng^ $lat ^$trans->{'nomerch'}^$trans->{'free'}^$trans->{'merchsignup'} |";
#
# Calculate the desired box size
#

my $earth_radius = $earth_radius_km;
if ($usonly == 1) {
    $earth_radius = $earth_radius_mi;
}
my $box_lat_chg = (($range)/$earth_radius) * $rad2deg;
my $box_lon_chg = (($range)/($earth_radius * cos($lat*$deg2rad)))*$rad2deg;
$box[0] = $lng+$box_lon_chg;
$box[1] = $lat+$box_lat_chg;
$box[2] = $lng-$box_lon_chg;
$box[3] = $lat-$box_lat_chg;
$cookiedata .= "Top Left ^ " . $box[0] . "^" . $box[1] . " | Lower Right^ " . $box[2] . "^" . $box[3] . " | ";

my $qry = <<"EOD";
 SELECT DISTINCT
     mal.id,
     mal.merchant_id,
     mal.vendor_id,
     mal.location_name,
     COALESCE(mal.address1, '') AS address1,
     COALESCE(mal.address2, '') AS address2,
     COALESCE(mal.address3, '') AS address3,
     mal.city,
     mal.state,
     mal.country,
     mal.phone,
     mal.postalcode,
     mal.default_currency_code,
     mal.location_name_url,
     mal.longitude,
     mal.latitude,
     COALESCE(mam.url,'') AS url,
     mam.discount_type,
     mam.discount_subtype,
     COALESCE(mam.special,' ') AS special,
     COALESCE(mam.discount_blurb,'') AS discount_blurb,
     mam.business_type,
     d.cap,
     d.discount,
     v.blurb,
     COALESCE(v.coupon,'') AS coupon,
     bc.description AS category_description,
     scl.maincat as maincat_code,
     bc2.description AS maincat_desc,
     CASE
         WHEN
             mam.discount_type =  13
         THEN
             1
         WHEN
             mam.discount_type IN (12, 3)
         THEN
             2
         ELSE
             0
     END AS preference
     FROM
         merchant_affiliate_locations mal
     JOIN
         merchant_affiliates_master mam
         ON
            mal.merchant_id = mam.id
--     $keyword_join
     JOIN
         merchant_affiliate_discounts d
         ON
            mal.id = d.location_id
     JOIN
         vendors v
         ON
            v.vendor_id = mal.vendor_id
     JOIN
         business_codes('$language_code') bc
         ON
            mam.business_type = bc.code
     JOIN
        subcategory_link scl
        ON
         scl.category = mam.business_type
     JOIN
         business_codes('$language_code') bc2
         ON
            bc2.code = scl.maincat
     WHERE
         mal.longitude <= $box[0] and mal.longitude >= $box[2] and
         mal.latitude <= $box[1] and mal.latitude >= $box[3] and
         v.vendor_group = 5
         AND
             NOW()::date >= d.start_date
         AND
           (
             NOW()::date <= d.end_date
             OR
             d.end_date IS NULL
           )
         AND
             mam.discount_type IN (11, 3, 12, 2, 15, 14, 1, 0, 10, 13)
         AND
             v.status= 1
-- exclude our house testing account
         AND
             mam.id != 41
--         $keyword_where
    ORDER by mal.location_name;
EOD

my $exchange_rate_query =<<EOT;

    SELECT
         ern.rate,
         cc.description
    FROM
         currency_by_country cbc
    JOIN
         exchange_rates_now ern
       ON
          ern."code" = cbc.currency_code
    JOIN
         currency_codes cc
       ON
          cc."code" = cbc.currency_code
   WHERE
        cbc.country_code= ?

EOT

#
# load the merchant data into the array the javascript will use to place the pushpins
#
#    warn $qry . "\n";
my $tdata2 = $db->prepare($qry);
my $rv = $tdata2->execute;
$cookiedata =~ s/\n|\r/ /g;
#
# This loop goes through each merchant that are within the range box
#
while (my $tmp2 = $tdata2->fetchrow_hashref)
{
#
# Get the exchange rate for this entry
#
    my $country_code = uc($tmp2->{'country'});
    my ($exchange_rate, $currency_name) = $db->selectrow_array($exchange_rate_query, undef, $country_code);
    $exchange_rate = sprintf("%.3f", $exchange_rate);

    my $blurb = $cgi->escapeHTML($tmp2->{'discount_blurb'}) || '';
    my $blurb2 = $cgi->escapeHTML($tmp2->{'coupon'}) || '';
    my $blurb3 = $cgi->escapeHTML($tmp2->{'blurb'}) || '';
    chomp($tmp2->{'maincat_desc'});
#
# Clean up some of the data so it will display correctly
#
    $tmp2->{'location_name'} =~ s/\"/\'/g;
    $blurb =~ s/\n|\r/ /g;
    $blurb2 =~ s/\n|\r/ /g;
    $blurb3 =~ s/\n|\r/ /g;
    $blurb2 =~ s/&lt;/</g;
    $blurb2 =~ s/&gt;/>/g;
    $blurb2 =~ s/&quot;/\'/g;
#
# Dump out Specific merchants if necessary
#    if ($tmp2->{'location_name'} =~ m/Fisioterapia/) {
#	warn Dumper($tmp2);
#    }

#
# determine if merchant is a tracking merchant or not
#
    my $blurb4 = '';
    if ($tmp2->{'discount_type'} >= 0 && $tmp2->{'discount_type'} <= 5)
    {
#        warn Dumper($tmp2);
		$blurb4 = "ClubCash: " . sprintf('%.2f', $tmp2->{'discount'} * .4 * 100.0) . '%';

        my @minidee = split ('\|', ($cgi->cookie("minidee") || ''));
        
        if ($minidee[8] && $minidee[8] eq "v")
        {
            $blurb4 .= "<br/>Pay Points: " . sprintf('%.2f', $tmp2->{'discount'} * .2 * 100.0 / $exchange_rate) . '%';
            #$blurb4 .= "<br/>Referral Commission: " . sprintf ("%.2f\%", $tmp2->{'discount'} * .2 * 100.0);
        }
    }   
#
# Set up the discount amount
#
    else
    {
        if ($tmp2->{'discount_subtype'} >= 1 && $tmp2->{'discount_subtype'} <= 3)
        {
		    if ($tmp2->{'discount_subtype'} == 1)
		    { # Flat amount off
	                $blurb4 = $trans->{'amtoff'} ." ". $tmp2->{'special'};
		    }
		    elsif ($tmp2->{'discount_subtype'} == 2)
		    { # percentage off
	                $blurb4 = $trans->{'pctoff'} ." " . $tmp2->{'special'} * 100 . "&#37;";
		    }
		    elsif ($tmp2->{'discount_subtype'} == 3)
		    { # bogo
				if ($tmp2->{'special'} eq "free")
				{
	                    $blurb4 = $trans->{'buy1free'};
				}
				
				if ($tmp2->{'special'} eq "half")
				{
			    	$blurb4 = $trans->{'buy1half'};
				}
		    }
		}
		else
		{
            if ($tmp2->{'special'} eq ' ')
            {
                $tmp2->{'special'} = $tmp2->{'discount'};
            }
            
            $blurb4 = ($trans->{'pctoff'} ." " . $tmp2->{'special'} * 100 . "&#37;") if _looks_like_number($tmp2->{'special'});
            
            if ($tmp2->{'special'} eq "free")
            {
	       		$blurb4 = $trans->{'buy1free'};
            }
            
            if ($tmp2->{'special'} eq "half")
            {
	        	$blurb4 = $trans->{'buy1half'};
            }
            
            if ($tmp2->{'discount_type'} == 10 && _looks_like_number($tmp2->{'special'}))
            {
                if ($tmp2->{'special'} < 1.0)
                {
                    $tmp2->{'special'} *= 100;
                }
                $blurb4 = $trans->{'pctoff'} ." ". $tmp2->{'special'} . "&#37;";
            }
            
            if ($tmp2->{'discount_type'} == 13)
            {
                $blurb4 = $trans->{'amtoff'} ." ". $tmp2->{'special'} ;
	    	}
        }
    }
#
# Write the merchant to the javascript string
#
    $blurb4 =~ s/\n|\r/ /g;
    $tmp2->{'location_name'} =~ s/"/\\"/g;
    $tmp2->{'address1'} =~ s/"/\\"/g;
    my $locname = lc($gs->Prepare_UTF8($tmp2->{'location_name'}));
    $cookiedata .= "$locname^ $tmp2->{'longitude'}^ $tmp2->{'latitude'}^ $tmp2->{'business_type'}^ $tmp2->{'category_description'}^ $tmp2->{'address1'}^ $tmp2->{'phone'}^ $blurb4^ $blurb^ $blurb3^ $blurb2^ $tmp2->{'id'}^$tmp2->{'discount_type'}^$tmp2->{'maincat_code'}^$tmp2->{'maincat_desc'}";
#    %categories->{$tmp2->{category_description}} = $tmp2->{business_type};
    $categories{$tmp2->{'maincat_desc'}} = $tmp2->{'maincat_code'};
#
# Get the keyword selection
#
    my $qry2 = "SELECT mk.keyword from merchant_keywords mk
            JOIN merchant_keywords__merchant_master__links mkmm
                 ON mkmm.merchant_keywords_id = mk.merchant_keywords_id
            WHERE mkmm.merchant_id = $tmp2->{'merchant_id'}";
    my $tdata3 = $db->prepare($qry2);
    my $rv = $tdata3->execute;
#
# Add the keywords to the javascript string
#
    while (my $tmp3 = $tdata3->fetchrow_hashref)
    {
		$cookiedata .= "^$tmp3->{keyword}";
    }
    $cookiedata .= "|";
}
$cookiedata .= "\";";
if ($iplocat ne " ") {
#    warn Dumper($loccookie);
    print $cgi->header(-cookie=>$loccookie);
}
else {
    print $cgi->header();
}
print "<html>\n";
print "<head>\n";
print "<title>$trans->{'maps'}</title>\n";
#
# put the javascript out that will be used to display the map and push pins
#
print <<"EOT";
<link type="text/css" rel="stylesheet" href="/mall/_includes/css/rewardsmap.css">

<script type="text/javascript" src="/mall/_includes/js/rewardsmap-language.js"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/mall/_includes/js/rewardsmap.js"></script>
<script type="text/javascript" src="/mall/_includes/js/javacookie.js"></script>
<script type="text/javascript">
    var cook = $cookiedata;
</script>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
<script type="text/javascript" src="/mall/_includes/js/toggle.js"></script>

EOT
#
# put out the rest of the data
#
    print "</head>\n";
    print "<body onload=\"initialize()\">\n";
#    print "<form onsubmit=\"Checkeyword()\">";

	print  "<p class=\"slide\" ><button class=\"btn\" id=\"asearch\">$trans->{'advance'}</button></p>";
	print  "<div class=\"panel\">";

    print "<form onsubmit=\"return false\">";
    print "<select name=\"Category\" id=\"category\" onChange=\"Display_Cat_Map(this)\">";
    print "<option value=\"none\" selected=\"yes\">$trans->{'category'}</option>";
    foreach my $cat(sort keys %categories)
    { 
        if ($categories{$cat} ne "")
        {
            my $catname = $categories{$cat};
	    	print "<option value=\"$catname\"> ".$cat." </option>";
		}
    }
    print "</select>\n";
    print "<div id=\"category2div\">";
    print "<select name=\"Category2\" id=\"category2\" onChange=\"Display_Cat2_Map(this)\">";
    print "<option value=\"none\" selected=\"yes\">$trans->{subcategory}</option>";
    foreach my $cat(sort keys %categories)
    { 
        if ($categories{$cat} ne "")
        {
            my $catname = $categories{$cat};
	    	print "<option value=\"$catname\"> ".$cat." </option>";
		}
    }
    print "</select>\n";
    print "</div>\n";
    print  "<div class=\"fieldBox\">\n";
    print "<input type=\"text\" size=\"30\" name=\"search\" id=\"search\" onfocus=\"value=''\" value=\"$trans->{keyword}\" onchange=\"Checkeyword()\" >";	
	print "<input type=\"text\" title=\"Search\" class=\"fieldBox_btn\" id=\"search\" type=\"submit\" onclick=\"Checkeyword()\" onchange=\"Chedkeyword()\"  >";	
	print "</div>";
	
	print "</form>";
	
	print "</div>\n";	
    print "<div id=\"main\" style=\"width:100%; height:100%;\">\n";
    print "<div id=\"map_canvas\" style=\"width:68%; height:635px; float:left; overflow: visible;\"></div>\n";
    print "<div id=\"side_title\" >" . $trans->{'merchant'} . "</div>\n";
    print "<div id=\"side_html\" >\n";
    print "</div>\n";
    print "<p>\n";
    print "</body></html>\n";


sub _looks_like_number
{
	if (exists &looks_like_number)
	{
		return looks_like_number($_[0]);
	}
	# the old server does not have the necessary perl version with that core module
	return 1 if  $_[0] =~ /^-?\d+\.?\d*$/;
}