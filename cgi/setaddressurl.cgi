#!/usr/local/bin/perl
#
# Given the merchant location id, create the business url and do a redirect
#
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use LWP::UserAgent;
use URI::Escape;
use G_S;
use Data::Dumper;
use CGI;
use CGI::Cookie;

my $cgi = new CGI;
my $gs = new G_S;
my $url="http://www.clubshop.com/business/";
my $params = {};
my $qry = '';
#
# Open the daatabase to get the needed information
#
my $cgiini = "ipaddress.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
#
# Get The form input (using GET) these are currently location, change, check
#
my @formdata = split (/&/, $ENV{'QUERY_STRING'});
foreach  my $pair (@formdata) {
    my ($key, $value) = split(/=/, $pair);
    $value =~ tr/+/ /;
    $value =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
    $params->{$key} = $value;
}
#
# Check if location has our default stuff in it, and if so remove it
#
if ($params->{location_id}) {
    $qry = "Select * from merchant_affiliate_locations where id = ".$params->{location_id}.";";
    warn "setaddress.cgi: $qry \n";
    my $tdata = $db->prepare($qry);
    my $rv = $tdata->execute;
    my $tmp = '';
    while ($tmp = $tdata->fetchrow_hashref) {
        $url .= "$tmp->{country}/$tmp->{state}/$tmp->{city}/$tmp->{location_name_url}";
    }
    warn "setaddress.cgi: $url \n";
}
#
# redirect to the ipaddress page
#    print "Location:http://www.clubshop.com/maps/rewardsdirectory.shtml\n\n";
print $cgi->redirect(-URL=> $url);
