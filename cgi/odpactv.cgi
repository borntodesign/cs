#!/usr/bin/perl -w
# odpactv : One Downline Pool Activity creator
# We want to ensure the trigger of the Trial Partner Report viewed activity when coming in from an email link originating from HQ.
# The simplest way to do that is to include a parameter that only we will have... the members.oid being one
# That makes it so that if VIPs fabricate the URL and start clicking on them, it will not trigger the activity

use strict;
use lib '/home/httpd/cgi-lib';
use CGI;
use DB_Connect;

my $destination = 'http://www.clubshop.com/partner/index.html?id=';
my $cgi = new CGI;

# we are expecting 2 parameters: i for ID -and- o for oid
my $id = $cgi->param('i');
my $oid = $cgi->param('o');

# both parameters should be numeric values
$id = '' if $id =~ /\D/;
$oid = '' if $oid =~ /\D/;

if (! $id)
{
	print $cgi->redirect($destination);
	exit;
}
elsif (! $oid)
{
	print $cgi->redirect($destination . $id);
	exit;
}

my $db = DB_Connect::DB_Connect('generic') || exit;

# this insert will only succeed if the id and oid match the member record
$db->do('INSERT INTO activity_tracking (id, "class") SELECT m.id, 51::SMALLINT FROM members m JOIN od.pool ON od.pool.id=m.id WHERE id=? AND oid=?', undef, $id, $oid);
print $cgi->redirect($destination . $id);
$db->disconnect;
exit;
