#!/usr/bin/perl -w
###### TransferFunds.cgi
######
###### A script that allows one Partner to transfer funds to another Partner (this is the initial assumption)
######		using the pre-pay funding system (soa table).
######
###### started as a direct copy of pay4_others.cgi: 01/22/15	Bill MacArthur
###### deployed: 01/28/15
###### modified: 09/25/15	Bill MacArthur

use strict;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;
require XML::LibXML::Simple;
use Template;
use Encode;
use Data::Serializer;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use FTA;
binmode STDOUT, ":encoding(utf8)";

###### CONSTANTS
my $TESTMODE = 0;	# set to -1- to NOT commit anything... should be set to -0- for normal operation
my $minXfer = 25;	# per Dick Sept 2015: No transfers of less than $25.00 are permitted to be made to another Partner that is not in their downline.

my $SERIALIZER = Data::Serializer->new(
	'serializer'		=> 'Storable',
	'digester'   		=> 'MD5',
	'cipher'     		=> 'Blowfish',
	'secret'     		=> 'eojh574#@!',
	'serializer_token'	=> 0
);

my $cgiini = "pay4_others.cgi";
my $ttPath = '/home/clubshop.com/cgi-templates/TT/TransferFunds/';
my %TMPL =
(
	'content'	=> 10497,	# our XML language pack
	'start'		=> 'screen1.tt',
	'confirm'	=> 'screen2.tt',
	'transfer'	=> 'screen3.tt',
	'wrap'		=> 10962
);

my %TABLES =
(
	'soa'		=> 'soa',
	'members'	=> 'members',
	'cbc'		=> 'currency_by_country'
);

###### GLOBALS
my $db = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $TT = Template->new({'ABSOLUTE' => 1});
my $ME = $q->script_name;

(my $path = $q->path_info) =~ s#^/+##;
my ($action, $item_code)  = (split /\//, $path);	# See if the admin is trying to purchase.
$action ||= $q->param('action') || '';			# Get the action if it wasn't in the path.
my $SubmittedID = PreProcess_Input($q->param('id'));
my $Amount = PreProcess_Input($q->param('amount') || '');	# by defaulting to the empty string, we will trigger an error in CheckXferAmount()
my $CurrCode = $q->param('currency_code');

my ($member_id, $mem_info, @ActionList) = ();
my %Misc = ();	# for all those bits of data that we do not want to create another global var for, but that we want to make into the template
my %DEBUG = ();
my $access_type = '';
my $login_error = 0;
my %messages = ();
my $balance = {};

################################
###### MAIN starts here.
################################

# just comment the following line out to disable the ability to debug based upon the debug parameter... like when this is in production ;)
enableDebugging();

# Get the info for a logged in member.
Get_Cookie();	# this routine also connects to the DB if there is a legit login
my $fta = FTA->new($mem_info->{'id'}, $db);
my $lang_blocks = ParseLanguagePack();

if ( $access_type eq 'auth' )
{
	my $tmpl = 'start';
	$balance = Get_Balance();

	# if they have no balance, then they cannot do anything
	if ($balance->{'account_balance'} <= 0)
	{
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'no_balance'};
		Display_Page('start', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen1'}});
	}

	# If sent a list of IDs to confirm.
	if ( $action eq 'confirm' )
	{
		if (! $SubmittedID)
		{
			push @{$messages{'bad'}}, $lang_blocks->{'enter_id'};
			Display_Page('start', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen1'}});
		}
		elsif ((! CheckCurrency()) || ! CheckXferAmount())
		{
			Display_Page('start', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen1'}});
		}
		elsif (! Confirm_List())
		{
			Display_Page('start', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen1'}});
		} 

		Display_Page('confirm', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen2'}});
	}
	# If sent a list of IDs to pay.
	elsif ( $action eq 'transfer' )
	{
		Transfer();		# Process the list to be paid.
	}
	else
	{
		Display_Page('start', {'title'=>$lang_blocks->{'xfer_titles'}->{'screen1'}});
	}
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	Err(qq#Please log in <a href="$LOGIN_URL?destination=$ME">HERE</a> to use this form.#);
}

Exit();

################################
###### Subroutines starts here.
################################

=head3 CheckCurrency

Created only to be prepared for what may be down the road in offering the ability to transfer amounts that are not USD

=head4 Returns

Returns true on success.
Otherwise, sets a message in @{$messages{'bad'}} and returns false.

=cut

sub CheckCurrency
{
	if ($CurrCode ne 'USD')
	{
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'invalid_currency'};
		return undef;
	}
	
	return 1;
}

sub CheckInUpline
{
	my $ckid = shift;
	my ($rv) = $db->selectrow_array('SELECT upline FROM relationships WHERE id=? AND upline=?', undef, $mem_info->{'id'}, $ckid);
	return $rv;
}

=head3 CheckXferAmount

=head4 Returns

Returns true if the value is a positive number, false otherwise.

=cut

sub CheckXferAmount
{
	# we should have a numeric amount > 0

	my $a = $db->selectrow_array('SELECT isnumeric(?)', undef, $Amount);
	if (! $a)
	{
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'invalid_number'};
	}
	elsif ($Amount <= 0)
	{
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'invalid_amount'};
	}
	else
	{
		return 1;
	}
	
	return undef;
}

##### Builds a confirm list of the IDs submitted.
sub Confirm_List
{
	my $id = $SubmittedID;
	my $qry = "
		SELECT
			m.id, m.alias, m.firstname, m.lastname, m.membertype, m.vip_date, m.country, cbc.currency_code,
			mt.can_receive_xfer_funds,
			pa.id AS pay_agent_id
		FROM members m
		JOIN currency_by_country cbc
			ON cbc.country_code=m.country
		JOIN member_types mt
			ON mt.code=m.membertype
		LEFT JOIN pay_agent.pay_agents pa
			ON pa.id=m.id
			AND pa.active=TRUE
		WHERE ";
	
	my $data = ();

	# If we have a letter, it must be an alias.
	if ( $id =~ m/\D/ )
	{
		$id = uc($id);
		$data = $db->selectrow_hashref("$qry m.alias= ?", undef, $id);
	}
	# Otherwise, it's an ID.
	else
	{
		$data = $db->selectrow_hashref("$qry m.id= ?", undef, $id);
	}

	if ( $data )
	{
		$data->{'row_cost'} = '0.00';
		my $minXferApplies = 0;
		
		if ($mem_info->{'pay_agent_id'} && $data->{'pay_agent_id'})
		{
			push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'no_pa_xfer'};
		}
		# Check if in the downline
		elsif ($mem_info->{'id'} == $data->{'id'})
		{
			push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'no_self_xfer'};
		}
		else
		{
			if (! $gs->Check_in_downline($db, 'members', $mem_info->{'id'}, $data->{'id'}) )
			{
				$minXferApplies = 1;

				# 02/06/15 Dick wants them to be able to send funds upline as well
				if (CheckInUpline($data->{'id'}))
				{
					# do nothing, we are good
				}
				# can send crossline to partners in the same country
				elsif ($fta->country_authorized_crossline($data->{'country'}))	# as of 02/11/15 they need to be able to TPs, so let's just open it up entirely and let can_receive_transfer_funds take it from there
				{
					# do nothing
				}
				else
				{
					push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'not_dn_not_up'};
				}
			}
		}

		if (! $data->{'can_receive_xfer_funds'})
		{
			push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'invalid'};
		}

		if ($minXferApplies && $minXfer > $Amount)	# per Dick Sept 2015: No transfers of less than $25.00 are permitted to be made to another Partner that is not in their downline.
		{
			push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'minxfer_outside_downline'};
		}

		return undef if $messages{'bad'};
		return undef unless CheckXferAmount();
		$data->{'row_cost'} = sprintf('%.2f', $Amount);
	}
	else
	{
		$data->{'id'} = $id;
		$data->{'alias'} = $id;
		$data->{'row_cost'} = 0;
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'not_inline'};
		return undef;
	}
		
	$Misc{'running_cost'} = sprintf('%.2f', ($Misc{'running_cost'} += $data->{'row_cost'}));

	$data->{'running_cost'} = $Misc{'running_cost'};
	$data->{'keyval'} = CreateKeyVal($data->{'row_cost'}, $data);
	push @ActionList, $data;
#	die Dump(\@ActionList);
	return 1;
}

sub country_authorized_crossline
{
	my ($me, $ccode) = @_;
	my ($rv) = $db->selectrow_array('
		SELECT country_code FROM fee_transmission_agent_countries
		WHERE crossline_auth=TRUE AND country_code=? AND id= ?',
		undef,
		$ccode,
		$me);
	return $rv;
}

=head3 CreateKeyVal

=head4 Arguments

	The transfer amount
	A hashref of data related to the transfer recipient (currently the ID and alias)

=head4 Returns

	A string that will become the value for the associated control

=cut

sub CreateKeyVal
{
	my ($pricing, $data) = @_;
	my %package = (
		'amount' => $pricing,
		'id' => $data->{'id'},
		'alias' => $data->{'alias'},
		'firstname' => $data->{'firstname'},
		'lastname' => $data->{'lastname'},
		'xfer_ID' => $member_id
	);
	
	return $SERIALIZER->serialize(\%package);
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $pagename = shift;
	my $args = shift;
	my $tmpl = $ttPath . $TMPL{$pagename};	# that template should be a TT template
	my $wrap = $gs->Get_Object($db, $TMPL{'wrap'}) || die "Failed to load template: $TMPL{'wrap'}\n";
	
	my $data = {
		'SubmittedID' => $SubmittedID,
		'Amount' => $Amount,
		'lang_blocks' => $lang_blocks,
		'params' => {
			'action' => $action,
			'debug' => ($q->param('debug') || '')
		},
		'script_name' => $ME,
		'member' => $mem_info,
		'balance' => $balance,
		'messages' => \%messages,
		'ActionList' => \@ActionList,
		'misc' => \%Misc
	};
	
	# dynamically load certain data passed in
	foreach (qw/results payeeID/)
	{
		# don't clobber existing data structures
		die "data->{$_} already exists" if $data->{$_};
		$data->{$_} = $args->{$_} if $args->{$_};
	}

	if ($DEBUG{'data'})
	{
		print $q->header('-charset'=>'utf-8', '-type'=>'text/plain'), Dump($data);
	}
	else
	{
		print $q->header('-charset'=>'utf-8');
		my $content = ();
		my $rv = $TT->process($tmpl, $data, \$content);
		if ($rv)
		{
			$TT->process(\$wrap, {'content'=>$content, 'title'=>$args->{'title'}}) || print HandleError($TT->error, $TT->error); #'There has been an unexpected error processing the template'
		}
		else
		{
			print HandleError($TT->error, $TT->error);
		}
	}
	
	Exit();
}

sub Dump
{
	eval('require Data::Dumper;');
	return Data::Dumper::Dumper($_[0]);
}

sub enableDebugging
{
	my $points = $q->param('debug') || '';
	$DEBUG{$_} = 1 foreach split(/,/, $points);
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), "<br />$_[0]<br />";
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

=head3 ExtractKeyVal

	Deserialize the value received back on a parameter that was created by CreateKeyVal

=head4 Arguments

	A textual value
	
=head4 Returns

	A hash reference identical to that created by CreateKeyVal()
	
=cut

sub ExtractKeyVal
{
	return $SERIALIZER->deserialize($_[0]) if $_[0];
	return undef;
}

###### Get the pre-pay balance for the submitting VIP.
sub Get_Balance
{
	return $db->selectrow_hashref("
		SELECT id, account_balance, CASE WHEN transferrable_balance < 0 THEN 0 ELSE transferrable_balance END AS transferrable_balance
		FROM soa_account_balance_payable
		WHERE id = ?", undef, $mem_info->{'id'}) || 0;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# Get the member Login ID and info hash from the VIP cookie.
	( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if ( $member_id && $member_id !~ m/\D/ )
	{
		$access_type = 'auth';
		exit unless $db = DB_Connect($cgiini, (), {'enable_lockout'=>1});
	}
	else
	{
		print $q->redirect($LOGIN_URL);
		exit;
	}

	$db->{'AutoCommit'} = 0 if $TESTMODE;
	$db->{'RaiseError'} = 1;
	$mem_info = $db->selectrow_hashref("
		SELECT
			m.id, m.alias, m.firstname, m.lastname, m.country,
			cbc.currency_code,
			pa.id AS pay_agent_id
		FROM $TABLES{'members'} m
		JOIN $TABLES{'cbc'} cbc ON cbc.country_code=m.country
		LEFT JOIN pay_agent.pay_agents pa
			ON pa.id=m.id
			AND pa.active=TRUE
		WHERE m.id = ?",
		undef, $member_id);
}

sub HandleError
{
	my ($error, $return_text, $email_flag) = @_;

	if ($email_flag)
	{
		# Build the message
		my %mail = (
				'From' => 'pay4_others.cgi@clubshop.com',
				'To' => 'webmaster@dhs-club.com',
	       		'Subject'=> $return_text,
				'Content-Type'=> 'text/plain; charset="utf-8"',
				'Message' => $error
		);
	
		sendmail(%mail);
	}

	return $return_text
}

sub ParseLanguagePack
{
	my $content_xml = $gs->GetObject($TMPL{'content'}) || die "Failed to load XML template: $TMPL{'content'}\n";
	
	# it would be nice to parse the whole document in one swoop, but then when we insert HTML into the nodes, we would have problems with XMLin
	my $tmp = XML::LibXML::Simple::XMLin($content_xml, 'NoAttr'=>1);
	
	# it seems that TT doesn't work quite right with utf-8 characters and the utf-8 flag
#	_utf8_off($tmp);
	
	my %data = (
		'application_link' => $q->a({'-href'=>'https://www.clubshop.com/cgi/appx/upg', '-onclick'=>'window.open(this.href);return false;'}, 'https://www.clubshop.com/cgi/appx/upg')
	);
	
	return ParseLanguagePackKeys($tmp, \%data);
}

sub ParseLanguagePackKeys
{
	my ($tmp, $data) = @_;

	my %rv = ();

	foreach (keys %{$tmp})
	{
		if (ref $tmp->{$_} eq 'HASH')	# the XML has nested nodes and so some of the keys will be hashrefs
		{
			$rv{$_} = ParseLanguagePackKeys($tmp->{$_}, $data);
			next;
		}
		
		
		$TT->process(\$tmp->{$_}, $data, \$rv{$_});
	}
	return \%rv;
}

sub PreProcess_Input
{
	my $p = shift;
	return unless defined $p;
	
###### filter out leading & trailing whitespace
		$p =~ s/^\s*//;
		$p =~ s/\s*$//;

	return $p;		
}

sub Transfer
{
	$balance = Get_Balance();
	my $data = eval{ ExtractKeyVal($q->param('keyval')) };

	unless ($data && $data->{'id'})
	{
		push @{$messages{'bad'}}, 'There was an error processing keyval: invalid';
	}
	elsif ($data->{'amount'} > $balance->{'transferrable_balance'})
	{
		push @{$messages{'bad'}}, $lang_blocks->{'insufficient_transferrable_balance'};
	}
	elsif ($data->{'xfer_ID'} != $member_id)
	{
		push @{$messages{'bad'}}, 'There was an error processing keyval: The identity does not match yours.';
	}
	else
	{
		my ($trans_id) = $db->selectrow_array("
			SELECT create_ca2ca_xfer(
				?,
				?,
				15030,
				15031,
				?,
				?,
				?,
				?,
				?,
				NULL, NULL)
			", undef,
			$member_id,
			$data->{'id'},
			$data->{'amount'},
			"$lang_blocks->{'transferred2'} $data->{'alias'}",
			"$lang_blocks->{'transferred_from'} $mem_info->{'alias'}",
			"Transferred to: $data->{'alias'}",
			"Transferred from: $mem_info->{'alias'}");

		if ($trans_id)
		{
			$Misc{'data'} = $data;
		}
		else
		{
			push @{$messages{'bad'}}, $lang_blocks->{'caxfer_failed'} . $q->br . date();
		}
	}
	
	my $title = $lang_blocks->{'xfer_titles'}->{'screen3'};
	unless ($messages{'bad'})
	{
		$balance = Get_Balance();
		
	}
	else
	{
		$title = 'Error';
	}

	Display_Page('transfer', {'title'=>$title});
}

sub _utf8_off
{
        my $hr = shift;
        foreach (keys %{$hr})
        {
                if (ref $hr->{$_} eq 'HASH')
                {
                	_utf8_off($hr->{$_});
                }
                else
                {
                	Encode::_utf8_off($hr->{$_});
                }
        }
}

__END__

02/06/15	Change the account_balance_payable to be -0- if it is actually less than -0-
02/11/15	there have actually be scads of changes opening the gates ever further each time since initial deployment
			Today's change actually gives them the ability to transfer to anybody crossline in the registered countries
05/18/15	Added binmode to STDOUT
06/02/15	Made it so that a Pay Agent cannot transfer to another Pay Agent
09/25/15	Added the limitation of a minimum transfer amount outside of downline
