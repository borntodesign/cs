#!/usr/bin/perl -w
###### spec1.cgi
###### a special mailing list generator to get all the memberships for whom I am a Builder
# and who have NOT filled out a survey or made their 'initial' PP purchase
###### created: 09/17/08	Bill MacArthur
###### last modified:

$| = 1;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S qw($LOGIN_URL);
use DB_Connect;

###### GLOBALS
my $q = new CGI;
our $action = $q->param('action');
my $type = $q->param('type');

our ($db, @List, $message) = ();

our ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . ${\$q->script_name()} . "\">$LOGIN_URL</a>");
	exit;
}

###### if they are not a VIP, they don't have access to this interface
if ( $meminfo->{membertype} !~ 'v' )
{
	print 	$q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h3('This facility is for VIP members only'),
		$q->end_html();
		exit;
}

unless ($db = DB_Connect('spec1')){ exit }
$db->{RaiseError} = 1;
($meminfo->{oid}) = $db->selectrow_array('SELECT oid FROM members WHERE id=?',undef, $id) || die "Failed to obtain oid";

my %files = (
	no_survey => 'Have not taken survey',
	no_purchase => 'Have not made initial purchase'
);
my (%def_filename, %filename, %file_details) = ();
foreach (keys %files){
	$def_filename{$_} = "/home/httpd/html/vip/g/$meminfo->{oid}-$id-$_.txt";
	$filename{$_} = LoadWebFileName($_);
	$file_details{$_} = FileDetails($_);
}

unless ($action){
	Def_Header();
	Display();
}
elsif ($action eq 'no_survey'){
	GenerateFileScreen();
}
elsif ($action eq 'no_purchase'){
	GenerateFileScreen();
}
else { die "Unrecognized action: $action"; }
$db->disconnect;
exit;

sub Def_Header
{
	print $q->header,
	$q->start_html(
		-encoding=>'utf-8',
		-style=>{-code=>'
			body{
				background-color: #FFFFFF;
				width: 550px;
			}
			h2{
				font-family: Arial, Verdana, sans-serif;
				font-size: medium;
				color: #4E7526;
			}
			div {
				font-family: Verdana, Arial, sans-serif;
				font-size: smaller;
			}
			p {
				font-family: Verdana, Arial, sans-serif;
				font-size: smaller;
			}
			td {
				padding: 0.2em 0.5em;
				vertical-align:top;
				font-family: Verdana, Arial, sans-serif;
				font-size: small;
			}
			table.a{
				border:1px solid #6A9D33;
				background-color: #D2E9BA;
			}
		'}
	);
	print $q->h2('<img src="/images/cartworld-52x40.gif" width="52" height="40" alt="" />New Customer Report');
}

sub Display
{
	unless ($message){
		print '<p>The New Customer Report provides Builder Team Coaches a list of all Membership 
  types in their non-Builder Networks who have either not taken the ClubShop Mall 
  Survey, or have not made an initial purchase at the ClubShop Mall.</p>';
	}
	else
	{
		print $message;
	}

	if (grep $filename{$_}, keys %files){
		print '<div>';
		print "Files available for download:<br />";
		my $tblData = '';
		foreach (keys %files){
			$tblData .= DisplayFileDetails($_) if $filename{$_};
		}
		print $q->table({-class=>'a'}, $tblData) if $tblData;
		print '</div>';
	}

	print $q->p(
		'<img src="/images/survey_check.gif" width="43" height="24" border="0" alt="survey"/>' .
		$q->a({-href=>$q->url . "?action=no_survey"}, 'Click') . " to generate a new file: <b>Have not taken survey</b>"
	);
	print $q->p(
		'<img src="/images/tag_small.gif" width="43" height="24" border="0" alt="rewards_tag"/>' .
		$q->a({-href=>$q->url . "?action=no_purchase"}, 'Click') . " to generate a new file: <b>Have not made initial purchase</b>"
	);
	print $q->end_html;
}

sub DisplayFileDetails
{
	my $fn = shift;
	my $rv = '<tr><td>';
	$rv .= $q->a({-href=>$filename{$fn}, -onclick=>'window.open(this.href);return false;'}, $files{$fn});
	$rv .= '</td><td>';
	$rv .= "Generated on: $file_details{$fn}{stamp}<br />";
	$rv .= "File size: $file_details{$fn}{size}<br />";
	$rv .= "Line count: $file_details{$fn}{lines}";
	$rv .= '</td></tr>';
	return $rv;
}

sub FileDetails
{
	my $fn = shift;

	return undef unless $filename{$fn};
	my %rv = ();
	my @stats = stat($def_filename{$fn});

	$rv{stamp} = localtime( $stats[9] );
	$rv{size} = $stats[7];
	$rv{lines} = qx/wc -l $def_filename{$fn}/;
	$rv{lines} =~ s/(\d+).*/$1/g;	# wc -l produces this "0 12-no_purchase.txt", we only want the line count
	return \%rv;
}

sub GenerateFile
{
	my @output_def = qw/firstname lastname alias emailaddress mail_option_lvl membertype language_pref phone cellphone other_contact_info/;
	open (OUT, ">$def_filename{$action}") || die "Failed to open $def_filename{$action} for writing";
	print OUT qq|"First Name","Last Name","ID","Email Address","Email Type","Member Type","Language Preference","Telephone","Cell Phone","Skype Name"\n|;
	
	my $sth = $db->prepare(_base_qry());
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		my $line = '';
		$line .= '"' . (defined $tmp->{$_} ? $tmp->{$_} : '') . '",' foreach(@output_def);
		$line =~ s/,$/\n/;	# replace the trailing comma with a newline
		print OUT $line;
	}
	close OUT;
	$filename{$action} = LoadWebFileName($action);
	$file_details{$action} = FileDetails($action);
	return $filename{$action} ? 1 : 0;
}

sub GenerateFileScreen
{
	Def_Header();
	print $q->h4('Please wait while a new file is generated for you');
	my $rv = GenerateFile();
	if ($rv){
		$message .= $q->div('Success!');
	} else {
		$message = $q->p('Something went wrong. A new file could not be generated.<br />' .
			$q->a({-href=>$q->url}, 'Try it again'));
	}

	Display();
}

sub LoadWebFileName
{
	my $fn = shift;
	return (-e $def_filename{$fn}) ? "/vip/g/$meminfo->{oid}-$id-$fn.txt" : '';
}

sub _base_qry
{
	my $base_cols = 'm.id, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype,
		m.phone, m.cellphone, m.other_contact_info, m.mail_option_lvl, m.language_pref';
	my %joins = (
		no_survey => 'LEFT JOIN questionnaires_user_status qus ON qus.userid=outer_tmp.id
						WHERE qus.userid IS NULL',
		no_purchase => 'LEFT JOIN reward_point_incentives rpi ON rpi.id=outer_tmp.id
						WHERE rpi.id IS NULL'
	); 
	return <<"QUERY";
SELECT outer_tmp.*
FROM
(
	SELECT $base_cols
	FROM members m
	INNER JOIN my_upline_vip muv ON muv.id=m.id
	WHERE m.membertype IN ('m','s','v') AND muv.upline= $id

	UNION

	SELECT $base_cols
	FROM members m
	INNER JOIN my_upline_vip muv ON muv.id=m.id
	INNER JOIN
	(
		SELECT mr.*
		FROM my_upline_by_role mr
		LEFT JOIN nbteam_vw nbt on nbt.id=mr.id
		WHERE mr.upline= $id
		AND mr.upline_role=100 AND COALESCE(nbt.level,0)<100
	) AS tmp ON tmp.id=muv.upline
) AS outer_tmp
$joins{$action}
;
QUERY
}