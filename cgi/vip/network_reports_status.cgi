#!/usr/bin/perl -w
###### Program Name: network_reports_status.cgi
###### originally Written:	07/13/04	Karl Kohrt
###### heavily modified 03/21/13	Bill MacArthur
###### Shows a list of links to network reports that were requested via network_reports.cgi
###### and generated previously via the background process clubcontact_generator.pl
######
######	last modified: 03/13/15 Bill MacArthur

use XML::LibXML;
use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw($LOGIN_URL);
use DB_Connect;
use GI;
use XML::local_utils;

###### CONSTANTS
our $MEM_LEVELS = 'v';				# Which membertypes can access this script.
our $FILE_LOCATION = '/vip/g/';
my %TMPL = (
	'XML'		=> 10832	# this is shared with network_reports.cgi
);
my $network_reports_tbl = 'network_reports';

###### GLOBALS
my $q = new CGI;
$q->charset('utf-8');
my $gs = new G_S;
my $lu = XML::local_utils->new;
my $ME = $q->url;
my $GI = GI->new();
my ($db, $lang_blocks) = ();
our $member_id = '';
our $alias = '';
our $member_info = {};
#our $membertype = '';
our $login_error = 0;

################################
###### MAIN starts here.
################################

Get_Cookie();
Load_lang_blocks();

unless ( $login_error )
{
	my $action = $q->param('action') || '';

	# If no action was passed, just display the list of reports.
	if ( $action eq '' )
	{
		if ( my $report_list = Get_List() )
		{
			if ( scalar (keys %{$report_list}) > 0 )
			{
				# Build out the raw data for display.
				my $built_list = Build_Results( $report_list );
				
				# Display the list to the browser.
				Do_Page( $built_list );
			}
			else
			{
				# Display a no reports message.
				Err($lang_blocks->{'noreports'});
			}			
		}
		else
		{
			# Display a failure message.
			Err($lang_blocks->{'xretrieve'});
		}
	}
	elsif ( $action eq 'show_args' )
	{
		Show_Args();
	}
	else
	{
		Err('The action requested is not supported.');
	}

##### Use this if the checks are implemented in the beginning of the script.
#	}
#####	
	$db->disconnect;
}

exit;

################################
###### Subroutines start here.
################################

# Build out the results part of the report page.
sub Build_Results		
{
	my $list = shift;
	my $report = '';
	my $bg = '#ffffee';
	
	my @Sort = (	'pk',
			'report_name',
			'size',
			'submitted',
			'started',
			'ended');
	
	my $results .= $q->Tr({'-bgcolor'=>$bg}, $q->th(\@Sort), $q->th('criteria'));

	foreach $report ( sort {$b<=>$a} keys %{$list} )
	{
		###### alternate our row color
		$bg = ($bg eq '#ffffee') ? '#eeeeee' : '#ffffee';
		$results .= qq|<tr bgcolor="$bg">\n|;
		foreach (@Sort)
		{
			# If this is the report name field and there is a filename to link to, create a link.
			if ( $_ eq 'report_name' && $list->{$report}{'filename'} )
			{
				$list->{$report}{'report_name'} = $gs->Prepare_UTF8($list->{$report}{'report_name'}, 'encode');
				$list->{$report}{'report_name'} = qq|<a href="$FILE_LOCATION$list->{$report}{'filename'}" target="_blank">$list->{$report}{'report_name'}</a>|;
			}
			# If this is the size field and it has a value.
			elsif ($_ eq 'size' && defined $list->{$report}{'size'})
			{
				# If it is 0 bytes.
				if ( $list->{$report}{'size'} == 0)
				{
					$list->{$report}{'size'} = $lang_blocks->{'empty_see_criteria'};
				}
				# If it is marked -1, there was an error during execution.
				if ( $list->{$report}{'size'} == -1)
				{
					$list->{$report}{'size'} = 'DHS Processing Error';
				}
			}
			# Add the item's info to the report row.
			$results .= $q->td({'-class'=>'data'}, $list->{$report}{$_} || '&nbsp;');
		}
		# Add the criteria link to the report row.
		$results .= $q->td({'-class'=>'data'},
					$q->a({'-href'=>$ME . "?action=show_args&pk=" . $list->{$report}{'pk'},
						'-target'=>'_blank',
						'-width'=>100,
						'-height'=>100}, 'Click Here'));
	}
	$results = $q->table({'-border'=>1, '-cellpadding'=>2, '-cellspacing'=>2}, $results);
	return $results;
}

# Display the page to the browser.
sub Do_Page
{
	my $list = shift;

	$list = $q->div( $q->a({'-href'=>"network_reports.cgi"}, $lang_blocks->{'request_another'}) ) . $q->br . $list;

	$list .= $q->p($lang_blocks->{'x30days'});
	$list .= $q->br;
	print $q->header(),
		$GI->wrap({
			'title'=>"Network Reports List",
			'style'=>$GI->StyleLink('/css/cgi/clubcontact.css'),
			'script'=>'<script src="//www.clubshop.com/cgi/PSA/script/13" type="text/javascript"></script>'
#					<script src="/js/clubcontact.js" type="text/javascript"></script>',
			,'content'=>\$list
		});
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	my $mem_info = '';
	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		my $admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator');
		my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect('network_reports_status.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}

#		unless ( ($alias = $member_id = $q->param('id')) && ($membertype = $q->param('mtype')) )
		unless ( $alias = $member_id = $q->param('id') )
		{
			Err('Missing a required parameter');
			$login_error = 1;
		}
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect('network_reports_status.cgi');
		$db->{'RaiseError'} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id )
		{
			# Assign the info found in the VIP cookie.
			$alias = $mem_info->{'alias'};
#			$membertype = $mem_info->{'membertype'};					
		}
		# If we didn't get a member ID, we'll check for a ppreport cookie.
#		else
#		{
#			$member_id = G_S::PreProcess_Input($q->cookie('id')) || '';
#			$alias = $db->selectrow_array("SELECT alias
#								FROM 	members
#								WHERE id = $member_id") if $member_id;
#			# If we found an alias, they must be a member.
#			if ( $alias ) { $membertype = "m" };
#		}					
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			$login_error = 1;
		}
	}
}

###### Get the list of reports for the particular id.
sub Get_List
{
	my $list = $db->selectall_hashref("SELECT pk,
							id,
							report_name,
							filename,
							submitted::timestamp(0),
							started::timestamp(0),
							ended::timestamp(0),
							accessed::timestamp(0),
							arg_string,
							size     
						FROM $network_reports_tbl
						WHERE id = $member_id",
						'pk');
	return $list;
}

###### Prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), $_[0];
	Exit();
}

sub Load_lang_blocks
{
	my $xml = $gs->Get_Object( $db, $TMPL{'XML'} ) || die "Failed to load lang_blocks\n";
	$lang_blocks = $lu->flatXMLtoHashref($xml);
	$gs->Prepare_UTF8($lang_blocks, 'encode');
}
								
###### Redirect the member to the login page. 
sub RedirectLogin
{
	print $q->redirect( $LOGIN_URL . "?destination=" . $ME );
}

###### Display the arguments that were used to create a specific report.
sub Show_Args
{
	my $pk = $q->param('pk');
	my $login_prompt = qq#You may need to <a href="#. $LOGIN_URL . "?destination=" . $ME . qq#">login here</a>.#;
	if ( $pk =~ /\D/ || $member_id =~ /\D/ )
	{ 
		Err("The primary key(pk) and member id must be numeric." . $login_prompt);
	}
	else
	{
		my @arg_string = $db->selectrow_array("	SELECT arg_string
								FROM 	$network_reports_tbl
								WHERE 	pk = ?
								AND 	id = $member_id", undef, $pk);
		if ( ! defined $arg_string[0] )
		{ 
			Err("The primary key(pk) and member id doesn't appear to be in the reports table: undef" . $q->br . $login_prompt);
		}
		elsif ( $arg_string[0] eq '' )
		{ 
			Err("The primary key(pk) and member id doesn't appear to be in the reports table: empty" . $q->br . $login_prompt);
		}
		elsif ( $arg_string[0] !~ m/<root/) # it's a previous version
		{
			Show_Args_retired($arg_string[0]);
			return;
		}
		else 	
		{		
			my $dom = XML::LibXML->new->parse_string( $arg_string[0] );
			my $startID = ($dom->findnodes('/root/start_id'))[0]->textContent;

			my $tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			my $report = $q->h4($lang_blocks->{'following_criteria'});
			$report .= $q->p("<strong>$lang_blocks->{'startingid'}</strong> $tab $startID");
			
			$report .= "<h4>$lang_blocks->{'idsx'}</h4><ul>";
			
			foreach my $item ($dom->findnodes('/root/xids/*'))
			{
				$report .= $q->li($item->textContent);
			}
			
			$report .= '</ul>' . $q->p("<strong>$lang_blocks->{'baserpt'}</strong> $tab " . ($dom->findnodes('/root/tables/base_function'))[0]->getAttribute('description'));
			
			$report .= "<h4>$lang_blocks->{'columns_requested'}</h4><ul>";
			
			foreach my $item ($dom->findnodes('/root/columns/*'))
			{
				$report .= $q->li($item->getAttribute('description'));
			}
			
			$report .= "</ul><h4>$lang_blocks->{'filters_applied'}</h4><ul>";
			
			foreach my $item ($dom->findnodes('/root/criteria/*'))
			{
				$report .= $q->li($item->getAttribute('description'));
			}
			
			print $q->header('-expires'=>'now'), $q->start_html, "$report</ul>", $q->end_html;
		}						
	}
}

sub Show_Args_retired
{

	my $login_prompt = qq#You may need to <a href="#
				. $LOGIN_URL . "?destination=" . $ME
				. qq#">login here</a>.#;
	my @arg_string = shift;
	
			# Split out the arguments to be parsed later.
			my @arguments = eval $arg_string[0];
			my $id = $arguments[0];
			my $OUTPUT_ARGS = $arguments[1];
			my $FILTER_ARGS = $arguments[2];
			my $TABLE_ARGS = $arguments[3];

			my $tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			my $report = "Your legacy request used the following criteria:<br><br>";
			$report .= "Starting ID:<br>$tab$id";
			$report .= "<br>Columns requested:<br>";
			foreach ( @$OUTPUT_ARGS ) { $report .= "$tab$_<br>"; }
			$report .= "<br>Filters Applied:<br>";
			foreach ( @$FILTER_ARGS ) { $report .= "$tab$_->{'column'} $_->{'operator'} $_->{'value'} \($_->{'exclude'}\) <br>"; }
			print $q->header('-expires'=>'now'), $report;

}

###### 07/22/04 Added the action and subroutine to show criteria used to create a report.
###### 12/28/04 Added the 30-day removal note and the login prompt with Show_Args errors.
###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login. Added query to get alias value.
###### 07/18/12 added a javascript tag for the GPS ACL system in Do_Page()
# 03/13/15 added the charset for CGI

__END__

This is a bogus example of a version 1 XML configuration

<root version="1"><start_id>4773425</start_id>
<xids><id>4773425</id></xids>
<columns><column description="First Name">firstname</column>
<column description="Last Name">lastname</column>
<column description="ID">alias</column>
<column description="Email Address">emailaddress</column>
<column description="Email Type">mail_option_lvl</column>
<column description="Member Type">membertype</column>
<column description="Pay Level">a_level_stable.a_level</column>
<column description="Personal Pay Points">refcomp.ppp</column>
<column description="ClubCash Balance">rp_summary_vw.balance</column>
<column description="Country">country</column>
<column description="Language Preference">language_pref</column>
</columns>
<criteria><item description="Country in list: AF,DZ" name="country">AF,DZ</item>
<item description="Member Type in list: Member, Affiliate, Partner, Affinity Group" name="membertype">s,m,v,ag</item>
<item description="Pay level greater than TP" name="pay_level"><op>TRUE</op><pay_level>2</pay_level></item>
<item description="Language preference in list: af,sq,hy" name="language_pref">af,sq,hy</item>
<item description="Good email addresses only:" name="mail_option_lvl"><op>&gt;</op><value>0</value></item>
</criteria>
<tables><table>rp_summary_vw</table><table>refcomp</table><table>a_level_stable</table>
<base_function description="Personal">network.network_reports_personal</base_function>
</tables></root>