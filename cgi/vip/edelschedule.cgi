#!/usr/bin/perl -w
###### Email Deletion Schedule 
###### Written by Stephen Martin 11/14/2002
###### last modified: 06/03/05	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use POSIX qw(locale_h);
use locale;
use Time::JulianDay;

setlocale( LC_CTYPE, "en_US.ISO8859-1" );

use lib ('/home/httpd/cgi-lib');
use G_S; ###### HOLDS OUR GLOBAL CONFIGURATION VARIABLES
use DB_Connect;

$| = 1;
our $q = new CGI;

###### GLOBALS

our($db);                        ###### global DB handle

my $id       = $q->param('id');
my $schedule = $q->param('schedule');
my $deletion = $q->param('deletion');
my $action   = $q->param('action') || '';

my $SCH_BEGIN;
my $SCH_END;
my $SCH;
my $DEL;
my $IDS;
my $message;
my $m1;
my $m2;
my $m3;
my $m4;
my $m5;
my $self = $q->url();

unless ( $db = DB_Connect::DB_Connect('edelschedule.cgi') ) { exit }

my $TMPL = G_S::Get_Object( $db, 10049 ) || die "Couldn't retrieve the template object 10049.";

###### Create Default values 

$m1 = "No scheduled deletions.";
$m2 = "The following ID\'s are scheduled for deletion";
$m3 = "The following ID\'s were deleted ";
$m4 = "No deletions took place on this date.";
$m5 = "No scheduled deletions today.";

$SCH_BEGIN = get_date();
#$SCH_END   = get_scheduled_runs();
$SCH   = get_scheduled_runs();

#$SCH = gen_options( $SCH_BEGIN, $SCH_END );

$DEL = get_processed_runs();

# if ( !$DEL )
# {
#     my $hdate =
#       substr( $SCH_BEGIN, 5, 2 ) . "/"
#       . substr( $SCH_BEGIN, 8, 2 ) . "/"
#       . substr( $SCH_BEGIN, 0, 4 );
#     $DEL = <<EOD;
# <option value="$SCH_BEGIN">$hdate</option>
# EOD
# }

###### Catch request to display IDS for a range of dates.

if ( $action eq "SYS_PROCESS_LIST" && !$id )
{

    unless ( $schedule || $deletion )
	{
        PErr("Missing Input Parameter<br>Please selected a date from the drop down");
        goto 'END';
	}

    if ( $schedule && $deletion )
	{
        PErr("Only one Date may be selected");
        goto 'END';
    }

    if ($schedule) {
        $IDS = gen_grid($schedule, 'N');

        if ( ! $IDS ) {
            $message = $m1;
        }
        else {
            $message = $m2 . " on " . $schedule;
        }

    }
    else {
        $IDS = gen_grid($deletion, 'Y');

        if ( !$IDS ) {
            $message = $m4;
        }
        else {
            $message = $m3 . " on " . $deletion;
        }
    }
}
elsif ( $action eq "SYS_PROCESS_LIST" && $id )
{
    unless ($id) {
        Err("Missing Input Parameter ID");
        goto 'END';
    }

    $id =~ s/\D//g;

    $IDS = find_id($id);
    if ( !$IDS ) {
        $message = "No ID found.";
    }
    else {
        $message = "ID found.";
    }

}
else
{
    $IDS = gen_grid($SCH_BEGIN, 'N');

    if ( !$IDS ) {
        $message = $m5;
    }
    else {
        $message = $m2;
    }

}

$TMPL =~ s/%%SCHEDULE%%/$SCH/g;
$TMPL =~ s/%%DELETION%%/$DEL/g;
$TMPL =~ s/%%IDS%%/$IDS/g;
$TMPL =~ s/%%MSG%%/$message/g;
$TMPL =~ s/%%SELF%%/$self/g;

print $q->header(), $TMPL;

END:
$db->disconnect;
exit;

###### ###### ###### ###### ######

sub PErr
{ 
    print $q->header( -expires => 'now' );
    print $q->start_html( -bgcolor => '#ffffff' );
    print "<font color=\"\#330099\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><strong>\n";
    print $q->h4( $_[0] );
    print "</font></strong>\n";
    print $q->end_html();
}

sub Err
{
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' ),
      'There following error has occurred!<br><br>';
    print $q->h4( $_[0] ), scalar localtime(), ' (PST)';
    print $q->end_html();
}

# sub get_scheduled_runs
# {
#     my ( $sth, $rv, $qry, $data );
# 
#     $qry = "SELECT DISTINCT 
#          coalesce(MAX(expires),now()) as expires
#         FROM
#          bad_email_addr
#         WHERE expires >= now()
# 	AND processed='N'";
# 
#     $sth = $db->prepare($qry);
# 
#     $rv = $sth->execute;
#     defined $rv or die $sth->errstr;
# 
#     $data = $sth->fetchrow_hashref;
# 
#     $rv = $sth->finish();
# 
#     return $data->{expires};
# }

sub get_processed_runs
{
    my ( $sth, $rv, $qry, $data );
    my $html;
    my $hdate;

    $qry = "SELECT DISTINCT 
         coalesce(expires, now())::DATE as expires 
        FROM
         bad_email_addr
        WHERE processed='Y'
	--AND expires <= now()";

    $sth = $db->prepare($qry);

    $rv = $sth->execute;
    defined $rv or die $sth->errstr;

    $html = "";

    while ( $data = $sth->fetchrow_hashref )
	{
        $hdate =
          substr( $data->{expires}, 5, 2 ) . "/"
          . substr( $data->{expires}, 8, 2 ) . "/"
          . substr( $data->{expires}, 0, 4 );
        $html = $html . "<option value=\'$hdate\'>$data->{expires}</option>";
    }

    $rv = $sth->finish();

    return $html;

}

sub get_scheduled_runs
{
    my ( $sth, $rv, $qry, $data );
    my $html;
    my $hdate;

    $qry = "SELECT DISTINCT 
         coalesce(expires, now())::DATE as expires 
        FROM
         bad_email_addr
        WHERE processed='N'
	--AND	expires => now()";

    $sth = $db->prepare($qry);

    $rv = $sth->execute;
    defined $rv or die $sth->errstr;

    $html = "";

    while ( $data = $sth->fetchrow_hashref )
	{
        $hdate =
          substr( $data->{expires}, 5, 2 ) . "/"
          . substr( $data->{expires}, 8, 2 ) . "/"
          . substr( $data->{expires}, 0, 4 );
        $html = $html . "<option value=\'$hdate\'>$data->{expires}</option>";
    }

    $rv = $sth->finish();

    return $html;

}

sub get_date {
    my $mday;
    my $mon;
    my $year;

    ( $mday, $mon, $year ) = (localtime)[ 3, 4, 5 ];

    $mon++;

    $year = $year + 1900;

    return $year . "-"
      . sprintf( "%02d", $mon ) . "-"
      . sprintf( "%02d", $mday );

}

sub gen_options
{
    my ($SCH_BEGIN) = @_;
    shift;
    my ($SCH_END) = @_;

    my $mdate;
    my $hdate;
    my $html;

    my $a = julian_day(
        substr( $SCH_BEGIN, 0, 4 ),
        substr( $SCH_BEGIN, 5, 2 ),
        substr( $SCH_BEGIN, 8, 2 )
    );
    my $b = julian_day(
        substr( $SCH_END, 0, 4 ),
        substr( $SCH_END, 5, 2 ),
        substr( $SCH_END, 8, 2 )
    );

    $html = "";

    foreach ( $a .. $b ) {
        ( my $y, my $m, my $d ) = inverse_julian_day($_);
        $mdate = $y . "-" . $m . "-" . $d;
        $hdate = $m . "/" . $d . "/" . $y;

        $html = $html . <<EOH;
<option value="$mdate">$hdate</option>
EOH
    }

    return $html;

}

sub gen_grid
{
   
    my $now = shift;
	my $processed = shift || 'N';

    my ( $sth, $rv, $qry, $d1, $d2, $d3, $d4 );

    my $html;

    open (STDERR, ">/dev/null") or die "can't redirect STDERR: $!\n";

    $qry = "SELECT 
             bad_email_addr.id as id,
             members.alias     as alias
           FROM 
            bad_email_addr 
           LEFT JOIN members  ON  bad_email_addr.id = members.id
           WHERE 
            bad_email_addr.expires = ? AND processed= '$processed'
           ORDER BY 
            bad_email_addr.id ASC";

#print "<b>Debug now = $now</b><br>\n";
#print "<br>\n";
#print "qry          = $qry<br>\n";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($now);
    defined $rv or die $sth->errstr;

    $html = "";

    while ( $d1 = $sth->fetchrow_hashref ) {
        $d2 = $sth->fetchrow_hashref;
        $d3 = $sth->fetchrow_hashref;
        $d4 = $sth->fetchrow_hashref;

        $html = $html . qq~
<tr>
 <td nowrap bgcolor="#FFCCD0">$d1->{id}</td>
 <td nowrap>$d2->{id}</td>
 <td nowrap bgcolor="#FFCCD0">$d3->{id}</td>
 <td nowrap>$d4->{id}</td>
</tr>
\n~
    }
    
    $rv = $sth->finish();

    close(STDERR);

    return $html;
}

sub find_id {
    my ($id) = @_;

    my ( $sth, $rv, $qry, $data );

    my $html;

    $html = "";

    $qry = "SELECT
          bad_email_addr.id as id,
          bad_email_addr.expires as expires,
          members.alias     as alias
         FROM
          bad_email_addr
         LEFT JOIN members  ON  bad_email_addr.id = members.id
         WHERE
            bad_email_addr.id = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);
    defined $rv or die $sth->errstr;

    $data = $sth->fetchrow_hashref;

    $rv = $sth->finish();

    if ( $data->{id} ) {
        $html = qq~
<tr>
 <td nowrap bgcolor="#FFCCD0" colspan="4">
  $data->{alias} deletion selected for $data->{expires}
 </td>
</tr>
\n~;
    }
    else {
        $html = qq~
<tr>
 <td nowrap bgcolor="#FFCCD0" colspan="4">
  $data->{alias} NOT found.
 </td>
</tr>
\n~;
    }

 return $html;

}

###### 02/10/04 enabled the -w switch and went to the 'use' style for modules
###### cleaned up the code just a tad
###### 02/11/04 made the list of available dates just that, dates, instead of timestamps
###### 06/03/05 made the lists really look at the 'processed' state

