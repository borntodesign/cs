#!/usr/bin/perl -w
################################################################
# unpaid_vips.cgi
#
# This is generates a report of unpaid VIPs.
# 
# Deployed: 06/08
# Author: Keith Hasely
# Version: 0.1
# last modified: 04/04/12	Bill MacArthur
################################################################

################################################################
# Includes
################################################################s
use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw($LOGIN_URL);
use DB_Connect;
use Switch;
use XML::local_utils;

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
# GLOBALS
our $q           = CGI->new;
our $ME          = $q->url;
our $db          = undef;
our $member_id   = '';
our $member_info = { id => '', membertype => '' };
our $gs = new G_S;

# LOCALS
my $step = defined($q->param('step'))? $q->param('step'):'';
my $xml_doc = '';
my $xsl = '';
my $content_xml;
my $xml = '';
my $param = {};
################################################################
# Subroutine(s)
################################################################
###############################
# Check to see if the user is
# logged in, if they are
# return their userid.
#
# Param		userdata	hash	
# Return	int
###############################
sub loginCheck
{
#	my $role = {};
	my $error_message = '';
	($member_id, $member_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') ); 
	
	
	if ($member_id && $member_id !~ /\D/ && $member_info->{membertype} eq 'v')
	{
#		$role = $db->selectrow_hashref("SELECT level FROM nbteam_vw WHERE id= ?", undef, $member_id);
		
		return '';# if $role;
	}
	
	
	if (!$member_id)
	{
		$error_message = 'You must be logged in to do that.';
		print $q->redirect( $LOGIN_URL . "?destination=" .$ME );
		return qq(<error>not_logged_in</error>);
	} else {
		$error_message = 'You must be a VIP to view this report.';

		return qq(<error>not_vip</error>);
	}
}

###############################
# Display the form to submit
#
# param		int	time_period
# return	None
###############################
sub displayResults
{
	my $args = shift || {};
	my $where_time_period = '';
#	my $role = {};
	my $query = '';
	my $sth = {};
	my $results_xml = '';
	
#	$role = $db->selectrow_hashref("SELECT level FROM nbteam_vw WHERE id= ?", undef, $member_id);
	
	
	switch ($args->{time_period}) 
	{
		case 1 {$where_time_period = " (s.end_date < nop_seed.paid_thru AND (s.end_date + interval '1 month') > nop_seed.paid_thru)
			OR (s.end_date > nop_seed.paid_thru AND s.void=TRUE) ";}
		case 2 {$where_time_period = " (s.end_date + interval '1 month') < nop_seed.paid_thru ";}
		else {$where_time_period = " s.end_date < nop_seed.paid_thru OR s.void=TRUE ";}
	}
	
	$query = "
SELECT 
	m.id, m.alias, m.firstname, m.lastname, m.emailaddress
FROM 
	nop_seed, members m
	INNER JOIN subscriptions s  
	ON s.subscription_type IN (1,3)
	AND s.member_id=m.id
WHERE 
	$where_time_period
	AND m.id IN my_team(?)
ORDER BY 1
	";

die $query;
	$sth = $db->prepare($query);
	
#	$sth->execute($member_id,$member_id,$role->{level});
	$sth->execute($member_id);
	
	while (my $row = $sth->fetchrow_hashref)
	{
		$results_xml .= q(<member>);
		
		foreach my $keys (sort keys %{$row})
		{
			$results_xml .= ($row->{$keys})?"<$keys>$row->{$keys}</$keys>":"<$keys />";
		}
		
		$results_xml .= q(</member>);
		
	}
	
	
	return qq(<data>$results_xml</data>\n<time_period>$args->{time_period}</time_period>);
	
	
}
################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db ||= DB_Connect('Unpaid_VIPs');
exit unless $db;
$db->{RaiseError} = 1;
$db->{pg_enable_utf8} = 1;

#Check to see if this member has permission to view this report.
warn 'ck login';
$xml_doc = loginCheck();
if(!$xml_doc)
{
	$param = defined($q->param('time_period'))? {time_period=>$q->param('time_period')}: {time_period=>0};
    $xml_doc .= displayResults($param); 
}

$xsl = $gs->Get_Object($db, 10601) || 
		die "$@\nFailed to load XSL template: $xsl\n";

$content_xml = $gs->Get_Object($db, 10602) || 
		die "Failed to load XML template: 10602\n";

###############################
# Combine the applicatble
# pieces of the XML document.
###############################
$xml ||=  	qq(<base>\n) .
			qq(<script_name>$ME</script_name>) .
			$content_xml .
			$xml_doc .
			qq(\n</base>);

print $q->header(-charset=>'UTF-8');
		
print XML::local_utils::xslt($xsl, $xml);

$db->disconnect if defined $db;
exit;

# 08/07/08 revised the paid_thru SQL evaluations from using the mop to using subscriptions