#!/usr/bin/perl -w
###### tree.cgi
###### generate the genealogy 'Tree' reports based upon the rspid network (referral sponsorship)
###### initially created: 08/9/05	Bill MacArthur
###### since the original tree.cgi actually did this job, but was repurposed, this script, released 08/13, has a number of differences to show different forms of data
###### the original tree.cgi lived on Glocal Income whereas we are porting this one over to Clubshop
###### last modified: 07/15/15	Bill MacArthur

# 07/15/15 custom hacks put into place to allow 4773425 and 3887940 unlimited downline access per Dick

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw($EXEC_BASE $LOGIN_URL);
use XML::local_utils;
use XML::Simple;
use XML::LibXSLT;
no warnings 'recursion';
binmode STDOUT, ":encoding(utf8)";
#use Data::Dumper;	# for testing only

my $MaxPageVIPs = 2000; # the maximum number of Partners we will display

# these tables are used in Details() and in the main Looper query
my %TABLES = (
	'grp' => {
		'refcomp' => 'refcomp',
		'relationships' => 'relationships',
		'a_level' => 'a_level_stable',
		'coach' => 'my_coach'
	},
	'tp' => {
		'refcomp' => 'refcomp',
		'relationships' => 'tp_relationships',
		'a_level' => 'a_level_stable',
		'coach' => 'my_coach'
	}
);

my %TABLEJOINS = (
	'grp' => "
		JOIN grp.grp_complete gcc
			ON gcc.id=r.id
			AND gcc.upline= ?
		
		LEFT JOIN grp.grp_credit gc	/* we do a straight join so as to automatically exclude those for whom we are not receiving GRP credit, paid or unpaid */
			ON gc.id=r.id
			AND gc.upline= ?
		
		LEFT JOIN mviews.distinct_tp_relationships_partner_upline hastps
			ON hastps.id=r.id

		LEFT JOIN pgpp
			ON pgpp.id=r.id
			
		LEFT JOIN mviews.my_rspid_prvs mprvs
			ON mprvs.id=r.id
	",
	
	'tp' => "
		JOIN my_upline_vip
			ON my_upline_vip.id=r.id
			
		LEFT JOIN od.pool_vw
			ON od.pool_vw.id=r.id
			
		LEFT JOIN member_ranking_summary mrs
			ON mrs.member_id=r.id
	
	/* the following join is necessary to determine if my coach is somebody for whom I get grp credit, which is necessary for access checks in ShowEmail() */
		LEFT JOIN grp.grp_credit gc
			ON gc.id=my_upline_vip.upline
			AND gc.upline= ?
	"
);

my %XTRACOLS = (
	'grp' => '
			, COALESCE(pgpp.personal_gpp,0) AS personal_gpp
			, gcc.id AS pot_grp_credit
			, gc.id AS grp_credit
			, hastps.id AS prtp_flag
			, COALESCE(mprvs.prvs, rc.prvs, 0) AS prvs',
			
	'tp' => ", date_fmt_by_country(od.pool_vw.deadline, ?) AS deadline
			, mrs.ranking_summary AS prospect_rank
			, mrs.last_action_time::DATE AS ranking_last_action_time
			, gc.id AS grp_credit
			, COALESCE(od.pool_vw.pnsid::TEXT,'') AS od_pool_pnsid
			, my_upline_vip.upline AS my_upline_partner
			, COALESCE(rc.prvs,0) AS prvs"
);

my %TMPL = (
	'grp' => {
		'horizXSL'	=> 11028,
		'horiz2vertXSL'	=> 10468,
		'base'		=> 11029,	# this is the final layout document
		'lang_blocks'	=> 10473
	},
	'tp' => {
		'horizXSL'	=> 11091,
		'horiz2vertXSL'	=> 10468,
		'base'		=> 11090,	# this is the final layout document
		'lang_blocks'	=> 10473
	},
	'rptwin' => {
		'1' => 10476,	# the legacy layout doc for the report pop-up window
		'2' => 11062,	# the layout doc for the menu called from the Trial Partner report by a new Partner (less links than the regular one they would get from the Group report)
		'3' => 11093	# the layout doc for the menu called from the Partner version of the Trial Partner group report
	}
);

# we'll count the number of loops while creating our result set
my $Count = 0;

# for now we'll simply place any errors in this var (make sure it's valid xhtml)
# if present, an <errors> node will be created in the root
my $errors = '';

my $data_xml = '<root>';
my (%Filters, %cookieParts, $reportee, $slabels, %Coaches, $db, %GRPstopsCache, @CoacheesCoachees, $ADMIN, $rptwinParam) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});

my $xmlu = new XML::local_utils;
my $xs = new XML::Simple;

my @path = split('/', $q->path_info);
my $rpttype = $path[1] || 'grp';	# other values are 'tp' and 'team'
$rpttype =~ s/\s*//;				# get rid of whitespace to try and alleviate what appears to be some caller errors where a bogus path is submitted

if ($rpttype eq 'team')
{
#	Err('Team reports are no longer available');
	print $q->redirect('https://www.clubshop.com/p/');
	Exit();
}
elsif ($rpttype ne 'tp')
{
	$rpttype = 'grp';
}

my $script_name = $q->script_name;

my $xml_param = $q->param('xml') || '';	# values for this are "vert", "horiz", "vip", "coaches", "xml"

my $BREAKLOOP = 0;
my $MAXROWS = 0;
my @c4IDs = ();		# the one's for whom I am technically the "coach";

if ($rptwinParam = $q->param('rptwin'))
{
	# now validate it
	die "Invalid rptwin param" unless grep $rptwinParam eq $_, keys %{$TMPL{'rptwin'}};
}

# this query should contain the simple membership data we want to display up front
# more expensive queries that would involve left joins here are implemented
# as explicit direct queries in a sub-routine below
# the rough cost as of 02/13 for this query is 630
# cost went up to 823 after adding the relationships again as rcoach

my $qry = "
	SELECT
		m.id,
		m.spid AS rspid,
		m.alias,
		m.firstname ||' '|| m.lastname AS name,
		COALESCE(m.emailaddress,'') AS emailaddress,	-- pools do not have one
		COALESCE(m.country,'') AS country,			-- pools do not have one
		m.membertype
		, COALESCE(rc.ppp,0) AS ppp
--		, rc.prvs
		, COALESCE(rc.prp_pp,0) AS prp_pp
		, rc.sales_req_count AS execs
		, COALESCE(rc.opp,0) AS tpp
		, COALESCE(rc.plpp,0) AS pmpp
		, COALESCE(rc.op,0) AS grp
		, COALESCE(rc.grp_pp,0) AS grp_pp
		, COALESCE(rc.prv_handicap,0) AS grp_nop
		, COALESCE(rc.a_level, 0) AS level
		, COALESCE(als.a_level, 0) AS stable_a_level
		, COALESCE(ra.a_level, 0) AS last_month_a_level
		, rcoach.upline AS coach_id
		, rc.sv_grp
		, rc.sv_grp_pp
		, mdx.cnt AS downline_exec_cnt
		$XTRACOLS{$rpttype}

	FROM $TABLES{$rpttype}->{'relationships'} r
	
	JOIN members m
		ON m.id=r.id

	JOIN $TABLES{$rpttype}->{'coach'} rcoach
		ON r.id=rcoach.id

	$TABLEJOINS{$rpttype}

	LEFT JOIN $TABLES{$rpttype}->{'refcomp'} rc
		ON rc.id=m.id

	LEFT JOIN $TABLES{$rpttype}->{'a_level'} als
		ON als.id=m.id

	LEFT JOIN reference.a_level ra
		ON ra.id=m.id

	LEFT JOIN mviews.my_downline_exec_cnt mdx
		ON mdx.id=m.id
			
	WHERE r.next=TRUE
	AND r.upline= ?
	ORDER BY rc.a_level DESC NULLS LAST, id ASC";

#die $qry;
###### error handling provided by LoginCK
exit unless my $meminfo = LoginCK();

$db = DB_Connect::DB_Connect('tree.cgi') || exit;
$db->{'RaiseError'} = 1;

($meminfo->{'pay_level'}) = $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= $meminfo->{'id'}") || 0;
($meminfo->{'country'}) = $db->selectrow_array("SELECT country FROM members WHERE id= $meminfo->{'id'}") || 'US';
($meminfo->{'seq'}, $meminfo->{'pnsid'}) = $db->selectrow_array("SELECT seq, pnsid FROM mviews.positions WHERE id= $meminfo->{'id'}");

#TrackUsage();	# move this to somewhere else in the processing chain so that it is more selective in tracking.... there has been a lot of change to this script since this was setup

###### jump out of the regular processing here if we are just populating the pop-up
if ($rptwinParam)
{
	RptWin();
	Exit();
}

LoadSubscriptionLabels();

my $rid = $gs->PreProcess_Input( uc($q->param('id') || '') );

###### let's get details
###### if we don't need any more than basic cookie stuff,
###### we can change this to only look up details when it's not the user
$reportee = Details($rid || $meminfo->{'id'});

if (! $reportee->{'id'})
{
	Err("We are sorry, but the reportee cannot be identified");
}
# per Dick 02/13/14: Is there anyway we can can redirect all new Partners to see their TP Report when they try and look at their Team Report ? http://www.clubshop.com/cgi/vip/tree.cgi/team
elsif ($reportee->{'od_pool_pnsid'} && $rpttype eq 'team')	# they are in the Trial Partner line
{
	print $q->redirect("http://www.clubshop.com/cgi/TrialPartner?id=$reportee->{'id'}");
	Exit();
}

###### get our report preferences
LoadFilters();

###### $reportee is now a hashref (as long as we found somebody :> )
# the validator will take care of error handling
if ( ValidateReportee() )
{
	Do_Report($reportee);
}

Exit();

###### ######

sub AddCoachCache
{
	my $cid = shift;
	return unless $cid;
	unless ($Coaches{ $cid})
	{
		$Coaches{ $cid } = $db->selectrow_hashref("
			SELECT alias, emailaddress, (firstname||' '||lastname) AS name FROM members WHERE id= $cid");
	}
}

sub BreakLoop
{
	return 1 if $BREAKLOOP;
	$Count++;
	# even a 500 count page is grossly unmanageable
	if ($Count > $MaxPageVIPs)
	{
		$BREAKLOOP = 1;
		my $msg = "<i>This report is truncated and may not display properly.</i><br />
			The number of Partners displayed would exceed $MaxPageVIPs.";
		$errors .= $q->h5($msg);
		return 1;
	}
	return 0;
}

sub Build_Data_Node
{
	my $rv = qq|<script_name description="the rpptype is automatically appended to the actual URL">$script_name/$rpttype</script_name><script_name_bare>$script_name</script_name_bare>\n|;
#	$rv .= "<data>\n";
#	$rv .= "<stop_role>$Filters{'stop_role'}</stop_role>\n";
#	$rv .= "</data>\n";
	return $rv;
}

sub CoachXML
{
	my $rv = '<coaches>';
	foreach my $c (keys %Coaches)
	{
		$rv .= qq|<coach cid="$c" alias="$Coaches{$c}->{'alias'}" emailaddress="$Coaches{$c}->{'emailaddress'}" name="$Coaches{$c}->{'name'}" />|;
	}
	return $rv . '</coaches>';
}

sub Details
{
	###### get whatever data is desired for this party
	my $id = shift;
	return {} unless $id;

	if ($id =~ m/\D/)
	{
		($id) = $db->selectrow_array('SELECT id FROM members WHERE alias= ?', undef, $id) || return;
	}
	
	my $qry = "
		WITH per AS (
			SELECT get_working_period(1,NOW()) AS period
		),
		mypgpp AS (
			SELECT ta_live.id, SUM(ta_live.fpp) AS personal_gpp
			FROM ta_live
			JOIN per ON per.period=ta_live.period
			WHERE ta_live.id= $id
			GROUP BY ta_live.id
		)
		SELECT
			m.id,
			m.alias,
			m.spid AS rspid,
			m.membertype,				/* a number of these COALESCE's are because of member pools which do not have the regular membership data... and we want to avoid uninit'd var errors */
--			COALESCE(suffix,'') AS suffix,
			m.firstname ||' '|| m.lastname AS name,
			COALESCE(m.emailaddress,'') AS emailaddress,
			COALESCE(m.address1,'') AS address1,
			COALESCE(m.address2,'') AS address2,
			COALESCE(m.city,'') AS city,
			COALESCE(m.state,'') AS state,
			COALESCE(m.country,'') AS country,
			COALESCE(m.postalcode,'') AS postalcode,
			COALESCE(m.phone,'') AS phone
			, COALESCE(rc.ppp,0) AS ppp
			, COALESCE(mprvs.prvs,rc.prvs,0) AS prvs
			, COALESCE(rc.prp_pp,0) AS prp_pp
			, COALESCE(rc.sales_req_count,0) AS execs
			, COALESCE(rc.opp,0) AS tpp
			, COALESCE(rc.plpp,0) AS pmpp
			, COALESCE(rc.op,0) AS grp
			, COALESCE(rc.grp_pp,0) AS grp_pp
			, COALESCE(rc.prv_handicap,0) AS grp_nop
			, COALESCE(rc.a_level, 0) AS a_level
			, COALESCE(rc.a_level, 0) AS level		-- rather than just change the above colname and end up breaking things, we will create another version for use where 'level' is expected
			, COALESCE(mypgpp.personal_gpp,0) AS personal_gpp
			, COALESCE(ra.a_level, 0) AS last_month_a_level
			, COALESCE(rcoach.upline::TEXT, '') AS coach_id		-- this can be NULL if the coach is nspid and not in the rspid upline
			, COALESCE(als.a_level, 0) AS stable_a_level
			, COALESCE(rc.sv_grp::TEXT, '') AS sv_grp
			, COALESCE(rc.sv_grp_pp::TEXT, '') AS sv_grp_pp
			, COALESCE(mdx.cnt,0) AS downline_exec_cnt
			, COALESCE(od.pool_vw.pnsid::TEXT,'') AS od_pool_pnsid
			, COALESCE(date_fmt_by_country(od.pool_vw.deadline, '$meminfo->{'country'}'), '') AS deadline
			, COALESCE(mrs.ranking_summary::TEXT,'') AS prospect_rank
			, COALESCE(date_fmt_by_country(mrs.last_action_time, '$meminfo->{'country'}'), '') AS ranking_last_action_time
			, COALESCE(hastps.id::TEXT, '') AS prtp_flag
			, mviews.positions.seq
			, mviews.positions.pnsid
			
		FROM members m
		
		JOIN mviews.positions
			ON mviews.positions.id=m.id

		LEFT JOIN $TABLES{$rpttype}->{'coach'} rcoach
			ON m.id=rcoach.id
			
		LEFT JOIN mypgpp
			ON mypgpp.id=m.id

		LEFT JOIN $TABLES{$rpttype}->{'refcomp'} rc
			ON rc.id=m.id

		LEFT JOIN reference.a_level ra
			ON ra.id=m.id

		LEFT JOIN a_level_stable als
			ON als.id=m.id

		LEFT JOIN mviews.my_downline_exec_cnt mdx
			ON mdx.id=m.id
			
		LEFT JOIN od.pool_vw
			ON od.pool_vw.id=m.id

		LEFT JOIN member_ranking_summary mrs
			ON mrs.member_id=m.id
			
		LEFT JOIN mviews.distinct_tp_relationships_partner_upline hastps
			ON hastps.id=m.id
		
		LEFT JOIN mviews.my_rspid_prvs mprvs
			ON mprvs.id=m.id
			
		WHERE m.id= $id";
	my $rv = $db->selectrow_hashref($qry) || return;

	if ($rv->{'membertype'} eq 'v')
	{
		($rv->{'stype'}, $rv->{'paid_thru'}) = $db->selectrow_array("SELECT subscription_type, paid FROM partner_subscription_monthly WHERE member_id= $rv->{'id'}");
	}

	return $rv;
}

sub Display
{
	if ($xml_param eq 'vert')
	{
		EmitXML($data_xml);
		return;
	}

	my $xml = '<base>';
	$xml .= CoachXML();
	my $lng = $gs->GetObject($TMPL{$rpttype}->{'lang_blocks'}) || die "Failed to load lang_blocks XML\n";
	$xml .= $lng;
	$xml .= $xs->XMLout($meminfo, 'RootName'=>'user', 'NoAttr'=>1);

	$xml .= $xs->XMLout($reportee, 'RootName'=>'reportee', 'NoAttr'=>1);
	$xml .= $slabels;

###### include the other bits of data we need
	$xml .= Build_Data_Node();

###### don't display the 'table' tree data if we are showing the other XML parts	
	$xml .= ($data_xml || '') if (! $xml_param) || $xml_param eq 'xml';

# include our errors node here if there are any
	$xml .= "<errors>$errors</errors>\n" if $errors;

	$xml .= '</base>';
	if ($xml_param eq 'xml')
	{
		EmitXML($xml);
	}
	else
	{
		my $xsl = $gs->GetObject($TMPL{$rpttype}->{'base'}) || die "Failed to load base\n";
		$xml = $xmlu->xslt($xsl, $xml);
		$xml = $gs->Prepare_UTF8($xml);	# re-enabled 05/08/15
		print $q->header(
			'-type'=>'text/html',
			'-charset'=>'utf-8'
		), $xml;
	}
}

sub Do_Report
{
	my $sth = $db->prepare('SELECT id FROM relationships WHERE coach=TRUE AND upline=?');
	$sth->execute($reportee->{'id'});
	while (my ($id) = $sth->fetchrow_array)
	{
		push @c4IDs, $id;
	}

	# create a temp table of personal GPP data for use by the SQL utilized by Looper()
	# only needed by the grp report
	if ($rpttype eq 'grp')
	{
		$db->do("
			WITH per AS (
				SELECT get_working_period(1,NOW()) AS period
			)
			SELECT ta_live.id, SUM(ta_live.fpp) AS personal_gpp
			INTO TEMP TABLE pgpp
			FROM ta_live
			JOIN per ON per.period=ta_live.period
			GROUP BY ta_live.id
		");
	}

	if ($rpttype eq 'team')
	{
		my $tmp = $_[0];
		while ($tmp->{'id'})
		{
			$tmp = Looper($tmp, 0);
			warn "back from Looper with: $tmp id: $tmp->{'id'}";
			$MAXROWS++;
		}
	}
	else
	{
		Looper( $_[0], -1 );
	}
	
	###### at this point global $data_xml contains raw VIP downline data

	$data_xml .= "<maxrows>$MAXROWS</maxrows>$slabels";
	if ($xml_param eq 'vip')
	{
		EmitXML($data_xml . '</root>');
		return;
	}

	if ($xml_param eq 'coaches')
	{
		EmitXML(CoachXML());
		return;
	}
	else
	{
		$data_xml .= CoachXML();
	}

	my $lng = $gs->GetObject($TMPL{$rpttype}->{'lang_blocks'}) || die "Failed to load lang_blocks XML\n";
	
	###### include the other bits of data we need
	$data_xml .= Build_Data_Node();

	$data_xml .= '<admin>1</admin>' if $ADMIN;
	$data_xml .= $lng . '</root>';

	###### convert to a horizontal Table
	my $xsl = $gs->GetObject($TMPL{$rpttype}->{'horizXSL'}) || die "Failed to load horizXSL\n";
	$data_xml = "<root><maxrows>$MAXROWS</maxrows>" . $gs->Prepare_UTF8($xmlu->xslt($xsl, $data_xml)) . ($ADMIN ? '<admin>1</admin>' : '') . '</root>';

	if ($xml_param eq 'horiz')
	{
		EmitXML($data_xml);
		return;
	}

	###### convert horizontal Table to a vertical Table
	$xsl = $gs->GetObject($TMPL{$rpttype}->{'horiz2vertXSL'}) || die "Failed to load horiz2vertXSL\n";
	$data_xml = '<tree-view>' . $gs->Prepare_UTF8($xmlu->xslt($xsl, $data_xml)) . '</tree-view>';

	Display();
}

sub EmitXML
{
	print $q->header('-type'=>'text/plain', '-charset'=>'utf-8'), $_[0];
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();

	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetPNSDetails
{
	my $v = shift;
	($v->{'pns_pid'}) = $db->selectrow_array("SELECT pid FROM network.partner_network_separators WHERE pnsid= $v->{'id'}");
	($v->{'pns_fl_id'}) = $db->selectrow_array("SELECT id FROM network.spids WHERE spid= $v->{'id'}");
}

sub GetPNSstats
{
	my $v = shift;
	($v->{'count_tp'}, $v->{'count_v'}) = $db->selectrow_array("SELECT tp, v FROM od.pool_counts WHERE id= $v->{'id'}");
}

# the caller expects a true value if we are flagging the condition
sub GRP_StopsHere
{
	return undef unless $rpttype eq 'grp';	# if we are not doing a GRP report, then we will not proceed with the following test
											# in other words, we will continue drilling for a Team report, at least as far as this function is concerned
											
	LoadGRP_StopsHere_Cache() unless $GRPstopsCache{'loaded'};
	
	my $v = shift;
	return (grep {$v->{'id'} == $_} @{ $GRPstopsCache{'cache'} }) ? 1 : undef;
}

sub LoadFilters
{
	parseCookieParts();

	###### we are not using an actual user defined level filter at this time,
	###### but in case we do, we'll assign it here
	# if we are displaying a Team Report and the viewer is less than Exec, we will drill past Execs, otherwise they will probably not be able to Jump to that section that would normally closed
	# if we are an Exec, then stopping at Execs is desirable as the reports can quickly become very unwieldy

	$Filters{'stop_level'} = $cookieParts{'stop_level'} || (($meminfo->{'pay_level'} < 11 && $rpttype eq 'team') ? 30 : 11);

	$Filters{'stop_role'} = '';
}

sub LoadGRP_StopsHere_Cache
{
	$GRPstopsCache{'loaded'} = 1;	# we need this flag as there could absolutely be an empty cache, where one gets GRP credit for everyone in their organization
	my $sth = $db->prepare('SELECT g.id FROM grp.grp_stops_here g WHERE g.upline= ?');
	$sth->execute($reportee->{'id'});
	while (my $id = $sth->fetchrow_array)
	{
		push @{ $GRPstopsCache{'cache'} }, $id;
	}
	
	$GRPstopsCache{'cache'} ||= [];
}

sub LoadSubscriptionLabels
{
	my $data = $gs->select_array_of_hashrefs("SELECT subscription_type, label FROM subscription_types WHERE sub_class='VM'");
	$slabels = "<slabels>\n";
	foreach (@{$data})
	{
		$slabels .= qq|<label subscription_type="$_->{'subscription_type'}">$_->{'label'}</label>\n|;
	}
	$slabels .= "</slabels>\n";
}

sub LoginCK
{
	my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	
###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ($id && $id !~ /\D/)
	{
		$ADMIN = 1 if $id == 1;
		return $meminfo;
	}

###### if they are not a Partner or Staff, they don't have access to the report
	if ( $meminfo && (ref $meminfo eq 'HASH') && ! grep {$_ eq $meminfo->{'membertype'}} ('v', 'staff') )
	{
		Err('These Reports are for Partners only'),
		return;
	}

	print $q->redirect("$LOGIN_URL?destination=$script_name/$rpttype");

	return;
}

sub Looper
{
	my $v = shift;
	
	$v->{'showemail'} = ShowEmail($v);
	$v->{'showlinks'} = ($v->{'membertype'} eq 'mp' || $v->{'membertype'} eq 'pns') ? 1 : $v->{'showemail'};
	my $rowcnt = shift;

	my $xclude = 0;
	
	if ($rpttype eq 'team' && $v->{'id'} == $reportee->{'id'})	# for team reports, we do not want the reportee as the first partner in the list
	{
		$xclude = 1;
	}
	elsif ($rpttype ne 'tp')	# for Trial Partner reports, we should only be pulling in those we want and not have to exclude any <fingers crossed>
	{
		# the first pass thru should be our reportee, who we do not want to exclude if he is still in the TP line
		$xclude = 1 if $v->{'od_pool_pnsid'} && ($reportee->{'id'} != $v->{'id'}) &&  ($v->{'membertype'} eq 'v' || $v->{'membertype'} eq 'mp');
	}
	
	unless ($xclude)
	{
		$rowcnt++;
		WriteXML($v);
		$data_xml .= "</vip>\n" if $rpttype eq 'team';	# we do not want nested parties for a team report as the "depth" of the XML 
	}
#	$rowcnt++ unless $xclude;
#	WriteXML($v) unless $xclude;	# omit these parties as they are still in the trial partner line

	AddCoachCache($v->{'coach_id'});
	
	###### if we are filtering something, or we are going to stop, this is where we will do it
	unless (StopCK($v) || BreakLoop())
	{
		my $sth = $db->prepare($qry);

		if ($rpttype eq 'tp')
		{
			$sth->execute($meminfo->{'country'}, $meminfo->{'id'}, $v->{'id'});
		}
		if ($rpttype eq 'team')
		{
			$sth->execute($v->{'id'});
		}
		else
		{
			# the first argument in the execute constrains the join on grp_credit... so we are going to use the viewer's ID instead of the reportee's ID
			$sth->execute($meminfo->{'id'}, $meminfo->{'id'}, $v->{'id'});
		}

		while (my $tmp = $sth->fetchrow_hashref)
		{
#			$gs->Prepare_UTF8($tmp, 'decode');
#			warn Dumper($_);
			if ($rpttype eq 'team')
			{
				# unfortunately the bottom partner just above the pns will have two rows returned
				# we do not want the one down in the TP line, just the pns
				next if $tmp->{'od_pool_pnsid'} && $tmp->{'membertype'} eq 'v';
				return $tmp;
			}
			else
			{
				Looper($tmp, $rowcnt);
			}
		}
	}
	
	return undef if $rpttype eq 'team';
	
	$data_xml .= "</vip>\n" unless $xclude;
	$MAXROWS = $rowcnt if $rowcnt > $MAXROWS;
}

sub parseCookieParts
{
	my $ck = $q->cookie('viptree') || return;
	my @list = split(/\|/, $ck);
	foreach my $part (@list)
	{
		my ($p1, $p2) = split(/=/, $part);
		$cookieParts{$p1} = $p2 || '';
	}
}

sub RptWin
{
	###### create the content for our little pop-up window with a report list
	###### we should have received a 'rid', Report ID
	
	$rid = $gs->PreProcess_Input( $q->param('rid') );
	unless ($rid)
	{
		Err('Missing rid parameter');
	}
	elsif ( $rid =~ /\D/ or length $rid > 10 )
	{
		Err("Invalid rid: $rid");
	}
	else
	{
		$reportee = Details($rid);
		return unless ValidateReportee();
		
		GetPNSDetails($reportee) if $reportee->{'membertype'} eq 'pns';	# that routine will create some new keys on the hashref
		
		# we could be calling this list outside of a tree report.... like from a Trial Partner report for the taproot Member Pool
		# in that case, we only want the member list drop-down and not the report type switcher
		# so we are going to expect a parameter that we will pass to the template so it can do what is needed, since we are only expecting one certain value, we will only pass that
		my $filter = $q->param('filter') || '';
		$filter = '' unless $filter eq 'member_lists_only';

		my $xml = '<base>';
		$xml .= $gs->GetObject($TMPL{$rpttype}->{'lang_blocks'}) || die "Failed to load $TMPL{'lang_blocks'}\n";

		$xml .= $xs->XMLout($reportee, 'RootName'=>'reportee', 'NoAttr'=>1);
		$xml .= $xs->XMLout($meminfo, 'RootName'=>'user', 'NoAttr'=>1);
		$xml .= '<admin>1</admin>' if $ADMIN;
		
		$xml .= "<rpttype>$rpttype</rpttype><script_name>$script_name</script_name><filter>$filter</filter></base>";
			
		if ($xml_param)
		{
			EmitXML($xml);
		}
		else
		{
			my $xsl = $gs->GetObject($TMPL{'rptwin'}->{ $rptwinParam} ) || die "Failed to load XSLT\n";
			$xml = $xmlu->xslt($xsl, $xml);
			print $q->header('-type'=>'text/html', '-charset'=>'utf-8'), $xml;
		}
	}
}

# create a flag to determine if the reportee will be able to see the email address of the particular downline
sub ShowEmail
{
	my $dn = shift;
	return 0 if $dn->{'membertype'} eq 'mp';	# no bother for a pool
	return 1 if $dn->{'id'} == $reportee->{'id'} && $reportee->{'id'} == $meminfo->{'id'};	# if I am looking at my own report, we are good
	return 1 if $meminfo->{'id'} == 3127951 || $meminfo->{'id'} == 1;
	return 1 if $meminfo->{'id'} == $dn->{'coach_id'};
	my $_reportee = shift || $reportee;
	
	# we are called as the first item to hit Looper(), in which case we need to reference the user;
	return ShowEmail($dn, Details($meminfo->{'id'})) if $dn->{'id'} == $_reportee->{'id'} && $_reportee->{'id'} != $meminfo->{'id'};
	
	# for GRP reports, if the GRP stops at this person, we will not show the links
	return 0 if GRP_StopsHere($dn);
	
	# if we are getting grp credit, then we should be able to see it
	return 1 if $dn->{'grp_credit'};
	
	# this is to address Trial Partners under a Partner
	return 1 if ($dn->{'my_upline_partner'} || 0) == $reportee->{'id'};
	
	# for GRP reports, our straight join against the grp_complete table ensures that we at least potentially have GRP credit if things get paid up
#	return 1 if $rpttype eq 'grp';
	
	return 1 if $dn->{'pot_grp_credit'};

	return 0; 
}

# create a flag to determine if the reportee will be able to see the links of the particular downline
sub ShowLinks
{
	my $dn = shift;
	return 1 if $dn->{'membertype'} eq 'mp';	# no bother for a pool
	return ShowEmail($dn);
}

sub StopCK
{
	my $v = shift;
	###### we'll ignore anything on our reportee
	return undef if $v->{'id'} == $reportee->{'id'};

	###### we will return true if we don't want to proceed past this party
	return 1 if (
		$v->{'stable_a_level'} >= $Filters{'stop_level'} ||
		GRP_StopsHere($v)
	);
	return undef;
}

sub TrackUsage
{
        $db->do("INSERT INTO activity_tracking (id, class) VALUES ($meminfo->{'id'}, 32)");
        return;
}

sub ValidateReportee
{
	unless ($reportee)
	{
	###### we can implement a better kind of error handling later
	###### this is just a catcher at this point
		Err("We are sorry, but $rid cannot be identified");
	}
	elsif ($reportee->{'id'} == 1)
	{
		# for some reason not worth analyzing, this thing goes into a long graveyard spiral trying to produce a report for 01
		# and since there is no real reason to do so, it must be a mistake
		Err("A report cannot be produced for 01");
	}
	elsif ($reportee->{'id'} == $meminfo->{'id'})
	{
		return 1;
	}
	elsif ($meminfo->{'id'} == 3127951 || $meminfo->{'id'} == 1)	# Dick or 01 can do anything
	{
		# in order for the report to work more normally, we will make believe that the viewer is the actual reportee
		$meminfo = $reportee;
		return 1;
	}
	elsif ($reportee->{'membertype'} eq 'mp' || $reportee->{'membertype'} eq 'pns')
	{
		# as of August 2013, every Partner can access the menu for a Pool as the nspid_frontline report is filtered to only show them memberships in the GRP network
		return 1;
	}
	# new hack per Dick 07/15/15 Bill - can you provide Bouzid and Shili the ability to access all records for the Partners in their Group, even the ones below Execs or other Coaches, just like you allow 01 to do the same?
	# well we can't do that as it would give 'em access to the entire network, not just below their group, so we will do this instead
	elsif ($db->selectrow_array("SELECT network.access_hack1($reportee->{'id'}, $meminfo->{'id'})"))
	{
		# in order for the report to work more normally, we will make believe that the viewer is the actual reportee
		$meminfo = $reportee;
		return 1;
	}

	# for ordinary Partner access we use one type of access ck, but for pools we are more restrictive
	# per Dick 04/09/13: Create "Frontline Reports" for each Pool and allow the RD+ upline from it to be able to access it.
	my $func = 'network.access_ck1';
	$func = 'network.access_ck_pools' if $reportee->{'membertype'} eq 'mp';
	
	my ($rv) = $db->selectrow_array("SELECT $func($reportee->{'id'}, $meminfo->{'id'})");
	if ($rv)
	{
		return 1;
	}
		# 04/30/13 Dick: Meanwhile, can you allow Execs with the ability to see downline Exec's Team Reports?
		# 10/04/13 Dick: Bill - we are seeing the need for upline Execs to be able to show the Team Reports in their Biz Opp Presentations and Webinars.
		# So let's modify the rule to allow 1 Star+ Execs to be able to access downline Exec's Team Reports, okay?
		# 12/02/13 (in reference to Thouraya not being able to see past Bouzid... she was only an Exec for Novermber)
		# Dick: Bill - I thought we corrected this so that upline Execs could access their downline Exec's Team Reports?
#	elsif ($reportee->{'stable_a_level'} >= 11 && $meminfo->{'pay_level'} >= 11)
#	{
#		# we will at least make sure that the reportee is in the downline of the viewer
#		my ($rv) = $db->selectrow_array("SELECT id FROM network.relationships WHERE id= $reportee->{'id'} AND upline= $meminfo->{'id'}");
#		return $rv;
#	}

#	Err("$rid could not be found in your organization");
	Err("Your pay level or coaching position are insufficient to gain access to this information.");
	return undef;
}

sub WriteXML
{
	my $v = shift;
	###### first we have to look up our details
	###### it is cheaper SQL wise to do it this way than with left joins

	($v->{'stype'}, $v->{'paid_thru'}) = $db->selectrow_array("SELECT subscription_type, paid FROM partner_subscription_monthly WHERE member_id= $v->{'id'}");
	($v->{'nops'}) = $db->selectrow_array("SELECT nops FROM mviews.my_rspid_nops WHERE id= $v->{'id'}");

	foreach (qw/stype paid_thru nops/)
	{
		# although we omit these below, the keys are still created undef at times and cause errors when the XML is created on the reportee and meminfo
		$v->{$_} = '' unless defined $v->{$_};
	}
	
	# call us paid thru if we are a member pool
	$v->{'paid_thru'} = 1 if $v->{'membertype'} eq 'mp';
	
	# get the pool ID under me if I am a Partner Network Separator
	# also get the party immediately down in the nspid line
	# and get the counts stats
	if ($v->{'membertype'} eq 'pns')
	{
		GetPNSDetails($v);
		GetPNSstats($v);
	}
	
	$data_xml .= qq|<vip membertype="$v->{'membertype'}"|;
	$data_xml .= qq# nops="$v->{'nops'}"# if $v->{'nops'};
	$data_xml .= q| prtp_flag="1"| if $v->{'prtp_flag'};
	$data_xml .= qq| my_upline_partner="$v->{'my_upline_partner'}"| if $v->{'my_upline_partner'};
	
	# this section assigns empty values to the attribute nodes if there is no true value
	foreach (qw/name id rspid alias emailaddress stype country coach_id grp_credit/)
	{
		$data_xml .= qq# $_="${\($v->{$_} || '')}"#;
	}
	
	# this section assigns -0- values to the attribute nodes if there is no true value
	foreach (qw/level tpp pmpp execs personal_gpp paid_thru last_month_a_level prvs prp_pp showemail showlinks grp grp_pp grp_nop prospect_rank/)
	{
		$data_xml .= qq# $_="${\($v->{$_} || 0)}"#;
	}
	
	# this section create attribute nodes if there is a true value
	foreach (qw/od_pool_pnsid pns_pid count_tp count_v deadline ranking_last_action_time sv_grp sv_grp_pp/)
	{
		$data_xml .= qq# $_="$v->{$_}"# if $v->{$_};
	}
	
	if ($v->{'rspid'} == $reportee->{'id'}) 
	{
		$data_xml .= qq# myrspid="1"#;
	}

	if (grep $_ == $v->{'id'}, @c4IDs)
	{
		$data_xml .= qq# coach="1"#;
	}
	
	if ($v->{'membertype'} eq 'mp' || $v->{'membertype'} eq 'pns')
	{
		$data_xml .= qq# pool="1"#;
	}
	
	if (GRP_StopsHere($v))
	{
		$data_xml .= qq# grp_stops="1"#;
	}
	
	$data_xml .= qq# downline_exec_cnt="$v->{'downline_exec_cnt'}"# if $v->{'downline_exec_cnt'} && $rpttype eq 'grp';	# we do not need this for team or tp reports
	
	$data_xml .= ">\n";
}

__END__

###### 08/23/05 added the RptWin routine
###### 08/29/05 added extra contact info in the Details routine
# 08/30/05 added several COALESCE's to the detail routine to get rid of unit'd var errors
###### 09/12/05 added role data gathering for the rptwin routine
# 11/16/05 tweaked to display <25 pp as normal if the party is in fact 'paid thru'
# 04/03/07 made the default filter to stop at the builder role instead of the highest role
# 04/04/07 various revisions to to make it easier to clean up the html of extraneous empty tr tags using the xslt
# the templates were revised substantially, so they cannot be used with the previous version of this script
###### 11/09/07 revised some SQL in WriteXML to correct for VIPs who are not in network_calcs
# 08/07/08 revised WriteXML as far as gathering network_calcs and paid_thru data
# 08/22/08 added the cancellation status into the basic member detail mix
###### 12/03/08 a new implementation of the cancellation status
###### plus moved several other SQL chunks into the main query as the left joins work fine now
###### Oct/Nov 2011 various tweaks, most notably that an Exec cannot peek below another Exec
###### 01/03/12 made this, which was a temp version the live version, whilst ripping out the old pool and role stuff
###### 04/03/12 removed now unneeded columns from the various queries
###### 04/09/12 we take the PPP out, we put the PPP in, we .... to the tune of "The Hokey Pokey"
###### 04/11/12 added "qpp" to the mix of PP values pulled in
###### 11/26/12 some signiicant changes involving the new PP designators
###### 12/14/12 added the last_month_a_level to the mix... this also entailed blending the language pack with the initial data XML for use in rendering the initial table
###### this was changed over to TNT 2.0 for Feb. 2013 release
###### 02/12/13 finished integration of the team coach data for each tree Partner
###### 02/19/13 added in the flags for showing the email address and links for downline parties
###### 03/25/13 tweaked the ShowEmail routine to use the stable_a_level for evaluating the reportee
###### 04/09/13 add a flag into the XML to indicate a pool
###### 06/18/13 added the leadership_stamp to the mix
###### 10/24/13 added the ablity to pass a fixed parameter value to the rptwin template to control it's output
###### 10/29/13 making it so that new Partners do not appear in this report until they come out of the Trial Partner lines
###### 11/04/13 latest revision rolled into production
###### 12/02/13 further tweakage in ValidateReportee to allow Execs to see past Execs
###### 02/13/14 implemented a redirect on the team report when the reportee is a Trial Partner
###### 02/21/14 due to the complexity of implementing so many logic branches for so many items in the rptwin for different scenarios, even for the same ID,
###### we are going to provide the flexibility of selecting the template based upon the actual value of the rptwin parameter
###### 07/21/14 tweaked out Looper() to not exclude our reportee from the XML even if he is still in the TP line
###### 08/07/14 removed the leadership_stamp from the mix... let's see, that lasted just a little more than a year ;-(
###### 11/07/14 added more coalesce's to the Details() SQL in order to try silencing uninit'd var errors
###### 01/29/15 revisions in Looper() and Do_Report() to reorganize the vip xml into a straight line instead of nested when the report is a team report
######			this is to mitigate the recursion depth warnings thrown by XSL... the horiz xsl was changed to use this revised approach
04/10/15
	Removed team report functionality
05/08/15
	Dealt with some utf8 issues that came up after updating DBD::Pg
05/21/15
	Wove mviews.my_rspid_prvs into the mix for providing PRV counts instead of using "refcomp" as the value in there is changing to reflect PRVs one is getting credit for
05/22/15
	Tweaks to the assignment process of $rpttype to try and mitigate what appear invalid values received
06/01/15
	Error handling tweaks to deal with the inability to find the reportee