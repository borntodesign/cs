#!/usr/bin/perl -w
###### coop_replace.cgi
######
###### A script that allows the VIPs to replace removed Co-Op members  
###### via the order queue (coop_orders table). 
######
###### created: 05/12/05	Karl Kohrt
######
###### modified: 05/05/15	Bill MacArthur
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser);

###### CONSTANTS
our $default_status = 'p';	# The default order status. p=pending

###### GLOBALS
my $q = new CGI;
$q->charset('utf-8');
our $gs = new G_S;
our $cgiini = "coop_replace.cgi";
our $ME = $q->script_name;
our $xs = new XML::Simple(noAttr=>1);
our %TMPL = (	'form'		=> 10445,	# The replacement form xsl.
		'content'	=> 10446,	# The replacement form content xml.
		'config'	=> 10409	# The donor accounts and other coop config xml.
		);
our $db = '';
our $member_id = '';
our $mem_info = '';
our $alias = '';
our $replace_id = '';	# The replacement id internal to the script.
our $access_type = '';
our $max_days = '';		# The maximum time since the original transfer pulled from the xml content file.
our $login_error = 0;
my (@errors, @messages) = ();
our $xml = '';
our $config_xml = '';
our $params_xml = '';
our $params_hash = ();
our $donor_list = '';
my $replacement = ();	# this will become a hashref of our party that is being replaced

################################
###### MAIN starts here.
################################

# Get the info for a logged in member.
Get_Cookie();	

# If they are logged in as a VIP or an Admin.
if ( $access_type eq 'auth' || $access_type eq 'admin' )
{
	our $action = $q->param('action');
	# Put the cookie's member id and alias in the hash if we don't already have one.
	$params_hash->{id} ||= $member_id;
	$params_hash->{alias} ||= $mem_info->{alias};

	# Get the config file with the max days and donor list info.
	$config_xml = $gs->Get_Object($db, $TMPL{config}) || die "Failed to load XML template: $TMPL{config}\n";
	
	# Get the maxdays value. We need this value early for display purposes.
	($max_days = $config_xml) =~ s/^(.*?)replace_maxdays>|<\/replace_maxdays(.*?)$//gs;
	
	# Get the donor list since we're working with this file already.
	($donor_list = $config_xml) =~ s/^(.*?)donor_accounts>|<\/donor_accounts(.*?)$//gs;

	# Unless we have an action, just display the request page further down in the code.
	unless ( $action && $q->param('replace_id') )
	{
	}
	# If it's a replace request...
	elsif ( $action eq 'replace' )
	{
		Load_Params();		# Load the params into a local hash.

		# Clear the ID of the standard garbage, then put it in a local variable.
		$params_hash->{'replace_id'} = uc( G_S::PreProcess_Input($params_hash->{'replace_id'}) );

		Get_Replacement($params_hash->{'replace_id'});
		$replace_id = $replacement->{'id'} unless @errors;

		# Has the member been removed.
		Check_Removed($replace_id) unless @errors;

		# Has the member been replaced already.
		Check_Replaced($replace_id) unless @errors;

		# Is the member currently or ever been sponsored by the requestor
		# OR is the requestor the upline Builder
		Check_Sponsored($replace_id) unless @errors;

		# Was the member ever part of the Coop OR
		# Has the member transfer gone beyond the maximum time limit.
		Check_Trans_Limit($replace_id, Check_Coop_Src($replace_id)) unless @errors;

		unless (@errors)
		{
			if ( Insert_Order() )
			{
				push (@messages, 'success');
				# Clear the ID field to deter double ordering.
				$params_hash->{'replace_id'} = '';
			}
			else
			{
				push (@errors, 'fail_insert');
			}
		}
	}

	# Put the params into an xml format.
	$params_xml = $xs->XMLout($params_hash, RootName=>'params');
	
	# Now build out the page.
	Display_Page();
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	Err(qq#Please log in <a href="$LOGIN_URL?destination=$ME">HERE</a> to use this form.#);
}

$db->disconnect if $db;
exit;

################################
###### Subroutines start here.
################################

sub Check_Coop_Src
{
	my $search_id = shift;
	my ($rv) = $db->selectrow_array("
		SELECT c.coop_id
		FROM coop.coops c
		JOIN original_spid os
			ON os.orig_spid=c.coop_id
		WHERE c.house_acct=TRUE
		AND os.id= ?", undef, $search_id);
	return $rv if $rv;
	push (@errors, 'nocoop');
	return undef;
}

###### Check if the member has been replaced already.
sub Check_Replaced
{
	my $search_id = shift;
	my $replaced = $db->selectrow_array("
		SELECT stamp::date
		FROM 	coop_replace
		WHERE 	replace_id = $search_id;");
	push (@errors, 'replaced', $replaced) if $replaced;
	return 1;
}

###### Check if the member has been removed.
sub Check_Removed
{
	my $search_id = shift;
	my $id = $db->selectrow_array("
		SELECT id FROM members WHERE id = $search_id AND membertype = 'r'");
	push (@errors, 'notremoved') unless $id;
	return 1;
}

###### Check if the member is sponsored by the requestor.
sub Check_Sponsored
{
	my $search_id = shift;
	my $id =
		$db->selectrow_array("
		SELECT id
		FROM 	members
		WHERE 	id = ? AND spid = ?", undef, $search_id, $member_id) ||

		$db->selectrow_array("
		SELECT id
		FROM 	spid_arc
		WHERE 	id = ? AND spid = ?", undef, $search_id, $member_id) ||

		$db->selectrow_array("
		SELECT id
		FROM 	members_arc
		WHERE 	id = ? AND spid = ?", undef, $search_id, $member_id) ||

	###### not really a sponsorship test, but the easiest place to put it
	###### check to see if the user is performing the Builder role... NOT a BUILDER TEST ANYMORE AS WE DO NOT HAVE BUILDERs
	###### now it is merely an upline test
		$db->selectrow_array("
		SELECT id
		FROM 	my_upline(?)
		WHERE 	id = ?", undef, $search_id, $member_id);
	push (@errors, 'notsponsored') unless $id;
	return 1;
}

###### Check if the co-op member's transfer has gone beyond the maximum time limit.
sub Check_Trans_Limit
{
	my $search_id = shift;
	# if we don't receive an original spid, then we were never part of the coop
	my $os = shift || return;

	my ($last_stamp) = $db->selectrow_array("
		SELECT stamp FROM (
			SELECT stamp FROM members_arc WHERE id= ? AND operator ~ 'coop'
			UNION
			SELECT stamp FROM spid_arc WHERE id= ? AND operator ~ 'coop'
		) AS tmp ORDER BY stamp DESC LIMIT 1", undef, $search_id, $search_id);

	# If there are no archives, get the stamp from the current member record.
	unless ( $last_stamp )
	{
		$last_stamp = $db->selectrow_array("
			SELECT stamp::DATE FROM members WHERE id= ?", undef,$search_id);
	}

	# Get the current date and time less the limit in postgres format for comparison.
	my $limit = $db->selectrow_array("SELECT NOW()::DATE - 5");

	push (@errors, 'translimit') if $limit gt $last_stamp;

	return 1;
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	# Get the request form content.
	my $content_xml = $gs->Get_Object($db, $TMPL{content}) || die "Failed to load XML template: $TMPL{content}\n";
	
	# Combine the pieces of the xml document.
	my $xml = "<base><script_name>$ME</script_name><errors>";
	
	foreach(@errors)
	{
		$xml .= "<error>$_</error>\n";
	}
	$xml .= '</errors><messages>';
	
	foreach(@messages)
	{
		$xml .= "<message>$_</message>\n";
	}
	$xml .= "</messages>
		$content_xml
		$params_xml
		</base>";


	if ($q->param('xml'))
	{
		print "Content-type: text/plain\n\n$xml";
	}
	else
	{
		# Load the country menu into an xml block.
		my $xsl = $gs->Get_Object($db, $TMPL{form}) || die "Failed to load XSL template: $TMPL{form}\n";

		print $q->header(-charset=>'utf-8'), XML::local_utils::xslt($xsl, $xml);
	}
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator');
		my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
		{
			Err("You must be logged in as an Admin to use this form.");
		}
		else
		{
			# Mark this as an admin user.
			$access_type = 'admin';
			$member_id = $q->param('id');
			$mem_info = $db->selectrow_hashref("
				SELECT * FROM members WHERE id = ?", undef, $member_id);
			$params_hash->{'admin'} = 1;
			$db->{'RaiseError'} = 1;
		}
	}
	# If this is a member.		 
	else
	{
		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
		if ( $member_id && $member_id !~ m/\D/ )
		{
			$access_type = 'auth';
			unless ($db = DB_Connect($cgiini)) {exit}
			$db->{'RaiseError'} = 1;
		}
	}
}

###### Get the actual ID if an alias was provided.
sub Get_Replacement
{
	my $id = shift;
	unless ($id)
	{
		push @errors, "No ID received\n";
		return;
	}
	
	my $qry = 'SELECT id, spid, country FROM members WHERE ' . ($id =~ m/\D/ ? 'alias=?' : 'id=?');
	$replacement = $db->selectrow_hashref($qry, undef, $id);
	
	# now get the membertype they were before removal
	($replacement->{'prior_membertype'}) = $db->selectrow_array("
		SELECT membertype
		FROM members_arc
		WHERE membertype IN ('m','s')
		AND id= $replacement->{'id'}
		ORDER BY stamp DESC LIMIT 1");

	push (@errors, 'fail_id') unless $replacement;
}

###### Insert an order into the queue - coop_orders table.
sub Insert_Order
{
	# Start the query off with the operator field.
	( my $order_type = $config_xml) =~ s/^(.*?)replace_ordertype>|<\/replace_ordertype(.*?)$//gs;

	my %ascii_hash = %{$params_hash};
	my $ascii_hash = \%ascii_hash;

	# Automatically void all test account orders.
	$ascii_hash->{'status'} = 'v' if $ascii_hash->{'id'} == 12 || $ascii_hash->{'id'} == 2994131;

	my $recipient = $db->selectrow_hashref("SELECT id, country, language_pref FROM members WHERE id= $replacement->{'spid'}");
	
	# Build the query.
	my $order_num = $db->selectrow_array("SELECT nextval('public.coop_orders_pk_seq')");

	if ( $order_num )
	{
		# this used to put the requesters ID into the ID slot...
		# but when the requester is not the actual current sponsor, the current sponsor loses the replacement to the upline
		my $errlvl = $db->{'RaiseError'};
		$db->{'RaiseError'} = 0 if $errlvl;
		$db->begin_work;
		$db->do("INSERT INTO coop_orders ( 
					pk,
					id,
					quantity,
					price,
					pay_type,
					order_type,
					status,
					operator,
					country_list,
					membertype ) 
			VALUES ( ?,?,?,?,?,?,?,?,?,? )", undef,
					$order_num,
					$recipient->{'id'},
					'1',
					'0',
					'FR',
					$order_type,
					$ascii_hash->{'status'} || $default_status,
					$cgiini,
					"$recipient->{'country'},$replacement->{'country'}",
					$replacement->{'prior_membertype'});
		if ($db->err)
		{
			push @errors, 'There was an error inserting a new co-op order';
			warn $db->errstr;
			$db->rollback;
			return 0;
		}

		$db->do("INSERT INTO coop_replace ( 
					replace_id,
					request_id,
					order_num) 
			VALUES ( ?,?,? )", undef,
					$replace_id,
					$ascii_hash->{'id'},
					$order_num);
		if ($db->err)
		{
			push @errors, 'There was an error inserting a new co-op replacement record';
			warn $db->errstr;
			$db->rollback;
			return 0;
		}
		$db->commit;
		$db->{'RaiseError'} = $errlvl if $errlvl;
		return 1;
	}
}

###### Load the params into a local hash.
sub Load_Params
{
	# Get the submitted parameters.
	foreach ( $q->param() )
	{
		$params_hash->{$_} = $q->param($_) || '';
		$params_hash->{$_} = '' if 	$params_hash->{$_} eq 'blank';
	}
}

__END__

###### 02/02/06 Changed the INSERT/INSERT from a "prepare" to a "do" due to the 8.1 DB upgrade.
######		Also commented out the ConvHashref reference.
###### 08/11/06 revised the algorithms for determining coop participation and
###### purchase date, removed a defunct sub and coded a new one: Check_Coop_Src
###### departed from the modification of the xslt to instead place all necessary
###### messages, errors, etc. into the xml to be used by the revised xslt template
###### lots of other code optimization
###### 07/11/12 lots of revisions pertaining to replacements, like making the replacement go to the current sponsor instead of the user (think upline)
###### also added the sponsor's country and the replacement's country to the country list and the prior membertype to enforce a same membertype replacement
05/05/15	Revised the SQL in Insert_Order to split up the prepared statements to keep a newer DBI happy
			Also, added error handling to avoid some potentially nasty DB errors being presented