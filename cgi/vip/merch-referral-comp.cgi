#!/usr/bin/perl -w
###### merch-referral-comp.cgi
###### VIP report on merchant referral compensation (the aggregates of those %5 amounts garnered by signing up merchants)
###### created: 12/09	Bill MacArthur
###### last modified: 

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw/$LOGIN_URL/;
use XML::Simple;
use Encode 'encode_utf8';
use Template;
use GI;

# for now we'll simply place any errors in this var (make sure it's valid xhtml)
# if present, an <errors> node will be created in the root
my $errors = my $alerts = '';
my %params = ();	# used during the addfollowup action
my $q = new CGI;
my $gs = new G_S;
my $xs = new XML::Simple;
my $TT = Template->new();

my %TMPL = (
	lang_blocks	=> 10767,
	tmpl => 10766,
);

###### error handling provided by LoginCK
exit unless my $meminfo = LoginCK();

my $db = DB_Connect::DB_Connect('vip-merch-referral-comp.cgi') || exit;
$db->{RaiseError} = 1;

my $xml = $gs->Get_Object($db, $TMPL{lang_blocks}) ||
	die "Failed to load lang_blocks XML\n";
$xml = XMLin($xml, NoAttr => 1);	# convert our XML into a hashref
# turn off the utf-8 flag for the data in there
$xml->{$_} = encode_utf8($xml->{$_}) foreach keys %{$xml};

my $period = $q->param('period') || '';
$period = '' if $period =~ /\D/;
$period ||= $db->selectrow_array('SELECT get_working_period(1,NOW())');

my ($merchants, $totals) = GetData();
Display();

End();

###### ###### ######

sub cboDates
{
#	return '';
	my $sth = $db->prepare("
		SELECT DISTINCT p.period, p.open, (p.open ||' - '|| COALESCE(p.close, NOW()::DATE)) AS label
		FROM ta_live ta
		JOIN periods p
			ON p.period=ta.period
		WHERE ta.id= ?
		AND ta.sub_type=1
		
		UNION
		
		SELECT DISTINCT p.period, p.open, (p.open ||' - '|| COALESCE(p.close, NOW()::DATE)) AS label
		FROM periods p
		WHERE p.period= $period
		
		ORDER BY open DESC");
	$sth->execute($meminfo->{id});
	my (@vals, %lbls) = ();
	while (my $tmp = $sth->fetchrow_hashref){
		push @vals, $tmp->{period};
		$lbls{$tmp->{period}} = $tmp->{label};
	}
	
	return $q->popup_menu(
		-name=>'period',
		-default=>$period,
		-values=>\@vals,
		-labels=>\%lbls,
		-onchange=>'this.parentNode.submit()',
		-class=>'ctl',
		-style=>'background-color:#dfc'
	);
}

sub Display
{
	my $tmpl = $gs->Get_Object($db, $TMPL{tmpl}) ||
		die "Failed to load template: $TMPL{tmpl}\n";
		
	# as of 12/09, all the report verbiage is contained in the language pack,
	# so we need to interpolate any placeholders within the individual elements before inserting them into the main template
	my %tmp = ();
	$TT->process(\$xml->{$_}, {member=>$meminfo}, \$tmp{$_}) foreach keys %{$xml};
	$xml = \%tmp;
	my $GI = GI->new();
	my $rv = ();
	print $q->header(-charset=>'utf-8');
	$TT->process(\$tmpl,
		{
			member=>$meminfo,
			merchants=>$merchants,
			totals=>$totals,
			cboDates=>cboDates(),
			xml=>$xml,
			script_name=>$q->script_name
		}, \$rv);
	print $GI->wrap({
		content=>\$rv,
		title=>$xml->{mr_title},
		style=> $q->style({-type=>'text/css'}, '
			tr.bg-lt-orange, tr.bg-lt-green > td {color:#FC6F3F;}
			#mainTbl {margin:auto;}') .
			$GI->StyleLink('/css/sales-rpt.css')
	});
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub GetData
{
	my $sth = $db->prepare('
		SELECT
--	ta.id,
			mal.location_name,
			SUM(ta.commission) AS commission,
			SUM(ta.npp) AS npp,
			COUNT(ta.*) AS num_of_sales,
			SUM(t.amount) AS sales_ttl,
			AVG(t.amount)::numeric(8,2) AS avg_sale,
			COUNT(DISTINCT t.id) AS unique_shoppers
		FROM ta_live ta
		JOIN transactions t
			ON t.trans_id=ta.transaction_ref
		JOIN merchant_affiliate_locations mal
			ON mal.vendor_id=t.vendor_id
		WHERE ta.sub_type=1
		AND ta.id= ?
		AND ta.period= ?
		GROUP BY 
			ta.id,
			mal.location_name
		ORDER BY mal.location_name;
	');
	$sth->execute($meminfo->{id}, $period);
	my (%totals, @merchants) = ();
	while (my $tmp = $sth->fetchrow_hashref){
		push @merchants, $tmp;
		$totals{$_} += $tmp->{$_} foreach (qw/commission npp num_of_sales sales_ttl/);
	}
	return \@merchants, \%totals;
}

sub LoginCK
{
	my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic'));
###### a valid id will not have anything besides digits
	unless ($id && $meminfo && $id !~ /\D/)
	{
		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
		return;
	}
###### if they are not a VIP, they don't have access to the report
	elsif ( $meminfo->{membertype} !~ 'v' )
	{
		Err('These Reports are for VIP members only'),
	}

	return $meminfo;
}