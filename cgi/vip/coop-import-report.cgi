#!/usr/bin/perl -w
###### coop-import-report.cgi
###### the reporting interface for the automated coop importing system
###### created: 11/08	Bill MacArthur
###### last modified:

$| = 1;

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S qw($LOGIN_URL);
use DB_Connect;

###### GLOBALS
my $q = new CGI;
my $coop = ();		# will end up being our coop hashref
my $provider = ();	# will end up being our provider hashref
my $report_id = $q->param('report_id') || '';
die 'Invalid report_id submitted' if $report_id =~ /\D/;

my $coop_id = $q->param('coop_id');
unless ($coop_id){
	print $q->header, $q->start_html, $q->start_form(-method=>'get'), 'Enter a numeric Coop ID<br />',
		$q->textfield(-name=>'coop_id'), $q->submit('Submit'), $q->end_form, $q->end_html;
	exit;
}
elsif($coop_id =~ /\D/){
	die "Invalid 'coop_id' parameter\n";
}
my $provider_id = $q->param('provider_id') || '';
die 'Invalid provider_id submitted' if $provider_id =~ /\D/;

my ($db, @List) = ();
													
our ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . ${\$q->script_name()} . "\">$LOGIN_URL</a>");
}

# we'll use this DB connect label even though this is not that script... close enough
exit unless $db = DB_Connect('spec-coop');
$db->{RaiseError} = 1;

$coop = $db->selectrow_hashref('
	SELECT c.coop_id, m.alias FROM coop.coops c, members m WHERE c.coop_id=m.id AND c.coop_id= ?', undef, $coop_id);
die "Coop lookup failed: $coop_id\n" unless $coop;

###### if they are not in the MAP, they don't have access to this interface
unless ( $db->selectrow_array("
	SELECT TRUE FROM coop.coop_admins ca WHERE ca.coop_id= $coop->{coop_id} AND ca.member_id= ?", undef, $id) )
{
	Err('Access Denied');
}

# if a provider ID was not submitted, then we'll check on a list of providers for this Coop
# if there are none, then we are done
# if there is only one, we can go straight to it,
# otherwise, we'll provide them a simple list to choose from
# *if* there is a provider ID submitted, we can proceed with whatever reporting for that particular ID

unless ($provider_id)
{
	SelectProvider();
}
else
{
	GetProviderInfo();
	unless ($report_id)
	{
		###### provide a list of reports to choose from
		ImportListReport();
	}
	else
	{
		###### crank out a report detail
		ReportDetail();
	}
}

_exit();

###### ###### Start of Sub-Routines

sub Display
{
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-title=>'Spec-Coop',
			-encoding=>'utf-8',
			-style=>{-code=>'
				input{margin:0.1em; margin-right: 0.5em; width:7em;} div, p{font-size:90%;}
				textarea{font-size:85%; margin-right:0.5em;} table {border-collapse:collapse;}
				table#mainTbl {margin:1em 0;}
				td {vertical-align:top; padding: 0.2em 0.5em 0.2em 0.2em;}
				table#mainTbl td { border:1px solid silver; } table#groupFrmTbl td { border:0; padding:0.2em;}'},
			-script=>{
				-type=>'text/javascript',
				-code=>"function specrst(){
					document.forms.mainfrm.recipient.value='';
					document.forms.mainfrm.num_to_transfer.value=''; }"
			}
		),


	print '<table id="mainTbl"><tr><td>',
		$q->start_form(-method=>'get', -id=>'mainfrm'),
		$q->span({-style=>'font-weight:bold;font-size:smaller;'}, 'Shopper and non-JS Associate members'),
		$q->p('This interface will transfer the oldest memberships regardless of membertype'),
		$q->hidden('c'), $q->hidden(-name=>'action', -value=>'process');

	print $q->p(
			$q->textfield(-name=>'recipient', -force=>1) . 'Enter the transfer recipient<br />' .
			$q->textfield(-name=>'num_to_transfer', -force=>1) . 'Enter the number to transfer<br />' .
			$q->button(-onclick=>'specrst()', -value=>'Reset') . $q->submit('Process')
		),
		$q->end_form, '</td><td>';

	print $q->start_form(-method=>'get', -id=>'groupfrm'),
		$q->span({-style=>'font-weight:bold;font-size:smaller;'}, 'VIPs and JS Associate members'),
		$q->p('This interface will perform any transfers you specify unless they are on the Exclusion List'),
		$q->hidden('c'), $q->hidden(-name=>'action', -value=>'process'), $q->hidden(-name=>'group', -value=>1),
		'<table id="groupFrmTbl">',
		$q->Tr( $q->td($q->textfield(-name=>'recipient', -force=>1)) . $q->td('Enter the transfer recipient') ),
		$q->Tr( $q->td($q->textarea(-name=>'grouplist', -force=>1, -cols=>12, -rows=>10, -style=>'width:9em;')) .
				$q->td('Enter the memberships to transfer') ),
		'</table>',
		$q->button(-value=>'Reset') . $q->submit('Process'),
		'</form></td></tr></table>';
	print $q->start_form(-onsubmit=>'return false', -id=>'proclistfrm');

		$q->end_form, $q->end_html;
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
	_exit();
}

sub GetProviderInfo
{
	$provider = $db->selectrow_hashref('
		SELECT provider_name, provider_id, active FROM coop.providers WHERE provider_id= ?', undef, $provider_id);
	Err("Provider lookup failed for ID: $provider_id") unless $provider;
}

sub ImportListReport
{
	###### crank out a list of reports from which they can choose for list details
	print $q->header(-charset=>'utf-8'),
		$q->start_html(
			-style=>{-src=>'/css/generic-report.css'}
		),
		$q->h4('Choose from these reports for list details:'),
		'<table class="report">',
		'<tr><th>Report ID</th><th>Items Accepted</th><th>Items Rejected</th><th>Timestamp</th></tr>';
	my $sth = $db->prepare("
		SELECT pk, stamp, accepted, rejected
		FROM coop.imports
		WHERE coop_id= $coop->{coop_id} AND provider_id= $provider->{provider_id}
		ORDER BY stamp DESC
		LIMIT 60
	");
	$sth->execute;
	my $class = 'a';
	while (my $tmp = $sth->fetchrow_hashref)
	{
		print qq|<tr class="$class">|;
		print $q->td( $q->a(
			{-href=>$q->url . "?coop_id=$coop->{coop_id};provider_id=$provider->{provider_id};report_id=$tmp->{pk}"}, $tmp->{pk}
		));
		print $q->td($tmp->{accepted}), $q->td($tmp->{rejected}), $q->td($tmp->{stamp});
		print '</tr>';
	}
	print '</table>', $q->end_html;
	_exit();
}

sub ReportDetail
{
	###### provide some details on a particular imported batch
	my $report = $db->selectrow_hashref("
		SELECT response_list, accepted, rejected, stamp
		FROM coop.imports
		WHERE coop_id=$coop->{coop_id} AND provider_id= $provider->{provider_id} AND pk= ?", undef, $report_id);
	print $q->header(-charset=>'utf-8'), $q->start_html(
		-style=>{-code=>'th {padding-left:0.5em; text-align:left;}'}
	);
	print $q->table({-style=>'margin-bottom:1em;'},
		"<tr><td>Provider:</td><th>$provider->{provider_name}</th></tr>
		<tr><td>Provider ID:</td><th>$provider->{provider_id}</th></tr>
		<tr><td>Coop ID:</td><th>$coop->{coop_id}</th></tr>
		<tr><td>Report ID:</td><th>$report_id</th></tr>
		<tr><td>Items Accepted:</td><th>$report->{accepted}</th></tr>
		<tr><td>Items Rejected:</td><th>$report->{rejected}</th></tr>");
	print $q->start_form,
		$q->textarea(
			-name=>'response_list',
			-rows=>30,
			-cols=>100,
			-style=>'width:100%',
			-value=>$report->{response_list}),
		$q->end_form, $q->end_html;
}

sub SelectProvider
{
	my @providers = ();
	my $sth = $db->prepare("SELECT coop_id, provider_id, provider_name FROM coop.providers WHERE coop_id= $coop->{coop_id}");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @providers, $tmp;
	}
	
	unless (@providers)
	{
		Err('There are no providers for this Coop');
		goto 'END';
	}
	elsif (scalar @providers == 1)
	{
		$provider_id = $providers[0];
		GetProviderInfo();
		ImportListReport();
	}
	else
	{
		# provide a simple list of providers from which they can choose
		print $q->header(-charset=>'utf-8'), $q->start_html, $q->h4('Choose a provider:');
		foreach (@providers)
		{
			print $q->p( $q->a({
				-href=>  $q->url . "?coop_id=$_->{coop_id};provider_id=$_->{provider_id}"
			}, $_->{provider_name}));
		}
		print $q->end_html;
	}
}

sub _exit
{
	$db->disconnect if $db;
	exit;
}