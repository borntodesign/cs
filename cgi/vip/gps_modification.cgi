#!/usr/bin/perl -w
###### gps_modification.cgi
###### GPS Modification Function
###### newly released: 05/29/12	Bill MacArthur
###### last modified: 05/28/15	Bill MacArthur

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use Template;
use Encode;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw/$EXEC_BASE $LOGIN_URL/;
use XML::local_utils;
#use Data::Dumper;	# testing only
binmode STDOUT, ":encoding(utf8)";

my %TMPL = (
	'xml'	=> 10957,
	'main'	=> 10958
);

my $ALLOW_FREE_UPGRADES = 0;	# set to -0- for normal operation, otherwise they can get the upgrade for nothing
my $q = new CGI;
my $TT = Template->new();
my $gs = new G_S;
my ($db, $lang_blocks, %params, $USER_ID, $meminfo, $stats, $messages, $alerts , $errors, $xSubscription) = ();
my %AllowedDowngrades = ();	# the keys will be set to the allowed subscription types which they can downgrade to
my $PendingDowngrade = ();	# this will hold a hashref of the pending downgrade record if it exists

( $USER_ID, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
if (!$USER_ID || $USER_ID =~ /\D/)
{
	Err("A valid <a href=\"$LOGIN_URL\">login</a> is required to use this interface.");
	Exit();
}


$db ||= DB_Connect::DB_Connect('gps_modification') || exit;
$db->{'RaiseError'} = 1;
my $SubscriptionTypes = $db->selectall_hashref("SELECT * FROM subscription_types WHERE sub_class='VM' AND renewal=TRUE", 'subscription_type');
GetPendingDowngrade();

# we need their pay level
($meminfo->{'a_level'}) = $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= ?", undef, $USER_ID);
	
# we also need their payment method
($meminfo->{'payment_method'}) = $db->selectrow_array("SELECT payment_method FROM mop WHERE id= ?", undef, $USER_ID);
	
# are they paid up to date?
($meminfo->{'nop'}) = $db->selectrow_array("SELECT nop(?)", undef, $USER_ID);

# we need their native currency...
# This is defunct as of 06/03/13 --> and their vip_date as they cannot downgrade to BASIC if they became partners before 06/01/2012 (business policy as of June 2012)
($meminfo->{'vip_date'}, $meminfo->{'currency_code'}) = $db->selectrow_array("
	SELECT m.vip_date, cbc.currency_code
	FROM members m
	JOIN currency_by_country cbc
		ON cbc.country_code=m.country
	WHERE m.id= ?", undef, $USER_ID);

LoadParams();

LoadLangBlocks();

GetSubscription();

unless ($params{'_action'})
{
	Output();
}
elsif ($params{'_action'} eq 'rc')
{
	# we should be receiving an scode parameter to tell us what verbiage to present as this should be the success screen
}
elsif ($params{'_action'} eq 'change')
{
	ValidateStype();

	# we should be receiving an stype parameter to tell us what we are changing to
	# if the change is successful, we will redirect them back using the _action=rc and an scode to load a lang_node as an alert message
	
	# determine if we are authorized to change to that option... theoretically we should not encounter a situation like this as we should not offer the option to begin with
#	unless ($authorized_changes{$params{'stype'}})
	unless ($AllowedDowngrades{$params{'stype'}})
	{
		$errors = 'Changing to that option is not available for your pay level/language preference/payment method';
		Output();
	}
	
	CreatePendingDowngrade();
	GetSubscription();
	GetPendingDowngrade();

	Output();
}
elsif ($params{'_action'} eq 'cancel_pending')
{
	CancelDowngrade();
	$PendingDowngrade = ();
	Output();
}
else
{
	Err("Undefined action: $params{'_action'}");
}

Exit();


###### ###### Start of Sub Routines ###### ######

sub CancelDowngrade
{
	$db->do("DELETE FROM subscription_changes_pending WHERE member_id= $xSubscription->{'member_id'}");
}

sub CreatePendingDowngrade
{
	$db->begin_work;
	CancelDowngrade();
	$db->do("
		INSERT INTO subscription_changes_pending (
			member_id,
			old_subscription_type,
			new_subscription_type,
			effective_date
		) VALUES (
			$xSubscription->{'member_id'},
			$xSubscription->{'subscription_type'},
			?,
			('$xSubscription->{'end_date'}'::DATE +1)
		);
	", undef, $q->param('stype'));
	$db->commit;
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'<h4>There has been a problem</h4>',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
	
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetPendingDowngrade
{
	$PendingDowngrade = $db->selectrow_hashref("
		SELECT scp.*, st.label
		FROM subscription_changes_pending scp
		JOIN subscription_types ST
			ON st.subscription_type=scp.new_subscription_type
		WHERE scp.member_id=?", undef, $USER_ID);
}
sub GetSubscription
{
	$xSubscription = $db->selectrow_hashref("
		SELECT s.*, st.label, st.weight
		FROM subscriptions s
		JOIN subscription_types st
			ON st.subscription_type=s.subscription_type
		WHERE st.sub_class= 'VM'
		AND s.member_id= ?
	", undef, $USER_ID);
	
	SetAllowedDowngrades();
}

sub LoadLangBlocks
{
	my $tmp = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to retrieve language pack template\n";
#	$tmp = decode_utf8($tmp);
	
#	$TT->process(
#		\$tmp,
#		{'rpt_month'=>$comm_month, 'Links'=>\%Links},
#		\$lang_blocks
#	);

	$lang_blocks = XML::local_utils::flatXMLtoHashref($tmp);
}

sub LoadParams
{
	foreach (qw/_action stype scode/)
	{
		$params{$_} = $q->param($_) || '';
	}
}

sub Output
{
#	ParseLangBlocks();

	my $tmp = $gs->Get_Object($db, $TMPL{'main'}) || die "Failed to retrieve main template\n";
	my $tmpl = ();
#	warn "tmp loaded:\n $tmp";
	my $rv = $TT->process(\$tmp,
		{
			'xSubscription' => $xSubscription,
			'member' => $meminfo,
			'lang_blocks' => $lang_blocks,
			'script_name' => $q->script_name(),
			'params' => \%params,
			'messages' => $messages,
			'alerts' => $alerts,
			'errors' => $errors,
			'SubscriptionTypes' => $SubscriptionTypes,
			'PendingDowngrade' => $PendingDowngrade,
			'AllowedDowngrades' => \%AllowedDowngrades
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}
#warn "tt done";
	# reprocess the template since some of our lang nodes contain placeholders
	print $q->header('-charset'=>'utf-8');
	$rv = $TT->process(\$tmpl,
		{
			'xSubscription' => $xSubscription,
			'member' => $meminfo,
			'params' => \%params
		}) ||
		die $TT->error();

	Exit();
}

sub SetAllowedDowngrades
{
	return unless $xSubscription; # can't do anything if I don't know what I am now
	
	# they cannot go any lower than the lowest
	my ($lowest) = $db->selectrow_array("SELECT subscription_type FROM subscription_types WHERE sub_class='VM' AND renewal=TRUE ORDER BY weight ASC LIMIT 1");
	
	if ($lowest == $xSubscription->{'weight'})
	{
		return;
	}
	
	foreach my $stype (keys %{$SubscriptionTypes})
	{
		# they are not allowed to downgrade below PRO if they upgraded prior to June 1, 2012 (under the old 1 subscription type system)
		# as of September there is a new BASIC PLUS subscription type to which anybody can downgrade
#		next if $meminfo->{'vip_date'} lt '2012-06-01' && $SubscriptionTypes->{$stype}->{'weight'} < 15;	# 20 is the weight of the PRO subscription as of 06/12
		
		$AllowedDowngrades{$stype} = 1 if $SubscriptionTypes->{$stype}->{'weight'} < $xSubscription->{'weight'};
	}
	
	# we could leave here with an empty hash if the party is already at the lowest possible subscription type they qualify for
}

sub ValidateStype
{
	# stype should be one of these, if it is not, we will empty it and set an error
	$errors = "Unrecognized stype: $params{'stype'}" unless grep {$params{'stype'} eq $_} keys %{$SubscriptionTypes};
	Output() if $errors;
}

# just keeping this around in case it is decided to let them upgrade using this interface... this would be a start
#sub MakeChange
#{
#	my $change_fee = shift;
#
#	# we will delete our existing subscription and create a new one in it's place with the same term
#	# if there is a premium in price and PP, we will attach the difference to the new subscription if it their existing one is current
#	if ($ALLOW_FREE_UPGRADES || $meminfo->{'nop'} || $change_fee->{'usd'} <= 0)
#	{
#		$xSubscription->{'amount'} = 0;
#		$xSubscription->{'usd'} = 0;
#		$xSubscription->{'pp_value'} = 0;
#	}
#	else
#	{
#		$xSubscription->{'amount'} = $change_fee->{'amount'};
#		$xSubscription->{'usd'} = $change_fee->{'usd'};
#		$xSubscription->{'pp_value'} = $change_fee->{'pp_value'};
#	}
#	
#	$db->{'RaiseError'} = 0;	# we will trap errors
#	$db->begin_work;			# roll this into a transaction
#	
#	# get some money if some is due... if we cannot get it, then skip the rest and let 'em know
#	my $ca = ();
#	if ($change_fee->{'usd'} > 0)
#	{
#		($ca) = $db->selectrow_array("
#			SELECT debit_trans_simple(?, $change_fee->{'soa_trans_type'}, ($change_fee->{'usd'} * -1))
#		", undef, $USER_ID);
#
#		if ($db->errstr)
#		{
#			$errors = "There was a database error creating a Club Account debit:<br />" . $db->errstr;
#		}
#		elsif (! $ca)
#		{
#			$errors = "Your Club Account had an insufficient balance to cover your subscription upgrade";
#		}
#	}
#	
#	unless ($errors)
#	{
#		$db->do("DELETE FROM subscriptions WHERE pk= $xSubscription->{'pk'}");
#		if ($db->errstr)
#		{
#			$errors = "There was a database error expunging your existing subscription:<br />" . $db->errstr;
#		}
#		else
#		{
#			$db->do("
#				INSERT INTO subscriptions (
#					member_id,
#					subscription_type,
#					start_date,
#					end_date,
#					price,
#					currency_code,
#					pp_value,
#					usd,
#					notes,
#					operator,
#					soa_trans_id,
#					void
#				) VALUES (
#					$xSubscription->{'member_id'},
#					$change_fee->{'subscription_type'},
#					'$xSubscription->{'start_date'}',
#					'$xSubscription->{'end_date'}',
#					$xSubscription->{'amount'},
#					'$change_fee->{'currency_code'}',
#					$xSubscription->{'pp_value'},
#					$xSubscription->{'usd'},
#					'Subscription upgrade/downgrade',
#					?,
#					?,
#					?
#				)
#			", undef, $q->url('-relative'=>1), $ca, $xSubscription->{'void'});
#			
#			if ($db->errstr)
#			{
#				$errors = "There was a database error creating your new subscription:<br />" . $db->errstr . "<br />" . $q->url('-relative'=>1);
#			}
#		}
#	}
#
#	if ($errors)
#	{
#		$db->rollback;
#	}
#	else
#	{
#		$db->commit;
#		$messages = 'Your subscription change was successful!';
#	}
#	$db->{'RaiseError'} = 1;
#}

__END__

06/03/13	We will now allow subscription downgrades to occur for GPS subscribers prior to June 2012... Bill - Remove the filter.

08/19/13	No functional changes... just ripped out some old commented stuff before realizing I needed to be working in a different script

05/28/15	Added binmode to deal with utf-8 changes resulting from the recent upgrade of DBD::Pg
			Also disabled the no longer needed utf-8 prepartion in LoadLangBlocks()