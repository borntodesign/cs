#!/usr/bin/perl -w
###### JSInfo.cgi
###### return a set of data in JSON
# created: 03/23/12	Bill MacArthur
###### last modified:	01/07/15 Bill MacArthur

use CGI;
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;
use JSON;

my $json = JSON->new->allow_nonref;
my $q = new CGI;
my $rv = ();

###### GLOBALS

exit unless my $db = DB_Connect::DB_Connect('generic');

my $gs = G_S->new({'db'=>$db, 'CGI'=>$q});
my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
if ( $id && $id !~ /\D/ && $meminfo->{'membertype'} eq 'v' )
{
	$rv->{'refcomp'} = $db->selectrow_hashref('
		SELECT od.onekplus(op) AS op, od.thirtykplus(opp) AS opp, prvs, sales_req_count, ppp
		FROM refcomp WHERE id= ?', undef, $id);
	$rv->{'personal'} = $meminfo;
}
$gs->Prepare_UTF8($rv, 'decode');
print $q->header('-type'=>'text/javascript', '-charset'=>'utf-8', -'expires'=>'-1d');
print 'var _JS_Information = ' . ($q->param('pretty') ? $json->pretty->encode($rv) : $json->encode($rv)) . ';';

$db->disconnect;
exit;

__END__

10/23/14	applied Prepare_UTF8 to the return hash after finding arabic names totally ruined
01/07/15	added an expires header since we have a report of somebody seeing somebody else's name at the PBC