#!/usr/bin/perl -w
# spcrd.cgi	A Special Redirector :)
=pod
Initially created to fill a special need to ensure a VIP is logged in and part of Fab's organization before
redirecting them to a special Coop shopping cart of Fab's.
=cut

use CGI;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;

my $cgi = new CGI;
my $gs = new G_S;
my $db = DB_Connect::DB_Connect('generic') || exit;
$db->{RaiseError} = 1;
my ($id, $meminfo) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic') );
Err('Login required') if $id =~ /\D/;

my $path = ();
(undef, $path) = split('/', $cgi->path_info);
Err('No path provided') unless $path;
Err("Not in the organization of $path") unless $db->selectrow_array('
	SELECT upline FROM relationships WHERE id= ? AND upline= ?', undef, $id, $path);

my $rv = _rd2();
$rv ? print $cgi->redirect($rv) : Err('Invalid path');
End();

###### ######

sub End
{
	$db->disconnnect if $db;
	exit;
}

sub Err
{
	print $cgi->header(), $cgi->start_html(), $cgi->h4($_[0]), $cgi->end_html;
	End();
}

sub _rd2
{
	my %map = (
		159991 => "http://www.clubshop.com/index.cml?id=$id"
	);
	return $map{$path};
}