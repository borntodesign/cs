#!/usr/bin/perl -w
###### MemberModification.cgi (changed from bulk_bem.cgi)
###### accept a list of 'bad' email addresses and mark the corresponding memberships BEM
###### also accept aliases or IDs and provide downgrade and phone number deletion functionalities
###### created: 02/19/04	Bill MacArthur
###### last modified: 03/12/12	Bill MacArthur

$| = 1;

use diagnostics -verbose;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S qw($LOGIN_URL);
use DB_Connect;

###### GLOBALS
my @EXCUSALS = (159991);	# enter member IDs to allow them to process an unlimited number
my $NOSQL = 0;	###### set to zero to effect the SQL updates
my $q = new CGI;
my $ME = 'MemberModification';	###### this will be our DB 'operator' of record
my $action = $q->param('action');
my $type = $q->param('type');

my %TYPES = (	# the value will be used to identify the process type with the saved list, it may also be used in SQL
	'bem'		=> {limit=>100, value=>-1, label=>'Bad'},
	'blocked'	=> {limit=>100, value=>-2, label=>'Blocked'},
	'remove'	=> {limit=>100, value=>'r', label=>'Removed'},
	'restore'	=> {limit=>100, value=>2, label=>'Restored'},
	'downgrade'	=> {limit=>100, value=>'s', label=>'Downgraded'},
	'del_tel'	=> {limit=>100, value=>'dt', label=>'Telephone Number Deleted'}
);
my ($db, @List) = ();
my ($id_sth, $alias_sth, $em_sth) = (); # statement handles we will initialize only once but use often in Process()
my $num_processed = 0;
my $memberships = 0;

###### we will send the list to 'support' for their perusal if the following activity counts are hit
my %alert_thresholds = (
	not_in_downline => {threshold=>5, cnt=>0},
	restore => {threshold=>10, cnt=>0},
	not_found => {threshold=>5, cnt=>0},
	no_phone => {threshold=>3, cnt=>0}
);
our %TMPL = (
	main	=> 10267
);

our ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . ${\$q->script_name()} . "\">$LOGIN_URL</a>");
	exit;
}

###### if they are not a VIP, they don't have access to this interface
if ( $meminfo->{membertype} !~ 'v' )
{
	print 	$q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3('This facility is for VIP members only'),
		$q->end_html();
		exit;
}

unless ($db = DB_Connect('MemberModification')){ exit }
$db->{RaiseError} = 1;
my $sth = $db->prepare("SELECT NOW()");
$sth->execute;
my $start = $sth->fetchrow_array;
$sth->finish;

my $Sofar = LimitCK();

if (! AccessCK())
{
	Err('You do not qualify to use this facility.');
}
elsif (! $action)
{
	my $tmpl = G_S::Get_Object($db, $TMPL{'main'}) || die "Failed to load template: $TMPL{main}";

	$tmpl =~ s/%%name%%/$meminfo->{firstname} $meminfo->{lastname}/;
	$tmpl =~ s/%%action%%/$q->script_name/eg;
	$tmpl =~ s/%%sofar%%/$Sofar/;

	print $q->header('-charset'=>'utf-8'), $tmpl;
}
elsif ($action eq 'process')
{
	unless ( $type && grep /$type/, ('bem', 'blocked', 'remove', 'restore', 'downgrade', 'del_tel') )
	{
		Err('Please select the type of processing you want to apply to this list.');
		goto 'END';
	}

	if ($Sofar >= $TYPES{$type}{'limit'})
	{
		Err("You have processed $Sofar within the last 24 hours.<br />The daily limit is: $TYPES{$type}{'limit'}");
	}

	my $list = $q->param('list');
	unless ($list)
	{
		Err('Please enter the memberships you want processed into the box.');
		goto 'END';
	}

	###### remove carriage returns that may come in from windows
	###### and leading/trailing whitespace
	$list =~ s/\r+|^\s*//g;
	$list =~ s/\n\s*|\s*\n/\n/g;
			
	###### get rid of extraneous newlines
	$list =~ s/^\n+|\n+$//;
	$list =~ s/\n+/\n/g;

	@List = split /\n/, $list;
	unless (@List)
	{
		Err('Please enter the memberships you want processed into the box.');
		goto 'END';
	}

	###### we are all set to begin processing
	###### in order to present a report that is semi aligned in columns
	###### we'll do plain text rather than HTML
	###### we'll also save the 'report' as we go so we can save it in the DB
	print 	$q->header('text/plain'),
		"Please wait while your list is processed.\n",
		"You will be told when the process is complete.\n\n";

	my $rpt = '';
	foreach (@List)
	{
		# the stuff some of these people submit is...
		s/^\s*|\s*$//g;

		my $line = "$_\t\t";
		my ($rv, $flag) = Process($_);
		$line .= $rv;
		print "$line\n";
		$rpt .= "$line\n";
		last if $flag;
	}

	if ($id > 1){
		print "\nSaving this report under your name for future reference if needed.\n";
		if ( Record(\$rpt) )
		{
			print "Report saved.\n";
		}
		else
		{
			print "Report save Failed!\n";
		}
	} else { print "\nThe report will not be saved for admin (01).\n"; }

	print 	"\n Processing is complete.\n",
		" You submitted ${\scalar @List} entrie/s.\n",
		" You have processed $num_processed entrie/s.\n",
		" This affected $memberships membership/s.\n",
		"\nYou may print this page for your records now.\n";

	AlertAdmin(\$rpt) if grep {$alert_thresholds{$_}{cnt} >= $alert_thresholds{$_}{threshold}} keys %alert_thresholds;
}
else
{
	Err('Undefined Action.');
}

END:
$db->disconnect;
exit;

###### ###### Start of Sub-Routines

sub AccessCK
{
	return 1; ###### for now
}

sub AlertAdmin
{
	my $msg = shift;	###### a scalar ref
	use Mail::Sendmail;
	sendmail(
		message=>"$meminfo->{alias}, $meminfo->{firstname} $meminfo->{lastname} has submitted this list:\n\n$$msg",
		subject=>'Member Modification usage alert',
#		to=>'support1@dhs-club.com',
		cc=>'webmaster@dhs-club.com',
		from=>'MemberModification.cgi@clubshop.com',
		'Content-Type'=>'text/plain; charset="utf-8"'
	);
}

sub Del_Tel
{
	return 1 if $NOSQL;
	###### we are receiving a hashref of the party being worked on
	my $rv = $db->do("
		UPDATE members SET
			phone = NULL,
			operator = '$ME',
			memo= 'phone number deletion initiated by $meminfo->{'alias'}'
		WHERE id= $_[0]->{id}");

	return ($rv eq '1') ? 1 : undef;
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub LimitCK
{
	return 0 if grep $_ == $id, @EXCUSALS;
	my $qry = "
		SELECT SUM(num_processed)
		FROM bulk_bems
		WHERE id= $id
		AND processed BETWEEN (NOW() - INTERVAL '1 day') AND NOW() ";
	$qry .= qq|AND "type"= '$TYPES{$type}{'value'}'| if $type;
						
	my ($cnt) = $db->selectrow_array($qry);
	return $cnt || 0;
}		

sub Process
{
	my $result = '';
	my $identifier = shift;	###### could be alias, id or emailaddress
	my ($sth) = ();
	my $sql = q|
		SELECT id, spid, mail_option_lvl, alias, membertype,
			COALESCE(phone,'') AS phone, mt.label AS mtype
		FROM 	members m
		INNER JOIN "Member_Types" mt ON m.membertype=mt.code
		WHERE|;

	if ($identifier =~ /@/)
	{
		###### initialize the global handle
		$em_sth ||= $db->prepare("$sql m.emailaddress = ?");
		$sth = $em_sth;
		$identifier = lc $identifier;	###### make sure the email address is lower case
	}
	elsif ($identifier =~ /\D/)
	{
		###### initialize the global handle
		$alias_sth ||= $db->prepare("$sql m.alias = ?");
		$sth = $alias_sth;
		$identifier = uc $identifier;	###### make sure the alias is upper case
	}
	else
	{
		###### initialize the global handle
		$id_sth ||= $db->prepare("$sql m.id = ?");
		$sth = $id_sth;
	}

	$sth->execute($identifier);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		my $rv = UplineCK($tmp->{id});
		my $ck = 0;

		unless (defined $rv)
		{
			if ($tmp->{membertype} ne 'm' && $tmp->{membertype} ne 's')
			{
				$result .= "\n$tmp->{alias} untouched due to membertype: $tmp->{mtype}";
			}
			elsif ($type eq 'remove' || $type eq 'downgrade')
			{
				if ($tmp->{membertype} eq 's' && $type eq 'downgrade')
				{
					$result .= "\n$tmp->{alias} untouched due to membertype: $tmp->{mtype}";
				}
				elsif ( Remove( $tmp, $TYPES{$type}{value} ) )
				{
					$result .= "\n$tmp->{alias} '$TYPES{$type}{label}'";
					$memberships++;
					$ck = 1;
				}
				else
				{
					$result .= "\n$tmp->{alias} update failed";
				}
			}
			elsif ($type eq 'del_tel')
			{
				if (! $tmp->{phone})
				{
					$result .= "\n$tmp->{alias} untouched - phone field empty: $tmp->{mtype}";
					$alert_thresholds{no_phone}{cnt}++;
				}
				elsif ( Del_Tel( $tmp ) )
				{
					$result .= "\n$tmp->{alias} '$TYPES{$type}{label}'";
					$memberships++;
					$ck = 1;
				}
				else
				{
					$result .= "\n$tmp->{alias} update failed";
				}
			}
			else
			{
				###### we won't fix 'em if they ain't broke
				if ($tmp->{'mail_option_lvl'} == $TYPES{$type}{'value'} ||
					($tmp->{'mail_option_lvl'} > 0 && $type eq 'restore')
				)
				{
					$result .= "\n$tmp->{alias} already marked '$TYPES{$type}{'label'}'";
				}
				else
				{
					if ( Simple_Update( $tmp->{'id'}, $TYPES{$type}{'value'} ) )
					{
						$result .= "\n$tmp->{alias} marked '$TYPES{$type}{'label'}'";
						$memberships++;
						$ck = 1;
						$alert_thresholds{'restore'}{'cnt'}++;
					}
					else
					{
						$result .= "\n$tmp->{alias} update failed";
					}
				}
			}
		}
		elsif ($rv eq '-1')
		{
			$result .= "\nxxxxxxx not in your downline";
			$alert_thresholds{'not_in_downline'}{'cnt'}++;
		}
		else
		{
			$result .= "\n$tmp->{'alias'} $rv";
		}

		$num_processed++ if $ck;
	}
	$sth->finish;
	my $done;
	if ($id > 1 && ($num_processed + $Sofar) >= $TYPES{$type}{'limit'} && ! grep $_ == $id, @EXCUSALS)
	{
		$result .= "\nYou have reached your limit for this period.\n";
		$result .= "Processing halted.";
		$done = 1;
	}
	return ($result, $done) if $result;
	$alert_thresholds{'not_found'}{'cnt'}++;
	return 'Not found';
}

sub Record
{
	return 1 if $NOSQL;

	my $rpt = shift;
	
	my $sth = $db->prepare("
		INSERT INTO bulk_bems (
			id,
			num_submitted,
			num_processed,
			memberships_affected,
			list,
			entered,
			processed,
			\"type\"
		) VALUES (
			$id,
			${\scalar @List},
			$num_processed,
			$memberships,
			?,
			'$start',
			NOW(),
			?
		)");

	my $rv = $sth->execute($$rpt, $TYPES{$type}{value});
	$sth->finish;

	return ($rv eq '1') ? 1 : undef;
}				

sub Remove
{
	return 1 if $NOSQL;
	###### our args are 1. hashref of the member being worked on, 2. the membertype they are going to
	my $rv = $db->do("
		UPDATE members SET
			spid= $_[0]->{spid},
			operator= '$ME',
			memo= 'rollup from removal/downgrade of previous sponsor: $_[0]->{alias}\ninitiated by: $meminfo->{alias}'
		WHERE 	spid= $_[0]->{id};

		UPDATE members SET
			membertype= '$_[1]',
			operator= '$ME',
			memo= 'removal/downgrade action initiated by: $meminfo->{alias}'
		WHERE 	id= $_[0]->{id}");

	return ($rv eq '1') ? 1 : undef;
}

sub Simple_Update
{
	return 1 if $NOSQL;

	my $rv = $db->do("
		UPDATE members SET
			mail_option_lvl= $_[1],
			operator= '$ME',
			memo= 'mail option change initiated by $meminfo->{alias}'
		WHERE id= $_[0]");

	return ($rv eq '1') ? 1 : undef;
}

sub UplineCK
{
	my $ck_id = shift;
	my $loop_id = $ck_id;
	my $sth = $db->prepare("SELECT spid FROM members WHERE id= ?");
	my $err = ();
			
	while ($loop_id != $id)
	{
		$sth->execute($loop_id);
		$loop_id = $sth->fetchrow_array;
		unless ($loop_id)
		{
			$err = 'Upline Search Failed';
			last;
		}
		elsif ($ck_id == $loop_id)
		{
			$err = 'Loop Detected!';
			last;
		}
		elsif ($loop_id == 1 && $id != 1)
		{
			$err = -1;
			last;
		}
	}

	$sth->finish;
	return $err;	
}

# released 02/23/04
# 04/26/05 fixed single quoting of vars in Errs
###### 06/03/05 added the 'restore' functionality
###### and modularized the coding in the Process function
# 05/15/07 added a bypass to not save the report when done by 01
# 06/12/07 added a bypass to not limit a batch size for 01
# 06/26/07 added the ability to process aliases and IDs as well as to perform downgrades and phone number expunging
# 09/16/08 revised the list processor to remove leading and trailing whitespace from the list items themselves
# (this is in addition to the whitespace filtering we had been doing on the list as a whole)