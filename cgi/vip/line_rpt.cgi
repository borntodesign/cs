#!/usr/bin/perl -w
###### line_rpt.cgi	Member Line Report
###### Created: 11/25/03 Karl Kohrt
###### This will output a list of members between the requesting VIP
######		and a specific member in their downline
######		using a field and a value supplied by the VIP.
######
###### Admin access is of the form:
######		http://www.clubshop.com/cgi/line_rpt.cgi?admin=1&rid=<requesting member id>&mid=<requested member id>
######
###### last modified: 05/13/15 Bill MacArthur

use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use strict;
use lib ('/home/httpd/cgi-lib');
use XML::local_utils;
use G_S qw($LOGIN_URL);
use GI;
use DB_Connect;
require cs_admin;
require ADMIN_DB;
binmode STDOUT, ":encoding(utf8)";

###### in order to accept params included in a path, like when we are coming in from a redirect
###### we'll assign the params from the path info, that way we don't have to rewrite because of calls to q->param
###### this of course overwrites any existing params in a query string, but there shouldn't be any anyway
($ENV{'QUERY_STRING'} = $ENV{'PATH_INFO'}) =~ s#/## if $ENV{'PATH_INFO'};

###### CONSTANTS
my %TBL = (
 	'members'	=> 'members',
	'a_level'	=> 'a_level',
	'a_values'	=> 'level_values'
);

my %TMPL = (
	'xml'		=> 10912,
	'report' 	=> 10238,
	'search' 	=> 10239
);

###### GLOBALS
my ($db, $id, $meminfo, $lang_blocks) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'cgi'=>$q});
my $TT = Template->new();
my $ME = $q->url;
my $Rid = '';
my $tbl = '';
my $count = 0;
my $first_one = 1;
my $text_list = '';
my $email_list = '';
my $alias = '';
my $firstname = '';
my $lastname = '';
my $ADMIN = $q->param('admin');
my $Mid = $q->param('mid') || '';				# The member info we are searching for.
my $search_field = $q->param('search_field') || '';	# The field we are searching by.
my @List = ();						# Duplicate ID check list.	
my $results = '';						# The initial ID check result.
my $results_count = 0;
my $current_id = '';

# We want to allow transparent access for HQ use without logging in as the VIP
my $adminCK = $q->cookie('cs_admin');
if ($ADMIN && $adminCK)
{
	my $cs = cs_admin->new();
	my $admin = $cs->authenticate($adminCK);

	exit unless $db = ADMIN_DB::DB_Connect( 'line_rpt.cgi', $admin->{'username'}, $admin->{'password'} );

	$Rid = $q->param('rid') || '';		# The VIP we are Reporting on
	# The Rid shouldn't contain any letters
	if($Rid =~ /\D/)
	{
		Err("Bad Rid passed: $Rid<br />This report works only on numeric IDs.");
	}
}
else
{
	# we'll first verify the identity of our user
	($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

	# a valid id will not have anything besides digits
	# if it matches non-digits it may just be an expired cookie
	if ( !$id || $id =~ /\D/ )
	{
		my $return_link = "$LOGIN_URL?destination=$ME/" . CGI::escape($q->query_string());
		print $q->redirect($return_link);
		exit;
	}

	# Make sure they are at a high enough level to access the report.
	elsif ( $meminfo->{'membertype'} ne 'v' )
	{
		Err( $lang_blocks->{'err_not4u'} );
	}
	else
	{
		exit unless $db = DB_Connect('line_rpt.cgi');
		$Rid ||= $id;			# Assign the cookie value.
	}
}

LoadLangBlocks();

my $GI = GI->new('cgi'=>$q, 'db'=>$db);

my $MBR_DETAIL_SCRIPT = ();
my $NuWin = 'nuDetailWin(this)';

$MBR_DETAIL_SCRIPT .= 'https://www.clubshop.com/cgi/vip/member-followup.cgi';
$NuWin = 'nuwin(this)';	# this will open up a whole new window instead of the small window

# Unless we have a value to search for.
unless ( $Mid )
{
	# Display the form to enter a Member id.
	Do_Page('search');
}	
# If we have a value to search for.
else
{
	# Remove any leading or trailing white space.
	$Mid =~ s/(^\s*|\s*$)//g;

	# If the ID field was selected or we came in from admin with no search field.		
	if ( $search_field eq "id" || $search_field eq "" )
	{
		# If we have an alphanumeric Mid (alias)
		if ( $Mid =~ /\D/ )
		{
			# Query the corresponding numeric ID.
			$Mid = uc $Mid ;
			my $sth = $db->prepare("
				SELECT lpad(id::TEXT,7,'0')
				FROM members WHERE alias= ?");
			$results_count = $sth->execute($Mid);
			$results = $sth->fetchrow_array;
			$sth->finish;
		}
		# Otherwise, put the ID in the $results variable for later assignment.
		else
		{
			$results  = $Mid;
			$results_count = 1;
		}
	}
	# Otherwise, build a query using the field and info provided by the VIP.
	else
	{
		my $qry= "	SELECT lpad(id::TEXT,7,'0') as id, firstname, lastname
				FROM members WHERE $search_field = ?";		
		my $sth = $db->prepare($qry);
		$results_count = $sth->execute(lc $Mid);

		# If we found only one match, assign it directly to $results.
		if ( $results_count == 1 )
		{
			my $tmp = $sth->fetchrow_hashref;
			$results = $tmp->{id};
		}
		# Otherwise, put all of the results in a list.
		else
		{						
			while ($results = $sth->fetchrow_hashref)
			{ 	
				push (@List, $results);
			}
		}											
		$sth->finish;
	}	

	# If we ended up with a single, valid ID to search by, assign it to our ID holder.
	# Then let's build the list to be put out to the page.
	if ( $results_count == 1 )
	{
		$Mid = $results;
		Create_Line_List();		# Create the upline list.

		# If the last ID in the upline list matches that of the requestor,
		# then the requested member was in their downline.				
		if ( $current_id == $Rid )
		{
			# Display the report page.
			Do_Page('report');
		}
		# If they requested a member ID not in their downline.	
		elsif ( $current_id == 1 )
		{
			Err($gs->ParseString($lang_blocks->{'err_not_downline'}, $Mid) . TryAgain() );
		}						
		# If they requested a non-existent member ID.	
		elsif ( $current_id == 0 )
		{
			Err( $gs->ParseString($lang_blocks->{'err_mid_not_valid'}, $Mid) . TryAgain() );
		}						
		else
		{
		# this case is indicative of an failure to traverse the network upline
			Err("$lang_blocks->{'err_search_fail'}<br /><table>$tbl</table>");
		}
	}
	# If we ended up with a more than one valid ID to search by, build a display list.
	elsif ( $results_count > 1 )
	{
		Create_Selection_List();		# Create and display the list of matching members.
	}
	# Otherwise, report the bad search value.	
	else
	{ 
		Err( $gs->ParseString($lang_blocks->{'err_not_found'}, $search_field, $Mid) . TryAgain() );
	}
}

Exit();

##############################
###### Subroutines start here.
##############################


sub Create_Hidden_Admin
{
	if ($ADMIN)
	{
		$ADMIN = $q->hidden('-name'=>'admin', '-value'=>1);
		$ADMIN .= $q->hidden('-name'=>'rid', '-value'=>$Rid);
	}
	else
	{
		 $ADMIN = '';
	}
	return $ADMIN;
}

###### Create the list of members in the line.
sub Create_Line_List
{
	$db->{'RaiseError'} = 1;			# Let's report DB errors on the spot
	($current_id = $Mid) =~ s/\D//g;		# Start with the original requested ID.
	my $class = 'w';				# Start with a white background for our info row.

	# Prepare the query once.
	###### rather than pulling in all the data (*, the whole row),
	###### we'll pull in exactly what we need
	my $qry = "	SELECT m.id,
				m.spid,
				m.alias,
				m.firstname,
				m.lastname,
				m.membertype,
				mt.display_code,
				m.emailaddress,
				COALESCE(m.country,'') AS country,
				COALESCE(m.language_pref,'') AS language_pref,
				v.description
			FROM 	$TBL{'members'} m
			JOIN member_types mt
				ON mt.code=m.membertype
			LEFT JOIN $TBL{'a_level'} a ON m.id = a.id
			LEFT JOIN $TBL{'a_values'} v ON v.lvalue = a.a_level
			WHERE 	m.id = ?";
	my $sth = $db->prepare($qry);

	# Until we find the requesting member's ID or the #1 ID, we'll keep going.
	while ( $current_id != $Rid 
		&& $current_id > 1 
		&& $current_id != 0 )
	{
		my $row = '';
		$sth->execute($current_id);
		my $tmp = $sth->fetchrow_hashref();
		# If we found some member info, add it to the output list.
		if ( $tmp->{'id'} )
		{
			unless( grep $_ eq $tmp->{'membertype'}, (qw/ma/))	# exclude the merchant affiliate listings
			{

				# Put a newline before every line except the first.
				$text_list .= "\n" unless $first_one;
				$email_list .= "\n" unless $first_one;
				$count++;
				foreach (keys %{$tmp})
				{
					unless (defined $tmp->{$_} && $tmp->{$_} ne '')
					{
						$tmp->{$_} = '&nbsp;';
					}
				}
				$row .= qq#
					<tr class="$class">
					<td><a href="http://www.clubshop.com/cgi/vip/member-followup.cgi?rid=$tmp->{'id'}" onclick="return nuwin(this)">$tmp->{'alias'}</a></td>
					<td align="center">$tmp->{'display_code'}</td>
					<td><a href="$MBR_DETAIL_SCRIPT?refid=$tmp->{'id'}" onclick="return $NuWin">$tmp->{'firstname'} $tmp->{'lastname'}</a></td>
					#;
	
				if ($tmp->{'emailaddress'} =~ m/@/)
				{
					$row .= qq!
						<td>
						<a href="mailto:$tmp->{'emailaddress'}">$tmp->{'emailaddress'}</a>
						</td>\n!;
				}
				else
				{
					$row .= qq#<td>$tmp->{'emailaddress'}</td>#;
				}
	
				$row .= qq#
					<td align="center">$tmp->{'country'}</td>

					<td>$tmp->{'description'}</td>

					</tr>
				#;
	
				# Put the new row on the top of the list.
				$tbl = $row . $tbl;
				$text_list .= "$tmp->{'alias'},$tmp->{'firstname'},$tmp->{'lastname'},$tmp->{'emailaddress'},$tmp->{'country'},$tmp->{'language_pref'}";
				$email_list .= "$tmp->{'emailaddress'},";
				
				$class = ($class eq 'w') ? ('g') : ('w');		# Alternate the row's color.
	
				# The first time through will be the requested member's info.
				# Save it for later display.
				if ($first_one)
				{
					$alias = $tmp->{'alias'};
					$firstname = $tmp->{'firstname'};
					$lastname = $tmp->{'lastname'};
					$first_one = 0;
				}
			}
			$current_id = $tmp->{'spid'};
			if ( Dupe_Ck($current_id) )
			{
				$tbl = "<tr><td colspan=\"3\">Loop detected! Search terminated!</td></tr>" . $tbl;
				last;
			}
			else { push (@List, $current_id) }
		}
		else
		{
			$current_id = 0;
		}
	}
	$sth->finish;
		
	# chop the trailing comma off the email list
	chop($email_list);
}

###### Create the list of matching members to select from.
sub Create_Selection_List
{
	my $tbl = qq|
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<html>
		<head><title></title>

		<style TYPE="text/css">    
		<!--
		.w{
			background-color: white;
		}
		.g{
			background-color: silver;
		}
		th{
			font-size: 10pt;
			font-family: Arial, Helvetica, sans-serif;
			font-weight: normal;
		}

		td{
			font-size: 9pt;
			font-family: Arial, Helvetica, sans-serif;
			font-weight: normal;
		}
		-->
		</style>
		<body bgcolor="#ffffff">

		<div align="center">
		<h2>$lang_blocks->{'searchRez'}</h2>$lang_blocks->{'more_details'}</div><p>
			<table align="center" border="0" cellspacing="0" cellpadding="2">
 				<tr>
				<td><b> $lang_blocks->{'member_id'} </b></td>
			<td><b> $lang_blocks->{'firstname'} </b></td>
			<td><b> $lang_blocks->{'lastname'} </b></td>
			</tr>|;


	my $row = '';
	my $class = 'w';		# Start with a white background for our row.

	foreach my $tmp (@List)
	{
		# If the member found is in the requestor's downline.
		if ( $gs->Check_in_downline($db, 'members', $Rid, $tmp->{'id'}) )
		{		
			$row = '';
			# Add the member info to the output list.
			foreach (keys %{$tmp})
			{
				unless (defined $tmp->{$_} && $tmp->{$_} ne '')
				{
					$tmp->{$_} = '&nbsp;';
				}
			}
			$row .= qq#	<tr class="$class">
					<td><a href="$ME?mid=$tmp->{id}">$tmp->{id}</a></td>
					<td>$tmp->{firstname}</td>
					<td>$tmp->{lastname}</td>
					</tr>#;

			# Put the new row on the top of the list.
			$tbl .= $row;
			$class = ($class eq 'w') ? ('g') : ('w');		# Alternate the row's color.
		}																					
	}
	# Tack on the 'New Search' button here.
	$tbl .= qq!	</table>
			<div align="center">
			<form action="$ME">
			<input type="submit" value="$lang_blocks->{'new_search'}">
			</form>
			</div>!;

	Wrap($tbl);	
}

###### Display the report page.
sub Do_Page
{
	my $which = shift;	
	my $_tmpl = $gs->GetObject($TMPL{$which}) || die "Failed to load template $TMPL{$which}";

	my $dat = {
		'ME'=>$ME,
		'HIDDEN_ADMIN'=>Create_Hidden_Admin(),
		'lang_blocks' => $lang_blocks
	};

	# Do the substitutions on the report page.
	if ( $which eq 'report' )
	{
		$dat->{'alias'} = $alias;
		$dat->{'firstname'} = $firstname;
		$dat->{'lastname'} = $lastname;
		$dat->{'TABLE'} = $tbl;
		$dat->{'TEXT_LIST'} = $text_list;
		$dat->{'EMAIL_LIST'} = $email_list;
		$dat->{'URL'} = $q->script_name();
		$dat->{'Rid'} = $Rid;
		$dat->{'COUNT'} = $count;
	}													

	my $tmpl = ();
	my $rv = $TT->process(\$_tmpl, $dat, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}
	Wrap($tmpl);
}											

###### Check for a duplicated member ID
sub Dupe_Ck
{
	my $id = shift;
	foreach (@List){ return 1 if $id eq $_ }
	return 0;
}

###### Display an error message to the browser.											
sub Err
{
	$db ||= DB_Connect('line_rpt.cgi');
	LoadLangBlocks() unless $lang_blocks;
	Wrap($q->h4({'-style'=>'margin-top:0'}, $_[0]) . '<br /><br />Current server time: <tt>' . scalar localtime() . ' PST.</tt>');
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub LoadLangBlocks
{
	$lang_blocks = $gs->GetObject($TMPL{'xml'}) || die "Failed to retrieve language pack template\n";
	$lang_blocks = encode_utf8($lang_blocks);
	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

sub TryAgain
{
	return qq| <a href="javascript:history.back()">$lang_blocks->{'err_try_again'}</a>|
}

sub Wrap
{
	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_blocks->{'title'},
			'style'=>$q->style({'-type'=>'text/css'}, '
				.nav_footer_grey:link {
				    color: #666666;
				    font-size: 10px;
				    text-decoration: none;
				}
				.nav_footer_grey:visited {
				    color: #666666;
				    font-size: 10px;
				    text-decoration: none;
				}
				.nav_footer_grey:hover {
				    color: #CCCCCC;
				    font-size: 10px;
				    text-decoration: underline;
				}
				.nav_footer_grey:active {
				    color: #666666;
				    font-size: 10px;
				    text-decoration: none;
				}'),
			'content'=>\$_[0]
		});
}

###### 12/16/03 Added Dupe_Ck to check for an infinite looping of member ID's
######			Added '$current_id != 0' to the while loop in case of bad ID.
###### changed the subject line in the mailto anchor to place in the upline's name
###### instead of member ID
###### 03/01/04 adjusted the list contents to add the alias and break down the name
###### into first/last components
###### 03/18/04 Modified main code to allow for searches by fields other than ID.
######		Added the sub Create_Selection_List to display a list if > 1 match found.
######		Moved much of the main code into a new sub called Create_Line_List.
###### 03/29/04 tweaked the Create Sel list routine to render a complete HTML doc
###### 04/06/04 repointed the correct templates instead of the test ones
###### 04/22/04 tweaked the javascript links on the 'error' reports, also lower cased
###### email address parameters before doing the lookup				
###### 04/27/04 fixed an edge case where there the script bombed
###### due to a broken line of sponsorship
###### 05/27/04 added the extra column for the 'role'
###### 06/09/04 changed the nbteam 'table' to nbteam_vw to exclude inactive roles
###### 01/19/05 changed the bad login screen to a simple redirect to avoid browser
###### caching of the error
# 10/12/06 added utf-8 charset to the headers
# 12/07/06 formatted member name as a link that opens a member detail window
# 04/14/09 added the country and language_pref to the text list
###### 06/12/09 added an exclusion from the listing generator to ignore the new ma membertype
###### 07/08/09 revised the link that is provided on the name based on the role
# 02/09/2011 g.baker postgres 9.0 port lpad(text,int,text) casts
# 03/10/11 added the country to the report listings
###### 01/06/12 lots of revisions, including making this report translatable and moving to Template
###### 01/21/14 revised the member followup destination to be CS instead of GI
# 05/13/15 just added the binmode to deal with the recent upgrade of DBD::Pg