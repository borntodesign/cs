#!/usr/bin/perl -w
###### auction.cgi
###### handle auction bids
###### created: 12/05/07	Bill MacArthur
###### last modified: 12/03/09	Bill MacArthur

$| = 1;

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S qw($LOGIN_URL);
use DB_Connect;
use Business::CreditCard;

my $q = new CGI;
my $AuctionIncrement = 1;	# the amount that a bid must exceed a previous bid

###### run only in SSL mode
unless ($q->https){
	my $url = $q->self_url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
###### why not just reload the form with a message and their bid in place
my @param_list = qw/action cvv2 Expiration Card_Type2 card_name card_number card_type method amount auction_id/;
my %params = ();
LoadParams();
die "auction_id parameter not received\n" unless $params{auction_id};

#my $redirect_URL = 'https://www.clubshop.com/coop_thankyou.html';
my $redirect_URL = $q->url . "?bid_successful=1;auction_id=$params{auction_id}";

our %TMPL = (
	main	=> 10581
);

our ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("Login required <a href=\"$LOGIN_URL\">$LOGIN_URL</a>");
	exit;
}

###### if they are not a VIP, they don't have access to this interface
if ( $meminfo->{membertype} !~ 'v' )
{
	print 	$q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h3('This facility is for VIP members only'),
		$q->end_html();
		exit;
}

exit unless my $db = DB_Connect('auction.cgi');
$db->{RaiseError} = 1;
goto 'END' unless my $auction = GetAuction();

###### are they excluded from bidding?
if (Excluded()){
	Alert('You are currently excluded from participating in auctions.
		Please <a href="/contact.xml">contact us</a> for more information.');
}
elsif (! OrganizationCK()){
	###### alerts taken care of in the sub
}
elsif (! $params{action})
{
	my $tmpl = G_S::Get_Object($db, $TMPL{main}) || die "Failed to load template: $TMPL{main}";

	$tmpl =~ s/%%bidder_id%%/$meminfo->{alias}/;
	$tmpl =~ s/%%form_action%%/$q->script_name/eg;
	$tmpl =~ s/%%auction_id%%/$auction->{auction_id}/;
	$tmpl =~ s/%%auction_end_text%%/$auction->{auction_end_text}/;
	$tmpl =~ s/%%auction_description%%/$auction->{description}/;
	$tmpl =~ s/%%high_bid%%/$auction->{high_bid}/;
	$tmpl =~ s/%%high_bidder_alias%%/$auction->{high_bidder_alias}/;
	$tmpl =~ s/<!--%%bid_successful%%-->/BidSuccessful()/e;
	print $q->header(-charset=>'utf-8', -expires=>'now'), $tmpl;
}
elsif ($params{action} eq 'bid')
{
	unless ($params{amount}){
		Alert('Bid amount missing');
		goto 'END';
	}
# 	if ($auction->{organization_id}){	###### they must be part of this organization to participate
# 		unless ($db->selectrow_array("
# 			SELECT upline FROM relationships WHERE id= ? AND upline= $auction->{organization_id}", undef, $id))
# 		{
# 			my $org = $db->selectrow_hashref("
# 				SELECT alias, (firstname ||' '|| lastname) AS name
# 				FROM members WHERE id= $auction->{organization_id}");
# 			Alert("You must be part of the $org->{name} organization to participate in this auction.");
# 			goto 'END';
# 		}
# 	}
	if ($params{amount} < ($auction->{high_bid} + $AuctionIncrement)){
		Alert("Your bid ($params{amount}) is not \$$AuctionIncrement greater than the current high bid: $auction->{high_bid}");
		goto 'END'
	}
	if ($params{method} eq 'Club Account'){
		###### do they really have the necessary funds in their account?
		my ($rv) = $db->selectrow_array('SELECT SUM(amount) FROM soa WHERE void=FALSE AND id= ?', undef, $id);
		if ($rv < $params{amount}){
			Alert("Your Club Account balance ( $rv ) is insufficient to cover your bid: $params{amount}");
			goto 'END';
		}
	} else {
		###### make sure they submitted all the other info for credit card
		die "Credit Card Name is missing\n" unless $params{card_name};
		die "Credit Card Number is missing\n" unless $params{card_number};
		die "Card expiration is missing\n" unless $params{Expiration};
	}
	if (Record()){
		print $q->redirect($redirect_URL);
	} else {
		Err('There was an error while processing your bid');
	}
}
else
{
	Err('Undefined Action.');
}

END:
$db->disconnect;
exit;

###### ###### Start of Sub-Routines

sub Alert
{
	print $q->header(-charset=>'utf-8'), $q->start_html(-encoding=>'utf-8');
	print $q->div($_[0]), $q->end_html;
}

sub BidSuccessful
{
	return '' unless $q->param('bid_successful');
	return $q->p({-style=>'color:green; font-weight:bold; text-align:center;'}, 'Your bid was successful');
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Excluded
{
	return $db->selectrow_array('SELECT id FROM auction_bidders_excluded WHERE active=TRUE AND id= ?', undef, $id);
}

sub GetAuction
{
	my $lock = shift;
	my $qry = "
		SELECT
			CASE WHEN NOW() <= auction_start THEN FALSE ELSE TRUE END AS started,
			CASE WHEN NOW() > auction_end THEN TRUE ELSE FALSE END AS ended,
			auction_id,
			description,
			notes,
			high_bid_id,
			COALESCE(high_bidder_alias,'') AS high_bidder_alias,
			auction_start,
			auction_end,
			high_bid_stamp,
			organization_id,
			auction_end_text,
			high_bid,
			pay_trans_id,
			void
		FROM auctions
		WHERE auction_id= ?";
	$qry .= ' FOR UPDATE' if $lock;
	my $rv = $db->selectrow_hashref($qry, undef, $params{auction_id});
	unless ($rv->{auction_id}){
		Err('Invalid auction_id');
		return undef;
	}
	elsif ($rv->{ended} || $rv->{pay_trans_id} || $rv->{void}){
		Alert('This auction has ended');
		return undef;
	}
	elsif (! $rv->{started}){
		Alert('This auction has not started');
		return undef;
	}
	return $rv;
}

sub LoadParams
{
	foreach (@param_list){
		$params{$_} = G_S::PreProcess_Input($q->param($_) || '');
	}
	###### some rudimentary checks
	if ($params{card_number}){
		###### get rid of extraneous stuff
		$params{card_number} =~ s/\s|-|\.//g;
		die "Invalid card number: $params{card_number}\n" if $params{card_number} =~ m/\D/;
		die "Card number appears to be invalid: $params{card_number}\n" unless validate($params{card_number});
	}
	$params{amount} =~ s/,/./g;
	$params{amount} = int($params{amount} || 0);
	die "Invalid auction_id parameter: $params{auction_id}\n" if $params{auction_id} =~ m/\D/;
}		

sub OrganizationCK
{
	return 1 unless $auction->{organization_id};
	return 1 if $id == $auction->{organization_id};	###### just in case they are checking ;)
	unless ($db->selectrow_array("
		SELECT upline FROM relationships WHERE id= ? AND upline= $auction->{organization_id}", undef, $id))
	{
		my $org = $db->selectrow_hashref("
			SELECT alias, (firstname ||' '|| lastname) AS name
			FROM members WHERE id= $auction->{organization_id}");
		Alert("You must be part of the $org->{name} organization to participate in this auction.");
		return undef;
	}
	return 1;
}

sub Record
{
	my $bidder_info = "Bidder ID: $meminfo->{alias}
Bid Amount: $params{amount}
Payment Type: $params{method}
";
	$bidder_info .= "Credit Card Name: $params{card_name}
Card Type: $params{card_type}
Card Number: $params{card_number}
CVV2: $params{cvv2}
Card Expiry: $params{Expiration}" if $params{card_number};

	my ($bid_id) = $db->selectrow_array("SELECT NEXTVAL('auction_bids_bid_id_seq')");
	my $rv = $db->do("
		INSERT INTO auction_bids (bid_id, auction_id, bidder_id, bid_amount, bidder_info)
		VALUES ($bid_id, $auction->{auction_id},?,?,?);
		
		UPDATE auctions SET high_bid=  ?, high_bid_id = $bid_id, high_bidder_alias= ?, high_bid_stamp=NOW()
		WHERE auction_id= $auction->{auction_id};", undef,
		($meminfo->{id}, $params{amount}, $bidder_info, $params{amount}, $meminfo->{alias}));
	die "SQL returned: $rv\n" unless $rv eq '1';
	return 1;
}				

# 09/25/09 revised the query in Get_Auction to explicitly define the columns, including a coalesce to avoid an unit'd var error
# 11/09/09 added handling in Get_Auction to recognize voided or paid auctions as well as ones that have ended
