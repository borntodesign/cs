#!/usr/bin/perl 
###### personal-stats-forCC.cgi
###### called by a script tag in the VIP Control Center, simply returns a few stats for a given VIP
# created: 12/20/11	Bill MacArthur
###### last modified:

use CGI;
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;

my $q = new CGI;
my $rv = {
	'op'=>0,	# Organizational Partners
	'prp'=>0	# Personally Referred Partners
};

###### GLOBALS

exit unless our $db = DB_Connect::DB_Connect('generic');

my ($id, $meminfo) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
if ( $id && $id !~ /\D/ && $meminfo->{'membertype'} eq 'v' )
{
	($rv->{'op'}) = $db->selectrow_array('SELECT od.onekplus(oq) FROM od.v WHERE id=?', undef, $id) || 0;
	($rv->{'prp'}) = $db->selectrow_array('SELECT cnt FROM my_prp_count WHERE id=?', undef, $id) || 0;
}
print $q->header('-type'=>'text/javascript');
print "var my_op='$rv->{'op'}'; var my_prp='$rv->{'prp'}';";

$db->disconnect;
exit;
