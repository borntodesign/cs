#!/usr/bin/perl -w
###### mv-members.cgi
###### the member, partner, VIP, what have you transfer interface
###### newly created: Dec. 2010	Bill MacArthur
###### last modified: 05/12/16	Bill MacArthur

use strict;
use Digest::MD5 qw( md5_hex );
use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use lib ('/home/httpd/cgi-lib');
require ADMIN_DB;
use G_S qw($LOGIN_URL);
use DB_Connect;
use Template;
use GI;
use XML::local_utils;
use Admin::Masquerade;

#use Data::Dumper;	# only for debugging

my %DBtbls = (
	'depths'	=> 'depths',
	'members'	=> 'members',
	'my_coach'	=> 'my_coach',
	'activity_rpt_data'	=> 'activity_rpt_data',
	'backend_processing'	=> 'backend_processing',
	'vip_transfers_log'	=> 'vip_transfers_log'
);
my $defActivityType = 22;		# when we create an activity record for this transfer, this will be it's type
my $tpXferActivityType = 70;	# the type we will use for transfers of Trial Partners
my $smXferActivityType = 71;	# the type we will use for transfers of Members/Affiliates

my ($admin, $db, $realOP, %param, @mvid, $lang_blocks, @alerts, @lp, @countries, $cSpid) = ();
my $MANAGED = 0;

my $SESSKEY = 'mid';	# the session key that will be used to indicate the "managed ID" when in coaching mode
my $Session = CGI::Session->new();
my $masq = Admin::Masquerade->new('Session'=>$Session);

my $lu = XML::local_utils->new;
my $TT = Template->new({'INCLUDE_PATH' => '/home/httpd/cgi-templates/TT/mv-members'});
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});

my $Path = $q->path_info || '';
my $ME = $q->script_name;
my $ME_Path = $q->script_name . $Path;
$Path =~ s#^/##;

my $key = 'onykey!23#';	###### for salting the recipient hash
my $LIMIT = 25;			###### the default number of results we will return for a list

my %TMPL = (
	'no-pay'	=> 1319,
	'Error'		=> 1307,
	'screen1'	=> 1322,
	'XML'		=> 1323,
	'screen2'	=> 1324,
	'screen3-partner'	=> 1327,
	'screen3-tp'	=> 1329,
	'screen3'	=> 1325,
	'screen4'	=> 1326,
	'Current Sponsor Selection' => 1328,
	'list-*'	=> 'mv-members.Genericlist.tt',
	'list-tp'	=> 'mv-members.TPlist.tt'
);

###### these globals pertain to an actual transfer
#my ($firstID, $lastID, $transfer_list, $proc_status) = '';
my ($lastID, $transfer_list, $proc_status) = '';

my $recipient = {};
my $activity = $q->param('activity') || '';
my $formlist = \'';

Load_Params();

###### evaluate our user
###### we will first check for admin credentials
###### if present, we will go in as 01 with 'super credentials'
###### a regular login as 01 will permit the same operations
if ((my $operator = $q->cookie('operator')) && (my $pwd = $q->cookie('pwd')) && ! $masq->Active)
{
	exit unless $db = ADMIN_DB::DB_Connect( 'mv-members', $operator, $pwd );
	$db->{'RaiseError'} = 1;
	$db->{'pg_enable_utf8'} = 0;
	$realOP = {
		'id'		=> 1,
		'alias'		=> 'DC00001',
		'firstname'	=> $operator,
		'lastname'	=> 'The DHS Club',
		'emailaddress'	=> '',
		'spid'		=> 1,
		'membertype'	=> 'v',
		'lang_pref'	=> 'en',
		'isacoach' => 1
	};
	$admin = 1;
}
else
{
	( my $id, $realOP ) = $masq->authen_ses_key;
	( $id, $realOP ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') ) unless $id;

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ((! $id) || $id =~ /\D/ )
	{
		Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "/$Path\">$LOGIN_URL</a>");
	}

	###### since admin use is above, we will prohibit 01 use without it
	###### this will keep ex-employees who know the login credentials from
	###### being able to access this function
	if ($id == 1)
	{
		Err('To use this interface as 01, you must be logged into the database.');
	}

	exit unless $db = DB_Connect('mv-members', 1, {'enable_lockout'=>1});
	$db->{'RaiseError'} = 1;
	$db->{'pg_enable_utf8'} = 0;
	unless ($realOP->{'membertype'} eq 'v')
	{
		Err('This resource is for the exclusive use of Partners.');
	}

	###### exclude NOPs from the application	10/25/14 - Dick: As Coaches are unable to make transfers for them, enable Partners with unpaid fees to be able to make transfers.
#	my $sth = $db->prepare("SELECT id FROM nop WHERE id= $id");
#	$sth->execute;
#	$id = $sth->fetchrow_array;
#	$sth->finish;
#	if ($id)
#	{
#		Err('', $TMPL{'no-pay'});
#	}
	
	$realOP->{'lang_pref'} = $gs->Get_LangPref();
	($realOP->{'isacoach'}) = $db->selectrow_array("SELECT TRUE FROM $DBtbls{'my_coach'} WHERE upline= $realOP->{'id'} LIMIT 1");
}

Load_lang_blocks();

if ($realOP->{'isacoach'} && $Path eq 'coach')
{
	if (! $activity)
	{
#	push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'managed_party_prompt'});

		# we are starting fresh, so let's clear the old session parameter (but not expunge the session as it may be in use for other parameters)
		$Session->clear($SESSKEY);
#		$Session->delete();
		$Session->flush();
		
		Do_Page( $TMPL{'Current Sponsor Selection'} );
	}
	elsif ($activity eq 'setcspid')
	{
		# we should have selected a current sponsor and if so, we can initialize a session
		if (my $mid = $q->param('mid'))
		{
			# the mid parameter should be numeric.. if it is not, we'll just skip creating the session
			if ($mid !~ m/\D/)
			{
				if (my $mh = Can_Manage_Current_Spid($mid))
				{
					$Session->param($SESSKEY, $mh);
					$cSpid = $mh;
					$activity = '';	# reset this so that we will fall into the recipient selection
					$MANAGED = 1;
				}
				else
				{
					push @alerts, $q->div({'-class'=>'note_nop'}, "$lang_blocks->{'mgt_auth_error'}: $mid");
					Do_Page( $TMPL{'Current Sponsor Selection'} );
				}
			}
			else
			{
				push @alerts, $q->div({'-class'=>'note_nop'}, $lang_blocks->{'select_managed_party'});
				Do_Page( $TMPL{'Current Sponsor Selection'} );
			}
		}
	}
	else
	{
		$cSpid = $Session->param($SESSKEY);
		$MANAGED = 1;
	}
}

# we will differentiate as we go along, between the real operator and the current sponsor
$cSpid ||= $realOP;

if (! $activity )
{
	push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'recipient_prompt'});
	Do_Page( $TMPL{'screen1'} );
}

###### the same goes here
###### unless we need different content based on transfer type
###### we will just present our form with the proper block showing for this step						
unless ($param{'pd'})
{
	push @alerts, $q->div({'-class'=>'note_nop'}, $lang_blocks->{'select_recipient'});
	Do_Page($TMPL{'screen1'});
	Exit();
}
elsif (! $param{'pd_hash'})
{
	###### verify the recipient
	unless (Validate_Recipient())
	{
		Do_Page( $TMPL{'screen1'} );
		Exit();
	}
}

###### we should have a pd_hash for the activities beyond this point
unless (Validate_Pid($param{'pd_hash'}))
{
	Err('PD hash failed to validate');
	Exit();
}
else
{
	$recipient = Generate_Pid_Hash($param{'pd_hash'});
	if ( $param{'pd'} !~ /$recipient->{'id'}|$recipient->{'alias'}/ )
	{
		Err('PD hash doesn`t match PD');
		Exit();
	}
}

if ($activity eq 'start')
{
	Do_Page($TMPL{'screen2'}, {'title'=>'title2', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'cboLP'=>cboLP(), 'cboCountries'=>cboCountries()}});
}
elsif ($activity eq 'getlist')
{
	my $tmpl = ();
	if ($param{'transfer_type'} eq 'v')
	{
		$tmpl = $TMPL{'screen3-partner'};
	}
	elsif ($param{'transfer_type'} eq 'tp')
	{
		$tmpl = $TMPL{'screen3-tp'};
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'tp_shown_below'});
	}
	else
	{
		$tmpl = $TMPL{'screen3'};
	}
	
	Do_Page($tmpl, {'title'=>'title3', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'list' => Create_List($param{'transfer_type'})}});
}
elsif ($activity eq 'transfer')
{
	my $rv = ();
	if (Pre_Transfer_Checks())
	{
		# if we are transferring Trial Partners, we have the option of transferring one or more directly to the recipient, or creating a stack from the list
		if ($param{'transfer_type'} eq 'tp' && $param{'xfer_type'} eq 'stacking')
		{
			$rv = Transfer_Stack();
		}
		else
		{
			$rv = Transfer_Group();
		}
	}
	else
	{
		Exit();
	}
	
	if ($rv)
	{
		Update_Transfer_SEQ();
	}

	$proc_status .= 'Job finished.';
	Do_Page(
		$TMPL{'screen4'},
		{'title'=>'title4', 'data'=>{'ctlOptions' => TransferTypeOptions(), 'proc_status'=>$proc_status}}
	);
}
else
{
	Err('Invalid activity received.');
}

Exit();

###### start of subroutines

sub Can_Manage_Current_Spid
{
	my $id = shift;
	
	# first off, make sure that the user is the coach of the ID
	my ($rv) = $db->selectrow_array("SELECT upline FROM $DBtbls{'my_coach'} WHERE id= $id AND upline= $realOP->{'id'}");
	return () unless $rv;
	
	# now make sure that the ID is a Partner (or Trial Partner per Dick 09/15/14) and return that ID
	return $db->selectrow_hashref("SELECT id, alias, firstname||' '||lastname AS name FROM $DBtbls{'members'} WHERE membertype IN ('v','tp') AND id= $id");
}

sub CanSponsor
{
	my $mtype = shift;
	my %a = ();

	# determine if and who I can sponsor
	# the idea is that if I can login then I can see what membertypes I can sponsor
	# then those will be filtered on who can login of those
# the rules have changed... 01/12
# instead there is a precise ability defined that I prefer to not record in the DB

# per Dick 02/12/14: Bill - we really need to update the Transfer Function to allow for the transfer of Trial Partners to downline TPs or Partners.
	my $labels = $db->selectall_hashref('
		SELECT mt."code", mt.label
		FROM member_types mt', 'code');
	my %perms = (
		'v'=>[ qw/v ma ag m s tp/ ],
		'm'=>[ qw/s/ ],
		'tp'=>[ qw/m s tp v/ ]	# Dick 08/05/14: We need to allow the Transfer Function to be able to transfer a (new) Partner to a TP... 
	);

	foreach my $tmp (@{$perms{$mtype}})
	{
		$a{$tmp} = {'can_sponsor'=>$tmp, 'label'=>$labels->{$tmp}->{'label'}};
	}
	return \%a;
}

sub cboCountries
{
	my $list = $db->selectall_arrayref('SELECT country_code, country_name FROM country_names_translated(?)', undef, $realOP->{'lang_pref'});
	my @vals = '';
	my %labels = ();
	foreach (@{$list})
	{
		push @vals, $_->[0];
		$labels{$_->[0]} = $_->[1];
	}
	return $q->popup_menu(
		'-name'=>'countries',
		'-values'=>\@vals,
		'-labels'=>\%labels,
		'-multiple'=>1
	);
}

sub cboLP
{
	my $list = $db->selectall_arrayref('SELECT code2, description FROM language_codes(?) WHERE active=TRUE', undef, $realOP->{'lang_pref'});
	my @vals = '';
	my %labels = ();
	foreach (@{$list})
	{
		push @vals, $_->[0];
		$labels{$_->[0]} = $_->[1];
	}
	return $q->popup_menu(
		'-name'=>'lp',
		'-values'=>\@vals,
		'-labels'=>\%labels,
		'-multiple'=>1
	);
}

sub Check_Transfer_SEQ
{
	unless ($param{'seq'})
	{
		Err('Missing seq parameter.');
	}

	my ($seq, $complete) = $db->selectrow_array("
		SELECT seq, complete
		FROM $DBtbls{'vip_transfers_log'}
		WHERE 	transferer= $realOP->{'id'}
		AND 	seq= $param{'seq'}
	");

	unless ($seq)
	{
		Err('Seq not found or does not match.');
	}
	elsif ($complete)
	{
		Err($lang_blocks->{'already_done'});
	}

	return 1;
}

sub Create_List
{
	my $mtype = shift;
	my $ADDL_JOINS = my $ADDL_COLS = '';
	
	# Dick wants both m's and s's to be included in the "Members" lists
# as of 09/24/12 Dick says "Break them out please."
	# and since we cannot simply insert other bare user submitted data directly into the SQL, we will create a mask
	# the value of a given hash key will be directly inserted into the SQL to provide a membertype IN (xxx)
	my %mtype_map = (
		'm'=>"'m'",
		'v'=>"'v'",
		'ag'=>"'ag'",
		'ma'=>"'ma'",
		's'=>"'s'",
		'tp'=>"'tp'"
	);

###### develop a query of potential transferees
	# Dick wants to remove the limit if the list is generating Trial Partners
	$LIMIT = 1000 if $mtype eq 'tp';
	
	my $limit = $param{'limit'} || $LIMIT;
	# ignore values that are not strictly numeric
	$limit = $LIMIT if $limit =~ m/\D/;

	my $orderBy = "m.id $param{'sortOrder'}";
	
	my $AND = '';

	if ($mtype eq 'tp')
	{
		$ADDL_JOINS = '
			JOIN od.pool
				ON od.pool.id=m.id
			LEFT JOIN member_ranking_summary mrs
				ON mrs.member_id=m.id';
		$ADDL_COLS = ", date_fmt_by_country(od.pool.deadline, '$realOP->{'lang_pref'}') AS deadline
			, mrs.ranking_summary AS prospect_rank
			, mrs.last_action_time::DATE AS ranking_last_action_time
			, od.pool.deadline AS raw_deadline";
			
		$orderBy = "raw_deadline ASC";	# specified by Dick
	}
	
	if (@lp)
	{
		my @tmp = ();
		push @tmp, $db->quote($_) foreach @lp;
		$AND = ' AND m.language_pref IN (' . join(',', @tmp) . ')';
	}

	if (@countries)
	{
		my @tmp = ();
		push @tmp, $db->quote($_) foreach @countries;
		$AND = ' AND m.country IN (' . join(',', @tmp) . ')';
	}
	
# with the advent of TNT 2.0 and the transferral of PRP credit, we have to guard against sponsor inversion in our nspid line
# this can occur if we transfer someone on our frontline to somebody else who is actually at a lower position in the nspid line...
# making it impossible for the new sponsor to see their newly transferred Partner
# since actually filtering each and every potential transferee against the recipient would exert an enormous cost,
# we will instead compare network.depths and should be almost as effective with almost no cost
# so let's get the network.depth for our recipient and we will exclude anybody whose network depth is less
#	my ($flp_depth) = $db->selectrow_array('SELECT depth FROM network.depths WHERE id= ?', undef, $param{'flp'}) || 0;
	my ($recipient_depth) = $db->selectrow_array('SELECT depth FROM network.depths WHERE id= ?', undef, $recipient->{'id'}) || 0;
	
###### this SQL is used in all cases
###### we'll add things onto the end as necessary for the individual list type
	my $qry = qq#
		WITH
			cnt AS (SELECT * FROM country_names_translated('$realOP->{'lang_pref'}')) /* that lang_pref var is also used elsewhere below */
		SELECT DISTINCT
			m.id,
			m.spid,
			m.alias,
			m.mail_option_lvl AS ms,
			d.depth,
			CASE
				WHEN m.membertype= 'ag' THEN m.company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			CASE
				WHEN m.membertype <> 'ma' THEN '&nbsp;'
				ELSE 'Merchant Affiliate'
			END AS flags,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.mail_option_lvl,
			membertype,
			mt.display_code,
			COALESCE(lc.description,'') AS language_pref_name,
			m.language_pref,
			cnt.country_name
	
		$ADDL_COLS
		
		FROM $DBtbls{'members'} m
		JOIN cnt
			ON cnt.country_code=m.country
		JOIN network.depths nd
			ON nd.id=m.id
		JOIN $DBtbls{'depths'} d
			ON m.id=d.id
		JOIN member_types mt
			ON mt.code=m.membertype
		
		$ADDL_JOINS
		
		LEFT JOIN language_codes('$realOP->{'lang_pref'}') lc
			ON lc.code2=m.language_pref
		
		WHERE 	m.spid= ?

	-- exclude 'Front Line Point' which may or may not be our recipient
		AND 	m.id != ?

	-- exclude BEMs... but only when they are NOT VIPs -or- Trial Partners
		AND 	(m.membertype IN ('v','tp') OR m.mail_option_lvl > 0)
		
	-- exclude parties who are at a higher network depth so we can mitigate nspid sponsor inversion..
		AND nd.depth >= $recipient_depth
	#;

	$qry .= "$AND AND m.membertype IN ($mtype_map{$mtype})";

	$qry .= "\nORDER BY $orderBy LIMIT $limit";
#die "id: $realOP->{'id'}\nflp: $param{'flp'}\nmtype: $mtype\n$qry";

	my $list = '';

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($cSpid->{'id'}, $param{'flp'});
	if ($rv eq '0E0')
	{
#		die "$qry \n\n'$realOP->{'lang_pref'}', '$realOP->{'lang_pref'}', $realOP->{'id'}, $param{'flp'}";
		$list = $q->p({'-class'=>'blu_alert'}, $lang_blocks->{'no_transferees'});
	}
	else
	{
		my $sortedIDs = '';
		my @data = ();
		while (my $rv = $sth->fetchrow_hashref)
		{
			$rv->{'mypid'} = $q->escape(Generate_Pid($rv));
#			$rv->{'mypid'} = Generate_Pid($rv);				# apparently these values don't need to be escaped, as they are not unes
			push @data, $rv;
			$sortedIDs .= "$rv->{'id'},";
		}
		chop $sortedIDs;	# be rid of the trailing comma
		
		my $tmpl_key = $param{'transfer_type'} eq 'tp' ? 'list-tp' : 'list-*';
		$TT->process($TMPL{$tmpl_key}, {'sth'=>\@data, 'lb'=>$lang_blocks}, \$list) || warn "TT failed to process template: $TMPL{$tmpl_key}\n" . $TT->error;
		
		$list = Create_List_Form(\$list, $sortedIDs);
	}
#warn $list;
	return $list;
}

sub Create_List_Form
{
	my $list = shift;
	my $sortedIDs = shift;
	
	my $form = $q->start_form(
		'-action'=>$ME_Path,
		'-name'=>'list',
		'-method'=>'post',
		'-onsubmit'=>'return ListFrmSubmit(this)'
	) . "\n";
	$form .= $q->hidden('-name'=>'activity', '-value'=>'transfer', '-force'=>1) . "\n";
	$form .= $q->hidden('transfer_type') . "\n";
	
	###### in order to keep them from submitting multiple times
	###### we'll give them a variable number 'key' to go with this transfer
	###### if they later try to submit more than once we can stop 'em
	my ($seq) = $db->selectrow_array("SELECT NEXTVAL('generic_pk_seq')");
	unless ( $db->do("
		INSERT INTO $DBtbls{'vip_transfers_log'} (seq, recipient_id, transferer)
		VALUES ($seq, $recipient->{'id'}, $realOP->{'id'})") )
	{
		return $q->p({'-class'=>'confirm'}, 'Failed to setup transfer sequence.');
	}
	$form .= $q->hidden('-name'=>'seq', '-value'=>$seq);

	###### if this is an AP or CSR list
	###### we'll provide buttons at the top & bottom
	###### since it could be a lengthy list
	$form .= ctl_buttons() if ($param{'transfer_type'} eq 'm' || $param{'transfer_type'} eq 's');
	
#	foreach('pd','pd_hash','rid','rid_hash')
	foreach('pd','rid','rid_hash')
	{
		$form .= "\n" . $q->hidden($_);
	}

	# CGI.pm does not handle multi-byte characters properly in this case :-(
#	$form .= "\n" . $q->hidden('-name'=>'pd_hash', '-force'=>1, '-value'=>'PPPPPPPPPPPPPP'.$gs->Prepare_UTF8($param{'pd_hash'}, 'encode'));
	$form .= qq#<input type="hidden" name="pd_hash" value="$param{'pd_hash'}" />#;
#	$form .= "\n" . $q->hidden('-name'=>'pd_hash', '-force'=>1, '-value'=>$param{'pd_hash'});

	if ($param{'transfer_type'} eq 'tp')
	{
		$form .= "$lang_blocks->{'select_xfer_type'}: ";
		$form .= qq|<input class="xfertype" type="radio" checked="checked" value="individual" name="xfer_type" />|;
		$form .= $q->a({'-class'=>'hdr xfertype', '-href'=>'#', '-title'=>$lang_blocks->{'xfer_type_individual_explanation'}}, $lang_blocks->{'xfer_type_individual'});
		$form .= qq|<input class="xfertype" type="radio" value="stacking" name="xfer_type" />|;
		$form .= $q->a({'-class'=>'hdr xfertype', '-href'=>'#', '-title'=>$lang_blocks->{'xfer_type_stacking_explanation'}}, $lang_blocks->{'xfer_type_stacking'});
		$form .= $q->hidden({'-name'=>'sortedIDs', '-value'=>$sortedIDs});
	}
	
	$form .= "<br /><br />\n$$list\n<br /> ";
	
	###### lets put in our buttons if this is not a Partner transfer... we will be providing specially labeled buttons directly in the template in that case
	$form .= ctl_buttons('x') unless $param{'transfer_type'} eq 'v';
	$form .= "\n</form>\n";
	return $form;
}

sub ctl_buttons
{
	my $arg = shift;

	###### if I am going to have a duplicate set of buttons, I'll need
	###### to create distinct names
	$arg = $arg ? ($arg . '_') : '';
	my $buttons = ();

	###### we will have an additional 'select all' button
	###### only for simple stack transfers
#	if ($Path eq 'simple' || $Path eq 'autostack')
	if ($param{'transfer_type'} eq 'tp')
	{
		$buttons = $q->button(
			'-value'=>$lang_blocks->{'select_all'},
			'-onclick'=>"for(x=0; x<document.forms.list.mvid.length; x++) document.forms.list.mvid[x].checked=true;",
			'-class'=>'btn_select_all',
			'-onmouseout'=>'resetBtn(this)',
			'-onmouseover'=>"setBtn(this, 'select_all')",
			'-name'=>$arg . 'select_all'
		) . "\n";
	}

	$buttons .= $q->button(
		'-value'=>$lang_blocks->{'cancel'},
		'-onclick'=>"location.href='$ME_Path'",
		'-class'=>'btn_cancel',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'cancel')",
		'-name'=>$arg . 'cancel'
	) . "\n";

	$buttons .= $q->reset(
		'-class'=>'btn_reset',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'reset')",
		'-value'=>$lang_blocks->{'reset'}
	) . "\n";

	$buttons .= $q->submit(
		'-value'=>$lang_blocks->{'perform_xfer'},
		'-class'=>'btn_transfer',
		'-onmouseout'=>'resetBtn(this)',
		'-onmouseover'=>"setBtn(this, 'transfer')"
	)  . "\n";

	return $buttons;
}

sub Do_Page
{
	my ($TMPL, $args) = @_;

	###### if I receive a digit only, I know I have to retrieve the template
	if ($TMPL !~ /\D/)
	{
		$TMPL = $gs->GetObject($TMPL) || die "Failed to retrieve template: $TMPL";
	}
	
	my $rv = '';
	# take care of our language nodes
	$realOP->{'firstname_bolded'} = $q->b($realOP->{'firstname'});
	
	Parse_Lang_Blocks({'realOP' => $realOP, 'user' => $realOP, 'recipient' => $recipient});
	
#warn Dumper($args);
	my %data_hash = (
			'lang_blocks' => $lang_blocks,
			'realOP' => $realOP,
			'user' => $realOP,
			'alerts' => \@alerts,
			'script_name' => $ME,
			'ME_Path' => $ME_Path,
			'param' => \%param,
			'hidden_lp' => Hidden_LP(),
			'MidList' => \&MidList,
			'cSpid' => $cSpid,
			'MANAGED' => $MANAGED,
			'masq_notice_block' => $masq->NoticeBlock
	, %{ $args->{'data'} || {} } );
#print $Session->header('-expires'=>'now', '-charset'=>'utf-8'), $args->{'data'}->{'list'}; Exit();
#$TMPL = $gs->Prepare_UTF8($TMPL, 'encode');
#warn Dumper(\%data_hash);
	$TT->process(\$TMPL, \%data_hash, \$rv);
		
	my $GI = GI->new();
	print $Session->header('-expires'=>'now', '-charset'=>'utf-8'),	# by using $Session, the session cookie is automatically handled
		$GI->wrap({
			'title'=>($lang_blocks->{ $args->{'title'} || '' } || $args->{'title'} || $lang_blocks->{'title1'}),
			'style'=>$GI->StyleLink('/css/mv-members.css'),
			'content'=>\$rv,
			'script'=>'<script type="text/javascript" src="/js/mv-members.js"></script>'
		});
	
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	my $msg = shift || '';
	my $tmpl = shift || $TMPL{'Error'};
	if ($db)
	{
		$tmpl = $gs->GetObject($tmpl) || die "Failed to load template: $tmpl\n\nPlus you have this 'error':\n$msg\n";
	}

###### if we don't have a DB connection, then we'll setup a vanilla template
	else
	{
		$tmpl = $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Transfer error');
		$tmpl .= '%%error_msg%%<br /><br />';
		$tmpl .= '%%timestamp%%';
		$tmpl .= $q->end_html();
	}

	$tmpl =~ s/%%error_msg%%/$msg/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
	
	Exit();
}

sub Generate_Pid
{
###### my pid will consist of 7 characters of a hash + the transfer ID
###### & a pipe delimited list of personal data
	my $row = shift;
	return unless $row;

	my $pid = "$row->{'id'}|$row->{'spid'}|$row->{'alias'}|$row->{'firstname'}|";
	$pid .= "$row->{'lastname'}|$row->{'emailaddress'}|$row->{'membertype'}|";
	$pid .= "$row->{'depth'}|$row->{'ms'}";
#	$pid = $gs->Prepare_UTF8($pid, 'encode');
	$pid = md5_hex($pid . $key) . "|$pid";

	return $pid;
}

sub Generate_Pid_Hash
{
###### generate a hash based on the data in the passed pid and pass it back as a ref
###### refer to Generate_Pid for the values contained in the pid
	my $pid = shift;
	return unless $pid;
	
	my @vals = split(/\|/, $pid);
	shift @vals;
	my %r = (
		'id'		=> $vals[0],
		'spid'		=> $vals[1],
		'alias'		=> $vals[2],
		'firstname'	=> $vals[3],
		'lastname'	=> $vals[4],
		'emailaddress'	=> $vals[5],
		'membertype'	=> $vals[6],
		'depth'		=> $vals[7],
		'ms'		=> $vals[8]
	);
	return \%r;
}

sub Get_OID
{
	return $db->selectrow_array("SELECT oid FROM $DBtbls{'members'} WHERE id= $_[0]");
}

sub Get_Member_Record
{
	my $id = shift;
	return unless $id;

	my $rv = $db->selectrow_hashref(qq#
		SELECT m.id,
			m.spid,
			m.alias,
			CASE
				WHEN m.membertype= 'ag' THEN company_name
				ELSE m.firstname
			END AS firstname,
			CASE
				WHEN m.membertype= 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.membertype,
			mt.display_code,
			d.depth,
			m.mail_option_lvl AS ms,
			COALESCE(m.language_pref,'') AS language_pref,	-- is defaulting to English really the best option?
			m.oid
		FROM $DBtbls{'members'} m
		JOIN depths d
			ON m.id=d.id
		JOIN member_types mt
			ON mt."code"=m.membertype
		WHERE 	${\($id =~ /\D/ ? 'm.alias' : 'm.id')} = ?
	#, undef, $id);
	
	($rv->{'language_pref_name'}) = $db->selectrow_array('SELECT description FROM language_codes(?, ?)', undef, $rv->{'language_pref'}, $realOP->{'lang_pref'}) || '';
	return $rv;
}

sub Hidden_LP
{
	my $rv = '';
	foreach (@lp)
	{
		$rv .= $q->hidden('-name'=>'lp', '-force'=>1, '-value'=>$_);		
	}
	return $rv;
}

sub Load_lang_blocks
{
	my $xml = $gs->GetObject( $TMPL{'XML'} ) || die "Failed to load lang_blocks\n";
	$lang_blocks = $lu->flatXMLtoHashref($xml);
}

sub Load_Params
{
	my ($tmp) = ();

	foreach ('pd_hash', 'rid_hash', 'transfer_type', 'country', 'xfer_type', 'sortedIDs')
	{
		$param{$_} = $q->param($_) || '';
	}
	# one of two values for this one
	$param{'xfer_type'} = '' unless grep $param{'xfer_type'} eq $_, ('','stacking','individual');
	
	$param{'pd'} = $gs->PreProcess_Input(uc $q->param('pd')) || '';
	
	###### load specific parameters that we want to do bounds checking on
	foreach ('flp','limit','seq')
	{
		if ($tmp = $q->param($_) || '')
		{
			die 'Invalid $_ parameter received' if $tmp =~ /\D/;
			$param{$_} = $tmp;
		}
	}

	if ($tmp = $q->param('sortOrder'))
	{
		unless ($tmp =~ /^asc$|^desc$/i)
		{
			die 'Invalid sortOrder parameter received';
		}
		$param{'sortOrder'} = $tmp;
	}
	else
	{
		$param{'sortOrder'} = '';	# better than null
	}

	foreach ($q->param('mvid'))
	{
		push @mvid, $q->unescape($_);
	}
	
	foreach($q->param('lp'))
	{
		next unless $_;
		push @lp, $_;
	}
	
	foreach($q->param('countries'))
	{
		next unless $_ && m/[A-Z]{2}/;	# this param should be a 2 uppercase letter country code
		push @countries, $_;
	}
}

sub MidList
{
	# we will be returning a list of <option> nodes representing the parties for whom I am the coach
	my $mtypes = "'v','tp'";	# the member types we want showing in the list
	my $rv = '';
	my $il = $gs->select_array_of_hashrefs("
		SELECT m.id, m.alias, m.firstname ||' '|| m.lastname AS fullname, m.membertype
		FROM $DBtbls{'members'} m
		JOIN $DBtbls{'my_coach'} mc
			ON mc.id=m.id
		WHERE mc.upline= $realOP->{'id'}	-- that has been validated as numeric
		AND m.membertype IN ($mtypes)
		ORDER BY m.id
	");
	
	# I am assuming that since this is initially being developed to only enable coaches to transfer Trial Partners,
	# and that we only want those in the list who actually have Trial Partners sponsored... so we will exclude those who do not
	#my $sth = $db->prepare("SELECT TRUE FROM $DBtbls{'members'} WHERE membertype='tp' AND spid=? LIMIT 1");
	
#	Bill - we are unable to make transfers of a TP from a Partner that has a single TP. When the list shows for eligible TPs/Partners to make transfers from, it doesn't show Partners with one TP. So no transfers can be made.
#	I have been making these transfers on behalf of Coaches using https://www.clubshop.com/cgi/admin/01-mv-members.cgi
#	Can you rectify this so that Partners with one TP also show on the list when using the Coach Management Mode? 
	my $sth = $db->prepare("
		SELECT TRUE
		FROM $DBtbls{'members'} m
		/*JOIN od.pool
			ON od.pool.id=m.id*/
		WHERE m.spid= ?
		AND m.membertype='tp'
		GROUP BY m.spid
		HAVING COUNT(*) >= ?
	");

	# Dick does not want coaches to be able to manage the parties they are the coach for if that party is a coach themself
	my $csth = $db->prepare("SELECT TRUE FROM $DBtbls{'my_coach'} WHERE upline= ? LIMIT 1");
	
	foreach (@{$il})
	{
		my $cnt = $_->{'membertype'} eq 'v' ? 1 : 2;
		next unless $db->selectrow_array($sth, undef, $_->{'id'}, $cnt);
		next if $db->selectrow_array($csth, undef, $_->{'id'});
		$rv .= $q->option({'-value'=>$_->{'id'}}, "$_->{'alias'} : $_->{'fullname'}");
	}
	
	if (! $rv)
	{
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'no_managees'});
	}
	
	# put an empty option on the front
	return $q->option({'-value'=>''}, $lang_blocks->{'managed_party_prompt'}) . $rv;
}

sub Parse_Lang_Blocks
{
	# the reason for doing this way is that we don't have to worry about breaking the XML
	# *prior* to parsing it out into lang_blocks
	# we can do our conversion to a hash and then take care of each node individually
	my $data = shift;

	foreach my $k (keys %{$lang_blocks})
	{
		$lang_blocks->{$k} = Parse_Lang_Blocks_Node($k, $data);
	}
}

=head3 Parse_Lang_Blocks_Node

While we will want to perform substitutions on the general set of nodes prior to finally rendering a page,
there are cases, such as during error generation where we will stuff a node into the @alerts array
and we need a particular node to have substitution performed on it before the whole is finally done.

This routine performs that one-off substitution and is used by Parse_Lang_Blocks to boot

=cut

sub Parse_Lang_Blocks_Node
{
	my ($node_name, $data) = @_;
	my $rv;
	$TT->process(\$lang_blocks->{$node_name}, $data, \$rv);
	return $rv;
}

sub Pre_Transfer_Checks
{
	if (@mvid)
	{
		return unless Check_Transfer_SEQ();

		return 1;
	}
	else
	{
		###### evidently there were no transferees selected
	#	$alert .= $q->p({-class=>'confirm'}, 'You failed to make your selections.');
	#	$formlist = Create_List();
	#	Do_Page($TMPL{main}, 'list');
	###### we don't have the parameters to to create a list so we'll error
	###### out and let them press the back button
		Err("You failed to make your selections.<br />
			Please back up and check one or more parties to transfer.");
		return;
	}
}

sub Query_Insert_Activity
{
	my ($id, $spid, $activity_type) = @_;
	$activity_type ||= 22;
	my $memo = $masq->User || $realOP->{'alias'};
	
	return "
			INSERT INTO $DBtbls{'activity_rpt_data'} (
				id,
				spid,
				activity_type,
				val_text
			) VALUES (
				$id,
				$spid,
				$activity_type,
				${\$db->quote($memo)}
			);";
}

sub Query_Insert_Backend_Processing
{
	my ($val_int1, $val_int2, $proctype) = @_;
	$proctype ||= 7;
	
	return "
		INSERT INTO $DBtbls{'backend_processing'} (
			val_int1,
			val_int2,
			proctype
		) VALUES (
			$val_int1,
			$val_int2,
			$proctype
		);";
}

sub Query_Update_Members_and_Depths
{
	my ($id, $spid, $depth, $memo) = @_;
	$memo = $db->quote( $masq->PrependMasqInfo($memo) );
	
	return "
		UPDATE $DBtbls{'members'} SET
			spid= $spid,
			stamp= NOW(),
			memo= $memo,
			operator= 'mv-members.cgi'
		WHERE id = $id;

		UPDATE $DBtbls{'depths'} SET
			depth= $depth
		WHERE id= $id;";
}

sub Query_Update_Relationships
{
	my $row = shift;	# should be a hashref
	my $qry = '';
	
	if ($row->{'membertype'} eq 'tp')
	{
		$qry = "\nSELECT create_tp_relationships_records($row->{'id'});";
	}
	elsif ($row->{'membertype'} eq 'v')
	{
		$qry = "\nSELECT create_relationships_records($row->{'id'});";
	}
	
	return $qry;
}

sub Transfer_Group
{
###### we'll transfer a group of members to another party
###### we'll build our transfer LIST as we go
###### we have our IDs as the keys of the hash and our matching data as the value
	my ($qry, $row) = ();
	my $memo = "Transferred by $realOP->{'alias'}" . ($MANAGED ? " on behalf of $cSpid->{'alias'}" : '');
	
	($recipient->{'my_upline_vip'}) = $db->selectrow_array("SELECT upline FROM my_upline_vip WHERE id= $recipient->{'id'}");

	foreach(@mvid)
	{
		unless (Validate_Pid($_))
		{
			$proc_status .= "Skipping - Error validating pid: $_ <br />\n";
			next;
		}
		$row = Generate_Pid_Hash($_);
		
		$proc_status .= "Transferring $row->{'alias'} <br />\n";
		$qry = Query_Update_Members_and_Depths($row->{'id'}, $recipient->{'id'}, ($recipient->{'depth'} +1), $memo);
		
		$qry .= Query_Insert_Backend_Processing($row->{'id'}, $recipient->{'id'});

	###### our activity report entry
	###### credit goes to the operator
	# we will only create the activity if we are transferring beyond my "partner" sponsorship,
	# in other words, the recipient is not a partner and the upline Partner of the recipient is not me
		if ($recipient->{'membertype'} eq 'v' || $recipient->{'my_upline_vip'} != $cSpid->{'id'})
		{
			# use the TP transfer activity type if the transferee is a TP
			# -or- the one specific to Members/Affiliates
			my $ttype = 
				$row->{'membertype'} eq 'tp' ? $tpXferActivityType :
				$row->{'membertype'} eq 'm' ? $smXferActivityType :
				$row->{'membertype'} eq 's' ? $smXferActivityType :
				$defActivityType;
			$qry .= Query_Insert_Activity($row->{'id'}, $recipient->{'id'}, $ttype);
		}
		

	###### this next section takes care of updating upline relationships where they exist
		$qry .= Query_Update_Relationships($row);
		
		if ($recipient->{'membertype'} eq 'v')
		{
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}, $row->{'emailaddress'}\n";
		}
		else
		{
	###### if it is not a VIP receiving the transfer, we use a different format
			$transfer_list .= "$row->{'alias'}, $row->{'firstname'}, $row->{'lastname'}\n";
		}

#print "qry = $qry\n"; Exit();

		$db->do($qry);
	}

	$proc_status .= " - Done.<br />\n";

	if ($transfer_list)
	{
		return 1;
	}

	return undef;
}

sub Transfer_Stack
{
###### we'll transfer a group of Trial Partners, that we have stacked, to another party
###### we'll build our transfer LIST as we go
###### we have our IDs as the keys of the hash and our matching data as the value
	my ($qry) = ();
	my $memo = "Transferred by $realOP->{'alias'}" . ($MANAGED ? " on behalf of $cSpid->{'alias'}" : '');
	my $rcpt = $recipient;
	my $depth = $recipient->{'depth'};
	($recipient->{'my_upline_vip'}) = $db->selectrow_array("SELECT upline FROM my_upline_vip WHERE id= $recipient->{'id'}");
	
	# we need to process these in the same order as the list was originally presented, namely by deadline ASC (as of 09/10/14)
	# so we need to create a hash that we can randomly access since we cannot be sure that @mvid represents the original sort order
	my @oo = split(/,/, $param{'sortedIDs'});
	my %mvid = ();
	foreach(@mvid)
	{
		unless (Validate_Pid($_))
		{
#			print "Skipping - Error validating pid: $_ <br />\n";
			$proc_status .= "Skipping - Error validating pid: $_ <br />\n";
			next;
		}
		my $row = Generate_Pid_Hash($_);
		$mvid{$row->{'id'}} = $row;
	}
	
	foreach(@oo)
	{
		next unless $mvid{$_};
		# since we are creating a stack, we can simply increment this value
		$depth++;
		
#		print "Transferring $row->{'alias'} <br />\n";
		$proc_status .= "Transferring $mvid{$_}->{'alias'} to $rcpt->{'alias'}<br />\n";
		$qry = Query_Update_Members_and_Depths($mvid{$_}->{'id'}, $rcpt->{'id'}, $depth, $memo);

		$qry .= Query_Insert_Backend_Processing($mvid{$_}->{'id'}, $rcpt->{'id'});

	###### our activity report entry
	###### credit goes to the operator
	# we will only create the activity if we are transferring beyond my "partner" sponsorship,
	# in other words, the recipient is not a partner and the upline Partner of the recipient is not me
		if ($recipient->{'membertype'} eq 'v' || $recipient->{'my_upline_vip'} != $cSpid->{'id'})
		{
			# use the TP transfer activity type if the transferee is a TP
			$qry .= Query_Insert_Activity($mvid{$_}->{'id'}, $rcpt->{'id'}, ($mvid{$_}->{'membertype'} eq 'tp' ? $tpXferActivityType : $defActivityType));
		}

	###### this next section takes care of updating upline relationships where they exist
		$qry .= Query_Update_Relationships($mvid{$_});
	
	###### create our progress list
		if ($recipient->{'membertype'} eq 'v')
		{
			$transfer_list .= "$mvid{$_}->{'alias'}, $mvid{$_}->{'firstname'}, $mvid{$_}->{'lastname'}, $mvid{$_}->{'emailaddress'}\n";
		}
		else
		{
	###### if it is not a VIP receiving the transfer, we use a different format
			$transfer_list .= "$mvid{$_}->{'alias'}, $mvid{$_}->{'firstname'}, $mvid{$_}->{'lastname'}\n";
		}

#print "qry = $qry\n"; Exit();

		$db->do($qry);
#		$firstID ||= $row->{'id'};

		# now change our recipient to the last party that was transferred
		$rcpt = $mvid{$_};
	}

	$proc_status .= " - Done.<br />\n";

	if ($transfer_list)
	{
		return 1;
	}

	return undef;
}

sub TransferTypeOptions
{
	my @options = ();

	# as of 09/09/14 with managed mode, we can only transfer TPs, so we will create options that can be embellished if necessary
	my @map = $MANAGED ? qw/tp/ : qw/v ag ma m s tp/;
	
	# generate our membertype options that this recipient can receive
	$recipient->{'can_sponsor'} ||= CanSponsor($recipient->{'membertype'});
	
	# we must assume that "somebody" is going to specify the order of the options
	# and since I am not going to add more data to the DB for doing this
	# I will do a sort in here even though it means if another membertype is added that should be transferrable
	# this code will have to be embellished
#	foreach my $mt (qw/v ag ma m s/)
	foreach my $mt (@map)
	{
		push @options, $recipient->{'can_sponsor'}->{$mt} if $recipient->{'can_sponsor'}->{$mt};
	}
	return \@options;	
}

sub Update_Transfer_SEQ
{
	$db->do("
		UPDATE $DBtbls{'vip_transfers_log'}
		SET complete= TRUE
		WHERE transferer= $realOP->{'id'} AND seq= $param{'seq'}
	");
}

sub Validate_Pid
{
###### my pid will consist of a hash & a pipe delimited list of 
###### personal data, the first field of which is the numeric ID

	my $var = shift;
	return unless $var;
	my @val = split(/\|/, $var, 2);

	$val[1] =~ /(^\d*)\|/;
	$val[1] = $gs->Prepare_UTF8($val[1], 'encode');
	return ( md5_hex( $val[1] . $key) eq $val[0] ) ? 1 : 0;
}

sub Validate_Recipient
{
	$recipient = Get_Member_Record($param{'pd'});

	unless ( $recipient )
	{
		# the specified recipient was either empty or has no match in the DB
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'invalid_recipient'});
		return;
	}
	elsif ( $recipient->{'id'} == $cSpid->{'id'})
	{
		# you cannot be the recipient... duh
		push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'ucannot'});
		return;
	}
	
	# CanSponsor will return an empty hashref if nothing else
	$recipient->{'can_sponsor'} = CanSponsor($recipient->{'membertype'});
	unless ( keys %{$recipient->{'can_sponsor'}} )
	{
		push @alerts, $q->div({'-class'=>'y_alert'}, Parse_Lang_Blocks_Node('cannot_receive_transfers', {'recipient'=>$recipient}) );
		return;
	}
	
	my ($rv) = ();
	my $tmp = $recipient;
	my $startID = $recipient->{'spid'};
	my $sth = $db->prepare("
		SELECT id,
			spid,
			alias,
			CASE
				WHEN membertype= 'ag' THEN company_name
				ELSE firstname
			END AS firstname,
			CASE
				WHEN membertype= 'ag' THEN ''
				ELSE lastname
			END AS lastname,
			emailaddress,
			membertype,
			0 AS depth,
			mail_option_lvl AS ms
		FROM $DBtbls{'members'}
		WHERE id= ?");

	###### flp will be our Front Line Point of connection to the recipient
	###### we'll need this value later so we can exclude them from any lists
	my $flp = $tmp->{'id'};

	while ($tmp->{'spid'} != $cSpid->{'id'} && $tmp->{'spid'} > 1)
	{
		$rv = $sth->execute($tmp->{'spid'});
		die "Failed to retrieve sponsor record for: $tmp->{'spid'}" unless $rv;

		$tmp = $sth->fetchrow_hashref;
		die "Loop detected!" if $tmp->{'spid'} == $startID;

		$flp = $tmp->{'id'};
	}

	if ($tmp->{'spid'} == 1 && $cSpid->{'id'} != 1)
	{
#		# OK, we may be in our nspid organization, that's why we made it all the way to the top
#		($rv) = $db->selectrow_array("SELECT network.is_nspid_above_you($recipient->{'id'}, $cSpid->{'id'})");
#		unless ($rv)
#		{
	# as of Feb 2015, transfers to downline nspids is called a no-no
			push @alerts, $q->div({'-class'=>'y_alert'}, $lang_blocks->{'not_in_organization'});
			return;
#		}
	}

	# if the recipient is only in our nspid organization, then this value will end up being 01 or somewhere above us,
	# but it won't matter for what we are using the flp for in the next step, namely that we do not select somebody on my frontline who is upline of the recipient
	$param{'flp'} = $flp;

###### we'll assign the numeric ID to pd just in case we don't have it
###### and also set the pd param
	$param{'pd'} = $recipient->{'id'};
	$param{'pd_hash'} = Generate_Pid($recipient);
	$param{'recipient'} = $recipient;	# we will need this data when we Do_Page()
	return $recipient;
}

__END__

###### 11/24/04 changed the list form's method to an explicit 'post' since some VIPs
###### were having trouble submitting more than 13 members for transfer
###### also fixed up the utf8 mailing to properly encode the data and doctor the headers
###### 12/20/04 fixed the broken checks following the VIP under member check towards the top
###### also added a restriction on the amount of PP a party can have before being
###### disqualified from being transferred
###### 01/12/05 fixed the cases where $q->param was embedded in error messages bare
###### 01/14/05 added a coalesce to the language_pref col in getting the member rec.
###### 01/17/05 revised the %%script_name%% placeholder to get q->url instead so that
###### the path information would not be embedded (this was causing problems in jump to)
###### 01/18/05 made the emailing alert conditional on the recipient's membertype
###### 01/27/05 added a hack to allow 01 to perform transfers over 250 PP
###### added another hack to allow 01 to override an explicit denial of mgt. rights
###### 02/11/05 put the actual network calcs table into the mix rather than
###### the view which was killing us
###### 05/10/05 just a little javascript tweak and a little preprocessing of the pd param
###### 05/12/05 changed the error handler in Pre_Transfer_Checks to simply abort
###### 09/08/05 revised to allow managing members with the exclusion of parties
###### originally sponsored by the member
###### 01/25/06 Added -charset=utf-8 to the $q->header statement.
###### to pull in the necessary level and role information for real and rid
# 07/06/06 revised the SQL for list generation to show AG company names
###### 02/08/08 further tweakage of the BEM exclusion implemented in Jan. '08
# 09/16/08 added an insert for notification type 81 on a VIP transfer
###### 07/15/09 disabled mailings to recipients with bad mail setting
###### just quickie revisions to move this to the GI domain
###### 11/18/10 integration of the transfer of AMPs with VIPs
# 02/24/11 disabled the email to VIP recipients of a transfer
###### 01/10/12 lot's of changes involving many things, like who can sponsor whom and a lot of code cleanup
###### 04/09/12 ripped out things related to 'amp's
###### 09/24/12 added 's' membertype to the list in TransferTypeOptions per Dick's request to be able to transfer "Members"
###### 10/27/12 added functionality to enable the generated lists to match the language pref of the recipient or to select from a language list
###### these changes also involved displaying language prefs of the recipient and the potential transferees
###### 03/14/13 weaving the nspid network into this thing as far as being able to transfer to recipients who may be only in the nspid network
###### 03/20/13 implemented a primitive hack to filter out potential transferees when their network depth is less than the recipients to try and mitigate nspid sponsor inversion
###### 04/09/13 made the previous really work as it was relying on the regular depth of the recipient rather than the network.depth
###### 04/11/13 we'll get this right yet... it was excluding those of greater depth instead of those of lesser depth
###### 04/18/13 yet another tweak to the previous logic and also introduced the reassignment to a different pool if the party being transferred is not a Partner
###### 07/10/13 added Affiliates (m) to the list is CanSponsor() for Shoppers
###### 11/08/13 added the ability to transfer certain membertypes to Trial Partners
###### 02/12/14 added the ability to transfer Trial Partners themselves, also reinstituted the nspid check for all membertypes during the transfer list development to ensure we do not get sponsor inversion
###### 02/13/14 disabled the network.spids management integration (updating network.spids) as we no longer need to much around with that
###### 03/26/14 added support for a countries drop-down to use as a filter
###### 08/09/14 added the ability to transfer Partners to Trial Partners
###### 08/28/14 added the insertion of a backend_processing record for each transfer
###### 10/13/14 revised the SQL in MidList to even show those who only have one measly TP to transfer away
###### 10/16/14 revised the SQL again to provide a variable group > number and to also just use the 'tp' membertype and get rid of the join on od.pool
###### 10/25/14 disabled the nop lockout
###### 01/12/15 revised the creation of the partner transfer activity to 1. make it conditional to only get created when the transfer left one's own partner sponsorship
# and 2. to break out the transfer of a Trial Partner into a separate activity type
02/27/15
	removed the ability to transfer to downline nspids in Validate_Recipient()
03/16/15
	made it so the list will provide BEM Trial Partners
	Also added yet another transfer notification type to distinguish Member/Affiliate transfers at the activity reports
05/08/15
	added binmode to deal with encoding issues that have arisen from the latest update of DBD::Pg