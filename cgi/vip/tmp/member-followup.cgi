#!/usr/bin/perl -w
###### member-followup.cgi
###### Member Followup Management interface
###### created: 03/09	Bill MacArthur
###### last modified: 07/15/15	Bill MacArthur

# 07/15/15 custom hacks put into place to allow 4773425 and 3887940 unlimited downline access per Dick

use strict;
use CGI;
use Date::Calc qw(Add_Delta_Days Today);	# used in ReminderDateCBO()
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw/$LOGIN_URL/;
use MemberFollowup;
require MemberDetail;
use XML::Simple;
use ParseMe;
use QuickLinks;
use EmailTemplates::Links;
binmode STDOUT, ":encoding(utf8)";
# for admin access
use ADMIN_DB;
require cs_admin;

###### CONSTANTS

my $ADMIN = ();	# this will end up defined if admin is using
my $db = ();

# for now we'll simply place any errors in this var (make sure it's valid xhtml)
# if present, an <errors> node will be created in the root
my $errors = my $alerts = '';
my %params = ();	# used during the addfollowup action
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $xs = new XML::Simple;

my %TMPL = (
	'lang_blocks' => 10665,
	'tmpl' => 10666,
	'search' => 10668
);

###### error handling provided by LoginCK
exit unless my $meminfo = LoginCK();

$db ||= DB_Connect::DB_Connect('member-followup.cgi') || exit;
#$db->{RaiseError} = 1;
#($meminfo->{'pay_level'}) = $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= $meminfo->{'id'}") || 0;
($meminfo->{'seq'}, $meminfo->{'pnsid'}) = $db->selectrow_array("SELECT seq, pnsid FROM mviews.positions WHERE id= $meminfo->{'id'}");

my $pay_level_labels = $db->selectall_hashref("SELECT lvalue, lname FROM level_values", 'lvalue');
my $xml = $gs->GetObject($TMPL{'lang_blocks'}) || die "Failed to load lang_blocks XML\n";
$xml = XMLin($xml, 'NoAttr' => 1);	# convert our XML into a hashref

#$gs->Prepare_UTF8($xml, 'encode');

# our $xml contains a 'javascript' node with nodes under it
# we need to convert those and place them on the root as we cannot process subnodes in ParseMe
my $js = $xml->{'javascript'};
# since we are inserting text strings in javascript vars, we need to escape newlines to keep it happy
foreach (keys %{$js})
{
	$js->{$_} =~ s/\r//g;
	$js->{$_} =~ s#(\n)#\\$1#g;
	$xml->{$_} = $js->{$_};
}
delete $xml->{'javascript'};

my $mf = MemberFollowup->new($db);

my $action = $gs->GetPathParts($q->path_info) || 'report'; # we are expecting an optional path argument to define our action

# refid is for compatibility with older scripts passing an id on that parameter
my $rid = $gs->PreProcess_Input($q->param('rid') || $q->param('refid'));	# this will be looked up and converted to an integer value if a member match is found

if ($action eq 'bulkfollowup')
{
#	End() unless CheckRole();
	###### this is not actually called from within this report's interface, but from the activity report
	LoadParams(qw/followup activities/);
	unless ($params{'activities'} && $params{'followup'})
	{
		print $q->header('text/plain'), 'No parameters received';
	}
	else
	{
		BulkFollowup();
	}
	End();
}

my $member;
if ( (! $rid) || ! ($member = FindMember()) )
{
	SearchForm();
	End();
}

unless (CheckPrivileges())
{
	# error handling takes place in CheckPrivileges()
}
elsif ($action eq 'report')
{
	Display();
}
elsif ($action eq 'addfollowup')
{
	LoadParams(qw/followup reminder reminder_date activities/);
#	AddFollowup() if $params{'activities'} && $params{'followup'};
	AddFollowup() if $params{'followup'};
	AddReminder() if $params{'reminder'} && $params{'reminder_date'};
	Display();
}
elsif ($action eq 'vipnotes')
{
	LoadParams(qw/vipnotes _action/);
	SetVipNotes();
	Display();
}
elsif ($action eq 'delreminder')
{
	LoadParams(qw/pk/);
	DeleteReminder();
	Display();
}
else
{
	Err('Invalid action');
}

End();

###### Sub-routines Start ######

sub AddFollowup
{
	$mf->DecodeActivities($params{'activities'});
	$mf->CreateMemberFollowup($member->{'id'}, $meminfo->{'id'}, $params{'followup'}, $params{'activities'});
}

sub AddReminder
{
	my $rv = $mf->CreateReminder($member->{'id'}, $meminfo, $params{'reminder'}, $params{'reminder_date'});
	if ($rv eq '1')
	{
		$alerts = $q->span({'-style'=>'color:green'}, "$xml->{'reminder_was_setup'}: $params{'reminder_date'}");
	}
	else
	{
		$alerts = $q->span({'-style'=>'color:red'}, "$xml->{'reminder_not_setup'}: $params{'reminder_date'}");
	}
}

=head4
	BulkFollowup
	This routine is currently used from within the activity reports to perform bulk followup entries
	for items like new member followup and such that would be impractical to enter individually.

	Since we are receiving 'activities' parameters that are self-checkable,
	we can be reasonably sure about line of sponsorship at least.
	They wouldn't be able to generate the correct parameters without having access to the correct report.
	We still have to check their role, however, in case they are not qualified to be doing the followup
=cut

sub BulkFollowup
{
	$mf->DecodeActivities($params{'activities'});
	# our arrayref contained in $params{activities} now just holds activity_rpt_date pk values
	# we need member IDs to actually create a followup,
	# so we will look each member id up based on the pk
	# then we will create the followup
	my $sth = $db->prepare('SELECT id FROM activity_rpt_data WHERE pk= ?');
	my $rv = '';
	foreach my $a (@{$params{'activities'}}){
		unless ($a){
			$rv .= "An invalid parameter was received: NULL decoded activities\n";
			next;
		}
		$sth->execute($a);
		my ($id) = $sth->fetchrow_array;
		eval( $mf->CreateMemberFollowup($id, $meminfo->{'id'}, $params{'followup'}, $a) );
	###### if they try to enter followup on a previously followed up item, a violation will occur
	###### we want to ignore those, but trap any others
		if ($@ and $@ !~ m/duplicate key violates unique/)
		{
			$rv .= "\n$@\n";
		}
		else
		{
			$rv .= "$a,";
		}
	}
	$sth->finish;
	# the caller a CVS response
	chop $rv;
	print $q->header('text/plain'), $rv;
}

sub CheckPrivileges
{
	###### we need to check their qualifications for access
	###### currently they have to be a 'network builder' or higher
	###### or they have to be the direct sponsor

	if ($member->{'id'} == $meminfo->{'id'})
	{
		Err('You cannot view your own followup record.');
		return undef;
	}
	
	if (grep {$_ eq $member->{'membertype'}} (qw|agu c d r t xag|))
	{
		my ($label) = $db->selectrow_array(qq|
			SELECT label FROM "Member_Types" WHERE "code" = '$member->{'membertype'}'|);
		Err("This report is unavailable on member type: $member->{'membertype'} - $label");
		return undef;
	}

	return 1 if $meminfo->{'id'} == $member->{'spid'};
	
	###### now we need to verify that the logged in party is really the upline
	###### of the refid

	my $rv = $db->selectrow_array("SELECT network.access_ck1($member->{'id'}, $meminfo->{'id'})");
#	warn "statement returned ($member->{'id'}, $meminfo->{'id'}): $rv";
	return 1 if $rv;
	
	# hack implementation per Dick to allow 4773425 and 3887940 access to anybody downline
	if ($db->selectrow_array("SELECT network.access_hack1($member->{'id'}, $meminfo->{'id'})"))
	{
		return 1;
	}

	Err($xml->{'noxs'} || "Your pay level or coaching position are insufficient to gain access to this information.");
	return undef;
}

sub createEmailTemplatesRelationships
{
	my $data = shift;

	my %rv = ();
#	$rv{'sponsor'} = ($member->{'spid'} == $meminfo->{'id'}) ? 1:0;
	$rv{'sponsor'} = ($data->{'sponsor'} == $meminfo->{'id'}) ? 1:0;
	$rv{'coach'} = ($data->{'coach'} == $meminfo->{'id'}) ? 1:0;
	$rv{'exec'} = ($data->{'exec'} == $meminfo->{'id'}) ? 1:0;

	return \%rv;
}

sub DeleteReminder
{
	my $pk = $mf->DecodeActivity($params{'pk'});
	if ($pk && $pk !~ m/\D/)
	{
		$db->do("DELETE FROM activity_rpt_data WHERE pk= ? AND id= ?", undef, $pk, $rid);
	}
	else
	{
		Err("There has been an error determining the reminder record");
	}
}

sub Display
{
	my $ql = QuickLinks->new($db, $gs->Get_LangPref);

	my $report_list =
		$member->{'membertype'} eq 'v' ? [7,15,1,16,4,10,3,12] :
		$member->{'membertype'} eq 'tp' ? [12,15,14] :
		$member->{'membertype'} eq 'm' ? [12,15] :
		$member->{'membertype'} eq 'ag' ? [12,17] : [12];

	if ($member->{'membertype'} eq 'tp')
	{
		# only provide the TP group report if they would have one
		push(@{$report_list}, 13) if $db->selectrow_array("SELECT TRUE FROM members WHERE membertype IN ('tp','v') AND spid= $member->{'id'} LIMIT 1");
	}
	
	my $tmpl = $gs->GetObject($TMPL{'tmpl'}) || die "Failed to load mail template: $TMPL{'tmpl'}\n";

	my $data = {
		'memberdetail'=>MemberDetail::Generate($db, $rid),
		'followup_tbl'=> GenerateFollowupTbl(),
		'followup_done_tbl'=> GenerateFollowupDoneTbl(),
		'pending_reminders'=>GeneratePendingReminders(),
		'script_name'=>$q->script_name,
		'reminder_date_cbo'=>ReminderDateCBO(),
		'alerts'=>$alerts,
		'reports'=> $ql->BuildOptionsList($member, $report_list)
	};
	ParseMe::ParseAll(\$tmpl, {'lb'=>$xml, 'data'=>$data, 'member'=>$member,
		'admin'=>{'notice'=>($ADMIN ? '<div style="color:red; font-weight:bold; text-align:center;">ADMIN MODE</div>' : '')}});
	print $q->header(
			'-type'=>'text/html',
			'-charset'=>'utf-8',
		), $tmpl;
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
	End();
}

sub FindMember
{
	my $qry = 'SELECT id, spid, alias, firstname, lastname, membertype FROM members WHERE ';
	if (($q->param('searchtype') || '') eq 'cellphone')
	{
		# we are using a functional index on the cellphone column to effectively ignore the spurious hyphens and spaces we have in the data
		$qry .= "TRANSLATE(cellphone,'- ','')"
	}
	elsif ($rid !~ /\D/)
	{
		$qry .= 'id';
	}
	elsif ($rid =~ /@/)
	{
		$qry.= 'emailaddress';
		$rid = lc $rid;
	}
	else
	{
		$qry .= 'alias';
		$rid = uc $rid;
	}
	my $sth = $db->prepare("$qry =?");
	$sth->execute($rid);
	my @l = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @l, $tmp;
	}
	
	unless (@l)
	{
		$errors .= "Failed to find a matching membership for: $rid";
		return undef;
	}

	if (scalar @l > 1)
	{
		$errors .= "More than one matching membership was identified for: $rid";
		return undef;
	}
	$rid = $l[0]->{'id'};
	return $l[0];
}

sub GenerateFollowupDoneTbl
{
	my $rv = '';
	# pull in the followed up activities and "followups" that are not associated with activities
	my $sth = $db->prepare(qq|
		SELECT
			ard.rpt_date,
			mft.val_text,
			m.alias,
			mft.entered::DATE,
			mft.pk,
			at.label,
			COALESCE(ard.val_text, '') AS reminder,
			ard.activity_type

		FROM member_followup_text mft
		JOIN members m
			ON m.id=mft.upline_id
		LEFT JOIN member_followup_key mfk
			ON mfk.mft_pk=mft.pk
		LEFT JOIN activity_rpt_data ard
			ON ard.pk=mfk.ard_pk
		LEFT JOIN activity_types at
			ON at.activity_type=ard.activity_type
		WHERE mft.id= ?
		ORDER BY mft.entered DESC, ard.rpt_date ASC
	|);
	$sth->execute($member->{'id'});
	my @followed = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @followed, $tmp;
	}
	
	my %f = ();
	my @fsort = ();
	if (@followed)
	{
		foreach my $entry(@followed)
		{
			# the followup reference will be used to build the bottom line in the particular group
			# the actual followup item itself
			# the array items will be used to build the activity entry rows to which the followup item applies
			unless ($f{$entry->{'pk'}})
			{
				push @fsort, $entry;
			}
			
			push @{$f{$entry->{'pk'}}}, $entry;
		}
	}
	
	foreach my $followup (@fsort)
	{
		$rv .= '<table class="followups_tbl">';
		foreach my $entry(@{$f{$followup->{'pk'}}})
		{
			# there may not be any associated activities, if that is the case, these values will be empty
			next unless $entry->{'rpt_date'};

			if (($entry->{'activity_type'} || 0) == 23)	# Aug. 2012, Dick wants the label to show instead of the numeric pay level value
			{
				$entry->{'reminder'} = $pay_level_labels->{ $entry->{'reminder'} }->{'lname'};
			}
			
			$rv .= $q->Tr({'-class'=>'activities_done'},
				$q->td({'-class'=>'date'}, $entry->{'rpt_date'}) .
				$q->td({'-class'=>'activity_type'}, $entry->{'label'}) .
				$q->td({'-class'=>'done_good'}, '&#10003;') .
				$q->td({'-class'=>($entry->{'reminder'} ? 'reminder' : 'empty_reminder')}, $entry->{'reminder'})
			);
		}
		
		$rv .= $q->Tr({'-class'=>'followup'},
			$q->td({'-class'=>'date'}, $followup->{'entered'}) .
			$q->td({'-class'=>'upline_id'}, $followup->{'alias'}) .
			$q->td({'-class'=>'followup_text', '-colspan'=>2}, $followup->{'val_text'})
		);
		$rv .= '</table>';
	}
	return $rv;
}

sub GenerateFollowupTbl
{
#	my $rels = $db->selectrow_hashref('SELECT coach, exec FROM my_coach_my_exec WHERE id= ?', undef, $member->{'id'});
	my $rels = $db->selectrow_hashref('SELECT sponsor, coach, exec FROM my_sponsor_coach_exec WHERE id= ?', undef, $member->{'id'});
	
	# if they load this interface too early after the new member is created, the underlying my_exec record will turn up empty and the 'exec' value will be null
	($rels->{'exec'}) = $db->selectrow_array('SELECT id FROM my_exec(?)', undef, $member->{'id'}) unless $rels->{'exec'};
	
	my $emtl = EmailTemplates::Links->new({'gs'=>$gs, 'recipientID'=>$member->{'id'}, 'recipient'=>$member});
	$emtl->setRelationships( createEmailTemplatesRelationships($rels) );
	
	my $rv = '';
	###### we'll first pull in activities that have not been followed up on, but which require followup
	my $sth = $db->prepare(qq|
		SELECT
			ard.pk,
			ard.rpt_date,
			at.label,
			COALESCE(ard.val_text,'') AS reminder,
			ard.activity_type
			
		FROM activity_rpt_data ard
		JOIN activity_types at
			ON at.activity_type=ard.activity_type
		LEFT JOIN member_followup_key mfk
			ON mfk.ard_pk=ard.pk
		WHERE at.followup=TRUE
		AND mfk.ard_pk IS NULL
		AND ard.id= ?
		AND ard.rpt_date <= NOW()::DATE
		ORDER BY ard.rpt_date DESC
	|);
	$sth->execute($member->{'id'});
	my @notfollowed = ();
	
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @notfollowed, $tmp;
	}

	if (@notfollowed)
	{
		$rv .= '<table class="activities_tbl">';
		my $eo = 'odd';
		foreach my $entry(@notfollowed)
		{
			if (($entry->{'activity_type'} || 0) == 23)	# Aug. 2012, Dick wants the label to show instead of the numeric pay level value
			{
				$entry->{'reminder'} = $pay_level_labels->{ $entry->{'reminder'} }->{'lname'};
			}
			
			my $emicon = '';
			$emicon = $emtl->anchorIconLight($entry->{'activity_type'}) if $emtl->getTmplID($entry->{'activity_type'});
			$rv .= $q->Tr({'-class'=>$eo},
				$q->td({'-class'=>'date'}, $entry->{'rpt_date'}) .
				$q->td({'-class'=>'activity_type'}, $entry->{'label'}) .
				$q->td({'-class'=>'ckbx'},
					$q->checkbox(
						'-name' => 'activities',
						'-value' => $mf->EncodeActivity($entry->{'pk'}),
						'-label' => ''
					) .
					$emicon
				) .
				$q->td({'-class'=>($entry->{'reminder'} ? 'reminder' : 'empty_reminder')}, $entry->{'reminder'})
			);
			$eo = $eo eq 'odd' ? 'even' : 'odd';
		}
		$rv .= '</table>';
	}

	return $rv;
}

sub GeneratePendingReminders
{
	my $rv = '';
	# pull in the reminders that are in the future
	my $sth = $db->prepare(qq|
		SELECT
			ard.pk,
			ard.rpt_date,
			COALESCE(ard.val_text, '') AS reminder
		FROM activity_rpt_data ard
		WHERE ard.id= ?
		AND ard.rpt_date > NOW()
		AND ard.activity_type=33
		ORDER BY ard.rpt_date ASC
	|);
	$sth->execute($member->{'id'});
	my @followed = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @followed, $tmp;
	}
	
	my %f = ();
	my @fsort = ();
	if (@followed)
	{
		$rv .= '<table class="followups_tbl">';
		my $eo = 'odd';
		foreach my $entry(@followed)
		{
			my $dellink = $q->script_name . "/delreminder?rid=$rid&pk=" . $mf->EncodeActivity($entry->{'pk'});
			$rv .= $q->Tr({'-class'=>$eo},
				$q->td({'-class'=>'date'}, $entry->{'rpt_date'}) .
				$q->td({'-class'=>($entry->{'reminder'} ? 'reminder' : 'empty_reminder')}, $entry->{'reminder'}) .
				$q->td( $q->a({'-href'=>$dellink}, $xml->{'delreminder'} ) )
			);
			$eo = $eo eq 'odd' ? 'even' : 'odd';
		}
		$rv .= '</table>';
	}
	return $rv;
}

sub LoadParams
{
	my @list = @_;
	foreach (@list)
	{
		if ($_ eq 'activities')
		{
			# we handle these as an array because they can be multiples
			foreach ($q->param($_))
			{
				push @{$params{'activities'}}, $gs->Prepare_UTF8($_);
			}
		}
		else
		{
			$params{$_} = $gs->Prepare_UTF8($q->param($_));
		}
	}
}

sub LoginCK
{
	# hack in admin access for Dick
	# Get the admin user and try logging into the DB
	if ($q->cookie('cs_admin'))
	{
		my $cs = cs_admin->new({'cgi'=>$q});
		my $operator = $cs->authenticate();
		if ($operator)
		{
			# Check for errors.
			exit unless $db = ADMIN_DB::DB_Connect( 'member-followup.cgi', $operator->{'username'}, $operator->{'password'} );
			$ADMIN = 1;
			return {
				'id' => 1,
				'alias' => "HQ Staff: $operator->{'username'}",
				'firstname' => $operator->{'username'},
				'lastname' => 'Staff'
			};
		}
	}

	my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic'));
#	my ($id, $meminfo) = GI_Authen::authen_ses_key( undef, undef, $q->cookie('GI_Authen_General') );

###### a valid id will not have anything besides digits
	unless ($id && $meminfo && $id !~ /\D/)
	{
		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
		return;
	}
###### if they are not a VIP, they don't have access to the report
	elsif ( $meminfo->{'membertype'} !~ 'v' )
	{
		Err('These Reports are for Partner members only'),
	}

	return $meminfo;
}

sub ReminderDateCBO
{
	my @dates = '';
	for (my $x=0; $x < 31; $x++)
	{
		# start with tomorrow and go out 30 days
		push @dates, sprintf("%4d-%02d-%02d", Add_Delta_Days( Add_Delta_Days(Today(), 1), $x))
	}
	return $q->popup_menu(
		'-name' => 'reminder_date',
		'-values' => \@dates
	);
}

sub SearchForm
{
	my $tmpl = $gs->GetObject($TMPL{'search'}) ||	die "Failed to load template: $TMPL{'search'}\n";

	my $data = {
		'script_name'=>$q->script_name,
		'alerts'=>$alerts,
		'errors'=>$errors
	};
	ParseMe::ParseAll(\$tmpl, {'lb'=>$xml, 'data'=>$data});
	print $q->header(
			'-type'=>'text/html',
			'-charset'=>'utf-8',
		), $tmpl;
}

sub SetVipNotes
{
	$params{'_action'} ||= 'add';	# we should never have an empty param, but go figure
	unless ($params{'vipnotes'})
	{
		$alerts = $q->span({'-style'=>'color:red'}, $xml->{'missing_vipnotes'});
		return;
	}

	# do we have a record in place already?
	my $xnotes = ();
	($xnotes) = $db->selectrow_array("SELECT val_text FROM member_followup_notes WHERE id= $member->{'id'}") if $params{'_action'} eq 'add';

	my $note = $xnotes ? "$xnotes\n" : '';
	my ($now) = $db->selectrow_array('SELECT NOW()::DATE');
	$note .= "$now, $meminfo->{'alias'}: " . $q->escapeHTML($params{'vipnotes'});

	$db->do("DELETE FROM member_followup_notes WHERE id= $member->{'id'}");
	$db->do("INSERT INTO member_followup_notes (id, val_text) VALUES ($member->{'id'}, ?)", undef, $note);
}

# 04/17/09 added checks to disallow viewing followup records on inactive or non-standard members, ie. c,r,t,etc
# 01/31/11 no significant changes made
# 12/12/11 revised the check for inclusion in a "pool"
###### removed the role checking stuff, more single quote escaping of hash keys
###### 08/27/12 made it so that activity type 23 (new pay level) displays the abbreviation instead of the numeric value
###### 11/12/12 added the capability to search by cellphone
###### 02/12/13 slight tweak to deal with an undefined search parameter
###### 06/14/13 no real changes, only small rearrangements after recognizing that the DB function the LoginCK routine was using needed some changes to work properly
###### 02/17/14 put the no access "error" message in the language pack and also defined TP as a particular membertype for the purpose of generation of BuildOptionsList
# 04/29/14 integrated the conditional email icon instead of the unconditional one
# 09/03/14 discovered that we were still running with a "test" template, so moved it back to the primary
#	also added some items to the BuildOptionsList for trial partners
# 11/10/14 broke the SQL statements out into separate db calls in SetVipNotes()
# 02/04/15 added a QuickLinks configuration for AGs
# 03/04/15 slight mods to change the concept of the sponsor from the literal sponsor to the partner sponsor
# 05/06/15 added another line of code in GenerateFollowupTbl to pull in the Exec if the do-it-all SQL call failed to obtain that value
# 05/11/15 made it possible to set a follow up record without checking an activity box
# 05/14/15 added the recipient key to the initialization call to emtl
