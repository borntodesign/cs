#!/usr/bin/perl 
###### Program Name: network_reports.cgi
###### Written:	07/12/04	Karl Kohrt
######
###### Build and store a member's request for a downline network report.
###### It allows the user to define criteria for the report.
######
###### last modified: 07/02/15 Bill MacArthur

use lib ('/home/httpd/cgi-lib');
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use G_S qw($LOGIN_URL);
use DB_Connect;
use XML::local_utils;
use GI;
use DateTime;
#use Data::Dumper;	# for testing only

###### CONSTANTS
my $MEM_LEVELS = 'v';			# Which membertypes can access this script.
my @MEMBERTYPES = qw/s m v ag tp ma/;	# The membertypes that can appear in the reports.	
my %OPERATORS = (
	'1' => 'NULL',	# ==
	'2' => 'TRUE',	# >
	'3' => 'FALSE',	# <
	'4' => '&lt;',
	'5' => '&gt;',
	'6' => '='
);
my %TMPL = (
#	'request'	=> 10328,
	'request'	=> '/home/clubshop.com/cgi-templates/TT/network_reports.tt',
	'XML'		=> 10832
);
# The order in which the columns should appear in the combo box.
my @COLUMN_LIST =
(
	'firstname',
	'lastname',
	'alias',
	'emailaddress',
	'mail_option_lvl',
	'membertype',
	'spid',
	'id',
	'join_date',
	'a_level_stable.a_level',
	'refcomp.ppp',
	'refcomp.opp',
	'rp_summary_vw.balance',
	'address1',
	'address2',
	'city',
	'state',
	'postalcode',
	'country',
	'language_pref',
	'phone',
	'cellphone',
	'other_contact_info',
	'company_name',
	'od.pool_vw.deadline',
	'unpaid'
);			
# The values that will populate the combo box.
my %COLUMN_LIST = (
			'firstname'		=> {
						'english'	=> 'First Name',
						'selected'	=> 'selected="selected"' },							
			'lastname'		=> {
						'english'	=> 'Last Name',
						'selected'	=> 'selected="selected"' },
			'alias'		=> {
						'english'	=> 'ID',
						'selected'	=> 'selected="selected"' },
			'emailaddress'	=> {
						'english'	=> 'Email Address',
						'selected'	=> 'selected="selected"' },
			'mail_option_lvl' => {
						'english'	=> 'Email Type',
						'selected'	=> 'selected="selected"' },
			'membertype'		=> {
						'english'	=> 'Member Type',
						'selected'	=> 'selected="selected"' },
			'spid'			=> {
						'english'	=> 'Sponsor ID',
						'selected'	=> '' },
			'id'			=> {
						'english'	=> 'Numeric ID',
						'selected'	=> '' },
			'join_date'		=> {
						'english'	=> 'Join Date',
						'selected'	=> '' },
			'a_level_stable.a_level'	=> {
						'english'	=> 'Pay Level',
						'selected'	=> '' },
			'refcomp.ppp'	=> {
						'english'	=> 'Personal Pay Points',
						'selected'	=> '' },
			'refcomp.opp'	=> {
						'english'	=> 'Team Pay Points',
						'selected'	=> '' },
			'rp_summary_vw.balance'	=> {
						'english'	=> 'ClubCash Balance',
						'selected'	=> '' },
			'address1'	=> {
						'english'	=> 'Address 1',
						'selected'	=> '' },
			'address2'	=> {
						'english'	=> 'Address 2',
						'selected'	=> '' },
			'city'	=> {
						'english'	=> 'City',
						'selected'	=> '' },
			'state'	=> {
						'english'	=> 'State/Province',
						'selected'	=> '' },
			'postalcode'	=> {
						'english'	=> 'Postal Code',
						'selected'	=> '' },
			'country'	=> {
						'english'	=> 'Country',
						'selected'	=> '' },
			'language_pref'	=> {
						'english'	=> 'Language Preference',
						'selected'	=> '' },
			'phone'	=> {
						'english'	=> 'Telephone',
						'selected'	=> '' },
			'cellphone'	=> {
						'english'	=> 'Cell Phone',
						'selected'	=> '' },
			'other_contact_info'	=> {
						'english'	=> 'Skype Name',
						'selected'	=> '' },
			'company_name'	=> {
						'english'	=> 'Company Name',
						'selected'	=> '' },
			'od.pool_vw.deadline'	=> {
						'english'	=> 'Trial Partner Expiration Date',
						'selected'	=> '' },
			'unpaid'				=> {
						'english'	=> 'Unpaid',
						'selected'	=> '',
						'id'		=> 1,
						'disabled'	=> 1 }
			);

###### GLOBALS
my ($db, $lang_blocks) = ();
my $TT = Template->new({'ABSOLUTE' => 1});
my $GI = GI->new();
my $q = new CGI;
$q->charset('utf-8');
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my $lu = XML::local_utils->new;
my $ME = $q->script_name;
my $action = $q->param('action') || '';
my $report_name = $q->escapeHTML(substr(($q->param('report_name') || 'Network Report'),0,50));
my $member_id = ();					# The requesting member's ID.
my $start_id = ();					# The starting ID for the report.
my $alias = ();
my $membertype = ();
my $login_error = 0;
my $header = '';
my %TABLES = ();
my $lang_pref = $gs->Get_LangPref || 'en';

################################
###### MAIN starts here.
################################

my $member_info = Get_Cookie();
Load_lang_blocks();
my $lv = $db->selectall_hashref('SELECT lvalue, lname, description FROM level_values', 'lvalue');

unless ( $login_error )
{
	# Make sure they are at a high enough membership level to access the forms.
	if ( $membertype !~ $MEM_LEVELS )
	{
		Err("DHS ID #$alias<br /><br/>
			These reports are not available at your membership level($membertype).");
	}
	else
	{
		if ( $action eq 'submit' )
		{
			# If no starting ID was passed, default to the requester's ID.
			$start_id = $q->param('start') || $member_id;
			
			# Remove any whitespace and/or leading zeros from the ID.
			$start_id =~ s/\s//g;
			$start_id =~ s/^0*//;

			# If the start_id submitted is an alias, convert it.
			if ( $start_id =~ /\D/ )
			{
				my $sth = $db->prepare("SELECT id from members WHERE alias = ?;");
				$sth->execute(uc $start_id);
				$start_id = $sth->fetchrow_array || $member_id;
			}
						
			# Default - The starting id is the requesting member's id.
			my $downline_ok = 1;

			# If not, check to see if the start_id is in their downline.
			unless ( $start_id == $member_id )
			{	
				#$downline_ok = $gs->Check_in_downline( $db, 'members', $member_id, $start_id );
				# no good since network.relationships went away($downline_ok) = $db->selectrow_array("SELECT network.is_nspid_above_you($start_id, $member_id)");
				($downline_ok) = $db->selectrow_array("SELECT network.access_ck_partner_partner($start_id, $member_id)");
			}

			# If they are reporting on themselves or their downline.			
			if ( $downline_ok )
			{
				# Create the argument string used for report generation.
				my $arg_string = Create_Arg_String();

				# Submit the request to the database.
				if ( Submit_Request( $member_id, $arg_string ) )
				{
					# Display a success message.
					my $content = "<div align='center'>
						$lang_blocks->{'submit_success'}<br /><br />
						<a href='$ME'>$lang_blocks->{'reqanother'}</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='http://www.clubshop.com/cgi/vip/network_reports_status.cgi'>$lang_blocks->{'view_rpt_status'}</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='http://www.clubshop.com/p/index.shtml'>$lang_blocks->{'return2cc'}</a>
						</div>";
					print $q->header('-charset'=>'utf-8'),
					$GI->wrap({
						'title'=>$lang_blocks->{'title'},
						'style'=>$GI->StyleLink('/css/cgi/clubcontact.css'),
						'content'=>\$content
					});
				}
				else
				{
					# Display a failure message.
					Err("<div align='center'>
						$lang_blocks->{'submit_failure'}<br /><br />
						<a href='$ME'>$lang_blocks->{'reqanother'}</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='http://www.clubshop.com/cgi/vip/network_reports_status.cgi'>$lang_blocks->{'view_rpt_status'}</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href='http://www.clubshop.com/p/index.shtml'>$lang_blocks->{'return2cc'}</a>
						</div>"
						);
				}
			}
			else
			{
				# Display a failure message.
				Err($lang_blocks->{'bad_starting'});
			}
		}
		elsif ( $action eq '' )
		{
			Display_Request_Page();
		}
		else
		{
			Err('The action requested is not supported.');
		}
	}	
	$db->disconnect;
}
exit;

################################
###### Subroutines start here.
################################

###### Create the column list combo box.
sub cbo_Column_List
{
	my $cbox = qq!<select name="col_list" size="5" class="input" multiple="multiple">!;
	foreach ( @COLUMN_LIST )
	{
		my $opts = {};
		$opts->{'value'} = $_;
		$opts->{'selected'} = 'selected' if $COLUMN_LIST{$_}->{'selected'};
		$opts->{'id'} = 'col_list_' . $_ if $COLUMN_LIST{$_}->{'id'};
		$opts->{'disabled'} = 'disabled' if $COLUMN_LIST{$_}->{'disabled'};
		$cbox .= $q->option($opts, $COLUMN_LIST{$_}->{'english'});
#		$cbox .= qq!<option value="$_" $COLUMN_LIST{$_}->{'selected'}>$COLUMN_LIST{$_}->{'english'}</option>!;
	}		
	$cbox .= "</select>";
	return $cbox;
}	

sub cboCountryList
{
	my $sth = $db->prepare("SELECT country_code, country_name FROM country_names_translated(?)");
	$sth->execute($lang_pref);
	my @vals = '';								# these two lines create our initial entries
	my %labels = (''=>$lang_blocks->{'all'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @vals, $tmp->{'country_code'};
		$labels{ $tmp->{'country_code'} } = $gs->Prepare_UTF8($tmp->{'country_name'}, 'encode');	# for the live server
#		$labels{ $tmp->{'country_code'} } = $tmp->{'country_name'};	# for the older server
	}
	
	return $q->popup_menu(
		'-name'=>'country',
		'-values'=>\@vals,
		'-class'=>'ctl',
		'-labels'=>\%labels,
		'-default'=>'',
		'-multiple'=>'multiple',
		'-size'=>5,
		'-style'=>'vertical-align:top'
	);
}

sub cboLanguageList
{
	my $sth = $db->prepare("SELECT code2, description FROM language_codes(?) WHERE active=TRUE");
	$sth->execute($lang_pref);
	my @vals = '';								# these two lines create our initial entries
	my %labels = (''=>$lang_blocks->{'all'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @vals, $tmp->{'code2'};
		$labels{ $tmp->{'code2'} } = $tmp->{'description'};
	}
	
	return $q->popup_menu(
		'-name'=>'language_pref',
		'-values'=>\@vals,
		'-class'=>'ctl',
		'-labels'=>\%labels,
		'-default'=>'',
		'-multiple'=>'multiple',
		'-size'=>5,
		'-style'=>'vertical-align:top'
	);
}

sub cboRptType
{
	my $id = shift;
#	my $rid = Validate_ID() || $meminfo;	# we never present the menu page as a menu page for the jump to ID

	my @vals = ('personal', 'prps');	# what every Partner gets
#	push @vals, 'coach' if $member_info->{'a_level'} >= 5;	# BM+ are coaches
	push(@vals, 'coach') if $db->selectrow_array("SELECT id FROM my_coach WHERE upline= $member_info->{'id'} LIMIT 1");
	push @vals, 'entire' if $member_info->{'a_level'} >= 8;	# ED+ are entitled to an entire team report

	my %labels = (
		'personal'=>'Personal',
		'prps'=>'PRPs',
		'coach'=>'Coach',
		'entire'=>'Team'
	);

	return $q->popup_menu(
		'-name'=>'rptype',
		'-values'=>\@vals,
		'-class'=>'ctl',
		'-labels'=>\%labels,
		'-default'=>'personal'
	);
}

sub cboTPexpy
{
	my $now = DateTime->now;
	my @dates = '';

	for (my $x = 1; $x < 31; $x++)
	{
		push @dates, $now->ymd();
		$now->add('days' => 1);
	}

	return $q->popup_menu(
		'-name'=>'tpx_date',
		'-values'=>\@dates,
		'-default'=>''
	);
}

###### Create the argument string to be inserted into the report request table.
sub Create_Arg_String
{
	# The values that we want to filter out and how we want them filtered.
	my $FILTER_ARGS = "<criteria>" .
		CriteriaCountry() .
		CriteriaFormerPartners() .
		CriteriaMembertype() .
		CriteriaPayLevel() .
		CriteriaLanguagePref() .
		CriteriaMailOptionLvl() .
		CriteriaTPexpiration() .
		CriteriaUnpaid() .
		"</criteria>\n";

	my $rv =
		qq'<root version="1"><start_id>$start_id</start_id>\n' .
		Xids() .
		Columns() .
		$FILTER_ARGS .
		Tables() .
		'</root>';

#	$arg_string =~ s/[\n\r\t]//g;
	return $rv;
}

sub CriteriaCountry
{
	my @countries = $q->param('country');	# if the all option is selected, the value is empty.... so we will not have any country criteria
	return '' unless $countries[0];
	my $rv = '';
	foreach my $c (@countries)
	{
		next unless $c =~ m/^[A-Z]{2}$/;
		$rv .= "$c,";
	}
	$rv =~ s/,$//; # remove trailing comma if present
	return qq|<item description="$lang_blocks->{'criteria_lbl_country'} $rv" name="country">$rv</item>\n|;
}

sub CriteriaFormerPartners
{
	return '' unless $q->param('former_partners');
	
	$TABLES{'priorvs'} = 1;
	
	# this is a minimal bit of XML just to keep the reporting functionality... there is no need for a real SQL query modifier on here as it is handled by the straight join
	return return qq|<item description="$lang_blocks->{'former_partners'}"></item>\n|;
}

sub CriteriaLanguagePref
{
	my @prefs = $q->param('language_pref');	# if the all option is selected, the value is empty.... so we will not have any language_pref criteria
	return '' unless $prefs[0];
	my $rv = '';
	foreach my $lp (@prefs)
	{
		next unless $lp =~ m/^[a-z]{2}$/;
		$rv .= "$lp,";
	}
	$rv =~ s/,$//; # remove trailing comma if present
	return qq|<item description="$lang_blocks->{'criteria_lbl_lang_pref'} $rv" name="language_pref">$rv</item>\n|;
}

sub CriteriaMailOptionLvl
{
	# if they have selected good email addresses only
	if ($q->param('nonbem'))
	{
		return qq|<item description="$lang_blocks->{'nonbem'}" name="mail_option_lvl"><op>&gt;</op><value>0</value></item>\n|;
	}
	else
	{
		return '';
	}
}

sub CriteriaMembertype
{
	my $membertypes = $db->selectall_hashref('SELECT "code", label FROM "Member_Types" WHERE active=TRUE', 'code');
	my @mtypes = $q->param('mtype');

	# give it our defaults if they "selected" 'All'
	@mtypes = @MEMBERTYPES unless $mtypes[0];
	
	my $rv = '';
	foreach my $mtype (@mtypes)
	{
		next unless grep $mtype eq $_, @MEMBERTYPES;
		$rv .= "$mtype,";
	}
	$rv =~ s/,$//; # remove trailing comma if present
	
	my $description = $lang_blocks->{'criteria_lbl_mtype'};
	foreach my $d (@mtypes)
	{
		$description .= " $membertypes->{$d}->{'label'},";
	}
	chop $description;
	return qq|<item description="$description" name="membertype">$rv</item>\n|; # = ANY ('{$rv}')
}

sub CriteriaPayLevel
{
	return '' unless $q->param('a_level') && $q->param('a_level_op');
	my $description = 'Pay level ';
	my $op = '';
	# If we have A_level filters... they will really be passed into the network_report_x functions as they are not a final criteria
			
	# If this is an operator in our hash, assign it to the filter.
	if ( $OPERATORS{$q->param('a_level_op')} )
	{
		$op = $OPERATORS{$q->param('a_level_op')};
		$description .=
			($op eq 'NULL') ? ('equals ' . $lv->{$q->param('a_level')}->{'lname'}) :
			($op eq 'TRUE') ? ('greater than ' . $lv->{$q->param('a_level')}->{'lname'}) :
								('less than ' . $lv->{$q->param('a_level')}->{'lname'});
	}
	# Otherwise, error out since this means someone has probably munged the URL.
	else
	{
		my $error = $q->param('a_level_op');
		Err("An invalid Achievement Level ($error) was submitted. Your request was not processed.");
		exit;
	}

	return qq|<item description="$description" name="pay_level"><op>$op</op><pay_level>| . $q->param('a_level') . "</pay_level></item>\n";
}

sub CriteriaTPexpiration
{
	return '' unless $q->param('tpx_date') && $q->param('tpx_op');

	# now validate that the date we received was really a date
	my $xdate = $q->param('tpx_date');
	eval{ $xdate = $db->selectrow_array('SELECT ?::DATE', undef, $xdate) };
	Err('The expiration date received was invalid: ' . $q->param('tpx_date')) if $@;
	
	my $description = 'Trial Partner Expiration Date ';
	my $op = '';
			
	# If this is an operator in our hash, assign it to the filter.
	if ( $OPERATORS{$q->param('tpx_op')} && grep $q->param('tpx_op') eq $_, (4,5,6) )
	{
		$op = $OPERATORS{$q->param('tpx_op')};
		$description .= "$op $xdate";
#			($op eq '=') ? ('equals ' . $xdate) :
#			($op eq '>') ? ('greater than ' . $xdate) :
#								('less than ' . $xdate);
	}
	# Otherwise, error out since this means someone has probably munged the URL.
	else
	{
		my $error = $q->param('tpx_op');
		Err("An invalid comparison operator ($error) was submitted. Your request was not processed.");
	}

	# if they have not selected the deadline date as an output field, we still need to create the join for this criteria to work
	$TABLES{'od.pool_vw'} = 1;
	return qq|<item description="$description" name="od.pool_vw.deadline"><op>$op</op><value>| . $xdate . "</value></item>\n";
}

sub CriteriaUnpaid
{
	# if they have selected good email addresses only
	return '' unless $q->param('unpaid');
	
	$TABLES{'nop'} = 1;
	
	# this is a minimal bit of XML just to keep the reporting functionality... there is no need for a real SQL query modifier on here as it is handled by the straight join
	return return qq|<item description="$lang_blocks->{'unpaid'}"></item>\n|;
}

###### Display the request page.
sub Display_Request_Page
{
#	my $report_body = $gs->Get_Object($db, $TMPL{'request'}) || die "Couldn't open $TMPL{'request'}";

	my $rv = '';
	my $data = {
			'lang_blocks' => $lang_blocks,
			'form_action' => $ME,
			'timestamp' => scalar localtime(),
			'column_list' => cbo_Column_List(),
			'cboCountryList' => cboCountryList(),
			'cboLanguageList' => cboLanguageList(),
			'cboReportType' => cboRptType(),
			'cboTPexpy' => cboTPexpy(),
			'member_info' => $member_info
		};

#	$TT->process(\$report_body, $data, \$rv);
	$TT->process($TMPL{'request'}, $data, \$rv);
	print $q->header,
		$GI->wrap({
			'title'=>$lang_blocks->{'title'},
			'style'=>$GI->StyleLink('/css/cgi/clubcontact.css'),
			'script'=>'<script src="/js/FriendlySelects.js" type="text/javascript"></script>
					<script src="/js/clubcontact.js" type="text/javascript"></script>',
			'content'=>\$rv
		});
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	my $mem_info = '';
	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		my $admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect('network_reports.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}
		unless ( ($alias = $member_id = $q->param('id')) )
		{
			Err('Missing a required parameter');
			$login_error = 1;
		}
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect('network_reports.cgi');
		$db->{'RaiseError'} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id )
		{
			# Assign the info found in the VIP cookie.
			$alias = $mem_info->{'alias'};
			$membertype = $mem_info->{'membertype'};					
		}
					
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			$login_error = 1;
		}
	}
	
	# we need the pay level to determine what report types to show on the initial screen
	($mem_info->{'a_level'}) = $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= $member_id") || 0;
	
	return $mem_info;
}

###### Prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), $_[0];
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Load_lang_blocks
{
	my $xml = $gs->GetObject($TMPL{'XML'} ) || die "Failed to load lang_blocks\n";
	$lang_blocks = $lu->flatXMLtoHashref($xml);
	$gs->Prepare_UTF8($lang_blocks,'encode');
}

###### Get the output args(columns) and put them into a string format.
sub Columns
{
	# The columns that we want output.
	my @OUTPUT_ARGS = $q->param('col_list');
	my $OUTPUT_ARGS = "<columns>";
	foreach my $col ( @OUTPUT_ARGS )
	{
		# ignore any that are not in our defined list
		next unless grep $col eq $_, @COLUMN_LIST;

		$OUTPUT_ARGS .= qq|<column description="$COLUMN_LIST{$col}->{'english'}">$col</column>\n|;

		# While we're looking at the output column list, lets note 
		#  which tables are involved if the field name includes a table, which means it will have a dot in it
		if ($col =~ m/(.*)\.\w+$/)	# if we come through without a dot in the column name, we do not have to specify the table
		{
			$TABLES{$1} = '1';
		}			
	}

	$OUTPUT_ARGS .= qq|<column description="Former Partner">former_partner</column>\n| if $q->param('former_partners');
	$OUTPUT_ARGS .= "</columns>\n";

	return $OUTPUT_ARGS;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	print $q->redirect( $LOGIN_URL . "?destination=" . $ME );
}

###### Submit the request to be processed by the backend script - network_reports.pl.
sub Submit_Request
{
	my ( $id, $arg_string ) = @_;
	my $sth = $db->prepare("
		INSERT INTO network_reports (
			id,
			arg_string,
			report_name,
			submitted )
		VALUES	(	?, ?,?, NOW());");
	my $rv = $sth->execute($id, $arg_string, $report_name);
	return $rv;
}

sub Tables
{
	my $TABLE_ARGS = "<tables>";
	foreach ( keys %TABLES )
	{
		# List all tables except for the members table.
		next if $_ eq 'members';
		
		$TABLE_ARGS .= "<table>$_</table>";
	}

	$TABLE_ARGS .= '<base_function ';
	$TABLE_ARGS .=
		($q->param('rptype') eq 'personal') ? 'description="Personal">network.network_reports_personal' :
		($q->param('rptype') eq 'prps') ? 'description="PRPs">network.network_reports_prps' :
		($q->param('rptype') eq 'coach') ? 'description="Coach">network.network_reports_by_coach' :
		($q->param('rptype') eq 'entire') ? 'description="Director/Entire">network.network_reports_by_director' : Err('Invalid report type received: ' . $q->param('rptype'));
	$TABLE_ARGS .= "</base_function></tables>";

	return $TABLE_ARGS;
}

sub Xids
{
	my $xidlist = $q->param('xlist') || return '';
	my $rv = '<xids>';
	$xidlist =~ s/\r//g;				# Remove any windows return codes.
	foreach my $id (split (/\n/, $xidlist))
	{
		$id =~ s/\s*//g;	# get rid of all whitespace
		($id) = $db->selectrow_array('SELECT id FROM members WHERE alias= ?', undef, $id) if $id =~ m/\D/;
		next unless $id;
		$rv .= "<id>$id</id>";
	}
	
	return $rv . "</xids>\n";
}

__END__

###### 07/23/04 Added the Numeric ID field to the output list. Changed 'ID' to look at alias instead of id.
###### 08/18/04 Added a check of the start_id to determine if it is an alias and to find ID if it is.			
###### 10/26/04 Added the Join Date field to the output list.
###### 12/09/04 Added code to strip whitespace and leading 0's from member IDs.
###### 12/29/04 Added the error page for those not at a high enough role to access the script.
######		Removed the Modify_This javascript call from the cbo_Column_List menu. 
######			(Also removed the Modify_This call from the template's Membertype menu.)
###### 02/02/05 Changed the access level from Builder Team Coach(100) to Builder Intern(40) per Dick.
###### 02/03/05 Added the Personal Pay Points field to the output list.
###### 02/15/05 Added the Email Type field to the output list.
###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login. Added query to get alias value.
###### 03/14/05 Changed $ME assignment from $q->url to $q->script_name. Oops!
###### 04/18/05 Changes to template #10328 DreamTeam level menu only. Removed the Communicator role,
######		added the Marketing Director, and moved the Strategist from above to below the Trainer. 
# 05/31/06 a quick application of substr to the assignment of the report_name parameter
# 07/05/06 added handling for the Company Name
# 11/29/06 added Reward Points Balance to the list of columns
###### 12/19/07 removed phone_ac and added cellphone & other_contact_info
# 09/21/08 adding filtering of leading and trailing whitespace on the submitted exclusion list items
# 12/18/08 added a check for exclusion items that were only spaces
###### 01/11 added additional filters
###### 03/11 g.baker changed Reward Points title to CLubCash
###### 12/02/11 started removing things related to the role system
###### 01/31/12 finally moved into place
###### 03/06/12 revamped to remove references to the old role, member_pools and network calcs datasets
###### 11/14/13 added 'tp' to the list of available report membertypes
###### 02/27/14 added routines and other values to support the trial partner expiration date as a filter and column selection
# 05/09/14 changed the concept of a coach for cboRptType to actually look to see if the user is a coach to somebody
# 07/22/14 added functionality for the Former Partners filter
# 02/12/15 added merchant affiliates to the list of memberships that can be pulled

03/13/15
	Added HTML escaping of the report name as well as applying Prepare_UTF8 so that valid UTF-8 characters could make it into the DB
05/06/15
	Added team PP into the values available at the reports
06/10/15
	Revised the downline check to use network.access_ck_partner_partner() instead of the older DB function which relied on the defunct network.relationships