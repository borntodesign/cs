#!/usr/bin/perl -w
###### activity_rpt
###### renders reports on various 'activities' in one's downline
###### created: 01/15/04	Bill MacArthur
###### last modified: 02/11/16	Bill MacArthur

# 07/15/15 custom hacks put into place to allow 4773425 and 3887940 unlimited downline access per Dick
# hacks removed 02/2016 per Dick

###### we will not plan for Admin access due to the hassle of having to retain an ID parameter
###### they can login as the individual if necessary
use 5.010;
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::LibXML::Simple;
use Template;
use JSON;
use Encode;
use Data::Dumper;	# for debugging only
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
use DB_Connect;
use MemberFollowup;
use EmailTemplates::Links;
#binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
my $IGNORE_STAFF = 1;	# when testing partner functionality, it is so much easier to not have the thing default to 01 while being logged in as staff
my $TT = Template->new();
my $q = new CGI;
$q->charset('utf-8');

my $NO_DATA_FLAG = 0;
my %blts = (			# image bullets used in the report
	'i' => {
		'-src'=>'/images/icons/icon_more_info.gif',	# this is the little circle with an "i" in it for more information
		'-height'=>15,
		'-width'=>15,
		'-alt'=>'',
		'-border'=>0
	},
	'g' => {
		'-src'=>'/images/rd-grn-blt-10x10.gif',
		'-height'=>10,
		'-width'=>10,
		'-alt'=>'',
		'-border'=>0
	},
	'o' => {
		'-src'=>'/images/bullet-orange-arrow-10x10.gif',
		'-height'=>10,
		'-width'=>10,
		'-alt'=>'',
		'-border'=>0
	}
);

###### by adding a constantly changing value as a path,
###### perhaps we can avoid browser caching
my $script_name = $q->script_name . "/${\time}";

###### if we are looking for these activity types, we'll be populating the javascript for reports
my $generate_js = 0;
$generate_js = 1 if grep $q->param($_), (1,2);

###### this is our acceptable list of parameters
###### we'll preload it with alphas and after loading options, we'll add in the numerics
my @param_list = qw/rpt_date_start rpt_date_end sort subscription-consent confirm rptype/;

my $action = $q->param('action');
my ($db, %params, %cookies, %options, @ck, $rid, @option_sort, %conditionals, $id, $meminfo, $lang_blocks, %CSVlists, %atypes_present) = ();
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $page_language = 'en';	# just a default which can be reset when we load the lang blocks
my $_exclusion_list = '';
my $option_list = '';

my %TMPL = (
	'menu'		=> 7000, #3101
	'report'	=> 'projects/activity_rpt/report.tt',#7001, #10190
	'xml'		=> 7006,
	'richtext'	=> 7007,
	'batch_time'	=> 7002
);

my %TABLES = (	# convenient for testing
	'activity_types' => 'activity_types',
	'activity_rpt_data' => 'activity_rpt_data',
	'activity_types_translated' => 'activity_types_translated',
	'ard_personal'	=> 'network.ard_personal',
	'ard_prps'		=> 'network.ard_prps',
	'ard_coach'		=> 'network.ard_coach',
	'ard_group'		=> 'network.ard_group',
	'ard_team'		=> 'network.ard_team',
	'member_followup_key' => 'member_followup_key'
);

my $MFUR = 'https://www.clubshop.com/cgi/vip/member-followup.cgi';

# if a membertype appears in here, then the only activities we will show for them are in the value
my %HideThese = (
	't'	=> [],
	'c'	=> [],
	'r'	=> [15],
	'd'	=> []
);

# converted these to classes
my %bgcolor = (1=>'f', 2=>'e');

# Dick wants a staff login to have 01 access...
if ((my $ck = $q->cookie('cs_admin')) && ! $IGNORE_STAFF)
{
	# no point pulling in this stuff for the occasional admin use
	eval {
		require ADMIN_DB;
		require cs_admin;
	};
	my $cs = cs_admin->new({'cgi'=>$q});

	my $creds = $cs->authenticate($ck);
	exit unless $db = ADMIN_DB::DB_Connect( 'genealogy', $creds->{'username'}, $creds->{'password'} );
	$id = 1;
	$meminfo = $db->selectrow_hashref('SELECT id, alias, firstname, lastname, membertype, 11 AS a_level FROM members WHERE id= 1');
	$gs->Prepare_UTF8($meminfo);
}
else
{
	( $id, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
}

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	print $q->redirect("$LOGIN_URL?destination=" . $script_name);
	exit;
}

###### if they are not a VIP, they don't have access to the report
if ( $meminfo->{'membertype'} !~ 'v' )
{
	print
		$q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3('Activity Reports are for Partner members only'),
		$q->end_html();
		exit;
}

$db ||= DB_Connect('activity_rpt', undef, {'return_redirects'=>1});
if (! $db)
{
	Err("Something went wrong trying to get a database connection");
	exit;
}
elsif (! ref($db))	# we will receive a string back from DB_Connect if we failed our connection
{
	print $q->redirect($db);
	exit;
}

$db->{'RaiseError'} = 1;
#my $emtl = EmailTemplates::Links->new({'gs'=>$gs});

$meminfo->{'a_level'} ||= $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= $meminfo->{'id'}") || 0;
$meminfo->{'iscoach'} = $db->selectrow_array("SELECT is_partner_coach($meminfo->{'id'})");

my $mf = MemberFollowup->new($db);

Get_Option_List();
Load_Params();
LoadLangBlocks();

unless ($action)
{
	Load_Cookies();
	$rid = Validate_ID( GetReportee() );
	exit if defined $rid && $rid == 0;
	
	Select_Options();
}
elsif ($action eq 'report')
{
	TrackUsage();
	
	###### we'll check to see if this is supposed to be a downline report
	###### and validate the input if so
	###### the 'rid' comes from the text field, the '_rid' from the select box
	$rid = GetReportee();

	if ($rid)
	{
		$rid = Validate_ID($rid);	# this returns a hashref
		Exit() unless $rid;
	}
	else
	{
		$rid = $meminfo;
	}

	if (! exists $rid->{'iscoach'})
	{
		$rid->{'iscoach'} = $db->selectrow_array("SELECT is_partner_coach($meminfo->{'id'})");
	}
	
	if ( Validate_Params() )
	{
		###### if they haven't selected any options, then this will be empty
		###### and it creates a nasty SQL error
		unless ($option_list)
		{
			Err( $lang_blocks->{'you_must_select_act'} );
		}
		else
		{
			Create_Cookie();
			Do_Report();
		}
	}
	else
	{
	###### if their parameters are incorrect or empty, then we'll just go back
	###### to square one
		Load_Cookies();
		Select_Options();
	}
}
else
{
	Err('Undefined Action.');
}

Exit();

###### Start of sub-routines ######

sub ARInstructions
{
	my ($acttype, $id, $lbl) = @_;
	return $q->a({
		'class'	=> 'cluetip_activator',
		'href'	=> '#',
		'rel'	=> "/cgi/vip/activity_report_instructions.cgi?acttype=$acttype;rid=$id;vid=$rid->{'id'}",
		'title'	=> $lbl
	}, Bullet({'type'=>'i', 'alt'=>$lbl}));
}

sub Bullet
{
	my $args = shift || {};
#warn 'args: '. Dumper($args);
	my $btn = $blts{ $args->{'type'} } || $blts{'g'};
	$btn->{'-alt'} = $args->{'alt'} if $args->{'alt'};
	$btn->{'-title'} = $args->{'title'} if $args->{'title'};
	
	$btn->{'-style'} = 'vertical-align:middle;' . ($args->{'style'} || '');
		
	if ($args->{'anchor'})
	{
		my $hr = {};

		$hr->{'-title'} = $args->{'title'} if $args->{'title'};
		$hr->{'-class'} = $args->{'class'} if $args->{'class'};
		$hr->{'-data-id'} = $args->{'data-id'} if $args->{'data-id'};
		$hr->{'-href'} = ($args->{'href'} || '#'),
		$hr->{'-onclick'} = "$args->{'onclick'}; return false;" if $args->{'onclick'};
#warn 'hr: '. Dumper($hr);
		$btn = $q->a($hr, $q->img($btn))
	}
	else
	{
		$btn = $q->img($btn);
	}
	return $btn;
}

sub cboDateSelector
{
	###### create a drop down box for an available range of dates
	my ($sth, @list) = ();

	$sth = $db->prepare("
	/*	SELECT DISTINCT ark.rpt_date
		FROM activity_rpt_key ark
		WHERE ark.id= ?
		AND ark.rpt_date <= NOW()::DATE
		ORDER BY ark.rpt_date DESC */

		WITH mygroup AS (
			SELECT grp.id
			FROM grp.grp_complete_less_stops grp
			WHERE grp.upline= ?
		),
		mydates AS (
			SELECT m.rpt_date
			FROM $TABLES{'activity_rpt_data'} m
			JOIN mygroup grp ON grp.id=m.id
--			WHERE m.rpt_date <= NOW()::DATE
			
			UNION
			
			SELECT m.rpt_date
			FROM mygroup
			JOIN network.my_upline_partner mup ON mup.upline=mygroup.id
			JOIN $TABLES{'activity_rpt_data'} m ON m.id=mup.id
--			WHERE m.rpt_date <= NOW()::DATE
			
			UNION
			
			SELECT m.rpt_date
			FROM my_upline_vip mup
			JOIN members ON members.id=mup.id 
			JOIN $TABLES{'activity_rpt_data'} m ON m.id=mup.id
			WHERE mup.upline= ?
			AND members.membertype <> 'v'
		)
		SELECT DISTINCT rpt_date FROM mydates WHERE rpt_date <= NOW()::DATE ORDER BY rpt_date DESC");

	$sth->execute($id, $id);
	while (my $tmp = $sth->fetchrow_array)
	{
		push (@list, $tmp);
	}

	$sth->finish;

	###### those with no data to report on will have no dates (empty combos)
	unless (@list)
	{
		$NO_DATA_FLAG = 1;
		
		# that flag will tell them there is no activity and so it is highly unlikely that they will order a report
		# but in order to give 'em credit for the purpose of 30 day cancel refund, we will create the activity
		# otherwise if they have data, they have to pull an actual report
		TrackUsage();
	}

	# as of Mar. '09, Dick wants the drop-downs to initialize at yesterday
	my ($yesterday) = $db->selectrow_array('SELECT NOW()::DATE -1');

	# 12/12 Dick wants to show them their last visit so that they can set the start date... so we will default to that if we know it
	my $startDef = $cookies{'activity_rpt_last_visit'} || $yesterday;
	
	return $q->popup_menu(
		'-name'=>'rpt_date_start',
		'-values'=>\@list,
		'-class'=>'ctl',
		'-default'=>$startDef
		),

		$q->popup_menu(
		'-name'=>'rpt_date_end',
		'-values'=>\@list,
		'-class'=>'ctl',
		'-default'=>$startDef
		);

}

sub cboJumpTo
{
	my @vals = '';
	my %keys = (''=>'');

	# get every Partner on my team for whom I am the Coach
	my $sth = $db->prepare("
		SELECT m.id, m.alias, (m.firstname ||' '|| m.lastname) AS name
		FROM members m
		JOIN relationships a
			ON a.id=m.id
		WHERE upline= ?
		AND coach=TRUE
		ORDER BY m.id", undef, );
	$sth->execute($meminfo->{'id'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$keys{$tmp->{'id'}} = "$tmp->{'alias'} - $tmp->{'name'}",
		push @vals, $tmp->{'id'};
	}
	
	return '' if (scalar @vals) == 1;

	return $q->popup_menu(
		'-name'=>'_rid',
		'-values'=>\@vals,
		'-class'=>'ctl',
		'-labels'=>\%keys,
		'-default'=>'',
		'-force'=>1
		);
}

sub cboRptType
{
	my $id = shift;
#	my $rid = Validate_ID() || $meminfo;	# we never present the menu page as a menu page for the jump to ID

	my @vals = ('personal', 'prps');	# what every Partner gets
	push @vals, ('coach','group') if $meminfo->{'iscoach'};
	push @vals, 'entire' if $meminfo->{'a_level'} >= 11;	# Exec+ are entitled to an entire team report

	my %labels = (
		'personal'=> $lang_blocks->{'pers'},
		'prps'=>'PRPs',
		'coach'=> $lang_blocks->{'coach'},
		'entire'=> $lang_blocks->{'team'},
		'group'=> $lang_blocks->{'group'}
	);

	return $q->popup_menu(
		'-name'=>'rptype',
		'-values'=>\@vals,
		'-class'=>'ctl',
		'-labels'=>\%labels,
		'-default'=>'personal'
	);
}
sub cboSort
{
	###### create a combo box for sort order
	return $q->popup_menu(
		'-name'=>'sort',
		'-values'=>[1,2,3],
		'-class'=>'ctl',
		'-labels'=>{1=> $lang_blocks->{'member_id'}, 2=> $lang_blocks->{'activity_type'}, 3=> $lang_blocks->{'date'}},
		'-default'=>($q->cookie('activity_rpt_sort') || '')
		);
}

# a replacement for CGI.pm's since there seems to be no way to have it omit the label and so no way to control the alignment of the label
sub checkbox
{
	my $args = shift;
	return qq|<label><input type="checkbox" class="$args->{'-class'}" name="activities" value="$args->{'-value'}" />| .
			$q->span($args->{'-label'}) .
			'</label>';
}

sub Create_Cookie
{
	###### if we have option parameters, we'll create a new cookie
	###### otherwise we won't mess with whatever may already exist
	my $cookie = '';

	foreach (sort {$a cmp $b} keys %params)
	{
		if ($_ eq 'sort')
		{
			push (@ck,
				$q->cookie(
				'-name'=>'activity_rpt_sort',
				'-expires'=>'+1M',
				'-value'=>$params{sort}
			));
		}

		next if /\D/;
		$cookie .= $_ .",";
	}

	if ($cookie)
	{
		chop $cookie;
		push (@ck,
			$q->cookie(
			'-name'=>'activity_rpt',
			'-expires'=>'+1M',
			'-value'=>$cookie
		));
	}
}

sub createEmailTemplatesRelationships
{
	my $data = shift;

	my %rv = ();
	$rv{'sponsor'} = ($data->{'my_upline_vip'} == $rid->{'id'}) ? 1:0;
	$rv{'coach'} = ($data->{'my_coach'} == $rid->{'id'}) ? 1:0;
	$rv{'exec'} = ($data->{'my_exec'} == $rid->{'id'}) ? 1:0;
	return \%rv;
}

# take the elements for a given "activity" and create a list of the val_text fields
sub CreateList
{
	my $ar = shift;
	my $sep = shift || '<br />';
	my $rv = '';
	return unless $ar;
	foreach (@{$ar})
	{
		# if we already have some text, then append our separator before adding the next element
		$rv .= $sep if $rv;
		$rv .= $_->{'val_text'};
	}
	return $rv;
}

sub CSV_List_Item_1
{
	my $l = shift;
	return "$l->{'alias'},$l->{'firstname'},$l->{'lastname'},$l->{'emailaddress'},$l->{'country'},$l->{'language_pref'}\n";
}

sub CSV_List_Item_3
{
	my $l = shift;
	return "$l->{'alias'},$l->{'rpt_date'},$l->{'firstname'},$l->{'lastname'},$l->{'val_text'}\n";
}

sub Do_Report
{
	my $tbl = '';
	
	$tbl = Get_Report_Data();
	unless ( $tbl )
	{
		$tbl = qq|<div style="color: #cc0000">$lang_blocks->{'noactivity'}</div>\n|;
	}
#print STDERR Dumper(\%options);
#print STDERR Dumper(\%CSVlists);
#print STDERR Dumper(\%atypes_present);
	my $tmpl = $gs->GetTemplate($TMPL{'report'}) ||	die "Failed to load template: $TMPL{'report'}";
	print $q->header('-cookie'=>\@ck, '-expires'=>'now', '-charset'=>'utf-8');
 	
	my $rv = $TT->process(\$tmpl, {
		'atypes_present'	=> \%atypes_present,
		'options'	=> \%options,
		'lb'		=> $lang_blocks,
		'meminfo'	=> $meminfo,
		'rid'		=> $rid,
		'tbl'		=> $tbl,
		'action'	=> $script_name,
		'timestamp'	=> scalar localtime(),
		'legend'	=> Legend(),
		'batch_time'		=> ($gs->GetObject($TMPL{'batch_time'}) || '<i>Error</i>'),
		'rpt_date_start'	=> $params{'rpt_date_start'},
		'rpt_date_end'		=> $params{'rpt_date_end'},
		'page_language'		=> $page_language,
		'CSVlists'	=>\%CSVlists
	});

	print $TT->error() if ! $rv;
}		

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GenerateQuery
{
	my $list = '';
	my $XTBL = '';
	my $EXTRA_COLS = '';
	my $qry_modifier_1 = '';
	my $qry_modifier_2 = '';
	my $ARDfunc = $TABLES{'ard_personal'};	# the default per Dick

	###### if we only want followup items that have not yet been followed up on
	if ($q->param('funeeded') || $q->param('pfuneeded'))
	{
		# if they have selected the followup box, we will respect the date ranges selected
		$qry_modifier_1 .= "AND at.followup=TRUE AND mfk.ard_pk IS NULL\n";
		$qry_modifier_1 .= "AND rpt_date BETWEEN '$params{'rpt_date_start'}' AND '$params{'rpt_date_end'}'\n"
	}
	else
	{
		# if they have not selected the followup box, they will get the date ranges they selected *AND* they will get all unfollowed up activity available
		$qry_modifier_1 .= "AND ((at.followup=TRUE AND mfk.ard_pk IS NULL) OR rpt_date BETWEEN '$params{'rpt_date_start'}' AND '$params{'rpt_date_end'}')\n";
	}

	if ($params{'rptype'} eq 'entire')
	{
		$ARDfunc = $TABLES{'ard_team'};
	}
	elsif ($params{'rptype'} eq 'coach')
	{
		$ARDfunc = $TABLES{'ard_coach'};
	}
	elsif ($params{'rptype'} eq 'prps')
	{
		$ARDfunc = $TABLES{'ard_prps'};
	}
	elsif ($params{'rptype'} eq 'group')
	{
		$ARDfunc = $TABLES{'ard_group'};
	}
	elsif ($params{'rptype'} eq 'personal')
	{
		$qry_modifier_1 .= "AND mt.ppp_credit=TRUE\n";
	}
	
	# if the viewer is looking at a downline who is at a higher pay level, then the viewer only gets to see stuff according to his pay level,
	# otherwise, the view will reflect the stuff the downline would ordinarily see
	my $paylevel = $meminfo->{'a_level'} < $rid->{'a_level'} ? $meminfo->{'a_level'} : $rid->{'a_level'};
	
	# As of Dec 2015 we are introducing the concept of sub-activities, which will not be selected directly but will be listed as instructions for the triggering activity.
	# So, we need to include those activity types by matching up selected options with matching sub_type_of values
	my $oList = $option_list;
	foreach my $at (split(',', $option_list))
	{
		next unless $options{$at}->{'has_children'};
		foreach my $key (keys %options)
		{
			next unless $options{$key}->{'sub_type_of'};
			$oList .= ",$key" if $options{$key}->{'sub_type_of'} == $at;
		}
	}
#warn $oList;
	my $qry = qq#
		WITH ard AS (
			SELECT *
--			FROM $ARDfunc($rid->{'id'}, ($paylevel)::SMALLINT, NOW()::DATE -31, '$params{'rpt_date_end'}', '{$oList}')
			FROM $ARDfunc($rid->{'id'}, ($paylevel)::SMALLINT, NOW()::DATE -31, NOW()::DATE, '{$oList}')
		)
		SELECT m.alias,
			m.id,
			m.spid AS current_spid,
			m.firstname,
			m.lastname,
			m.membertype,
			CASE WHEN m.membertype= 'ag' THEN m.company_name
			ELSE m.firstname || ' ' || m.lastname
			END AS name,
			COALESCE(m.emailaddress, '') AS emailaddress,
			COALESCE(m.country,'') AS country,
			COALESCE(m.language_pref,'') AS language_pref,
			ard.rpt_date,
			COALESCE(ard.val_text,'') AS val_text,
			ard.activity_type,
			ard.spid,
			CASE
				WHEN mfk.mft_pk IS NOT NULL
				OR at.followup = FALSE
					THEN 'fud'::TEXT
				ELSE 'fun'::TEXT
			END AS followup,
			at.bulk_fu_allowed,
			at.ar_list_type,
			ard.pk,
			ard.id AS ard_id,
			at.sort_order,
			at.sub_type_of,
			at.in_preference_to,
			mc.upline AS my_coach,
			COALESCE(mx.upline,0) AS my_exec,	/* in certain cases, like really new parties, they may not have that record setup yet */
			COALESCE(muv.upline, m.spid) AS my_upline_vip
			
			$EXTRA_COLS

		FROM members m
		JOIN member_types mt
			ON mt."code"=m.membertype
		JOIN ard
			ON m.id=ard.id
		JOIN $TABLES{'activity_types'} at
			ON at.activity_type = ard.activity_type
		LEFT JOIN $TABLES{'member_followup_key'} mfk
			ON mfk.ard_pk = ard.pk
		LEFT JOIN my_coach mc
			ON mc.id=m.id
		LEFT JOIN my_exec mx
			ON mx.id=m.id
		LEFT JOIN my_upline_vip muv
			ON muv.id=m.id
		WHERE TRUE=TRUE	-- that is just so there will always be a WHERE and every other modifier can be AND

		$qry_modifier_1
		
		ORDER BY $params{'ORDER BY'}, ard.pk ASC#;

#print $q->header('text/plain'),$qry; Exit();
	return $qry;
}

sub Get_Option_List
{
	my $lang_pref = $gs->Get_LangPref() || 'en';
	
	###### get all of our options - we'll need all/some of them whatever the case
	my $sth = $db->prepare(qq|
		SELECT
			at.activity_type AS "type",
			att.label,
			at.active,
			at.sort_order,
			at.grouping,
			at.bulk_fu_allowed,
			at.ar_list_type,
			at.sub_type_of,
			at.has_children,
			at.instructions_link,
			at.privilege_level
		FROM $TABLES{'activity_types'} at
		JOIN $TABLES{'activity_types_translated'}(?) att
			ON att.activity_type=at.activity_type
		WHERE at.visible= TRUE
		ORDER BY at.sort_order|);

	$sth->execute($lang_pref);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$options{$tmp->{'type'}} = $tmp;
		push (@param_list, $tmp->{'type'});
		push (@option_sort, $tmp->{'type'});
	}
	$sth->finish;
}

sub Get_Report_Data
{
	###### pull out a subscriber's data from the key table
	my $tbl = '';
	my $bg = 2;
	my @sort = ();
	my $qry = GenerateQuery() || die "Failed to generate a query";
#die $qry;
	my (@key_list, %flat) = ();
	my $pay_level_labels = $db->selectall_hashref("SELECT lvalue, lname FROM level_values", 'lvalue');
	
	my $sth = $db->prepare( $qry );
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		# trying to weave the exclusions into the SQL is too cumbersome
		next if HideThis($tmp);
		
		$gs->Prepare_UTF8($tmp, 'encode');
		
=head4
	Since we are/may be reporting many activities for each individual on one line for a given date
	we have to flatten many rows of data returned (1 for each activity recorded) into one for display
	We do this by creating a data structure like this
	%flat = (
		"id_number,date string" => {
			data => (one row of data which will contain the general personal info),
			activities => {
				1 => [data-row-hashref, data-row-hashref],
				2 => [data-row-hashref, data-row-hashref],
				.... for the rest of the activities we have pulled in on this query
			},
			followup => ('fud' or 'fun'),
			in_preference_to => {in_preference_to activity type/s}
		}
	)
	
	So this gives us columns for each party on a given date.
	The presence of a key under the activities key indicates a recorded activity.
=cut

		my $flatKey = "$tmp->{'id'}, $tmp->{'rpt_date'}";
		
		###### check to see if we have a key to match this ID & date
		###### if not, place it on the list for use in generating the report later
		unless ( exists $flat{$flatKey} )
		{
			push (@key_list, $flatKey);
		}
		
		# The whole in_preference_to requirement is somewhat of a gory hack, but if we have such a value in our record, we will set the matching key in our 'in_preference_to' hash
		# If the activity type we are on at the moment matches one of these keys, then we will abort further processing.
		# With proper configurations setup, we should never end up with a $flat{} key that is empty
		$flat{ $flatKey }->{'in_preference_to'}->{$tmp->{'in_preference_to'}} = 1 if $tmp->{'in_preference_to'};
		next if exists $flat{ $flatKey }->{'in_preference_to'}->{$tmp->{'activity_type'}};

		###### save our complete record under this key
		$flat{ $flatKey }->{'data'} = $tmp;
		
		###### create/replace the activity key for this one
		###### also if we have some text, we'll concatenate to whatever may already exist
		# this deals with items where a like activity actually is triggered for similar but not identical events
		# such as started Trainee 1 and then started Trainee 2
		# With the advent of sub-activities that are sometimes triggered for a date subsequent to the triggering activity, we need to create a key for the parent as it may not exist
		if ($tmp->{'sub_type_of'})
		{
			push @{ $flat{ $flatKey }->{'activities'}->{ $tmp->{'sub_type_of'} } }, $tmp;
		}
		else
		{
			push @{ $flat{ $flatKey }->{'activities'}->{ $tmp->{'activity_type'} } }, $tmp;
		}
		

		# we also have to deal with activities that may require followup vs. those that do not
		# for any given ID on a given date
		$flat{$flatKey}->{'followup'} ||= $tmp->{'followup'};
		
		# after the initial set, it may need to be reset if there is an activity that needs followup
		$flat{$flatKey}->{'followup'} = 'fun' if $tmp->{'followup'} eq 'fun';
	}
	$sth->finish;
	
	if (@key_list)
	{
		$tbl = ReportHeaders() . '<tbody>';
	}
#print STDERR Dumper(\%flat);
	foreach my $tmp (@key_list)	# in other words, each row of the report... the ID/date for the row
	{
		my $rowID = $tmp;
		$rowID =~ s/,\s*/-/g;
		
		$tbl .= qq|<tr class="$bgcolor{$bg}" id="tr_$rowID">\n|;

		###### do our ID column
		$flat{$tmp}->{'data'}->{'alias_converted'} =
			($id == $flat{$tmp}->{'data'}->{'id'} || HideThis($flat{$tmp}->{'data'})) ?
				$flat{$tmp}->{'data'}->{'alias'} :
				$q->a({
					'-href'=>"$MFUR?rid=$flat{$tmp}->{'data'}->{'id'}",
					'-class'=>"$flat{$tmp}->{'followup'} mfurAnchor"
				},
				$flat{$tmp}->{'data'}->{'alias'}
		);

		$tbl .= $q->td({'-class'=>'data alias'}, $flat{$tmp}->{'data'}->{'alias_converted'}) . "\n";

	# Create our controls.
	
	# we could conceivably have one or more activities that allow bulk followup
	# and these may not be in the main $flat{$tmp} value set
	# so we have to loop through the activities themselves to generate one or more checkboxes
		my @boxes = ();
		my (%InColBoxes, %InColFu) = ();
		foreach my $at (sort {$a <=> $b} keys %{ $flat{$tmp}->{'activities'} })
		{
			# this hash will tell our template what activity types are really present in the report as far as followup form items go
#			$atypes_present{$at} = $options{$at}->{'sort_order'};
#print STDERR "at: $at, key $tmp, [0]: " . Dumper($flat{$tmp}->{'activities'}->{$at}->[0]);
#			if ($flat{$tmp}->{'activities'}->{$at}->[0]->{'ar_list_type'} && $flat{$tmp}->{'activities'}->{$at}->[0]->{'followup'} eq 'fun')
#			{
#				#call the function by name reference
#				$CSVlists{$at} .= &{\&{'CSV_List_Item_' . $flat{$tmp}->{'activities'}->{$at}->[0]->{'ar_list_type'}}}( $flat{$tmp}->{'activities'}->{$at}->[0] );
#				$options{$at}->{'x_showfrm'} = 1;
#			}
			
			foreach my $row (sort {$a->{'pk'} <=> $b->{'pk'}} @{$flat{$tmp}->{'activities'}->{$at}} )
			{
#print STDERR Dumper($row);
				if ($row->{'bulk_fu_allowed'} && $row->{'followup'} eq 'fun')
				{
					$CSVlists{$row->{'activity_type'}} .= &{\&{'CSV_List_Item_' . $row->{'ar_list_type'}}}( $row );
					$options{$row->{'activity_type'}}->{'x_showfrm'} = 1;
					
					# this hash will tell our template what activity types are really present in the report as far as followup form items go
					$atypes_present{$row->{'activity_type'}} = $options{$row->{'activity_type'}}->{'sort_order'};
					
					if ($row->{'sub_type_of'})	# is this a sub-activity? If so we want a followup checkbox within the column for the parent activity
					{
						push(@{$InColBoxes{$row->{'sub_type_of'}}}, $row);
					}
					else
					{
						push(@{$InColBoxes{$row->{'activity_type'}}}, $row);
					}
					
#					$options{$at}->{'x_show_fu_btn'} = 1;
					$options{$row->{'activity_type'}}->{'x_show_fu_btn'} = 1;
				}
				elsif ($row->{'sub_type_of'})	# for other sub-activities, we need to create inline entries in the parent column
				{
					push(@{$InColFu{$row->{'sub_type_of'}}}, $row);
				}
			}
		}

		my $ckbx = '';

# this was when we put all the checkboxes in a column adjacent to the ID	
#		$tbl .= $q->td({'-class'=>'checkbox'}, $ckbx);

		$tbl .= $q->td({'-class'=>'data'}, $flat{$tmp}->{'data'}->{'rpt_date'}) . "\n";
		
		$flat{$tmp}->{'data'}->{'name'} = $q->a( {'-href'=>"mailto:$flat{$tmp}->{'data'}->{'emailaddress'}"}, $flat{$tmp}->{'data'}->{'name'});
			
		$tbl .= $q->td({'-class'=>'data'}, $flat{$tmp}->{'data'}->{'name'}) . "\n";

		###### do our variable rows
		foreach my $x ( TrickySort() )	# $x = the activity type we are working with... the column in the row
		{
			if ($flat{$tmp}->{'activities'}->{$x})
			{
				my $lbl = "$options{$x}->{'label'}.";
				my $emicon = Bullet({
						'type'=>'g',
						'alt'=>$lbl,
						'title'=>$lbl
				});
				
				###### in most cases, the bullet will be fine, but........
				my $content = '';
				if ($InColBoxes{$x})
				{
					my $boxnum = scalar @{$InColBoxes{$x}};
					for (my $y=0; $y < $boxnum; $y++)
					{
						# if the checkbox is for a sub-activity, we will label it accordingly,
						# otherwise, we will know what it is by the column ID
						my ($ckbxlbl, $title) = ();
						my $cssClass = "ckbx_$InColBoxes{$x}->[$y]->{'activity_type'}";
						if ($InColBoxes{$x}->[$y]->{'sub_type_of'})
						{
							$ckbxlbl = $q->escapeHTML($options{$InColBoxes{$x}->[$y]->{'activity_type'}}->{'label'});
						}
						else
						{
							$title = $q->escapeHTML($options{$InColBoxes{$x}->[$y]->{'activity_type'}}->{'label'});
							$cssClass .= ' masterTooltip';
							$ckbxlbl = '';
						}

							
#						$content .= checkbox({
#								'-class' => "ckbx_$InColBoxes{$x}->[$y]->{'activity_type'}",
#								'-name' => 'activities',
#								'-value' => $mf->EncodeActivity( $InColBoxes{$x}->[$y]->{'pk'} ),
#								'-label' => $ckbxlbl,
#								'-data-pk' => $InColBoxes{$x}->[$y]->{'pk'}
#						});
						$content .= qq|<label><input type="checkbox" name="activities" value="|
							. $mf->EncodeActivity( $InColBoxes{$x}->[$y]->{'pk'} ) .
							qq|" class="$cssClass" data-pk="$InColBoxes{$x}->[$y]->{'pk'}"|
							. ($title ? qq| title="$title"| : '') .
							qq| /><span>$ckbxlbl</span></label>|;
						$content .= $q->br unless $y == ($boxnum -1);	# the last element
					}
				}
				
				if ($InColFu{$x})
				{
					$content .= '<ul class="InColFu">';
					my $boxnum = scalar @{$InColFu{$x}};
					for (my $y=0; $y < $boxnum; $y++)
					{
						# if the checkbox is for a sub-activity, we will label it accordingly,
						# otherwise, we will know what it is by the column ID
						my $ckbxlbl = $options{$InColFu{$x}->[$y]->{'activity_type'}}->{'label'};
						
						$content .= $q->li({'-class'=>$InColFu{$x}->[$y]->{'followup'}}, $ckbxlbl);
					}
					$content .= '</ul>';
				}
				
				if ($content)
				{
					$tbl .= $q->td({'-class'=>'data'}, $content);
				}
				elsif ($x == 22 || $x == 3 || $x == 70)
				{
					$tbl .= $q->td({'-class'=>'data'}, CreateList($flat{$tmp}->{'activities'}->{ $x }) . $emicon);
				}
				# New Partner
				elsif ($x == 12)
				{
					$tbl .= $q->td({'class'=>'data'}, ARInstructions($x, $flat{$tmp}->{'data'}->{'id'}, $lbl) . $emicon);
				}
				###### new pay level achieved... we need to show the PL abbreviation
				elsif ($x == 23)
				{
					$tbl .= $q->td({'-class'=>'data'}, $pay_level_labels->{ $flat{$tmp}->{'activities'}->{$x}->[0]->{'val_text'} }->{'lname'} . $emicon);
				}
				###### GPS upgrade
				elsif ($x == 61)
				{
					$tbl .= $q->td({'-class'=>'data'}, $flat{$tmp}->{'activities'}->{$x}->[0]->{'val_text'} . $emicon);
				}
				###### reminders that haven't been followed up on
				elsif ($x == 33 && $flat{$tmp}->{'data'}->{'followup'} eq 'fun')
				{
					$tbl .= $q->td({'-class'=>'type33'}, CreateList($flat{$tmp}->{'activities'}->{ $x }));
				}
				else
				{
					my $hr = ();

					if (grep {$_ == $x} (21,4,5,6))
					{
						$hr = Bullet({
							'type'=>'o',
							'alt'=>$lbl,
							'title'=>$lbl,
#							'onclick'=>"openTransRpt($flat{$tmp}{'data'}{'id'})"
							'data-id' => $flat{$tmp}->{'data'}->{'id'},
							'anchor' => 1,
							'class' => 'openTransRpt'
						});
					}
					
					$hr ||= $emicon ? $emicon : Bullet({
						'type'=>'g',
						'alt'=>$lbl,
						'title'=>$lbl});

					$tbl .= $q->td({'-class'=>'hasBullet'}, $hr);
				}
			}
			else
			{
				$tbl .= '<td></td>';
			}
		}

		$tbl .= "</tr></tbody>\n";

		$bg = ($bg == 2) ? 1 : 2;
	}

	$tbl = $q->table({'-class'=>'reportTbl'}, $tbl) if $tbl;
	return $tbl;
}		

sub GetReportee
{
	return uc $gs->PreProcess_Input($q->param('rid') || $q->param('_rid'));
}

# we don't want to show activities having to do with removed members EXCEPT their removal activity
# the same goes for cancellations EXCEPT for their cancellation finalization
#... you get the idea
# trying to weave this stuff into the query is too cumbersome
# As of January, we need to also filter out stuff that should only be seen by direct sponsors, coaches, Execs
sub HideThis
{
	my $i = shift;
	# we do not want to show followup reminders on a particular VIP to that VIP ;)
	return 1 if $i->{'ard_id'} == $id && $i->{'activity_type'} == 33;
	
	# create the bitmask of the reportee as it relates to the relationship with this party
	# 0001 = sponsor, 0010 = coach, 0100 = Exec
	my $mask = 0;
	$mask += 1 if $rid->{'id'} == $i->{'my_upline_vip'};
	$mask += 2 if $rid->{'id'} == $i->{'my_coach'};
	$mask += 4 if $rid->{'id'} == $i->{'my_exec'};
#print STDERR "mask: $mask - at $i->{'activity_type'} - pl $options{$i->{'activity_type'}}->{'privilege_level'}\n";
	# if we have no privilege level set, then we are done... access is for everybody
	# otherwise see if our mask intersects with the privilege level
	# 0001 = sponsor, 0010 = coach, 0100 = exec and the combinations of course
	if ($options{$i->{'activity_type'}}->{'privilege_level'} && !($options{$i->{'activity_type'}}->{'privilege_level'} & $mask))
	{
		return 1;
	}
#print STDERR "mask did not disallow\n";
	return 0 if ! $HideThese{$i->{'membertype'}};
	return 0 if $HideThese{$i->{'membertype'}} && grep $i->{'activity_type'} eq $_, @{$HideThese{$i->{'membertype'}}};
	
	return 1;
}

sub Legend
{
	###### create a legend of option types and their descriptions
	###### this will make the report compact
	my $leg = '';

	foreach (TrickySort())
	{
#		$leg .= $q->li("$options{$_}{sort_order} - $options{$_}{label}") . "\n";
		$leg .= $q->Tr(
			$q->td({'-class'=>"col1"}, $options{$_}->{'sort_order'}) .
			$q->td(" - $options{$_}->{'label'}") #.
		#	$q->td({'-class'=>'furoles'}, ($options{$_}{'furoles'} || ''))
		);
	}

	return $leg;
}

sub Load_Cookies
{
	###### get all of our options from the cookie if they have it
	###### cookie takes the form of a digit foreach option selected
	###### separated by commas
	my $cookie = $q->cookie('activity_rpt');

	if ($cookie)
	{
		###### sanity checks - there should only be digits and commas
		###### normally we wouldn't have to be this anal, but since the values
		###### may be used directly in a SQL statement we can't be too cautious
		if ( length $cookie > 150 )
		{
			push (@ck, $q->cookie(
					'-name'=>'activity_rpt',
					'-expires'=>'now',
					'-value'=>''
			));
			return;
		}
	
		###### get rid of any unwanted characters
		$cookie =~ tr/[0-9]|,//dc;
	
		foreach ( split( /,/, $cookie ))
		{
			$cookies{$_} = 1;
		}
	}

	$cookie = $q->cookie('activity_rpt_last_visit');
	
	if ($cookie)
	{
		$cookies{'activity_rpt_last_visit'} = $cookie;
	}
	else
	{
		if (($cookie) = $db->selectrow_array("
			SELECT rpt_date
			FROM $TABLES{'activity_rpt_data'}
			WHERE activity_type =24
			AND id= ?
			ORDER BY rpt_date DESC LIMIT 1", undef, $id))
		{
			$cookies{'activity_rpt_last_visit'} = $cookie;
			push (@ck,
				$q->cookie(
				'-name'=>'activity_rpt_last_visit',
				'-value'=>$cookie,
				'-path'=>'/cgi',
				'-expires'=>'+12h'
			));
		}
		else
		{
			$cookies{'activity_rpt_last_visit'} = '';
		}
	}
}

sub LoadLangBlocks
{
	($lang_blocks, $page_language) = $gs->GetObject($TMPL{'xml'});
	die "Failed to retrieve the XML template\n" unless $lang_blocks;

	my $inserts = $gs->GetObject($TMPL{'richtext'}) || die "Failed to retrieve the richtext template\n";
	$lang_blocks = XML::LibXML::Simple::XMLin($lang_blocks, 'NoAttr'=>1);

	$inserts = {'inserts' => decode_json($inserts)};
	
	# we have to doctor the strings -after- extracting them from the XML or the whole purpose of keeping HTML out of the XML goes bye bye
	foreach my $k (keys %{$lang_blocks})
	{
		# need a new $tmp each time otherwise TT just keeps adding to it
		my $tmp = ();
		$TT->process(\$lang_blocks->{$k}, $inserts, \$tmp);
#		$lang_blocks->{$k} = encode_utf8($tmp);
		$lang_blocks->{$k} = $tmp;
	}

}

sub Load_Params
{
	###### put all of our parameters in the param hash
	foreach my $p ($q->param)
	{
	###### this keeps us from loading params we aren't expecting
		next unless grep { /$p/ } @param_list;

	###### we'll initialize our numbered params with a zero rather than an empty
		if ( grep $_ eq $p, (qw/rpt_date_start rpt_date_end sort subscription-consent confirm rptype/) )
		{
			$params{$p} = $q->param($p) || '';
		}
		else
		{
			$params{$p} = $q->param($p) || 0;
			$option_list .= $p .',';
		}
	}
	
	chop $option_list;
}

sub ReportHeaders
{
	my $hd = qq|<thead><tr class="$bgcolor{1}">\n|;

	###### our fixed headers
#	$hd .= $q->th({'-colspan'=>2}, 'ID') . "\n";
	$hd .= $q->th('ID') . "\n";
	$hd .= $q->th('Date') . "\n";
	$hd .= $q->th('Name') . "\n";
	
	###### our variable headers
	foreach (TrickySort())
	{
		$hd .= $q->th({'-class'=>'acttype masterTooltip', '-title'=>$options{$_}->{'label'}}, $options{$_}->{'sort_order'}) . "\n";
	}

	return "$hd </tr></thead>\n";
}

sub Select_Options
{
	###### present the page that gives them the options available for reporting
	my $tmpl = $gs->GetTemplate($TMPL{'menu'}) || die "Failed to load template: $TMPL{'menu'}";

	my $list = '';
	my $bg = 2;
	my @Date_cbo = cboDateSelector();

	foreach (@option_sort)
	{
		if ($NO_DATA_FLAG)
		{
			$list = "<tr><td style=\"color: #990000\">$lang_blocks->{'noactivity'}</td></tr>\n";
			last;
		}

		my $tmp = $options{$_};
		my $control = ();
		
		###### if the type is not active and they don't have a cookie indicating
		###### they've used it before, then we won't even display it
		unless ( $tmp->{'active'} || $cookies{$_} )
		{
			next;
		}
		elsif ($tmp->{'sub_type_of'})
		{
			next;	# we are not putting these on the selection menu as the parent is all that is needed
		}
		elsif ( $tmp->{'active'} )
		{
			$control = $q->checkbox(
				'-name'=>$tmp->{'type'},
				'-value'=>1,
				'-checked'=>($cookies{$tmp->{'type'}} || 0),
				'-label'=>''
			);
		}
# column abandoned Feb 2016
#		$control ||= $tmp->{'inactive_msg'};

		$list .= $q->Tr({'-class'=>$bgcolor{$bg}},
			$q->td({'-class'=>$options{$_}->{'grouping'}}, $tmp->{'label'}) . "\n" .
		#	$q->td({'-class'=>'furoles'}, $tmp->{'furoles'}) .
			$q->td({'-class'=>'ctl'}, $control) . "\n"
		) . "\n";

		$bg = ($bg == 1) ? 2 : 1;
	}


	$_exclusion_list =~ s#,#\n#g;
#	$rid = GetReportee();
#warn "rid id: $rid->{'id'}";
	$conditionals{1} = $rid->{'id'};
	
	print $q->header('-cookie'=>\@ck, '-expires'=>'now', '-charset'=>'utf-8');
	my $rv = $TT->process(\$tmpl,
		{
			'conditionals'			=>\%conditionals,
			'exclusion_list'		=> $_exclusion_list,
			'meminfo'				=> $meminfo,
			'list'					=> $list,
			'action'				=> $script_name,
			'rpt_date_start'		=> $Date_cbo[0],
			'rpt_date_end'			=> $Date_cbo[1],
			'sort'					=> cboSort(),
			'timestamp'				=> scalar localtime(),
			'rid'					=> $rid,
			'cboJumpTo'				=> cboJumpTo(),
			'activity_rpt_last_visit'	=> $cookies{'activity_rpt_last_visit'},
			'cboRptType'			=> cboRptType($rid->{'id'}),
			'lb'					=> $lang_blocks,
			'page_language'			=> $page_language,
		});
	print $TT->error() if ! $rv;
	Exit();
}

sub TrackUsage
{
	return if $id == 1;
	state $fl = 0;
	return if $fl;	# once we come through here once, we don't need to do it again
	
	$db->do("INSERT INTO activity_tracking (id, class) VALUES ($id, 24)");
	$fl++;
	return;
}		
	
sub TrickySort
{
	###### since our option 'types' are not grouped by activity type
	###### we have assigned a sort order to the options
	###### in order to make the display friendly we have to use these values
	###### as labels rather than the real type value
	###### In other words, we may show a 23 as the item number, but that really represents the sorting number as opposed to the real activity_type primary key value
	my @tmp = ();
	foreach my $i (@option_sort)
	{
		push (@tmp, $i) if grep {/(^|,)$i(,|$)/} (eval ($option_list));
	}
	return @tmp;
}				
		
sub Validate_Params
{
	###### make sure we have sane appearances of dates and other items
	###### which will be incorporated into SQL

	###### rather than reporting 'errors', we'll just default things that are bad

	foreach ('rpt_date_start', 'rpt_date_end')
	{
		unless ( $params{$_} && $params{$_} =~ /\d{4}\-\d{2}\-\d{2}/ )
		{
			return;
		}
	}

	$params{'ORDER BY'} = '';

	###### pre-defined sort orders for the reports of subscribers
	my %OrderBy = (
		1 => 'm.id',
		2 => 'ard.activity_type',
		3 => 'ard.rpt_date'
	);

	foreach (keys %OrderBy)
	{
		$params{'ORDER BY'} = $OrderBy{$_} if $params{'sort'} eq $_;
	}

	unless ($params{'ORDER BY'})
	{
		return ();
	}

	return 1;
}	

sub Validate_ID
{
	###### make sure the ID is valid, getting some data on them in the process
	my $rid = shift || '';
	return unless $rid;

	if (length $rid > 9)
	{
		Err("The submitted ID, $rid, is invalid.");
		return 0;
	}

	my $col = ($rid =~ /\D/) ? 'm.alias' : 'm.id';

	my $sth = $db->prepare("
		SELECT m.id, m.alias, m.firstname, m.lastname, COALESCE(a.a_level,0) AS a_level
		FROM members m
		LEFT JOIN a_level_stable a
			ON a.id=m.id
		WHERE $col = ?");

	$sth->execute($rid);
	my $_rid = $sth->fetchrow_hashref;
	$sth->finish;
	unless ($_rid)
	{
		Err("The membership for $rid cannot be found.");
		return 0;
	}

#	unless (
#		$db->selectrow_array("SELECT network.access_ck1($_rid->{'id'}, $meminfo->{'id'})") ||
#		$db->selectrow_array("SELECT network.access_hack1($_rid->{'id'}, $meminfo->{'id'})")	# note one is access_ck1 the other is access_hack1
#	)
	unless ($db->selectrow_array("SELECT network.access_ck1($_rid->{'id'}, $meminfo->{'id'})"))
	{
		Err($lang_blocks->{'noaxs4u'});
		return 0;
	}

	$gs->Prepare_UTF8($_rid, 'encode');
	return $_rid;
}

__END__

01/21/04 added the activity tracking routine
01/26/04 added "use diagnostics -verbose" to try and track down an unit'd var error in 'eval(32)'
02/17/04 added special handling for activities 25 & 26
03/19/04 fixed a running on line situation in activity 25/26 caused by repeated label concatenation
05/06/04 fixed the grep in TrickySort to only match exact items
05/25/04 added handling for type 28, personal line report accesses
05/27/04 changed the display for type 28 to the time_left_in_queue
05/28/04 altered the hyperlink on the member aliases and also added handling for user ID and 'role' placeholders in the template
06/07/04 corrected a problematic javascript:void()
06/11/04 expanded the special labels for certain activities to be unique for
	each activity instead of each row - this was accomplished by changing the flat
	{activities} value to a hash with the necessary data for the labels
	instead of a list with the label data in the {data} hash
10/21/04 added an extra SQL query modifier in Get Option List to pull on visibles
11/15/04 tweaked the usage tracking routine SQL to look for a record by data
01/31/05 dealt with unexpected 'actions' in the NonSubscriber routine
08/18/05 went to classes on the table rows instead of bgcolor
08/25/05 added the insertion of the rid parameter into the Jumpto control on a call to the selection page
10/10/05 added pulling of the role so that some items could be filtered
01/26/06 added utf-8 as the charset in header calls
03/10/06 revised the SQL in the list generator to utilize the reportee's exclusion list regardless of the user
09/29/06 heavy modification to the main query for optimization
08/01/07 added the insertion of a Club Account transaction on subscription
03/18/09 tweaked some of the report HTML and added the building of a CSV list for certain report types
04/22/09 added the exclusion of members from reports that should no longer be in there, like r,c,t (with exceptions)
07/09/09 added the ability to deliver a report that only includes unfollowed up activities for which I am personally responsible
08/14/09 tweaked to skip displaying follow-up reminders on a particular VIP to the VIP themselves (so they don't see their upline's comments)
04/06/11 revised the method of creating the bulk followup checkboxes to interate through the activity list instead of simply evaluating the first row on the main key
06/13/11 a small SQL tweak to the main query to NOT show as followup required
	activities for which a followup_party has not been set even if the activity is marked as followup required
	this addresses scenarios where those with a role recorded in the system make it so
	the system does not set a followup party as they are at or above the required role themselves
06/14/11 it turns out that the "Followup Needed" activity falls into the scenario we filtered above to *not show*
	added an additional field to the activity_types table and added handling in the SQL to show activities flagged even if there is no followup_party
	this is necessary since the presence of a followup_party value mandates that the activity be followed up on
	this means shooting oneself in the foot if the item is not subsequently followed up on
	there are many cases where followup is no longer needed, think an AP has removed themselves
12/20/11 made it so followup required activities are flagged in the abscence of a followup party as there are no more followup parties set
03/08/12 added the concept of one's entire non-Exec team to the "Team Captain" and "personal PP" report types
08/27/12 made it so that the new pay level shows instead of just the bullet
10/06/12 added special display handling for the new GPS upgrade activity (61)
12/07/12 added a bypass for 01 to view a downline Exec's report
02/13 heavy revision to not make use of the activity_rpt_keys any more
02/19/13 rolled out with changes to the access checks as well as moving back over to the clubshop domain
04/16/13 replaced the NULL argument to the ard_XXX function for the a_level with a real value... it is very important for the director/team report
07/30/13 released with the new icon for coaching/followup information
08/16/13 made the report translatable
08/22/13 back into production with the production templates
09/18/13 revised the SQL in cboDateSelector to reflect the way we are doing things now... not with activity_rpt_keys
12/20/13 added handling for creating CSV lists for activities 51,60,62
04/15/14 added handling for EmailTemplates::Links
11/19/14 added $csvlist_type39
01/06/15	removed the EmailTemplates system

02/20/15
	Upgraded to cs_admin for staff authentication
	Moved the main report template out into the file system using GetTemplate()
	Commented out Activate_Subscription
	Also commented out the unsubscribe action block
04/10/15
	Changed network.relationships in cboJumpTo() to simply relationships as the former is going away
04/23/15
	Made it so that pulling up the report interface gives credit for the activity *IF* there is no data to report on
05/12/15
	Rolled out with changes causing *all* un-followed up activity to be reported, regardless of the date range selected.
	Also instituted some utf-8 changes to compensate for the recently upgraded DBD::Pg

02/11/16
	Major changes to this script, the report template, CSS and javascript, making reverting to the previous version impossible without changing all related components
	