#!/usr/bin/perl -w
###### activity_report_instructions.cgi
###### accept certain parameters received from an AJAX call in an activity report
###### return some text to display

use strict;
use CGI;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;

my $cgi = new CGI;

my $acttype = $cgi->param('acttype') || Err('No activity type received');
my $rid = $cgi->param('rid') || Err('No ID parameter received');
eval( "use ActivityReport::ar$acttype;" );
die $@ if $@;

# 07/31/13 Dick authorized the use of a parameter for the viewer ID instead of relying on the login cookie ID
my $vid = $cgi->param('vid');

my ($db, $id, $meminfo) = ();
my $gs = G_S->new({'db'=>\$db});

LoginCK() || Err('No login detected');

$db = DB_Connect('activity_report_instructions.cgi');

my $ar = "ActivityReport::ar$acttype"->new({'db'=>$db, 'cgi'=>$cgi, 'G_S'=>$gs});
my $lang_pref = $gs->Get_LangPref;	# that method uses wantarray to determine whether or not to return a scalar or an arrayref... so it cannot be used directly in the method call below
my ($rtype, $rv) = $ar->ReturnText($rid, ($vid || $id), $lang_pref);
print $cgi->header($rtype), $$rv;	# yes the actual return value is a reference that needs to be dereferenced
Exit();

###### ######

sub Err
{
	print $cgi->header('text/plain'), $_[0];
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub LoginCK
{
	( $id, $meminfo ) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic') );
	
###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	return 1 if ( $id && $id !~ /\D/ );
	return ();
}