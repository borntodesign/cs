#!/usr/bin/perl -w
# clubcash_redemption.cgi
# Allow Redemption of clubcash
#
# 2011-02-18 g.baker   Created
# last changed: 06/01/15	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw/$LOGIN_URL/;
use ParseMe
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':all';
use DBI;
use DB_Connect;
use Email::Valid;
use Mail::Sendmail;
use ClubCash;

###### GLOBALS/Cookies
my $accounting = 'acfspvr@clubshop.com';
my $q = new CGI;
$q->charset('utf-8');
my $db = '';
my $gs = new G_S;
my $ME = $q->script_name;
my $xs = new XML::Simple('NoAttr'=>1);
my $cgiini = 'clubcash_redemption.cgi';
my $messages = '';
my $member_id = 0;
my $mem_info = '';
my $member_details = ();
my $login_error = 0;
my $login_type = '';
my %TMPL = ('xml'=> 10864
	     );
my $msg1='';
my $msg2='';
my $msg3='';
my $msg4='';
my $msg5='';
my $msg6='';
my $tdclassb2 = "<div align='center'><span class='Blue_Bold_Regular_Text'>";
my $tdclassb = $tdclassb2;
my $tdclasso = "<div align='center'><span class='Orange_Bold_11'>";
my $reward_cookie = {'pay_type' => ' '};
#sub Do_Page();

# Get and test the cookie information for access.

Get_Cookie();

# Get the xml language for putting out the page and convert to hash

my $xmt = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}\n";
my $lu = new XML::local_utils;
my $xml = $lu->flatXMLtoHashref($xmt);

### Get parameters from the web page

my $default = 1;
my %FORM = ();
    foreach (param()){
	$FORM{$_} = param($_);
        $reward_cookie->{$_} = param($_);
	warn "Into param:: $_ -- $FORM{$_}\n";
    }
if ($FORM{'submit'}) {
    $default = 0;
}

#Check the amount field for a comma, convert to a decimal
$FORM{'amount'} =~ s/,/./g if $FORM{'amount'};


##############################
# Check For ID Number or Die #
##############################

    if ($member_id eq '') {
	print header();
        print start_html();
	print "<P>Access Denied! No ID Number Submitted.<P>\n";
	print end_html();
	Exit();
    }

#
# Get the members info for the top of the page
#

my $qry = "
	SELECT m.firstname,
		m.lastname,
		m.alias,
		m.id,
		m.country,
		m.membertype,
		coalesce(mo.val_text,cu.currency_code,'USD') as currency_code,
		cr.rate,
		mt.gi_no_dupes,
		rc.five,
		rc.three,
		rc.twentyfive,
		pa.id AS pay_agent_id
	FROM members m
	LEFT JOIN member_options mo
		on (mo.id = m.id and mo.option_type = 14)
	LEFT JOIN currency_by_country cu
		ON (m.country = cu.country_code)
	LEFT JOIN exchange_rates_now cr
		ON (cr.code = coalesce(mo.val_text,cu.currency_code,'USD'))
	JOIN member_types mt
		ON (mt.code = m.membertype)
	LEFT JOIN reward_currencies rc
		ON (rc.code = coalesce(mo.val_text,cu.currency_code,'USD'))
	LEFT JOIN pay_agent.pay_agents pa
		ON pa.id=m.id
		AND pa.active=TRUE
	WHERE m.id = $member_id;";

my $mdata = $db->prepare($qry);
my $rv = $mdata->execute;
my $tmp2 = $mdata->fetchrow_hashref;
$gs->Prepare_UTF8($tmp2);	#this is needed on the newer server

my $amount = ClubCash::Get_ClubCash_Amounts($member_id, $cgiini);
my $min_amt = $tmp2->{'twentyfive'};


if ($default == 1) {
   Do_Page();
   Exit();
}
#
# Data was entered from the page check it out 
#
my $trans_type = 2;
my $trans_descript = 'Reward Points Redemption';
my $reward_amount = 0;
my $notes = '';
my $proc = 0;
if ($FORM{'amount'} < $min_amt || $FORM{'amount'} > $amount->{'redeem'}) {
    $msg1 = $xml->{'msg1'};
    Do_Page();
   Exit();
}
#
# Check if the currency of the member is the same as the current currency in the rewards table
#
if ($amount->{'currency'} ne $tmp2->{'currency_code'}) {
    $msg1 = $xml->{no_currency_match};
    Do_Page();
    Exit();
}

$reward_amount = $FORM{'amount'};
#
# check the pay_type. make sure the appropriate info is there for that type
#
my $cookie='';
if ($FORM{'pay_type'} eq "paypal") {
    if (!Email::Valid->address($FORM{'ppemail'})) {
       $msg2 = $xml->{msg2};
       Do_Page();
       Exit();
   }
    $trans_type = 8;
    $trans_descript = 'PayPal Reward Points Redemption';
    $notes = $FORM{'ppemail'};
    $cookie  = $q->cookie('-name'=>'reward',
                         '-value'=>'pay_type=paypal,ppemail='.$FORM{'ppemail'},
                         '-expires'=>'1yr',
                         '-path'=>'/');
}

elsif ($FORM{'pay_type'} eq "direct") {
    if ($FORM{'bankname'} eq '' ||
        $FORM{'bankname'} eq $xml->{'bnkname'} ||
        $FORM{'iban'} eq '' ||
        $FORM{'iban'} eq "IBAN" ||
        $FORM{'recipient'} eq '' ||
        $FORM{'recipient'} eq $xml->{'bnkrecpt'}) {
        $msg5 = $xml->{'msg5'};
        Do_Page();
	Exit();
    }
    $trans_type = 2;
    $trans_descript = 'Direct Bank Transfer Reward Points Redemption';
    $cookie  = $q->cookie('-name'=>'reward',
                         '-value'=>'pay_type=direct,bankname='.$FORM{'bankname'}.',iban='.$FORM{'iban'}.',bnkrecpt='.$FORM{'recipient'},
                         '-expires'=>'1yr',
                         '-path'=>'/');
    $notes = $FORM{'bankname'} . " :: " . $FORM{'iban'} . " :: " . $FORM{'recipient'};
}

elsif ($FORM{'pay_type'} eq "check") {
    $trans_type = 2;
    $notes = "Check desired";
    $trans_descript = 'Check Desired Reward Points Redemption';
    $cookie  = $q->cookie('-name'=>'reward',
                         '-value'=>'pay_type=check',
                         '-expires'=>'1yr',
                         '-path'=>'/');
}

elsif ($FORM{'pay_type'} eq "pay_agent") {
    $trans_type = 2;
    $notes = "";
    $trans_descript = 'Pay Agent Reward Points Redemption';
    $cookie  = $q->cookie('-name'=>'reward',
                         '-value'=>'pay_type=pay_agent',
                         '-expires'=>'1yr',
                         '-path'=>'/');
}

#
# Check for putting amount into clubcash
#
elsif ($FORM{'pay_type'} eq "club") {
    $trans_type = 10;
    $notes = "To Club Account";
    $trans_descript = 'Club Account Reward Points Redemption';
    $cookie  = $q->cookie('-name'=>'reward',
                         '-value'=>'pay_type=club',
                         '-expires'=>'1yr',
                         '-path'=>'/');
    $proc=1;
    my $tmp_amt = $reward_amount / $tmp2->{'rate'};
    $qry = "INSERT into soa (trans_id,
                             id,
                             trans_type,
                             amount,
                             description,
                             memo,
                             entered,
                             batch,
                             reconciled,
                             operator,
                             void,
                             ref_trans_id)
           VALUES (default, 
                   $tmp2->{'id'},
                   9200,
                   $tmp_amt,
                   'Clubcash Reward redemption',
                   NULL,
                   NOW(),
                   default,
                   default,
                   default,
                   default,
                   NULL);";
#warn $qry . "\n";
$mdata = $db->prepare($qry);
$rv = $mdata->execute;
}
#
# Nothing was picked, error out and try again
#
else {
    $msg6 = $xml->{'msg6'};
    Do_Page();
    Exit();
}

if ($FORM{'pay_type'} ne "club") {
#
# Send mail to accounting (Cindy)
#
	sendmail( 'To'=> $accounting,
	             'From'=>'clubshop@clubshop.com',
	             'Subject'=>'ClubCash Rewards redemption',
	             'Message'=> "A $trans_descript has been requested for: $tmp2->{'id'}\nThe amount is: $reward_amount in $tmp2->{'currency_code'}\nThe service fee is $tmp2->{'three'}"
	    ) or die $Mail::Sendmail::error;
}
#
# set up the info for the redemption record
#
$reward_amount = $reward_amount * -1.0;
$qry = "INSERT into reward_transactions (id,
                                         trans_id,
                                         reward_points,
                                         reward_type,
                                         cur_xchg_rate,
                                         currency,
                                         stamp,
                                         notes,
                                         created,
                                         operator,
                                         date_redeemable,
                                         date_reported,
                                         vendor_id,
                                         processed,
                                         total_pending_rewards,
                                         staff_notes)
           VALUES ($tmp2->{'id'},
                   NULL,
                   $reward_amount,
                   $trans_type,
                   1.0,
                   '$tmp2->{'currency_code'}',
                   NOW(),
                   '$notes',
                   NOW(),
                   '$tmp2->{'alias'}',
                   NOW()::DATE,
                   NOW()::DATE,
                   NULL,
                   '$proc'::BOOL,
                   0.0,
                   NULL);";
warn $qry . "\n";
$mdata = $db->prepare($qry);
$rv = $mdata->execute;

#
# Put out thankyou page
#
print header('-cookie'=>$cookie);
print start_html('-title'=>$xml->{'title'}, 
                 '-style'=>{'-src'=>'/css/reward_details.css'});

print "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
print "   <tr><td>$tdclassb $xml->{'page_header'}</span></div></td></tr>\n";

print "   <tr><td>$tdclasso $tmp2->{'firstname'} $tmp2->{'lastname'}, $tmp2->{'alias'}</span></div></td></tr>\n";
printf ("   <tr><td>$tdclasso $xml->{'avail'} %.2f $amount->{'currency'}</span></div></td></tr>\n",$amount->{'redeem'} + $reward_amount);
printf ("   <tr><td>$tdclasso $xml->{'pending'} %.2f $amount->{'currency'}</span></div></td></tr>\n",$amount->{'pending'});
print "<tr><td>&nbsp;</td></tr>\n";
print "<tr><td>$tdclassb $xml->{'done1'}</span></div></td></tr>\n";
printf ("<tr><td>$tdclassb $xml->{'done2'} %.2f</span></div></td></tr>\n",$reward_amount*-1);
printf ("<tr><td>$tdclassb $xml->{'done3'} %.2f</span></div></td></tr>\n",$tmp2->{'three'});
print "<tr><td>$tdclassb $xml->{'done4'}</span></div></td></tr>\n";
print "</table>\n</body>\n</html>\n";
#
# Done with everything
#

### Internal functions

sub Get_Cookie
{
    # If this is an admin user. 
    if ( $q->param('admin') )
    {
	# Get the admin user and try logging into the DB
	my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd
');
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	# Check for errors.
	unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
	{
	    $login_error = 1;
	}
	# Mark this as an admin user.
	$login_type = 'admin';
    }
    # If this is a member. 
    else
    {
	unless ($db = DB_Connect($cgiini)) {exit}
	$db->{'RaiseError'} = 1;

	# Get the member Login ID and info hash from the VIP cookie.
	( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

	if ( $member_id && $member_id !~ m/\D/ )
	{
	    $login_type = 'auth';
	}
        else {
	    	print redirect("$LOGIN_URL?destination=http://www.clubshop.com/cgi/clubcash_redemption.cgi");
            Exit();
        }
        if ($member_id =~ /^[0-9]+$/) {   # check to make sure member_id is numeric                                                                 
        }
        else {
	    	print redirect("$LOGIN_URL?destination=http://www.clubshop.com/cgi/clubcash_redemption.cgi");
            Exit();
        }

    }
}

sub Do_Page
{
	my $tdnccb = "<div align='left'><span class='Blue_Bold_Regular_Text'>";
	my $tdneg = "<div align='left'><span class='neg'>";
	my $tdspanb = "<span class='Blue_Bold_Regular_Text'>";
	my $lang = $q->param('language_pref');
	print header();
	print start_html('-title'=>$xml->{'title'}, 
	                 '-style'=>{'-src'=>'/css/reward_details.css'});
	
	#
	# Top of form
	print "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
	print "   <tr><td>$tdclassb $xml->{page_header}</span></div></td></tr>\n";
	
	print "   <tr><td>$tdclasso $tmp2->{'firstname'} $tmp2->{'lastname'}, $tmp2->{'alias'}</span></div></td></tr>\n";
	printf ("   <tr><td>$tdclasso $xml->{'avail'} %.2f $amount->{'currency'}</span></div></td></tr>\n",$amount->{'redeem'});
	printf ("   <tr><td>$tdclasso $xml->{'pending'} %.2f $amount->{'currency'}</span></div></td></tr>\n",$amount->{'pending'});
	#
	# The form starts here
	print $q->start_form('-method'=>'post');
	printf ("   <tr><td>$tdclassb $xml->{min1} %.2f %s </span></div></td></tr>\n", $min_amt, $amount->{'currency'});
	#
	# Not enough pending cash to do anything, let them know
	if ($min_amt > $amount->{'redeem'}) {
	    my $tmpamt = sprintf ("%.2f %s", $min_amt-$amount->{'redeem'}, $amount->{'currency'});
	    my $strng = $gs->ParseString($xml->{'notenough'}, $tmpamt);
	    print "<tr><td>$tdclassb2 $strng</span></div></td></tr>";
	    print "</form>\n";
	    print "</table>\n";
	    print "</body>\n";
	    print "</html>\n";
	    Exit();
	}
	#
	# There is enough cash for a transaction start new table
	print "<tr><td><div align='center'><table width='33%' border='0' cellspacing='0' cellpadding='0'>\n";
	printf ("   <tr><td>$tdnccb $xml->{how}</span></div></td></tr>\n");
	print "     <tr><td><div align='right'><table width='90%' border='0' cellspacing='0' cellpadding='0'>\n";
	if ($msg1 ne '') {
	   printf ("   <tr><td class='neg'> $msg1</span></div></td></tr>\n");
	}
	print "             <tr><td>$tdspanb<input type='text' size='7' name='amount'> $amount->{'currency'}</span></td></tr></table></div></td></tr>\n";
	#
	# from here down, its getting how they would like to be paid
	print "            <tr><td><div align='left'>$tdspanb$xml->{hrcv}</span></div></td></tr>\n";
	#
	# Paypal
	my $val = ' ';
	my $email = 'PayPal ' . $xml->{'email'};
	if ($reward_cookie->{'pay_type'} eq "paypal") {
	    $val = "checked='yes'";
	    $email = $reward_cookie->{'ppemail'};
	}
	print "            <tr><td><div align='left'>$tdspanb<input type='radio' name='pay_type' value='paypal' $val>Pay Pal</span></div></td></tr>\n";
	print "            <tr><td><div align='right'><table width='90%' border='0' cellspacing='0' cellpadding='0'>\n";
	print "                     <tr><td>$tdspanb<input type='text' size='50' name='ppemail' value='$email'> </span></td></tr>\n";
	if ($msg2 ne '') {
	   printf ("   <tr><td class='neg'> $msg2</span></div></td></tr>\n");
	}
	print "</table></div></td></tr>\n";
	
	# Paper check
	if ($tmp2->{'country'} eq 'US') {
	    $val = '';
	    if ($reward_cookie->{'pay_type'} eq 'check') {
	       $val = 'checked="yes"';
	    }
	    print "<tr><td><div align='left'>$tdspanb<input type='radio' name='pay_type' value='check'>Check</span></div></td></tr>\n";
	}
	#
	
	# ClubAccount
	$val = '';
	if ($reward_cookie->{'pay_type'} eq 'club') {
		$val = 'checked="yes"';
	}
	print "<tr><td><div align='left'>$tdspanb<input type='radio' name='pay_type' value='club' $val>ClubAccount</span></div></td></tr>\n";

	$val = '';
	
	# Pay Agent... as long as the user is not a Pay Agent
	if (! $tmp2->{'pay_agent_id'})
	{
		if ($reward_cookie->{'pay_type'} eq 'pay_agent')
		{
			$val = 'checked="yes"';
		}
		print "<tr><td><div align='left'>$tdspanb<input type='radio' name='pay_type' value='pay_agent' $val>Pay Agent</span></div></td></tr>\n";
	}
	   
	if ($msg6 ne '')
	{
	   printf ("   <tr><td class='neg'> $msg6</span></div></td></tr>\n");
	}
	#
	# Done with form
	printf ("<tr><td><div align='left'>$tdspanb$xml->{proc1} %.2f %s $xml->{proc2}</span></div></td></tr>\n", $tmp2->{'three'}, $amount->{'currency'});
	print "<tr><td><div align='center'><tr><td><input name='submit' type='submit' value='Submit'></div></td></tr>\n";
	print "</table>\n";
	print "</form>\n";
	print "<tr><td>&nbsp;</td></tr>\n";
	print "<tr><td><div align='left'><tr><td>$xml->{'dbddisc'}</div></td></tr>\n";
	print "</table>\n";
	print "</body>\n";
	print "</html>\n";
}

sub Exit
{
	$mdata->finish if $mdata;	# Bill can't be bothered to track the logic and close it wherever
	$db->disconnect if $db;
	exit;
}
###########################################################################

__END__

# 04/21/14 various code cleanup changes, plus the addition of pay_agent disbursal
# 05/15/14 had to add Prepare_UTF8 to the member data as it does not render properly on the newer server without it
# 11/19/14 pulled in the global login URL to use in the redirects, also implemented Exit()
04/15/15	Just some cleanup to mitigate warnings in the server log
06/01/15	Added stuff to exclude the ability for active Pay Agents to select the Pay Agent disbursement option
