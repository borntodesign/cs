#!/usr/bin/perl -w
###### logout.cgi
###### expire login cookies
###### created: 09/18/13	Bill MacArthur
###### last modified:	05/15/14	Bill MacArthur
###### version 1.0

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use G_S;

my ($db, @cookies) = ();
my $q = new CGI;

my %TMPL = (
	'default' => 10925,
	'no_top_reload' => 11085
);

Logout();
my ($tmp, $path) = split('/', ($q->path_info || ''));
Redirect() if ($path || '') eq 'redirect';

$db = DB_Connect('generic') || exit;
my $gs = G_S->new({'db'=>$db, 'CGI'=>$q});

# this deals with an empty parameter and avoiding uninit'd var errors
my $tmpl = $q->param('tmpl') || 'default';

# this deals with a template name that doesn't exist
$tmpl = $TMPL{$tmpl} || $TMPL{'default'};
$tmpl = $gs->GetObject($tmpl) || die "Failed to load template: $tmpl";
print $q->header('-charset'=>'utf-8', '-cookies'=>\@cookies), $tmpl;
$db->disconnect;
exit;

sub Logout
{
	###### just disable the cookies
#	push (@cookies, $q->cookie('-name'=>'id', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'pa', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>"AuthCustom_Generic", '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>"AuthCustom_maf", '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'minidee', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'PHPSESSID', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
	
	# I found that with a leading dot in the domain or no dot at all, Firefox would not delete a cookie belonging to www.clubshop.com so....
	foreach(qw/PRSESS id pa AuthCustom_Generic AuthCustom_maf minidee PHPSESSID country pnsid PartnerSubscription/)
	{
		push (@cookies, $q->cookie('-name'=>$_, '-domain'=>'.clubshop.com', '-expires'=>'now', '-value'=>'', '-path'=>'/'));
		push (@cookies, $q->cookie('-name'=>$_, '-domain'=>'www.clubshop.com', '-expires'=>'now', '-value'=>'', '-path'=>'/'));
	}
#	push (@cookies, $q->cookie('-name'=>'id', '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'pa', '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>"AuthCustom_Generic", '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>"AuthCustom_maf", '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'minidee', '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
#	push (@cookies, $q->cookie('-name'=>'PHPSESSID', '-domain'=>'www.clubshop.com', '-expires'=>'-1d', '-value'=>''));
}

sub Redirect
{
	# that shtml page will show one as logged in if sent directly to it, even though the cookies have been blown away, since the page loads before this script has a chance to
	print $q->redirect('-location'=> ($q->https ? 'https':'http') . '://www.clubshop.com/cs/logout.shtml', '-cookies'=>\@cookies);
	exit;
}

__END__

09/18/13	just a little code polish
05/13/14	added the no_top_reload template