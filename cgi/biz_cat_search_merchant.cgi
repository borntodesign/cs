#!/usr/bin/perl -w
###### biz_cat_search.pl
######
###### A script to search the active business codes and produce a list of possible
###### business categories based upon search terms provided by the user.
######
###### created: 09/29/03	Karl Kohrt
######
###### Last modified: 11/11/15	Bill MacArthur
	
use strict;
use CGI::Carp qw(fatalsToBrowser);
use CGI;
						
###### we are assuming operation on the web server at this time
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;
use DHSGlobals;

binmode STDOUT, ":encoding(utf8)";

###### GLOBALS
my $gs = new G_S;

# Our master html template.
my $MASTER_TMPL = (DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20091027) ? 10682 : 10678 ;
our $ON_COLOR = '#eeffee';		# the color of one alternating row
our $OFF_COLOR = '#eeeeee';		# the color of the other
our $rowcolor = '';
our $tmp = '';
our $q = new CGI;
my $message = '';			# Error message string.
my $orig_search_value = '';	# The search value(s) passed into the script.
my $search_value = '';		# The search value(s) after cleaning up.
my @search_terms = ();		# The search terms found in the $search_value string.
our $exact_matches = '';
#our $related_matches = '';
exit unless our $db = DB_Connect('biz_cat_search');

$db->{RaiseError} = 1;

# If a search value was submitted.
if ( $search_value = $q->param('search_value') )
{
	$gs->PreProcess_Input($search_value);
	$orig_search_value = $search_value;

	# If this is a quoted string...
	if ( $search_value =~ m/^".*"$/ )
	{
		# Create a search term consiting of one string.
		$search_value =~ s/"//g;
		$orig_search_value =~ s/"/&quot;/g;
		@search_terms = $search_value;
	}
	else
	{	
		# Split out the individual search terms.
		@search_terms = split (/\s{1}/, $search_value);
	}
	Query_YP();		# Find the 'Yellow Page' category matches.
#	Query_NAICS();	# Find the NAICS category matches.
}

Do_Page();
 
$db->disconnect;
exit;

##############################
###### Subroutines start here.
##############################

###### Create a link for the description and optional code.
sub Create_Link
{
	my $description = shift;	# The description which will become the link.
	my $code = shift;			# The description key/code.
	my $desc = '';
	my $link = '';

	$rowcolor = ($rowcolor eq $ON_COLOR) ? $OFF_COLOR : $ON_COLOR;
	###### we have to escape extraneous single quotes in the variables in order to not break the javascript
	($desc = $description) =~ s#'#\\'#g;

	$link = qq|<a class='nav_footer_brown' onclick="TransferData($code, '$desc'); return false;" href="javascript:void(0)">|;
	$link .= CGI::escapeHTML( $description ) . '</a>';
	return $link;
}

###### Display the page.
sub Do_Page
{	# Get the template and sub in our exact matches and related (NAICS) matches.
	my $tmpl = $gs->Get_Object($db, $MASTER_TMPL) || die "Failed to load template: $MASTER_TMPL\n";

	$tmpl =~ s/%%action%%/$q->script_name/eg;
	$tmpl =~ s/%%message%%/$message/g;
	if ( $orig_search_value )
	{
		$tmpl =~ s/%%search_value%%/$orig_search_value/g;
		$tmpl =~ s/%%exact_matches%%/$exact_matches/e;
#		$tmpl =~ s/%%related_matches%%/$related_matches/e;
	}
	else
	{			
		$tmpl =~ s/%%search_value%%//g;
		$tmpl =~ s/conditional-searchvalue-open.+?conditional-searchvalue-close//sg;
	}		
	print $q->header(-charset=>'utf-8'), $tmpl;
}

###### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff', -title=>'Error', -encoding=>'utf-8');
	print $q->h4("<b>$_[0]</b>");
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt></p>';
	print $q->end_html();
}

###### Query the exact matches from the Yellow Pages categories.
sub Query_YP
{
	my $count = 0;
	# Query the descriptions based upon the YP matches.
	my $qry = "
		SELECT description,	code
		FROM business_codes(?)
		WHERE\n";
	# Add in the search terms.
	foreach ( @search_terms )
	{
		# Unless this is the first term, we need to separate terms with OR's.
		if ( ++$count == 1 )
		{
			$qry .= "(";
		}
		else
		{
			$qry .= " OR\n";
		}
		$qry .= " description ~* ?"
	}	
	$qry .= ") ORDER BY description";
	my $lp = $gs->Get_LangPref();
	my $sth = $db->prepare($qry);
	$sth->execute($lp, @search_terms);				
	while ($tmp = $sth->fetchrow_hashref)
	{
		# Add a description to the headings list.
#		push( @yp_headings, $tmp->{yp_heading} );

		# Add a description link to the template substitution string.
		$exact_matches .= Create_Link($tmp->{description}, $tmp->{code});
		$exact_matches .= "<br />";
	}
	$sth->finish;

	if ( length($exact_matches) == 0 ) { $exact_matches = 'None' };
	return 1;
}

# 11/08/06 went to the DB_Connect model for DB connection
###### ripped out the NAICS concept to go with the YP date concept which is now contained in business_codes
# 11/11/15 added the utf8 binmode