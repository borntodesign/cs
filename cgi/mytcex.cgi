#!/usr/bin/perl -w
###### mytcex.cgi
###### Created: 01/09/12	Bill MacArthur
###### A simple report to render the Team Coach and Exec for a given ID
###### The Team Coach, as currently defined by Dick (Feb. 2013), is the Partner sponsor who is BM+ or the first nspid network BM+
###### Admin access is of the form:
######		http://www.clubshop.com/cgi/mytcex.cgi?admin=1;rid=some member ID
######
###### last modified: 05/28/15	Bill MacArthur

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use strict;
use lib ('/home/httpd/cgi-lib');
require XML::local_utils;
use G_S qw($LOGIN_URL);
use GI;
use DB_Connect;
require ADMIN_DB;
require cs_admin;
binmode STDOUT, ":encoding(utf8)";
###### CONSTANTS

my %TMPL = (
	'xml'		=> 10913,
	'report' 	=> 10914
);

###### GLOBALS
my ($db, $id, $meminfo, $lang_blocks, $Rid) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $TT = Template->new();
my $ME = $q->url;
my $ADMIN = $q->param('admin');
my $GI = GI->new('cgi'=>$q, 'db'=>$db);

Get_Cookie();
LoadLangBlocks();

Err("No login detected nor rid parameter passed") unless $Rid;

Do_Page();
Exit();

##############################
###### Subroutines start here.
##############################

###### Display the report page.
sub Do_Page
{
	my $_tmpl = $gs->GetObject($TMPL{'report'}) || die "Failed to load template $TMPL{'report'}";
	my $coach = $db->selectrow_hashref("
		SELECT m.alias, m.firstname, m.lastname, m.emailaddress, m.phone, m.cellphone, m.country, m.other_contact_info 
		FROM my_coach	r	/* this is necessary since the interface is being used by non-partners */
		JOIN members m
			ON r.upline=m.id
		WHERE r.id= $Rid
	--	AND r.coach=TRUE");
	$coach->{'telephone'} = $coach->{'phone'} || $coach->{'cellphone'};
#	my $exec = $db->selectrow_hashref("SELECT * FROM my_exec($Rid)");
#	$exec->{'telephone'} = $exec->{'phone'} || $exec->{'cellphone'};
	
	my $tmpl = ();
#	my $rv = $TT->process(\$_tmpl, {'exec'=>$exec, 'tc'=>$coach, 'lang_blocks'=>$lang_blocks}, \$tmpl);
	my $rv = $TT->process(\$_tmpl, {'tc'=>$coach, 'lang_blocks'=>$lang_blocks}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}
	Wrap($tmpl);
}											

###### Display an error message to the browser.											
sub Err
{
	$db ||= DB_Connect('generic');
	LoadLangBlocks() unless $lang_blocks;
	Wrap($q->h4({'-style'=>'margin-top:0'}, $_[0]) . '<br /><br />Current server time: <tt>' . scalar localtime() . ' PST.</tt>');
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		unless ($q->https)
		{
			my $url = $q->self_url;
			$url =~ s/^http/https/;
			print $q->redirect($url);
			Exit();
		}
		# Get the admin user and try logging into the DB
#		my $operator = $q->cookie('operator');
#		my $pwd = $q->cookie('pwd');
		my $cs = cs_admin->new({'cgi'=>$q});
		my $creds = $cs->authenticate($q->cookie('cs_admin'));
		
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} ))
		{
			Err("You must be logged in as an Admin to use this form.");
		}
		else
		{
			# Mark this as an admin user.
			$ADMIN = 1;
			$Rid = $q->param('rid');
		}
		Err("No rid parameter, which is required in ADMIN mode") unless $Rid;
	}
	# If this is a member.		 
	else
	{
		# Get the member Login ID and info hash from the VIP cookie.
		( $id, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
		if ( !$id || $id =~ /\D/ )
		{
			my $return_link = "$LOGIN_URL?destination=$ME/" . CGI::escape($q->query_string());
			print $q->redirect($return_link);
			exit;
		}
		if ( $id && $id !~ m/\D/ )
		{
			exit unless $db = DB_Connect('generic');
			$db->{'RaiseError'} = 1;
			$Rid = $id;
		}
	}
}

sub LoadLangBlocks
{
	$lang_blocks = $gs->GetObject($TMPL{'xml'}) || die "Failed to retrieve language pack template\n";
	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

sub Wrap
{
	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_blocks->{'heading'},
			'style'=>$q->style({'-type'=>'text/css'}, ''),
			'content'=>\$_[0]
		});
}

__END__

04/10/15	Nothing really important, just a little cleanup and integration of cs_admin
05/28/15	Added binmode
