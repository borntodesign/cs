#!/usr/bin/perl -w
###### cbmycards.cgi
###### Written: 04/24/2003	Karl Kohrt
######
###### This script will display the member's current personal ClubBucks cards and allow them to print
###### selected cards. It will also provide an interface to order additional personal cards
###### if his/her limit (MAXPERSONALCARDS) has not been exceeded. In each case, the option to order new
###### distribution cards in order to fill out a sheet (MAXCARDSPERPAGE) is given.  
###### If new personal cards are ordered we update the database appropriately, then if new distribution 
###### have been ordered we send the CBID's to the cbneworder.cgi script, and then send the CBID numbers
###### on to the cbcards.cgi script for display/printing(directly to cbcards.cgi if no new cards ordered).
######
###### Last modified: 06/26/03	Karl Kohrt
######

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S qw( $LOGIN_URL );
use DB_Connect;


###### CONSTANTS
use constant MAXCARDSPERPAGE => 10;	# Maxinum number of cards per printed page.	
use constant MAXPERSONALCARDS => 10;	# Maxinum number of cards per member.
use constant RIGHTARROW => 1;		# Creates a right pointing arrow in cbcards.cgi display.
use constant RIGHTANDDOWNARROW => 2;	# Creates a right and a down pointing arrow as above.
use constant CARDFRONT => 4;		# Creates a ClubBucks card front in cbcards.cgi display.


# The path to the card display script.
use constant CBCARDS_CGI_PATH => 'https://www.clubshop.com/cgi/cbcards.cgi';
# The path to the distribution card order script.
use constant CBNEWORDER_CGI_PATH => 'https://www.clubshop.com/cgi/cbneworder.cgi';

###### GLOBALS
our %TMPL = (	Salt		=> 96,
		NoneAvail 	=> 10148,
		Row		=> 10150,
		Report		=> 10151);
		
our $q = new CGI;
our ($db, $cbid_list) = ();
our ($UID, $meminfo) = ();
our (@cbids, @labels) = ();
our $current_member_id = '';
our $ME = $q->url;

###### LOCALS
my ($sth, $tmpl, $data, $counter) = ();
my $num_p_cbids = 0;		# How many personal CBID's were passed in to this script.
my $cards_avail = 0;		# How many cards the member has available.

##########################################
###### MAIN program begins here.
##########################################

our %cookies = ();		# these are the cookies passed
foreach ($q->cookie()){
	$cookies{$_} = $q->cookie($_);
}

###### validate the user
( $UID, $meminfo ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if we decide to exclude access based on an ACCESS DENIED password, we'll do it here
	
###### if the UID has any non-digits it means that its bad/expired whatever
if (!$UID || $UID =~ /\D/){ &RedirectLogin(); exit }



# DB_Connect does its own error handling, so we only need exit if it doesn't work
unless ($db = DB_Connect('cbmycards.cgi')){exit}
my $salt = &G_S::Get_Object($db, $TMPL{'Salt'}) || die "Failed to load template: $TMPL{'NoneAvail'}";

# If a VIP, Member, or Affinity Group process the order.
if (($meminfo->{membertype} =~ m/v/ ) || ($meminfo->{membertype} eq 'm') || ($meminfo->{membertype} eq 'ag') || ($meminfo->{membertype} eq 's') || ($meminfo->{membertype} eq 'ma'))
{
	# Establish the currently logged in member ID.
	$current_member_id = $UID;

	# Find out what action is to be taken.
	my $action = $q->param("action");
	my $selection = $q->param("selection");

	# If this is the first time through, display the initial page.
	unless ($action)
	{
		# Display the page with no error messages.
		Display_Page('','');
		exit;
	}

	# Otherwise, create the URL to be passed to cbneworder.cgi or cbcards.cgi.
	elsif ($action eq "reprint")
	{
		$counter = 0;
		# Put all passed, numeric paramaters(personal CBID's) into an array.
		foreach $data ($q->param())	
		{
			unless (($data =~ m/\D/) || ($counter++ >= MAXCARDSPERPAGE/2))
			{
				# With these personal ID's, we load a RIGHTARROW flag to point to them at display time.
				push @cbids, RIGHTARROW, $data;
			}		
		}		
		# Number of personal CBID's.
		$num_p_cbids = @cbids;

		# If no personal cards were selected, display the initial page with an error.
		if ($num_p_cbids == 0)
		{
			# Display the page with no error messages.
			Display_Page('*** Error: You did not select any Personal Cards to print. Try again. ***','');
			exit;
		}

		# If we're printing selected, personal cards only.
		if ($selection eq "selected_only")
		{
			# Pad the URL with 0's so we don't create bogus CB cards.					
			$counter = $num_p_cbids;
			while (++$counter <= MAXCARDSPERPAGE){ push @cbids, 0 }

			# Create the csv list of CBID's for the call to the display/print cgi script.
			$cbid_list = join ',', @cbids;

			Preview();
		}
		# If we're filling out the sheet.
		else
		{
			if (($num_p_cbids >= 2) && ($num_p_cbids < MAXCARDSPERPAGE))
			{
				# Change the cbcards.cgi's javascript flag to indicate right and down.
				$cbids[$num_p_cbids-2] = RIGHTANDDOWNARROW;
			}
			# Create the csv list of CBID's for the call to the neworder cgi script.
			$cbid_list = join ',', @cbids;
			Full_Sheet();
		}
	}

	elsif ($action eq "new_order"){
		# Count the number of labels actually passed.
		foreach ($q->param("label")){ push(@labels, $_) if $_ };
		my $num_labels = @labels;

		# If no labels for personal cards were entered, 
		#	display the initial page with an error.
		if ($num_labels == 0)
		{
			# Display the page with no error messages.
			Display_Page('', '*** Error: You did not enter any new names. Try again. ***');
			exit;
		}

		# Test if requested number of cards are available for this member.
		if (Check_Limit() > 0){
			# Request the new, personal CBID's.
			New_Order($num_labels);
			$num_p_cbids = @cbids;
			if ($selection eq "selected_only")	{
				# Pad the URL with 0's so we don't create bogus CB cards.					
				$counter = $num_p_cbids;
				while (++$counter <= MAXCARDSPERPAGE){
					push @cbids, 0;
				}
				# Create the csv list of CBID's for the call to the display/print cgi script.
				$cbid_list = join ',', @cbids;
				Preview();
			}
			else{
				if ($num_p_cbids >= 2){
					# Change the cbcards.cig's javascript flag to indicate right and down.
					$cbids[$num_p_cbids-2] = RIGHTANDDOWNARROW;
				}
				# Create the csv list of CBID's for the call to the neworder cgi script.
				$cbid_list = join ',', @cbids;
				Full_Sheet();
			}
		}
		else{
			# Print out an error message telling them they have no more cards available.
			$tmpl = &G_S::Get_Object($db, $TMPL{'NoneAvail'}) || die "Failed to load template: $TMPL{'NoneAvail'}";
			$tmpl =~ s/%%USE%%/personal use/;
			$tmpl =~ s/%%ONSTART%%/<!--/;
			$tmpl =~ s/%%ONSTOP%%/-->/;
		
			print $q->header();
			print $tmpl;
		}

	}		

	# If the script was called with invalid parameters, kick the hacker out.	
	else
	{
		&Err("You can't call this script without the proper parameters. -ref:cbmycards.cgi-");
		exit;
	}
}

###### If not a VIP, Member, or Affinity Group, print an error message.
else
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff'), $q->h3('ClubBucks Card distribution is available to VIPs, Members, and Affinity Groups only.'), $q->end_html();
}

exit;



##########################################
###### Subroutines start here.
##########################################

##########################################
###### Check to see how many cards the member still has available.
sub Check_Limit{

	my $how_many = 0;
	my ($sth, $qry, $rv) = ();

	$qry = "SELECT count(*)
		FROM clubbucks_master
		WHERE current_member_id = ?
		AND (class = 4 OR class = 5)";
	$sth = $db->prepare($qry);	$rv = $sth->execute($current_member_id);
	defined $rv or die $sth->errstr;
	# How many cards the member has on record.	
	$how_many = $sth->fetchrow_array;

	# How many cards are still available to the member.
	$how_many = MAXPERSONALCARDS - $how_many;

	return $how_many;
}


##########################################
###### Display the initial page showing existing personal cards 
######	and space to request new personal cards.
sub Display_Page{

	# Local variables.
	my ($sth, $qry, $rv) = ();
	my $message1 = shift;
	my $message2 = shift;
	my $data = '';

	$qry = "SELECT id, label
		FROM clubbucks_master
		WHERE current_member_id = ?
		AND (class = 4 OR class = 5)";
	$sth = $db->prepare($qry);
 	$rv = $sth->execute($current_member_id);
	defined $rv or die $sth->errstr;

	# Get the main report template and the row template.
	my $report = &G_S::Get_Object($db, $TMPL{'Report'}) || die "Failed to load template: $TMPL{'Report'}";
	my $rowmaster = &G_S::Get_Object($db, $TMPL{'Row'}) || die "Failed to load template: $TMPL{'Row'}";
	my $rowdetail = '';
	my $onerow = '';

	# Default is to show the existing CBID table.
	my $how_many = 0;
	while ($data = $sth->fetchrow_hashref)
	{
		# Build the existing cards table here.
		$onerow = $rowmaster;

		$data->{label} = ($data->{label} || '&nbsp;');
		$onerow =~ s/%%LABEL%%/$data->{label}/;
		$onerow =~ s/%%CBID%%/$data->{id}/g;
		$rowdetail .= $onerow;
		++$how_many;
	}

	# If no personal cards exist, don't show the table & form structure for existing cards.
	if ($how_many == 0)
	{
		$rowdetail = "";
		$message1 = "<DIV ALIGN=center class='lbl2'>You have no existing Personal ClubBucks Cards.<BR></DIV><BR>";
		$report =~ s/%%EXISTSTART%%/<!--/;
		$report =~ s/%%EXISTSTOP%%/-->/;
	}
	# Otherwise show the table & form structure for existing cards.		
	else
	{
		$report =~ s/%%EXISTSTART%%//;
		$report =~ s/%%EXISTSTOP%%//;
	}	

	# Display up to MAXCARDSPERPAGE / 2 new CBID request blanks, 
	#	dependent upon how many already exist.
	$how_many = MAXPERSONALCARDS - $how_many;
	if ($how_many > (MAXCARDSPERPAGE/2)){ $how_many = MAXCARDSPERPAGE/2 };

	my $orderdetail = "";
	if ($how_many > 0)
	{
		# Show the table & form structure for a new card order.
		$report =~ s/%%ORDERSTART%%//;
		$report =~ s/%%ORDERSTOP%%//;

		# Build out the Label fields used to order new personal CBID's here.
		$report =~ s/%%HOWMANY%%/$how_many/;

		while ($how_many-- > 0)
		{
			$orderdetail .= "<TR><TD><FONT CLASS=lbl2>ClubBucks Card User Name:</FONT></TD>";
			$orderdetail .= "<TD><INPUT TYPE=text NAME=label SIZE=20 MAXLENGTH=20></TD></TR>";
		}
	}
	else
	{	
		# Don't show the table & form structure for a new card order.
		$report =~ s/%%ORDERSTART%%/<!--/;
		$report =~ s/%%ORDERSTOP%%/-->/;
	}

	# Build the main report page.
	$report =~ s/%%MESSAGE1%%/$message1/;
	$report =~ s/%%MESSAGE2%%/$message2/;
	$report =~ s/%%ROWDETAIL%%/$rowdetail/;
	$report =~ s/%%ME%%/$ME/g;
	$report =~ s/%%ORDERDETAIL%%/$orderdetail/;
	
	# Display the final report page.
	print $q->header(-expires=>'now'), $report;
}

##########################################
sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "*<BR><B>", $_[0], "</B><BR>*";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

################################################ Send the CBID numbers to the separate New Order script to fill out the rest of the page.
sub Full_Sheet{
	my $path = CBNEWORDER_CGI_PATH . "/" . md5_hex("$salt $cbid_list") . "/" . $cbid_list;
	print $q->redirect($path);
	# Use this line for testing purposes only. You must comment out the print statement above.
#	&Err("$path");
}
			
##########################################
###### Obtain a list of new CBID numbers and initialize them in the database.
sub New_Order{
	# Set the number of cards to be queried from the table.
	my $how_many = shift;
	my $filler = '';
	my ($counter, $current) = 0;
	my ($sth, $qry, $rv) = ();
	my @new_cbids = ();

	# Choose the greaphic to be displayed next to the card number.
	if ( ($how_many == 1) && ($labels[0] eq 'Quick Card') )
	{
		$filler = CARDFRONT;
	}
	else
	{			
		$filler = RIGHTARROW;
	}

	# Get new CBID's from the database.
	$qry = "SELECT nextval('clubbucks_id_seq')";
	$sth = $db->prepare($qry);
	while ($counter++ < $how_many)
	{
		$rv = $sth->execute;
		defined $rv or die $sth->errstr;

		$current = $sth->fetchrow_array;

		# Make new list for the insertion routine below.
		push @new_cbids, $current;
		# Make a complete list for the url
		push @cbids, $filler, $current;


	}
	$sth->finish();

	# Insert the new CBID's into the table and initialize with CBID, Member ID,
	#    Label, class = 4 (Temporary) and mark Activated as true.	
	$qry = "INSERT INTO clubbucks_master
		(id, current_member_id, label, class)
		VALUES
		(?, ?, ?, ?)";
	$sth = $db->prepare($qry);

	foreach $current (@new_cbids)
	{
		my $label = shift @labels;
		$rv = $sth->execute($current, $current_member_id, $label, 4);
		defined $rv or die $sth->errstr;
	}	
	$sth->finish();
}

##########################################
###### Send the CBID numbers to the display script.
sub Preview{
	my $path = CBCARDS_CGI_PATH . "/" . md5_hex("$salt $cbid_list") . "/" . $cbid_list;
	print $q->redirect($path);
}

##########################################
sub RedirectLogin{

	my $me = $q->script_name;
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print "Please login at <a href=\"$LOGIN_URL?destination=$me\">$LOGIN_URL</a><br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
	

	
	
}

###### 04/28/03 Added protection against selecting more than 5 cards to print.
###### 04/30/03 Removed all references to the void and activated
######          columns in the database.
###### 06/26/03 Added the 'Quick Card' functionality to allow creation of a single, personal card.

