#!/usr/bin/perl -w

=head1 NAME:
business.cgi

	Display a list of links to and individual store, or display an individual store.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Display a list of links to merchants individual store display, and the merchants individual store display for printing.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname, DHS::UrlSafe

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "/home/httpd/cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use DHS::UrlSafe;
use MerchantAffiliates;
use URI::Escape;
use DHS::Templates;
use XML::local_utils;
use Data::Dumper;
#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $DHS_Templates = DHS::Templates->new();
my $MerchantAffiliates = {};
my $G_S = G_S->new({'db'=>\$db, 'CGI'=>$CGI});

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $xml = '';
my $xsl = '';
my $html_template = '';
my %template_values = ();
my $messages = {};
my $messages_xml = '';
my $params = '';
my $data = {};
my @fields_to_build_store_url = ('country', 'state', 'city', 'location_name_url');
my $skip_processing_html = 0;
my $control = '';
my $page_content = '';

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub EmptyNotUndef
{
	return '' unless defined $_[0];

	unless (ref $_[0])
	{
		return $_[0];
	}
	elsif (ref $_[0] eq 'HASH')
	{
		$_[0]->{$_} = defined $_[0]->{$_} ? $_[0]->{$_} : '' foreach keys %{$_[0]};
	}
	else
	{
		die "We don't handle other ref types";
	}
}

####################################################
# Main
####################################################

exit unless $db = DB_Connect('merchants');
$db->{'RaiseError'} = 1;

eval{
	
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100817)
	{
		#TODO: Add the live object IDs before you push to production.
		$xml = $G_S->GetObject(10813);
		$xsl = $G_S->GetObject(10814);
		$html_template = $DHS_Templates->retrieveShtmlTemplate('/cs/_includes/templates/merchant_shell.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'));
	}
	else
	{
		$xml = $G_S->GetObject(10810);
		$xsl = $G_S->GetObject(10809);
		$html_template = $DHS_Templates->retrieveShtmlTemplate('test_shell.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'));
		#warn "\n\n Got Templates \n" if ! DHSGlobals::PRODUCTION_SERVER;
	}
	
	$page_content = $G_S->Prepare_UTF8( XML::Simple::XMLin($xml) );
	
#	my @search_parameters = $DHS_UrlSafe->processURL($CGI->url('-absolute'=>1), 1);
# Keith's assumption that this would always be called as a URL rewrite makes it a bear to debug
# so we will determine what the URL is we want to send to processURL ;)
	my $url = $CGI->self_url =~ m/business\.cgi/ ?
		'/business' . $CGI->path_info : $CGI->url('-absolute'=>1);
#warn "url: $url";

	my @search_parameters = $DHS_UrlSafe->processURL($url, 1);
#warn "parameter: $_" foreach @search_parameters;
	$template_values{'CONTROL'} = $control = $search_parameters[0];
	shift(@search_parameters);
	#$search_parameters[0] country
	#$search_parameters[1] province/state
	#$search_parameters[2] city/town/village
	#$search_parameters[3] store
	
	
	my %location_params = ();
	
	#If the country code doesn't meet the minimum requirements why bother.
	if($search_parameters[0] && $search_parameters[0] =~ /^\w{2}$/)
	{
		$location_params{$fields_to_build_store_url[0]} = $search_parameters[0] if $search_parameters[0];
		$location_params{$fields_to_build_store_url[1]} = $search_parameters[1] if $search_parameters[1];
		$location_params{$fields_to_build_store_url[2]} = $search_parameters[2] if $search_parameters[2];
		$location_params{$fields_to_build_store_url[3]} = $search_parameters[3] if $search_parameters[3];
	}
	
	$location_params{$fields_to_build_store_url[2]} = 'Porto Buffolè' if ($location_params{$fields_to_build_store_url[2]} =~ /^Porto Buffol/ && $location_params{$fields_to_build_store_url[0]} eq 'IT');
	
	my @reward_merchant_package_ids = ();
	my $reward_merchant_package_ids_csv = undef;
	
	#TODO: Change this, if the merchant is logged in show the free ones
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	#This was added so if a merchant is logged in, and are a free non tracking merchant thier store will show up with the if you upgrade you will get this message.
	if(ref($cookie) eq 'HASH' && exists $cookie->{'ma_info'}->{'membertype'})
	{
		foreach (keys %{$MerchantAffiliates->{__merchant_type_by_package}})
		{
			push @reward_merchant_package_ids, $_;
		}
	}
	else
	{
		foreach (keys %{$MerchantAffiliates->{__merchant_type_by_package}})
		{
			next if ($MerchantAffiliates->{'__package_upgrade_values'}->{$_} < 10);
			push @reward_merchant_package_ids, $_;
		}
		
	}
	
	
	$reward_merchant_package_ids_csv = join(' , ', @reward_merchant_package_ids);
	
	#warn "\n\n reward_merchant_package_ids_csv: $reward_merchant_package_ids_csv \n\n";
	
	my $number_of_params_specified = scalar(keys %location_params);
	
	if($number_of_params_specified < 3)
	{
#warn "number_of_params_specified: $number_of_params_specified";
		my @results = ();
		
		if($number_of_params_specified == 2 && exists $location_params{$fields_to_build_store_url[0]} && exists $location_params{$fields_to_build_store_url[1]})
		{
			@results = $MerchantAffiliates->retrieveMerchantLocationCities('active', $location_params{$fields_to_build_store_url[0]}, $location_params{$fields_to_build_store_url[1]}, $reward_merchant_package_ids_csv);
		}
		elsif($number_of_params_specified == 1 && exists $location_params{$fields_to_build_store_url[0]})
		{
			@results = $MerchantAffiliates->retrieveMerchantLocationProvinces('active', $location_params{$fields_to_build_store_url[0]}, $reward_merchant_package_ids_csv);	
		}
		else
		{
			@results = $MerchantAffiliates->retrieveMerchantLocationCountries('active', $reward_merchant_package_ids_csv);			
		}
		
		
		if(scalar(@results) > 0)
		{
			my @links = ();
			
			my %title_listings = ();
			
			foreach (@results)
			{
				my @url_for_individual_location = ($control);
				my $url_name = '';
				
				foreach my $index (@fields_to_build_store_url)
				{
					last if ! $_->{$index};
					
					$url_name = $_->{$index . '_label'} ? $_->{$index . '_label'}: $_->{$index};
					
					push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($_->{$index});
					
				}
				
				
				my $country = $_->{'country_label'} ? $_->{'country_label'} : $_->{'country'};
				my $state = $_->{'state_label'} ? $_->{'state_label'} : $_->{'state'};
				my $city = $_->{'city'};
				
				if($country && $state && $city)
				{
					$title_listings{'country'}->{"$country"} = "$country";
					$title_listings{'state'}->{"$state"} = "$country";
					$title_listings{'city'}->{"$city"} = "$city";
				}
				elsif($country && $state)
				{
					$title_listings{'country'}->{"$country"} = "$country";
					$title_listings{'state'}->{"$state"} = "$country";
				}
				else
				{
					$title_listings{'country'}->{"$country"} = "$country";
				}
				
				
				my $url_for_location = join('/', @url_for_individual_location);
				
				push @links, qq(<a class="nav_orange" href="/$url_for_location">$url_name</a>);
			
			}
			

			
			
			#my @title = keys %title_listings;
			
			#The content for Country, and/or Province.
			$template_values{'TITLE'} = '' if ! $template_values{'TITLE'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{city}}) if $title_listings{'city'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{state}}) if $title_listings{'state'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{country}});
			
			$template_values{'PAGE_CONTENT'} = join('<br />', @links);
			
		}
		else
		{
			if(scalar(@search_parameters) > 0)
			{
				unshift @search_parameters, $control;
				pop(@search_parameters);
				
				my @redirect_url_parts = ();
				foreach my $index (@search_parameters)
				{
					last if ! $index;
					
					push @redirect_url_parts, $DHS_UrlSafe->encodeUrlPart($index);
				}
				
				my $new_search_url_for_location = join('/', @redirect_url_parts);
				
				#warn "\n\n redirect /$new_search_url_for_location \n";
				print $CGI->redirect('-uri'=>"/$new_search_url_for_location");
				
				$skip_processing_html = 1;
				
			}
		}

	}
	else
	{
		my @results = $MerchantAffiliates->retrieveLocations(\%location_params, $reward_merchant_package_ids_csv);
		#warn "\n\n Got Locations \n" if ! DHSGlobals::PRODUCTION_SERVER;
		
	#	use Data::Dumper;
	#	my $temp = \@results;
	#	warn "\n\n" . Dumper($temp) . "\n\n";
		
		if(scalar(@results) > 0 && $number_of_params_specified < scalar(@fields_to_build_store_url))
		{
			
			my @links = ();
			my %title_listings = ();
									
			foreach (@results)
			{
#				#TODO: Check to see if Discount Types of 4 are valid.
#				my $merchant_type_name = $MerchantAffiliates->getMerchantTypeNameById($_->{discount_type});
#				
#				#warn "$_->{location_name_url}: $merchant_type_name $_->{discount_type}";
#				next if (!$merchant_type_name || $merchant_type_name eq 'offline');
				
				my @url_for_individual_location = ($control);
				
				foreach my $index (@fields_to_build_store_url)
				{
					last if ! $_->{$index};
					
					push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($_->{$index});
					
					
					
				}
				
				my $url_for_location = join('/', @url_for_individual_location);
				
				my $country = $_->{'country_label'} ? $_->{'country_label'} : $_->{'country'};
				my $state = $_->{'state_label'} ? $_->{'state_label'} : $_->{'state'};
				my $city = $_->{'city'};
				
				$title_listings{'country'}->{$_->{'country'}} = $_->{'country'};
				$title_listings{'state'}->{$_->{'state'}} = $_->{'state'};
				$title_listings{'city'}->{$_->{'city'}} = $_->{'city'};
				$title_listings{'locations'}->{$_->{'location_name'}} = $_->{'location_name'};
				
				push @links, qq(<a class="nav_orange" href="/$url_for_location">$_->{location_name}</a>);
			
			}
			
			if (scalar(@links) < 1)
			{
				
				if(scalar(@search_parameters) > 0)
				{
					unshift @search_parameters, $control;
					pop(@search_parameters);
					
					my @redirect_url_parts = ();
					foreach my $index (@search_parameters)
					{
						last if ! $index;
						
						push @redirect_url_parts, $DHS_UrlSafe->encodeUrlPart($index);
					}
					
					my $new_search_url_for_location = join('/', @redirect_url_parts);
					
					#warn "\n\n listing redirect /$new_search_url_for_location \n";
					print $CGI->redirect(-uri=>"/$new_search_url_for_location");
					$skip_processing_html = 1;
					
				}
			}
			
			#The links for store names.
			$template_values{'TITLE'} = '' if ! $template_values{'TITLE'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{locations}}) if $title_listings{'locations'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{city}}) if $title_listings{'city'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{state}}) if $title_listings{'state'};
			$template_values{'TITLE'} .= ' ' . join(' ', keys %{$title_listings{country}});
			
			$template_values{'PAGE_CONTENT'} = join('<br />', @links);
		}
		elsif(scalar(@results) == 1)
		{
			
			my $language_preference = $G_S->Get_LangPref();
			my $temp_business_description = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($language_preference, $results[0]->{'business_type'})) if $results[0]->{'business_type'}; 
			$results[0]->{'biz_description'} = $temp_business_description->[0];
			
			my $keywords = $MerchantAffiliates->retrieveMerchantKeywordsByID($results[0]->{'merchant_id'});
			
			$results[0]->{'keywords'} = join(', ', @$keywords) if (scalar @$keywords);
			
			
			if($MerchantAffiliates->getMerchantTypeNameById($results[0]->{'discount_type'}) eq 'offline')
			{
				#TODO: Create a class for exchange rates.
				my $exchange_rate = 1;
				($exchange_rate) = $db->selectrow_array('
				
				SELECT 
					ern.rate
				FROM 
					currency_by_country cbc
				JOIN 
					exchange_rates_now ern
					ON 
						ern."code" = cbc.currency_code
				WHERE 
					cbc.country_code= ?', undef, $results[0]->{'country'});
				
				$results[0]->{'reward_points'} =  sprintf("%.2f", ($results[0]->{'discount'} - ($results[0]->{'discount'} * .4)) * 100 / $exchange_rate);
				
			}
			else
			{
				if($results[0]->{'special'})
				{
					if ($results[0]->{'special'} =~ /half|free/)
					{
							$results[0]->{'bogo'} = $results[0]->{'special'};
							delete $results[0]->{'special'};
					}
					elsif($results[0]->{'special'} =~ /0\.\d+/)
					{
						$results[0]->{'special'} = sprintf("%.0f", $results[0]->{'special'} * 100) . '%'
					}
					else
					{
						$results[0]->{'special'} = sprintf("%.2f", $results[0]->{'special'});
					}
					
				}
			}
			
			$results[0]->{'url'} = undef if ($results[0]->{'discount_type'} !~ /00|^11$|^12$|^13$|00/);
			
			#warn "\n\n MerchantAffiliates->{'__package_upgrade_values'}->{results[0]->{'discount_type'}} : " . $MerchantAffiliates->{'__package_upgrade_values'}->{$results[0]->{'discount_type'}};
			
			$messages->{warnings}->{upgrade_to_get} = 1 if ($MerchantAffiliates->{'__package_upgrade_values'}->{$results[0]->{'discount_type'}} < 10);
			
			$messages_xml = XML::Simple::XMLout($messages, 'RootName'=>"data", 'NoAttr'=>1 );
			
			# take care of all the undef key/vals
			EmptyNotUndef($results[0]);
			$params = XML::Simple::XMLout($results[0], 'RootName'=>"params", 'NoAttr'=>1 );
			
		$xml = <<EOT;

		<base>
			
			$messages_xml
			
			$params
			
			$xml
			
		</base>

EOT

			
			
			$template_values{'TITLE'} = "$results[0]->{location_name} $results[0]->{city} $results[0]->{state} $results[0]->{biz_description}";
			$template_values{'PRINT_CONTENT'} = $template_values{'PAGE_CONTENT'} = $G_S->Prepare_UTF8( XML::local_utils::xslt($xsl, $xml) );
			
			$template_values{'LOGO'} = $results[0]->{'coupon'};
			
			$template_values{'LOCATION_NAME'} = $results[0]->{'location_name'};
			
			if (exists $results[0]->{'keywords'})
			{
				my $keywords = lc($results[0]->{'keywords'});
				
				$keywords =~ s/,|\s-\s|"||/ /g;
				$keywords =~ s/\s\s/ /g;
				$template_values{'META_KEYWORDS'} = qq(<meta name="keywords" content="$keywords" />);
			}
			
			if (exists $results[0]->{'blurb'} && $results[0]->{'blurb'})
			{
				my $description = $results[0]->{'blurb'};
				
				$description =~ s/,|\s-\s|"||/ /g;
				$description =~ s/\s\s/ /g;
				
				
				$template_values{'META_DESCRIPTION'} = qq(<meta name="description" content="$description" />);
			}

						
			$template_values{'PRINT_CONTENT'} = $DHS_Templates->processHtmlTemplate(\%template_values, $template_values{'PRINT_CONTENT'});	
			
			#warn "\n\n Got Location \n" if ! DHSGlobals::PRODUCTION_SERVER;
		}
		else
		{
				if(scalar(@search_parameters) > 0)
				{
					unshift @search_parameters, $control;
					pop(@search_parameters);
					
					my @redirect_url_parts = ();
					foreach my $index (@search_parameters)
					{
						last if ! $index;
						
						push @redirect_url_parts, $DHS_UrlSafe->encodeUrlPart($index);
					}
					
					my $new_search_url_for_location = join('/', @redirect_url_parts);
					
					#warn "\n\n redirect /$new_search_url_for_location \n";
					print $CGI->redirect(-uri=>"/$new_search_url_for_location");
					
					$skip_processing_html = 1;
					
				}
	
		}
	}
	
	
	if(! $skip_processing_html)
	{
		
		
		$template_values{'TITLE'} =~ s/\W/ /g if $template_values{'TITLE'};
		
		$template_values{'TITLE'} = $template_values{'TITLE'} ? "$page_content->{'title_prefix'} - $template_values{'TITLE'}" : $page_content->{'title_prefix'};
		
		if(! $template_values{'META_KEYWORDS'})
		{
			my $keywords = $template_values{'TITLE'};
			$keywords =~ s/,|"|\s-\s/ /g;
			$keywords =~ s/\s\s/ /g;
			$template_values{'META_KEYWORDS'} = qq(<meta name="keywords" content="$keywords" />);			
		}

		
		print $CGI->header('text/html');
		print $DHS_Templates->processHtmlTemplate(\%template_values, $html_template);
		
#		print $CGI->header('text/plain');
#		print $xml;
		
		#warn "\n\n Done Processing Template \n" if ! DHSGlobals::PRODUCTION_SERVER;
	}
	
#	$params = XML::Simple::XMLout($data, RootName => "params" );
#	$messages_xml = XML::Simple::XMLout($messages, RootName => 'data', NoAttr => 1) if (ref($messages) eq 'HASH');


};
if($@)
{
	
	use MailTools;
	use Data::Dumper;
	
	my $http_environment_variables = {};
	my $parameters = {};
	

	foreach ($CGI->param())
	{
		$parameters->{$_} = $CGI->param($_);
	}
	
	foreach ($CGI->http())
	{
		 $http_environment_variables->{$_} = $CGI->http($_);
	}
	
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>" Error:\n $@ "
	);
	
	
	$email{'text'} .= "\n\n http_environment_variables: \n" . Dumper($http_environment_variables);
	$email{'text'} .= "\n\n parameters: \n" . Dumper($parameters);
	
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}


exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change
	06/23/11	Bill MacArthur: revised the way the URL was determined so that this thing could
	run directly as a script instead of just as a URL rewrite

=cut


