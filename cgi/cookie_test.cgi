#!/usr/bin/perl -w

use CGI ':all';
print header('-expires'=>'now', '-charset'=>'utf-8'), start_html('-bgcolor'=>'#ffffff');
unless (cookie()){
	print h1('No cookies received');
	print a({-href=>'/faqs/cookie_test_example.html'}, 'Click here to see what it should look like.');
}
else{
	print h4('These are the individual cookies we received from you.');
	foreach (cookie()){
 		my $ck = cookie($_);
		if (length $ck > 30){
			$ck = substr($ck, 0, 30) . ' -- truncated for brevity';
		}
		print "<b>$_ </b>= $ck<br />\n";
	}

}
print end_html;
exit;



