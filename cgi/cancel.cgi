#!/usr/bin/perl -w
# cancel.cgi
# the cancellation interface for APs and VIPs
# deployed: 11/01/10	Bill MacArthur
# The original design called for the cancelee to be fully logged in,
# so we will be working on the assumption that we have a full login cookie for identity.
# Otherwise, we will redirect them to the regular login prompt rather than handling authentication in here.
# last modified: 04/23/15	Bill MacArthur

use strict;
require XML::Simple;
use CGI();
use CGI::Carp qw(fatalsToBrowser);
use Template;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw($LOGIN_URL);

# Globals
my $HQ = 'webmaster@dhs-club.com,l.young@dhs-club.com';	# who is gonna get notified when we call _notifyHQ
my ($db, $id, $meminfo, $cx30, $subshell, $lb, $content, @cookies, $ADMIN, $procID);
my $cgi = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$cgi});
my %TMPL = (
	'xml'	=> 1049,
	'shell'	=> 1050
);

my $physical = '<p>
	Clubshop Rewards<br />
	955 Morisson Ave<br />
	Englewood, FL, USA 34223
	</p>';

# we are looking for an _action parameter and a "reason" parameter
my $action = $cgi->param('_action') || '';
$action = $cgi->param('realaction') . '_confirmed' if $action eq 'x_confirmed';
my $reason = $cgi->param('reason');

LoginCK() unless AdminCK();	# DB connection happens in here if they are logged in or are admin

End() unless $db;
LoadLanguagePack();	# load our Language Blocks into $lb

# we have to wait for these as we need the language pack loaded
my %CxBtns = (
	'cancel' => $cgi->button('-value'=>$lb->{'c12'}, '-onclick'=>"btnAction('cancel')"),
	'downgrade2amp' => $cgi->button('-value'=>$lb->{'c45'}, '-onclick'=>"btnAction('downgrade')"),
	'downgrade2ap' => $cgi->button('-value'=>$lb->{'c25'}, '-onclick'=>"btnAction('downgrade')"),
	'x_confirmed' => $cgi->button('-value'=>$lb->{'iamsure'}, '-onclick'=>"btnAction('x_confirmed')"),
	'not_confirmed' => $cgi->button('-value'=>$lb->{'do_not_cancel_me'}, '-onclick'=>"btnAction('cancel_cancel')"),
);

=head4
The basic flow goes like this, since we know who they are at this point.
	Prompt them to stay in based on their membertype and their 30 day cancellation status
	Process their cancellation based on their membertype, their 30 day cancel status and the action they have chosen
		(downgrade or cancel GI completely)
	Respond based on how we handled their cancellation

The final content will be placed into our "shell" template using TT placeholders along with whatever incidental language bits.
We will populate a standard sub-shell, created in here for now, with the various language bits that are necessary.
That rendered sub-shell will be placed into the real shell.
This provides us the flexibility to create different sub-shells if we need to without having to do any redevelopment.

=cut

unless ($action)
{
	# decide which prompt they should get and branch to it
	if ($meminfo->{'membertype'} eq 'v' && $cx30)
	{
		Prompt_v30();
	}
	elsif ($meminfo->{'membertype'} eq 'v')
	{
		Prompt_v();
	}
	elsif ($meminfo->{'membertype'} eq 'm')
	{
		Prompt_ap();
	}
	else
	{
		die "Unhandled cancellation type\n";
	}
}
elsif ($action eq 'cancel')	# this could be an Affiliate or Partner action
{
	if ($meminfo->{'membertype'} eq 'm')
	{
		CancelAP();
		RecordCancellation() if $ADMIN;
	}
	elsif ($meminfo->{'membertype'} eq 'v')
	{
		AreYouSure();
	}
	else
	{
		die "Unhandled Cancel action on membertype: $meminfo->{'membertype'}\n";
	}
}
elsif ($action eq 'downgrade')	# this should be a partner action only
{
	AreYouSure();
}
elsif ($action eq 'cancel_confirmed')	# this should be a partner action only
{
	if ($meminfo->{'membertype'} eq 'v')
	{
		$ADMIN ? CancelV() :_notifyHQ();
	}
	else
	{
		die "Unhandled Cancel action on membertype: $meminfo->{'membertype'}\n";
	}
	RecordCancellation() if $ADMIN;
}
elsif ($action eq 'downgrade_confirmed')	# this should be a partner action only
{
	if ($meminfo->{'membertype'} eq 'v')
	{
		$ADMIN ? DowngradeV() :_notifyHQ();
	}
	else
	{
		die "Unhandled Downgrade action on membertype: $meminfo->{'membertype'}\n";
	}
	RecordCancellation() if $ADMIN;
}
elsif ($action eq 'cancel_cancel')
{
	$content = $lb->{'ok_noxl'} . $cgi->br . $cgi->br;
}
else
{
	die "Invalid _action: $action";
}

DoPage();

End();

###### start of subroutines

sub _notifyHQ
{
	use Mail::Sendmail;
	$gs->Prepare_UTF8($meminfo, 'encode');
#	$reason = $gs->Prepare_UTF8($reason || '');
	$reason ||= '';
	sendmail(
		'Content-Type'=> 'text/plain; charset="UTF-8"',
		'to'=>$HQ,
		'from'=>'Cancelation Interface <clubshop@clubshop.com',
		'subject'=>"Cancel Submitted: $meminfo->{'alias'}",
		'message'=>"The following Partner has submitted their cancellation:\n\n$meminfo->{'alias'} - $meminfo->{'firstname'} $meminfo->{'lastname'}\nReason: $reason" .
			"\nCancellation type: " . ($action || '')
	) || die "Failed to send notification to HQ";
	
	# our response to the cancelee
	$content = "$lb->{'xcl_submitted'}<br /><br />";
}

sub AdminCK
{
	my $ck = $cgi->cookie('cs_admin');
	return unless $ck;
	
	$procID = $cgi->param('id');
	die "Admin mode requires a numeric 'id' parameter" unless $procID && $procID !~ m/\D/;
	
	# no point pulling in this stuff for the occasional admin use
	eval {
		require ADMIN_DB;
		require cs_admin;
	};
	my $cs = cs_admin->new({'cgi'=>$cgi});
	my $creds = $cs->authenticate($ck);
	exit unless $db = ADMIN_DB::DB_Connect( 'cancel', $creds->{'username'}, $creds->{'password'} );
	$db->{'RaiseError'} = 1;
	
	$meminfo = $db->selectrow_hashref("SELECT id, alias, firstname, lastname, membertype FROM members WHERE id= $procID");
	die "Failed to identify a membership by id: $procID" unless $meminfo;
	$id = $meminfo->{'id'};
	$ADMIN = 1;
}

sub AreYouSure
{
	$content = SubShell2(
		$cgi->p("$lb->{'are_you_sure1'} $lb->{'are_you_sure2'}"),
	# CGI fills in the value received by default.. and goobers it up terribly, so we are taking matters into our own hands
		$cgi->hidden('-name'=>'reason', '-value'=>$gs->Prepare_UTF8($reason), '-force'=>1) .
		$cgi->hidden('-name'=>'realaction', '-value'=>$action) .
		"$CxBtns{'not_confirmed'}<br /><br />$CxBtns{'x_confirmed'}<br /><br />"
	);
}

sub CancelAP
{
	# as of 11/01/10 this is identical to the AMP non-30 day cancellation
	my $list = RollupList('s');
	
	$content =
		$cgi->p("$lb->{'c21'} $lb->{'c55'}") .
		$cgi->p($lb->{'c36'});

	CreateNotification(110, $list);
	ChangeMembertype('s');
	ResetCookie();
}

sub CancelV
{
	my $list = RollupList('s');
	
	if ($cx30){
		$content =
			$cgi->p("$lb->{'c64'} $lb->{'c26'} $lb->{'c13'} $lb->{'c18'}") .
			$cgi->p($lb->{'c36'});	# your CSR membership is intact
		
		CreateNotification(116, $list);
	}
	else
	{
	# as of 11/01/10 this is identical to the AMP non-30 day cancellation
		$content =
			$cgi->p("$lb->{'c64'} $lb->{'c55'}") .
			$cgi->p($lb->{'c36'});
			
		CreateNotification(115, $list);
	}

	ChangeMembertype('s');
	ResetPosition();
	RecordActivity(52);
	ResetCookie();
}

sub ChangeMembertype
{
	# we are expecting 1 argument: the membertype we are changing to
	$db->do("UPDATE members SET operator='cancel.cgi', memo=?, membertype='$_[0]' WHERE id=?", undef, $reason, $id);
}

sub CreateNotification
{
	# we are expecting 2 arguments: notification_type and the list of rollups
	$db->do("INSERT INTO notifications (id, notification_type, memo) VALUES (?, $_[0], ?)", undef, $id, $_[1]);
}

sub CreateHyperlinks
{
	# We need certain hyperlinks to replace certain placeholders.
	# the placeholders read like: hyperlinks.apbc.CLICK_HERE
	# where "hyperlinks" is the key on the data passed to TT
	# apbc represents the actual URL
	# and CLICK_HERE represents a node with a similar value in our language pack that will be hyperlinked
	my %rv;
	my $a = sub { return $cgi->a({
			'-href'=>$_[0],
			'-onclick'=>'window.open(this.href); return false;',
			'-class'=>'urla'
		}, $lb->{$_[1]})};
	$rv{'apbc'}->{'CLICK_HERE'} = &$a("/cgi/apbc?alias=$meminfo->{'alias'}", 'c40');
	$rv{'ampbc'}->{'CLICK_HERE'} = &$a("/cgi/apbc?alias=$meminfo->{'alias'}", 'c40');	# this is also the current Affiliate Business Center
	$rv{'viptree'}->{'CLICK_HERE'} = &$a('/cgi/vip/tree.cgi', 'c40');
	return \%rv;
}

sub DoPage
{
	my $rv;

	my $TT = Template->new();
	
	$gs->Prepare_UTF8($meminfo, 'encode');
	$gs->Prepare_UTF8($lb, 'encode');
	my $data = {
		'hyperlinks' => CreateHyperlinks(),
		'member' => $meminfo,
		'lb' => $lb,
		'action' => $action,
		'ADMIN' => $ADMIN
	};
	
	$content = $gs->Prepare_UTF8($content, 'encode');
	
	# we have to pass our content through TT to take care of any placeholders in there
	# before passing it into TT for placement into our shell
	$TT->process(\$content, $data, \$data->{'content'});
	
	my $shell = $gs->GetObject($TMPL{'shell'}) || die "Failed to load the shell template";

	$TT->process(\$shell, $data, \$rv);
	print $cgi->header('-charset'=>'utf-8', '-cookies'=>\@cookies), $rv;
}

sub DowngradeV
{
	my $list = RollupList('m');
	
	if ($cx30){
		$content =
			$cgi->p("$lb->{'c28'} $lb->{'c58'}") .
			$cgi->p("$lb->{'c8'} $lb->{'c13'}") .
			$cgi->p($lb->{'c1_amp'}) .
			$cgi->p($lb->{'c9'});	# thank you for remaining with GI
		
		CreateNotification(117, $list);
	}
	else
	{
		$content =
			$cgi->p("$lb->{'c28'} $lb->{'c58'}") .
			$cgi->p($lb->{'c1_amp'}) .
			$cgi->p($lb->{'c9'});	# thank you for remaining with GI
		
		CreateNotification(118, $list);
	}

#	ChangeMembertype('m');
	$db->do("SELECT to_dtp_m($id)");
	ResetPosition();
	RecordActivity(53);
	ResetCookie('m');
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub LoadLanguagePack
{
	my $xml = $gs->GetObject($TMPL{'xml'}) || die "Failed to load XML language pack";
	my $xs = XML::Simple->new('NoAttr' => 1);
	$lb = $xs->XMLin($xml);
}

sub LoginCK
{
	( $id, $meminfo ) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ( $id && $id !~ /\D/ )
	{
		# nothing to do if they are good
	}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
	else
	{
		print $cgi->redirect("$LOGIN_URL?destination=" . $cgi->script_name);
		exit;
	}

	exit unless $db = DB_Connect('cancel');
	# for testing verbiage, we can set it to -0- and get everything to rollback but still get the verbiage
	$db->{'AutoCommit'} = 0;
	$db->{'RaiseError'} = 1;

	return 1;
}

sub Prompt_ap
{
	my $conditional = $meminfo->{'pool_id'} ? $cgi->li("$lb->{'c17'} $lb->{'c50'}") : '';
	$content = SubShell(
		$lb->{'c6'},
		$lb->{'c54'},
			$conditional .
			$cgi->li($lb->{'c60'}) .
			$cgi->li($lb->{'c14'}) .
			$cgi->li($lb->{'c34'}) .
			$cgi->li($lb->{'c29'}),
		"$lb->{'c41'} $lb->{'c63'}",
		$lb->{'c66'},
		"$lb->{'c5'} $lb->{'c65'} $lb->{'c59'}",
		"$lb->{'c57'} $lb->{'c62'} $lb->{'c33'}",
		$CxBtns{'cancel'}
	);
}

sub Prompt_v
{
	$content = SubShell(
		$lb->{'c42'},
		$lb->{'c16'},
			$cgi->li($lb->{'c29'}) .
			$cgi->li("$lb->{'c38'} $lb->{'c10'} $lb->{'c51'}") .
			$cgi->li("$lb->{'c23'} $lb->{'c48'} $lb->{'c43'}" .
				$cgi->ul({'-class'=>'ilist'},
					$cgi->li($lb->{'c4'}) .
#					$cgi->li("$lb->{'c32'} $lb->{'c47'}") . # c47 refers to the organization report which is no longer
					$cgi->li($lb->{'c32'}) .
					$cgi->li($lb->{'c15'})
				)
			),
		"$lb->{'c46'} $lb->{'c3'}",
		$lb->{'c67'},
		"$lb->{'c27'} $lb->{'c65'} $lb->{'c19'}",
		"$lb->{'c2'} $lb->{'c62'} $lb->{'c33'}",
		"$CxBtns{'downgrade2amp'} $CxBtns{'cancel'}"
	);
}

sub Prompt_v30
{
	$content = SubShell(
		$lb->{'c42'},
		"$lb->{'c49'} $lb->{'c16'}",
			$cgi->li($lb->{'c29'}) .
			$cgi->li("$lb->{'c38'} $lb->{'c10'} $lb->{'c51'}") .
			$cgi->li("$lb->{'c23'} $lb->{'c68'} $lb->{'c43'}" .
				$cgi->ul({'-class'=>'ilist'},
					$cgi->li($lb->{'c4'}) .
#					$cgi->li("$lb->{'c32'} $lb->{'c47'}") .
					$cgi->li($lb->{'c32'}) .
					$cgi->li($lb->{'c15'})
				)
			),
		"$lb->{'c46'} $lb->{'c3'}",
		$lb->{'c67'},
		"$lb->{'c27'} $lb->{'c65'} $lb->{'c19'}",
		"$lb->{'c2'} $lb->{'c62'} $lb->{'c33'}",
		"$CxBtns{'downgrade2amp'} $CxBtns{'cancel'}"
	);
}

sub RecordActivity
{
	$db->do("SELECT activity_entry(?, $_[0])", undef, $id);
}

sub RecordCancellation
{
	# we are doing this prmarily to record their reason for canceling
	$db->do("DELETE FROM cancellations WHERE id= ?", undef, $id);
	$db->do("INSERT INTO cancellations (id, status, memo, membertype) VALUES (?, 'p', ?, ?)", undef, $id, $reason, $meminfo->{'membertype'});
}

sub ResetCookie
{
	# we are expecting 1 argument: the new member type
	# if that is undef, then we are expiring the cookie
	unless ($_[0])
	{
		push @cookies, $cgi->cookie('-value'=>'', '-expires'=>'-1d', '-name'=>'AuthCustom_Generic');
	}
	else
	{
		my %h = %{$meminfo};
		# I need to create a copy as we are using the original membertype later on in the RecordCancellation routine
		$h{'membertype'} = $_[0];
		push @cookies, $gs->Create_AuthCustom_Generic_cookie(\%h);
	}
}

# take 'em out of the network.positions table, update the mviews.positions table
sub ResetPosition
{
	$db->do("
		DELETE FROM network.positions WHERE id= $id;
		UPDATE mviews.positions SET seq= maxintval() WHERE id= $id;
	");
}

sub RollupList
{
	# we are expecting 1 argument: the cancelee's membertype once we are done
	# based on what membertype we are changing to, we will develop a list of IDs that will get automatically rolled up
	my $sth = $db->prepare("
		SELECT m.id
		FROM members m
		JOIN member_types mt
			ON m.membertype=mt.code
		LEFT JOIN membertype_sponsors ms
			ON ms.can_sponsor=m.membertype
			AND ms.membertype='$_[0]'
		WHERE spid= ?
		AND mt.can_login=TRUE
		AND ms.can_sponsor IS NULL
	");
	$sth->execute($id);
	my $rv;
	while (my ($tmp) = $sth->fetchrow_array)
	{
		$rv .= "$tmp\n";
	}
	return $rv;
}

=text
	0 = "header phrase"
	1 = initial blurb
	2 = the bulleted list elements
	3 = blurb to consider
	4 = mailing instructions
	5 = mailing caveats
	6 = prompt for reason
	7 = buttons
=cut

sub SubShell
{
	return <<EOT;
	<span class="Header_Main">$_[0]</span>
	<p>
	$_[1]
	</p>
	<ul>
	$_[2]
	</ul>
	<p>
	$_[3]
	</p>
	<div style="border:1px solid grey; padding: 1em;">
	$_[4]
	$physical
	<p>
	$_[5]
	</p>
	<p>
	$_[6]
	</p>
	<form method="post" action="${\$cgi->script_name}" style="text-align:center" id="form1">
	<input type="hidden" name="id" value="$id" />
	<input type="hidden" name="_action" value="" id="_action" />
	<textarea cols="60" rows="3" name="reason"></textarea>
	<div style="margin-top:0.5em">
	$_[7]
	</div>
	</form>
	</div>
	<script type="text/javascript">
	function btnAction(myaction)
	{
		document.getElementById('_action').value= myaction;
		document.getElementById('form1').submit();
	}
	</script>
EOT
}

sub SubShell2
{
	return <<EOT;
	<p>
	$_[0]
	</p>
	<form method="post" action="${\$cgi->script_name}" style="text-align:center" id="form1">
	<input type="hidden" name="id" value="$id" />
	<input type="hidden" name="_action" value="" id="_action" />
	$_[1]
	</form>

	<script type="text/javascript">
	function btnAction(myaction)
	{
		document.getElementById('_action').value= myaction;
		document.getElementById('form1').submit();
	}
	</script>
EOT
}

__END__

Change Log

11/03/10	added the membertype field to the cancellations table so that we could distinguish the type of member that created the record
11/16/10	created a copy of the hash referred to in meminfo for use in recreating the cookie so as to retain the original membertype in the original hash
03/22/12	AMPs are going away... 'nuf said
05/20/14	moved back to Clubshop and some small code cleanup
02/02/15	Dealt with encoding issues in DoPage()
04/02/15	Dealt with encoding issues in _notifyHQ() as well as encoding issues with CGI.pm in recreating the 'reason' hidden tag in AreYouSure()
04/23/15	Changed DowngradeV to use to_dtp_m(). Made both cancel and downgrade of partners call the new ResetPosition()
