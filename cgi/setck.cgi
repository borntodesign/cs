#!/usr/bin/perl -w
###### 
###### 
###### created:
###### last modified:

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DBI;
use DB_Connect;
use CGI;

my $q = new CGI;
my $id = uc $q->param('id');
goto 'END' unless $id;
my $db;
unless ($db = DB_Connect('generic')) {exit}
my $qry = "SELECT id FROM members WHERE " . ($id =~ /\D/ ? 'alias=?' : 'id=?');
$id = $db->selectrow_array($qry, undef, $id);
goto 'END' unless $id;
my @ck = ();
foreach (qw/refspid sponsorID/)
{
	push (@ck, $q->cookie(
		-name=>$_,
		-value=>$id,
		-domain=>'.clubshop.com',
		-expires=>'+1y'));
}
END:
print $q->header(-cookies=>\@ck, -type=>'text/css');
$db->disconnect if $db;
exit;



