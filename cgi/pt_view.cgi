#!/usr/bin/perl -w
###### pt_view.cgi 		Retrieve raw HTML template for id as long as it is public & active
###### originally written by: Stephen Martin
###### last modified 07/02/04	Bill MacArthur

use DBI;
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;

$| = 1;   # do not buffer error messages

###### DEFINITIONS
our $OBJECTS = "cgi_objects";

###### GLOBALS
our $q = new CGI; 
our ($db, $data, $TMPL );

our $id = $q->param('id');
unless ($id)
{
	Err("Missing ID parameter");
	exit;
}
$id =~ s/\D//g;

###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');

if ($operator && $pwd)
{
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = &ADMIN_DB::DB_Connect( 'pt_view', $operator, $pwd ))
	{
		Err('Your administrative login could not be verified.');
		exit;
	}
}

###### this will be our VIP login
else{	
	my ( $id ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ( $id && $id !~ /\D/ ){}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
	else
	{
		Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
		exit;
	}

	unless ($db = DB_Connect('pt_view')){ exit }
}
$db->{RaiseError} = 1;

###### get the record on our reportee
my $qry = "select obj_value from $OBJECTS where public_view = 't' AND active = 't' AND obj_id = ?";
my $sth = $db->prepare($qry);
$sth->execute($id);
$data = $sth->fetchrow_hashref; 
$sth->finish;

unless ($data->{obj_value})
{
	Err("Cannot find a match on that ID: $id<br />It may not exist or it may NOT be PUBLIC."); 
	goto "END";
}

$TMPL = $data->{obj_value};
$TMPL =  $q->escapeHTML($TMPL);
$TMPL =~ s/cM//g;
$TMPL =~ s#\n#<br />#g;

$TMPL = $TMPL . <<EOHTML1;
<br />
<div align="center">
 <form>
  <input type="submit" name="ExecC" value=" Go Back " onClick="window.history.go(-1); return false;" class="in" />
 </form>
</div>
EOHTML1
print $q->header();
print $q->start_html(-bgcolor=>'#ffffff', -title=>'View');


print "<font style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: \#000099;\">";
print $TMPL;
print "</font>";

print $q->end_html();

END:
$db->disconnect;

exit;

sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print "$_[0]<br /><br />", scalar localtime();
	print $q->end_html();
}

###### 12/18/02 went to the 'use' style for custom modules and the AuthCustom_Generic cookie
###### 07/02/04 fixed a javascript error
