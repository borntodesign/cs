#!/usr/bin/perl -w
###### tpgroup.cgi
###### generate the "Group" reports for Trial Partners
###### initially created as a direct copy of cgi/vip/tree.cgi: 08/11/14	Bill MacArthur
###### last modified: 11/06/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw($EXEC_BASE $LOGIN_URL);
use XML::local_utils;
use XML::Simple;
use XML::LibXSLT;
require Template;
require TT2::ParseMe;

#use Data::Dumper;	# for testing only
binmode STDOUT, ":encoding(utf8)";

my $MaxPageVIPs = 1500; # the maximum number of Partners we will display
my %TABLES = (
	'grp' => {
		'refcomp' => 'refcomp',
		'relationships' => 'tp_relationships',
		'a_level' => 'a_level_stable'
	}
);

my %TABLEJOINS = (
	'grp' => ""
);

my %XTRACOLS = (
	'grp' => ''
);

my %TMPL = (
	'grp' => {
		'horizXSL'	=> 11088,
		'horiz2vertXSL'	=> 10468,
		'base'		=> 11089,	# this is the final layout document
		'lang_blocks'	=> 10473
	},
	'TT' => { 'final' => '/home/httpd/cgi-templates/TT/tpgroup-final.tt' }
);

# we'll count the number of loops while creating our result set
my $Count = 0;

# for now we'll simply place any errors in this var (make sure it's valid xhtml)
# if present, an <errors> node will be created in the root
my $errors = '';

my $data_xml = '<root>';
my ($reportee, %Coaches, $db, @CoacheesCoachees, $ADMIN, $meminfo, $selfReportee) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $TT = Template->new({'ABSOLUTE' => 1});
my $xmlu = new XML::local_utils;
my $xs = new XML::Simple;

#my $rpttype = $path[1] || 'grp';
my $rpttype = 'grp';

my $script_name = $q->script_name;

my $xml_param = $q->param('xml') || '';	# values for this are "vert", "horiz", "vip", "coaches"

my $BREAKLOOP = 0;
my $MAXROWS = 0;

# this query should contain the simple membership data we want to display up front
# more expensive queries that would involve left joins here are implemented
# as explicit direct queries in a sub-routine below

my $qry = "
	SELECT
		m.id,
		m.spid AS rspid,
		m.alias,
		m.firstname ||' '|| m.lastname AS name,
		COALESCE(m.emailaddress,'') AS emailaddress,	-- pools do not have one
		COALESCE(m.country,'') AS country,			-- pools do not have one
		m.membertype
		, COALESCE(rc.ppp,0) AS ppp
		, rc.prvs
--		, rc.sales_req_count AS execs
		, COALESCE(rc.opp,0) AS tpp
		, COALESCE(rc.plpp,0) AS pmpp
		, COALESCE(rc.op,0) AS grp
--		, COALESCE(rc.prv_handicap,0) AS grp_nop
--		, COALESCE(rc.a_level, 0) AS level
--		, COALESCE(als.a_level, 0) AS stable_a_level
--		, COALESCE(ra.a_level, 0) AS last_month_a_level
		, my_coach.upline AS coach_id
		, COALESCE(rc.xone,0) AS ttl_grp
--		, COALESCE(rc.xtwo,0) AS ttl_grp_nopays
--		, (COALESCE(rc.xone,0) - COALESCE(rc.op,0)) AS ttl_grp_diff
--		, date_fmt_by_country(od.pool_vw.deadline, ?) AS deadline
--		, mrs.ranking_summary AS prospect_rank
--		, mrs.last_action_time::DATE AS ranking_last_action_time
		, ps_cnts.tp
		$XTRACOLS{$rpttype}

	FROM $TABLES{$rpttype}->{'relationships'} r
	
	JOIN members m
		ON m.id=r.id

	JOIN ps_cnts
		ON ps_cnts.id=r.id

	$TABLEJOINS{$rpttype}
/*
	LEFT JOIN $TABLES{$rpttype}->{'relationships'} rcoach
		ON r.id=rcoach.id
		AND rcoach.coach=TRUE
*/
	LEFT JOIN my_coach
		ON my_coach.id=r.id
		
	LEFT JOIN $TABLES{$rpttype}->{'refcomp'} rc
		ON rc.id=m.id
/*
	LEFT JOIN $TABLES{$rpttype}->{'a_level'} als
		ON als.id=m.id

	LEFT JOIN reference.a_level ra
		ON ra.id=m.id

	LEFT JOIN od.pool_vw
		ON od.pool_vw.id=m.id

	LEFT JOIN member_ranking_summary mrs
		ON mrs.member_id=m.id
*/
	WHERE r.next=TRUE
	AND r.upline= ?
--	ORDER BY rc.a_level DESC NULLS LAST, id ASC
	ORDER BY ps_cnts.tp DESC, id ASC";

###### error handling provided by LoginCK
$meminfo = LoginCK();

$db = DB_Connect::DB_Connect('tree.cgi') || exit;
$db->{'RaiseError'} = 1;

# beyond deciding who the reportee is, these values are used only to determine if we are tracking
my $pID = $gs->PreProcess_Input( uc($q->param('id')) ) || '';
my $ckID = $q->cookie('id') || $pID;
my $loginID = $meminfo->{'id'} || $pID;
my $rid = $pID || $ckID || $loginID;

###### we need to set a value for this meminfo key as it is used in many evaluations
$meminfo->{'id'} ||= 0;

$reportee = Details($rid) || Err("We're sorry, but that is an invalid membership ID: $rid");

$pID = $reportee->{'id'};	# we are comparing numeric values in TrackUsage() and the pID could be an alphanumeric alias

###### $reportee is now a hashref (as long as we found somebody :> )
# the validator will take care of error handling
if ( ValidateReportee() )
{
	TrackUsage();
	Do_Report($reportee);
}

Exit();

###### ######

sub AddCoachCache
{
	my $cid = shift;
	return unless $cid;
	unless ($Coaches{ $cid})
	{
		$Coaches{ $cid } = $db->selectrow_hashref("SELECT alias, emailaddress, (firstname||' '||lastname) AS name FROM members WHERE id= $cid");
	}
}

sub BreakLoop
{
	return 1 if $BREAKLOOP;
	$Count++;
	# even a 500 count page is grossly unmanageable
	if ($Count > $MaxPageVIPs)
	{
		$BREAKLOOP = 1;
		my $msg = "<i>This report is truncated and may not display properly.</i><br />
			The number of Partners displayed would exceed $MaxPageVIPs.
			Please 'Jump To:' the desired Partner or select a lower 'Stop at:' role.";
		$errors .= $q->h5($msg);
		return 1;
	}
	return 0;
}

sub CoachXML
{
	my $rv = '<coaches>';
	foreach my $c (keys %Coaches)
	{
		$rv .= qq|<coach cid="$c" alias="$Coaches{$c}->{'alias'}" emailaddress="$Coaches{$c}->{'emailaddress'}" name="$Coaches{$c}->{'name'}" />|;
	}
	return $rv . '</coaches>';
}

sub Details
{
	###### get whatever data is desired for this party
	my $id = shift;
	
	unless ($id)
	{
		print $q->redirect("$LOGIN_URL?destination=$script_name");
		Exit();
	}
	
	if ($id =~ m/\D/)
	{
		($id) = $db->selectrow_array('SELECT id FROM members WHERE alias= ?', undef, $id) || return;
	}
	
	my $def_lang_pref = $gs->Get_LangPref();
	$def_lang_pref = $db->quote($def_lang_pref);
	
	my $qry = "
		WITH per AS (
			SELECT get_working_period(1,NOW()) AS period
		),
		potcomm AS (
			SELECT MAX(shares) AS shares
			FROM network.level_values_tnt lv
			LEFT JOIN refcomp rc
				ON rc.opp >= lv.tmpp
				AND rc.prvs >= lv.prp
				AND rc.id= $id
			WHERE rc.id IS NOT NULL
			OR lv.pay_level=1
		)
		SELECT
			m.id,
			m.alias,
			m.spid AS rspid,
			m.membertype,
		/* a number of these COALESCE's are because of member pools which do not have the regular membership data... and we want to avoid uninit'd var errors */
			COALESCE(m.suffix,'') AS suffix,
			m.firstname,
			m.lastname,
			m.firstname ||' '|| m.lastname AS name,
			COALESCE(m.emailaddress,'') AS emailaddress,
			COALESCE(m.address1,'') AS address1,
			m.membertype,
			COALESCE(m.address2,'') AS address2,
			COALESCE(m.city,'') AS city,
			COALESCE(m.state,'') AS state,
			COALESCE(m.country,'') AS country,
			COALESCE(m.postalcode,'') AS postalcode,
			COALESCE(m.phone,'') AS phone
			, COALESCE(m.language_pref, 'en') AS language_pref
			, COALESCE(rc.ppp,0) AS ppp
			, COALESCE(rc.prvs,0) AS prvs
			, COALESCE(rc.opp,0) AS tpp
			, format_number_by_locale(COALESCE(rc.opp,0), m.country, COALESCE(m.language_pref, $def_lang_pref)) AS tpp_formatted
			, COALESCE(rc.op,0) AS grp
			, COALESCE(my_coach.upline::TEXT, '') AS coach_id		-- this can be NULL if the coach is nspid and not in the rspid upline
			, COALESCE(od.pool_vw.pnsid::TEXT,'') AS od_pool_pnsid
			, date_fmt_by_country(od.pool_vw.deadline, m.country) AS deadline
			, format_currency_by_locale(exchange_usd_for_cbc(potcomm.shares * spv.pp, m.country), m.country, COALESCE(m.language_pref, $def_lang_pref)) AS potential_commission
			
		FROM potcomm, members m

		JOIN subscription_point_values_by_country spv
			ON spv.country_code=m.country
			AND spv.subscription_type=40
			
		LEFT JOIN $TABLES{$rpttype}->{'refcomp'} rc
			ON rc.id=m.id

		LEFT JOIN my_coach
			ON my_coach.id=m.id
			
		LEFT JOIN od.pool_vw
			ON od.pool_vw.id=m.id
		
		LEFT JOIN network.potential_commission npc
			ON npc.id=m.id
			
		WHERE m.id= $id";
	my $rv = $db->selectrow_hashref($qry) || return;

#warn Dumper($rv);	
	return $rv;
}

sub Display
{
	my $xml = $gs->GetObject($TMPL{$rpttype}->{'lang_blocks'}) || die "Failed to load lang_blocks XML\n";
	$xml = $xs->XMLin($xml, 'NoAttr'=>1);

	TT2::ParseMe::Parse($xml, {
		'reportee'=>$reportee,
		'CGI'=>$q,
		'links'=>{'team_rpt'=>$q->a({'href'=>"/cgi/TrialPartner.cgi?id=$reportee->{'id'}"}, $reportee->{'tpp_formatted'})}
		}, $TT);

	print $q->header(
		'-type'=>'text/html',
		'-charset'=>'utf-8'
	);

	$TT->process( $TMPL{'TT'}->{'final'},
		{
		'lang_blocks'=>$xml,
		'reportee'=>$reportee,
		'Coaches'=>\%Coaches,
#		'meminfo'=>$meminfo,
		'errors'=>$errors,
		'tree_view'=>$data_xml,
		'script_name'=>$script_name
		}
	) || print $TT->error();
}

sub Do_Report
{
	Looper( $_[0], -1 );
	###### at this point global $data_xml contains raw VIP downline data

#	$data_xml .= "<maxrows>$MAXROWS</maxrows>$slabels";
	if ($xml_param eq 'vip')
	{
		EmitXML($data_xml . '</root>');
		return;
	}

	if ($xml_param eq 'coaches')
	{
		EmitXML(CoachXML());
		return;
	}
	else
	{
		$data_xml .= CoachXML();
	}

	my $lng = $gs->GetObject($TMPL{$rpttype}->{'lang_blocks'}) || die "Failed to load lang_blocks XML\n";
	
	$data_xml .= '<admin>1</admin>' if $ADMIN;
	$data_xml .= $lng . '</root>';

	###### convert to a horizontal Table
	my $xsl = $gs->GetObject($TMPL{$rpttype}->{'horizXSL'}) || die "Failed to load horizXSL\n";
	$data_xml = "<root><maxrows>$MAXROWS</maxrows>" . $xmlu->xslt($xsl, $data_xml) . ($ADMIN ? '<admin>1</admin>' : '') . '</root>';
	if ($xml_param eq 'horiz')
	{
		EmitXML($data_xml);
		return;
	}

	###### convert horizontal Table to a vertical Table
	$xsl = $gs->GetObject($TMPL{$rpttype}->{'horiz2vertXSL'}) || die "Failed to load horiz2vertXSL\n";
	$data_xml = $xmlu->xslt($xsl, $data_xml);

	if ($xml_param eq 'vert')
	{
		EmitXML($data_xml);
		return;
	}

	Display();
}

sub EmitXML
{
	print $q->header('-type'=>'text/plain', '-charset'=>'utf-8'), $_[0];
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();

	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetPNSDetails
{
	my $v = shift;
	($v->{'pns_pid'}) = $db->selectrow_array("SELECT pid FROM network.partner_network_separators WHERE pnsid= $v->{'id'}");
	($v->{'pns_fl_id'}) = $db->selectrow_array("SELECT id FROM network.spids WHERE spid= $v->{'id'}");
}

sub GetPNSstats
{
	my $v = shift;
	($v->{'count_tp'}, $v->{'count_v'}) = $db->selectrow_array("SELECT tp, v FROM od.pool_counts WHERE id= $v->{'id'}");
}

sub LoginCK
{
	my ($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ($id && $id !~ /\D/)
	{
		$ADMIN = 1 if $id == 1;
		return $meminfo;
	}

###### if they are not a Partner or Staff, they don't have access to the report
	if ( (ref $meminfo eq 'HASH') && $meminfo->{'id'} && ! grep {$_ eq $meminfo->{'membertype'}} ('v', 'staff', 'tp') )
	{
		Err('These Reports are for Partners/Trial Partners only'),
		return;
	}

	return;
}

sub Looper
{
	no warnings 'recursion';
	my $v = shift;
	my $rowcnt = shift;

	# the first pass thru should be our reportee, who we do not want to exclude if he is still in the TP line
	my $xclude = 0;#1 if $v->{'od_pool_pnsid'} && ($reportee->{'id'} != $v->{'id'}) &&  ($v->{'membertype'} eq 'v' || $v->{'membertype'} eq 'mp' || $v->{'membertype'} eq 'tp');
	$rowcnt++ unless $xclude;
	WriteXML($v) unless $xclude;	# omit these parties as they are still in the trial partner line

	AddCoachCache($v->{'coach_id'});
	
	###### if we are filtering something, or we are going to stop, this is where we will do it
	unless (BreakLoop())
	{
		my $sth = $db->prepare($qry);

		$sth->execute($v->{'id'});
		while ($_ = $sth->fetchrow_hashref)
		{
#			warn Dumper($_);
			Looper($_, $rowcnt);
		}
	}
	$data_xml .= "</vip>\n" unless $xclude;
	$MAXROWS = $rowcnt if $rowcnt > $MAXROWS;
}

sub TrackUsage
{
	# only track if the viewer is the actual party pulling the report
	# by "coalescing" the comparison with ourself, we avoid having to determine if the comparison variable is empty or not before proceeding
	# the final 
	return if
		$rid ne ($pID || $rid) ||
		$rid ne ($ckID || $rid) ||
		$rid ne ($loginID || $rid);
#warn "tracking $reportee->{'id'}";
	$db->do("INSERT INTO activity_tracking (id, class) VALUES ($reportee->{'id'}, 68)");
	return;
}

sub ValidateReportee
{
	unless ($reportee)
	{
	###### we can implement a better kind of error handling later
	###### this is just a catcher at this point
		Err("We are sorry, but $rid cannot be identified");
	}
	elsif ($reportee->{'id'} == 1)
	{
		# for some reason not worth analyzing, this thing goes into a long graveyard spiral trying to produce a report for 01
		# and since there is no real reason to do so, it must be a mistake
		Err("A report cannot be produced for 01");
	}
	elsif ($reportee->{'id'} == ($meminfo->{'id'} || -1))
	{
		return 1;
	}
	elsif ($meminfo->{'id'} == 3127951 || $meminfo->{'id'} == 1)	# Dick or 01 can do anything
	{
		# in order for the report to work more normally, we will make believe that the viewer is the actual reportee
		$meminfo = $reportee;
		return 1;
	}
	elsif ($reportee->{'membertype'} eq 'mp' || $reportee->{'membertype'} eq 'pns')
	{
		# as of August 2013, every Partner can access the menu for a Pool as the nspid_frontline report is filtered to only show them memberships in the GRP network
		return 1;
	}
	elsif ($reportee->{'membertype'} eq 'v')
	{
		# per Dick as we create this: (New) Partners listed in the TP Team Report, will be able to see their own Partner Group Reports,
		# so there is no need for any Partner to see their own listing in or have access to a Trial Partner Group Report.
		print $q->redirect('/cgi/vip/tree.cgi/grp');
		Exit();
	}
	
	# unless we get some kind of direction in regards to global access to this report, we may as well just let 'em go
	return 1 unless $meminfo->{'id'};
	
	# for ordinary Partner access we use one type of access ck, but for pools we are more restrictive
	# per Dick 04/09/13: Create "Frontline Reports" for each Pool and allow the RD+ upline from it to be able to access it.
	my $func = 'network.access_ck1';
	$func = 'network.access_ck_pools' if $reportee->{'membertype'} eq 'mp';
	
	my ($rv) = $db->selectrow_array("SELECT $func($reportee->{'id'}, $meminfo->{'id'})");
	if ($rv)
	{
		return 1;
	}

	Err("Your pay level or coaching position are insufficient to gain access to this information.");
	return undef;
}

sub WriteXML
{
	my $v = shift;
	###### first we have to look up our details
	###### it is cheaper SQL wise to do it this way than with left joins

	($v->{'stype'}, $v->{'paid_thru'}) = $db->selectrow_array("SELECT subscription_type, paid FROM partner_subscription_monthly WHERE member_id= $v->{'id'}");
	($v->{'nops'}) = $db->selectrow_array("SELECT nops FROM mviews.my_rspid_nops WHERE id= $v->{'id'}");	# for now we will leave a null alone

	# call us paid thru if we are a member pool
	$v->{'paid_thru'} = 1 if $v->{'membertype'} eq 'mp';
	
	# get the pool ID under me if I am a Partner Network Separator
	# also get the party immediately down in the nspid line
	# and get the counts stats
	if ($v->{'membertype'} eq 'pns')
	{
		GetPNSDetails($v);
		GetPNSstats($v);
	}
	
	$data_xml .= '<vip';
	$data_xml .= qq# membertype="$v->{'membertype'}"#;
	
	# this section assigns empty values to the attribute nodes if there is no true value
	foreach (qw/name id rspid alias emailaddress stype country coach_id/)
	{
		$data_xml .= qq# $_="${\($v->{$_} || '')}"#;
	}
	
	# this section assigns -0- values to the attribute nodes if there is no true value
	foreach (qw/level tpp ppp pmpp ompp personal_gpp paid_thru prvs grp/)
	{
		$data_xml .= qq# $_="${\($v->{$_} || 0)}"#;
	}
	
	# this section create attribute nodes if there is a true value
	foreach (qw/od_pool_pnsid pns_pid count_tp count_v/)
	{
		$data_xml .= qq# $_="$v->{$_}"# if $v->{$_};
	}
	
	if ($v->{'rspid'} == $reportee->{'id'}) 
	{
		$data_xml .= qq# myrspid="1"#;
	}

	if ($v->{'membertype'} eq 'mp' || $v->{'membertype'} eq 'pns')
	{
		$data_xml .= qq# pool="1" membertype="$v->{'membertype'}"#;
	}
	
	$data_xml .= ">\n";
}

__END__

10/03/14	Reactivated TrackUsage()
			Added the potential commission into the Details as well as tpp_formatted
			Also added 'links' in the parsing of the XML in Display()
			
01/05/15	Revised the mechanism for determining when to track usage
01/13/15	Added error handling for the case when we get nothing back from Details()
01/28/15	Removed a bunch of SQL columns that are not used. Also removed them from the XML write out
04/10/15	removed an access check that relied upon network.relationships as that is going away
05/12/15	Minor utf8 tweak to the XML to deal with a recent upgrade of DBD::Pg
05/27/15	Ended up adding the binmode to really fix other encoding issues that arose under differing circumstances
11/06/15	removed network.potential_commission from Details() and utilized a blend of other SQL to determine a number based upon the current comp plan