#!/usr/bin/perl -w
###### pireport2.cgi
###### Personal Income Report (used to be potential income report ;> )
###### newly released: 02/29/12	Bill MacArthur
###### last modified: 05/13/13	Bill MacArthur

# notice the "2" in the name
# this "version" is just meant to serve up the months of Jan. - Mar. 2012 due to changes in the comp plan and underlying data changes as well

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use Template;
use Encode;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
require XML::LibXML::Simple;
use G_S qw/$EXEC_BASE $LOGIN_URL/;
use XML::local_utils;
use GI;
#use Data::Dumper;	# testing only

my %TMPL = (
	'xml'		=> 10911,
	'months'	=> 10572,
	'TT'		=> 10910 # -> live template # a test template -> 10233
);

my %Links = (
	'compplan' => 'http://www.clubshop.com/manual/compensation/partner.html'
);

my %Months = (
	'01' => 'January',
	'02' => 'February',
	'03' => 'March',
	'04' => 'April',
	'05' => 'May',
	'06' => 'June',
	'07' => 'July',
	'08' => 'August',
	'09' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December'
);

# the pay qualifier columns as returned by Get_Mbr_Stats()
# having them listed here makes it easier to use the list wherever...
my @cols = qw/frontline_ppp op/;

# the same values mapped to the corresponding column in the level_values table
my %cols2lvlvals = (
#	'prvs'=>'ovspid_vips',
	'frontline_ppp'=>'frontline_pp',
#	'opp'=>'organizational_pp',
	'op'=>'op_threshold',
#	'qpp'=>'frontline_pp'
#	'ppp'=>'ppp_threshold',
#	'sales_req_count'=>'sales_trans_threshold'
);
my $q = new CGI;
my $TT = Template->new();
my $gs = new G_S;


my ($db, $lang_blocks, %params, $USER_ID, $meminfo) = ();

###### for now we'll accept a simple member ID
###### and if it turns out to be a Partner, we'll require a login so as to protect
###### the privacy of the Partners

my $ADMIN = AdminCK();
	
my $id = $q->cookie('id');
die "Bad ID cookie" if $id && $id =~ /\D/;

LoadParams();

my $rid = uc($params{'rid'});
$rid ||= ($q->cookie('id') || '') unless $ADMIN;	# if in admin mode, we will only accept a parameter for identification

unless ($rid)
{
	my $error = $ADMIN ? 'No rid parameter' : 'You must be ' .
		$q->a({'-href'=>"$LOGIN_URL?destination=" . $q->script_name}, 'logged in') .
		' to use this report.';
	Err();
	exit;
}

$db ||= DB_Connect::DB_Connect('pireport.cgi') || exit;
$db->{'RaiseError'} = 1;
my $GI = GI->new('cgi'=>$q, 'db'=>$db);
LoadLangBlocks();

my $level_values = $db->selectall_hashref('SELECT * FROM level_values WHERE lvalue > 0', 'lvalue');
my $rpte = $db->selectrow_hashref("
	SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype,
		cbc.currency_code,
		exr.rate,
		cc.exponent AS currency_exponent,
		COALESCE(a.a_level, 0) AS a_level,
		(my_exec(m.id)).id AS my_exec
	FROM members m
	JOIN currency_by_country cbc
		ON cbc.country_code=m.country
	JOIN exchange_rates_now exr
		ON exr.code=cbc.currency_code
	JOIN currency_codes cc
		ON cbc.currency_code=cc.code
	LEFT JOIN a_level_stable a
		ON a.id=m.id
	WHERE ${\($rid =~ m/\D/ ? 'm.alias':'m.id')} = ?", undef, $rid);

unless ($rpte)
{
	Err("No matching records for: $rid");
	Exit();
}

###### are they a valid membership?
unless ((grep $_ eq $rpte->{'membertype'}, qw/v/) || $ADMIN)
{
	Err( $gs->ParseString($lang_blocks->{'no_rpt_available'}, $rpte->{'membertype'}) );
	Exit();
}

if ($rpte->{'membertype'} eq 'v' && ! $ADMIN)
{
	###### since this is a VIP we are reporting on we will do the following
	# make sure the user is logged in
	# make sure the user, if not the reportee, is upline
	( $USER_ID, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if ($USER_ID =~ /\D/)
	{
		Err("A valid <a href=\"$LOGIN_URL\">login</a> is required to view Partner reports.");
		Exit();
	}

	###### make sure that the reportee is our downline... and not an Exec
	if ($USER_ID != $rpte->{'id'})
	{
		if (! $db->selectrow_array("
				SELECT id FROM relationships WHERE id= ? AND upline= ?", undef, $rpte->{'id'}, $USER_ID)
		)
		{
			Err( $gs->ParseString($lang_blocks->{'not_in_dn'}, $rid) );
			Exit();
		}

		# get rid of the second conditional to simply block upline access to even a frontline Exec's report
		# if you do, you may as well, comment out the 'my_exec' portion of the SQL where it is called
		if ($rpte->{'a_level'} >= $EXEC_BASE && $rpte->{'my_exec'} != $USER_ID)
		{
			Err($lang_blocks->{'no_see_exec'});
			Exit();
		}
	}
}

Output();

Exit();


###### ###### Start of Sub Routines ###### ######

sub AdminCK
{
#	return 0;	# uncomment to mimic ordinary operation even by Admin

	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return 0 unless $operator && $pwd;

	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	$db = ADMIN_DB::DB_Connect( 'pireport.cgi', $operator, $pwd ) ||
#	$db = ADMIN_DB::DB_Connect( 'generic', $operator, $pwd ) ||
		"Failed to connect to the database with staff login";
	return $operator;
}

=h3 Brackets

	Let's say our partner has enough prvs to qualify for pay level 9 (ED),
	but only enough pt to qualify as a pay level 6 (SM)...
	The next pay level they are shooting for is 7 (GM)
	This will return a hashref like this:
	{
		'prvs' => {
			'good' => 9
		},
		'pt' => {
			'behind' => 6,
			'next' => 7
		},....
	}
	
=cut

sub Brackets
{
	my $stats = shift;
	my %rv = ();
	my $next_level = $stats->{'a_level'} + 1;
	$next_level = 2 if $next_level < 2;	# 2 is our lowest level
	my @action = ();
	
	# first determine which values we have met or exceeded for our next level
	# and create a list of cols for which we have not
	foreach my $col (@cols)
	{
		if ($stats->{$col} >= $level_values->{$next_level}->{$cols2lvlvals{$col}})
		{
			foreach my $pl (sort {$a<=>$b} keys %{$level_values})
			{
				# this sets the column_name->good key to the pay_level value we match for this column
				$rv{$col}->{'good'} = $pl if $stats->{$col} >= $level_values->{$pl}->{$cols2lvlvals{$col}};
			}
		}
		else
		{
			push @action, $col;
		}
	}
	
	# now loop through the pay levels from bottom to top and identify where we are
	# and where we need to be for the next pay level
	foreach my $pl (sort {$a<=>$b} keys %{$level_values})
	{
		$next_level = $pl + 1;
		$next_level = $pl unless $level_values->{$next_level};	# we cannot evaluate values beyond the data we have
		foreach my $col (@action)
		{
			# this sets the column_name->behind key to the pay_level value we match for this column
			$rv{$col}->{'behind'} = $pl if $stats->{$col} >= $level_values->{$pl}->{$cols2lvlvals{$col}};
			
			# this sets the column_name->next key to the pay_level value we match for this column
			# once we set this key on the way up, we do not reset each time through like setting a high water mark
			$rv{$col}->{'next'} = $next_level if
				$stats->{$col} < $level_values->{$next_level}->{$cols2lvlvals{$col}} &&
				$stats->{'a_level'} < $next_level &&
				! $rv{$col}->{'next'};
		}
	}
	
	# only 2 columns need to have a pay_level higher than Exec for the display to work correctly
	# let's limit the others to 11
	foreach my $col (@cols)
	{
#		warn "$col";
		next if grep $_ eq $col, (qw/prvs frontline_ppp/);
#		warn "$col";
		foreach my $k (keys %{$rv{$col}})
		{
			$rv{$col}->{$k} = 11 if $rv{$col}->{$k} > 11;
		}
	}
	
	# for Execs, we need to build a bracket flag for the number of Stars
	if ($stats->{'a_level'} >= $EXEC_BASE)
	{
#		warn "stars: $stats->{'stars'}";
		if ($stats->{'stars'} > $level_values->{$stats->{'a_level'}}->{'execs_req'})
		{
			$rv{'execs'}->{'good'} = $stats->{'a_level'};
		}
		else
		{
			foreach my $pl (sort {$a<=>$b} keys %{$level_values})
			{
				$next_level = $pl + 1;
				$next_level = $pl unless $level_values->{$next_level};	# we cannot evaluate values beyond the data we have
				# this sets the column_name->behind key to the pay_level value we match for this column
				$rv{'execs'}->{'behind'} = $pl if $stats->{'stars'} >= $level_values->{$pl}->{'execs_req'};
					
				# this sets the column_name->next key to the pay_level value we match for this column
				# once we set this key on the way up, we do not reset each time through like setting a high water mark
				$rv{'execs'}->{'next'} = $next_level if
					$stats->{'stars'} < $level_values->{$next_level}->{'execs_req'} &&
					$stats->{'a_level'} < $next_level &&
					! $rv{'execs'}->{'next'};
			}
		}
	}
	return \%rv;
}

sub cboCommissionMonths
{
	my %keys = (''=>''); # put a value in to have the first line of the combo box have a label
	my @vals = '';
	my $sth = $db->prepare("
		SELECT DISTINCT rd.commission_month
		FROM archives.refcomp_me_dates rd
		JOIN archives.refcomp_me_batches rb
			ON rb.run_date=rd.run_date
		WHERE rd.commission_month < first_of_month()
		AND rb.id= $rpte->{'id'}
		ORDER BY rd.commission_month DESC
	");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_array)
	{
		push @vals, $tmp;
		$keys{$tmp} = $tmp;
	}
	return $q->popup_menu(
		'-class' => 'cbocomm',
		'-name' => 'month',
		'-values' => \@vals,
		'-labels' => \%keys,
		'-default' => ''
	);
}

sub EmitXML
{
	print "Content-type: text/plain\n\n$_[0]\n";
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'<h4>There has been a problem</h4>',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Get_Mbr_Stats
{
	my $cols = ''; # the default
	my $table = 'refcomp rb'; # the default
	my $where = "WHERE rb.id = $rpte->{'id'}";	# the default
	if ($params{'action'} eq 'archive' && $params{'month'} =~ m/\d{4}-\d{2}-\d{2}/)
	{
		$cols = ',rb.paid,rb.run_date';	# we need to know if this commission month has been paid on
		$table = 'archives.refcomp_me rb';
		$where .= ' AND rb.commission_month= ' . $db->quote($params{'month'});
		$where .= ' ORDER BY run_date DESC LIMIT 1';	# once we have more than one run, we want the most recent
	}

	my $rv = $db->selectrow_hashref("
		SELECT
--			rb.prvs,
--			rb.frontline_ppp,
			rb.plpp AS frontline_ppp,	-- for now let's avoid having to heavily revise the templates and coding
			rb.op,
			od.onekplus(rb.op) AS op_txt,
--			rb.opp,
			od.thirtykplus(rb.opp) AS opp_txt,
--			rb.ppp,
			rb.plpp AS qpp,
--			rb.sales_req_count,
			rb.my50 AS my50,
			rb.my25 AS my25,
			rb.dividends  AS dividends,
			rb.referral_commissions AS referral_commissions,
			rb.minimum_commission_guarantee AS minimum_commission_guarantee,
			rb.a_level,
			rb.analysis
			$cols
		FROM $table
		$where");
#warn Dumper($rv);	
	# if the month we are looking at has been paid, then we will look up the exchange rate for that date
	# this way future visits to the same data will give the same results
	# this may not be the case if we were to use today's exchange rate every time
	if ($rv->{'paid'})
	{
		# this will obtain the most recent exchange preceeding the exact date if there is nothing for the exact date
		($rpte->{'rate'}) = $db->selectrow_array(qq|
			SELECT "rate"
			FROM exchange_rates
			WHERE "date"::DATE <= '$rv->{'run_date'}'
			AND "code"= '$rpte->{'currency_code'}'
			ORDER BY "date" DESC
			limit 1|) || die "Failed to obtain an exchange rate for $rpte->{'currency_code'} ON $rv->{'run_date'}";
	}

	# in order to have uniform rounding, we have to round our individual values
	# as they will all be added together in the template and any non-rounded fractions
	# ultimately end up causing us to be over by 1 or 2 in our total
	foreach (qw/my50 my25 dividends referral_commissions minimum_commission_guarantee/)
	{
		$rv->{$_} *= $rpte->{'rate'};
		$rv->{$_} = sprintf "\%\.$rpte->{'currency_exponent'}f", $rv->{$_};
	}
	
	# we do not have a specific column designating the number of stars an Exec has
	# so we will count the nodes in the analysis
	my @s = $rv->{'analysis'} =~ m/<exec>/g;
	$rv->{'stars'} = (scalar @s) || 0;
	return $rv;
}

sub LoadLangBlocks
{
	my $months = $gs->Get_Object($db, $TMPL{'months'}) || die "Failed to retrieve the months template\n";
	$months = XML::LibXML::Simple::XMLin($months);
	$months = $months->{'months'}->{'month'};

	my @d = split(/-/, $db->selectrow_array('SELECT NOW()::DATE'));

	my $tmp = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to retrieve language pack template\n";
	$tmp = decode_utf8($tmp);
	
	my $comm_month = $months->{ $Months{$d[1]} }->{'content'}; # the default
	# we want to omit the month because we will be doing something else within the template
	$comm_month = '' if $params{'action'} eq 'archive' && $params{'month'};
	
	$TT->process(
		\$tmp,
		{'rpt_month'=>$comm_month, 'Links'=>\%Links},
		\$lang_blocks
	);

	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

sub LoadParams
{
	foreach (qw/action month rid/)
	{
		$params{$_} = $q->param($_) || '';
	}
}
sub Output
{
	my $stats = Get_Mbr_Stats();
#	warn Dumper(Brackets($stats));
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}) ||	die "Failed to retrieve main template\n";
	my $tmpl = ();
	my $averages = $db->selectall_hashref('SELECT * FROM refcomp_averages', 'pay_level');
	
# for now, we want to disable certain page elements, so we will set an arbitrary action 
#	$params{'action'}='x' unless $params{'action'};
	my $rv = $TT->process(\$tmp,
		{
			'averages'=>$averages,
			'member'=>$rpte,
			'lang_blocks'=>$lang_blocks,
			'stats'=>$stats,
			'cboCommissionMonths' => cboCommissionMonths(),
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo,
			'level_values' => $level_values,
			'brackets'=>Brackets($stats)
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_blocks->{'title'},
#			'style'=> $q->style({'-type'=>'text/css'}, '.cbocomm {font-size:100%;} a {color:#2A85E8; text-decoration:none;}'),
			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/greybox-jquery.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$tmpl
		});	
}

###### 01/04/12 besides retaining the basic routines, this report is really a transformation to a simple TT template rendering for now
###### Jan. 2012 vast modifications
###### 03/02/12 added the check for a Partner trying to look at an Exec's report
###### 03/06/12 finished the above allowing a first level deep lookup
###### 03/08/12 tweaked out Admin mode
###### 04/03/12 added ordering to the results in cboCommissionMonths
###### 04/11/12 revisions to the qualifiers including the replacement of frontline_pp with qpp
###### 05/13/13 just a simple tweak to allow staff to look at archived reports for now-non-Partners
