#!/usr/bin/perl -w
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
require PDF::API2::Simple;

my $cgi = new CGI;
    my $pdf = PDF::API2::Simple->new( 
                                   #  file => 'output.pdf'
                                    );

    $pdf->add_font('Helvetica');
        $pdf->add_font('Helvetica-Bold');
    $pdf->add_font('Verdana');
        $pdf->add_font('Arial');
    $pdf->add_page();

#    $pdf->link( 'http://search.cpan.org', 'A Hyperlink',
#                x => 350,
#                y => $pdf->height - 150 );
$pdf->set_font('Helvetica-Bold',36);
$pdf->text('List date: ' . scalar localtime());
$pdf->set_font('Arial',12);
$pdf->text("", 'autoflow' => 'on');
$pdf->text("",autoflow => 'on');
my $text = " - All work and no play makes Jack a dull boy ";
$text .= ' 1234567890 1234567890 1234567890 1234567890 1234567890';
my $x = 10;
    for (my $i = 0; $i < 250; $i++) {

         $pdf->text("$i $text", x=>$x, autoflow => 'on');
         $x++;
    }

#    $pdf->save();

print $cgi->header('application/pdf');
print $pdf->stringify;