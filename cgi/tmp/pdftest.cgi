#!/usr/bin/perl -w
use strict;
use CGI;
use PDF::API2;
use CGI::Carp qw(fatalsToBrowser);

my $cgi = new CGI;

# Create a blank PDF file
my $pdf = PDF::API2->new();

    # Add a blank page
my $page = $pdf->page();

    # Set the page size
#    $page->mediabox('Letter');

    # Add a built-in font to the PDF
my $font = $pdf->corefont('Helvetica-Bold');

    # Add an external TTF font to the PDF
#    $font = $pdf->ttfont('/path/to/font.ttf');

    # Add some text to the page
my $text = $page->text();
$text->font($font, 20);
    $text->translate(200, 700);
    $text->text('Hello World!');
my $y=20;
for (my $x = 680; $x > -200; $x-=20)
{
    $text->translate($y, $x);
    $text->text('Hello World! 12345678901234567890123456789012345678901234567890',autoflow => 'on');
    $y--;
}
    # Save the PDF
#    $pdf->saveas('/path/to/new.pdf');

print $cgi->header('application/pdf');
print $pdf->stringify;