#!/usr/bin/perl -w
###### funding.cgi
######
###### A script that allows the VIPs to purchase pre-pay funding
###### (soa table) which will be used later to pay for other VIPs' fees, etc. 
######
###### created: 11/04/05	Karl Kohrt
######
###### modified: 06/08/15	Bill MacArthur
	
use strict;
use 5.010;
use feature 'state';
use Business::CreditCard;
use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use MIME::Lite;
require XML::Simple;
use lib ('/home/httpd/cgi-lib');
require ADMIN_DB;
require GI_ccauth;
use G_S qw ($LOGIN_URL);
use DB_Connect;
require XML::local_utils;
require CC_Fraud_Ck;
use State_List;

#use Data::Dumper;	# only for debugging
binmode STDOUT, ":encoding(utf8)";

# this environment needs to be set to avoid openssl errors in the log
$ENV{'HOME'} = '/tmp';

###### CONSTANTS
my $SOA_VW = 'soa_public_ca_vw';
my $CC_MIN = 10;			# Minimum amount for a credit card deposit.
my $AP_FUNDING_MIN = 10;		# Minimum Alert Pay & EcoPay funding amount
our $PAYOUT_MIN = 25;			# Minimum amount that we will pay out.
our $FIRST_MENU_YEAR = 2007;
my $MAX_MENU_YEAR = (localtime)[5] + 1900;	
our @numerics = qw/id quantity price order_type cc_cvv2 cc_exp/;
our $TRACKING = 1;			# Change to 0 for no logging.
our $PATH = "/var/log";	# Where the logs are kept.
our $ACCOUNTING_ADDRESS = 'acfspvr@dhs-club.com';# 'webmaster@clubshop.com';#acfspvr@dhs-club.com';
our @fieldnames = qw/id
			amount
			acct_holder
			addr
			city
			state
			postalcode
			country
			cc_number
			cc_cvv2
			cc_exp
			/;
our $soa = "soa";
my $q = new CGI;
$q->charset('utf-8');
our $cgiini = "funding.cgi";
our $operator = $cgiini;
our $ME = $q->script_name;
our $xs = new XML::Simple('noAttr'=>1);
our %TMPL = (
	'cc_form'		=> 10495,	# The credit card funding form xsl.
	'ep_form'		=> 10669,	# The EcoPay funding form xsl.
	'ap_form'		=> 10656,	# Alert Pay funding form XSL
	'ap2_form'		=> 10658,	# Alert Pay confirmation (with fee)
	'pp1_form'		=> 10576,	# the initial paypal form xsl (almost identical to the MB version)
	'pp2_form'		=> 10577,	# the confirmation paypal form (where we embed our encrypted data)
	'content'		=> 10496,	# The general content xml.
	'countries'		=> 10407,	# The countries list xml.
	'trans'			=> 'projects/funding.cgi/index.xsl', #10500,	# The transactions report xsl.... 10163 is a temp template
	'payout_step1'	=> 'projects/funding.cgi/withdrawal-1.xsl', #10672
	'payout_step2'	=> 'projects/funding.cgi/withdrawal-2.xsl',#10673
	'months.xml'	=> 10572,
	'pay_agent_notice'	=> 11107
);

my %EcoPay = (
	'Password' => 'wd3mHRtk7CF1',
	'Merchantaccountnumber' => '106884',
	'PaymentpageID' => '1059'
);

# these codes should reflect the manual methods
# the codes themselves can be found in public.soa_transaction_types
# the "need_account" key will trigger the generation of an input element
# for entering one's account email address
# it will also enable code checking to ensure that a value is received
my %PayoutMethods = (
	9300	=> {
		'label' => 'PayPal',
		'need_account' => 1
	},
	9320	=> {
		'label' => 'Pay Agent',
		'need_account' => 0
	},
	9410	=> {
		'label' =>	'Paper Check'
	},
	9442	=> {
		'label' =>	'Payza',
		'need_account'=>1
	},
	9447	=> {
		'label' =>	'EcoCard',
		'need_account'=>1			# actually we need an account number instead of an email address
	}
);
# this makes it easier than commenting out the keys above
# active ones here will also trigger the display for this item in the interface
# if the item is already coded in the XSL
my @ActivePaymentMethods = (9300, 9410, 9320);

###### GLOBALS
my ($db, @BalancesCache, $member_id, $mem_info) = ();
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $access_type = '';
my $login_error = 0;

# whatever is placed in $messages should be valid XHTML as it will be merged into the XML for processing
our $messages = '';
our $action = '';
our $form = $q->param('form') || '';

# fix it so they cannot request the Money Booker form
$form = 'ap' if $form eq 'mb';
our $xsl = $form ? ($form . "_form") : 'trans';			# The default deposit page.
our $xml = '';
our $menus_xml = '';
our $params_xml = '';
our $pricing_xml = '';
our $content_xml = '';
our $data_xml = '';
our $params_hash= {};
our $price_info = {};

################################
###### MAIN starts here.
################################

# Get the info for a logged in member.
Get_Cookie();

$action = $q->param('action') || '';

# payout actions are not allowed if they owe fees, that is they are nopay
if ((grep $action eq $_, (qw/payout confirm disburse/)) && UnpaidCk())
{
	$action = '' ;
	# I don't really like the way Karl handled messages in here, but I don't want to bother redoing it... Bill
	$messages .= qq#<div class="bad" align="center">
		<xsl:value-of select="//base/lang_blocks/messages/nopay_no_disburse" />
		</div>#;
}

# If they are logged in as a VIP or an Admin.
if ( $access_type eq 'auth' || $access_type eq 'admin' )
{
	$action ||= 'report';
	$params_hash->{'script_name'} = $q->script_name;

	# If it's an order, we'll attempt to process it.
	if ( $action eq 'order' )
	{
		# Load the params into a local hash.
		Load_Params();

		# Unless there are errors in the order, we'll try to place the order.
		unless ( Check_Params() )
		{
			my $ccc = CC_Fraud_Ck->new({'db'=>$db});
			if (! $ccc->Check_Member_Authorized($mem_info))	# this method returns the ID if there is a match, indicating okay
			{
				$ccc = $ccc->Check_Country_And_Ensure_Card_Authorized({	# this method returns undef if okay -or- a textual string as an error message
					'country_code'=>$mem_info->{'country'},
			#		'ipaddr'=>$q->remote_addr,
					'ccnum'=>$params_hash->{'cc_number'}
				});

				if ($ccc)
				{
					Err($ccc);
				}
			}
			
			# Place an order for the requested funds.
			my ($success, $reject_info) = '';
			($success, $reject_info) = CC_Process_Order($params_hash);
			
			if ( $success )
			{
				# Place the funds in the table.
				if ( my $trans_num = Insert_Funds() )
				{
					$messages = qq#<span class="good">
							<xsl:value-of select="//base/lang_blocks/messages/success1" />
							<xsl:text> $trans_num.</xsl:text>
							<br/><xsl:text> \$$params_hash->{'amount'} </xsl:text>
							<xsl:value-of select="//base/lang_blocks/messages/success2" />
							<br/><xsl:value-of select="//base/lang_blocks/messages/success3" />
							</span>#;
					# Clear the amount field to deter double ordering.
					$params_hash->{'amount'} = '';

					# reload the transaction list
					$data_xml = Get_Transactions() . GenerateBalancesXML( Get_Balances() );
				}
				else
				{
					$messages = qq#<span class="bad">
							<xsl:value-of select="//base/lang_blocks/messages/fail_insert1" />
							<br/><xsl:value-of select="//base/lang_blocks/messages/fail_insert2" />
							</span>#;
					$data_xml = GenerateBalancesXML( Get_Balances() );
					###### the default for this path is trans, but since we weren't successful
					###### we can bring back the CC submittal form
					$xsl = 'cc_form';

					# Send accounting an email to let them know their was a problem.
					my $email_message = qq!This credit card charge was completed, but was not credited to the member's DHS Club Pre-Pay account due to a database failure. Please credit it to the member's account via the admin interface as soon as possible:\n
					DHS ID:	$mem_info->{'alias'}
					Name:		$mem_info->{'firstname'} $mem_info->{'lastname'}
					Amount:	\$$params_hash->{'amount'}!;
					Email_Accounting($email_message);
				}
			}
			else
			{
				$messages = qq#<span class="bad">
						<xsl:value-of select="//base/lang_blocks/messages/fail_charge" />
						<br/><xsl:text>$reject_info </xsl:text>
						</span>#;
				$data_xml = GenerateBalancesXML( Get_Balances() );
				###### the default for this path is trans, but since we weren't successful
				###### we can bring back the CC submittal form
				$xsl = 'cc_form';
			}
		}
		else { $xsl = 'cc_form'; }
	}
	# If this is a transaction report request.
	elsif ( $action eq 'report' )
	{
		#	$xsl ||= "trans";	# Karl originally wrote this as a simple funding form, the other stuff came in later
		# so the branching logic is less than optimal
		# Get the member's transactions for the specified timeframe.
		Load_Params('id');
		$data_xml = Get_Transactions() . GenerateBalancesXML( Get_Balances() );
	}
	# If this is a payout request.
	elsif ( $action eq 'payout' )
	{
		# Assemble the member's data and get the payout request form.
		$data_xml .= GenerateBalancesXML( Get_Balances() ) . $params_xml;
		LoadPayAgentData();
		
		$xsl = "payout_step1";
	}
	# this confirm is to present disbursement figures for confirmation
	elsif ($action eq 'confirm')
	{
		# Get the member's payout figures and the request form.
		my ($balance, $transferable_balance, $withdrawable_balance) = Get_Balances();
		Load_Params('payhow', 'amount', 'pay_agent_selected');
		
		$params_hash->{'pay_agent_selected_label'} = AgentLabel($params_hash->{'pay_agent_selected'}) if $params_hash->{'pay_agent_selected'};
		
		# Assemble the member's data and get the payout request form.
		$data_xml =
			GenerateBalancesXML($balance, $transferable_balance, $withdrawable_balance)
			. "<payment_by>$PayoutMethods{$params_hash->{'payhow'}}->{'label'}</payment_by>";

		unless (Check_Payhow())
		{
			$xsl = 'payout_step1';
			LoadPayAgentData();
		}
#		elsif (Check_Payout_Params($transferable_balance))
		elsif (Check_Payout_Params($withdrawable_balance))	# changed 05/04/15
		{
			# calculate our fee
			$data_xml .= GenerateConfirmData();
			$xsl = 'payout_step2';
		}
		else
		{
			$xsl = 'payout_step1';
			LoadPayAgentData();
		}
	}
	# once they have confirmed the disbursement figures, we... disburse :)
	elsif ($action eq 'disburse')
	{
#		my ($tmp_balance, $non_cash_balance, $cash_balance, $pending_balance) = Get_Balances();
		Load_Params('payhow', 'amount', 'account_email', 'pay_agent_selected');
		
		$data_xml =
			GenerateBalancesXML( Get_Balances() )
			. "<payment_by>$PayoutMethods{$params_hash->{'payhow'}}->{'label'}</payment_by>";
	
		my ($request_amount) = $db->selectrow_array('SELECT allowable_request_from_soa(?, ?, ?)',
				undef, $mem_info->{'id'}, $params_hash->{'payhow'}, $params_hash->{'amount'});

		unless ( Check_Payout_Params($request_amount))
		{
			$xsl = 'payout_step1';
			LoadPayAgentData();
		}
		elsif ( ! Check_Payout_Account() )
		{
			$data_xml .= GenerateConfirmData();
			$xsl = 'payout_step2';
		}
		elsif ( Create_Payout($params_hash->{'payhow'}, $params_hash->{'amount'}) )
		{
			# Send accounting an email to let them know about the request.
			my $email_message = qq!This member has requested the following disbursement from their Club Account:\n
DHS ID:\t\t$mem_info->{'alias'}
Name:\t\t$mem_info->{'firstname'} $mem_info->{'lastname'}
Amount:\t\t\$$params_hash->{'amount'}

Pay Method:\t$PayoutMethods{$params_hash->{'payhow'}}->{'label'}!;

			if ( $PayoutMethods{$params_hash->{'payhow'}}->{'need_account'} ) 
			{
				$email_message .= qq!\nAccount:\t$params_hash->{'account_email'}!;
			}
			elsif ($params_hash->{'payhow'} == 9320)
			{
				$email_message .= "\nPay Agent Selected: " . AgentLabel($params_hash->{'pay_agent_selected'});
				$email_message .= "\nThe transaction has already been committed. Further action is unnecessary.\n";
			}
			
			if ($params_hash->{'payhow'} == 9320)
			{
				EmailPayAgent();
			}
			else
			{
				Email_Accounting($email_message);
			}

			# rather than reloading the disbursment like we previously did
			# we'll just redirect them back to the main interface
			print $q->redirect($q->url . '#list-bottom');
			End();
		}
		else
		{
			# if we failed to create payout, that routine creates an error message that will be used
		}
	}
	# Otherwise, get some data to pre-populate the form.
	else
	{
		# Get the default user info and load it into the params xml block.
#		Get_Member_Details();
#		$mem_info->{balance} = Get_Balances() || 0;
		die "Invalid action: $action\n";
	}

	# Get the form content.
	$content_xml = $gs->GetObject($TMPL{'content'}) || die "Failed to load XML template: $TMPL{'content'}\n";

	# Add the leading D to the cc_exp field(for xml menu building).
	$params_hash->{'cc_exp'} = "D" . $params_hash->{'cc_exp'} if $params_hash->{'cc_exp'};

	# Add a unique number to the member's ID so that moneybookers.com has a unique transaction ID each time.
	$params_hash->{'transaction_id'} = $params_hash->{'id'} . "-" . time() if $xsl eq "mb_form";

	# Put the params into an xml format.
	$params_xml = $xs->XMLout($params_hash, 'RootName'=>'params');

	# Build out the menus.
	$menus_xml = "<menus>" . Build_Menus() . "</menus>";
	
	# Now build out the page.
	Display_Page($xsl);
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	Err(qq#Please log in <a href="$LOGIN_URL?destination=$ME">HERE</a> to use this form.#);
}

End();

################################
###### Subroutines start here.
################################

sub AgentLabel
{
	my $id = shift;
	return '' unless $id;
	state $agent = $db->selectrow_array("
		SELECT m.firstname||' '||m.lastname||' - '||m.alias AS label
		FROM members m
		JOIN pay_agent.agents pa
			ON m.id=pa.id
		WHERE pa.id= $id");
	return $agent;
}

sub AvailablePayAgents
{
	my $d = $db->selectall_arrayref("
		SELECT m.id, m.firstname||' '||m.lastname||' - '||m.alias AS label
		FROM members m
		JOIN pay_agent.agents_by_country pa
			ON m.id=pa.id
		WHERE pa.country_code= ?", undef, $mem_info->{'country'});
	return undef unless $d;

	my %rv = map { $_->[0] => $_->[1] } @{$d};

	return \%rv;
}

sub Build_Menu_Available_PayAgents
{
	my $agents = shift;
	$agents = AvailablePayAgents() if ! $agents;
	return '' unless $agents;
	return $q->popup_menu(
		'-name'		=> 'pay_agent_selected',
		'-values'	=> [keys %{$agents}],
		'-labels'	=> $agents
	);
}

###### Build and combine the different menus.
sub Build_Menus
{
	my $menus = '';

	# Load the country menu into an xml block.
	my $countries = $gs->GetObject($TMPL{'countries'}) || die "Failed to load XML template: $TMPL{'countries'}\n";
	$menus .= $countries;

	# Load the state menu into an xml block based upon the country in the member record.
	$menus .= Build_Menu_States();

	# Add the Credit Card expiration dates menu.
	$menus .= Build_Menu_CCExp();

	# Add the months list menu.
	$menus .= Build_Menu_Months();

	# Add the years list menu.
	$menus .= Build_Menu_Years();
	return $menus;
}

###### Build an xml version of a menu of credit card expiration dates.
sub Build_Menu_CCExp
{
	my @cc_exp_order = ();
	my $cc_exp = ();
	my $shortyear = '';
	my ( $month, $year ) = (localtime)[ 4, 5 ];
	$month += 1; 
	$year += 1900;
	my $maxyear = $year + 10;	# 10 years of expiration dates.

	while ( $year <= $maxyear )
	{
		($shortyear = $year) =~ s/^20//;
		while ( $month <= 12 )
		{
			$month = ( length($month) == 1 ) ? "0" . $month : $month;
			# Add the 'D' because xml needs an alpha for the tag.
			my $key = 'D' . $month . $shortyear;
			$cc_exp->{$key} = $month . "/" . $year;
			# Build an array for ordering the menu.
			push @cc_exp_order, $key;
			$month += 1;
		}
		# It's a new year, so start with month 1 - January.
		$month = 1;
		$year += 1;
	}
	# Build out the xml structure.
	my $xml = '';
	foreach ( @cc_exp_order )
	{
		$xml .= "<$_>$cc_exp->{$_}</$_>\n";
	}
	$xml = "<cc_exp>$xml</cc_exp>";
	return $xml;
}

sub Build_Menu_Months
{
#	(my $xml = $content_xml) =~ s/^(.*?)months>|<\/months(.*?)$//gs;
	my $xml = $gs->GetObject($TMPL{'months.xml'}) || die "Failed to load months.xml";
	$xml =~ s/^(.*?)months>|<\/months(.*?)$//gs;
	return "<months>$xml</months>";
#	return $xml;
}

###### Build a drop-down menu of the states.
sub Build_Menu_States
{
	my %S = %State_List::STATE_CODES;
#       my @SORTED_KEYS = sort ( keys %S );
	my $xml = '';
	my $tag = '';
	foreach ( sort keys %S ) { $tag = lc $S{$_}; $xml .= "<$tag>$_</$tag>\n"; }
	$xml = "<state>$xml</state>";
}

sub Build_Menu_Years
{
	my $year = $FIRST_MENU_YEAR;
	my $xml = '';
	while ( $year <= $MAX_MENU_YEAR )
	{
		$xml .= qq#<year value="$year">$year</year>\n#;
		$year++;
	}
	# Finish up the xml structure.
	return "<years>$xml</years>";
}


###### Process a credit card charge.
sub CC_Process_Order
{
	my $order = shift;

	# this routine returns either 1 for success, 0 for failures that handle their own errors,
	# or an actual failed authorization error message

	my $cc_order_desc = "Pre-Pay Funding";
	
	my %DATA = ();
	$DATA{'order_description'} = $cc_order_desc;
	$DATA{'id'} = $order->{'id'};
	$DATA{'amount'} = $order->{'amount'};
	$DATA{'invoice_number'} = "CA-$order->{'alias'}";
	$DATA{'account_holder_firstname'} = $cc_order_desc;
	$DATA{'account_holder_lastname'} = $order->{'acct_holder'};
	$DATA{'account_holder_address'} = $order->{'addr'};
	$DATA{'account_holder_city'} = $order->{'city'};
	$order->{'state'} = $order->{'province'} if $order->{'country'} ne 'us';
	$DATA{'account_holder_state'} = $order->{'state'};
	$DATA{'account_holder_zip'} = $order->{'postalcode'};
	$DATA{'account_holder_country'} = $order->{'country'};
	$DATA{'payment_method'} = 'CC';
	$DATA{'cc_number'} = $order->{'cc_number'};
	$DATA{'cc_expiration'} = $order->{'cc_exp'};
	$DATA{'card_code'} = $order->{'cc_cvv2'};
	$DATA{'phone'} = '';
	$DATA{'phone'} .= $mem_info->{'phone'} if $mem_info->{'phone'};
	$DATA{'emailaddress'} = $mem_info->{'emailaddress'};
	$DATA{'ipaddr'} = $q->remote_addr;
	
	my ( $success, $auth_error ) = '';
	( $success, $auth_error ) = CCAuth::Authorize(\%DATA, {'member'=>$mem_info});
# Test values below. Comment out the line above when you don't want to run the card.
#	$success = 1;
#	$auth_error = 'This is a test.';

	if ( $success != 1 )
	{
		# Get rid of embedded html and leading & trailing spaces.
		$auth_error =~ s/<.*?>//g;
		$auth_error =~ s/^\s*|\s*$//g;
	}

	# Log the authorization in case we have problems later.
	my $log_err = $auth_error || "none";

	# Open the log files if logging is turned on above.
	open (TRACK_LOG, ">>$PATH/funding_tracking.log") || die "Failed to open funding_tracking.log\n" if $TRACKING;
	print TRACK_LOG scalar(localtime) . " - ";
	print TRACK_LOG "ID#$mem_info->{'id'}; amount=$order->{'amount'}; error= $log_err\n";
	close TRACK_LOG;
	return $success, $auth_error;
}

sub Check_Payout_Account
{
	# do we need an account identifier?
	# then it will be either an email address or a number
	if ( $PayoutMethods{$params_hash->{'payhow'}}->{'need_account'} ){
		if(! $params_hash->{'account_email'} ||
			($params_hash->{'payhow'} eq 9447 && $params_hash->{'account_email'} =~ m/\D/) ||
			($params_hash->{'payhow'} ne 9447 && ! $gs->Check_Email($params_hash->{'account_email'})) )
		{
			$messages .= qq#<span class="bad">
						<xsl:value-of select="//base/lang_blocks/messages/bad_account" />
						</span><br/>#;
			return 0;
		}
	}
	return 1;
}

###### Check the parameters for errors.
sub Check_Params
{
	# Clear the fields of the standard garbage.
	foreach my $key (keys %{$params_hash})
	{
		$params_hash->{$key} = $gs->PreProcess_Input($params_hash->{$key});
		if ( (grep { /$key/ } @numerics) && ($params_hash->{$key} =~ m/\D/) )
		{
			$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_$key" /><br/>#;
		}
	}
	# A credit card payment requires all fields except not both province and state.
	# Check required fields as necessary.
	foreach ( @fieldnames )
	{
		# All but the state and province fields must have info. The price field is hidden.
		if ( ( ! defined $params_hash->{$_}
			|| $params_hash->{$_} eq ''
			|| $params_hash->{$_} eq 'blank')
			&& $_ ne 'province'
			&& $_ ne 'state'
			&& $_ ne 'price')
		{
			$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/empty_field" />
						<xsl:text>: </xsl:text>
						<xsl:value-of select="//base/lang_blocks/$_" /><br/>#;
		}
	}
	# If the country is blank we will assume it's US.
	if ( $params_hash->{'country'} eq '' )
	{
		$params_hash->{'country'} = "us";
	}
	# If the amount is less than the minimum.
	if ( $params_hash->{'amount'} < $CC_MIN )
	{
		$messages .= qq#<xsl:value-of select="//base/lang_blocks/cc_min" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="//base/lang_blocks/messages/dep_req" /><br/>#;
	}

	#We have issues where people submit "20,00", and they are then charged 2000, and nothing is credited to their club account.  
	#Even though there is front end validation they seem to slip by, so we put this here.  If there is any non numeric,
	#data in the amount we throw the format error.  Our CC processing company strips out all characters that arent supported 
	#by the double data type.
	if ($params_hash->{'amount'} =~ /\D/ && $params_hash->{'amount'} !~ /^\d+\.\d{2}$/)
	{
		$messages .= qq(<xsl:value-of select="//base/lang_blocks/messages/bad_amount" />
				<xsl:text> </xsl:text>);
	}
	
	# Perform various province & state checks.
	# Check if both province & state are empty.
	if ( $params_hash->{'province'} eq '' && $params_hash->{'state'} eq '' )
	{
		# If the country is NON - US, the province field must be filled in.
		if ( $params_hash->{'country'} ne 'us' )
		{
			$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_province" /><br/>#;
		}
		# If the country is US, the state field must be filled in.
		elsif ( $params_hash->{'country'} eq 'us' && $params_hash->{'state'} eq '')
		{
			$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_state" /><br/>#;
		}
	}
	# Check if both province and state have been filled out.
	elsif ( $params_hash->{'province'} && $params_hash->{'state'} )
	{
		$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/both_prov_state" />#;
	}
	# Check if the country is NON - US, the province field must be filled in.
	elsif ( $params_hash->{'country'} ne 'us' && $params_hash->{'province'} eq '')
	{
		$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_province" /><br/>#;
	}
	# Check if the country is US, the state field must be filled in.
	elsif ( $params_hash->{'country'} eq 'us' && $params_hash->{'state'} eq '')
	{
		$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_state" /><br/>#;
	}
	# Check for a valid credit card number.
	unless ( Business::CreditCard::validate($params_hash->{'cc_number'}) && $params_hash->{'cc_number'} !~ m/\D/ )
	{
		$messages .= qq#<xsl:value-of select="//base/lang_blocks/messages/bad_cc_number" /><br/>#;
	}

	# If there are any error messages, make them look bad;-)
	$messages = qq#<span class="bad">$messages</span># if $messages;
	return $messages;
}

sub Check_Payhow
{
	unless ($params_hash->{'payhow'})
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/cashout_a" />
					</span><br/>#;
		return 0;
	}
	return 1;
}

###### Check the payout parameters for errors.
sub Check_Payout_Params
{
	my $avail_amount = shift;

	# Check for currency format.
	if ( $params_hash->{'amount'} !~ m#^\d+\.?\d*$# )
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/messages/bad_amount" />
					</span><br/>#;
	}
	
	# Check that the amount requested is available.
	if ( $params_hash->{'amount'} > $avail_amount )
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/messages/high_amount" />
					</span><br/>#;
	}
	
	# Check that the amount requested is above our minimum.
	if ( $params_hash->{'amount'} < $PAYOUT_MIN )
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/messages/low_amount" />
					</span><br/>#;
	}
	
	# if they have selected pay agent, then they must have submitted a pay_agent_selected param
	if ( $params_hash->{'payhow'} == 9320 && ! $params_hash->{'pay_agent_selected'})
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/messages/must_choose_agent" />
					</span><br/>#;
	}
	return $messages ? 0 : 1;
}

###### Try to create a payout transaction.
sub Create_Payout
{
	my ($payhow, $amount) = @_;
	
	my $trans_num = ();
	
	# if we are doing a Pay Agent payout, then we will need to create all the related Pay Agent records
	if ($payhow == 9320)
	{
=comment
 9320 | Pay Agent Disbursement
 9325 | Pay Agent Disbursement Fee
 9321 | Pay Agent Disbursement (Self)
 9327 | Pay Agent Disbursement Funds
 9328 | Pay Agent Fees Received
=cut
		my $batch = rand();
		my $desc = "Pay Agent: " . AgentLabel($params_hash->{'pay_agent_selected'});
		$db->begin_work;
		$trans_num = $db->selectrow_array('SELECT debit_trans_complete(?,?,?,?,?,?)', undef,
							$member_id,
							$payhow,
					 		($amount * -1),
							$desc,
							$batch,
							$operator
					);
					
		my $fee_trans = $db->selectrow_hashref("SELECT * FROM soa WHERE batch='$batch' AND trans_type=9325");
		
		# now create a credit on the Pay Agent's account and link it to the debit in the requester's
		my ($xid) = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')");
		$db->do("INSERT INTO soa (trans_id, id, trans_type, amount, description, batch, ref_trans_id, reconciled) VALUES ($xid, ?, 9327, $amount, ?, ?, $trans_num, TRUE)", undef,
			$params_hash->{'pay_agent_selected'},
			"Pay Agent payout to: $mem_info->{'alias'}",
			$batch);
		# link the requester's transaction to this one in the Agent's
		$db->do("UPDATE soa SET reconciled=TRUE, ref_trans_id= $xid WHERE trans_id= $trans_num");
		
		# do the same thing for the fee value
		my $fee_desc = "References: $xid";
		($xid) = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')");
		$db->do("
			INSERT INTO soa (trans_id, id, trans_type, amount, description, batch, ref_trans_id, reconciled)
			VALUES ($xid, ?, 9328, $fee_trans->{'amount'} * -1, ?, ?, $fee_trans->{'trans_id'}, TRUE)",
			undef,
			$params_hash->{'pay_agent_selected'},
			$fee_desc,
			$batch);
		# link the requester's fee transaction to this one in the Agent's
		$db->do("UPDATE soa SET reconciled=TRUE, ref_trans_id= $xid WHERE trans_id= $fee_trans->{'trans_id'}");

		$db->commit;
	}
	else
	{
		$trans_num = $db->selectrow_array('SELECT debit_trans_complete(?,?,?,?,NULL,?)',
							undef,
							( 	$member_id,
								$payhow,
					 			($amount * -1),
								$params_hash->{'account_email'},
								$operator
							) );
	}
	
	if ( $trans_num )
	{
		$messages .= qq#<span class="good">
					<xsl:value-of select="//base/lang_blocks/messages/success1" />
					<xsl:text> $trans_num.</xsl:text>
					</span><br/>#;
	}
	else
	{
		$messages .= qq#<span class="bad">
					<xsl:value-of select="//base/lang_blocks/messages/fail_disburse" />
					</span><br/>#;
	}
	return $trans_num;
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $xsl_template = shift;
	my $active_pms = '<active_payment_methods>';
	$active_pms .= "<method>$_</method>" foreach @ActivePaymentMethods;
	$active_pms .= '</active_payment_methods>';
	# Combine the pieces of the xml document.
	my $xml =
		"<base>
		$active_pms
		$content_xml
		<data>$data_xml"
		. $xs->XMLout($mem_info, 'RootName'=>'user')
		. $xs->XMLout(AgentLabel($params_hash->{'pay_agent_selected'}), 'RootName'=>'pay_agent_selected') 
		. "<payout_minimum_value>$PAYOUT_MIN</payout_minimum_value></data>
		$pricing_xml
		$params_xml
		$menus_xml
		</base>";

	if ($q->param('xml'))
	{
		print "Content-type: text/plain\n\n$xml";
	}
	else
	{
		# Get the xsl template.
		die "Invalid form argument\n" unless $TMPL{$xsl_template};	###### since the value is tainted
		my $xsl = $gs->GetTemplate($TMPL{$xsl_template}) || die "Failed to load XSL template: $TMPL{$xsl_template}\n";

		$xsl =~ s/%%ME%%/$ME/g;
		
		# in keeping with the way this script and the templates were originally authored
		# we'll keep doing this
		$messages = qq#<div id="messages">$messages</div># if $messages;
		$xsl =~ s#%%messages%%#$messages#;

		# If we're in as Admin, let's carry it on and show it.
		my $adminlink = '';
		my $admin = '';
		if (  $access_type eq 'admin' && $action eq 'report' )
		{
			$adminlink = "?admin=1;id=$params_hash->{'id'}";
			$admin .= qq#<input type="hidden" name="admin" value="1"/><input type="hidden" name="id" value="$params_hash->{'id'}"/>#;
			$admin .= qq#<tr><td colspan="4" 
						style="background-color: red; 
							color: white;
							font-weight: bold;
							text-align: center"> ADMIN MODE </td></tr>#;	
		}
		$xsl =~ s/%%adminlink%%/$adminlink/g;
		$xsl =~ s/%%admin%%/$admin/g;

		$xsl =~ s/%%balance%%//g;		
		print $q->header, XML::local_utils::xslt($xsl, $xml);
	}
}

###### Generate e-mail to the Accounting Dept.
sub Email_Accounting
{
	my $email_message = shift;
	# Build the message
	my $message = MIME::Lite->new(
           	'From'     => 'funding.cgi',
           	'To'       => $ACCOUNTING_ADDRESS,
           	'Subject'  => "CA disbursement request: $PayoutMethods{$params_hash->{'payhow'}}->{'label'}",
           	'Type'     => "text/plain",
           	'Encoding' => '8bit',
           	'Data'     => $email_message);
	$message->attr("content-type" => "text/plain");
    $message->attr("content-type.charset" => "utf-8");

	# Send the message
	$message->send;

	return 1;
}

sub EmailPayAgent
{
	my $tmpl = $gs->GetTemplate($TMPL{'pay_agent_notice'}) || warn "Could not load template: $TMPL{'pay_agent_notice'}";
	return unless $tmpl;
	
	# strip out comments after handling windows style carriage returns
	$tmpl =~ s/(\r\n)|\r/\n/g;
	$tmpl =~ s/#.*\n//g;
	
	# extract the subject
	$tmpl =~ s/Subject:\s*(.*)\n//;
	my $subject = $1;
	
	my ($tmp, $TT) = ();

	my $sender = $db->selectrow_hashref("
		SELECT alias, emailaddress, firstname, lastname, firstname||' '||lastname AS name, address1, address2, city, state, country, postalcode
		FROM members
		WHERE id=?", undef, $member_id);
	my $agent = $db->selectrow_hashref("
		SELECT COALESCE(pa.emailaddress, m.emailaddress) AS emailaddress, firstname, lastname
		FROM members m
		JOIN pay_agent.agents_by_country pa
			ON pa.id=m.id
			AND pa.country_code= ?
		WHERE m.id=?", undef, $sender->{'country'}, $params_hash->{'pay_agent_selected'});
	# no point in loading that whole class every time when it will only be used occasionally
	eval {
		use Template;
		$TT = new Template;
	};
	$TT->process(\$tmpl, {'agent'=>$agent, 'sender'=>$sender}, \$tmp);
	my $message = MIME::Lite->new(
		'From'		=> 'acf1@dhs-club.com',
		'To'		=> encode('MIME-Header', qq|"$agent->{'firstname'} $agent->{'lastname'}" <$agent->{'emailaddress'}>|),
		'Cc'		=> encode('MIME-Header', qq|"$sender->{'firstname'} $sender->{'lastname'}" <$sender->{'emailaddress'}>|),
		'Bcc'       => $ACCOUNTING_ADDRESS,
		'Subject'	=> $subject,
		'Encoding' => '8bit',
		'Data'     => $gs->Prepare_UTF8($tmp, 'encode'));
	$message->attr("content-type" => "text/plain");
    $message->attr("content-type.charset" => "utf-8");

	# Send the message
	$message->send('smtp', 'localhost');
}

sub End
{
	$db->disconnect if $db;
	exit;
}

##### prints an error message to the browser.
sub Err
{
	my $err = shift;
	$err = Encode::encode_utf8($err) if utf8::is_utf8($err);

	print $q->header('-expires'=>'now', '-charset'=>'utf-8');
	print q| <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html><head><title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<body>|;
	print "<div />$err<div />";
	print $q->end_html;
	
	End();
}

sub Get_Balances
{
	my $cacheOK = shift;	# to be backwards compatible an undef is ok to mean that that we want fresh data
	
	return @BalancesCache if @BalancesCache && $cacheOK;

	@BalancesCache = $db->selectrow_array("
		SELECT account_balance,
			transferrable_balance,
			withdrawable_balance
		FROM soa_account_balance_payable WHERE id= ?", undef, $mem_info->{'id'});
	# we are looking for 4 values,
	# however if the record is null we return undef and the vars on the other end up undef
	for (my $x=0; $x < 4; $x++)
	{
		$BalancesCache[$x] = sprintf "%.2f", ($BalancesCache[$x] || 0);
	}
	return @BalancesCache;
}

sub GenerateBalancesXML
{
	my ($balance, $transferable_balance, $withdrawable_balance) = @_;
	return "<balance>$balance</balance>
			<transferable_balance>$transferable_balance</transferable_balance>
			<withdrawable_balance>$withdrawable_balance</withdrawable_balance>";
}

sub GenerateConfirmData
{
	my ($request_amount) = $db->selectrow_array('SELECT allowable_request_from_soa(?, ?, ?)',
		undef, $mem_info->{'id'}, $params_hash->{'payhow'}, $params_hash->{'amount'});
	die "Failed to obtain a verified request amount\n" unless defined $request_amount;

	my ($fee) = $db->selectrow_array('SELECT calculate_soa_fee(?, ?)', undef, $request_amount, $params_hash->{'payhow'});
	die "Failed to obtain the fee for: params_hash->{'payhow'}\n" unless defined $fee;

	# since we are not actually creating a transaction with the fee, which is negative, but instead displaying it
	# we'll invert the value
	$fee *= -1;
	my ($fee_label) = $db->selectrow_array('SELECT fee_label FROM soa_fees WHERE trans_type= ?', undef, $params_hash->{'payhow'}) || '';
	my $rv = "<confirm><fee>$fee</fee><request_amount>$request_amount</request_amount><fee_label>$fee_label</fee_label>";
	
	$rv .= '<need_account/>' if $PayoutMethods{$params_hash->{'payhow'}}->{'need_account'};
	$rv .= "</confirm>\n";
	return $rv;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		# note, these cookies are only present in an https session... ie. admin mode will fail if called http
		my ($cs, $ck, $creds) = ();
		# no point loading this module for general partner use
		eval {
			require cs_admin;
			$cs = cs_admin->new({'cgi'=>$q});
		};
		unless (
			($ck = $q->cookie('cs_admin')) && 
			($creds = $cs->authenticate($ck)) &&
		# Check for errors.
			($db = ADMIN_DB::DB_Connect( $cgiini, $creds->{'username'}, $creds->{'password'}) ))
		{
			Err("You must be logged in as an Admin to use this form.");
		}
		else
		{
			# Mark this as an admin user.
			$access_type = 'admin';
			$member_id = $q->param('id');
			$params_hash->{'admin'} = 1;
		}
	}
	# If this is a member.		 
	else
	{
		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id && $member_id !~ m/\D/ )
		{
			$access_type = 'auth';
			exit unless $db = DB_Connect($cgiini);
			$db->{'RaiseError'} = 1;
		}
	}

	if ($access_type)
	{
		$mem_info = $db->selectrow_hashref("
			SELECT m.id,
				m.alias,
				m.firstname,
				m.lastname,
				m.membertype,
				m.country,
				COALESCE(m.phone::TEXT,'') AS phone,
				m.emailaddress,
				COALESCE(m.language_pref, 'en') AS language_pref,
				COALESCE(nop.flag, 0) AS nopay,
				COALESCE(m.vip_date::TEXT,'') AS vip_date,
				COALESCE(pa.id::TEXT,'') AS is_pay_agent
			FROM members m
			LEFT JOIN nop
				ON m.id=nop.id
			LEFT JOIN pay_agent.pay_agents pa
				ON pa.id=m.id
				AND pa.active=TRUE
			WHERE m.id = ?", undef, $member_id);
	}
}

###### Get the member details to pre-populate the form.
#sub Get_Member_Details
#{
#	my $tmp_hash = $db->selectrow_hashref("
#		SELECT acct_holder,
#			address AS addr,
#			city,
#			state,
#			postal_code AS postalcode,
#			LOWER(country) AS country
#		FROM mop WHERE id = $member_id");
#
#	#  Make the 'US' state lower case for the menu.
#	if ( $tmp_hash->{'country'} eq 'us' )
#	{
#		$tmp_hash->{'state'} = lc($tmp_hash->{'state'});
#	}
#	# In the case of a foreign country, move the state name to the province field.
#	#  This won't be perfect, but should work more often than leaving state as-is.
#	else
#	{
#		$tmp_hash->{'province'} = $tmp_hash->{'state'};
#		$tmp_hash->{'state'} = '';
#	}
#
#	foreach ( keys %{$tmp_hash} )
#	{
#		$params_hash->{$_} = $tmp_hash->{$_};
#	}
#}

###### Get the member's transactions for the specified timeframe.
sub Get_Transactions
{
	my $start_mon = $q->param('period') || 'current';
	my ($start, $stop) = '';

	# If request is for the current month.
	if ( $start_mon eq 'current' )
	{
		($start, $stop) = $db->selectrow_array("SELECT first_of_month(), last_of_month()");
	}
	# If request is for the previous month.
	elsif ( $start_mon eq 'previous' )
	{
		($start, $stop) = $db->selectrow_array("	
					SELECT first_of_another_month((first_of_month() - INTERVAL '1 DAY')::DATE),
						(first_of_month() - INTERVAL '1 DAY')::DATE");
	}
	# Otherwise, build out the start and stop dates from the passed params.
	else
	{
		$start = $q->param('start_year') . "-" . $q->param('start_mon') . "-01";
		# Set as the first of the month, letting the SQL figure out the last day of the month.
		$stop = $q->param('stop_year') . "-" . $q->param('stop_mon') . "-01";
	}

	# Get the last day of the stop month.
	$stop = $db->selectrow_array("SELECT last_of_another_month(?::DATE)", undef, $stop);

	# Get the transactions.
	my @transactions = ();

	my ($beginning_balance) = $db->selectrow_array("
		SELECT SUM(amount)
		FROM soa
		WHERE --COALESCE(batch,'') != 'cs123112' 
		--comm_statement_item IS NULL
		--AND 
		void=FALSE AND id= $mem_info->{'id'} AND entered::DATE < ?", undef, $start) || 0;

	my $sth = $db->prepare("
		SELECT --'T' || s.trans_id AS trans_code,
			s.trans_id,
			s.trans_type,
			s.amount,
			COALESCE(s.description, '') AS description,
			s.entered,
			s.void,
			coalesce(s.batch,'-1') AS batch,
			st.name,
			s.frozen

		FROM $SOA_VW s
		JOIN soa_transaction_types st
			ON s.trans_type = st.id
		WHERE 	s.id = $mem_info->{'id'}
		AND 	s.entered::DATE BETWEEN ? AND ?
		ORDER BY s.entered ASC, s.trans_id ASC");
	$sth->execute($start, $stop);
	
	# we are obtaining these in reverse order so that we can take our account balance and reverse engineer the running balance at each transaction
	# we will unshift them onto the array so the stack is in the proper order when we are done
	my $running_balance = $beginning_balance;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$running_balance = sprintf('%.2f', ($running_balance + $tmp->{'amount'}) ) unless $tmp->{'void'};
		$tmp->{'running_balance'} = $tmp->{'void'} ? '' : $running_balance;
		push @transactions, $tmp;
	}

	@transactions = '' unless @transactions;
	return "<start>$start</start><stop>$stop</stop><beginning_balance>$beginning_balance</beginning_balance><ending_balance>$running_balance</ending_balance>" .
		$xs->XMLout(\@transactions, 'RootName'=>'transactions');
}

###### Insert funds into their pre-pay account - soa table and return the transaction number.
sub Insert_Funds
{
	# Build the query if we can get a transaction number from the soa table.
	if ( my $trans_num = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')") )
	{
		my $qry = "INSERT INTO soa ( trans_id,
						id,
						trans_type,
						amount,
						description,
						operator )
				VALUES ( ?, ?, ?, ?, ?, ? )";
		my $sth = $db->prepare($qry);
		my $rv = $sth->execute(
			$trans_num,
			$params_hash->{'id'},
			7000,
			$params_hash->{'amount'},
			'Pre-Pay Funding',
			$cgiini);
		return $trans_num if $rv;
	}
	return 0;
}

sub IsValidPayoutMethod
{
	return unless $params_hash->{'payhow'};
	die "Invalid payout method: $params_hash->{'payhow'}" unless grep $_ eq $params_hash->{'payhow'}, @ActivePaymentMethods;
}

sub LoadPayAgentData
{
	if (my $agents = AvailablePayAgents())
	{
		$data_xml .= '<pay_agent_available>1</pay_agent_available>';
		$data_xml .= Build_Menu_Available_PayAgents($agents);
	}
}

###### Load the params into a local hash.
sub Load_Params
{
	my @params = @_;
	@params = $q->param() unless @params;

	# Get the submitted parameters.
	foreach ( @params )
	{
		$params_hash->{$_} = $gs->Prepare_UTF8($q->param($_) || '');
		$params_hash->{$_} = '' if $params_hash->{$_} eq 'blank';
	}

	# Remove the leading D from the cc_exp field(added for xml menu building).
	$params_hash->{'cc_exp'} =~ s/D// if $params_hash->{'cc_exp'};
	die "Invalid pay agent selection" if $params_hash->{'pay_agent_selected'} && $params_hash->{'pay_agent_selected'} =~ m/\D/;	# should be integer value
	IsValidPayoutMethod();
}

# Jan. 2013, Dick wants to get tight about releasing funds if a Partner is unpaid, or if a non-Partner could possibly have "owed fees" when they dropped from being a Partner
# rather than get real technical about this, we will simply check the nop flag for Partners
# and if not a Partner we will check the latest date that they were a Partner and see if they had a valid subscription at that time
# in either case of TRUE, accounting is going to get involved
sub UnpaidCk
{
	return 1 if $mem_info->{'nopay'};
	return 0 if $mem_info->{'membertype'} eq 'v';	# if they are a partner and they did not get flagged as a nop, we are good
	return 0 if ! $mem_info->{'vip_date'};			# if they do not have a vip_date, then they were never a partner and so we don't need to check anything else
	
	my ($rv) = $db->selectrow_array("
		WITH history AS (
			SELECT id, membertype, stamp::DATE
			FROM members
			WHERE id= $mem_info->{'id'}
			UNION
			SELECT id, membertype, stamp::DATE
			FROM members_arc
			WHERE id= $mem_info->{'id'}
			ORDER BY stamp DESC),
		lastv AS (SELECT id, stamp FROM history WHERE membertype='v' ORDER BY stamp DESC LIMIT 1),
		lastv_date AS (SELECT history.id, history.stamp FROM history JOIN lastv ON history.id=lastv.id WHERE history.stamp > lastv.stamp ORDER BY history.stamp LIMIT 1)
		SELECT DISTINCT s.member_id
		FROM subscriptions_history s
		JOIN subscription_types st ON st.subscription_type=s.subscription_type AND st.sub_class='VM'
		JOIN lastv_date ON lastv_date.id=s.member_id
		WHERE s.member_id= $mem_info->{'id'}
		AND s.void=FALSE
		AND lastv_date.stamp::DATE BETWEEN s.start_date AND s.end_date
	");
	
	return 1 unless $rv;	# if we got no result back, then we were not paid at the last time we were a Partner
	return 0;
}

__END__
###### 11/15/05 Added handling for the upgrade voucher in the reports.
###### 12/09/05 Added additional language to the success message
######		to remind the VIP to apply the newly deposited funds.
###### 12/15/05 Added the Moneybookers form and handling.
###### 02/09/06 Added the Admin mode to Display_Page for reporting only.
###### 02/10/06 Added handling of voided transactions in Get_Balance and
######		in Display_Page and added the link to the void script.
###### 03/10/06 Added handling for CC deposits less than $10.
######		Also added external javascript handling for CC and Moneybooker deposits under $10.
###### 03/27/06 Added handling for disbursement check requests.
###### 04/27/06 Added a "payable" flag (p) to Moneybookers payment type in the database so that
######		members can request disbursement of those type of funds. No change to the script.
# 12/11/06 revised to have 10 years worth of CC exp. dates
###### 08/10/07 moved the log file open/close into the CC routine since that is the only place it is used
###### 11/12/07 revised the balance inquiry code to use the DB view designed for the purpose
###### 11/27/07 tweaked to load CC funding errors back into the same interface
###### instead of losing them by loading the statment interface after failure
# 01/04/08 added country into the mix for the member info
# 10/08 added the pending_items_total to the mix for disbursement requests
# 10/07/08 added the member argument to the authorization call, also changed the invoice number to reflect 'CA'
###### 02/25/08 revised to add funding through Alert Pay
###### 04/14/09 major revisions to support multiple payout methods and the associated fees in a uniform manner
###### 04/15/09 added a check and decline of disbursement requests from VIPs who are nopay
#	2009-12-04	Keith	Validation of the "amount" field was added to the  "CC)Process_Order"
#						People were entering their amounts in an incorrect format, "20,00", their credit card was
#						being charged "2000", and the insert into their club account was erroring out, so there was
#						no record of the transaction.
# 09/22/10 tweaked to use GI_ccauth instead of ccauth... ccauth is now for ClubShop use only
###### 01/07/13 added the concepts of beginning, ending and running balances
###### also enhanced the concept of nopay to extend to non-Partners who may have owed a fee when they went to non-Partner
###### 06/07/13 had to make changes to the UnpaidCk so that it worked properly... had bad SQL
###### 12/04/13 removed Alert Pay (Payza) from the active payment methods
###### 02/04/14 added a year to the initial $MAX_MENU_YEAR calculation at the head... that is a lame way of doing it at this point, but I cannot take the time to fix it
###### 04/18/14 per Dick: ClubAccount - create a new minimum balance of $25.00 before a withdrawal can be made.
###### also put in the Pay Agent disbursal option
# 11/19/14 revised UnpaidCk() to recognize non-Partners who have never been partners so as to not evaluate them for past due fees
# 01/02/15 had to add type casting to the last_of_another_month() argument since we now have two versions
03/10/15
	Changes in Create_Payout() to "automate" the process of Pay Agent disbursements
	Also added EmailPayAgent()
03/12/15
	modified the CC_Fraud_Ck processing
03/19/15
	Repointed the query in AvailablePayAgents to point to a different VIEW
04/08/15
	Revised EmailPayAgent to use the email address associated with the PA record if present
05/04/15
	Changed the balance that is checked when they are requesting a Pay Agent disbursement.. from transferrable to withdrawable
	Also fixed the SQL in EmailPayAgent()
05/18/15
	Doctored up the utf-8 flag when constructing the email to the pay agent
05/30/15
	Added the pull in of one's own ID if they are a pay agent, in order to influence the payment options page not to allow them to transfer to another pay agent
06/04/15
	Added MIME encoding of the composite email address in EmailPayAgent()