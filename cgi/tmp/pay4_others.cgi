#!/usr/bin/perl -w
###### pay4_others.cgi
######
###### A script that allows one Partner to pay for another Partner's subscriptions 
######		using the pre-pay funding system (soa table).
######
###### created: 10/19/05	Karl Kohrt
###### heavily modified June 2012... like basically a replacement in place
###### modified: 11/24/14	Bill MacArthur
=comment
YES! :)

On 2/28/2014 10:38 PM, Bill MacArthur wrote:
> Is this something you want to add to the list... to enable they, themselves to take a free upgrade to PRO, at the subscription payment interface, if they have at least 3 PRPs?
=cut

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;
require XML::LibXML::Simple;
use Template;
use Data::Serializer;
use Encode;

###### CONSTANTS
my $FREE_PRO = 1;
my $TESTMODE = 0;	# set to -1- to NOT commit anything... should be set to -0- for normal operation
my $SERIALIZER = Data::Serializer->new(
	'serializer'		=> 'Storable',
	'digester'   		=> 'MD5',
	'cipher'     		=> 'Blowfish',
	'secret'     		=> 'eojh574#@!',
	'serializer_token'	=> 0
);

my $cgiini = "pay4_others.cgi";
my %TMPL =
(
	'content'	=> 10497,	# our XML language pack
	'start'		=> 10965,#10163
	'confirm'	=> 10964,#10192
	'pay'		=> 10963,#10236
	'confirm_ug'=> 10960,
	'payug'		=> 10961,
	'wrap'		=> 10962
);

my %TABLES =
(
	'soa'		=> 'soa',
	'members'	=> 'members',
	'cbc'		=> 'currency_by_country'
);

###### GLOBALS
my $db = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>$db});
my $TT = Template->new();
my $ME = $q->script_name;

(my $path = $q->path_info) =~ s#^/+##;
my ($action, $item_code)  = (split /\//, $path);	# See if the admin is trying to purchase.
$action ||= $q->param('action') || '';			# Get the action if it wasn't in the path.
$item_code ||= $q->param('item') || '';			# Get the item if it wasn't in the path.
my $SubmittedList = $q->param('list');

my $operator = $q->cookie('operator') || '';		# Get the admin user if exists.
my ($member_id, $mem_info, %SUBSCRIPTION_PRICING, @ActionList) = ();
my %Misc = ();	# for all those bits of data that we do not want to create another global var for, but that we want to make into the template
my %DEBUG = ();
my $access_type = '';
my $login_error = 0;
our %messages = ();
my $balance = 0;

################################
###### MAIN starts here.
################################

# just comment the following line out to disable the ability to debug based upon the debug parameter... like when this is in production ;)
enableDebugging();

# Get the info for a logged in member.
Get_Cookie();	# this routine also connects to the DB if there is a legit login
my $fta = fta::new($mem_info->{'id'}, $db);

my $SubscriptionTypes = $db->selectall_hashref("
	SELECT st.*
	FROM subscription_types st
	WHERE st.sub_class IN ('VA','VM')
--	AND st.renewal=TRUE
	", 'subscription_type');

my $lang_blocks = ParseLanguagePack();
#die Dump($lang_blocks);
# If they are logged in as a Partner or an Admin.
if ( $access_type eq 'auth' || $access_type eq 'admin' )
{
	my $tmpl = 'start';
	$balance = Get_Balance();

	# if they have no balance, then they cannot do anything
	if ($balance <= 0 && (! $FREE_PRO))
	{
		push @{$messages{'bad'}}, $lang_blocks->{'messages'}->{'no_balance'};
		Display_Page('start', {'title'=>$lang_blocks->{'title'}});
		Exit();
	}

	# If sent a list of IDs to confirm.
	if ( $action eq 'confirm' )
	{
		# Get the pricing/product data.
		if ($item_code eq 'ug')
		{
			Confirm_Upgrade_List();
			die Dump(\@ActionList) if $DEBUG{1};
			Display_Page('confirm_ug', {'title'=>$lang_blocks->{'pay4new'}});
			Exit();
		}
		elsif ($item_code eq 's')
		{
			Confirm_List();
			die Dump(\@ActionList) if $DEBUG{1};
			Display_Page('confirm', {'title'=>$lang_blocks->{'pay4new'}});
			Exit();
		}
		else
		{
			push @{$messages{'bad'}}, 'please_select_item_type';
		}
	}
	# If sent a list of IDs to pay.
	elsif ( $action eq 'pay' )
	{
		Pay_List();		# Process the list to be paid.
		Exit();
	}
	elsif ( $action eq 'payug')
	{
		Pay_UG_List();
		Exit();
	}
	else
	{
		
	}
	
	Display_Page('start', {'title'=>$lang_blocks->{'title'}});
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	Err(qq#Please log in <a href="$LOGIN_URL?destination=$ME">HERE</a> to use this form.#);
}

Exit();

################################
###### Subroutines starts here.
################################

##### Builds a confirm list of the IDs submitted.
sub Confirm_List
{
	my @list = @_;
	my $goodCnt = 0;
	my $qry = "
		SELECT
			m.id, m.alias, m.firstname, m.lastname, m.membertype, m.vip_date, m.country, cbc.currency_code
		FROM members m
		JOIN currency_by_country cbc
			ON cbc.country_code=m.country
		WHERE ";
	my $sth_alias = $db->prepare("$qry m.alias=?");
	my $sth_id = $db->prepare("$qry m.id=?");
	my $confirm_balance = $balance;
	$Misc{'running_cost'} = 0;
	
	foreach ( @list ? @list : ExtractListItems() )
	{
		my $data = ();

		# If we have a letter, it must be an alias.
		if ( $_ =~ m/\D/ )
		{
			$_ = uc($_);
			$sth_alias->execute($_);
			$data = $sth_alias->fetchrow_hashref();
		}
		# Otherwise, it's an ID.
		else
		{
			$sth_id->execute($_);
			$data = $sth_id->fetchrow_hashref();
		}

		if ( $data )
		{
			$data->{'row_cost'} = '0.00';

			# Check if in the downline unless we were fed a list. In that case we are using a processed list we know are good.
			if (! @list && ! $gs->Check_in_downline($db, 'members', $mem_info->{'id'}, $data->{'id'}) )
			{
#				if ($mem_info->{'country'} eq $data->{'country'})
#				{
#					# Oct 2014 Dick and Pat together agreed that the party does not have to be downline as long as they are in the same country
#				}
				if ($fta->country_authorized_crossline($data->{'country'}))
				{
					# Oct 2014 Dick and Pat together agreed that the party does not have to be downline as long as they are in the same country
					# Feb 2015 Pat wants them to be able to pay crossline as long as their registered for that country
				}
				else
				{
					$data->{'error_node'} = 'not_inline_not_authorized';
				}
			}
			elsif (! @list && $data->{'membertype'} ne 'v')
			{
				$data->{'error_node'} = 'invalid';
			}

			if ( ! $data->{'error_node'} )
			{
				$data->{'subscriptions'}->{'monthly'}->{'current'} = GetMemberSubscriptionMonthly({'id'=>$data->{'id'}});

				unless ($data->{'subscriptions'}->{'monthly'}->{'current'})
				{
					# this is an extremely edge case, but we will handle it, even if it only kicks in while testing
					$data->{'error_node'} = 'Unable to retrieve a monthly partner suscription';
					$data->{'row_cost'} = 0;
					push @ActionList, $data;
					next;
				}
				
				SetRenewalStype($data->{'subscriptions'}->{'monthly'}->{'current'});
				
				$data->{'subscriptions'}->{'annual'}->{'current'} = GetMemberSubscriptionAnnual({'id'=>$data->{'id'}});
				unless ($data->{'subscriptions'}->{'annual'}->{'current'})
				{
					# this is an extremely edge case, but we will handle it, even if it only kicks in while testing
					$data->{'error_node'} = 'Unable to retrieve an annual partner suscription';
					$data->{'row_cost'} = 0;
					push @ActionList, $data;
					next;
				}
				
				SetRenewalStype($data->{'subscriptions'}->{'annual'}->{'current'});
				
				# if we are not paid on the monthly, then we will present the pricing for subscriptions available to them (as of June 2012, those who upgraded prior to 06/12 cannot downgrade to BASIC)
				if (! $data->{'subscriptions'}->{'monthly'}->{'current'}->{'paid'})
				{
					foreach my $stype (@{$db->selectall_arrayref("SELECT * FROM unnest(available_monthly_subscriptions('$data->{'vip_date'}'::DATE))")})
					{
						# after much debugging, it was discovered that we cannot add keys to that return value as it is actually a reference to a cached value
						# therefore subsequent iterations of this loop will change the keys that seem like they would be unique because of the "my" declaration
						my %renewal = %{SubscriptionPricing({'subscription_type'=>$stype->[0], 'currency_code'=>$data->{'currency_code'}})};

						$renewal{'itemtype'} = 'renewal';
						
						# upgrades create "column-name extended" values as there could be more than 1 month involved
						# mimic that here as the template is depending on these values
						foreach my $col (qw/amount pp usd/)
						{
							$renewal{"$col extended"} = $renewal{$col};	# we don't have extended prices for renewals... they are one month
						}
						
						$renewal{'xfactor'} = 1;
						$renewal{'keyval'} = CreateKeyVal(\%renewal, $data, 'monthly');

						$data->{'subscriptions'}->{'monthly'}->{'renewals'}->{ $renewal{'subscription_type'} } = \%renewal;
					}
					
					$data->{'row_cost'} = SubscriptionPricing( {'subscription_type'=>$data->{'subscriptions'}->{'monthly'}->{'current'}->{'renewal_stype'}, 'currency_code'=>$data->{'currency_code'}} )->{'usd'};

					$goodCnt++ if $mem_info->{'id'} != $data->{'id'};
				}
				# otherwise we will present them will all the upgrades from them up
				else
				{
					my $utypes = $db->selectall_arrayref("
						SELECT st.subscription_type
						FROM subscription_types st
						WHERE st.sub_class='VM'
						AND renewal=TRUE AND st.weight > $SubscriptionTypes->{ $data->{'subscriptions'}->{'monthly'}->{'current'}->{'subscription_type'} }->{'weight'}
						ORDER BY st.weight ASC");
					foreach my $stype (@{$utypes})
					{
						my $upgrade = SubscriptionUpgradePricing(
							$data->{'id'},
							{
								'subscription_type' => $data->{'subscriptions'}->{'monthly'}->{'current'}->{'renewal_stype'},
								'currency_code'=>$data->{'currency_code'}
							},
							$stype->[0],
							$data->{'subscriptions'}->{'monthly'}->{'current'}->{'number_of_months'}
						);
						$upgrade->{'itemtype'} = 'upgrade';
						$upgrade->{'keyval'} = CreateKeyVal($upgrade, $data, 'monthly');
						$data->{'subscriptions'}->{'monthly'}->{'upgrades'}->{ $upgrade->{'subscription_type'} } = $upgrade;
					}

					$goodCnt++ if $mem_info->{'id'} != $data->{'id'};
				}
					
				# OK, now take care of annual subscription renewals
				if (! $data->{'subscriptions'}->{'annual'}->{'current'}->{'paid'})
				{
					my $annual = SubscriptionPricing({'subscription_type'=>4, 'currency_code'=>$data->{'currency_code'}});
					$annual->{'keyval'} = CreateKeyVal($annual, $data, 'annual');
					$data->{'row_cost'} += $annual->{'usd'};
					$data->{'subscriptions'}->{'annual'}->{'renewal'} = $annual;
				}
			}
		}
		else
		{
			$data->{'id'} = $_;
			$data->{'alias'} = $_;
			$data->{'error_node'} = 'invalid';
			$data->{'row_cost'} = 0;
		}
		
		warn "row_cost: $data->{'row_cost'}" if $DEBUG{'rowcostlast'};
		$Misc{'running_cost'} = sprintf('%.2f', ($Misc{'running_cost'} += $data->{'row_cost'}));
		warn "misc.running_cost: $Misc{'running_cost'}" if $DEBUG{'misc.running_cost'};
		$data->{'running_cost'} = $Misc{'running_cost'};
		push @ActionList, $data;
		
#		warn Dump($data);
	}
	$sth_alias->finish;
	$sth_id->finish;

	FTAcheck($goodCnt);
}

# generate a list of IDs and the upgrade_tmp info if available and other instructions if not
sub Confirm_Upgrade_List
{
	my @list = @_;
	my $goodCnt = 0;
	my $qry = "
		SELECT
			m.id, m.alias, m.firstname, m.lastname, m.membertype, m.country, cbc.currency_code
		FROM members m
		JOIN currency_by_country cbc
			ON cbc.country_code=m.country
		WHERE ";
	my $sth_alias = $db->prepare("$qry m.alias=?");
	my $sth_id = $db->prepare("$qry m.id=?");
	
	my $utsth = $db->prepare("
		SELECT
			ut.subscription_type,
			ut.number_of_months,
			ut.amount,
			ut.last_modified::DATE AS last_modified,
			ut.payment_method,
			(NOW()::DATE -30) AS expiration_date	/* when we will consider this record expired...
													 * in other words if this query is run and the expiration date is greater than last_modified...
													*/
		FROM upgrade_tmp ut
		WHERE ut.id=?
	");
	my $confirm_balance = $balance;
	$Misc{'running_cost'} = 0;
	
	foreach ( @list ? @list : ExtractListItems() )
	{
		my $data = ();

		# If we have a letter, it must be an alias.
		if ( $_ =~ m/\D/ )
		{
			$_ = uc($_);
			$sth_alias->execute($_);
			$data = $sth_alias->fetchrow_hashref();
		}
		# Otherwise, it's an ID.
		else
		{
			$sth_id->execute($_);
			$data = $sth_id->fetchrow_hashref();
		}

		if ( $data )
		{
			$data->{'row_cost'} = '0.00';
			
			# Check if in the downline unless we were fed a list. In that case we are using a processed list we know are good.
			if (! @list && ! $gs->Check_in_downline($db, 'members', $mem_info->{'id'}, $data->{'id'}) )
			{
#				$data->{'error_node'} = 'not_inline';
				if ($mem_info->{'country'} eq $data->{'country'})
				{
					# Oct 2014 Dick and Pat together agreed that the party does not have to be downline as long as they are in the same country
				}
				else
				{
					$data->{'error_node'} = 'not_inline';
				}
			}
			elsif ($data->{'membertype'} eq 'v')
			{
				$data->{'error_node'} = 'already_v';
			}
			
			if ( ! $data->{'error_node'} )
			{
				
				# get our upgrade_tmp record... if it's available ;)
				$utsth->execute($data->{'id'});
				$data->{'upgrade_tmp'} = $utsth->fetchrow_hashref;
				
				if (! $data->{'upgrade_tmp'})
				{
					$data->{'error_node'} = 'no_upgrade_tmp';
				}
				elsif ($data->{'upgrade_tmp'}->{'expiration_date'} gt $data->{'upgrade_tmp'}->{'last_modified'})
				{
					$data->{'error_node'} = 'upgrade_tmp_expired';
				}
				elsif (! $data->{'upgrade_tmp'}->{'subscription_type'})	# if they do not have a subscription type, then they never got that far in the app
				{
					$data->{'error_node'} = 'no_upgrade_tmp';	# use the same message as if they had none at all
				}
				elsif ($data->{'upgrade_tmp'}->{'number_of_months'} == 1)	# as of 06/12, the only way to end up with only 1 month is to choose CC as your pay method
				{
					$data->{'error_node'} = 'badpm';
				}
			}
			
			if ( ! $data->{'error_node'} )
			{
#				my $pricing = SubscriptionPricing(
#					{'subscription_type'=>$data->{'upgrade_tmp'}->{'subscription_type'}, 'currency_code'=>$data->{'currency_code'}}
#				);
				
				#my $pricing = PartnerUpgradePrice($data->{'upgrade_tmp'}->{'subscription_type'}, $data->{'currency_code'}, $data->{'upgrade_tmp'}->{'number_of_months'});
#				$data->{'pricing'} = {
#					'currency_code'=>$pricing->{'currency_code'},
#					'subscription_type' =>$pricing->{'subscription_type'},
#					'usd'=> sprintf('%.2f', ($pricing->{'usd'} * $data->{'upgrade_tmp'}->{'number_of_months'})),
#					'pp'=>$pricing->{'pp'},
#					'amount'=> sprintf('%.2f', ($pricing->{'amount'} * $data->{'upgrade_tmp'}->{'number_of_months'})),
#					'soa_trans_type'=>$pricing->{'soa_trans_type'}
#				};
				$data->{'pricing'} = PartnerUpgradePrice($data->{'upgrade_tmp'}->{'subscription_type'}, $data->{'currency_code'}, $data->{'upgrade_tmp'}->{'number_of_months'});;
				$data->{'keyval'} = $SERIALIZER->serialize($data);
				$data->{'row_cost'} = $data->{'upgrade_tmp'}->{'amount'};

				$goodCnt++ if $mem_info->{'id'} != $data->{'id'};
			}
		}
		else
		{
			$data->{'id'} = $_;
			$data->{'alias'} = $_;
			$data->{'error_node'} = 'invalid';
			$data->{'row_cost'} = 0;
		}
		
		warn "row_cost: $data->{'row_cost'}" if $DEBUG{'rowcostlast'};
		$Misc{'running_cost'} = sprintf('%.2f', ($Misc{'running_cost'} += $data->{'row_cost'}));
		warn "misc.running_cost: $Misc{'running_cost'}" if $DEBUG{'misc.running_cost'};
		$data->{'running_cost'} = $Misc{'running_cost'};
		push @ActionList, $data;
	}
	$sth_alias->finish;
	$sth_id->finish;
	
	FTAcheck($goodCnt);
}

=head3 CreateKeyVal

	Create a value foreach of our radio and checkbox inputs that are associated with paying for a subscription.
	We are going to create a hash of values derived from a renewal/upgrade and a current subscription.
	Then we are going to serialize that data and create a hash.
	We will concatenate it all together so that it can be inspected when we get it back to ensure there has been no tampering.

=head4 Arguments

	A SubscriptionPricing record
	A current subscription record
	A flag to indicate a renewal or an upgrade

=head4 Returns

	A string that will become the value for the associated control

=cut

sub CreateKeyVal
{
	my ($pricing, $data, $type) = @_;
	my %package = (
		'current' => $data->{'subscriptions'}->{$type}->{'current'},
		'pricing' => $pricing,
		'id' => $data->{'id'},
		'alias' => $data->{'alias'}
	);
	
	return $SERIALIZER->serialize(\%package);
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $pagename = shift;
	my $args = shift;
	my $tmpl = $gs->Get_Object($db, $TMPL{$pagename}) || die "Failed to load XSL template: $TMPL{$pagename}\n";
	my $wrap = $gs->Get_Object($db, $TMPL{'wrap'}) || die "Failed to load XSL template: $TMPL{'wrap'}\n";
	
	my $data = {
		'item_code' => $item_code,
		'lang_blocks' => $lang_blocks,
		'params' => {
			'action' => $action,
			'item' => $item_code,
			'SubmittedList' => $SubmittedList,
			'debug' => ($q->param('debug') || '')
		},
		'script_name' => $ME,
		'member' => $mem_info,
		'balance' => {'usd' => $balance},
		'messages' => \%messages,
		'ActionList' => \@ActionList,
		'misc' => \%Misc,
		'FREE_PRO' => $FREE_PRO
	};
	
	# dynamically load certain data passed in
	foreach (qw/results payeeID/)
	{
		# don't clobber existing data structures
		die "data->{$_} already exists" if $data->{$_};
		$data->{$_} = $args->{$_} if $args->{$_};
	}

	if ($DEBUG{'data'})
	{
		print $q->header('-charset'=>'utf-8', '-type'=>'text/plain'), Dump($data);
	}
	else
	{
		print $q->header('-charset'=>'utf-8');
		my $content = ();
		my $rv = $TT->process(\$tmpl, $data, \$content);
		if ($rv)
		{
			$TT->process(\$wrap, {'content'=>$content, 'title'=>$args->{'title'}}) || print HandleError($TT->error, $TT->error); #'There has been an unexpected error processing the template'
		}
		else
		{
			print HandleError($TT->error, $TT->error);
		}
	}
}

sub Dump
{
	eval('require Data::Dumper;');
	return Data::Dumper::Dumper($_[0]);
}

sub enableDebugging
{
	my $points = $q->param('debug') || '';
	$DEBUG{$_} = 1 foreach split(/,/, $points);
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), "<br />$_[0]<br />";
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

=head3 ExtractKeyVal

	Deserialize the value received back on a parameter that was created by CreateKeyVal

=head4 Arguments

	A textual value
	
=head4 Returns

	A hash reference identical to that created by CreateKeyVal()
	
=cut

sub ExtractKeyVal
{
	return $SERIALIZER->deserialize($_[0]) if $_[0];
	return undef;
}

=head3 ExtractListItems

	Takes the 'list' parameter, splits out the lines and does some whitespace filtering to create an array

=head4 Arguments

	None
	
=head4 Returns

	An array of IDs. These could be alphanumeric is the user entered them that way.
	
=cut

sub ExtractListItems
{
	my @rv = ();
	# Split out the member IDs into an array.
	my @tmp = split /\n/, $SubmittedList || '';
	foreach (@tmp)
	{
		# Get rid of all whitespace including windows return chars.
		$_ =~ s/\s//g;
		next if $_ eq '';
		push @rv, $_;
	}
	return @rv;
}

# this routine is called during the selection process, so we are going to pass the initial count to nonFTAerr()
sub FTAcheck
{
	my $cnt = shift;
	$fta->update_count($cnt);
	return if $fta->approved();
	push @{$messages{'bad'}}, nonFTAerr($fta->{'initial_count'});
}

sub nonFTAerr
{
	my $running_cnt = shift;
	return qq|<div style="text-align:left">
		$lang_blocks->{'messages'}->{'fta1'}<br />
		$lang_blocks->{'messages'}->{'fta2'} $fta->{'max_count'}<br />
		$lang_blocks->{'messages'}->{'fta3'} $running_cnt</div>|;
}

###### Get the pre-pay balance for the submitting VIP.
sub Get_Balance
{
	return $db->selectrow_array("
		SELECT SUM(amount)
		FROM $TABLES{'soa'}
		WHERE id = ?
		AND void = FALSE
		AND frozen=FALSE", undef, $mem_info->{'id'}) || 0;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator');
		my $pwd = $q->cookie('pwd');
		require ADMIN_DB;
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
		{
			Err("You must be logged in as an Admin to use this form.");
			Exit();
		}
		else
		{
			# Mark this as an admin user.
			$access_type = 'admin';
			$member_id = $q->param('id');
		}
	}
	# If this is a member.		 
	else
	{
		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
		if ( $member_id && $member_id !~ m/\D/ )
		{
			$access_type = 'auth';
			exit unless $db = DB_Connect($cgiini, (), {'enable_lockout'=>1});
		}
		else
		{
			print $q->redirect($LOGIN_URL);
			exit;
		}
	}

	$db->{'AutoCommit'} = 0 if $TESTMODE;
	$db->{'RaiseError'} = 1;
	$mem_info = $db->selectrow_hashref("
		SELECT
			m.id, m.alias, m.firstname, m.lastname, m.country,
			cbc.currency_code
		FROM $TABLES{'members'} m
		JOIN $TABLES{'cbc'} cbc ON cbc.country_code=m.country
		WHERE m.id = ?",
		undef, $member_id);
}

###### Get the upgrading member's info using a supplied ID.
sub Get_Info
{
	my $id = shift;
	my $info = ();

	# Get an ID if this looks like an alias.
	if ( $id =~ /\D/ )
	{
		$info = $db->selectrow_hashref("SELECT * FROM $TABLES{'members'} WHERE alias = ?", undef, uc($id));
	}
	# Get an email info if we have an ID.
	elsif ( $id )
	{
		$info = $db->selectrow_hashref("SELECT * FROM $TABLES{'members'} WHERE id = ?", undef, $id);
	}
	return $info;
}

=head3 GetMemberSubscriptionAnnual

	Get the current Annual subscriptions for a given ID.

=head4 Arguments

	Expecting a hashref with at least an 'id' key pointing to a numeric ID

=head4 Returns

	A hashref
	
=cut

sub GetMemberSubscriptionAnnual
{
	my $args = shift;
	return $db->selectrow_hashref("
			SELECT
				pk,
				member_id,
				subscription_type,
				start_date,
				end_date,
				price,
				currency_code,
				pp_value,
				usd,
				paid
			FROM partner_subscription_annual WHERE member_id= $args->{'id'}
		");
}

=head3 GetMemberSubscriptionMonthly

	Get the current Monthly subscriptions for a given ID.

=head4 Arguments

	Expecting a hashref with at least an 'id' key pointing to a numeric ID

=head4 Returns

	A hashref

=head5 Notes

	The VIEW used in the SQL can return a subscription as NOT paid that would otherwise show as paid in this scenario:
	After the end of the month but before fees are run and the nop_seed is updated.
	
	The reason for this is that we do not want a subscription to show as a simple upgrade if it really is expired.
	It can be "upgraded" for sure, but only by purchasing the entire subscription, not just the difference between the existing paid one and the upgraded one.
	
=cut

sub GetMemberSubscriptionMonthly
{
	my $args = shift;
	return $db->selectrow_hashref("
			SELECT
				pk,
				member_id,
				subscription_type,
				start_date,
				end_date,
				price,
				currency_code,
				pp_value,
				usd,
				paid,
				how_many_months_ahead(end_date)+1 AS number_of_months
			FROM partner_subscription_monthly_strict WHERE member_id= $args->{'id'}
		");
}

sub HandleError
{
	my ($error, $return_text, $email_flag) = @_;

	if ($email_flag)
	{
		# Build the message
		my %mail = (
				'From' => 'pay4_others.cgi@clubshop.com',
				'To' => 'webmaster@dhs-club.com',
	       		'Subject'=> $return_text,
				'Content-Type'=> 'text/plain; charset="utf-8"',
				'Message' => $error
		);
	
		sendmail(%mail);
	}

	return $return_text
}

sub ParseLanguagePack
{
	my $content_xml = $gs->Get_Object($db, $TMPL{'content'}) || die "Failed to load XML template: $TMPL{'content'}\n";
	
	# it would be nice to parse the whole document in one swoop, but then when we insert HTML into the nodes, we would have problems with XMLin
	my $tmp = XML::LibXML::Simple::XMLin($content_xml, 'NoAttr'=>1);
	
	# it seems that TT doesn't work quite right with utf-8 characters and the utf-8 flag
	_utf8_off($tmp);
	
	my %data = (
		'application_link' => $q->a({'-href'=>'https://www.clubshop.com/cgi/appx/upg', '-onclick'=>'window.open(this.href);return false;'}, 'https://www.clubshop.com/cgi/appx/upg')
	);
	
	return ParseLanguagePackKeys($tmp, \%data);
}

sub ParseLanguagePackKeys
{
	my ($tmp, $data) = @_;

	my %rv = ();

	foreach (keys %{$tmp})
	{
		if (ref $tmp->{$_} eq 'HASH')	# the XML has nested nodes and so some of the keys will be hashrefs
		{
			$rv{$_} = ParseLanguagePackKeys($tmp->{$_}, $data);
			next;
		}
		
		
		$TT->process(\$tmp->{$_}, $data, \$rv{$_});
	}
	return \%rv;
}

=head3 PartnerUpgradePrice

	Get the Partner upgrade price
	
=head4 Arguments

	A subscription type, currency code and the number of months we are spannng for the upgrade
	
=head4 Returns

	subscription_pricing_current like record

=cut

sub PartnerUpgradePrice
{
	my ($stype, $ccode, $num_of_months) = @_;
	my $rv = $db->selectrow_hashref("
		SELECT pug.*, st.soa_trans_type
		FROM partner_upgrade_price(CAST(? AS SMALLINT), ?, CAST(? AS SMALLINT)) AS pug
		JOIN subscription_types st
			ON st.subscription_type=pug.subscription_type", undef, $stype, $ccode, $num_of_months);
	return $rv;
}

sub Pay_List
{
	$db->{'RaiseError'} = 0;	# we do not want a DB exception to squelch the progress of reporting of successful transactions
	my (%results) = ();
	my @payeeID = $q->param('payeeID');
	warn Dump(\@payeeID) if $DEBUG{'Pay_List_start'};
	$results{$_} = {} foreach @payeeID;

	
	# these parameters are our annuals
	foreach ($q->param('payitem'))
	{
		my $tmp = ExtractKeyVal( $q->param('payitem') );
		$results{$tmp->{'id'}}->{'annual'} = $tmp;
		$results{$tmp->{'id'}}->{'alias'} = $tmp->{'alias'};
	}

	foreach ($q->param)
	{
		# our monthly payitems are named payitem_<id number>
		next unless m/payitem_\d?/;

		my $tmp = ExtractKeyVal( $q->param($_) );

		$results{$tmp->{'id'}}->{'monthly'} = $tmp;
		$results{$tmp->{'id'}}->{'alias'} = $tmp->{'alias'};
	}
	
	warn Dump(\%results) if $DEBUG{'Pay_List_params'};

	foreach my $id (@payeeID)
	{
		if (ProcessAnnual($results{$id}->{'annual'}))
		{
			warn 'ProcessAnnual: ' . Dump($results{$id}->{'annual'}) if $DEBUG{'ProcessAnnual_return'};
			$results{$id}->{'linecost'} += $results{$id}->{'annual'}->{'current'}->{'usd'} if $results{$id}->{'annual'}->{'results'} eq '1';
		}
		
		if (ProcessMonthly($results{$id}->{'monthly'}))
		{
			warn 'ProcessMonthly: ' . Dump($results{$id}->{'monthly'}) if $DEBUG{'ProcessMonthly_return'};
			$results{$id}->{'linecost'} += $results{$id}->{'monthly'}->{'current'}->{'usd'} if $results{$id}->{'monthly'}->{'results'} eq '1';
		}

		$results{$id}->{'linecost'} = sprintf('%.2f', ($results{$id}->{'linecost'} || 0));
	}
	
	$db->{'RaiseError'} = 1;
	$balance = Get_Balance();
	Display_Page('pay', {'results'=>\%results, 'payeeID'=>\@payeeID, 'title'=>$lang_blocks->{'title'}});
}

sub Pay_UG_List
{
	$db->{'RaiseError'} = 0;	# we do not want a DB exception to squelch the progress reporting of successful transactions
	my (%results) = ();
	my @payeeID = ();
	
	foreach my $item ($q->param('payitem'))
	{
		my $tmp = ExtractKeyVal($item);
		$results{$tmp->{'id'}} = $tmp;
		push @payeeID, $tmp->{'id'};	# this is used to preserve sort order
	}
	warn Dump(\@payeeID) if $DEBUG{'Pay_List_start'};
	warn Dump(\%results) if $DEBUG{'Pay_List_start'};
	
	warn Dump(\%results) if $DEBUG{'Pay_List_params'};

	foreach my $id (@payeeID)
	{
		my $rv = Get_Balance();

		if (($rv - $results{$id}->{'upgrade_tmp'}->{'amount'}) < 0)
		{
			$results{$id}->{'results'} = $lang_blocks->{'messages'}->{'no_balance'};
		}
		else
		{
			$db->begin_work unless $TESTMODE;
	
			# first off transfer the funds to the upgrader
			($rv) = $db->selectrow_array("
				SELECT create_ca2ca_xfer(
					?,
					$id,
					15020,
					15020,
					$results{$id}->{'upgrade_tmp'}->{'amount'},
					'Transfer to $results{$id}->{'alias'}',
					?,
					NULL,
					NULL,
					NULL,
					NULL)
			", undef, $mem_info->{'id'}, "Transfer from $mem_info->{'alias'}"); 
			
			unless ($rv)
			{
				$results{$id}->{'results'} = $lang_blocks->{'messages'}->{'caxfer_failed'};
			}
			else
			{
				($rv) = $db->selectrow_array("SELECT upgrade_from_tmp($id,NULL,NULL)");
				
				unless ($rv)
				{
					$db->rollback unless $TESTMODE;
					$results{$id}->{'results'} = $db->errstr ? $db->errstr : $lang_blocks->{'messages'}->{'db_error'};
				}
				else
				{
					$db->commit unless $TESTMODE;
					$results{$id}->{'results'} = 1;
					$results{$id}->{'linecost'} = $results{$id}->{'upgrade_tmp'}->{'amount'};
					$results{$id}->{'current'} = GetMemberSubscriptionMonthly({'id'=>$id});
				}
			}
		}
		
		$results{$id}->{'linecost'} = sprintf('%.2f', ($results{$id}->{'linecost'} || 0));
	}
	
	$db->{'RaiseError'} = 1;
	$balance = Get_Balance();
	Display_Page('payug', {'results'=>\%results, 'payeeID'=>\@payeeID, 'title'=>$lang_blocks->{'pay4new'}});
}

sub ProcessAnnual
{
	my $data = shift;
	return unless $data;
	
	my $rv = Get_Balance();
#warn "balance: $rv usd: $data->{'pricing'}->{'usd'}";
	if (($rv - $data->{'pricing'}->{'usd'}) < 0)
	{
		$data->{'results'} = $lang_blocks->{'messages'}->{'no_balance'};
		return undef;
	}

	$db->begin_work unless $TESTMODE;
	$rv = $db->do("DELETE FROM subscriptions WHERE pk= $data->{'current'}->{'pk'}");
	unless ($rv eq '1')
	{
		$data->{'results'} = $db->errstr || 'Delete of existing annual subscription failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}
	
	($rv) = $db->selectrow_array("SELECT NEXTVAL('subscriptions_pk_seq')");
	unless ($rv)
	{
		$data->{'results'} = $db->errstr || 'Creation of annual subscription ID failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}

	$rv = $db->do("
		INSERT INTO subscriptions (
			pk,
			member_id,
			subscription_type,
			start_date,
			end_date,
			price,
			currency_code,
			pp_value,
			usd
		) VALUES (
			$rv,
			$data->{'id'},
			$data->{'pricing'}->{'subscription_type'},
			'$data->{'current'}->{'end_date'}'::DATE + 1,
			'$data->{'current'}->{'end_date'}'::DATE + (SELECT term FROM subscription_types WHERE subscription_type= $data->{'pricing'}->{'subscription_type'}),
			$data->{'pricing'}->{'amount'},
			'$data->{'pricing'}->{'currency_code'}',
			$data->{'pricing'}->{'pp'},
			$data->{'pricing'}->{'usd'}
		)");
	unless ($rv)
	{
		$data->{'results'} = $db->errstr || 'Creation of new annual subscription failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}
	else
	{
		my $new = GetMemberSubscriptionAnnual({'id'=>$data->{'id'}});
		my $soa_trans_type = SubscriptionPricing($new)->{'soa_trans_type'};
		($rv) = $db->selectrow_array("
			SELECT debit_acct_create_trans(
				?,
				$soa_trans_type,
				$new->{'usd'} * -1,
				('$data->{'id'}:$new->{'pk'} $new->{'start_date'} ~ $new->{'end_date'}')::character varying,
				NULL::INTEGER,
				(current_user)::character varying)", undef, $mem_info->{'id'});
		unless ($rv)
		{
			$data->{'results'} = $db->errstr || 'Creation of new annual subscription debit failed';
			$db->rollback unless $TESTMODE;
			return undef;
		}
		else
		{
			$rv = $db->do("UPDATE subscriptions SET soa_trans_id= $rv WHERE pk= $new->{'pk'}");
			unless ($rv)
			{
				$data->{'results'} = $db->errstr || 'Creation of new annual subscription failed';
				$db->rollback unless $TESTMODE;
				return undef;
			}
		}
		
		# replace the old 'current' with the new
		$data->{'current'} = $new;
	}
	$db->commit unless $TESTMODE;
	$data->{'results'} = 1;
}

sub ProcessMonthly
{
	my $data = shift;
	return unless $data;

	my $rv = Get_Balance();
	if (($rv - $data->{'pricing'}->{'usd'}) < 0)
	{
		$data->{'results'} = $lang_blocks->{'messages'}->{'no_balance'};
		return undef;
	}
	
	# let's make sure if we do this one that we don't go over the edge of doing too many
	# first we will add one to the count and test to see if we are OK
	# regardless, we will remove the 1 we added and let it really be added in at the end of the processing if we are successful
	$fta->update_count(1) if  $mem_info->{'id'} != $data->{'id'};
	my $flag = $fta->approved;
	$fta->update_count(-1);
	unless ($flag)
	{
		warn Dump($data) if $DEBUG{'ProcessMonthly'};
		$data->{'results'} = nonFTAerr($fta->count);
		return undef;
	}
	
	$db->begin_work unless $TESTMODE;

	$rv = $db->do("DELETE FROM subscriptions WHERE pk= $data->{'current'}->{'pk'}");

	unless ($rv eq '1')
	{
		$data->{'results'} = $db->errstr || 'Delete of existing monthly subscription failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}
	
	($rv) = $db->selectrow_array("SELECT NEXTVAL('subscriptions_pk_seq')");
	unless ($rv)
	{
		$data->{'results'} = $db->errstr || 'Creation of monthly subscription ID failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}

	# this is the logic surrounding these dates
	# if we are doing a renewal, the current subscription will almost always end last month.. so we want to start on the first of this month
	# if for some edge case their current ends a prior month, we still want to pick up starting this month
	# If we are doing an upgrade, we won't be charging them for any months already elapsed in their current subscription, so we will start this month
	# ... bottom line, the first_of_month() is always the correct value
	#
	# For the end date, a renewal will only have a one month term, so we will go with the last_of_month()
	# For an upgrade, we will simply use the existing term end date.
	my $end_date = $data->{'pricing'}->{'itemtype'} eq 'upgrade' ? "'$data->{'current'}->{'end_date'}'::DATE" : 'last_of_month()';

	$rv = $db->do("
		INSERT INTO subscriptions (
			pk,
			member_id,
			subscription_type,
			start_date,
			end_date,
			price,
			currency_code,
			pp_value,
			usd
		) VALUES (
			$rv,
			$data->{'id'},
			$data->{'pricing'}->{'subscription_type'},
			first_of_month(),
			$end_date,
			$data->{'pricing'}->{'amount extended'},
			'$data->{'pricing'}->{'currency_code'}',
			$data->{'pricing'}->{'pp'},
			$data->{'pricing'}->{'usd extended'}
		)");
	unless ($rv)
	{
		$data->{'results'} = $db->errstr || 'Creation of new monthly subscription failed';
		$db->rollback unless $TESTMODE;
		return undef;
	}
	else
	{
		my $new = GetMemberSubscriptionMonthly({'id'=>$data->{'id'}});
		my $soa_trans_type = SubscriptionPricing($new)->{'soa_trans_type'};
		($rv) = $db->selectrow_array("
			SELECT debit_acct_create_trans(
				?,
				$soa_trans_type,
				$new->{'usd'} * -1,
				('$data->{'id'}:$new->{'pk'} $new->{'start_date'} ~ $new->{'end_date'}')::character varying,
				NULL::INTEGER,
				(current_user)::character varying)", undef, $mem_info->{'id'});
		unless ($rv)
		{
			$data->{'results'} = $db->errstr || 'Creation of new monthly subscription debit failed';
			$db->rollback unless $TESTMODE;
			return undef;
		}
		else
		{
			$rv = $db->do("UPDATE subscriptions SET soa_trans_id= $rv WHERE pk= $new->{'pk'}");
			unless ($rv)
			{
				$data->{'results'} = $db->errstr || 'Creation of new monthly subscription failed';
				$db->rollback unless $TESTMODE;
				return undef;
			}
		}

		# create our PP transactions in the future if our subscription is ending beyond this month
		if ($data->{'pricing'}->{'xfactor'} > 1)
		{
			for (my $x=1; $x <= ($data->{'pricing'}->{'xfactor'} -1); $x++)
			{
				my ($start_date) = $db->selectrow_array("SELECT ('$new->{'start_date'}'::DATE + INTERVAL '$x months')::DATE");
				($rv) = $db->selectrow_array("SELECT NEXTVAL('transactions_trans_id_seq')");
				$rv = $db->do("
					INSERT INTO transactions (
						trans_id,
						id,
						trans_type,
						amount,
						currency_code,
						native_currency_amount,
						trans_date,
						description
					) VALUES (
						$rv,
						$new->{'member_id'},
						$SubscriptionTypes->{$new->{'subscription_type'}}->{'trans_type'},
						0, --_subscription_pricing.usd,
						'$new->{'currency_code'}',
						0, --_subscription_pricing.price,
						'$start_date',
						'Subscription ID: $new->{'pk'}'
					);
					
					INSERT INTO transactions_extensions (trans_id, pp_value) VALUES ($rv, $new->{'pp_value'});
				");
				
				unless ($rv)
				{
					$data->{'results'} = $db->errstr || 'Creation of PP transactions in the future failed';
					$db->rollback unless $TESTMODE;
					return undef;
				}
			}
		}
		
		# replace the old 'current' with the new
		$data->{'current'} = $new;
	}
	$db->commit unless $TESTMODE;
	$fta->update_count(1);
	$data->{'results'} = 1;
}

=head3 SetRenewalStype

	When doing renewals, we cannot simply compare the existing subscription type as it may be an initial.
	So we will set a hash key 'renewal_stype' with a value for the renewal subscription type.
	Obviously, if they are already subscribed with a renewal subscription type, the renewal_stype will be the same.

=head4 Arguments

	A hashref with a 'subscription_type' key/value pair.
	
=head4 Returns

	This function will actually add the renewal_stype key/value pair to the hashref, but it will also return the hashref.
	
=cut

sub SetRenewalStype
{
	my $hr = shift;
	die "SetRenewalStype argument must be a hashref with a 'subscription_type' key/value pair" unless $hr && ref $hr eq 'HASH' && $hr->{'subscription_type'};
	
	# if our existing subscription_type is a renewal, we are set
	if ( $SubscriptionTypes->{ $hr->{'subscription_type'} }->{'renewal'} )
	{
		$hr->{'renewal_stype'} = $hr->{'subscription_type'};
	}
	else
	{
		$hr->{'renewal_stype'} = $SubscriptionTypes->{ $hr->{'subscription_type'} }->{'related'};
	}
	
	return $hr;
}

=head3 SubscriptionPricing

	Get a set of subscription prices

=head4 Arguments

	Expects a hashref with at least a 'currency_code' key.
	If the hashref also contains a subscription_type key, the results will be filtered on that type.

=head4 Returns

	A hashref of subscription_pricing_current like records.

=cut

sub SubscriptionPricing
{
	my $args = shift;
	warn Dump($args) if $DEBUG{'SubscriptionPricingIn'};
	
	die "Did not receive the required currency_code key" unless $args->{'currency_code'};
	
	# by stashing our data in the global hash, we effectively cache it for reuse
	unless ($SUBSCRIPTION_PRICING{ $args->{'currency_code'} })
	{
		$SUBSCRIPTION_PRICING{ $args->{'currency_code'} } = $db->selectall_hashref("
			SELECT
				spc.currency_code,
				spc.subscription_type,
				spc.usd,
				spc.pp,
				spc.amount,
				st.soa_trans_type
			FROM subscription_pricing_current spc
			JOIN subscription_types st
				ON st.subscription_type=spc.subscription_type
			WHERE st.sub_class IN ('VA','VM')
			AND spc.currency_code= '$args->{'currency_code'}'
			", 'subscription_type');
	}
	
	return $SUBSCRIPTION_PRICING{ $args->{'currency_code'} }->{ $args->{'subscription_type'} } if $args->{'subscription_type'};
	return $SUBSCRIPTION_PRICING{ $args->{'currency_code'} };
}

=head3 SubscriptionUpgradePricing

	Take a subscription_pricing recordset compared to another subscription_pricing recordset and create a set of subscription prices that reflect the differences
	
=head4 Arguments

	A hashref containing a 'subscription_type' key and a 'currency_code' key
	A subscription_type to compare with

=head4 Returns

	A hashref that should be identical to the structure of a %SUBSCRIPTION_PRICING record
	
=cut

sub SubscriptionUpgradePricing
{
	my $working_id = shift;
	my $base = shift;
	my $compare_stype = shift;
	my $xfactor = shift || 1;
	
	warn "SubscriptionUpgradePricing base: " . Dump($base) if $DEBUG{'SubscriptionUpgradePricingIn'};
	warn "SubscriptionUpgradePricing compare_stype: $compare_stype" if $DEBUG{'SubscriptionUpgradePricingIn'};
	
	return () unless $compare_stype;
	
	$base = SubscriptionPricing($base);
	my %rv = %{	SubscriptionPricing({'subscription_type'=>$compare_stype, 'currency_code'=>$base->{'currency_code'}}) };
	$rv{'xfactor'} = $xfactor;	# save this value for use later on
	
	warn "SubscriptionUpgradePricing rv hash: " . Dump(\%rv) if $DEBUG{'SubscriptionUpgradePricingIn'};
	
	# now go through our monetary fields and determine the differences
	foreach my $col (qw/amount pp usd/)
	{
		$rv{ "$col extended" } = sprintf('%.2f', ($rv{ $col } -= $base->{ $col }) * $xfactor);
	}
	
	# make our currency fields a standard format... this is not always accurate for some currencies that don't have precisely 2 digits after the decimal, but...
	foreach (qw/amount usd/)
	{
		$rv{$_} = sprintf('%.2f', $rv{$_});
	}
	
	# we are only extending this free offer to the Partner themselves... they cannot "upgrade" someone else for free
	if ($FREE_PRO && $rv{'subscription_type'} == 3 && $working_id == $mem_info->{'id'})
	{
		my ($prps) = $db->selectrow_array("SELECT prvs FROM refcomp WHERE id= $working_id") || 0;
		if ($prps >= 3)
		{
			foreach(qw/usd pp amount/)
			{
				$rv{$_} = 0;
				$rv{"$_ extended"} = 0;
			}
		}
	}

	return \%rv;
}

sub _utf8_off
{
        my $hr = shift;
        foreach (keys %{$hr})
        {
                if (ref $hr->{$_} eq 'HASH')
                {
                	_utf8_off($hr->{$_});
                }
                else
                {
                	Encode::_utf8_off($hr->{$_});
                }
        }
}

# this package was resurrected from the old interface to reimplement the enforcement of fee transmission agent requirements

package fta;

my @approvals = ();
my $count = 0;

sub new
{
	my $id = shift;
	my $db = shift;
	my $sth = $db->prepare('SELECT * FROM fee_transmission_agents_vw WHERE id=?');
	$sth->execute($id);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @approvals, $tmp;
	}
	
	($count) = $db->selectrow_array("
		SELECT COUNT(DISTINCT s.member_id)
		FROM soa
		JOIN subscriptions s
			ON soa.trans_id=s.soa_trans_id
		JOIN subscription_types st
			ON s.subscription_type=st.subscription_type
		WHERE s.member_id != soa.id
		AND soa.entered >= first_of_month()
	-- we will only check for monthly transactions... they can pay all the annuals they want :)
		AND st.sub_class= 'VM'
		AND s.void=FALSE
		AND soa.void=FALSE
		AND soa.id= ?", undef, $id) || 0;
	
	# for whatever reason, this would not get pulled in as a value below if it was alongside the other 2 global package vars
	my $nonFTAlimit = 3;	# if they are not registered as a Fees Transmission Agent, they can pay a max of this many monthly fees/upgrades
	
	return bless {'db'=>$db, 'id'=>$id, 'max_count'=>$nonFTAlimit, 'initial_count'=>$count};
}

sub approved
{
	my $me = shift;
	return 1 if @approvals;
	return 1 if $count <= $me->{'max_count'};

	return 0;
}

sub count
{
	return $count;
}

sub country_authorized_crossline($)
{
	my ($me, $ccode) = @_;
	my ($rv) = $me->{'db'}->selectrow_array('
		SELECT country_code FROM fee_transmission_agent_countries
		WHERE crossline_auth=TRUE AND country_code=? AND id= ?',
		undef,
		$ccode,
		$me->{'id'});
	return $rv;
}

sub currency_approved($)
{
	my $me = shift;
#	return 1 if $me->bypass();
	# if we are below the max, we are theoretically approved for any currency
	return 1 if $count < $me->{'max_count'};
	my $currency_code = shift;
	foreach (@approvals)
	{
		return 1 if $currency_code eq $_->{'currency_code'};
	}
	return 0;
}

sub update_count($)
{
	my $me = shift;
	my $inc = shift;
	$count += $inc;
}
# END OF package fta

__END__

07/03/12
 	Made it so all the VM subscription types are pulled in instead of just the renewals.
	This is necessary for when a list is built in SQL using the weight... no subscription type, no weight = DB error
	
07/10/12
	Added the _utf8_off routine to use in processing the keys of the imported language pack

07/17/12
	Made it so that the membertype of fees parties is checked to ensure that they are Partners so as to avoid allowing the payment of fees for those who are not.
	
09/06/12
	Added support for the BASIC PLUS subscription type
	as well as changing the way we handle subscription upgrades on what should really be considered an expired subscription, ie: end_date < now
	Also took the logic for determining the upgrade price out of here and put it in the DB for central usage.

10/06/13
	Added the Fee Transmission Agent limitation system.
	
	Also, debugged a thorny issue, discovered during testing of the FTA, where multiple items with the same subscription type and currency code would not be processed properly.
	In short it had to do with modifying a reference to a cached subscription pricing record.

03/25/14
	Implemented the free PRO subscription upgrade to those who have at least 3 PRPs

10/25/14
	Enabled payers to pay for somebody NOT in their organization as long as they are in the same country
	(although not done in here, also revised the fta vw in the DB to not enforce any kind of pay level limit since we keep degrading the requirement and that is hidden away)

11/17/14
	Replicated the 10/25 work in Confirm_Upgrade_List as well...

11/24/14
	Made the call to fee_transmission_agents recognize the 'active' flag.... dummy, that is using the VIEW which already incorporates the flag... reversed
	
