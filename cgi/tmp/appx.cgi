#!/usr/bin/perl -w

=head1 NAME:

	appx.cgi

=head1 DESCRIPTION:

	A member application based upon xml and xslt technology.

=head1 DEPLOYED:

	09/15/05	Bill MacArthur

=head1 MODIFIED:

	04/14/16	Bill MacArthur

=head1 PREREQUISITS:

	G_S qw($SALT $LOGIN_URL $ERR_MSG);
	CGI
	DB_Connect;
	MailingUtils;
	Encode;
	String::Random;													
	Data::Dumper;
	XML::Simple;
	XML::local_utils;
	ParseMe;
	CGI::Carp qw(fatalsToBrowser set_message);
	Digest::MD5 qw( md5_hex );
	Crypt::CBC;
	Mail::Sendmail;
	'excluded_domains.lib';
	'appx/admin.pm';
	Email::Valid

=cut

use strict;
use FindBin;

# this is needful so that the script can look in the GI cgi-lib directory when invoked from within that tree
use lib "$FindBin::Bin/../cgi-lib",'/home/httpd/cgi-lib';
# this one only needed when testing in a cgi/tmp directory

#use lib "$FindBin::Bin/../../cgi-lib"; 

#my $BaseDir = "$FindBin::Bin/..";
my $BaseDir = '/home/clubshop.com';	# no more GI

use appx::APPX qw(%APPXDEFS);
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use DB_Connect;
use MailingUtils;
use Encode;
use String::Random;													
use Data::Dumper;
require XML::Simple;
require XML::local_utils;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser set_message);
set_message($ERR_MSG);
use Digest::MD5 qw( md5_hex );
require Crypt::CBC;
use Mail::Sendmail;
require 'excluded_domains.lib';
require 'appx/admin.pm';
use Email::Valid;
use Login::csLogin;

=head1 CONSTANTS:

	$APP_AGE

=cut
					
###### CONSTANTS
our $APP_AGE = "30 days";	# An SQL Interval - how long we'll hold the process_state record.

=head1 GLOBALS:

	$q	obj	CGI Object
	$gs	obj	new G_S;
	$cipher	obj	Crypt::CBC->new({ key=>$SALT, cipher=>'DES'} );
	$script_num	int	The number of the application for the process_state table.
	$ME	string	script name
	$xs	obj new XML::Simple(NoAttr=>1);
	%TMPL	hash
	{
		'config_xml'		=> 10540,
		'content_xml'		=> 10406,	# initial value only, see Content_XML()
		'countries_xml'	=> 10407,
		'remove_txt'		=> 99,
		'language_pref_xml'	=> 10431,
		'xml_err'		=> 10435,	# a simple error handler template
		# the 'error' screen for an applicant with an existing temp membership
		'tmp_mbr_found'	=> 10447,
		# the 'error' screen for an applicant with multiple existing temp memberships
		'tmp_mbr_found_mult'	=> 10448,
		'confirmation_email'	=> 10416
	}

	$xml_tmpl = ();

	%Bypass	hash
	{	
		'temp_hash_insertion'	=> 1 # set these to 1 to bypass those portions
	}

	$mbr_qry strint	query

	$config_xml = our $data_xml = our $content_xml = '';

	($db, $config_hash, $sponsor, $app_type, @errors, $app_step, @email_matches) = ();
	%params hash
	$state = ();		# our session state data
	$mem_info = UserCK();	# a hashref of our logged in user (or undef if none)
	$member = ();		# this will be a hashref of our 'new' member data at some points
	$RedoFlag = ();		# a shared flag to restart our main 'loop' - *careful now*
	$SkipDisplay = ();	# a shared flag to bypass the Display routine
				# this would typically be used after a trapped error

	# a new item as of 08/14/08... intended to break out of processing further items in any given stage
	# in order to not bomb completely and typically call a redo

	$BreakStageProcessing = ();

	@cookies = ();		# push cookies in to have them spit out with the headers
	$states = ();		# a hashref of state/province information

	$ADMIN = Admin_Mode_Ck({'ck_only'=>1});
	$Merchant_login = MerchantLoginCK();
	$VIP_login = 1 if $mem_info && $mem_info->{membertype} eq 'v';

	# not to be confused with the 'parent' of this membership
	# this hash can hold keys that hold hashrefs to responsible parties upline
	%upline = ();

	###### rather than stuffing extra bits of data into the param hash
	###### we'll include an extra node under 'data' called 'extras'
	###### this hash will be the pot for those bits
	%extras = ();

	%non_xml_extras	- hash of "stuff" that will not be XMLized
	
	# if there is tracking code that belongs on the page, we'll stuff it into this key from within a configuration step
	$non_xml_extras{'tracking'}
	
	###### available to modules as necessary rather than having to do it each time in those
	###### extract our path information	
	# remember to use natural placeholder selectors ie. (1) is the first path element
	# the first (0) value is undef since the path starts with a /
	@path_info = split /\//, $q->path_info;

	$Wrap ... in certain instances a module will set this variable to a 'wrap' object which Display_Page will recognize
=cut

###### GLOBALS
our ($db, $config_hash, $sponsor, $app_type, @errors, $app_step, @email_matches, $Wrap, %non_xml_extras, %cache, $content_xml_hashref) = ();
our $q = new CGI;
our $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
our $cipher = Crypt::CBC->new({ 'key'=>$SALT, 'cipher'=>'DES'} );
our $script_num = 1;		# The number of the application for the process_state table.
our $ME = $q->script_name;
our $xs = new XML::Simple('NoAttr'=>1);
our %TMPL = (
	'config_xml'		=> 10540,
	'content_xml'		=> 10406,	# initial value only, see Content_XML()
	'countries_xml'	=> 10407,
	'remove_txt'		=> 99,
	'language_pref_xml'	=> 10431,
	'xml_err'		=> 10435,	# a simple error handler template
	# the 'error' screen for an applicant with an existing temp membership
	'tmp_mbr_found'	=> 10447,
	# the 'error' screen for an applicant with multiple existing temp memberships
	'tmp_mbr_found_mult'	=> 10448,
	'confirmation_email'	=> 10416
);
###### only used in Content_XML to determine if a template load/reload is necessary
###### needs to retain value between calls
our $xml_tmpl = ();

our %Bypass = (	# set these to 1 to bypass those portions
	'temp_hash_insertion'	=> 1
);
our $mbr_qry = "
	SELECT m.id, m.alias, m.spid, m.membertype,
		m.firstname, m.lastname,
		COALESCE(m.country, '') AS country,
		COALESCE(m.emailaddress, '') AS emailaddress,
		COALESCE(m.password, '') AS password,
		COALESCE(m.language_pref, '') AS language_pref,
		COALESCE(m.cellphone, '') AS cellphone,
		COALESCE(m.phone, '') AS phone,
		m.memo,
		m.oid,
		COALESCE(m.company_name, '') AS company_name
	FROM members m WHERE ";
our $config_xml = our $data_xml = our $content_xml = '';

our %params = ();
our $state = ();		# our session state data
our $mem_info = UserCK();	# a hashref of our logged in user (or undef if none)
our $member = ();		# this will be a hashref of our 'new' member data at some points
our $RedoFlag = ();		# a shared flag to restart our main 'loop' - *careful now*
our $SkipDisplay = ();	# a shared flag to bypass the Display routine
				# this would typically be used after a trapped error

# a new item as of 08/14/08... intended to break out of processing further items in any given stage
# in order to not bomb completely and typically call a redo
our $BreakStageProcessing = ();

our @cookies = ();		# push cookies in to have them spit out with the headers
our $states = ();		# a hashref of state/province information

our $ADMIN = Admin_Mode_Ck({'ck_only'=>1});
our $Merchant_login = MerchantLoginCK();
our $VIP_login = 1 if $mem_info && $mem_info->{'membertype'} eq 'v';

# not to be confused with the 'parent' of this membership
# this hash can hold keys that hold hashrefs to responsible parties upline
our %upline = ();

###### rather than stuffing extra bits of data into the param hash
###### we'll include an extra node under 'data' called 'extras'
###### this hash will be the pot for those bits
our %extras = ();

###### available to modules as necessary rather than having to do it each time in those
###### extract our path information
# remember to use natural placeholder selectors ie. (1) is the first path element
# the first (0) value is undef since the path starts with a /
our @path_info = split(/\//, $q->path_info);

################################
###### MAIN starts here.
################################

# Connect to the db.
exit unless $db = DB_Connect('appx');
$db->{'RaiseError'} = 1;
$db->{'pg_enable_utf8'} = 1;

if (Ip_Blocked())
{
	Err('<h4>IP BLOCK<br />' . $q->remote_addr .
		' is blocked from using this application.</h4>
		Please contact mbrsupport@dhs-club.com for further assistance.');
	Exit();
}

# Get the application config xml and put it in a hash for processing.
$config_xml = $gs->GetObject($TMPL{'config_xml'}) || die "Failed to load XML template: $TMPL{'config_xml'}\n";
$config_hash = $xs->XMLin("<opt>$config_xml</opt>", 'NoAttr'=>0);

###### we should be able to determine the app_type at this time
die "Failed to determine application type" unless $app_type = DetermineAppType();
$params{'app_type'} = $app_type;

###### pull in any 'track' specific routines
###### they should be available in the cgi-lib/appx directory as track-name .pm
###### by using 'require' instead of 'use', we avoid crashing the app if the module
###### is non-existent or broken
# (it'll only be broken for that track if the module is broken)
#warn "require 'appx/$app_type" . '.pm\'' if $APPXDEFS{'ACCTTYPE'} eq 'GI';
eval ("require '" . $APPXDEFS{'APPXLIBS'} . "/$app_type" . '.pm\'');
warn $@ if $@;

###### we should be able to determine the app_step at this time as well
die "Failed to determine application step" unless DetermineAppStep();

# we are adding this part to keep 'free' members or 'hackers' from flooding the application
Exit() if IP_Restriction_CK();

###### a quick hack for Will
###### we will record the referer if this is a non-VIP member application
###### in case there are any errors/problems here, we don't want to be bombing the whole appx system
if ( (grep $app_type eq $_, qw/mm ba default default_01/) &&
	$app_step eq 'step0' &&
	$q->referer()
){
	eval(q| $db->do('INSERT INTO tracking_appx_referers (referer) VALUES (?)', undef, $q->referer()) |);
}

our $cfg = new _my_private_;

# Load the params into a local hash.
Load_Params();

# call our module init block to initialize our module global vars, etc.
eval("$app_type" .'::init()');
warn $@ if $@ && $@ !~ /Undefined subroutine &\w+::init/;

	###### We will start a 'loop' here.
{	###### If we want to restart processing, we can issue a 'redo'.
	###### More elegant than a 'goto'. If necessary, we can move this up higher.
$RedoFlag = undef;	# careful now, we don't want to get in an infinite loop

###### we are doing these before checking params, so keep that in mind
###### we could receive some bogus stuff
foreach(@{$cfg->operations('stage0')})
{
	my $brk = eval $_;
	die $@ if $@;
	Exit() unless $brk;
	last if BreakStageProcessing();
}

redo if $RedoFlag;

Check_Params();

# we are going to provide the ability to coerce the application language via a param
$gs->CoerceLangPref($params{'dl'}) if $params{'dl'};

Load_States() if $cfg->vars('load_states');
Check_Standard_State();	# perhaps this should go after 'pre'? we'll try it here first

unless (@errors)
{
	foreach(@{$cfg->operations('pre')})
	{
		my $brk = eval $_;
		die $@ if $@;
		Exit() unless $brk;
		last if BreakStageProcessing();
	}
}

# can we assume that if the redo flag is set, that we can ignore any errors on the list?
redo if $RedoFlag;

if (@errors)
{
	# if there are errors in the application, we don't want to move to the next step
	# by setting the next step value, to our current step, we'll be sure to come back
	###### This same functionality may come in handy for other things later on :)
	$params{'next_step'} = $app_step;

	# This is a hack, but I think it will work out OK.
	# If not then we may have to include error_comp or other mechanisms
	# to ensure that the data structures are in place when needed.
	# The idea is that the current step may not have the 'vars' defined, so we need
	# to define them. Otherwise, the needed data won't be loaded.
	$cfg->vars('load_languages', 1) if defined $params{'language_pref'};
	$cfg->vars('load_countries', 1)
		if (defined $params{'country'} || defined $params{'mop_country'});
	$cfg->vars('load_states', 1) if defined $params{'state'};

	foreach(@{$cfg->operations('error_comp')})
	{
		my $brk = eval $_;
		die $@ if $@;
 		Exit() unless $brk;
	}
}
else
{
	foreach(@{$cfg->operations('post')})
	{
		my $brk = eval $_;
		die $@ if $@;
		Exit() unless $brk;
		last if BreakStageProcessing();
	}
}

redo if $RedoFlag;

# Now build out the page.
Display_Page() unless $SkipDisplay;

last;	###### out-of-control LOOP buster
}	###### the end of our 'loop' :)

Exit();

=head1 SUBROUTINES:

=cut

################################
###### Subroutines start here.
################################

sub Admin_Mode_Ck
{	###### are we staff logged into the database
	my $args = shift;

	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	###### we'll swap out DB connections if this is admin
	if ( ($q->cookie('operator') && $q->cookie('pwd')) &&
		(my $dbh = ADMIN_DB::DB_Connect( 'appx', $q->cookie('operator'), $q->cookie('pwd') )))
	{
		$db->disconnect if defined $db;
		$db = $dbh;
		return { operator => $q->cookie('operator'), pwd => $q->cookie('pwd') };
	}
	Err("Database login required for this track.") unless exists $args->{'ck_only'};
	return;
}

sub BreakStageProcessing
{
	if ($BreakStageProcessing)
	{
		$BreakStageProcessing = ();
		return 1;
	}

	return ();
}

###### Build and combine the different menus.
sub Build_Menus
{
	###### Since this is called elsewhere before errors are processed,
	###### the current step configuration  may not have called it, but it may
	###### be needed in an error handling state.
	Load_States() if $cfg->vars('load_states');

	my $menus = '';
	# Load the country menu into an xml block.
	$menus .= ($gs->GetObject($TMPL{'countries_xml'}) ||
		die "Failed to load XML template: $TMPL{'countries_xml'}\n")
		if $cfg->vars('load_countries');

	# Load the language menu into an xml block.
	$menus .= ($gs->GetObject($TMPL{'language_pref_xml'}) ||
		die "Failed to load XML template: $TMPL{language_pref_xml}\n")
		if $cfg->vars('load_languages');

	if (keys %{$states})
	{
		$menus .= "<state>\n";
		foreach my $st(values %{$states})
		{
			$menus .= qq|<$st->{lcode} val="$st->{ucode}">$st->{name}</$st->{lcode}>\n|;
		}
		$menus .= "</state>\n";
	}
	return \$menus;
}

sub Cell_or_Telephone
{
	###### a simple routine to verify that we have a least one of the phone number fields present
	###### this routine can be called from a config as desired
#	return 1 if $main::params{phone} || $main::params{cellphone};
#	push @main::errors, ['phone','phone-footnote'], ['cellphone',''];
#	return 1;
	return OneOfMany(['phone','cellphone'],(['phone-footnote'],['phone'],['cellphone']));
}

sub Check_for_existing_Unconfirmed
{	###### the email dupe check is broader than this and only applies when wanted
	###### this will apply for all new memberships (at least as of now 05/13/05)
	# We should have an email address parameter, if not we'll say 'No Problem!'
	return 1 unless $params{'emailaddress'};
	my $args = shift || {};
	$args->{'membertype'} ||= 'x';

	my $rv = $db->selectall_hashref("
		$mbr_qry membertype='$args->{'membertype'}' AND emailaddress= ?",
		'id', undef, $params{'emailaddress'});

	my $cnt = scalar keys %{$rv};
	return 1 unless $cnt;
	push @errors, ['generic_error', ($args->{'temp_member_exists'} || 'temp_member_exists')];

	###### We may not want to do the rest of this stuff in some cases,
	###### like on VIP signups. They'll simply get the error message.
	return 1 if $args->{'ck_only'};

	if ($cnt == 1)
	{
		($member) = values %{$rv};
#		die "Failed to resend confirmation email\n"
#			unless Email({'xsl'=> Parse_Memo_for_Resend() }, $rv->{'language_pref'});
		my ($tmpl, $html) = Parse_Memo_for_Resend();

		my $_rv = ();
		if ($html)
		{
			$_rv = EmailHtmlMailings($tmpl);
		}
		else {
			$_rv = Email({'xsl'=>$tmpl}, $member->{'language_pref'});
		}
		die "Failed to resend confirmation email\n" unless $_rv;

		if ($args->{'no_display'}){
			# if we are calling this routine with this argument, we'd better recognize the non-standard return value
			return $rv;
		}
		else
		{
			Display_Page({ 'xsl' => ($args->{'display_template'} || $TMPL{'tmp_mbr_found'}) });
			return;
		}
	}
	else
	{	###### we found more than one match, I guess support is getting involved
		Display_Page({'xsl'=>$TMPL{'tmp_mbr_found_mult'}});
		return;
	}
}

sub Check_Membership_Dupe
{
	my $arg = shift || '';
	my %args = ('emailaddress' => $params{'emailaddress'});

	###### we do not want to allow VIP direct signups to resign a removal or other deactivated memberships
	###### basically by making the ignore_mtypes = '_' we really ignore none :-))
	if ($arg eq 'vip'){
		$args{'ignore_mtypes'} = '_';
	}

	my @list = $gs->MembershipDupeCK(\%args);
	return 1 unless @list;

	if ($arg eq 'strict')
	{
		###### format an HTML list of ID/s that match
		my $html = '';
		foreach(@list)
		{
			$html .= $q->div( $q->a({-href=>"/cgi/idlookup.cgi?id=$_->{'id'}"}, $_->{'alias'}));
		}
		Err_xml({list=>['dupe_error']}, $html);
	}
	elsif ($arg eq 'vip')
	{
		Err_xml({list=>['duplicate_email']});
	}
	elsif ($arg eq 'cb')
	{
		push(@errors, ['membertype','shopper_only']);
		return 1;
	}
	return;
}

sub Check_Standard_State
{	###### if they have submitted a state, we'll make sure that it matches
	###### a corresponding state that we have on file
	###### if the state hash is empty then we know that there are no states on file
	return unless (keys %{$states}) && $params{state};
	my $match;
	foreach my $st (values %{$states})
	{
		$match = 1 if $params{state} eq $st->{ucode};

		###### the following 2 cases shouldn't normally exist, but it could
		###### happen if they have javascript turned off
		if (	(uc $params{state}) eq $st->{ucode} ||
			(uc $params{state}) eq (uc $st->{name}))
		{
			$params{state} = $st->{ucode};
			$match = 1;
		}
		last if $match;
	}
	###### if we didn't find a match, then we'll report an 'error'
	push @errors, ['state', 'standard_state'] unless $match;
}

sub Check_Xcl_Dupe
{
	###### we cannot allow anybody in whose email matches a cancel/term in the last 6 months
	return 1 unless $params{emailaddress};
	my @rv = ();
	my $sth = $db->prepare("
		SELECT m.id, m.alias, m.emailaddress, m.membertype, m.firstname, m.lastname
		FROM members m
		JOIN cancellations c
			ON c.id=m.id
		WHERE c.stamp > NOW() - INTERVAL '6 months'
		AND m.emailaddress= ?
	");
	$sth->execute($params{emailaddress});
	while (my $tmp = $sth->fetchrow_hashref){
		push @rv, $tmp;
	}

	# if we have any matches, we could redirect them to a static page, or do a custom template
	# for now we'll just place this English notice
	return 1 unless @rv;
	push @errors, ['generic_error', 'A canceled membership has been found matching that email address'];
	$BreakStageProcessing = 1;
	return 1;
}

sub Check_Params
{
	###### create a unique list of required params and submitted params
	my %uniq = map {$_ => 1} ((keys %params), $cfg->req);

	###### by keeping all our checks within the same loop
	###### we can keep our errors grouped and sorted together
	foreach (sort {$cfg->sort($a) <=> $cfg->sort($b)} keys %uniq)
	{
	###### do our 'required' checks first
		if ($params{$_} eq '')
		{
			if ($cfg->req($_))
			{
				die "A required parameter is missing: $_"
					if $cfg->req_err_level($_) eq 'die';
				push @errors, [$_, 'missing_error']
					if $cfg->req_err_level($_) eq 'warn';
			}
			next;	###### no use continuing without a value
		}

	###### perform max-length validations
		if ( $cfg->length($_) && (length($params{$_}) > $cfg->length($_)) )
		{
			push @errors, [$_, 'length_error']	;
			next;
		}

	###### perform our field specific validation
		my $checks = $cfg->check($_); # we are receiving an arrayref or undef
		# if there is no defined validation or no param value, then we're done
		next unless $checks;
		foreach my $ck_item (@$checks)
		{
			# we can perform simple matches
			# if we don't care about setting up error messages
			# like if we are going to 'die' on error
			# For these we will be specifying the correct condition.
			# If that doesn't match, then the 'error condition' will be triggered.
			if ($ck_item =~ m#/#)
			{
				my $rv = eval('$params{$_}' . "$ck_item");
				if (! $rv)
				{
					die "A required parameter is invalid: $_\n" .
						"Value submitted: $params{$_}\n"
						if $cfg->error_level($_) eq 'die';
					push @errors, ['generic_error', "Invalid $_ parameter"]
						if $cfg->error_level($_) eq 'warn';
				}
			}
			else	###### we'll handle errors in the respective routines
			{
			###### we will look for names that begin with 'Ck_' and divert
			###### those accordingly
				if ($ck_item =~ /^Ck_/){ Ck_($ck_item, $params{$_}, $_) }
				else
				{
					eval($ck_item . '($_)');
					die "$@\nsub=$ck_item\nvalue=$params{$_}\nname=$_\n" if $@;
				}
			}
		}

		###### we can force some things here, like the domain block checker
		###### (we don't want it omitted from a configuration)
		Ck_('Ck_Excluded_Domains', $params{$_}, $_) if /emailadd/;

		###### we don't want the same first and last names
		###### by pattern matching on a possible prefix, we'll be covered
		###### on a direct to VIP signup where we'll be asking for a co-applicant
		if (/(.*)lastname$/ && $params{($1 || '') . 'firstname'})
		{
			push @errors, [$_, 'name_match_disallowed']
				if $params{($1 || '') . 'firstname'} eq $params{($1 || '') . 'lastname'};
		}
	}
}

sub Ck_	###### a catchall validation routine
{
	my $sub = shift;
	my $value = shift;
	my $name = shift || '';
	die "Missing validation identifier" unless $sub;

	###### I guess we'll return FALSE if there is nothing to check (for now anyway)
	return unless $value;

	# rv is our flag that we caught something
	# err_code is an error defined in our master xml document under the errors node
	# in the abscence of a predefined code, this could be some text to give some
	# kind of intelligible error

	###### we may sometimes want to pass some textual values in as well, like for Ck_Invalid_Characters
	###### since this has proven impossible due to the environment, we will improvise by extracting
	###### the text and creating an argument to pass in after the regular $value argument
	my $xargs = ();
	if ($sub =~ s/\((.*)\)//){	# we are looking for a set of parenthesis with some text inside
		$xargs = $1;
		###### get rid of leading and trailing single/double quotes
		$xargs =~ s/^('|")|('|")$//g if $xargs;
	}
	my ($rv, $err_code, $err_txt) = eval($sub . '($value, $xargs)');
	die "$@\nsub=$sub\nvalue=$value\nname=$name\n" if $@;
	if ($rv)
	{
		if ((! $name) || $cfg->error_level($name) eq 'die')
		{
			my $errstr = "A parameter is invalid: ${\$name || 'name not defined'}\n";
			$errstr .= "Value submitted: $value\n";
			###### a special case for Ck_Invalid_Characters which will report what characters were bad
			$errstr .= 'rv Error reported: ' . $q->escapeHTML($rv) . "\n" if $rv ne '1';
			$errstr .= "Error code reported: $err_code\n" if $err_code;
			$errstr .= "Error reported: $err_txt\n" if $err_txt;
			die $errstr;
		}
		elsif ($cfg->error_level($name) eq 'warn')
		{
			if ($err_code){ push @errors, [$name, $err_code] }
			elsif ($err_txt){ push @errors, ['generic_error', $err_txt] }
			elsif ($rv ne '1'){ push @errors, [$name, $q->escapeHTML($rv)]}
			else {
			# theoretically, this case should not normally happen
			# but if there is a configuration setup error it could
				my $errstr = "Validation failed for $name: " . $q->escapeHTML($value);
				push @errors, ['generic_error', $errstr];
			}
		}
		else { # I guess we do nothing
		}
	}
}

sub Ck_AlphaNum_Only
{
	my $rv = $_[0] =~ /\P{IsAlnum}/;
	return ($rv) ? (1, 'alphanum_error') : undef;
}

sub Ck_Blocked_Domain
{
	###### this catches common spelling errors and a few bogus domains
	my $rv = $db->selectrow_array("
		SELECT domain FROM domains_blocked WHERE ? LIKE ('\%\@' || domain)", undef, $_[0]);
	return ($rv) ? (1, 'blocked_domain') : undef;
}

sub Ck_Bogus_Email_Domain
{
	###### this catches common spelling errors and a few bogus domains
	my $rv = $db->selectrow_array("
		SELECT domain FROM domains_bogus WHERE ? LIKE ('\%\@' || domain)", undef, $_[0]);
	return ($rv) ? (1, 'email_domain_problem') : undef;
}

sub Ck_Digits_Only
{
	my $rv = $_[0] =~ /\D/;
	return ($rv) ? (1, 'digits_only') : undef;
}

sub Ck_Email_Domain_Exists
{
	###### we should, at this point, have a valid email address
	###### but if we don't then we cannot really evaluate the domain, can we?
	return unless $_[0] =~ m/.+?@(.+)/;
# let's go with a standard module
	return Email::Valid->mx($_[0]) ? undef : (1, 'email_domain_problem');

	my $rv = ();
	# We seem to have had problems with timeouts,
	# so we'll give it a couple extra tries.
	for (my $x=0; $x<3; $x++)
	{
		$rv = gethostbyname($1);

	# some domains return empty
	# they only have MX records
		$rv ||= qx(dig $1 mx +short);
	# of course, just because we got something doesn't mean it is good
	# the actual DNS record may be a null MX like 127.0.0.1 or something like that
	# but at least this is a step in the right direction
		next unless $rv;
	# neither gethostbyname nor dig return an IP address in the current code
	# so this test is ineffective and actually bombs if the mx priority is -0-
	#	undef $rv if $rv =~ /^127|^0/;
		last;
	}
	return ($rv) ? undef : (1, 'email_domain_problem');
}

sub Ck_Email_Match
{
	###### since we need to compare with a local variable, this check is not
	###### as well suited for the Ck_ style of checking
	###### we have to hard code the value for the comparison
	return ($_[0] ne $params{'email_check'}) ?
		(1, 'email_check_failure') : undef;
}

sub Ck_Excluded_Domains
{
	return ($_[0] =~ /^abuse/i ||
		eval 'grep $_[0] =~ /$_$/i, @excluded_domains::LIST'
	) ? (1, 'excluded_domain') : undef;
}

sub Ck_For_Removes
{
	###### I don't think there was a comparable function in the old app
	###### but with the ability to have multiple memberships, we must check now
	my $rv = $db->selectrow_array("
		SELECT id FROM members WHERE membertype= 'r' AND emailaddress=?", undef, $_[0]);
	return ($rv) ? (1, 'email_remove_match') : undef;
}

sub Ck_Invalid_Characters
{
	###### our second argument should be a regex list of invalid characters like '&amp;' or '&amp;|,'
	my $rv = $_[0] =~ /($_[1])/;
	return $rv ? "Invalid Characters: $1" : undef;
}

sub Ck_Letters_Only
{
###### I don't believe we are using this now(04-06-05)
###### but we'll save it as a model for that kind of test
	$_ = shift;
	defined || return;
	my $field = shift;
	my $ignore_errs = shift;

	###### we check for digits and then for anything besides digits or letters
	###### (there is no utf8 shortcut for letters only)
	if (/\p{IsDigit}|\P{IsAlpha}/){}
}

sub Ck_Letters_Punc_Only
{
###### I don't believe we are using this now(04-06-05)
###### but we'll save it as a model for that kind of test
	$_ = shift;
	defined || return;
	my $field = shift;

	###### we will allow some forms of punctuation
	###### so in order to not trigger the following, we'll just remove those here
	s/`|'|\.|\:\&|,|#|\/|\\//g;
	if ( Ck_Letters_Only($_, $field, 1) ){}
}

sub Ck_No_Digits
{
	my $rv = $_[0] =~ /\d/;
	return ($rv) ? (1, 'alpha_error') : undef;
}

sub Ck_Password_Match
{
	###### since we need to compare with a local variable, this check is not
	###### as well suited for the Ck_ style of checking
	###### we have to hard code the value for the comparison
	return undef if $_[0] eq $params{'password_check'};
	push @errors, ['password_check',''];
	return (1, 'password_check_failure');
}

sub Ck_Valid_Email
{
	return (1, 'invalid_email_address') if ( $gs->Check_Email($_[0]) == 0 );
	return;
}

sub Ck_Valid_Tin
{
	###### ensure we have a valid taxpayer identification number
	###### this could be an SSN or an EIN
	use SSN::Validate;
	#my $ssn = SSN::Validate->new({'ignore_bad_combo' => 1});
	
	my $ssn = SSN::Validate->new();
	return undef if $ssn->valid_ssn($_[0]);
	return undef if $_[0] =~ m/\d{2}-*\d7/;
	push @errors, ['generic_error', 'Invalid SSN/EIN: Should be xxx-yy-zzzz or xx-yyyyyyy'];
	return 1;
}

sub Confirm_Membership
{	###### at this time there is no error handling built into the page that will
	###### come up, nor is there a return path, so errors end here
	$member->{'id'} = $db->selectrow_array("SELECT id FROM members
		WHERE alias= ? AND oid= ? AND password= ?", undef, (
		$params{'alias'},
		$params{'oid'},
		$params{'temp_password'}
	)) || die "Failed to retrieve member record with params provided\n";

	my $qry = 'UPDATE members SET membertype= ?, memo= memo || ?, ';
	my @list = (
		($params{'membertype'} || 'm'),	# our default membertype
		("\nconfirm IP: " . $q->remote_addr)
	);

	# we will try to update simply using the received parameters as our criteria
	foreach (qw/mail_option_lvl password/)
	{
		next unless defined $params{$_} && $params{$_};
		$qry .= "$_ = ?, ";
		push @list, $params{$_};
	}
	###### get rid of our last comma and tack on our SQL criteria
	###### since someone apparently managed to 'confirm' a new VIP upgrade
	###### we will add a brute force check here
	$qry =~ s/, $/ WHERE membertype='x' AND id= $member->{'id'}/;
	my $rv = $db->do($qry, undef, @list);
	unless ($rv eq '1')
	{
		my $err = $DBI::errstr || 'Undefined Error';
		Err("We couldn't update your member record. Error: $err<br />\n" .
		"Please contact <a href='mailto:mbrsupport\@dhs-club.com'>mbrsupport\@dhs-club.com</a>" .
		" with your member ID: $params{'alias'} for further assistance");
		return;
	}
	else
	{
		# we need to gather the data on our new member and upline
		###### we could just go *, but then we would have to worry about
		###### characters that would break the xml into which this data will go
		$member = $db->selectrow_hashref("
			SELECT
				m.id, m.spid, m.alias, m.firstname, m.lastname,
				m.emailaddress, m.password, m.oid,
				mt.label AS membertype_label,
				COALESCE(m.language_pref,'en') AS language_pref,
				m.membertype
			FROM members m, \"Member_Types\" mt
			WHERE mt.code=m.membertype AND m.id= $member->{'id'}")
			|| die "Failed to retrieve member record after update\n";

		RecordOrigSpidOptIn();

		# for the moment, upline will equal sponsor :)
		$sponsor = $db->selectrow_hashref("SELECT
			id,spid,alias,firstname,lastname,emailaddress, COALESCE(language_pref) AS language_pref
			FROM members WHERE id= $member->{'spid'}");# ||
		#die "Failed to retrieve sponsor record after update\n"
		# we won't die in this case

		$upline{'vip_sponsor'} = $sponsor;

		# create our ID cookie so that we'll have a 'simple login' automatically
		Create_MemberID_Cookie();

		# and create a full login cookie as well
		Create_AuthCustom_Generic_Cookie();
	}
	return 1;
}

sub Create_AuthCustom_Generic_Cookie
{
	my $csl = Login::csLogin->new({'cgi'=>$q});
#	my $ck = $gs->Create_AuthCustom_Generic_cookie($member);
	my $ck = $csl->Generate_AuthCustom_Generic_Cookie($member);
	push @cookies, $ck;
	return $ck;
}

sub Create_MemberID_Cookie
{
	push @cookies, $q->cookie(
		'-name'=>'id',
		'-domain'=>'.clubshop.com',
		'-expires'=>'+1y',
		'-value'=>$member->{'id'});
}

sub Create_Password
{	###### create a 'temporary' password
	# well put it in the params hash so it will end up available in the xml
	# for use as a parameter in the greetings email
	my $rd = new String::Random;
	$params{'password'} = $rd->randpattern("ccnn");
}

sub Create_Unconfirmed_Membership
{
	###### we can add things as necessary using a hashref
	my $args = shift;
	my $MembershipMemo = '';
	$MembershipMemo .= "$args->{'memo'}\n" if $args->{'memo'};
	$MembershipMemo .= "$params{'memo'}\n" if $params{'memo'};
	$MembershipMemo .= "unconfirmed membership: " unless $args->{'suppress_default_memo'};
	$MembershipMemo .= 'IP: ' . $q->remote_addr . "\n" unless $args->{'suppress_default_memo_ip'};

	# we will assign them a simple password, which they can change after they confirm
	# we'll put it in the params hash so it will end up available in the xml
	# for use as a parameter in the confirm email
	my $rd = new String::Random;
	$params{'password'} ||= $rd->randpattern("ccnn");	# if we have a param we'll use it
	my $rv = ($member = _create_membership({
		'memo'			=> $MembershipMemo,
		'membertype'	=> ($args->{'membertype'} || 'x'),
		'alias'			=> ($args->{'alias'} || undef)
	}));
	Create_MemberID_Cookie() if $rv;
	return 1 if $rv;

	###### there are friendlier ways, but this works for now
	die "Failed to create a membership\n";
}

sub CryptString
{
###### Encrypt/decrypt a text string.
	my ($action, $value) = @_;

	if ( $action eq 'encrypt' )
	{
		$value = $cipher->encrypt_hex($value);
	}
	elsif ( $action eq 'decrypt' )
	{
		$value = $cipher->decrypt_hex($value);
	}

	return $value;
}

sub debug
{
	# use this for whatever by calling it from within a configuration
	warn "$_[0]\nerrors:\n";
	foreach (@errors){warn "$_\n"}
	return 1;
}

=head3 deflect_house_to_xtp

Determine if the sponsor of the applicant is a house account of the correct type
If so, redirect them to the direct signup version of the Trial Partner app

=head4 Arguments

Accepts a hashref recognizing these keys:
	ids => a reference to an array of numeric IDs

Note: Only the IDs passed in will be considered. There is no hook in the DB to signify this requirement. It is a pure hack to get this working for Dick 04/14/16

=head4 Returns

1 if the referring sponsor is not a house account
otherwise issues a redirect to the client

=cut

sub deflect_house_to_xtp
{
	my $args = shift;
	my @ids = @{$args->{'ids'} || []};
	
	if (grep $_ == $params{'sponsorID'}, @ids)
	{
		my $sph = $params{'sph'} || ManageSPH('crypt');
		
		my $qs = $q->query_string;
		$qs =~ s/app_type=xc/app_type=xtp/;
		
		if ($qs !~ /sph=/)
		{
			$qs = 'sph=' . $sph . ';' . $qs;
		}
		
		print $q->redirect('https://www.clubshop.com/cgi/tmp/appx.cgi/' . $params{'sponsorID'} . '/xtp?' . $qs);
		$SkipDisplay = 1;
		return;
	}

	return 1;
}

sub DetermineAppStep
{
	my $arg = shift;	
	my $rv = $arg || $q->param('next_step') || 'step0';
	###### make sure we have a pre-defined app_step
	my $match = ();
	foreach (keys %{$config_hash->{$app_type}})
	{
		###### we are only looking for nodes matching 'stepxx'
		next if $_ !~ '^step\d+';
		if ($_ eq $rv)
		{
			$match = $_;
			last;
		}
	}
	die "Invalid app_step: $rv\n" if ($rv && !$match);
	$params{'app_step'} = $app_step = $match;
	return 1;
}

sub DetermineAppType
{
	###### Initially we should have an app type embedded in the path after the
	###### 'sponsor' or 'first' path component. Alternatively, we could receive an app_type param
	my $rv = $q->param('app_type') || $path_info[2];

	# some parties and maybe some browsers were confusing the //upg and similar URLs to have redundant
	# // , thereby taking matters into their own hands and breaking the URL in the process
	# we'll try to accomodate them by recognizing an alpha path component in the first slot if all else fails
	# with the exception of default_01, there won't be any tracks with a digit in them and it won't be broken ;)
	$rv ||= $path_info[1] if $path_info[1] && $path_info[1] !~ /\d/;

	$rv ||= 'default';
	$rv = FiddleAppType($rv);
	
	###### make sure we have defined the app_type
	my $match = ();
	foreach (keys %{$config_hash})
	{
		###### besides our fields key the only nodes in our root
		###### (on 03/30/05) are app_type nodes
		next if $_ eq 'fields';
		if ($_ eq $rv)
		{
			$match = $_;
			last;
		}
	}
	
	if ($rv && ! $match)
	{
		Err("Invalid app_type: $rv (${\$q->path_info})");
		Exit();
	}
	
	if (! $config_hash->{$match}->{'active'})
	{
		Err("This application type is not active: $match");
		Exit();
	}

	###### since we have a valid app type,
	###### let's see if we can load the configuration for it
	$/ = undef;	# slurpy
	unless ( open (CFG, "$BaseDir/cgi-templates/xml/appx/$match.xml") )
	{
		warn "pwd: " . qx/pwd/ . "\n";
		Err("Failed to load app type xml: $match.xml\n");
		Exit();
	}
	
	my $xml = <CFG>;
	close CFG;
	$/ = "\n";
	$config_hash->{$match} = $xs->XMLin($xml, 'NoAttr'=>0);
	
	###### merge this node into the master document
	$config_xml =~ s#<$match.*?/>#$xml#;
	return $match;			
}

=head3 DetermineSponsor()

=head4 Arguments

	Both arguments are optional, but DetermineSponsor() will accept an integer value as the first argument for the sponsor ID.
	Lacking this argument, the sponsorID param and the path_info value will be used.
	The second optional argument is an array reference to the acceptable sponsor membertypes.
	This defaults to: [qw(v m s)]

=head4 Returns

	Returns true if successful or false if not.
	This routine is intended to set global values and to simply work as one of the stages.
	
=cut

sub DetermineSponsor
{
	my $spid = shift || $q->param('sponsorID') || $path_info[1];

	die "Sponsor ID appears to be invalid" if $spid && length($spid) > 12;

	my $acceptable_sponsor_membertypes = shift || [qw(v m s tp)];

	###### now we can validate the ID... if we have one... duh
	if ($spid)
	{
		$spid = $db->selectrow_hashref(
			($mbr_qry . ($spid =~ /\D/ ? 'alias=?' : 'id=?')),
			undef, (uc $spid));
	}

	###### let's keep trying if we are empty handed
	unless ($spid)
	{
		$spid =  $mem_info->{'id'} || $q->cookie('refspid');

		if ($spid)
		{
			$spid = $db->selectrow_hashref(
				($mbr_qry . ($spid =~ /\D/ ? 'alias=?' : 'id=?')),
				undef, (uc $spid));
		}
		###### if we have an empty spid record, then let's give it to the house
		else
		{
			$spid = $db->selectrow_hashref(
				"$mbr_qry id=?",
				undef,
				((grep $_ eq $app_type, (['af','upg'])) ? 3 : 1));
		}
	}

	# the reason for putting this here is because we do not want to perform an upline search if our specified sponsor
	# needs to be publicly exposed and the application refuted
	my $rv = SponsorCK($spid);
	return $rv unless $rv;

	$sponsor = ValidSponsor($spid, $acceptable_sponsor_membertypes);
	$gs->Prepare_UTF8($sponsor);
	$params{'sponsorID'} = $sponsor->{'id'};
	
	$params{'sph'} ||= ManageSPH('crypt');	# 01/23/14 making this a conditional assignment as why overwrite the received value?? we may need to evaluate it elsewhere outside of this script...	
	return 1;
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	###### accept some stuff if we don't want/need the default
	# for now we'll handle:
	# xsl		- a replacement template ID
	# xml		- a replacement for all the normal stuff generated
	# extra_xml	- you got it, some extra to roll in with the regular
	my $args = shift || {};
	my $xsl = $args->{'xsl'} || $cfg->xslt;
	my $xml = $args->{'xml'};
	Roll_Data_XML() unless $xml;
	Load_Content_XML();
	
#	warn 'back from Load_Content_XML in Display_Page()' if $cfg->vars('xml') == 10580;
	
	# Combine the remaining pieces of the xml document if applicable.
	$xml ||=  	"<base>\n" .
			$config_xml .
			$data_xml .
			$content_xml .
			($args->{'extra_xml'} || '') .
			"<timestamp>${\scalar localtime()}</timestamp>" .
			"\n</base>";

	$xsl = eval ('$gs->GetObject($xsl)') || die "$@\nFailed to load XSL template: $xsl\n";

#	if ($q->param('xml') && $ADMIN)
	if ($q->param('xml'))
	{
		if ($q->param('xml') =~ '^all$|^1$'){ EmitXML($xml) }
		elsif ($q->param('xml') eq 'xsl'){ EmitXML($xsl) }
		elsif ($q->param('xml') eq 'config'){ EmitXML($config_xml) }
		elsif ($q->param('xml') eq 'content'){ EmitXML($content_xml) }
		elsif ($q->param('xml') eq 'data'){ EmitXML($data_xml) }
		else
		{	###### reroll data_xml into just what we want and use that
			Roll_Data_XML([$q->param('xml')]);
			EmitXML($data_xml)
		}
	}
	else
	{
		print $q->header('-charset'=>'utf-8', '-cookies'=>\@cookies);
#		print $q->header('text/plain');

#	warn 'going into xsl transformation in Display_Page()' if $cfg->vars('xml') == 10580;
	
		my $content = XML::local_utils::xslt($xsl, $xml);
	
#	warn 'done xslt transformation in Display_Page()' if $cfg->vars('xml') == 10580;
	
		PageInserts(\$content);
		if (my $wrap = ($args->{'wrap'} || $Wrap))
		{
#			warn 'preparing to wrap: content_xml utf8 flag: ' . Encode::is_utf8($content_xml);

			$content_xml = encode_utf8($content_xml);

			# sometimes we will want to have the XML document available as a hashref so TT can use it
			# if that is the case, then we should set the loadXML flag on our wrap object
			if ($wrap->{'loadXML'})
			{
				my $xmlhr = $xs->XMLin($content_xml);
				_utf8_off($xmlhr);
				$wrap->lang_nodes( $xmlhr )
			}
			print $wrap->wrap( {'content'=> $content} );
		}
		else
		{
			print $content;
		}
	}
}

=head3 Display_TT_Page

A method of rendering a Template Toolkit based page using a hashref of language nodes
and currently a "flat" XHTML document rendered into a hashref of language nodes.

=head4 Arguments

Currently requires this as an argument: {
	lang_nodes=> 'template ID of language pack' OR a hash reference containing the nodes,
	template=>'template ID to render'
}

=head4 Renders the page using Template Toolkit

Currently the language pack nodes are passed into TT on the lang_nodes key,
the $member hashref is also passed into TT on the 'member' key
$ME, the script name is passed in on the scriptname key

=cut

sub Display_TT_Page
{
	require appx::DoTT_Template;
	
	# we need a lang_nodes document and a TT template
	my $args = shift;
	die "Both a lang_nodes argument and a template argument are required\n" unless $args->{'lang_nodes'} && $args->{'template'};
	my $lang_nodes = $args->{'lang_nodes'};
	
	###### an alternative to using the default XML/XSLT page rendering in Display_Page
	# this method must be invoked directly from within a configuration document
	# by returning empty, appx will stop any further processing by default
	unless (ref $lang_nodes)
	{
		$lang_nodes = $main::gs->GetObject($args->{'lang_nodes'}) || die "Failed to load language pack: $args->{'lang_nodes'}\n";
		$lang_nodes = XML::local_utils::flatXHTMLtoHashref($lang_nodes) || die "Failed to extract the language pack: $args->{'lang_nodes'}\n";
	}
	
	# it seems that TT doesn't work quite right with utf-8 characters and the utf-8 flag
	# for example, passing a password with an unicode character with the utf-8 flag on renders an unreadable character
	# by turning off the flag, things render properly... so let's try turning it off closest to the point of usage
	# if things that are already in place become broken, we will have to move this transformation back to the caller
	_utf8_off($member);# = encode_utf8($member->{$_}) foreach keys %{$member};
	
	binmode STDOUT, ":encoding(utf8)" if $args->{'setbinmode'};
	
	DoTT_Template::Do({
		'db'=>$db,
		'cgi'=>$q,
		'gs'=>$gs,
		'wrap'=>$args->{'wrap'},
		'tmpl'=>$args->{'template'},
		'data'=>{
			'scriptname'=>$ME,
			'member'=>$member,
			'lang_nodes'=>$lang_nodes,
			'params'=>\%params,
			'extras'=>\%extras,
			'non_xml_extras'=>\%non_xml_extras
		}
	});
	return;
}

sub Dump_Errors
{
	return 1 unless @errors;
	
	###### a rather crude way to handle 'errors' that shouldn't be
	print "Content-type: text/plain\n\n";
	print "Error: $_\n" foreach @errors;
	return;
}

sub Email
{
	# a simple mail assembly routine
	# we are currently looking for an xsl key in our hashref arg
	# we'll apply that to our basic XML to extract our message and pass it on to the mail delivery sub
	my $arg = shift || die "No argument received\n";
		die "No XSL argument\n" unless $arg->{'xsl'};

	my $lang_spec = shift;
	my $xsl = $gs->GetObject($arg->{'xsl'}, $lang_spec) ||
		die "Failed to load template: $arg->{'xsl'}\n";

	# personalize the remove text
	Load_Content_XML();

	# we'll use a custom version if we receive the argument, otherwise we'll use the global version
	my $_content_xml = $arg->{'xml'} || $content_xml;

	Roll_Data_XML(['member','sponsor','user','extras']);
	my $mp = XML::local_utils::xslt($xsl, (
		'<base>' . $data_xml . $_content_xml . 
		'<remove_blocks>'. Remove_txt_xml($lang_spec) .
		'</remove_blocks></base>'
	));
	_send_mail($mp);

	###### We could probably do some error testing,
	###### but for now let's pretend everything's OK.
	return 1;
}

sub EmailHtmlMailings
{
	my $docid = shift || die "No docid received";
	eval('use HtmlMailings');	# no use loading this unless we absolutely need it
	die $@ if $@;

	my $HM = HtmlMailings->new({'db'=>$db});

	$HM->{'BrowserView'} = 0;
	my $lp = $params{'language_pref'} || $gs->Get_LangPref || 'en';

	$HM->init({'docid'=>$docid, 'lang_pref'=>$lp});

#	$HM->GenerateHtmlDocument({'member'=>$member}, $lp});
	my ($rv, $error) = $HM->SendMail({'member'=>$member});
#warn "SendMail rv= $rv , error= $error";
	return 1;
}

sub EmitXML
{
#	print $q->header(-charset=>'utf-8', -type=>'text/xml');
	print $q->header('-type'=>'text/plain');
	print $_[0];
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('Error'), $q->h3($_[0]), $q->p,
		$q->p('Current server time: ' . scalar localtime()), $q->end_html;
}

sub Err_xml
{	###### Instead of an rough english error message, we may want to use
	###### a translatable message. In that case we can put it/them
	###### into a simple stylesheet.
	my $args = shift;	# for now we expect a hashref with a key called 'list'
						# which points to an arrayref
						# we will also recognize an alternate XSLT template
	my $errata = shift || '';
	my $xml = "<error_errata>$errata</error_errata><error_list>\n";
	$xml .= ('<' . $_ . " />\n") foreach @{$args->{'list'}};
	$xml .= "</error_list>\n";
	Display_Page({'xsl'=>($args->{'xml_err'} || $TMPL{'xml_err'}), 'extra_xml'=>$xml});
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

=head3 FiddleAppType

	There are certain botches of app types that have been used. Rather than bombing out and refusing these, we will fiddle with them to make them work

=cut

sub FiddleAppType
{
	my $t = shift;
	
	my %map = (
		'AP' => 'af',
		'TP' => 'tp'
	);
	
	return $map{ uc($t) } ? $map{ uc($t) } : $t;
}

=head3 Get_Live_Record

	Get the standard dataset of member details based on an "ID" argument. Return the hashref.
	
	Requires an ID, alias or emailaddress as the first argument.
	As a second argument, it will also use an array ref with elements like this to modify the SQL directly
	["AND m.membertype='x'",....]

=cut

sub Get_Live_Record
{
	my $arg = shift || die "No argument received for Get_Live_Record";
	my $xsql = shift; 
	my $qry = '
		SELECT
			m.*,
			m.oid,
			mt.label AS membertype_label
		FROM members m
		JOIN "Member_Types" mt
			ON m.membertype=mt.code
		WHERE ';
	if ($arg !~ /\D/)
	{
		$qry .= 'm.id=?';
	}
	elsif ($arg =~ /\@/)
	{
		$qry .= 'm.emailaddress=?';
	}
	else
	{
		$qry .= 'm.alias=?';
	}
	
	foreach (@{$xsql})
	{
		$qry .= "\n$_";
	}

	my $rv = $db->selectrow_hashref($qry, undef, $arg);
	return unless $rv;

	# let's make all our values empty rather than uninitialized
	foreach (keys %{$rv})
	{
		$rv->{$_} = '' unless defined $rv->{$_};
	}

	return $rv;
}

sub Get_Temp_Record
{
	my @list = @_;
	my $m = $db->selectrow_hashref("$mbr_qry alias= ? AND oid= ? AND password= ?",
	undef, @list);
	return $m;
}

=head3 GetMemberFromAliasAndPKC

There are occasions when we want to just pull the member record (probably new) and display a page.
This routine expects an "alias" parameter and a "pkc" parameter which is the CryptString version of the alias.
It will set the global $member var based on the alias as long as the 2 params agree.

=cut

sub GetMemberFromAliasAndPKC
{
	# I see no way for this to be invoked incorrectly,
	# so we will just die instead of do the regular error handling if we get bogus values
	die "Both an alias and a pkc parameter are required\n" unless $params{'alias'} && $params{'pkc'};
	die "pkc is inconsistent\n" unless $params{'alias'} eq CryptString('decrypt', $params{'pkc'});

	$member = Get_Live_Record($params{'alias'});
	die "Failed to obtain a matching membership\n" unless $member;

	return $member;
}

#sub Ip_Blocked
#{
#	###### this was a DB based list before, but it is so short, I decided to put in
#	###### right in here
#	###### it may have outlived it's usefulness, but.......
#	return grep $q->remote_addr =~ $_, (
#		'207\.67\.223\.59',	# an IP identified with Bill Carton
#		'68\.7\.44\.67',		# an IP identified with Bill Carton
#		'213\.17\.236\.242',	# request from Cynthia - reference: PC3086919 & PC3097623
#		'206\.251\.224\.86');	# identified with Bill Carton - added 10/24/03
##		,'41\.221\.17\..+'	# a malevolent fraudster from Algeria
##		,'41\.221\.19\..+'
##		,'41\.221\.18\..+'
#	#	,'12.32.98.254'	# our IP entry for testing
##	);
#}

sub Ip_Blocked
{
	# we're baaaack!
	return $db->selectrow_array('SELECT ipaddr FROM ip_blocks WHERE ipaddr= ?', undef, $q->remote_addr);	
}

sub IP_Restriction_CK
{
	return if ( $VIP_login || $Merchant_login || $ADMIN 	# allow all these
		|| $app_type eq 'c'			# opt-in confirmations
							# initial redirects & mini-logins
		|| ($app_type eq 'redurl' && $app_step =~ m/step(0|10)/)
		|| $q->remote_addr eq '12.32.98.254'
	);

	my $ip = $db->selectrow_array("
		SELECT ip FROM temp_hash WHERE ip= ?", undef, $q->remote_addr);
	return unless $ip;

	my $msg = $gs->GetObject(10055) || die "Failed to load template 10055\n";
	Err($msg . " " . $q->remote_addr);
	return 1;
}

sub isHouseAccount
{
	my $args = shift;
	my $type = $args->{'house_acct_type'};
	$type = "'$type'" if $type;
	$type ||= "'CS','GI'";	# our default

	unless (
		$sponsor &&
		$db->selectrow_array("
			SELECT coop_id FROM coop.coops
			WHERE house_acct=TRUE AND house_acct_type IN ($type)
			AND coop_id= ?", undef, $sponsor->{'id'})
	)
	{
		main::Err('Use of this application is reserved');
		return 0;
	}
	return 1;
}

sub Load_Content_XML
{
	my $args = shift || {};
	if ($args->{'cache'} && $cache{'Load_Content_XML'})
	{
		$content_xml = $cache{'Load_Content_XML'};
		return;
	}
	
	###### conditionally pull in the correct XML doc (global lang_blocks)
	###### on a first run through, the var will be undef
	###### in rare cases it may on a redo require a different doc than the first time
	if (! defined $xml_tmpl || 
		$xml_tmpl != ((@errors ? $cfg->vars('err_xml') : '') ||
		$cfg->vars('xml') ||
		$TMPL{'content_xml'})
	)
	{
		$xml_tmpl = ((@errors ? $cfg->vars('err_xml') : '') || $cfg->vars('xml') || $TMPL{'content_xml'});
		$content_xml = $gs->GetObject($xml_tmpl) || die "Failed to load XML template: $xml_tmpl\n";
		
#		warn 'going to decode_utf8 in Load_Content_XML' if $xml_tmpl == 10580;
		
		$content_xml = decode_utf8($content_xml) unless Encode::is_utf8($content_xml);
		$cache{'Load_Content_XML'} = $content_xml if $args->{'cache'};
		
#		warn 'done with decode_utf8 in Load_Content_XML' if $xml_tmpl == 10580;
	}
}

sub Load_Content_XML_hashref
{
	Load_Content_XML({'cache'=>1});
	eval 'use XML::LibXML::Simple';
	my $xs = XML::LibXML::Simple->new('NoAttr'=>1);
	$content_xml_hashref = $xs->XMLin($content_xml);
}

sub Load_Email_List
{
	###### Rather than perform numerous database queries against different type
	###### memberships that match email addresses, we'll create a list to process
	return 1 unless $params{'emailaddress'};
	###### within the scripts.
	my $sth = $db->prepare("
		SELECT id,alias,membertype
		FROM members WHERE emailaddress=?");
	$sth->execute($params{'emailaddress'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push (@email_matches, $tmp);
	}
	$sth->finish;
	return 1;
}

###### Load the params into a local hash.
sub Load_Params
{
	###### we will only load params that we are expecting
	###### in other words, if they aren't in the configuration, we don't want 'em
	foreach ( keys %{$cfg->{'fields'}} )
	{
		###### turn on the utf8 flag for our params
		$params{$_} = decode_utf8( defined($q->param($_)) ? $q->param($_) : '');

		###### we have to treat some fields special
		###### like emails (PreProcess removes underscores)
		if (/^email/)
		{
			###### remove all control characters (below ASCII 32)
			###### and other characters that should never be needed in the data
			###### like ? % ^ $ ~ | * ! =
			$params{$_} =~ s/[\x00-\x1f]|\?|%|\^|\$|~|\||\*|!|=|\+//g;

			###### remove leading & trailing whitespace
			$params{$_} =~ s/^\s*|\s*$//g;
		}
		elsif (/^redurl/)	###### and '=' and '%'
		{	# in this case we don't care what we get
			# because will not be processing it
			# - do nothing -
		}
		elsif (/^referer/)
		{	# although we will be recording this, it will only be for internal consumption and we should get it all
			# - do nothing -
		}
		else
		{
			$params{$_} = $gs->PreProcess_Input($params{$_});
		}
	}

}

sub Load_States
{	###### populate our states hash
	# 04/16/09 now that we are using the update track, we need states at step0... before there are params
	# however, we'll give preference to a param as it would be freshest
	my $country = $params{'country'} || $member->{'country'};
	unless ($country)
	{
		return 1;
	}
	###### This routine can be called outside of the main program, like in the upg
	###### module which calls it and then tests the $states var.
	###### If we are already loaded, then there is no need to make another DB call.
	elsif (keys %{$states})
	{
		 return 1;
	}

	$states = $db->selectall_hashref("
		SELECT lower(code) AS lcode, code as ucode, name
		FROM state_codes WHERE country = ?", 'lcode', undef, $country);
}

sub LoadTransposeMeIntoNonXmlExtras
{
	my $country = shift || $params{'country'} || $member->{'country'};	# we may want to coerce the country to get US/USD specific pricing
	my $func = shift || 'format_currency';

	# if we still do not have a country, let's try to determine it by IP address
	($country) ||= $db->selectrow_array('SELECT countryshort FROM ip_latlong_lookup WHERE ?::inet BETWEEN inet_from AND inet_to', undef, $q->remote_addr) if $q->remote_addr;
	
	$country ||= 'US';
	my ($currency_code) = $db->selectrow_array("SELECT currency_code FROM currency_by_country WHERE country_code=?", undef, $country);
	$currency_code ||= 'USD';
	
	$non_xml_extras{'extra_javascripts'} = qq#
		<script type="text/javascript" src="//www.clubshop.com/js/transpose.js"></script>
		<script type="text/javascript" src="//www.clubshop.com/js/sprintf.js"></script>
		<script type="text/javascript" src="//www.clubshop.com/js/Currency.js"></script>
		
		<script type="text/javascript">
		    var my_language_code = "${\($main::gs->Get_LangPref || 'en')}";
		    var my_currency_code = "$currency_code";
		    var my_country_code = '$country';
		
		    \$(document).ready(function() {
		        Transpose.$func();
		    });
		</script>
	#;
	
	return 1;
}

sub Locate_Confirm_Membership
{
	###### let's see if we can identify this membership for confirmation
	###### we should have an alias, oid and 'password' params to work with
	my $m = Get_Temp_Record($params{'alias'}, $params{'oid'}, $params{'temp_password'});

	if (! $m->{'id'})
	{
		push @errors, ['no_match_confirm_params'];
	}
	elsif ($m->{'membertype'} eq 'x')
	{
		###### apparently we are set to move on
		###### move the the password slot
		###### The reason we didn't just place it in there to begin with is due to
		###### form labeling restrictions. If we called it password, then errors amongst
		###### other things get tagged to 'password'
		$params{'password'} = $params{'temp_password'};
	
		# capture the spid for use in decision making at confirmation time
		$member->{'spid'} = $m->{'spid'};
	
		# with the DB doing auto-transfers into pools upon confirmation
		# we need to retain the sponsor of the x membership for use by tracking code
		$extras{'original_sponsor'} = $db->selectrow_hashref("$mbr_qry id= $m->{'spid'}");
	
		# in order to make a default signup under in-house sponsorships,
		# we need to know the sponsor of the sponsor ;-(
		$sponsor = $extras{'original_sponsor'};
	}
	else
	{
		push @errors, ['already_confirmed'];
	}
	return 1;
}

sub Make_Connection_SSL
{
	###### since people will be prompted to change their password
	###### we may have those paranoid about the connection not being secure
	###### can call this routine from any 'track'
	###### or we may just end up calling it in all cases at some point
	return 1 if $q->https;
	(my $url = $q->self_url) =~ s/^http/https/;
	print $q->redirect($url);
	return;
}

sub ManageSPH
{
	# build or reconstitute our sponsor hash
	my $act = shift;
	my $list = '';
	my @l = qw/id spid alias emailaddress/;
	if ($act eq 'crypt')
	{
		foreach(@l){ $list .= "$sponsor->{$_}|" }
	}
	else
	{
			foreach(@l){ $list .= "\$sponsor->{$_}," }
	}
	chop $list;

	if ($act eq 'crypt')
	{
		return $cipher->encrypt_hex($list);
	}
	else
	{
		my $tmp = $cipher->decrypt_hex($params{'sph'});
# example values
# tmp: 3|2|DHS03|salesmgr@dhs-club.com
# list: $sponsor->{'id'},$sponsor->{spid},$sponsor->{alias},$sponsor->{emailaddress}
# after running this eval, the vars in the list will contain the values in tmp
		eval("($list) = split /\\|/, \$tmp");
	}
	
	$sponsor = $db->selectrow_hashref("$mbr_qry id=$sponsor->{'id'}");
	die "Parameter mismatch\nspid=$sponsor->{'id'}, param=$params{'sponsorID'}" unless $sponsor->{'id'} eq $params{'sponsorID'};

	return 1;
}

sub MerchantLoginCK
{
	my ($id) = $gs->authen_ses_key($q->cookie('AuthCustom_maf'), 'maf');

	if (! $id || $id =~ /\D/)
	{	###### it should be strictly numeric or its invalid
		return;
	}
	return 1;
}

=head3 OneOfMany

A routine that can be called from within a configuration when one of a number of fields is required.
That kind of scenario doesn't fit the simple required=yes/no for a given field.
The Cell_or_Telephone() function provided this when we simply needed to check for one of either of those,
however, the need expanded to cell or telephone OR Skype Name....

=head4 Parameters

	This routine expects an array of hashrefs composed like this:
	(
		{'field/parameter name that is required' => [a typical array reference to nodes or free text]},
		{}
	)

	Example (taken directly from Cell_or_Cellphone() which was converted to use this central routine):
	return OneOfMany({'phone'=>['phone','phone-footnote']},{'cellphone'=>['cellphone']});
	
	This tells the routine that either phone or cellphone are required.
	The first element in the arrayref tells the app which field to highlight for correction.
	The second element tells it what error text to display after the field name
	(if a node name, translated content is used, otherwise, the literal text is used)

=cut

sub OneOfMany
{
	my ($fields, @errors) = @_;
	
	foreach (@{$fields}){
		return 1 if $main::params{$_};
	}

	# setup a default so that we don't always have to define the error setup in our configuration
	# we will simply put the one_of_following message followed by the list of fields
	unless (@errors)
	{
		push @errors, ['one_of_following'];
		foreach (@{$fields})
		{
			push @errors, [$_];
		}
	}
	
	push @main::errors, @errors;
	return 1;
}

=head3 PageInserts

An internal function for doing simple regexp substitutions on the content before "printing" it out.
It is too problematical to insert some stuff (like javascripts) into the XML stream and integrate it into the document via XSLT.

=cut

sub PageInserts
{
#	warn "tracking: $non_xml_extras{'tracking'}";
	my $docref = shift;
	
	foreach (keys %non_xml_extras)
	{
		# no point in parsing the document if there is nothing to substitute
		next unless $non_xml_extras{$_};
		
		# we started with simply using HTML comments, but these do not make it through from an XML lang node
		# so we will go to TT style placeholders which could be used if we decide to go with TT at some point
		${$docref} =~ s#<!--$_-->|\[\%\s*$_\s*\%\]#$non_xml_extras{$_}#g;
	}
}

sub Parse_Cfrm
{
	return 1 unless $params{'cfrm'};
	my $args = shift;	###### optional args hashref
				# ck_only will return after checking rather than default behavior

	# this routine parses the initial parameter received when a confirm link is clicked
	# we are expecting an alias, oid & password
	# we'll put the vals into the param hash if everything looks good

	my @list = split(/,/, $params{'cfrm'});
	if (scalar @list != 3)
	{
		push @errors, ['generic_error', "Invalid parameter cfrm: $params{cfrm}"];
		###### that will result in a simple form presentation with the error
		###### listed at the top of the page
	}
	else
	{
		###### let's see if we have a matching record
		my $m = Get_Temp_Record(@list);
		if (! $m->{'id'})
		{
			push @errors, ['no_match_cfrm'];
		}
		elsif ($m->{'membertype'} eq 'x')
		{
			$params{'alias'} = $list[0];
			$params{'oid'} = $list[1];
			$params{'temp_password'} = $list[2];

			# reset our app step unless told otherwise
			return 1 if exists $args->{'ck_only'};

			###### apparently we are set to move on
			DetermineAppStep( $cfg->cfg('next_step') );

			# reset our configuration for the duration
			$cfg = new _my_private_
			$RedoFlag = 1;
		}
		else
		{
			push @errors, ['already_confirmed'];
		}
	}
	return 1;
}

sub Parse_Memo_for_Resend
{
	# This module is not for regular 'operations'
	# It is meant to work with another routine.
	$member->{'memo'} =~ /resend:(\d+)(HTML)*/;
	my $tmpl = $1 || $TMPL{'confirmation_email'};
	my $html = $2 || '';
	return ($tmpl, $html );
}

# update the original_spid record to reflect the opt-in date 
sub RecordOrigSpidOptIn
{
	$db->do("UPDATE original_spid SET opt_in_date=NOW() WHERE id= $member->{'id'}");
	return 1;
}

=comment
	put into place 10/21/15 to deal with Dick's insistence to have signups under him and a house account coach under him be redirected to other house account direct signups
	
	There is one optional argument, the application we should redirect to if there is an app_redirection_id
	The idea is that this function will be called in whatever configuration it is needed and the correct app type can be specified from there.
=cut
sub RedirectPlaceholderPartnerSponsorship
{
	my $destinationAppType = shift || 'xc';	# default to the direct signup 'm' application
	my ($redirectionID) = $db->selectrow_array("SELECT app_redirection_id FROM configurations.placeholder_partners WHERE id= ?", undef, $params{'sponsorID'});
	return 1 unless $redirectionID;
	
	print $q->redirect("$ME/$redirectionID/$destinationAppType");
	return;
}

sub Redirect_01
{
#	return 1 unless $ACCTTYPE eq 'CS';	# I'm only dealing with Clubshop 01 redirects now, we can figure out the rest later
###### if the sponsor is a house account, redirect to that track
#warn "sponsor ID: $sponsor->{'id'}";
	my $ha = $db->selectrow_hashref("
		SELECT * FROM coop.coops
		WHERE house_acct=TRUE AND house_acct_type= ? AND coop_id= ?", undef, $APPXDEFS{'ACCTTYPE'}, $sponsor->{'id'});
	return 1 unless $ha;
	print $q->redirect("$ME/$ha->{'coop_id'}/$APPXDEFS{'DEF_HOUSE_APP_TRACK'}");
	return;
}

sub Remove_txt_xml
{
###### it would be nice to have a complete xml solution,
###### but the variability and complexity
###### of the removal links precludes that for now
	my $lang_spec = shift;
	our $remove_txt_xml = $gs->GetObject($TMPL{'remove_txt'}, $lang_spec) ||
		die "Failed to load template: $TMPL{'remove_txt'}\n";
	$remove_txt_xml = decode_utf8($remove_txt_xml)
		unless Encode::is_utf8($remove_txt_xml);

	###### we will assume
	$remove_txt_xml =~ s/%%oid%%/$member->{oid}/g;
	$remove_txt_xml =~ s/%%emailaddress%%/$member->{emailaddress}/g;
	$remove_txt_xml = "<remove_txt>$remove_txt_xml</remove_txt>\n";
	return $remove_txt_xml;
}

sub RestoreState
{
	my $args = shift || {};
	###### move the saved parameters into the live hash with the exception
	###### of the ones that are pre-existent
	# unless we receive a 'force' argument
	my $rv = RetrieveState();
	die "Failed to retrieve process data.\n" unless $rv;

	###### the default root of the dumped data is $state, our global
	eval $rv;
	###### for now we have to do the same decoding thing here as we do with params
	###### once the DB is unicode we can drop this
#	$gs->Prepare_UTF8($state);

	foreach (keys %{$state})
	{
		$params{$_} = $state->{$_} unless defined $params{$_} && ! $args->{'force'};
	}
	return 1;
}

###### Retrieve the state of the app process from a dump
###### or simply retrieve some simple data elements.
###### Referenced by a pk value and its equiv encrypted value for security reasons.
###### The idea is that some component will know what to do with what comes back.
# this routine is called by other modules besides RestoreState()
sub RetrieveState
{
	my $data = ();
	# Find the primary key of the process state record using the encrypted value, if one exists.
	my $pk = CryptString('decrypt', $params{'pkc'});

	# If the crypted key matches the unencrypted key that was passed.
	if ( $pk && $pk eq $params{'pk'} )
	{
		# Get the params xml block using the provided pk value.
		$data = $db->selectrow_array("
			SELECT data
			FROM 	process_state
			WHERE 	pk = $pk
			AND 	application = 1");
	}
	# let the caller perform error handling if $data is undef
	# it may be just checking for existence and not want to die
	return $data;
}

sub Roll_Errors
{
	return '' unless @errors;
	###### combine all of our errors into an XML block
	my $rv = "<errors>\n";
	foreach (@errors){
		$rv .= "<$_->[0]";
		$rv .= ($_->[1]) ? ">$_->[1]</$_->[0]>\n" : "/>\n";
	}
	$rv .= "</errors>\n";
	return $rv;	
}

sub Roll_Data_XML
{
	my $test = {};
	foreach (keys %{$test})
	{
#		warn "utf8: $_ : " . Encode::is_utf8($test->{$_}) . "\n" ;
		$test->{$_} = decode_utf8($test->{$_});
#		warn "utf8: $_ : " . Encode::is_utf8($test->{$_}) . "\n" ;
	}
	
	my $arg = shift || 'data';
	$arg = [qw(member errors menus user sponsor params sponsor extras)] if $arg eq 'data';
	# arg can be an arrayref of *Just* the components we want instead of the default whole enchilada
	$data_xml = "<data>\n<script_name>$ME</script_name>\n";
	$data_xml .= '<browser><language_pref>' . $gs->Get_LangPref . '</language_pref></browser>';
	foreach (@$arg)
	{
		if ($_ eq 'params'){ $data_xml .= $xs->XMLout(\%params, 'RootName'=>'params') || '' }
		elsif ($_ eq 'errors'){ $data_xml .= Roll_Errors() }
		elsif ($_ eq 'menus'){ $data_xml .= '<menus>' . $${\Build_Menus()} . '</menus>' }
		elsif ($_ eq 'user'){ $data_xml .= $xs->XMLout($mem_info, 'RootName'=>'user') || '' }
		elsif ($_ eq 'sponsor'){ $data_xml .= $sponsor ? $xs->XMLout($sponsor, 'RootName'=>'sponsor') : '' }
		elsif ($_ eq 'upline'){ $data_xml .= $xs->XMLout(\%upline, 'RootName'=>'upline') || '' }
		elsif ($_ eq 'member')
		{
			$gs->Null_to_empty($member) if $member;
			$data_xml .= $xs->XMLout($member, 'RootName'=>'member') || '';
		}
		elsif ($_ eq 'extras'){ $data_xml .= $xs->XMLout(\%extras, 'RootName'=>'extras') || '' }
	}
	$data_xml .= "</data>";
}

sub SaveState
{
	# we'll either save the raw stuff we've been passed or the params
	my $data = shift;
	unless ($data)
	{
	# Data Dumper recognizes the utf-8 flag and converts on the fly to ascii.
	# So, we need to turn off the flag for it work as we want it to.
		my %local_params = %params;
#		_utf8_off(\%local_params);	# disabling this 06/05/15 to try and fix some bad encoding corruption that has begun to occur since the DBD::Pg upgrade
		$data = Data::Dumper->Dump([\%local_params],['state']);
	}

	# Find the primary key of the process state record, if one exists.
	my $pk;
	$pk = CryptString('decrypt', $params{'pkc'}) if $params{'pkc'};

	# If the crypted key matches the unencrypted key that was passed.
	if ( $pk && $pk eq $params{'pk'} )
	{
		# Update the process state record.
		die "Failed to update process state\n" unless $db->do("
			UPDATE process_state
			SET application = $script_num,
				data = ?,
				expires = NOW() + INTERVAL '$APP_AGE'
			WHERE pk = ?", undef, $data, $pk);
	}
	else
	{
		$pk = $db->selectrow_array("SELECT NEXTVAL('process_state_pk_seq');");
	# Insert a new process state record.
		die "Failed to preserve process state\n" unless $db->do("
			INSERT INTO process_state (pk, application, data, expires)
			VALUES (?, $script_num, ?, NOW() + INTERVAL '$APP_AGE')",
		undef, $pk, $data);

	# Put the pk and pk encrypted into the params hash.
		$params{'pk'} = $pk;
		$params{'pkc'} = CryptString('encrypt', $pk);
	}
	return 1;
}

=head3 setNotification

=head4 DESCRIPTION:

	This subroutine inserts a field in the notifications table.

=head4 PARAMS:

	$args[0]	int	The Member ID
	$args[1]	int	The Notificaton Type
	$args[2]	string The "memo" message
	
=head4 RETURNS:

	return int	1 on success, undef on failure

=cut

sub setNotification
{

	my @args = ();
	
	$args[0] = shift;
	$args[1] = shift;
	$args[2] = shift;
	
	return undef if $args[0] !~ /\d+/;
	return undef if $args[1] !~ /\d+/;
	
	my $query =<<EOT;
		INSERT
		INTO
		notifications
		(id, notification_type, memo)
		VALUES
		(?,?,?)
	
EOT
	
	return undef if(! $main::db->do($query, undef, @args));

	return 1;	
}

=head3 setRefererParam

	If we need to capture the referer for later recording,
	then we should call this early in the application cycle to get the real referer.
	Otherwise, "we" will be the referer.
	
	Basically, this routine will set the referer param to the $q->referer *unless* the parameter is already set.
	
=cut

sub setRefererParam
{
	$params{'referer'} ||= $q->referer;
	return 1;
}

sub SponsorCK
{
	my $sp = shift || $sponsor;
	# if we do not have a $sponsor, then other parts of the machine should be handling that - so we'll return true
	return 1 unless $sp;
#	return 1 if $sp->{'id'} == 1;
	
	###### are they a valid member?
	# we are going to tell the world that they are terminated without a lot of hoopla
	if ($sp->{'membertype'} eq 't')
	{
		Err('The sponsoring membership is terminated!');
		return undef;
	}

	###### if they are administratively suspended, then they are shut off
	my $rv = $db->selectrow_array("SELECT id FROM suspension_flag WHERE id= $sp->{'id'}");
	if ($rv)
	{
		Err('THIS SITE HAS BEEN ADMINISTRATIVELY CLOSED!');
		return 0;
	}

	return 1;
}

sub Temp_Hash
{
	my $arg = shift;
	my ($qry, $rv, $hash);
	# since we are no longer carrying this as a param, we don't need a hash
	# this combination of values should provide enough uniqueness to avoid
	# duplicate browser submissions
	foreach (qw'firstname lastname emailaddress city country')
	{ $hash .= "$params{$_}:" }

	###### We won't try to match on the ip because it could have changed.
	###### If it has, we may not match when we want to.
	$rv = $db->selectrow_array("
		SELECT 1 FROM temp_hash WHERE hash= ? LIMIT 1",
		undef, $hash);
	return if $rv;

	# in order to avoid an unfriendly DBI error, we'll turn it off for now
	###### I know what you're thinking, this should never error out... go figure
	$db->{'RaiseError'} = 0;
	$rv = $db->do("
		INSERT INTO temp_hash ( hash, ip ) VALUES (?,?)",
		undef, $hash, $q->remote_addr);
	# now turn it back on
	$db->{'RaiseError'} = 1;
	return ($rv eq '1') ? 1 : undef;
}

sub UserCK
{
	# UserCK_CS used to be named this, but with the advent of using appx across domains,
	# we need different methods of determining the user.
	# So we will simply rely on a value in the domain specific APPX module
	return $APPXDEFS{'ACCTTYPE'} eq 'GI' ? UserCK_GI() : UserCK_CS();
}

sub UserCK_CS
{
	###### we want to determine if a logged in party is doing this
	my $ck = $q->cookie('AuthCustom_Generic');
	return unless $ck;

	###### if they have a good cookie we'll capture their info
	my ($id, $meminfo) = $gs->authen_ses_key( $ck );
	if ($id && $meminfo && $id !~ /\D/)
	{
#		$gs->Prepare_UTF8($meminfo);
# as of 04/29/10 in at least the vip track, this does not work
# somehow the utf8 flag is already on in the data and so Prepare_UTF8() leaves it untouched
# however, the data is not in the same state as the other data, like the sponsor or the countries list even
# so, we are going to manually run it through decode_utf8 again and it seems to work fine
		$meminfo->{$_} = decode_utf8($meminfo->{$_}) foreach keys %{$meminfo};
		return $meminfo;
	}
	return;
}

sub UserCK_GI
{
	eval { require Apache::GI_Authen; };
	if ($@){
		warn $@;
		return;
	}
	my ($id, $meminfo) = GI_Authen::authen_ses_key( undef, undef, $q->cookie('GI_Authen_General') );
	if ($id && $meminfo && $id !~ /\D/)
	{
#		$gs->Prepare_UTF8($meminfo);
# as of 04/29/10 in at least the vip track, this does not work
# somehow the utf8 flag is already on in the data and so Prepare_UTF8() leaves it untouched
# however, the data is not in the same state as the other data, like the sponsor or the countries list even
# so, we are going to manually run it through decode_utf8 again and it seems to work fine
		$meminfo->{$_} = decode_utf8($meminfo->{$_}) foreach keys %{$meminfo};
		return $meminfo;
	}
	return;
}

sub ValidateCBID_test
{
###### we're actually validating that the values passed have not been munged
###### and that the CBID has not been activated by another invocation
	if ( $params{'cbh'} eq md5_hex($SALT . $params{'cbid'} . $sponsor->{'id'}) )
	{
		my $rec = $db->selectrow_hashref("
			SELECT id, class
			FROM 	clubbucks_master
			WHERE 	(class= 2 OR class= 3 OR class= 7)
			AND 	id= ?", undef, $params{'cbid'});

		###### if we retrieve a , we're all set, if not, then we can safely
		###### assume that it must have been activated since the hashing passed		
		if ($rec)
		{
			return 1;
		}
		else
		{
			ERR_MSG('Apparently this card has already been activated');
		}
	}
	return;
}

=head3 ValidSponsor()

=head4 Arguments

	A hash reference to a sponsor member record. It must contain at least an id, spid, and membertype
	
	An array reference to a list of membertypes we intend to sponsor.
	Normally this will contain one item, like 's' or 'm', but we may as well handle multiple items

=head4 Returns

	A hash reference to a member record
	
=head4 What it does

	Looks in the membertype_sponsors table to determine the valid sponsor membertypes for each item in the list of membertypes we want to sponsor.
	
=cut

sub ValidSponsor
{
	my ($spid, $mlist) = @_;
	unless ($spid && $spid->{'membertype'})
	{
		warn "spid: $spid";
		warn "$_ : $spid->{$_}" foreach (keys %{$spid});
		die "Invalid sponsor hashref received";
	}

	my @Valid = ();
	my $qry = "
		SELECT membertype
		FROM membertype_sponsors
		WHERE can_signup=TRUE
		AND can_sponsor IN ('" . join("','", @{$mlist}) . "')"; 
	my $sth = $db->prepare($qry);
	$sth->execute;
	while (my $tmp = $sth->fetchrow_array)
	{
		push @Valid, $tmp;
	}
	# if the sponsor we received is of the correct membertype, then we are done
	return $spid if grep {$_ eq $spid->{'membertype'}} @Valid;

	my $start_id = $spid->{'id'};
	$sth = $db->prepare("$mbr_qry id=?");
	do {
		$sth->execute($spid->{'spid'});
		$spid = $sth->fetchrow_hashref;
		die "Failed during sponsor upline search" unless $spid->{'id'};
	} while ((! grep $_ eq $spid->{'membertype'}, @Valid) && $start_id != $spid->{'id'});
	$sth->finish;
	return $spid;
}

###### ###### ###### END OF Functional Subroutines
###### infrastructure routines below
sub _create_membership
{
	my $args = shift;

	# before we begin with the actual member creation,
	# we'll handle our temp hash entry
	# by evaluating the bypass first, we can skip the temp_hash call
	if (! $Bypass{'temp_hash_insertion'} && ! Temp_Hash('in'))
	{
		Err('You have already submitted this information and it has been accepted');
		return;		
	}

	###### We will work with the params hash exclusively expecting params that
	###### match column names.
	###### That being the case, if we are restoring state, the data will have
	###### to be folded in.
	# We are going to assume that the configuration has taken care of data requirements
	# rather than make redundant checks here. (like firstname and lastname)

	my %p_hash = %params;

	###### first we have to create the start of an alias
	###### we'll start with the first characters of the names and move to the next
	###### if we don't have ordinary letter characters
	$p_hash{'firstname'} =~ /^.*?([a-z])/i;
	my $fc = uc $1 || 'A';
	$p_hash{'lastname'} =~ /^.*?([a-z])/i;
	my $lc = uc $1 || 'A';

	# we could create this list from the actual record schema, but for now we 
	# are safer just defining possible fields other than the ones we'll always do
	my @opt_cols = qw(prefix mdname suffix secprefix secfirstname secmdname seclastname
		secsuffix company_name emailaddress emailaddress2 address1 address2
		city state postalcode country phone fax_num password
		mail_option_lvl tin language_pref membertype cellphone other_contact_info);

	my $id = $db->selectrow_array("SELECT nextval('members_id_seq'::text)") || die "Failed to obtain an ID number\n";

	###### we may receive an alias argument like for AG applicants
	$args->{'alias'} ||= $fc . $lc . $id;

	my $qry_start = "INSERT INTO members (
		id, spid, firstname, lastname";
	my $qry_end = "$id, ?, ?, ?";
	my @arg_list = ($p_hash{'sponsorID'}, $p_hash{'firstname'}, $p_hash{'lastname'});

	foreach (@opt_cols)
	{
	###### in order to allow the script to overwrite a submitted parameter
	###### we will test for an argument and if found, skip assignment for the param
		next if (defined $args->{$_} ||
			! defined $p_hash{$_} ||
			$p_hash{$_} eq '');
		$qry_start .= ", $_";
		$qry_end .= ", ?";
		push @arg_list, $p_hash{$_};
 	}

	foreach (keys %{$args})
	{
		next unless defined $args->{$_};
		$qry_start .= ", $_";
		$qry_end .= ", ?";
		push @arg_list, $args->{$_};
 	}

	my $rv = $db->do("$qry_start ) VALUES ( $qry_end )", undef, @arg_list);
	die "Failed to create a membership\n" unless $rv && $rv eq '1';

	# Capture certain bits of signup info and record them outside of the member record.
	# we could default to $q->referer if the param were empty, but then we can get a bunch of valueless info
	# like the script itself with a parameter list
	# instead, if we really want to capture the referer, it's a simple matter of invoking setRefererParam() from a config
	$db->do("
		UPDATE original_spid
		SET signup_date=NOW(), ipaddress=?, landing_page=?
		WHERE id= $id", undef, $q->remote_addr, $params{'referer'});
	
	# now let's get a complete record including an oid
	
# for testing purposes, we'll not create a membership and we'll just pull up #12
#	$rv = $db->selectrow_hashref("SELECT m.*,m.oid FROM members m WHERE m.id= 12");
	$rv = Get_Live_Record($id);
	return $rv;
}

sub _send_mail
{
	# we should receive just a mailpiece which can be sent without further processing
	my $mp = shift;

	my $hdr = ParseMe::Grab_Headers(\$mp);

	MailingUtils::DrHeaders($hdr);
	my %mail = (
		%{$hdr},
		'Content-Type' => 'text/plain; charset="utf-8"',
		'message'=> $mp );

	return ( sendmail(%mail) ) ? undef : $Mail::Sendmail::error;
}

sub _utf8_off
{
	my $hr = shift;
	foreach (keys %{$hr})
	{
		if (ref $hr->{$_} eq 'HASH'){ _utf8_off($hr->{$_}) }
		else { Encode::_utf8_off($hr->{$_})}
	}
}

###### ###### ###### END OF SUBROUTINES

package _my_private_;
use Data::Dumper;

sub cfg
{
	my ($self, $arg) = @_;
	###### extract the value of a non-field item
	return $self->{$arg};
}

sub check
{
	###### return a 'list' of validation checks
	###### we'll start our list with 'pre_validation' checks
	###### then we'll add our global checks and finally tack on 'post_validation'
	my (@list, $s);
	foreach my $type ('pre_validation', 'validation', 'post_validation')
	{
		$s = $_[0]->{'fields'}{$_[1]}{$type};
		push @list, (ref $s ? @$s : ($s))
			if ($s && (! ref $s || ref $s eq 'ARRAY'));
	}
	return @list ? \@list : undef;
}

sub error_level
{
	return $_[0]->{'fields'}->{$_[1]}->{'error_level'} || '';
}

sub length
{
	return $_[0]->{'fields'}->{$_[1]}->{'max'};
}

sub new
{
	# we are assuming a one time call to this, although, I suppose it could
	# be reinitialized
	# we are also assuming that the config hash, app_type & app_step are initialized
	# we'll create our 'private' data structure from the existing ones
	die "Necessary contructs are not initialized" unless $config_hash && $app_type && $app_step;
	
	my %h = %{$config_hash->{$app_type}->{$app_step}};
	my $k = ();

	foreach (keys %{$h{'required'}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>1,
			'optional'=>0
			),
			%{$config_hash->{'fields'}{$_}},	# pick up our global config directives
			%{$h{'required'}{$_}}			# pick up our additional config directives
		);
	}

	foreach (keys %{$h{optional}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>0,
			'optional'=>1
			),
			%{$config_hash->{'fields'}{$_}},
			%{$h{'optional'}{$_}}
		);
	}

	# roll in the 'global' param reqs and optionals
	foreach (keys %{ $config_hash->{$app_type}->{'required'} })
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>1,
			'optional'=>0
			),
			%{$config_hash->{'fields'}{$_}},	# pick up our global config directives
			%{$config_hash->{$app_type}{'required'}{$_}}			# pick up our additional config directives
		);
	}

	foreach (keys %{ $config_hash->{$app_type}->{'optional'} })
	{
		%{$h{'fields'}->{$_}} = (
			(
			'required'=>0,
			'optional'=>1
			),
			%{$config_hash->{'fields'}->{$_}},
			%{$config_hash->{$app_type}->{'optional'}->{$_}}
		);
	}

	# we want to allow some vars that are global to the app_type
#warn "app_type: $app_type";
#warn Dumper($config_hash->{$app_type}->{'vars'});
	foreach $k (keys %{ $config_hash->{$app_type}->{'vars'} })
	{
#		warn "key: $k";
#		warn Dumper($config_hash->{$app_type}->{'vars'}->{$k});
		$h{'vars'}->{$k} = $config_hash->{$app_type}->{'vars'}->{$k};
	}

	###### if our xslt node is a 'text' node,
	###### we'll convert it to a parent with a 'default' child
	$h{'xslt'} = { 'default' => $h{'xslt'} } unless ref $h{'xslt'};

	# we want to do the same for some xsl templates that are global to the app_type
	# this would be the case for perhaps some specific 'error' or special cases
	foreach $k (keys %{$config_hash->{$app_type}{'xslt'}})
	{
		$h{'xslt'}->{$k} = $config_hash->{$app_type}->{'xslt'}{$k};
	}

	return bless \%h;
}

sub operations
{
	# create a list of operations that have to be performed
	my ($self, $arg) = @_;
	my $ops = $self->cfg('operations');
	my $rv = $ops->{$arg};
	# if we have an empty node in our XML, we'll have an empty hashref here
	return [] unless defined $rv && ref $rv ne 'HASH';
	# if we have a list, it should be represented as an arrayref
	return (ref $rv) ? $rv : [$rv];
}

sub opt{ return _flip_req('optional', @_) }

sub req { return _flip_req('required', @_) }

sub req_err_level { return $_[0]->{'fields'}->{$_[1]}->{'req_err_level'} || '' }
sub sort { return $_[0]->{'fields'}->{$_[1]}->{'sort_order'} || 100 }

sub value
{
	return $params{$_[1]};
}

sub vars
{
	my ($self, $arg, $newval) = @_;
	$self->{'vars'}->{$arg} = $newval if defined $newval;
	return $self->{'vars'}->{$arg};
}

sub xslt
{
	my ($self, $arg) = @_;
	# if we are looking for XSLT, then we'll apply a bit of logic
	# we may have an error condition requiring a different template
	# or we may not have any conditional templates
	# (if we're doing conditionals, then we'd best have an error template)
	if ($arg)
	{
		return $self->{'xslt'}->{$arg};
	}
	elsif (@errors)
	{
		return $self->{'xslt'}->{'error'} || $self->{'xslt'}->{'default'};
	}
	
	return $self->{'xslt'}->{'default'};
}

sub _flip_req
{
	my ($what, $self, $key, $value) = @_;
	my $not_what = ($what eq 'required') ? 'optional' : 'required';
	unless ($key)		###### I guess we want the whole enchilada
	{
		my @list;
		foreach (keys %{$self->{'fields'}})
		{
			push @list, $_ if $self->{'fields'}->{$_}->{$what};
		}
		return @list
	}
	elsif ($value)	###### we want to change one
	{			###### we should receive a hashref
				###### like {'param_name'=>1} to make it required
		$self->{'fields'}->{$key}->{$what} = $value;
		$self->{'fields'}->{$key}->{$not_what} = ('1' cmp $value);
		return $value;
	}
		
	return $self->{'fields'}->{$key}->{$what};
}

1;

###### ###### ###### END OF _my_private_ PACKAGE

__END__

=head1 CHANGE LOG:

	09/16/05 add Ck_Email_Domain_Exists
	09/19/05 adjusted the domain check to allow for timeouts
	also added the Make_Connection_SSL routine
	09/26/05 modified Create_unconfirmed.... to move any passed memo args to the beginning of the memo 
			 also added Parse_Memo_for_Resend,  also added an escaped comma fixer in Parse_cfrm
	10/03/05 added a final membertype check in the SQL criteria before confirmation
	10/13/05 added the password to the generic member query
	10/31/05 added the capability for 'Email' to accept a lang_spec
			this will be used initially in the vip track to force the member email to the selected language
	11/09/05 added Create_MemberID_Cookie and began placing cookies for unconfirmed members
	11/15/05 added the remove checker, added a die on eval error in the Ck_ sub,
			moved the Ck_Blocked_Domain into Ck_Excluded_Domains since that is what it really did,
			recreated a new Ck_Blocked_Domain that really checks the blocked domain table
			tweaked the confirmation error reporting when update was unsucessful
	11/22/05 tweaked the member query to explicityly declare the table in all columns ie. m.colname
	11/25/05 added a die on eval error in Check Params
	01/11/06 redeployed with the 'step' configuration blocks removed to separate xml files
	01/13/06 revised new() to bring in app_type global required/optional fields
	02/07/06 rolling out with several changes that coerce any potential UTF-8 data
			to have the utf-8 flag on (this is a must with this application to avoid)
			outputting double encoded characters
	02/23/06 revised the logic in the email domain checker to bail out on an invalid email address
			also added filtering for null mx domains
	04/04/06 added phone data to the global member query
	04/14/06 added utf8 handling for $meminfo coming in
	06/30/06 added company_name to the mbr_qry
	07/03/06 added capture of the spid in the Locate membership routine
	07/28/06 added the browser language pref to the default data XML
	07/31/06 added set_message to help in getting more info in error reporting
	08/23/06 revised the flow in Locate_Confirm_Membership to only do the additional stuff if we are all set
	11/21/06 added handling in the membership creation routines to handle membertype and alias arguments
	04/11/07 a slight tweak in Ck_Email_Domain_Exists to fix a broken evaluation
	08/17/07 added the path_info components as an arrayref value to the path_info key in the %extras hash
	11/13/07 added the Ck_Invalid_Characters routine
			and made some tweaks in Ck_ to handle the uniqueness of Ck_Invalid_Characters
	12/17/07 removed phone_ac from the app
	08/14/08 added the test for a terminated sponsor in DetermineSponsor
	2008/10/22	Keith Added the setNotification subroutine.
	08/06/09 went with the Email::Valid->mx method for evaluating the validity of the domain in Ck_Email_Domain_Exists
	09/10/09 adjusted the Determine Sponsor routine tio implement a different default sponsor ID for apps hosted on glocalincome
	09/25/09 adjusted Email() to recognize and use an 'xml' argument as the content value instead of the global content
	10/06/09 added ma to the list of valid sponsors
	11/24/09 added language_pref to the list of fields we pull in on the new member query in Confirm Membership
	12/22/09 made the regex match in Ck_Excluded_Domains look for ^abuse instead of abuse
	12/31/09 added the ability to suppress the default memo in Create_Unconfirmed_Membership
	02/09/10 added Create_AuthCustom_Generic_Cookie()
	04/27/10 added Check_Xcl_Dupe()
	04/29/10 further tweaks to Check_Membership_Dupe to query against all membertypes when the the 'vip' argument is received
	05/11/10 added the Display_TT_Page routine
	08/03/10 made use of the imported $APPXDEFS{'ACCTTYPE'}' in Redirect_01()
	11/16/10 revised UserCK and the way the "vip" track user was previously validated in the main script 
	06/07/11 revised the ValidSponsor to recognize the new membertype_sponsors.can_signup flag
	07/14/11 revised the sponsor determination in DetermineSponsor to pull in a house account if a sponsor record was not returned from the apparent referring party
	09/12/11 made Get_Live_Record() able to use an email address as an argument as wells the ID or alias
	02/09/12 added the Record_Additional_Orig_Spid_Info() routine
	04/26/12 extended the regex expression in PageInserts to also match on TT style placeholders
	05/30/12 added LoadTransposeMeIntoNonXmlExtras()
	08/14/12 minor non-functional changes like removing the residual 'goto' statements
	07/04/13 change the behavior of taking the first path component without checking it in DetermineSponsor()
	07/09/13 further modifications in DetermineSponsor() to fix the breakage of aliases as referral IDs in the path
	01/23/14 the only functional tweak was to make the assignment of the sph parameter in DetermineSponsor() conditional, in other words the routine will not overwrite a parameter received
			oh, and added the recognition of suppress_default_memo_ip in Create_Unconfirmed_Membership()
	09/25/14 added the ability for Load_Content_XML to cache the results... useful when it is called from within a configuration before it would normally be called
	12/26/14 Added FiddleAppType() to be used by DetermineAppType(). Also replaced the die commands with more friendly Err() calls.
	09/08/15 Minor tweaks in Load_States to return true if bailing out early... needed to keep the rest of the application happy
	04/14/16 Add deflect_house_to_xtp()
=cut
