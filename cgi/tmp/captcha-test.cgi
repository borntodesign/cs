#!/usr/bin/perl -w
use strict;
require Captcha::reCAPTCHA;
use CGI;
my $q=new CGI;
    my $c = Captcha::reCAPTCHA->new;
unless ($q->param('work')){
	print $q->header,$q->start_html;
	print $q->start_form, $q->hidden(-name=>'work', -value=>1);
    # Output form
	print $c->get_options_setter( {theme=>'white'} );
    print $c->get_html( '6LdyBwEAAAAAAIup30CI3BKCe-jLlDpyUjmQqD7q' );
	print $q->submit,$q->end_form,$q->end_html;
}
else {
print $q->header('text/plain');
	my $challenge = $q->param('recaptcha_challenge_field') || die "no recaptcha_challenge_field param\n";
	my $response = $q->param('recaptcha_response_field') || die "No recaptcha_response_field param\n";
    # Verify submission
    my $result = $c->check_answer(
        '6LdyBwEAAAAAAEgyYCF7geHYFoXIM0vS6ILU767-', $ENV{'REMOTE_ADDR'},
        $challenge, $response
    );
    if ( $result->{is_valid} ) {
        print "Yes!";
    }
    else {
        # Error
        print $result->{error};
    }
}

exit;
