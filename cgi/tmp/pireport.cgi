#!/usr/bin/perl -w
###### pireport.cgi
###### Personal Income Report (used to be potential income report ;> )
###### newly released: 02/29/12	Bill MacArthur
###### last modified: 05/23/15	Bill MacArthur

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use Template;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use XML::LibXML::Simple;
use G_S qw/$EXEC_BASE $LOGIN_URL/;
use XML::local_utils;
use GI;
use Refcomp::Analysis;
use DHS::ConvertCurrency;
use IncomeReport::Base;
require ADMIN_DB;
require cs_admin;
binmode STDOUT, ":encoding(utf8)";
#use Data::Dumper;	# testing only

my $STATS_TBL = 'refcomp';# 'refcomp_me';
my $TNT_LVL_VALUES_TBL = 'network.level_values_tnt';

my %TMPL = (
	'xml'		=> 10911,
	'months'	=> 10572,
	'TT'		=> ''		# this value is assigned further down by a method from IncomeReport::Base
);

my %Links = (
	'compplan' => 'http://www.clubshop.com/manual/compensation/partner.html',
	'gps_mod' => 'http://www.clubshop.com/cgi/vip/gps_modification.cgi',
	'earnings_statement' => 'https://www.clubshop.com/cgi/CommStmnt.cgi',
	'ca' =>'https://www.clubshop.com/cgi/funding.cgi?action=report',
	'closetag' => '</a>'
);

my %Months = (
	'01' => 'January',
	'02' => 'February',
	'03' => 'March',
	'04' => 'April',
	'05' => 'May',
	'06' => 'June',
	'07' => 'July',
	'08' => 'August',
	'09' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December'
);

# the pay qualifier columns as returned by Get_Mbr_Stats()
# having them listed here makes it easier to use the list wherever...
my @cols = qw/prp tpp grp prp_pp grp_pp sv_grp sv_grp_pp/;

my $q = new CGI;
my $TT = Template->new({'ABSOLUTE' => 1});
my $RA = Refcomp::Analysis->new();

my ($db, $lang_blocks, %params, $USER_ID, $meminfo, $stats, $Period, $RptType, $TNT_level_values, $cldrCurr, $cldrNumber);
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my $IRB = IncomeReport::Base->new('cgi'=>$q, 'db'=>\$db);
my $browserLangPref = $gs->Get_LangPref();

###### for now we'll accept a simple member ID
###### and if it turns out to be a Partner, we'll require a login so as to protect
###### the privacy of the Partners

my $ADMIN = AdminCK();

my $id = $q->cookie('id');
die "Bad ID cookie" if $id && $id =~ /\D/;

LoadParams();

my $rid = uc($params{'rid'});
$rid ||= ($q->cookie('id') || '') unless $ADMIN;	# if in admin mode, we will only accept a parameter for identification

unless ($rid)
{
	my $error = $ADMIN ? 'No rid parameter (rid=ID OF THE PARTNER). Necessary in ADMIN mode.' : 'You must be ' .
		$q->a({'-href'=>"$LOGIN_URL?destination=" . $q->script_name}, 'logged in') . ' to use this report.';
	Err($error);
	exit;
}

DB();

my $rpte = $db->selectrow_hashref("
	SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype, m.country, m.language_pref,
		cbc.currency_code,
		exr.rate,
		cc.exponent AS currency_exponent,
		COALESCE(a.a_level, 0) AS a_level,
		(my_exec(m.id)).id AS my_exec
	FROM members m
	JOIN currency_by_country cbc
		ON cbc.country_code=m.country
	JOIN exchange_rates_now exr
		ON exr.code=cbc.currency_code
	JOIN currency_codes cc
		ON cbc.currency_code=cc.code
	LEFT JOIN a_level_stable a
		ON a.id=m.id
	WHERE ${\($rid =~ m/\D/ ? 'm.alias':'m.id')} = ?", undef, $rid);

unless ($rpte)
{
	Err("No matching records for: $rid");
	Exit();
}

$IRB->rpte($rpte);

if ($params{'action'} eq 'archive' && $params{'month'} =~ m/\d{4}-\d{2}-\d{2}/)
{
	$RptType = 'ARCHIVE';
	my $rpt = '';
	($TMPL{'TT'}, $rpt) = $IRB->GetReportAndTemplate($params{'month'});

	$IRB->Redirect($rpt) unless $rpt eq $q->script_name;
	
	($Period) = $db->selectrow_array('SELECT get_period_by_date(1, ?)', undef, $params{'month'}) || die "Failed to obtain period for $params{'month'}";
}
else
{
	$RptType = 'LIVE';
	$params{'action'} = '';		# if we did not receive a valid "month" parameter as well as the "archive" action, then we will ignore the "archive" action
	($TMPL{'TT'}) = $IRB->GetReportAndTemplate();
}

if ($RptType eq 'ARCHIVE')
{
}
else
{
	# this is not an archived report, so there are some things we only need in a current month report
	# like the alternate PLPP qualifiers

	($Period) = $db->selectrow_array('SELECT get_period_by_date(1, NOW())');
	
	$TNT_level_values = $db->selectall_hashref("
		SELECT
			lv.pay_level,
			lv.prp,
			lv.grp,
			lv.tmpp,
			lv.tmpp AS tpp,
			lv.execs,
			lv.mcg,
			lv.prp_pp,
			lv.grp_pp,
			lv.sv_grp,
			lv.sv_grp_pp,
			lv.min_subscription_weight,
			lv.star_lines_sv,
			format_currency_by_locale((lv.mcg * $rpte->{'rate'})::INTEGER::NUMERIC,?,?) AS mcg_formatted
		FROM $TNT_LVL_VALUES_TBL lv
		WHERE lv.pay_level >= 0", 'pay_level', undef, $rpte->{'country'}, ($rpte->{'language_pref'} || $browserLangPref));
	
	foreach (keys %{$TNT_level_values})
	{
#0508		$TNT_level_values->{$_}->{'mcg_formatted'} = $gs->Prepare_UTF8($TNT_level_values->{$_}->{'mcg_formatted'}, 'decode');
	}
}

my $GI = GI->new('cgi'=>$q, 'db'=>$db);

my ($qpp_qualification_value) = $db->selectrow_array('SELECT qpp_qualifier()');

my $level_values = $db->selectall_hashref('
	SELECT lv.*
	FROM level_values lv
	WHERE lv.lvalue > 0', 'lvalue');
	
my $DHSCC = DHS::ConvertCurrency->new('db'=>$db, 'ccode'=>$rpte->{'country'}, 'lcode'=>($rpte->{'language_pref'} || $browserLangPref), 'currcode'=>$rpte->{'currency_code'});

$stats = Get_Mbr_Stats();
LoadLangBlocks(); # we must have our member information for currency formatting before we can load these

###### are they a valid membership?
unless ((grep $_ eq $rpte->{'membertype'}, qw/v tp/) || $ADMIN)
{
	Err( $gs->ParseString($lang_blocks->{'no_rpt_available'}, $rpte->{'membertype'}, $rpte->{'alias'}) );
	Exit();
}

if ($rpte->{'membertype'} eq 'v' && ! $ADMIN)
{
	###### since this is a VIP we are reporting on we will do the following
	# make sure the user is logged in
	# make sure the user, if not the reportee, is upline
	( $USER_ID, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if ((! $USER_ID) || ($USER_ID =~ /\D/))
	{
		Err(qq|A valid <a href="$LOGIN_URL?destination=${\$q->script_name}">login</a> is required to view Partner reports.|);
		Exit();
	}
	($meminfo->{'pay_level'}) = $db->selectrow_array("SELECT a_level FROM a_level_stable WHERE id= $meminfo->{'id'}") || 0;
	
	###### make sure that the reportee is our downline... and not an Exec
	if ($USER_ID != $rpte->{'id'})
	{
#		if (! $db->selectrow_array("SELECT id FROM relationships WHERE id= ? AND upline= ?", undef, $rpte->{'id'}, $USER_ID))
		if (! $db->selectrow_array("SELECT network.access_ck1($rpte->{'id'}, $meminfo->{'id'})"))
		{
			Err( $gs->ParseString($lang_blocks->{'not_in_dn'}, $rid) );
			Exit();
		}

		# get rid of the second conditional to simply block upline access to even a frontline Exec's report
		# if you do, you may as well, comment out the 'my_exec' portion of the SQL where it is called
		if ($rpte->{'a_level'} >= $EXEC_BASE && $rpte->{'my_exec'} != $USER_ID)
		{
			Err($lang_blocks->{'no_see_exec'});
			Exit();
		}
	}
}

Output();

Exit();


###### ###### Start of Sub Routines ###### ######

sub AdminCK
{
	if ($q->cookie('cs_admin'))
	{
		my $cs = cs_admin->new({'cgi'=>$q});
		my $creds = $cs->authenticate($q->cookie('cs_admin'));
		exit unless $db = ADMIN_DB::DB_Connect( 'pireport.cgi', $creds->{'username'}, $creds->{'password'} ) ||	die "Failed to connect to the database with staff login";
		$db->{'RaiseError'} = 1;
		return $creds->{'username'};
	}
	return 0;
}

=h3 Brackets

	Let's say our partner has enough prvs to qualify for pay level 9 (ED),
	but only enough pt to qualify as a pay level 6 (SM)...
	The next pay level they are shooting for is 7 (GM)
	This will return a hashref like this:
	{
		'prvs' => {
			'good' => 9
		},
		'pt' => {
			'behind' => 6,
			'next' => 7
		},....
	}
	
=cut

sub Brackets
{
	my $stats = shift;

	my %rv = ();
	my $next_level = $stats->{'a_level'} + 1;
	$next_level = 2 if $next_level < 2;	# 2 is our lowest level
	my @action = ();
	
	# first determine which values we have met
	foreach my $col (@cols)
	{
		$stats->{$col} ||= 0;

			foreach my $pl (sort {$a<=>$b} keys %{$TNT_level_values})
			{
				# this sets the column_name->good key to the pay_level value we match for this column
				$rv{$col}->{'good'} = $pl if $stats->{$col} >= ZeroNotNull($TNT_level_values->{$pl}->{$col});
			}
	}
	
	# now let's see what ones we need for the next pay level
	foreach my $col (@cols)
	{
		$stats->{$col} ||= 0;

		$rv{$col}->{'next'} = $next_level if ZeroNotNull($TNT_level_values->{$next_level}->{$col}) > $stats->{$col};
	}

	# for Execs, we need to build a bracket flag for the number of Stars
	if ($stats->{'a_level'} >= $EXEC_BASE)
	{
		foreach my $pl (sort {$a<=>$b} keys %{$TNT_level_values})
		{
			$next_level = $pl + 1;
			$next_level = $pl unless $TNT_level_values->{$next_level};	# we cannot evaluate values beyond the data we have
			
			# this sets the column_name->behind key to the pay_level value we match for this column
			$rv{'execs'}->{'good'} = $pl if $stats->{'stars'} >= $TNT_level_values->{$pl}->{'execs'};
				
			# this sets the column_name->next key to the pay_level value we match for this column
			# once we set this key on the way up, we do not reset each time through like setting a high water mark
			$rv{'execs'}->{'next'} = $next_level if
				$stats->{'stars'} < $TNT_level_values->{$next_level}->{'execs'} &&
				$stats->{'a_level'} < $next_level &&
				! $rv{'execs'}->{'next'};
		}
	}

	return \%rv;
}

sub DB
{
	return if $db;
	$db = DB_Connect::DB_Connect('pireport.cgi') || exit;
	$db->{'RaiseError'} = 1;
}

sub EmitXML
{
	print "Content-type: text/plain\n\n$_[0]\n";
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'<h4>There has been a problem</h4>',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Get_Mbr_Stats
{
	my $zcc = $db->quote($rpte->{'country'});
	my $zlp = $db->quote($rpte->{'language_pref'} || $browserLangPref);
	
	my $cols = ''; # the default
	my $table = "$STATS_TBL"; # the default
	my $where = "WHERE rb.id = $rpte->{'id'}";	# the default
	my $qry = '';
	
	if ($RptType eq 'ARCHIVE')
	{
		# we need to know if this commission month has been paid on
		$cols = ', rb.paid, rb.run_date, /*rb.period,*/ rb.commission_month, rb.prp_pp, rb.grp_pp';
		$table = 'archives.refcomp_me';
#		$where .= ' AND rb.commission_month= ' . $db->quote($params{'month'});
		$where .= " AND rb.period= $Period";
		$where .= ' ORDER BY rb.run_date DESC LIMIT 1';	# once we have more than one run, we want the most recent
		
		$qry = "
			SELECT
				rb.prvs AS prp,
				rb.opp AS tpp,	-- I know this value is a duplicate of the column below, but the new TNT archives need it like this
				rb.frontline_ppp,
				rb.op,
				rb.op AS grp,
				rb.prv_handicap AS grp_nops,
				od.onekplus(rb.op) AS op_txt,
				rb.ppp,
				rb.rpp,
				rb.qpp,
				rb.plpp AS exec_bonus_raw,
				COALESCE(rb.plpp,0) AS exec_bonus,	-- as of April 2013 the column is used for this
				rb.opp,
				(rb.frontline_ppp + rb.rpp) AS fpp_rpp,
				rb.my50 AS my50,
				rb.my25 AS my25,
				rb.dividends AS dividends,
				rb.referral_commissions AS referral_commissions,
				rb.minimum_commission_guarantee AS minimum_commission_guarantee,
				rb.a_level,
				rb.analysis,
				ic.id AS inactive_coach_id,
				ic.inactive,
				rb.sv_grp,
				rb.sv_grp_pp
				$cols
			FROM $table rb
			LEFT JOIN inactive_coach_by_period ic
				ON ic.id=rb.id
				AND ic.period=$Period
				
			$where";
	}
	else
	{
		# May 2015 we need to show a nopay the income he would receive if he were paid
		# so we need to make use of the function that will fill the values in for what looks like refcomp
		# if they are paid, the following query will return nothing
		# we will do the same for Trial Partners as they are not really qualified to receive pay
		if ($rpte->{'membertype'} eq 'tp' || $db->selectrow_array("SELECT id FROM nop WHERE id= $rpte->{'id'}"))
		{
			$table = "refcomp_for_nopays( $rpte->{'id'} )";
			$where = '';
		}
		$cols = ",(xpath('//sv/star_lines_sv/text()', rb.analysis::xml))[1] AS star_lines_sv";
		$qry = "
		SELECT
			rb.prvs AS prp,
			rb.qpp,
			rb.opp AS tpp,
			rb.my50 AS my50,
			rb.my25 AS my25,
			rb.op,
			rb.op AS grp,
			rb.prv_handicap AS grp_nops,
			rb.referral_commissions AS referral_commissions,
			rb.minimum_commission_guarantee AS minimum_commission_guarantee,
			rb.a_level,
			rb.analysis,
			rb.sales_req_count AS execs,
			COALESCE(rb.dividends,0) AS dividends,
			rb.plpp AS exec_bonus_raw,
			COALESCE(rb.plpp,0) AS exec_bonus,
			ic.id AS inactive_coach_id,
			ic.inactive,
			COALESCE(st.weight,0) AS subscription_weight,
			rb.prp_pp,
			rb.grp_pp,
			rb.sv_grp,
			rb.sv_grp_pp,
			nop.flag AS nopay,
			npc.potential_pay_level
			$cols
		FROM $table rb
		LEFT JOIN inactive_coach_by_period ic
			ON ic.id=rb.id
			AND ic.period=$Period
		LEFT JOIN partner_subscription_monthly psm
			ON psm.member_id=rb.id
		LEFT JOIN subscription_types st
			ON st.subscription_type=psm.subscription_type
		LEFT JOIN nop
			ON nop.id=rb.id
		LEFT JOIN network.potential_commission npc
			ON npc.id=rb.id
		$where";
	}

	#	we don't necessarily need to die here as an empty report is fine and allows selecting from the archive rpts || die "Failed to Get_Mbr_Stats";
	my $rv = $db->selectrow_hashref($qry);
	
	$rv->{'period'} = $Period;

	# our coach status is a 3 state "value" that exists only in the current month if it exists at all
	# if we have no record, then we do not have to display any verbiage regarding coach status
	if ($rv->{'inactive_coach_id'})
	{
		if (! defined $rv->{'inactive'})	# a null means that we have been manually reset to active or we are simply active
		{
			$rv->{'coach_status'} = 0;
		}
		elsif (! $rv->{'inactive'})		# a false 'inactive' field means that this is our one month warning
		{
			$rv->{'coach_status'} = 1;
		}
		else
		{
			$rv->{'coach_status'} = 2;
		}
	}
	elsif ($rv->{'a_level'} > 4)
	{
		$rv->{'coach_status'} = 0;		# if they do not have an explicit record, they default to ACTIVE
	}
	
	# if the month we are looking at has been paid, then we will look up the exchange rate for that date
	# this way future visits to the same data will give the same results
	# this may not be the case if we were to use today's exchange rate every time
	if ($rv->{'paid'})
	{
		# this will obtain the most recent exchange preceeding the exact date if there is nothing for the exact date
		($rpte->{'rate'}) = $db->selectrow_array(qq|
			SELECT "rate"
			FROM exchange_rates
			WHERE "date"::DATE <= '$rv->{'run_date'}'
			AND "code"= '$rpte->{'currency_code'}'
			ORDER BY "date" DESC
			limit 1|) || die "Failed to obtain an exchange rate for $rpte->{'currency_code'} ON $rv->{'run_date'}";
	}

	# May 2015 - Dick wants nopays to see what they would be making if they were paid, so we will extrapolate using the current matrix
	# note he also wants the promotion analysis to work just like for a paid partner, so we can merely change out the appropriate values at this point
	if ($RptType ne 'ARCHIVE')
	{
		
	}
	
	# in order to have uniform rounding, we have to round our individual values
	# as they will all be added together in the template and any non-rounded fractions
	# ultimately end up causing us to be over by 1 or 2 in our total
	foreach (qw/my50 my25 dividends referral_commissions minimum_commission_guarantee exec_bonus/)
	{
		$rv->{$_} *= $rpte->{'rate'};
		$rv->{$_} = sprintf "\%\.$rpte->{'currency_exponent'}f", $rv->{$_};
	}
	
	# we do not have a specific column designating the number of stars an Exec has
	# although we do now have such a "column", I'm not sure that we may need that package for other methods directly invoked from the template
	$RA->init($rv->{'analysis'});
	# ... now we do
	# but then again, the column we are using now in the current period (Feb 2015) may have been in use for it's intended purpose at some point in time for archive reports
	$rv->{'stars'} = $RA->numOfExecs;

	return $rv;
}

sub link
{
	my %a = @_;
	return '' unless %a;
	my $rv = '<a ';
	foreach(keys %a)
	{
		if ($_ eq 'href')
		{
			$rv .= qq|$_="$Links{$a{$_}}" |;
		}
		else
		{
			$rv .= qq|$_="| . $q->escapeHTML($a{$_}) . '" ';
		}
	}
	$rv =~ s/ $/>/;
	return $rv;
}

sub LoadLangBlocks
{
	my $months = $gs->GetObject($TMPL{'months'}) || die "Failed to retrieve the months template\n";
	$months = XML::LibXML::Simple::XMLin($months);
	$months = $months->{'months'}->{'month'};

	my @d = split(/-/, $db->selectrow_array('SELECT NOW()::DATE'));

	my $tmp = $gs->GetObject($TMPL{'xml'}) || die "Failed to retrieve language pack template\n";
#	$tmp = decode_utf8($tmp);
	
	my $comm_month = $months->{ $Months{$d[1]} }->{'content'}; # the default
	# we want to omit the month because we will be doing something else within the template
	$comm_month = '' if $params{'action'} eq 'archive' && $params{'month'};

	$TT->process(
		\$tmp,
		{
			'CGI'=>$q,
			'rpt_month'=>$comm_month,
			'Links'=>\%Links,
			'powerppcap'=>PowerPpCap(),
			'link'=>\&link,
			'DHSCC'=>$DHSCC,
			'TNT_level_values' => $TNT_level_values,
			'level_values' => $level_values,
			'stats' => $stats
		},
		\$lang_blocks
	);
	
	warn $TT->error() if $TT->error();

	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

sub LoadParams
{
	foreach (qw/action month rid/)
	{
		$params{$_} = $gs->Prepare_UTF8($q->param($_) || '');
	}
}

sub Output
{
	my ($tmp, $tmpl) = ();
#$TMPL{'TT'} = '/home/httpd/cgi-templates/TT/pireport/test.tt';
	# with the advent of using file based templates, we will evaluate whether or not this is a numeric DB template ID or a file based template
	# the template based one has to be a scalar ref when passed into TT
	if ($TMPL{'TT'} !~ m/\D/)
	{
		my $t = $gs->GetObject($TMPL{'TT'}) || die "Failed to retrieve main template: $TMPL{'TT'}\n";
		$tmp = \$t;
	}
	else
	{
		$tmp = $TMPL{'TT'};
	}
#die Dumper($TNT_level_values);
#	my $min_comms = $db->selectall_hashref("SELECT a_level, mcg FROM pay_level_mcg", 'a_level');
#0508	$gs->Prepare_UTF8($rpte);

	my $rv = $TT->process($tmp,
		{
			'RA' => $RA,
#			'min_comms' => $min_comms,
			'member' => $rpte,
			'lang_blocks' => $lang_blocks,
			'stats' => $stats,
			'cboCommissionMonths' => $IRB->cboCommissionMonths(),
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo,
			'level_values' => $level_values,
			'TNT_level_values' => $TNT_level_values,
			'brackets' => Brackets($stats),
			'qpp_qualification_value' => $qpp_qualification_value,
			'cgi' => $q,
			'DHSCC' => $DHSCC
		}, \$tmpl);

	unless ($rv)
	{
		die $TT->error();
	}

#0508	$tmpl = encode_utf8($tmpl);	# this may goober things up on the old server

	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_blocks->{'title'},
			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/greybox-jquery.css'),
			'content'=>\$tmpl
		});	
}

sub PowerPpCap	# the following is not valid with the now (spring 2013) version of power PP which has cap variations depending on member type)
				# but then again, we no longer attempt to show the power PP calculations either, so we will not worry about it right now
{
	# the idea is that the cap for any given interval will be the same across transaction types
	# so we will just take type 2
	# we are also assuming that there will be no interval overlap between valid entries
	my ($rv) = $db->selectrow_array('
		SELECT cap FROM configurations.power_pp
		WHERE trans_type=2
		AND COALESCE(?::DATE, NOW()::DATE) BETWEEN effective_date AND COALESCE(expiration_date, NOW()::DATE)
		', undef, ($params{'month'} || undef));
		
	return $rv;
}

sub ZeroNotNull
{
	return $_[0] || 0;
}

__END__

###### 01/04/12 besides retaining the basic routines, this report is really a transformation to a simple TT template rendering for now
###### Jan. 2012 vast modifications
###### 03/02/12 added the check for a Partner trying to look at an Exec's report
###### 03/06/12 finished the above allowing a first level deep lookup
###### 03/08/12 tweaked out Admin mode
###### 04/03/12 added ordering to the results in cboCommissionMonths
###### 04/11/12 revisions to the qualifiers including the replacement of frontline_pp with qpp
###### 04/24/12 made it so that we redirect to an older archive report script to handle Jan. - Mar. 2012 reports
###### 05/07/12 a gory hack to get two particular language nodes into production quickly
###### 06/12/12 just moved the real template back into place
###### 07/12/12 added $qpp_qualification_value to the mix of values available to TT in Output()
###### 08/14/12 added the use of Refcomp::Analysis to get data locked up in the analysis data of refcomp
###### 08/15/12 added the DB() routine so that we could make the connection at various unrelated places if needed
###### 10/01/12 added another template for August and September as the comp plan is changing again to omit the OP qualifier
###### 12/03/12 rolling out with the new inactive coach changes
###### 02/04/13 rolling out for the new TNT 2.0 comp plan
###### 03/13/13 adding the first modification to the new TNT plan... Leadership Bonuses effective for March
###### 05/08/13 adding another modification to the TNT plan... Exec bonuses effective for April
###### (however we can alter the current template in such a way as to make it work using TT logic)
###### 06/14/13 removed the data pull for average incomes and instead pull in min_comms
###### also disabled the 'behind' key in Brackets() since it was not working properly and it is no longer used anyway
###### 08/08/13 rolled out with the latest new thing in the plan, GRP
###### 09/02/13 revised to cover the latest version of the comp plan with Exec bonus shares, a different team commission shares way and the need for showing real PPP
###### 11/12/13 made the report available to Trial Partners as well
###### 11/23/13 added the subscription_weight into the mix of data... it will be used to alter the current month report if necessary
###### 07/11/14 more template changes to reflect another comp plan revision
###### 07/16/14 added UTF-8 decoding of the reportee to fix encoding issues when requesting non-English content
######		also added ZeroNotNull() and used it in various places where values were undef, most notably in certain archive reports
###### 10/08/14 significant changes include going to a file system based TT template as well as DHS::ConvertCurrency for doing formatting within the template
######		also reorganized some code at the top to get the reportee a little earlier since it is needed to initialize certain things like DHS::ConvertCurrency
######		also added the link() sub
###### 10/31/14 removed the used of the obsolete pay_level_mcg table
###### 02/05/15 added the star_lines_sv "column" to the current month query
# 05/08/15 fixed encoding issues that arose with the latest version of DBD::Pg
05/23/15	Added in a test for nopay reportee and the usage of a special version of refcomp to show them their potential income if they were to pay their fees
