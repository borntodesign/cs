#!/usr/bin/perl -w
use CGI;
print CGI::header();
#print CGI::header('text/plain');
#foreach (keys %ENV)
#{
#	print "$_ : $ENV{$_}\n";
#}
print "\nnow http\n\n";
foreach (CGI::http('Accept-Language'))
{
	print "$_\n";
}

require LWP::UserAgent;
 
my $ua = LWP::UserAgent->new;
$ua->default_header('Accept-Language' => CGI::http('Accept-Language'));

my $response = $ua->get('http://www.clubshop.com/gps.html');

if ($response->is_success) {
     print $response->content;  # or whatever
 }
 else {
     die $response->status_line;
 }