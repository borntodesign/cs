#!/usr/bin/perl
###### HP_enrollment.pl
###### process the Health Plan enrollment form
###### 02-22-2000	Bill MacArthur
use CGI ':all';
use strict;
use CGI::Carp qw(fatalsToBrowser);

###### Globals
my $mailprog = '/usr/sbin/sendmail';

my (@submit_errors);
my @recipients=qw/benefits@dhs-club.com prembfts@bellsouth.net/;

###### the format is 'field name' => 'Field description or prompt', validate flag
my %fields= (	IDNumber=>['Member ID Number',1],
		LastName=>['Last Name',1],
		MidI=>['M.I.',0],
		FirstName=>['First Name',1],
		Street=>['Street',1],
		Apt=>['Apt.',0],
		City=>['City',1],
		State=>['State',1],
		Country=>['Country',1],
		Zip=>['Postal Code',1],
		Hphone=>['Home Phone',1],
		Wphone=>['Work Phone',0],
		Fax=>['Fax',0],
		Email=>['E-Mail',1],
		Bdate=>['Birth Date',1],
		Gender=>['Gender',1],
		Social=>['Social Security #',1],
		Plan=>['Plan Choice',1],
		'Processing Fee'=>['Processing Fee',1],
		PayOption=>['Payment Option',1],
		BName=>['Billing Name',1],
		BAddress=>['Billing Address',1],
		BApt=>['Billing Address Apt.',0],
		BCity=>['Billing City',1],
		BState=>['Billing State/Province',1],
		BCountry=>['Billing Country',0],
		BZip=>['Billing Postal Code',1],
		BDayPhone=>['Billing Day Phone',1],
		BEvPhone=>['Billing Evening Phone',0],
		BCard=>['Name on Card',1],
		BNumbCard=>['Card Number',1],
		BMonth=>['Card Expiration Month',1],
		BYear=>['Card Expiration Year',1],
		BTypeCard=>['Card Type'],
		Referrer=>['Referrer',1],
		initial=>['You must `initial` the form.',1],
		thedate=>['You must `date` the form.',1]
);

foreach (keys (%fields)){
###### is the field flagged for validation?
	if ($fields{$_}[1] ==1){
		if (param($_) le ''){
	###### special handling for Social field for US citizens
			if ($_ eq 'Social'){
				my $country=uc(param('Country'));
				if ($country ne 'US' && $country ne 'USA' && $country ne 'UNITED STATES'){next}
			}
			push (@submit_errors,$fields{$_}[0]);
		}
		elsif ($_ eq 'Email'){
	###### perform rough email address validation
			if (&check_email(param($_))==0){
				my $msg='The E-mail address you submitted appears to be malformed. Please check it.';
				push (@submit_errors, $msg);
			}
		}
	}
}

if (scalar @submit_errors >0){
	print header(), start_html(-title=>'We require more information');
	print h3("I'm sorry. There is required information that is missing.");
	print h4('Please review the list below, then click your back button and provide the required information');
	print "<font color=\"#0000a0\">\n";
	foreach (@submit_errors){
		print "$_<BR>\n";
	}
	print "</font>\n";
	print h4('Thank You'), '<BR>Click ', a({-href=>'/contact.html'}, 'here'), ' if you need to contact us.';
	print end_html();
}
else {
###### all the fields have some kind of information so we'll proceed
###### first we'll check for trash language
###### let's wait on trash check, it may create problems
#	foreach (param()){
#		if (&Trash_Chek($_)==1){exit(0)}
#	}
	foreach (@recipients){
		&send_mail($_);
	}
###### send them a confirmation email with all the information
	&send_mail_client(param('Email'));
###### let's finish up by sending them a confirmation message page
	print redirect('https://clubshop.com/HP_app_confirmation.html');
}
exit(0);

sub check_email {
    # Initialize local email variable with input to subroutine.              #
    my $email = $_[0];

    # If the e-mail address contains:                                        #
    if ($email =~ /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/ ||

        # the e-mail address contains an invalid syntax.  Or, if the         #
        # syntax does not match the following regular expression pattern     #
        # it fails basic syntax verification.                                #

        $email !~ /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/) {

        # Basic syntax requires:  one or more characters before the @ sign,  #
        # followed by an optional '[', then any number of letters, numbers,  #
        # dashes or periods (valid domain/IP characters) ending in a period  #
        # and then 2 or 3 letters (for domain suffixes) or 1 to 3 numbers    #
        # (for IP addresses).  An ending bracket is also allowed as it is    #
        # valid syntax to have an email address like: user@[255.255.255.0]   #

        # Return a false value, since the e-mail address did not pass valid  #
        # syntax.                                                            #
        return 0;
    }

    else {

        # Return a true value, e-mail verification passed.                   #
        return 1;
    }
}

sub send_mail {

    # Open The Mail Program
    open(MAIL,"|$mailprog -t");

    print MAIL "To: $_[0]\n";
    print MAIL "From: Health_Plan_Enrollment_Form\@clubshop.com\n";

     print MAIL "Subject: Health Plan Enrollment Form\n\n";

	foreach (param()){
###### let's leave out blank fields
		if (param($_) gt ''){
			print MAIL "$_: ".param($_) . "\n\n";
		}
	}
	#print MAIL "\n";
    close (MAIL);
}

sub send_mail_client {

    # Open The Mail Program
    open(MAIL,"|$mailprog -t");

    print MAIL "To: $_[0]\n";
    print MAIL "From: Health_Plan_Application\@clubshop.com\n";

     print MAIL "Subject: Health Plan Application\n\n";
	print MAIL "Here is the list of information you have submitted.\n\n\n";
	foreach (param()){
###### let's leave out blank fields
		if (param($_) gt ''){
			if ($_ eq 'BNumbCard'){print MAIL "$fields{$_}[0]: < hidden >\n\n";}
			else{print MAIL "$fields{$_}[0]: ".param($_) . "\n\n";}
		}
	}
	print MAIL "\n";
	print MAIL "PLEASE CONTACT US IF ANY INFORMATION IS INCORRECT.\n\nThank You!\n\n";
    close (MAIL);
}

sub Trash_Chek {
# Check For Cusswords #
	my (@nasty, $cussword);
	open(CUSSWORDS, "./Data_files/cusswords.data");
	@nasty = <CUSSWORDS>; close (CUSSWORDS);

	foreach  $cussword (@nasty) {
		chop ($cussword);
		if ($_[0] =~ /$cussword/i ) {
			print header(), start_html();
			print "<P>Cusswords Submitted - Access Denied!</P>";
			print "<P>Please back up and remove any cusswords from your application and resubmit.</P>";
			print '</body></html>';
			return 1;
		}
	}
return 0;
}


