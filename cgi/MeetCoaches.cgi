#!/usr/bin/perl -w
###### MeetCoaches.cgi
###### A mini-"report", initially created to show Trial Partners their upline coaches
###### created: 10/23/13	Bill MacArthur
###### last modified:

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use lib ('/home/httpd/cgi-lib');
use XML::local_utils;
use G_S qw( $LOGIN_URL );
use DB_Connect;
#use Data::Dumper;	# only for debugging

my %TMPL = (
	'language_pack' => 11041,
	'main'	=> 11040,
	'loginprompt' => 11042
);

my ($db, $lb, @errors, $Path, @cookies, $sponsor, $coach, $exec, %tmplCache);

my $gs = G_S->new({'db'=>\$db});
my $q = new CGI;
my $TT = new Template;

#warn $q->cookie('AuthCustom_Generic');
my ($loginid, $login) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
#warn Dumper($login);
my $ckID = $q->cookie('id') || $q->param('id');

exit unless $db = DB_Connect('MeetCoaches');
$db->{'RaiseError'} = 1;

(undef, $Path) = split('/', $q->path_info);
$Path ||= '';	# if there was no path info, this makes it easier to evaluate without errors

if ($Path eq 'login')
{
	Login();
}

unless ($loginid && $loginid !~ m/\D/)	# they are fully logged in
{
	LoginPrompt()
}

MeetCoaches();

Exit();

###### 

sub Do_Page
{
	my $tmpl = shift || $TMPL{'main'};
	$tmpl = $gs->GetObject($tmpl) || warn "Failed to load template: $tmpl";
	print $q->header('-charset'=>'utf8', '-cookies'=>\@cookies);
	my $rv = $TT->process(
		\$tmpl, {
			'errors' => \@errors,
			'script_name' => $q->script_name,
			'member' => $login,
			'lb' => $lb,
			'sponsor' => $sponsor,
			'coach' => $coach,
			'exec' => $exec
		});
	print $TT->error unless $rv;	# $rv should be true if we were successful
	
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub FilterWhitespace
{
	my $p = shift || return '';
	$p =~ s/^\s*//;
	$p =~ s/\s*$//;
	return $p;
}

sub LoadLB
{
	my $member = shift;
	
	$tmplCache{$TMPL{'language_pack'}} ||= $gs->GetObject($TMPL{'language_pack'}) || warn "Failed to load language pack template: $TMPL{'language_pack'}";
	my $tmp = ();
	$TT->process(\$tmplCache{$TMPL{'language_pack'}}, {'member'=>$member}, \$tmp);
	my $lu = new XML::local_utils;
	$lb = $lu->flatXHTMLtoHashref($tmp);
}

sub Login
{
	my $alias = FilterWhitespace($q->param('alias'));
	my $pwd = FilterWhitespace($q->param('password'));

	unless ($alias && $pwd)
	{
		LoadLB();
		push @errors, $lb->{'missing_params'};
		LoginPrompt();
	}

	$login = $db->selectrow_hashref("
		SELECT id, spid, alias, alias AS username, firstname, lastname, emailaddress, membertype, country, COALESCE(status,'') AS status
		FROM members WHERE password=? AND " . ($alias =~ m/\D/ ? 'alias=?' : 'id=?'), undef, $pwd, uc($alias));
	
	unless ($login)
	{
		LoadLB();
		push @errors, $lb->{'login_mismatch'};
		LoginPrompt();
	}
	
	push @cookies, $gs->Create_AuthCustom_Generic_cookie($login);
	push @cookies, "id=$login->{'id'}; domain=.clubshop.com; path=/";
	MeetCoaches();
}

sub LoginPrompt
{
	if ($ckID)
	{
		$login = $db->selectrow_hashref("SELECT id, alias FROM members WHERE id= ?", undef, $ckID);
	}
	
	LoadLB($login);
	Do_Page($TMPL{'loginprompt'});
}

=head3 MeetCoaches

	
=cut

sub MeetCoaches
{
	# we need sponsor, coach and Exec... realizing that any or all could be the same
	$sponsor = $db->selectrow_hashref("
		SELECT m.id, m.alias, m.firstname, m.lastname, m.emailaddress, m.other_contact_info AS skype_name
		FROM my_upline_vip($login->{'spid'}) m
	");
	
	$coach = $db->selectrow_hashref("
		SELECT m.id, m.alias, m.firstname, m.lastname, m.emailaddress, m.other_contact_info AS skype_name
		FROM my_coach($login->{'id'}) m
	");
	
	$exec = $db->selectrow_hashref("
		SELECT m.id, m.alias, m.firstname, m.lastname, m.emailaddress, m.other_contact_info AS skype_name
		FROM my_exec($login->{'id'}) m
	");
	
	UplineDetails($_)foreach ($sponsor, $coach, $exec);
	
	LoadLB($login);
	Do_Page();
}

sub UplineDetails
{
	my $upline = shift;
	($upline->{'prw'}) = $db->selectrow_array("
		SELECT val_text FROM member_options WHERE option_type=2 AND id= $upline->{'id'}
	");
}