#!/usr/bin/perl -w
# AuthCustom_Generic_toJSON.cgi
# a simple script to help transition from perl generated full login cookies to PHP
# we will look for our customary AuthCustom_Generic cookie and take the hash, convert it to JSON and return it
# a PHP script can digest that value and do whatever... probably create a session so that this resource does not have to be called again
# created: 04/02/14	Bill MacArthur
# last modified: 

use strict;
use JSON;
use CGI();
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S;

# Globals
my $cgi = new CGI;
my $gs = new G_S;
my ( $id, $meminfo ) = $gs->authen_ses_key( $cgi->param('ck') || '' );
#warn $cgi->remote_addr;

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	$gs->Prepare_UTF8($meminfo);
	print $cgi->header('text/javascript'), encode_json($meminfo);
}
###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{

	print $cgi->header('text/javascript');
}

exit;
