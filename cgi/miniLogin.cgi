#!/usr/bin/perl -w
###### miniLogin.cgi
###### a simple login similar to the current ppreport login
###### if they enter a valid ID/alias we'll setup a simple id cookie
###### and pa cookie (if they are a member)
###### the simple ID cookie can be used to shop without a real user/pwd login
###### created: 12/10/04	Bill MacArthur
###### last modified: 06/11/15	Bill MacArthur
###### version 1.1

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use DB_Connect;

my (@cookies, $meminfo, $db) = ();
my $q = new CGI;
$q->charset('utf-8');
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my %TMPL = (
	login		=> 10357,
	login_return	=> 10358,	# this should be a simple javascript to close the window
	login_error	=> 10359	# and hopefully refresh the caller
);

if ($q->param('logout'))
{
	Logout();
}

my $id = $q->param('id') || '';
if (length $id > 15)
{
	Err('Invalid ID');
	exit;
}
$id = $gs->PreProcess_Input($id);

# the intention is to have this as a URL to which we'll 'redirect' after logging in
my $forward_to = $q->param('forward_to') || '';
my $cid = $q->param('cid') || '';

###### we don't need a DB connection until now

$db = DB_Connect::DB_Connect('miniLogin') || exit;

if (! $id)
{
	###### present default screen
	my $tmpl = $gs->Get_Object($db, $TMPL{login}) || die "Failed to load template $TMPL{login}\n";
	$tmpl =~ s/%%forward_to%%/CGI::unescape($forward_to)/eg;
	$tmpl =~ s/%%forward_to_escaped%%/CGI::escape($forward_to)/eg;
	$tmpl =~ s/%%script_name%%/$q->script_name/e;
	$tmpl =~ s/%%logout%%/$q->param('logout') || ''/e;

	# since we don't have an id, we'll use a passed cid (ID who gets Credit :>)
	$tmpl =~ s/%%id%%/$cid/g;
	print $q->header(-cookies=>\@cookies), $tmpl;
}
else
{
	my $rv = Login();
	if ($rv)
	{
		my $tmpl = $gs->Get_Object($db, $TMPL{login_return}) || die "Failed to load template $TMPL{login_return}\n";
		$tmpl =~ s/%%forward_to%%/CGI::unescape($forward_to)/eg;
		$tmpl =~ s/%%id%%/$meminfo->{'id'}/g;
		print $q->header(-cookies=>\@cookies), $tmpl;
	}
	###### errors should have been handled
}
END:
$db->disconnect;
exit;

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Login
{
	my $qry = "SELECT m.id, m.alias, m.country FROM members m ";
	###### if our ID is alphanumeric, then we'll assume an alias
	###### otherwise if its seven digits or less we'll assume an id
	###### else we'll try a clubbucks ID
# revised 03/25/10 to allow a full length DHS0000840228 card number login
	$id =~ s/^DHS(\d{10})$/$1/i;
	
	if ($id =~ /\D/){ $qry .= 'WHERE m.alias= ?' }
# they don't want numeric logins
#	elsif (length $id <= 7){ $qry .= 'WHERE m.id= ?' }
	else
	{
		$qry .= ',clubbucks_master cb
			WHERE 	cb.current_member_id=m.id
			AND 	cb.id= ?::BIGINT';
	}
	
	$meminfo = $db->selectrow_hashref($qry, undef, uc $id);			
	unless ($meminfo->{'id'})
	{
		my $tmpl = $gs->GetObject($TMPL{login_error}) || die "Failed to load template $TMPL{login_error}\n";
		$tmpl =~ s/%%script_name%%/$q->script_name/e;
		print $q->header('-expires'=>'now'), $tmpl;
		return;
	}
	
	my ($pnsid) = $db->selectrow_array("SELECT pnsid FROM mviews.positions_freshest WHERE id= $meminfo->{'id'}");
	push (@cookies, $q->cookie('-name'=>'pnsid', '-domain'=>'.clubshop.com', '-expires'=>'+1y', '-value'=>$pnsid)) if $pnsid;
	
	push (@cookies, $q->cookie('-name'=>'id', '-domain'=>'.clubshop.com', '-expires'=>'+1y', '-value'=>$meminfo->{'id'}));
	push (@cookies, $q->cookie('-name'=>'pa', '-domain'=>'.clubshop.com', '-expires'=>'+1y', '-value'=>$meminfo->{'alias'}));
	push (@cookies, $q->cookie('-name'=>'country', '-domain'=>'.clubshop.com', '-expires'=>'+1y', '-value'=>$meminfo->{'country'}));
	
	# expire the existing minidee cookie... it will get created again when the page is reloaded
	push (@cookies, $q->cookie('-name'=>'minidee', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));

	return 1;
}

sub Logout
{
	###### just disable the cookies
	push (@cookies, $q->cookie('-name'=>'id', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
	push (@cookies, $q->cookie('-name'=>'pa', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
	push (@cookies, $q->cookie('-name'=>"AuthCustom_Generic", '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
	push (@cookies, $q->cookie('-name'=>'minidee', '-domain'=>'.clubshop.com', '-expires'=>'-1d', '-value'=>''));
}

###### 12/16/04 added the .clubshop.com domain to the cookies
###### 01/11/05 disable numeric logins, they'll have to use a full alias or a CBID
# 05/04/06 added the minidee cookie to the list for logout deletion
# 07/15/10 added expiring of the minidee cookie on login... in case they have a minidee from a previous login
# 06/11/15 added the pnsid cookie