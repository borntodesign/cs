#!/usr/bin/perl -w
# trying the -w flag which should have been used from the get-go

# last modified: 09/09/11	Bill MacArthur

=head1 NAME:
cancel_submit.cgi

	The submit program for merchant cancel options.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Accept the parameters submitted by the merchant cancel screen, then processes the cancelation requests.

=head1
PREREQUISITS:

	strict
	warnings
	FindBin
	lib
	DHSGlobals
	CGI
	DB
	DB_Connect
	G_S
	DHS::UrlSafe
	URI::Escape
	MerchantAffiliates
	MailTools

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
#use lib "$FindBin::Bin/../../cgi-lib";
# the above is a pain when you want to test in a tmp directory and since we really know the path, let's just use it
use lib '/home/httpd/cgi-lib';
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use DHS::UrlSafe;
use URI::Escape;
use MerchantAffiliates;
use MailTools;
use CGI::Carp qw(fatalsToBrowser);

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	$db
	$MerchantAffiliates
	$CGI
	$DHS_UrlSafe
	$G_S

=cut

#our $ObjectName = SomeClass->new();
our $db = {};
my $MerchantAffiliates = {};


my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $G_S = G_S->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	%url_query_strings
	HASH
	Used to hold different parameters passed
	back to the display screen.  These values
	are a looped through, and assigned to
	the "url_query_string" variable.
	
	$url_query_string
	STRING
	The Query String created from the "url_query_strings" hash.
	
	%messages
	HASH
	The Notices, Warnings, and Errors passed back to the display
	
	$merchant_information
	HASHREF
	This holds the merchant information to update.
	
	$url
	STRING
	Hold the URL to forward the browser to.

=cut

my %url_query_strings = ();
my $url_query_string = '';
my %messages = ();
my $merchant_information = {};

my $url = $CGI->url();
$url =~ s/\.cgi/_status\.cgi/;

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

####################################################
# Main
####################################################

exit unless $db = DB_Connect('merchant_affiliates');
$db->{'RaiseError'} = 1;

eval{


	$MerchantAffiliates = MerchantAffiliates->new($db);

	die "Data paramter not received" unless $CGI->param('data');
	
#die $DHS_UrlSafe->decode($CGI->param('data'));
	($messages{'params'}->{'merchant_id'}, $messages{'params'}->{'action'}) = split(':', $DHS_UrlSafe->decode($CGI->param('data')));
#($messages{'params'}->{'merchant_id'}, $messages{'params'}->{'action'})= (40,'KEEP');

	if (exists $messages{'params'}->{'merchant_id'} && $messages{'params'}->{'merchant_id'} !~ /^\d+$/)
	{
		$messages{'warnings'}->{'merchant_id_incorrect_data_format'} = 'The information provided was not in the expected format.  Contact support.';
	}
	if (exists $messages{'params'}->{'action'} && $messages{'params'}->{'action'} !~ /^\w+$/)
	{
		$messages{'warnings'}->{'action_incorrect_data_format'} = 'The information provided was not in the expected format.  Contact support.';
	}
	
	$merchant_information = $MerchantAffiliates->retrieveMerchantMasterRecordByID($messages{'params'}->{'merchant_id'});
#warn Dumper($merchant_information);	
	$messages{'params'}->{'action'} = uc($messages{'params'}->{'action'}) if exists $messages{'params'}->{'action'};
	
	if(
		exists $messages{'params'} &&
		exists $messages{'params'}->{'action'} &&
		($messages{'params'}->{'action'} eq 'DOWNGRADE' || $messages{'params'}->{'action'} eq 'ACTIVATE')
	)
	{
		
		if($messages{'params'}->{'action'} eq 'ACTIVATE')
		{
			$merchant_information->{'status'} = 1;
		}
		
		#TODO: Decide what to do if their status is not active.
		#$merchant_information->{status}
		
		#We will not be changing the merchants payment information, it may be needed for billing.
		$merchant_information->{'discount_type'} = 15;
		$merchant_information->{'special'} = 0.1;
		$merchant_information->{'exception_percentage_of_sale'} = 0.0;
		
		my $subscription = {};
		$subscription->{'status'} = 'UPGRADE';
		$subscription->{'description'} = 'Upgraded by the Merchant. Merchant ID: ' . $messages{'params'}->{'merchant_id'};
		
		my $updated = $MerchantAffiliates->updateMerchantMasterInformation($messages{'params'}->{'merchant_id'}, $merchant_information, undef, undef, $subscription);
		
		if(! $updated || $updated eq '0E0')
		{
			$messages{'warnings'}->{'merchant_was_not_changed'} = 'Your information was not updated';
			#TODO: This rollback doesn't seem to server any purpose.  Look into it.  If updateMerchantMasterInformation fails the method should have already rolled things back.
		#	$MerchantAffiliates->{'db'}->rollback();
			# from the server error log: "rollback ineffective with AutoCommit enabled"
			# IMHO (Bill) BEGINs and ROLLBACKs belong where the SQL is occurring rather than after the fact ;)
			# either that or you issue a BEGIN in the same context as you issue a ROLLBACK ;)
		}
		else
		{
			$MerchantAffiliates->deleteMerchantPaymentPendingRecord($messages{'params'}->{'merchant_id'});
			my $blurb_updated = $MerchantAffiliates->adjustMerchantBlurbForCurrentPackageBenefits($messages{'params'}->{'merchant_id'});
			my %returned_keywords_update = $MerchantAffiliates->adjustMerchantKewordsForCurrentPackageBenefits($messages{'params'}->{'merchant_id'});
			
			$messages{'notices'}->{'blurb_adjusted'} = 'The blurb was truncated' if $blurb_updated;
			$messages{'notices'}->{'keywords_adjusted'} = 'The keywords were truncated.' if (exists $returned_keywords_update{'notices'}->{'max_number_of_keywords_exceeded'} && $returned_keywords_update{'notices'}->{'max_number_of_keywords_exceeded'});
			
			$messages{'notices'}->{'package_downgraded'} = 'Your profile is now updated and please login to your merchant interface account to make any other updates.' if($messages{'params'}->{'action'} ne 'ACTIVATE');
			
			if($messages{'params'}->{'action'} eq 'ACTIVATE')
			{
			
				$MerchantAffiliates->updateVendorStatusByMerchantID($messages{'params'}->{'merchant_id'}, $merchant_information->{'status'});
				$MerchantAffiliates->deleteMerchantPaymentPendingRecord($messages{'params'}->{'merchant_id'});
				
				$MerchantAffiliates->setMembers();
				my $member_type_updated = $MerchantAffiliates->{'Members'}->updateMember($merchant_information->{'member_id'}, {'membertype'=>'ma'});
				
				$messages{'notices'}->{'package_activated'} = 'Your merchant status is now activated again and please login to your merchant interface account to make any other updates.';
				
				if(! $member_type_updated || $member_type_updated eq '0E0')
				{
					
					use Data::Dumper;
					
					my %email = 
					(
						'to'=>'errors@dhs-club.com',
						'subject'=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
					);
					
					$email{'text'} = "A Merchant reactivated their Merchant Account, but their membership type was not changed to Merchant Affiliate. \n\n merchant_information:\n" . Dumper($merchant_information);
					
					my $MailTools = MailTools->new();
					$MailTools->sendTextEmail(\%email);
					
					$messages{'errors'}->{'script_error'} = [$email{'text'}];
				}
				
			}
		}
	}
	elsif($messages{'params'}->{'action'} && $messages{'params'}->{'action'} eq 'KEEP')
	{
		#If a merchant has been marked inactive, set them to active.
		#TODO: Decide what to do if their status is not active.
		#$merchant_information->{status}
		die "You cannot KEEP this merchant at this time. Please contact us. Your Merchant Status: $merchant_information->{'status'}" unless grep $merchant_information->{'status'} == $_, (0,1);

		# this is rough and crude, but in order to not get errors when clicking an email link more than once per day
		# just check to see if they have a subscription that extends beyond this month and if so, let's call it good
		# Obviously, if this action is anything more than a simple I wanna keep my status, we run the risk of goobering up business logic
		# but since we don't know what drove the original business logic and we don't want to rewrite huge chunks of code,
		# we'll try this simple solution

		my ($ck) = $db->selectrow_array('
			SELECT pk FROM subscriptions
			WHERE subscription_type BETWEEN 6 AND 9
			AND end_date > last_of_month()
			AND member_id=?',
			undef, $merchant_information->{'member_id'});
		if ($ck)
		{
			$messages{'notices'}->{'status_updated'} = 'Your profile is now updated, please login to your merchant interface account to make any other updates.';
			warn "We have found a type 6 subscription good past this month";
		}
		else
		{
#			my $subscription = {};
#			$subscription->{'status'} = 'UPGRADE';
#			$subscription->{'description'} = 'Upgraded by the Merchant. Merchant ID: ' . $messages{'params'}->{'merchant_id'};
			
#			my $updated = $MerchantAffiliates->updateMerchantMasterInformation($messages{'params'}->{'merchant_id'}, $merchant_information, undef, undef, $subscription);
				
			my @values = ($merchant_information->{'member_id'}, $MerchantAffiliates->{'__discount_type__subscription_type__mapping'}->{$merchant_information->{'discount_type'}});
			my ($create_merchant_subscription_returned_pk) = $db->selectrow_array('
				SELECT create_merchant_subscription(?, ?, NOW())', undef, @values) ||
				
				die 'The subscription could not be upgraded @values = (' .
					$merchant_information->{'member_id'} .
					',' .
					$MerchantAffiliates->{'__discount_type__subscription_type__mapping'}->{$merchant_information->{'discount_type'}} . ')';

			# if they have been deactivated before clicking the link, according to the business logic that prevailed at the time of the original code,
			# we should theoretically not allow them to KEEP
			# but... there is nothing built to interact with them, so we will let them keep if the subscription went through
			my $updated = $create_merchant_subscription_returned_pk;
			
			if ($merchant_information->{'status'} == 0)
			{
				$updated = $db->do("
					UPDATE merchant_affiliates_master SET
						status=1, notes='status set to active by merchant_options.cgi upon KEEP'
						WHERE id= ?", undef, $merchant_information->{'id'});
			}
			
			if(! $updated || $updated eq '0E0')
			{
	#warn "HEREEEEEEEEE";
				$messages{'warnings'}->{'merchant_was_not_changed'} = 'Your information was not updated';
				#TODO: This rollback doesn't seem to server any purpose.  Look into it.  If updateMerchantMasterInformation fails the method should have already rolled things back.
			#	$MerchantAffiliates->{'db'}->rollback();
				# from the server error log: "rollback ineffective with AutoCommit enabled"
				# IMHO (Bill) BEGINs and ROLLBACKs belong where the SQL is occurring rather than after the fact ;)
				# either that or you issue a BEGIN in the same context as you issue a ROLLBACK ;)
			}
			else
			{
				
				$MerchantAffiliates->setMembers();
				my $member_type_updated = $MerchantAffiliates->{'Members'}->updateMember($merchant_information->{'member_id'}, {'status'=>1});
				
				if($member_type_updated && $member_type_updated ne '0E0')
				{
					$messages{'notices'}->{'status_updated'} = 'Your profile is now updated, please login to your merchant interface account to make any other updates.';
				}
				else
				{
					
					use Data::Dumper;
					
					my %email = 
					(
						'from'=>'errors@dhs-club.com',
						'to'=>'errors@dhs-club.com',
						'subject'=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
					);
					
					my $temp = \%messages;
					$email{'text'} = "A Merchant opted to keep their Merchant Account, but their status type was not updated. \n\n MerchantAffiliates->{'Members'}->updateMember($merchant_information->{member_id}, {status=>1}): " . $member_type_updated . "\n\n merchant_information:\n" . Dumper($merchant_information) . "\n\n messages: " . Dumper($temp) . "\n\n CGI->param('data'): " . $CGI->param('data');
					
					my $MailTools = MailTools->new();
					$MailTools->sendTextEmail(\%email);
					
					$messages{'errors'}->{'script_error'} = $email{'text'};
				}
			
			}
		}	# end of subscription check else{} block
	}
	else
	{
		$messages{'warnings'}->{'select_action'} = 'One of the expected options were not available.';
	}
	
};
if($@)
{
	
	my $error_message = $@;
	my %email = 
	(
		'from'=>'errors@dhs-club.com',
		'to'=>'errors@dhs-club.com',
		'subject'=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
	);
	
	my $temp = \%messages;

	$email{'text'} = "There was a fatal error performing an action on a merchants account. Error Message: $error_message \n\n merchant_information:\n" . Dumper($merchant_information) . "\n\n messages: " . Dumper($temp) . "\n\n CGI->param('data'): " . $CGI->param('data');
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	$messages{'errors'}->{'script_error'} = [$email{'text'}];
	die $error_message;
}


# Decide where to redirect to, so they don't hit the refresh button
$messages{'params'}->{'discount_type'} = $messages{'params'}->{'discount_type'};

$url_query_strings{'data'} = $DHS_UrlSafe->encodeHashOrArray(\%messages);

foreach (keys %url_query_strings)
{
	$url_query_string .= '&' if $url_query_string;
	$url_query_string .= "$_=" . URI::Escape::uri_escape($url_query_strings{$_});
}

$url .= "?$url_query_string" if $url_query_string;

print $CGI->redirect('-uri'=>"$url");


exitScript();

__END__

=head1
ASSOCIATED SCRIPTS

L<maf::merchant_options_status.cgi>

=head1
CHANGE LOG

	2010-10-26	Keith	Updated the "KEEP" section to update the date of their subscription.
	2011-08-16	Bill MacArthur	trying the -w flag and also testing if they have clicked the KEEP link before
				using a crude hack by testing for a type 6 susbscription good into the future
	2011-09-09  Bill MacArthur	made it so that upon a KEEP, only the subscription is touched instead of the whole master record
				Also update the status to 1 (active) if set to 0 (inactive). Warn them if the status is anything besides 0 or 1 
	
=cut


