#!/usr/bin/perl

=head1 NAME:
script_name.cgi

	Brief description of the display script

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the display script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname, DHS::UrlSafe

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use MerchantAffiliates;
use DHS::UrlSafe;
use XML::local_utils;
use XML::Simple;
use DHS::Templates;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $DHS_Templates = DHS::Templates->new();
=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my $xml = '';
my $xsl = '';

my $params = {};
my $params_xml = '';

my $messages = {};
my $messages_xml = '';

my $countries_xml = '';

my $html_template = '';

my %template_values = ();

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


####################################################
# Main
####################################################

unless ($db = DB_Connect('merchant_affiliates')){exit}
$db->{RaiseError} = 1;

eval{
	
	if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100823)
	{
		$xml = G_S::Get_Object($db, 10816);
		$xsl = G_S::Get_Object($db, 10815);
		$html_template = $DHS_Templates->retrieveShtmlTemplate('/cs/_includes/templates/merchant_shell.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'));
	}
	else
	{
		$xml = G_S::Get_Object($db, 10814);
		$xsl = G_S::Get_Object($db, 10813);
		$html_template = $DHS_Templates->retrieveShtmlTemplate('test_shell.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'));
	}
	
	$messages = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data')) if $CGI->param('data');
	
	if(exists $messages->{params})
	{
		$params = $messages->{params};
		delete $messages->{params};
	}
	
	
	$messages_xml = XML::Simple::XMLout($messages, RootName => "data", NoAttr => 1 );
	
	
	my	$output_xml = <<EOT;
		<base>
			$messages_xml
			$xml
		</base>
		
EOT


$template_values{PAGE_CONTENT} = XML::local_utils::xslt($xsl, $output_xml);

print $CGI->header('text/html');
print $DHS_Templates->processHtmlTemplate(\%template_values, $html_template);
	
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}



exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


