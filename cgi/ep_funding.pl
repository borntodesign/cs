#!/usr/bin/perl -w
###### ep_funding.pl
######
###### A script that receives a transaction for pre-pay funding from EcoPay
######
###### this script started as a copy of ap_funding.pl
###### created: April 2009	Bill MacArthur
######
###### modified:
	
use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use G_S;
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);

###### CONSTANTS
my @EP_IPs = qw/217.21.162.164 217.21.162.162 213.129.76.104 213.129.76.105 217.21.160.97/;
 
# what we will place in front of the transaction ID from Alert Pay to ensure we don't find dupes on simply the number itself
my $FUNDING_TRANS_PREFIX = 'EP Trans ID: ';
my $FUNDING_TRANS_TYPE = 3007;

our @numerics = qw/TxID SVSTransactionID vip_id/;
our $TRACKING = 1;		# Change to 0 for no logging.
our $PATH = "/home/httpd/cgi-logs";	# Where the logs are kept.
#our $ACCOUNTING_ADDRESS = 'acfspvr@dhs-club.com';
my $ACCOUNTING_ADDRESS = 'webmaster@dhs-club.com';
our @fieldnames = qw/
	TxID
	SVSTransactionID
	Amount
	Currency
	ClientAccountNumberAtMerchant
	ClientAccountNumber
	EventID/;

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $cgiini = "ep_funding.cgi";

our $mem_info = '';
our $errors = '';
our $params_hash = {};

################################
###### MAIN starts here.
################################

exit unless my $db = DB_Connect('generic');
$db->{RaiseError} = 1;

# Open the log files if logging is turned on above.
open (TRACK_LOG, ">>$PATH/ecopay_tracking.log") if $TRACKING;

# Load the params into a local hash.
Load_Params();

# Log the data sent to us by EcoPay
Log(scalar(localtime) . " - ");
Log("ID#$params_hash->{vip_id}; " . $q->query_string);

End() unless PreliminaryChecks();
End() unless Check_Params();
End() unless my $TempRecord = TempRecordCk();

# Get the member's info.
$mem_info = $db->selectrow_hashref('SELECT * FROM members WHERE	id = ?', undef, $params_hash->{vip_id});
if ( $mem_info && (my $trans_num = Insert_Funds()) > 0 )
{
# forget about email
#		my $email = Member_Message($trans_num);
#		Email_Someone($email);
}
# Otherwise, let accounting know about the problem.
else
{
	my $email_message = '';
	my $submsg = '';
	unless ($mem_info)
	{
		$email_message = "Cannot find a VIP with ID: $params_hash->{vip_id}\n";
		AppendErrors("Cannot identify a membership with ID: $params_hash->{vip_id}");
		$submsg = 'Cannot identify membership';
	}
	elsif ( $trans_num < 0 )
	{
		my $tmp_trans = -1*$trans_num;
		$email_message = qq!The EcoPay website submitted a duplicate of SOA transaction #$tmp_trans. The duplicate was not added to any Club Account. Forward this on to IT:\n!;
		$submsg = "Duplicate SVSTransactionID: $params_hash->{SVSTransactionID}";
		AppendErrors($submsg);
	}
	else
	{
		$email_message = qq!This EcoPay funds transfer was completed, but was not credited to the member's Club Account due to a database error. Please credit it to the member's account via the admin interface as soon as possible:\n!;
	}
	$email_message .= qq!
	DHS ID:	$mem_info->{alias}
	Name: $mem_info->{firstname} $mem_info->{lastname}
	Amount:	$params_hash->{Amount}
	Ref. ID: $params_hash->{SVSTransactionID}!;
	my %email = (	to 		=> $ACCOUNTING_ADDRESS,
			bcc	 	=> '',
			subject 	=> 'Error - Club Account Funding System - EcoPay',
			from 		=> 'ep_funding.cgi<noreply@clubshop.com>',
			message 	=> $email_message);
	Email_Someone(\%email);
	# Log the insertion error.
	Log(" ----- ");
	Log("error= transaction insertion error:$submsg: ID#$params_hash->{vip_id}; amount=$params_hash->{Amount}");
}

End() if $errors;
print $q->header('text/plain'), "OK";

End();

################################
###### Subroutines start here.
################################

sub AppendErrors
{
	$errors .= "$_[0]\n";
}

###### Check the parameters for errors.
sub Check_Params
{
	# Return immediately unless this is a processed transaction.
	AppendErrors("EventID != 1 per docs") if $params_hash->{EventID} ne '1';

	# Clear the fields of the standard garbage.
	foreach my $key (keys %{$params_hash})
	{
		$params_hash->{$key} = G_S::PreProcess_Input($params_hash->{$key});
		if ( (grep { /$key/ } @numerics) && ($params_hash->{$key} !~ m/^\d+\.?\d*$/) )
		{
			AppendErrors("The $key field value ($params_hash->{$key}) must be numeric.");
		}
	}
	# Check required fields as necessary.
	foreach ( @fieldnames )
	{
		# All fields are required for this script.
		if ( ! defined $params_hash->{$_}
			|| $params_hash->{$_} eq ''
			|| $params_hash->{$_} eq 'blank')
		{
			AppendErrors("Missing value for the field: $_");
		}
	}

	AppendErrors("Currency of transaction is not USD") if $params_hash->{Currency} ne 'USD';

	return 1 unless $errors;

	my $email_message = qq!This EcoPay funds transfer was sent to us with a bad parameter(s), so it was not entered into the Club Account system. Contact IT for assistance:\n\n!;
	$email_message .= qq!$errors\n\nParameters:\n!;
	foreach (sort keys %{$params_hash} )
	{
		$email_message .= "\t$_= " . ($params_hash->{$_} || '') . "\n";
	}
	my %email = (
			to 		=> $ACCOUNTING_ADDRESS,
			bcc	 	=> '',
			subject	=> 'Error - Club Account Funding System - EcoPay',
			from 	=> 'ep_funding.cgi<noreply@clubshop.com>',
			message => $email_message);
	Email_Someone(\%email);

	# Log the insertion error.
	Log(" ----- ");
	Log("error= parameter format error; ID#$params_hash->{vip_id}; amount=$params_hash->{ap_totalamount}");
	return 0;
}

###### Generate e-mail to the appropriate party.
sub Email_Someone
{
	use MIME::Lite;
	my $email_hash = shift;

	# Build the message
	my $message = MIME::Lite->new(
           	From     => $email_hash->{from},
           	To       => $email_hash->{to},
           	Bcc      => $email_hash->{bcc},
           	Subject  => $email_hash->{subject},
           	Type     => 'text/plain; charset="utf-8"',
           	Encoding => '7bit',
           	Data     => $email_hash->{message});
	# Send the message
	$message->send('smtp','mail.dhs-club.com');

	return 1;
}

sub End
{
	print $q->header('text/plain'), "ERROR:\n$errors" if $errors;
	close TRACK_LOG if $TRACKING;
	$db->disconnect if $db;
	exit;
}

###### Insert funds into their pre-pay account - soa table and return the transaction number.
sub Insert_Funds
{
	# Check to see if this transaction has already been entered.
 	my ($trans_num) = $db->selectrow_array('
 		SELECT trans_id 
		FROM soa 
		WHERE description = ?', undef, ($FUNDING_TRANS_PREFIX . $params_hash->{SVSTransactionID}));

	# Build the query if not a duplicate and we can get a transaction number from the soa table.
	if ($trans_num){ return -1 * $trans_num; }	# A duplicate was found.
	elsif ( $trans_num = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')") )
	{
		my $rv = $db->do("INSERT INTO soa (
						trans_id,
						trans_type,
						id,
						amount,
						description,
						operator )
				VALUES ( $trans_num, $FUNDING_TRANS_TYPE, ?, ?, ?, ? );
				
				DELETE FROM ecopay_temp WHERE pk=$TempRecord;", undef,
						$params_hash->{vip_id},
						$params_hash->{Amount},
						($FUNDING_TRANS_PREFIX . $params_hash->{SVSTransactionID}),
						$cgiini);

		if ( $rv eq '1' ) { return $trans_num; }
		else 
		{
			# Log the authorization in case we have problems later.
			Log(" ----- ");
			Log("error= transaction insertion error; ID#$mem_info->{id}; amount=$params_hash->{amount}");

			return 0;
		}
	}
	else { return 0; }					# Couldn't get a transaction number.
}

###### Load the params into a local hash.
sub Load_Params
{
	# Get the submitted parameters.
	foreach ( $q->param() )
	{
		$params_hash->{$_} = $q->param($_) || '';
	}
	# rather than rewriting, we will create the vip_id field from a value we will really receive
	$params_hash->{vip_id} = $params_hash->{ClientAccountNumberAtMerchant} || '';
}

sub Log
{
	return unless $TRACKING;
	print TRACK_LOG "$_[0]\n";
}

sub PreliminaryChecks
{
# for testing
#return 1;
	my $remote_addr = $q->remote_addr;
	unless (grep $_ eq $remote_addr, @EP_IPs){
		my $msg = '';
		$msg = "IP address doesn't match an IP on the EcoPay list as of March 2009: $remote_addr";
		Log($msg);
		AppendErrors($msg);
		return 0;
	}
	return 1;
}

sub TempRecordCk
{
	# EcoPay requires a transaction ID value outbound to them
	# and recommends that we check the transaction ID inbound back from them, so we will
	# the leftovers in this table indicate transaction attempts that never finalized
	my ($rv) = $db->selectrow_array('SELECT pk FROM ecopay_temp WHERE pk= ? AND id= ? AND amount= ?',
		undef, $params_hash->{TxID}, $params_hash->{vip_id}, $params_hash->{Amount});
	AppendErrors("Cannot find an ecopay_temp transaction matching these values: pk=$params_hash->{TxID}, id=$params_hash->{vip_id}, amount=$params_hash->{Amount}") unless $rv; 
	return $rv;
}
