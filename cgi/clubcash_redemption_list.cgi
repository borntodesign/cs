#!/usr/bin/perl -w
# clubcash_monthly_detail.cgi
# create the necessary XML file for * given id and a given month/months

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use ParseMe
    require XML::Simple;
    require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':all';
use DBI;
use DB_Connect;
use ClubCash;

###### GLOBALS/Cookies

my $q = new CGI;
my $db = '';
our $gs = new G_S;
our $ME = $q->script_name;
our $xs = new XML::Simple(NoAttr=>1);
our $cgiini = 'clubcash_redemption_list.cgi';
our $messages = '';
my  $mem_info = '';
our $member_details = ();
our $login_error = 0;
our $login_type = '';
our %TMPL = ('xml'=> 10863,  
	     );
my $fnt='';
my $fnte='';
my $bgcolor1 = "#99ccff";
my $bgcolor2 = "#ffffff";
my $rowcount = 0;

# Get and test the cookie information for access.

my  $member_id = 0;
Get_Cookie();

if ($member_id == 12) {
    $member_id = 3218569;
}

# Get the xml language for putting out the page and convert to hash

my $xmt = $gs->Get_Object($db, $TMPL{xml}) || 
                                  die "Failed to load template: $TMPL{xml}\n";
my $lu = new XML::local_utils;
my $xml = $lu->flatXMLtoHashref($xmt);

##############################
# Check For ID Number or Die #
##############################

    if ($member_id eq '') {
	print header();
        print start_html();
	print "<P>Access Denied! No ID Number Submitted.<P>\n";
	print end_html();
	exit;
    }

#
# Get the members info for the top of the page
#

my $qry = "SELECT firstname,
                  lastname,
                  alias,
                  id
           FROM members
           WHERE id = $member_id;";
my $mdata = $db->prepare($qry);
my $rv = $mdata->execute;
my $tmp2 = $mdata->fetchrow_hashref;

my $lang = $gs->Get_LangPref();
#warn "Language preference is $lang\n";

#
# Get the data for the desired time period
#

$qry = "SELECT rt.id, 
                  rt.reward_points, 
                  rt.reward_type,
                  rt.currency,
                  rt.cur_xchg_rate,
                  rt.notes,
                  rt.created::DATE,
                  rt.date_reported,
                  coalesce(rttt.description,rtt.description,'Unknown') as description
           FROM reward_transactions rt
           LEFT JOIN reward_trans_types rtt ON
                (rt.reward_type = rtt.type)
           LEFT JOIN reward_trans_type_translated rttt ON
                (rttt.orig_type = rtt.type and rttt.lang_id = '$lang')
           WHERE rt.id = $tmp2->{id} AND
                 rt.reward_type in (2, 5, 8, 9, 10, 13, 14)
           ORDER BY rt.date_reported desc;";
#print $qry."\n";
my $tdata = $db->prepare($qry);
$rv = $tdata->execute;

# Set some stuff up for the report


print header();
print start_html(-title=>'$xml->{title}', 
                 -style=>{-src=>'/css/reward_details.css'});
print "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
print "   <tr>\n";
print "   <td align='left'> <div align='center'><span class='Blue_Large_Bold'>$xml->{page_header}</span></div></td></tr>\n";
print "   <tr><td><p align='center' class='Orange_Large_11'>$xml->{for} $tmp2->{firstname} $tmp2->{lastname}, $tmp2->{alias}</p>\n";
print "   </td></tr>\n";
print "   <tr> <td>";
print "<table border='0' width='100%' cellpadding='0',cellspacing='0' bgcolor='#EAEAEA'>\n";
print "   <tr class='b'>\n";
print "      <th class='Blue_Bold'>$xml->{tdate}</th>\n";
print "      <th class='Blue_Bold'>$xml->{ttype}</th>\n";
print "      <th class='Blue_Bold'>$xml->{pamt}</th>\n";
print "   </tr>\n";

### Loop through reward transactions

my $trclass = '';
my $tdclass = '';
my $tot_redeemed = 0;
my $amount = 0;
my $currency = '';

while (my $tmp = $tdata->fetchrow_hashref) {

## Determine row color and background

    $amount = $tmp->{reward_points} * $tmp->{cur_xchg_rate} * -1;
    $tot_redeemed += $amount;
    $currency = $tmp->{currency};

    if ($amount < 0) {
	$tdclass = " class='neg'";
    }
    else {
	$tdclass = '';
    }
    if ($rowcount % 2 == 0) {
       $trclass="class='a'";
    }
    else {
       $trclass = "class='b'";
    }
    $rowcount ++;

#
# Put the html out 
#
    print "<tr $trclass>\n";
    print "<td $tdclass>$tmp->{date_reported}</td>\n";
    print "<td $tdclass>$tmp->{description}</td>\n";
    printf ("<td align='right' $tdclass>%.2f %s</td>\n",$amount,$tmp->{currency});
    print "</tr>\n";
}
print "<tr $trclass>\n";
print "<td $tdclass>&nbsp;</td>\n";
print "<td $tdclass>$xml->{total}</td>\n";
printf ("<td align='right' $tdclass>%.2f  %s</td>\n",$tot_redeemed,$currency);
print "</tr>\n";
print "</table>\n";
print "</td></tr></table>\n";
print "</body>\n";
print "</html>\n";

### Internal functions

sub Get_Cookie
{
    # If this is an admin user. 
    if ( $q->param('admin') )
    {
	# Get the admin user and try logging into the DB
	my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd
');
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	# Check for errors.
	unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
	{
	    $login_error = 1;
	}
	# Mark this as an admin user.
	$login_type = 'admin';
    }
    # If this is a member. 
    else
    {
	unless ($db = DB_Connect($cgiini)) {exit}
	$db->{RaiseError} = 1;

	# Get the member Login ID and info hash from the VIP cookie.
	( $member_id, $mem_info ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

	if ( $member_id && $member_id !~ m/\D/ )
	{
	    $login_type = 'auth';
	}
        else {
            print redirect("/cgi-bin/Login.cgi?destination=http://www.clubshop.com/cgi/clubcash_redemption_list.cgi");
            die;
        }
        if ($member_id =~ /^[0-9]+$/) {   # check to make sure member_id is numeric                                                                 
        }
        else {
            print redirect("/cgi-bin/Login.cgi?destination=http://www.clubshop.com/cgi/clubcash_redemption_list.cgi");
            die;
        }

    }
}

