#!/usr/bin/perl -w
######  msg_sub_sys.cgi
######  This is part of the messaging sub-system
###### originally written by: Stephen Martin
###### last modified 03/14/07	Bill MacArthur

use strict;
use CGI qw(:standard :html3);
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw( md5_hex );
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw( $LOGIN_URL );

$| = 1;

###### GLOBALS
our( $UID, $meminfo) = ();
my $sth = ();
our $db        = ();
our $q         = new CGI;
our $id        = our $firstname = our $lastname = '';

my $qry;
my $TMPL;
my $MEMBER;
my $html;
my $mc;
my $EXEC;
my $v;
my $user_id;
my $oid;
my $moid;
my $mid;
my $mtype;
my $action;
my $x;
my $mbox_cookie;

my %MSG = ( 'N' => 'There are no messages waiting', );

my @MESSAGE_STK;
my $MESSAGE_COUNT;
my @COOKIEJAR;
my @SEEN_SYS_MESSAGES;
my $MESSAGE;
my $IMG;
my $ONCLICK0;
my $ONCLICK1;
my $DELBOX;
my $msysm;

my $script = "msg_sub_sys.cgi";

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

###### this is the threshold below which we look for an upline guide

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
}

@SEEN_SYS_MESSAGES = split(/X/,($q->cookie('msysm') || ''));

$v = $param{v};

###### validate the user
###### we'll skip some stuff if we're ADMIN
unless ( $param{admin} )
{
    ( $UID, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if we decide to exclude access based on an ACCESS DENIED password, we'll do it here

###### if the UID has any non-digits it means that its bad/expired whatever
    if ( !$UID || $UID =~ /\D/ ) { RedirectLogin(); exit }

    if ( $meminfo->{membertype} eq 'p' ) {
	print $q->header(-expires=>'now'), $q->start_html( -bgcolor => '#ffffff' );
       print $q->h3('This utilityVIP members only'), '<br /><br />Server time: <i>';
	print scalar localtime() . '</i> (PST)' . $q->end_html();
	exit;
    }
    $firstname = $meminfo->{firstname};
    $lastname  = $meminfo->{lastname};
    $id        = $UID;

###### DB_Connect does its own error handling, so we only need exit if it doesn't work
    unless ( $db = DB_Connect( 'msg_sub_sys.cgi' ) ) { exit }
}

###### now we'll take care of the ADMIN mode
else {

###### here is where we get our admin user and try logging into the DB
    my $operator = $q->cookie('operator');
    my $pwd      = $q->cookie('pwd');
    require '/home/httpd/cgi-lib/ADMIN_DB.lib';
    unless ( $db = ADMIN_DB::DB_Connect( 'msg_sub_sys.cgi', $operator, $pwd ) )
	{
        exit;
	}

    unless ( $param{id} )
	{
		Err('Missing a required parameter');
		goto 'END';
	}
    my $qry = "	SELECT lpad(Id::TEXT,7,'0') as id,
				FirstName,
				LastName,
				membertype,
				password
			FROM members
			WHERE id= '$param{id}'";
			
    if ( ID_Ck($qry) == 0 ) { goto 'END' }
}
$db->{RaiseError} = 1;

###### At this point we are verified and can commence operations

###### Collect everything about the member logged in 

$MEMBER = get_info( $UID, "id" );

$action = ($param{action} || '');
$x      = $param{x};

###### Catch Mark Message as viewed requests and deletions #######

if ( $action eq " Close Message Center " )
{

    $user_id = $q->param('user_id');
    $oid     = $q->param('oid');
    $moid    = $q->param('moid');

    unless ( $MEMBER->{oid} eq $oid )
	{
        Err("OID mismatch for request!");
        goto 'END';
	}

    my @VP = $q->param('v');
    my @XP = $q->param('x');

    # Process view requests 1st 

    foreach (@VP)
	{
        enter_viewed_timestamp($_);
	}

    # Process deletion requests 

    foreach (@XP)
	{
        xm($_);
	}

    my $cline = flatten(\@COOKIEJAR);

    $mbox_cookie = cookie( -name  =>'msysm',
                           -value => $cline,
                           -path  => '/',
                           -expires => '+90d');

    print $q->header( -expires => 'now',
                      -cookies => $mbox_cookie );

    print qq~
 <html>
  <body>
   <script type="text/javascript">

    window.close();
   </script>
  </body>
 </html>
\n~;
    
	goto 'END';
}

###### print the default template.

$TMPL = G_S::Get_Object( $db, 10048 );
unless ($TMPL)
{
	Err("Couldn't retrieve the template object.");
	goto 'END';
}

$MESSAGE_COUNT = messages_waiting( $MEMBER->{id} );

if ($MESSAGE_COUNT)
{
    ######  print Number of messages

    $TMPL =~ s/%%SYS_MESSAGE%%/$MESSAGE_COUNT/g;
}
else
{
    ###### Print No Messages

    $TMPL =~ s/%%SYS_MESSAGE%%/$MSG{'N'}/g;

}

if ( $MESSAGE_COUNT == 1 )
{
    $TMPL =~ s/%%MPLURAL%%/message/g;
}
else
{
    $TMPL =~ s/%%MPLURAL%%/messages/g;
}

$TMPL =~ s/%%firstname%%/$MEMBER->{firstname}/g;

$mc = 1;

foreach my $message_hash (@MESSAGE_STK)
{

    $v = "";

    if ( $message_hash->{msg_viewed} ) {
        $v = " checked ";
    }

    if ( seen_sys_msg($message_hash->{oid}, \@SEEN_SYS_MESSAGES) ) {
       $v =  " checked ";
    }

    if (! $message_hash->{msg_id} ) {
# escaping the message breaks HTML messages
#        $MESSAGE = CGI::escapeHTML($message_hash->{msg});
		$MESSAGE = $message_hash->{msg};
	# convert newlines into HTML linebreaks
#        $MESSAGE =~ s#\cM|\n|(\r\n)#\<br \/>#g;
        $MESSAGE =~ s#\n|(\r\n)#\<br \/>#g;
        $EXEC = "_msg_sys_msg_viewed";
    }
    else {
        $MESSAGE = G_S::Get_Object( $db, $message_hash->{msg_id} );
        $EXEC = "_msg_sys_msg_viewed";
    }

    if ( $message_hash->{id} eq "0" ) {
        $DELBOX = "";
    }
    else {
        $DELBOX = <<D1;
     <input type="checkbox" name="x"  value="$message_hash->{oid}">
D1
    }

    $html .= <<EOH;
 <tr>

    <td valign="top">$mc</td>
	<td valign="top" align="center">
	<input type="checkbox" name="v"  value="$message_hash->{oid}" $v>&nbsp;</td>
	<td valign="top">
	Date: $message_hash->{msg_entered} PT
	<br>
<div style="border:3px double navy; padding:3px; background-color:rgb(240,240,255); width:600px; overflow:auto;">$MESSAGE</div>
</td>
	<td valign="top" align="center">$DELBOX&nbsp;</td>
</tr>
<tr><td colspan="4"><hr align="center"><br></td></tr>
EOH

    $mc++;
}

$TMPL =~ s/%%messages_tr%%/$html/g;
$TMPL =~ s/%%SELF%%/$script/g;
$TMPL =~ s/%%USER_ID%%/$MEMBER->{id}/g;
$TMPL =~ s/%%MEMBER_OID%%/$MEMBER->{oid}/g;

print $q->header( -expires => 'now', -charset=>'utf-8' );
print $TMPL;

END:
$db->disconnect;
exit;

sub Err
{
	print $q->header( -expires => 'now' ),
	$q->start_html( -bgcolor => '#ffffff' ), 'There has been a problem<br><br>';
	print $q->h4( $_[0] ), scalar localtime(), ' (PST)';
	print $q->end_html();
}

sub Hidden_Admin
{
###### generate hidden tags for use when in admin mode
###### otherwise substitute nothing
    if ( $param{'admin'} && $param{'admin'} eq '1' ) { return '&admin=1' }
    else { return '' }
}

sub ID_Ck
{
    my $membertype;    ###### a temporary variable just to test with
    my $sth = $db->prepare( $_[0] );
    $sth->execute;

###### id, firstname, & lastname are globals
    ( $id, $firstname, $lastname, $membertype ) = $sth->fetchrow_array;
    $sth->finish;
    unless ($membertype) {
        Err('No match on the ID and Password received');
        return 0;
    }
    elsif ( $membertype ne 'v' ) {
        Err('According to our records, you are not a VIP');
        return 0;
    }
    return 1;
}

sub RedirectLogin
{
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' );
    print
"Please login at <a href=\"https://www.clubshop.com/cgi-bin/Login.cgi\">https://www.clubshop.com/cgi-bin/Login.cgi</a><br /><br />";
    print scalar localtime(), "(PST)";
    print $q->end_html;
}

sub get_info
{
    my ($id) = @_;
    shift;
    my ($sw) = @_;

    unless ( $id && $sw ) {
        Err("You have entered get_info with the wrong \# of parameters!");
        return;
    }

    my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = "	SELECT oid,* 
		FROM members
		WHERE $sw = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data;
}

sub messages_waiting {
    my ($id) = @_;

    my ( $sth, $qry, $data, $rv );    ###### database variables

    ###### Pull in Global Messages from the templates table(cgi_objects)
    ######   and from the customer specific table(messages).

    $qry = "	SELECT messages.oid,
			messages.id,
			messages.msg_id,
			cgi_objects.obj_value AS msg,
			messages.msg_viewed,
			messages.msg_entered
		FROM
			cgi_objects,
			messages
		WHERE cgi_objects.obj_id = messages.msg_id
		AND messages.id = ?
		UNION ALL
		SELECT messages.oid,
			messages.id,
			messages.msg_id,
			messages.msg,
			messages.msg_viewed,
			messages.msg_entered
		FROM
			messages
		WHERE messages.id = ?
		AND messages.msg is NOT NULL
		AND COALESCE(messages.msg_id, NULL) ISNULL
-- not sure why Stephen did it that way... going to try ordering by unread and newest first...
-- way more convenient for viewing if one has leftover messages retained
-- 		ORDER BY 1
		ORDER BY 5 DESC, 6 DESC";

    $sth = $db->prepare($qry);

    $rv = $sth->execute( 0, 0 );

    while ( $data = $sth->fetchrow_hashref )
	{
        push @MESSAGE_STK, $data;
	}

    $rv = $sth->finish();

    ###### Pull in ID Specific Messages from the templates table
    ######   (cgi_objects) and from the member specific table(messages).

    $qry = "	SELECT messages.oid,
			messages.id,
			messages.msg_id as msg_id,
			cgi_objects.obj_value AS msg,
			messages.msg_viewed,
			messages.msg_entered 
		FROM 
			cgi_objects,
			messages
		WHERE cgi_objects.obj_id = messages.msg_id
		AND messages.id = ? 
		UNION ALL
		SELECT messages.oid,
			messages.id,
			messages.msg_id,
			messages.msg,
			messages.msg_viewed,
			messages.msg_entered
		FROM 
			messages 
		WHERE messages.id = ?
		AND messages.msg IS NOT NULL
		AND COALESCE(messages.msg_id, NULL) ISNULL
-- not sure why Stephen did it that way... going to try ordering by unread and newest first...
-- way more convenient for viewing if one has leftover messages retained
-- 		ORDER BY 1
		ORDER BY 5 DESC, 6 DESC";

    $sth = $db->prepare($qry);

    $rv = $sth->execute( $id, $id );

    while ( $data = $sth->fetchrow_hashref )
	{
        push @MESSAGE_STK, $data;
	}

    $rv = $sth->finish();

    return scalar(@MESSAGE_STK);

}

sub enter_viewed_timestamp
{
    my ($moid) = @_;

    my ( $sth, $qry, $data, $rv, $TREC );    ###### database variables

    $qry = "SELECT id from messages where messages.oid = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($moid);

    $TREC = $sth->fetchrow_hashref();

    $rv = $sth->finish();

    if ($TREC->{id} eq "0")
    {
     push @COOKIEJAR,$moid;
     return;
    }

    $qry = "	UPDATE messages 
		SET msg_viewed = now()
		WHERE oid = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($moid);

    $rv = $sth->finish();

}

sub xm
{
    my ($moid) = @_;

    my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = "DELETE FROM messages WHERE oid = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($moid);

    $rv = $sth->finish();
}

sub flatten
{

 my $arrayref = $_[0];
 my @array    = @$arrayref;

 my $c;

 foreach (@array)
 {
  $c = $c . $_ . "X";
 }

return $c;
 
}

sub seen_sys_msg
{
 my ( $oid )  = $_[0];
 my $arrayref = $_[1];
 my @array    = @$arrayref;
 
 my $seen = 0;

 foreach ( @array )
 {
  if ( $_ == $oid )
  {
   $seen = 1;
  }
 }

 return $seen;

}

###### 12/17/02 went to the 'use' method of using custom modules, also went to AuthCustom_Generic cookie
###### 02/26/03 changed to make a DB connection under the cgi 'umbrella'
###### 03/12/03 added the messages table to the first half of the UNION ALL 
######          SELECTs to alleviate webservers errors. Also inserted defaults
######          using the construct || '' for several uninitialized variables.
###### 06/10/03 cleaned up for easier reading and fixed another unitialized var error
###### 12/30/04 Removed the window size control Javascript from template #10048 only.
######	05/01/06 Added the date to the individual messages.
# 03/14/07 removed the HTML escaping on the msg text (it breaks HTML messages)
# 02/09/2011 g.baker postgres 9.0 port lpad(text, int, text) casts
