#!/usr/bin/perl -w
###### ma_discount_edit.cgi
######
###### A script for editing the discount(s) of a Merchant Affiliate.
######
###### Created:	12/09/03 Karl Kohrt
######
###### last modified: 

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw( %URLS );
use DB_Connect;

$| = 1;   # do not buffer error messages

###### CONSTANTS
# Set the minimum discount type that can access the discount editor.
# Bronze=1, Silver=2, Gold=3, Platinum=4, Special Exception=5
use constant MIN_DISCOUNT_TYPE => 1;

# Set the minimum discount value that can access the discount editor.
#  A % divided by 100. Example: 0.1 = 10%
use constant MIN_DISCOUNT_VALUE => 0.00;
							
###### GLOBALS
our $db = ();
our $q = new CGI;
our $ME = $q->script_name;
our @location_list = ();	# The location list from the cookie.
our $location_id = '';	# The location list from the cookie.
our $special_exception = 5;	# A special exception discount marker.
our $sth = '';
our $id = '';			# The merchant id used throughout the program.
our $membertype = '';	# The membertype used throughout the program.
our $discounts_report = '';	# The discount records to be displayed to the browser.
our %current_discounts = ();# The current discount records used for comparisons.
our $comp_start_date = '';	# The start date in postgres format used for comparisons.
our $Login_ID = '';		# The cookie login id.
our $ma_info = '';		# The cookie hash.
our $which_merchant = '';	# Identifies which location/master record for display.
our $messages = '';		# Messages that appear at the top, below the title.
our $action = '';
our $updated = 0;		# Flag that shows whether we updated or not.
our ($now_day, $now_mon, $now_year) = (localtime(time))[3,4,5];
our $today = sprintf "%04d-%02d-%02d", 	$now_year + 1900,
						$now_mon + 1,
						$now_day;
our %TMPL = (	'report'	=> 10242,
		'discount'	=> 10243,
		'location_row'=> 10244,
		'locations'	=> 10245);
our %TBL = (	'discounts'	=> 'merchant_affiliate_discounts',
		'locations'	=> 'merchant_affiliate_locations',
		'master'	=> 'merchant_affiliates_master',
		'types'	=> 'merchant_discounts',
		'vendors'	=> 'vendors');
our %DAYS_IN_MONTH = (	'01' => 31,
				'02' => 29,
				'03' => 31,
				'04' => 30,
				'05' => 31,
				'06' => 30,
				'07' => 31,
				'08' => 31,
				'09' => 30,
				'10' => 31,
				'11' => 30,
				'12' => 31);
###### %field has the form of 'label','field name', 'value', 'html class type'
###### 'required flag (set to 1 if required)'.
###### class would be used for the label.
###### keys should match DB column names.
our %field = (	discount_type	=> {value=>'', class=>'heading', req=>1},
			start_date	=> {value=>'', class=>'heading', req=>1},
			discount	=> {value=>'', req=>0},
			rebate		=> {value=>'', req=>0},
			reb_com	=> {value=>'', req=>0},
			pp		=> {value=>'', req=>0} );

# Is the Merchant logged in?
if ( Get_Cookie() )
{
	# Get any path info and/or parameters that may have been passed in.
	$action = $q->param('action') || '';

	($location_id = $q->path_info()) =~ s/^\///;
	$location_id = $location_id || 0;

	# If this is a master update, set the location ID to the cookie ID.
	if ( ($location_id eq 'master_id') && ($membertype eq 'maf') )
	{
		$location_id = $Login_ID;
	}

	# Master logins will get the typical location list to select from unless
	#	they only have one location available to setup for.
	# If there is no $location_id(first time through), display the location(s) page, depending on
	#  whether this is a master or location login.
	unless ( $location_id )
	{
		# If it is a location login	
		if ( $membertype eq 'mal' )
		{
			# Recall script with the single location id in the path.
			print $q->redirect( $ME . "/" . $ma_info->{id} );
		}
		# If it is a master login...
		elsif ( $membertype eq 'maf' )
		{
			# If there is more than one location.	
			if ( scalar(@location_list) > 1 )
			{
				# Present location list with option to send a master mailing.
				Report_Locations();
			}
			else
			{
				# Recall script with the single location id in the path.
				print $q->redirect( $ME . "/" . $location_list[0] );
			}
		}
		else
		{
			# The membertype is not recognizable.
			Err("Your cookie info is corrupted. Please <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> again.");
		}
	}
	# If a location_id with non-numeric characters was passed, then we have an error.							
	elsif ( $location_id =~ /\D/ )
	{
		Err("Undefined location ID. Try again.");
	}
	# If we have a numeric location ID passed.
	else
	{
		# Load up the passed parameters into our field variables.		
		Load_Params();

		# If the id passed is in the cookie's list or is the master.
		if ( (grep {/$location_id/} @location_list)
			|| ($location_id == $id) )
		{
			Query_Discount();	# Get the existing discount(s) for display.
			Build_Discount();	# Build out the discount page body.

			# If we're updating.
			if ( $action eq 'update' )
			{
				Check_Required();
				Check_Date();
				unless ( $messages )	{ Get_Discount_Values() }
				# If we have no error messages at this point.
				unless ( $messages )
				{
					# Unless we are doing a master update.				
					unless ( $location_id == $Login_ID )
					{
						# Make the list the single location that we are updating.								
						@location_list = $location_id;
					}
					# Set a flag to check if we moved the start date.
					my $moved_start_date = 0;
					# Look at all discounts on our list until we find a lesser discount.
					foreach ( keys %current_discounts )
					{
						unless ( $moved_start_date )
						{
							my $next_month = Next_Month();
							
							# If the submitted discount is less than the current discount
							# and the start_date is earlier than the first of next month..			
							if ( $field{discount}{value} < $current_discounts{$_}
								&& $comp_start_date lt $next_month )
							{
								# Set the start date to the first of next month.		
								$field{start_date}{value} = $next_month;
								$moved_start_date = 1;
								$messages .= "<span class=\"b\">The Start Date was adjusted to the 1st of next month because you are reducing your discount.</span><br>";
							}
						}
					}								
					foreach ( keys %current_discounts )
					{
						my $rv = Update_Discount($_);
						unless (! $rv || $rv ne '1' )
						{
							$messages .= "<span class=\"b\">The Discount for location # $_ was updated.</span><br>";
						}
						else
						{	
							$messages .= "<span class=\"r\">There was an error while attempting to update the discount for location # $_.</span><br>";
						}
					}
					$updated = 1;
					Query_Discount();	# Get the existing discount(s) for display.
					Build_Discount();	# Build out the discount page body.
				}
			}
			# If no action was passed - first time in the script.
			elsif ( ! $action )
			{
				$messages .= '';
			}			
			# Get the current discount info.
			Do_Page();
		}
	}		
}
$db->disconnect;
exit;


######################################
###### Begin subroutines here.
######################################

###### Build the discount for this merchant or location.
sub Build_Discount
{
	my $master_discount = 0;
	my $location_discount = 0;
	my $tmp = '';
	my $this_discount = '';
	my $buttons = '';
	my $discount_row = G_S::Get_Object($db, $TMPL{'discount'}) || die "Failed to load template: $TMPL{'discount'}";
	# Calculate today's date in yyyy-mm-dd format to compare to start_date.
	$discounts_report = '';
	
	while ($tmp = $sth->fetchrow_hashref)
	{
		# Unless this is a special exception discount plan.		
		unless ( $tmp->{discount_type} == $special_exception )
		{
			# Get a fresh template.	
			$this_discount = $discount_row;
			$this_discount =~ s/%%ME%%/$ME/g;
			foreach ( keys %{$tmp} )
			{
				# Non-breaking spaces for null or blank fields.
				unless ( defined $tmp->{$_} ) { $tmp->{$_} = '&nbsp;' }
				$tmp->{$_} ||= '&nbsp;';
				if ( $_ =~ m/rebate|reb_com|pp|discount/ )
				{
					# Store the current discount in a hash for further testing.
					if ( $tmp->{start_date} le $today
						&& $_ eq 'discount'
						&& ($tmp->{end_date} gt $today || $tmp->{end_date} eq 'None') )
					{
						$current_discounts{$tmp->{location_id}} = $tmp->{discount};
					}			
					# Multiply percentage fields by 100 for proper display.
					$tmp->{$_} *= 100;
				}
				# If there's no end date.			
				elsif ( $_ =~ m/end_date/ && $tmp->{$_} =~ m/nbsp/ )
				{
					$tmp->{$_} = 'None';	
				}
				$this_discount =~ s/%%$_%%/$tmp->{$_}/g;
			}
			$discounts_report .= $this_discount;
		}
	}
	$sth->finish;
	return 1;		
}

###### Checks to see if all required fields have been filled out.
sub Check_Required
{
	my $list = shift || [keys %field];
	my $prompts = '';
	my $missing = 0;

	###### are we required			
	foreach (@$list)
	{
		#Err("current param= $_ : $field{$_}{value}");

		if ($field{$_}{req} == 1 && ($field{$_}{value} eq '' || ! defined $field{$_}{value}))
		{
		#Err("MISSING - current param= $_ : $field{$_}{value}");
			$missing = 1;
			$field{$_}{class} = 'r';
		}
	}
	if ($missing == 1)
	{
		$messages .= qq|<span class="r">Please enter the required information marked in Red.</span><br>|;
		return 0;
	}
	return 1;
}

###### Check the date to see if it is of the right format and >= today.
sub Check_Date
{
	my $valid = 0;

	if ( length($field{start_date}{value}) == 10 )
	{
		# Change the date to the standard US format (mm-dd-yyyy) for comparison to input.
		my $comp_day = substr($field{start_date}{value}, 3, 2);
		my $comp_mon = substr($field{start_date}{value}, 0, 2);
		my $comp_year = substr($field{start_date}{value}, 6, 4);
		$comp_start_date = "$comp_year-$comp_mon-$comp_day";

		# A date of the format mm-dd-yyyy, day <= number of days in in the month, > today .
		if ( ( $field{start_date}{value} =~ /^\d{2}-\d{2}-200\d{1}$/ )
			&& ( $comp_day <= $DAYS_IN_MONTH{$comp_mon} )
			&& ( $comp_start_date gt $today ))
		{
			$valid = 1;
		}							
	}
	unless ( $valid )
	{
		$messages .= "<span class=\"r\">Start date should be of the format mm-dd-yyyy and greater than today.
				</span><br>";
		$field{start_date}{class} = 'r';
	}				
	return $valid;
}

###### Displays the basic page(header & footer) to the browser with the discount embedded in it.
sub Do_Page
{
	my $tmpl = G_S::Get_Object($db, $TMPL{'report'}) || die "Failed to load template: $TMPL{'report'}";
	
	foreach ( keys %field )
	{
		$tmpl =~ s/%%$_\_class%%/$field{$_}{class}/;
		$tmpl =~ s/%%$_%%/$field{$_}{value}/;
	}
	# If we processed an update, blank out the update form.
	if ( $updated )
	{
		$tmpl =~ s/conditional-update-open.+?conditional-update-close//sg;
	}

	$tmpl =~ s/%%checked$field{discount_type}{value}%%/checked/;
	$tmpl =~ s/%%which_merchant%%/$which_merchant/g;
	$tmpl =~ s/%%messages%%/$messages/;
	$tmpl =~ s/%%ME%%/$ME\/$location_id/;
	$tmpl =~ s/%%discounts_report%%/$discounts_report/;
	print $q->header(), $tmpl;
}

###### prints an error to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<br><B>$_[0]</B>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	my ($admin, $operator) = ();	# Defined locally because we won't use admin access.

	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		$admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		$operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for connection errors.
		unless ($db = ADMIN_DB::DB_Connect( 'ma_discount_edit.cgi', $operator, $pwd ))
		{
			return 0;
		}
		# Get the info passed in via parameters.
		unless ( ($id = $q->param('merchant_id')) && ($membertype = $q->param('membertype')) )
		{
			Err("Missing a required parameter.");
			return 0;
		}
	}
	# If this is a merchant.		 
	else
	{
		unless ($db = &DB_Connect::DB_Connect('ma_discount_edit.cgi'))
		{
			return 0;
		}
		# Get the Login ID and Merchant Affiliate info hash from the cookie.
		( $Login_ID, $ma_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_maf'), 'maf' );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$Login_ID || $Login_ID =~ /\D/)
		{
			Err("You must <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> to use this application.");
			return 0;
		}
		$id = $ma_info->{id};
		$membertype = $ma_info->{membertype};
		# Split out the separate location id's from the list.
		@location_list = split (/\,/, $ma_info->{maflocations});
	}										
	return 1;
}
				
###### Get the discount values associated with the submitted discount.
sub Get_Discount_Values
{
	my $qry = "	SELECT	discount,
				rebate,
				reb_com,
				pp																				
			FROM 	$TBL{types} t												
			WHERE 	type = ?";
	$sth = $db->prepare($qry);
	my $rv = $sth->execute($field{discount_type}{value});
	( 	$field{discount}{value},
		$field{rebate}{value},
		$field{reb_com}{value},
		$field{pp}{value} )	 = $sth->fetchrow_array();
	$sth->finish;

	unless ( $rv )
	{
		$messages .= "<span class='r'>Database error - failed to retrieve discount percentages.<br>
				</span>";
	}
	return 1;
}

###### Load the data from the form into the script variables.
sub Load_Params
{
	###### Populating our field array hash values.
	###### This loop depends on the %field hash keys being identical to the passed parameter names.
	foreach (keys %field)
	{
		###### since our usual param || '' will lose a value equal to zero
		###### we are going to this two part version
		$field{$_}{value} = $q->param($_);
		$field{$_}{value} = '' unless defined $field{$_}{value};

		next unless $field{$_}{value};

		###### Filter out leading & trailing whitespace.
		$field{$_}{value} =~ s/^\s*//;
		$field{$_}{value} =~ s/\s*$//;

		###### this is our default filtering										
		$field{$_}{value} =~ s/('?)( ?)(,?)(\.?)(-)(\W*)(_*)/$1$2$3$4$5/g;
	}
}

###### Get the first of next month.
sub Next_Month
{
	my $qry = "	SELECT	date_trunc('month', NOW() + INTERVAL '1 month' )::date;";
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute();
	my $next_month = $sth->fetchrow_array();
	$sth->finish;

	unless ( $rv )
	{
		Err("Database error - failed to retrieve the value next month.");
	}
	return $next_month;
}

###### Query for the existing discounts.
sub Query_Discount
{
	my $rv = '';
	my @id_list = ();

	# Select the appropriate fields.
	my $qry = "	SELECT	d.*,
				coalesce(end_date::text, '') AS end_date,
				v.blurb,
				t.name								
			FROM 	$TBL{discounts} d,
				$TBL{locations} l,
				$TBL{vendors} v,
				$TBL{types} t												
			WHERE 	d.location_id = l.id
			AND	d.discount_type = t.type
			AND	d.discount_type != $special_exception
			AND	v.status = 1
			AND 	l.vendor_id = v.vendor_id ";

	# Logged in with a master ID...
	if ( $location_id == $Login_ID && $membertype eq 'maf' )
	{
		$which_merchant = "Master ";
		$qry .= " AND l.merchant_id = ?";
		@id_list = ($Login_ID);
	}				
	# ...or a location ID
	else
	{
		$which_merchant = "Location ";
		$qry .= " AND l.id = ?";
		@id_list = ($location_id);
	}

	# Get the current discounts.
	$qry .= "\n	AND ( end_date >= NOW()::date
				OR end_date IS NULL )
			ORDER BY location_id, start_date;";

	$sth = $db->prepare($qry);
	$sth->execute(@id_list);
	$which_merchant .= "# $location_id";
	return 1;
}

###### Display a list of locations if we have more than one.
sub Report_Locations
{
	my $html = '';
	my $data = '';
	my $one_row = '';
	my $location_id = 0;
	my $report_body = '';
	my $created = 0;			

	# Prepare a query to get details about each location id.
	my $qry = "	SELECT mal.id,
				mal.location_name,
				mal.city,
				mal.address1,
				mal.state,
				mal.country,
				v.vendor_group,
				v.status,
				d.discount_type
			FROM $TBL{locations} mal
			LEFT JOIN vendors v
			ON 	mal.vendor_id = v.vendor_id
			LEFT JOIN $TBL{discounts} d
			ON 	mal.id = d.location_id
			WHERE 	mal.id = ?
			AND	d.start_date <= NOW()::date
			AND 	( d.end_date >= NOW()::date
				OR d.end_date IS NULL )";
	my $sth = $db->prepare($qry);

	my $location_row = G_S::Get_Object($db, $TMPL{location_row}) || die "Couldn't open $TMPL{location_row}";
	# Display a line for each location in the list.
	foreach $location_id (@location_list)
	{				
 		# Get the detailed info for the location listing.
		$sth->execute($location_id);
		$data = $sth->fetchrow_hashref();
		next unless $data;
		
		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		###### instead of excluding inactive locations and MarketPlace locations
		###### we'll signify their status.....
		unless (defined $data->{status})
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}
		elsif ($data->{status} eq '0')
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}
		elsif ($data->{discount_type} == $special_exception )
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}
		elsif ($data->{status} eq '1')
		{
			$one_row =~ s#%%id%%#$q->a({-href	=> "$ME/$data->{id}",
							-target=> "_blank" },
							$data->{id} )#e;
		}
		else
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}

		$one_row =~ s/%%location_name%%/$data->{location_name}/;
		$data->{address1} = $data->{address1} || '&nbsp;';
		$data->{city} = $data->{city} || '&nbsp;';
		$data->{state} = $data->{state} || '&nbsp;';
		$data->{country} = $data->{country} || '&nbsp;';
		$one_row =~ s/%%address1%%/$data->{address1}/;
		$one_row =~ s/%%city%%/$data->{city}/;
		$one_row =~ s/%%state%%/$data->{state}/;
		$one_row =~ s/%%country%%/$data->{country}/;

		unless (defined $data->{status})
		{
			$one_row =~ s/%%status%%/Location Not Activated/;
		}
		elsif ($data->{status} eq '0')
		{
			$one_row =~ s/%%status%%/Location Inactive/;
		}
		elsif ($data->{discount_type} == $special_exception )
		{
			$one_row =~ s/%%status%%/Active - Special Exception*/;
		}
		elsif ($data->{status} eq '1')
		{
			$one_row =~ s/%%status%%/Active/;
		}
		else
		{
			$one_row =~ s/%%status%%/Incorrect Location Status/;
		}

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}

	# If this is a master login, include the master record.
	if ( $membertype eq 'maf' )
	{
		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		$one_row =~ s/%%id%%/$q->a( {	-href	=> $ME . "\/master_id",
							target=> "_blank" },
							"Master ID" )/e;
		$one_row =~ s/%%location_name%%/All Locations/;
		$one_row =~ s/%%address1%%/&nbsp;/;
		$one_row =~ s/%%city%%/&nbsp;/;
		$one_row =~ s/%%state%%/&nbsp;/;
		$one_row =~ s/%%country%%/&nbsp;/;
		$one_row =~ s/%%status%%/Active/;

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}	
	$sth->finish();

	# Get the body template and make the substitutions.	
	my $tmpl = G_S::Get_Object($db, $TMPL{locations}) || die "Couldn't open $TMPL{locations}";
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	print $q->header(-expires=>'now'), $tmpl;
}

###### Update the discount record if changes were made.
sub Update_Discount
{
	my $this_location = shift;
	my $qry = '';
	my $sth = '';
	my $rv = '';	
	my $qry_args = '';
	my (@list, $tmp, $fld) = ();
	my $first_one = 1;
	my $how_many = 0;
	my $data = '';	

	# Let's see how many current/future records we have.
	$qry .= "	SELECT count(*)
			FROM 	$TBL{discounts}
			WHERE 	location_id = ?
			AND 	(end_date >= NOW()::date
				OR end_date IS NULL)";		
	$sth = $db->prepare($qry);
	$rv = $sth->execute($this_location);
	$how_many = $sth->fetchrow_array();
	$sth->finish;

	# If there is only a current record, we'll insert a future discount record.
	if ( $how_many == 1 )
	{
		# First update the end date on the current discount record.
		$qry = "	UPDATE $TBL{discounts}
				SET 	end_date = ?::timestamp - INTERVAL '1 day',
					stamp = NOW()
				WHERE 	location_id = ?
				AND	end_date IS NULL;";
		$sth = $db->prepare($qry);
#Err("start_date= $field{'start_date'}{value}, this_location= $this_location<br>$qry");
		$rv = $sth->execute($field{'start_date'}{value}, $this_location);
		$sth->finish;

		# Let's create and run the insert query for the new discount record.
		$qry = "	INSERT INTO $TBL{discounts} ( ";
		foreach $fld (keys %field)
		{
			###### if this value is flagged as nodb it doesn't belong in the DB
			next if $field{$fld}{nodb};

			$qry .= " $fld, ";

			###### we'll insert numeric values directly so that we can convert to NULL as necessary
			if ($field{$fld}{numeric})
			{
				if ( $field{$fld}{value} eq '' )
				{
					$qry_args .= " NULL,";
				}
				# we need to convert our percentages into decimal equivalents
				elsif ( $fld =~ /discount|rebate|reb_com|pp/ )
				{
					$qry_args .= " $field{$fld}{value}/100,";
				}
				else
				{			
					$qry_args .= " $field{$fld}{value},";
				}
			}
			else
			{
				$qry_args .= " ?,";
				push (@list, $field{$fld}{value});
			}
		}
		$qry .= "	location_id,
				stamp";
		$qry_args .= " ?, ?)";
		push (@list, $this_location, 'NOW()');
		$qry .= " ) VALUES (";
		$qry .= $qry_args;
		$sth = $db->prepare($qry);
#Err("$qry"); foreach ( @list ) { Err($_); }
		$rv = $sth->execute(@list);
		$sth->finish;
		return $rv;
	}
	# If there has been a change and we have a future discount record, we'll update it.
	elsif ( $how_many == 2 )
	{
		# Set the end date on the current discount record.
		$qry = "	UPDATE $TBL{discounts}
				SET 	end_date = ?::timestamp - INTERVAL '1 day',
					stamp = NOW()
				WHERE 	location_id = ?
				AND	end_date >= NOW()::date;";
		$sth = $db->prepare($qry);
#Err("$qry<br>this_location= $this_location, start_date= $field{'start_date'}{value}");
		$rv = $sth->execute($field{'start_date'}{value}, $this_location);
		$sth->finish;
		unless ( $rv ) { return 0 } 

		$qry = "	UPDATE $TBL{discounts} SET";
		foreach $fld (keys %field)
		{
			###### if this value is flagged as nodb it doesn't belong in the DB
			next if $field{$fld}{nodb};

			###### we'll insert numeric values directly so that we can convert to NULL as necessary
			if ($field{$fld}{numeric})
			{
				if ( $field{$fld}{value} eq '' )
				{
					$field{$fld}{value} = 'NULL';
				}
				# we need to convert our percentages into decimal equivalents
				elsif ( $fld =~ /discount|rebate|reb_com|pp/ )
				{
					$field{$fld}{value} = $field{$fld}{value}/100;
				}
				$qry .= " $fld= $field{$fld}{value},";
			}
			else
			{
				$qry .= " $fld= ?,";
				push (@list, $field{$fld}{value});
			}
		}
		$qry .= "	stamp= NOW()
				WHERE 	location_id = ?
				AND 	start_date > NOW()::date
				AND	end_date IS NULL";

		push (@list);
		$sth = $db->prepare($qry);
#Err("$qry<br>this_location= $this_location");foreach ( @list ) { Err($_); }
		$rv = $sth->execute(@list, $this_location);
		$sth->finish;
		return $rv;
	}
	else
	{
		Err("More than two current/future discount records found for this merchant. Call IT.");
	}			
}












