#!/usr/bin/perl

=head1 NAME:
cancel.cgi

	The page that displays a merchants cancel options.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	This script detects whether a merchant is logged in with their 
	Master profile, or a location profile.  If they are logged in with
	the location profile they are redirected to a "you are not allowed to be here" page.
	A merchant logged-in with their Master Proile is presented with the options
	for canceling their account.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict
	warnings
	FindBin
	lib
	DHSGlobals
	CGI
	DB
	DB_Connect
	G_S
	MerchantAffiliates
	DHS::UrlSafe
	XML::local_utils
	XML::Simple
	MailTools

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use MerchantAffiliates;
use DHS::UrlSafe;
use XML::local_utils;
use XML::Simple;
use MailTools;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	$db
	$CGI
	$DHS_UrlSafe
	$G_S
	$MerchantAffiliates

=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $G_S = G_S->new();
my $MerchantAffiliates = {};


=head2
VARIABLES, HASHES, and ARRAYS:

	$xml
	STRING
	The XML file from the translation system
	
	$xsl
	STRING
	The pages Style Sheet
	
	$params
	HASHREF
	The values from the submitted data, if it is returned to this display
	
	$params_xml
	STRING
	The "params" data XMLized.
	
	$messages
	HASHREF
	The responses from the submit program.
	
	$messages_xml
	STRING
	The "messages" data XMLized.
	
	$submit_url
	STRING
	The url of the Submit program.

=cut

my $xml = '';
my $xsl = '';

my $params = {};
my $params_xml = '';

my $messages = {};
my $messages_xml = '';

my $submit_url = $CGI->url();
$submit_url =~ s/\.cgi/_submit\.cgi/;
	
=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.

=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayUnavailable

=head3
Description:

	Redirects the user to a page telling them they do not have permissions
	to view this screen, or this screen is unavailable.

=head3
Params:

	none

=head3
Returns:

	none

=cut
sub displayUnavailable
{
	
	print $CGI->redirect(-uri=>'/maf/payment/unavailable');
	
	exitScript();
	
}

####################################################
# Main
####################################################
eval{

	unless ($db = DB_Connect('merchant_affiliates')){exit}
	$db->{RaiseError} = 1;
	
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	displayUnavailable() if $cookie->{ma_info}->{membertype} eq 'mal';
	
	
	
	if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100823)
	{
		$xml = G_S::Get_Object($db, 10817);
		$xsl = G_S::Get_Object($db, 10818);
	}
	else
	{
		$xml = G_S::Get_Object($db, 10811);
		$xsl = G_S::Get_Object($db, 10812);
	}
	
	$messages = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data')) if $CGI->param('data');
	
	if(exists $messages->{params})
	{
		$params = $messages->{params};
		delete $messages->{params};
	}
	
	$params->{action} = $submit_url;
	
	
	$messages_xml = XML::Simple::XMLout($messages, RootName=>"data");
	$params_xml = XML::Simple::XMLout($params, RootName=>"params", NoAttr=>'1');
	
	
	my	$output_xml = <<EOT;
		<base>
			
			$messages_xml
		
			$params_xml
			
			$xml
			
		</base>
		
EOT



		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $output_xml);
		
#		print $CGI->header('text/plain');
#		print $output_xml;
	
	
};
if($@)
{
	
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}



	
	

exitScript();

__END__

=head1
ASSOCIATED SCRIPTS

L<maf::cancel_submit>

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


