#!/usr/bin/perl

# cgi_object_functions ID: 379

=head1 NAME:
update_payment2.cgi

	This script is used to update the *ONLINE* Merchants Payment Information, and Merchant Package.  
	It allows them to update CC info, and upgrade, or downgrade their Merchant Package.
	This started as a direct copy of update_payment.cgi

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use CGI;
use CGI::Carp 'fatalsToBrowser';
use Template;
use XML::LibXML::Simple;
use CGI::Cookie;
use Business::CreditCard;
use Data::Dumper;
use lib '/home/httpd/cgi-lib';
use DB;
use DB_Connect;
require G_S;
use DHS::CreditCard;
require ccauth;
require MerchantAffiliates;
use MailTools;
use DHSGlobals;
use Notifications;
use Mall;

my %TMPL = (
	'xml' => 10680,
	'TT' => 10969
);
our $CGI = CGI->new();
my $url = $CGI->self_url();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $languages_ref = {};
our $countries_xml = '';

####################################################
# Main
####################################################

exit unless my $db = DB_Connect('merchant_update_payment');
$db->{'RaiseError'} = 1;

my $G_S = G_S->new({'db'=>$db, 'CGI'=>$CGI});
my $MerchantAffiliates = MerchantAffiliates->new($db);

my $xml = $G_S->GetObject($TMPL{'xml'}) || die "Failed to load XML template: $TMPL{'xml'}";
my $template = $G_S->GetObject($TMPL{'TT'}) || die "Failed to load template: $TMPL{'TT'}";

if ($url =~ /admin/)
{
	$template =~ s/%%bodycolor%%/eeffee/g;
}
else
{
	$template =~ s/%%bodycolor%%/ffffff/g;
}

my $configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();	# as of Oct. 2012 this is template 10681
	
if(! $CGI->param('updated') && $CGI->param && ! $CGI->param('edit'))
{
	processApplication();
}
else
{
	displayApplication();
}

exitScript();

###### start of sub routines ######

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub displayApplication
{
	my $merchant_master_record_hashref = shift;
	my $errors = shift;

#	$countries_xml = $G_S->GetObject($TMPL{'countries'}) || die "Failed to load COUNTRIES template: $TMPL{'countries'}";

	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	RedirectOfflineMerchant($cookie) if $cookie->{'ma_info'}->{'id'};
	
#	my $errors_xml = '';
	my $notices_xml = '';
	
#
# $configuration_xml is the 10681 cgi containing all data necessary for 
#warn "app config:\n$configuration_xml";
#
	my $application_configuration = XMLin("<base>$configuration_xml</base>");

	my $information_hashref_arrayref = {};

#	
# If there was no information passed as a parameter into this sub for "merchant_master_record_hashref"
# retrieve the merchant master record, and member information.
#
	if (! $merchant_master_record_hashref)
	{
	
		eval{

			if ($CGI->param('edit') && $CGI->param('id'))
			{
			
			    $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($CGI->param('id'), {'with_pending_payment_information'=>1});
			}
			else
			{
			    displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
			
			    $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'},{'with_pending_payment_information'=>1});
			}
			
#
# Load the data necessary for the discounts
#
	# as of 10/12 there should be only subtype 2 for % off for online merchants... but we will leave this stuff in here for now
			$information_hashref_arrayref->{'discount_subtype'} = $merchant_master_record_hashref->{'discount_subtype'};
			
			if ($merchant_master_record_hashref->{'discount_subtype'} == 1)
			{
				$information_hashref_arrayref->{'flat_fee_off'} = [$merchant_master_record_hashref->{'special'}];
			}
			elsif ($merchant_master_record_hashref->{'discount_subtype'} == 2)
			{
			    if ($merchant_master_record_hashref->{'discount'} > 0.0)
			    {
			        $information_hashref_arrayref->{'percentage_off'} = [$merchant_master_record_hashref->{'special'}];
			    }
			    else
			    {
			        $information_hashref_arrayref->{'percentage_off'} = [$merchant_master_record_hashref->{'special'}];
			    }
			}
			else
			{
			    $information_hashref_arrayref->{'bogo_amt'} = [$merchant_master_record_hashref->{'special'}];
			}	
		};
		
		ErrorHandler($@, "A fatal error occured, contact Technical Support support\@dhs-club.com", 'displayApplication()') if $@;
	}
	else
	{
		# this is the one line leftover of all the cruft that was in here... whether a location login could ever submit any info I do not know
		displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
	}
	
	#This is here due to a conflict in the data, and the way we use XML	
#	$merchant_master_record_hashref->{'pay_payment_method'} = lc($merchant_master_record_hashref->{'pay_payment_method'}) if defined $merchant_master_record_hashref->{'pay_payment_method'};

	my $exchange_rates = $db->selectrow_hashref("
		SELECT ern.code, ern.rate
		FROM currency_by_country cbc
		JOIN exchange_rates_now ern
			ON cbc.currency_code = ern.code
		WHERE cbc.country_code = ?", undef, $merchant_master_record_hashref->{'country'});
	
	my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
	
	my $country_code_lower_case = lc($merchant_master_record_hashref->{'country'});
	
	if ($merchant_master_record_hashref->{'pay_cardnumber'} && $merchant_master_record_hashref->{'pay_cardnumber'} !~ /x/gi)
	{
		$merchant_master_record_hashref->{'pay_cardnumber'} =~ s/\s//g;
		$merchant_master_record_hashref->{'pay_cardnumber'} =~ s/^\d{12}/XXXXXXXXXXXX/;
	}
	
	$merchant_master_record_hashref->{'pay_cardcode'} = '' if ($merchant_master_record_hashref->{'pay_cardcode'});
	
	#If it is a regular price country convert the regular pricing
	
	$merchant_master_record_hashref->{'merchant_type'} = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record_hashref->{'discount_type'});
			
			
	#If it is a regular price country convert the regular pricing
#	if(defined $country_code_lower_case && exists $merchant_master_record_hashref->{$country_code_lower_case} && exists $pricing->{'regular_price_countries'}->{'offline'}->{$merchant_master_record_hashref->{$country_code_lower_case}})
#	{
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'free'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'bronze'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'silver'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'gold'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#	}
#	else
#	{
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'free'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'bronze'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'silver'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'gold'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#	}
#			
#	if(defined $country_code_lower_case && exists $merchant_master_record_hashref->{$country_code_lower_case} && exists $pricing->{'regular_price_countries'}->{'offline_rewards'}->{$merchant_master_record_hashref->{$country_code_lower_case}})
#	{
#				
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'free'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'basic'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'bronze'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'silver'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'gold'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'platinum'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#	}
#	else
#	{		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'free'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'basic'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#								
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'bronze'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'silver'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'gold'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#		
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'platinum'} * $exchange_rates->{'rate'};
##		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;	
#	}
#
#	$pricing->{'prices'}->{'converted'}->{'offline'}->{'monthly'} = sprintf "%.2f $exchange_rates->{'code'}", ($pricing->{'prices'}->{'offline'}->{'monthly'} || 0) * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline'}->{'monthly'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'free'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#	
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'basic'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#			
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'bronze'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#			
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'silver'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#			
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'gold'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#	
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'platinum'} * $exchange_rates->{'rate'};
##	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#
#	$configuration_xml .= XML::Simple::XMLout($pricing->{'prices'}->{'converted'}, 'RootName' => "converted", 'NoAttr' => 1);
			
	#Make the information XML friendly
	foreach (keys %$merchant_master_record_hashref)
	{
		next if ! defined $merchant_master_record_hashref->{$_};
			
		$information_hashref_arrayref->{$_} = [$merchant_master_record_hashref->{$_}];
	}
	
	if (defined $information_hashref_arrayref->{'pay_payment_method'} && ref($information_hashref_arrayref->{'pay_payment_method'}) ne 'ARRAY')
	{
		$information_hashref_arrayref->{'payment_method'} = $information_hashref_arrayref->{'pay_payment_method'} = [ lc($information_hashref_arrayref->{'pay_payment_method'}->[0]) ];
	}

	#Get the minimum balace a merchant needs to use the Funds Transfer, or the Club Account.
	#
	$information_hashref_arrayref->{'package_cost'} = [$pricing->{'prices'}->{$merchant_master_record_hashref->{'merchant_type'}}->{'seed'}];
	
	$information_hashref_arrayref->{'package_cost'}->[0] += $MerchantAffiliates->retrieveMerchantPackageCost($merchant_master_record_hashref->{'merchant_type'}, $merchant_master_record_hashref->{'mpp_discount_type'},$merchant_master_record_hashref->{'country'}) if (defined $merchant_master_record_hashref->{'mpp_discount_type'} && $merchant_master_record_hashref->{'mpp_discount_type'} > $merchant_master_record_hashref->{'discount_type'});
	
	$information_hashref_arrayref->{'package_cost'}->[0] = sprintf("%.2f", $information_hashref_arrayref->{'package_cost'}->[0] || 0);
	
	
	if ($CGI->param('updated'))
	{
		$notices_xml = XML::Simple::XMLout({'updated'=>['updated']}, 'RootName' => "notices" );
	}
		
#	$output_xml = <<EOT;
#		<base>
#			
#			<menus>
#				$countries_xml
#				
#				$configuration_xml
#				
#				$cc_expiration_dates_xml
#				
#			</menus>
#			
#			<data>
#				$errors_xml
#				$notices_xml
#				
#			</data>
#		
#			$params
#			
#			$xml
#			
#		</base>
#		
#EOT
	my $TT = Template->new();
	my $Mall = Mall->new({'db'=>$db, 'CGI'=>$CGI});
	my $CC = DHS::CreditCard->new({'CGI'=>$CGI});
	my $lang_pref = $G_S->Get_LangPref();
	my $lang_blocks = XMLin($xml, 'NoAttr'=>1);
warn Dumper($errors) if $errors;
#die Dumper($merchant_master_record_hashref);
	print $CGI->header(-charset=>'utf-8');
	$TT->process(\$template, {
		'lang_blocks' => $lang_blocks,
		'errors' => $errors,
		'params' => $information_hashref_arrayref,
		'merchant_master_record_hashref' => $merchant_master_record_hashref,
		'selectCtlPrimaryMallCats' => $Mall->selectCtlPrimaryMallCats($lang_pref, {
			'-name'=>"main_mall_category",
			'-class'=>"registration_dropmenu",
			'-id'=>"main_mall_category",
			'-default'=>$merchant_master_record_hashref->{'online_mall_category'}
		}),
		'cboCardExpiry' => $CC->cboCardExpiry({
			'-name'=>'pay_cardexp',
			'-default'=>$merchant_master_record_hashref->{'pay_cardexp'},
			'-id'=>"pay_cardexp",
			'-class'=>"registration_dropmenu"
		}),
		'cboPayCountry' => $G_S->cboCountry({
			'-name'=>"pay_country",
			'-id'=>"pay_country",
			'-onchange'=>"getStates()",
			'-class'=>"registration_dropmenu",
			'-default'=>$merchant_master_record_hashref->{'pay_country'}
		})
	});

	ErrorHandler($TT->error, 'Error processing template', 'displayApplication()') if $TT->error;
	exitScript();
}

sub displayUnavailable
{
	print $CGI->redirect('-uri'=>'/maf/payment/unavailable');
	exitScript();
}

sub ErrorHandler
{
	my ($error, $message, $subjectTag) = @_;
	
	print $CGI->header('text/plain');
	print "$message\n\n$message\n\nError: $error";
		
	my $MailTools = MailTools->new();
		
	$MailTools->sendTextEmail({
		'from'=>'errors@dhs-club.com',
		'to'=>'errors@dhs-club.com',
		'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: $subjectTag",
		'text'=>$message
	});
}

=comment

	This routine used to be cluttered with all kinds of stuff from the original applications. However, I (Bill) have ripped out most of it.
	The logic behind this thing now is this:
	If they submit a package upgrade request, we need all the card information, including the card code
	If they submit a package downgrade request, we don't need the card card.
	If they change any of the CC information beside the card code, we need to evaluate the card afresh.
	If they do not change any of the CC information *and* they are not doing an upgrade we do not need to much with it whatsoever.
	
=cut

sub processApplication
{
	
	my %merchant_information = ();
	$merchant_information{'merchant_type'} = 'online';	# manually set this as this *is* an interface for online merchants
	
	my %missing_fields = ();
	
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} ); #{};
	
	RedirectOfflineMerchant($cookie) if $cookie->{'ma_info'}->{'id'};
	
	my $update_the_master_record = 1;
	
	my @required_fields = qw/discount_type pay_cardnumber pay_cardexp pay_address pay_city pay_country/;
	my @recognized_fields = (@required_fields, qw/pay_cardcode pay_postalcode pay_state/);
	
	my $merchant_original_record = {};
	
	eval{
		displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
			
		$merchant_original_record = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'});
	};
	
	ErrorHandler($@, 'An error instanciating a class, or retrieving the Merchant Master Record.', 'processApplication()') if $@;

	# Assign the information the merchant submitted to the "$merchant_information" hash.
	# we are going to define exactly what we expect instead of just pulling in everything that was sent.... what if somebody is trying to exploit
	# not to mention that we can look at this list and know exactly what we have to work with.... important for the next poor sucker that has to work in here
	foreach my $key (@recognized_fields)
	{	
		$merchant_information{$key} = $CGI->param($key)
	}

	#Check for missing fields.
	foreach (@required_fields)
	{
		$missing_fields{$_} = ['Missing Field'] if(! defined $merchant_information{$_} || $merchant_information{$_} =~ /^\s*$/);
	}

	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	displayApplication(\%merchant_information, \%missing_fields) if (scalar (keys %missing_fields));
	
	# validate the number if they are submitting a new one... if they are *not* submitting a new one is will come across with the XXX.. obfuscation
	if ($merchant_information{'pay_cardnumber'} !~ /X/)
	{
		$missing_fields{'pay_cardnumber'} = ['Card number does not appear valid'] if ! Business::CreditCard::validate($merchant_information{'pay_cardnumber'});
	}

	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	displayApplication(\%merchant_information, \%missing_fields) if (scalar (keys %missing_fields));
die 'pkg upgrade value: '. $MerchantAffiliates->retrieveMerchantPackageCost('online', $merchant_information{'discount_type'},$merchant_original_record->{'country'}) . ' other: '. $MerchantAffiliates->retrieveMerchantPackageCost('online',22,$merchant_original_record->{'country'});	
	if ($merchant_information{'discount_type'} != $merchant_original_record->{'discount_type'})
	{
		
	}
			
#	my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
			
	#Charge the card the appropriate amount.

		#The postalcode is not a required field, but if they leave it blank we need to clear it out
		$merchant_information{'pay_postalcode'} = '' if ! defined $merchant_information{'pay_postalcode'};
		
		#For Charging the Credit Card, and Verification the card is valid. 
		
		my %cc_parameters = ();
		
		my $original_credit_card_number = $merchant_original_record->{'pay_cardnumber'} || '';
		
		
		if ($original_credit_card_number !~ /x/gi)
		{
			$original_credit_card_number =~ s/\s//g;
			$original_credit_card_number =~ s/^\d{12}/XXXXXXXXXXXX/;
		}
			
		
		$cc_parameters{'cc_number'} = ($merchant_information{'pay_cardnumber'} eq $original_credit_card_number)? $merchant_original_record->{'pay_cardnumber'}: $merchant_information{'pay_cardnumber'};			
		$cc_parameters{'card_code'} = $merchant_information{'pay_cardcode'};
		$cc_parameters{'cc_expiration'} = $merchant_information{'pay_cardexp'};
		$cc_parameters{'cc_expiration'} =~ s/^d//;
		
		$cc_parameters{'account_holder_lastname'} = $merchant_information{'pay_name'};
		$cc_parameters{'account_holder_address'} = $merchant_information{'pay_address'};
		$cc_parameters{'account_holder_city'} = $merchant_information{'pay_city'};
		$cc_parameters{'account_holder_state'} = (defined $merchant_information{'pay_state'} && $merchant_information{'pay_state'}) ? $merchant_information{'pay_state'} : $merchant_information{'cc_state'};
		$cc_parameters{'account_holder_zip'} = $merchant_information{'pay_postalcode'} if $merchant_information{'pay_postalcode'};
		$cc_parameters{'account_holder_country'} = $merchant_information{'pay_country'};
		
		$cc_parameters{'payment_method'} = 'CC';
		$cc_parameters{'invoice_number'} = 'MAU' . time;
		$cc_parameters{'id'} = 'MAU';
		
		my $returned_auth_info = '';
		
		if($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) > $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}) )
		{
			
			$cc_parameters{'amount'} = $MerchantAffiliates->retrieveMerchantPackageCost($merchant_information{'merchant_type'}, $merchant_information{'discount_type'}, $merchant_original_record->{'country'});
			
			$cc_parameters{'order_description'} = DHSGlobals::PRODUCTION_SERVER ? 'Merchant Package Update Credit Card Charge' : 'Merchant Package Update Credit Card Charge (Dev)';

			my $authorize_extra_parameters =  {};
			
			my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			
			my $operator = $CGI->cookie('operator') ? $CGI->cookie('operator') : "MA_User_$cookie->{'ma_info'}->{username}";
			
			#If the merchant is changing their payment type, and the merchant package is not being upgraded just run an authorize to confirm the credit card is in good standing.
			my $auth_only = 0;
			if($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) <= $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}) && $merchant_original_record->{'pay_payment_method'} ne 'CC')
			{
				
				$authorize_extra_parameters = {'auth_type'=>'AUTH_ONLY'};
				$cc_parameters{'amount'} = 1.5;
				$auth_only = 1;
			}
			
			#Just to be safe, if this is not the production server set auth_type to 'AUTH_ONLY' so we don't charge the card.
			$authorize_extra_parameters = {'auth_type'=>'AUTH_ONLY'} if ! DHSGlobals::PRODUCTION_SERVER;

			$returned_auth_info = CCAuth::Authorize(\%cc_parameters, $authorize_extra_parameters);
			
			if ($returned_auth_info ne '1')
			{
				
				my $error_code = $returned_auth_info;
				
				$error_code =~ s/^.*\s(\d+):.*/$1/gi;
				
				$error_code =~ s/\s*//gi;
				
				$returned_auth_info =~ s/^\d\s*|\d+:\s*|\<\/*b\>\s*|\<br\s\/\>//gi;

				$missing_fields{"cc_error_$error_code"} = [$returned_auth_info];
				$missing_fields{'cc_error'} = [$returned_auth_info];
				
				displayApplication(\%merchant_information, \%missing_fields);
				exitScript();
				
			}
			else
			{
				#Well credit their Club account here, if we do it in MerchantAffiliates::updateMerchantMasterInformation method it will happen in a transaction, 
				#and that is not the best approach for all situations.
				#Say the merchant is -0.50 in their Club Account, and we credit their account in the MerchantAffiliates::updateMerchantMasterInformation method
				#The trasaction will be rolled back if the subscription throws an error.
				my $description = "Credit Card Funding for a Merchant Package Upgrade";
			    if ($auth_only == 1)
			    {
					$cc_parameters{'amount'} = 0;
					$description = "Credit Card Authorization Check";
			    }
			    
				my @debit_club_account_data = (
					$merchant_original_record->{'member_id'}, 
					7000, 
					$cc_parameters{'amount'}, 
					$description,
					$operator
				);
			
				
				eval{
					
					my $transaction_id = $db->selectrow_array('SELECT debit_trans_complete(?,?,?,?,NULL,?)', undef,	@debit_club_account_data);
					
					die "There was an error crediting the members Club Account, after charging their credit card. transaction_id: $transaction_id  -  debit_club_account_data: " . Dumper(@debit_club_account_data) if (! $transaction_id || $transaction_id eq '0E0');
				};
				if($@)
				{
					my $error = $@;
					
					#warn $FindBin::RealScript . ' : ' . $error;
					
					$missing_fields{'script_error'} = ['Script Error'];
					
					my $MailTools = MailTools->new();
					
					my $message = "Error Crediting A Merchant Club Account : $error \n\nMerchant ID: $cookie->{'ma_info'}->{'id'}\n\n\%merchant_information: " . Dumper(%merchant_information);
					$MailTools->sendTextEmail({
						'from'=>'errors@dhs-club.com',
						'to'=>'errors@dhs-club.com',
						'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script",
						'text'=>$message
					});

					displayApplication(\%merchant_information, \%missing_fields);
					exitScript();
				}
				
			}
			
		}

		#No need to update their information that is done later.
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{
		$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}
	
warn 'update_the_master_record: ' . $update_the_master_record;
	
	if($update_the_master_record)
	{
		eval{
			
			my $subscription = {};
			
			#warn "merchant_information{'discount_type'}: $merchant_information{'discount_type'} > merchant_original_record->{'discount_type'}: $merchant_original_record->{'discount_type'}";
			
			if ($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) > $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
			{
				$subscription->{'status'} = 'UPGRADE';
				$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
			}
			elsif($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) < $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
			{
				$subscription->{'status'} = 'DOWNGRADE';
				$subscription->{'description'} = 'Downgraded by the Merchant. Merchant ID: ' . $cookie->{'ma_info'}->{'id'};
				$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
			}
			else
			{
				#If they arent upgrading, or downgrading there is no need do anything with subscriptions.
				$subscription = undef;
			}
			
#			my $updated = $MerchantAffiliates->updateMerchantMasterInformation($cookie->{'ma_info'}->{'id'}, \%merchant_information, undef, undef, $subscription, $debit_club_account);
			
#			if(! $updated)
#			{
#				$missing_fields{'not_updated'} = ['Your information was not updated'];
#				displayApplication(\%merchant_information, \%missing_fields);
#				exitScript();
#			}
		};
		if($@)
		{
			
			my $error = $@;
			
			#warn $FindBin::RealScript . ' : ' . $error;
			
			$missing_fields{'script_error'} = ['Script Error'];
			
			my $MailTools = MailTools->new();
			
			my $message = "Error Updating the Merchant Master Record : $error \n\nMerchant ID: $cookie->{'ma_info'}->{'id'}\n\n\%merchant_information: " . Dumper(%merchant_information);
			$MailTools->sendTextEmail({
				'from'=>'errors@dhs-club.com',
				'to'=>'errors@dhs-club.com',
				'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script",
				'text'=>$message
			});
		
			displayApplication(\%merchant_information, \%missing_fields);
			exitScript();
		}
	}
	
	my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated');
	
	print $CGI->redirect('-uri'=>$url);
	exitScript();
}

sub RedirectOfflineMerchant
{
	my $cookie = shift;
	
	# if they are not an online merchant, they should not be using this interface
	my $merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($cookie->{'ma_info'}->{'id'});
	if ($merchant_master_vendor_information_hashref->{'vendor_group'} && $merchant_master_vendor_information_hashref->{'vendor_group'} ne '6')
	{
		print $CGI->redirect('https://www.clubshop.com/cgi/maf/update_payment.cgi');
		exitScript();
	}
}

__END__

Sample data returns

At the beginning of processApplication

$merchant_original_record = 
$VAR1 = {
          'contact_info' => 'me',
          'emailaddress2' => 'webmaster@dhs-club.com',
          'eco_card_number' => undef,
          'state' => 'FL',
          'pay_payment_method' => 'CA',
          'stamp' => '2012-10-08 13:19:04.426791',
          'pay_address' => 'na',
          'lastname1' => 'Burke',
          'special' => undef,
          'password' => 'mac',
          'latitude' => '26.918101',
          'url' => undef,
          'emailaddress1' => 'webmaster@dhs-club.com',
          'business_type' => '1739',
          'postalcode' => '34224',
          'id' => '40',
          'discount_type' => '20',
          'pay_cardexp' => '313 ',
          'cap' => '1000',
          'pending_payment_method' => undef,
          'longitude' => '-82.329661',
          'url_lang_pref' => undef,
          'discount_subtype' => '2',
          'discount' => '0.0000',
          'pay_routing_number' => '123456789',
          'username' => 'bill',
          'reb_com' => '0.0125',
          'address1' => '2560 Placida Rd.',
          'business_name' => 'The DHS Club, Inc.',
          'ca_number_annual' => undef,
          'ca_number' => undef,
          'status' => '1',
          'pay_state' => 'FL',
          'pay_cardnumber' => '1234567890123456',
          'referral_id' => '1',
          'operator' => 'cgi',
          'city' => 'Englewood',
          'member_id' => '4652176',
          'fax' => '(941) 475-3436',
          'rebate' => '0.1000',
          'discount_blurb' => undef,
          'firstname1' => 'MR. Dick',
          'online_mall_category' => '18',
          'pay_postalcode' => '34224',
          'tin' => '987654321',
          'blurb' => 'Valid only on Sundays. Redheads receive double the discount.',
          'country' => 'US',
          'activated' => '2010-09-30',
          'pay_cardcode' => '432',
          'pay_account_number' => '1',
          'pp' => '0.01875',
          'phone' => '(941) 475-3435',
          'idnum_enter' => undef,
          'idnum_receipt' => undef,
          'pay_name' => 'Me',
          'notes' => 'status set to active by merchant_options.cgi upon KEEP',
          'pay_country' => 'US',
          'pay_city' => 'Englewood'
        };