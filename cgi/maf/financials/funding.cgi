#!/usr/bin/perl -w

=head1 NAME:
funding.cgi

	Forward to the Login Page which forwards the Merchant to the Funding Page for the Club Account.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	This script checks the Merchants credentials, if they have logged 
	in using a Master ID send them too the funding page via the login 
	page with their login credentials.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, CGI::Enurl, Sys::Hostname

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use MerchantAffiliates;
use G_S;
use CGI::Enurl;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $CGI = CGI->new();
our $G_S = G_S->new();
our $MerchantAffiliates;
our $db;


=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $query = <<EOT;
	SELECT
		*
	FROM
		some_table
EOT

=head1
SUBROUTINES

=head2
subroutineName

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub displayUnavailable
{
	print $CGI->redirect(-uri=>'/maf/financials/unavailable');
	exitScript();
}

####################################################
# Main
####################################################

unless ($db = DB_Connect('maf_financials')){exit}
$db->{RaiseError} = 1;

my $merchants_member_information = {};
my $merchant_information = {};

eval{
	$MerchantAffiliates = MerchantAffiliates->new($db);


	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)});

	die 'No Login' if $cookie->{ma_info}->{membertype} ne 'maf';


	$MerchantAffiliates->setMembers() if ! $MerchantAffiliates->{Members};



	$merchant_information = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{ma_info}->{id});

	$merchants_member_information = $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id=>$merchant_information->{member_id}});

};
if($@)
{
	displayUnavailable()
}


my $query_string = CGI::Enurl::enurl({credential_0=>$merchants_member_information->{alias}, credential_1=>$merchants_member_information->{password}, destination=>'/cgi/funding.cgi', submit=>'Continue'});

print $CGI->header('text/plain');

my ($member_id, $mem_info) = $G_S->authen_ses_key( $CGI->cookie('AuthCustom_Generic') );

use Data::Dumper;

print Dumper($member_id) . "\n\n";

print Dumper($mem_info) . "\n\n";


print $CGI->redirect(-uri=>"https://" . DHSGlobals::CLUBSHOP_DOT_COM . "/LOGIN?$query_string");


exitScript();

__END__

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


