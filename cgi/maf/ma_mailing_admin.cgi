#!/usr/bin/perl -w
###### ma_mailing_admin.cgi
###### an application for Merchant affiliates to administrate their mailings
######
###### Last Modified: 10/13/04	Karl Kohrt

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw(%URLS);
use DB_Connect;

###### CONSTANTS
use constant MAX_EMAIL_CHARS => 5000;	# Maximum number of characters per email.
use constant MIN_DISCOUNT_VALUE => 0.05;	# Min discount value to use this. Ex: 0.05 = 5% discount.
# Our email limits for each merchant type.
# Bronze=1, Silver=2, Gold=3, Platinum=4, Special Exception=5
our %MAX_EMAIL = (	'1'	=> 0,
			'2'	=> 1,
			'3'	=> 2,
			'4'	=> 4,
			'5'	=> 1 );
# This is the period to which our %MAX_EMAIL limits apply.
our $delivery_period = "NOW() - INTERVAL '1 month'";

# The vendor types that are allowed to use this feature.
# ClubBucks Merchant=5, MarketPlace Merchant=6, etc.
our $VENDOR_TYPES = '5';

###### GLOBALS
# our page templates
our %TMPL = (	'locations'		=> 10211,
		'location_row'	=> 10214,
		'editor'		=> 10212,
		'email_row'		=> 10213,
		'upgrade'		=> 10206);

# the tables we'll get data from
our %TBL = (	'master'	=> 'merchant_affiliates_master',
		'locations'	=> 'merchant_affiliate_locations',
		'discounts'	=> 'merchant_affiliate_discounts',
		'jobs'		=> 'maf_mail_jobs',
		'deliveries'	=> 'maf_mail_deliveries',
		'vendors'	=> 'vendors');

our $q = new CGI; 
our $ME = $q->script_name;
our @job_list = ();
our $min_email = 0;	# The minimum limit of email of all locations for this call to the script.
our $id = '';			# The merchant ID used throughout the script.
our @location_list = ();	# The location list from the cookie.
our $membertype = '';	# The membertype from the cookie.
our $Login_ID = '';		# The cookie login id.
our $ma_info = '';		# The cookie hash.
our $discount_type = '';	# The merchant's discount type.
our $vendor_type_ok = 1;	# The merchant's vendor type flag.
our $vendor_description = '';	# The merchant's vendor description.
our $which_merchant = '';	# Identifies which location/master record for display.
our $which_email = '';	# Identifies which email was passed into the script.
our $messages = '';		# Messages that appear at the top, below the title.
our $email = '';
our $max_reached = 0;
our $admin = '';		# Admin access parameter.
our $location_id = 0;	# The location ID passed into the script.
our $action = '';		# The action passed into the script.
our $subject = '';		# The subject passed into the script.
our $message_text = '';	# The message_text passed into the script.
our $sth = '';
our $db = '';
our $operator = '';
our $subject_prompt = "Enter NEW Subject Line here.";
our $message_prompt = "Enter NEW Message Text here.";

# Get the cookie info and connect to the db.
if ( Get_Cookie() )
{
	# Get any path info and/or parameters that may have been passed in.
	$action = $q->param('action');
	$which_email = $q->param('email_id') || '';
	($location_id = $q->path_info()) =~ s/^\///;
	$location_id = $location_id || 0;

	# If a location, check to see if the location is a vendor type that can use this feature. 
	if ( $location_id && $location_id ne 'master_id' ) { $vendor_type_ok = Check_Vendor_Type() }
	
	# If the Merchant is of the correct vendor type.
	if ( $vendor_type_ok )
	{
		# Check for special case location ID's.
		# If this is a master mailing, set the location ID to the cookie ID.
		if ( ($location_id eq 'master_id') && ($membertype eq 'maf') )
		{
			$location_id = $id;
		}
		# Assign the simple location ID or the numbered location ID.
		else
		{
			$location_id = $location_id || ( $q->param("location_id$which_email" ));
		}

		# Master logins will get the typical location list to select from unless
		#	they only have one location available to setup for.
		# If there is no $location_id(first time through), display the location(s) page, depending on
		#  whether this is a master or location login.
		unless ( $location_id )
		{
			# If it is a location login	
			if ( $membertype eq 'mal' )
			{
				# Recall script with the single location id in the path.
				print $q->redirect( $ME . "/" . $ma_info->{id} );
			}
	
			# If it is a master login...
			elsif ( $membertype eq 'maf' )
			{
				# If there is more than one location.	
				if ( scalar(@location_list) > 1 )
				{
					# Present location list with option to send a master mailing.
					Check_Discount($Login_ID, 'master');
					# Check to see if there have been jobs for the location or master ID.
					Get_Email_Jobs();
					Report_Locations();
				}
				else
				{
					# Recall script with the single location id in the path.
					print $q->redirect( $ME . "/" . $location_list[0] );
				}
			}
			else
			{
				# The membertype is not recognizable.
				Err("Your cookie info is corrupted. Please <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> again.");
			}
		}
		# If a location_id with non-numeric characters was passed, then we have an error.							
		elsif ( $location_id =~ /\D/ )
		{
			Err("Undefined location ID. Try again.");
		}
	
		# If we have a numeric location ID passed.
		else
		{
			my $login_type = 'location';
			if ( $location_id == $id && $membertype eq 'maf' ) { $login_type = 'master' }
			# If the id passed meets the minimum discount requirement
			#  and is in the cookie's master list or is the logged in location.
			if ( Check_Discount($location_id, $login_type)
				&& ((grep {/$location_id/} @location_list) || ($location_id == $id)) )
			{
				# Check to see if there have been jobs for the location or master ID.
				Get_Email_Jobs();

				# If no action was passed - first time in the script.
				unless ($action)
				{
					Build_Email();
					Do_Page();
				}
				# If we're making a change.
				elsif ( ($action eq 'create')
					|| ($action eq 'void') )
				{
					if ( $action eq 'void' )
					{
						Void_Email();
					}
					elsif ( $action eq 'create' )
					{
						#  Get the corresponding data.
						$subject = $q->param("subject$which_email");
						$message_text = $q->param("message_text$which_email");

						# Filter the subject line and message text.
						if ( Filter_Text() )
						{
							# Create the email in the database.
							Create_Email();
						}
					}
					else
					{
						$messages = "The action $action is not recognized."
					}
					Get_Email_Jobs();
					Build_Email();
					Do_Page();
				}
				else { Do_Page() }
			}
			# If the location called is not in the cookie list.
			elsif ( ! (grep {/$location_id/} @location_list) )
			{
				Err("The location ID passed is not related to your current Login ID.");
			}
			# If not at high enough discount levels, display upgrade page.
			else
			{
				my $tmpl = G_S::Get_Object($db, $TMPL{'upgrade'}) || die "Failed to load template: $TMPL{'upgrade'}";
				print $q->header(), $tmpl;
			}
		}
	}
	elsif ( ! $vendor_type_ok )
	{
		Err("This feature is not available for a '$vendor_description' location.");
	}			
	# If not at high enough discount levels, display upgrade page.
	else
	{
		my $tmpl = G_S::Get_Object($db, $TMPL{'upgrade'}) || die "Failed to load template: $TMPL{'upgrade'}";
		print $q->header(), $tmpl;
	}
}

END:
$db->disconnect;
exit;

######################################
###### Begin subroutines here.
######################################

###### Build html for all email for this merchant or location and add a create field if available.
sub Build_Email
{
	my $tmp = '';
	my $this_email = '';
	my $num_email = 0;
	my $email_row = G_S::Get_Object($db, $TMPL{'email_row'}) || die "Failed to load template: $TMPL{'email_row'}";
	
	foreach $tmp (@job_list)
	{
		unless ( defined $tmp->{mal_id} ) { $tmp->{mal_id} = 0 }
		if ( $tmp->{mal_id} == $location_id || $tmp->{ma_id} )
		{
			# Get a fresh template and do substitutions.	
			$this_email = $email_row;
			$this_email =~ s/%%ME%%/$ME/g;
			$this_email =~ s/%%email_id%%/$tmp->{pk}/g;
			$this_email =~ s/%%num_email%%/$num_email/g;
			$this_email =~ s/%%subject%%/$tmp->{subject}/g;
			$this_email =~ s/%%message_text%%/$tmp->{message_text}/g;
			$this_email =~ s/%%location_id%%/$location_id/;

			# Display which location this email is related to.
			# If it is a location job.
			if ( $tmp->{mal_id} )
			{
				$this_email =~ s/%%which_location%%/$tmp->{mal_id}/g;
			}
			# If it is a master job with a master login.
			elsif ( $tmp->{ma_id} && ($tmp->{ma_id} == $Login_ID) && ($location_id == $Login_ID) )
			{													
				$this_email =~ s/%%which_location%%/All locations/g;
			}
			# If it is a master job with a location login.
			else
			{													
				$this_email =~ s/%%which_location%%/All locations/g;
				# Hide the void button from the location login.
				$this_email =~ s/conditional-void-open.+?conditional-void-close//sg;
			}
			# Hide the Create button.
			$this_email =~ s/conditional-create-open.+?conditional-create-close//sg;

			if ( $tmp->{started} )
			{
				# Show the start time and hide the Void button.
				$this_email =~ s/%%started%%/$tmp->{started}/g;
				$this_email =~ s/conditional-void-open.+?conditional-void-close//sg;
				if ( $tmp->{delivered} )
				{
					$this_email =~ s/%%delivered%%/$tmp->{delivered}/g;
				}
				else
				{
					$this_email =~ s/%%delivered%%/Processing/g;
				}
			}
			else
			{
				# Hide the "Delivery was started..." text.
				$this_email =~ s/conditional-started-open.+?conditional-started-close//sg;
			}
			++$num_email;	
			$email .= $this_email;
		}
	}
	# Unless we are at our maximum email per period.
	unless ( $max_reached )
	{
		# Get a fresh template.	
		$this_email = $email_row;
		$this_email =~ s/%%ME%%/$ME/g;
		$this_email =~ s/%%email_id%%/_create/g;
		$this_email =~ s/%%num_email%%/$num_email/g;
		$this_email =~ s/%%subject%%/$subject_prompt/g;
		$this_email =~ s/%%message_text%%/$message_prompt/g;
		$this_email =~ s/%%location_id%%/$location_id/;

		# If this is a master login.				
		if ( $membertype eq 'maf' && $location_id == $Login_ID )
		{
			$this_email =~ s/%%which_location%%/All locations/g;
		}
		# Otherwise this is an individual location.						
		else
		{													
			$this_email =~ s/%%which_location%%/$location_id/g;
		}
		# Hide the Void button and the "Already started..." text,
		#  leaving only the Create button.
		$this_email =~ s/conditional-void-open.+?conditional-void-close//sg;
		$this_email =~ s/conditional-started-open.+?conditional-started-close//sg;
					
		++$num_email;	
		$email .= $this_email;
	}
	return $num_email;
}

# Check to see if the Merchant is at a high enough discount level to use the coupon benefit.
sub Check_Discount
{
	my ($id_passed, $login_type) = @_;
	my $discount = '';
	my $min_discount = 1.00;	# As below, a 100% discount to be replaced in the first query loop.
	$min_email = 1000;	# An arbitrarily high number to be replaced in the first query loop.

	# Select the appropriate fields.
	my $qry = "	SELECT	d.discount_type,
				d.discount
			FROM 	$TBL{discounts} d,
				$TBL{vendors} v,
				$TBL{locations} l
			WHERE	d.location_id = l.id
			AND	l.vendor_id = v.vendor_id
			AND	v.status = 1
			AND 	d.start_date <= NOW()::date
			AND 	( d.end_date IS NULL
				OR d.end_date >= NOW()::date )\n";

	# Logged in with a location ID...
	if ( $login_type eq 'location' )
	{
		$qry .= " AND d.location_id = ?";
	}				
	# ...or a master ID
	elsif ( $login_type eq 'master' )
	{
		$qry .= "	AND d.location_id = l.id
				AND l.merchant_id = ?";
	}			
	# Prepare and execute the discount check query.
	$sth = $db->prepare($qry);
	$sth->execute($id_passed);

	# Since this might be a master call, there could be more than one location.
	while ( ($discount_type, $discount) = $sth->fetchrow_array )
	{
		# If a location has a lower maximum than previous locations, reduce our #.
		if ( $MAX_EMAIL{$discount_type} < $min_email )
		{
			$min_email = $MAX_EMAIL{$discount_type};
		}
		# If a location has a lower discount than previous locations, reduce our #.
		if ( $discount < $min_discount )
		{
			$min_discount = $discount;
		}
	}		
	$sth->finish();

	# In case no records were found for whatever reason.
	if ( $min_email == 1000 ) { $min_email = 0 };
	if ( $min_discount == 1.00 ) { $min_discount = 0 };

	# If all locations can send at least one email
	# and all locations' percentage is high enough, return 1 for OK.
	if ( $min_email > 0
		&& $min_discount >= MIN_DISCOUNT_VALUE )
		{ return 1; }
	else
		{ return 0; }
}

# Check to see if the Merchant is an offline or a MarketPlace merchant.
sub Check_Vendor_Type
{
	my $qry = "	SELECT v.vendor_group,
				t.description
			FROM $TBL{locations} mal
			LEFT JOIN vendors v ON mal.vendor_id = v.vendor_id
			LEFT JOIN transaction_types t ON t.trans_type = v.vendor_group
			WHERE 	mal.id = ?";
	# Prepare and execute the vendor-type check query.
	$sth = $db->prepare($qry);
	$sth->execute($location_id);

	(my $vendor_type, $vendor_description) = $sth->fetchrow_array;
	$sth->finish();

	# If the vendor type is offline, return 1 for OK.
	if ( $vendor_type =~ m/$VENDOR_TYPES/ ) { return 1 }
	else { return 0 }
}

###### Create a new email.
sub Create_Email
{
	my $qry = '';
	my $tmp_id = '';

	# If not at the limit, create a new email.
	unless ( Get_Email_Jobs() )
	{
		# Insert the new email.
		$qry = "	INSERT INTO $TBL{jobs}
				(	subject,
					message_text,
					stamp,\n";
		# Logged in with a master ID, but not working on a single location.
 		if ( $membertype eq 'maf' && (! $location_id || $location_id == $Login_ID) )
		{
			$qry .= " ma_id ";
			$tmp_id = $id;
		}				
		# ...or a location ID
		elsif ( $membertype eq 'mal' || $location_id )
		{
			$qry .= " mal_id ";
			$tmp_id = $location_id;
		}
		else
		{
			Err("We couldn't tell what type of login you are using. Please login again.");
		}
		$qry .= ") VALUES (?, ?, NOW(), ?)";

		# Prepare and execute the query.
		$sth = $db->prepare($qry);

		if ( $sth->execute($subject, $message_text, $tmp_id) )
		{
			$messages = "<span class='b'>Email successfully created.<br></span>";
		}
		else
		{		
			$messages = "<span class='r'>Database error. Please try again.<br></span>";
		}
		$sth->finish();
		return 1;
	}
	else
	{
		$messages = "<span class='r'>You cannot create an email right now.<br></span>";
		return 0;
	}		
}

###### Displays the basic page(header & footer) to the browser with the $email embedded in it.
sub Do_Page
{
	my $tmpl = G_S::Get_Object($db, $TMPL{'editor'}) || die "Failed to load template: $TMPL{'editor'}";
	
	# Identify whether this is a location or the master.
	if ($location_id == $id) { $which_merchant = "Master " }
	else {	$which_merchant = "Location " }
	$which_merchant .= "# $location_id";

	$tmpl =~ s/%%which_merchant%%/$which_merchant/g;
	$tmpl =~ s/%%messages%%/$messages/;
	$tmpl =~ s/%%email%%/$email/;
	print $q->header(), $tmpl;
}

###### prints an error to the browser.
sub Err
{
	###### if we want to use a 'pretty' page as a holder for our message we'll try to
	###### pull up the template
	my $tmpl = '';
	my $error = shift;
			
#	G_S::Get_Object($db, 'TEMPLATE_NUMBER') if $db;
	
	unless ($tmpl)
	{
		$tmpl = qq|
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
			<html><head>
			<title>Error</title>
			</head>
			<body bgcolor="#ffffff">
			<br><b>%%message%%</b>
			<p>Current server time: <tt>%%timestamp%% PST</tt>
			</body></html>|;
	}

	$tmpl =~ s/%%message%%/$error/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header(-expires=>'now'), $tmpl;
}

###### Filter the text elements being submitted. Included is truncation, confirming that the fields
######		are not blank or not still the prompt values, and removing html tags.
sub Filter_Text
{
	my $rv = 1;

	# Remove all html tags.
	$message_text =~ s/<.*?>//g;
	$subject =~ s/<.*?>//g;

	# If we have more than the max characters allowed.
	if ( length($message_text) > MAX_EMAIL_CHARS )
	{
		# Truncate the value to the defined length.
		$message_text = substr $message_text, 0, MAX_EMAIL_CHARS;
		my $max = MAX_EMAIL_CHARS;
		$messages = "<span class='r'>The Email Body has been truncated to the maximum size of $max characters.<br>Please review it, make the necessary modifications, then try again.<br></span>";
		$rv = 0;
	}

	# Display an error if the text fields still equal the default prompts
	#	or if they don't have at least one alphanumeric character.
	if ( $subject eq $subject_prompt || ! ($subject =~ m/\w/) )
	{
		$messages = "<span class='r'>Please personalize the Subject Line, then try again.<br></span>";
		$rv = 0;
	}
	if ( $message_text eq $message_prompt || ! ($message_text =~ m/\w/) )
	{
		$messages .= "<span class='r'>Please personalize the Message Text, then try again.<br></span>";
		$rv = 0;
	}
	# If there was an error detected, re-populate the fields for the form to display.
	if ( $rv == 0 )
	{
		$subject_prompt = $subject;
		$message_prompt = $message_text;
	}
	return $rv;
}
																																																				
###### Get the info stored in the cookie and connect to the DB if we have a good cookie.
sub Get_Cookie
{
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		$admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		$operator = $q->cookie('operator');
		my $pwd = $q->cookie('pwd');
		
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for connection errors.
		unless ($db = ADMIN_DB::DB_Connect( 'ma_mailing_admin.cgi', $operator, $pwd ))
		{
			return 0;
		}
		# Get the info passed in via parameters.
		unless ( ($id = $q->param('merchant_id')) && ($membertype = $q->param('membertype')) )
		{
			Err("Missing a required parameter.");
			return 0;
		}
	}
	# If this is a merchant.		 
	else
	{
		$operator = 'cgi';
		unless ($db = &DB_Connect::DB_Connect('ma_mailing_admin.cgi'))
		{
			return 0;
		}
		# Get the Login ID and Merchant Affiliate info hash from the cookie.
		( $Login_ID, $ma_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_maf'), 'maf' );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$Login_ID || $Login_ID =~ /\D/)
		{ 			Err("You must <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> to use this application.");
			return 0;
		}
		$id = $ma_info->{id};
		$membertype = $ma_info->{membertype};
		@location_list = split (/\,/, $ma_info->{maflocations});
	}
	$db->{RaiseError} = 1;
	return 1;
}

###### Check to see if there have been any jobs for the location or master ID.
sub Get_Email_Jobs
{
	my %num_email = ();
	my $master_id = 0;
	$location_id ||= 0;			# Default location to zero to stop uninit. errors.
	$num_email{$id} = 0;			# Default # of caller email to stop uninit. errors.
	$num_email{$location_id} = 0;	# Default # of location email to stop uninit. errors.
	@job_list = ();
	$max_reached = 0;
	my $display_date = '';

	# let's see if we have a Master mailing that covers us (we may be a location login)
	my $qry = "	SELECT	pk , 
				ma_id,
				mal_id,
				message_text,
				subject,
				voided,
				stamp::TIMESTAMP(0) AS stamp,
				delivered::TIMESTAMP(0) AS delivered, 
				started::TIMESTAMP(0) AS started
			FROM $TBL{jobs}
			WHERE stamp >= $delivery_period
			AND voided != 't'
			AND ";

	# if we are logged in as a Master then we'll look up all of our locations as well as a master mailing
	if ($membertype eq 'maf')
	{
		$qry .= "( ma_id= ? OR ${\Location_List_Qry()} )\n";
	}
	# Otherwise we can look at just our location and the Master			
	else
	{
		###### get our Merchant ID
		my $sth = $db->prepare("
			SELECT merchant_id
			FROM $TBL{locations}
			WHERE id= ?");
		$sth->execute($id);

		my $merchant_id = $sth->fetchrow_array;
		$sth->finish;

		die "Failed to lookup your Merchant Affiliate master record." unless $merchant_id;
		
		$qry .= "( mal_id= ? OR ma_id= $merchant_id )\n";
	}
	$qry .= "ORDER BY stamp desc";

	my $sth = $db->prepare($qry);
	$sth->execute($id);

	# Put all current jobs on the list.
	while (my $tmp = $sth->fetchrow_hashref)
	{
	###### since the first one will be the youngest, we have to set this value here
		$display_date ||= $tmp->{stamp};
		push (@job_list, $tmp);
	}
	$sth->finish;

	foreach my $tmp(@job_list)
	{
		# If it's a master email job.
		if ( $tmp->{ma_id} )
		{
			$num_email{$tmp->{ma_id}}++;
			$master_id = $tmp->{ma_id};
		}
		# If it's a location email job.
		else
		{
			$num_email{$tmp->{mal_id}}++;
		}

		# If we're here as the master.
		if ( ($location_id == $id) && ($membertype eq 'maf') )
		{
			# Look at each location with email.
			foreach ( keys %num_email )
			{
				my $how_many = 0;	# How many email we have.
				my $how_few = 0;	# How few email we are allowed.
				my $this_discount_type = 0;
				# If it's the master that was passed.
				if ( $_ == $id )
				{
					$how_many = $num_email{$id};
					$how_few = $min_email;
				}
				else
				{
					$how_many = $num_email{$id} + $num_email{$_};
					my $sth = $db->prepare("
						SELECT	d.discount_type
						FROM 	$TBL{discounts} d
						WHERE 	d.location_id = ?
						AND 	d.start_date <= NOW()::date
						AND 	(
							d.end_date IS NULL
							OR
							d.end_date >= NOW()::date)
							");
					$sth->execute($_);
					($this_discount_type) = $sth->fetchrow_array;
					$how_few = $MAX_EMAIL{$this_discount_type};
					$sth->finish();
				}
				# If the sum total of a location and the master >= the set max.
				if ( $how_many >= $how_few && ! $max_reached )
				{
					$num_email{$_} ||= 0;
					$num_email{id} ||= 0;
					$max_reached = $num_email{id} + $num_email{$_};
				#	my $display_date = substr($tmp->{stamp}, 0, 10);
					$messages .= "<span class='r'>You may only email $min_email time(s) every 30 days. Your last email was on $display_date.</span><br />";
					unless ( $_ == $location_id )
					{
						$messages .= "<span class='r'>Location # $_ has $num_email{$_} email, combined with any master email, has caused you to reach your limit for master email.</span><br>";
					}						
				}
			}
		}
		# Otherwise, we must be here as a location.
		# If the sum total of the location and the master >= the set max.
		elsif ( ($num_email{$location_id} + $num_email{$master_id})
				>= $min_email
				&& ! $max_reached )
 		{			
			$max_reached = $num_email{$location_id} + $num_email{$master_id};
		#	my $display_date = substr($tmp->{stamp}, 0, 10);
			$messages .= "<span class='r'>You may only email $min_email time(s) every 30 days.  Your last email was on $display_date.</span><br>";
		}
	}
	return $max_reached;
}

###### Creates a portion of the query containing the locations.				
sub Location_List_Qry
{
	my $mod = '(';
					
	foreach (@location_list)
	{
		$mod .= " mal_id= $_ OR";
	}

	###### truncate the last OR and insert a closing parenthesis
	$mod =~ s/ OR$/\)/;
	return $mod;	
}	

###### Display a list of locations if we have more than one.
sub Report_Locations
{
	my $html = '';
	my $data = '';
	my $one_row = '';
	my $location_id = 0;
	my $report_body = '';
	my $created = 0;			
	my $master_created = 0;
	my $master_maximum = 0;

	# Prepare a query to get details about each location id.
	my $qry = "	SELECT mal.id,
				mal.location_name,
				mal.city,
				mal.address1,
				mal.state,
				mal.country,
				v.vendor_group,
				v.status,
				d.discount_type
			FROM $TBL{locations} mal
			LEFT JOIN $TBL{vendors} v
			ON 	mal.vendor_id = v.vendor_id
			LEFT JOIN merchant_affiliate_discounts d
			ON 	mal.id = d.location_id
			AND 	d.start_date <= NOW()::date
			AND 	(
				d.end_date IS NULL
				OR
				d.end_date >= NOW()::date
				)
			WHERE 	mal.id = ?";
				
	my $sth = $db->prepare($qry);

	my $location_row = G_S::Get_Object($db, $TMPL{location_row}) || die "Couldn't open $TMPL{location_row}";
	# Display a line for each location in the list.
	foreach $location_id (@location_list)
	{
		$created = 0;
		$master_created = 0;
				
		# Flag each of the locations that have a job during the current period.
		foreach my $tmp(@job_list)
		{
			$tmp->{ma_id} = $tmp->{ma_id} || 0;
			$tmp->{mal_id} = $tmp->{mal_id} || 0;
			if ( ($tmp->{ma_id})
				|| ($tmp->{mal_id} == $location_id ) )
			{
				$created++;
				$master_created++;
			}
		}

 		# Get the detailed info for the location listing.
		$sth->execute($location_id);
		$data = $sth->fetchrow_hashref();
		next unless $data;
		
		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		###### instead of excluding inactive locations and MarketPlace locations
		###### we'll signify their status.....
		unless (defined $data->{status})
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}
		elsif ($data->{status} eq '0')
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}
		elsif ($data->{status} eq '1' && $data->{vendor_group} eq '5')
		{
			$one_row =~ s#%%id%%#$q->a({-href	=> "$ME/$data->{id}",
							-target=> "_blank" },
							$data->{id} )#e;
		}
		else
		{
			$one_row =~ s/%%id%%/$data->{id}/;
		}

		$one_row =~ s/%%location_name%%/$data->{location_name}/;
		$data->{address1} ||= '&nbsp;';
		$data->{city} ||= '&nbsp;';
		$data->{state} ||= '&nbsp;';
		$data->{country} ||= '&nbsp;';
		$one_row =~ s/%%address1%%/$data->{address1}/;
		$one_row =~ s/%%city%%/$data->{city}/;
		$one_row =~ s/%%state%%/$data->{state}/;
		$one_row =~ s/%%country%%/$data->{country}/;


		unless (defined $data->{status})
		{
			$one_row =~ s/%%status%%/Location Not Activated/;
		}
		elsif ($data->{status} eq '0')
		{
			$one_row =~ s/%%status%%/Location Inactive/;
		}
		elsif ($data->{status} eq '1')
		{
			if ($data->{vendor_group} eq '5')
			{
				if ( $created >= $MAX_EMAIL{$data->{discount_type}} )
				{
					$one_row =~ s/%%status%%/Not Available/;
				}
				else
				{
					$one_row =~ s/%%status%%/Available/;
				}
				# Mark the fact that the master's max has been reached.
				if ( $master_created >= $MAX_EMAIL{$data->{discount_type}} )
				{
					$master_maximum = 1;
				}
			}
			else
			{
				$one_row =~ s#%%status%%#N/A MarketPlace Location#;
			}
		}
		else
		{
			$one_row =~ s/%%status%%/Incorrect Location Status/;
		}

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}

	# If this is a master login, include the master record.
	if ( $membertype eq 'maf' )
	{
		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		$one_row =~ s/%%id%%/$q->a( {	-href	=> $ME . "\/master_id",
							target=> "_blank" },
							"Master ID" )/e;
		$one_row =~ s/%%location_name%%/All Locations/;
		$one_row =~ s/%%address1%%/&nbsp;/;
		$one_row =~ s/%%city%%/&nbsp;/;
		$one_row =~ s/%%state%%/&nbsp;/;
		$one_row =~ s/%%country%%/&nbsp;/;
		if ( $master_maximum  )
		{
			$one_row =~ s/%%status%%/Not Available/;
		}
		else { $one_row =~ s/%%status%%/Available/ }

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}	
	$sth->finish();

	# Get the body template and make the substitutions.	
	my $tmpl = G_S::Get_Object($db, $TMPL{locations}) || die "Couldn't open $TMPL{locations}";
	$tmpl =~ s/%%MONTHNAME%%//;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$tmpl =~ s/%%MONTHLY_DUE%%//;
	$tmpl =~ s/%%TOTAL%%//;

	print $q->header(-expires=>'now'), $tmpl;
}

###### Void the selected email from the table.
sub Void_Email
{
	my $qry = '';
	my $tmp_id = '';

	# The void query.
	$qry = "	UPDATE $TBL{jobs}
			SET 	voided = 't'
			WHERE pk = ?
			AND started IS NULL
			AND ";

	# Logged in with a master ID, but not working on a single location.
 	if ( $membertype eq 'maf' && (! $location_id || $location_id == $Login_ID) )
	{
		$qry .= " ma_id = ?";
		$tmp_id = $id;
	}				
	# ...or a location ID.
	elsif ( $membertype eq 'mal' || ($location_id != $Login_ID) )
	{
		$qry .= " mal_id = ?";
		$tmp_id = $location_id;
	}
	else
	{
		$messages = "<span class='r'>This action is not allowed from this location.<br></span>";
		$sth->finish();
		return 0;
	}		

	# Prepare and execute the delete query.
	$sth = $db->prepare($qry);
	my $rv = $sth->execute($which_email, $tmp_id);
	if ( $rv == 1 )
	{
		$messages = "<span class='b'>Email successfully voided.<br></span>";
	}
	else
	{			
		$messages = "<span class='r'>This email can't be voided from this location login.<br></span>";
	}
	$sth->finish();
	return 1;
}

###### 09/15/03 revamped to exclude all inapplicable locations from mailing availability
###### this does not include the ultimate need to exclude certain locations
###### who are using a different discount structure than the Merchant master record
###### 12/01/03 Added the ability for different merchant discount types to have different
######			maximum numbers of email per month.
###### 12/05/03 Added '$num_email{$id} ||= 0;' in order to alleviate uninitialized value errors.
###### 12/05/03 Added Check_Vendor_Type to produce an error if location is not of a type
######			that can use this feature.
###### 12/15/03 Added an error handler in the case of more than MAX_EMAIL_CHARS in the body.
###### 12/16/03 Corrected mis-handling of master email counts. They were based upon the master
######			discount values, which are obsolete. We are now looping through all active
######			locations and saving the minimum values as the master's values.			
###### 12/30/03 Increased the email max to 5000 characters per Dick's instruction.
###### 02/18/04 revised the date presented for the last mailing to actually be the
###### 		last if there was more than one
###### 08/05/04 Added grouping parenthesis in Main around the second half of the 
######			"if ( Check_Discount ... && ..." statement.
######		Before this change, a location with less than the minimum discount could gain access.
###### 10/13/04 Added a missing parenthesis in the $db->prepare statement near line 704.
