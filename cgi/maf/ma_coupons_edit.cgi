#!/usr/bin/perl -w
###### ma_coupons_edit.cgi
######
###### A script for editing the coupons of Merchant Affiliates
###### Admin access is made by passing the following parameters:
######
######		'?admin=1&merchant_id=<id number>&membertype=<mal|maf>'.
######
###### Created:	07/25/03	Karl Kohrt
######
###### last modified: 02/10/04	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw( %URLS );
use DB_Connect;

$| = 1;   # do not buffer error messages

###### CONSTANTS
# Set the minimum discount type that can access the coupon benefit.
# Bronze=1, Silver=2, Gold=3, Platinum=4, Special Exception=5
use constant MIN_DISCOUNT_TYPE => 4;

# Set the minimum discount value that can access the coupon benefit.
#  A % divided by 100. Example: 0.1 = 10%
use constant MIN_DISCOUNT_VALUE => 0.05;

# The script that displays the coupons in their final format.							
use constant COUPON_DISPLAY_SCRIPT => "http://www.clubshop.com/cgi/ma_coupons.cgi";
							
use constant MAX_COUPONS => 2;		# Maximum number of coupons per location or master.
use constant MAX_COUPON_CHARS => 200;	# Maximum number of characters per coupon.
							
###### GLOBALS
our $db = ();
our $q = new CGI;
our $sth = '';
our $id = '';			# The merchant id used throughout the program.
our $membertype = '';	# The membertype used throughout the program.
our $coupons = '';		# String which holds the coupons for display to the browser.
our $which_coupon = '';	# Which record to update/create.
our $value = '';		# Coupon text value passed in by the merchant.
our $expiration = '';	# Expiration date passed in by the merchant.							
our $Login_ID = '';		# The cookie login id.
our $ma_info = '';		# The cookie hash.
our $which_merchant = '';	# Identifies which location/master record for display.
our $messages = '';		# Messages that appear at the top, below the title.
our $admin = '';		# Admin access parameter.
our $operator = '';		# Admin operator value.
our $notes = '';		# Admin notes value.

our %TMPL = (	'Report'	=> 10207,
		'Coupon'	=> 10208,
		'Upgrade'	=> 10206);

our %TBL = (	'coupons'	=> 'merchant_affiliate_coupons',
		'locations'	=> 'merchant_affiliate_locations',
		'master'	=> 'merchant_affiliates_master');

our %DAYS_IN_MONTH = (	'01' => 31,
				'02' => 29,
				'03' => 31,
				'04' => 30,
				'05' => 31,
				'06' => 30,
				'07' => 31,
				'08' => 31,
				'09' => 30,
				'10' => 31,
				'11' => 30,
				'12' => 31);

our $action = $q->param('action') || '';
our $ME = $q->script_name;

# Is the Merchant or Admin cookie available - logged in?
if ( Get_Cookie() )
{
	# Is the Merchant at a high enough discount level?
	if ( Check_Discount() )
	{
		# If no action was passed - first time in the script.
		unless ($action)
		{
			# Build and execute a query for coupons?
			Query_Coupons();

			# If coupons we're found with the query, we prepared them for display.		
				if ( Build_Coupons() )
				{
					# Now put them out to the browser.
					$messages = "";
				}
				else
				{
					$messages .= "<span class='r'>
							No Coupons were found for Merchant $which_merchant.
							</span>"
				}						
				Do_Report();
		}
		# If we're updating or creating.
		elsif ( ($action eq 'update')
				|| ($action eq 'create')
				|| ($action eq 'delete') )
		{
			# Find out which button was pressed,
			#  then get the corresponding data.
			$which_coupon = $q->param('coupon_id');
			$value = $q->param("value$which_coupon");
			$expiration = $q->param("expiration$which_coupon");

			if ( $admin )
			{
				$notes = $q->param("notes$which_coupon");
			}
			else
			{
				$notes = 'Merchant update.';
			}

			if ($action eq 'delete')
			{
				Delete_Coupon();
			}
			elsif ( Date_Valid() )
			{
				# Truncate the value to the defined length.
				$value = substr $value, 0, MAX_COUPON_CHARS;
				if ($action eq 'update')
				{
					Update_Coupon();
				}
				elsif ($action eq 'create')
				{
					Create_Coupon();
				}
			}														
			Query_Coupons();
			Build_Coupons();
			Do_Report();
		}
		else
		{
			$messages = "The action $action is not recognized."
		}
	}
	# If not at high enough discount levels, display Upgrade page.
	else
	{
		my $tmpl = G_S::Get_Object($db, $TMPL{'Upgrade'}) || die "Failed to load template: $TMPL{'Upgrade'}";
		print $q->header(), $tmpl;
	}
}
$db->disconnect;
exit;


######################################
###### Begin subroutines here.
######################################

###### Build a list of all coupons for this merchant or location.
sub Build_Coupons
{
	my $master_coupons = 0;
	my $location_coupons = 0;
	my $num_coupons = 0;
	my $tmp = '';
	my $this_coupon = '';
	my $buttons = '';
	my $num_forms = 0;
	my $coupon_row = G_S::Get_Object($db, $TMPL{'Coupon'}) || die "Failed to load template: $TMPL{'Coupon'}";
	
	while ($tmp = $sth->fetchrow_hashref)
	{
		# Get a fresh template.	
		$this_coupon = $coupon_row;
		# Count how many of each type of coupon that we found.
		$tmp->{ma_id} ? ++$master_coupons : ++$location_coupons;

		$this_coupon =~ s/%%ME%%/$ME/g;
		$this_coupon =~ s/%%coupon_id%%/$tmp->{id}/g;
		$this_coupon =~ s/%%value%%/$tmp->{value}/g;
		$this_coupon =~ s/%%expiration%%/$tmp->{expiration}/g;

		# If this is a location login but a master coupon...								
		if ( $membertype eq 'mal'
			 && $tmp->{ma_id} )
		{
			# Label it as a master coupon.
			$buttons .= "This <b>Master Coupon<\/b> can't be changed while logged in as location # $id.<br>";
			# Provide a button to display the finished coupon(s) in another window.
			my $go_here = COUPON_DISPLAY_SCRIPT . "?coupon_id=$tmp->{id}";
			$buttons .= $q->button(	-name	=> '',
							-value	=> 'View Coupon',
							-onclick=>"window.open('$go_here')");
		}
		else
		{
			# Otherwise, provide update and delete buttons.
			$buttons = $q->submit(	-name	=> '',
							-value	=> 'Update Coupon',
							-onclick=>"document.forms[$num_forms].action.value='update'");
			$buttons .= "&nbsp;&nbsp;";
			$buttons .= $q->submit(	-name	=> '',
							-value	=> 'Delete Coupon',
							-onclick=>"document.forms[$num_forms].action.value='delete'");
			$buttons .= $q->hidden(	-name	=> 'action',
							-value	=> '');
			$buttons .= "&nbsp;&nbsp;";

			# Provide a button to display the finished coupon(s) in another window.
			my $go_here = COUPON_DISPLAY_SCRIPT . "?coupon_id=$tmp->{id}";
			$buttons .= $q->button(	-name	=> '',
							-value	=> 'View Coupon',
							-onclick=>"window.open('$go_here')");
		}						
		$this_coupon =~ s/%%bottom_line%%/$buttons/g;

		# If this is an admin login, display all the table fields.
		if ( $admin )
		{
			$tmp->{notes} = $tmp->{notes} || '';
			$this_coupon =~ s/%%operator%%/$tmp->{operator}/g;
			$this_coupon =~ s/%%stamp%%/$tmp->{stamp}/g;
			$this_coupon =~ s/%%notes%%/$tmp->{notes}/g;
			$this_coupon =~ s/%%admin%%/$admin/g;
			$this_coupon =~ s/%%merchant_id%%/$id/g;
			$this_coupon =~ s/%%membertype%%/$membertype/g;
		}
		else
		{								
			$this_coupon =~ s/conditional-admin-open.+?conditional-admin-close//sg;
		}
		++$num_forms;	
		$coupons .= $this_coupon;
	}
	$sth->finish;
	# Determine which is the number of coupons based upon		
	#  whether we're logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		$num_coupons = $master_coupons;
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		$num_coupons = $location_coupons;
	}

	# If there are still coupons available, display fields and button to create.
	if ( $num_coupons < MAX_COUPONS )
	{
		# Get a fresh template.	
		$this_coupon = $coupon_row;
		# Count how many of each type of coupon that we found.
		$tmp->{ma_id} ? ++$master_coupons : ++$location_coupons;

		$this_coupon =~ s/%%ME%%/$ME/g;
		$this_coupon =~ s/%%coupon_id%%/_create/g;
		$this_coupon =~ s/%%value%%/Enter NEW coupon text here./g;
		$this_coupon =~ s/%%expiration%%//g;

		# Provide a create button.
		$buttons = $q->submit(	-name	=> '',
						-value	=> 'Create Coupon',
						-onclick=>"document.forms[$num_forms].action.value='create'");
		$buttons .= $q->hidden(	-name	=> 'action',
						-value	=> '');
		$this_coupon =~ s/%%bottom_line%%/$buttons/g;
						
		if ( $admin )
		{
			$this_coupon =~ s/%%operator%%//g;
			$this_coupon =~ s/%%stamp%%//g;
			$this_coupon =~ s/%%notes%%//g;
			$this_coupon =~ s/%%admin%%/$admin/g;
			$this_coupon =~ s/%%merchant_id%%/$id/g;
			$this_coupon =~ s/%%membertype%%/$membertype/g;
		}
		else
		{								
			$this_coupon =~ s/conditional-admin-open.+?conditional-admin-close//sg;
		}
		$coupons .= $this_coupon;
	}
	return $num_coupons;		
}

# Check to see if the Merchant is at a high enough discount level to use the coupon benefit.
sub Check_Discount
{
	# Select the appropriate fields.
	my $qry = "	SELECT	m.discount_type,
				m.discount				
			FROM 	$TBL{master} m";

	# Logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		$qry .= " WHERE m.id = ?";
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		$qry .= ", $TBL{locations} l
				WHERE l.merchant_id = m.id
				AND l.id = ?";
	}			
	# Prepare and execute the discount check query.
	$sth = $db->prepare($qry);
	$sth->execute($id);

	my ($discount_type, $discount) = $sth->fetchrow_array;
	$sth->finish;

	# If the discount type and percentage is high enough, return 1 for OK.
	if ( $discount_type >= MIN_DISCOUNT_TYPE && $discount >= MIN_DISCOUNT_VALUE )
		{ return 1; }
	else
		{ return 0; }
}

###### Create a new coupon.
sub Create_Coupon
{
	my $rv = '';
	my @id_list = ();
	my $qry = '';
	my $how_many = 0;

	# Count how many coupons the location or master has.
	$qry = "	SELECT count(*)				
			FROM 	$TBL{coupons} c
			WHERE ";

	# Logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		$qry .= " ma_id = ?";
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		$qry .= " mal_id = ?";
	}
	# Prepare and execute the count query.
	$sth = $db->prepare($qry);

	$sth->execute($id);
	($how_many) = $sth->fetchrow_array;
	$sth->finish();

	# If not at the limit, create a new coupon.
	if ( $how_many < MAX_COUPONS )
	{
		# Insert the new coupon.
		$qry = "	INSERT INTO $TBL{coupons}
				(	value,
					expiration,
					operator,
					notes,
					stamp,\n";

		# Logged in with a master ID...
		if ( $membertype eq 'maf' )
		{
			$qry .= " ma_id ";
		}				
		# ...or a location ID
		elsif ( $membertype eq 'mal' )
		{
			$qry .= " mal_id ";
		}

		$qry .= ") VALUES (?, ?, ?, ?, NOW(), ?)";

		# Prepare and execute the query.
		$sth = $db->prepare($qry);

		if ( $sth->execute($value, $expiration, $operator, $notes, $id) )
		{
			$messages .= "<span class='b'>Coupon successfully created.</span>";
		}
		else
		{			
			$messages .= "<span class='r'>Database error. Please try again.</span>";
		}
		$sth->finish();
		return 1;
	}
}

###### Check the date to see if it is of the right format and >= today.
sub Date_Valid
{
	my ($mday, $mon, $year) = ();
	my $exp_month = substr $expiration, 5, 2;	# The month of the expiration date.
	my $exp_days = substr $expiration, 8, 2;	# The day of the expiration date.
	my $valid = 1;				# Start with a valid date flag.

	# Calculate today's date in yyyy-mm-dd format to compare to expiration.
	($mday, $mon, $year) = (localtime(time))[3,4,5];
	my $today = sprintf "%04d-%02d-%02d", $year + 1900,
							$mon + 1,
							$mday;

	# A date of the format yyyy-mm-dd, day <= number of days in in the month, >= today .
	unless( ( $expiration =~ /^200\d{1}-\d{2}-\d{2}$/ )
		&& ( $exp_days <= $DAYS_IN_MONTH{$exp_month} )
		&& ( $expiration ge $today ))
	{
		$valid = 0;
		$messages .= "<span class='r'>No action was taken because you entered the date '$expiration'.
				<br>Expiration date should be of the format yyyy-mm-dd and not earlier than today.
				</span>";
	}
	return $valid;
}

###### Delete the selected coupon from the table.
sub Delete_Coupon
{
	my $qry = '';

	# The delete query.
	$qry = "	DELETE				
			FROM 	$TBL{coupons}
			WHERE id = ?
			AND ";

	# Logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		$qry .= " ma_id = ?";
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		$qry .= " mal_id = ?";
	}
	# Prepare and execute the delete query.
	$sth = $db->prepare($qry);

	if ( $sth->execute($which_coupon, $id) )
	{
		$messages .= "<span class='b'>Coupon successfully deleted.</span>";
	}
	else
	{			
		$messages .= "<span class='r'>Database error. Please try again.</span>";
	}
	$sth->finish();
	return 1;
}

###### Displays the basic page(header & footer) to the browser with the $coupons embedded in it.
sub Do_Report
{
	my $tmpl = G_S::Get_Object($db, $TMPL{'Report'}) || die "Failed to load template: $TMPL{'Report'}";
	
	$tmpl =~ s/%%which_merchant%%/$which_merchant/g;
	$tmpl =~ s/%%messages%%/$messages/;
	$tmpl =~ s/%%coupons%%/$coupons/;
	print $q->header(), $tmpl;
}

###### prints an error to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<br><B>$_[0]</B>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		$admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		$operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for connection errors.
		unless ($db = ADMIN_DB::DB_Connect( 'ma_coupons_edit.cgi', $operator, $pwd ))
		{
			return 0;
		}
		# Get the info passed in via parameters.
		unless ( ($id = $q->param('merchant_id')) && ($membertype = $q->param('membertype')) )
		{
			Err("Missing a required parameter.");
			return 0;
		}
	}
	# If this is a merchant.		 
	else
	{
		$operator = 'cgi';
		unless ($db = &DB_Connect::DB_Connect('ma_coupons_edit.cgi'))
		{
			return 0;
		}
		# Get the Login ID and Merchant Affiliate info hash from the cookie.
		( $Login_ID, $ma_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_maf'), 'maf' );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$Login_ID || $Login_ID =~ /\D/)
		{
			Err("You must <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> to use this application.");
			return 0;
		}
		$id = $ma_info->{id};
		$membertype = $ma_info->{membertype};
	}										
	return 1;
}

###### Build and run the query for the existing coupons.
sub Query_Coupons
{
	my $rv = '';
	my @id_list = ();

	# Select the appropriate fields.
	my $qry = "	SELECT	c.id,
				c.value,
				c.expiration,
				c.ma_id,
				c.mal_id,
				c.operator,
				c.stamp,
				c.notes,
				m.discount_type,
				m.discount				
			FROM 	$TBL{coupons} c,
				$TBL{master} m";

	# Logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		 $which_merchant = "Master ";
		$qry .= "	WHERE c.ma_id = m.id
					AND c.ma_id = ?";
		@id_list = ($id);
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		 $which_merchant = "Location ";
		$qry .= "	, $TBL{locations} l
				WHERE (( c.mal_id = l.id
					AND m.id = l.merchant_id
					AND l.id = ?)
				OR
				( c.ma_id = l.merchant_id
					AND m.id = l.merchant_id
					AND l.id = ?))";
		@id_list = ($id, $id);
	}
	else
	{
		Err("Your cookie is corrupted. Please logout and <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> again.");
		return 0;
	}

	# Order by coupon id so the display order doesn't change.
	$qry .= "\nORDER BY ma_id, c.id;";

	# Prepare and execute the coupons query.
	$sth = $db->prepare($qry);
	$sth->execute(@id_list);
	$which_merchant .= "# $id";
	return 1;
}

###### Update an existing coupon.
sub Update_Coupon
{
	# Select the appropriate fields.
	my $qry = "	UPDATE $TBL{coupons}
			SET 	value = ?,
				expiration = ?,
				operator = ?,
				notes = ?,
				stamp = NOW()
			WHERE id = ?
			AND ";

	# Logged in with a master ID...
	if ( $membertype eq 'maf' )
	{
		$qry .= " ma_id = ?";
	}				
	# ...or a location ID
	elsif ( $membertype eq 'mal' )
	{
		$qry .= " mal_id = ?";
	}

	# Prepare and execute the query.
	$sth = $db->prepare($qry);

	if ( $sth->execute($value, $expiration, $operator, $notes, $which_coupon, $id) )
	{
		$messages .= "<span class='b'>Coupon successfully updated.</span>";
		return 1;
	}
	else
	{			
		$messages .= "<span class='r'>Database error. Please try again.</span>";
		return 0;
	}
}

###### 02/10/04 fixed a broken active statement handle in Check_Discount
