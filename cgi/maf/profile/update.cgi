#!/usr/bin/perl

=head1 NAME:
maf::profile::update.cgi

	This script determines if the Merchant has logged in with 
	location credential, or master credentials.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	The merchants cookie is retrieved, decoded, and checked 
	to see if they have the Master credentials, or the 
	Location credential.  If the Master Credentials are 
	recognize the Merchant is presented with a page with links
	allowing them to update their master profile information,
	and edit their locations, or add new locations.  If they
	have the Location Credentials they are redirected to a page
	to update thier location information.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict
	warnings
	FindBin
	DHSGlobals
	CGI
	DB
	DB_Connect
	G_S
	XML::local_utils
	XML::Simple
	CGI
	CGI::Cookie
	MerchantAffiliates
	MailTools

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI;
use CGI::Cookie;
use MerchantAffiliates;
use MailTools;
use DHS::UrlSafe;
use Data::Dumper;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
	
	$db
	$CGI
	$G_S
	$MerchantAffiliates

=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $G_S = G_S->new();
my $MerchantAffiliates = {};
my $DHS_UrlSafe = {};

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.
	
	$cookie	HASHREF
	$xml	SCALAR
	$xsl	SCALAR
	$configuration_xml	SCALAR
	$application_configuration	HASHREF

=cut

my $cookie = {};
my $xml = '';
my $xsl = '';
my $configuration_xml = '';
my $application_configuration = {};
my @fields_to_build_store_url = ('country', 'state', 'city', 'location_name_url');

=head1
SUBROUTINES

=head2 exitScript

=over

=item I<Description:>

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=item I<Params:>

	none

=item I<Returns:>

	none

=back

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2 displayUnavailable

=over

=item I<Description:>

	Redirect the user to the unavailable page.

=item I<Params:>

	none

=item I<Returns:>

	none

=back

=cut

sub displayUnavailable
{
	print $CGI->redirect(-uri=>'/maf/profile/unavailable');
	exitScript();
}

####################################################
# Main
####################################################

unless ($db = DB_Connect('merchant_affiliates')){exit}

$db->{RaiseError} = 1;

#			Master Cookie
#			$VAR1 = {
#			          'ma_info' => {
#			                         'membertype' => 'maf',
#			                         'id' => '41',
#			                         'maflocations' => '9995,9755,9986,9900,10217,9797,9735',
#			                         'mafname' => 'The DHS Club, Inc.',
#			                         'username' => 'bill'
#			                       },
#			          'login_id' => '41'
#			        };
#
#
#		Location Cookie
#		$VAR1 = {
#		          'ma_info' => {
#		                         'membertype' => 'mal',
#		                         'id' => '9735',
#		                         'maflocations' => '9735',
#		                         'mafname' => 'The DHS Club, Inc.',
#		                         'username' => 'bill'
#		                       },
#		          'login_id' => '9735'
#		        };

eval{

	$MerchantAffiliates = MerchantAffiliates->new($db);
	
	$DHS_UrlSafe = DHS::UrlSafe->new();
	
	$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );

};
if($@)
{
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"There was an error instanciating the MerchantAffilates Class, or retrieving the Merchants cookie.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
}



if($cookie->{ma_info}->{membertype} eq 'maf')
{
	eval{
		
		$xml = G_S::Get_Object($db, 10729);
		$xsl = G_S::Get_Object($db, 10728);
		
		$configuration_xml = G_S::Get_Object($db, 10681);
		
		
		$application_configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");
		
		my $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{ma_info}->{id});
		
#		my $locations_array_of_hashrefs = $MerchantAffiliates->retrieveAllMerchantLocationsByMerchantID($cookie->{ma_info}->{id},1);
		my $locations_array_of_hashrefs = $MerchantAffiliates->retrieveAllMerchantLocationsByMerchantID($cookie->{ma_info}->{id},0);
		
		
		my $merchant_master_location_record_hashref = $MerchantAffiliates->retrieveMerchantMasterLocationByMerchantID($cookie->{ma_info}->{id});
		
		my @url_for_individual_location = ('business');
		foreach my $index (@fields_to_build_store_url)
		{
			last if ! $merchant_master_location_record_hashref->{$index};
			push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($merchant_master_location_record_hashref->{$index});
		}
		
		$merchant_master_record_hashref->{store_url} = '/' . join('/', @url_for_individual_location);
                $merchant_master_record_hashref->{store_url} = '';
		
		foreach (@$locations_array_of_hashrefs)
		{
			my @url_for_individual_location = ('business');
			foreach my $index (@fields_to_build_store_url)
			{
				last if ! $_->{$index};
				push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($_->{$index});
			}
			$_->{store_url} = '/' . join('/', @url_for_individual_location);
			$_->{store_url} = '';
		}
		

		
		
		
		
		

		

		
		my $formatted_master_record_xml = '';
		my $formatted_locations = '';
		
		
		my $merchants_package_name = $MerchantAffiliates->getMerchantPackageNameById($merchant_master_record_hashref->{discount_type});
		my $merchant_type = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record_hashref->{discount_type});
		
		# This is populated if we will be allowing them to add locations.
		#TODO: When we have a way of distinguishing the difference between offline, and online stores change this.
		
		my $add_locations = ($merchant_master_record_hashref->{status} == 1 && $merchants_package_name && $merchant_type && exists($application_configuration->{number_of_locations}->{$merchant_type}->{$merchants_package_name}) && scalar(@$locations_array_of_hashrefs) < $application_configuration->{number_of_locations}->{$merchant_type}->{$merchants_package_name}) ? '<more_locations>1</more_locations>':'';
		
		#Remove the undef elements from the $merchant_master_record_hashref, "undef"s will trigger warnings.
		foreach (keys %$merchant_master_record_hashref)
		{
			delete $merchant_master_record_hashref->{$_} if ! defined $merchant_master_record_hashref->{$_};
		}
		
		$formatted_master_record_xml = XML::Simple::XMLout($merchant_master_record_hashref, NoAttr=>1, RootName=>'master');
		
		$formatted_locations = XML::Simple::XMLout({location=>$locations_array_of_hashrefs}, NoAttr=>1, RootName=>'locations');
		
		
		my $xml_to_output =<<EOT;

		<base>
		$formatted_master_record_xml
		$formatted_locations
		$add_locations
		$xml
		</base>

EOT


		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $xml_to_output);

#		print $CGI->header('text/plain');
#		print $xml_to_output;
#		print "\n\n";
#		use Data::Dumper;
#		print Dumper($locations_array_of_hashrefs);
	
	};
	if($@)
	{
			my %email = 
			(
				from=>'errors@dhs-club.com',
				to=>'errors@dhs-club.com',
				subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
				text=>"Put some error messages here.
						$@
					  "
			);
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%email);
	}
	
}
elsif($cookie->{ma_info}->{membertype} eq 'mal')
{
	print $CGI->redirect(-uri=>"/cgi/maf/profile/location.cgi?id=$cookie->{ma_info}->{id}");
}
else
{
	displayUnavailable();
}
	
	
exitScript();

__END__

=head1
SEE ALSO

	FindBin, DHSGlobals, CGI, DB, DB_Connect, G_S, XML::local_utils, XML::Simple, CGI, CGI::Cookie, MerchantAffiliates, MailTools

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


