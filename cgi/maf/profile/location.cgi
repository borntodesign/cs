#!/usr/bin/perl -w

=head1 NAME:
maf::profile::location.cgi

	This script is the display for the Merchants
	Location Profile Update screen.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Retrieve a single location for a merchant, and display
	the information so they can update it.  When the form
	is submitted it is passed to "maf/profile/location_submit.cgi"
	
	If the submit page finds invalid data, or errors the submitted
	form information is passed back, in "CGI::param('data')", with
	error messages.  This information is encoded using 
	"DHS::UrlSafe::encodeHashOrArray()".  If "CGI::param('data')"
	holds data, the merchants current information in the database 
	is not retrieved. 

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict
	FindBin
	DB
	DB_Connect
	G_S
	XML::local_utils
	XML::Simple
	CGI
	CGI::Cookie
	MerchantAffiliates
	MailTools
	DHS::UrlSafe

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DB;
use DB_Connect;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI;
use CGI::Cookie;
use MerchantAffiliates;
use MailTools;
use DHS::UrlSafe;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
	
	$db
	$CGI
	$G_S
	$MerchantAffiliates
	$DHS_UrlSafe

=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $G_S = G_S->new();
my $MerchantAffiliates = {};
my $DHS_UrlSafe = DHS::UrlSafe->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.
	
	$xml	SCALAR
	$xsl	SCALAR
	$configuration_xml	SCALAR
	$countries_xml	SCALAR
	$application_configuration	HASHREF
	merchant_location_record_hashref	HASHREF
	$errors	HASHREF
	$warnings	HASHREF
	$notices	HASHREF
	$errors_xml	SCALAR
	$warnings_xml	SCALAR
	$notices_xml	SCALAR
	$params	SCALAR
	$information_hashref_arrayref	HASHREF
	$cookie	HASHREF
	$location_id	SCALAR
	$ok_to_add_new_location BOOL
	$view_password	SCALAR

=cut

my $xml = '';
my $xsl = '';
my $configuration_xml = '';
my $countries_xml = '';
my $provinces_xml = '';
my $application_configuration = {};
my $merchant_location_record_hashref = {};
my $errors = {};
my $warnings = {};
my $notices = {};

my $errors_xml = '';
my $warnings_xml = '';
my $notices_xml = '';
my $params = '';
my $information_hashref_arrayref = {};
my $cookie = {};
my $location_id = '';
my $ok_to_add_new_location = 1;
my $view_password = '';

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayUnavailable

=head3
Description:

	The browser is sent a redirect to "/maf/profile/unavailable"
	then exitScript() is called.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub displayUnavailable
{
	print $CGI->redirect(-uri=>'/maf/profile/unavailable');
	exitScript();
}

=head2
displayUpdate

=head3
Description:

	The browser is sent a redirect to "/cgi/maf/profile/update.cgi"
	then exitScript() is called.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub displayUpdate
{
	print $CGI->redirect(-uri=>'/cgi/maf/profile/update.cgi');
	exitScript();
}


####################################################
# Main
####################################################
unless ($db = DB_Connect('merchant_affiliates')){exit}
$db->{RaiseError} = 1;


#			Master Cookie
#			$VAR1 = {
#			          'ma_info' => {
#			                         'membertype' => 'maf',
#			                         'id' => '41',
#			                         'maflocations' => '9995,9755,9986,9900,10217,9797,9735',
#			                         'mafname' => 'The DHS Club, Inc.',
#			                         'username' => 'bill'
#			                       },
#			          'login_id' => '41'
#			        };
#
#
#		Location Cookie
#		$VAR1 = {
#		          'ma_info' => {
#		                         'membertype' => 'mal',
#		                         'id' => '9735',
#		                         'maflocations' => '9735',
#		                         'mafname' => 'The DHS Club, Inc.',
#		                         'username' => 'bill'
#		                       },
#		          'login_id' => '9735'
#		        };

eval{
	
#if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 11272009)
#{
	$xml = G_S::Get_Object($db, 10726);
	$xsl = G_S::Get_Object($db, 10727);
	
#	$configuration_xml = G_S::Get_Object($db, 10681);
#	
#} else {
#	$xml = G_S::Get_Object($db, 10717);
#	$xsl = G_S::Get_Object($db, 10718);
#	
#	$configuration_xml = G_S::Get_Object($db, 10677);
#}

	$countries_xml = G_S::Get_Object($db, 10407);



	$MerchantAffiliates = MerchantAffiliates->new($db);
	
	$configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();

	$application_configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");
	
	$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	
	# Assign the information the merchant submitted to the "$merchant_information" hash.
	
	if(! $CGI->param('data'))
	{
		foreach my $key ($CGI->param())
		{
			$merchant_location_record_hashref->{$key} = $CGI->param($key) if $key !~ /submit|^_|^x$|^y$|^\s*$/i;
		}
		
	} else {
		$merchant_location_record_hashref = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data')) if $CGI->param('data');
	}
	

	
	if(exists $merchant_location_record_hashref->{errors})
	{
		$errors = $merchant_location_record_hashref->{errors};
		delete $merchant_location_record_hashref->{errors};
		
		foreach (keys %$errors)
		{
			$errors->{$_} = [$errors->{$_}];
		}
		
	}
	
	if(exists $merchant_location_record_hashref->{warnings})
	{
		$warnings = $merchant_location_record_hashref->{warnings};
		delete $merchant_location_record_hashref->{warnings};
		
		foreach (keys %$warnings)
		{
			$warnings->{$_} = [$warnings->{$_}];
		}
		
	}
	
	if(exists $merchant_location_record_hashref->{notices})
	{
		$notices = $merchant_location_record_hashref->{notices};
		delete $merchant_location_record_hashref->{notices};
		
		foreach (keys %$notices)
		{
			$notices->{$_} = [$notices->{$_}];
		}
		
	}
	
	#Check to see if the merchant_id was passed, if it wasn't this should be the first request of this form.
	if(!$merchant_location_record_hashref->{merchant_id} && $merchant_location_record_hashref->{id})
	{
		#If the merchant has logged in with only a location id only retrieve that location information
		$location_id = ($cookie->{ma_info}->{membertype} eq 'mal') ? $cookie->{ma_info}->{id}: $merchant_location_record_hashref->{id};
		
		$merchant_location_record_hashref = $MerchantAffiliates->retrieveMerchantLocationByLocationID($location_id);
	}
	


	$merchant_location_record_hashref->{action} = $CGI->url();
	$merchant_location_record_hashref->{action} =~ s/\.cgi/_submit\.cgi/;
	
	
};
if($@)
{
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
}

#If the Merchant is logged in with a location cookie, and they don't have access to the requested store, or if they are requesting a new location give them the unavailable page.
displayUnavailable() if ($cookie->{ma_info}->{membertype} eq 'mal' && (! $location_id  || $cookie->{ma_info}->{id} ne $merchant_location_record_hashref->{id}));

#If the Merchant is logged in with the master credentials, the location_id is set, and the requested location is not one of theirs give them the unavailable page.
displayUnavailable() if ($cookie->{ma_info}->{membertype} eq 'maf' && $location_id && $cookie->{ma_info}->{id} ne $merchant_location_record_hashref->{merchant_id});


eval{
	
	if( ! $merchant_location_record_hashref->{discount} || ! $merchant_location_record_hashref->{discount_type} || ! $merchant_location_record_hashref->{status})
	{
	
		my $merchant_id = ($cookie->{ma_info}->{membertype} eq 'maf') ? $cookie->{ma_info}->{id} : $merchant_location_record_hashref->{merchant_id};
		my $merchant_master_record = $MerchantAffiliates->retrieveMerchantMasterRecordByID($merchant_id);

		if( ! $merchant_location_record_hashref->{discount} || ! $merchant_location_record_hashref->{discount_type})
		{
			$merchant_location_record_hashref->{merchant_id} = $merchant_master_record->{id};
			$merchant_location_record_hashref->{location_name} = $merchant_master_record->{business_name};
			$merchant_location_record_hashref->{notes} = $merchant_master_record->{blurb};
			$merchant_location_record_hashref->{url} = $merchant_master_record->{url};
			$merchant_location_record_hashref->{discount} = $merchant_master_record->{discount} ? $merchant_master_record->{discount}: 0.00;
			$merchant_location_record_hashref->{discount_type} = $merchant_master_record->{discount_type};
			
		}
		
		$merchant_location_record_hashref->{status} = ($merchant_master_record->{status} == 1) ? $merchant_master_record->{status}:0;
		

		if (! $location_id && $cookie->{ma_info}->{membertype} eq 'maf')
		{
			my $locations_array_of_hashrefs = $MerchantAffiliates->retrieveAllMerchantLocationsByMerchantID($cookie->{ma_info}->{id},1);
			my $merchants_package_name = $MerchantAffiliates->getMerchantPackageNameById($merchant_master_record->{discount_type});
			my $merchant_type = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record->{discount_type});
			
			$ok_to_add_new_location = (scalar(@$locations_array_of_hashrefs) < $application_configuration->{number_of_locations}->{$merchant_type}->{$merchants_package_name}) ? 1:0;
			
			$merchant_location_record_hashref->{coupon} = $locations_array_of_hashrefs->[0]->{coupon};
		}
	
	
	}
};
if($@)
{
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		text=>"Put some error messages here.
				$@
			  "
	);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	
}

#TODO: add an error message for the user when the Update page is displayed.
displayUpdate() if ! $ok_to_add_new_location;





	
	#Make the information XML friendly
	foreach (keys %$merchant_location_record_hashref)
	{
		next if (! defined $merchant_location_record_hashref->{$_} || ! $_);
			
		$information_hashref_arrayref->{$_} = [$merchant_location_record_hashref->{$_}];
	}
	
	$params = XML::Simple::XMLout($information_hashref_arrayref, RootName => "params" );
	
	
	if ($errors)
	{
		$errors_xml = XML::Simple::XMLout($errors, RootName => "errors" );
	}
	
	$notices_xml = XML::Simple::XMLout($notices, RootName => "notices" ) if $notices;
	
	$view_password = '<view_password>1</view_password>' if($cookie->{ma_info}->{membertype} eq 'maf');
	
	my	$output_xml = <<EOT;
		<base>
			
			<menus>
				$countries_xml
				$provinces_xml
				$configuration_xml
			</menus>
			
			<data>
				$errors_xml
				$warnings_xml
				$notices_xml
				$view_password
			</data>
		
			$params
			
			$xml
			
		</base>
		
EOT
	
	
#			print $CGI->header('text/plain');
#			print $output_xml;
#			exitScript();
	
	
	
	eval{
		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $output_xml);
		
#		print $CGI->header('text/plain');
#		print $output_xml;
		
	};
	if($@)
	{
		
		my %email = 
		(
			to=>'errors@dhs-club.com',
			subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
			text=>"Put some error messages here.
					$@
				  "
		);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
		
	}
	


exitScript();

__END__

=head1
ASSOCIATED SCRIPTS

L<maf::profile::location_submit>

=head1
SEE ALSO

strict, FindBin, DB, DB_Connect, G_S, XML::local_utils, XML::Simple, CGI, CGI::Cookie, MerchantAffiliates, MailTools, DHS::UrlSafe
	
	
=head1
CHANGE LOG

	Date	Name	Description 
						

=cut


