#!/usr/bin/perl

=head1 NAME:
maf::profile::location_submit.cgi

	Validate then process the information form the location form, maf::profile::location.cgi.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Validate then process the information form the location form, maf::profile::location.cgi.
	If the the information is not valid, the formdata, along with error messages are encoded
	placed in the "data" param then the user is redirected back to the location 
	form, maf::profile::location.cgi.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict
	warnings
	FindBin
	DHSGlobals
	CGI
	DB
	DB_Connect
	G_S
	URI::Escape
	MailTools
	MerchantAffiliates
	DHS::UrlSafe

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use URI::Escape;
use MailTools;
use MerchantAffiliates;
use DHS::UrlSafe;


#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
	
	$db
	$CGI
	$G_S
	$MerchantAffiliates
	$DHS_UrlSafe

=cut

#our $ObjectName = SomeClass->new();
our $db;
my $CGI = CGI->new();
my $G_S = G_S->new();
my $MerchantAffiliates = {};
my $DHS_UrlSafe = DHS::UrlSafe->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.
	
	%merchant_location_info	HASH
	$current_location_info	HASHREF
	$cookie	HASHREF
	@notices	ARRAY
	@warnings	ARRAY
	@errors	ARRAY
	$merchant_location_id	SCALAR
	$merchant_status	SCALAR
	$url	SCALAR

=cut

my %merchant_location_info = ();
my $current_location_info = {};
my $cookie = {};
my @notices = ();
my @warnings = ();
my @errors = ();
my $merchant_location_id = 0;
my $merchant_status = 0;
my $url = $CGI->url();
$url =~ s/_submit\.cgi/\.cgi/;



=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayUnavailable

=head3
Description:

	Redirect the user to the unavailable page.

=head3
Params:

	none

=head3
Returns:

	none

=cut

sub displayUnavailable
{
	print $CGI->redirect(-uri=>'/maf/profile/unavailable');
	exitScript();
}

####################################################
# Main
####################################################
unless ($db = DB_Connect('merchant_affiliates')){exit}
$db->{RaiseError} = 1;




	
	
	
eval{
	
	#First well extract the information, so if an exception is thrown we return to the input form with the information the user submited.
	$merchant_location_id = $CGI->param('id') if $CGI->param('id');
	
	$merchant_location_info{address1} = $CGI->param('address1') if $CGI->param('address1');
	$merchant_location_info{address2} = $CGI->param('address2') if $CGI->param('address2');
	$merchant_location_info{address3} = $CGI->param('address3') if $CGI->param('address3');
	$merchant_location_info{city} = $CGI->param('city') if $CGI->param('city');
	$merchant_location_info{state} = $CGI->param('state') if $CGI->param('state');
	$merchant_location_info{country} = $CGI->param('country') if $CGI->param('country');
	$merchant_location_info{postalcode} = $CGI->param('postalcode') if $CGI->param('postalcode');
        $merchant_location_info{longitude} = $CGI->param('longitude') if $CGI->param('longitude');
        $merchant_location_info{latitude} = $CGI->param('latitude') if $CGI->param('latitude');
	$merchant_location_info{phone} = $CGI->param('phone') if $CGI->param('phone');
	($merchant_location_info{username} = $CGI->param('username')) =~ s/\s//g if $CGI->param('username');# A username is required
	($merchant_location_info{password} = $CGI->param('password')) =~ s/\s//g if $CGI->param('password');# A password is required
	
	$merchant_location_info{scanner} = $CGI->param('scanner') ? $CGI->param('scanner'): '';
	
	$merchant_location_info{operator} = $CGI->cookie('operator') ? $CGI->cookie('operator') : 'cgi';
	
	$merchant_location_info{notes} = $CGI->param('notes') if $CGI->param('notes');
	
	#Most of these are passed from hidden fields on the update page.
	$merchant_location_info{merchant_id} = $CGI->param('merchant_id'); #This one is alread checked above.
	$merchant_location_info{location_name} = $CGI->param('location_name');
  	
	$merchant_location_info{url} = $CGI->param('url');
	$merchant_location_info{discount} = $CGI->param('discount');
	$merchant_location_info{discount_type} = $CGI->param('discount_type');
	$merchant_location_info{coupon} = $CGI->param('coupon');
		
	#If the $merchant_location_id is not set this is a new location, let's get the current status of the marchant to use when we create the location record.
	$merchant_status = $CGI->param('status') if ! $merchant_location_id;
		
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	
	$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	#TODO: Check for required fields, and validate the needed fields	
	$merchant_location_info{errors}->{username} = 'The username is a required field.' if ! $merchant_location_info{username};
	$merchant_location_info{errors}->{password} = 'The password is a required field.' if ! $merchant_location_info{password};
	
	$merchant_location_info{errors}->{address1} = 'The Address is a required field.' if ! $merchant_location_info{address1};
	$merchant_location_info{errors}->{city} = 'The City is a required field.' if ! $merchant_location_info{city};
	$merchant_location_info{errors}->{state} = 'The State is a required field.' if ! $merchant_location_info{state};
	$merchant_location_info{errors}->{country} = 'The Country is a required field.' if ! $merchant_location_info{country};
	
	$merchant_location_info{errors}->{phone} = 'The Phone is a required field.' if ! $merchant_location_info{phone};
	
	
	$current_location_info = $MerchantAffiliates->retrieveMerchantLocationByLocationID($merchant_location_id) if $merchant_location_id;
	
};
if($@)
{
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
	);
	
	#TODO: Add some more meaningful information here;
	
	$email{text} = $merchant_location_info{errors}->{script_error} = $@;
	
	$merchant_location_info{id} = $merchant_location_id if $merchant_location_id;
	$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%merchant_location_info);
	print $CGI->redirect(-uri=>"$url");
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	

	exitScript();
		
}


#TODO: Instead of using "displayUnavailable()" 
#If the Merchant is logged in with a location cookie, and they don't have access to the location, or if they are requesting to create a new location give them the unavailable page.
displayUnavailable() if ($cookie->{ma_info}->{membertype} eq 'mal' && (! $CGI->param('id')  || $cookie->{ma_info}->{id} ne $CGI->param('id')));

#If the Merchant is logged in with the master credentials, the id is set, and the requested location is not one of theirs give them the unavailable page.
displayUnavailable() if ($cookie->{ma_info}->{membertype} eq 'maf' && $cookie->{ma_info}->{id} ne $CGI->param('merchant_id') );

displayUnavailable() if ($cookie->{ma_info}->{membertype} eq 'maf' && $CGI->param('id') &&  $current_location_info->{id} ne $CGI->param('id'));


#If there were any errors, just send them back to the input form.
if (exists $merchant_location_info{errors})
{
	$merchant_location_info{id} = $merchant_location_id if $merchant_location_id;
	$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%merchant_location_info);
	print $CGI->redirect(-uri=>"$url");
	exitScript();
}



eval{
		
	if ($merchant_location_id)
	{

		#There's probably no need to remove this, but there's also no need to update it.
#		delete $merchant_location_info{merchant_id};
#		delete $merchant_location_info{location_name};
#  		
#		delete $merchant_location_info{url};
#		delete $merchant_location_info{discount};
#		delete $merchant_location_info{discount_type};
		
		
		my $stuff = $MerchantAffiliates->updateMerchantLocationInformation($merchant_location_id, \%merchant_location_info);#Add a check for updated
		
	}
	else
	{
		$merchant_location_id = $MerchantAffiliates->createMerchantLocationInformation(\%merchant_location_info,$merchant_status);
		
	}
	
	
	
	
	
		
};
if($@)
{
	
	my %email = 
	(
		from=>'errors@dhs-club.com',
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
	);
	
	#TODO: Add some more meaningful information here;
	
	$email{text} = $merchant_location_info{errors}->{script_error} = $@;
	
	$merchant_location_info{id} = $merchant_location_id if $merchant_location_id;
	$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%merchant_location_info);
	print $CGI->redirect(-uri=>"$url");
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	

	exitScript();
		
}

	%merchant_location_info = (id=>$merchant_location_id, notices=>{updated=>'Updated'});
	$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%merchant_location_info);
	print $CGI->redirect(-uri=>"$url");
	
#	$url .= "?id=$merchant_location_id" if $merchant_location_id;
#	print $CGI->redirect(-uri=>"$url");
	
	

exitScript();

__END__

=head1
ASSOCIATED SCRIPTS

L<maf::profile::location>

=head1
SEE ALSO

strict, FindBin, DHSGlobals, CGI, DB, DB_Connect, G_S, URI::Escape, MailTools, MerchantAffiliates, DHS::UrlSafe


=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


