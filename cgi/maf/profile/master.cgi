#!/usr/bin/perl

=head1 NAME:
maf::profile::master.cgi

	This script is used to update the Merchants Master Profile information.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	This script is used to update the Merchants Master Profile information.  
	When the master profile information is updated their primary location 
	is updated as well.

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict
	warnings
	FindBin
	lib
	DB
	DB_Connect
	G_S
	XML::local_utils
	XML::Simple
	CGI
	CGI::Cookie
	File::Copy
	Image::Size
	MerchantAffiliates

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
#use lib "$FindBin::Bin/../../../cgi-lib";
use lib '/home/httpd/cgi-lib';
use DB;
use DB_Connect;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI;
#use CGI::Cookie;
use CGI::Carp qw(fatalsToBrowser);
use MerchantAffiliates;
use File::Copy;
require Image::Size;
use MailTools;
use DHS::UrlSafe;
use Locations;

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
	
	$db
	$CGI
	$MerchantAffiliates
	$DHS_UrlSafe

=cut

our $db;
our $CGI = CGI->new();
our $G_S = G_S->new({'db'=>\$db, 'CGI'=>$CGI});
our $MerchantAffiliates = {};
our $DHS_UrlSafe = DHS::UrlSafe->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.
	
	$xml	scalar
	$xsl	scalar
	$configuration_xml
	$langauges_xml	scalar
	$languages_ref	hashref
	$countries_xml	scalar

=cut

my %TMPL = (
	'countries_xml' => 10407,
	'xml' => 10694,
	'xsl' => 10695,
	'configuration_xml' => 10681
);
our $xml = '';
our $xsl = '';
our $configuration_xml = '';
our $langauges_xml = '';
our $languages_ref = {};
our $countries_xml = '';
our $provinces_xml = '';

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayApplication

=head3
Description:

	Here we display the application


=head3
Params:

	merchant_master_record_hashref
	HASHREF
	The merchant data, where the index is named the same 
	as the form element it populates. If no hashref is 
	passed in the merchants current information is retrieved 
	From the database.
	
	errors
	HASHREF
	Error Messages where the index is named for the node in the 
	language document.

=head3
Returns:

	none

=cut

sub displayApplication
{
	my $merchant_master_record_hashref = shift;
	my $missing_fields = shift;
	
	
	my $forwarded_messages = {};
	$forwarded_messages = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data')) if $CGI->param('data');
	
	#Yes I know this isn't pretty, but once it was decided to combine the keywords with this screen it became a hack.
	#At some point this screen should be broken into two scripts.
	foreach (keys %$missing_fields)
	{
		$forwarded_messages->{'errors'}->{$_} = $missing_fields->{$_};
	}

	my $merchant_keyword_record_hashref = {};
	
	my $errors_xml = '';
	my $warnings_xml = '';
	my $notices_xml = '';
	
	my $application_configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	my $merchant_master_vendor_information_hashref = {};
	
	
	$countries_xml = $G_S->GetObject($TMPL{'countries_xml'});
	
	$languages_ref = $db->selectall_arrayref("SELECT '<' || code2 || '>' ||  description || '</' || code2 || '>'  FROM language_codes WHERE active = true ORDER BY description");
	
	foreach (@$languages_ref)
	{
	   $langauges_xml .= $_->[0];
	}
	
	$langauges_xml = "<language>$langauges_xml</language>";
	
	
	my $params = '';

	my $information_hashref_arrayref = {};
	
	my $cookie = {};
	
	
		eval{
			$MerchantAffiliates = MerchantAffiliates->new($db);
			
			$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			
			displayUnavailable() if $cookie->{'ma_info'}->{membertype} eq 'mal';
			
		};
		if($@)
		{
			#TODO: Come up with something better for a fatal error.
			my %email = 
			(
				from=>'errors@dhs-club.com',
				to=>'errors@dhs-club.com',
				subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
				text=>$@
			);
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%email);
			
			$forwarded_messages->{'errors'}->{'script_error'} = 1;
			
#			print $CGI->header('text/plain');
#			print $email{'text'};
#			exitScript();
		}
	

	
	# If there was no information passed as a parameter into this sub for "merchant_master_record_hashref"
	# retrieve the merchant master record, member information, and the master location vendor info.
	if (! $merchant_master_record_hashref)
	{
		my $merchant_member_info = {};

	
		
		eval{
			
			
			$merchant_master_record_hashref = $MerchantAffiliates->getMerchantMaster({id=>$cookie->{'ma_info'}->{id}});
			
			$merchant_master_record_hashref = $merchant_master_record_hashref->[0];
			
			$MerchantAffiliates->setMembers() if ! $MerchantAffiliates->{Members};
			
			$merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($cookie->{'ma_info'}->{id});
			
			$merchant_member_info = $MerchantAffiliates->{Members}->getMemberInformationByMemberID({member_id => $merchant_master_record_hashref->{member_id}});
			
			
		};
		if($@)
		{
			#TODO: Come up with something better for a fatal error.
			my %email = 
			(
				from=>'errors@dhs-club.com',
				to=>'errors@dhs-club.com',
				subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
				text=>$@
			);
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%email);
			
			$forwarded_messages->{'errors'}->{'script_error'} = 1
#			print $CGI->header('text/plain');
#			print $email{'text'};
#			exitScript();
		}




		####################################################################################################################
		# Here we process field specific information, so it works with the common.xsl Style Sheet.
		####################################################################################################################
		
		if($merchant_master_record_hashref->{url_language_pref})
		{
			$information_hashref_arrayref->{url_language_pref} = lc($merchant_master_record_hashref->{url_language_pref});
			$information_hashref_arrayref->{url_language_pref} = [$information_hashref_arrayref->{url_language_pref}];
		}
		
		$merchant_master_record_hashref->{discount} =~ s/0*$// if (exists $merchant_master_record_hashref->{discount} && $merchant_master_record_hashref->{discount}); 
		
#		foreach (keys %{$application_configuration->{exception_percentage_of_sale}})
#		{
#			
#			if ($merchant_master_record_hashref->{discount} eq $application_configuration->{exception_percentage_of_sale}->{$_}->{val})
#			{
#				$information_hashref_arrayref->{exception_percentage_of_sale} = [$merchant_master_record_hashref->{discount}];
#				last;
#			}
#			
#		}
#		
#		$information_hashref_arrayref->{percentage_of_sale} = [$merchant_master_record_hashref->{discount}] if (! defined $information_hashref_arrayref->{exception_percentage_of_sale});
		

		$information_hashref_arrayref->{language} = lc($merchant_member_info->{language_pref});
			
		$information_hashref_arrayref->{language} = [$information_hashref_arrayref->{language}];
			
		$information_hashref_arrayref->{home_phone} = [$merchant_member_info->{phone}];
		
		$information_hashref_arrayref->{tin} = [$merchant_member_info->{tin}];
		
		
		#$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?', undef, ($merchant_master_record_hashref->{business_type}));
		my $lp = $G_S->Get_LangPref();
		$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($lp, $merchant_master_record_hashref->{business_type}));
		
		$merchant_master_vendor_information_hashref->{coupon} =~ s/>/\/>/ if (exists $merchant_master_vendor_information_hashref->{coupon} && $merchant_master_vendor_information_hashref->{coupon} && $merchant_master_vendor_information_hashref->{coupon} !~ /\/\s*>/);
		
		$information_hashref_arrayref->{blurb} = [$merchant_master_vendor_information_hashref->{blurb}];
		
		####################################################################################################################
		
		
	}
	
	#TODO: Come up with something better for fatal errors.
	eval{
		
		$merchant_keyword_record_hashref->{'keywords'} = $MerchantAffiliates->retrieveMerchantKeywordsByID($cookie->{'ma_info'}->{id});
		
		if ($merchant_master_record_hashref->{'country'})
		{
		
			my $Locations = Locations->new($db);
			$provinces_xml = $Locations->getXMLProvincesByCountryCode({country_code => $merchant_master_record_hashref->{'country'}});
		
		}
		
		
	};
	if($@)
	{
		
		use Data::Dumper;
		
		my %email = 
		(
			from=>'errors@dhs-club.com',
			to=>'errors@dhs-club.com',
			subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript"
		);
		
		$email{text} = "There was a fatal error retrieving the merchants Key words, or the province list for the merchant. " . Dumper($merchant_master_record_hashref) . "\n\nError: " . $@;
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
		
		$forwarded_messages->{'errors'}->{'script_error'} = 1;
		
#		print $CGI->header('text/plain');
#		print $email{'text'};
#		exitScript();
			
	}
	
	#Make the information XML friendly

	foreach (keys %$merchant_master_record_hashref)
	{
		next if (! defined $merchant_master_record_hashref->{$_} || !$_ || $_ eq 'keywords');
			
		$information_hashref_arrayref->{$_} = [$merchant_master_record_hashref->{$_}];
	}
	
	$information_hashref_arrayref->{'keywords'}->{'keyword'} = $merchant_keyword_record_hashref->{'keywords'} if ref $merchant_keyword_record_hashref->{'keywords'} eq 'ARRAY';
		
	#$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?', undef, ($merchant_master_record_hashref->{business_type}));
	my $lp = $G_S->Get_LangPref();
	$information_hashref_arrayref->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($lp, $merchant_master_record_hashref->{business_type}));
        $information_hashref_arrayref->{status} = [$merchant_master_record_hashref->{status}];	
	
	$params = XML::Simple::XMLout($information_hashref_arrayref, RootName => "params" );
	
	
	if ($forwarded_messages->{'errors'})
	{
		
		foreach (keys %{$forwarded_messages->{'errors'}})
		{
			$forwarded_messages->{'errors'}->{$_} = [$forwarded_messages->{'errors'}->{$_}];
		}
		
		$errors_xml = XML::Simple::XMLout($forwarded_messages->{'errors'}, RootName => "errors" );	
	}
	
	if ($forwarded_messages->{'warnings'})
	{
		
		foreach (keys %{$forwarded_messages->{'warnings'}})
		{
			$forwarded_messages->{'warnings'}->{$_} = [$forwarded_messages->{'warnings'}->{$_}];
		}
		
		$warnings_xml = XML::Simple::XMLout($forwarded_messages->{'warnings'}, RootName => "warnings" );
	}

	if ($forwarded_messages->{'notices'})
	{
		foreach (keys %{$forwarded_messages->{'notices'}})
		{
			$forwarded_messages->{'notices'}->{$_} = [$forwarded_messages->{'notices'}->{$_}];
		}
		
		$notices_xml = XML::Simple::XMLout($forwarded_messages->{'notices'}, RootName => "notices" );
	}

	my	$output_xml = <<EOT;

		<base>
		
			<menus>
				$countries_xml
				
				$provinces_xml
				
				$configuration_xml
				
				$langauges_xml
				
			</menus>
			
			<data>
			
				$errors_xml
				$warnings_xml
				$notices_xml
			
			</data>
			
			<banner>
			
				$merchant_master_vendor_information_hashref->{coupon}
			
			</banner>
			
			$params
			
			$xml
		
		</base>

EOT
	
	# for now, May 2012, Will wants to lockout the new online merchants from this interface as it is offline merchant specific
	if ($merchant_master_vendor_information_hashref->{'vendor_group'} eq '6')
	{
		print $CGI->redirect('/maf/profile/onlineMafLockout.html');
		exitScript();
	}
	
	
	eval{

		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $output_xml);
		
#		print $CGI->header('text/plain');
#		print $output_xml;
		
	};
	if($@)
	{
		my $error_message = $@;
		
		print $CGI->header('text/plain');
		print $error_message;
		
		my %error_email = 
						(
							from=>'errors@dhs-club.com',
							to=>'errors@dhs-club.com',
							subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
							text=>"There was an redirecting a merchant to step 3 of the registration from.\n Error Message: \n $error_message ",
						);
				
		$error_email{text} .= "\n output_xml: $output_xml \n";
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%error_email);
		
		#warn $FindBin::RealScript . ' - ' . $@;
		
#		print "\n\n";
#		
#		print $xsl;
#		print "\n\n";
#		print $output_xml;
		
		
	}
	
	exitScript();
}

=head2
processApplication

=head3
Description:

	Here we process the application.  The input
	is parsed, validated, then the appropriate methods
	are called to update the information.  If an error
	occurs the "displayApplicaton" sub is passed the
	updated information, and also the error messages.


=head3
Params:

	None
	The input is taken from "$CGI->param()"

=head3
Returns:

	none

=cut

sub processApplication
{
	
	my %merchant_information = ();
	
	my %missing_fields = ();

	my $MerchantAffiliates = MerchantAffiliates->new($db);
	my $CGI_Upload = {};
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} ); #{};
	my $application_configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	my %merchant_banner_info = ('location' => $application_configuration->{'banner'}->{'location'});
	my @merchant_keywords = ();
	my %messages_for_forwarding = ();
	my $merchant_master_record_hashref = {};


	# for now, May 2012, Will wants to lockout the new online merchants from this interface as it is offline merchant specific
	my $merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($cookie->{'ma_info'}->{id});
	if ($merchant_master_vendor_information_hashref->{'vendor_group'} eq '6')
	{
		print $CGI->redirect('/maf/profile/onlineMafLockout.html');
		exitScript();
	}

	# Assign the information the merchant submitted to the "$merchant_information" hash.
	foreach my $key ($CGI->param())
	{
		next if $key =~ /submit|biz_description/i;
		#$CGI->param($key) &&
		$merchant_information{$key} = $CGI->param($key) if $key !~ /^_|^x$|^y$/;
	}

	eval
	{
#			$MerchantAffiliates = MerchantAffiliates->new($db);
#			$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			$merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{id});
			
			
			$merchant_information{'discount'} = $merchant_master_record_hashref->{'discount'} ? $merchant_master_record_hashref->{'discount'}: 0.00;
			$merchant_information{'cap'} = $merchant_master_record_hashref->{'cap'};
			$merchant_information{'discount_type'} = $merchant_master_record_hashref->{'discount_type'};
			
			
			#TODO: Think about how to handle pre-existing email addresses.
			$MerchantAffiliates->setMembers()  if ! $MerchantAffiliates->{Members};
			
			%merchant_banner_info = ('location' => $application_configuration->{'banner'}->{'location'});
			
			#																			Y										N																																																																																																																																														Y																																																																																																																																													
			$missing_fields{emailaddress1_preexisting_update} = ['Missing Field'] if ($merchant_information{emailaddress1} && ! $MerchantAffiliates->{Members}->checkEmailDuplicates($merchant_information{emailaddress1}, $merchant_master_record_hashref->{member_id}) && ! $MerchantAffiliates->{Members}->checkEmailDomain($merchant_information{emailaddress1}));
			
			$missing_fields{emailaddress2_preexisting_update} = ['Missing Field'] if ($merchant_information{emailaddress2} && ! $MerchantAffiliates->{Members}->checkEmailDuplicates($merchant_information{emailaddress2}, $merchant_master_record_hashref->{member_id}) && ! $MerchantAffiliates->{Members}->checkEmailDomain($merchant_information{emailaddress2}));
			
			if ($merchant_information{emailaddress1})
			{
	 			$missing_fields{emailaddress1_preexisting_update} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress1}, 1));
			}
			
			if($merchant_information{emailaddress2})
			{
 				$missing_fields{emailaddress2_preexisting_update} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress2}, 1));
			}
			
			$missing_fields{emailaddress_should_not_match} = ['Missing Field'] if ($merchant_information{emailaddress1} && $merchant_information{emailaddress2} && $merchant_information{emailaddress1} eq $merchant_information{emailaddress2});
			
			
#			if($merchant_master_record_hashref->{'discount_type'} < 1 && ! $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#			}
#			elsif(! $merchant_information{exception_percentage_of_sale} && ! $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#			}
#			elsif($merchant_information{exception_percentage_of_sale} && $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale_not_both} = ['Missing Field'];
#			}
			
			#TODO: Make an exception for the current individual
			if (defined $merchant_information{'username'})
			{
				$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingUsernameDuplicate($merchant_information{'username'}));
				$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkMasterUsernameDuplicates($merchant_information{'username'}));
				$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkLocationUsernameDuplicates($merchant_information{'username'}));
			}
			
			if (exists $merchant_information{'banner'} && $merchant_information{'banner'})
			{
				
				$merchant_banner_info{temp_banner_file} = $CGI->tmpFileName($merchant_information{'banner'});
				
				$merchant_banner_info{light_weitht_file_handler} = $CGI->upload('banner');
				
				($merchant_banner_info{width}, $merchant_banner_info{height}, $merchant_banner_info{mime_type}) = Image::Size::imgsize($merchant_banner_info{light_weitht_file_handler});
				
				$missing_fields{banner_wrong_file_type} = ['Missing Field'] if $merchant_banner_info{mime_type} !~ /GIF|JPEG|JPG|PNG/i;
				$missing_fields{banner_wrong_file_width} = ['Missing Field'] if $merchant_banner_info{width} > $application_configuration->{'banner'}->{'max_width'};
				$missing_fields{banner_wrong_file_height} = ['Missing Field'] if $merchant_banner_info{height} > $application_configuration->{'banner'}->{'max_height'};
				$missing_fields{banner_size_to_large} = ['Missing Field'] if (-s $merchant_banner_info{temp_banner_file} > (1024 * $application_configuration->{'banner'}->{'max_size'}));
				
			}
			
			
#			warn 'if(exists $merchant_information{'keywords'} && $merchant_information{'keywords'})' . "\n";
#			warn 'if(' . exists($merchant_information{'keywords'}) . ' && ' . $merchant_information{'keywords'} . ")\n\n";
			#TODO: Add keyword processing here.
			if(exists($merchant_information{'keywords'}) && $merchant_information{'keywords'})
			{
				
				#warn 'Entered if(exists($merchant_information{'keywords'}) && $merchant_information{'keywords'})';
				
				#TODO: Determin if this is wanted.  "$original_keyword_submission" is used for emailing errors.
				#$original_keyword_submission = $merchant_information{'keywords'};
				
				$merchant_information{'keywords'}  =~ s/\s+/\n/g;
				
				$merchant_information{'keywords'} =~ s/\s+$//;
				
				my @temp_merchant_keywords = split(/\n/,$merchant_information{'keywords'});
				
				
				# $something = $merchant_information{'keywords'};
				# $application_configuration
				#$merchant_master_record_hashref
				$merchant_master_record_hashref->{'merchant_type'} = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record_hashref->{'discount_type'});				
				my $merchants_package_name = $MerchantAffiliates->getMerchantPackageNameById($merchant_master_record_hashref->{'discount_type'});
				my $max_number_of_keywords = $application_configuration->{'number_of_keywords'}->{$merchant_master_record_hashref->{'merchant_type'}}->{$merchants_package_name};
				
				
				#TODO: When we are using the database cache set length to be that of what the length of the keyword field is.
				my $length = $MerchantAffiliates->getKeywordLength();
				
				#warn "\n max_number_of_keywords: $max_number_of_keywords\n";
				
				foreach (@temp_merchant_keywords)
				{
					chomp($_);
					
					next if ! $_;
					
					
					#TODO: add a check here for the maximum number of keywords, if they have reached their maximum call "last".
					if (scalar(@merchant_keywords) >= $max_number_of_keywords)
					{
						$messages_for_forwarding{'notices'}->{max_number_of_keywords_exceeded} = 'The maximum number of keywords for your package has been exceeded.';
						last;
					}
					
					my $original_value = $_;
					
					$original_value =~ s/\s+$//;
					$_ =~ s/\s+$//;
					$_ =~ s/;|\(|\)//g;
					
					$original_value =~ s/^\s+//;
					
					$_ =~ s/\s//g;
					$_ = uc($_);
					my $string_length = length($_);
					
					if ($string_length > $length)
					{
						my $characters_to_remove = $string_length - $length;
						$_ = substr($_, 0, - $characters_to_remove);
					}
					
					if (length($original_value) != length($_))
					{
						if(exists $messages_for_forwarding{'warnings'}->{modified_keywords})
						{
							$messages_for_forwarding{'warnings'}->{modified_keywords} .= "$original_value - $_ \n";
						}
						else
						{
							$messages_for_forwarding{'warnings'}->{'keywords'} = "Modified";
							$messages_for_forwarding{'warnings'}->{modified_keywords} = "$original_value - $_ \n";
						}
					}
					
					push @merchant_keywords, $_; 
					
				}

				delete $merchant_information{'keywords'};
			}

			#Check the length
			
			if($merchant_information{'blurb'})
			{
				
				my $merchant_type = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record_hashref->{'discount_type'});
				my $merchant_package = $MerchantAffiliates->getMerchantPackageNameById($merchant_master_record_hashref->{'discount_type'});
				my $length = (exists $application_configuration->{'blurb'}->{'maximum_characters'} && $application_configuration->{'blurb'}->{'maximum_characters'} ) ? $application_configuration->{'blurb'}->{'maximum_characters'}:0;
				
				$length = $application_configuration->{'blurb'}->{'max_characters_per_package'}->{$merchant_type}->{$merchant_package} if (exists $application_configuration->{'blurb'}->{'max_characters_per_package'}->{$merchant_type} && exists $application_configuration->{'blurb'}->{'max_characters_per_package'}->{$merchant_type}->{$merchant_package} &&  $application_configuration->{'blurb'}->{'max_characters_per_package'}->{$merchant_type}->{$merchant_package});
				
				$length++;
				
				my $original_value = $merchant_information{'blurb'};
				
				$original_value =~ s/\s+$//;
				$original_value =~ s/;|\(|\)//g;
				
				$merchant_information{'blurb'} =~ s/\s+$//;
				$merchant_information{'blurb'} =~ s/;|\(|\)//g;
				
				
				my $string_length = length($merchant_information{'blurb'});
				
				if ($string_length > $length)
				{
					my $characters_to_remove = $string_length - $length;
					$merchant_information{'blurb'} = substr($merchant_information{'blurb'}, 0, - $characters_to_remove);
				}
				
				$messages_for_forwarding{'warnings'}->{'blurb'} = "Modified" if (length($original_value) != length($merchant_information{'blurb'}));
				
			}
			
			if(scalar (keys %missing_fields))
			{
#				use Data::Dumper;
#				warn "\n\nmerchant_master_record_hashref:\n" . Dumper($merchant_master_record_hashref) . "\n\n";
#				
#				my $temp = \%merchant_information;
#				warn "\n\nmerchant_information:\n" . Dumper($temp) . "\n\n";
				
				$merchant_information{'discount_type'} = $merchant_master_record_hashref->{'discount_type'};
				
#				if ($merchant_master_record_hashref->{discount} < 0.05)
#				{
#					$merchant_information{exception_percentage_of_sale} = $merchant_master_record_hashref->{discount};
#					$merchant_information{exception_percentage_of_sale} =~ s/0*$//;
#				}
				
			}

		};
		if($@)
		{
			
			my $error_message = $@;
			
			#warn "Error: $FindBin::RealBin / $FindBin::RealScript ::processApplication - There was an error instanciating a Class, or Validating the submitted information for Merchant ID: $cookie->{'ma_info'}->{id}. $error_message";
			
			$missing_fields{'script_error'} = ['Script Error'];
			
			my %email = 
			(
				from=>'errors@dhs-club.com',
				to=>'errors@dhs-club.com',
				subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . "$FindBin::RealScript :: processApplication",
				text=>"There was an error validating the Merchant Master Information for Merchant ID: $cookie->{'ma_info'}->{id}
						$error_message
					  "
			);
		
			use MailTools;
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%email);
	

		}	
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
#		use Data::Dumper;
#		print $CGI->header('text/plain');
#		print Dumper(%missing_fields);
#		exitScript();
		
		#Remove this, it causes an error when the XML XSL translation, the use needs to reupload the banner if there is an error.
		delete $merchant_information{'banner'};
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}
	
	
	eval{
		
		#processing for the merchant banner.
		if($merchant_information{'banner'})
		{
			$merchant_banner_info{mime_type} = lc($merchant_banner_info{mime_type});
			
			die "We can not write to the folder the merchant banners are stored in. $application_configuration->{'banner'}->{'location'} " if(! -W $application_configuration->{'banner'}->{'location'} );
		
			$merchant_banner_info{io_handler} = $merchant_banner_info{light_weitht_file_handler}->handle;
			die 'The banner could not be coppied.' if (! File::Copy::copy($merchant_banner_info{io_handler}, "$application_configuration->{'banner'}->{'location'}ma_$cookie->{'ma_info'}->{id}.$merchant_banner_info{mime_type}"));
				
			$merchant_information{'banner'} = '<img src="' . $application_configuration->{'banner'}->{web_location} . 'ma_' . $cookie->{'ma_info'}->{id} . '.' . $merchant_banner_info{mime_type} . '" width="' . $merchant_banner_info{width} . '" height="' . $merchant_banner_info{height} . '" />';
		}
		
		
		$merchant_information{delete_banner} = "$application_configuration->{'banner'}->{'location'}ma_$cookie->{'ma_info'}->{id}.*" if ($merchant_information{delete_banner});

		
		#TODO: Add keyword processing here.
		if(scalar(@merchant_keywords))
		{
			my $insert_keyword_errors = $MerchantAffiliates->insertMerchantKeywords($cookie->{'ma_info'}->{id}, \@merchant_keywords);
			
			if($insert_keyword_errors->{'delete'} && scalar(@{$insert_keyword_errors->{'delete'}}))
			{
				$messages_for_forwarding{'warnings'}->{'delete_keywords'} = 'There was an error deleteing the following key words.';
				$messages_for_forwarding{'warnings'}->{'non_delete_keywords'} = join(', ', @{$insert_keyword_errors->{'delete'}});
			}
			
			if($insert_keyword_errors->{'insert'} && scalar(@{$insert_keyword_errors->{'insert'}}))
			{
				$messages_for_forwarding{'warnings'}->{'insert_keywords'} = 'There was an error creating the following key words.';
				$messages_for_forwarding{'warnings'}->{'non_inserted_keywords'} = join(', ', @{$insert_keyword_errors->{'insert'}});
				
			}
			
		}
		else
		{
			my $merchants_keywords = $MerchantAffiliates->retrieveMerchantKeywordsByID($cookie->{'ma_info'}->{id});
			$MerchantAffiliates->deleteMerchantKeywordLinks($cookie->{'ma_info'}->{id});
			$MerchantAffiliates->deleteMerchantKeywords($merchants_keywords);
		}
		
		delete $merchant_information{'keywords'} if exists($merchant_information{'keywords'}); # = \@merchant_keywords;
		
		
		my $member_id = undef;
		if($merchant_information{'member_id'} && defined $merchant_information{'tin'})
		{
			$member_id = $merchant_information{'member_id'};
					
		}

		delete $merchant_information{'member_id'};
		
		die " There was an error updating the Master Information for Merchant ID: $cookie->{'ma_info'}->{id} " if ! $MerchantAffiliates->updateMerchantMasterInformation($cookie->{'ma_info'}->{id}, \%merchant_information, $member_id);
		
		#Remove this, it causes an error when the XML XSL translation, the use needs to reupload the banner if there is an error.
		delete $merchant_information{'banner'} if $merchant_information{'banner'};
	};
	if($@)
	{
		
		$missing_fields{'script_error'} = ['Script Error'];
		my $error_message = $@;
		warn $FindBin::RealScript . ' : ' . $error_message;
		#Remove this, it causes an error when the XML XSL translation, the use needs to reupload the banner if there is an error.
		delete $merchant_information{'banner'} if $merchant_information{'banner'};
		
		my %email = 
		(
			from=>'errors@dhs-club.com',
			to=>'errors@dhs-club.com',
			subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
			text=>"There was an error updateing the Merchant Master Information for Merchant ID: $cookie->{'ma_info'}->{id}
					$error_message
				  "
		);
	
		use MailTools;
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
	
	
		#warn "Error updating the info: $email{text}";
	
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
		
	}
	
	$messages_for_forwarding{'notices'}->{updated} = 'Updated';
	$messages_for_forwarding{'notices'}->{update_status} = 1 if $merchant_master_record_hashref->{status} ne '1';
	
	
	
	my $url = $CGI->url() . "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%messages_for_forwarding);
	
	#my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated');
	
	#warn "URL: $url";
	
	print $CGI->redirect('-uri'=>$url);
	exitScript();
}

sub displayUnavailable
{
	
	print $CGI->redirect('-uri'=>'/maf/profile/unavailable');
	exitScript();
	
}
####################################################
# Main
####################################################

unless ($db = DB_Connect('merchant_affiliates')){exit}
$db->{'RaiseError'} = 1;

$xml = $G_S->GetObject($TMPL{'xml'});
$xsl = $G_S->GetObject($TMPL{'xsl'});
	
$configuration_xml = $G_S->GetObject($TMPL{'configuration_xml'});
	
if(! $CGI->param('data') && $CGI->param)
{
	processApplication();
} else {
	displayApplication();
}


exitScript();

__END__

=head1
CHANGE LOG

	2009-08-05
	Keith
	Fixed a bug in the "processApplication" function.  
	When one would submit information that is to be removed, 
	the information was ignored.  Now Parameters that are empty are not ignored. 
						
	2009-09-23
	Keith
	Fixed a bug in the "processApplication" sub, when 
	MerchantAffiliates::updateMerchantMasterInformation was called if it returned 0 
	the merchant was not notified their information was not updated.  
						
	2010-06-16
	Keith
	Removed the "percentage of sale", and the "exception percentage of sale" fields.
	They are now on the Package Update page.
	
	2010-07-13
	Keith
	Updated This script so it meets the V 3.0 specifications of the merchant project.
	
	2010-07-26
	Bill
	Converted from ypcats2naics to the use of business_codes() for translable business types
	
	2012/05/12 Bill
	Implemented a crude lockout feature to keep online merchants from using this offline merchant centric interface
	
=cut

