#!/usr/bin/perl -w
###### ma_trans_rpt.cgi
###### 
###### Display merchant affiliate transaction summaries by month or day.
###### The script is driven by either a Master Merchant Affiliate ID or a Location ID
###### which is either found in the login cookie, or in the case of the Location ID,
###### may be supplied as a parameter via a link provided to the Master Merchant Affiliate.
###### 
###### Created: 06/10/2003	Karl Kohrt
###### 
###### Last modified: 02/21/12 Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use DB_Connect;
#use POSIX qw(locale_h);
#use locale;
#setlocale(LC_CTYPE, "en_US.ISO8859-1");
use State_List qw( %STATE_CODES );
use Digest::MD5 qw(md5_hex);
use G_S qw( $LOGIN_URL $SALT);

###### GLOBALS

my $transaction_entry_script = '/cgi/maf/maf_transactions.cgi';

###### our page template
our %TMPL = (
	'month'		=> 10180,
	'month_row'		=> 10179,
	'locations'		=> 10187,
	'location_row'	=> 10188,
	'day'			=> 10178,
	'day_row'		=> 10177
);

###### the tables we'll get data from
our %TBL = (
	'master'	=> 'merchant_affiliates_master',
	'locations'	=> 'merchant_affiliate_locations',
	'transactions'=> 'merchant_affiliates_transactions',
	'vendors'	=> 'vendors',
	'ta_live'	=> 'ta_live',
	'periods'	=> 'periods',
	'discounts'	=> 'merchant_affiliate_discounts',
	'codes'	=> 'currency_codes'
);

# Merchant types based on the vendor_group field from the vendors table.
our %MERCHANTS = (	5 => { 'description'	=> 'ClubBucks',		# Which group this is.
				'id_phrase'	=> 'Rewards Card ID',		# The forms' ID display header.
				'id_table'	=> 'clubbucks_master',	# The table for lookups.
				'id_field'	=> 'clubbucks_id',		# The transactions ID field.
				'id_format'	=> "%%the_id%%::bigint"},	# The format for a query.
			6 => { 'description'	=> 'MarketPlace',
				'id_phrase'	=> 'DHS Club ID',
				'id_table'	=> 'members',
				'id_field'	=> 'alias',
				'id_format'	=> "'%%the_id%%'"}
			);
# day = day reporting, search = search results
our %conditionals = (	'day'=>1,
				'search'=>0,
				'admin'=>0);
our $q = new CGI;
our $ME = $q->url;

our $db = '';
our $location_id = '';
our $mal_type = '';				# The Merchant Affiliate Location type.
our ( $Login_ID, $ma_info ) = ();
our @location_list = '';
our $day_to_report = '';
our $discount = '0';
our $action = '';
our $cbid = '';
our $trans_id = '';
our $period = '';
our $period_menu = '';
our %periods_hash = ('' => 'Select a Period');
our $default_code = '';			# The currency code for display purposes only.

################################
###### MAIN starts here.
################################


unless ($db = DB_Connect( 'ma_trans_rpt.cgi' )){exit}
$db->{RaiseError} = 1;

Get_Cookie();		# Get the Merchant info from the cookie.
Get_Curr_Period();	# Get the period that was passed or default to the current period.
Get_Periods();	# Get the periods to be reported on.

# Get any path info and/or parameters that may have been passed in.
($location_id = $q->path_info()) =~ s/^\///;
$action = $q->param('action');
$cbid = $q->param('cbid');
# Uppercase the ID in case it is an alias.
$cbid = uc($cbid);

$trans_id = $q->param('trans_id');

# If there is no $location_id(first time through), display the location(s) page, depending on
#  whether this is a master or location login.

unless ( $location_id )
{
	# If it is a location login, recall the script with the single id in the path.	
	if ( $ma_info->{membertype} eq 'mal' )
	{
		print $q->redirect( $ME . "/" . $ma_info->{id} );
	}
	
	# If it is a master login...
	elsif ( $ma_info->{membertype} eq 'maf' )
	{
		my $how_many_locations = @location_list;

		# If there is only one location, recall the script with the single id in the path.
		if ( $how_many_locations == 1 )
		{
			print $q->redirect( $ME . "/" . $location_list[0] );
		}
		# Otherwise, display a list of locations to choose from.
		else
		{
			Report_Locations();
		}
	}
}
	
# If a location_id with non-numeric characters was passed, then we have an error.							
elsif ( $location_id =~ /\D/ )
{
	Err('Undefined action.')
}
	
# If we have a numeric location ID passed.
else
{
	# If the id passed is in the cookie's list.
	if ( grep {/$location_id/} @location_list )
	{
		# Get the location type.
		$mal_type = Get_Vendor_Group() if $location_id;

		$day_to_report = $q->param('date');

		# If there is a true date passed in or a value to be searched for.
		if ( ($day_to_report && ($day_to_report ne 'Search Results'))
			|| ( $cbid || $trans_id ) )
		{
			# If this is a search request, do the search.
			if ( $day_to_report eq 'Search Results' )
			{
				G_S::PreProcess_Input($cbid);
				G_S::PreProcess_Input($trans_id);
				$trans_id ||= '';
				# If transaction search criteria is not a number, error out.
				if ( $trans_id =~ /\D/ )
				{
					Err("Transaction ID field only accepts numbers.
						&nbsp;&nbsp;<a href=\"$ME/$location_id\">Search Again</a>");
				}
				else
				{
					Report_Search();
				}
			}
			# Otherwise display the requested day's transactions.
			else { Report_Daily(); }
		}
		# Otherwise, display the month-to-date report with links to specific days.
		else
		{
			# The merchant info needed to produce an invoice look for Europe.
			my $location_info = Get_Location_Info($location_id);
			# Run the report.
			Report_Period($location_info);
		}
	}
	else
	{
		Err("Location $location_id does not exist in our records.
			<BR>If this is a brand new location, try 
			<A HREF='$LOGIN_URL/maf?action=logout'>
			logging out</A>
			and then logging back in again.")
	}
}

$db->disconnect;
exit;

################################
###### Subroutines start here.
################################

###### Create a combo box based upon the parmeters passed in.
sub Create_Combo{
	my ($name, $list, $labels, $def) = @_;
	return $q->popup_menu(	-name		=> $name,
					-values	=> $list,
					-labels	=> $labels,
					-default	=> $def,
					-onChange	=> 'document.forms[0].submit()');
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now', -charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error', -encoding=>'utf-8');
	print $q->p($_[0]);
	print $q->end_html();
}

# Fill out an individual transaction row.
sub Fill_Transaction_Row
{
	my $one_row = shift;
	my $data = shift;
	my $md5h = md5_hex("$SALT $location_id $data->{record_id} $data->{trans_amt} $data->{native_currency_amt} $data->{clubbucks_id}");
	
	$one_row =~ s/%%RECORD_ID%%/$q->a( {	-href	=> $transaction_entry_script
								. "\/" . $location_id
								. "\/" . $md5h
								. "?action=refund"
								. "&id=" . $data->{record_id}
								. "&purchase_amt=" . $data->{trans_amt}
								. "&native_amt=" . $data->{native_currency_amt}
								. "&native_code=" . $data->{native_currency_code}
								. "&clubbucks_id=" . $data->{clubbucks_id}},
							$data->{record_id} )/e;
	$one_row =~ s/%%CBID%%/$data->{clubbucks_id}/;
	$one_row =~ s/%%NATIVE_AMT%%/$data->{native_currency_amt}/;
	$one_row =~ s/%%TRANS_AMT%%/$data->{trans_amt}/;
	$one_row =~ s/%%TRANS_DATE%%/$data->{trans_date}/;
	$one_row =~ s/%%ENTER_DATE%%/$data->{enter_date}/;

	# Set the currency code - we assume all transactions for the day used the same currency as the last transaction.
	$default_code = $data->{'native_currency_code'};

	# Return the new row.
	return $one_row;
}		

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# Get the Login ID and Merchant Affiliate info hash.
	( $Login_ID, $ma_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_maf'), 'maf' );

	# If the Login ID doesn't exist or has any non-digits send them to login again.
	if (!$Login_ID || $Login_ID =~ /\D/)
	{
		RedirectLogin();
		exit;
	}

	# Split out the separate location id's from the list.
	@location_list = split (/\,/, $ma_info->{'maflocations'});

}

# Get the current period or the period passed via a parameter.
sub Get_Curr_Period
{
	my ($qry, $sth) = ();
			
	# Get the period that was passed or default to the current period.
	unless ( $period = $q->param('period') )
	{
		$qry = "SELECT period
			FROM 	periods
			WHERE 	open <= NOW()::date
			AND	close IS NULL
			AND 	type=2;";
		$sth = $db->prepare($qry);
		$sth->execute();
		$period  = $sth->fetchrow_array;
	}
}												

###### Get the info on a specific location.
sub Get_Location_Info
{
	my $loc_id = shift;
	my $loc_info = $db->selectrow_hashref("	SELECT *
							FROM $TBL{locations}
							WHERE id = $location_id");
	return $loc_info;
}

###### Get the reporting periods and build a drop-down menu.
sub Get_Periods	
{
	my $qry = '';
	my $sth = '';
	my $data = '';
	my @list = ('');

	$qry = "	SELECT period, open, close
			FROM 	periods
			WHERE 	open <= NOW()::date
			AND	(close IS NULL
				OR close <= NOW()::date)
			AND 	type=2
			ORDER BY open DESC;";
	$sth = $db->prepare($qry);
	$sth->execute();

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@list, $data->{period});
		$data->{close} ||= "current";
		$periods_hash{$data->{period}} = "$data->{open} to $data->{close}";
	}
	$sth->finish;

	$period_menu = Create_Combo('period', \@list, \%periods_hash);
}

###### Get the vendor group code which indicates the location type.
sub Get_Vendor_Group
{
	my $code = $db->selectrow_array("	SELECT v.vendor_group
						FROM 	$TBL{locations} l
							INNER JOIN $TBL{vendors} v
							ON l.vendor_id = v.vendor_id
						WHERE l.id = $location_id;" );
	return $code;
}

###### Display the report of transactions for a specified date.
sub Report_Daily
{
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $total = 0;

	# Prepare a query to get detailed transactions for the date and location.
	my $qry = "	SELECT record_id,
				coalesce(clubbucks_id::VARCHAR, alias) AS clubbucks_id,
				trans_amt,
				native_currency_amt,
				native_currency_code,
				trans_date,
				enter_date::timestamp(0)
			FROM $TBL{'transactions'}
			WHERE mal_id = ?
			AND enter_date::date = ?
			ORDER BY record_id";

	my $sth = $db->prepare($qry);

	# Get the detailed info for the location.
	$sth->execute($location_id, $day_to_report);
	my $report_row = G_S::Get_Object($db, $TMPL{'day_row'}) || die "Couldn't open $TMPL{'day_row'}";

	while ( $data = $sth->fetchrow_hashref)
	{
		# Add the new row to the other rows.
		$report_body .= Fill_Transaction_Row($report_row, $data);

		# Add the native transaction amount to the aggregate total.
		$total += $data->{'native_currency_amt'};
	}		

	$sth->finish();
	$qry = "	SELECT 	description
		FROM 	$TBL{codes}
		WHERE	code = ?";
	$sth = $db->prepare($qry);
	$sth->execute($default_code);
	my $currency_description = $sth->fetchrow_array;
	$sth->finish();

	my $tmpl = G_S::Get_Object($db, $TMPL{day}) || die "Couldn't open $TMPL{day}";

	# If our conditional flag is set we'll leave that block alone.
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$tmpl =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	$tmpl =~ s/%%LOCATION_ID%%/$location_id/g;
	$tmpl =~ s/%%DAY_TO_REPORT%%/$day_to_report/g;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$total = sprintf("%.2f", $total);
	$tmpl =~ s/%%TOTAL%%/$default_code$total/;
	$tmpl =~ s/%%CURRENCY_DESCRIPTION%%/$currency_description/;
	$tmpl =~ s/%%ID_HEADER%%/$MERCHANTS{$mal_type}{id_phrase}/g;

	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

###### Display a list of locations if we have more than one.
sub Report_Locations
{
	my $html = '';
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $total = 0;
	my $monthly_due = 0;
	my $discount_error = 0;

	# Prepare a query to get details about each location id.
	my $qry = "SELECT id, location_name, city, address1, state, country
			FROM $TBL{locations}
			WHERE id = ?";
	my $sth = $db->prepare($qry);
	
	# Prepare a query to get transaction totals for each location.
	my $ttl_qry = '
		SELECT
			SUM(total) AS total,
			SUM(total * discount) AS discount
		FROM 	(
					SELECT
						t.trans_date,
						SUM(t.trans_amt) AS total,
						d.discount
					FROM
						merchant_affiliates_transactions t
					JOIN
						merchant_affiliate_discounts d
						ON t.mal_id = d.location_id
					JOIN
						periods p
						ON t.period = p.period
					WHERE 	t.mal_id = ?
					AND 	p.period = ?
					AND 	t.trans_date >= d.start_date
					AND	(t.trans_date <= d.end_date
						OR d.end_date IS NULL)
					GROUP BY t.trans_date, d.discount
		)
				AS date_totals;
			';
	my $ttl_sth = $db->prepare($ttl_qry);

	my $location_row = G_S::Get_Object($db, $TMPL{'location_row'}) || die "Couldn't open $TMPL{'location_row'}";
	# Display a line for each location in the list.
	foreach $location_id (@location_list)
	{
		# Get the detailed info for the location listing.
		$sth->execute($location_id);
		$data = $sth->fetchrow_hashref();
		
		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		# Default if no value is possible for a field.
		$data->{'address1'} = $data->{'address1'} || '&nbsp;';
		$data->{'city'} = $data->{'city'} || '&nbsp;';
		$data->{'state'} = $data->{'state'} || '&nbsp;';
		$data->{'country'} = $data->{'country'} || '&nbsp;';

		$one_row =~ s#%%id%%#$q->a({-href => $ME . '/' . $data->{id}}, $data->{id} )#e;
		$one_row =~ s/%%location_name%%/$data->{location_name}/;
		$one_row =~ s/%%address1%%/$data->{address1}/;
		$one_row =~ s/%%city%%/$data->{city}/;
		$one_row =~ s/%%state%%/$data->{state}/;
		$one_row =~ s/%%country%%/$data->{country}/;

		# Get the location transaction total.
		$ttl_sth->execute($location_id, $period);
		$data = $ttl_sth->fetchrow_hashref();

		# Add the transaction amount to the aggregate total.
		#  Since 'sum' can be null, we'll make it a zero to avoid warnings
		$total += $data->{'total'} || 0;
		$monthly_due += $data->{'discount'} || 0;

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}

	$sth->finish();

	# Get the body template and make the substitutions.	
	my $tmpl = G_S::Get_Object($db, $TMPL{'locations'}) || die "Couldn't open $TMPL{locations}";
	$tmpl =~ s/%%PERIOD%%/$periods_hash{$period}/;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$monthly_due = sprintf("%.2f", $monthly_due);
	$tmpl =~ s/%%MONTHLY_DUE%%/$monthly_due/;
	$total = sprintf("%.2f", $total);
	$tmpl =~ s/%%TOTAL%%/$total/;

	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
}

###### Display the Period's summary report with daily links and totals.
sub Report_Period
{
	my $loc_info = shift;
	my $data = '';
	my $tax_id_number = '';
	my $one_row = '';
	my $report_body = '';
	my $year = '';
	my $total = 0;
	my $monthly_due = 0;

	# Prepare a query to get transaction totals for all locations.
#	my $qry = qq|
#		SELECT
#			cnt AS "count",
#			enter_date::date,
#			date_sum,
#			date_sum * discount AS discount_total
#
#		FROM (
#				SELECT
#					COUNT(*) AS cnt,
#					t.enter_date,
#					SUM(t.trans_amt) AS date_sum,
#					d.discount
#				FROM
#					$TBL{transactions} t,
#					$TBL{discounts} d,
#					$TBL{periods} p
#				WHERE 	t.mal_id = ?
#				AND 	t.mal_id = d.location_id
#				AND 	p.period = ?
#				AND 	t.period = p.period
#				AND 	t.enter_date::date >= d.start_date
#				AND	(t.enter_date::date <= d.end_date
#					OR d.end_date IS NULL)
#				GROUP BY t.enter_date::date, d.discount
#		)
#			AS date_totals;
#		|;
	
	my $qry = qq|
		SELECT
			COUNT(*) AS "count",
			enter_date::date,
			SUM(trans_amt) AS date_sum,
			SUM(amount_due) AS discount_total
		FROM merchant_affiliates_transactions_vw
		WHERE mal_id= ?
		AND period= ?
		GROUP BY enter_date::DATE
		ORDER BY enter_date::DATE
	|;

	my $tax_id_query =<<EOT;

		SELECT
			m.tin
		FROM
			members AS m
			JOIN
			merchant_affiliates_master AS mam
			ON
			m.id = mam.member_id
			JOIN
			merchant_affiliate_locations AS mal
			ON
			mam.id = mal.merchant_id
		WHERE
			mal.id = ?

EOT



	my $sth = $db->prepare($tax_id_query);
	$sth->execute($location_id);
	
	my $tax_info = $sth->fetchrow_hashref;
	
	$tax_id_number = $tax_info->{'tin'} if $tax_info->{'tin'};
	
	$sth->finish();
	
	$sth = undef;
	
	$sth = $db->prepare($qry);

	# Get the info for the daily transaction summaries.
	$sth->execute($location_id, $period);
	
	
	
	
	my $report_row = G_S::Get_Object($db, $TMPL{'month_row'}) || die "Couldn't open $TMPL{'month_row'}";
	
	while ( $data = $sth->fetchrow_hashref)
	{
		# Start with a fresh template, then make all the substitutions.
		$one_row = $report_row;

		$one_row =~ s#%%DATE%%#$q->a({'-href'=> $ME . '/' . $location_id . "?date=" . $data->{'enter_date'} }, $data->{'enter_date'} )#e;
		$one_row =~ s/%%DATE_SUM%%/$data->{'date_sum'}/;
		$one_row =~ s/%%DATE_COUNT%%/$data->{'count'}/;

		# Add the transaction amount to the aggregate total.
		$total += $data->{'date_sum'};

		# Add the discount amount to the aggregate total.
		$monthly_due += $data->{'discount_total'};

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}		

	$sth->finish();

	my $tmpl = G_S::Get_Object($db, $TMPL{'month'}) || die "Couldn't open $TMPL{'month'}";

	# If our conditional flag is set we'll leave that block alone.
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$tmpl =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	$tmpl =~ s/%%ME%%/$ME/g;
	$tmpl =~ s/%%LOCATION_ID%%/$location_id/g;
	$tmpl =~ s/%%ID_HEADER%%/$MERCHANTS{$mal_type}{id_phrase}/g;

	$tmpl =~ s/%%LOC_NAME%%/$loc_info->{location_name}/g;
	my $address = "$loc_info->{address1}<br>" || '';
	$address .= "$loc_info->{address2}<br>" if $loc_info->{address2};
	$address .= "$loc_info->{address3}<br>" if $loc_info->{address3};
	$tmpl =~ s/%%LOC_ADDRESS%%/$address/g;
	my $city_state = "$loc_info->{city}" || '';
	$city_state .= ", " if ($city_state && $loc_info->{state});
	$city_state .= "$loc_info->{state}&nbsp;&nbsp;" if $loc_info->{state};
	$tmpl =~ s/%%LOC_CITY_STATE%%/$city_state/g;
	$tmpl =~ s/%%LOC_COUNTRY%%/$loc_info->{country}/g;
	$tmpl =~ s/%%LOC_POSTALCODE%%/$loc_info->{postalcode}/g;

	$tmpl =~ s/%%PERIOD_MENU%%/$period_menu/g;
	$tmpl =~ s/%%PERIOD%%/$periods_hash{$period}/g;
	$tmpl =~ s/%%PERIOD_NUM%%/$period/g;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$tmpl =~ s/%%DISCOUNT%%/$discount/;
	$monthly_due = sprintf("%.2f", $monthly_due);
	$tmpl =~ s/%%MONTHLY_DUE%%/$monthly_due/;
	$total = sprintf("%.2f", $total);
	$tmpl =~ s/%%TOTAL%%/$total/;
	
	$tmpl =~ s/%%TAX_ID_NUMBER%%/$tax_id_number/;
	
	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

###### Display the search results with refund transaction links to maf_transactions.cgi.
sub Report_Search
{
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $month = '';
	my $month_to_query = '';

	# Blank out the monthly day report messages and show the search messages.
	$conditionals{day} = 0;
	$conditionals{search} = 1;

	# Prepare a query to get detailed transactions for the date and location.
	my $qry = "	SELECT record_id,
				coalesce(clubbucks_id::VARCHAR, alias) AS clubbucks_id,
				trans_amt,
				native_currency_amt,
				native_currency_code,
				trans_date,
				enter_date::timestamp(0)
			FROM $TBL{transactions}
			WHERE mal_id = ? ";


	# Get the month if it was passed and include it in the query.
	if ( $month = $q->param('month') )
	{
		if ( length($month) == 1 )
		{
			$month = '0' . $month;		# Pad a single digit month.
		}
		$month_to_query = "'%-" . $month . "-%'";	# Pad for the LIKE query.
		$qry .= " AND trans_date LIKE $month_to_query";
	}

	# If we have values provided, include them in the query.
	if ( $cbid )
	{
		# Add in the proper field and format, then sub in the ID value.
		$qry .= " AND $MERCHANTS{$mal_type}{id_field}
					= $MERCHANTS{$mal_type}{id_format}";
		$qry =~ s/%%the_id%%/$cbid/;
	}
	if ( $trans_id )
	{
		$qry .= " AND record_id = $trans_id"
	}			

	$qry .= " ORDER BY enter_date, trans_date";
	my $sth = $db->prepare($qry);

	# Get the info for the daily transaction summaries.
	$sth->execute($location_id);

	my $report_row = G_S::Get_Object($db, $TMPL{day_row}) || die "Couldn't open $TMPL{day_row}";

	my $num_records = 0;
	while ( $data = $sth->fetchrow_hashref)
	{
		# Add the new row to the other rows.
		$report_body .= Fill_Transaction_Row($report_row, $data);
		++$num_records;
	}
	if ( $num_records == 0 )
	{
		$report_body = "No matching transactions were found.
				&nbsp;&nbsp;<a href=\"$ME/$location_id?month=$month\">Search Again</a>";
	}

	$sth->finish();

	my $tmpl = G_S::Get_Object($db, $TMPL{day}) || die "Couldn't open $TMPL{day}";

	# If our conditional flag is set we'll leave that block alone.
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$tmpl =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	$tmpl =~ s/%%LOCATION_ID%%/$location_id/g;
	$tmpl =~ s/%%DAY_TO_REPORT%%/$day_to_report/g;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$tmpl =~ s/%%ID_HEADER%%/$MERCHANTS{$mal_type}{id_phrase}/g;

	print $q->header(-expires=>'now'), $tmpl;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff', -encoding=>'utf-8');
	print "Please login at <a href=\"$LOGIN_URL/maf?destination=$me\">$LOGIN_URL/maf</a><br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

###### 06/23/03 changed the redirect for one location to pull the actual location ID rather than the scalar of the array
###### 07/02/03 Added the month-to-date transaction total, discount(Report_Locations only) and the the month name to 
###### total due the DHS Club into the Report_Monthly and Report_Locations subs.
###### 07/07/03 fixed some uninitialized var errors and also the total amt. of transactions value on the master report
###### 07/08/03 Changed the monthly_due calculation to calculate each location's monthly_due
######			individually and removed 'GROUP BY trans_date' from $ttl_qry.
###### 07/09/03 Added the search functionality.
###### 07/11/03 Added the md5_hex code to the URL being passed for a refund transaction.
###### 08/04/03 Added lines $data->{sum} = $data->{sum} || 0; and $discount = $discount || 0;
######		 	to monthly summary page to stop log errors when values are NULL.
###### 08/25/03 Added '&nbsp;' defaults to the Report Locations fields that may contain blank values.
###### 10/25/03 Modified to report by periods instead of calendar months. Also modified to look 
######			at the correct discount structure record for each transaction.
###### 10/28/03 Changed date to report by to the entry date instead of the transaction date.
###### 10/30/03 Added the native_currency_amt to the refund link and the same column to the daily report.
###### 11/04/03 Added the original currency code to the transaction refund URL's.
###### 03/02/05 Added header info, location info, and a statement # to the Report_Period sub and template
######		to emulate an invoice required by the European merchants per Fabrizio Perotti.
###### 11/17/05 Added code to handle reporting transactions using a DHS Club alias and
######		creating refund strings using an alias too. Almost all the changes
######		are based on the new hash %MERCHANTS which is based upon the merchant's vendor group.
###### 07/28/09 The textual conted of 'id_phrase' was changed to 'Rewards Card ID'.
###### 08/18/10 removed the target attribute on the links for locations