#!/usr/bin/perl -w

=head1 NAME:
master_profile.cgi

	This script is used to update the Merchants Master Profile information.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
#use lib "$FindBin::Bin/../../cgi-lib";
use lib '/home/httpd/cgi-lib';
use DB;
use DB_Connect;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Cookie;
use MerchantAffiliates;
use Merch_Tools;
use MailTools;

use Data::Dumper;	# for debugging only
#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
our $CGI = CGI->new();
our $G_S = G_S->new();
our $MA = {};

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $xml = '';
our $xsl = '';
our $configuration_xml = '';
our $langauges_xml = '';
our $languages_ref = {};
our $countries_xml = '';


####################################################
# Main
####################################################

exit unless $db = DB_Connect('maf/setup.cgi');
$db->{'RaiseError'} = 1;

my $merch_data = {};
	
$MA = MerchantAffiliates->new($db);
my $cookie = $MA->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
$merch_data = $MA->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'}) if $cookie->{'ma_info'}->{'membertype'} ne 'mal';
			
warn Dumper($merch_data);
#if($@)
#{
#	print $CGI->header('text/plain');
#	print "A fatal error occured, contact support.";
#	warn $FindBin::RealScript . ' - ' . $@;
#	exitScript();
#}
#
# Get Language and html for display
#
if ($merch_data->{'discount_type'} > 19)
{
	$xml = G_S::Get_Object($db, 10933);
	$xsl = G_S::Get_Object($db, 10934);
	
	if ($CGI->param && ! $CGI->param('updated') && $CGI->param)
	{
		processApplication($merch_data);
	}
	else
	{
		displayApplication($merch_data);
	}
}
else
{
    $xml = G_S::Get_Object($db, 10697);
    $xsl = G_S::Get_Object($db, 10698);

    if(! $CGI->param('updated') && $CGI->param('activate'))
    {
        processApplication_offline($merch_data);
    }
    else
    {
        displayApplication($merch_data);
    }
}


exitScript();

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub displayApplication
{
    my $merch_data = shift;
    my $errors = shift;
	
    my $errors_xml = '';
    my $notices_xml = '';
    my $information_hashref_arrayref = {};
    my $params = {};
	


    #Make the information XML friendly
    foreach (keys %$merch_data)
	{
		next if ! defined $merch_data->{$_};
			
		$information_hashref_arrayref->{$_} = [$merch_data->{$_}];
	}
		
			
	$params = XML::Simple::XMLout($information_hashref_arrayref, 'RootName' => "params" ) if $information_hashref_arrayref->{'status'};
	
	
	if ($errors)
	{
		$errors_xml = XML::Simple::XMLout($errors, 'RootName' => "errors" );
	}


	if ($CGI->param('updated'))
	{
		$notices_xml = XML::Simple::XMLout({'updated'=>['updated']}, 'RootName' => "notices" );
	}
		
	my	$output_xml = <<EOT;
		<base>
			
			<data>
				$errors_xml
				$notices_xml
			</data>
		
			$params
			
			$xml
			
		</base>
		
EOT
	
	
	print $CGI->header('-charset'=>'utf-8');
	print XML::local_utils::xslt($xsl, $output_xml);

	exitScript();
}

sub processApplication
{
	
    my $merch_info = shift;
    if ($CGI->param && $CGI->param('activate'))
    {
		$MA = MerchantAffiliates->new($db);
		$merch_info->{'status'} = 3;
		$MA->updateMerchantMasterInformation($merch_info->{'id'}, $merch_info);
		$MA->updateVendorStatusByMerchantID($merch_info->{'id'}, 3);
#                                                                                                                                                      
# Send Email to Carina                                                                                                                                 
#                                                                                                                                                      
		my $qry = "Select * FROM merchant_affiliate_locations WHERE merchant_id = ?";
		my $stmt = $db->prepare($qry);
		$stmt->execute($merch_info->{'id'});
		my $merch_locs = $stmt->fetchrow_hashref;
		my %email =
		(
			'from'=>'"maf/setup.cgi"<clubshop@dhs-club.com>',
			'to'=>'clubshoprewards@clubshop.com',
			'subject'=>'Online Merchant Changed from accepted to pending',
			'text'=>"Merchant ID: $merch_info->{'id'}\nVendor_id: $merch_locs->{'vendor_id'}\nMerchant URL: $merch_info->{'url'}",
		);
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
#
# Send email to merchant
#
		my $html = '';
		$html = G_S::Get_Object($db, 10936);
		my $translation_xml = G_S::Get_Object($db, 10933);
		my $translation = XML::Simple::XMLin($translation_xml);
		$html = Merch_Tools::addLanguage($html, $translation->{'email4'});
		$html =~ s/%%params_firstname%%/$merch_info->{'firstname1'}/g;
		$html =~ s/%%p5%%//g;
		$html =~ s/%%p6%%//g;
		$html =~ s/%%p7%%//g;
		%email =
		(
			'from'=>'info@ClubShop.com',
			'to'=>"$merch_info->{'emailaddress1'}",
			'subject'=>'New Online Merchant Registration ',
			'text'=>$html,
		);

		$MailTools->sendTextEmail(\%email);
    }

	my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated');
	
	print $CGI->redirect('-uri'=>$url);
	exitScript();
}

sub processApplication_offline
{
	
    my $merch_info = shift;
    if ($CGI->param && $CGI->param('activate'))
    {
		$MA = MerchantAffiliates->new($db);
		$merch_info->{'status'} = 1;
		$MA->updateMerchantMasterInformation($merch_info->{'id'}, $merch_info);
		$MA->updateVendorStatusByMerchantID($merch_info->{'id'}, $merch_info->{'status'});
    }

	my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated');
	
	print $CGI->redirect('-uri'=>$url);
	exitScript();
}


__END__

=head1
CHANGE LOG

	Date	Made By		Description of the Change

=cut

