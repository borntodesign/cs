#!/usr/bin/perl -w
###### maf_transactions.cgi
###### 05/22/2003	Karl Kohrt
######
###### This script will accept a "textarea" field loaded with a list of
###### CSV value triplets separated by newline characters. The value triplets consist of
###### a ClubBucks ID with or without the DHS prefix,
###### a value not to exceed 999999.99, and a date of the format mm-dd-yyyy.
######
###### It will also accept a properly formatted refund request from ma_trans_rpt.cgi:
######
######		maf_transactions.cgi/<location id>/<md5 hash of the following values>
######							?action=refund
######							&id=<transaction id>
######							&purchase_amt=<amount>
######							&native_amt=<native currency amount>
######							&clubbucks_id=<cbid>
######
###### and display a refund form. Using this form, the Merchant Affiliate can process a
###### refund against an existing transaction.
######
###### Last modified: 2014-11-30	Bill MacArthur

use strict;

use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S qw( $COACH_LEVEL $SALT %URLS );
use DB_Connect;
use Data::Dumper;	# only needed for testing/debugging

###### CONSTANTS
# html templates
our %TMPL = (	'entry_form'	=> 10167,
		'refund_form'	=> 10193);

our %TBL = (	'locations'	=> 'merchant_affiliate_locations',
		'transactions'	=> 'merchant_affiliates_transactions',
		'vendors'		=> 'vendors',
		'rates'		=> 'exchange_rates',
		'codes'		=> 'currency_codes');

# Merchant types based on the vendor_group field from the vendors table.
our %MERCHANTS = (
	5 => { 'description'	=> 'ClubBucks',		# Which group this is.
		'id_phrase'	=> 'Rewards Card Number',		# The forms' ID display header.
		'id_table'	=> 'clubbucks_master',	# The table for lookups.
		'id_field_cv'	=> 'id',			# The Check_Valid ID field.
		'id_field_it'	=> 'clubbucks_id'		# The Insert_Transactions ID field.
	},
	6 => { 'description'	=> 'MarketPlace',
		'id_phrase'	=> 'DHS Club ID',
		'id_table'	=> 'members',
		'id_field_cv'	=> 'alias',
		'id_field_it'	=> 'alias'
	}
);
###### GLOBALS
our $q = new CGI;
our $ME = $q->url;
our $transactions = '';			# A variable for our anonymous hashrefs.
our @transactions = ();			# An array of transaction hashrefs.
our $db = '';
our $trans_list = '';
our $current_tmpl = '';			# The template that we will display.
our $message = '';				# Any error messages that may occur.
our $message_class = 'r';			# Default message is an error - red.
# The report of successful submitted transactions.
our $success_submission_report = "<U>Transactions accepted " . (localtime) . "</U><BR>";
our $failed_submission_report = '';	# The report of failed submitted transactions.
our $incomplete_report = '';		# The report of incomplete transactions.
our $invalid_report = '';			# The report of invalid transactions.
our $insert_report = '';			# The report of database insert errors.
our $fore_days = 1*60*60*24;		# The max. days forward that a transaction can be dated.
our $back_days = 31*60*60*24;		# The max. days backward that a transaction can be dated.
						# (Days*seconds*minutes*hours)
our $Login_ID = '';				# The cookie's login id.
our $ma_info = '';				# The cookie's info hash.
our @location_list = '';			# The cookie's location info.
our $mal_id = '';				# The Merchant Affiliate Location id.
our $mal_type = '';				# The Merchant Affiliate Location type.
our $action = '';
our $refund_clubbucks_id = '';		# The ClubBucks ID for a refund request.
our $orig_purchase_amt = '';		# The original purchase amount for a refund request.
our $orig_native_amt = '';			# The original native currency amount for a refund request.
our $orig_curr_code = '';			# The original native currency code for a refund request.
my $offset_id = ();			# The original transaction id for a refund request.
our $trans_notes = '';			# Notes to be added to the transaction.
our $trans_amt = '';				# The refund transaction amount.
our $trans_date = '';			# The refund transaction date.
our $md5h = '';				# The security hash of values passed for a refund.
our $conversion_rate = 0;			# The most recent currency conversion rate.
our $default_code = '';			# The default currency code from the cookie.
our $rate_code = '';				# The currency code used locally.
our $currency_menu = '';
our %currency_hash = (	'' => 'Choose One');

# The fields passed to this script which are equivalent to those in the table.
our @field_names = qw/clubbucks_id trans_amt trans_date/;	

##########################################
###### MAIN program begins here.
##########################################

unless ($db = DB_Connect::DB_Connect('maf_transactions.cgi')){exit}
$db->{RaiseError} = 1;

# Get the list of transactions, if any have been passed.
$trans_list = $q->param('trans_list_text') || '';

# Get any parameters that may have been passed in with an action.
$action = $q->param('action') || '';
if ( $action )
{
	$refund_clubbucks_id = $q->param('clubbucks_id');
	$orig_purchase_amt = $q->param('purchase_amt');
	$orig_native_amt = $q->param('native_amt');
	$orig_curr_code = $q->param('native_code');
	$offset_id = $q->param('id');
	$trans_notes = $q->param('trans_notes');
	$trans_amt = $q->param('trans_amt');	
}

goto 'END' unless Get_Cookie();		# Get the Merchant info from the cookie.
my $rv = Get_Path();				# Get the Merchant Affiliate Location ID.
$mal_type = Get_Vendor_Group() if $rv == 1; # Get the location type if a location_id was submitted.

# Get the merchant's default currency conversion code or default to US dollars.
$default_code = $q->param('exchange_code') || $q->cookie('currency_code') || '';

if ($rv == 0)
{
	# We should never get here, but just in case.
	Err("No locations were found for this Merchant, please <a href='/cgi/maf/setup.cgi'>activate</a> your Merchant Account.");
}
elsif ($rv > 1)
{
	Report_Locations();
}
		
# If we didn't receive a currency code.
elsif ( ! $default_code && $trans_list )
{
	$incomplete_report = '<b>Please choose a Currency Code.</b>';
	$failed_submission_report = $trans_list;
	$current_tmpl = G_S::Get_Object($db, $TMPL{'entry_form'}) || die "Failed to load template $TMPL{'entry_form'}";
	Do_Page($current_tmpl);
}

# If there are parameters passed, parse through them, check them, and either insert them
#  in the database or output appropriate error messages to the browser.
elsif ( $trans_list )
{
	Get_Rate('current');		# Get the currency conversion rate.
	Parse_Transactions();	# Parse the transactions into an array of hashes.
	Check_Complete();		# Check that all fields for all transactions were completed.
	Check_Valid();		# Check that all fields for all transactions are valid values.
	Submission_Report();		# Report the submitted transactions for display to browser.

	# If there are no errors reported, insert the transactions into the database.
	unless ( $incomplete_report || $invalid_report )
	{
		# If the insertion was not successful.
		unless ( Insert_Transactions() )
		{
			$insert_report = "<b>The database insert did not complete - no transactions were added.</b><br />";
		}
	}
	$current_tmpl = G_S::Get_Object($db, $TMPL{'entry_form'}) || die "Failed to load template $TMPL{'entry_form'}";
	Do_Page($current_tmpl);
}
# If this is a refund request.
elsif ( $action eq 'refund' )
{
	# If there is an md5 hash, check it to see if this is a valid request.
	if ( $md5h )
	{
		unless( $md5h eq md5_hex("$SALT $mal_id $offset_id $orig_purchase_amt $orig_native_amt $refund_clubbucks_id") )
		{
			Err("Corrupted data, please try again.");
			goto 'END';
		}
	}
			
	# If there is an amount, process the submitted refund.
	if ( $trans_amt )
	{
		# Make sure we have a positive value for the validity checks.
		if ( $trans_amt < 0 ) { $trans_amt *= -1 }

		# Set the transaction date to today's date in the format yyyy-mm-dd.
		my ($day, $mon, $year) = ();
		($day, $mon, $year) = (localtime)[3,4,5];
		$mon += 1; $year += 1900;
		if ( length($day) == 1 ) { $day = '0' . $day }	# Pad a single digit day.
		if ( length($mon) == 1 ) { $mon = '0' . $mon }	# Pad a single digit month.

		# Put the date into standard 'US' format.
		$trans_date = "$mon-$day-$year";

		# We build a transaction string so it will work with our existing subroutines.				
		$trans_list = $refund_clubbucks_id . ","
				. $trans_amt . ","
				. $trans_date;
		Get_Rate('refund');		# Get the rate associated with the original transaction.
		Check_Amount();		# Check that the refund amount isn't too much.
		Parse_Transactions();	# Parse the transaction.
		Check_Complete();		# Check that all fields were completed.
		Check_Valid();		# Check that all fields are valid values.
		Submission_Report();		# Report the submitted transaction for display to browser.

		# Get the default template for a refund.
		$current_tmpl = G_S::Get_Object($db, $TMPL{'refund_form'}) || die "Failed to load template $TMPL{'refund_form'}";

		# If there are no errors reported, insert the transaction into the database.
		unless ( $incomplete_report || $invalid_report )
		{
			# Change the amount to a negative value.
			if ($transactions[0]->{trans_amt} > 0 )
			{
				$transactions[0]->{trans_amt} *= -1;
			}

			# If the insertion was not successful.
			unless ( Insert_Transactions() )
			{
				$insert_report = "<b>The database insert did not complete - no transactions were added.</b><br />";
			}
			else
			{
				# We output the normal transaction form after a successful refund.
				$current_tmpl = G_S::Get_Object($db, $TMPL{'entry_form'}) || die "Failed to load template $TMPL{'entry_form'}";
			}									
		}
	}
	# This is the first time into the refund form.
	else
	{
		$success_submission_report = "Welcome to the Merchant Affiliate Refund Entry Form
						 - Location # %%LOCATION_ID%%";
		$message_class = 'heading';
		$current_tmpl = G_S::Get_Object($db, $TMPL{'refund_form'}) || die "Failed to load template $TMPL{'refund_form'}";
	}
	Do_Page($current_tmpl);
}
# If no parameters(transactions list), then this is the first time on the page.	
else
{
	$success_submission_report = '<b>Merchant Transaction Online Entry Form - Location # ' . $mal_id . '</b>';
	$message_class = 'heading';
	$current_tmpl = G_S::Get_Object($db, $TMPL{'entry_form'}) || die "Failed to load template $TMPL{'entry_form'}";
	Do_Page($current_tmpl);
}	
END:
$db->disconnect;
exit;

##############################
###### Subroutines start here.
##############################

###### Check that the refund is not greater than the original purchase less any other refunds.
sub Check_Amount
{
	# If the requested refund is greater than the original purchase amount, reject it. 
	if ( ( $trans_amt > $orig_native_amt) || ($trans_amt == 0) )
	{
		$invalid_report .= "The requested Refund Amount ($trans_amt) is greater than the Original Purchase Amount.<br>";
	}
	# Otherwise query the database for previous refunds against the original purchase.
	else
	{	
		my $sth = $db->prepare("
			SELECT SUM(native_currency_amt)
			FROM $TBL{transactions}
			WHERE offset_id = ?");
		$sth->execute($offset_id);

		my $sum_refunds = $sth->fetchrow_array() || 0;
		$sth->finish();

		# If the requested refund is greater than the original purchase amount 
		#  along with any previous refunds , reject it.
		my $refund_avail = $orig_native_amt + $sum_refunds;

		if ( $trans_amt > $refund_avail )
		{
			$refund_avail = sprintf("%.2f", $refund_avail);
			$invalid_report .= "The requested Refund Amount ($trans_amt) is greater than 
						the Original Purchase Amount less any prior refunds ($refund_avail ).<br />";
		}
	}
}

###### Check that each transaction has all three values.
sub Check_Complete
{
	my $field = '';
	my $one_transaction = '';
	my $trans_number = 0;	
	my $trans_error = 0;
	my $incomplete = 0;		# No incomplete transactions.

	# Check every transaction in the transaction list.
	foreach $one_transaction (@transactions)
	{
		$trans_error = 0;
		$trans_number++;

		# Check every field in each transaction.
		foreach $field (@field_names)
		{
			# If a field is blank and no error on this transaction yet,
			#  then mark the transaction incomplete.
			if ( !$trans_error && $one_transaction->{$field} eq '' )
			{
				$incomplete = 1;
				$trans_error = 1;
				$incomplete_report .= Display_Transaction($one_transaction, $trans_number);
			}
		}
		# If we had a transaction error, add a newline.			
		if ($trans_error) { $incomplete_report .= "<br />"; }
	}
	# If any transaction reported incomplete, add the heading to the report.
	if ($incomplete)
	{
		$incomplete_report = "<b><u>Missing values reported:</u></b>"
					 . "<br />" . $incomplete_report . "<br />";
	}
}

###### Check that each transaction has all valid values.
sub Check_Valid
{
	my $field = '';
	my $one_transaction = '';
	my $trans_number = 0;
	my $trans_error = 0;
	my $invalid = 0;		# No invalid transactions.
	my ($mday, $mon, $year) = ();

	# Calculate the valid date range for a transaction.
	($mday, $mon, $year) = (localtime(time + $fore_days))[3,4,5];
	my $latest_date = sprintf "%04d-%02d-%02d", $year + 1900,
							$mon + 1,
							$mday;
	($mday, $mon, $year) = (localtime(time - $back_days))[3,4,5];
	my $earliest_date = sprintf "%04d-%02d-%02d", $year + 1900,
							$mon + 1,
							$mday;
						
	# Check every transaction in the transaction list.
	foreach $one_transaction (@transactions)
	{
		$trans_error = 0;
		$trans_number++;

		##### Do the cursory checks first, then the database checks later if no errors.
		# ClubBucks number (we should have already filtered out the leading 'DHS' if present)
		if ($mal_type == 5 && $one_transaction->{clubbucks_id} =~ /\D/)
		{
			$invalid = 1;
			# Do this only if no error reported yet in this transaction.
			unless ($trans_error)
			{
				$invalid_report .= Display_Transaction($one_transaction, $trans_number);
				$trans_error = 1;
			}
			$invalid_report .= " *ClubBucks Number";
		}
		# DHS Club ID has to be a complete *alias*
		elsif ($mal_type == 6 && $one_transaction->{clubbucks_id} !~ /\D/)
		{
			$invalid = 1;
			# Do this only if no error reported yet in this transaction.
			unless ($trans_error)
			{
				$invalid_report .= Display_Transaction($one_transaction, $trans_number);
				$trans_error = 1;
			}
			$invalid_report .= " *DHS Club ID";
		}

		# Amount - From 0 to 6 digits, 
		#  then optionally followed by a decimal point and up to 2 more digits.
		unless( $one_transaction->{trans_amt} =~ /(^(\d{0,6})(\.\d{1,2})?$)/ )
		{
			$invalid = 1;
			# Do this only if no error reported yet in this transaction.
			unless ($trans_error)
			{
				$invalid_report .= Display_Transaction($one_transaction, $trans_number);
				$trans_error = 1;
			}
			$invalid_report .= " *Amount";
		}
		# Date - A date in the defined range of the format yyyy-mm-dd.
		unless( ( $one_transaction->{db_date} =~ /^20\d{2}-\d{2}-\d{2}$/ ) &&
			(	$one_transaction->{db_date} ge $earliest_date &&
				$one_transaction->{db_date} le $latest_date ))
		{
			$invalid = 1;
			# Do this only if no error reported yet in this transaction.
			unless ($trans_error)
			{
				$invalid_report .= Display_Transaction($one_transaction, $trans_number);
				$trans_error = 1;
			}
			$invalid_report .= " *Date format/range";
		}
		# If we had a transaction error, add a newline.			
		if ($trans_error) { $invalid_report .= "<br />"; }
	}

	# Now we'll do the database checks, but only if there were no errors found above.
	unless ($invalid)
	{
		my ($sth, $rv, $count) = ();
		my $qry = '';
		$trans_number = 0;

		# Check every transaction in the transaction list.
		foreach $one_transaction (@transactions)
		{
			$trans_error = 0;
			$trans_number++;

			# Check to see if there is an ID match in the database.
			$qry = "SELECT count(*)
					FROM $MERCHANTS{$mal_type}{id_table}
					WHERE $MERCHANTS{$mal_type}{id_field_cv} = ?";
			$sth = $db->prepare($qry);
			$rv = $sth->execute($one_transaction->{clubbucks_id});

			# If there are no existing matches.
			unless ( $count = $sth->fetchrow_array() )
			{
				$invalid = 1;
				$trans_error = 1;
				$invalid_report .= Display_Transaction($one_transaction, $trans_number);
				$invalid_report .= " *$MERCHANTS{$mal_type}{id_phrase} does not exist.";
			}
			# If we had a transaction error, add a newline.			
			if ($trans_error) { $invalid_report .= "<br />"; }
		}
	}
	
	# If any transaction reported invalid, add the heading and footnotes to the report.
	if ($invalid)
	{
		$invalid_report = '<b><u>Invalid values reported:</u></b><br />'
		. "$invalid_report<br />"
		. '&nbsp;<b><u>TIPS for Data Formatting:</u></b><br />'
		. '&nbsp;Amount - Letters are not allowed, values up to 999999.99 only, no more than 2 decimal places.<br />'
		. '&nbsp;Date - Use the format mm-dd-yyyy (ex. 05-31-2003).<br />';
		$invalid_report .= ($mal_type == 5) ?
			'ClubBucks Number - DHS Club IDs cannot be used' :
			'DHS Club ID - ClubBucks Numbers cannot be used';
	}
}

###### Create a combo box based upon the parmeters passed in.
sub Create_Combo{
	my ($name, $list, $labels, $def) = @_;
	return $q->popup_menu(	-name		=> $name,
					-values	=> $list,
					-labels	=> $labels,
					-default	=> $def,
					-force		=> 'true');
}

###### Build a single transaction display record and pass it back.
sub Display_Transaction{

	# Get the array element holding the transaction and the number.
	my $the_transaction = shift;
	my $trans_number = shift;

	my $display = "Transaction # " . $trans_number . " - ";
	my $first_field = 1;
	my $field = '';
	
	foreach $field (@field_names)
	{
		# Don't include a comma before the first field value.
		if ($first_field) { $first_field = 0 }
		else { $display .= ", " }
		$display .= $the_transaction->{$field};
	}
	return $display;
}	

###### Build and output the page to the browser.
sub Do_Page{

	my $TMPL = shift;

	# Create a cookie to hold the selected currency code.
	my $currency_cookie = $q->cookie(	-name	=> 'currency_code',
						-value	=> $default_code,
						-expires => '+1y');
	# If error reports were generated by the Check functions, display those reports
	#  and put the submitted list in the Cumulative Transaction List text area to be corrected.
	if ( $incomplete_report || $invalid_report || $insert_report)
	{
		$message = $incomplete_report . $invalid_report . $insert_report;
		$TMPL =~ s/%%TRANS_LIST%%/$failed_submission_report/;
	}
	# Otherwise put the submitted report out to the browser in the header area.
	else
	{
		$message = $success_submission_report;
		$message_class = 'heading';
		$TMPL =~ s/%%TRANS_LIST%%//;
	}
	$TMPL =~ s/%%SUBMIT_ME%%/$ME\/$mal_id\/$md5h/;
	$TMPL =~ s/%%ID_HEADER%%/$MERCHANTS{$mal_type}{id_phrase}/g;
	$TMPL =~ s/%%MESSAGE%%/$message/;
	$TMPL =~ s/%%MESSAGE-CLASS%%/$message_class/;
	$TMPL =~ s/%%LOCATION_ID%%/$mal_id/g;
	$currency_menu = Get_Currencies();
	$TMPL =~ s/%%CURRENCY_MENU%%/$currency_menu/g;

	if ( $action eq 'refund' )
	{
		$TMPL =~ s/%%CLUBBUCKS_ID%%/$refund_clubbucks_id/g;
		$TMPL =~ s/%%PURCHASE_AMT%%/$orig_purchase_amt/g;
		$TMPL =~ s/%%NATIVE_CURRENCY_AMT%%/$orig_native_amt/g;
		$TMPL =~ s/%%NATIVE_CURRENCY_CODE%%/$orig_curr_code/g;
		$TMPL =~ s/%%ORIG_ID%%/$offset_id/g;
	}

	# Send the current currency exchange rate to the cookie.	
	print $q->header(-charset=>'utf-8', -cookie=>$currency_cookie);
	print $TMPL;
}

##### prints an error message to the browser.
sub Err{
	print $q->header(-charset=>'utf-8', -expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<div><b>", $_[0], "</b></div>";
	print '<p>Current server time: <tt>' . scalar (localtime) . ' PST</tt>';
	print $q->end_html();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# Get the Login ID and Merchant Affiliate info hash.
	( $Login_ID, $ma_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_maf'), 'maf' );
#warn Dumper($ma_info);
	# If the Login ID doesn't exist or has any non-digits send them to login again.
	if (!$Login_ID || $Login_ID =~ /\D/)
	{
		Err("You must <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> to use this application.");
		return 0;
	}

	# Split out the separate location id's from the list.
	@location_list = split (/\,/, $ma_info->{maflocations});
	
	return 1;
}

###### Get the currency codes & descriptions and build a drop-down menu.
sub Get_Currencies	
{
	my $qry = '';
	my $sth = '';
	my $data = '';
	my @list = ('', 'USD');	# Force a blank and USD to appear at the top of the menu.

	$qry = "	SELECT code,
				description
			FROM 	currency_codes
			WHERE 	active = 't'
			ORDER BY description;";
	$sth = $db->prepare($qry);
	$sth->execute();

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@list, $data->{code});
		$data->{close} ||= "current";
		$currency_hash{$data->{code}} = "$data->{description}";
	}
	$sth->finish;
	$currency_menu = Create_Combo('exchange_code', \@list, \%currency_hash, $default_code);
}

###### Get the path info.
sub Get_Path
{
	my $path = '';
	
	# Get any path info that may have been passed in.
	($path = $q->path_info()) =~ s/^\///;

	# Split out the hash value and the personal CBID number list.	
	($mal_id, $md5h) =  split /\//, $path;
	$md5h = $md5h || '';
	
	unless ( $mal_id )
	{
		# If it is a location login, recall the script with the single id in the path.	
		if ( $ma_info->{membertype} eq 'mal' )
		{
			$mal_id = $ma_info->{id};
		}
		# If it is a master login...
		elsif ( $ma_info->{membertype} eq 'maf' )
		{
			my $how_many_locations = scalar(@location_list);

			# If there is only one location, recall the script with the single id in the path.
			if ( $how_many_locations == 1 )
			{
				$mal_id = $location_list[0];
			}
			# Otherwise, display a list of locations to choose from.
			else
			{
				return $how_many_locations;
			}
		}
	}
	# If a location_id with non-numeric characters was passed, then we have an error.							
	elsif ( $mal_id =~ /\D/ )
	{
		Err("Corrupt Location ID.");
		return 0;
	}

	# Check to see if the mal_id is a legitimate id.
	# If the id passed is in the cookie's list.
	unless ( grep {/$mal_id/} @location_list )
	{
		Err("Location $mal_id does not exist in your records.");
		return 0;
	}

	###### at this time even unactivated or inactivated Locations will be included in the cookie list
	###### so let's verify the validity of this location
	my $sth = $db->prepare("	SELECT v.vendor_id
					FROM 	$TBL{vendors} v,
						$TBL{locations} l
					WHERE 	l.vendor_id=v.vendor_id
					AND 	v.status= 1
					AND 	l.id= $mal_id");
	$sth->execute;
	my $rv = $sth->fetchrow_array;
	$sth->finish;
	if ($rv)
	{
		return 1;
	}
	else
	{
		Err("Cannot identify a valid/active Location, please <a href='/cgi/maf/setup.cgi'>activate</a> your Merchant Account. ");
		return 0;
	}
}

###### Get the currency conversion rate.
sub Get_Rate
{
	my $date = shift;
	my $qry = '';
	if ( $date eq 'current' )
	{
		# Get the exchange rate based on the default currency code.
		$rate_code = $default_code;

		# Get the most recent, active rate.
		$qry = "	SELECT r.rate, r.date
				FROM	$TBL{rates} r
					INNER JOIN $TBL{codes} c
					ON r.code = c.code
				WHERE 	r.code = ?
				AND 	c.active = 't'
				ORDER BY r.date DESC
				LIMIT 1";
	}
	elsif ( $date eq 'refund' )
	{
		# Otherwise, get the date associated with the original transaction.
		$qry = "	SELECT enter_date::DATE			
				FROM	$TBL{transactions}
				WHERE record_id = ?";
		my $sth = $db->prepare($qry);
		my $rv = $sth->execute($offset_id);
		if ( defined $rv )
		{
			( $date, $rate_code ) = $sth->fetchrow_array();
#			$date = "'" . $date . "'";
		}
		else { $invalid_report .= "Couldn't retrieve the original transaction date." }
		$sth->finish();
		$rate_code = $orig_curr_code;

		# Get the most recent rate corresponding to the date found above.
		$qry = "	SELECT rate, date
				FROM	$TBL{rates}
				WHERE 	code = ?
				AND 	date::TEXT ~ '$date'
				ORDER BY date DESC
				LIMIT 1";
	}

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($rate_code);
	if ( $rv eq '1')
	{
		($conversion_rate, $date) = $sth->fetchrow_array();
	}
	else {
		$invalid_report .= "	Couldn't retrieve the conversion rate.<br />
			Rate Code: $rate_code<br />
			Date: $date";
	}
	$sth->finish();

	return 1;	
}
		
###### Get the vendor group code which indicates the location type.
sub Get_Vendor_Group
{
	my ($code) = $db->selectrow_array("
		SELECT v.vendor_group
		FROM 	$TBL{locations} l
		INNER JOIN $TBL{vendors} v
		ON l.vendor_id = v.vendor_id
		WHERE l.id = $mal_id;" );
	return $code;
}

###### Insert the transactions into the database.
sub Insert_Transactions
{
	my ($rv, $one_transaction, $converted_amt, $err) = ();

	$db->begin_work;
	my $sth = $db->prepare("
		INSERT INTO $TBL{'transactions'}
			(	mal_id,
				$MERCHANTS{$mal_type}{'id_field_it'},
				trans_amt,
				trans_date,
				native_currency_amt,
				native_currency_code,
				offset_id,
				notes
			)
			VALUES
			(	$mal_id,
				?,
				?,
				?,
				?,
				?,				
				?,				
				?
			)");

	# Insert every transaction in the transaction list.
	foreach $one_transaction (@transactions)
	{
		$converted_amt = $one_transaction->{'trans_amt'} / $conversion_rate;
		$sth->execute(
			$one_transaction->{'clubbucks_id'},
			$converted_amt,
			$one_transaction->{'db_date'},
			$one_transaction->{'trans_amt'},
			$rate_code,
			$offset_id,
			$trans_notes
		);
		
		if ($err = $db->err)
		{
			$db->rollback;
			return undef;
		} 
	}

	$db->commit;
	
	return 1;
}

###### Load the transactions from the textarea into an array of hashes.
sub Parse_Transactions{
			
	# Split out the triplets into an array.	
	my @trans_list = split /\n/, $trans_list;

	# Now split out the individual elements into an array of hashes.
	my $one_transaction = '';
	my $counter = 0;
	
	# For every transaction in the transaction list.
	foreach $one_transaction (@trans_list)
	{
		my @field_values = split(/,/, $one_transaction);
		my $field = '';	

		# Create an anonymous hashref, then fill it with the field values.
		$transactions = {};
		foreach $field (@field_names)
		{		
			$transactions->{$field} = shift @field_values;
			# Remove any leading or trailing blank spaces, as well as newline characters.
			$transactions->{$field} =~ s/^\s+//;
			$transactions->{$field} =~ s/\s+$//;
			# Remove any DHS or dhs prefixes from the CBID's.
			$transactions->{$field} =~ s/(^DHS)//i;
		}

		# Re-format the date for the database and put it in the hash.
		$transactions->{db_date} = Db_Date_Format('US', $transactions->{trans_date});
		# Uppercase the ID in case it is an alias.
		$transactions->{clubbucks_id} = uc($transactions->{clubbucks_id});

		# Put the hahsref onto an array.
		push @transactions, $transactions;
	}
}

###### Reformat a date into the database format for comparisons.
sub Db_Date_Format
{
	my $day = '';
	my $mon = '';
	my $year = '';

	# Read in the current date style and the date.
	my $date_style = shift;
	my $orig_date = shift;

	# If this is a U.S. style date.
	if ( $date_style eq 'US' )
	{
		$day = substr($orig_date, 3, 2);
		$mon = substr($orig_date, 0, 2);	
		$year = substr($orig_date, 6, 4);
	}
	else
	{
		Err("Db_Format_Date error - '$date_style' was not understood.");
	}

	# Put date in the database format and return it to the main program.
	my $new_date = "$year-$mon-$day";
	return $new_date;	
}		

###### Display a list of locations if we have more than one.
sub Report_Locations{
	my $html = '';
	my $counter = 0;

	# print a title for the browser window.
	print $q->header(-expires=>'now');
	my $local_style = qq|<!--
				.heading {
					font-size: 15px;
					font-style: normal;
					font-weight: bold;
					color: #000000;
					font-family:  Arial, Helvetica, sans-serif ;
					}

				.n {
					font-size: 11px;
					font-style: normal;
					color: #000000;
					font-family:  Arial, Helvetica, sans-serif ;
					}

				//-->|;
	print $q->start_html(-encoding=>'utf-8',
				-bgcolor => '#ffffff', 
				-title	=> 'Location List for Transaction Entry',
				-link	=> '#000000',
				-vlink	=> '#000FFF',
				-alink	=> '#FFF000',
				-style	=> $local_style);

	# print a simple page header.
	print qq!
		<table width=97% border="1" align="center" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF">
		<tr><td class="heading">
		<b>Location List for Transaction Entry</b>
		</td></tr>

		<tr><td>
		<table align="center" border="1" width="75%" cellpadding="3" cellspacing="0" bgcolor="#C9DACF">
 			<tr>
			<td nowrap><b> Location ID </b></td>
			<td nowrap><b> Location Name </b></td>
 			<td nowrap><b> Address </b></td>
 			<td nowrap><b> City </b></td>
			<td nowrap><b> State </b></td>
			<td nowrap><b> Country </b></td>
			</tr>!;

	# Prepare a query to get details about each location id that has active status.
	my $qry = "SELECT 	l.id,
				l.location_name,
				l.city, l.address1,
				l.state,
				l.country
			FROM 	$TBL{locations} l,
				$TBL{vendors} v			
			WHERE 	l.id = ?
			AND 	v.vendor_id = l.vendor_id
			AND	v.status = 1";
	my $sth = $db->prepare($qry);
			
	# Display a line for each location in the list.
	foreach my $location (@location_list)
	{
		# Get the detailed info for all active location listings.
		$sth->execute($location);
		if ( my $result = $sth->fetchrow_hashref() )
		{
			$counter++;
			$result->{address1} = $result->{address1} || '&nbsp;';
			$result->{city} = $result->{city} || '&nbsp;';
			$result->{state} = $result->{state} || '&nbsp;';
			$result->{country} = $result->{country} || '&nbsp;';
			print qq!
				<tr class="n">
				<td><a href="$ME/$result->{id}"> $result->{id} </a></td>
				<td>$result->{location_name}</td>
				<td>$result->{address1}</td>
				<td>$result->{city}</td>
				<td>$result->{state}</td>
				<td>$result->{country}</td>  
				</tr>!;
		}
	}
	unless ( $counter )
	{
		print qq!<tr><td colspan="6" align="center">
				You have no 'Active' locations at this time.
				</td></tr>!;
	}
	print qq!</td></tr></table></table>!;
	print $q->end_html();

	$sth->finish();
}

###### Display the transaction list in a report format.
sub Submission_Report
{
	my $field = '';
	my $line_break = '';
	my $one_transaction = '';
	my $converted_amt = 0;
	
	# Display the whole transaction list
	foreach $one_transaction (@transactions)
	{
		my $first_field = 1;
		foreach $field (@field_names)
		{
			# Don't include a comma before the first field value.
			if ($first_field) { $first_field = 0 }
			else
			{
				$success_submission_report .= ", ";
				$failed_submission_report .= ", ";
			}
			# If this is the amount field.				
			if ( $field eq 'trans_amt' )
			{
				# If this is a refund transaction.				
				if ( $action eq 'refund' )
				{
					# Prefix the amount with a refund note.				
					$success_submission_report .= "Refund - ";
					$failed_submission_report .= "- ";
				}
				# If this is US dollars, show the dollar sign.	
				if ( $rate_code eq 'USD' )
				{
					$success_submission_report .= "\$";
				}
				else
				{
					$success_submission_report .= "$rate_code";
				}
			}
			$success_submission_report .= $one_transaction->{$field};
			$failed_submission_report .= $one_transaction->{$field};

			# If not in US dollars and we have a conversion rate.	
			if ( $field eq 'trans_amt' && $rate_code ne 'USD' && $conversion_rate)
			{
				# Put the converted amount in the success report.	
				$converted_amt = $one_transaction->{$field} / $conversion_rate;
				# Format with 2 decimal points to match the db record if successful.
				$converted_amt = sprintf("%.2f", $converted_amt);
				$success_submission_report .= " = US\$$converted_amt";
			}
		}
		$success_submission_report .=  "<br />";
		$failed_submission_report .=  "\n";
	}
	# Remove the last newline.
	chop $failed_submission_report;
}


###### 07/11/03 Removed the possibility of entering a negative value on the transaction form
###### 		and added the refund functionality.
###### 07/16/03 Modified the insert query to use @note_list instead of a single
######  $trans_notes value, so the query wouldn't die with more than one transaction.
###### 08/05/03 Modified the location listing subroutine so that it won't show inactive locations.
###### 08/15/03 Added default values for $md5h and $trans_list. Also added an error message
######		where $rv == 0, in case no active locations are available.
###### 08/21/03 Added a subroutine that will accept a date in mm-dd-yyyy format,
######		and convert it into a yyyy-mm-dd format for the database. This reformatted date
######		will be held in the transaction->{db_date} hash variable(s). All comparisons with 
######		and insertions into the database will be done with this db_date value.
###### 09/02/03 Increased the allowable transaction dates from 7 days to 30 days per Dick.
###### 09/23/03 Added a message in case multiple locations exist, but none are active.
###### 10/09/03 Added the padding for a single digit day while creating a refund transaction.
###### 10/30/03 Added the currency conversion functionality for both original transactions 
######			and refund transactions.
###### 11/24/03 Modified Get_Rate sub so that it can also retrieve the rate for a refund.
######			Discontinued calculation of rate from passed parameter values.
###### 11/25/03 Added the variable $rate_code to carry either $default_code for new or
######			$orig_curr_code for refunds locally through the script.
###### 12/10/03 Added styles to the Report_Locations subroutine.
######		Changed foredays(future transactions) from 31 to 1 per Bill.
###### 01/23/04 changed the return test on the currency SQL
###### 03/22/04 In sub Get_Rate, moved the $qry build into the if-then-else statement (was right after)
######		and removed the date constraint from the current $qry.
###### 03/31/04 Added ::bigint to '$one_transaction->{clubbucks_id}::bigint' in the Check_Valid subroutine.
###### 04/27/05 Added "WHERE active = 't'" to get only active currencies in the sub Get_Currencies.
######		Also added "active = 't'" to the sub Get_Rate.
######		Also chopped the final \n from the $failed_submission_report.
###### 11/17/05 Added code to handle entering transactions using a DHS Club alias. Almost all changes
######		are based on the new hash %MERCHANTS which is based upon the merchant's vendor group.
###### 02/06/06 Changed prepare/execute query in Insert_Trancations to a "do" due to the 8.1 DB upgrade.
###### 08/09/07 various html tweaks, changed the substitution on the SQL when querying on cbid or alias to utilize
###### built in DBI quoting, also found that explicitly defining a cbid as bigint is no longer necessary
###### embellished the Tips on data failure to identify cbid or alias 
#      2010-004     Keith  I updated the regular expression that checks the date, the year was using 200\d{1}, 
#                   so I canged it to 20\d{2} so a year greater than 2009 will be accepted.
#      2010-09-13   Keith  I updated some of the error messages to include the activate link.
# 02/09/2011 g.baker postgres 9.0 port date/text casts
# 11/30/14 revised Insert_Transactions to make multiple statement executions since the newer version of DBI will not permit multiple prepared statements in one call
#		also made the default for $offset_id undef instead of the text string 'NULL' since we are using the value as a DBI insert instead of a text insert