#!/usr/bin/perl -w
###### ma-mail2-mysponsor.cgi
###### a simple little one-off to fulfill the need for a mailto link in the Merchant portal shell
###### created: 08/10	Bill MacArthur
###### last modified: 
###### the intent at this moment is for it to reside inside of cgi/maf which is merchant authentication protected

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use DB_Connect;

my $q = new CGI;
my $gs = new G_S;
my ($db, $ma) = ();

CheckLogin();
my $sp = $db->selectrow_hashref(
	"SELECT firstname || ' ' || lastname AS name, emailaddress FROM members WHERE id= ?", undef, $ma->{'spid'});
$sp->{'name'} = $q->escape($sp->{'name'});

print
	$q->header(-type=>'text/javascript', -charset=>'utf-8'),
	qq|var mail2 = 'mailto:"$sp->{'name'}" <$sp->{'emailaddress'}>';|;

End();

###### ######

sub CheckLogin
{
	( my $id, $ma ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ((! $id) || $id =~ /\D/ )
	{
		Err("A valid merchant login is required");
		End();
	}
	elsif ($ma->{membertype} ne 'ma')
	{
		Err('This resource is for the exclusive use of Merchants.');
		End();
	}
	exit unless $db = DB_Connect('generic');
	$db->{RaiseError} = 1;
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	print $q->header, $q->start_html('Error'), $q->div($_[0]), $q->end_html;
	End();
}
