#!/usr/bin/perl

=head1 NAME:
cancel_submit.cgi

	The submit program for merchant cancel options.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Accept the parameters submitted by the merchant cancel screen, then processes the cancelation requests.

=head1
PREREQUISITS:

	strict
	warnings
	FindBin
	lib
	DHSGlobals
	CGI
	DB
	DB_Connect
	G_S
	DHS::UrlSafe
	URI::Escape
	MerchantAffiliates
	MailTools

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use lib "$FindBin::Bin/../../cgi-lib";
use DHSGlobals;
use CGI;
use DB;
use DB_Connect;
use G_S;
use DHS::UrlSafe;
use URI::Escape;
use MerchantAffiliates;
use MailTools;

#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

=head1
GLOBALS

=head2
CLASSES:

	$db
	$MerchantAffiliates
	$CGI
	$DHS_UrlSafe
	$G_S

=cut

#our $ObjectName = SomeClass->new();
our $db = {};
my $MerchantAffiliates = {};


my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $G_S = G_S->new();

=head2
VARIABLES, HASHES, and ARRAYS:

	%url_query_strings
	HASH
	Used to hold different parameters passed
	back to the display screen.  These values
	are a looped through, and assigned to
	the "url_query_string" variable.
	
	$url_query_string
	STRING
	The Query String created from the "url_query_strings" hash.
	
	%messages
	HASH
	The Notices, Warnings, and Errors passed back to the display
	
	$merchant_information
	HASHREF
	This holds the merchant information to update.

=cut

my %url_query_strings = ();
my $url_query_string = '';
my %messages = ();
my $merchant_information = {};

my $url = $CGI->url();
$url =~ s/_submit\.cgi/\.cgi/;

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}

=head2
displayUnavailable

=head3
Description:

	Redirects the user to a page telling them they don't have permissions
	to view this screen, or this screen is unavailable.

=head3
Params:

	none

=head3
Returns:

	none

=cut
sub displayUnavailable
{
	
	print $CGI->redirect(-uri=>'/maf/payment/unavailable');
	
	exitScript();
	
}

####################################################
# Main
####################################################

unless ($db = DB_Connect('merchant_affiliates')){exit}
$db->{RaiseError} = 1;

eval{


	$MerchantAffiliates = MerchantAffiliates->new($db);
	
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	
	displayUnavailable() if $cookie->{ma_info}->{membertype} eq 'mal';
	
	$merchant_information = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{ma_info}->{id});
	
	$messages{params}->{discount_type} = $CGI->param('discount_type');
	
	$messages{params}->{discount_type} =~ s/\s//g;
		
	if(defined $messages{params}->{discount_type} && $messages{params}->{discount_type} =~ /^\d+$/)
	{
		if($messages{params}->{discount_type} == 15)
		{
			#I Should check for pending information, and remove it.
			
			
			
			#The merchants payment information is automatically nulled out in the Merchant Affiliates Class when discount_type 15 is selected.
			$merchant_information->{discount_type} = 15;
			$merchant_information->{special} = 0.1;
			$merchant_information->{exception_percentage_of_sale} = 0.0;
			
			my $subscription = {};
			$subscription->{status} = 'DOWNGRADE';
			$subscription->{description} = 'Downgraded by the Merchant. Merchant ID: ' . $cookie->{ma_info}->{id};
			
			
			my $updated = $MerchantAffiliates->updateMerchantMasterInformation($cookie->{ma_info}->{id}, $merchant_information, undef, undef, $subscription);
			
			
			
			if(! $updated)
			{
				$messages{warnings}->{not_updated} = ['Your information was not updated'];
			}
			else
			{
				$MerchantAffiliates->deleteMerchantPaymentPendingRecord($cookie->{ma_info}->{id});
				#my $blurb_updated = $MerchantAffiliates->adjustMerchantBlurbForCurrentPackageBenefits($cookie->{ma_info}->{id});
				$MerchantAffiliates->adjustMerchantBlurbForCurrentPackageBenefits($cookie->{ma_info}->{id});
				#my %returned_keywords_update = $MerchantAffiliates->adjustMerchantKewordsForCurrentPackageBenefits($cookie->{ma_info}->{id});
				$MerchantAffiliates->adjustMerchantKewordsForCurrentPackageBenefits($cookie->{ma_info}->{id});
				
#				$messages{notices}->{blurb_adjusted} = ['The blurb was truncated'] if $blurb_updated;
#				$messages{notices}->{keywords_adjusted} = ['The keywords were truncated.'] if (exists $returned_keywords_update{notices}->{max_number_of_keywords_exceeded} && $returned_keywords_update{notices}->{max_number_of_keywords_exceeded});
				
				$messages{notices}->{package_changed} = ['Your Merchant Package was changed to Free, and your Direct Discount percentage was changed to 10%.'];
			}
			
			
			
		}
		else
		{
			$messages{warnings}->{select_discount_type} = ['One of the cancel options must be selected.'];
		}
		
	}
	elsif(defined $messages{params}->{discount_type} && $messages{params}->{discount_type} =~ /^\w+$/ && $messages{params}->{discount_type} eq 'CANCEL')
	{
		$merchant_information->{status} = 0;
		my $updated = $MerchantAffiliates->updateMerchantMasterInformation($cookie->{ma_info}->{id}, $merchant_information);
		
		if($updated && $updated ne '0E0')
		{
			$MerchantAffiliates->updateVendorStatusByMerchantID($cookie->{ma_info}->{id}, $merchant_information->{status});
			$MerchantAffiliates->deleteMerchantPaymentPendingRecord($cookie->{ma_info}->{id});
			
			$MerchantAffiliates->setMembers();
			my $member_type_updated = $MerchantAffiliates->{Members}->updateMember($merchant_information->{member_id}, {membertype=>'s'});
			
			$messages{notices}->{canceled} = ['Your Merchant Account was canceled.'];
			$messages{notices}->{logout} = [1];
			
			if(! $member_type_updated || $member_type_updated eq '0E0')
			{
				
				use Data::Dumper;
				
				my %email = 
				(
					to=>'errors@dhs-club.com',
					subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
				);
				
				$email{text} = "A Merchant canceled their Merchant Account, but their membership type was not changed to shopper. \n\n merchant_information:\n" . Dumper($merchant_information);
				
				my $MailTools = MailTools->new();
				$MailTools->sendTextEmail(\%email);
				
				$messages{errors}->{script_error} = [$email{text}];
			}
			
			
		}
		else
		{
			$messages{warnings}->{merchant_was_not_changed} = ['We were not able cancel your Merchant Account, contact support.'];
		}
		
	}
	else
	{
		$messages{warnings}->{select_discount_type} = ['One of the cancel options must be selected.'];
	}
	
};
if($@)
{
	
	my $error_message = $@;
	my %email = 
	(
		to=>'errors@dhs-club.com',
		subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
	);
	
	$email{text} = "There was a fatal error canceling a merchants account. Error Message: $error_message \n\n merchant_information:\n" . Dumper($merchant_information);
	
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
	$messages{errors}->{script_error} = [$email{text}];
	
}


$messages{params}->{discount_type} = $messages{params}->{discount_type};

$url_query_strings{data} = $DHS_UrlSafe->encodeHashOrArray(\%messages);

foreach (keys %url_query_strings)
{
	$url_query_string .= '&' if $url_query_string;
	$url_query_string .= "$_=" . URI::Escape::uri_escape($url_query_strings{$_});
}

$url .= "?$url_query_string" if $url_query_string;

print $CGI->redirect(-uri=>"$url");


exitScript();

__END__

=head1
ASSOCIATED SCRIPTS

L<maf::cancel>

=head1
CHANGE LOG

	Date	Made By	Description of the Change

=cut


