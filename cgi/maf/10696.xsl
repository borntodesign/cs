<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
		media-type="text/html" />
		<xsl:include href = "/home/clubshop.com/cgi-templates/xsl/common.xsl" />
		<xsl:include href = "/home/clubshop.com/html/merchants/xsl/navigation.xsl" />
		<!-- xsl:include href = "/Users/khasely/workspace/Club_Shop/cgi-templates/xsl/common.xsl" / -->
	<xsl:template match="/">
	<html>
	<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - Title - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
      <xsl:value-of select="//lang_blocks/title"/>      
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
</title>

<meta name="title" content="clubshop rewards incentive and loyalty program"/>
<meta name="keywords" content="clubshop rewards incentive and loyalty program, reward popints"/>
<meta name="abstract" content="clubshop rewards incentive and loyalty program "/>
<meta name="coverage" content="worldwide"/>
<meta name="distribution" content="global"/>
<meta name="author" content="clubshop rewards incentive and loyalty program"/>
<meta name="design-development" content="carsten rieger"/>

<meta name="publisher" content="clubshop rewards incentive and loyalty program"/>
<meta name="company" content="clubshop"/>
<meta name="copyright" content="copyright © 2009 clubshop rewards incentive and loyalty program. all rights reserved"/>
<meta name="page-topic" content=" clubshop rewards incentive and loyalty program "/> 
<meta name="robots" content="index,follow"/>
<meta name="rating" content="all"/>
<meta name="audience" content="general"/>
<meta name="revisit-after" content="7 days"/>

<link href="/css/club-rewards-general.css" rel="stylesheet" type="text/css" />
<link href="/css/maf/update_payment.css" rel="stylesheet" type="text/css" />

<script src="/js/resource.js" type="text/javascript"></script>
<script src="/js/state_list.js" type="text/javascript"></script>
<script src="/js/getstates.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/js/jquery-plugins/jquery.maxlength.js"></script>
<script type="text/javascript" src="/js/maf/paymen_package_options.js"></script>
<script type="text/javascript" src="/js/maf/update_payment.js"></script>
<script type="text/javascript" src="/js/tooltip.js"></script>
  <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<style>
		.tip_trigger{cursor:help;}
		
		.tip {
			color: #FFF;
			background:#00376F;
			display:none; /*--Hides by default--*/
			padding:10px;
			position:absolute;    z-index:1000;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			width: 225px;
			text-align:left;
		}
   
</style>
<style type="text/css">
body {
background-color:white;
background-image:none;
}
</style>

</head>

<body bgcolor="#FFFFFF">


                <div align="center">
              		<span class="style12">
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!--  This is where the page header goes. -->
					<xsl:value-of select="//lang_blocks/heading_payment_package_update"/>
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
	            	</span>
              	<span class="Text_Body_Content">
              		<!-- br / -->
                    <!-- br / -->
                    <!-- Blah Blah Blah goes here -->
                </span>
              </div>
              	
            	
	            <br />
				<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">
              
              
                	<tbody>
                  		<tr>
                    		<td colspan="3" align="center" valign="top" bgcolor="#FCA641" class="Text_Body_Content" background="/images/bg/bg_header-orange-923x23.png">
                    			<span class="style13"><!-- xsl:value-of select="//lang_blocks/form_heading" / --></span>
                    			<br />
                    		</td>
                    	</tr>            
            			<tr>
                    		<td colspan="3" align="left" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content">
                    
                    
				<xsl:if test="//data/errors/*">
					<div align="left">
						<xsl:call-template name="output_errors" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>
				<xsl:if test="//data/warnings/*">
					<div align="left">
						<xsl:call-template name="output_warnings" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>
				<xsl:if test="//data/notices/*">
					<div align="left">
						<xsl:call-template name="output_notices" />
					</div>
					<hr align="left" width="100%" size="2" color="#FCBF12" />
				</xsl:if>

						<span class="style5">
			            	<xsl:value-of select="//lang_blocks/required_fields" />
			            </span>
                
                <hr align="left" width="100%" size="2" color="#FCBF12" />
                                	            <span class="Text_Body_Content">
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<form id="form1" name="form1" method="post" action="/cgi/maf/update_payment.cgi">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
						<td colspan="2">
<xsl:if test="//params/discount_type &gt; 0 and //params/discount_type &lt; 4">
	<span class="style5"><xsl:value-of select="//lang_blocks/legacy_package" /></span>

					  <table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<th><xsl:value-of select="//lang_blocks/merchant_type" /></th>
							<th width="65%"><xsl:value-of select="//lang_blocks/benefits" /></th>
							<th width="15%"><xsl:value-of select="//lang_blocks/initial_fee" /></th>
							<th></th>
						</tr>

<xsl:if test="//params/discount_type=1">
						<tr>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/bronze/merchant_type_off_bronze" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/bronze/benefits_off_bronze" /></td>
							<td valign="top">
								
								
								<xsl:value-of select="//menus/converted/offline/bronze" />
								
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="offline_bronze" value="1" >
									<xsl:if test="//params/discount_type=1"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
								</input>
							</td>
						</tr>
</xsl:if>
<xsl:if test="//params/discount_type=2">
						<tr>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/silver/merchant_type_off_silver" /></td>
							
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/silver/benefits_off_silver" /></td>
							<td valign="top">
								<xsl:value-of select="//menus/converted/offline/silver" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="offline_silver" value="2" >
									<xsl:if test="//params/discount_type=2"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
								</input>
							</td>
						</tr>
</xsl:if>
<xsl:if test="//params/discount_type=3">
						<tr>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/gold/merchant_type_off_gold" /></td>
							
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/gold/benefits_off_gold" /></td>
							<td valign="top">
								<xsl:value-of select="//menus/converted/offline/gold" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="offline_gold" value="3" >
									<xsl:if test="//params/discount_type=3"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
								</input>
							</td>
						</tr>
						</xsl:if>
						</table>
<hr align="left" color="#fcbf12" size="2" width="100%" />
						</xsl:if>
					  <table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<th align="left"><xsl:value-of select="//lang_blocks/merchant_type" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/discount" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/bogo" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/ff" /></th>
							<th align="left"><xsl:value-of select="//lang_blocks/type_of_discount" /></th>							
							<th align="right"><xsl:value-of select="//lang_blocks/initial_fee" /></th>
							<th align="right"><xsl:value-of select="//lang_blocks/monthly_fee" /></th>
							<th></th>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/merchant_type_free" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/benefits_free" />
                                                                         </span></a></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/free/discount_free" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/free" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/free" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_free" value="15" >
									<xsl:if test="//params/discount_type=15">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline/free/merchant_type_free" />
                                                                   <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline/free/benefits_free" />
                                                                   </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline/free/discount_free" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td>

							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/tracked" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline/free" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline/monthly" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="offline_free" value="0">
									<xsl:if test="//params/discount_type=0"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/merchant_type_off_basic" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/benefits_off_basic" />
                                                                         </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/basic/discount_basic" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/no" /></td>

							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/basic" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/basic" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_basic" value="14" >
									<xsl:if test="//params/discount_type=14">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/merchant_type_off_bronze" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/benefits_off_bronze" />
                                                                         </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/bronze/discount_bronze" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/bronze" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/bronze" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_bronze" value="10" >
									<xsl:if test="//params/discount_type=10">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/merchant_type_off_silver" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/benefits_off_silver" />
                                                                         </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/silver/discount_silver" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/silver" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/silver" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_silver" value="11" >
									<xsl:if test="//params/discount_type=11">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/merchant_type_off_gold" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/benefits_off_gold" />
                                                                         </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/gold/discount_gold" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/gold" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/gold" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_gold" value="12" >
									<xsl:if test="//params/discount_type=12">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td valign="top"><a class="tip_trigger" href="#"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/merchant_type_off_platinum" />
                                                                         <span class="tip" style="display: none; top: 234px; left: 231px;">
                                                                         <xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/benefits_off_platinum" />
                                                                         </span></a>
                                                        </td>
							<td valign="top"><xsl:value-of select="//lang_blocks/merchant_types/offline_rewards/platinum/discount_platinum" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/yes" /></td>
							<td valign="top"><xsl:value-of select="//lang_blocks/type_of_discounts/direct" /></td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/platinum" />
							</td>
							<td valign="top" align="right">
								<xsl:value-of select="//menus/converted/offline_rewards/monthly/platinum" />
							</td>
							<td valign="top">
								<input type="radio" name="discount_type" id="rewards_merchant_platinum" value="13" >
									<xsl:if test="//params/discount_type=13">
										<xsl:attribute name="checked">checked</xsl:attribute>
									</xsl:if>
								</input>
							</td>
						</tr>
						<tr>
							<td colspan="8" class="small">
								<xsl:value-of select="//lang_blocks/index/bogo" />
								<br />
								<xsl:value-of select="//lang_blocks/index/ff" />
							</td>
						</tr>
						</table>
						<hr align="left" color="#fcbf12" size="2" width="100%" />
						
						









<!-- Reward Non Tracking Merchant Discount Options -->
               <table width="80%" align="center" id="free_reward_merchant_benefits" ><!-- class="hide" -->
				<tr>

					<td valign="top">
						<xsl:value-of select="//lang_blocks/instant_percentage_of_sale_off" />:
					</td>
                                      	<td valign="top">
                                      	<div align="right">
                                      		<xsl:call-template name="build_menu">
							<xsl:with-param name="fieldname" select="'percentage_off'" />
							<xsl:with-param name="id" select="'percentage_off'" />
							<xsl:with-param name="class" select="'registration_dropmenu'" />
							<xsl:with-param name="default_value"></xsl:with-param>
							<xsl:with-param name="natural_sort" select="'1'" />
							<xsl:with-param name="disabled"><xsl:if test="not(//params/percentage_off)">disabled</xsl:if></xsl:with-param>
						</xsl:call-template>
						
<br />
<span class="small"><xsl:value-of select="//lang_blocks/buy_one_get_one_explanation" /></span>
                                    	  </div></td>
                         <td valign="top">
				<input type="radio" name="discount_subtype" id="discount_subtype" value="2" >
					<xsl:if test="//params/discount_subtype = '2'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
			  </td>
		</tr>
		<tr id="free_reward_merchant_benefits_flat_fee" > <!-- class="hide" -->
                          <td valign="top">
                               	<xsl:value-of select="//lang_blocks/fields/flat_fee_off" />:
                          </td>
                          <td valign="top">
                              <div align="right">
               			   <xsl:call-template name="build_input">
					<xsl:with-param name="fieldname" select="'flat_fee_off'" />
					<xsl:with-param name="id" select="'flat_fee_off'" />
					<xsl:with-param name="class" select="'registration_TextField'" />
					<xsl:with-param name="maxlength" select="'10'" />
					<xsl:with-param name="default_value"></xsl:with-param>
					<xsl:with-param name="disabled"><xsl:if test="not(//params/flat_fee_off)">disabled</xsl:if></xsl:with-param>
				    </xsl:call-template>
			           <br />
                                   <span class="small"><xsl:value-of select="//lang_blocks/flat_fee_off_explanation" /></span>
                               </div></td>
                          <td valign="top">
				<input type="radio" name="discount_subtype" id="discount_subtype" value="1" >
					<xsl:if test="//params/discount_subtype = '1'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
                          </td>
                      </tr>
                      <tr id="bogo_main">
                          <td valign="top">
                               	<xsl:value-of select="//lang_blocks/special" />:
                          </td>
                          <td>
                               <div align="right">
                               		<xsl:call-template name="build_menu">
						<xsl:with-param name="fieldname" select="'special'" />
						<xsl:with-param name="id" select="'special'" />
						<xsl:with-param name="default_value"></xsl:with-param>
						<xsl:with-param name="class" select="'registration_dropmenu'" />
						<xsl:with-param name="natural_sort" select="'1'" />
						<xsl:with-param name="disabled"><xsl:if test="not(//params/special)">disabled</xsl:if></xsl:with-param>
					</xsl:call-template>
					<br />
                                            <span class="small"><xsl:value-of select="//lang_blocks/buy_one_get_one_explanation" /></span>
                                   </div>
                          </td>
                                        <td valign="top">
						<input type="radio" name="discount_subtype" id="discount_subtype" value="3" >
							<xsl:if test="//params/discount_subtype = '3'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
						</input>
                                        </td>

</tr>
                                      </table>
                                      
</td>
</tr>
<tr>
<td colspan="2">

<hr align="left" width="100%" size="2" color="#FCBF12" />

<table width="80%" border="0" cellpadding="2" cellspacing="0" align="center">

			<tr>
				<td width="50%" align="left" valign="top">
					<span>
						<xsl:call-template name="build_label">
							<xsl:with-param name="fieldname" select="'discount_blurb'" />
						</xsl:call-template>
					</span>
				</td>
				<td width="50%" align="right">

					<textarea name="discount_blurb" id="discount_blurb" class="registration_TextField" rows="5">
                          		<xsl:attribute name="maxlength"><xsl:value-of select="//discount_blurb/maximum_characters" /></xsl:attribute>
                    <xsl:value-of select="//params/discount_blurb" />
					</textarea>

					<p><xsl:value-of select="//lang_blocks/blurb_characters_left" />: <span class="charsLeft"><xsl:value-of select="//discount_blurb/maximum_characters" /></span></p>

				</td>
			</tr>

</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0" align="center" id="payment_types">	

                       <tr>
                        <td colspan="2">
				<hr align="left" width="100%" size="2" color="#FCBF12" />
                        </td>
                        </tr>

                    		
                    		                        <tr>
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'payment_method'" />
								<xsl:with-param name="add_class" select="'style5'" />
							</xsl:call-template>
							
						  </td>
                          <td width="50%" align="right">
							<xsl:call-template name="build_menu">
								<xsl:with-param name="fieldname" select="'pay_payment_method'" />
								<xsl:with-param name="id" select="'pay_payment_method'" />
								<xsl:with-param name="use_nodeset" select="'payment_method'" />
								<xsl:with-param name="class" select="'registration_dropmenu'" />
								<xsl:with-param name="onchange" select="'paymentRequirements(this);'" />
							</xsl:call-template>
                          </td>
                        </tr>

<tr>
<td colspan="2" width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="2" id="credit_card_payment_options">
                        <tr>
                          <td width="50%" align="left" >
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_type'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
							</xsl:call-template>
						  </td>
                          <td width="50%" align="right">
	                          	<xsl:call-template name="build_menu">
						<xsl:with-param name="fieldname" select="'cc_type'" />
						<xsl:with-param name="id" select="'cc_type'" />
					<!-- xsl:with-param name="onchange" select="'getStates()'" / -->
						<xsl:with-param name="class" select="'registration_dropmenu'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr>
                          <td align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_number'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td align="right">
	                          	<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_cardnumber'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'16'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        <tr>
                          <td align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'ccv_number'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
							</xsl:call-template>
						  </td>
                          <td align="right">
	                          	<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_cardcode'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'4'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        <tr>
                          <td align="left">
	                          <xsl:call-template name="build_label">
					<xsl:with-param name="fieldname" select="'cc_expiration_date'" />
					<xsl:with-param name="add_class">style5</xsl:with-param>
				   </xsl:call-template>
			  </td>
                          <td align="right">
					<xsl:call-template name="build_menu">
						<xsl:with-param name="fieldname" select="'pay_cardexp'" />
						<xsl:with-param name="use_nodeset" select="'cc_exp'" />
						<xsl:with-param name="class" select="'registration_dropmenu'" />
						<xsl:with-param name="natural_sort" select="'yes'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr>
                          <td align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_name_on_card'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td align="right">
	                          	<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_name'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'100'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left"><hr align="left" width="100%" size="2" color="#FCBF12" /></td>
                        </tr>
                        <tr>
                          <td align="left">
		                        <xsl:call-template name="build_label">
						<xsl:with-param name="fieldname" select="'cc_address'" />
						<xsl:with-param name="add_class">style5</xsl:with-param>
					</xsl:call-template>
			  </td>
                          <td align="right">
	                          	<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_address'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'100'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        
                        <tr>
                          <td align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_city'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
								
							</xsl:call-template>
						  </td>
                          <td align="right">
	                          	<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_city'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'50'" />
					</xsl:call-template>
                          </td>
                        </tr>
                        <tr>
                          <td align="left">
                          	<xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'cc_country'" />
								<xsl:with-param name="add_class">style5</xsl:with-param>
							</xsl:call-template>
                          </td>
                          <td align="right">
					<xsl:call-template name="build_menu">
						<xsl:with-param name="fieldname" select="'pay_country'" />
						<xsl:with-param name="id" select="'country'" />
						<xsl:with-param name="onchange">getStates('cc_state')</xsl:with-param> 
						<xsl:with-param name="class" select="'registration_dropmenu'" />
						<xsl:with-param name="use_nodeset" select="'country'" />
					</xsl:call-template>
                          </td>
                        </tr>
			<tr>
				<td align="left">
					<xsl:call-template name="build_label">
						<xsl:with-param name="fieldname" select="'cc_state'" />
						<xsl:with-param name="add_class">style5</xsl:with-param>
					</xsl:call-template>
				</td>
				<td align="right">
	                          	<xsl:call-template name="build_dynamic_state">
						<xsl:with-param name="fieldname" select="'pay_state'" />
						<xsl:with-param name="onfocus" select="'needs_country()'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
				<!-- if country wasn't required, then we would do the following instead -->
				<!--xsl:with-param name="onchange" select="'getStates()'" /-->
					</xsl:call-template>
                	        </td>
                        </tr>
                        <tr>
                         	 <td align="left">
		                        <xsl:call-template name="build_label">
						<xsl:with-param name="fieldname" select="'cc_postalcode'" />
						<xsl:with-param name="add_class">style5</xsl:with-param>
					</xsl:call-template>
				  </td>
                        	  <td align="right">
                          		<xsl:call-template name="build_input">
						<xsl:with-param name="fieldname" select="'pay_postalcode'" />
						<xsl:with-param name="class" select="'registration_TextField'" />
						<xsl:with-param name="maxlength" select="'20'" />
					</xsl:call-template>
                          	</td>
                        </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" id="club_account_payment_options">
<xsl:choose>
	<xsl:when test="//params/ca_number and //params/ca_number_annual">
                        <tr>
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'monthly_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/paying_firstname" /><xsl:text> </xsl:text><xsl:value-of select="//params/paying_lastname" /> - <xsl:value-of select="//params/ca_number" />
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			
				<input type="radio" name="ca_number">
					<xsl:attribute name="value"><xsl:value-of select="//params/member_id" /></xsl:attribute>
					<xsl:if test="//params/ca_number = ''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          </td>
						</tr>
						<tr style="height: 6px"><td></td></tr>
                        <tr bgcolor="#fdebd6">
                          <td width="50%" align="left" id="clubAccountNumber2" style="padding-left: 4px;">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'annual_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/paying_firstname" /><xsl:text> </xsl:text><xsl:value-of select="//params/paying_lastname" /> - <xsl:value-of select="//params/ca_number_annual" />
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<input type="radio" name="ca_number_annual">
					<xsl:attribute name="value"><xsl:value-of select="//params/ca_number_annual" /></xsl:attribute>
					<xsl:if test="//params/ca_number_annual"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

                          </td>
						</tr>
						<tr bgcolor="#fdebd6">
                          <td width="50%" align="left">
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/firstname1" /><xsl:text> </xsl:text><xsl:value-of select="//params/lastname1" /> - <a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<input type="radio" name="ca_number_annual">
					<xsl:attribute name="value"><xsl:value-of select="//params/member_id" /></xsl:attribute>
					<xsl:if test="//params/cca_number_annual = ''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

                          </td>
						</tr>
	</xsl:when>
	<xsl:when test="//params/ca_number">
                        <tr id="clubAccountNumberRow">
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'annual_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/firstname1" /><xsl:text> </xsl:text><xsl:value-of select="//params/lastname1" /> - <a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          </td>
						</tr>
						<tr>
							<td colspan="2" style="height: 10px;"> </td>
						</tr>
                        <tr bgcolor="#fdebd6">
                          <td width="50%" align="left" style="padding-left: 4px;">
                          
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'monthly_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/paying_firstname" /><xsl:text> </xsl:text><xsl:value-of select="//params/paying_lastname" /> - <xsl:value-of select="//params/ca_number" />
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<input type="radio" name="ca_number" id="ca_number">
					<xsl:attribute name="value"><xsl:value-of select="//params/ca_number" /></xsl:attribute>
					<xsl:if test="//params/ca_number"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          </td>
						</tr>
                        <tr bgcolor="#fdebd6">
                          <td width="50%" align="left">
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/firstname1" /><xsl:text> </xsl:text><xsl:value-of select="//params/lastname1" /> - <a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<input type="radio" name="ca_number" id="ca_number">
					<xsl:attribute name="value"><xsl:value-of select="//params/member_id" /></xsl:attribute>
					<xsl:if test="//params/ca_number = ''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				
                          </td>
						</tr>

	</xsl:when>
	<xsl:when test="//params/ca_number_annual">
                        <tr id="clubAccountNumberRow">
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'monthly_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/firstname1" /><xsl:text> </xsl:text><xsl:value-of select="//params/lastname1" /> - <a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          </td>
						</tr>
						<tr>
							<td colspan="2" style="height: 10px;"> </td>
						</tr>
                        <tr  bgcolor="#fdebd6">
                          <td width="50%" align="left" id="clubAccountNumber2"  style="padding-left: 4px;">
                         
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'annual_payment_club_account_number'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/paying_firstname" /><xsl:text> </xsl:text><xsl:value-of select="//params/paying_lastname" /> - <xsl:value-of select="//params/ca_number_annual" />
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<input type="radio" name="ca_number_annual" id="ca_number_annual">
					<xsl:attribute name="value"><xsl:value-of select="//params/ca_number_annual" /></xsl:attribute>
					<xsl:if test="//params/ca_number_annual"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
				</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				
                          </td>
						</tr>
						<tr bgcolor="#fdebd6">
                          <td width="50%" align="left">
			 		 	  </td>
                          <td width="50%" align="right">
                          	<xsl:value-of select="//params/firstname1" /><xsl:text> </xsl:text><xsl:value-of select="//params/lastname1" /> - <a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<input type="radio" name="ca_number_annual" id="ca_number_annual">
						<xsl:attribute name="value"><xsl:value-of select="//params/member_id" /></xsl:attribute>
						<xsl:if test="//params/cca_number_annual = ''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
					</input>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
				
                          </td>
						</tr>
	</xsl:when>
	<xsl:otherwise>
                        <tr>
                          <td width="50%" align="left">
                          <xsl:call-template name="build_label">
								<xsl:with-param name="fieldname" select="'ca_number_update'" />
								<xsl:with-param name="id" select="'clubAccountNumber'" />
							</xsl:call-template>
			 		 	  </td>
                          <td width="50%" align="right">
                          	<a class="nav_orange" href="/cgi/funding.cgi?action=report" target="_blank"><xsl:value-of select="//params/member_id" /></a>
                          	                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          </td>
						</tr>
</xsl:otherwise>
</xsl:choose>
<!--   -->
                        <tr>
                          <td colspan="2" align="left">
                          	<xsl:if test="//params/ca_number or //params/ca_number_annual">
                          		<br />
                          		<xsl:value-of select="//payment_explanations/update_club_account_self_pay" />
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                          	</xsl:if>
                          	<br />
                          	<br />
                          	<xsl:value-of select="//payment_explanations/update_club_account" /><xsl:text> </xsl:text><a class="nav_orange" href="/cgi/converter.cgi">https://www.clubshop.com/cgi/converter.cgi 
						  </a> <xsl:text>, </xsl:text><xsl:value-of select="//payment_explanations/update_club_account2" />


                          </td>
                        </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" id="international_funds_transfer_payment_options">
                        <tr>
                          <td colspan="2" align="left">
                          	<xsl:value-of select="//payment_explanations/update_int_fund_transfer" />
						  </td>
                        </tr>
                        
                        <tr>
                          <td colspan="2" align="left"><script type="text/javascript"><xsl:comment>
paymentRequirements(document.getElementById("pay_payment_method"));
//</xsl:comment></script>
                          	<hr align="left" width="100%" size="2" color="#FCBF12" />
                          </td>
                        </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="2">
				
				<input type="hidden" name="merchant_type">
					<xsl:attribute name="value">
						<xsl:value-of select="//params/merchant_type" />
					</xsl:attribute>
				</input>
				<input type="hidden" name="country">
					<xsl:attribute name="value">
						<xsl:value-of select="//params/country" />
					</xsl:attribute>
				</input>
                    		</td>
                    	</tr>
                    	<tr>
                    		<td align="center" bgcolor="#FCA541" colspan="2" style="height: 27px;">
                          		<input type="image" id="Submit" value="Submit" alt="Submit" src="/images/btn/btn_submit-91x27.png" />
                    		</td>
                    	</tr>
                    </table>
				</form>

					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
					<!-- - - - - - - - - - - - - - - - - - - - - - - - - -->
	            </span>
              
           
            <br />
            </td>
            </tr>
            </tbody>
            </table>
            
            <br />
            
</body>
</html>
</xsl:template>
</xsl:stylesheet>
