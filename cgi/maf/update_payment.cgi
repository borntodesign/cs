#!/usr/bin/perl

=head1 NAME:
update_payment.cgi

	This script is used to update the Merchants Payment Information, and Merchant Package.  
	It allows them to switch Payment Methods, and upgrade, or downgrade their Merchant Package.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
use CGI;
use lib '/home/httpd/cgi-lib';
use DB;
use DB_Connect;
use G_S;
use XML::local_utils;
use XML::Simple;
use CGI::Cookie;
use CreditCard;
require MerchantAffiliates;
use MailTools;
use DHSGlobals;
use Notifications;
use Data::Dumper;
use CGI::Carp 'fatalsToBrowser';

my %TMPL = (
	'xml' => 10680,
	'xsl' => 10930,
	'countries' => 10407,
	'config' => 10681
);
our $CGI = CGI->new();
my $url = $CGI->self_url();

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

our $langauges_xml = '';
our $languages_ref = {};
our $countries_xml = '';

####################################################
# Main
####################################################

exit unless my $db = DB_Connect('merchant_update_payment');
$db->{'RaiseError'} = 1;

my $G_S = G_S->new({'db'=>$db});
my $MerchantAffiliates = MerchantAffiliates->new($db);

my $xml = $G_S->GetObject($TMPL{'xml'}) || die "Failed to load XML template: $TMPL{'xml'}";
my $xsl = $G_S->GetObject($TMPL{'xsl'}) || die "Failed to load XSL template: $TMPL{'xsl'}";

if ($url =~ /admin/)
{
	$xsl =~ s/%%bodycolor%%/eeffee/g;
}
else
{
	$xsl =~ s/%%bodycolor%%/ffffff/g;
}

my $configuration_xml = $G_S->GetObject($TMPL{'config'}) || die "Failed to load CONFIG template: $TMPL{'config'}";
	
if(! $CGI->param('updated') && $CGI->param && ! $CGI->param('edit'))
{
	eval{
		processApplication();
	};
	if($@)
	{
		my $error = $@;
		#warn $FindBin::RealScript . ' - ' . $error;
		
		my $MailTools = MailTools->new();
		my $message = "Error: $error \n\nThere was an error in processApplication()";
		$MailTools->sendTextEmail({
			'from'=>'errors@dhs-club.com',
			'to'=>'errors@dhs-club.com',
			'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: Main",
			'text'=>$message
		});
								
		displayUnavailable();
	}
	
}
else
{
	eval{
		displayApplication();
	};
	if($@)
	{
		
		my $error = $@;
		
		my $message = $FindBin::RealScript . ' :: displayApplication()  - ' . $error;
		
		#warn $message;
		
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail({
			'from'=>'errors@dhs-club.com',
			'to'=>'errors@dhs-club.com',
			'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: Main",
			'text'=>$message
		});
				
		displayUnavailable();
	}
}

exitScript();


sub exitScript
{
	$db->disconnect if $db;
	exit;
}


sub displayApplication
{
	my $merchant_master_record_hashref = shift;

	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
	my $errors = shift;
	
	my $errors_xml = '';
	my $notices_xml = '';
	
	my $cc_expiration_dates_xml = $G_S->buildCreditCardExpirationDate();
	
#
# $configuration_xml is the 10681 cgi containing all data necessary for 
#warn "app config:\n$configuration_xml";
#
	my $application_configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	my $exchange_rates = {};
	
	$countries_xml = $G_S->GetObject($TMPL{'countries'}) || die "Failed to load COUNTRIES template: $TMPL{'countries'}";
	
	my $params = '';

	my $information_hashref_arrayref = {};
	my	$output_xml = '';
#	
# If there was no information passed as a parameter into this sub for "merchant_master_record_hashref"
# retrieve the merchant master record, and member information.
#
	if (!$merchant_master_record_hashref)
	{
		my $merchant_member_info = {};
		my $paying_sponsors_info = {};
		
		eval{
			
#			$MerchantAffiliates = MerchantAffiliates->new($db);
			
#			my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			if ($CGI->param('edit') && $CGI->param('id'))
			{
			
			    $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($CGI->param('id'), {'with_pending_payment_information'=>1});
			}
			else
			{
			    displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
			
			    $merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'},{'with_pending_payment_information'=>1});
			}
			
			$MerchantAffiliates->setMembers() if ! $MerchantAffiliates->{'Members'};
			
			$merchant_member_info = $MerchantAffiliates->{'Members'}->getMemberInformationByMemberID({'member_id'=>$merchant_master_record_hashref->{'member_id'}});
			
			if($merchant_master_record_hashref->{'ca_number'} || $merchant_master_record_hashref->{'ca_number_annual'})
			{
				my $id_to_get = $merchant_master_record_hashref->{'ca_number'} ? $merchant_master_record_hashref->{'ca_number'} : $merchant_master_record_hashref->{'ca_number_annual'};
				
				$paying_sponsors_info = $MerchantAffiliates->{'Members'}->getMemberInformationByMemberID({'member_id'=>$id_to_get});
				
				$merchant_master_record_hashref->{'paying_firstname'} = $paying_sponsors_info->{'firstname'};
				$merchant_master_record_hashref->{'paying_lastname'} = $paying_sponsors_info->{'lastname'};
				
			}
#
# Load the data necessary for the discounts
#
			$information_hashref_arrayref->{'discount_subtype'} = $merchant_master_record_hashref->{'discount_subtype'};
			
			if ($merchant_master_record_hashref->{'discount_subtype'} == 1)
			{
				$information_hashref_arrayref->{'flat_fee_off'} = [$merchant_master_record_hashref->{'special'}];
			}
			elsif ($merchant_master_record_hashref->{'discount_subtype'} == 2)
			{
			    if ($merchant_master_record_hashref->{'discount'} > 0.0)
			    {
			        $information_hashref_arrayref->{'percentage_off'} = [$merchant_master_record_hashref->{'special'}];
			    }
			    else
			    {
			        $information_hashref_arrayref->{'percentage_off'} = [$merchant_master_record_hashref->{'special'}];
			    }
			}
			else
			{
			    $information_hashref_arrayref->{'bogo_amt'} = [$merchant_master_record_hashref->{'special'}];
			}


#			if($merchant_master_record_hashref->{'special'})
#			{
#				
#				
#				if ($merchant_master_record_hashref->{'special'} =~ /\W/g)
#				{
#					
#					if ($merchant_master_record_hashref->{'special'} =~ /\W\W/g)
#					{
#						foreach (keys %{$application_configuration->{other_off}})
#						{
#							if ($merchant_master_record_hashref->{'special'} eq $application_configuration->{other_off}->{$_}->{val})
#							{
#								$information_hashref_arrayref->{other_off} = [$merchant_master_record_hashref->{'special'}];
#								last;
#							}
#						}
#					}
#					else
#					{
#						$merchant_master_record_hashref->{'special'} =~ s/0*$//;
#						foreach (keys %{$application_configuration->{'percentage_off'}})
#						{
#							if ($merchant_master_record_hashref->{'special'} eq $application_configuration->{'percentage_off'}->{$_}->{val})
#							{
#								$information_hashref_arrayref->{'percentage_off'} = [$merchant_master_record_hashref->{'special'}];
#								last;
#							}
#						}
#					}
#					
#				}
#				else
#				{
#					
#					
#					foreach (keys %{$application_configuration->{'flat_fee_off'}})
#					{
#						if ($merchant_master_record_hashref->{'special'} eq $application_configuration->{'flat_fee_off'}->{$_}->{val})
#						{
#							$information_hashref_arrayref->{'flat_fee_off'} = [$merchant_master_record_hashref->{'special'}];
#							last;
#						}
#					}
#					
#				}
#		    }
			
		};
		if($@)
		{
			my $error = $@;
			
			warn $FindBin::RealScript . ' - ' . $error;
			
			print $CGI->header('text/plain');
			
			print "A fatal error occured, contact Technical Support support\@dhs-club.com.\n";
			
			my $MailTools = MailTools->new();
			
			my $message = "\nError: \n $error \n\n";
			
			$MailTools->sendTextEmail({
				'from'=>'errors@dhs-club.com',
				'to'=>'errors@dhs-club.com',
				'subject'=>'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: displayApplication()",
				'text'=>$message
			});
			
			
			exitScript();
		}
		
	}
	else
	{
		
		my $merchant_member_info = {};
		my $paying_sponsors_info = {};
		
		eval{
			
#			$MerchantAffiliates = MerchantAffiliates->new($db);
			
#			my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			
			displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
			
			my $temp_merchant_master_record_hashref = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'}, {'with_pending_payment_information'=>1});
			
			$MerchantAffiliates->setMembers() if ! $MerchantAffiliates->{'Members'};
			
			$merchant_member_info = $MerchantAffiliates->{'Members'}->getMemberInformationByMemberID({'member_id'=>$temp_merchant_master_record_hashref->{'member_id'}});
			
			
			$merchant_master_record_hashref->{'firstname1'} = $temp_merchant_master_record_hashref->{'firstname1'};
			$merchant_master_record_hashref->{'lastname1'} = $temp_merchant_master_record_hashref->{'lastname1'};
				
			if($temp_merchant_master_record_hashref->{'ca_number'} || $temp_merchant_master_record_hashref->{'ca_number_annual'})
			{
				my $id_to_get = $temp_merchant_master_record_hashref->{'ca_number'} ? $temp_merchant_master_record_hashref->{'ca_number'} : $temp_merchant_master_record_hashref->{'ca_number_annual'};
				
				$paying_sponsors_info = $MerchantAffiliates->{'Members'}->getMemberInformationByMemberID({'member_id'=>$id_to_get});
				
				$merchant_master_record_hashref->{'paying_firstname'} = $paying_sponsors_info->{'firstname'};
				$merchant_master_record_hashref->{'paying_lastname'} = $paying_sponsors_info->{'lastname'};
				
			}
			
			
		};
		if($@)
		{
			my $error = $@;
			
			warn $FindBin::RealScript . ' - ' . $error;
			
			print $CGI->header('text/plain');
			
			print "A fatal error occured, contact technical support at support1\@dhs-club.com.\n";
			
			my $MailTools = MailTools->new();
			
			my $message = "\n Error: \n $error \n";
			
			$MailTools->sendTextEmail({
				'from'=>'errors@dhs-club.com',
				'to'=>'errors@dhs-club.com',
				'subject'=>'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: displayApplication()",
				'text'=>$message
			});

			exitScript();
		}
	}
	
	#This is here due to a conflict in the data, and the way we use XML	
	$merchant_master_record_hashref->{'pay_payment_method'} = lc($merchant_master_record_hashref->{'pay_payment_method'}) if defined $merchant_master_record_hashref->{'pay_payment_method'};

	#$exchange_rates = $db->selectrow_hashref("SELECT ern.code, CASE WHEN ern.code = 'EUR' THEN 1 ELSE ern.rate END AS rate FROM currency_by_country cbc	JOIN exchange_rates_now ern ON cbc.currency_code = ern.code WHERE cbc.country_code = '$merchant_master_record_hashref->{'country'}'");						
	$exchange_rates = $db->selectrow_hashref("
		SELECT ern.code, ern.rate
		FROM currency_by_country cbc
		JOIN exchange_rates_now ern
			ON cbc.currency_code = ern.code
		WHERE cbc.country_code = ?", undef, $merchant_master_record_hashref->{'country'});
	
	my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
	
	my $country_code_lower_case = lc($merchant_master_record_hashref->{'country'});
	
	if ($merchant_master_record_hashref->{'pay_cardnumber'} && $merchant_master_record_hashref->{'pay_cardnumber'} !~ /x/gi)
	{
		$merchant_master_record_hashref->{'pay_cardnumber'} =~ s/\s//g;
		$merchant_master_record_hashref->{'pay_cardnumber'} =~ s/^\d{12}/XXXXXXXXXXXX/;
	}
	
	$merchant_master_record_hashref->{'pay_cardcode'} = '' if ($merchant_master_record_hashref->{'pay_cardcode'});
	
	#If it is a regular price country convert the regular pricing
	
	$merchant_master_record_hashref->{'merchant_type'} = $MerchantAffiliates->getMerchantTypeNameById($merchant_master_record_hashref->{'discount_type'});
			
			
	#If it is a regular price country convert the regular pricing
	if(defined $country_code_lower_case && exists $merchant_master_record_hashref->{$country_code_lower_case} && exists $pricing->{'regular_price_countries'}->{'offline'}->{$merchant_master_record_hashref->{$country_code_lower_case}})
	{
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'free'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'bronze'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'silver'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'regular'}->{'gold'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
	}
	else
	{
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'free'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'bronze'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'silver'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline'}->{'sale'}->{'gold'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
	}
			
	if(defined $country_code_lower_case && exists $merchant_master_record_hashref->{$country_code_lower_case} && exists $pricing->{'regular_price_countries'}->{'offline_rewards'}->{$merchant_master_record_hashref->{$country_code_lower_case}})
	{
				
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'free'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'basic'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'bronze'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'silver'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'gold'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'regular'}->{'platinum'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
	}
	else
	{		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'free'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
				
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'basic'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
								
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'bronze'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'silver'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'gold'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
		
		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'sale'}->{'platinum'} * $exchange_rates->{'rate'};
#		$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;	
	}

	$pricing->{'prices'}->{'converted'}->{'offline'}->{'monthly'} = sprintf "%.2f $exchange_rates->{'code'}", ($pricing->{'prices'}->{'offline'}->{'monthly'} || 0) * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline'}->{'monthly'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;

	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'free'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'free'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'free'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
	
	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'basic'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'basic'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'basic'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'bronze'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'bronze'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'bronze'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'silver'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'silver'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'silver'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'gold'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'gold'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'gold'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
	
	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'platinum'} = sprintf "%.2f $exchange_rates->{'code'}", $pricing->{'prices'}->{'offline_rewards'}->{'monthly'}->{'platinum'} * $exchange_rates->{'rate'};
#	$pricing->{'prices'}->{'converted'}->{'offline_rewards'}->{'monthly'}->{'platinum'} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;

	$configuration_xml .= XML::Simple::XMLout($pricing->{'prices'}->{'converted'}, 'RootName' => "converted", 'NoAttr' => 1);
			
	#Make the information XML friendly
	foreach (keys %$merchant_master_record_hashref)
	{
		next if ! defined $merchant_master_record_hashref->{$_};
			
		$information_hashref_arrayref->{$_} = [$merchant_master_record_hashref->{$_}];
	}
	
	if (defined $information_hashref_arrayref->{'pay_payment_method'} && ref($information_hashref_arrayref->{'pay_payment_method'}) ne 'ARRAY')
	{
		$information_hashref_arrayref->{'payment_method'} = $information_hashref_arrayref->{'pay_payment_method'} = [ lc($information_hashref_arrayref->{'pay_payment_method'}->[0]) ];
	}

	#Get the minimum balace a merchant needs to use the Funds Transfer, or the Club Account.
	#
	$information_hashref_arrayref->{'package_cost'} = [$pricing->{'prices'}->{$merchant_master_record_hashref->{'merchant_type'}}->{'seed'}];
	
	
	#TODO: I need to determin if they are just switching payment methods, if they are just show the amount they need to supply.
	#Get the Merchants Club Account Balance.
	my $account_balance = $db->selectrow_hashref('SELECT account_balance FROM soa_account_balance_payable WHERE id= ?', undef, $merchant_master_record_hashref->{'member_id'});
	
	$information_hashref_arrayref->{'package_cost'}->[0] += $MerchantAffiliates->retrieveMerchantPackageCost($merchant_master_record_hashref->{'merchant_type'}, $merchant_master_record_hashref->{'mpp_discount_type'},$merchant_master_record_hashref->{'country'}) if (defined $merchant_master_record_hashref->{'mpp_discount_type'} && $merchant_master_record_hashref->{'mpp_discount_type'} > $merchant_master_record_hashref->{'discount_type'});
	
	#Uncomment this if we want to subtract the amount they owe from their current club account balance.
	#$information_hashref_arrayref->{'package_cost'}->[0] -= $account_balance->{'account_balance'};
	
	#Uncomment this if we wish to display the amount in their currency, if you uncomment this you may also need to modify the XSL stylesheet as well.
	#$information_hashref_arrayref->{'package_cost'}->[0] = sprintf "%.2f $exchange_rates->{'code'}", $information_hashref_arrayref->{'package_cost'}->[0] * $exchange_rates->{'rate'};
	
	$information_hashref_arrayref->{'package_cost'}->[0] = sprintf "%.2f", $information_hashref_arrayref->{'package_cost'}->[0];
	
#	$information_hashref_arrayref->{'package_cost'}->[0] =~ s/^([−+]?\d+)(\d{3})/$1,$2/;

	
#warn "before conversion to params: " . Dumper($information_hashref_arrayref) . "\n";
	$params = XML::Simple::XMLout($information_hashref_arrayref, 'RootName' => "params" );
	
	
	if ($errors)
	{
		$errors_xml = XML::Simple::XMLout($errors, 'RootName' => "errors" );
	}


	if ($CGI->param('updated'))
	{
		$notices_xml = XML::Simple::XMLout({'updated'=>['updated']}, 'RootName' => "notices" );
	}
		
#warn "update_payment2: " . $params . "\n";
	$output_xml = <<EOT;
		<base>
			
			<menus>
				$countries_xml
				
				$configuration_xml
				
				$cc_expiration_dates_xml
				
			</menus>
			
			<data>
				$errors_xml
				$notices_xml
				
			</data>
		
			$params
			
			$xml
			
		</base>
		
EOT
	
	# for now, May 2012, Will wants to lockout the new online merchants from this interface as it is offline merchant specific
	my $merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($cookie->{'ma_info'}->{'id'});
	if ($merchant_master_vendor_information_hashref->{'vendor_group'} eq '6')
	{
		print $CGI->redirect('/maf/profile/onlineMafLockout.html');
		exitScript();
	}
	

	eval{
		
#		use Data::Dumper;
#		print $CGI->header('text/plain');
#		print $output_xml;
#		print Dumper($information_hashref_arrayref);
#		exitScript();
		
#warn $output_xml;
#		print $xsl;
		
		print $CGI->header(-charset=>'utf-8');
		print XML::local_utils::xslt($xsl, $output_xml);
		
#		print $CGI->header('text/plain');
#		print $output_xml;
		
		
	};
	if($@)
	{
		
		my $error = $@;
		# warn $FindBin::RealScript . ' - ' . $error;
		
		print $CGI->header('text/plain');
		print "There was an error creating the view.  A Technician has been notified, but please contact technical support support1\@dhs-club.com.";
		
		my $MailTools = MailTools->new();

		my $message = "There was an error creating the view\n\nError: $error \n\n XML: \n $output_xml";
		
		$MailTools->sendTextEmail({
						'from'=>'errors@dhs-club.com',
						'to'=>'errors@dhs-club.com',
						'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: displayApplication()",
						'text'=>$message
					});
		
		
	}
	
	exitScript();
	
}

sub processApplication
{
	
	my %merchant_information = ();
	
	my %missing_fields = ();
	
	my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} ); #{};
	
	# for now, May 2012, Will wants to lockout the new online merchants from this interface as it is offline merchant specific
	my $merchant_master_vendor_information_hashref = $MerchantAffiliates->retrieveMerchantMasterVendorInformationByMerchantID($cookie->{'ma_info'}->{'id'});
	if ($merchant_master_vendor_information_hashref->{'vendor_group'} eq '6')
	{
		print $CGI->redirect('/maf/profile/onlineMafLockout.html');
		exitScript();
	}
	
	my $update_the_master_record = 1;
	
	my %required_fields = 
	(
			2	=>	
					[
						'discount_type',
						'discount_subtype'
					],
	);
	
	my $merchant_original_record = {};
	
	eval{
		
#		$MerchantAffiliates = MerchantAffiliates->new($db);
		
#		$cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			
		displayUnavailable() if $cookie->{'ma_info'}->{'membertype'} eq 'mal';
			
		$merchant_original_record = $MerchantAffiliates->retrieveMerchantMasterRecordByID($cookie->{'ma_info'}->{'id'});
		
	};
	if($@)
	{
		my $error = $@;
		#warn $FindBin::RealScript . ' : ' . $error;
		
		$missing_fields{'script_error'} = ['Script Error'];
		
		my $MailTools = MailTools->new();

		my $message = "An error Instanciating a class, or retriving the Merchant Master Record. \n Error: \n $error \n";
		
		$MailTools->sendTextEmail({
			'from'=>'errors@dhs-club.com',
			'to'=>'errors@dhs-club.com',
			'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script :: processApplication()",
			'text'=>$message
		});
		
		
		
	}
#	warn "update_payment: " . $CGI->param('discount_type') . "\n";
	
	if($CGI->param('discount_type') != 15)
	{
	
		push @{$required_fields{2}}, 'pay_payment_method';
		
		# If the payment method is Credit Card, add the required fields for credit card.
		push @{$required_fields{2}}, 'cc_type', 'pay_cardnumber', 'pay_cardcode', 'pay_cardexp', 'pay_name', 'pay_address', 'pay_city', 'pay_country' if($CGI->param('pay_payment_method') eq 'cc');
		
		push @{$required_fields{2}}, 'pay_iban','pay_swift','pay_bank_name','pay_bank_city','pay_bank_address','pay_bank_state','pay_bank_country', 'pay_account_number' if($CGI->param('pay_payment_method') eq 'bd');
	}
	
	# Assign the information the merchant submitted to the "$merchant_information" hash.
	foreach my $key ($CGI->param())
	{
		next if $key =~ /submit|biz_description/i;
		
		$merchant_information{$key} = $CGI->param($key) if $CGI->param($key) && $key !~ /^_|^x$|^y$/;
		
		$merchant_information{'pay_cardexp'} =~ s/^\w//g if($key eq 'pay_cardexp' && $merchant_information{'pay_cardexp'});
		
		#The discount type could be 0 so we assign it here.
		$merchant_information{$key} = $CGI->param($key) if $key =~ /^discount_type$/;
		$merchant_information{$key} = uc($CGI->param($key)) if $key =~ /^pay_payment_method$|^mpp_pay_payment_method$/;
#		warn "update_payment: key: " . $key . " :: " . $CGI->param($key) . "\n";
		
	}
#
# Load in original cc info if available
#
        if ($merchant_information{'pay_cardnumber'} && $merchant_information{'pay_cardnumber'} =~ /XXXXXXXXXXXX/)
        {
            $merchant_information{'pay_cardnumber'} = $merchant_original_record->{'pay_cardnumber'};
            $merchant_information{'pay_cardcode'} = $merchant_original_record->{'pay_cardcode'};
            $merchant_information{'pay_cardexp'} = $merchant_original_record->{'pay_cardexp'};
            my $first_num = substr($merchant_information{'pay_cardnumber'}, 0, 1);
            if ($first_num == 3)
            {
                $merchant_information{'cc_type'} = "amex";
		    }
		    elsif ($first_num == 4)
		    {
				$merchant_information{'cc_type'} = "vs";
		    }
		    elsif ($first_num == 5)
		    {
				$merchant_information{'cc_type'} = "mc";
		    }
		    elsif ($first_num == 6)
		    {
				$merchant_information{'cc_type'} = "di";
		    }
		}
#	
#This information is needed so "MerchantAffiliates->updateMasterInformation" updates the vendor, and discount records.
#
	$merchant_information{'cap'} = $merchant_original_record->{'cap'};
	$merchant_information{'blurb'} = $merchant_original_record->{'blurb'};
	$merchant_information{'country'} = $merchant_original_record->{'country'};
	$merchant_information{'business_name'} = $merchant_original_record->{'business_name'};
	$merchant_information{'state'} = $merchant_original_record->{'state'};
	$merchant_information{'city'} = $merchant_original_record->{'city'};
	$merchant_information{'postalcode'} = $merchant_original_record->{'postalcode'};
	$merchant_information{'phone'} = $merchant_original_record->{'phone'};
	$merchant_information{'username'} = $merchant_original_record->{'username'};
	$merchant_information{'password'} = $merchant_original_record->{'password'};
	
#
#If the merchant is a free merchant there is no need for payment information so well clear it out
#
	$merchant_information{'special'} = undef;
	if($merchant_information{'discount_type'} == 15)
	{
		delete $merchant_information{'pay_payment_method'};
	}
#	
# because of the way they try to use special for everything, try to load the correct data into it
#
	if ($merchant_information{'discount_subtype'} == 1)   #This is flat_fee
	{
            $merchant_information{'special'} = $merchant_information{'flat_fee_off'};
            $merchant_information{'discount'} = 0;
	}
	elsif ($merchant_information{'discount_subtype'} == 2)   # This is percentage_off
	{
	    $merchant_information{'special'} = $merchant_information{'percentage_off'};
	    $merchant_information{'discount'} = $merchant_information{'percentage_off'};
	}
	else   #This should be bogo
	{
            $merchant_information{'special'} = $merchant_information{'bogo_amt'};
            $merchant_information{'discount'} = 0;
	}
#
# For some reason the following fields need to be set
#
	if ($merchant_information{'discount_subtype'} == 2)
	{
		$merchant_information{'percentage_of_sale'} = $merchant_information{'percentage_off'};
		$merchant_information{'exception_percentage_of_sale'} = 0.;
	}


					
	#This is a bit of a hack, but after several modifications of the form, and the payment processing what else is there to do.
	delete $merchant_information{'ca_number'} if exists $merchant_information{'ca_number'} &&  $merchant_information{'ca_number'} == $merchant_original_record->{'member_id'};
	delete $merchant_information{'ca_number_annual'} if exists $merchant_information{'ca_number_annual'} && $merchant_information{'ca_number_annual'} == $merchant_original_record->{'member_id'};
	
	#This is here for security purposes.  The merchant should not be able to specify a paying sponsor, so if it is specified there was an attempt at being unscrupulous.
	
	if(exists $merchant_information{'pay_payment_method'} && $merchant_information{'pay_payment_method'} eq 'CA' && exists $merchant_information{'ca_number'} && $merchant_information{'ca_number'} != $merchant_original_record->{'ca_number'})
	{
	
		my $warning_email = 
		{
			'from'=>'errors@clubshop.com',
			'to'=>'errors@clubshop.com',
		};
		
		$warning_email->{'subject'} = DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript - ERROR: Potential Unscrupulous Activity";
		$warning_email->{'text'} = "A merchant attempted to update their Club Account Monthly Payment information ('ca_number' field), but it looks like a new paying sponsor was specified, but that shouldn't be possible from this screen.\n\n";
		
		my $temp = \%merchant_information;
		
		$warning_email->{'text'} .= "merchant_information:\n" . Dumper($temp);
		$warning_email->{'text'} .= "\n\nmerchant_original_record:\n" . Dumper($merchant_original_record);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail($warning_email);
		
		$missing_fields{'ca_number'} = ['Incorrect Information'];
	
	}
	
	if(exists $merchant_information{'pay_payment_method'} && $merchant_information{'pay_payment_method'} eq 'CA' && exists $merchant_information{'ca_number_annual'} && $merchant_information{'ca_number_annual'} != $merchant_original_record->{'ca_number_annual'} )
	{
	
		my $warning_email = 
		{
			'from'=>'errors@clubshop.com',
			'to'=>'errors@clubshop.com',
		};
		
		$warning_email->{'subject'} = DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript - ERROR: Potential Unscrupulous Activity";
		$warning_email->{'text'} = "A merchant attempted to update their Club Account Annual Payment information ('ca_number_annual' field), but it looks like a new paying sponsor was specified, but that shouldn't be possible from this screen.\n\n";
		
		my $temp = \%merchant_information;
		
		$warning_email->{'text'} .= "merchant_information:\n" . Dumper($temp);
		$warning_email->{'text'} .= "\n\nmerchant_original_record:\n" . Dumper($merchant_original_record);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail($warning_email);
		
		$missing_fields{'ca_number_annual'} = ['Incorrect Information'];
	
	}

	if(exists $merchant_information{'pay_payment_method'} && $merchant_information{'pay_payment_method'} eq 'CC')
	{
		#Check to make sure it is a real card number.
		#TODO: Look into a meaningfull error message for this one.
		
		if ($merchant_information{'pay_cardnumber'} !~ /x/gi)
		{
			$missing_fields{'pay_cardnumber'} = ['Missing Field'] if ($merchant_information{'pay_cardnumber'} && ! CreditCard::validate($merchant_information{'pay_cardnumber'}));
		}
		
		$missing_fields{'pay_state'} = ['Missing Field'] if(! $merchant_information{'cc_state'} && ! $merchant_information{'pay_state'});
	
	
	}
#	elsif($merchant_information{'pay_payment_method'} eq 'ec')
#	{
#		$missing_fields{ec_number} = ['Missing Field'] 
#	}

	#Check for missing fields.
	foreach (@{$required_fields{2}})
	{
		$missing_fields{$_} = ['Missing Field'] if(! defined $merchant_information{$_} || $merchant_information{$_} =~ /^\s*$/);
	}
	
	#warn "MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}): " . $MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'});
	
	if($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline' && ! $merchant_information{'percentage_of_sale'})
	{
		$missing_fields{'percentage_of_sale'} = ['Missing Field'];
	}
	elsif($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline_rewards' && ! $merchant_information{'percentage_of_sale'} && ! $merchant_information{'special'})
	{
		$missing_fields{'discount_option_is_required'} = ['Missing Field'];
	}

#	my $temp = \%missing_fields;
#	warn "\n missing_fields:\n" . Dumper($temp);
#	
#	
#	$temp = \%merchant_information;
#	warn "\n merchant_information:\n" . Dumper($temp);
	
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
		$merchant_information{'pay_state'} = $merchant_information{'cc_state'} if (defined $merchant_information{'cc_state'} && $merchant_information{'cc_state'});
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}
	
	
	my $configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
			
	my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
			
	my $country = lc($merchant_information{'country'});
			
	my $pricing_type = $pricing->{'regular_price_countries'}->{$merchant_information{'merchant_type'}}->{$country} ? 'regular':'sale';
			
	my $package_price_label = '';
	
	$package_price_label = $MerchantAffiliates->getMerchantPackageNameById($merchant_information{'discount_type'});
	
	my $debit_club_account = [];
	
	
	#If someone else is paying for their package put them in pending, and send an email to their paying sponsor.
	if($merchant_information{'pay_payment_method'} && $merchant_information{'pay_payment_method'} eq 'CA' && $merchant_information{'ca_number_annual'} && $merchant_information{'ca_number_annual'} && $MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) > $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
	{
	
		my %pending_payment_information = ();
		$pending_payment_information{'discount_type'} = $merchant_information{'discount_type'};
		$pending_payment_information{'merchant_id'} = $pending_payment_information{'merchant_id'} = $cookie->{'ma_info'}->{'id'};
		$pending_payment_information{'ca_number'} = $merchant_information{'ca_number_annual'};
		$pending_payment_information{'pay_payment_method'} = $merchant_information{'pay_payment_method'};
		$pending_payment_information{'special'} = $merchant_information{'special'} if($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline_rewards');
		$pending_payment_information{'discount'} = $merchant_information{'discount'} if($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline');
		$pending_payment_information{'discount_blurb'} = $merchant_information{'discount_blurb'};
		#TODO: check the values in %pending_payment_information.
		my $rows_effected = $MerchantAffiliates->insertMerchantPendingPaymentInformation(\%pending_payment_information);
		$update_the_master_record = 0;
		
		if(! $rows_effected || $rows_effected eq '0E0')
		{
			$missing_fields{'script_error'} = ['Script Error'];
		}
		else
		{
			
			#I should set the notification here
			my $Notifications = Notifications->new($db);
			
			my %notification = 
			(
				'notification_type'=>106,
				'spid'=>$pending_payment_information{'ca_number'},
				'id'=>$pending_payment_information{'merchant_id'}
			);
			
			my $notification_id = $Notifications->createNotification(\%notification);
			
			if(! $notification_id)
			{
				$missing_fields{'script_error'} = ['Script Error'];
				
				my $MailTools = MailTools->new();
				
				my $temp = \%notification;
				my $notification_dump = Dumper($temp);
				
				
				$MailTools->sendTextEmail({
					'from'=>'errors@dhs-club.com',
					'to'=>'errors@dhs-club.com',
					'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script ",
					'text'=>"Merchant ID $pending_payment_information{'merchant_id'} attempted to request an upgrade, but there was an error setting the following notification, thier sponsor needs to be notified.  $notification_dump"
				});
			}
			
			#We'll allow them to continue on, and update their information, but first well reset their payment information so nothing is changed untill the paying sponsor gives their approval.
			#This is important due to they may choose to pay for their own monthly fees, and we want that change to go through.
			$merchant_information{'discount_type'} = $merchant_original_record->{'discount_type'};
		}
		
		#$update_the_master_record = 0;
	
	}
	#Charge the card the appropriate amount.
	elsif($merchant_information{'pay_payment_method'} && $merchant_information{'pay_payment_method'} eq 'CC')
	{
		##########################################################################
		#Make the fields not pertaining to CC transactions are cleared of content
		##########################################################################
		$merchant_information{'ca_number'} = '';
		$merchant_information{'ca_number_annual'} = '';
#		$merchant_information{ec_number} = '';
		
		#Bank Draft
		$merchant_information{'pay_iban'} = '';
		$merchant_information{'pay_swift'} = '';
#		$merchant_information{pay_bank_name} = '';
#		$merchant_information{pay_bank_city} = '';
#		$merchant_information{pay_bank_address} = '';
#		$merchant_information{pay_bank_state} = '';
#		$merchant_information{pay_bank_country} = '';
#		$merchant_information{pay_bank_postalcode} = '';
		$merchant_information{'pay_account_number'} = '';	
		$merchant_information{'pay_routing_number'} = '';		
		
		#The postalcode is not a required field, but if they leave it blank we need to clear it out
		$merchant_information{'pay_postalcode'} = '' if ! defined $merchant_information{'pay_postalcode'};
		
		#For Charging the Credit Card, and Verification the card is valid. 
		require 'ccauth.lib';
		
		my %cc_parameters = ();
		
		my $original_credit_card_number = $merchant_original_record->{'pay_cardnumber'};
		
		
		if ($original_credit_card_number && $original_credit_card_number !~ /x/gi)
		{
			$original_credit_card_number =~ s/\s//g;
			$original_credit_card_number =~ s/^\d{12}/XXXXXXXXXXXX/;
		}
			
		
		$cc_parameters{'cc_number'} = ($merchant_information{'pay_cardnumber'} eq $original_credit_card_number)? $merchant_original_record->{'pay_cardnumber'}: $merchant_information{'pay_cardnumber'};			
		$cc_parameters{'card_code'} = $merchant_information{'pay_cardcode'};
		$cc_parameters{'cc_expiration'} = $merchant_information{'pay_cardexp'};
		$cc_parameters{'cc_expiration'} =~ s/^d//;
		
		$cc_parameters{'account_holder_lastname'} = $merchant_information{'pay_name'};
		$cc_parameters{'account_holder_address'} = $merchant_information{'pay_address'};
		$cc_parameters{'account_holder_city'} = $merchant_information{'pay_city'};
		$cc_parameters{'account_holder_state'} = (defined $merchant_information{'pay_state'} && $merchant_information{'pay_state'}) ? $merchant_information{'pay_state'} : $merchant_information{'cc_state'};
		$cc_parameters{'account_holder_zip'} = $merchant_information{'pay_postalcode'} if $merchant_information{'pay_postalcode'};
		$cc_parameters{'account_holder_country'} = $merchant_information{'pay_country'};
		
		$cc_parameters{'payment_method'} = 'CC';
		$cc_parameters{'invoice_number'} = 'MAU' . time;
		$cc_parameters{'id'} = 'MAU';
		
		
		my $returned_auth_info = '';
		
		
		if($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) > $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}) || $merchant_original_record->{'pay_payment_method'} ne 'CC')
		{
			
			#$cc_parameters{'amount'} = $pricing->{'prices'}->{$merchant_information{'merchant_type'}}->{$pricing_type}->{$package_price_label};
			$cc_parameters{'amount'} = $MerchantAffiliates->retrieveMerchantPackageCost($merchant_information{'merchant_type'}, $merchant_information{'discount_type'}, $merchant_original_record->{'country'});
			
			$cc_parameters{'order_description'} = DHSGlobals::PRODUCTION_SERVER ? 'Merchant Package Update Credit Card Charge' : 'Merchant Package Update Credit Card Charge (Dev)';

			my $authorize_extra_parameters =  {};
			
			my $cookie = $MerchantAffiliates->retrieveMerchantCookie(sub{$CGI->cookie(@_)}, sub{$G_S->authen_ses_key(@_)} );
			
			my $operator = $CGI->cookie('operator') ? $CGI->cookie('operator') : "MA_User_$cookie->{'ma_info'}->{username}";
			
#			push @$debit_club_account, {
#											transaction_type=>7000,
#											amount=>$cc_parameters{'amount'},
#											description=>"Credit Card Funding for a Merchant Package Upgrade",
#											operator=>$operator,
#											'member_id'=>$merchant_original_record->{'member_id'}
#			};
			
			
			 
			
			#If the merchant is changing their payment type, and the merchant package is not being upgraded just run an authorize to confirm the credit card is in good standing.
			my $auth_only = 0;
			if($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) <= $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}) && $merchant_original_record->{'pay_payment_method'} ne 'CC')
			{
				
				$authorize_extra_parameters = {'auth_type'=>'AUTH_ONLY'};
				$cc_parameters{'amount'} = 1.5;
				$auth_only = 1;
				$debit_club_account = undef;
				
			}
			
			#Just to be safe, if this is not the production server set auth_type to 'AUTH_ONLY' so we don't charge the card.
			$authorize_extra_parameters = {'auth_type'=>'AUTH_ONLY'} if ! DHSGlobals::PRODUCTION_SERVER;

			
			$returned_auth_info = CCAuth::Authorize(\%cc_parameters, $authorize_extra_parameters);
			

			
			if ($returned_auth_info ne '1')
			{
				
				my $error_code = $returned_auth_info;
				
				$error_code =~ s/^.*\s(\d+):.*/$1/gi;
				
				$error_code =~ s/\s*//gi;
				
				$returned_auth_info =~ s/^\d\s*|\d+:\s*|\<\/*b\>\s*|\<br\s\/\>//gi;

				$missing_fields{"cc_error_$error_code"} = [$returned_auth_info];
				$missing_fields{'cc_error'} = [$returned_auth_info];
				
				#I'll put this here just to be safe.
				$debit_club_account = undef;
				
				displayApplication(\%merchant_information, \%missing_fields);
				exitScript();
				
			}
			else
			{
				#Well credit their Club account here, if we do it in MerchantAffiliates::updateMerchantMasterInformation method it will happen in a transaction, 
				#and that is not the best approach for all situations.
				#Say the merchant is -0.50 in their Club Account, and we credit their account in the MerchantAffiliates::updateMerchantMasterInformation method
				#The trasaction will be rolled back if the subscription throws an error.
				my $description = "Credit Card Funding for a Merchant Package Upgrade";
			    if ($auth_only == 1)
			    {
					$cc_parameters{'amount'} = 0;
					$description = "Credit Card Authorization Check";
			    }
			    
				my @debit_club_account_data = (
					$merchant_original_record->{'member_id'}, 
					7000, 
					$cc_parameters{'amount'}, 
					$description,
					$operator
				);
			
				
				eval{
					
					my $transaction_id = $db->selectrow_array('SELECT debit_trans_complete(?,?,?,?,NULL,?)', undef,	@debit_club_account_data);
					
					die "There was an error crediting the members Club Account, after charging their credit card. transaction_id: $transaction_id  -  debit_club_account_data: " . Dumper(@debit_club_account_data) if (! $transaction_id || $transaction_id eq '0E0');
				};
				if($@)
				{
					my $error = $@;
					
					#warn $FindBin::RealScript . ' : ' . $error;
					
					$missing_fields{'script_error'} = ['Script Error'];
					
					my $MailTools = MailTools->new();
					
					my $message = "Error Crediting A Merchant Club Account : $error \n\nMerchant ID: $cookie->{'ma_info'}->{'id'}\n\n\%merchant_information: " . Dumper(%merchant_information);
					$MailTools->sendTextEmail({
						'from'=>'errors@dhs-club.com',
						'to'=>'errors@dhs-club.com',
						'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script",
						'text'=>$message
					});

					$debit_club_account = undef;
					displayApplication(\%merchant_information, \%missing_fields);
					exitScript();
				}
				
			}
			
			$debit_club_account = undef;
		}

		#No need to update their information that is done later.
	}
	elsif($merchant_information{'pay_payment_method'} && ($merchant_information{'pay_payment_method'} eq 'CA' || $merchant_information{'pay_payment_method'} eq 'IF'))
	{
		
		my $club_account_number = ($merchant_information{'pay_payment_method'} eq 'CA' && exists $merchant_information{'ca_number_annual'} && $merchant_information{'ca_number_annual'}) ? $merchant_information{'ca_number_annual'} : $merchant_original_record->{'member_id'};
		my $account_balance = $db->selectrow_hashref('SELECT account_balance FROM soa_account_balance_payable WHERE id= ?', undef, $club_account_number);
		my $package_amount = $MerchantAffiliates->retrieveMerchantPackageCost($merchant_information{'merchant_type'}, $merchant_information{'discount_type'}, $merchant_original_record->{'country'});
		my $minimum_club_account_amount =  $MerchantAffiliates->retrieveMerchantSeedCost($merchant_information{'merchant_type'});
		
#		warn "\n\n ";
#		warn " package_amount: $package_amount";
#		warn " merchant_information{'merchant_type'}: $merchant_information{'merchant_type'}";
#		warn " minimum_club_account_amount =  MerchantAffiliates->retrieveMerchantSeedCost(merchant_information{'merchant_type'}): $minimum_club_account_amount =  MerchantAffiliates->retrieveMerchantSeedCost($merchant_information{'merchant_type'}) : " . $MerchantAffiliates->retrieveMerchantSeedCost($merchant_information{'merchant_type'});
		
		
		$package_amount += $minimum_club_account_amount;
		
#		warn " package_amount: $package_amount";
#		warn " account_balance: " . Dumper($account_balance);
#		warn "\n\n";
		
		#We'll prep this information in case we need to use it.
		my %pending_payment_information = ();
		$pending_payment_information{'discount_type'} = $merchant_information{'discount_type'};
		$pending_payment_information{'discount_blurb'} = $merchant_information{'discount_blurb'};
		$pending_payment_information{'merchant_id'} = $cookie->{'ma_info'}->{'id'};
		$pending_payment_information{'pay_payment_method'} = $merchant_information{'pay_payment_method'};
		$pending_payment_information{'special'} = $merchant_information{'special'} if($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline_rewards');
		$pending_payment_information{'discount'} = $merchant_information{'discount'} if($MerchantAffiliates->getMerchantTypeNameById($merchant_information{'discount_type'}) eq 'offline');

		
		#We'll prep the email information in case we need to use it.
		my $accounting_email = 
			{
				'from'=>'support1@clubshop.com',
				'subject'=>'International Funds Transfer Merchant Package Upgrade',
				'text'=>"
Our Dearest Accountant,

The following merchant has requested to change their Merchant Package, and/or changed thier payment type to the International Funds Transfer option, but they don't have the minimum balance in their Club Account to fulfill the request.

Merchant Master ID: $merchant_original_record->{'id'}
Business Name: $merchant_original_record->{business_name}

Kindest Regards,
Your Beloved IT Department
"
			};
		
		$accounting_email->{to} = DHSGlobals::PRODUCTION_SERVER ? 'meract@dhs-club.com' : 'general@clubshop.com';
		
		
		#If they are downgrading, or just changing their payment type we don't need to charge, but if they are switching 
		#from the Credit Card payment type we need to verify that at least minimum funds are available.
		#if($merchant_information{'discount_type'} <= $merchant_original_record->{'discount_type'})
		if($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) <= $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
		{
			#warn "if($merchant_information{'discount_type'} <= $merchant_original_record->{'discount_type'})\n";
			
			#Check to see if their previous payment type was Credit Card, and if they can afford the change 
			if ($merchant_original_record->{'pay_payment_method'} && $merchant_original_record->{'pay_payment_method'} eq 'CC' && $account_balance->{'account_balance'} <= $minimum_club_account_amount)
			{
				#warn "if ($merchant_original_record->{'pay_payment_method'} eq 'CC' && $account_balance->{'account_balance'} <= $minimum_club_account_amount)\n";
				
				#TODO: Here we probably need to add a check for Club Account
				if($merchant_information{'pay_payment_method'} eq 'IF')
			 	{
			 		
			 		$MerchantAffiliates->insertMerchantPendingPaymentInformation(\%pending_payment_information);
					$update_the_master_record = 0;
			 		
			 		my $MailTools = MailTools->new();
					$MailTools->sendTextEmail($accounting_email);
					
			 	}
			 	#If they are switching from Credit Card to club account they can only pay by their own club account at this point.
			 	else
			 	{
			 		$missing_fields{'ca_account_balance'} = ['ca_account_balance'];
			 	}
			 	
			}
			elsif($account_balance->{'account_balance'} < $minimum_club_account_amount)
			{
				
				if($merchant_information{'pay_payment_method'} eq 'IF')
			 	{
			 		
			 		$MerchantAffiliates->insertMerchantPendingPaymentInformation(\%pending_payment_information);
					$update_the_master_record = 0;
					
			 		my $MailTools = MailTools->new();
					$MailTools->sendTextEmail($accounting_email);
					
			 	}
			 	else
			 	{
			 		$missing_fields{'ca_account_balance'} = ['ca_account_balance'];
			 	}
			}
			
		} else {
			
			#If they are upgrading
			
			#Check their account balance, compaired to what is available
			if($account_balance->{'account_balance'} < $package_amount)
			{
				if($merchant_information{'pay_payment_method'} eq 'IF')
			 	{
			 		
					$accounting_email->{'text'}="
Our Dearest Accountant,

The following merchant has requested to upgrade their Merchant Package, and opted to pay via an International Funds Transfer.

Merchant Master ID: $merchant_original_record->{'id'}
Business Name: $merchant_original_record->{business_name}

Kindest Regards,
Your Beloved IT Department
";

			 		$MerchantAffiliates->insertMerchantPendingPaymentInformation(\%pending_payment_information);
					$update_the_master_record = 0;
					
			 		my $MailTools = MailTools->new();
					$MailTools->sendTextEmail($accounting_email);
			 		
			 	}
			 	else
			 	{
			 		$missing_fields{'ca_account_balance'} = ['ca_account_balance'];
			 	}
			}
			
		}
	}
	
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{
		$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
		
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}
	
	#warn 'update_the_master_record: ' . $update_the_master_record;
	
	if($update_the_master_record)
	{
		eval{
			
			my $subscription = {};
			
			#warn "merchant_information{'discount_type'}: $merchant_information{'discount_type'} > merchant_original_record->{'discount_type'}: $merchant_original_record->{'discount_type'}";
			
			if ($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) > $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
			{
				$subscription->{'status'} = 'UPGRADE';
				$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
			}
			elsif($MerchantAffiliates->getPackageUpgradeValues($merchant_information{'discount_type'}) < $MerchantAffiliates->getPackageUpgradeValues($merchant_original_record->{'discount_type'}))
			{
				$subscription->{'status'} = 'DOWNGRADE';
				$subscription->{'description'} = 'Downgraded by the Merchant. Merchant ID: ' . $cookie->{'ma_info'}->{'id'};
				$merchant_information{'member_id'} = $merchant_original_record->{'member_id'};
			}
			else
			{
				#If they arent upgrading, or down grading there is no need do anything with subscriptions.
				$subscription = undef;
			}
			
			
			#$merchant_information{business_name} = $merchant_original_record->{business_name};
			
			#This isn't needed, and having it unecessarily updates the master location record when we call the "updateMerchantMasterInformation" method.
			#delete $merchant_information{'country'} if exists $merchant_information{'country'};
			
#			my $temp = \%merchant_information;
#			warn "\n merchant_information:\n" . Dumper($temp);
#			warn "\n merchant_original_record:\n" . Dumper($merchant_original_record);
#			warn "\n subscription:\n" . Dumper($subscription);
#			warn "\n debit_club_account: \n" . Dumper($debit_club_account);
			
			my $updated = $MerchantAffiliates->updateMerchantMasterInformation($cookie->{'ma_info'}->{'id'}, \%merchant_information, undef, undef, $subscription, $debit_club_account);
			
			if(! $updated)
			{
				$missing_fields{'not_updated'} = ['Your information was not updated'];
				displayApplication(\%merchant_information, \%missing_fields);
				exitScript();
			}
			elsif($subscription && exists $subscription->{'status'} && $subscription->{'status'} eq 'DOWNGRADE')
			{
				
				my $blurb_updated = $MerchantAffiliates->adjustMerchantBlurbForCurrentPackageBenefits($cookie->{'ma_info'}->{'id'});
				my %returned_keywords_update = $MerchantAffiliates->adjustMerchantKewordsForCurrentPackageBenefits($cookie->{'ma_info'}->{'id'});
				
				$missing_fields{'blurb_adjusted'} = ['The blurb was truncated'] if $blurb_updated;
				$missing_fields{'keywords_adjusted'} = ['The keywords were truncated.'] if (exists $returned_keywords_update{'notices'}->{'max_number_of_keywords_exceeded'} && $returned_keywords_update{'notices'}->{max_number_of_keywords_exceeded});
				
			}
		};
		if($@)
		{
			
			my $error = $@;
			
			#warn $FindBin::RealScript . ' : ' . $error;
			
			$missing_fields{'script_error'} = ['Script Error'];
			
			my $MailTools = MailTools->new();
			
			my $message = "Error Updateing the Merchant Master Record : $error \n\nMerchant ID: $cookie->{'ma_info'}->{'id'}\n\n\%merchant_information: " . Dumper(%merchant_information);
			$MailTools->sendTextEmail({
				'from'=>'errors@dhs-club.com',
				'to'=>'errors@dhs-club.com',
				'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::Bin / $FindBin::Script",
				'text'=>$message
			});
			
			
			
			
			
			displayApplication(\%merchant_information, \%missing_fields);
			exitScript();
			
		}
	}
	
	my $url = $CGI->url() . '?' . URI::Escape::uri_escape('updated') . '=' . URI::Escape::uri_escape('updated');
	
	print $CGI->redirect('-uri'=>$url);
	exitScript();
}



sub displayUnavailable
{
	
	print $CGI->redirect('-uri'=>'/maf/payment/unavailable');
	
	exitScript();
	
}

__END__

=head1
CHANGE LOG

	2009-07-31	Keith	Added the ability to use EU Bank Drafts.
	2009-08-05	Keith	Added Email Error Handling.

=cut


