#!/usr/bin/perl -w
###### banners.cgi	a simple script to spit out an HTML 'banner' from a selected image and submitted ID
###### 01/03/2003	Bill MacArthur

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
$| = 1;   # do not buffer error messages

###### GLOBALS
our $q = new CGI;
our ($db, $imgW, $imgH);

my $alias = uc $q->param('id');
unless ($alias){ &Err('You must submit your ID'); exit }

my $img = $q->param('img');
unless ( $img && $img =~ /.*,.*,.*/ ){ &Err('Bad or missing image parameter'); exit }
($img, $imgW, $imgH) = split /,/, $img;

unless ($db = DB_Connect('banners')){exit}
my $sth = $db->prepare("SELECT id, membertype, label FROM members m, \"Member_Types\" mt WHERE m.membertype=mt.code AND alias= ?");
$sth->execute($alias);
my ($id, $mtype, $membertype) = $sth->fetchrow_array; $sth->finish;

if ($id && $mtype =~ /m|p|v|ag/){
	my $page = &G_S::Get_Object($db, 6);
	unless ($page){ &Err('Could not load object template') }
	else{
		print $q->header;
		$page =~ s/%%id%%/$alias/g;
		$page =~ s/%%img%%/$img/g;
		$page =~ s/%%imgH%%/$imgH/g;
		$page =~ s/%%imgW%%/$imgW/g;
		print $page;
	}
}
elsif($id){
	my $msg = qq#We're sorry, your membertype disqualifies you from being a referrer.<br>
Your membertype: <b>$membertype</b><br>
Please contact us for further assistance.<br><br>
<a href="javascript:self.close()">close this window</a>#;
	&Err($msg);
}
else{ &Err('Cannot find a membership that matches that ID') }

END:
$db->disconnect;
exit;

sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### 09/25/02 added the affinity group membertype to the valid matches
###### 01/03/03 went to the 'use' style for custom modules

