#!/usr/bin/perl -w
# new 'cert.cgi'
# originally written by:	Chris Handley
###### last modified: 01/26/04	Bill MacArthur
# Script to produce a color certificate for a WEB browser. 

use CGI::Carp qw(fatalsToBrowser);
use DBI;
use CGI ':all';
use strict;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;

$| = 1;

our $id = param('X');
unless ($id)
{
	Err('Missing parameter: X');
	exit;
}
	
our ($firstname, $lastname, $dateparm, $level, $offset) = ();

my ($db);
unless ($db = DB_Connect('cert.cgi')){exit}

my $qry = "
	SELECT m.firstname,
		m.lastname,
		a.entered,
		a.level
	FROM 	members m,
		level_achievements a
	WHERE  m.id = a.id
	AND 	a.id = ?";

my $sth = $db->prepare($qry);
$sth->execute($id);
($firstname, $lastname, $dateparm, $level) = $sth->fetchrow_array; 
$sth->finish;
$db->disconnect;

unless ($level)
{
	Err('No matching achievement record found for this ID.');
}
else
{
	if ($ENV{'HTTP_USER_AGENT'} =~ /MSIE/) { $offset = "308px" } else { $offset = "308px" };
	&display_cert;
}
exit;

sub display_cert
{

print "Content-type: text/html\n\n";

print "<html><head><title>e-Business Institute Certificate</title></head><BODY bgcolor=\"#ffffff\">";

# to try an keep the certificate a realistic as possible I have matched the color of the font to 
# the original graphics of the certificate background.
# Set font colors -  BLUE #617BAE for level 1 thru 5 otherwise brown

my $foncol="#617BAE";
if ($level > 7 ) { $foncol="#AB7571"; }

if ($level eq 3) { print qq!<P><IMG src="/TEST/aa-tlcert.jpg" width"620" height="460" border="0"></P>!;  }
if ($level eq 4) { print qq!<P><IMG src="/TEST/aa-tccert.jpg" width"620" height="460" border="0"></P>!; }
if ($level eq 5) { print qq!<P><IMG src="/TEST/aa-bmcert.jpg" width"620" height="460" border="0"></P>!; }
if ($level eq 6) { print qq!<P><IMG src="/TEST/aa-smcert.jpg" width"620" height="460" border="0"></P>!; }
if ($level eq 7) { print qq!<P><IMG src="/TEST/aa-gmcert.jpg" width"620" height="460" border="0"></P>!; }
if ($level eq 8) { print qq!<P><IMG src="/TEST/aa-rdcert.jpg" width"620" height="460" border="0"></P>!;  }
if ($level eq 9) { print qq!<P><IMG src="/TEST/aa-emcert.jpg" width"620" height="460" border="0"></P>!;  }
if ($level eq 10) { print qq!<P><IMG src="/TEST/aa-ddcert.jpg" width"620" height="460" border="0"></P>!;  }
if ($level >= 11) { print qq!<P><IMG src="/TEST/aa-edcert.jpg" width"620" height="460" border="0"></P>!;  }


print qq!
<DIV style="width : 600px;height : 60px;top :$offset;left : 20px;
  position : absolute;
  z-index : 1;
" id="Layer2" align="center"> 
  <DIV align="center">
    <TABLE border="0" height="100" width="520">
      <TBODY> 
      <TR> 
        <TD colspan="2" align="center" height="78" width="520"> 
<I><font size=6 color=$foncol><b>$firstname $lastname</font></b> </I>        
        </TD>
      </TR>
       <TR> 
        <TD colspan="2" align="left"  height="5" width="520"> 
        </TD>
      </TR>
  <TR> 
        <TD colspan="2" align="left"  height="20" width="520"> 
<I><font size=2 color=$foncol><b>Date: $dateparm</font></b> </I>        
        </TD>
      </TR>
       </TBODY> 
    </TABLE>
</DIV>
</BODY>
</HTML>	!;


}

sub Err
{
	print header(-expires=>'now'), start_html(-bgcolor=>'#ffffff');
	print "$_[0]<br /><br />Server time: <i>", scalar localtime(), '</i> (PST)';
	print end_html();
}

###### 01/15/02 revised to call the vip_level table from the main database instead of the web server DB
###### also incorporated the global DB connection routine and merged the separate queries
###### added the Err routine
###### 12/23/02 went to the 'use' style for global modules
###### 01/26/04 fixed the reference to the now defunct table vip_level, however it looks
###### like this script may be defunct since I cannot find a website link to it :)
