#!/usr/bin/perl -w
# (P)artner(S)ubscription(A)ccess x
# a script to work the the Partner Subscription Access system
# this script handles redirects from resources for which the user has insufficient privileges based upon their subscripton type
# last modified: 09/12/12	Bill MacArthur

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
require LWP::UserAgent;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use PartnerSubscriptionAccess;

my $cgi = new CGI;
my $ua = LWP::UserAgent->new;

$ua->default_header('Accept-Language' => $cgi->http('Accept-Language'));
my ($db) = ();
DB();
my $psa = PartnerSubscriptionAccess->new($cgi, {'db'=>$db});

my $resourceID = $cgi->param('resourceID') || '';

my $subscription = $psa->ExtractCookieValue() || {'weight'=>0};
my $acl = $psa->ACL($resourceID);

# now we evaluate what to do
unless ($acl)
{
	# this case should not happen, but let's imagine it does
	x_1();
}
elsif ($subscription->{'weight'} == 0)	# "For non-subscribers (qualifying as a Partner without a GPS subscription) trying to access"
{
	x_1();
}
elsif ($subscription->{'weight'} < 20)	# "For BASIC trying to access PRO & PREMIUM accessible pages"
{
	x_2();
}
elsif ($subscription->{'weight'} < 40)	# "For PRO/PLUS trying to access PREMIUM pages"
{
	x_3();
}
else									# we should never get here, but we did while testing and having a PREMIER subscription
{
	print $cgi->header('text/plain'), 'OK';
}

Exit();

###### start of subroutines

sub DB
{
	$db ||= DB_Connect::DB_Connect('PSA') || exit;
	return $db;
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetPage
{
	my $page = shift;
	my $response = $ua->get($page);
	
	if ($response->is_success)
	{
		return \($response->content);
	}
	else
	{
		# no use dieing
		print $cgi->redirect($page);
	}
}

sub Hide
{
	my ($tagname, $content) = @_;
	
	# We are looking for comment tags like this: <!--tagname-open-->Some exposed content<!--tagname-close-->
	# We want it to look like this when we are done: <!--tagname_open Some exposed content tagname_close-->
	$$content =~ s/<!--$tagname-open-->/<!-- /g;
	$$content =~ s/<!--$tagname-close-->/ -->/g;
}

sub InsertSubscriptionLabel
{
	my $content = shift;
	$$content =~ s/\[\% xSubscription\.label \%\]/$subscription->{'label'}/g;
}

sub Show
{
	my ($tagname, $content) = @_;
	
	# We are looking for content that is commented out like this: <!--tagname-open Some hidden content tagname-close-->
	# We want it to look like this: Some hidden content
	$$content =~ s/<!--$tagname-open//g;
	$$content =~ s/$tagname-close-->//g;
}

sub x_1
{
#	my $content = GetPage('http://www.clubshop.com/gps.html');	the url changed in June 2013
	my $content = GetPage('http://www.clubshop.com/gps_new.html');
	print $cgi->header;
	Show('tag_1', $content);
	Show('tag_2', $content);
	Hide('tag_3', $content);
	print $$content;
}

sub x_2
{
	my $content = GetPage('http://www.clubshop.com/manual/gps/overview.html');
	print $cgi->header;
	InsertSubscriptionLabel($content);
	Hide('tag_1', $content);
	Show('tag_2', $content);
	print $$content;
}

sub x_3
{
	my $content = GetPage('http://www.clubshop.com/manual/gps/overview.html');
	print $cgi->header;
	InsertSubscriptionLabel($content);
	Hide('tag_1', $content);
	Show('tag_3', $content);
	print $$content;
}

__END__

09/12/12
	Added handling for when this script is called by a party having a PREMIER subscription
