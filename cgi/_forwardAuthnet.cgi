#!/usr/bin/perl -w
use strict;

#use HTTP::Request;
use LWP::UserAgent;
use HTTP::Request;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;
die "Request denied for your IP" unless ($q->remote_addr eq '208.69.229.17') || ($q->remote_addr eq '208.69.229.4');
my $query_string = $q->query_string;
$query_string =~ s/;/&/g;
warn $query_string;
#my $url = 'http://www.clubshop.com/cgi/TrialPartner.cgi';
my $url = 'https://secure.authorize.net/gateway/transact.dll';

my $ua = LWP::UserAgent->new;
my $req = HTTP::Request->new('POST' => $url);
$req->content_type('application/x-www-form-urlencoded');
$req->content($query_string);
my $res = $ua->request($req);
print $q->header('text/plain');
print $res->decoded_content . "\n";
warn $res->decoded_content;