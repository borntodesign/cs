#!/usr/bin/perl -w
###### activate.cgi
###### cgi script to activate a ClubBucks card
###### last modified 05/21/14	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw( md5_hex );
use G_S qw( %MEMBER_TYPES $ERROR_PAGE);
use DB_Connect;

my ($db, $record, $spid) = ();

###### these are the membertypes that are allowed to sponsor
#my $VALID_SPONSOR_TYPES = 'm|v|ag|ma';

my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $MEMBERAPP = 'http://' . $q->server_name . '/cgi/appx.cgi';

my $cbid = $gs->PreProcess_Input($q->param('cbid')) || '';

###### a hack to handle the erroneously printed cards with the 'DHS' prepender
$cbid =~ s/^DHS//i;

###### perform preliminary tests on the submitted value before actually checking it
###### against the database
unless ( $cbid )
{
	Err('No activation ID received. Please back up and try again.');
}
elsif ( length $cbid > 10 || $cbid =~ /\D/ )
{
	Err('Invalid activation ID received. Please back up and try again.');
}

exit unless $db = DB_Connect::DB_Connect('activate');

$record = GetCBIDrecord($cbid);

###### our next goal is to get a sponsor ID
if (!$record)
{
	Err('Cannot find a match for that activation ID.');
}

###### we'll assume a good number first
elsif ($record->{'class'} == 2)		###### our simple assigned class
{
	$spid = GetValidSponsor( $record->{'referral_id'} );
}

elsif ($record->{'class'} == 3)		###### our assigned-secondary class
{
	my $primary = GetCBIDrecord( $record->{'referral_clubbucks_id'} );
	unless ($primary)
	{	###### this is a long shot, but we may as well anticipate it will happen
		Err("Failed to locate the referring card number: $record->{'referral_clubbucks_id'}");
	}

###### theoretically, the person who handed out these secondary cards has activated their own
###### since this is real life, we cannot make that assumption, so we'll make the sponsor
###### the referral ID if the primary has not yet been activated
	$spid = $primary->{'current_member_id'} || $primary->{'referral_id'};
	$spid = GetValidSponsor( $spid );
}

###### now we'll deal with bogus entries
elsif ($record->{'class'} == 6)
{
	Err('That activation ID has been VOIDED.');
}

elsif ($record->{'class'} =~ /4|5/)
{
	Err('That ID has already been activated.');
}

elsif ($record->{'class'} == 1)
{
	Err('That is not a valid activation ID. It is unassigned.');
}

elsif ($record->{'class'} == 7)
{
	###### these are cards that are 'owned' but not quite ready for activation
	###### if we have received a referral_id and its good, then we can proceed
	###### otherwise, we're gonna have to deal with it
	my $referral_id = $gs->PreProcess_Input($q->param('referral_id'));
	unless ($referral_id)
	{
		Step_2();
		Exit();
	}
	else
	{
		###### we must assume from the start that we could be receiving either
		###### a Club alias, a numeric CBID or a ClubBucks ID as printed on a card ie. DHS0000256789
		###### we will consider a value which contains non-numeric characters as an alias
		###### unless it begins with 'DHS'
		###### anything else will be considered a CBID
		if ($referral_id =~ /^\D/ && $referral_id !~ /^DHS/i)
		{
			$spid = $gs->Alias2ID($db, 'members', $referral_id);
		}
		else
		{
			###### trim off the DHS is it exists
			$referral_id =~ s/^DHS//i;

			my $primary = GetCBIDrecord( $referral_id );
			if ($primary)
			{
				$spid = $primary->{'current_member_id'} || $primary->{'referral_id'};
			}
		}
		$spid = GetValidSponsor( $spid ) if $spid;

		###### now to make sure that the spid we have is owner of the card or at least in their downline
		if ($spid)
		{
			unless ( $gs->Check_in_downline($db, 'members', $record->{'referral_id'}, $spid->{'id'}) )
			{
				Err(qq|There is a problem with the Referral ID you entered.<br />
					Please try again.<br />
					If you continue to get this error, please contact the person who gave you this card.<br />
					Thank you.|);
			}
		}
	}
}

###### at this point we should have a spid
unless ($spid)
{
	Err('Failed to obtain a sponsor record.');
}	
else
{
	my $salt = $gs->GetObject(96) || die "Failed to retrieve object: 96";
	my $hash = md5_hex( $salt.$cbid.$spid->{'id'} );
	print $q->redirect( "$MEMBERAPP/$spid->{'id'}/cbid?cbid=$cbid&cbh=$hash" );
}

Exit();

######

sub Err
{
	my $TMPL = $ERROR_PAGE;

###### if we can pick the customized version, we'll do it
	if ($db)
	{
		$TMPL = $gs->GetObject(10149) || $TMPL;
	}
					
	$TMPL =~ s/%%MESSAGE%%/$_[0]/;
	$TMPL =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header('-charset'=>'utf-8'), $TMPL;
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetCBIDrecord
{
	my $id = shift;
	return unless $id;	
	my $sth = $db->prepare("	SELECT id,
						class,
						referral_id,
						referral_clubbucks_id,
						current_member_id
					FROM 	clubbucks_master
					WHERE 	id= ?::bigint
					AND 	class IN (2,3,4,5,7)");
	$sth->execute($id);
	$id = $sth->fetchrow_hashref;
	$sth->finish;
	return $id;
}

sub GetValidSponsor
{
	my $tmp = shift;	# this should be the spid, or potential spid, of the card activator
	return unless $tmp;

	# we will assume Shopper since we do not know whether the applicant will end up shopper or affiliate, if they end up applying for a membership
	$tmp = $db->selectrow_hashref("SELECT find_qualified_sponsor($tmp,'s') AS id");
###### initialize a hashref var		
#	$tmp = { 'spid' => $tmp };
#	
#	my $sth = $db->prepare("
#		SELECT id,
#				spid,
#				membertype
#		FROM 	members
#		WHERE 	id= ?");
#
####### recursively search for a valid sponsor				
#	do
#	{
#		$sth->execute($tmp->{'spid'});
#		$tmp = $sth->fetchrow_hashref;
#				
#	} while (
#			$tmp &&
#			$tmp->{'id'} > 1 &&
#			$tmp->{'membertype'} !~ /$VALID_SPONSOR_TYPES/);
#
#	$sth->finish;
	return $tmp;			
}

sub Step_2
{
	###### display a page for the user to enter a referral ID so that we can proceed
	my $tmpl = $gs->GetObject(10210) || die "Failed to retrieve template: 10210";

	$tmpl =~ s/%%action%%/$q->script_name/e;
	$tmpl =~ s/%%cbid%%/$cbid/;

	print $q->header('-charset'=>'utf-8'), $tmpl;
}		

###### 05/27/03 revised the clubbucks SQL to force a bigint comparison in order to use the index
###### 07/08/03 fixed the SQL in GetCBIDrecord to pull in the current_member_id in order to assign the sponsor correctly
###### 08/08/03 applied filtering for leading and trailing whitespace
# 09/15/05 changed the member application destination
# 02/23/10 revised the member app URL to be dynamically defined to incorporate the dev environment
# 05/21/14 converted from mod-perl to ordinary CGI, added Exit() and removed goto's