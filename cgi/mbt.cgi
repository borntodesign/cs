#!/usr/bin/perl -w
# mbt.cgi
=pod Merchant Bulk Transactions
Initially created to fill a special need to accept bulk merchant transactions.

The idea is that this script will be invoked by another machine.
Simple 4xx errors are totally acceptable for any failures.
We should never send a 200 response except on success.
=cut

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use Digest::SHA1  qw(sha1_hex);
use Mail::Sendmail;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;

my $cgi = new CGI;
my $ERR_EMAIL_CONTACT = 'webmaster@dhs-club.com';	# if we want to receive the 400 errors, put an emailaddress in here

Err("This interface must be invoked as HTTPS") unless $cgi->https;
my (%params, $db) = ();

foreach (qw/transmitter_id password file_transaction_data transaction_data checksum checksum_method test/)
{
	my $tmp = $cgi->param($_);
	next unless defined $tmp;
	$params{$_} = $tmp;
}

Err("Unable to authenticate without transmitter_id and password") unless $params{transmitter_id} && $params{password};

# if they submitted a file that data overwrites the textarea
$params{'transaction_data'} = LoadFile( $params{'file_transaction_data'} ) if $params{'file_transaction_data'};

Err("transaction_data is undefined") unless $params{transaction_data};
#$params{transaction_data} .= "\n" unless $params{transaction_data} =~ /\n$/;

# if they are doing a file upload, they will have created their own
#$params{transaction_data} =~ s/\r//g unless $params{'file_transaction_data'}

$db = DB_Connect::DB_Connect('generic', undef, {'fail_silently'=>1}) ||
	die "Failed to obtain a DB connection";
$db->{RaiseError} = 1;

# this is pretty straightforward... authenticate, do a checksum if it is called for,
# reply with a 200 if OK or a 400 error otherwise

my $transmitter = $db->selectrow_hashref('
	SELECT * FROM merchant_affiliate_transactions_bulk_transmitters WHERE transmitter_id=? AND password=?',
	undef, $params{transmitter_id}, $params{password});
Err("Authentication failed") unless $transmitter;
Err("Your account status is deactivated") unless $transmitter->{active};

ValidateCheckSum(); # any errors will be responded to from there
my $TEST = $params{test} ? 'TRUE' : 'FALSE';
$db->do("INSERT INTO merchant_affiliate_transactions_bulk
		(transmitter_id, transaction_data, ip_addr, test)
		VALUES ($transmitter->{transmitter_id}, ?,?, $TEST)",
	undef, $params{transaction_data}, $cgi->remote_addr);

print $cgi->header();

End();

###### ######

sub End
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	print $cgi->header(-status=>400, -type=>'text/plain'), $_[0];
	if ($ERR_EMAIL_CONTACT){
		my $msg = "Error: $_[0]\n";
		$msg .= "$_ : $params{$_}\n" foreach keys %params;
		$msg .= "ip addr: " . $cgi->remote_addr;
		my $rv = sendmail(
			'To' => $ERR_EMAIL_CONTACT,
			'From' => 'mbt.cgi@www.clubshop.com',
			'Subject' => '400 error',
			'Message' => $msg
		);
		warn $Mail::Sendmail::error unless $rv;
	}
	End();
}

sub LoadFile
{
	my $fh = shift;
	
warn "fh: $fh\n";
my $length=0;
	my $rv = '';
	while (<$fh>){
		$rv .= $_;
		$length += length($_);
	}
warn "length: $length\n";
warn "length rv: " . length($rv) . "\n";
	return $rv;
}

sub ValidateCheckSum
{
	return unless $params{checksum} || $transmitter->{checksum_method} || $params{checksum_method};
	Err("Transmitter records indicate use of a checksum_method, but no checksum parameter was received")
		if $transmitter->{checksum_method} && ! $params{checksum};
	Err("A checksum_method was received by no checksum was received") if $params{checksum_method} && ! $params{checksum};
	Err("No checksum_method defined") if $params{checksum} && ! ($transmitter->{checksum_method} || $params{checksum_method});

	# the method specified in the transmitter record overrides any parameter
	my $method = $transmitter->{checksum_method} || $params{checksum_method};

	# at this point we should have a checksum and a method for validating it
	if ($method eq 'md5'){
#warn "doing md5 ck\n";
		my $ck = md5_hex($params{transaction_data});#, $transmitter->{hash_salt}
#warn "ck=$ck\n";
		Err("Checksum did not validate: $ck\ntransaction_data length: " . length($params{transaction_data}))
			unless $ck eq $params{checksum};
	}
	elsif ($method eq 'sha1'){
		my $ck = sha1_hex($params{transaction_data});
		Err("Checksum did not validate: $ck\ntransaction_data length: " . length($params{transaction_data}))
			unless $ck eq $params{checksum};
	}
	else {
		Err("Unrecognized checksum_method");
	}
}

