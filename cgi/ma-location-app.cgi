#!/usr/bin/perl -w
###### ma-location-app.cgi
######
###### The application for entering and updating Individual locations of real Merchant Affiliates.
######  A call to update requires an update action and the location ID and is of the format:
######		ma-location-app.cgi?action=update&location_id=<location ID #>
######
###### 05/29/2003	Karl Kohrt
######
###### last modified: 02/13/06 	Karl Kohrt


use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw( $COACH_LEVEL $SALT %URLS );

###### GLOBALS
# Countries that require zip codes.
our @zip_countries = qw/US CA/;

our $q = new CGI;
our $ME = $q->url;
our %TEMPLATES = (	MAIN		=> 10168,
			OMA_MAIN	=> 10215,
			locations	=> 10191,
			location_row	=> 10188,
			options	=> 10216);

our ($db, $StateList, $StateHash) = ();
our $ma_info = '';
our @location_list = ();

###### our default prompt
our $default_prompt= qq|<span class="n">Fields with a </span><span class="r">red *</span><span class="n"> are required.</span>|; ;
our $prompts = '';

###### %field has the form of 'label','field name', 'value', 'html class type'
###### 'required flag (set to 1 if required)'.
###### class would be used for the label.
###### keys should match DB column names.
###### instead of nodb=>1 to signify which fields not to use in a query
###### we'll explicitly specify which ones to use
###### this way any keys that are added after this initial definition will automatically be left out of a query
our %field = (
	id		=> {value=>'', class=>'n', req=>0, db=>1},
	merchant_id	=> {value=>'', class=>'n', req=>0},
	vendor_id	=> {value=>'', class=>'n', req=>0},
	location_name	=> {value=>'', class=>'n', req=>1, db=>1},
	address1	=> {value=>'', class=>'n', req=>1, db=>1},
	address2	=> {value=>'', class=>'n', req=>0, db=>1},
	address3	=> {value=>'', class=>'n', req=>0, db=>1},
	city		=> {value=>'', class=>'n', req=>1, db=>1},
	state		=> {value=>'', class=>'n', req=>0, db=>1},
	state_code	=> {value=>'', class=>'n', req=>0},
	country	        => {value=>'', class=>'n', req=>1, db=>1},
	postalcode	=> {value=>'', class=>'n', req=>0, db=>1},
        longitude       => {value=>'', class='n', req=>0, db=>1},
        latitude        => {value=>'', class='n', req=>0, db=>1},
	phone		=> {value=>'', class=>'n', req=>1, db=>1},
	username	=> {value=>'', class=>'n', req=>1, db=>1},
	password	=> {value=>'', class=>'n', req=>1, db=>1},
	stamp		=> {value=>'', class=>'n', req=>0},
	operator	=> {value=>'', class=>'n', req=>0},
	notes		=> {value=>'', class=>'n', req=>0},
	url		=> {value=>'', class=>'n', req=>0},
	rebate		=> {value=>'', class=>'n', req=>0},
	reb_com	=> {value=>'', class=>'n', req=>0},
	pp		=> {value=>'', class=>'n', req=>0},
	cap		=> {value=>'', class=>'n', req=>0},
	discount	=> {value=>'', class=>'n', req=>0},
	status 	=> {value=>'', class=>'n', req=>0},
	vendor_group 	=> {value=>'', class=>'n', req=>0});

our $postalcode_required_class = 'wt';	# The 'required marker' default is white like the background.

###### these are flags for presenting or hiding blocks of the template that we want
###### to handle conditionally : 	1 = Enter new location info. (new unactivated Merchant)
######					2 = Successfully entered new location.
######					3 = Update existing location info.
######					4 = New Location for ACTIVATED Merchant
######					5 = Fee agreement for an 'oma' app
######					6 = Signature block for an 'oma' app											
our %conditionals = (1=>1, 2=>0, 3=>0, 4=>0, 5=>0, 6=>0);

##########################################
###### MAIN program begins here.
##########################################

###### there are two defined apptypes: oma (for online MA)
###### regular 'ma'
our $apptype = $q->param('apptype') || '';

our $action = $q->param('action') || '';

###### get our user from the cookie they should have
my $ck = $q->cookie('AuthCustom_maf');
unless ($ck)
{
	Err("You must <a href=\"$URLS{LOGIN}/maf?destination=$ME?apptype=$apptype\">login</a> to use this application.");
	exit;
}

( $field{merchant_id}{value}, $ma_info ) = G_S::authen_ses_key($ck, 'maf');

if (! $field{merchant_id}{value} || $field{merchant_id}{value} =~ /\D/)
{	###### it should be strictly numeric or its invalid
	Err("Your login has expired or is otherwise invalid.<br />Please <a href=\"$URLS{LOGIN}/maf?destination=$ME?apptype=$apptype\">login</a> again.");
	exit;
}

@location_list = split (/\,/, $ma_info->{maflocations});


if ( $ma_info->{membertype} eq 'mal' && $action ne 'update')
{
	Err("You're logged in as location #$field{merchant_id}{value}.&nbsp;&nbsp;If you wish to add a new location,<br />
		please logout and then <a href=\"$URLS{LOGIN}/maf?destination=$ME\">login</a> again using your \"Main Store\" ID and password.");
	exit;
}

unless ( $db = DB_Connect('ma-location-app.cgi') ){ exit }
$db->{RaiseError} = 1;

###### if this is an application and we don't yet have an apptype,
###### we'll display the selection form for choosing
if (! $apptype && $action ne 'update')
{
	Do_App_Options();
	goto 'END';
}

###### if we are doing a 'MarketPlace' location, we'll change the required fields
if ($apptype eq 'oma')
{
	foreach ('address1','city','state','country','phone')
	{
		$field{$_}{req} = 0;
	}

	$field{url}{req} = 1;

	###### we'll also display the fee agreement if this is a new application 
	$conditionals{5} = 1 if $action ne 'update';
}

###### we'll default to the 'getinfo' action to fill in the app with Master defaults
###### for MarketPlace applications
if (! $action && $apptype eq 'oma')
{
	$action = 'getinfo';
}

###### we enter the application for the first time
unless ($action)
{
	$prompts = $default_prompt;
	Do_Page();
}
elsif ($action eq 'getinfo')
{
	# Get the info from the Master Merchant Affiliate record.
	my $data = Get_Master_Info();

	if ($data)
	{
		foreach (keys %{$data})
		{
			$field{$_}{value} = $data->{$_};
		}

		# Do this here mainly to populate the state combo box.
		Check_Required();

		$prompts = $default_prompt;
		Do_Page();
	}
	else
	{
		Err('#1 Failed to retrieve master Merchant record');
	}
}
elsif ($action eq 'submit')
{
	Load_Params();

	###### If we return true, then we'll redo the page with errors flagged				
	if ( Check_Required() )
	{
		Do_Page();
	}

	###### Otherwise we'll create a location record and redraw the page for them to print out.
	###### We'll change the display with the different conditional structures
	else
	{

	###### check to see if we have done this before
		my $rv = G_S::Temp_Hash_ck(	$db,
						$q->remote_addr,
						$field{merchant_id}{value},
						$field{location_name}{value},
						$field{address1}{value},
						$field{phone}{value}
						);

	###### uncomment to debug without processing
	#	$rv = 1;
		
		if ($rv)
		{
			Err('It appears that you already submitted this application.');
		}
		else
		{
			$rv = Create_Location();
			if (! $rv || $rv ne '1' )
			{
				Err("There was an error accepting your application. Please try again.");
			}
			else
			{

			###### create a temp_hash record so that this cannot be submitted again
				G_S::Temp_Hash_insert(	$db,
								$q->remote_addr,
								$field{merchant_id}{value},
								$field{location_name}{value},
								$field{address1}{value},
								$field{phone}{value}
							);

			# Blank out the primary messages and show the secondary messages.
				$conditionals{1} = 0;

			# if they are a new vanilla Merchant Affiliate app, they need to see #2
			# otherwise they need to see #4
				if ($apptype eq 'oma')
				{
					$conditionals{4} = 1;
					$conditionals{6} = 1;
				}
				else
				{
					###### lets see if this is an activated Merchant
					my $data = Get_Master_Info();
					unless ($data)
					{
						Err('#2 Failed to retrieve master Merchant record');
						goto 'END';
					}

					unless (defined $data->{status})
					{
						$conditionals{2} = 1;
					}
					else
					{
						$conditionals{4} = 1;
					}
				}

				###### nullify the form so they cannot submit it again
				$ME = '';									

				Do_Page();
			}
		}
	}
}
elsif ($action eq 'update')
{
	# Blank out the primary messages and show the update messages.
	$conditionals{1} = 0;
	$conditionals{2} = 0;
	$conditionals{3} = 1;

	# The Location ID was passed in from outside this script.
	if ( $field{id}{value} = $q->param('location_id') )
	{
		unless ( Check_Location_ID() )
		{
			RedirectLogin();
			goto 'END';
		}

		Get_Location_Info();	# Get the info from the database.
	
		# Mark all required fields - needed because we're adding fields after signup.
		Check_Required();

		# in order to display the correct template for the location type
		# we'll have to set the apptype here
		$apptype ||= ($field{vendor_group}{value} eq '5') ? 'ma' : 'oma';

		Do_Page();		# Display the page.
	}
	# The Location ID was passed with a click of the update button.
	elsif ( $field{id}{value} = $q->param('id') )
	{
		Load_Params();	# Load the info passed via the parameters.
		Check_Location_ID();	# Check the passed ID, exit if it doesn't match the cookie.

		# Unless we are missing a required field, continue on.				
		unless ( Check_Required() )
		{
			# If the update was successful...
			if ( Update_Location() )
			{
				$prompts = qq|<div style="color: #0000ff">Location record successfully updated.</div>|;

				# Get the info from the database.
				Get_Location_Info();
			}
			# Otherwise there was an error.
			else
			{
				$prompts = qq|<div style="color: #ff0000">There was an error with the update. Please try again.</div>|;
			}
		}

		# Display the page.
		Do_Page();
	}
	# This is the initial entry into the update section.
	else
	{
		# First time through update, so we'll display the location(s) page, depending on
		#  whether this is a master or location login. If it is a location login, 
		#  recall the script with the single id in the path.	
		if ( $ma_info->{membertype} eq 'mal' )
		{
			print $q->redirect( $ME . "?action=update&location_id=" . $ma_info->{id} );
		}
		# If it is a master login...
		elsif ( $ma_info->{membertype} eq 'maf' )
		{
			my $how_many_locations = scalar @location_list;

			# If there is only one location, recall the script with the single id in the path.
			if ( $how_many_locations == 1 )
			{
				print $q->redirect( $ME . "?action=update&location_id=" . $location_list[0] );
			}
			# Otherwise, display a list of locations to choose from.
			else
			{
				Report_Locations();
			}
		}
	}																			
}

###### our last catchall case		
else { Err('Undefined action') }

END:
$db->disconnect;
exit;

###### Creates a combo box from which a state may be selected.
sub cboState
{
	###### if our list is not populated, then we'll put something in
	if ( ! defined $StateList || scalar @{$StateList} < 1)
	{
		$StateList = [''];
		$StateHash = {'' => 'Enter value below'};
	}

	my $name = shift;
	return $q->popup_menu(	-name=>$name,
					-default=>$field{state_code}{value},
					-values=>$StateList,
					-labels=>$StateHash,
					-force=>1);
}

###### Check to see that the location ID in the URL matches the location ID in the cookie.
sub Check_Location_ID
{
	foreach ( @location_list )
	{
		# If the cookie's location ID is in the list, return true
		return 1 if $_ eq $field{id}{value};
	}
}

###### Checks to see if all required fields have been filled out.
sub Check_Required
{
	###### if we have a country, then we can check to see if we have state data for it
	###### if we do, we can force the state requirement
	if ($apptype ne 'oma' && $field{country}{value})
	{
		$field{state_code}{req} = 1 if G_S::Country_Has_States($db, $field{country}{value});
	}

	###### we'll try doing this first in case we pick up the code from the submitted state field
	foreach (keys %field)
	{
		if (/state/ && ! /code/)
		{
			###### check for a standardized state
			my ($success, $country_match) = ();

			(
			$field{$_}{value},
			$field{($_ . '_code')}{value},
			$success,
			$country_match,
			$StateList,
			$StateHash
			) = G_S::Standard_State(	$db,
							$field{$_}{value},
							$field{($_ . '_code')}{value},
							$field{country}{value}
							);
		}
	}

	my $missing = 0;

	###### are we required			
	foreach (keys %field)
	{
		if ( ($_ =~ /postalcode/) )
		{
			$field{$_}{req} = Check_Zips();
			if ( $field{$_}{req} ) { $postalcode_required_class = 'r' };
		}						
		if ($field{$_}{req} == 1 && ! $field{$_}{value})
		{
			$missing = 1;
			$field{$_}{class} = 'r';
		}
	}

	if ($missing == 1)
	{
		$prompts = qq|<div style="color: #ff0000">Please enter the required information marked in Red.</div>|;
	}
	
	###### check the validity of the submitted value or carried value
	if ($field{submitted_id}{value} || $field{member_id}{value})
	{
		my $id = $field{member_id}{value} || $field{submitted_id}{value};
		my $qry = "SELECT id FROM members WHERE ";
		
		$qry .= ($id =~ /\D/) ? "alias= ?" : "id= ?";
		$id = $db->selectrow_array($qry, undef, $id);
		
		if ($id){ $field{member_id}{value} = $id }
		else
		{
			$field{member_id}{value} = '';
			$prompts .= qq|<div style="color: #aa0000">Unable to find a matching DHS Club member with the ID you provided.</div>|;
			$field{member_id}{class} = 'r';
		}
	}

	return $prompts;
}

###### Check to see if the merchant record requires a zip code.
sub Check_Zips
{
	$field{'vendor-group'}{value} ||= '';	# Avoid uninitialized errors.
	# If this is not an Online Merchant Affiliate.
	if ( $field{'vendor-group'}{value} ne '6' )
	{
		# Look at all countries in a list.		
		foreach ( @zip_countries )
		{
			# If an item in the list equals the country submitted, require zip codes.
			if ( $_ eq $field{country}{value} ) { return 1 }
		}
	}							
	# If we made it this far, this merchant doesn't require zip codes.
	return 0;	
}

###### Create special control menus for the page to be displayed.
sub Create_controls
{
	my @list = @_;

	foreach (@list){
	###### now we'll convert the 'value' to the needed form element with the value included
		if ( /country/ )
		{
			$field{$_}{control} = G_S::cbo_Country($q, $_, $field{$_}{value}, '');
		}
		elsif ( /state/ )
		{
			$field{$_}{control} = cboState($_);
		}
	###### this part used only for script generated forms
		else
		{
			$field{$_}{control} = $q->textfield(	-name=>$_,
									-size=>34,
									-maxlength=>50);
		}
	}
}

###### Create the Location record and insert it into the database.
sub Create_Location
{
	$field{id}{value} = $db->selectrow_array("SELECT NEXTVAL('merchant_affiliate_locations_id_seq')");
	return 'Failed to obtain a Location ID number' unless $field{id}{value};

	my $qry = '';
	my $vendor_id = 'NULL';	###### our default 'value'

	###### create our vendor record from our default Merchant master values
	###### by pulling in the status, we'll automatically setup active vendors or unactivated vendor
	###### depending on the state of the Merchant

		###### get the next vendor ID to link to
		$vendor_id = $db->selectrow_array("SELECT NEXTVAL('vendor_id_seq')");
		return 'Failed to generate a vendor ID.' unless $vendor_id;

		my $vg = ($apptype eq 'oma') ? 6 : 5;
		my $Url = $db->quote($field{url}{value});

		$qry = "	INSERT INTO vendors (
					vendor_id,
					vendor_name,
					vendor_group,
					status,									 
					url
				)
				SELECT $vendor_id AS vendor_id,
					'mal: $field{id}{value}' AS vendor_name,
					$vg AS vendor_group,						
					NULL AS status,
					CASE
						WHEN $Url ISNULL OR $Url = '' THEN url
						ELSE $Url
					END AS url
				FROM merchant_affiliates_master
				WHERE id= $field{merchant_id}{value};\n";

	# The notes for the location registration.
	my @list = "Merchant signup.\nIP: " . $q->remote_addr;

	$qry .= "INSERT INTO merchant_affiliate_locations (	vendor_id,
									merchant_id,
									notes\n";

	###### the first placeholder will for the notes field, above.
	my $qryvals = " VALUES ( $vendor_id, $field{merchant_id}{value}, ?\n";

	###### now we'll build out
	foreach my $fld (keys %field)
	{
		###### if this value is flagged as nodb it doesn't belong in the DB
		next unless $field{$fld}{db};

		###### if there is no value to insert, we'll leave it out of the query
		if ($field{$fld}{value})
		{
			$qry .= ", $fld\n";
			$qryvals .= ", ?";
			push (@list, $field{$fld}{value});
		}
	}
	$qry .= " ) $qryvals );\n";

	# Now the discount record.
	$qry .= "INSERT INTO merchant_affiliate_discounts (	location_id,
									discount_type,
									rebate,
									reb_com,
									pp,
									cap,	
									discount,
									start_date								
			)
			SELECT $field{id}{value} AS location_id,
				discount_type,
				rebate,
				reb_com,
				pp,
				cap,
				discount,
				NOW()::date AS start_date
				FROM merchant_affiliates_master
				WHERE id= $field{merchant_id}{value};\n";

#Err($qry); return 1;
		
	my $rv = $db->do($qry, undef, @list);
	return $rv;
}

sub Do_App_Options
{
	my $TMPL = G_S::Get_Object($db, $TEMPLATES{'options'}) || die "Cannot open Template: $TEMPLATES{'options'}";

	$TMPL =~ s/%%action%%/$ME/g;
	print $q->header(-charset=>'utf-8'), $TMPL;
}			

###### Display the page to the browser.
sub Do_Page
{
	my $tmpl = shift;
	$tmpl ||= ($apptype eq 'oma') ? 'OMA_MAIN' : 'MAIN';

	my $TMPL = G_S::Get_Object($db, $TEMPLATES{$tmpl}) || die "Cannot open Template: $TEMPLATES{$tmpl}";

	Create_controls('state_code', 'country');
	
	###### If our conditional flag is set we'll leave that block alone.
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$TMPL =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	###### Go through our list and parse out the placeholders.
	foreach (keys %field)
	{
		if ($field{$_}{control})
		{
			$TMPL =~ s/%%$_-control%%/$field{$_}{control}/;
		}
		else
		{
			###### some of our vals could be NULL or empty
			###### depending on where they go in the HTML doc, we'll handle them differently
			foreach ('rebate','reb_com','pp','discount','cap')
			{
				###### these values need to be non-breaking spaces since they are going into table
				$field{$_}{value} = '&nbsp;' unless $field{$_}{value};
			}

			$TMPL =~ s/%%$_%%/$field{$_}{value}/g;
		}

		$TMPL =~ s/%%$_-class%%/$field{$_}{class}/;
	}

	###### since $ME will be empty after the app is accepted, we need to fill our hyperlink with a real value
	$TMPL =~ s/%%my_url%%/$q->url/e;
	$TMPL =~ s/%%postalcode_required_class%%/$postalcode_required_class/;
	$TMPL =~ s/%%action%%/$ME/g;
	$TMPL =~ s/%%prompts%%/$prompts/;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $TMPL;
}

sub Err
{
	###### if we want to use a 'pretty' page as a holder for our message we'll try to
	###### pull up the template
	my $tmpl = '';
	my $error = shift;

###### for now we don't have a template defined, so we won't bother with the DB call
#	G_S::Get_Object($db, 'TEMPLATE_NUMBER') if $db;
	
	unless ($tmpl)
	{
		$tmpl = qq|
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
			<html><head>
			<title>Error</title>
			</head>
			<body bgcolor="#ffffff">
			<br><b>%%message%%</b>
			<p>Current server time: <tt>%%timestamp%% PST</tt>
			</body></html>|;
	}

	$tmpl =~ s/%%message%%/$error/;
	$tmpl =~ s/%%timestamp%%/scalar localtime()/e;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

###### Get the appropriate information from the location record passed to the script.
sub Get_Location_Info
{
	my $qry = "	SELECT merchant_id,
				location_name,
				address1,
				address2,
				address3,
				city,
				state,
				mal.country,
				mal.postalcode,
                                mal.latitude,
                                mal.longitude,
				phone,
				username,
				password,
				v.url,
				('\%' || ROUND(v.discount::NUMERIC * 100, 2)) AS discount,
				v.rebate,
				v.reb_com,
				v.pp,
				COALESCE(('\$' || v.cap::TEXT), '&nbsp;') AS cap,
				v.vendor_group
			FROM merchant_affiliate_locations mal
			LEFT JOIN vendors v ON v.vendor_id=mal.vendor_id
			WHERE mal.id = ?";

	my $data = $db->selectrow_hashref($qry, undef, $field{id}{value});

	# Populate the form fields with the Master data.
	foreach (keys %{$data})
	{
		$field{$_}{value} = $data->{$_} || '';	
	}
}

###### Get the appropriate information from the master record for this merchant.
sub Get_Master_Info
{

	my $qry = "	SELECT business_name AS location_name,
				address1,
				city,
				state,
				country,
				postalcode,
				phone,
				username,
				password,
				url,
				('\%' || ROUND(discount * 100, 2)) AS discount,
				rebate,
				reb_com,
				pp,
				COALESCE(('\$' || cap::TEXT), '&nbsp;') AS cap,
				status
			 FROM merchant_affiliates_master
			 WHERE id = ?";

	my $data = $db->selectrow_hashref($qry, undef, $field{merchant_id}{value});
	return $data;
}

###### Load the data from the form into the script variables.
sub Load_Params
{
	###### Populating our field array hash values.
	###### This loop depends on the %field hash keys being identical to the passed parameter names.
	foreach (keys %field){

		###### we are getting our merchant_id from the cookie unless we are updating.
		next if ( $_ eq 'merchant_id' && $action ne 'update' );
					
		$field{$_}{value} = $q->param($_) || '';
		next unless $field{$_}{value};

		$field{$_}{value} =~ G_S::PreProcess_Input( $field{$_}{value} );
			
		if ( /city|state|name/ )
		{
			###### we will upper case all the first letters of the name fields, city, state
			###### except username
			$field{$_}{value} = ucfirst($field{$_}{value}) unless /username/;
		}
		elsif ( /submitted_id/ )
		{
			$field{$_}{value} = uc $field{$_}{value};
		}
	}
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->self_url;

	print $q->header(-expires=>'now', -charset=>'utf-8'), $q->start_html(-bgcolor=>'#ffffff');
	print "The ID provided in the URL doesn't match your Location ID(s).<BR>
		If you have multiple Login ID's, you may logout and log back in
		<a href=\"$URLS{LOGIN}/maf?destination=$me\">here</a>:<BR><BR>";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

###### Display a list of locations if we have more than one.
sub Report_Locations
{
	my $html = '';
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $location_id = '';

	# Prepare a query to get details about each location id.
	my $qry = "	SELECT ma.id,
				ma.location_name,
				ma.city,
				ma.address1,
				ma.state,
				ma.country,
				v.vendor_group
			FROM 	merchant_affiliate_locations ma,
				vendors v
			WHERE 	v.vendor_id=ma.vendor_id
			AND 	v.status = 1
			AND	ma.id = ?";
	my $sth = $db->prepare($qry);

	my $location_row = G_S::Get_Object($db, $TEMPLATES{location_row}) || die "Couldn't open $TEMPLATES{location_row}";

	# Display a line for each location in the list.
	foreach $location_id (sort @location_list)
	{
		# Get the detailed info for the location listing.
		$sth->execute($location_id);
		$data = $sth->fetchrow_hashref();

		###### at this time (07/21/03) the location list derived from the cookie will contain all locations including inactive one
		###### since they won't show up in the list due to exclusion in the SQL above
		###### we'll skip the empty dataset
		next unless $data;

		###### set our apptype
		my $apptype = ($data->{vendor_group} eq '5') ? 'ma' : 'oma';

		# Start with a fresh template, then make all the substitutions.
		$one_row = $location_row;

		$one_row =~ s/%%id%%/$q->a( {	-href	=> "$ME?action=update&location_id=$data->{id}&apptype=$apptype",
							-target=> "Location_Window_" . $data->{id}},
							$data->{id} )/e;
		$one_row =~ s/%%location_name%%/$data->{location_name}/;
		$one_row =~ s/%%address1%%/${\($data->{address1} || '&nbsp;')}/;
		$one_row =~ s/%%city%%/${\($data->{city} || '&nbsp;')}/;
		$one_row =~ s/%%state%%/${\($data->{state} || '&nbsp;')}/;
		$one_row =~ s/%%country%%/${\($data->{country} || '&nbsp;')}/;

		# Add the new row to the other rows.
		$report_body .= $one_row;
	}

	$sth->finish();

	###### if report body is empty, then for some reason there were no locations
	$report_body ||= ( $one_row =~ s/%%.*?%%// );
								
	# Get the body template and make the substitutions.	
	my $tmpl = G_S::Get_Object($db, $TEMPLATES{locations}) || die "Couldn't open $TEMPLATES{locations}";
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;

	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

###### Update the location information.
sub Update_Location
{
	# The notes for the location update.
	my @list = "\nMerchant update.\nIP: " . $q->remote_addr;

 	my $qry = "	UPDATE merchant_affiliate_locations
			SET 	stamp = NOW(),
 				operator = CURRENT_USER,
				notes = ?,";

	foreach (keys %field)
	{
		next unless $field{$_}{db};

		if (! defined $field{$_}{value} || $field{$_}{value} eq '')
		{
			$qry .= "\n$_ = NULL,";
		}
		else
		{
			push (@list, $field{$_}{value});
			$qry .= "\n$_ = ?,";
		}
	}
	
	chop $qry;
	$qry .= "\nWHERE id = $field{id}{value}";

#	Troubleshooting line - Uncomment to view data and disconnect query execution.
#	Err($qry); foreach (@list) { Err($_) }; return 1;

	return $db->do($qry, undef, @list);
}

###### 06/24/03 Added the /maf reference to the 'login expired' href.
###### 06/27/03 revised the input filtering mechanisms
###### 07/02/03 Added the update functionality.
###### 07/10/03 Changed the MAIN template from test template 10189 back to 10168 after copying 10189 to 10168.
###### 07/14/03 Moved the call to Check_Location_ID() before the call to Get_Location_Info() in the update section.
###### 07/21/03 added an expires header modifier in Do_Page
###### also added a check for active locations in the Report_Locations routine by checking the status of the associated vendor record
###### 09/04/03 finished moving changes into place to handle 'MarketPlace' locations
###### 09/18/03 Added handling for the postalcode field.
###### 10/20/03 Added creation of the merchant_affiliate_discount record.
###### 11/12/03 Removed the insertion of the discount info into the vendors table.
###### 11/13/03 Added Canada - CA to the list of countries that require a zip code.
###### 02/07/06 made the insert/update queries execute within a 'do' instead of a prepare/execute
###### 02/13/06 Reduced several "$sth prepare/execute/finish" combos to their equivalent "selectrow..." statement.

