#!/usr/bin/perl -w
###### mailstatus.cgi
###### accepts user emailaddress and oid to effect a membertype change which will affect mail out to the user
###### this script is also admin aware
###### the behavior was changed on 05/26/10 to allow removal by a member by simply using an emailaddress
###### it will still work to identify a specific membership if an oid is provided as well
###### changed for going to now reward_transactions table
###### the latest change, 07/22/13, involves requiring two factor identification, be that emailaddress -and- oid or password -or- a valid full login cookie
###### this is to mitigate the removal of memberships by parties other than themselves... such as upline Partners with an axe to grind
# last modified: 05/12/15	Bill MacArthur

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use DBI;
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
use DB_Connect;
require XML::local_utils;
require ClubCash;
require cs_admin;
require ADMIN_DB;

$| = 1;

###### GLOBALS
my ($dbh, $adminCK, $admin, $adminFlg, $id, $meminfo, $operator) = ();
my $db = \&db;
my $q = new CGI;
$q->charset('utf-8');
my $gs = G_S->new({'CGI'=>$q, 'db'=>$db});

my %TMPL = (
	'shopper' 	=> 10117,
	'remove'	=> 10116,
	'cancel' 	=> 10118,
	'main_xsl'	=> 10560,	# opening screen
	'main_xml'	=> 10561);	# opening screen

my %conditionals = (1=>0, 2=>0);

if ($adminCK = $q->cookie('cs_admin'))
{
	my $cs = cs_admin->new();
	$admin = $cs->authenticate($adminCK);
	$adminFlg = 1;
}

my $action = $q->param('action');

my $oid = $q->param('oid') || $q->param('o') || '';
$oid =~ s/^\s*//;
$oid =~ s/\s*$//;

my $pwd = $q->param('pwd') || '';
$pwd =~ s/^\s*//;
$pwd =~ s/\s*$//;

my $email = lc($q->param('email') || $q->param('e') || '');
$email =~ s/^\s*//;
$email =~ s/\s*$//;

###### if we receive a 'downgrade' parameter, we'll drop the 'remove' option
$conditionals{1} = 1 if $q->param('d');

if (! $adminFlg)
{
	if ($q->cookie('AuthCustom_Generic') && !($oid || $pwd))	# if they are logged in and we are -not- receiving explicit two factor authentication, then do the login cookie branch
	{
		( $id, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
		
		###### a valid id will not have anything besides digits
		###### if it matches non-digits it may just be an expired cookie
		if ( $id && $id !~ /\D/ )
		{
			###### we don't need to do anything if its good
		}
		
		###### if the cookie test returns anything other than digits, then they are not logged in or its expired
		else
		{
			print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
			Exit();
		}
		
		# we have an email address in our cookie, but not an oid or password, so let's lookup the values and put them in their slots for regular processing below
		($email, $oid) = db()->selectrow_array('SELECT emailaddress, oid FROM members WHERE id= ?', undef, $id);
	}
	elsif (! $email)	# if they are not logged in, then they need at least the email address
	{
		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
#		Err('Missing parameter: email address.');
		Exit();
	}
	
	unless ($oid || $pwd)
	{
		Err('Missing either: unique number -or- password');
		Exit();
	}
}
elsif (! ($email && $oid))	# we are in admin mode without the necessary parameters
{
	Err('These parameters are required in admin mode: email address and oid (unique number)');
	Exit();
}

$operator = db()->quote(($admin->{'username'} || 'mailstatus.cgi'));

my $qry = '
	SELECT m.id,
			m.membertype,
			mt.label as status,
			m.alias,
			m.spid,
			m.emailaddress,
			m.firstname,
			m.lastname,
			m.oid,
			mt.can_remove_self
	FROM members m
	JOIN "Member_Types" mt
		ON m.membertype = mt."code"
	WHERE mt.active=TRUE';

my @args = ();
if ($email)
{
	push @args, $email;
	$qry .= ' AND m.emailaddress= ?';
}

if ($oid && $oid !~ /\D/)
{
	push @args, $oid;
	$qry .= ' AND m.oid= ?';
}

if ($pwd)
{
	push @args, $pwd;
	$qry .= ' AND password= ?';
}

my $sth = db()->prepare($qry);
my $rv = $sth->execute(@args);
if ($rv ne '0E0' && $rv > 1)
{
	Err('We have found multiple memberships matching that email address.<br />
		Please contact <a href="mailto:mbrsupport@clubshop.com">mbrsupport@clubshop.com</a> for assistance.<br />
		We apologize for the inconvenience.');
	$sth->finish;
	Exit();
}

my $member = $sth->fetchrow_hashref;
$sth->finish;

if (! $member )
{
	Err('An active membership was not found with those identifiers.');
	Exit();
}
elsif (! $member->{'can_remove_self'})
{
	Alert(qq|You cannot use this interface to remove a membership of type: $member->{'label'}<br />
		Please contact <a href="mailto:mbrsupport\@clubshop.com">mbrsupport\@clubshop.com</a> for assistance.<br />
		We apologize for the inconvenience.|);
	Exit();
}

unless ($action)
{	###### there is no action passed on a simple click-thru to this script

	###### remove the 'change to shopper option' for shoppers :)
	$conditionals{2} = 1 if $member->{'membertype'} eq 's';
	Options_Screen();
}
elsif ($action =~ m/remov/i)
{
	my $memo = "member action\n" . $q->remote_addr;
	if ($adminFlg)
	{
		$memo = "admin action";
		$memo .= ($q->param('comment')) ? ("\n" . $q->param('comment')) : '';
	}
	$memo = $dbh->quote($memo);

	$qry = "
		UPDATE members SET
		 	membertype= 'r',
			stamp= NOW(),
			operator= $operator,
			memo= $memo
		WHERE oid= $member->{'oid'};

		INSERT INTO notifications (id, notification_type)
		VALUES ($member->{'id'}, 2);
		
		DELETE FROM od.pool WHERE id= $member->{'id'};
		DELETE FROM network.positions WHERE id= $member->{'id'};
		UPDATE mviews.positions SET seq= maxintval() WHERE id= $member->{'id'};";

	if ( $dbh->do($qry) )
	{
		TrialPartnerOptOutActivity();
		if ($adminFlg)
		{
			print $q->redirect("https://www.clubshop.com/cgi/admin/memberinfo.cgi?id=$member->{'id'}");
		}
		else
		{
			Confirmation('remove');
		}
	}
	else
	{
		Err("There has been a problem updating your record: $DBI::errstr");
	}
}
elsif ($action =~ m/shop/i)
{
	my $memo = "member action\n" . $q->remote_addr;
	if ($adminFlg)
	{
		$memo = "admin action";
		$memo .= ($q->param('comment')) ? ("\n" . $q->param('comment')) : '';
	}
	$memo = $dbh->quote($memo);

	$qry = "
		UPDATE members SET
		 	membertype= 's',
			stamp= NOW(),
			operator= $operator,
			memo= $memo
		WHERE oid= $member->{'oid'};

		INSERT INTO notifications (id, notification_type)
		VALUES ($member->{'id'}, 3);
		
		DELETE FROM od.pool WHERE id= $member->{'id'};
		DELETE FROM network.positions WHERE id= $member->{'id'};
		UPDATE mviews.positions SET seq= maxintval() WHERE id= $member->{'id'};";

	if ( $dbh->do($qry) )
	{
		TrialPartnerOptOutActivity();
		if ($adminFlg)
		{
			print $q->redirect("https://www.clubshop.com/cgi/admin/memberinfo.cgi?id=$member->{id}");
		}
		else
		{
			Confirmation('shopper');
		}
	}
	else
	{
		Err("There has been a problem updating your record: $DBI::errstr");
	}
}
elsif ($adminFlg && $action =~ /duplicate/i)
{
	my $memo = 'admin action';
	if ($q->param('comment'))
	{
		$memo .= "\n" . $q->param('comment');
	}
	$memo = db()->quote($memo);

	if ( $dbh->do("
/*
		UPDATE members SET
		 	stamp= NOW(),
			operator= $operator,
			memo= 'previous sponsor being marked duplicate',
			spid= $member->{'spid'}
		WHERE spid= $member->{'id'};	
*/
		UPDATE members SET
		 	membertype= 'd',
			stamp= NOW(),
			operator= $operator,
			memo= $memo
		WHERE oid= $member->{'oid'};
		
		DELETE FROM od.pool WHERE id= $member->{'id'};
		DELETE FROM network.positions WHERE id= $member->{'id'};
		UPDATE mviews.positions SET seq= maxintval() WHERE id= $member->{'id'};") )
	{
		print 	$q->redirect("https://www.clubshop.com/cgi/admin/memberinfo.cgi?id=$member->{'id'}");
	}
	else
	{
		Err("The operation does not seem to have completed correctly: $DBI::errstr");
	}
}
else
{
	Confirmation('cancel');
}

Exit();

sub Alert
{
	my %ALERTS = (
		'r' => "Your membership is already marked as '$member->{status}'",
		't' => "Your membership is marked as '$member->{status}'",
		'd' => "Your membership is marked as '$member->{status}'",
		'c' => "Your membership is marked as '$member->{status}'",
		'v' => "You are a Partner");

	print 	$q->header(),
		$q->start_html('-bgcolor'=>'#FFFFFF'),
		$q->h4($ALERTS{$member->{'membertype'}});
		
	if ($member->{'membertype'} ne 'v')
	{
		print "<p>You cannot change your member status at this time.<br />Please contact us.</p>";
	}
	else
	{
		print "<p>You cannot change your member status here.<br />Please see our \n",
			$q->a({'-href'=>'http://www.clubshop.com/helpdesk.xml'}, 'FAQ'),
			'</p>';
	}
	 
	print <<FOOTER;
<!-- Content Footer -->
<table cellspacing="3" width="50%">
<tr><td>
<a href="http://www.clubshop.com/contact.xml">
<img src="/images/to/tocontactus.gif" alt="Contact Us!" border="0" height="29" width="90" /></a>
</td>
<td>
<a href="http://www.clubshop.com/">
<img src="/images/to/toclubshoplink.gif" alt="To ClubShop.com" border="0" height="29" width="121" /></a>
</td></tr></table><hr />
FOOTER

	print $q->end_html();
}

sub Confirmation
{
	my $TEXT = $gs->GetObject($TMPL{$_[0]}) || die "Failed to load template: $TMPL{$_[0]}\n";
	print $q->header();
	print $TEXT;
}

sub Create_hidden
{
	my $hid = '';
	if ($adminFlg)
	{
		$hid = "<table><tr><td valign=\"top\">\n";
		$hid .= $q->submit(	'-value'=>'Mark as Duplicate',
					'-onClick'=>"document.forms[0].action.value='Mark as Duplicate'");
		$hid .= '</td><td style=\"font-size: 9pt;\">&nbsp;&nbsp;&nbsp;<b>Memo: </b></td><td>';
		$hid .= $q->textarea(
				'-name'=>'comment',
				'-rows'=>3,
				'-cols'=>40);
		$hid .= "</td></tr><tr><td colspan=\"3\" style=\"color: #ff0000; font-size: 9pt;\">\n";
		$hid .= "All parties under this member will roll up to their sponsor.<br />
			If this is not what you want, do not proceed.</td></tr></table>\n";
	}

	foreach ($q->param())
	{	###### we need to pass all our parameters that have values except the action param
		$hid .= $q->hidden($_)."\n" if $_ ne 'action';
	}
	# if we didn't receive an oid parameter, we still want to create the hidden field from the oid of the member record
	$hid .= $q->hidden('-name'=>'oid', '-value'=>$member->{'oid'}) unless $oid;
	return $hid;
}

sub db
{
	return $dbh if $dbh;

	if ($adminFlg)
	{
		exit unless $dbh = ADMIN_DB::DB_Connect( 'mailstatus', $admin->{'username'}, $admin->{'password'} );
	}
	else
	{
		exit unless $dbh = DB_Connect('mailstatus');
	}
	$dbh->{'RaiseError'} = 1;
	return $dbh;
}

sub Err
{
	print
		$q->header(),
		$q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Removal error'),
		$q->h4('There has been a problem'),
		$q->div($_[0]);
	print $q->end_html();
}

sub Exit
{
	$dbh->disconnect if $dbh;
	exit;
}

sub Options_Screen
{
	my $xml = $gs->GetObject($TMPL{'main_xml'}) || die "Failed to load template: $TMPL{'main_xml'}\n";
	my $xsl = $gs->GetObject($TMPL{'main_xsl'}) || die "Failed to load template: $TMPL{'main_xsl'}\n";

	foreach (keys %conditionals)
	{
		$xsl =~ s/conditional-$_-open.+?conditional-$_-close/ /sg if $conditionals{$_};
	}

	my $PAGE = XML::local_utils::xslt($xsl, $xml);

	# Here we get the reward points total.
	my $cc_amt = ClubCash::Get_ClubCash_Amounts($member->{'id'}, 'mailstatus');
	$member->{'points'} = $cc_amt->{'pending'} + $cc_amt->{'redeem'};
	$member->{'currency'} = $cc_amt->{'currency'};

	print $q->header('-charset'=>'utf-8');
	$PAGE =~ s/%%alias%%/$member->{alias}/g;
	$PAGE =~ s/%%points%%/$member->{points}/g;
    $PAGE =~ s/%%currency%%/$member->{currency}/g;
	$PAGE =~ s/%%status%%/$member->{status}/g;
	$PAGE =~ s/%%hidden%%/Create_hidden()/e;
	$PAGE =~ s/%%action%%/$q->script_name/eg;
	print $PAGE;	
}

# the person downgrading/removing is a Trial Partner
# per Dick: can we make this an Activity too, as Partners see them as TP's  to followup on one day, only to see them disappear with no knowledge or understanding what happened.
sub TrialPartnerOptOutActivity 
{
	$dbh->do("INSERT INTO activity_rpt_data(id, spid, activity_type) VALUES ($member->{'id'}, $member->{'spid'}, 67)");
}

__END__

###### 07/31/01 changed the first query to simply get the email address and then compare rather than to select
###### using oid and email address as the query modifiers
###### 08/24/01 converted to OO CGI methods
###### 11/21/01 made the src for image tags referential
###### 11/26/01 remapped the paths to templates
###### 04/19/02 changed to perform rollups of anybody under a removal or downgrade
###### as well as inserting an entry into the notifications system for batch processing
###### 06/24/02 fixed a bug which would pull up a membership for removal using just an oid even if the email address didn't match
###### 06/27/02 added extra checks on submitted vars to keep them within reasonable limits
###### 10/21/02 moved the membertype check to preceed any action stuff since we had a case of a VIP removing themselves, probably through URL munging 
###### 12/19/02 went to the 'use' method for custom modules
###### 01/30/03 went to the cgi_object system for most templates
###### 05/30/03 changed the DB connection to NOT connect under its own name
###### also did a little reformatting for easier reading and removed some references to Partners
###### 12/09/03 disabled the email check -NOT- turns out it didn't need it
###### 01/20/04 added admin capability to 'remove' those without an email address
###### also added duplicate handling capabilities
###### 01/21/04 blended the comments and operator into admin 'removals' as well
###### 02/09/04 added handling for the 'd' parameter which is simply a flag that the
###### app will be used for explicit downgrade rather than downgrade/removal
###### 07/07/04 revised the lenght boundary check on the oid to be a numerical limit ck.
###### 10/20/05 Added the lower() function to the emailaddress in the query's $email_criteria 
######		due to not finding "Johnson48Sweetie@aol.com" for removal.
######	04/24/06 Modified Options_Screen with the new xml/xsl form handling and reward points.
# 09/12/08 changed the URL for the 'FAQ'
# 09/30/09 added the IP address to the memo for removals and downgrades
# 05/14/10 minor tweaks to the cannot do it screen, nost notably a fix in the contact page URL
# 02/23/11 replaced reward_trans select with Get_ClubCash_Amounts
###### 03/08/12 made it so that an alphanumeric "oid" value is ignored
###### since so many people apparently confused by the field label/description/instructions
###### 07/23/13 rolled out with added identification functionalities and a redirection to login if called without parameters
04/14/15
	Added SQL for managing mviews.positions, altered the logic that determined if they could remove themselves to use the flag in member types, also did some code cleanup
	