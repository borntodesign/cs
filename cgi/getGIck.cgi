#!/usr/bin/perl -w
###### getGIck.cgi.cgi
######
###### A script intended as an SSI to create an IFRAME tag to load a simple GI script that will set a GI login cookie 
######
###### created: 07/06/10	Bill MacArthur
######
###### modified: 
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;

# 
my $q = new CGI;

my ( $member_id, $mem_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
Err('invalid login') unless $member_id && $member_id !~ m/\D/;

Err('failed to obtain a DB connection') unless my $db = DB_Connect('generic', 0, {fail_silently=>1});

# the password is not contained in the cookie, so we have to look it up
# since we are not doing GI logins for non-GI memberships we'll do a join to exclude all others
$mem_info = $db->selectrow_hashref('
	SELECT m.alias, m.password
	FROM members m
	JOIN "Member_Types" mt
		ON mt."code"=m.membertype
	WHERE mt.gi_no_dupes=TRUE
	AND m.id= ?', undef, $member_id);

Err() unless $mem_info;

my $url = 'https://www.glocalincome.com/cgi/Login.cgi?';
$url .= '_action=ckonly;';
$url .= 'username=' . $q->escape($mem_info->{alias}) . ';';
$url .= 'password=' . $q->escape($mem_info->{password});

print $q->header('text/plain');
print qq|<iframe height="200" src="$url" style="visibility:hidden" width="800"></iframe>|;

Exit();

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	# we don't want any nasty errors, so we will simply issue a warning and deliver a header and an empty document
	warn "getGIck.cgi: $_[0]" if $_[0];
	print $q->header();
	Exit();
}