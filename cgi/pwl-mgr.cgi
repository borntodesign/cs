#!/usr/bin/perl -w
###### pwl-mgr.cgi	an application for managing ones Personal WebLink
###### since the personal mailing flag is incorporated into the mgt. interface,
###### we'll be  handling that as well
######	last modified 02/01/2006	Bill MacArthur

use CGI();
use CGI::Carp qw(fatalsToBrowser);
use strict;
use DBI;
use Digest::MD5 qw( md5_hex );
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL %MEMBER_TYPES );
use DB_Connect;

###### GLOBALS
our %TMPL = (	Main	=> 10124,
		Error	=> 10125);

our $q = new CGI;
our $ME = $q->script_name;
our ($db) = ();
our $OPTIONS_TBL = 'member_options';	###### the table that holds our member options
our $OPTION_TYPE = 2;			###### the type of option that refers to a WebLink
our %conditionals = (js1=>1);

our ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
unless ( $id && $id !~ /\D/ )
{
	my $msg = "You must be logged in to use this function.<br><a href=\"$LOGIN_URL?destination=";
	$msg .= $q->script_name . "\">Click here</a> to login.";
	ErrUser($msg); exit;
}

unless ( $meminfo->{membertype} eq 'v' )
{
	ErrUser("This resource is not available to $MEMBER_TYPES{$meminfo->{membertype}}s.");
	exit;
}

unless ($db = DB_Connect('pwl-mgr.cgi')){exit}
$db->{RaiseError} = 1;

###### if there is no action parameter, we'll simply output a template
unless ( $q->param('action') )
{
	###### remove the javascript on the initial invocation
	$conditionals{js1} = 0;
	Do_Page();
}
elsif ( $q->param('action') eq 'update')
{
	my $wl = $q->param('wl') || '';
	unless ( $wl )
	{
		$db->do("
			DELETE FROM $OPTIONS_TBL
			WHERE option_type= $OPTION_TYPE AND id= $id");
		Do_Page();
	}
	elsif ($wl =~ /clubshop\.com/i)
	{
		ErrUser('Weblinks cannot contain references to clubshop.com sites.');
	}
	elsif ( length $wl > 200 )
	{
		ErrUser('WebLink cannot exceed 200 characters');
	}
	elsif ( $wl !~ m#^(http|https)(://)#)
	{
		my $msg = '	WebLink must be a well formed URL.<br />
				Please press your back button and see the note at the bottom of the page.';
		ErrUser($msg);
	}
	else
	{
		my $res = $db->do("
			UPDATE $OPTIONS_TBL SET
				stamp= NOW(),
				val_text= ?
			WHERE option_type= $OPTION_TYPE AND id= ?", undef, ($wl, $id));

###### if we don't get an update of 1, then we will need to start fresh/new
		unless ($res eq '1')
		{
			$db->do("
				DELETE FROM $OPTIONS_TBL WHERE option_type= $OPTION_TYPE AND id= ?;
				INSERT INTO $OPTIONS_TBL (id, option_type, val_bool, val_text)
				VALUES (?, $OPTION_TYPE, TRUE, ?)", undef, ($id, $id, $wl));
		}
		Do_Page();
	}
}
elsif ($q->param('action') eq 'pmflag')
{
	my $qry = "DELETE FROM member_options WHERE id= $id AND option_type= 3";

###### we will simply delete the row if they elect not to have the mailings
###### that way we can just create a row if they decide to and we don't have to worry about updates
	if ($q->param('pmflag')){
		$qry .= ";\nINSERT INTO member_options (id, option_type, val_bool) VALUES ($id, 3, FALSE)";
	}
	$db->do($qry);
	Do_Page();
}
else{ ErrUser('Undefined Action') }
$db->disconnect;
exit;

sub Do_Page
{
	my $tmpl = shift || 'Main';
	my $wl = Get_WebLink() || '';

###### unless we insert some kind of value in the Click to Test link, browsers default to the home directory
	my $wl_test = $wl || 'javascript:void()';

	my $page = G_S::Get_Object($db, $TMPL{$tmpl}) || die "Couldn't open the template: $TMPL{$tmpl}\n";

	my ($btn1, $btn2) = Generate_pmflag_btns();

	###### if our conditional flag is set we'll leave that block alone
	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$page =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}

	$page =~ s/%%ON-button%%/$btn1/;
	$page =~ s/%%OFF-button%%/$btn2/;
	$page =~ s/%%wl%%/$wl/;
	$page =~ s/%%wl-test%%/$wl_test/;
	$page =~ s/%%action%%/$ME/g;
	print $q->header(-charset=>'utf-8'), $page;
}

sub ErrUser
{
###### we'll use a pretty template if we can get it, otherwise, we'll go vanilla
	my ($tmpl) = ();
	if ($db)
	{
		$tmpl = G_S::Get_Object($db, $TMPL{Error}) ||
		die "Failed to load template: $TMPL{Error}\n\nPlus you have this 'error':\n$_[0]\n";
	}

###### if we don't have a DB connection, then we'll setup a vanilla template
	unless ($tmpl)
	{
		$tmpl = $q->start_html(-bgcolor=>'#ffffff', -title=>'Transfer error');
		$tmpl .= '%%error_msg%%<br /><br />';
		$tmpl .= '%%timestamp%%';
		$tmpl .= $q->end_html();
	}

	my $stamp = scalar localtime();
	$tmpl =~ s/%%error_msg%%/$_[0]/;
	$tmpl =~ s/%%timestamp%%/$stamp/;
	print $q->header(-expires=>'now', -charset=>'utf-8'), $tmpl;
}

sub Generate_pmflag_btns
{
	my $btn1 = qq#<input type="radio" name="pmflag" value="0" #;
	my $btn2 = qq#<input type="radio" name="pmflag" value="1" #;

	my $sth = $db->prepare("
		SELECT id FROM
		$OPTIONS_TBL
		WHERE option_type= 3
		AND val_bool= FALSE
		AND id= $id");
		
	$sth->execute;
	my $tmp = $sth->fetchrow_array;
	$sth->finish;
	
	if($tmp)
	{
		$btn2 .= 'checked />';
		$btn1 .= '/>';
	}
	else
	{
		$btn1 .= 'checked />';
		$btn2 .= '/>';
	}
	return ($btn1, $btn2);
}

sub Get_WebLink
{
	my $sth = $db->prepare("
		SELECT COALESCE(val_text, '') AS weblink
		FROM $OPTIONS_TBL WHERE option_type= 2 AND id= $id");
	$sth->execute;
	my $wl = $sth->fetchrow_array;
	$sth->finish;
	return $wl;
}

###### 02/17/04 implemented a test for clubshop.com in the weblink submissions
###### in order to flag them as errors
###### 07/12/04 added a conditional for a javascript to work with the 'control panel'
# 02/01/06 revised the delete statements to go inside a 'do' instead of a 'prepare'