#!/usr/local/bin/perl
#
# This script gets your current location by either ip address, a default address set by you earlier, or if you type
# in an address. Given that address, it locates all merchants within a given diameter (actually a box). From that list,
# all of the categories are collected and placed in a pull down menu.  
use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Cookie;
use Data::Dumper;

my $cgi = new CGI;
my $cookiedata = "";
#
# no default or current found, use the ipaddress
#
    my $ip = $cgi->remote_host();
    my @tokes = split('\.',, $ip);
    my $nip = @tokes[3] + @tokes[2] * 256 + @tokes[1] * 256 * 256 + @tokes[0] * 256 * 256 * 256;
    my $iplocat = "Englewood, Fl, US | -83.3536 | 26.8896 | 25.0 | Miles";
    warn Dumper($iplocat);
    my $loccookie = $cgi->cookie( -name=>"rewards_cur_location",-expires=>"+45m", -domain=>"clubshop.com", -value=>$iplocat);
    warn Dumper($loccookie);

    print $cgi->header(-cookie=>$loccookie);
    warn "Putting out cookie";
print "<html>\n";
print "<head>\n";
print "<title>IP Address Display</title>\n";
    print "</head>\n";
    print "<body onload=\"initialize()\">\n";
print "</body></html>\n";
