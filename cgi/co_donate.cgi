#!/usr/bin/perl -w
###### co_donate.cgi
######
###### The member script for searching and selecting Charitable Organization records.
######		They can also record a donation preference with this script.
######
###### created: 02/22/05	Karl Kohrt
######
###### modified: 12/01/06 Shane Partain
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use DBI;
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser);

###### CONSTANTS
use State_List qw( %STATE_CODES );
our $SEL_AGE = '30 days';	# The age of a selection before another can be submitted.
				#  A valid PostgreSQL Interval value.

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our $nom_script = "/cgi/tmp/co_nomination.cgi";
our $xs = new XML::Simple(NoAttr=>1);
our %TMPL = (	'search'	=> 10393,
		'xml'		=> 10638,
		'display'	=> 10636,
		'searchlist'	=> 10442,
		'mylist'	=> 10443 );

# 10636
# 10395

our $db = '';
our $xml_extra = '';
our $donation_xml = '';
our $member_id = '';
our $mem_info = ();
our $action = ( $q->param('action') || '' );
our $deactivate = ($q->param('deactivate') || '' );
our $search_field = $q->param('search_field');
our $search_value = $q->param('search_value') || '';
$search_value = G_S::PreProcess_Input($search_value) if $search_value;
our $alias = '';
our $membertype = '';
our $access_type = '';
our $login_error = 0;
#our $data = '';
our $status_message = '';
our $search_value2 = '';
our $msg = '';
our $result_list = ();

our @field_names = qw/type type_id co_name firstname lastname
			address1 address2 city state country postalcode
			phone fax stamp active category/;
# Create a string to match the array above for use in queries.
our $field_names_string = join ', ', @field_names;
our @char_fields = qw/=type
			=co_name
			=firstname
			=lastname
			=address1
			=address2
			=city
			=state
			=country
			=postalcode
			=phone
			=fax
			=active/;
our @int_fields = qw/=id
			=type_id
			=category/;

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

# Continue if we have a good login/cookie.
unless ( $login_error )
{
	###### If this is the first time through, just print the Search page.
	unless ( $action )
	{
		Do_Page('search');
	}
	elsif ($action eq 'search')
	{	
		# Check the validity of the field and the value provided by the user.
		if ( Check_Validity() )
		{
			Run_Query();
			# If no records were found.
			unless ( $result_list )
			{
				$msg .= qq!<span class="bad"><xsl:value-of select="//lang_blocks/messages/no_match" /></span>!;
				Do_Page('search');
			}
			# If a list of records were found.
			elsif ( scalar (keys %{$result_list}) > 1 )
			{
				# Create a list report.
				Do_Page('searchlist');
			}
			# If one record was found.
			else
			{
#				$data = shift @result_list;
				Do_Page('display');
			}
		}
		else
		{
			Do_Page('search');
		}
	}
	elsif ($action eq 'show')
	{
		if ( Check_Validity() )
		{
			Run_Query();

			###### at this point we should have one row of data
			unless ( $result_list )
			{
				$msg .= qq!<span class="bad"><xsl:value-of select="//base/lang_blocks/messages/no_match" /></span>!;
				Do_Page('search');
			}
			else
			{		
#				$data = shift @result_list;
				Do_Page('display');
			}
		}
		else
		{
			Do_Page('search');
		}
	}	
	elsif ( $action eq 'donate' )
	{
		(my $how_much = $q->param('how_much')) =~ s/[^(0-9|\.)]//g;
		$how_much = sprintf "%.2f", $how_much;

		# Put the donation information into an XML format.
		$donation_xml = "	<what>" . $q->param('what') . "</what>
					<how>" . $q->param('how') . "</how>
					<how_much>" . $how_much . "</how_much>
					";
		# If a donation record exists for this member and CO.
		if ( my $donation = Donation_Exists() )
		{
			# If it's active, give a message and get out.
			if ( $donation->{active} )
			{
				$msg = qq!<span class=\"bad\"><xsl:value-of select="//base/lang_blocks/messages/don_active" /></span>\n!;
			}
			# If it's not active, update and activate it.
			else
			{
				Donate('update', $donation->{pk});
			}
		}
		else { Donate('insert') }

		# Get the most recent data and display it.
		$search_field = 'type';
		$search_value = $q->param('type');
		$search_value2 = $q->param('type_id');
		Run_Query();

		Do_Page('display');
	}
	elsif ($action eq 'mylist')
	{
		if ($deactivate){ deactivate(); }
		# Get the member's list of donations.
		Get_Donations();
		Do_Page('mylist');
	}
	else { Err('Undefined action.') }
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	$msg .= qq#\n	<span class="bad">
				<xsl:value-of select="//lang_blocks/messages/login1" />
				<xsl:text> </xsl:text>
				<a href="$LOGIN_URL?destination=$ME">
				<xsl:value-of select="//lang_blocks/messages/login2" />
				</a>
				</span><br/>\n#;
	Do_Page('search');
}

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

###### A country drop-down menu.
sub cbo_Country
{
	require '/home/httpd/cgi-lib/CC.list';
	return $q->popup_menu(	-name=>'ccodes',
					-class=>'note',
					-values=>\@CC::COUNTRY_CODES,
					-default=>'',
					-labels=>\%CC::COUNTRIES,
					-force=>'true',
					-onchange=>"document.forms[0].search_value.value=document.forms[0].ccodes.value",
					-onFocus=>"document.forms[0].search_field[4].checked=true;document.forms[0].search_value.value=document.forms[0].ccodes.value");
}

###### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	# No value to check if searching all.
	if ( $search_field eq 'all' ) { return 1 }

	if ( (grep { /=$search_field/ } @char_fields)
		&& (defined $search_value)
		&& ($search_value ne '') ) {} # Do nothing because all keyboard input is acceptable.
	elsif (grep { /=$search_field/ } @int_fields)
	{	# Contains letters so not valid for the integer field.
		if ($search_value =~ /\D/)
		{
			$msg= $q->span({-style=>'color: #ff0000'},
				qq!<xsl:value-of select="//lang_blocks/messages/bad_num" /> $search_field.!);
			return;	
		}
	}
	elsif (! defined $search_value || $search_value eq '' )
		{
			$msg= $q->span({-style=>'color: #ff0000'},
				qq!<xsl:value-of select="//lang_blocks/messages/blank" />!);
			return;	
		}
	else
	{
		$msg= $q->span({-style=>'color: #ff0000'},
				qq!<xsl:value-of select="//lang_blocks/messages/invalid" />!);
		return;				
	}
	return 1;						
}

sub deactivate
{
	$deactivate = $db->quote($deactivate);
	$db->do("UPDATE co_selections
		SET active=FALSE
		WHERE pk=$deactivate
		AND member_id=$member_id");	
}

###### Display record(s) to the browser.
sub Do_Page
{
	my $tmpl_name = shift;
	my $xml = $gs->Get_Object($db, $TMPL{xml}) ||
		die "Failed to load template: $TMPL{xml}\n";
	# Add the user info for page personalization.
	if ( $mem_info )
	{
		my $user_xml = $xs->XMLout($mem_info, RootName=>'user') ;
		$xml .= $user_xml;
	}

	my $xsl = G_S::Get_Object($db, $TMPL{$tmpl_name}) || die "Couldn't open $TMPL{$tmpl_name}";
	my $results_xml = '';
	unless ( $tmpl_name eq 'search' )
	{
		# Combine the CO $results_xml with the content xml.
		$results_xml = $xs->XMLout($result_list, RootName=>'results');
	}
	$xml = "<base>\n"
		. $xml . "\n"
		. $xml_extra . "\n"
		. $results_xml
		. "\n</base>\n";
	if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
	else
	{
		if ( $msg )
		{
			$msg = qq#<tr><td colspan="5" align="center"><hr width="30%"/>
					$msg
					<hr width="30%"/></td></tr>#;
		}
		$xsl =~ s/%%messages%%/$msg/g;
		$xsl =~ s/%%ME%%/$ME/g;
		$xsl =~ s/%%nom_script%%/$nom_script/g;
		$xsl =~ s/%%LOGIN_URL%%/$LOGIN_URL/g;

		my $xml = XML::local_utils::xslt($xsl, $xml);

		# This goes after the XSL transformation since it has forbidden characters.
		$xml =~ s/%%ccodes%%/cbo_Country()/ge;
		print $q->header(-charset=>'utf-8'), $xml;
	}
}

###### Insert/update a donation record.
sub Donate
{
	my ($sql_action, $pk) = @_;
	my $value_list = '';
	my @values = ();
	my $qry = '';
	my $sth = '';
	my $rv = '';
	my $status_string = '';

	if ( $sql_action eq 'insert' )
	{
		# Insert the donation selection into the co_selections table.
		$qry = "	INSERT INTO 	co_selections (
						member_id,
						type,
						type_id,
						donation,
						active,
						start,
						stamp,
						operator
				) VALUES (	?,
						?,
						?,
						?,
						't',
						NOW(),
						NOW(),
						current_user	);";
		push (@values, $member_id, 
				$q->param('type'),
				$q->param('type_id'),
				$donation_xml);
	}
	elsif ( $sql_action eq 'update' )
	{
		# Update the donation selection found in the co_selections table.
		$qry = "	UPDATE co_selections
				SET	donation = ?,
					active = 't',
					start = NOW(),
					stamp = NOW(),
					operator = current_user
				WHERE 	pk = ?;";
		push (@values, $donation_xml, $pk);
	}
#Err($qry); foreach ( @values ) { Err($_); }
	$sth = $db->prepare($qry);
	$rv = $sth->execute(@values);
	$sth->finish;

	if ( $rv )
	{
		$msg = qq!<span class=\"good\"><xsl:value-of select="//base/lang_blocks/messages/don_success" /></span>\n!;
	}
	else
	{
		$msg = qq!<span class=\"bad\"><xsl:value-of select="//base/lang_blocks/messages/don_fail" /></span>\n!;
	}		
	return 1;
}

###### Check to see if a donation record already exists for this member and CO.
sub Donation_Exists
{
	my $record = $db->selectrow_hashref("	SELECT * 
							FROM 	co_selections
							WHERE 	member_id= ?
							AND	type= ?
							AND	type_id= ?;",
								undef, 
								($member_id, 
								$q->param('type'),
								$q->param('type_id')));
#foreach ( keys %{$record} ) { Err("$_= $record->{$_}"); }
	if ( $record->{pk} ) { return $record }
	else { return 0 }
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br>$_[0]<br>";
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
#	if ( $q->param('admin') )
#	{
#		# Get the admin user and try logging into the DB
#		my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
#		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
#		# Check for errors.
#		unless ($db = ADMIN_DB::DB_Connect( 'co_donate.cgi', $operator, $pwd ))
#		{
#			$login_error = 1;
#		}
#		# Mark this as an admin user.
#		$admin = 1;
#	}
#	# If this is a member.		 
#	else
#	{
		unless ($db = DB_Connect( 'co_donate.cgi' )){exit}
		$db->{RaiseError} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id )
		{
			# Assign the info found in the VIP cookie.
			$alias = $mem_info->{alias};
			$membertype = $mem_info->{membertype};					
		}
		# If we didn't get a member ID, we'll check for a ppreport cookie unless they're donating.
		elsif ( $action ne 'donate' )
		{
			$member_id = G_S::PreProcess_Input($q->cookie('id')) || '';
			$alias = $db->selectrow_array("SELECT alias
								FROM 	members
								WHERE id = $member_id") if $member_id;
			# If we found an alias, they must be a member.
			if ( $alias ) { $membertype = "m" };
		}					
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
#			RedirectLogin();
			$login_error = 1;
		}
#	}
}

###### Get all donations for this member.
sub Get_Donations
{
	my $sth = $db->prepare("	SELECT s.pk,
						s.type,
						s.type_id,
						s.active,
						s.donation,
						s.start::date,
						s.stamp,
						l.co_name
					FROM 	co_selections s
						INNER JOIN co_list_view l
							ON s.type = l.type
							AND s.type_id = l.type_id
					WHERE 	s.member_id= ?;");
				# Use this if we don't want to show de-activated donations.
				#	AND 	(l.active = 't'
				#		OR l.active IS NULL);");
	$sth->execute( $member_id );
	
	$xml_extra =	"<donations>";
	while ( my $data = $sth->fetchrow_hashref)
	{
		$result_list->{"D$data->{pk}"} = $data;
		$xml_extra .= "<D$data->{pk}>$data->{donation}</D$data->{pk}>";
		$xml_extra .= "<D$data->{pk}><pk>" . '/cgi/co_donate.cgi?action=mylist;deactivate=' . 
				$data->{pk} . "</pk></D$data->{pk}>";
 	}		
	$xml_extra .= "</donations>";
	$sth->finish();

	# Put out a message if no records were found.
	unless (scalar (keys %{$result_list}))
	{
		$msg = qq!<span class=\"bad\"><xsl:value-of select="//base/lang_blocks/messages/no_match" /></span>\n!;
	}		
	return 1;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	print $q->redirect( $LOGIN_URL . "?destination=" .$ME );
}

##### Runs the specified query, pushing the results onto a list (array).
sub Run_Query
{
	my ($qry, $sth) = ();
	my @values = ();

	$qry = "SELECT * FROM co_list_view ";

	if ($search_field eq 'all')
	{
		$qry .= "WHERE (active = '1' OR active IS NULL) ";
	}
	else
	{
		$qry .= "WHERE ";

		if ( (grep { /=$search_field/ } @char_fields)
			&& ($search_field ne 'type') )
		{ 
			$qry .= "$search_field ~* ?";
		}
		else
		{
			$qry .= "$search_field = ?";
			# If this is a search by type, we also need the type_id field.
			if ( $search_field eq 'type' )
			{ 
				$qry .= " AND type_id = ?";
			}
		}
		# Get only active or pending COs. 
		$qry .= " AND (active = '1' OR active IS NULL)";

		@values = ($search_value);
		$search_value2 = $q->param('search_value2') if $q->param('search_value2');
		push (@values, $search_value2) if $search_value2 && $search_field ne 'all';
	}
	$qry .= " ORDER BY co_name;";
	$sth = $db->prepare($qry);
	$sth->execute( @values );
	
	while ( my $data = $sth->fetchrow_hashref)
	{
		# Blank out the sensitive data since it can be viewed with the 'xml=1' parameter.
		$data->{username} = '*';
		$data->{password} = '*';
		$data->{notes} = '*';
		$data->{co_notes} = '*';
		$data->{referral_id} = '*';
		$data->{operator} = '*';
		$data->{co_operator} = '*';

		foreach ( keys %{$data} ) { $data->{$_} ||= '' }
		$result_list->{"$data->{type}$data->{id}"} = $data;
 	}		
	$sth->finish();
}


###### 08/23/05 Added the de-activated donation selections into the results list.
# 12/01/06 Added a link to deactivate a selection






