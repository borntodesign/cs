#!/usr/bin/perl -w
###### cb_card_transfer.cgi						
###### Written: 08/05/2003	Karl Kohrt
######
###### This script will allow a member to transfer ownership of ClubBucks cards.
######
###### Last modified: 02/17/2014	Bill MacArthur
######

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S qw( $SALT %URLS );
use DB_Connect;

###### CONSTANTS
use constant MAXPERSONALCARDS => 12;	# Maxinum number of cards per member.

###### GLOBALS
our %TBL = (	'master'	=> 'clubbucks_master',
		'members'	=> 'members');
our %TMPL = (	'many_cards'	=> 10209,
		'one_card'	=> 10220,
		'tri_fold_a'	=> 10224);
our $page_template = '';
our $member_levels = "('v','m','ag','ma','tp')";		# The array of membertypes that can receive transfers.
our $one_card_member_levels = "('v','m','s','ag','ma','tp')";	# The array of membertypes that can receive one card.
our $cards_per_sheet = 0;
		
our $q = new CGI;
our $ME = $q->url;
our $action = ( $q->param('action') || '' );
our $db = '';				
our $Login_ID = '';
our $member_info = '';
our $membertype = '';
our $dover_msg = '';
our $admin = 0;		# Default is no admin access.
our $operator = '';
our $transfer_id = '';
our $transfer_name = '';
our $confirm_page = 0;
#our $param_list = '';
our $path = '';
our $sheets_requested = 0;
our $init_card1 = 0;
our $init_card2 = 0;
our $init_card3 = 0;
our $init_card4 = 0;
our $init_card5 = 0;
our $init_card6 = 0;
our $checked_1 = '';		# Layout selection radio button values.
our $checked_3A = '';
our $checked_3B = '';
our $checked_3C = '';
our $checked_6A = '';

# Added an = before each fieldname so grep can identify the beginning of/and the whole word.
our @char_fields = qw/=trans_member_id =layout =confirm_one =md5h/;
our @int_fields = qw/=trans_cbid_id =cbid_low =cbid_high =member_id =sheets/;

###### %the_order holds the values that have been submitted by the user. It has the form of 
###### 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'.
our %the_order = (	trans_member_id => {value=>'', class=>'n', req=>0,
						english=> 'Member ID'},
			trans_cbid_id	=> {value=>'', class=>'n', req=>0,
						english=> 'ClubBucks Card ID'},
			cbid_low	=> {value=>'', class=>'n', req=>1,
						english=> 'Lowest Card number on Sheet'},
			cbid_high	=> {value=>'', class=>'n', req=>0,
						english=> 'Lowest Card number on Last Sheet'},
			sheets		=> {value=>'', class=>'n', req=>0},
			md5h		=> {value=>'', class=>'n', req=>0},
			layout		=> {value=>'', class=>'n', req=>1,
						english=> 'Sheet Layout Selection'}
			);
			

###### LOCALS
my ($sth, $tmpl, $data, $counter) = ();

##########################################
###### MAIN program begins here.
##########################################

# Is the Member or Admin cookie available - logged in?
if ( Get_Cookie() )
{
	# Get any special path info that was passed.
	($path = $q->path_info()) =~ s/^\///;

	# Split out the special path info and the hash value.
#	($md5h, $param_list) =  split /\//, $path;
	($the_order{layout}{value}) =  split /\//, $path;

	###### if we don't get it from the path we'll try the param
	$the_order{layout}{value} ||= $q->param('layout');								

	# This is a single card transfer.
	if ( $the_order{layout}{value} && $the_order{layout}{value} eq 'layout_1' )
	{
		# Set the appropriate template if this is a single card transfer.		
		$page_template = 'one_card';
	}
	# This is a tri-fold brochure transfer.
	elsif ( $the_order{layout}{value} && $the_order{layout}{value} eq 'layout_3A' )
	{
		# Set the appropriate template if this is a single card transfer.		
		$page_template = 'tri_fold_a';
		$checked_3A = 'checked';
	}
	# This is a 6-card sheet of transfer.
	else
	{
		$page_template = 'many_cards';
		$checked_6A = 'checked';
	}						
	# If this is the first time through, just display the transfer form.
	unless ( $action )
	{
		Do_Page();
	}
	# If this is a transfer request.
	elsif ($action eq 'transfer')
	{
		Load_Params();	# Load the submitted order data into the hash.
		Check_Layout();	# Assign values according to the layout.

		# Unless we have a hash value passed, check the values.
		unless ( $the_order{md5h}{value} )
		{
			# This check is here for future functionality only - to provide a range.
			# As of 08/08/03, only the low value field appears on the form.
			# If not supplied an ending cbid, we default to one sheet.
			unless ( $the_order{cbid_high}{value} )
			{
				$the_order{cbid_high}{value} = $the_order{cbid_low}{value};
			}
			# Make sure all required fields are filled.
			Check_Required();

			# Check the validity of the field and the values provided by the user.
			Check_Validity();

			# We'll do these only if all previous checks are clean,
			#	since these checks require valid data. 
			unless ( $dover_msg )
			{
				# Check that the end - start represents a multiple of full sheets.
				Check_Full_Sheets();

				# Again, we'll do these only if all previous checks are clean,
				#	since these checks require calls to the database. 
				unless ( $dover_msg )
				{
					# Check that all the CBID's belong to the transferring member.
					Check_CBIDs();	

					# Check to see if the member ID supplied, or the CBID related
 					#  member ID, or the CBID is valid and in the downline.
					Check_Trans_Member_ID();

					# If this is a one card transfer...
					if ( $cards_per_sheet == 1 ) 
					{
						# Check that the member has not reached their limit of personal cards.
						Check_Maxcards();

						unless ( $dover_msg )
						{
							$confirm_page = 1;

							# Display the confirmation dialogue.
							$dover_msg .= $q->span({-class=>'b'},
								"Activating ClubBucks card #: $the_order{cbid_low}{value}<br>\n
								Associating the card with: $transfer_name<br>\n");

							# Create the hash of values that have already been checked.
#							$ME .= "/" . md5_hex("$SALT $param_list");
#							$q->param(-name=>'md5h' -value=> md5_hex($SALT . Param_list()) );
							$the_order{md5h}{value} = md5_hex($SALT . Param_list());

						#	Debug();

						} # end $dover_msg
					} # end cards_per_sheet == 1
				} # end $dover_msg
			} # end $dover_msg
		} # end $md5h
		# If the data was changed at the URL level.
		elsif ( $the_order{md5h}{value} ne md5_hex($SALT . Param_list()) )
		{
			Err("The data being submitted is corrupted.");

		#	Debug();
			goto 'END';
		}
		# Otherwise, this is a confirmation.						
		else
		{		
			# Pick up the value from the parameters since this is a confirmation.			
			$sheets_requested = $sheets_requested || $q->param('sheets');
		}

		# If we don't have a 'do over' message at this point, update the CBID's.
		unless ($dover_msg)
		{
			Initialize_Sheet();

			unless ( Update_Ids() )
			{
				$dover_msg .= $q->span({-class=>'r'},
					"The previous transfer did not process normally,
					 so it was aborted completely. 
					Please try again.<br>\n");
			}
			else
			{
				my $high_card = $the_order{cbid_low}{value} + $cards_per_sheet - 1;

				if ( $cards_per_sheet == 1 )
				{
					$dover_msg .= $q->span({-class=>'b'},
						"Card number $the_order{cbid_low}{value} is now activated and usable by ");
				}
				# Tri-fold brochure with low card = primary card.
				elsif ( $cards_per_sheet == 3 )
				{
					$dover_msg .= $q->span({-class=>'b'},
						"The tri-fold brochure with primary card number
						 $the_order{cbid_low}{value} was successfully transferred to ");
				}
				# 6-card sheet all referred to by the same member ID.
				elsif ( $cards_per_sheet == 6 )
				{
					$dover_msg .= $q->span({-class=>'b'},
						"Card numbers $the_order{cbid_low}{value} through
						 $high_card were successfully transferred to ");
				}

				# If a member ID was passed or found associated with the passed CBID.
				if ( $the_order{trans_member_id}{value} 
					|| ($transfer_id && $the_order{trans_cbid_id}{value}) )
				{
					my $tmp_id = $transfer_id || $the_order{trans_member_id}{value};
					$dover_msg .= $q->span({-class=>'b'},
							"Member # $tmp_id.<br>\n");
				}
				# If a CBID was passed.
				else
				{
					$dover_msg .= $q->span({-class=>'b'},
							"ClubBucks Card # $the_order{trans_cbid_id}{value}.<br>\n");
				}

				$the_order{cbid_high}{value} = $the_order{cbid_low}{value} = $the_order{md5h}{value} = '';
			}
		}

		# Re-draw the page with the any error messages.	
		Do_Page();
	}
	# If the script was called with invalid parameters...	
	else
	{
		Err("The action $action is not allowed.");
	}
}

END:			
$db->disconnect;
exit;

##########################################
###### Subroutines start here.
##########################################

###### Check to see that all the CBID's provided belong to the transferring member,
######		are of the right class, and the cbid_low value is the first card on a sheet.
sub Check_CBIDs
{

	my $how_many = 0;
	my ($sth, $qry, $rv) = ();
	my $first_card = '';
	my $expected = $the_order{cbid_high}{value}
				- $the_order{cbid_low}{value}
				+ $cards_per_sheet;

	# If this is a card transfer of a 6-card sheet (first card labeled, class 2 or 7).
	if ( $cards_per_sheet == 6 )
	{
		# Build the query to confirm the low CBID provided is the first on the sheet.
		$qry = "SELECT label
			FROM $TBL{master}
			WHERE id = ?::bigint
			AND 	(class = 7
				OR class = 2)";

		$sth = $db->prepare($qry);
		$rv = $sth->execute(	$the_order{cbid_low}{value});
		defined $rv or die $sth->errstr;

		# Get the label off of the first card?	
		$first_card = $sth->fetchrow_array || '';
	}
	elsif ( $cards_per_sheet == 3 )
	{
		# Build the query to confirm the low CBID provided is the first on the sheet.
		$qry = "SELECT count(*)
			FROM 	$TBL{master}
			WHERE 	referral_clubbucks_id = ?::bigint
			AND	((id = $the_order{cbid_low}{value} + 1)
				OR (id = $the_order{cbid_low}{value} + 2));";
		$sth = $db->prepare($qry);
		$rv = $sth->execute(	$the_order{cbid_low}{value});
		defined $rv or die $sth->errstr;

		# If there are two cards referred to by the provided CBID, it's the first card.?	
		if ( (my $tmp = $sth->fetchrow_array) == 2 )
		{
			$first_card = '1';
		}
	}

	# If the label identifies this as the first card on a sheet
	#	or we are transferring only one card.
	if ( $first_card eq '1' || $cards_per_sheet == 1 )
	{
		# Build the query to find how many CBID's in the range are owned by the VIP
		#	and are still available.
		$qry = "SELECT count(*)
			FROM $TBL{master}
			WHERE id BETWEEN $the_order{cbid_low}{value}::BIGINT
					 AND ($the_order{cbid_high}{value} + $cards_per_sheet - 1)::BIGINT\n";

		if ( $cards_per_sheet == 3 )
		{
			$qry .= " 	AND ((referral_id = $Login_ID
						AND referral_clubbucks_id IS NULL
						AND class = 2)
					OR (referral_clubbucks_id = $the_order{cbid_low}{value}
						AND class = 3));";
		}
		else
		{
			$qry .= " 	AND referral_id = $Login_ID
					AND (class = 7 OR class = 2)";
		}

		# If this is a one card transfer.
		if ( $cards_per_sheet == 1 )
		{
			# Check to make sure there are no class 3 cards referring back to this card.		
			$qry .= "\n AND 0 = (SELECT count(*)
						FROM clubbucks_master
						WHERE referral_clubbucks_id = $the_order{cbid_low}{value}::BIGINT
						AND class = 3)";
		}
		$sth = $db->prepare($qry);
		$rv = $sth->execute();
		defined $rv or die $sth->errstr;

		# How many cards?	
		$how_many = $sth->fetchrow_array;

		# If we don't find exactly how many CBID's that are in the range provided.	
		if ( $how_many != $expected )
		{
			if ($cards_per_sheet == 1)
			{
				$dover_msg .= $q->span({-class=>'r'}, "That card number cannot be transferred. Please verify the card number.<br>\n");
			}
			else
			{
				$dover_msg .= $q->span({-class=>'r'}, "A card that you are trying to transfer is either active, owned by another member,<br>or refers to or is referred to by another ClubBucks card.<br>");
			}
		}
	}
	else
	{
		$dover_msg .= $q->span({-class=>'r'},
			"The '$the_order{cbid_low}{english}' that you provided is not the lowest or is assigned according to our records.<br>");
	}
}

######	Check that the cbid_end - cbid_start represents a multiple of full sheets.
sub Check_Full_Sheets
{		
	# If the user provided the values necessary.
	if ($cards_per_sheet && $the_order{cbid_low}{value} && $the_order{cbid_high}{value})
	{
		# Calculate how many sheets were requested based upon the CBID's provided.
		$sheets_requested = ((($the_order{cbid_high}{value} - $the_order{cbid_low}{value})
				/ $cards_per_sheet) + 1);
	}
 
	# If the calculated sheets doesn't represent a multiple of full sheets. 
	unless ( int $sheets_requested == $sheets_requested )
	{
		$sheets_requested = sprintf("%10.2f", $sheets_requested);
		$dover_msg .= $q->span({-class=>'r'},
			"You've requested a transfer of a partial sheet of cards.<br>
			Please check and correct the 'Lowest Card number(s)' you entered.<br>\n");
		return 0; 
	}
	return 1;
}					

###### Check the layout submitted and assign values accordingly.
sub Check_Layout
{
	if ( $the_order{layout}{value} =~ /layout_1/ )
	{	
		$cards_per_sheet = 1;
		$checked_1 = 'checked';
	}
	elsif ( $the_order{layout}{value} =~ /layout_3/ )
	{	
		$cards_per_sheet = 3;
		if ( $the_order{layout}{value} eq 'layout_3A' )
		{
			$checked_3A = 'checked';
 		}
		elsif ( $the_order{layout}{value} eq 'layout_3B' )
		{
			$checked_3B = 'checked';
		}
		elsif ( $the_order{layout}{value} eq 'layout_3C' )
		{
			$checked_3C = 'checked';
		}
	}
	elsif ( $the_order{layout}{value} =~ /layout_6/ )
	{	
		$cards_per_sheet = 6;
		$checked_6A = 'checked';	# Only one 6-card layout exists for now.
	}
}

###### Check to see if the member has reached the limit for personal cards.
sub Check_Maxcards
{
	my ($sth, $qry, $rv) = ();
	my $temp_id = '';

	# If this is an alias, we need to get rid of the letters.
	( $temp_id = $the_order{trans_member_id}{value} ) =~ s/\D//g;
	$qry = "SELECT count(*)
		FROM clubbucks_master
		WHERE current_member_id = ?
		AND (class = 4 OR class = 5)";

	$sth = $db->prepare($qry);
 	$rv = $sth->execute($temp_id);

	defined $rv or die $sth->errstr;

	my $data = $sth->fetchrow_hashref;
	my $how_many = $data->{count};

	# If the member has reached their limit of existing personal cards.
	if ( $how_many >= MAXPERSONALCARDS )
	{
		$dover_msg .= $q->span({-class=>'r'},
				"No action was taken.<br />\n
				The member whose ID you entered has reached their limit of personal cards.<br>");
		return 0;
	}
	return 1;
}

######	Check to see if the Member ID to be transferred to is valid.
sub Check_Trans_Member_ID
{
	my ($sth, $qry, $rv, $data) = ();
	my $search_field = '';
	my $referral_id = ();
	my $firstname = '';
	my $lastname = '';

	# Unless we have been passed a member ID to transfer.										
	unless ( $transfer_id = $the_order{trans_member_id}{value} )
	{
		unless ( $cards_per_sheet == 3 )
		{
			# We will try to get the member ID and/or the referral_id from the CBID passed.
			$qry = "SELECT current_member_id,
					referral_id
				FROM 	$TBL{master}
				WHERE id = ?::bigint;";
			$sth = $db->prepare($qry);
			$sth->execute($the_order{trans_cbid_id}{value});
			($transfer_id, $referral_id) = $sth->fetchrow_array;
			$sth->finish();
		}
		# Tri-folds can't be transferred to another CBID.						
		else
		{
			$the_order{trans_member_id}{class} = 'r';
			$dover_msg .= $q->span({-class=>'r'}, "The ClubBucks Card ID field cannot be used with a Tri-fold brochure layout.<br />\n");
			return 0;
		}
	}
	# A member ID was passed in as the transferee or found connected to the CBID passed.
	if ( $transfer_id )
	{
		if ( $transfer_id =~ /\D/)
		{
			###### contains letters so it can't be an ID
			$transfer_id = uc $transfer_id;
			$search_field = 'alias = ';
		}
		else
		{
			$search_field = 'id = ';
		}
		$search_field .= "\'$transfer_id\'";

		# Is the member of the correct type to get this/these card(s)?	
		$qry = "SELECT id,
				firstname,
				lastname
			FROM 	$TBL{members}
			WHERE 	$search_field
			AND 	membertype IN ";
		$sth = $db->prepare( $qry . ($cards_per_sheet == 1 ? $one_card_member_levels : $member_levels) );

		$sth->execute();
		$data = $sth->fetchrow_hashref;

		unless ( $data->{id} )
		{
			 # The member ID is not of the correct type to distribute cards.
			$the_order{trans_member_id}{class} = 'r';
			$dover_msg .= $q->span({-class=>'r'}, "The '$the_order{trans_member_id}{english}' 	either does not exist or is not qualified to receive cards.<br>\n");
		}
		else
		{
			$transfer_id = $the_order{trans_member_id}{value} = $data->{id};	
			$transfer_name = $data->{firstname} . " " . $data->{lastname};

			# Is the member in the VIP's downline?	
			unless ( G_S::Check_in_downline(	$db,
								'members',
								$Login_ID,
								$transfer_id) )
			{
				 # The member ID is not in the VIP's downline.
				$the_order{trans_member_id}{class} = 'r';
				$dover_msg .= $q->span({-class=>'r'}, "The '$the_order{trans_member_id}{english}' is not in your downline, so they cannot receive these cards.<br>\n");
			}
		}
		$sth->finish();
	}
	# A CBID was passed in as the transferee.
	else
	{
		$referral_id ||= 0;		# Avoid initialization errors.

		# Is the referring member of the CBID in the VIP's downline?	
		unless ( G_S::Check_in_downline(	$db,
							'members',
							$Login_ID,
							$referral_id)
				|| $referral_id == $Login_ID )
		{
			 # The referral member ID on the card is not in the VIP's downline.
			$the_order{trans_member_id}{class} = 'r';
			$dover_msg .= $q->span({-class=>'r'},
						"The '$the_order{trans_cbid_id}{english}' you entered does not have a member ID associated with it.<br>
						The 'referring member' of the card is not in your downline, so the cardholder cannot receive these cards.<br>");
		}
	}	
}		
								
######	Check required fields and assign new html classes as necessary.
sub Check_Required
{
	foreach (keys %the_order)
	{
		if (($the_order{$_}{value} eq '') && ($the_order{$_}{req} == 1))
		{
		 	###### a required field is empty so we'll flag it.
			$the_order{$_}{class} = 'r';
			$dover_msg .= $q->span({-class=>'r'}, "Please fill in the required fields marked in <b>RED</b><br>\n");
		}
	}

	# Check to see if neither the member ID nor the CBID field were filled.
	if (($the_order{trans_member_id}{value} eq '') && ($the_order{trans_cbid_id}{value} eq ''))	
	{
		# a required field is empty so we'll flag it.
		$the_order{trans_member_id}{class} = 'r';
		$the_order{trans_cbid_id}{class} = 'r';
		$dover_msg .= $q->span({-class=>'r'}, "Please fill in the required fields marked in <b>RED</b><br>\n");
	}

	# Check to see if both the member ID and the CBID field were filled.
	if (($the_order{trans_member_id}{value} ne '') && ($the_order{trans_cbid_id}{value} ne ''))	
	{
		# Both options are filled, so we'll flag it.
		$the_order{trans_member_id}{class} = 'r';
		$the_order{trans_cbid_id}{class} = 'r';
		$dover_msg .= $q->span({-class=>'r'},
			"Both the '$the_order{trans_member_id}{english}' and the 
			'$the_order{trans_cbid_id}{english}' fields cannot be filled at the same time.<br>
			Please delete one of the field values and try again.<br>\n");
	}

	if ($dover_msg) { return 0; }
	else { return 1; }
}

##### Checks the validity of the Search Field and the Search Value before executing a query.
sub Check_Validity
{
	foreach my $hashkey (keys %the_order)
	{
		if (grep { /=$hashkey/ } @char_fields)		
		{
			# Do nothing because all keyboard input is acceptable.
		}
		elsif (grep { /=$hashkey/ } @int_fields)
		{
			if ($the_order{$hashkey}{value} =~ /\D/)
			{
		 		# Contains letters so not valid for the number fields.
				$dover_msg .= $q->span({-class=>'r'}, "Invalid value in the&nbsp;&nbsp;'$the_order{$hashkey}{english}'&nbsp;&nbsp;field - use only whole numbers.<br>");
			}
		}
		else 
		{
			$dover_msg .= $q->span({-class=>'r'}, "Undefined field $hashkey submitted.<br>\n");
		}
	}
}

###### Print our main search form with any pertinent messages.
sub Do_Page
{
	my $tmpl = G_S::Get_Object($db, $TMPL{$page_template}) || die "Couldn't open: $TMPL{$page_template}";
	$tmpl =~ s/%%action%%/$ME/g;
	$tmpl =~ s/%%message%%/$dover_msg/;
	$tmpl =~ s/%%sheets%%/$sheets_requested/;
	$tmpl =~ s/%%trans_member_id%%/$the_order{trans_member_id}{value}/;
	$tmpl =~ s/%%cbid_low%%/$the_order{cbid_low}{value}/;
	$tmpl =~ s/%%checked_3A%%/$checked_3A/;
	$tmpl =~ s/%%checked_6A%%/$checked_6A/;
	$tmpl =~ s/%%checked_1%%/$checked_1/;
	$tmpl =~ s/%%md5h%%/$the_order{md5h}{value}/g;

	# Comment out buttons dependent upon whether this is a confirmation or submission.
	if ( $confirm_page )
	{
		$tmpl =~ s/conditional-submit-open.+?conditional-submit-close//sg;
	}
	else
	{
		$tmpl =~ s/conditional-confirm-open.+?conditional-confirm-close//sg;
		$the_order{cbid_high}{value} = '';
	}
			
	foreach my $key (keys %the_order)
	{
		$tmpl =~ s/%%$key%%/$the_order{$key}{value}/g;
		$tmpl =~ s/%%class_$key%%/$the_order{$key}{class}/g;
	}

	print $q->header(-expires=>'now'), $tmpl ;
}

sub Debug
{
	print $q->header, $q->start_html;

	foreach (keys %the_order)
	{
		print "key: $_ = $the_order{$_}{value}<br>\n";
	}
	print Param_list();
	print "<br>\n";
	print md5_hex($SALT . Param_list());
	print $q->end_html;
}						

###### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>", $_[0], "</B><BR>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		$admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		$operator = $q->cookie('operator');
		my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';

		# Check for connection errors.
		unless ($db = ADMIN_DB::DB_Connect( 'cb_card_transfer.cgi', $operator, $pwd ))
		{
			return 0;
		}
		# Get the info passed in via parameters.
		unless ( ($Login_ID = $q->param('member_id')) && ($membertype = $q->param('membertype')) )
		{
			Err("Missing a required parameter.");
			return 0;
		}
	}
	# If this is a member.		 
	else
	{
		$operator = 'cgi';

		# Check for connection errors.
		unless ($db = DB_Connect::DB_Connect('cb_card_transfer.cgi'))
		{
			return 0;
		}
		# Get the Login ID and Member info hash from the cookie.
		( $Login_ID, $member_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$Login_ID || $Login_ID =~ /\D/)
		{
 			Err("You must <a href=\"$URLS{LOGIN}?destination=${\$q->self_url}\">login</a> to use this application.");
			return 0;
		}
	}
	return 1;
}


###### Set the first sheet's individual card ID's to the appropriate field in the database.
sub Initialize_Sheet
{
   	if ($cards_per_sheet == 1)
	{
		# Nothing to initialize since it is only one card.
	}
	# If these are trifold brochures.
   	elsif ($cards_per_sheet == 3)
	{			
		# Assign values to the cards on the first sheet depending upon the layout.
		if ( $the_order{layout}{value} eq 'layout_3A' )
		{
			$init_card1 = $the_order{cbid_low}{value};	
			$init_card2 = $the_order{cbid_low}{value} + 1;
			$init_card3 = $the_order{cbid_low}{value} + 2;
 		}
		elsif ( $the_order{layout}{value} eq 'layout_3B' )
		{
			$init_card1 = $the_order{cbid_low}{value} + 1;
			$init_card2 = $the_order{cbid_low}{value};
			$init_card3 = $the_order{cbid_low}{value} + 2;
		}
		elsif ( $the_order{layout}{value} eq 'layout_3C' )
		{
			$init_card1 = $the_order{cbid_low}{value} + 2;
			$init_card2 = $the_order{cbid_low}{value} + 1;
			$init_card3 = $the_order{cbid_low}{value};
		}
		else	# We've got a problem with the layout variable.
		{
				Err("I can't tell which layout you selected.");
		}
	}
   	elsif ($cards_per_sheet == 6)
	{			
		# Assign values to the cards on the first sheet depending upon the layout.
		if ( $the_order{layout}{value} eq 'layout_6A' )
		{
			$init_card1 = $the_order{cbid_low}{value};	
			$init_card2 = $the_order{cbid_low}{value} + 1;
			$init_card3 = $the_order{cbid_low}{value} + 2;
			$init_card4 = $the_order{cbid_low}{value} + 3;
			$init_card5 = $the_order{cbid_low}{value} + 4;
			$init_card6 = $the_order{cbid_low}{value} + 5;
		}
		else	# We've got a problem with the layout variable.
		{
				Err("I can't tell which layout you selected.");
		}
	}
	else	# We've got a problem with the $cards_per_sheet variable.
	{
		Err("We can't work with $cards_per_sheet cards per sheet.
			 Contact customer service for assistance.");
	}
}

###### Load the submitted data parameters into the program's hash variable.
sub Load_Params
{
	###### populating our the_order array hash values
	###### this loop depends on the %the_order hash keys being identical to the passed parameter names
	foreach (keys %the_order)
	{
		###### by doing the conditional assignment, I preserve any values I've already loaded
		$the_order{$_}{value} ||= $q->param($_) || '';

		###### filter out leading & trailing whitespace
		$the_order{$_}{value} =~ s/^\s*//;
		$the_order{$_}{value} =~ s/\s*$//;

		###### Remove DHS prefix from any CBID values if included.
		if ( $_ =~ /cbid/ )
		{
			$the_order{$_}{value} =~ s/(\D)//g;
		}

#		$param_list .= $the_order{$_}{value};
 	}
}

sub Param_list
{
	my $list = '';
	foreach(qw/ trans_member_id trans_cbid_id cbid_low cbid_high /)
	{
		$list .= $the_order{$_}{value};
	}

	return	$list;
}

###### Build and run the update query for CBID numbers in the database.
sub Update_Ids
{
	my $counter = 0;
	my $id_value = 0;

	my ($sth, $master_qry, $rv) = ();
	my ($card1, $card2, $card3, $card4, $card5, $card6) = ();
	my $memo = "member update  - IP: " . $q->remote_addr;;

	# Append each individual sheet's update command to the combined statement.
	for ($counter = 0;
		$counter < ($sheets_requested * $cards_per_sheet);
		$counter += $cards_per_sheet)
	{
		# Begin building out the update query.
		$master_qry .= "	UPDATE $TBL{master}
					SET 	stamp = NOW(),
						operator = CURRENT_USER,
						memo = '$memo',\n";
		# If a member ID was passed or found associated with the passed CBID.
		if ( $the_order{trans_member_id}{value} 
			|| ($transfer_id && $the_order{trans_cbid_id}{value}) )
		{
			if ($cards_per_sheet == 1)
			{
				# Set the single card to active class.
				$master_qry .= "	referral_clubbucks_id = NULL,
							current_member_id = ?,
							label = 'One Card Activation',
							class = 4\n";	
			}
			elsif ($cards_per_sheet == 3)
			{
				# Set the primary card's referral_id only.
				$master_qry .= " 	referral_id = ?\n";			
			}
			elsif ($cards_per_sheet == 6)
			{
				# Set the sheet cards to referral_id class.
				$master_qry .= " 	referral_clubbucks_id = NULL,
							referral_id = ?,
							class = 2\n";			
			}
			$id_value = $transfer_id || $the_order{trans_member_id}{value};				
		}
		# If a CBID was passed.
		elsif ( $the_order{trans_cbid_id}{value} )
		{
			$master_qry .= "	referral_clubbucks_id = ?,
						class = 3\n";
			$id_value = $the_order{trans_cbid_id}{value};
		}
		else	# We should have ruled this out long before, but just in case.
		{
			$dover_msg = "There was a problem identifying whether you submitted a Member ID
					or a ClubBucks Card ID.<br>
					No transfer was performed. Please try again.";
		}

		# If this is a one card transfer.
		if ($cards_per_sheet == 1)
		{
			# The first card is the only card submitted.
			$card1 = $the_order{cbid_low}{value};
			$master_qry .= " WHERE id = $card1 ::bigint;";
		}		
		elsif ($cards_per_sheet == 3)
		{
			# The first card is the primary card on the brochure.
			$card1 = $init_card1 + $counter;
			$master_qry .= " WHERE id = $card1 ::bigint;";
		}		
		# If these are 6-card sheets.
		elsif ($cards_per_sheet == 6)
		{		
			# The first card is the first card on the sheet.
			$card1 = $init_card1 + $counter;
			$card2 = $init_card2 + $counter;
			$card3 = $init_card3 + $counter;
			$card4 = $init_card4 + $counter;
			$card5 = $init_card5 + $counter;
			$card6 = $init_card6 + $counter;

			$master_qry .= "	WHERE id = $card1 ::bigint
						OR id = $card2 ::bigint
						OR id = $card3 ::bigint
						OR id = $card4 ::bigint
						OR id = $card5 ::bigint
						OR id = $card6 ::bigint;";
		}
		else	# We've got a problem with the $cards_per_sheet variable.
		{
			$dover_msg = "I can't work with $cards_per_sheet cards per sheet.<br />
					No transfer was performed. Please try again.";
		}
	}
	unless ( $dover_msg )
	{
		$sth = $db->prepare($master_qry);

	# Uncomment this to troubleshoot without running the query.
	#	Err("id_value = $id_value<br><br>$master_qry");	return 0;

		$rv = $sth->execute($id_value);
		defined $rv or return 0;
	}
	return 1;
}

###### 08/26/03 Modified the confirmation message to include the member and card numbers.
###### 09/03/03 Added the functionality that allows a single card to be activated/associated
######			with a member's ID or with one in their downline.
###### 09/04/03 added filtering for leading/trailing whitespace in the parameters
###### also reworked the layout parameter to work in the path so that it would carry through on a redirect to login
###### moved the md5 hash into the parameter list to keep the path clean
###### also added forced SQL casts to BIGINT for queries using the BIGINT columns so that the planner would use the indices
###### 09/15/03 Added 'ag' to membertypes that are allowed.
###### 09/16/03 Added removal of 'DHS' prefix from any cbid values passed in.
###### 09/22/03 Added handling for the tri-fold brochures.
###### 09/30/03 Moved tri-fold handling onto a separate template - like the one-card transfer.
###### 10/17/03 Added $one_card_member_levels to sub Check_Trans_Member_ID so that shoppers 
######			could receive a one card activation.
######		Also corrected Check_Maxcards to temporarily remove alias letters for the query.
# 11/02/09 revised the way the membertypes were evaluated in the assignee checking
# 11/23/09 added ma's back into the mix that allows them to be assigned a card
# 12/16/09 added ma's to the list of membertypes that can receive card assignments
###### 06/02/11 added AMPs to the list of membertypes who can receive transfers
# 02/17/14 changed amp to tp for recipients