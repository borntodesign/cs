#!/usr/bin/perl -w
###### FrontLine.cgi
###### 
###### 
###### 
###### Admin access is of the form:
######		http://www.clubshop.com/cgi/member_frontline.cgi?admin=1&id=<member id>
###### 
###### Created: 03/18/14	Bill MacArthur
###### based upon member_frontline.cgi by Karl Kohrt (most code moved to a class)
###### 
###### Last modified: 05/11/15	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use FrontLine;
binmode STDOUT, ":encoding(utf8)";

my %rpt_types = (
	'rspid' => {
		'authz' => ['m','ag','ma','v','tp'],	# regular (personal) frontline reports are available to these membertypes
		'label' => 'Frontline Reports'
	},
	'nspid' => {
		'authz' => ['v'],						# these are like taproot reports and Trial partner line reports... only available to Partners
		'label' => 'Network Frontline Reports'
	}
);

my $cgi = new CGI;
my ($tmp, $rpt_type) = split(/\//, $cgi->path_info);	# $tmp ends up null
$rpt_type ||= 'rspid';

# this call will initialize %FrontLine::params
my $F = FrontLine->new({'cgi'=>$cgi, 'report_type'=>$rpt_type});	# most error handling in this class will result in $F->Err() being called and then exit being called

# Get and test the cookie information for access.
# if there is a login error, it should have been detected and handled in the class
# Report the member's frontline if we have a good login/cookie.
# this call will initialize the DB handle on $FrontLine::db
unless ( $F->Get_Cookie() )
{
	Err('Undefined login error');
}

# OK, they are authenticated, now let's see if they are authorized to go any further based upon membertype
###### we'll let admin look at anyone's frontline
unless ( $F->param('admin') || ( grep $_ eq $F->member_info->{'membertype'}, @{ $rpt_types{$rpt_type}->{'authz'} } ))
{
		Err("$rpt_types{$rpt_type}->{'label'} are not accessible at your membership level: " . $F->member_info->{'membertype'});
}

$F->Report_Members($F->member_info->{'id'}) if Privileged();
exit;

################################
###### Subroutines start here.
################################

##### Prints an error message to the browser.
sub Err
{
	print
		$cgi->header('-expires'=>'now'),
		$cgi->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print "<br />$_[0]<br />";
	print $cgi->end_html();
	exit;
}

sub Privileged
{
	# we don't bother invoking this as the nspid reports are self limiting by virtue of selecting only parties whom the user has the privilege of seeing
	# in other words, they will always pull an empty report if they try on an ID that is not under them
	# also for nspid reports, we do not want to rewrite the member_info
	return 1 if $rpt_type eq 'nspid';

	###### if we don't have a refid param then we do nothing in here
	my $refid = $F->param('refid') || $F->member_info->{'id'};
	return 1 if (! $refid || $refid == $F->member_info->{'id'});

	if ($refid =~ /\D/ || length $refid > 8)
	{
		Err("Bad refid received");
	}

	###### now we need to verify that the logged in party is really the upline
	###### of the refid
#	if (! $gs->Check_in_downline($db, 'members', $member_id, $refid) )
	unless ($F->db->selectrow_array("SELECT network.access_ck1($refid, ?)", undef, $F->member_info->{'id'}))
	{
		Err("$refid was not found in your downline.");
	}

	###### change our 'login' info to reflect the refid party
	$F->GetMemberInfo($refid);
	return 1;
}