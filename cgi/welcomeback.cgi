#!/usr/bin/perl -w
###### welcomeback.cgi
###### an interface for getting prior partners back in the door
###### originally started by hacking up TrialPartner.cgi
###### created: 07/22/14	Bill MacArthur
###### last modified: 

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use lib ('/home/httpd/cgi-lib');
use XML::local_utils;
use G_S qw( $LOGIN_URL );
use DB_Connect;
use Mail::Sendmail;
#use Data::Dumper;	# only for debugging

my $emailContact = 'acf1@dhs-club.com';
my $DEBUG = 0;	# set this to -0- to turn off debug statements on STDERR
#my $noTrackPartner = 0;	# set this to -0- to create a tracking activity for visiting this report by a Partner who is still in the line
my %TMPL = (
	'language_pack' => 11087
);

my ($db, $lb, @errors, @alerts, $id, $data, %Cache, $reqStatus);
my $loginType = '';

my $gs = G_S->new({'db'=>\$db});
my $q = new CGI;
my $TT = Template->new({'ABSOLUTE' => 1});

my ($loginid, $member) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

exit unless $db = DB_Connect('welcomeback');
$db->{'RaiseError'} = 1;

my $ckID = $q->cookie('id') || '';
$ckID = '' if $ckID =~ m/\D/;	# this should be numeric only

my $paramID = $q->param('id') || '';
$paramID = $gs->Alias2ID($db, 'members', $paramID) if $paramID && $paramID =~ m/\D/;

if ($paramID)
{
	$loginType = 'param';
	$id = $paramID;
	HandleNA() unless isPriorPartner($paramID);	# we do not return from this
}
elsif ($loginid && $loginid !~ m/\D/)	# they are fully logged in
{
	debug("loginid= $loginid");
	$loginType = 'full';

	if ($member->{'membertype'} eq 'v')	# they are a Partner so render as complete
	{
		Do_Page();
	}
	else
	{
		HandleNA() unless isPriorPartner($loginid);	# we do not return from this
		$data->{'ca_balance'} = GetCA_Balance();
		$id = $loginid;
	}
#	debug("inside full login... id= $id");
}
elsif ($ckID)	# we are not fully logged in but we do have a simple login
{
	$loginType = 'lite';
	$id = $ckID;
	HandleNA() unless isPriorPartner($ckID);	# we do not return from this	
}

InitMember();
if ($q->param('reactivate'))
{
	RequestReactivation();
}
else
{
	if (my $r = CheckReactivationRequest())	# we should be returning true here
	{
		LoadLB();
		push @alerts, ($reqStatus->{'funded'} ? $lb->{'reqispending'} : $lb->{'reqispending_funding'});
	}
}
Do_Page($TMPL{'main'});


###### beginning of sub-routines

sub CheckReactivationRequest
{
	my $flg = shift;
	return unless InitMember();
	return 1 if $reqStatus->{'id'} && ! $flg;
	
	$reqStatus = $db->selectrow_hashref("SELECT wbr.id FROM welcomeback_requests wbr WHERE wbr.id= $member->{'id'}");
	return 0 unless $reqStatus->{'id'};

	$reqStatus->{'funded'} = 1 if GetCA_Balance() >= (UpgradePrice()->{'usd'} || 0);
	return 1;
}

sub debug
{
	return unless $DEBUG;
	warn $_[0];
}

sub Do_Page
{
	print $q->header('-charset'=>'utf8');
	
	LoadLB() unless $lb;
	
	$TT->process(
		'/home/httpd/cgi-templates/TT/welcomeback.tt', {
			'errors' => \@errors,
			'alerts' => \@alerts,
			'script_name' => $q->script_name,
			'member' => $member,
			'lb' => $lb,
			'data' => $data,
			'LOGIN_URL' => $LOGIN_URL,
			'loginType' => $loginType,
			'reqStatus' => $reqStatus,
			'upg_fee'=>UpgradePriceFormatted()
		}) || print $TT->error;
	Exit();
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetCA_Balance
{
	return $db->selectrow_array("SELECT account_balance FROM soa_account_balance_payable WHERE id= ?", undef, $id) || 0
}

sub HandleNA	# NA not applicable
{
	InitMember();
	LoadLB();
	
	# if we are not a candidate for this offer, but we are already a partner, we will not show the errors
	push @errors, $lb->{'err_notpriorp'}, $lb->{'err_visitappx'} unless ($member->{'membertype'} || '') eq 'v';
	Do_Page();
}

sub InitMember
{
	return unless $id;
	
	# we need to at least have the country and membertype for our $member
	return 1 if $member->{'country'} && $member->{'membertype'};
	return $member = $db->selectrow_hashref("SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.country, m.membertype, m.emailaddress FROM members m WHERE m.id= $id");
}

sub isPriorPartner
{
	my $id = shift || return;
	return $db->selectrow_array('SELECT TRUE FROM priorvs WHERE id=?', undef, $id);
}

sub LoadLB
{
	return if $lb;	# no use rerunning this again
	
	my $tmp = ();
	$tmp = $gs->GetObject($TMPL{'language_pack'}) || die "Failed to load language pack template: $TMPL{'language_pack'}";

	$TT->process(\$tmp, {
		'script_name' => $q->script_name,
		'LOGIN_URL' => $LOGIN_URL
		}, \$lb);
	my $lu = new XML::local_utils;
	$lb = $lu->flatXHTMLtoHashref($lb);
}

#sub RecordActivity
#{
#	# actually, if we are fully logged in, we should have a simple cookie ID as well
#	return unless $loginType >= 1 && $ckID eq $id;
#	return if $member->{'membertype'} eq 'v' && $noTrackPartner;
#	
#	$db->do('INSERT INTO activity_tracking (id, "class") VALUES (?, 51)', undef, $id);
#}

sub RequestReactivation
{
	LoadLB();
	
	if ($loginType ne 'full')
	{
		push @errors, $lb->{'full_login_required'};
		return;
	}
	
	# make sure a request is not already in place
	if (my $r = CheckReactivationRequest())
	{
		push @errors, ($reqStatus->{'funding'} ? $lb->{'reqispending'} : $lb->{'reqispending_funding'});
		return;
	}
	
	$db->do('INSERT INTO welcomeback_requests (id) VALUES (?)', undef, $id);
	Mail::Sendmail::sendmail(
		'From' => $member->{'emailaddress'},
		'To' => $emailContact,
		'Subject' => 'Please reactivate my GPS subscription!',
		'Text' => "Name: $member->{'firstname'} $member->{'lastname'}\nID Number: $member->{'alias'}\nEmail: $member->{'emailaddress'}"
	);
	
	if (my $r = CheckReactivationRequest())	# we should be returning true here
	{
		push @alerts, ($reqStatus->{'funded'} ? $lb->{'reqispending'} : $lb->{'reqispending_funding'});
		return;
	}
}

sub UpgradePrice
{
	return {} if $member->{'membertype'} eq 'v' || ! $member->{'country'};
	
	$Cache{'UpgradePrice'} ||= $db->selectrow_hashref("
		SELECT currency_code, amount, usd
		FROM subscription_pricing_by_country
		WHERE subscription_type = 41
		AND country_code= ?", undef, $member->{'country'});
	
	return $Cache{'UpgradePrice'};
}

sub UpgradePriceFormatted
{
	my $p = UpgradePrice();
	return 'Unable to calculate' unless $p->{'usd'};
	return "\$$p->{'usd'} USD" if $p->{'currency_code'} eq 'USD';
	return "$p->{'amount'} $p->{'currency_code'} / $p->{'usd'} USD";
}

__END__

