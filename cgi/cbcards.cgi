#!/usr/bin/perl -w
###### cbcards.cgi
###### 04/17/2003	Karl Kohrt
######
###### This script will take a list or range of values, provided via the user interface
###### or by the cbneworder.cgi script still to be developed. Using that list or range, it 
###### will insert 10 values into the ClubBucks "soft-card" html template.
######
###### Last modified: 08/01/03 Karl Kohrt

use strict;

use lib ('/home/httpd/cgi-lib');

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S;
use DB_Connect;

###### CONSTANTS
use constant MAXCARDS => 10;		# Maxinum number of cards per printed page.
our %TMPL = (	Cards	=> 10144,
		Salt	=> 96);

# The format of a Quick Card request. The second field ('id') is arbitrary since we won't use it.
our @quick_card_format = (4,'id',0,0,0,0,0,0,0,0);

###### GLOBALS
our $q = new CGI;
our $ME = $q->url();
our @cbids = ();
our ($db, $path, $md5h, $values) = ();

##########################################
###### MAIN program begins here.
##########################################

unless ($db = &DB_Connect::DB_Connect('cbcards.cgi')){exit}
$db->{RaiseError} = 1;
my $salt = &G_S::Get_Object($db, $TMPL{'Salt'}) || die "Failed to load template: $TMPL{'NoneAvail'}";

###### we need to get rid of the leading forward slash.
($path = $q->path_info()) =~ s/^\///;

###### If there are no parameters passed, just print the testing page.
unless ( $path )
{
	&Err("You can't call this script without proper parameters. -ref:cbcards.cgi-");
	exit;
}

###### Preview the cards using the values submitted.				
else
{
	# Split out the hash value and the CBID number list.	
	($md5h, $values) =  split /\//, $path;

	# We either have a good hash value passed or we are testing.
	if (($md5h eq md5_hex("$salt $values")) || ($md5h eq 'test'))
	{
		# If we have a range(i.e., 1-10), parse out the list of numbers.		
		if ($values =~ m/\-/){ Parse_Values();
		}
		# If we have a csv list, split them up.		
		else{ @cbids = split /\,/, $values, 10;
		}		
		Preview();
	}
	else
	{
		# If "test" or a properly formatted hash is not present, then error out.
		&Err("Corrupted data, try again.  -ref:cbcards.cgi-")
	}			
}

exit;

###### Subroutines start here.

###### For test purposes only - Display the page where we can define the value list or range to be printed.
sub Define_Values{
	
	print $q->header(), $q->start_html(-bgcolor=>'#ffffff');
	print "Enter /test/ followed by up to 10 comma separated values<BR>"; 
	print "after the .cgi in the Address/Location bar above,<BR>";
	print "then press Enter on your keyboard.<BR>";
	print "<BR>Example: ";
	print $q->a({-href=>'./test/1,2,3,4,5,6,7,8,9,10'}, "$ME/test/1,2,3,4,5,6,7,8,9,10");
	print $q->end_html();
}

sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>", $_[0], "</B><BR>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### If given a value range, we will parse the list of numbers.
sub Parse_Values{
	my ($start, $end) = ();		
	my $counter = 0;

	($start, $end) = split /\-/, $values;	
	my $current = $start;

	while (($current <= $end) && ($counter <= MAXCARDS))
	{
		push @cbids, $current;
		++$current;
		++$counter;
	}
}

###### Display the print preview of the cards to be printed.
sub Preview{
	my $CBID = ();
	my $counter = 0;
	my $quick_card = 1;	# Flag identifying a quick card submission.

	my $tmpl = &G_S::Get_Object($db, $TMPL{'Cards'}) || die "Failed to load template: $TMPL{'Cards'}";

	# Test for a single, Quick card format.
	for ($counter = 0; $counter < 10; $counter++)
	{
		# Look at all but the second field, since it is a variable id number.
		unless ($counter == 1)
		{
			# Check to see if the format matches a quick card.
			if ( $cbids[$counter] != $quick_card_format[$counter] )
			{
				$quick_card = 0;	# This is not a quick card submission.
			}
		}
	}		

	foreach $CBID (@cbids) 
	{
		###### buffer the ID number out if we receive less than 10
		my $len = length($CBID);
		if ($len < 10){ $CBID = sprintf "%010d", $CBID }

		$tmpl =~ s/%%CBID%%/$CBID/;
	}
	# If it is a quick card, blank out extra links and image holders
	if ($quick_card)
	{
		$tmpl =~ s/conditional-10card-open.+?conditional-10card-close//sg;
	}
																	
	print $q->header(), $tmpl;
}


###### Modification comments go here.
###### 04/23/03 Added the salt value and removed the call to Define_Values() when no path was provided.
###### 08/01/03 Modified so that one card conditionally removes links and extra image holders
######			from the html template.
