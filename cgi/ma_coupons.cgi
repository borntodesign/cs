#!/usr/bin/perl -w
###### ma_coupons.cgi
######
###### A script for displaying the current coupons of Merchant Affiliates.
###### Valid parameters include any of the following as a single parameter:
######
######		?location_id=<location id>
###### 	?master_id=<master id>
###### 	?coupon_id=<coupon id>
######
###### Created:	07/24/03	Karl Kohrt
######
###### last modified: 12/08/03	Karl Kohrt

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use DB_Connect;

$| = 1;   # do not buffer error messages

###### GLOBALS
our $db = ();
our $q = new CGI;
our $sth = '';
our $id = '';
our $coupons = '';
our $business_name = '';
our $message = '';

our %TMPL = (	'Coupon'	=> 10205,
		'Report'	=> 10204);

our %TBL = (	'coupons'	=> 'merchant_affiliate_coupons',
		'locations'	=> 'merchant_affiliate_locations',
		'master'	=> 'merchant_affiliates_master');

unless ($db = &DB_Connect::DB_Connect('ma_coupons.cgi')){exit}
$db->{RaiseError} = 1;

if ( Query_Coupons() )
{
	if ( Build_Coupons() )
	{
		Do_Report();
	}
	else
	{
		Err("No coupon exists for this merchant location.");
	}						
}

$db->disconnect;
exit;


######################################
###### Begin subroutines here.
######################################

###### Build a list of all coupons for this merchant or location.
sub Build_Coupons
{
	my $new_coupon = '';
	my $num_coupons = 0;
	my $tmp = '';	
	my $coupon_template = G_S::Get_Object($db, $TMPL{'Coupon'}) || die "Failed to load template: $TMPL{'Coupon'}";

	while ($tmp = $sth->fetchrow_hashref)
	{
		++$num_coupons;
		$new_coupon = $coupon_template;
		
		# Blank out any non-existent fields to avoid log errors.
		$tmp->{address1} = $tmp->{address1} || '';
		$tmp->{city} = $tmp->{city} || '';
		$tmp->{state} = $tmp->{state} || '';
		$tmp->{phone} = $tmp->{phone} || '';
		$tmp->{value} = $tmp->{value} || '';
		$tmp->{value} =~ s/[\n]/<br>/g;

		# Now substitue the values into the html template.
		$new_coupon =~ s/%%location_name%%/$tmp->{location_name}/;
		$new_coupon =~ s/%%address1%%/$tmp->{address1}/;
		$new_coupon =~ s/%%city%%/$tmp->{city}/;
		$new_coupon =~ s/%%state%%/$tmp->{state}/;
		$new_coupon =~ s/%%phone%%/$tmp->{phone}/;
		$new_coupon =~ s/%%value%%/$tmp->{value}/;
		$new_coupon =~ s/%%expiration%%/$tmp->{expiration}/;
		$coupons .= $new_coupon;
		# The master business name for the report header.
		$business_name = $tmp->{business_name};

		# If this was a coupon ID and master login, we only display the first instance of the coupon.
		#  (For example, a Master login will change all location coupons, but only show one.)
		if ( $q->param('coupon_id') && $tmp->{ma_id} )
		{
			$message = "<br>NOTE: This coupon will appear for all locations
						associated with Master ID # $tmp->{ma_id}.<br>";	
			last;
		}
	}
	$sth->finish;
	return $num_coupons;		
}

###### Displays the basic page(header & footer) to the browser with the $coupons embedded in it.
sub Do_Report
{
	my $tmpl = G_S::Get_Object($db, $TMPL{'Report'}) || die "Failed to load template: $TMPL{'Report'}";
	
	$tmpl =~ s/%%business_name%%/$business_name/;
	$tmpl =~ s/%%coupons%%/$coupons/;
	$tmpl =~ s/%%message%%/$message/;
	print $q->header(), $tmpl;
}

###### prints an error to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<B>$_[0]</B>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}
###### Build and run the query for the existing coupons.
sub Query_Coupons
{
	my $rv = '';
	my @id_list = ();

	# Select the appropriate fields.
	my $qry = "	SELECT	c.value,
				c.expiration,
				c.ma_id,
				l.location_name,
				l.address1,
				l.city,
				l.state,
				l.phone,
				m.business_name
			FROM 	$TBL{coupons} c,
				$TBL{locations} l,
				$TBL{master} m
			WHERE ";

	# Were we passed a master ID...
	if ( $id = $q->param('master_id') )
	{
		$qry .= "(	( c.mal_id = l.id
					AND l.merchant_id = m.id
					AND l.merchant_id = ? )
				OR
				( c.ma_id = l.merchant_id
					AND l.merchant_id = m.id
					AND l.merchant_id = ? ))
				AND c.expiration >= NOW()::DATE";
		@id_list = ($id, $id);	
	}
	# ...or a location ID
	elsif ( $id = $q->param('location_id') )
	{
		$qry .= "(	( c.mal_id = l.id
					AND l.merchant_id = m.id
					AND l.id = ? )
				OR
				( c.ma_id = l.merchant_id
					AND l.merchant_id = m.id
					AND l.id = ? ))
				AND c.expiration >= NOW()::DATE";
		@id_list = ($id, $id);	
	}
	elsif ( $id = $q->param('coupon_id') )
	{
		$qry .= "	l.merchant_id = m.id AND
				( c.mal_id = l.id
					OR
				c.ma_id = l.merchant_id )	
				AND c.id = ?";
		@id_list = ($id);	
	}
	else
	{
		Err("This script requires a properly formatted master, location, or coupon parmeter to function properly.");
		return 0;
	}			

	# Prepare and execute the coupons query.
	$sth = $db->prepare($qry);
	$sth->execute(@id_list);

	return 1;
}


###### 07-31-03 Modified the query - it wasn't showing the location coupons with a master id
###### 	nor the master coupons with the location id. Also added the query by coupon id 
######		functionality for displaying a specific coupon after editing.
###### 12/08/03 Modified the error message for a non-existent email request.





