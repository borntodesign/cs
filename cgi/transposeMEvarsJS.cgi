#!/usr/bin/perl -w
# transposeMEvarsJS.cgi
# provide a bit of javascript for the currency transposing framework
# this will create the vars needed... see the end for the format
# last modified: 11/25/14	Bill MacArthur

=head3 transposeMEvarsJS.cgi

	The logic behind this little script is to determine the language preference of the user,
	The country of the user and the currency of that country.
	We will start by looking at a full login cookie if available.
	If that is not present, then a simple ID cookie.
	If not that then we will do an IP address lookup to see what country.
	
=cut

use strict;
use lib '/home/httpd/cgi-lib';
use CGI;
use DB_Connect;
use G_S;

my $cgi = new CGI;
my $gs = new G_S;
my $db = DB_Connect::DB_Connect('generic') || exit;

my ($country, $currcode, $lang_pref) = ();
my $ipaddr = $cgi->remote_addr;
my ($id, $meminfo) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic'));

# we don't expect to use this, but you never know what might happen
# ... that did not take long... we need to coerce the values in cases where other parties are looking at something that needs to remain "static" in it's representation
$id = $cgi->param('id') || '';
$id = '' if $id =~ /\D/;

###### a valid id will not have anything besides digits, so let's try the ID cookie
# the reason for not just doing this as an alternative in the initial assignment is that the parameter could be goobered due to programming bugs
# and we want to afford the user as much possibility to match as we can
$id ||= $cgi->cookie('id');

# this should be a simple number
$id = '' if $id && $id =~ /\D/;

# if we have a cookie, then lets lookup our country
($country) =  $db->selectrow_array('SELECT country FROM members WHERE id=?', undef, $id) if $id;

if ((! $country) && $ipaddr)
{
	($country) = $db->selectrow_array('SELECT countryshort FROM ip_latlong_lookup WHERE ?::inet BETWEEN inet_from AND inet_to', undef, $ipaddr);
}

if ($country)
{
	($currcode) = $db->selectrow_array('SELECT currency_code FROM currency_by_country WHERE country_code=?', undef, $country);
}

# set defaults if we are still empty
$country ||= 'US';
$currcode ||= 'USD';
$lang_pref = $gs->Get_LangPref() || 'en';

print $cgi->header('-charset'=>'utf-8', '-type'=>'text/javascript'),
	qq|
		var my_language_code = '$lang_pref';
		var my_currency_code = '$currcode';
		var my_country_code = '$country';|;
	

$db->disconnect;
exit;

__END__

<script type="text/javascript">
	var my_language_code = 'ie';
	var my_currency_code = 'VND';
	var my_country_code = 'FR';
</script>


Change Log:

04/10/12	Moved the id parameter evaluation ahead of everything so that this thing can be coerced based upon that value
11/13/14	Made the initialization of $id be empty by default to avoid unit'd var errors later on
11/25/14	Well that was ineffective at that point as it just assumed another undef value from cookie()... so fixed that