#!/usr/bin/perl -w
###### mopi.cgi
###### Method Of Payment Interface (for member or Admin use)
######
###### last modified 06/08/15 Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
require Crypt::CBC;
use Digest::MD5 qw( md5_hex );
use Business::CreditCard;
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;
use ccauth;
use ccauth_parse;
use ADMIN_DB;
require cs_admin;
binmode STDOUT, ":encoding(utf8)";

###### Globals
my $PAYPAL_YES = 0;

my $q = new CGI;
$q->charset('utf-8');
my $URL = $q->url();
my $cipher = new Crypt::CBC( 'd*f8k@-0' );
my ($id, $db, $data, %param, $creds) = ();
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $cs = cs_admin->new({'cgi'=>$q});

my $adminFLAG = $q->param('admin');
my $action = $q->param('action') || '';
my %FIELDS = ('cc_exp_mo' => 'Month of Credit Card expiration',
		'cc_exp_yr' => 'Year of Credit Card expiration',
		'cc_number' => 'Credit Card number',
		'acct_holder' => 'Account Holder name',
		'address' => 'Account Holder Address',
		'city' => 'Account Holder City',
		'state' => 'Account Holder State',
		'postal_code' => 'Account Holder Postal Code',
		'country' => 'Account Holder Country',
		'bank_name' => 'eCheck Bank Name',
		'account_number' => 'eCheck Account Number',
		'routing_number' => 'eCheck Routing Number',
		'payment_method' => 'Method of Payment',
		'card_code' => 'CVV2');

my $InputPageTmpl = 10062;
my $archive_tmpl = 10570;
my $arclist_tmpl = 10569;
my $mem_info = ();

###### these are my 'templates for the confirmation page when they are done
my %FINISH_PAGE = (	'updated' => 10063,
			'cancelled' => 10064);


###### we don't want any complaints about this page being insecure
###### so in case someone links it without defining https, we'll redirect
unless ($q->https)
{
	my $url = $q->self_url();
 	$url =~ s/^http/https/;
 	print $q->redirect($url);
 	exit;
}
 
unless ($adminFLAG)
{
	($id, $mem_info) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	unless ( $id && $id !~ /\D/ )
	{
###### by appending the path to this script to the end of the login script, the login script will redirect
###### back here when it is done
		my $msg = "You must be logged in to use this function.<br><a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">Click here</a> to login.";
		Err($msg);
	}
	$gs->Prepare_UTF8($mem_info);
}
else
{
	$id = $q->param('id');
	unless ($id)
	{
		Err($q->h3('No member ID passed'));
	}
}
###### OK, we should now have the ID we are reporting on

###### we could have tested for the admin flag, but just in case we have someone trying to access things
###### without going through the admin interface, we may be able to stop 'em here
if ($adminFLAG || $q->cookie('cs_admin'))
{
	$creds = $cs->authenticate($q->cookie('cs_admin'));
	exit unless $db = ADMIN_DB::DB_Connect( 'generic', $creds->{'username'}, $creds->{'password'} );

	###### we are not giving everyone in the organization access to this function
	###### only those in the accounting group
	if (ADMIN_DB::CheckGroup($db, $creds->{'username'}, 'accounting') != 1)
	{
		Err($q->h3('You must be a member of the accounting group to use this form'));
	}

###### Convert alias to a numeric ID
	if ( $id =~ /\D/ )
	{ 
		unless ( $id = $gs->Alias2ID($db, "members", $id) )
		{
			Err('<b>No matching record.</b>');
		}
	}

	$mem_info = $db->selectrow_hashref('SELECT * FROM members WHERE id=?',undef, $id);
}
else
{
###### if we don't have admin cookies, then we'll assume we're a member using the app
###### they will be logged in as user 'cgi' then
	exit unless $db = DB_Connect('mopi.cgi');
}

$db->{'RaiseError'} = 1;

###### OK, now we have a database connection and an 'ID' to report on

###### if there is no action, then we are just displaying info. If the action is "arc" then display
###### archive info.
unless ($action && $action !~ /^arc$/)
{
	my $qry = '';
	unless ($action && $adminFLAG)
	{
		$qry = 'SELECT mop.*, m.firstname, m.lastname FROM members m LEFT JOIN mop ON mop.id=m.id WHERE ';
	}
	else
	{
		$qry = 'SELECT mop_arc.*, m.firstname, m.lastname FROM members m LEFT JOIN  mop_arc ON mop_arc.id=m.id WHERE ';
	}

###### in most cases we'll have a numeric ID, but there could be times when an admin passes an alias
	$qry .= ($id =~ /^\d/) ? ( "m.id = '$id'") : ("m.alias = '$id'");
	
	if (my $stamp = $q->param('stamp') )
	{
		if ( ($action eq 'arc') && $adminFLAG )
		{
			$qry .= ' AND mop_arc.stamp=' . $db->quote($stamp) . ';';
		}	
	}

#print $q->header,$q->start_html, $qry; exit;

	my $sth = $db->prepare($qry);
	$sth->execute;
	$data = $sth->fetchrow_hashref;
	$sth->finish;
	unless ($data)
	{
		Err('<b>No matching record.</b>');
	}

###### since we are doing a LEFT JOIN the mop could be empty
###### in that case we cannot update anything
	unless ($data->{'id'} gt '')
	{
		Err('<b>There is no payment method record to match this member.</b>');
	}

###### since some of the returned values could be NULL, we'll assign empties to them to quiet warnings
###### about uninitialised values
	foreach (keys %$data)
	{
		$data->{$_} = '' unless $data->{$_};
	}

	InputPage();
}
elsif ($action eq 'cancel')
{
	Finish_Page('cancelled');
}
elsif ($action eq 'update')
{
###### first we'll validate the id that is passed
	Load_Params();
	my $yesno = Validate_Params();
	unless ( $yesno eq '1' )
	{
		Err($yesno);
	}
	
	unless ( Update() == 1)
	{
		Err("There has been an error updating your records.<br />" . $db->err . '<br />' . $db->errstr);
	}
	else { Finish_Page('updated') }
}
elsif ($action eq 'arc_list')
{
	ArcList();		
}
else
{
	Err('Undefined action specified!');
}

$db->disconnect;
exit;

########

sub Admin_Block
{
	## Omit this info in the archive record
	my $live_record = '';
	my $arc_list_lnk = '';
	unless ( $action )
	{
		$live_record = '<tr bgcolor="#cccccc"><td colspan="5" align="center" style="color: #0000cc; font-size: 9pt">
These three fields and values are available only in Admin mode.
</td></tr>';
		$arc_list_lnk = '<a href="https://www.clubshop.com/cgi/mopi.cgi?id=' . $id . ';admin=1;action=arc_list">Archive Records</a>';
	}
	else
	{
		$arc_list_lnk = '&nbsp;';
	}

	return <<BLK;
<tr bgcolor="#cccccc"><td colspan="3" class="frm">
mop Notes:<br />
<textarea cols="40" rows="3" name="notes">$data->{'notes'}</textarea>
</td>
<td colspan="2">
	$arc_list_lnk
	<p><a href="/cgi/admin/subscriptions.cgi?id=$id">Subscriptions</a></p>
	<br />
Last modified:<br />
<input type="text" name="last modified" value="$data->{'stamp'}" size="20" readonly="readonly" />
</td>
</tr>
$live_record
BLK
}

sub ArcList
{
	my $tblDATA = '';
	my $messages = '<br />';
	
	$id =~ s/^\D*(\d+)$/$1/;
	my $qry = "
		SELECT id, acct_holder, cc_number, cc_exp, payment_method, stamp, notes
		FROM    mop_arc
		WHERE	id= ?
		ORDER BY stamp DESC";

	my $sth = $db->prepare($qry);
	$sth->execute($id);
	$data = $sth->fetchrow_hashref;

	unless ($data)
	{
		$sth->finish;
#                return 0;
		$tblDATA = "<tr><td colspan=\"9\">No archive records available</td></tr>";
	}
	else
	{
        my $bgcolor = '#ffffee';
        
        my $arc_link = '';
        my $id_link = '';

        do {
        ###### lets fill in the blank table cells
			foreach (keys %{$data})
			{
                        #next if $_ eq 'oid';
                        $data->{$_} = '&nbsp;' unless (defined $data->{$_} && $data->{$_} =~ '\w');
			}

                ###### lets truncate the memo if it's too long
                ###### they can bring up the whole record if necessary anyway
                if (length $data->{'notes'} > 100)
                {
                        $data->{'notes'} = substr($data->{'notes'}, 0, 100);

                        ###### now we'll let 'em know it's been cut off
                        $data->{'notes'} .= '<br /><i>MEMO TRUNCATED</i>';
                }

                ###### since newlines in the data will not render properly in the html table
                ###### we'll change them to html linebreaks
                $data->{'notes'} =~ s/\n/<br>/g;

                # When no oid is present, don't provide a link.
#                $arc_link = (! $data->{oid}) ?
#                                       $q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, 'N/A') :
			$arc_link = $q->td({'-valign'=>'top'}, $q->a({'-href'=>$q->url()."?action=arc;admin=1&stamp=$data->{'stamp'};id=$id", '-target'=>'_blank'}, '*') );

			$data->{'stamp'} =~ s/^(.*):.*$/$1/;
			$tblDATA .= $q->Tr({'-bgcolor'=>$bgcolor},
				$arc_link .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'id'}) .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'acct_holder'}) .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'cc_number'}) .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'cc_exp'}) .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'payment_method'}) .
#				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{paid_thru}) .
				$q->td({'-valign'=>'top', '-class'=>'data', '-nowrap'=>''}, $data->{'stamp'}) .
				$q->td({'-valign'=>'top', '-class'=>'data'}, $data->{'notes'})
            );
            
            $bgcolor = ($bgcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
        } while $data = $sth->fetchrow_hashref;
        $sth->finish;
	}

	my $tmpl = $gs->GetObject($arclist_tmpl) || die "Failed to load template: $archive_tmpl";

	$tmpl =~ s/%%data%%/$tblDATA/;
	$tmpl =~ s/%%messages%%/$messages/;
	$tmpl =~ s/%%id%%/$id/g;

	print $q->header, $tmpl;
	return 1;
}


sub cboExpMonth
{
###### build a select dropdown box for the CC expiration month
###### we want the current month to be selected if there is one
	my $month = ($data->{'cc_exp'}) ? ( substr($data->{'cc_exp'}, 0, 2) ) : ('');
	$q->popup_menu(
		'-name'=> 'cc_exp_mo',
		'-values'=>['', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
		'-default'=>$month);
}

sub cboExpYear
{
###### build a select dropdown box for the CC expiration year
###### we want the current year to be selected if there is one
	my $year = ($data->{'cc_exp'}) ? ( substr($data->{'cc_exp'}, 2, 2) ) : ('');

###### we want the first element to be null and then we want the next one to be the current one
###### if its less than this year
###### then we want them to be this year followed by the next 10 years
###### this is being a bit elaborate, but I hate looking at dropdowns that have expired options in them
	my @YR = ('');

	my $yr = [localtime()]->[5];
###### the year element of localtime is the number of years after 1900, so we just need the last 2 characters
	my $curr_yr = substr($yr, 1, 2);

###### if their record shows a year less than the current year, we want to add it to the list so
###### that it can be the selected option showing them what the scoop currently is
	if ($year && $year lt $curr_yr)
	{
		push (@YR, $year);
	}

	for (my $x = $yr; $x <= ($yr + 10); $x++ )
	{
		push (@YR, substr($x, 1 ,2));
	}
	$q->popup_menu('-name'=>'cc_exp_yr',
				'-values'=>\@YR,
				'-default'=>$year);
}

sub cboPayMethod
{
	my $ppyes = 0;
	my %lbls = (
		'AP' => 'Payza',
		'CC' => 'Credit/Debit Card',
		'Money_Order' => 'Money Order',
		'ECHECK' => 'eCheck',
		'PP' => 'PayPal',
		'CA' => 'Club Account',
		'Paid_By_Other' => 'Paid By Other',
		'' => '');
	# The payment methods currently offered as options to the VIPs.
	my @PM = ('Money_Order', 'CC', 'CA');

	# If we don't openly offer this VIP's current payment method, we need to add it to the menu list.
	unless ( grep { /$data->{'payment_method'}/ } @PM )
	{
		push (@PM, $data->{'payment_method'});
	}

# No PayPal VIPs as of 11/22/05, so commenting this seciton out.
###### now check to see if PayPal is active, if it is, or if we are currently PayPal, we'll include it
	$ppyes = 1 if $data->{'payment_method'} eq 'PP';
	
##### we may want to exclude PayPal in every case, like if we are ever testing the switch
##### so if the constant is OFF, we won't include it even if its on otherwise
	if ((not $ppyes) && $PAYPAL_YES)
	{
		my $sth = $db->prepare("SELECT active FROM payment_methods WHERE method= 'PP'");
		$sth->execute;
		my $flag = $sth->fetchrow_array;
		$sth->finish;
		$ppyes = 1 if $flag and $flag == 1;
	}
	
	push (@PM, 'PP') if $ppyes;

###### the order of these items is important because of embedded javascript in the template that
###### refers to the selections as elements in an array
	$q->popup_menu(
		'-name' => 'payment_method',
		'-values' => \@PM,
		'-labels' => \%lbls,
		'-default' => $data->{'payment_method'},
		'-onChange' => 'clear_fields()');
}

sub CheckGroup
{
	my $sth = $db->prepare("SELECT usesysid FROM pg_user WHERE usename = '$creds->{'username'}'");
	$sth->execute;
	my $sid = $sth->fetchrow_array;
	$sth->finish;
	
	$sth = $db->prepare("SELECT grolist FROM pg_group WHERE groname = 'accounting'");
	$sth->execute;
	my $res = $sth->fetchrow_array;
	$sth->finish;
	($res =~ /$sid/) ? (return 1) : (return)
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8'),
		$_[0],
		'<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>',
		$q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Finish_Page
{
###### print a closing page affirming their action
	my ($page) = @_;
#	my $pplink = '<br /><br />';

	my $tmpl = $gs->GetObject($FINISH_PAGE{$page}) || die "Failed to load template: $FINISH_PAGE{$page}\n";
	print $q->header;
#	$tmpl =~ s/%%PayPal_Link%%/$pplink/;
	print $tmpl;
}

sub IDH
{
###### create a hash on the ID to make sure we are talking the right ID when we do an update
###### besides the id, we'll add another value just to keep it interesting for those who are hackers
	$cipher->encrypt_hex( $id .'loeilksdoimlmapipokipslzl3l2dm9ml' );
}

sub InputPage
{
	$InputPageTmpl = $archive_tmpl if $action eq 'arc' && $adminFLAG;
	my $hidden = my $FLAG = my $ADMIN_BLOCK = '';
	my $tmpl = $gs->GetObject($InputPageTmpl) || die "Failed to load template: $InputPageTmpl\n";

	if ($adminFLAG)
	{
		$hidden = $q->hidden('-name'=>'admin', '-value'=>1);
		$FLAG = '&nbsp;&nbsp;&nbsp;<span style="font-weight: bold; color: #ff0000;">ADMIN Mode</span>';
		$ADMIN_BLOCK = Admin_Block();
	}
	else
	{
		if ($data->{'payment_method'} eq 'CC')
		{
# this doesn't work... the operator returns -0- for the large "numbers"
#			$hidden .= $q->hidden(-name=>'ccnum', -value=> ~$data->{'cc_number'});

###### I'm sure there is an easier way, but I don't know it at the moment
###### we're are stepping through each character up to the last four and substituting an asterisk for it
			for (my $x = 0; $x <= (length $data->{'cc_number'}) - 5; $x++){
				$data->{'cc_number'} =~ s/\d+?/*/;
			}
		}
		elsif ($data->{'payment_method'} eq 'ECHECK')
		{
			$hidden.= $q->hidden('-name'=>'anum', '-value'=> ~$data->{'account_number'});
			for (my $x = 0; $x <= (length $data->{'account_number'}) - 5; $x++)
			{
				$data->{'account_number'} =~ s/\d+?/*/;
			}
		}
	}
	print $q->header('-expires'=>'now');
	$tmpl =~ s/%%adminFLAG%%/$FLAG/g;
	$tmpl =~ s/%%ADMIN_BLOCK%%/$ADMIN_BLOCK/;
	$tmpl =~ s/%%hidden%%/$hidden/g;
	$tmpl =~ s/%%url%%/$URL/g;
	$tmpl =~ s/%%id%%/$id/g;
	$tmpl =~ s/%%idh%%/&IDH()/e;
	$tmpl =~ s/%%firstname%%/$data->{firstname}/g;
	$tmpl =~ s/%%lastname%%/$data->{lastname}/g;
	$tmpl =~ s/%%acct_holder%%/$data->{'acct_holder'}/g;
	$tmpl =~ s/%%address%%/$data->{address}/g;
	$tmpl =~ s/%%city%%/$data->{city}/g;
	$tmpl =~ s/%%state%%/$data->{state}/g;
	$tmpl =~ s/%%postal_code%%/$data->{postal_code}/g;
	$tmpl =~ s/%%country%%/G_S::cbo_Country($q, 'country', $data->{'country'})/e;
	$tmpl =~ s/%%cc_number%%/$data->{'cc_number'}/g;
	$tmpl =~ s/%%acct_number%%/$data->{'account_number'}/g;
	$tmpl =~ s/%%routing_number%%/$data->{'routing_number'}/g;
	$tmpl =~ s/%%bank_name%%/$data->{'bank_name'}/g;
	$tmpl =~ s/%%payment_method%%/&cboPayMethod()/e;
	$tmpl =~ s/%%oldpayment_method%%/$data->{'payment_method'}/g;
#	$tmpl =~ s/%%paid_thru%%/$data->{paid_thru}/g;
#	$tmpl =~ s/%%annual_fee_paid_thru%%/$data->{annual_fee_paid_thru}/;
	$tmpl =~ s/%%cc_exp_mo%%/cboExpMonth()/e;
	$tmpl =~ s/%%cc_exp_yr%%/cboExpYear()/e;
	$tmpl =~ s/%%card_code%%/$data->{card_code}/g;
	print $tmpl;
}

sub Load_Params
{
	foreach (($q->param()), ('oldpayment_method','payment_method'))
	{
		$param{$_} = $gs->Prepare_UTF8($q->param($_) || '');
	}
}

sub Notify_ACF_of_ECheck
{
	my $qry = "select flag from nop,members where members.id=nop.id and members.";
	$qry .= ($id =~ /^\d/) ? ( "id = '$id'") : ("alias = '$id'");
	my $sth = $db->prepare($qry);
	$sth->execute;
	my $nop = $sth->fetchrow_array;
	$sth->finish;
	return unless $nop; # only send notification to Acctg if VIP is a no pay

	return if ($param{'oldpayment_method'} eq 'ECHECK' && # when method already ECHECK and
	   	   $param{'oldbank_name'} eq $param{'bank_name'} && # no eCheck info changed
	     	   $param{'oldaccount_number'} eq $param{'account_number'} &&
	    	   $param{'oldrouting_number'} eq $param{'routing_number'}); # no need to notify

###### notify HQ about eCheck processing that needs ATTENTION!!

        open (MAIL, "|/usr/sbin/sendmail -t") || return;
        print MAIL "From: mopi.cgi\n";
        print MAIL "To: acf1\@dhs-club.com\n";
        print MAIL "Subject: eCheck Change Notification\n";
        print MAIL "The following party has changed their method of payment information and is presently a NO PAY.\n\n";
        print MAIL "       ID: $id\n";
        print MAIL "     Name: $param{'acct_holder'}\n";
        print MAIL "   Acct #: $param{'account_number'}\n";
        print MAIL "Routing #: $param{'routing_number'}\n";
        print MAIL "Bank Name: $param{'bank_name'}\n";
#        print MAIL "Paid Thru: $param{original_paid_thru}\n";
	print MAIL "Last Mthd: $param{'oldpayment_method'}\n";
        print MAIL "\n1) Verify this Partner is not a fee deduct for the months that are past due.\n2) Bill the fee seperately if they are not a fee deduct for the past due months.\n";
        close MAIL;
	return;
}

sub Update
{
	my ($qry, $list) = ();

	Notify_ACF_of_ECheck() if ($param{'payment_method'} eq 'ECHECK');

###### we will loop through our FIELDS, since they are the same names as the mop columns
###### we can construct our query string from them and their corresponding param values
###### we'll just exclude the FIELDS that are not columns
	foreach (keys %FIELDS){ $list .= "$_\n" unless ($_ =~ /cc_exp/) }
	foreach(keys %param)
	{
		 if ($list =~ /$_/)
		{
			$qry .= " $_= " . $db->quote($param{$_}) . ',';
		}
	}
###### now we'll finish our query by concatenating the separate month/year vals for the CC exp.
	$qry .= " cc_exp = '$param{'cc_exp_mo'}$param{'cc_exp_yr'}', stamp = NOW()";
		
###### we'll explicitly add the admin fields if running in admin mode
	if ($adminFLAG)
	{
		$param{'notes'} = $db->quote("updated by: $creds->{'username'} \n" . $param{'notes'});
#		$qry .= ", notes = $param{'notes'}, paid_thru = '$param{paid_thru}', annual_fee_paid_thru = '$param{annual_fee_paid_thru}'";
		$qry .= ", notes = $param{'notes'}";
	}
	else
	{
		$qry .= ", notes = 'Info updated by member,  ' || NOW()";
	}
	$qry = "UPDATE mop SET $qry WHERE id = $id";

###### uncomment the following line to inspect the query without doing the actual update
#	print $q->header,$q->start_html,$qry; return 0;
	return $db->do($qry);
}

sub Validate_Params
{
###### our initial list of values to test for
	my @list = qw/acct_holder address city state postal_code country/;

###### we will perform validation and return either a (1) or an error message
	unless ( $param{'idh'} ne IDH())
	{
		return 'How can this be? ID doesn`t match hash!';
	}

	my ($msg) = ();
	my $msgend = '<br />Please press your back button and correct the problem';
	unless ($param{'payment_method'})
	{
		return 'How can this be? No Payment method selected!';
	}
	if ($param{'payment_method'} eq 'CC')
	{
		push (@list, 'cc_number', 'cc_exp_mo', 'cc_exp_yr');
	}
	elsif ($param{'payment_method'} eq 'ECHECK')
	{
		unless ($param{'country'} && $param{'country'} eq 'US')
		{
			return 'Payment by eCheck is valid only for account holders in the United States';
		}
		push (@list, 'bank_name', 'account_number', 'routing_number');
	}
###### with money order, Paid_By_Other or PayPal, we don't need any values
	elsif ($param{'payment_method'} =~ /Money_Order|PP|Paid_By_Other|MB|CA/)
	{
		@list = ();
	}
	else
	{
		return 'How can this be? Invalid payment method!';
	}

	foreach (@list)
	{
		unless ($param{$_})
		{
			$msg .= "$FIELDS{$_} is required.<br>\n";
		}

###### we'll filter out whitespaces and dashes from our number fields here
		elsif ($_ =~ /_number/)
		{
			$param{$_} =~ s/\s|-//g;
		}
	}

	return $msg . $msgend if $msg;

###### various validations begin here now that we know there is some data to test
###### although some of these tests are simplistic, they should help stop hacker type individuals

	if ($param{'payment_method'} eq 'CC')
	{
###### since we are inserting asterisk protected numbers in the cc and acct number fields
###### we need to restablish the values if they haven't been changed altogether
###### I do this by reconstituting the inverted cc number and checking the end of it against
###### the end of the returned string if is has asterisks
###### if they are the same, I will just put the reconstituted number in place of the asterisk laced number
		if ($param{'cc_number'} =~ /(\*+)(\d{4})/)
		{
#			my $ccnum = ~$param{ccnum};
#			if ($2 eq substr($ccnum, -4))
#			{
#				$param{'cc_number'} = $ccnum;
#			}
			($param{'cc_number'}) = $db->selectrow_array('SELECT cc_number FROM mop WHERE id= ?', undef, $mem_info->{'id'});
		}
		
		###### this routine will return undef unless it is not happy, then it will return a textual error message
		my $rv = CCAuth::NameCK({'account_holder_firstname'=>'', 'account_holder_lastname'=>$param{'acct_holder'}},	{'member'=>$mem_info});
# we will let admin do things a normal user is not allowed to do themselves
		return ccauth_parse::Report($rv) if $rv && ! $adminFLAG;

###### the expiration date parts should only be 2 digits each
		if ($param{'cc_exp_mo'} !~ /\d{2}/ && $param{'cc_exp_yr'} !~ /\d{2}/)
		{
			$msg .= 'Expiration dates are invalid<br />';
		}

		if ($param{'cc_number'} =~ /\D/)
		{
			$msg .= 'Credit Card number is invalid.<br />Only digits are allowed';
		}
		elsif (! validate($param{'cc_number'}))	# validate() is exported by default by Business::CreditCard
		{
			$msg .= 'Credit Card number is invalid.';
		}

###### check to be sure the expiration date is not past
###### localtime returns the month as Jan. = zero
		my $nowM = [localtime()]->[4] + 1; my $nowY = substr([localtime()]->[5], 1, 2);
		if ($param{'cc_exp_yr'} lt $nowY || ($param{'cc_exp_yr'} eq $nowY && $param{'cc_exp_mo'} < $nowM))
		{
			$msg .= 'The expiration date selected has expired.';
		}

		
###### at this point we have a 'good' credit number, so let's make sure we don't have more than the max
###### instances of it already in the DB
		my $sth = $db->prepare("SELECT COUNT(id) FROM mop where cc_number = '$param{'cc_number'}' AND id <> $mem_info->{'id'}");
		$sth->execute;
		my $cnt = $sth->fetchrow_array;
		$sth->finish;
		
		if ($cnt > 1 && ! $adminFLAG)
		{
			return qq#Sorry, we have more than 2 instances of this payment method.<br />
Please contact <a href="mailto: acf1\@dhs-club.com">acf1\@dhs-club.com</a> for further assistance.#;
		}
	}
	elsif($param{'payment_method'} eq 'ECHECK')
	{
		if ($param{'account_number'} =~ /(\*+)(\d{4})/)
		{
			my $anum = ~$param{anum};
			if ($2 eq substr($anum, -4)){ $param{'account_number'} = $anum }
		}
		
		if ($param{'account_number'} =~ /\D/)
		{
			$msg .= 'Account number is invalid.<br />Only digits are allowed<br />';
		}
		
		if ($param{'routing_number'} =~ /\D/)
		{
			$msg .= 'Routing number is invalid.<br />Only digits are allowed<br />';
		}
		
###### at this point we have a 'good' account number, so let's make sure we don't have more than the max
###### instances of it already in the DB
		my $sth = $db->prepare("SELECT COUNT(id) FROM mop where account_number = '$param{'account_number'}'");
		$sth->execute;
		my $cnt = $sth->fetchrow_array;
		$sth->finish;
		
		if ($cnt > 2)
		{
			return qq#Sorry, we have more than 2 instances of this payment method.<br />
Please contact <a href="mailto: acf\@dhs-club.com">acf1\@dhs-club.com</a> for further assistance.#;
		}
	}
	
	if ($msg)
	{
		return $msg . $msgend;
	}
	else {return 1}
}

###### 12/26/01 added DBI escaping of the update values
###### 01/07/02 changed the month comparison in Validate_Params exp. date check
###### 01/17/02 added some extra path information to the end of the redirect link when
###### they need to login first, that way when login redirects them the mopi URL looks different
###### and they don't get what the browser may have cached from the previous instance
###### 02/13/02 added 'duplicate' payment method checking
###### 04/03/02 reduced the key size to comply with the updated module and changed the evaluation expression
###### in the update action block so that $yesno could compare if it contained an error message
###### 04/18/02 added notification to Acctg if method of payment changes for eCheck and the VIP is a NO PAY
###### 07/03/02 added PayPal hooks for changing to & from PayPal
###### 10/03/02 added Upgrade lockout if 30 days since upgrade have not elapsed.
###### 11/22/02 added handling of cvv2 data and switched over to the database templating system
###### 12/17/02 went to the 'use' style for custom modules, also went to AuthCustom_Generic cookie
###### 03/03/03 fixed an html error in the admin block
###### 03/05/03 changed the combo box CC option to credit/debit, also disabled inclusion of ECHECK by default
##### 04/06/2003 - modified interval statements as noted below for postgres7.3:
#####  ...now() + interval('12 month') ) + 1 as paid_thru...
#####  ...cast(now() + interval '12 month' + interval '1 day' AS date) as paid_thru...
###### 04/07/03 further revisions of date SQL
###### 08/04/03 Added the default option to the Load_Params line: $param{$_} = $q->param($_) || '';
###### 12/16/03 enabled admin ability to have more than two instances of a mop
###### adjusted call to cboCountry to the new version
###### 10/05/04 tried addressing uninit'd vars regarding payment method
###### also did a little rearranging to make the code more readable in some places
###### 03/02/05 disabled the 30 day hold on payment method changes
###### 11/22/05 Added handling in cboPayMethod for the new payment method 'Paid_By_Other'
###### 12/18/06 Added functionality to provide access to archive records
###### 12/19/06 Added the annual_fee_paid_thru field to the admin block
###### 12/17/07 removed the determine_lockout routine altogether since it has been commented out for over 2 years
###### various other amputations and tweaks
###### 03/03/08 added the MB option to the list of options in Validate_Params
###### 10/09/08 added the use of Business::CreditCard to the mix for validating cards
# 09/08/09 made it so we no longer pass a parameter with the real card number obfuscated... instead if they do not change the value, we will look up the current value and use that
# 12/27/10 made it possible to set a credit account to a name other than the membership
###### 04/04/13 removed residual stuff pertaining to annual and monthly paid_thru dates, also reenabled PayPal as a payment method in the interface
# 06/24/14 only minor tweaks to get encoding right on the newer server without the CGI.pm hack
# 06/08/15 added binmode, Prepare_UTF8() to $mem_info as the param loading