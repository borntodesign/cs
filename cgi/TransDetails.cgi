#!/usr/bin/perl -w
###### TransDetails.cgi
###### 
###### Display member transaction details for the current period or the period submitted.
###### The script is driven by a Member ID which is found in the login cookie.
###### 
###### originally Created: 07/18/2003	Karl Kohrt
###### 
###### Last modified: 04/24/10	Bill MacArthur

# this was renamed from mbr_trans_rpt.cgi when moving back to Clubshop from GI
# as that named report now serves another purpose

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use XML::Simple;
use Template;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw/$LOGIN_URL/;
use GI;
#require gi_admin;
use ADMIN_DB;
#require Apache::GI_Authen;
#use GI_Authen_Constants qw($LoginScript);
require ClubCash;
#use Data::Dumper;	# only needed for debugging

###### GLOBALS###### our page templates

my %TMPL = (
	'period'		=> 10945,#1410,
	'xml'			=> 1412
);

#$Template::Parser::DEBUG = 1;
#$Template::Directive::Pretty = 1;
my $TT = new Template;
my $gs = G_S->new();
my $q = CGI->new();
my $db = ();
my $lang_nodes = '';
my $member_id = '';
my $member_info = '';
my $admin = ();
my $period_menu = '';
my %periods_hash = ();
my $GI = ();

# the key is the membertype
# the inner keys represent the "template" that will be used to create the data table and individual rows
my %subTMPL = (
	'v' => {
		'tbl' => _table_rppp(),
		'row' => _row_rppp()
	},
#	'amp' => {
#		'tbl' => _table_rppp(),
#		'row' => _row_rppp()
#	},
	'm' => {
		'tbl' => _table_rppp(),
		'row' => _row_rppp()
	},
	's' => {
		'tbl' => _table_rpnopp(),
		'row' => _row_rpnopp()
	}
);

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
# Report the transactions for the requested month if we have a good login/cookie.
if ( Get_Cookie() )
{
	$GI = GI->new('cgi'=>$q, 'db'=>$db);
	LoadLanguagePack();

	###### we need to implement a 'report on' functionality for upline VIPs
	###### we've already processed the 'id' parameter if we are ADMIN
	###### but we need to do so for VIP access
	if (! $admin)
	{
		my $tmp = ValidateRid( $q->param('id'));

		if (defined $tmp)
		{
			GetReportee($tmp);	# this just changes the member_id and member_info variables to reflect the reportee
			Err("Membership is inactive: $tmp") unless $member_info->{'can_login'};
		}
	}

	if ($member_id == 12)
	{
	    $member_id = 3218569;
	}
	Get_Periods();
	Report();
}

Exit();

###################################### Subroutines start here.
################################

sub AdminCK
{
#	return 0;	# uncomment to mimic ordinary operation even by Admin

	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return 0 unless $operator && $pwd;

	$db = ADMIN_DB::DB_Connect( 'pireport.cgi', $operator, $pwd ) ||
		"Failed to connect to the database with staff login";
	return $operator;
}

###### Create a combo box based upon the parmeters passed in.
sub Create_Combo
{
	my ($name, $list, $labels, $def) = @_;
	return $q->popup_menu(
		'-name' 	=> $name,
		'-values'	=> $list,
		'-labels'	=> $labels,
		'-default'	=> $def,
		'-onChange' => 'document.forms[0].submit()');
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error', '-encoding'=>'utf-8'),
		$q->h3($_[0]),
		$q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Fill_Transaction_Row
{
	my $one_row = shift;	# we are receiving a reference
	my $data = shift;
        my $tmpamt = sprintf("%.2f",$data->{'cash'} * $data->{'rate'});
	$data->{'rebate'} = $tmpamt;

	$data->{'nca'} = $data->{'native_currency_amount'} ? "$data->{'native_currency_amount'} $data->{'currency_code'}" : '';

	###### escaping is necessary since we may have data that is not HTML compliant
	######	'&' for example
	$data->{'biz_name'} = $q->escapeHTML($data->{'biz_name'});

	my $rv;
	$TT->process($one_row, $data, \$rv);
	return $rv;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{

	# If this is an admin user.		 
	if ( $q->param('admin') )
	{

		# Get the admin user and try logging into the DB
#		my $gi = gi_admin->new({'cgi'=>$q});
#		my $operator = $gi->authenticate();

		# Check for errors.
#		return unless $db = ADMIN_DB::DB_Connect( 'member_trans_rpt.cgi', $operator->{'username'}, $operator->{'password'} );
		return unless AdminCK();
		$db->{RaiseError} = 1;
		
		unless ($q->param('id') && $q->param('id') !~ /\D/)
		{
			Err('Missing or invalid parameter: id');
		}
		else
		{
			GetReportee($q->param('id'));
		}

		$admin =  $q->param('admin');	 
	}

	# If this is a member.
 	else
	{
		exit unless $db = DB_Connect( 'member_trans_rpt.cgi' );

		$db->{'RaiseError'} = 1;
		# Get the member Login ID and info hash.
#		( $member_id, $member_info ) = GI_Authen::authen_ses_key( undef, undef, $q->cookie('GI_Authen_General') );

		( $member_id, $member_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			return;
		}
	}

	return 1;
}

###### Get the reporting periods and build a drop-down menu.
sub Get_Periods	
{
	my $data = '';

	my @list = ();
	my %period_lbls = ();

	# by tying the periods to transactions, we can avoid a long list of empty periods
	my $sth = $db->prepare(q|
		SELECT DISTINCT
			p.period,
			p.open,
			COALESCE(p.close, NOW()::DATE) AS close
			
		FROM periods p
		JOIN transactions t
			ON(
				t.apportioned::DATE BETWEEN p.open AND COALESCE(p.close, NOW()::DATE)
				OR
				t.entered::DATE BETWEEN p.open AND COALESCE(p.close, NOW()::DATE)
			)
		WHERE 	t.id = ?
		AND 	p."type"=1 
		AND p.open >= NOW() - INTERVAL '4 months'

	/*
		add this to ensure that if there are no transactions so far this month,
		that this month is still on the list as this month is the default
	*/
		UNION
		SELECT
			p.period,
			p.open,
			COALESCE(p.close, NOW()::DATE) AS close
		FROM periods p
		WHERE p.period= get_working_period(1,NOW())
		
		ORDER BY "open" DESC|);
	$sth->execute($member_id);

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@list, $data->{'period'});
		$periods_hash{$data->{'period'}}{'label'} = $period_lbls{$data->{'period'}} = "$data->{'open'} to $data->{'close'}";
		$periods_hash{$data->{'period'}}{'open'} = $data->{'open'};
		$periods_hash{$data->{'period'}}{close} = $data->{close};
	}
	$sth->finish;
	$period_menu = qq#<span class="fb_r">Reporting Periods</span><br />\n#;

	$period_menu .= Create_Combo('period', \@list, \%period_lbls);
	$period_menu .= "&nbsp;" . $q->submit('-value'=>'Go');
}

sub LoadLanguagePack
{
	my $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to load xml language pack: $TMPL{xml}";
	$lang_nodes = XMLin($xml, 'NoAttr' => 1);
}

sub GetReportee
{
	my $tmp = shift;
	$member_id = $tmp;
	$member_info = $db->selectrow_hashref('
		SELECT m.alias, m.id, m.spid, m.membertype, m.emailaddress, m.firstname, m.lastname, mt.can_login
		FROM members m
		JOIN "Member_Types" mt
			ON mt."code"=m.membertype
		WHERE m.id= ?', undef, $tmp) || die "Failed to load member: $tmp";
}

sub GetRP
{
    my $rwdamt = ClubCash::Get_ClubCash_Amounts($member_id, 'member_trans_rpt.cgi');
    my $tmpx = sprintf("%.2f %s", $rwdamt->{'redeem'}, $rwdamt->{'currency'});
    my ($rv) = $tmpx;
	return $rv;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
#	print $q->redirect("$LoginScript?err=9;destination=$me");
	print $q->redirect("$LOGIN_URL?destination=$me");
}

###### Display the transaction report for the requested period.
sub Report
{
#	my $one_row = '';
	my $trans_total = 0;
	my $rebate_total = 0;
	my $ppp_total = 0;
	my $rpp_total = 0;
	my $year = '';

	# Get the period that was passed or default to the current period.
	my ($period) = $q->param('period') || $db->selectrow_array('SELECT get_working_period(1, NOW())');

	# Get the member's transactions for the period specified.
	my $sth = $db->prepare("
		SELECT t.trans_id,
			SUBSTRING(t.trans_date::TEXT FROM 6) AS trans_date,
			SUBSTRING(t.trans_date::TEXT FROM 1 FOR 4) AS trans_year,
			COALESCE(t.amount,0) AS amount,
			SUM(ta.rebate) AS rebate,
			SUM(ta.commission) AS commission,
			SUM(ta.ppp) AS ppp,
			SUM(ta.rpp) AS rpp,
			ty.description,
			CASE
				WHEN t.trans_type IN (5,6) THEN mal.location_name
				ELSE v.vendor_name
			END AS biz_name,
			COALESCE(t.currency_code,'USD') AS currency_code,
			t.native_currency_amount,
			COALESCE(rp.reward_points,0) as cash,
			COALESCE(rp.cur_xchg_rate,1) as rate,
			COALESCE(t.description,'') AS trans_desc

		FROM transactions t
		-- this used to be an inner join
		LEFT JOIN vendors v
			ON v.vendor_id=t.vendor_id
		INNER JOIN transaction_types ty
			ON t.trans_type=ty.trans_type
		LEFT JOIN ta_live ta
			ON ta.transaction_ref=t.trans_id AND ta.id=t.id
		LEFT JOIN merchant_affiliate_locations mal
			ON mal.vendor_id=v.vendor_id
		LEFT JOIN reward_transactions rp
			ON rp.trans_id = t.trans_id

		WHERE t.id = ?
 		AND ((ta.id ISNULL AND t.entered::DATE BETWEEN ? AND ?) OR ta.period = ?)
		AND t.void = FALSE
		GROUP BY
			t.trans_id,
			t.trans_date,
			t.amount,
			ty.description,
			t.trans_type,
			mal.location_name,
			v.vendor_name,
			t.currency_code,
			t.native_currency_amount,
			rp.reward_points,
			rp.cur_xchg_rate,
			t.description

		ORDER BY
			trans_date,
			description,
			biz_name
	");
#die "$member_id, $periods_hash{$period}{open}, $periods_hash{$period}{close}, $period\n";
	$sth->execute( $member_id, $periods_hash{$period}{'open'}, $periods_hash{$period}{'close'}, $period );

	my $report_body = '';
	my $report_row = $subTMPL{ $member_info->{'membertype'} }->{'row'};

	while ( my $data = $sth->fetchrow_hashref)
	{
		# Add the new row to the other rows.
		$report_body .= Fill_Transaction_Row(\$report_row, $data);

		# Add the individual amounts to the aggregate totals.
		$trans_total += $data->{'amount'} || 0;
		$rebate_total += $data->{'cash'} * $data->{'rate'} || 0;
		$ppp_total += $data->{'ppp'} || 0;
		$rpp_total += $data->{'rpp'} || 0;
		$year = $data->{'trans_year'};						
	}		

	# there is one lang_node that contains TT placholders
	# rather than reparse the complete document just to take care of these, we'll do so now
	my $tmp;
	$TT->process(\$lang_nodes->{'period'}, {'period' => {'open' => $periods_hash{$period}->{'open'}, 'close' => $periods_hash{$period}->{'close'} }}, \$tmp);
	$lang_nodes->{'period'} = $tmp;
	
	my $table;
	my $data = {
		'report_body' => $report_body,
		'trans_total' => sprintf('%.2f', $trans_total),
		'ppp_total' => sprintf('%.2f', $ppp_total),
		'rpp_total' => sprintf('%.2f', $rpp_total),
		'rebate_total' => sprintf('%.2f', $rebate_total),
		'lang_nodes' => $lang_nodes
	};
	
	# for admin use we may end up in here for non GI memberships, so we'll give 'em the simple AP report
	my $tmpl = $subTMPL{$member_info->{'membertype'}}->{'tbl'} || $subTMPL{'m'}->{'tbl'};
	$TT->process(\$tmpl, $data, \$table);

	$tmpl = $gs->Get_Object($db, $TMPL{'period'}) || die "Couldn't open $TMPL{'period'}";
	my $rv;
	$data = {
		'lang_nodes' => $lang_nodes,
		'member' => $member_info,
		'ME' => $q->url,
		'admin' => ($q->param('admin') || ''),
		'cboPeriod' => $period_menu,
		'table' => $table,
		'rpttl' => GetRP()
	};
	$TT->process(\$tmpl, $data, \$rv);
#	print $q->header('-charset'=>'utf-8'), $rv;

	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_nodes->{'title1'},
			'style'=> $q->style({'-type'=>'text/css'}, '
				td.rp {
					text-align:right;
					padding-right:1em;
				}
				
				td.data { font-weight:normal;}'),
#			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/greybox-jquery.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$rv
		});	
}

sub ValidateRid
{
	my $tmp = shift;
	return unless $tmp;

	if ($tmp =~ /\D/ || length $tmp > 7)
	{
		Err($lang_nodes->{'badid'});
	}

	###### now let's make sure they are in my downline
	if ($gs->Check_in_downline($db, 'members', $member_id, $tmp))
	{
		return $tmp;
	}
	else
	{
		Err("$lang_nodes->{'notindn'} $tmp");
	}
}

sub _row_rppp
{
	return <<'EOT';
<tr>
<td align="center" class="data">[% trans_date %]</td>
<td align="left" class="data">[% description %]</td>
<td align="left" class="data">[% biz_name %]</td>
<td align="left" class="data">[% trans_desc %]</td>
<td class="data rp">[% nca %]</td>
<td class="data rp">[% amount %]</td>
<td class="data rp">[% rebate %]</td>
<td class="data rp">[% rpp %]</td>
<td class="data rp">[% ppp %]</td>
</tr>
EOT
}

sub _row_rpnopp
{
	return <<'EOT';
<tr>
<td align="center" class="data">[% trans_date %]</td>
<td align="left" class="data">[% description %]</td>
<td align="left" class="data">[% biz_name %]</td>
<td align="left" class="data">[% trans_desc %]</td>
<td class="data rp">[% nca %]</td>
<td class="data rp">[% amount %]</td>
<td class="data rp">[% rebate %]</td>
</tr>
EOT
}

sub _table_rppp
{
	return <<'EOT';

<table border="1" style="background-color:white; empty-cells:show;" cellpadding="3" cellspacing="0">
<thead>
<tr> 
<th class="heading">[% lang_nodes.date %]</th>
<th class="heading">[% lang_nodes.trans_type %]</th>
<th class="heading">[% lang_nodes.merch_name %]</th>
<th class="heading">[% lang_nodes.trans_desc %]</th>
<th class="heading">[% lang_nodes.rpt_purch_amt %]</th>
<th class="heading">[% lang_nodes.purch_amt %]</th>
<th class="heading">[% lang_nodes.rp %]</th>
<th class="heading">RPP</th>
<th class="heading">PPP<!--[% lang_nodes.pp %]--></th>
</tr>
</thead>
<tbody>

[% report_body %]

</tbody>
<tfoot>
<tr>

<td colspan="5" align="right" class="heading">[% lang_nodes.totals %]</td>

<td align="right" class="heading rp">$[% trans_total %]</td>

<td align="right" class="heading rp">[% rebate_total %]</td>
<td align="right" class="heading rp">[% rpp_total %]</td>
<td align="right" class="heading rp">[% ppp_total %]</td>

</tr>
</tfoot>
</table>

EOT
}

sub _table_rpnopp
{
	return <<'EOT';
	
<table border="1" style="background-color:white; empty-cells:show;" cellpadding="3" cellspacing="0">
<thead>
<tr> 
<th class="heading">[% lang_nodes.date %]</th>
<th class="heading">[% lang_nodes.trans_type %]</th>
<th class="heading">[% lang_nodes.merch_name %]</th>
<th class="heading">[% lang_nodes.trans_descr %]</th>
<th class="heading">[% lang_nodes.rpt_purch_amt %]</th>
<th class="heading">[% lang_nodes.purch_amt %]</th>
<th class="heading">[% lang_nodes.rp %]</th>
</tr>
</thead>
<tbody>

[% report_body %]

</tbody>
<tfoot>
<tr>

<td colspan="5" align="right" class="heading">[% lang_nodes.totals %]</td>

<td align="right" class="heading">$[% trans_total %]</td>

<td align="right" class="heading">[% rebate_total %]</td>

</tr>
</tfoot>
</table>

EOT
}

###### 10/15/2003 Added access for the free members via the 'pa' cookie.
###### 12/05/2003 Added the carry over balance info to the report.
###### 01/15/04 removed the stuff pertaining to the $membertype since it doesn't seem
###### applicable, also added functionality for VIPs to view a downlines report
###### 01/26/04 repaired the history reporting which was broken after the inclusion
###### of the VIP downline functionality
###### 02/26/04 made the DB disconnect conditional since there are circumstances
###### where it may not be active
###### 01/06/05 added a hack to display 'reward points'
###### 02/11/05 Removed the int() function from $reward_points just prior to display because
######		it was rounding down an integer value by 1 for some reason.
###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login. Added query to get alias value.
###### 03/02/06 revised the SQL in Get_Periods to rely on 'open' rather than 'period'
# 07/06/06 revised the SQL for gathering transactions, also employed the global login URL
###### 09/06/06 revised the ordering for the date selector combo
###### 09/20/06 adjusted to require full login for personal access
# 10/11/06 tweaked the periods query to pull last day of month apportionments that were being omitted
# due to the between evaluating dates instead of timestamps
# 01/05/07 had to extend the hack to display reward points instead of rebates
###### 06/14/07 since the ta_live records before 2005 have been removed, I have removed the rebate hack
###### also ripped out the carry_over stuff since we don't use that anymore
###### also revised the SQL for pulling transactions so that transactions will come up in the abscence of ta_live records
###### this allows reporting of transactions that have not yet been apportioned as well as ones for which there is
###### no payout amount (like when we give Walmart credit to DHS Club Kids but the member keeps the transaction)
# 02/13/08 revised the transaction SQL to remove distinct and use SUMs instead since an MA purchase from a downline could result in 2 distinctly different ta_live records
# 07/02/08 expanded the concept of the period gathering SQL to use the entered or the apportioned date... this was necessary to accomodate a recent glitch where some transactions were reapportioned after period lockdown
# ideally we could just query ta_live for distinct periods, but the query cost is about 20 times the current query
# 08/11/08 made the vendor table a left join so as to pull in VIP subscriptions which are not currently associated with any particular vendor
###### 09/22/08 added formatting to the RP total
# 09/17/09 added handling for the native currency in the reports
###### 11/17/09 tweaked the report generation query to handle transactions with no personally related ta records
# 03/02/11 replaced reward points with clubcash
# 03/24/11 added code for amp member type
###### 04/24/12 added RPP to the column mix for reports which include PP