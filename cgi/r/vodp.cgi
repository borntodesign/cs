#!/usr/bin/perl -w
# vodp : One Downline Pool redirector for VIPs
# a "fixed" URL for VIPs to use to access the Trial Partner Position reports

use strict;
use lib '/home/httpd/cgi-lib';
use CGI;
use DB_Connect;

my $destination = 'http://www.glocalincome.com/cgi/odp.cgi?id=';
my $cgi = new CGI;

# we are expecting an ID in the path
my $id = $cgi->path_info;
(undef, $id) = split('/', $id);		# this works because the path looks like this: /AB1233456

if (! $id)
{
	print $cgi->redirect($destination);
	exit;
}

my $db = DB_Connect::DB_Connect('generic') || exit;

($id) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, uc($id)) if $id =~ m/\D/;
# we could have come up with nothing...
if (! $id)
{
	print $cgi->redirect($destination);
}
else
{
	# we are safe directly inserting the ID value as we know it is strictly numeric at this point
	# no activity created for VIP visits
#	$db->do('INSERT INTO activity_tracking (id, "class") VALUES ($id, 51)');
	print $cgi->redirect($destination . $id);
}

$db->disconnect;
exit;
