#!/usr/bin/perl -w
# actodp : One Downline Pool Activity creator
# We want to ensure the trigger of the Trial Partner Report viewed activity when coming in from an email link
# this script will do just that

##### long before 03/22/13 this thing was defunct, but since people are still hitting it, we are going to disable the records creation lines

use strict;
use lib '/home/httpd/cgi-lib';
use CGI;
use DB_Connect;

my $destination = 'http://www.glocalincome.com/cgi/odp.cgi?id=';
my $cgi = new CGI;

# we are expecting an ID in the path
my $id = $cgi->path_info;
(undef, $id) = split('/', $id); 

if (! $id)
{
	print $cgi->redirect($destination);
	exit;
}

#my $db = DB_Connect::DB_Connect('generic') || exit;

#($id) = $db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, uc($id)) if $id =~ m/\D/;
# we could have come up with nothing...
if (! $id)
{
	print $cgi->redirect($destination);
}
else
{
	# we are safe directly inserting the ID value as we know it is strictly numeric at this point
#	$db->do(qq|INSERT INTO activity_tracking (id, "class") VALUES ($id, 51)|);
#	$db->do(qq|INSERT INTO member_ranking_daily_trans (member_id, action) VALUES ($id, 5)|);
	print $cgi->redirect($destination . $id);
}

#$db->disconnect;
exit;
