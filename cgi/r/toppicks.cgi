#!/usr/bin/perl -w
# toppicks.cgi : give 'em an ID cookie and send them on their way to their toppicks page, embedded in their mall

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $destination_base = 'http://www.clubshop.com/mall/';
my $cgi = new CGI;

# we are expecting an ID and a country code and a mall ID in the path
my ($id, $cc, $mall_id, @cookies) = ();
my $path = $cgi->path_info;
(undef, $id, $cc, $mall_id) = split('/', $path); 
$cc ||= 'US';
$mall_id ||= 1;

if (! $id || $id =~ m/\D/)	# we will not accept an alias as we're not doing DB lookups
{
	# die "Could not extract an ID from the path";
	# do nothing
}
else
{
	push @cookies, $cgi->cookie('-name'=>'id','-value'=>$id, '-domain'=>'.clubshop.com', '-expires'=>'+1y');
}
print $cgi->redirect('-location'=>$destination_base . "$cc/?if=" . $cgi->escape("/cgi/top_picks2.cgi?mall_id=$mall_id"), '-cookies'=>\@cookies);
exit;
