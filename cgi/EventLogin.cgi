#!/usr/bin/perl -w
###### EventLogin.cgi
###### a resource to provide a specialized login for event functions
###### - a most rudimentary work in progress -
###### created: 09/12/07 Bill MacArthur
###### last modified:

use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
require EventLogin;
use DB_Connect;

my $q = new CGI;
unless ($q->https){	# coerce SSL usage
	my $url = $q->url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my $db = DB_Connect::DB_Connect('EventLogin') || exit;

###### require a VIP to be logged in to operate this thing
goto 'END' unless VIP_Login_Ck();

my $action = $q->param('action') || '';

# this global value can be appended to and it will appear towards the top of the interfaces
my $ErrorMsg = '';
# this global will be inserted into the menu interface to open a new window
# rather than 'directing' a form to a new window
my $JScript = '';
my @Cookies = ();

if ($action eq 'login')
{
	Login() ? ShowEventMenu() : LoginForm();
}
elsif (! LoggedIn()){ LoginForm(); }
elsif (! $action){ ShowEventMenu(); }
elsif ($action){
	DoAction();
	ShowEventMenu();
}
else { die "Unhandled exception\n"; }

END:
$db->disconnect;
exit;

sub DoAction
{
	###### rather than clutter up the basic logic flow above
	###### we'll handle our anticipated actions here and bomb generically if necessary
	my %map = (
		memberapp => \&_memberapp,
		upgapp => \&_upgapp,
		nmupgapp => \&_nmupgapp
	);
	die "Undefined action\n" unless grep $_ eq $action, keys %map;
	&{$map{$action}};
}

sub Error
{
	###### it is best to simply render an interface with an error message
	###### however, when they are attempting an action, like a member application that opens in a new window,
	###### we don't really want to create another instance of the interface
	print $q->header(-charset=>'utf-8', -expires=>'now'),
		$q->start_html(-title=>'Error', -style=>{-src=>'/css/EventLogin.css'}),
		$q->h4('An error has occured'), $q->div({-class=>'error'}, $_[0]), $q->end_html;
}

sub LoggedIn
{
	return EventLogin::ValidateTicket( $q->cookie('EventLogin') );
}

sub Login
{
	###### process a login
	my $name = $q->param('name');
	my $pwd = $q->param('pwd');
	my $event = $q->param('eventname');
	$ErrorMsg .= $q->div('Username missing') unless $name;
	$ErrorMsg .= $q->div('Password missing') unless $pwd;
	$ErrorMsg .= $q->div('Event Name missing') unless $event;
	return undef if $ErrorMsg;

	###### validate our credentials and if good, create a cookie
	my $rv = $db->selectrow_hashref('SELECT * FROM events WHERE eventname = ?', undef, $event);
	unless ($rv->{eventname}){
		$ErrorMsg .= $q->div('Event is invalid or expired');
		return undef;
	}
	unless ($rv->{name} eq $name && $rv->{pwd} eq $pwd){
		$ErrorMsg .= $q->div('Invalid login credentials');
		return undef;
	}
	push (@Cookies, EventLogin::CreateTicket( $rv ));
}

sub LoginForm
{
	###### create a list of events available for login
	my @eventnames = ();
	my $sth = $db->prepare("
		SELECT eventname FROM events
		WHERE expired=FALSE AND NOW() BETWEEN valid_from AND valid_to ORDER BY valid_from");
	$sth->execute;
	while (my ($tmp) = $sth->fetchrow_array){ push @eventnames, $tmp; }
	my $eventCtl = $q->popup_menu(
		-name=>'eventname',
		-values=>\@eventnames
	);
	my $title = 'DHS Club Event Login';
	print $q->header(-charset=>'utf-8', -expires=>'now'),
		$q->start_html(
			-title=>$title,
			-encoding=>'utf-8',
			-style=>{-src=>'/css/EventLogin.css'},
			-script=>{-src=>'/js/EventLogin.js'}),
		$q->h4($title);
	print $q->div({-class=>'error'}, $ErrorMsg) if $ErrorMsg;
	unless (@eventnames){ print $q->p('There are no active events at the moment'); }
	else {
		print $q->start_form(-method=>'post'), $q->table(
			$q->Tr($q->td('Select Your Event:') . $q->td($eventCtl)) .
			$q->Tr($q->td($q->label({-for=>'name'}, 'Username:')) .
				$q->td($q->textfield(-id=>'name', -name=>'name'))
			) .
			$q->Tr($q->td($q->label({-for=>'pwd'}, 'Password:')) .
				$q->td($q->textfield(-id=>'pwd', -name=>'pwd'))
			)			
		),
			$q->hidden(-name=>'action', -value=>'login'), $q->submit('Login'), $q->end_form;
	}
	print $q->end_html;
}

sub ShowEventMenu
{
	###### a simple form for now
	my $title = 'DHS Club Event Functions';
	print $q->header(-charset=>'utf-8', -cookies=>\@Cookies, -expires=>'now'),
		$q->start_html(
			-title=>$title,
			-encoding=>'utf-8',
			-style=>{-src=>'/css/EventLogin.css'},
			-script=>{-src=>'/js/EventLogin.js'}),
		$q->h4($title);
	print $q->div({-class=>'error'}, $ErrorMsg) if $ErrorMsg;
	print $JScript if $JScript;
	###### 'VIP' direct signup form
	print $q->start_form, $q->hidden(-name=>'action', -value=>'memberapp', -force=>1),
		$q->fieldset( $q->legend('Sign Up Members') .
			$q->label({-for=>'sponsorID'}, 'Sponsor ID: ') .
			$q->textfield(-id=>'sponsorID', -name=>'sponsorID') .
			$q->submit('Submit') . $q->button(-label=>'Reset', -onclick=>"activeReset('sponsorID')")
		),
		$q->end_form;

	###### the member -> VIP upgrade application (just a simple link for now)
	print $q->start_form, $q->hidden(-name=>'action', -value=>'upgapp', -force=>1),
		$q->fieldset( $q->legend('Member to VIP Upgrade') .
			$q->submit('Upgrade Member to VIP')
		),
		$q->end_form;

	###### Non-Member -> VIP app
	print $q->start_form, $q->hidden(-name=>'action', -value=>'nmupgapp', -force=>1),
		$q->fieldset( $q->legend('Non-Member VIP Application') .
			$q->label({-for=>'nmsponsorID'}, 'Sponsor ID: ') .
			$q->textfield(-id=>'nmsponsorID', -name=>'sponsorID') .
			$q->submit('Non-Member to VIP') . $q->button(-label=>'Reset', -onclick=>"activeReset('nmsponsorID')")
		),
		$q->end_form;

	print $q->end_html;
}

sub VIP_Login_Ck
{
	my( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

	if ( !$id || $id =~ /\D/ || $meminfo->{membertype} ne 'v')
	{
		Error("VIP login is required to access this interface: <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
#		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
		return 0;
	}
	return 1;
}

sub _memberapp
{
	###### redirect them to the VIP direct memberapp after checking some vals
	###### like the ID they submitted ;)
	my $spid = $q->param('sponsorID');
	unless ($spid){
		$ErrorMsg = $q->div('The sponsor ID was empty');
		return;
	}
	my $qry = 'SELECT id FROM members WHERE ' . ($spid =~ /\D/ ? 'alias=?' : 'id=?');
	my ($clean) = $db->selectrow_array($qry, undef, uc $spid);

	unless ($clean){
		$ErrorMsg = $q->div("The sponsor ID was invalid: $spid");
		return;
	}
	$JScript = qq|<script type="text/javascript">
		window.open('https://www.clubshop.com/cgi/appx.cgi/$clean/vip');
		</script>|;
}

sub _nmupgapp
{
	###### the non-member -> VIP app requires a sponsorID (for the purpose of Events anyway)
	my $spid = $q->param('sponsorID');
	unless ($spid){
		$ErrorMsg = $q->div('The sponsor ID was empty');
		return;
	}
	my $qry = 'SELECT id FROM members WHERE ' . ($spid =~ /\D/ ? 'alias=?' : 'id=?');
	my ($clean) = $db->selectrow_array($qry, undef, uc $spid);

	unless ($clean){
		$ErrorMsg = $q->div("The sponsor ID was invalid: $spid");
		return;
	}
	$JScript = qq|<script type="text/javascript">
		window.open('https://www.clubshop.com/cgi/appx.cgi//upg?flg=dv;sponsorID=$clean');
		</script>|;
}

sub _upgapp
{	###### for now just a simple redirect
	$JScript = qq|<script type="text/javascript">
		window.open('https://www.clubshop.com/cgi/appx.cgi//upg');
		</script>|;
}


