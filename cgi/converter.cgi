#!/usr/bin/perl -w
###### converter.cgi
###### 05/22/2003	Karl Kohrt
######
###### A currency converter that uses the following parameters:
######		code = The 3-letter currency code from the table currency_codes.
######		amount = The US dollar amount to be converted.
######
###### Last modified: 05/28/15 Bill MacArthur

use strict;

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use G_S;
require XML::local_utils;

###### CONSTANTS
our %TMPL = (	'xsl'	=> 10381,
		'xml'	=> 10382);
our %TBL = (	'codes'	=> 'currency_codes',
		'rates'	=> 'exchange_rates');

###### GLOBALS
our $q = new CGI;
$q->charset('utf-8');
our $gs = new G_S;
our $ME = $q->url;
our $db = '';
our $action = '';
our $amount = '';				# The amount to be converted.
our $total = '';				# The converted amount.
our $conversion_rate = 1;			# The most recent currency conversion rate.
our $from_code = '';				# The currency exchange code.
our $to_code = '';				# The currency exchange code.
our $currency = '';
our %currency_hash = ();
our @currency_list = ();

##########################################
###### MAIN program begins here.
##########################################

unless ($db = &DB_Connect::DB_Connect('converter.cgi')){exit}
$db->{RaiseError} = 1;

# Get the passed in currency code or the person's default code if cookie exists and no action.
$from_code = uc( $q->param('from_code') ) || $q->cookie('from_currency') || $q->cookie('currency_code') || 'USD';
$to_code = uc( $q->param('to_code') ) || $q->cookie('to_currency') || 'USD';
$amount = $q->param('amount') || '';

# some users submit values that are formatted "incorrectly", so lets doctor up the amount
if ($amount =~ m/,/)
{
	# if they are using a comma as the decimal separator with two trailing digits, let's convert it do a period
	$amount =~ s/(,)\d{2}$/\./;

	# remove all other commas
	$amount =~ s/,//g;
}
$amount = '' if $amount !~ m/^\d+\.?\d*$/;

if ( $amount && $from_code && $to_code )
{
	$total = Convert();	# Do the currency conversion.
}

# Display the converter and the conversion if we have one.
Do_Page();

$db->disconnect;
exit;

##############################
###### Subroutines start here.
##############################

###### Do a currency conversion.
sub Convert
{
	# Get the most recent rate and date.
	(my $from_rate) = $db->selectrow_array("
						SELECT r.rate
						FROM	$TBL{rates} r
						JOIN $TBL{codes} c
							ON r.code = c.code
						WHERE 	r.code = '$from_code'
						AND 	c.active = 't'
						ORDER BY r.date DESC
						LIMIT 1;");
	(my $to_rate, $currency) = $db->selectrow_array("
						SELECT r.rate, c.description
						FROM	$TBL{rates} r
						JOIN $TBL{codes} c
							ON r.code = c.code
						WHERE 	r.code = '$to_code'
						AND 	c.active = 't'
						ORDER BY r.date DESC
						LIMIT 1;");
	return (sprintf("%.2f", ($to_rate / $from_rate) * $amount));	
}
###### Create a combo box based upon the parmeters passed in.
sub Create_Combo
{
	my ($name, $list, $labels, $def) = @_;
	return $q->popup_menu(	-name		=> $name,
					-values	=> $list,
					-labels	=> $labels,
					-default	=> $def,
					-class		=> 'menu',
					-force		=> 'true');
}

###### Build and output the page to the browser.
sub Do_Page
{
	my $xsl = $gs->Get_Object($db, $TMPL{xsl}) || die "Failed to load XSL template: $TMPL{xsl}\n";

	my $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to load template: $TMPL{xml}\n";

	$total = ' ' unless $total;
	
	if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
	else
	{
		Get_Currencies();
		my $menu = Create_Combo("from_code", \@currency_list, \%currency_hash, $from_code);
		$xsl =~ s/%%CURRENCY_FROM_MENU%%/$menu/g;
		$menu = Create_Combo("to_code", \@currency_list, \%currency_hash, $to_code);
		$xsl =~ s/%%CURRENCY_TO_MENU%%/$menu/g;
		$xsl =~ s/%%ME%%/$ME/g;
		$xsl =~ s/%%AMOUNT%%/$amount/g;
		$xsl =~ s/%%TOTAL%%/$total/g;
		$currency .= qq#<hr width="50%"/># if $currency;
		$xsl =~ s/%%CURRENCY%%/$currency/g;

		my $from_cookie = $q->cookie(	-name	=> 'from_currency',
							-value	=> $from_code,
							-expires => '+1y');
		my $to_cookie = $q->cookie(		-name	=> 'to_currency',
							-value	=> $to_code,
							-expires => '+1y');
		# Create the currency exchange rate cookies.	
		print $q->header( 	-charset=>'utf-8',
					-cookie=>[$from_cookie,$to_cookie]
				);
		# print the page.
		print XML::local_utils::xslt($xsl, $xml);
	}
}

##### Prints an error message to the browser.
sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>", $_[0], "</B><BR>";
	print '<p>Current server time: <tt>' . scalar (localtime) . ' PST</tt>';
	print $q->end_html();
}

###### Get the currency codes & descriptions and build a drop-down menu.
sub Get_Currencies	
{
	my $qry = '';
	my $sth = '';
	my $data = '';

	$qry = "SELECT DISTINCT cc.code,
				cc.description
			FROM 	$TBL{codes} cc
			JOIN currency_by_country cbc
				ON cbc.currency_code=cc.code	-- this join makes it so we only show currencies that are actively in use around the world
			WHERE 	cc.active = 't'
			ORDER BY cc.description;";
	$sth = $db->prepare($qry);
	$sth->execute();

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@currency_list, $data->{code});
		$data->{close} ||= "current";
		$currency_hash{$data->{code}} = "$data->{description}";
	}
	$sth->finish;
}
		

###### 04/27/05 Added "WHERE active = 't'" to get only active currencies in the sub Get_Currencies.
######		Also added "active = 't'" to the sub Convert.
###### 03/23/06 Modified so that it now offers two currencies - from and to - instead of
######		only allowing conversion into US Dollars.
# 05/28/15 minor mods to avoid bad amount values being used in the comparison