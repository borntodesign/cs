#!/usr/bin/perl -w
###### Stripe.cgi
######
###### A script that allows that handles Stripe transactions
###### Originally copied from PP.cgi
######
###### created: 05/18	Bill MacArthur
######
###### modified: 
	
use strict;
use Template;
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util::Numeric qw(isnum);
use Mail::Sendmail;
use Data::Dumper;
use Scalar::Util qw(looks_like_number);
use lib ('/home/httpd/cgi-lib');
use CS_Stripe;
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use GI;
use XML::local_utils;
use Clubshop::Session;

###### ###### be sure to set that 'test' value correctly... 0=LIVE 1=TEST
my $cspp = CS_Stripe->new({'test'=>0});
my $TT = new Template;
my $q = new CGI;
my ($db, %params, $errors, $lang_blocks) =();
my $gs = G_S->new({'db'=>\$db});
my %TMPL = (
	'XML' => 11141,
	'LoadStripeCheckout'                    => 11142,
	'startStripeCheckout_Funding' 		=> 11140
);
my $txnType = 3011; # the id in the soa_transaction_types table
my $app_id = 406;
my $expiry = "1 hour";	# An SQL Interval - how long we'll hold the process_state record.

my ( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
if (! $member_id || $member_id =~ m/\D/)
{
	print $q->redirect("$LOGIN_URL?destination=" . $q->url);
	Exit();
}

LoadParams();

# if we did not fail at loading params, then we can go ahead and make a DB connection
$db = DB_Connect('Stripe.cgi');
$db->{'RaiseError'} = 1;
my $GI = GI->new('cgi'=>$q, 'db'=>$db);
my $session = Clubshop::Session->new('db' => $db, 'app_id' => $app_id, 'expiry' => $expiry);

($mem_info->{'country'}) = $db->selectrow_array('SELECT country FROM members WHERE id= ?', undef, $member_id);

LoadLangBlocks();

my @path_parts = split(/\//, $q->path_info);
shift @path_parts;  # the first element is empty because the path starts with a backslash

if ($params{'x'} && $params{'x'} eq 'startStripeCheckout')
{
	if ($params{'sfunc'} eq 'funding')
	{
		startStripeCheckout_Funding();
	}
	elsif ($params{'sfunc'} eq 'funding-confirm')
	{
                LoadStripeCheckout();
	}
}
elsif (@path_parts && $path_parts[0] eq 'Callback')
{
	if ($path_parts[1] eq 'ca-funding')
	{
		CaFunding();
	}
}

die "Unexpected logic branch: x:$params{'x'} sfunc:$params{'sfunc'}";

###### start of subroutines

=head3 CaFunding

	Called upon a return from the Stripe Checkout for CA funding.
	The module docs say these fields are available on the success method

Returns a hash with the following keys:

  Token
  TransactionID
  TransactionType
  PaymentType
  PaymentDate
  GrossAmount
  FeeAmount
  SettleAmount
  TaxAmount
  ExchangeRate
  PaymentStatus
  PendingReason
  BillingAgreementID (if BillingType 'MerchantInitiatedBilling'
                      was specified during SetExpressCheckout)
=cut

sub CaFunding
{
        my $process_state = $session->RetrieveState($params{'pk'}, $params{'pkc'}, 1);
        unless ($process_state)
        {
		$errors = $lang_blocks->{'something_went_wrong'};
		startStripeCheckout_Funding();
        }
        
	my $resp = $cspp->API->charges_create(
            'amount'    => $params{'amount'} * 100,
            'card'      => $params{'stripeToken'},
            'description'   => $params{'description'},
            'metadata[pk]'  => $params{'pk'},
            'metadata[pkc]'  => $params{'pkc'},
        );

	if (! $resp)
	{
		$errors = $lang_blocks->{'paypal_error_1' } . ' ' . $cspp->API->error->{'message'};
		startStripeCheckout_Funding();
	} else {
                $resp = $cspp->API->success;
        }

	if ($resp->{'status'} eq 'succeeded')	# could be "Completed" -or- "Completed-Funds-Held"
	{
            # at this point our pk & pkc params should match the metadata values we passed to Stripe
            # so also should the amount in the Stripe response match the value we passed to it
            # If these do not match, then that means that some tampering have taken place with params.
            # So abort.
                unless ($params{'pk'} == $resp->{'metadata'}{'pk'} && $params{'pkc'} eq $resp->{'metadata'}{'pkc'} && ($params{'amount'} * 100) == $resp->{'amount'})
                {
                        $errors = $lang_blocks->{'something_went_wrong'};
                        startStripeCheckout_Funding();
                }
                
		my ($trans_id) = $db->selectrow_array(q#SELECT nextval(('"soa_trans_id_seq"'::text)::regclass)#);
		unless ($trans_id)
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		my $rv = $db->do("
			INSERT INTO soa (trans_id, trans_type, id, amount, description, reconciled, memo)
			VALUES (
				$trans_id,
				$txnType,
				$member_id,
				?,
				?,
				TRUE,
				?
			)
			", undef, $params{'amount'}, "Stripe Trans ID: $resp->{'id'}", "Gross: $params{'amount'} - Payer: $resp->{'source'}{'name'} - Fee: " . $resp->{'application_fee'} || 0);
		
		unless ($rv && $rv eq '1')
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		print $q->redirect('https://www.clubshop.com/cgi/funding.cgi');
		Exit();
	}

=comment
	elsif ($resp{'PaymentStatus'} eq 'In-Progress')
	{
		$errors = "$lang_blocks->{'payment_in_progress'} $lang_blocks->{'try_reload'}";
		SimpleError();
	}
	elsif ($resp{'PaymentStatus'} eq 'Failed')
	{
		$errors = $lang_blocks->{'payment_failed'};
		startExpressCheckout_Funding();
	}
	elsif ($resp{'PaymentStatus'} eq 'Pending')
	{
		$errors = $lang_blocks->{'payment_pending'};
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Pending PayPal payment',
			'From' => 'PP.cgi@www.clubshop.com',
			'Message' => 'A PayPal funding request has come through as "Pending". Here are some details on the Checkout:' . "\n" . Dumper(\%CoDetails)
		);
		startExpressCheckout_Funding();
	}
=cut
	else
	{
		$errors = "Unexpected PaymentStatus: " . $cspp->API->success->{'status'};
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Unexpected PaymentStatus Stripe payment',
			'From' => 'Stripe.cgi@www.clubshop.com',
			'Message' => "A Stripe funding request has come through as ${$cspp->API->success->{'status'}}. Here are some details on the Checkout:\n" . Dumper($cspp->API->success)
		);
		startExpressCheckout_Funding();
	}
	# for PaymentStatus return values, see: https://stripe.com/docs/api
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub LoadLangBlocks
{
	$lang_blocks = $gs->Get_Object($db, $TMPL{'XML'}) || die "Failed to retrieve language pack template\n";
#	$tmp = decode_utf8($tmp);
	
	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

=head 3 LoadParams

	We are accepting certain parameters only and then usually for certain specific values. We will enforce this in here.
	
=cut

sub LoadParams
{
	# if the value is -0-, then we expect that named parameter but the value is freestyle
	my %Allowed = (
		'x' 		=> [qw# VerifyDeposit startStripeCheckout GetTransactionDetails Callback #],
		'dest'		=> [qw# /cgi/funding.cgi #],
		'sfunc'		=> [qw# funding funding-confirm ca-funding #],
		'id'		=> 0,
		'token'		=> 0,
		'amount'	=> 0,
		'stripeToken'	=> 0,
		'stripeTokenType' => [qw# card #],
		'stripeEmail'     => 0,
		'pk'		=> 0,
		'pkc'	        => 0
	);
	
	foreach my $pn ($q->param)
	{
		if (exists $Allowed{$pn})
		{
			if ($Allowed{$pn})	# if there are expected values in the map, make sure the value passed is in the list
			{
				$params{$pn} = $q->param($pn);
				die "Unrecognized parameter value: $pn: $params{$pn}" unless grep $_ eq $params{$pn}, @{$Allowed{$pn}};
			}
			else	# otherwise we can accept whatever value
			{
				$params{$pn} = $q->param($pn);
			}
		}
		else
		{
			# if a parameter comes in that we do not recognize, we will just skip it
		}
	}
}

sub LoadStripeCheckout
{
	unless (defined $params{'amount'} && isnum($params{'amount'}) && $params{'amount'} >= 10)
	{
		$errors = $lang_blocks->{'min_deposit_amount'};
		startStripeCheckout_Funding();
	}
	
	my $stripe_amount = $params{'amount'} * 100;   # per Stripes docs: All API requests expect amounts to be provided in a currency’s smallest unit. For example, to charge $10 USD, provide an amount value of 1000 (i.e, 1000 cents). 
	if (sprintf('%.0f', $stripe_amount) != $stripe_amount)    # crude check to ensure that we have no more than two digits after the decimal place
	{
		$errors = $lang_blocks->{'wrong_decimal_count'};
		startStripeCheckout_Funding();
	}

        my ($fee_rate) = $db->selectrow_array("SELECT fee_label FROM soa_fees WHERE trans_type= $txnType");
	my ($fee) = $db->selectrow_array("SELECT calculate_soa_fee($params{'amount'}, $txnType)");
	unless (defined $fee && looks_like_number($fee))
	{
		$errors = "$lang_blocks->{'notcalc'} $lang_blocks->{'try_again'}";
		startStripeCheckout_Funding();
	}
	
	my $total = $params{'amount'} + $fee;
	
	my $tmpl = $gs->GetObject($TMPL{'LoadStripeCheckout'}) || die "Failed to retrieve template: $TMPL{'LoadStripeCheckout'}";
        my %data = (
		'ME'			=> $q->url . '/Callback/ca-funding',
		'errors'		=> $errors,
		'lang_blocks'		=> $lang_blocks,
		'mem_info'              => $mem_info,
		'amount'                => $params{'amount'},
		'stripe_amount'		=> $stripe_amount,
		'fee'			=> sprintf('%.2f', $fee),
		'total'			=> sprintf('%.2f', $total),
		'publicKey'             => $cspp->{'creds'}{'publicKey'}
	);
	
	# now save the amount, fee and total so that when they come back, we can be sure that the values have not been tampered with
	($data{'pk'}, $data{'pkc'}) = $session->SaveState($params{'pk'}, $params{'pkc'}, {
		'amount'		=> $data{'amount'},
		'fee'			=> $data{'fee'},
		'total'			=> $data{'total'}
		});

	my $tmp = ();
	$TT->process(\$tmpl, \%data, \$tmp) || die $TT->error();
	print $q->header('-charset'=>'utf-8');
	print $GI->wrap({
			'title'=> $lang_blocks->{'title_ca'},
			'style'=> $q->style({'-type'=>'text/css'}, '.number { text-align:right; }'),
			'content'=>\$tmp
		});
	Exit();
}

sub SimpleError
{
	print $q->header('-charset'=>'utf-8'), $q->start_html, $q->div($errors), $q->end_html;
	Exit();
}

sub startStripeCheckout_Funding
{
	my $tmpl = $gs->GetObject($TMPL{'startStripeCheckout_Funding'}) || die "Failed to retrieve template: $TMPL{'startExpressCheckout_Funding'}";
	my ($fee_rate) = $db->selectrow_array("SELECT fee_label FROM soa_fees WHERE trans_type= $txnType");
	my %data = (
		'ME'		    => $q->url,
		'funding_fee_rate'  => $fee_rate,
		'errors'            => $errors,
		'lang_blocks'	    => $lang_blocks,
		'publicKey'         => $cspp->{creds}{publicKey}
	);
	
	my $tmp = ();
	$TT->process(\$tmpl, \%data, \$tmp) || die $TT->error();
	print $q->header('-charset'=>'utf-8');
	print $GI->wrap({
			'title'=> $lang_blocks->{'title_ca'},
			'style'=> $q->style({'-type'=>'text/css'}, '.number { text-align:right; }'),
#			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/CommStmnt.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$tmp
		});
	Exit();
}

__END__

