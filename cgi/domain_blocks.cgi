#!/usr/bin/perl -w
###### domain_blocks.cgi
###### a routine to view the mail_refused_domains table (domains that will not accept our mail)
###### this is for use by the public
###### last modified 12/23/02	Bill MacArthur

use strict;
use DBI;
use CGI;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);

$| = 1;   # do not buffer error messages
$" = '';

###### GLOBALS
my $q = new CGI; 
my ($db);
my $mrd_cnt = 0;			###### the number of blocked domains
my $num_of_cols = 4;			###### the number of columns in our output table
my $country = $q->param('c') || 'all';
my %TMPL = (	all => 300,
		US =>	301);
my %QUERY = (	all =>	'SELECT domain, 1 AS flagged FROM domains_blocked ORDER BY domain',
		US =>	"SELECT domain, contact_address FROM domains_blocked WHERE country= '$country' ORDER BY domain");

my @ELIST;

unless ($db = DB_Connect('domain_blocks')){exit}

print $q->header(-expires=>'now');

###### get our template object
my $sth = $db->prepare("SELECT obj_value FROM cgi_objects WHERE obj_id= $TMPL{$country}");
$sth->execute;
my $tmpl = $sth->fetchrow_array; $sth->finish;
unless ($tmpl){ &Err("Couldn't retrieve the template"); goto 'END' }

###### lets get our data
$sth = $db->prepare($QUERY{$country}); $sth->execute;
my $dat = $sth->fetchall_arrayref; $sth->finish;

my $time = scalar localtime();

if ($country eq 'all'){
	&Do_Page();
}
elsif ($country eq 'US'){
	&Do_Page_($country);
}


END:
$db->disconnect;
exit;

sub Do_Page_($){
	my $country = shift;
	my ($tbl, $tmp);
	my $rowcolor = '#ffffee';
	
        my $counter=0;

	foreach(@{$dat}){
		$tmp = $_;
		$tbl .= qq|<tr bgcolor=\"$rowcolor\">\n|;
		$tbl .= qq|<td class="d">$tmp->[0]</td>\n|;
		$tbl .= qq|<td class="c"><a href="mailto: $tmp->[1]">$tmp->[1]</a></td>\n|;
		$tbl .= "</tr>\n";
		$rowcolor = ($rowcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');

        $ELIST[$counter]= $tmp->[1] . " " . "<" . $tmp->[0] . ">" . "\r\n";
        $counter++;
	}

###### now let's output our page
	$tmpl =~ s/%%timestamp%%/$time/;
			$tmpl =~ s/%%tbl%%/$tbl/;
        $tmpl =~ s/%%ELIST%%/@ELIST/g;

	print $tmpl;

}

sub Do_Page{
###### output the page with our complete list of domains
	my ($x, $y, $tbl, $tmp);
	my $rowcolor = '#ffffee';

	$mrd_cnt = scalar @$dat;

###### instead of handling what would be blank columns at the end of table creation, we'll push empty
###### vals onto the end of the stack to give an evenly divisible number with the number of columns
	my $remainder = $mrd_cnt % $num_of_cols;
	if ($remainder){
		for ($x=0; $x < $num_of_cols - $remainder; $x++){ push (@$dat, ['&nbsp;', 1]) }
	}

###### we'll divide the total lines of dat by the number of cols to tell us the number of table rows
	my $new_cnt = scalar @$dat;
	for ($x=0; $x < $new_cnt/$num_of_cols; $x++){
		$tbl .= "<tr bgcolor=\"$rowcolor\">\n";
###### we'll insert the desired number of columns into each row
		for ($y=0; $y < $num_of_cols; $y++){
			$tmp = shift @$dat;
			$tbl .= $q->td({-class=>"d$tmp->[1]"}, $tmp->[0]);
		}
		$tbl .= "</tr>\n";
		$rowcolor = ($rowcolor eq '#ffffee') ? ('#eeeeee') : ('#ffffee');
	}

	$tbl = $q->table({-border=>1, -cellspacing=>0, -cellpadding=>4}, $tbl);

###### now let's output our page
	$tmpl =~ s/%%timestamp%%/$time/;
	$tmpl =~ s/%%count%%/$mrd_cnt/;
	$tmpl =~ s/%%tbl%%/$tbl/;
	print $tmpl;
}

sub Err{
	print $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $_[0], '<br />', scalar localtime();
	print $q->end_html();
}

###### 09/10/02 added handling for a parameter which will change the report to a custom listing of US blocks
###### 10/24/02 added a sort order to the listing by country
###### 10/20/02 added the textbox with the list of emails for mass emailing
###### 12/23/02 went to the 'use' style for global modules
