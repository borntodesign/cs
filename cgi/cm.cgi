#!/usr/bin/perl -w
# cm.cgi Choose Mall
# just a simple redirector for handling the "view your country's mall" links
# created: 11/20/09	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use DB_Connect;
my $q = new CGI;
my $c = $q->path_info || '';
$c =~ s#^/##;
die "No country code received" unless $c;

my $db = DB_Connect('generic') || exit;
my $rv = $db->selectrow_array('SELECT url FROM malls WHERE active=TRUE AND country_code=?', undef, $c);
$rv ||= 'http://www.clubshop.com/mall/US/';
print $q->redirect($rv);
$db->disconnect;
exit;