#!/usr/bin/perl -w
###### JumpStart
###### a simple application to 'enroll' a member in the 'Jump Start' program
###### created 05-19-04	Bill MacArthur
###### last modified: 03/30/10	Bill MacArthur
				
use strict;
use lib ('/home/httpd/cgi-lib');
use ParseMe;
use DB_Connect;	
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw(%MEMBER_TYPES $BUILDER_LEVEL);
use Mail::Sendmail;
		
my %TMPL = (
	main			=> 10306,
	enrollment_complete	=> 10307,
	upline_email		=> 10308
);
our $q = new CGI; 

# we don't do this anymore for now 09/29/10
print $q->redirect('http://www.glocalincome.com/errors/jsoff.html');
exit;

my $id = uc $q->param('a');
unless ($id)
{
	Err('Mising the ID parameter.');
	exit;
}
elsif ($id !~ /\D/)
{
	Err("Invalid ID received: $id");
	exit;
}	

my $db = DB_Connect::DB_Connect('JumpStart') || exit;
$db->{RaiseError} = 1;

unless (MemberCK())
{
	Err('Cannot find a match for the submitted ID');
	goto 'END'
}
elsif ($id->{membertype} ne 'm')
{
	Err("This program is only available to $MEMBER_TYPES{m}.
		Your membertype is: $MEMBER_TYPES{$id->{membertype}}");
}
elsif ($id->{mtr_test})
{
	if ($id->{mtr_test} eq 'WAITING')
	{
		Err('It appears that you are already signed up.');
	}
	else
	{
		Err('It appears that your upline has already begun building under you.');
	}
}
elsif (! $q->param('action'))
{
	###### present the main page
	my $tmpl = G_S::Get_Object($db, $TMPL{main}) ||
		die "Failed to load template: $TMPL{main}";

	$tmpl =~ s/%%action%%/$q->script_name/eg;
	ParseMe::ParseAll(\$tmpl, {member=>$id});
	print $q->header(-expires=>'now'), $tmpl;
}
elsif ($q->param('action') eq 'doit')
{
	my $rv = $db->do("
		INSERT INTO member_transfer_recipients (id, entered)
		VALUES ($id->{id}, NOW()::DATE + INTERVAL '1 YEAR');

		INSERT INTO activity_rpt_data (id, spid, activity_type)
		VALUES ($id->{id}, $id->{spid}, 40);
	");
	unless ($rv eq '1')
	{
		Err("We're sorry, there has been an error while trying to enroll you.
			<br />Please refer to your <a href=\"/cgi-bin/ppreport.cgi?a=$id->{alias}\">Personal Report</a>
			and contact the upline listed there telling them you want to participate.
			<br />Thank you.");
	}
	else
	{
		if ( my ($rv, $vip, $builder) = SendMail() )
		{
			my $tmpl = G_S::Get_Object($db, $TMPL{enrollment_complete}) ||
				die "Failed to load template: $TMPL{enrollment_complete}";

			ParseMe::ParseAll(\$tmpl, {
				member=>$id,
				vip=>$vip,
				builder=>$builder
			});
			print $q->header(-expires=>'now'), $tmpl;
		}
	}
}
else
{
	Err('Invalid action');
}

END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff', -title=>'Error'),
		$_[0],
		'<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>',
		$q->end_html();
}

sub MemberCK
{
	###### check to see if we have a member record and get a few particulars
	my $sth = $db->prepare("
		SELECT m.id, m.spid, alias, firstname, lastname, membertype,
			COALESCE(emailaddress, '') AS emailaddress,
			COALESCE(phone, '') AS phone,
		--	COALESCE(phone_ac, '') AS area_code,
			CASE
				WHEN mtr.entered > NOW()::DATE THEN 'WAITING'::TEXT
				WHEN mtr.entered <= NOW()::DATE THEN 'IN'::TEXT
				ELSE ''
			END AS mtr_test
		FROM members m
		LEFT JOIN member_transfer_recipients mtr
		ON mtr.id=m.id
		WHERE m.alias= ?");
	$sth->execute($id);
	$id = $sth->fetchrow_hashref;
	$sth->finish;
	return ($id->{id}) ? 1 : 0;
}

sub SendMail
{
	my $tmpl = G_S::Get_Object($db, $TMPL{upline_email}) ||
		die "Failed to load template: $TMPL{upline_email}";

	my $vip = G_S::Get_VIP_Sponsor($db, $id->{spid});
	unless ($vip->{id})
	{
		Err('Failed to find VIP sponsor');
		return;
	}

	###### now we'll check for an upline Builder starting with the VIP
	my $sth = $db->prepare("
		SELECT m.id,
			m.spid,
			m.firstname,
			m.lastname,
			m.emailaddress,
			m.membertype,
			COALESCE(nbt.level, 0) AS level
		FROM 	members m
		LEFT JOIN nbteam nbt
		ON 	nbt.id=m.id
		WHERE 	m.id= ?");

	my $tmp = {spid=>$vip->{id}};
	my $refid = $vip->{id};
	do
	{
		$sth->execute($tmp->{spid});
		$tmp = $sth->fetchrow_hashref;
		unless ($tmp->{spid})
		{
			Err('Upline search failed while looking for Builder.');
			return;
		}
		elsif ($tmp->{spid} eq $refid)
		{
			Err("Loop detected on: $refid");
			return;
		}
	} while ($tmp->{id} > 1 && $tmp->{level} < $BUILDER_LEVEL);
	$sth->finish;

	###### at this point, our $tmp is our Builder
	###### if he is one and the same, then we'll zero it out so as not to duplicate
	###### the TO: & the CC:
	# $tmp = {} if $tmp->{id} == $vip->{id};

	###### we'll pass the extra flag to squash the extra placeholders
	ParseMe::ParseAll(\$tmpl, {
		member	=> $id,
		vip	=> $vip,
		builder=> $tmp
	}, 1);
	my $hdr = ParseMe::Grab_Headers(\$tmpl);

	my %mail = ( Message=>$tmpl );
	foreach (keys %{$hdr}){ $mail{$_} = $hdr->{$_} }

	my $rv = sendmail(%mail);
	return (1, $vip, $tmp) if $rv;

	Err("Error notifying upline:<br />$Mail::Sendmail::error");
	return;	
}

###### 05/24/04 added passing of the vip and bldr from sendmailback to the main routine
###### for insertion into the HTML template				
###### 05/27/04 made the future date exactly 1 year ahead so as to make it useful for analyzing
###### stagnation
# 03/30/10 added the activity entry