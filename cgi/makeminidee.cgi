#!/usr/bin/perl -w

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use CGI ':all';
use DB_Connect;
use minidee;
binmode STDOUT, ":encoding(utf8)";

my $q = new CGI;
$q->charset('utf-8');
my $gs = new G_S;
my $db = '';
my $member_id = '';
my $mem_info = '';
my $login_type = '';

# Get the member Login ID and info hash from the VIP cookie.

( $member_id, $mem_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

if ( $member_id && $member_id !~ m/\D/ ) {
    $login_type = 'auth';
}
else {
    $member_id = $q->cookie('id');
}
unless ($db = DB_Connect('generic')) {exit}
$db->{'RaiseError'} = 1;
my $minidee = minidee::get_data($member_id);
my $cookie = minidee::create_cookie($minidee);
my $combined = minidee::combine_fields($minidee);
print $q->header('-cookie'=>$cookie, '-type'=>'text/javascript', '-expires'=>'now', '-Pragma'=>'no-cache');
print "var curtag = '$combined';\n";
print "var splittag = curtag.split('|');\n";




