#!/usr/bin/perl -w
# LoginMerchants.cgi
# started as a direct copy of LoginMembers.cgi
# created: 09/17/13	Bill MacArthur
# last modified: 06/11/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
#use Data::Dumper;	# for debugging only
use Login::Login qw(%errors_map);
require Login::csmLogin;
binmode STDOUT, ":encoding(utf8)";

my $cgi = new CGI;
my $gs = G_S->new();
my ($params, $tmpl, $language_pack, $member);

my %TMPL = (
	'default_login' 		=> 547,		# default login form
	'default_success'		=> 544		# this renders a simple window with a javascript that causes the top window to load the destination URL
);

my $csL = Login::csmLogin->new({'cgi'=>$cgi, 'gs'=>$gs});
$gs->db( $csL->db );


$params = $csL->LoadParams();

$language_pack = $csL->LoadLanguagePack();
$gs->Prepare_UTF8($language_pack, 'encode');

unless ($params->{'_action'})
{
	# we may have received an error code and an error string
	# for now, we will just place our translated error if we got one
	$csL->errors( $language_pack->{ $errors_map{$params->{'err'}} } ) if $params->{'err'} && $errors_map{ $params->{'err'} };
	LoginForm();
}
elsif ($params->{'_action'} eq 'login')
{
	Login();
}
#elsif ($params->{'_action'} eq 'ckonly')
#{
#	CookieOnly();
#}
elsif ($params->{'_action'} eq 'logout')
{
	$csL->Logout();
}
else
{
	$csL->Error("Unrecognized _action parameter: $params->{'_action'}");
}

$csL->Exit();

###### ######

sub CreateCookies
{
	my $member = shift;
	$csL->setHeaderCookie( $csL->Generate_AuthCustom_maf_Cookie( $member ) );
	$csL->setHeaderCookie( $csL->Generate_AuthCustom_Generic_Cookie( $member->{'ma_membership'} ) );
	$csL->setPnsidCookie($member->{'ma_membership'}->{'id'});
	$csL->setHeaderCookie( $cgi->cookie('-name'=>'country', '-value'=>$member->{'ma_membership'}->{'country'}, '-expires'=>'1y', '-domain'=>'.clubshop.com', '-path'=>'/') );
	my $e = $csL->setHeaderCookie( $cgi->cookie('-name'=>'id', '-value'=>$member->{'ma_membership'}->{'id'}, '-expires'=>'1y', '-domain'=>'.clubshop.com', '-path'=>'/') );
#	warn Dumper( $e);
}

sub Login
{
	if (! $params->{'username'})
	{
		$csL->errors( $language_pack->{'nousername'} );
	}
	
	if (! $params->{'password'})
	{
		$csL->errors( $language_pack->{'nopwd'} );
	}

	if (! $params->{'merchantid'})
	{
		$csL->errors( $language_pack->{'err_no_mid'} );
	}
	elsif ($params->{'merchantid'} =~ m/\D/)	# the merchant ID is numeric only
	{
		$csL->errors( $language_pack->{'merchid_not_mid'} );
		$csL->errors( $language_pack->{'merchid_is_int'} );
		$csL->errors( $language_pack->{'merchid_refer'} );
	}

	if (! $params->{'masterbranch'})
	{
		$csL->errors( $language_pack->{'err_no_merch_type'} );
	}

	if ($csL->errors)
	{
		LoginForm();	# LoginForm() exits when done
	}
	else
	{
		my $error = ();
		($member, $error) =  $csL->authenticate($params->{'username'}, $params->{'password'}, $params->{'merchantid'}, $params->{'masterbranch'});

		if ($error)
		{
			$csL->errors( $language_pack->{ $errors_map{$error} } || "Error code: $error : $errors_map{$error}" );
			
			LoginForm();
		}

		CreateCookies($member);
		
		# location only logins will not have an associated membership login
		if ($member->{'ma_membership'}->{'id'})
		{
			$csL->db->do("INSERT INTO membership_logins (id, ipaddr) VALUES (?,?)", undef, $member->{'id'}, $cgi->remote_addr);
		}

		$csL->Display($TMPL{'default_success'}, $member);
	}
}

=head3 LoginForm

Render a login form.
We will use the default login form template unless we have a valid formID HTTP parameter

=cut

sub LoginForm
{
	$csL->NegateCookies() if $csL->errors;
	$csL->Display($TMPL{$params->{'formID'} || 'default_login'});
	$csL->Exit();
}

__END__

Change log:

05/11/15	Added the binmode on stdout
06/11/15	Added the pnsid cookie creator in CreateCookies()