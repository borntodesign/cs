#!/usr/bin/perl -w
# FeesForMeJS.cgi started as an exact copy of transposeMEvarsJS.cgi 01/17/13
# to provide a bit of javascript for the country/currency specific pricing of certain GPS fees
# this will create the vars needed... see the end for the format
# last modified: 

=head3

	The logic behind this little script is to determine the language preference of the user,
	The country of the user and the currency of that country.
	We will start by looking at a full login cookie if available.
	If that is not present, then a simple ID cookie.
	If not that then we will do an IP address lookup to see what country.
	
=cut

use strict;
use lib '/home/httpd/cgi-lib';
use CGI;
use DB_Connect;
use G_S;

my $cgi = new CGI;
my $gs = new G_S;
my $db = DB_Connect::DB_Connect('generic') || exit;

my ($country, $currcode, $lang_pref) = ();
my $ipaddr = $cgi->remote_addr;
my ($id, $meminfo) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic'));

# we don't expect to use this, but you never know what might happen
# ... that did not take long... we need to coerce the values in cases where other parties are looking at something that needs to remain "static" in it's representation
$id = $cgi->param('id');

###### a valid id will not have anything besides digits, so let's try the ID cookie
$id ||= $cgi->cookie('id') if (! $id) || ($id !~ /\D/);

# this should be a simple number
$id = '' if $id =~ /\D/;

# if we have a cookie, then lets lookup our country
($country) =  $db->selectrow_array('SELECT country FROM members WHERE id=?', undef, $id) if $id;

if ((! $country) && $ipaddr)
{
	($country) = $db->selectrow_array('
		SELECT countryshort FROM ip_latlong_lookup WHERE ?::inet BETWEEN inet_from AND inet_to', undef, $ipaddr);
}

if ($country)
{
	($currcode) = $db->selectrow_array('SELECT currency_code FROM currency_by_country WHERE country_code=?', undef, $country);
}

# set defaults if we are still empty
$country ||= 'US';
$currcode ||= 'USD';
$lang_pref = $gs->Get_LangPref() || 'en';
my $subpricing = $db->selectall_hashref("
	SELECT subscription_type, amount, currency_code
	FROM subscription_pricing_current
	WHERE subscription_type IN (2,40,41,48,49)
	AND currency_code='$currcode'", 'subscription_type');

print $cgi->header('-charset'=>'utf-8', '-type'=>'text/javascript'),
	qq|
		var my_language_code = '$lang_pref';
		var my_currency_code = '$currcode';
		var my_country_code = '$country';
	
		var pricing = new Object;
		|;
			
print "pricing = {
		basic_initial: " . sprintf('%.2f', ($subpricing->{2}->{'amount'} + $subpricing->{40}->{'amount'} + $subpricing->{41}->{'amount'})) . ",
		basic_monthly: " . sprintf('%.2f', $subpricing->{41}->{'amount'}) . ",
		basic_plus_initial: " . sprintf('%.2f', ($subpricing->{2}->{'amount'} + $subpricing->{48}->{'amount'} + $subpricing->{49}->{'amount'})) . ",
		basic_plus_monthly: " . sprintf('%.2f', $subpricing->{49}->{'amount'}) . '};';

$db->disconnect;
exit;

__END__

<script type="text/javascript">
	var my_language_code = 'ie';
	var my_currency_code = 'VND';
	var my_country_code = 'FR';
</script>


Change Log:

04/10/12	Moved the id parameter evaluation ahead of everything so that this thing can be coerced based upon that value
