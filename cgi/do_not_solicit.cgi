#!/usr/bin/perl -w
###### do_not_solicit.cgi
######
###### A script that inserts an email address in the do_not_solicit table.
######
###### created: 08/18/05	Karl Kohrt
######
###### modified: 09/15/05 Karl Kohrt
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DBI;
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use Email::Valid;
use Solicit;

###### CONSTANTS

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our $xs = new XML::Simple(NoAttr=>1);
our %TMPL = (	content	=> 10474,
		submit		=> 10475,
		search		=> 10486,
		list		=> 10487
		);
our $db = '';
our $xml = '';
our $action = $q->param('action') || '';			# Get the action if exists.
our $emailaddress = $q->param('emailaddress') || '';	# Get the email if one was submitted.
our $operator = $q->cookie('operator') || '';		# Get the admin user if exists.
our $notes = '';
our $messages = '';
our $do_mail = '';
our $dont_mail = '';
our $do_count = 0;

################################
###### MAIN starts here.
################################

unless ($db = DB_Connect('do_not_solicit.cgi')) {exit}
$db->{RaiseError} = 1;

if ($emailaddress)
{
	$action = "submit";
	# If it's a valid email address.
	if ( Email::Valid->address($emailaddress) )
	{
		# Include the user's IP address in the record and the operator if we have one..
		$notes .= "$operator: " if $operator;
		$notes .= $q->remote_addr;

		my $stamp = '';
		$stamp = Solicit::Do_Not_Solicit($db, $emailaddress, $notes);

		# If we didn't get a stamp back, we created a new record.
		unless ( $stamp )
		{					
			$messages .= qq#\n	<span class="good">
						<xsl:text>'</xsl:text>$emailaddress<xsl:text>' </xsl:text>
						<xsl:value-of select="//messages/success" />
						</span><br/>\n#;
		}
		# This is where the email already exists in the list.
		elsif ( $stamp && $stamp != -1 )
		{
			$messages .= qq#\n<span class="good">
					<xsl:text>'</xsl:text>$emailaddress<xsl:text>' </xsl:text>
					<xsl:value-of select="//messages/already" />
					<xsl:text> </xsl:text>$stamp<xsl:text>.</xsl:text>
					<br/>\n
					<xsl:value-of select="//messages/error" />
					</span><br/>\n#;
		}
		# This is a database failure.
		else
		{
			$messages .= qq#\n<span class="bad">
					<xsl:value-of select="//messages/failure" />
					<xsl:value-of select="//messages/error" />
					</span><br/>\n#;
		}
	}
	# If it's an invalid email address.
	else
	{					
		$messages .= qq#\n	<span class="bad">
					<xsl:text>'</xsl:text>$emailaddress<xsl:text>' </xsl:text>
					<xsl:value-of select="//messages/invalid" />
					<br/><xsl:value-of select="//messages/invalid2" />
					</span><br/>\n#;
	}
}
elsif ( $action eq 'list' )
{
	List();
}
# If no valid action submitted, default to the email submission page.
elsif ( ! $action )
{
	$action = "submit";
}

Display_Page($action);

$db->disconnect if $db;
exit(0);

################################
###### Subroutines starts here.
################################

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $pagename = shift;
	my $xsl = $gs->Get_Object($db, $TMPL{$pagename}) ||
		die "Failed to load XSL template: $TMPL{$pagename}\n";

	my $xml = $gs->Get_Object($db, $TMPL{content}) ||
		die "Failed to load template: $TMPL{content}\n";

	if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
	else
	{
		if ( $messages )
		{
			$messages = qq#<div align="center"><hr width="30%"/>
					$messages
					<hr width="30%"/></div>#;
		}
		$xsl =~ s/%%messages%%/$messages/g;
		$xsl =~ s/%%ME%%/$ME/g;
		$xsl =~ s/%%do_mail%%/$do_mail/g;
		$xsl =~ s/%%do_count%%/$do_count/g;
		$xsl =~ s/%%dont_mail%%/$dont_mail/g;

		print $q->header(-charset=>'utf-8'), XML::local_utils::xslt($xsl, $xml);
	}
}

##### Lists email addresses that can and can't(in the DNS list) be emailed.
sub List
{
	# Split out the email addresses into an array.	
	my @email_list = split /\n/, $q->param('list') || '';
	my $sth = $db->prepare("	SELECT emailaddress
					FROM 	do_not_solicit
					WHERE 	emailaddress = ?;");
	foreach ( @email_list )
	{
		# Get rid of all whitespace including windows return chars.
		$_ =~ s/\s//g;
		my $exists = $sth->execute($_);
		if ( $exists == 1 ) 	{ $dont_mail .= "$_\n"; }
		else 			{ $do_mail .= "$_\n"; $do_count++; }
	}
	if ( $do_mail ) { chop $do_mail; }
	if ( $dont_mail ) { chop $dont_mail; }
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}


###### 09/15/05 Added the search and list forms along with the List subroutine to give 
######		the members the ability to search for email addresses in the DNS list.