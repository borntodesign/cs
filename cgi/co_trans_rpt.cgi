#!/usr/bin/perl -w
###### co_trans_rpt.cgi
######
###### A report to show Charitable Org. donations.
######
###### created: 07/18/05	Karl Kohrt
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use DBI;
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);

###### CONSTANTS
our @MEM_TYPES = "s|m|v"; 	# Membertypes that can make a donation.

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our $nom_script = "/cgi/tmp/co_nomination.cgi";
our $don_script = "/cgi/tmp/co_donate.cgi";
our $xs = new XML::Simple(NoAttr=>1);
our %TMPL = (
	xsl		=> 10465,
	content	=> 10394
);

our $db = '';
our $member_id = '';
our $mem_info = '';
our $alias = '';
our $admin = 0;
our $login_error = 0;
our $qualified = '';
our $errors = '';
our $message = '';

# The ID of the VIP to be displayed, if not the VIP's cookie value.
our $report_id = $q->param('report_id') || '';
# The Charitable Org. type if it is a CO.
our $co_type = $q->param('co_type') || '';
# The report start date if one is provided.
our $start = $q->param('start') || '';
# The report end date if one is provided.
our $end = $q->param('end') || '';

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

# Continue if we have a good login/cookie.
unless ( $login_error )
{
	our $content = $gs->Get_Object($db, $TMPL{content}) ||
		die "Failed to load template: $TMPL{content}\n";
	# If this is an admin login, use the member ID from the cookie.
	# We can also use this block for Upline VIP access by checking report_id in downline.
	if ( $admin )
	{
		# If there's no report ID submitted.
		unless ( $report_id )
		{
			# Display a search form.
			Display_Search();
		}
		# Otherwise, get the members's info that would otherwise have been in the cookie.
		else
		{
			$mem_info = $db->selectrow_hashref("	SELECT id,
										alias,
										emailaddress,
										firstname,
										lastname,
										spid,
										membertype
									FROM 	members
									WHERE 	id = $report_id");
		}
	}
	# If not an admin login, use the member's id found in the cookie.
	else
	{
		$report_id = $member_id;	
		# Get the members's info if we don't have it already.
		unless ( $mem_info )
		{
			$mem_info = $db->selectrow_hashref("	SELECT id,
										alias,
										emailaddress,
										firstname,
										lastname,
										spid,
										membertype
									FROM 	members
									WHERE 	id = $report_id");
		}
	}
	# Work on the report if we have an ID.
	if ( $report_id )
	{
		# Get the transactions.
		my $qry = Build_Query();
		my $transactions = $db->selectall_hashref($qry, 'trans_code') || '';

		# utf-8ize our meminfo
		$gs->ConvHashref('utf8', $mem_info);
		# prepare our XML
		my $xml= "<base>\n"
			. $content
			. $xs->XMLout($mem_info, RootName=>'user')
			. $xs->XMLout($transactions, RootName=>'data')
			. "\n</base>\n";

		if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
		else
		{
			my $xsl = $gs->Get_Object($db, $TMPL{xsl}) ||
				die "Failed to load XSL template: $TMPL{xsl}\n";
			$xsl =~ s/%%ME%%/$ME/g;
			$xsl =~ s/%%nom_script%%/$nom_script/g;
			$xsl =~ s/%%don_script%%/$don_script/g;
			print $q->header(-charset=>'utf-8'), XML::local_utils::xslt($xsl, $xml);
		}
	}
}
$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

###### Build the transaction query.
sub Build_Query
{
	my $qry = "	SELECT 'T' || c.trans_id AS trans_code, c.*, l.co_name AS description
			FROM 	co_trans c
				INNER JOIN co_list_view l
				ON c.type = l.type
				AND c.type_id = l.type_id WHERE ";

	# Is this a donating member...
	if ( $mem_info->{membertype} && (grep { /$mem_info->{membertype}/ } @MEM_TYPES ))
	{
		$qry .= " c.member_id = $report_id";
	}
	# ...Or is this a Charitable Org.
	else
	{
		$co_type = 'ag' unless ( $co_type );
		$qry .= " c.type_id = $report_id AND c.type = '$co_type'";
		$mem_info->{co_name} = $db->selectrow_array(" 	SELECT co_name
									FROM 	co_list_view
									WHERE 	type = '$co_type'
									AND 	type_id = $report_id");
	}

	# If we were given a start date, add it.
	if ( $start )
	{
		$qry .= " AND trans_date >= '$start'";
		$mem_info->{start} = $start;
	}
	# If we were given an end date, add it.
	if ( $end )
	{
		$qry .= " AND trans_date <= '$end'";
		$mem_info->{end} = $end;
	}
	return $qry;
}

###### Display the search page.
sub Display_Search
{
	print $q->header(-expires=>'now');
	print 'No Admin Search page is available at this time.<br>' . $message;				
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'co_trans_rpt.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}
		# Mark this as an admin user.
		$admin = 1;
	}
	# If this is a member.		 
	else
	{
		unless ($db = DB_Connect( 'co_trans_rpt.cgi' )){exit}
		$db->{RaiseError} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id )
		{
			# Assign the info found in the VIP cookie.
			$alias = $mem_info->{alias};
		}
#		# If we didn't get a member ID, we'll check for a ppreport cookie.
#		else
#		{
#			$member_id = G_S::PreProcess_Input($q->cookie('id')) || '';
#			$alias = $db->selectrow_array("SELECT alias
#								FROM 	members
#								WHERE id = $member_id") if $member_id;
#			# If we found an alias, they must be a member.
#			if ( $alias ) { $mem_info->{membertype} = "m" };
#		}					
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			$login_error = 1;
		}
	}
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	print $q->redirect( $LOGIN_URL . "?destination=" .$ME );
}








