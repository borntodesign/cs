#!/usr/bin/perl -w
###### vendorList
###### crank out a list of vendors to drive the mall pages
###### created: April '08	Bill MacArthur
###### last modified:	05/01/08	Bill MacArthur

use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use Apache::Request;

our $NEWDAYS = 30;	###### the number of days a vendor will be consider New after being entered
our %param = ();
our $r = shift;

our $rv = ();
if ( $rv = LoadParams() )
{
	Err($rv);
	&Apache::exit;
}
our $db = DB_Connect('vendorList', undef, {return_redirects=>1});
$db->{RaiseError} = 1;

# we'll get textual URls back with the invocation method above, so we can test
# for a valid DB handle
if (! ref $db)
{
	Err("<error>$db</error>");
	$db = ();
}
else{
#	$db->{RaiseError} = 1;
 	_default_handler();
}
$db->disconnect;
&Apache::exit;

###### ###### ###### END of MAIN PROGRAM ###### ###### ######

sub ampCK {
	my $amp = shift;
	return $amp if $amp eq '&amp;';
	$amp =~ s/&/&amp;/;
	return $amp;
}

sub Err
{
	$r->send_http_header('text/html');
	$r->print("<error>$_[0]</error>");
}

sub EscapeEntities {
	my $arg = shift;
	$arg =~ s/&/&amp;/g;;
	return $arg;
}

sub LoadParams
{
	%param = ();
	my $rq = Apache::Request->new($r);
	###### we are only expecting these arguments
	my @expected = qw/mall_id catid alpha_range/;
	foreach(@expected){
		$param{$_} = $rq->param($_);
	}
	return ('bad or missing mall_id param:' .  $param{mall_id} || '')
		if (! $param{mall_id}) or $param{mall_id} =~ /\D/;
	###### we should never receive both a catid and alpha_range, so we'll check for one or the other
	if (defined $param{catid} && $param{catid} gt ''){
		return "bad catid param: $param{catid}" if $param{catid} =~ /\D/;
	}
	elsif (defined $param{alpha_range}){
		return "bad alpha_range param: $param{alpha_range}" if $param{alpha_range} !~ /^\w+-\w+$/;
	}
	else{
		return 'missing catid or alpha_range param';
	}
	return undef;
}

sub _default_handler
{
	my $catname = '';
	my @qry_args = $param{mall_id};
	my $qry = "
		SELECT v.vendor_name, v.vendor_id,
			COALESCE(mv.pp_range_lower,v.pp_range_lower) AS pp_range_lower,
			COALESCE(mv.pp_range_upper,v.pp_range_upper) AS pp_range_upper,
			COALESCE(mv.vrp_range_lower,v.vrp_range_lower) AS vrp_range_lower,
			COALESCE(mv.vrp_range_upper,v.vrp_range_upper) AS vrp_range_upper,
			mv.flag_sale AS sale, mv.flag_coupon AS code, mv.flag_payout_details AS payout_details,
			flag_shipping_incentive AS shipping_incentive, languages, v.primary_mallcat,
			CASE WHEN mv.rp_increase_expiration >= NOW()::DATE THEN TRUE ELSE FALSE END AS reward_increase,
			CASE WHEN entered < NOW()::DATE - $NEWDAYS THEN FALSE ELSE TRUE END AS new,
			mv.gift_cert_url\n";
	###### we only need this value when doing alpha pages
	$qry .= ', COALESCE(mct.catname,mc.catname) AS primary_mallcat_text ' if $param{alpha_range};
	$qry .= 'FROM mall_vendors mv
		INNER JOIN vendors_plus_ranges_plus_mp v ON v.vendor_id=mv.vendor_id
		AND v.status>0 AND mv.active=TRUE
		AND mv.mall_id= ? ';

	###### we are only expecting one or the other
	if (defined $param{catid}){
		$qry .= 'INNER JOIN vendor_categories vc ON vc.vendor_id=v.vendor_id AND vc.catid= ?';
		push @qry_args, $param{catid};
	
		###### now get our category name to shovel back to the caller along with our list
		($catname) = $db->selectrow_array('
			SELECT COALESCE(mct.catname,mc.catname)
			FROM mallcats mc
			LEFT JOIN mallcats_translated mct ON mc.catid=mct.catid
				AND mct.language_code= (SELECT language_code FROM malls WHERE mall_id= ?)
			WHERE mc.catid= ?
			', undef, $param{mall_id}, $param{catid});
		$catname = EscapeEntities($catname);
	}
	else{
		###### we should already have validated a correctly formed parameter
		my ($b,$e) = split /-/, $param{alpha_range};
		push @qry_args, ($b, $e);
		###### we need our primary category name AND the primary_mallcat may be NULL
		$qry .= "LEFT JOIN mallcats mc ON v.primary_mallcat=mc.catid
			INNER JOIN malls ON mv.mall_id=malls.mall_id
			LEFT JOIN mallcats_translated mct ON mct.catid=v.primary_mallcat
				AND mct.language_code= malls.language_code
			WHERE UPPER(SUBSTR(v.vendor_name,1,1)) BETWEEN ? AND ?";
	}

	$qry .= ' ORDER BY v.vendor_name';

#Err("$qry $qry_args[0],$qry_args[1], $qry_args[2]");return;

	my $sth = $db->prepare($qry);
	if ($DBI::errstr){
		Err($DBI::errstr);
		return;
	}
	$sth->execute(@qry_args);

	my $rv = '';
	while (my $tmp = $sth->fetchrow_hashref){
		$rv .= '<v fl="' . (uc substr($tmp->{vendor_name},0,1)) . '"';
	###### we will handle the boolean values differently as they will come out as -0- when false
		foreach (qw/sale code reward_increase new shipping_incentive payout_details/)
		{
			next unless $tmp->{$_};
			$rv .= qq| $_="$tmp->{$_}"|;
		}
		foreach (qw/vendor_id pp_range_lower pp_range_upper primary_mallcat
			vrp_range_lower vrp_range_upper/)
		{
			next unless defined $tmp->{$_};
			$rv .= qq| $_="$tmp->{$_}"|;
		}
		$rv .= '><vendor_name>' . EscapeEntities($tmp->{vendor_name}) . '</vendor_name>';
		$rv .= '<primary_mallcat_text>' . EscapeEntities($tmp->{primary_mallcat_text}) . '</primary_mallcat_text>'
			if $tmp->{primary_mallcat_text};
		foreach (qw/gift_cert_url languages/){
			next unless defined $tmp->{$_};
			if (/url$/){
				# take care of unescaped ampersands in URLs
				$tmp->{$_} =~ s/(&.{0,4})/ampCK($1)/eg;
				$rv .= "<$_>$tmp->{$_}</$_>\n";
			} else {
				$rv .= qq|<$_>| . EscapeEntities($tmp->{$_}) . qq|</$_>|;
			}
		}
		$rv .= "</v>\n";
	}

	$r->send_http_header('text/xml');
	$r->print("<root><vendors>$rv</vendors><catname>$catname</catname></root>");
}

# 05/01/08 removed handling for the text_slots
