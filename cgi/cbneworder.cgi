#!/usr/bin/perl -w
###### cbneworder.cgi
###### Written: 04/21/2003	Karl Kohrt
######
###### This script will request MAXCARDSPERPAGE new ClubBucks ID numbers from the database, or
###### MAXCARDSPERPAGE-1 if a card has been newly assigned to the member as his/her personal card, 
###### update the database appropriately, and then send the CBID numbers on to the 
###### cbcards.cgi script for display/printing.
######
###### Input:
######	cbmycards.cgi/<md5_hex($salt $cbid_list)>/
######		-- Requests MAXCARDSPERPAGE new class 2 distribution cards.
###### 	cbmycards.cgi/<md5_hex($salt $cbid_list)>/[comma separated values list]
######		-- Displays class 4 CBID's passed via the CSV list ($cbid_list).
######		and requests new class 2 distribution cards to fill the page.
######	cbmycards.cgi/full_page/
######		-- Displays a full page of new CBID's.
######	cbmycards.cgi/one_card/
######		-- Displays a single new card, front and back on the same page.
######
###### Last modified: 03/15/2004	Karl Kohrt
######

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S qw( $LOGIN_URL );
use DB_Connect;


###### CONSTANTS
# Maxinum number of cards per printed page.
use constant MAXCARDSPERPAGE => 10;		

# Maxinum number of cards per member.
use constant MAXCARDSPERMEMBER => 2000;		

# The path to the cbcards.cgi script.
use constant CBCARDS_CGI_PATH => 'https://www.clubshop.com/cgi/cbcards.cgi';

###### GLOBALS
our %TMPL = (	NoneAvail 	=> 10148,
		Salt		=> 96);		
our $q = new CGI;
our $db = ();
our $cbid_list = '';
our $md5h = '';
our ($UID, $meminfo) = ();
our @cbids = ();
our $referral_id = '';
our $card_front_code = 4;

###### LOCALS
my ($sth, $path, $tmpl) = ();
my $num_p_cbids = 0;		# How many personal CBID's were passed in to this script.
my $cards_avail = 0;		# How many cards the member has available.
my $salt = '';

##########################################
###### MAIN program begins here.
##########################################

our %cookies = ();		# these are the cookies passed
foreach ($q->cookie()){
	$cookies{$_} = $q->cookie($_);
}



###### validate the user
( $UID, $meminfo ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if we decide to exclude access based on an ACCESS DENIED password, we'll do it here

###### if the UID has any non-digits it means that its bad/expired whatever
if (!$UID || $UID =~ /\D/){ &RedirectLogin(); exit }

# DB_Connect does its own error handling, so we only need exit if it doesn't work
unless ($db = DB_Connect('cbneworder.cgi')){exit}
$salt = &G_S::Get_Object($db, $TMPL{'Salt'}) || die "Failed to load template: $TMPL{'Salt'}";

# If a VIP, Member, or Affinity Group process the order.
if (($meminfo->{membertype} =~ m/v/ ) || ($meminfo->{membertype} eq 'm') || ($meminfo->{membertype} eq 'ag') || ($meminfo->{membertype} eq 'ma'))
{
	# Get the currently logged in member ID.
	$referral_id = $UID;

	# Get rid of the leading forward slash.
	($path = $q->path_info()) =~ s/^\///;

	# Split out the hash value and the personal CBID number list.	
	($md5h, $cbid_list) =  split /\//, $path;

	# If there is a hash value, we must have been passed personal CBID's.
	if ($md5h)
	{
		# Initialize $cbid_list to suppress errors in case this is a full_page request.
		$cbid_list = $cbid_list || '';

		# We either have a good hash value passed or a request for a full page.
		if (($md5h eq md5_hex("$salt $cbid_list")) || ($md5h eq 'full_page') || ($md5h eq 'one_card'))
		{
			# Split up the csv list and count the personal CBIDs.		
			@cbids = split /\,/, $cbid_list, 10;
			$num_p_cbids = @cbids;

			# Test if requested number of cards are available for this member.
			$cards_avail = Check_Limit(MAXCARDSPERPAGE-$num_p_cbids);

			if ($cards_avail > 0)
			{
				# If this is the simple, single folding card.
				if ($md5h eq 'one_card')
				{
					# Get the number of cards specified.
					$cards_avail = 1;
				}
				else
				{
					# Get enough new CBID's to fill the page.
					$cards_avail = MAXCARDSPERPAGE-$num_p_cbids;
				}
			}
		}
		# If a properly formatted hash, one card request or a full page request is not present, 
		#	then error out and exit.
		else
		{
			&Err("Corrupted data, try again. -ref:cbneworder.cgi-");
			exit;
		}			
	}
	# If the script was called with no parameters, kick the hacker out.	
	else
	{
		&Err("You can't call this script without the proper parameters. -ref:cbneworder.cgi-");
		exit;
	}


	#If cards are available to the member, or personal CBID's were passed.				
	if (($cards_avail > 0) || ($cbid_list ne '')){

		# If we were passed 10 personal CBID's (or CBID's and java flags for display).
		if ( $num_p_cbids == 10){
			Preview();
		}	
		# If we need more CBID's to fill the sheet and can order new cards.
		elsif ($cards_avail > 0){
			# Get the numbers from the database.
			New_Order($cards_avail);

			# If this is the simple, single folding card.
			if ($md5h eq 'one_card')
			{
				# Add the code that displays the front of the card.
				push @cbids, $card_front_code;
				# Start counting at 2 because we have a back and a front on the page - 2 placeholders.
				my $counter = 2;
				while (++$counter <= MAXCARDSPERPAGE)
				{
					# Pad the URL with 0's so we don't create bogus CB cards.					
					push @cbids, 0;
				}
			}

			# Create the csv list of CBID's for the call to the display/print cgi script.
			$cbid_list = join ',', @cbids;

			# Display the numbers in card format.
			Preview();
		}
		# If we have personal CBID's but no new CBID's available.
		else{
			my $counter = $num_p_cbids;
			while (++$counter <= MAXCARDSPERPAGE)
			{
				# Pad the URL with 0's so we don't create bogus CB cards.					
				push @cbids, 0;
			}
			
			# Create the csv list of CBID's for the call to the display/print cgi script.
			$cbid_list = join ',', @cbids;

			# Display a page with a link to print just the personal CB cards. 
			$tmpl = &G_S::Get_Object($db, $TMPL{'NoneAvail'}) || die "Failed to load template: $TMPL{'NoneAvail'}";
			my $path = CBCARDS_CGI_PATH . "/" . md5_hex("$salt $cbid_list") . "/" . $cbid_list;
			$tmpl =~ s/%%USE%%/distribution to others/;
			$tmpl =~ s/%%PATH%%/$path/;
			$tmpl =~ s/%%ONSTART%%//;
			$tmpl =~ s/%%ONSTOP%%//;

			print $q->header();
			print $tmpl;
		}
	}
	else{
		$tmpl = &G_S::Get_Object($db, $TMPL{'NoneAvail'}) || die "Failed to load template: $TMPL{'NoneAvail'}";
		$tmpl =~ s/%%USE%%/distribution to others/;
		$tmpl =~ s/%%ONSTART%%/<!--/;
		$tmpl =~ s/%%ONSTOP%%/-->/;

		print $q->header();
		print $tmpl;
	}
}

###### If not a VIP, Member, or Affinity Group, print an error message.
else
{
		print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff'), $q->h3('ClubBucks Card distribution is available to VIPs, Members, and Affinity Groups only.'), $q->end_html();
}

exit;

##########################################
###### Subroutines start here.
##########################################

sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "<BR><B>", $_[0], "</B><BR>";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

###### Check to see how many cards the member still has available.
sub Check_Limit{

	# Read the number of cards requested by the member.
	my $how_many = shift;
	my ($sth, $qry, $rv) = ();

	# Look only for class 2 (non-activated cards) 
	# with a card number > 500000000 (created online).
	$qry = "SELECT count(*)
		FROM clubbucks_master
		WHERE referral_id = ?
		AND class = 2
		AND id >= 500000000";
	$sth = $db->prepare($qry);
	$rv = $sth->execute($referral_id);
	defined $rv or die $sth->errstr;
	$how_many = $sth->fetchrow_array;

	$how_many = MAXCARDSPERMEMBER - $how_many;

	return $how_many;
}

###### Obtain a list of new CBID numbers and initialize them in the database.
sub New_Order{

	# Read the number of cards to be queried from the table.
	my $how_many = shift;
	my ($counter, $current) = 0;
	my ($sth, $qry, $rv) = ();
	my @new_cbids = ();

	# Get new CBID's from the database.
	$qry = "SELECT nextval('clubbucks_id_seq')";
	$sth = $db->prepare($qry);
	while ($counter++ < $how_many)
	{
		$rv = $sth->execute;
		defined $rv or die $sth->errstr;
		$current = $sth->fetchrow_array;

		# Make both a complete list and a list of new CBID's only.
		push @new_cbids, $current;
		push @cbids, $current;
	}
	$sth->finish();

	# Insert the new CBID's into the table and initialize with CBID, Referring member ID,
	#    class = 2 (Assigned).
	$qry = "INSERT INTO clubbucks_master
		(id, referral_id, class)
		VALUES
		(?, ?, ?)";
	$sth = $db->prepare($qry);
	foreach $current (@new_cbids)
	{
		$rv = $sth->execute($current, $referral_id, 2);
		defined $rv or die $sth->errstr;
	}	
	$sth->finish();

}

###### Send the CBID numbers to the display script.
sub Preview{
	my $path = CBCARDS_CGI_PATH . "/" . md5_hex("$salt $cbid_list") . "/" . $cbid_list;
	print $q->redirect($path);
}

sub RedirectLogin{
	my $me = $q->script_name;
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print "Please login at <a href=\"$LOGIN_URL?destination=$me\">$LOGIN_URL</a><br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

###### Modification comments go here.

###### 04/28/03 - Removed the "test" backdoor used for URL direct testing.
###### 05/06/03 - Increased limit of MAXCARDSPERMEMBER to 500.
###### 05/28/03 - Re-initialized $cbid_list after the check to see if we have an md5h value,
######             in order to suppress md5_hex errors in case of a full_page request.
###### 06/20/03 - Added the one_card option that allows printing of a single new card,
######		front and back on the same page.
###### unknown  - Increased limit of MAXCARDSPERMEMBER to 1000.
###### 08/07/03 - Increased limit of MAXCARDSPERMEMBER to 1500.
###### 03/15/04 - Increased limit of MAXCARDSPERMEMBER to 2000.
###### 	Modified Check_Limit query to look only for class 2 (non-activated
######		 cards) with a card number > 500000000 (created online).




