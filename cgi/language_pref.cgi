#!/usr/bin/perl -w
###### language_pref.cgi
###### 
###### Display a form from which a member can change their language preference in the members table.
###### 
###### Created: 08/27/04	Karl Kohrt
###### 
###### Last modified: 02/23/05 Karl Kohrt

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw( $LOGIN_URL );
use DBI;
use DB_Connect;
#use POSIX qw(locale_h);
#use locale;
#setlocale(LC_CTYPE, "en_US.ISO8859-1");

###### CONSTANTS
our %TMPL = (	'email_blurb'	=> 10339,
		'selections'	=> 10340,
		'confirm'	=> 10341);

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->url;
our $action = $q->param('action') || '';
our $db = '';
our $member_id = '';
our $alias = '';
our $login_error = 0;

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

# Continue if we have a good login/cookie.
unless ( $login_error )
{
	# If no language preference is on file in the members table.
	if ( No_Language_Pref() || $action eq 'update' )
	{
		unless ( $action )
		{
			Display_Selections();
		}
		elsif ( $action eq 'update' )
		{
			my $language_pref = $q->param('language_pref');

			# Unless the user selected no language.				
			unless ( $language_pref eq '' )
			{
				# If we successfully updated the language preference.			
				if ( Update_Language_Pref($language_pref) )
				{
					Display_Confirmation();
				}
				else
				{
					Err("There was an error while saving your language preference.");
				}
			}
			# If no language was selected, close the pop-up window.
			else {	Close_Popup()	}																		
		}
	}
	# If there is already a language preference in the member record, do nothing.
	else { Err("") }	
	$db->disconnect;
}
exit(0);

################################
###### Subroutines start here.
################################

sub cboLanguage
{
	my $default_value = $gs->{language_prefs}[0][1] || '';
	my %labels = ();
	my @values = ();
	my $tmp = '';
	my $sth = $db->prepare("	SELECT code2, description
					FROM 	language_codes
					WHERE 	active = 'true'
					ORDER BY description;");
	$sth->execute();
	$labels{''} = 'Select a Language';
	push @values, '';
	while ( $tmp = $sth->fetchrow_hashref )
	{
		$labels{$tmp->{code2}} = $tmp->{description};
		push @values, $tmp->{code2};
	}
	return $q->popup_menu(
		-name=>'language_pref',
		-values=>\@values,
		-labels=>\%labels,
		-default=>$default_value,
		-force=>1);
}

###### Close the pop-up window.
sub Close_Popup
{
	my $js_string = qq!	<script language="JavaScript" type="text/javascript">
				self.close()					
				</script>!;
	print $q->header(), $js_string;
}				
###### Display the success confirmation page.
sub Display_Confirmation
{
	# Get the confirmation template.
	my $confirm = $gs->Get_Object($db, $TMPL{confirm}) || die "Couldn't open $TMPL{confirm}";
	print $q->header(), $confirm;
}

###### Display the language selection form.
sub Display_Selections
{
	my $report_body = '';

	# Get the selection template.
	$report_body = $gs->Get_Object($db, $TMPL{selections}) || die "Couldn't open $TMPL{selections}";

	my $blurb = $gs->Get_Object($db, $TMPL{email_blurb});
	$report_body =~ s/%%BLURB%%/$blurb/g;
	my $languages = cboLanguage();
	$report_body =~ s/%%LANGUAGES%%/$languages/g;
	$report_body =~ s/%%ALIAS%%/$alias/g;
	$report_body =~ s/%%ACTION%%/update/g;
	$report_body =~ s/%%ME%%/$ME/g;

	# Lay out the template into one, long line for Javascript to digest.	
	$report_body =~ s/\r\n//g;
	$report_body =~ s/\n//g;
	$report_body =~ s/"/\\"/g;

	print $q->header(), 
qq#function language_pref() {
var nuwin= window.open("", "languages", "height=300,width=400");
if ( nuwin )
{			
	nuwin.document.open();
	nuwin.document.write("$report_body");
	nuwin.document.close();
}
else
	alert("Your Language Preference is not set. Please use the 'Personal' menu, 'Update Profile' page to select your preferred language for receiving DHS email.");
}
language_pref();#;
}

##### Prints an error message to the browser.
sub Err
{
	my $msg = shift;
	print $q->header, $msg;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	my $mem_info = '';
	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		my $admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'language_pref.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}
		unless ( ($alias = $member_id = $q->param('id')))
		{
			Err('Missing a required parameter');
			$login_error = 1;
		}
	}
	# If this is a member.		 
	else
	{
		unless ($db = DB_Connect( 'language_pref.cgi' )){exit}
		$db->{RaiseError} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id )
		{
			# Assign the info found in the VIP cookie.
			$alias = $mem_info->{alias};
		}
		# If we didn't get a member ID, we'll check for a ppreport cookie.
		else
		{
			$member_id = $gs->PreProcess_Input($q->cookie('id')) || '';
			$alias = $db->selectrow_array("SELECT alias
								FROM 	members
								WHERE id = $member_id") if $member_id;


		}					
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			$login_error = 1;
		}
	}
}

###### Get the member's language info from the members table.
sub No_Language_Pref
{
	my @data = $db->selectrow_array("	SELECT language_pref
						FROM 	members
						WHERE 	id = $member_id");

	# Either we found a language preference...
	if ( defined $data[0] && $data[0] =~ /\w/ ) { return 0; }
	# ...or we didn't.
	else { return 1; }
}	
		
###### Redirect the member to the login page. 
sub RedirectLogin
{
	print $q->redirect( $LOGIN_URL . "?destination=" . $ME );
}

###### Update the member's language preference in the members table.
sub Update_Language_Pref
{
	my $language_pref = shift;
	my $sth = $db->prepare("	UPDATE	members
					SET 	language_pref = ?,
						stamp = NOW(),
						memo = 'Email Language Preference updated.'
					WHERE 	id = ?");
	my $rv = $sth->execute($language_pref, $member_id);
	$sth->finish;

	return $rv;								
}	


###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login. Added query to get alias value.
