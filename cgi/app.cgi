#!/usr/bin/perl -w
###### last modified: 12/17/2007	Bill MacArthur
###### since this script will attempt a DB connection either as the ADMIN user or cgi,
###### it may not be suitable for APACHE::DBI

use Digest::MD5  qw( md5_hex );
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw(%MEMBER_TYPES $COACH_LEVEL $DATA_CONTACT);	###### HOLDS OUR GLOBAL CONFIGURATION VARIABLES
use DB_Connect;
use State_List;
use Mail::Sendmail;
use MailingUtils;
use ParseMe;
require 'excluded_domains.lib';

$| = 1;
our $q = new CGI;

###### GLOBALS

###### SET this var to 1 ONLY when needed for testing as it bypasses payment authorization i.e. NO $$$$$$$
our $BYPASS_PAYMENT_AUTHORIZATION = 0;

###### set this to zero to disable PayPal hooks
our $PayPal_YES = 0;

our $UPGRADE_TMP_TBL = 'upgrade_tmp';		###### our temp upgrade record table
our $UPGRADE_TMP_ARC_TBL = 'upgrade_tmp_arc';	###### archive of the same for saving repeated auth attempts
our $IPN_TBL = 'ipn';				###### our PayPal payment notifications table

###### the monthly VIP subscription fee
our $MONTHLY_FEE = '25.00';
our $BILLING_DAY = '05';	###### the day of the month we want VIP PayPal subscriptions to renew

our $MAX_AUTHS = 1;	###### the maximum allowed number of VIPs a method of payment is allowed to have
our $MAX_AUTHS_ADMIN = 200000; ###### same as above except for ADMIN mode
our $MYURL = $q->script_name();
our $mailprog = '/usr/sbin/sendmail';
our ($db) = ();	###### global DB handle

our $FRAUD_COUNT = 5;
our $FRAUD_ALERT = qq#You have exceeded the maximum number of declined upgrade attempts.<br />
	Please contact acf\@dhs-club.com for further assistance.#;

###### the following is a flag which when enabled (1) will cause an email to be generated
###### for any 'fraud' attempts
our $FRAUD_NOTIFY = 1;

our $PROCESSING_ACCEPTANCE_MSG = qq|<p class="in">
	Your application has been accepted for processing.<br />
	<b style="color: #ff0000">Do not back up and resubmit as you will incur additional charges.</b>
	<br />If you have questions regarding this application, please
	contact us at <A HREF="mailto:acf\@dhs-club.com">acf\@dhs-club.com</A></p>\n|;

###### a flag that allows special admin access (this is set below after a successful admin DB connection)
our $ADMINMODE = ();

###### we had a mailing going out with 'a' as the parameter instead of 'alias'
our $alias = G_S::PreProcess_Input( uc($q->param('alias')) || uc ($q->param('a')) || '' );
our $action = $q->param('action') || '';
	###### ACTIONS
	###### app -- a call to just fill in the ID field on the application (we'll parse the actual static page)
	###### <none> -- there is no action param at the start of an info update or an upgrade
	###### step2 -- the next action after the start of an info update or upgrade
	###### confirm -- the submitted info from above has been displayed and confirmed by the user
	###### MOP_CC, MOP_ECK -- a CC or eCheck payment submission
	###### MOP_PP -- PayPal submission
	###### bypass -- an ADMIN action that bypasses payment
	###### payment -- they have entered payment information for authorization

###### now this is a hack to prefill the ID field in what would have been the static page
###### there is no need to validate anything or even connect to the DB, so we'll put it here at the top
###### if there is any error in performing this process, we'll just issue a redirect to the static page
if ($alias && $action && $action eq 'app')
{
	if (Prefill() == 0)
	{
		print $q->redirect('http://www.clubshop.com/VIP_app.html');
	}
	exit;
}
###### coerce the utype parameter since there is only one kind
$q->param(-name=>'utype', -value=>'M2V');
our $type = $q->param('utype') || 'M2V';
our $UPDATE = $q->param('update');

our $usage = ($UPDATE) ? ('update') : ('upgrade');

our $DB_error_contact = 'webmaster@dhs-club.com';
our %FORM_HTML = (	update=>{	form=>               10102,
					confirm=>            10103},
			upgrade=>{	form=>               10104,
					confirm=>            10105,
					MOP=>                10072,	###### payment options
					MOP_PP=>             10073,	###### pay by PayPal
					MOP_CC=>             10108,	###### pay by credit card
					MOP_ECHECK=>         10109,	###### pay by echeck
					vf=>                 10076,	###### the footer of the final page
					pf=>                 10086,	###### same for partners
					congrats_header=>    10088,	###### header for the final page
					failure=>            10078},	###### failure document
			m_update=>{	form=>               10106,	###### member updates
					confirm=>            10107}
);

our %email_notifications = (	member=>{	M2V=> 101, X2V => 101},
					sponsor=>{	M2V=> 10081, X2V => 10081});
###### upline is CC'd on sponsor letter

our %UPGRADE_TYPE = (
	M2V=>{	amt	=>'49.95',
		text	=>'Member to VIP',
		file	=> 10083,
		type	=>'v',
		title	=>'VIP',
		pp	=>.5005},
			
	X2V=>{	amt	=>'25.00',
		text	=>'(Ex VIP) Member to VIP',
		file	=> 10270,
		type	=>'v',
		title	=>'VIP',
		pp	=>1}
);

our $dover_flag = 0;	###### Do Over flag & msg
our $dover_msg = '';
our ($data) = ();	###### our global record hashref (could be either a member record or a tmp upgrade rec)

###### Global Template Object Variables

our $FHTMLTMPL;

###### %field has the form of 'field name', 'value', 'html class type' (n=normal, r will raise an error flag),
###### 'required flag (set to 1 if required)'
my %field = (	Prefix =>		{value=>'', class=>'n', req=>0},
		secPrefix =>		{value=>'', class=>'n', req=>0},
		FirstName =>		{value=>'', class=>'n', req=>1},
		secFirstName =>	{value=>'', class=>'n', req=>0},
		MDName =>		{value=>'', class=>'n', req=>0},
		secMDName =>		{value=>'', class=>'n', req=>0},
		LastName =>		{value=>'', class=>'n', req=>1},
		secLastName =>	{value=>'', class=>'n', req=>0},
		Suffix =>		{value=>'', class=>'n', req=>0},
		secSuffix =>		{value=>'', class=>'n', req=>0},
		Company_Name =>	{value=>'', class=>'n', req=>0},
		Address1 =>		{value=>'', class=>'n', req=>1},
		Address2 =>		{value=>'', class=>'n', req=>0},
		City =>		{value=>'', class=>'n', req=>1},
		State =>		{value=>'', class=>'n', req=>0},
		State2 =>		{value=>'', class=>'n', req=>0},
		PostalCode =>		{value=>'', class=>'n', req=>1},
		Country =>		{value=>'', class=>'n', req=>1},
#		Phone_AC =>		{value=>'', class=>'n', req=>1},
		Phone =>		{value=>'', class=>'n', req=>1},
		Fax_Num =>		{value=>'', class=>'n', req=>0},
		TIN =>			{value=>'', class=>'n', req=>1},
		Password =>		{value=>'', class=>'n', req=>1},
		language_pref =>	{value=>'', class=>'n', req=>0},
		EMailAddress =>	{value=>'', class=>'n', req=>1},
		EMailAddress2 =>	{value=>'', class=>'n', req=>0});

###### we wait to connect to the DB until we are sure we've got a good ID
###### here is where we get our admin user and try logging into the DB
my $operator = $q->cookie('operator');
my $pwd = $q->cookie('pwd');

###### we will not allow admin access for update use (we have an in-house interface for that)
if ($operator && $pwd && not $UPDATE)
{
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'app.cgi', $operator, $pwd )){exit}
	$ADMINMODE = 1;
	$MAX_AUTHS = $MAX_AUTHS_ADMIN;
}
else
{
###### if we don't have admin cookies, then we'll assume we're a member using the app
###### they will be logged in as user 'cgi' then

###### we are including a flag with this connection so that the connection will be identified by the script name
	unless ($db = DB_Connect('app.cgi')){exit}

}
 
###### we need a database connection to get these values
our $FRAUD_NOTIFICATIONS_ADDRESS = G_S::Get_Object($db, 9006, 1);

###### Trap Month End Process Begins ######

my $rurl = "http:\/\/www.clubshop.com\/maint.html";
my $isql = "Select distinct status from system_status limit 1";
my $istat;
my $sth;
my $rv;

#   $sth = $db->prepare($isql);

#   $rv = $sth->execute();
#   defined $rv or die $sth->errstr;

#   $sth->bind_columns(undef, \$istat);

#   $sth->fetch();

#   $rv = $sth->finish();

#   if ( $istat eq "CLOSED" )
#   {
#    print $q->redirect(-uri=>$rurl);
#   }

###### Trap Month End Process Ends ######

###### we need to put the header here since error handling for the DB connection will handle its own header
print $q->header(-charset=>'utf-8');

###### if there is no action then this is the beginning of an info update or upgrade
unless ( $action )
{		###### ###### START OF NO ACTION ###### ######

	###### check that the we have an ID
	unless ($alias || $q->cookie('AuthCustom_Generic'))
	{	###### we are missing a vital parameter
		Error_G('No ID received.<br />Please go back and enter your ID.');
		goto 'END';
	}


	###### we'll base our query on whether it is an upgrade or update
	###### an upgrade will only check that we have a match on the submitted ID (alias)
	##### whereas we will have to match another unique identifier if they want to do an update

	##### our basic query is always the same
	my $qry = "SELECT *, oid FROM members WHERE ";

	##### I'm using db->quote becuase I'd rather not have DBI errors cropping up if they submit something with quotes
	my $qry_modifier = "alias = " . $db->quote($alias);
	if ( $UPDATE )
	{

	##### we will only have an o param for a non-paying member info update
		my $oid = G_S::PreProcess_Input($q->param('o')) || '';

	##### now we'll check that the oid is strictly numeric, else there will be a DB error
		if ($oid =~ /\D/)
		{
			Error_G('The unique number must only contain numbers.<br />Please backup and try again.');
			goto 'END';
		}

		my $password = $q->param('password') || '';

	##### we'll check for a VIP cookie and allow access based on the cookie rather than additional login
		my ($id) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') ) if $q->cookie('AuthCustom_Generic');

	##### if they have the cookie and it is valid it will return their numeric ID
	##### if the cookie is not valid, it will contain an error message
	##### if they don't have the cookie, the returned value =''
		if ( $id && $id =~ /\D/ )
		{
			my $msg = "You must be logged in to use this application.<br />$id<br /><a href=\"https://www.clubshop.com/cgi-bin/Login.cgi\">Click here</a> to login.";
			Error_G($msg);
			goto 'END';
		}

		if ($id)
		{
			$qry .= "id = $id";
		}
		elsif ($oid)
		{
			$qry .= "$qry_modifier AND oid = '$oid'";
		}
		else
		{
			$qry .= "$qry_modifier AND password = " . $db->quote($password);
		}
	}
	else{ $qry .= $qry_modifier }

	my $sth = $db->prepare($qry);
	$sth->execute;
	$data = $sth->fetchrow_hashref;
	$sth->finish;

	unless ($data)
	{	###### there was no matching ID
		if ( $UPDATE )
		{
			Error_G('Bad ID / Password combination');
		}else{
			Failure_notification("Invalid ID: $alias");
		}
		goto 'END';
	}

	###### make sure they are allowed to use this form if its an update
	if ($UPDATE)
	{
		if ($data->{membertype} !~ /m|v|s|ag/)
		{
			my $msg = "Only 'Associate Members', 'Shopper Members' and 'VIPs' are allowed to change their information.<br>";
			$msg .= "Member status: $MEMBER_TYPES{$data->{membertype}}<br>\n";
			$msg .= "Contact " . $q->a({-href=>"mailto:$DATA_CONTACT"}, $DATA_CONTACT);
			$msg .= ' for more information. Please include your ID number.<br>Thank you.';
			Error_G($msg);
			goto 'END';
		}
	}

	###### its an upgrade so lets do some additional tests for that	
	else
	{

	###### in case they have an authorized tmp upgrade record, we'll tell 'em and direct them accordingly
		goto 'END' if Check_Already_Authorized();

	###### make sure they are not a fraud risk by verifying that we don't have a flagged tmp upgrade record already
	###### fraud check does its own error handling
		goto 'END' if Fraud_Check();

	###### we'll check that they have chosen the correct upgrade form if this is an upgrade
	###### Check_up...... does its own error handling
		if (Check_upgrade_type() != 1)
		{
			goto 'END';
		}

	}

	###### we will create a first stage ticket to avoid having to recheck the validity of the membership after the 
	###### form is submitted. we also need to pass these params since we won't be building them again
	$q->param(-name=>'id', -value=>$data->{id});
	$q->param(-name=>'idh', -value=>md5_hex("$data->{id} $data->{membertype} $data->{oid}"));
#	$q->param(-name=>'spid', -value=>$data->{spid});
 	$q->param(-name=>'alias', -value=>(uc $alias));
	$q->param(-name=>'o', -value=>$data->{oid});
	$q->param(-name=>'membertype', -value=>$data->{membertype});
#	$q->param(-name=>'country', -value=>$data->{country});

###### now we'll spit out the appropriately filled in form
	Load_Data();
	Create_controls();
	Do_Page();
}
###### end of IF NOT $action



###### we've got all our input, let 'em check it
elsif ($action eq 'step2')
{
	Load_Params();
 
###### we need to reset some required fields for members to be not required ON UPDATES
	if ($UPDATE && $q->param('membertype') !~ /v/)
	{
		foreach ( qw/ Phone TIN EMailAddress / )
		{
			$field{$_}{req} = 0;
		}
	}


###### since hash verification takes care of its own error notification, we need do nothing further if it fails
	if ( Verify_ID_Hash() == 1 )
	{

###### Validate() creates the full hash param
		my $rv = Validate();
		if ( $rv == 1)
		{
			Confirmation();
		}
		elsif ( $rv == -1 ){}	###### do nothing, an error handler should have already taken care of anything
		else
		{
			Create_controls();
			Do_Page();
		}
	}
}

###### we are ready to proceed to the payment part of this transaction
###### but we first want to write out a temporary upgrade record
elsif ($action eq 'confirmed' && not $UPDATE)
{
	Load_Params();
	if ( Verify_ID_Hash() == 1 )
	{

###### since we will not have a full hash in ADMIN MODE, we need to bypass this verification
		unless ($ADMINMODE)
		{
			if ( Verify_Full_Hash() != 1 )
			{
				goto 'END';
			}
		}

###### these checks are redundant along each step, but some people could just back up their browser and 
###### skip a previous check
		if ( Check_Already_Authorized() || Fraud_Check())
		{
			goto 'END';
		}

###### create_tmp.. takes care of its own error reporting so we are all set if it fails
		if ( Create_tmp_record() == 1 )
		{

###### if we are not in ADMIN mode we'll proceed as normal
			unless ($ADMINMODE)
			{

	###### we could branch out to an email notification routine here that would require a response before proceeding

	###### but for now we will just proceed to the payment options section as if we just responded
				Present_MOP();
			}

###### we ARE in ADMIN mode so we'll interrupt the flow to allow bypassing the payment process
			else
			{
				###### ADMIN BREAK ######
				Admin_Break();
			}
		}
	}
}

###### they have confirmed their INFO UPDATE so let's update their records
elsif ($action eq 'confirmed' && $UPDATE)
{

###### this section is used for information updates only
	Load_Params();
	if ( Verify_ID_Hash() == 1 && Verify_Full_Hash() == 1)
	{

		Update_Record();
		if (defined $DBI::errstr)
		{
			DB_error("There was an error updating your records.");
		}

###### THIS IS WHERE OUR records updated message goes
		else
		{
			my $msg;
			print $q->h3('Your records were successfully updated.<br />Thank You!');
			if ($data->{membertype} eq 'm')
			{ 
				$msg = $q->a({-href=>'http://www.clubshop.com/index.html'}, 'Clubshop Home');
			}
			else
			{
				$msg = $q->a({-href=>'http://www.clubshop.com/vip/control/index.shtml'}, 'Control Center');
			}

			print $q->h3( $msg ), $q->end_html();
		}
	}
}

###### this is the entry point for those responding to an email confirmation
elsif ($action eq 'MOP')
{

###### if we get no data back we cannot proceed
	if ($data = Get_Tmp_Record())
	{
		Present_MOP() unless (Check_Already_Authorized() || Fraud_Check());
	}
}

elsif ($action eq 'MOP_ECHECK')
{
	Upgrade_Payment_Form();
}

elsif ($action eq 'MOP_CC')
{
	Upgrade_Payment_Form();
}

elsif ($action eq 'MOP_PP')
{
	Upgrade_Payment_Form();
}

###### this is currently where we authorize CC & eCheck payments
elsif ($action eq 'payment')
{
	$data = Get_Tmp_Record();
	if ($data)
	{
		unless (Check_Already_Authorized() || Fraud_Check())
		{
			Finish_header();
			my $res = Process_Order();
###### if a zero is returned, then all error processing has taken place and we can simply quit
###### if a one is returned, the transaction was authorized
###### otherwise, a textual message will be returned
			if ($res =~ /\D/ )
			{
				Failure_notification("Your payment could not be processed.<br>$res", 1);
			}
			elsif ($res == 1)
			{
				Upgrade();
				if ($DBI::err)
				{
					DB_error("There was an error updating your records");
				}
				else
				{
					Notifications();
				}
			}
		}
	}

}

###### this is an ADMIN only action
elsif ($action eq 'bypass' && $ADMINMODE)
{
	$data = Get_Tmp_Record();
	if ($data)
	{
		unless (Fraud_Check())
		{
			Finish_header();
			Upgrade();
			if ($DBI::err)
			{
				DB_error("There was an error updating this member's records");
			}
			else
			{
				Notifications();
			}
		}
	}
}

###### this is the action they will do if they have an authorized temp record that has not been upgraded for some reason
elsif ($action eq 'Activate')
{

###### get tmp record handles its own error reporting
	$data = Get_Tmp_Record();
	if ($data)
	{
		if ($data->{authorized} == 1)
		{
			Finish_header();
			Upgrade();
			if ($DBI::err)
			{
				DB_error("There was an error updating your records");
			}
			else
			{
				Notifications();
			}
		}
		else
		{
			Error_G("Couldn't find an authorized pending upgrade on file for you.");
		}
	}
}

###### this is where our PayPal people will come back in after setting up there subscription
elsif ($action eq 'ppreturn')
{
	$data = Get_Tmp_Record();
	if ($data)
	{
	    unless (Check_Already_Authorized() || &Fraud_Check())
		{
		Finish_header();
		print qq#Waiting for your PayPal authorization to come through.<br />Please be patient.<br />
If you close this screen, press reload or refresh in your browser or go elsewhere, you may miss important
information or otherwise interfere with the final upgrade process.<br />#;
		my $loop = 0;
		while ($loop < 4)
		{

###### we'll do a little loop and print something while we wait
###### we'll retest 4 times in a 2 minute period which should be adequate
			my $x = 0;
			while ($x < 15){ $x++; print "*"; sleep 2 }
			$data = &Get_Tmp_Record();
			if ($data->{authorized} eq '1'){
				print "<br />\n";
				&Upgrade();
				if ($DBI::err){ &DB_error("There was an error updating your records") }
				else { &Notifications() }
				last;			
			}
			else{

###### if it's not authorized we'll check to see if its pending
				my $sth = $db->prepare("SELECT * FROM $IPN_TBL WHERE id = $data->{id} AND payment_status= 'Pending'");
				$sth->execute;
				my $ck = $sth->fetchrow_hashref; $sth->finish;
				if ($ck){
					print <<PPENDING;
<br /><h4>We have located your PayPal payment, but it is currently pending.</h4>
Pending reason: <b>$ck->{pending_reason}</b><br /><br />
Once your payment is 'Completed' then you will be able to finish your upgrade.<br />
This application will direct you to activate your authorized upgrade at that time.<br />
Please come back to this application after you receive notice from PayPal that your payment has completed.
</div></body></html>
PPENDING
					last;
				}
			}
			print "<br />\n"; $loop++;
		}
	    }
	}
}

###### this would be manual app that will pull up an unauthorized tmp record
###### if there is no tmp record, it will redirect back to a normal app
elsif ($action eq 'manual' && $ADMINMODE)
{
	my $sth = $db->prepare("
		SELECT u.*,
			m.oid
		FROM 	$UPGRADE_TMP_TBL u,
			members m
		WHERE m.id=u.id
		AND u.alias= ?");

	$sth->execute($alias);
	$data = $sth->fetchrow_hashref;
	$sth->finish;
	unless ($data)
	{
		print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title></title>
<META HTTP-EQUIV="Refresh" CONTENT="3; URL=https://www.clubshop.com/cgi/admin/funding_admin.cgi"> 
</head><body BGCOLOR="#ffffff">No matching temp upgrade record found for: $alias<br />
You will now be redirected to admin funding interface.<br />
If not, click <a href="https://www.clubshop.com/cgi/admin/funding_admin.cgi">here</a>.
</body></html>|;
	}
	else
	{
###### we will create a first stage ticket to avoid having to recheck the validity of the membership after the 
###### form is submitted. we also need to pass these params since we won't be building them again
	$q->param(-name=>'id', -value=>$data->{id});
	$q->param(-name=>'idh', -value=>md5_hex("$data->{id} $data->{membertype} $data->{oid}"));
#	$q->param(-name=>'spid', -value=>$data->{spid});
 	$q->param(-name=>'alias', -value=>(uc $alias));
	$q->param(-name=>'o', -value=>$data->{oid});
	$q->param(-name=>'membertype', -value=>$data->{membertype});
	$type = $data->{utype};
	$q->param(-name=>'utype', -value=>$type);

###### now we'll spit out the appropriately filled in form
		Load_Data();
		Create_controls();
		Do_Page(1);
	}
}

###### we should never get to here, but go figure
else
{
	Error_G('Undefined action');
}

END:
$db->disconnect;
exit;
###### ###### ###### ###### ######

sub Admin_Break
{
	my @YR = ();
	my $yr = [localtime()]->[5] + 1900;
	my $now = scalar localtime();
	my $curr_month = [localtime()]->[4] +1; $curr_month = '0' . $curr_month if $curr_month <10;
	my %labels = ('MOP'=>'NO', 'bypass'=>'YES');

	print $q->start_html(-bgcolor=>'#ffffff',
				-head=>$q->Link({	-href=>'/css/vipapp.css',
							-rel=>'stylesheet',
							-type=>'text/css'})
		);
	print $q->h4('Bypass payment processing?');
	print $q->start_form(-action=>$MYURL);

	print Create_hidden();

	print $q->radio_group(-name=>'action', -values=>['MOP','bypass'], -linebreak=>1, -labels=>\%labels, -default=>'MOP', -force=>1);

	print '<br /><b>Paid Thru: </b>';
	print $q->popup_menu(-name=> 'paidthrumo',
				-values=>['01', '02', '03', '04', '05', '06',
					'07', '08', '09', '10', '11', '12'],
				-default=>$curr_month), ' Month  ';

	for (my $x = $yr - 1; $x <= ($yr + 9); $x++ )
	{
		push (@YR, $x);
	}
	
	print $q->popup_menu(-name=>'paidthruyr',
				-values=>\@YR,
				-default=>$yr), ' Year';
	
	print "<br />NOTE: Paid Thru fields ignored unless bypassing payment processing is 'YES'.\n<br />";
	print "<span class=\"fp\" style=\"color: #aa0000\">If you enter a paid thru beyond this month, be sure to enter a CA subscription transaction for each month beyond this month with the appropriate month as the transaction date.</span>\n";
	print '<br /><h4>Effective Upgrade Date</h4>Change if required<br />';
	print "<span class=\"fp\" style=\"color: #aa0000\">If you enter an upgrade date prior to this month, it is unlikely that the PP will be allocated in the prior month. Check with IT to see if the period is closed.</span><br />\n";
	print $q->textfield(-name=>'upgradedate', -value=>$now, -size=>30), $q->br();
	print $q->submit(), $q->end_form(), $q->end_html();
}

sub cbo_Country
{
	require '/home/httpd/cgi-lib/CC.list';
	my $param_name = $_[0];
	return $q->popup_menu(	-name=>$param_name,
					-class=>'in',
					-values=>\@CC::COUNTRY_CODES,
					-default=>$data->{country},
					-labels=>\%CC::COUNTRIES);
}

sub cboLanguage
{
	my $default_value = $field{language_pref}{value} || 'en';
	my %labels = ();
	my @values = ();
	my $tmp = '';
	my $sth = $db->prepare("	SELECT code2, description
					FROM 	language_codes
					WHERE 	active = 'true'
					ORDER BY description;");
	$sth->execute();
	$labels{''} = 'Select a Language';
	while ( $tmp = $sth->fetchrow_hashref )
	{
		$labels{$tmp->{code2}} = $tmp->{description};
		push @values, $tmp->{code2};
	}	
	return $q->popup_menu(
		-name=>'language_pref',
		-values=>\@values,
		-labels=>\%labels,
		-default=>$default_value,
		-force=>1,
		-onchange=>"alert(\"${\G_S::Get_Object($db, 10339)}\")");
}

sub cbo_State_List
{
        my $param_name = $_[0];
        my %S  = %State_List::STATE_CODES;

        my @SORTED_KEYS;
        my $SORTED_KEYS;
        my $key;
        my $value;
        my $html;

        my $sel;

$sel = "";

$html=<<EOH1;
<select name="$param_name" size="1">
<option value=""></option>
EOH1

@SORTED_KEYS = sort ( keys %S );

foreach $key (@SORTED_KEYS) 
{
# warn "\$data->{state}  :",$data->{state},"\n";
# warn "\$S{\$key}       :",$S{$key},"\n";

if ( $data->{state} )
{
 if ( $data->{state} eq $S{$key}) {
 #      warn "Hit on \$data->{state}\n"; 
      $sel = "SELECTED";
 #      warn "\$sel set to $sel\n";
    } else { 
            $sel ="";
    }
}

if ( $q->param('State2'))
{
 if ( $q->param('State2') eq $S{$key}) {
  #     warn "Hit on \$param->{State2}\n"; 
      $sel = "SELECTED";
  #     warn "\$sel set to $sel\n";
      } else { 
  $sel ="";
  }
}

 # warn "About to build option string \$sel = $sel\n";

 $html= $html. "<option value=\"" . $S{$key} . "\" " .  $sel ." >" . $key . "</option>\n";
}

$html = $html . "</SELECT>\n";

return $html;
}

sub Check_Already_Authorized
{
###### besides checking for an authorized tmp upgrade record
###### we'll also check for a 'Pending' PayPal record if PayPal hooks are set
	my ($sth, $ck) = ();

###### since we may already have the data (if this dataset is not from the initial member record lookup)
###### we won't bother to look it up again (we check for the existence of the key and then the value
###### to keep warnings happy)
	unless ( exists $data->{authorized} && $data->{authorized})
	{
		$sth = $db->prepare("
			SELECT authorized, id, tmp_id
			FROM $UPGRADE_TMP_TBL
			WHERE id = $data->{id}
			AND authorized= 1");
		$sth->execute;
		$ck = $sth->fetchrow_hashref;
		$sth->finish;
	}

###### since we got a one, we'll tell 'em so
###### since we will not have an 'authorized' key on an initial submission
###### we'll keep warnings happy by first checking for its existence befor checking it for a value
	if ($ck || (exists $data->{authorized} && $data->{authorized} eq '1'))
	{
		my $id = $data->{id} || $ck->{id};
		my $o = $data->{tmp_id} || $ck->{tmp_id};
		print <<ALERT;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title></title></head>
<body bgcolor="#ffffff"><div align="center">
<IMG SRC="/images/vipapp-blue3.gif" BORDER="0" ALT="" />
<h4>We found a pending upgrade on file for you for which payment has already been authorized.</h4>
Click the button to activate your upgrade.
<form action="$MYURL" method="post">
<input type="hidden" name="id" value="$id"><input type="hidden" name="o" value="$o" />
<input type="submit" name="action" value="Activate" />
${\$q->hidden('utype')}	
</form></div></body></html>
ALERT
		return 1; 
	}

###### this is our PayPal hook
	if ($PayPal_YES)
	{
		$sth = $db->prepare("SELECT * FROM $IPN_TBL WHERE id = $data->{id} AND payment_status= 'Pending'");
		$sth->execute;
		my $ck = $sth->fetchrow_hashref; $sth->finish;
		if ($ck)
		{
			print <<ALERT;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title></title></head>
<body bgcolor="#ffffff"><div align="center">
<IMG SRC="/images/vipapp-blue3.gif" WIDTH="588" HEIGHT="51" BORDER="0" ALT="">
<h4>We found a pending upgrade on file for you for which a PayPal payment is currently pending.</h4>
Pending reason: <b>$ck->{pending_reason}</b><br /><br />
Once your payment is 'Completed' then you will be able to finish your upgrade.<br />
This application will direct you to activate your authorized upgrade at that time.
</div></body></html>
ALERT
		return 1; 
		}
	}
}

sub Check_upgrade_type
{
###### make sure we have a valid 'type' parameter
###### this used to pertain to one of the 3 options
###### since there are no options any more, the one value that comes through is supposed
###### to signify that they agree to the $49.95 fee for upgrading
###### they need to click the radio button to make that happen
     	unless ($type eq 'M2V' || $type eq 'X2V')
	{
     		Failure_notification('
			You must check the box to indicate your understanding of the payment terms.
			<br />Please back up and do so to proceed.');
		return 0;
	}

###### verify membership status
###### 05/08/01 allowing 'removes' to upgrade to avoid the backlash of removal complaints
     	if ($data->{membertype} !~ /m|v|s|r/)
	{###### their membership is not current or standing
     		Failure_notification("Your membership is no longer valid.<br />Membership status: $MEMBER_TYPES{$data->{membertype}}");
     		return 0;
     	}

###### just in case they have chosen the wrong form or are just plain ignorant
     	if ($data->{membertype} =~ /v/i)
	{
     		if ($data->{membertype} =~ m/v/i)
		{
	     		Failure_notification("You are already a VIP");
     			return 0;
		}
     	}

	###### check to see if they qualify for the special upgrade option
	###### this is a limited time deal, so we may be able to remove this soon
	if ($type eq 'X2V')
	{
		my $sth = $db->prepare("SELECT vip_date FROM members WHERE id= ?");
		$sth->execute($data->{id});
		my $rv = $sth->fetchrow_array;
		$sth->finish;

		unless ($rv)
		{
			Failure_notification( qq|
				Only members who have been VIPs before qualify for this upgrade option.
				<br />Please <a href="/VIP_app.html">click here</a> to access the regular application.
			|);

			return 0;
		}
	}
	return 1;
}

sub Ck_Excluded_Domains
{
        return ($_[0] =~ /abuse/i ||
                eval 'grep $_[0] =~ /$_$/i, @excluded_domains::LIST'
        ) ? 1 : undef;
}
	
sub Confirmation
{
	my ($data, $hid, %values);

###### let's create a new hash to use without all the name reference fields
	foreach (keys %field)
	{
	
	###### we'll skip all the fields referring to the name because we want to combine them
		next if($_ =~ m/name/ || $_ =~ m/fix/);
		$values{$_} = $field{$_}{value};
	}

###### now we'll create two new fields for our primary and secondary applicants
	$values{'Applicant Name'} = "$field{Prefix}{value} " if ($field{Prefix}{value} gt '');
	$values{'Applicant Name'} .= $field{FirstName}{value};
	$values{'Applicant Name'} .= " $field{MDName}{value}" if ($field{MDName}{value} gt '');
	$values{'Applicant Name'} .= " $field{LastName}{value}";
	$values{'Applicant Name'} .= " $field{Suffix}{value}" if ($field{Suffix}{value} gt '');
	$values{'Joint Applicant Name'} = "$field{secPrefix}{value} " if ($field{secPrefix}{value} gt '');
	$values{'Joint Applicant Name'} .= $field{secFirstName}{value};
	$values{'Joint Applicant Name'} .= " $field{secMDName}{value}" if ($field{secMDName}{value} gt '');
	$values{'Joint Applicant Name'} .= " $field{secLastName}{value}";
	$values{'Joint Applicant Name'} .= " $field{secSuffix}{value}" if ($field{secSuffix}{value} gt '');
	my @sort = ('Applicant Name', 'Joint Applicant Name', 'Company_Name', 'Address1', 'Address2', 'City', 
			'State', 'State2','PostalCode', 'Country', 'Phone', 'Fax_Num', 'TIN', 'Password', 
			'EMailAddress', 'EMailAddress2', 'language_pref');

	foreach(@sort){
		$values{$_} = '&nbsp;' if ($values{$_} eq '' || $values{$_} eq ' ');
	}

	$hid = &Create_hidden();
#Error_G('debug'); goto 'END';
#	$data = $q->table({-border=>0, -cellpadding=>3}, $data);

	$FHTMLTMPL  = G_S::Get_Object($db, $FORM_HTML{$usage}{confirm}, 1)
                       || die "Could not find object $FORM_HTML{$usage}{confirm} $!\n";

        #warn "Template being retreived ", $FORM_HTML{$usage}{confirm}, "\n";
        #warn "Template Point 1\n";

        # Find State List 

	$FHTMLTMPL  =~ s/%%hidden%%/$hid/;
	$FHTMLTMPL  =~ s/%%action%%/$MYURL/;
	$FHTMLTMPL  =~ s/%%timestamp%%/scalar localtime()/e;

	# Get the language preference description to display in place of the code.
	$values{language_pref} = $db->selectrow_array("	SELECT description
								FROM 	language_codes
								WHERE 	code2 = '$field{language_pref}{value}';");
	foreach(@sort)
        {

         $FHTMLTMPL =~ s/%%$_%%/$values{$_}/g;
        }
       
        if ( ! $field{State}{value} ) { $FHTMLTMPL =~ s/%%State%%/\&nbsp;/g; }
        if ( ! $field{State2}{value} ) { $FHTMLTMPL =~ s/%%State2%%/\&nbsp;/g; }

	print $FHTMLTMPL;
 
}

sub Congratulations
{
###### this part is the completion of the page begun in finish_header()

	my $page = ($data->{utype} =~ m/v/i) ? ($FORM_HTML{upgrade}{vf}) : ($FORM_HTML{upgrade}{pf});

        $FHTMLTMPL  = G_S::Get_Object($db, $page, 1)
         || die "Could not find object $page $!\n";

        $FHTMLTMPL =~ s/%%alias%%/$data->{alias}/g;

        print $FHTMLTMPL;

}

sub Create_controls
{
	foreach (keys %field)
	{
	###### now we'll convert the 'value' to the needed form element with the value included
		if ($_ eq 'Country')
		{
			###### country is a special drop down box
			$field{$_}{value} = G_S::cbo_Country($q, 'Country', $field{$_}{value}, 'in');
		}
		elsif ($_ =~ /Prefix/)
		{
			$field{$_}{value} = $q->popup_menu(
				-name=>$_,
				-class=>'in',
				-default=> $field{$_}{value},
				-values=>['', 'Mr.', 'Mrs.', 'Ms.', 'Miss', 'Dr.']);
		}
		elsif ($_ =~ /Suffix/)
		{
			$field{$_}{value} = $q->popup_menu(
				-name=>$_,
				-class=>'in',
				-default=> $field{$_}{value},
				-values=>['', 'Sr', 'Jr', 'III', 'IV', 'V', 'Phd.']);
		}
	}
}

sub Create_hidden
{
	my $hid = '';
	my $modifier = shift || '';

	if ($modifier eq 'info_only')
	{
	###### we only want the informational params for our hash comparisons
		foreach (keys %field)
		{
			$hid .= $q->hidden($_)."\n";
		}
		$hid .= $q->param('id')."\n";
		$hid .= $q->param('spid')."\n";
		$hid .= $q->param('alias')."\n";
	}
	else
	{

		foreach ($q->param())
		{
	###### we need to pass all our parameters that have values
	###### except the action param and the ID hash param
	#		$hid .= $q->hidden($_)."\n" if ($_ ne 'action' && $_ ne 'idh');
			$hid .= $q->hidden($_)."\n" if ($_ ne 'action');
		}
	}
	return $hid;
}

sub Create_tmp_record
{
###### first we have to get any existing 'authorized' value to carry over into the new record
###### until we are fully migrated to appx, we need to pull the amount out as well
###### for admin
	my $sth = $db->prepare("SELECT authorized, amount FROM $UPGRADE_TMP_TBL WHERE id= $data->{id}");
	$sth->execute;
	my ($authz, $amount) = $sth->fetchrow_array;
	$sth->finish;

	if ($DBI::err)
	{
		DB_error("DBI problem trying to retrieve a tmp record in Create_tmp_record().");
		return 0;
	}
	$authz = 0 unless $authz;

	my $o = $q->param('o');
	my $alias = uc $q->param('alias');
#	my $qry = "DELETE FROM $UPGRADE_TMP_TBL WHERE tmp_id = $o;\n";
	my $qry = "DELETE FROM $UPGRADE_TMP_TBL WHERE id = $data->{id};\n";
	$qry .= "INSERT INTO $UPGRADE_TMP_TBL (tmp_id, id, alias, utype, amount, authorized";
	my @VALS;

# At this point we munge state2 into state if so that they become one and the same again.

        if ( $field{'State2'} ne "" )
        {

         if ( ! $field{'State'} {value}  )
         {

          # We have selected state2 from the drop down! 
          $field{'State'} {value} = $field{'State2'} {value} ;
          delete $field{'State2'};
         }
        }

	foreach (keys %field){
		push (@VALS, $_) if $field{$_}{value} gt '';
	}

	foreach (@VALS){ $qry .= ", $_" }
	$qry .= ") VALUES($o, $data->{id}, '$alias', '$type', '${\($amount || $UPGRADE_TYPE{$type}{amt})}', $authz";
	foreach (@VALS){ $qry .= ", " . $db->quote($field{$_}{value}) }
	$qry .= ")";
#print "<b>Debug</b><br> QUERY = $qry<br>\n";
	$db->do($qry);
	if ($DBI::err){ DB_error("DBI problem with transaction in Create_tmp_record()."); return 0}
	return 1;
}

sub DB_error
{
###### if we have already begun outputting a page, we don't want html headers again
	my ($msg, $flag) = @_;
	if ($flag){ print '<br />' }
	else{ print $q->start_html(-title=>'Database Error!', -bgcolor=>'#ffffff') }
	
	print "There has been a database error. The problem and the specific error are shown below.<br>\n";
	print "Please " . $q->a({-href=>"mailto:$DB_error_contact"}, 'contact')." us with this information.<br>";
	print "<b>Problem:</b> $msg<br><b>Error:</b> $DBI::errstr\n";
	print $q->end_html();
}

sub Do_Page
{
###### if in ADMIN mode and pulling from a temp upgrade record, we'll be receiving a flag
###### we can then alter our ADMIN mode message to indicate this fact
	my $msg = 'ADMINISTRATIVE MODE';
	$msg .= ' Pulling from TMP upgrade record' if $_[0];
	my $sub = $q->span({-style=>'font-size: 18pt; color: red; font-weight: bold;'}, $msg);
	my $hidden = Create_hidden();
	my $usg = ($data->{membertype} eq 'm' && $UPDATE) ? ('m_update') : ($usage);
	

###### we will substitute the normal header graphic with a text header if we are using this as ADMIN
###### in the event someone messes with the html template we at least don't have to be concerned about
###### extraneous admin verbiage showing up on the page

        $FHTMLTMPL  = G_S::Get_Object($db, $FORM_HTML{$usg}{form}, 1)
               || die "Could not find object $FORM_HTML{$usg}{form} $!\n";


        #warn "Template being retreived ", $FORM_HTML{$usg}{form}, "\n";
	#warn "Template point 2\n";

        if ($ADMINMODE)
        {
         $FHTMLTMPL =~ s#(<!-- MARKER1 DO NOT REMOVE -->)(.*)(<!-- /MARKER1 DO NOT REMOVE -->)#$sub#s
        }
        
        my $state_list =  cbo_State_List('State2');
	 my $language_pref = cboLanguage();
 
        $FHTMLTMPL =~ s/%%message%%/$dover_msg/;
        $FHTMLTMPL =~ s/%%hidden%%/$hidden/;

        $FHTMLTMPL =~ s/%%action%%/$MYURL/;
        $FHTMLTMPL =~ s/%%timestamp%%/scalar localtime()/e;
        $FHTMLTMPL =~ s/%%state_list%%/$state_list/g;
        $FHTMLTMPL =~ s/%%language_pref%%/$language_pref/g;

        #warn "Country retreived is ",$data->{country},"\n";

        if ( ! $data->{country} ) 
        {
         $data->{country} = "US";
        }

        if (  $data->{country} ne "US" )
        {
          #warn "Country is NON-US\n";
          $FHTMLTMPL =~ s/%%State%%/$data->{state}/g;
        }
          else
        {
         #warn "Country is US\n";
         $FHTMLTMPL =~ s/%%State%%//g;
         $field{'State'}{value} = '';
        }

	foreach my $key (keys %field)
	{
                $FHTMLTMPL =~ s/%%$key%%/$field{$key}{value}/g;
                $FHTMLTMPL =~ s/%%class_$key%%/$field{$key}{class}/g;
	}

        print $FHTMLTMPL;
}

sub DuplicateMOP_Auth
{
###### param 1 = CC or ACH, param 2 is the acct. number, param 3 is our member ID
	my ($col, $acct, $id) = @_;
	my $cnt = 0;
	my $tmp;

	$col = ($col eq 'CC') ? 'cc_number' : 'account_number';

###### by counting only records that don't match the ID, we'll avoid the match we would have when a Partner
###### wants to upgrade to VIP with an account we already have on file
	my $sth = $db->prepare("SELECT id FROM mop WHERE $col = ?");
	$sth->execute($acct);
	while ($tmp = $sth->fetchrow_array){
		$cnt ++ if $id != $tmp;
	}
	$sth->finish;
	return $cnt;
}

sub Error_G{
	print $q->start_html(-bgcolor=>'#ffffff');
	print $q->h3($_[0]), scalar localtime(), $q->end_html;
}

sub ErrMsg
{
###### provides a simple error report from which they will have to back out to correct field input errors
	print qq|<h3 style="color: #ff0000;">Some required information was missing or erroneous.</h3>|;
	print qq|<h4>Please note the message/s below and press your 'Back' button to provide the information.</h4>|;
	print qq|<ul style="font-weight: bold; color: #ff0000;">\n|;
	foreach (@_){	print "<li>$_</li>\n"}
	print '</ul>', scalar localtime(), '</body></html>';
}

sub Failure_notification
{
###### if we are passed a flag, then we need to eliminate the html header
	my ($msg, $flag) = @_;

        $FHTMLTMPL  = G_S::Get_Object($db, $FORM_HTML{upgrade}{failure} , 1)
               || die "Could not find object $FORM_HTML{upgrade}{failure} $!\n";

        undef $/;

	$FHTMLTMPL =~ s#.+<!-- DO NOT REMOVE THIS COMMENT -->##msi if $flag;
	$FHTMLTMPL =~ s/%%message%%/$msg/g;

        $/ = "\n";
        
        print $FHTMLTMPL;
}

sub Finish_header
{
###### output the html for the start of our final upgrade page
###### the rest will be output by the script and confirmation()

        $FHTMLTMPL  = &G_S::Get_Object($db, $FORM_HTML{upgrade}{congrats_header}, 1)
               || print $q->start_html(),  "Could not find object $FORM_HTML{upgrade}{congrats_header} $!\n";


        print $FHTMLTMPL;
}

sub Fraud_Check
{
	return if $ADMINMODE;

###### determine if they have matched or exceeded the max number of authorization attempts
###### we'll handle 'error' reporting here since it will be a standard message

	my $qry = qq#SELECT MIN(authorized) FROM(
SELECT authorized FROM $UPGRADE_TMP_TBL WHERE id = $data->{id}
UNION ALL
SELECT authorized FROM $UPGRADE_TMP_ARC_TBL WHERE id = $data->{id}
) AS tmp#;

	my $sth = $db->prepare($qry);
	$sth->execute;
	my $ck = $sth->fetchrow_array;
	$sth->finish;



###### since we may have a NULL result set, we'll first check for the existence of a value
	if ($ck && $ck <= -$FRAUD_COUNT)
	{
		Failure_notification($FRAUD_ALERT);
		Fraud_Notify() if $FRAUD_NOTIFY;
		return 1;
	}
}

sub Fraud_Notify
{
###### notify HQ about a 'fraud' attempt
	unless ($FRAUD_NOTIFICATIONS_ADDRESS){ die "Couldn't retrieve the fraud notification address - 9006\n" }
	my $NOTIFICATIONS_CC = G_S::Get_Object($db, 9007, 1);

	open (MAIL, "|$mailprog -t") || return;
	print MAIL "From: app.cgi\n";
	print MAIL "To: $FRAUD_NOTIFICATIONS_ADDRESS\n";
	print MAIL "CC: $NOTIFICATIONS_CC\n" if $NOTIFICATIONS_CC;
	print MAIL "Subject: Potential Fraud ALERT\n";
	print MAIL "The following party has tried to upgrade beyond the max decline count of $FRAUD_COUNT.\n";
	print MAIL "ID: $data->{id}\n";
	print MAIL "More information is available in the temp upgrade record table and its archive\n";
	close MAIL;
}

sub Get_Day
{
	my @lastday = (31,28,31,30,31,30,31,31,30,31,30,31);
	$lastday[1] = 28 + &isLeapYear();
	return $lastday[$_[0]];
}

sub Get_Month_Year
{
	my @month = qw/January February March April May June July August September October November December/;
	my @t = localtime;
	return ($month[$t[4]], $t[5] + 1900, $t[4]);
}

sub Get_Tmp_Record
{
	my $o = $q->param('o');
	my $id = $q->param('id');
	unless (($id && $id !~ /\D/) && ($o && $o !~ /\D/))
	{
		Error_G('Missing required parameters');
		return;
	}

###### rather than trying to handle the repercussions of an error here, we'll just let DBI do it
	$db->{RaiseError} = 1;

###### since we now need the spid at the end for email notifications, we'll have to bring it in here
	my $sth = $db->prepare("	SELECT u.*,
						m.spid
					FROM 	$UPGRADE_TMP_TBL u,
						members m
					WHERE 	u.id= $id
					AND 	u.id=m.id
					AND 	u.tmp_id= $o");
	$sth->execute;
	my $rec = $sth->fetchrow_hashref;
	$sth->finish;
	
	$db->{RaiseError} = 0;
	
	unless ($rec)
	{
		Failure_notification(qq#
			We cannot find a pending upgrade record on you.<br />
			You will have to start over.<br /><br />
			<a href="https://www.clubshop.com/VIP_app.html">Click here</a> to do so.#);
		return;
	}
	###### coerce the utype for now
	$rec->{utype} = 'M2V';
	return $rec;
}

sub isLeapYear
{
	my @t = localtime;
	my $y = $t[5];
	($y % 4 == 0) && !(($y % 100 == 0) && ($y % 400 != 0));
}

sub ISP_address_ck
{
###### check to see if an emailaddress is in the 'throw_away_domains' list
###### if it is NOT we'll assume it is a valid ISP address and return a 'true' flag
	my ($add) = @_;

###### extract the domain from the address ( we are assuming we are receiving a valid address)
	$add =~ s/(.+@)(.+)/$2/;
	$add = lc $add;

	my $sth = $db->prepare("SELECT domain FROM throw_away_domains WHERE domain ~ ?");
	$sth->execute($add);
	my $res = $sth->fetchrow_array; $sth->finish;
	return ($res) ? (0) : (1);
}

sub Load_Data
{
###### this procedure presumes that the field keys are identical to the passed parameters
###### we'll only populate the form if a vip is using it for updating or we are in administrative mode
	if ($UPDATE || $ADMINMODE)
	{
		foreach my $key (keys %field)
		{
			$field{$key}{value} = $data->{lc($key)} || '';
		}
	}
	else
	{	###### to protect confidentiality, we will only fill in the name if this is an upgrade
		$field{FirstName}{value} = $data->{firstname} || '';
		$field{LastName}{value} = $data->{lastname} || '';
	}

###### remove spaces in the area code field since they cause problems in the browser form
#	$field{Phone_AC}{value} =~ s/\s//g;
}

sub Load_Params
{
###### populating our field array hash values
###### this loop depends on the %field hash keys being identical to the passed parameter names
	foreach (keys %field)
	{
		$field{$_}{value} = $q->param($_) || '';
		next if (not $field{$_}{value});

###### filter out leading & trailing whitespace
		$field{$_}{value} =~ s/^\s*//;
		$field{$_}{value} =~ s/\s*$//;

###### let's filter for extraneous characters (this may pose a problem with other character sets)
###### we will allow digits in the 'Company Name' field
		if ($_  eq 'Company_Name')
		{
			$field{$_}{value} =~ s/('*)( ?)(\W*)/$1$2/g;
		}

###### we'll be more restrictive on the other name fields
		elsif ($_ =~ /name/i)
		{
		###### we want to keep any spaces and apostrophes but remove non letters
		###### we will also allow hyphens and periods
			$field{$_}{value} =~ s/('*)( ?)(-|\.*)(\W*)(\d*)/$1$2$3/g;
		}
		elsif ($_ =~ /email/i)
		{
			$field{$_}{value} =~ s/,|\t|\r|\n|\f|\[M|`|"|;|:|\+|=|\s|!|#|%|\^|&|\*|~//g;

###### we will lower case all emailaddresses
			$field{$_}{value} = lc($field{$_}{value});
		}
		else
		{
###### inevitably someone needs a character that isn't alphanumeric
###### since I'm tired of filtering madness, I am going to just let 'em enter what they want
#			$field{$_}{value} =~ s/('?)( ?)(,?)(\.?)(\W*)(_*)/$1$2$3$4/g;
		}
		if ($_ =~ m/city|state|name/i)
		{
###### we will upper case all the first letters of the name fields, city, state
			$field{$_}{value} = ucfirst($field{$_}{value});
		}
	}
}

sub Mail_Spam_Policy
{
        my $STMPL = G_S::Get_Object($db, 10087, 1) || die "Failed to load template: 10087";

        $STMPL =~ s/%%email%%/$data->{emailaddress}/g;
        $STMPL =~ s/%%firstname%%/$data->{firstname}/g;

###### perform AOL link creation if we are mailing to an AOL address
		if ($data->{emailaddress} =~ m/\@aol\.com/)
		{
			$STMPL =~ s#(http\S*)#<a href="$1">$1</a>#g;
		}

	open (SPAMMAIL, "| $mailprog -t");
	print SPAMMAIL $STMPL,"\n";
	close (SPAMMAIL);
}

sub Mailto
{
###### first param is the mail type, second is the sponsor hashref if there is one
###### with all upline notifications going out from a backend process, we'll negate 'upline' stuff for now
	my ($mtype) = @_;

###### get the first upline VIP for contact info
	my $sp = G_S::Get_VIP_Sponsor($db, $data->{spid});
	G_S::ConvHashref('utf8', $sp);
	G_S::ConvHashref('utf8', $data);

	my $MSG = G_S::Get_Object($db, $email_notifications{$mtype}{$data->{utype}}) 
          || die "Couldn't get notifications object $email_notifications{$mtype}{$type}";

	$MSG =~ s/%%SpFirstName%%/$sp->{firstname}/g;
	$MSG =~ s/%%SpLastName%%/$sp->{lastname}/g;
	$MSG =~ s/%%SpEmail%%/$sp->{emailaddress}/g;
#	$MSG =~ s/%%upline%%/$upline/;
	$MSG =~ s/%%firstname%%/$data->{firstname}/g;
	$MSG =~ s/%%lastname%%/$data->{lastname}/g;
	$MSG =~ s/%%email%%/$data->{emailaddress}/g;
	$MSG =~ s/%%alias%%/$data->{alias}/g;
	$MSG =~ s/%%password%%/$data->{password}/g;
	$MSG =~ s/%%id%%/$data->{id}/g;

###### perform AOL link creation if we are mailing to an AOL address
#	if ($data->{emailaddress} =~ m/\@aol\.com/){
#		$MSG =~ s#(http\S*)#<a href="$1">$1</a>#g;
#	}


#	open (MAIL, "|$mailprog -t") || die "Couldn't open $mailprog";
#	print MAIL $MSG;
#	close MAIL;
	my $hdr = ParseMe::Grab_Headers(\$MSG);
	MailingUtils::DrHeaders($hdr);
	my %mail = (
		%{$hdr},
		'Content-Type' => 'text/plain; charset="utf-8"',
		message=> $MSG );
	unless ( sendmail(%mail) )
	{
		Error_G("There has been an error sending you a confirmation email.<br />\n$Mail::Sendmail::error\n");
	}

}

sub Notifications
{

	Congratulations();

###### we will notify all upline from the backend processor since we could have any membertype as a sponsor
#	Mailto('sponsor');

	Mailto('member');

###### mail them the spam policy
# disabled as of 01/08/03
#	&Mail_Spam_Policy();
}

sub Opt2003
{
	# rehooked 02-05-04
	#return '';

	###### create a notification hook for inclusion with our upgrade SQL
	###### first get some necessary live data
	my $sth = $db->prepare("SELECT id, spid, vip_date FROM members WHERE id= ?");
	$sth->execute($data->{id});
	my $dat = $sth->fetchrow_hashref || die "Member record lookup failed";
	$sth->finish;

	###### since this opt2003 program is available only to new upgrades
	###### members with a previous upgrade are ineligible
	return '' if $dat->{vip_date};

	return "INSERT INTO notifications (
			id,
			spid,
			notification_type
		) VALUES (
			$dat->{id},
			$dat->{spid},
			2003
		);\n";
}

sub PayPal_form
{
###### create a hash to pass to the PayPal module from which will be returned a form to submit to
###### PayPal for creating a new sucscription
	require PayPal;

	my %vals;
	my $NAME = $q->url();

###### this is the link they will go to if they 'Cancel' at the PayPal site
###### before finishing their subscription setup
	$vals{cancel_return} = "$NAME?action=MOP&o=$data->{tmp_id}&id=$data->{id}";

###### this is the link they will be taken to if they complete their PayPal subscription
	$vals{return} = "$NAME?action=ppreturn&o=$data->{tmp_id}&id=$data->{id}";


	$vals{item_name} = ($data->{utype} =~ /V/) ? ('VIP Subscription') : ('Partner Subscription');

	$vals{id} = $data->{id};

	$vals{firstname} = $data->{firstname};
	$vals{lastname} = $data->{lastname};
	$vals{address1} = $data->{address1};
	$vals{address2} = $data->{address2} if $data->{address2};
	$vals{city} = $data->{city};
	$vals{state} = $data->{state};
	$vals{postalcode} = $data->{postalcode};

	if ($data->{utype} =~ /V/){

###### first we need to find out how many days until the next billing cycle begins
###### we'll calculate the first period to be equal to that number
###### that way all of our subscriptions will be billed on the same day
		my $qry = "SELECT date_part('day', (
(
date_part('month',(now() + interval '1 month' ))::text
|| '/$BILLING_DAY/' ||
date_part('year',(now() + interval '1 month' ))::text)::date - now()
))";
		my $sth = $db->prepare($qry); $sth->execute;
		my $days2go = $sth->fetchrow_array; $sth->finish;

		$vals{initial_amount} = $data->{amount};
		$vals{initial_period} = $days2go;

		$vals{recurring_amount} = $MONTHLY_FEE;
		$vals{recurring_period} = 1;
		$vals{recurring_period_units} = "M";
 
		$vals{recurring_flag} = 1;
	}else{
		$vals{recurring_amount} = $data->{amount};
		$vals{recurring_period} = 1;
		$vals{recurring_period_units} = "Y";
	}
 
	return &PayPal::PP_subscr(\%vals);
}

sub Prefill{
###### this routine just reads in the static application page and substitutes a prefilled textbox for the ID
	open (PAGE, '/home/httpd/html/VIP_app.html') || return 0;
	undef $/;
	my $page = <PAGE>; close PAGE;
	$page =~ s/<!-- TAG 1 -->.*<!-- TAG 2 -->/<input type="text" size="20" name="alias" value="$alias" \/>/s;
	print $q->header(), $page;
	return 1;
}

sub Present_MOP{
###### output a payment options page from which they will decide how they are going to proceed with payment
###### since we don't want to have to alter the script every time eCheck and CC are to be disabled
###### we'll check our active status from the DB
	my ($options) = ();
	my $sth = $db->prepare("SELECT method, description, active FROM payment_methods ORDER BY method");
	$sth->execute;
	my $dat = $sth->fetchall_arrayref; $sth->finish;
	foreach (@{$dat}){

###### if we have PayPal hooks turned off, we don't want to render that option even if the DB says its active
		unless ($PayPal_YES){ next if $_->[0] =~ /PP/ }
		$options .= qq#<input type="radio" name="action" value="MOP_$_->[0]" /> $_->[1]<br />\n# if $_->[2] eq '1';
	}

	my $hid = $q->hidden('id') . $q->hidden('o');
	$hid .= $q->hidden('upgradedate') if $ADMINMODE;

        $FHTMLTMPL  = &G_S::Get_Object($db, $FORM_HTML{upgrade}{MOP}, 1)
               || die "Could not find object $FORM_HTML{upgrade}{MOP} $!\n";


        #warn "Template being retreived ", $FORM_HTML{upgrade}{MOP}, "\n";
        #warn "Template point 3\n";

        $FHTMLTMPL =~ s/%%action%%/$MYURL/g;
	$FHTMLTMPL =~ s/%%hidden%%/$hid/g;
	$FHTMLTMPL =~ s/%%OPTIONS%%/$options/g;
	$FHTMLTMPL =~ s/%%timestamp%%/scalar localtime()/e;

        print $FHTMLTMPL;

}

sub Process_Order
{
###################
# Authorize Order #
###################
###### this routine returns either 1 for success, 0 for failures that handle their own errors,
###### or an actual failed authorization error message

	require '/home/httpd/cgi-lib/ccauth.lib';

	my (@problem, %FORM, @ERR, $reason, $iaddr, $paddr, $autoship, $desc);
	my $date = scalar localtime;
	my %ERRMSG = (
		AMOUNT => 'No transaction amount was received',
		toa_agreement => 'You must check one of the Fee Authorization Agreement buttons',
		ACCTNO => 'You must provide your Checking Account Number',
		type_of_card => 'You must select the type of card you wish to use',
		payment_type => 'You must select a payment type, i.e. Credit Card or Check',
		CARDNUM => 'You must provide a Card Account Number',
		BANKNAME => 'You must provide the name of your Checking Account Bank',
		ABACODE => "You must provide the 'Routing Number' from your check",
		EXPDATE => 'You must select an Expiration Date',
		account_holder_fullname => 'The Account Holder Name cannot be blank'
	);

###### this is where we pull in the payment info parameters
###### ###### ###### ###### ###### ######
	
	my @pay_params = qw/AMOUNT toa_agreement ACCTNO type_of_card payment_type
		CARDNUM BANKNAME ABACODE EXPDATE account_holder_fullname
		account_holder_address account_holder_city account_holder_state
		account_holder_state2 account_holder_zip account_holder_country
		card_code/;
	my $ACCT_NUM_FIELDS = 'ACCTNO CARDNUM ABACODE';

###### we'll hardcode the amount in instead of depending on a passed parameter which could be hacked
###### the parameter is still passed, but we're overwriting it
	$FORM{AMOUNT} = $UPGRADE_TYPE{$type}{amt};

	foreach (@pay_params)
	{
		###### 05/25/04 making the default '' instead of undef;
		$FORM{$_} = $q->param($_) || '';

###### filter out non digit characters on account number types
######	of fields which have a value
		if ($ACCT_NUM_FIELDS =~ /$_/ && $FORM{$_})
		{
			$FORM{$_} =~ s/\D//g;
		}
	}

###### get rid on any extraneous credit card number characters & check length
	$FORM{CARDNUM} =~ s/\D//g;
	if (length $FORM{CARDNUM} > 16)
	{
		push (@problem,
			'The credit card number submitted has too many characters.
			 The maximum number of digits is 16.');
	}

###### account_holder_state is examined and replaced with account_holder_state2 if it is empty

	if ( $FORM{account_holder_state} eq "" )
	{
		$FORM{account_holder_state} = $FORM{account_holder_state2};
	}

###### Lets verify we have the necessary information to process the transaction
###### first we check for our mandatory agreement and payment option buttons
	if (not $FORM{toa_agreement} or $FORM{toa_agreement} !~ m/yes|no/i)
	{	###### they didn't check an agreement box
		push (@problem, $ERRMSG{toa_agreement});
	}
	elsif ($FORM{toa_agreement} =~ m/no/i)
	{	###### they don't agree to the terms, so Sorry!
		return "You must agree to the payment terms or we can't proceed\n";
	}

	if (not $FORM{payment_type} or $FORM{payment_type} !~ m/cc|ach/i)
	{	###### no payment type chosen
		push (@problem, $ERRMSG{payment_type});
	}

###### next we check for specific payment info
	elsif ($FORM{payment_type} =~ m/cc/i)
	{	###### credit card payment

	###### force some data into card_code if they didn't provide it
	###### requested by Brenda 05/29/03 to cause code failures by those banks that support it
	###### but the code is not provided for checking
		$FORM{card_code} ||= '000';

		foreach('type_of_card','account_holder_fullname','CARDNUM','EXPDATE')
		{
			if ($FORM{$_} eq '')
			{	###### we have a blank that is required
				push (@problem, $ERRMSG{$_});
			}
		}
	}
	
	elsif ($FORM{payment_type} =~ m/ach/i)
	{	###### check payment
		###### first we'll check to make sure they are US people
		if ($FORM{account_holder_country} ne 'US')
		{
			return 'We cannot accept checks unless you live in the US';
		}
		else
		{
			foreach('BANKNAME','ACCTNO','ABACODE', 'account_holder_fullname')
			{
				if ($FORM{$_} eq '')
				{	###### we have a blank that is required
					push (@problem, $ERRMSG{$_});
	}	}	}	}

	if (scalar @problem > 0)
	{	###### we had some problems
		ErrMsg(@problem);
		return 0;
	}

	$desc = "VIP Upgrade";
	
###### get the next auto increment number
	my $sth = $db->prepare("SELECT nextval('inv_num_invoice_seq')");
	$sth->execute;
	my $invoice = $sth->fetchrow_array;
	$sth->finish;

	my %DATA = ();
	$DATA{order_description} = $desc;
	$DATA{id} = $data->{alias};
	$DATA{amount} = $FORM{AMOUNT};
	$DATA{invoice_number} = $invoice;
	$DATA{emailaddress} = $data->{emailaddress};
	$DATA{ship_address} = $data->{address1};
	$DATA{ship_city} = $data->{city};
	$DATA{ship_state} = $data->{state};
	$DATA{ship_zip} = $data->{postalcode};
	$DATA{ship_country} = $data->{country};
	$DATA{ship_firstname} =$data->{firstname};
	$DATA{ship_lastname} = $data->{lastname};
	$DATA{ship_company_name} = $data->{company_name};
	$DATA{phone} = $data->{phone};
	$DATA{fax} = $data->{fax_num};
	$DATA{account_holder_firstname} = '';
	$DATA{account_holder_lastname} = $FORM{account_holder_fullname};
	$DATA{account_holder_address} = $FORM{account_holder_address};
	$DATA{account_holder_city} = $FORM{account_holder_city};
	$DATA{account_holder_state} = $FORM{account_holder_state};
	$DATA{account_holder_zip} = $FORM{account_holder_zip};
	$DATA{account_holder_country} = $FORM{account_holder_country};
###### another processing message
	print qq|<span class="in">* * * Checking payment records</span><br>\n|;

	my $AUTHS = 0;
	if ($FORM{'payment_type'} eq "ACH") {
		$DATA{payment_method} = 'ECHECK';
		$DATA{bank_account_number} = $FORM{ACCTNO};
		$DATA{bank_name} = $FORM{BANKNAME};
		$DATA{aba_code} = $FORM{ABACODE};
		$AUTHS = &DuplicateMOP_Auth('ACH', $FORM{ACCTNO}, $data->{id});
	}else{
		$DATA{payment_method} = 'CC';
		$DATA{cc_number} = $FORM{CARDNUM};
		$DATA{cc_expiration} = $FORM{EXPDATE};
                $DATA{card_code} = $FORM{card_code};
		$AUTHS = &DuplicateMOP_Auth('CC', $FORM{CARDNUM}, $data->{id});

	}
	if ($AUTHS >= $MAX_AUTHS){
		my $msg = qq|The maximum allowable number of memberships authorized on this account has been reached
<br />Please click <a href="http://www.clubshop.com/authorizationindex.html">here</a> for more information.\n|;
		return $msg;
	}

###### if we got this far, we'll tell 'em their application has been accepted for processing.
	print $PROCESSING_ACCEPTANCE_MSG;	

###### update the upgrade_tmp record with the MOP info
	my $qry = "UPDATE $UPGRADE_TMP_TBL set payment_method= '$DATA{payment_method}',";
	$qry .= "acct_holder= " . $db->quote($FORM{account_holder_fullname}) . "," if $FORM{account_holder_fullname};
	$qry .= "mop_address= " . $db->quote($DATA{account_holder_address}) . "," if $DATA{account_holder_address};
	$qry .= "mop_city= " . $db->quote($DATA{account_holder_city}) . "," if $DATA{account_holder_city};
	$qry .= "mop_state= " . $db->quote($DATA{account_holder_state}) . "," if $DATA{account_holder_state};
	$qry .= "mop_postal_code= " . $db->quote($DATA{account_holder_zip}) . "," if $DATA{account_holder_zip};
	$qry .= "mop_country= " . $db->quote($DATA{account_holder_country}) . "," if $DATA{account_holder_country};
	$qry .= "cc_number= " . $db->quote($DATA{cc_number}) . "," if $DATA{cc_number};
        $qry .= "card_code= " . $db->quote($DATA{card_code}) . "," if $DATA{card_code};
	$qry .= "cc_exp= " . $db->quote($DATA{cc_expiration}) . "," if $DATA{cc_expiration};
	$qry .= "account_number= " . $db->quote($DATA{bank_account_number}) . "," if $DATA{bank_account_number};
	$qry .= "bank_name= " . $db->quote($DATA{bank_name}) . "," if $DATA{bank_name};
	$qry .= "routing_number= " . $db->quote($DATA{aba_code}) . "," if $DATA{aba_code};

###### cutoff the trailing comma
	chop $qry;
	$qry .= " WHERE id = $data->{id}";

	$db->do($qry);
	if ($DBI::err){&DB_error("Couldn't update your temporary upgrade record", 1); return 0}

###### another processing message
	print qq|<span class="in">* * * Processing your payment with Authorize.net<br>\n|;
	print qq|<b style="color: #ff0000">Unless you get a 'Congratulations' screen, do not assume your upgrade is complete.</b><br />\n|;
	print qq|Instead, check your email for an authorize.net receipt in a few minutes. If you have one, 
<b style="color: #ff0000">DO NOT</b> submit your application again. You may be charged twice.</span>\n|;

###### following line to bypasses authorization when testing
###### ###### bypass actual authorization
	goto 'SKIP' if $BYPASS_PAYMENT_AUTHORIZATION;

	my ($success, $auth_problem) = &CCAuth::Authorize(\%DATA);

       if($success != 1) {
###### we want to keep track of failed authorizations
		$qry = "UPDATE $UPGRADE_TMP_TBL SET authorized= authorized -1, last_modified= NOW(), 
authz_results= ? WHERE id = $data->{id}";

###### get rid of embedded html and leading & trailing spaces before we insert the reason into the DB
		my $prob = $auth_problem; $prob =~ s/<.*?>//g; $prob =~ s/^\s*|\s*$//g;
		$db->do($qry, {}, $prob);

###### now we'll archive the record in case they go back and try some different information
###### we can't roll it into a transaction since the one must be updated before it can be inserted
	$db->do("INSERT INTO $UPGRADE_TMP_ARC_TBL SELECT * FROM $UPGRADE_TMP_TBL WHERE id = $data->{id}");

###### if they are at the max declines with this decline, we'll let 'em know not to try again
		if (abs ($data->{authorized} -1) >= $FRAUD_COUNT){
			$auth_problem .= qq#<br /><br />You have reached the maximum number of declined attempts.<br />
Please contact <a href="mailto: $FRAUD_NOTIFICATIONS_ADDRESS">$FRAUD_NOTIFICATIONS_ADDRESS</a> for assistance.#;
		}
	 	push (@problem, $auth_problem);
		return "@problem\n";
       }
SKIP:

###### another processing message
	print qq|<span class="in">Congratulations, your payment was processed</span><br>\n|;

###### we will mark their tmp record authorized in case anything goes wrong beyond here
###### that way we can easily do the upgrade later
	$qry = "UPDATE $UPGRADE_TMP_TBL SET authorized= 1, last_modified= NOW(), authz_results= NULL,
authorized_date= NOW()
WHERE id = $data->{id}";
	$db->do($qry);
	return 1;
}

sub Update_Record
{
###### this routine handles INFO updates only, UPGRADES are handled at Upgrade()

###### putting some of the parameters through the DBI quoting function even though they should not all need it
###### this is an information update only, they are not allowed to change any of the name information

if ( $field{State}{value} eq "" )
{
 $field{State}{value} = $field{State2}{value};
}

	my $qry = "
		UPDATE members SET
			Address1= ?,
			Address2= ?,
			City= ?,
			State= ?,
			PostalCode= ?,
			Country= '$field{Country}{value}',
--			Phone_AC= ?,
			Phone= ?,
			Fax_Num = ?,
			TIN= '$field{TIN}{value}',
			Password= '$field{Password}{value}',
			EMailAddress= '$field{EMailAddress}{value}',
			EMailAddress2= '$field{EMailAddress2}{value}',
			language_pref= '$field{language_pref}{value}',
			operator= 'app.cgi as update',
			stamp= NOW(),
			memo = 'information update by member'
		WHERE id = $data->{id}";


	my $sth = $db->prepare($qry);
# DEBUG QUERY 
#print "query = $qry\n";
	$sth->execute(
		$field{Address1}{value},
		$field{Address2}{value},
		$field{City}{value},
		$field{State}{value},
		$field{PostalCode}{value},
#		$field{Phone_AC}{value},
		$field{Phone}{value},
		$field{Fax_Num}{value});
	$sth->finish;
}

sub Upgrade
{
die "This application should not be used for upgrades\n";
###### this routine handles the actual upgrade
	my ($mtype, $qry_modifier, $mop_field, $paid_thru, $sth) = ();

###### first we'll get our tmp record
	return unless $data = Get_Tmp_Record() ;

###### we'll capture the IP address for the records
	my $ipaddr = $q->remote_addr();

	print qq|<span class="in">* * * Captured your IP address as: $ipaddr</span><br />\n|;

###### if we got here directly because of an authorized tmp upgrade transaction,
###### we'll set the upgrade date to the date of the authorization
	my $now = ($data->{authorized_date}) ? ("'$data->{authorized_date}'") : ('NOW()');

###### if we are in ADMIN MODE we may have a post dated upgrade
	if ($ADMINMODE && $q->param('upgradedate'))
	{
		$now = "'" . $q->param('upgradedate') . "'";
	}

###### when bypass = YES use the specified paid thru date
	if ($action eq 'bypass' && $ADMINMODE)
	{

		# generate an end of month date for the selected month
		$sth = $db->prepare("SELECT date_trunc('month', ?::DATE + interval '1 month' )::DATE -1");
		$sth->execute( ($q->param('paidthruyr') . "-" . $q->param('paidthrumo') . "-01") );
		$paid_thru = $sth->fetchrow_array;
		$sth->finish;
	}
	else
	{
###### now lets get our end of the month date so we can set the paid_thru date

###### this array holds the numeric end of month day,
###### (end of Feb. is incorrect for leap year, but it works) since Jan. is month 1, we'll pad [0]
		my @eom = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

		$sth = $db->prepare("
			SELECT DATE_PART('month',
			CAST($now AS DATE)) AS month,
			DATE_PART('year', CAST($now AS DATE)) AS year"
		);
		$sth->execute;
		my $tmp = $sth->fetchrow_hashref;
		$sth->finish;
		$paid_thru = "$tmp->{month}/$eom[$tmp->{month}]/";

		$paid_thru .= $tmp->{year};
	}

###### opt2003 hook
	my $qry = Opt2003();
	print qq|<span class="in">* * * Flagging you for the 3 for Free program</span><br />\n| if $qry;

###### an update message
	print qq|<span class="in">* * * Updating your records</span><br />\n|;

###### now we'll create the member record update
	$qry .= "
		UPDATE members SET
			membertype= 'v',
			status= 1,
			operator= 'app.cgi',
			stamp= NOW(),
			memo= 'member upgrade\nIP: $ipaddr', ";
	
	$qry .= "vip_date= $now, ";

	foreach (keys %field)
	{
		$_ = lc $_;
		$qry .= "$_= ${\$db->quote($data->{$_})}," if ($data->{$_});
	}
	chop $qry;
	$qry .= " WHERE id= $data->{id};\n";

###### now we'll loop through the the tmp record mop fields and create a mop update with those values
	$qry .= "	DELETE FROM mop WHERE id = $data->{id};
			INSERT INTO mop (id, paid_thru,";

	$qry_modifier = "$data->{id}, '$paid_thru',";

	foreach (qw/	acct_holder
			cc_number
			cc_exp
			bank_name
			routing_number
			account_number
			payment_method
			mop_address
			mop_city
			mop_state
			mop_postal_code
			mop_country
			card_code/)
	{

###### our tmp record mop fields are the same as the mop table with a 'mop_' prepended
		($mop_field = $_) =~ s/mop_//;
		if ($data->{$_})
		{
			$qry .= "\n$mop_field,";
			$qry_modifier .= "\n${\$db->quote($data->{$_})},";
		}
	}
	chop $qry_modifier;
	chop $qry;

	$qry .= ") VALUES ($qry_modifier);\n";

###### now we'll delete our tmp upgrade record
	$qry .= "DELETE FROM upgrade_tmp WHERE id = $data->{id};\n";

###### set a notifications flag on a VIP upgrade
#	my $ntype = ($utype eq 'v') ? 4 : 6;
	$qry .= "INSERT INTO notifications (
			id,
			notification_type
		) VALUES (
			$data->{id},
			4
		);\n";

	###### insert our CA subscription transaction
	$qry .= "INSERT INTO transactions (
			id,
			spid,
			membertype,
			trans_type,
			vendor_id,
			amount,
			description,
			trans_date,
			entered,
			cv_mult,
			reb_mult,
			comm_mult,
			rebate,
			reb_com,
			pp,
			operator
		) VALUES (
			$data->{id},
			$data->{spid},
			'v',
			8,
			828,
			$UPGRADE_TYPE{$type}{amt},
			${\(($type eq 'M2V') ? 	'\'Initial CA subscription\'' :
							'\'Reactivated CA subscription\'')},
			NOW(),
			NOW(),
			1,
			0,
			0,
			0,
			0,
			$UPGRADE_TYPE{$type}{pp},
			'Upgrade App'
		)";

	$db->do($qry);

###### the following is for debugging query problems
#$qry =~ s/\n/<br />/g; print "$qry\n";
}

sub Upgrade_Payment_Form
{
	my ($line, $TEXT, $mop_name_field) = ();

###### if we are in ADMIN mode we will allow the mop account holder's name to be changed
	if ($ADMINMODE)
	{
		$mop_name_field =  qq|<input type="text" size="30" maxlength="30" name="account_holder_fullname" value="%%FirstName%% %%LastName%%" />\n|;
	}
	else
									{
		$mop_name_field = qq|
			<input type="hidden" name="account_holder_fullname" value="%%FirstName%% %%LastName%%" />
			<table border="1" cellspacing="0"><tr>
			<td bgcolor="#beccdc" class="in">%%FirstName%% %%LastName%%
			</td></tr></table>\n|;
	}

###### first we'll verify that we have a tmp record to work with
	return unless $data = Get_Tmp_Record();
	my $PayPal = ($action =~ /PP/i) ? (PayPal_form()) : ('');

###### now update their payment method before continuing
	my $PM = '';
	($PM = $action) =~ s/(\w*)(_)(\w)/$3/;

	$db->do("
		UPDATE $UPGRADE_TMP_TBL SET
			payment_method= '$PM'
		WHERE id= $data->{id}");
	if ($DBI::err)
	{
		DB_error("Failed to set payment method in Upgrade_Payment_Form()");
		return;
	}

       $TEXT = G_S::Get_Object($db, $UPGRADE_TYPE{$data->{utype}}{file}, 1)
               || die "Could not find object $UPGRADE_TYPE{$data->{utype}}{file} $!\n";

       $FHTMLTMPL  = G_S::Get_Object($db, $FORM_HTML{upgrade}{$action}, 1)
               || die "Could not find object $FORM_HTML{upgrade}{$action} $!\n";
 

	my $hid = Create_hidden();
        

	my $state_list;
	my $province;

        $state_list = cbo_State_List('account_holder_state2');

        if ( $data->{country} eq "US" or $data->{country} eq "" )
        {
           $province = "";
        }
         else 
        {
           $province = $data->{state};
        }

	$FHTMLTMPL =~ s/%%utype%%/$data->{utype}/g;
	$FHTMLTMPL =~ s/%%mop_name%%/$mop_name_field/g;
	$FHTMLTMPL =~ s/%%action%%/$MYURL/g;
	$FHTMLTMPL =~ s/%%upgrade_type%%/$UPGRADE_TYPE{$data->{utype}}{text}/g;
	$FHTMLTMPL =~ s/%%hidden%%/$hid/g;
	$FHTMLTMPL =~ s/%%authorization_amt%%/$UPGRADE_TYPE{$data->{utype}}{amt}/g;
	$FHTMLTMPL =~ s/%%authorization_text%%/$TEXT/g;
	$FHTMLTMPL =~ s/%%FirstName%%/$data->{firstname}/g;
	$FHTMLTMPL =~ s/%%LastName%%/$data->{lastname}/g;
	$FHTMLTMPL =~ s/%%Address1%%/$data->{address1}/g;
	$FHTMLTMPL =~ s/%%City%%/$data->{city}/g;
	$FHTMLTMPL =~ s/%%State%%/$province/g;
	$FHTMLTMPL =~ s/%%State2%%/$state_list/g;
	$FHTMLTMPL =~ s/%%Zip%%/$data->{postalcode}/g;
	$FHTMLTMPL =~ s/%%Country%%/G_S::cbo_Country($q, 'account_holder_country', $data->{country}, 'in')/e;
	$FHTMLTMPL =~ s/%%PAYPAL_form%%/$PayPal/g;
	$FHTMLTMPL =~ s/%%timestamp%%/scalar localtime()/e;

	print $FHTMLTMPL;

}

sub Upline_List
{
	my $spid = $_[0];
	my ($sth, $qry, $email, $upline, $level);
	my $cnt = 0;
	my $exec = 0;
	while ($spid > 1 && $cnt < 3 && $exec == 0 )
	{
		$qry = "SELECT m.spid, m.emailaddress, COALESCE(v.a_level, 0) AS a_level
FROM members m LEFT JOIN a_level v ON v.id = m.id
WHERE m.id = $spid";
		$sth = $db->prepare($qry);
		$sth->execute; ($spid, $email, $level) = $sth->fetchrow_array; $sth->finish;
		$upline .= "$email,";
		$cnt++;
		$exec = 1 if ($level > 8 || $level >= $COACH_LEVEL);
	}
	chop $upline;
	return $upline;
}

sub Validate
{
###### first we'll check for trade restrictions to their selected country
	my $rv = G_S::Trade_Restriction_ck($db, $field{Country}{value});
	if ($rv)
	{
		Error_G($rv);
		return -1;
	}

	######	here we'll check required fields and assign new html classes as necessary
	foreach (keys %field)
	{

		if ($field{$_}{value} eq '' && $field{$_}{req} == 1)
		{
	###### a required field is empty
	###### we have to treat the TIN differently depending on the country
			next if ($_ eq 'TIN' && $field{Country}{value} ne 'US');

			$field{$_}{class} = 'r';					###### so we'll flag it
			$dover_flag = 1;
			$dover_msg = $q->span({-class=>'r'}, "Please fill in the required fields marked in <b>RED</b><br>\n");
		}
	}


	###### Perform various state & state2 checks
	###### 1st check if both State & State2 are empty require one to be input

	if ( $field{State}{value} eq '' && $field{State2}{value} eq '' )
	{
		$field{State}{class} = 'r';  ###### so we'll flag it
		$dover_flag = 1;
		$dover_msg = $q->span({-class=>'r'}, "You <b>MUST</b> enter a State or a Province<br>\n");
	}

	###### 2nd check if both are fillout error..

	if ( $field{State}{value} && $field{State2}{value} )
	{
		$field{State}{class} = 'r';  ###### so we'll flag it
		$dover_flag = 1;
		$dover_msg = $q->span({-class=>'r'}, "Please enter <b>ONLY</b> a State or a Province<br>\n");
	}

	###### 3rd Check if the country is blank we will assume its US

	if ( $field{Country}{value} eq '' )
	{
		$field{Country}{value} = "US";
	}

	###### 4th Check if the country is NON - US the State field(Province) must be filled in

	if ( $field{Country}{value} ne 'US' && $field{State}{value} eq '')
	{
		$field{State}{class} = 'r';  ###### so we'll flag it
		$dover_flag = 1;
		$dover_msg = $q->span({-class=>'r'}, "Please enter a Province and <n>NOT</b> a State<br>\n");
	}

	###### 5th Check if the country is US the State2 field must be filled in

	if ( $field{Country}{value} eq 'US' && $field{State2}{value} eq '')
	{
		$field{State2}{class} = 'r';  ###### so we'll flag it
		$dover_flag = 1;
		$dover_msg = $q->span({-class=>'r'}, "Please select a State and <n>NOT</b> a Province<br>\n");
	}

	###### all required info was not submitted, so we'll redo the page with ommissions flagged
	if ($dover_flag == 1)
	{
		return 0;
	}

###### make sure they aren't in the excluded domains list
	if (Ck_Excluded_Domains($field{EMailAddress}{value} || ''))
	{	
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "That email address is not allowed.<br />Please contact abuse\@dhs-club.com.</b><br />\n");
		$field{EMailAddress}{class} = 'r';
	}
	if (Ck_Excluded_Domains($field{EMailAddress2}{value} || ''))
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "That email address is not allowed.<br />Please contact abuse\@dhs-club.com.</b><br />\n");
		$field{EMailAddress2}{class} = 'r';
	}

###### email address doesn't fit standard patterns
	if ( $field{EMailAddress}{value} && ! G_S::Check_Email($field{EMailAddress}{value}) )
	{	
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Email address doesn't appear to be a valid address.<br />Please check it.</b><br>\n");
		$field{EMailAddress}{class} = 'r';
	}

	if ( $field{EMailAddress2}{value} && ! G_S::Check_Email($field{EMailAddress2}{value}) )
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Alternate email address doesn't appear to be a valid address.<br>Please check it.</b><br>\n");
		$field{EMailAddress2}{class} = 'r';
	}

###### check that we have a valid ISP emailaddress in at least one of the email fields for Partners & VIPs & upgrades
###### if the primary is an ISP address, then the secondary can be blank
###### otherwise, the secondary has to be an ISP address
###### disabled 01/28/03 per Dick
# 	if (($q->param('membertype') =~ /v|p/ || not $UPDATE) && not $ADMINMODE){
# 	   if (&ISP_address_ck($field{EMailAddress}{value}) != 1){
# 		unless($field{EMailAddress2}{value} && &ISP_address_ck($field{EMailAddress2}{value}) ==1){
# 			$dover_flag = 1;
# 			$dover_msg .= $q->span({-class=>'r'}, "You must submit at least one email address that is provided by an Internet Service Provider.</b><br>\n");
# 			$field{EMailAddress2}{class} = 'r';
# 			$field{EMailAddress}{class} = 'r';
# 			return 0;
# 		}
# 	   }
# 	}


###### check for duplicate email addresses
###### we pass a SQL delimited list of membertypes against which we want to check
	my ($match, $match2) = G_S::Duplicate_Email_CK(
		$db,
		$data->{id},
		$field{EMailAddress}{value},
		$field{EMailAddress2}{value},
		"'v','c','t','ag','agu'"
	);

###### because the params can = '', they can match an address, so we have to eliminate matches on empties
###### by first testing for a value, hence ($match && ...) first checks for a value
	if (($match && $match eq $field{EMailAddress}{value}) or ($match2 && $match2 eq $field{EMailAddress}{value}))
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Email address is a duplicate of one we have on file for another member.</b><br />\n");
		$field{EMailAddress}{class} = 'r';
	}

	if (( $match2 && $match2 eq $field{EMailAddress2}{value}) or ($match && $match eq $field{EMailAddress2}{value}))
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Alternate email address is a duplicate of one we have on file for another member.</b><br />\n");
		$field{EMailAddress2}{class} = 'r';
	}
	
###### make sure they are not in a blocked domain
	if (G_S::Mail_Refused($db, $field{EMailAddress}{value}))
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Email address is from a domain known to use a blacklist. Please use a different email address.</b><br />\n");
		$field{EMailAddress}{class} = 'r';
	}

	if (G_S::Mail_Refused($db, $field{EMailAddress2}{value}))
	{
		$dover_flag = 1;
		$dover_msg .= $q->span({-class=>'r'}, "Alternate email address is from a domain known to use a blacklist. Please use a different alternate email address.</b><br />\n");
		$field{EMailAddress2}{class} = 'r';
	}

###### if they are a US resident, we'll check the validity of their submitted TIN
###### unless it is not required for another reason
	if ($field{Country}{value} eq 'US' && $field{TIN}{req} == 1)
	{

###### since all TINs are nine digits, we'll count the number of digits and fail it if its short
		my $digits = $field{TIN}{value} =~ tr/0-9//;
		if ($digits != 9)
		{
			$dover_flag = 1;
			$dover_msg .= qq#<span class="r">Your TIN does not contain the right number of digits. There should be nine digits.<br />\n#;
			$field{TIN}{class} = 'r';
		}
	}

###### 12/24/02 Linda M. wants to require at least 2 characters in the lastname field
	if ( length $field{LastName}{value} < 2 )
	{
		$dover_flag = 1;
		$dover_msg .= qq#<span class="r">You must provide us with your complete surname.</span><br />\n#;
		$field{LastName}{class} = 'r';
	}

	if ($dover_flag == 1)
	{
		return 0;
	}
	else
	{
		$q->param(-name=>'fullh', -value=> md5_hex( Create_hidden('info_only')));
		return 1;
	}
}

sub Verify_Full_Hash
{
###### we will only have this if we are carrying values between the first submission and
###### a subsequent submission like after the user confirms submitted info
	my $compare = Create_hidden('info_only');
	unless ( md5_hex( $compare ) eq $q->param('fullh') )
	{
		Failure_notification("Corrupted data passed.<br>Please start over.");
		return 0;
	}
		
###### I'm assigning the data the param because one is guaranteed here, whereas if I did it above before
###### a country was submitted, an error was generated by -w
	$data->{country} = $q->param('Country');
	return 1;
}

sub Verify_ID_Hash
{
###### since we don't have the record pulled in, we'll setup some minimal values from the params
	$data->{id} = $q->param('id') || '';
	$data->{membertype} = $q->param('membertype') || '';
	$data->{oid} = $q->param('o') || '';

	unless ( md5_hex("$data->{id} $data->{membertype} $data->{oid}") eq $q->param('idh'))
	{
		Failure_notification("Corrupted id passed.<br>Please start over.");
		return 0;
	}
	return 1;
}

###### 04/16/2001 added the mailing of the spam policy to new VIPs
###### 04/17/2001 made the form not fill out with any existing info if this is an upgrade (in Load_Data)
###### 05/03/2001 added administrative bypassing of payment authorization
###### 05/14/2001 implemented payment info writing to a database table
###### 05/17/01 added flag setting for the duplicate account payment authorization
###### 05/18/01 added checking for anything beyond duplicate MOP, added auto incrementing for invoice numbers and
###### tracking for order incompletion through the same table
###### 05/18/01 added dupe email checking on the alternate email address column as well as the primary
###### 05/21/01 added handling for a partner to VIP upgrade with a pre-existing mop record, added insertion of
###### paid thru date on new mop inserts
###### 05/29/01 made the state/province a bound value in the update string since a single quote could crash the query
###### 05/31/01 put the header output at the top of the script to help with timeouts and added process
###### progress messages to keep the connection open
###### 06/12/01 added upline notifications
###### 06/28/01 modified the parameters sent to authorize.net to make use of Version 3 (w/name verification)
###### 07/06/01 added db->quote for temp record insert, changed CGI to OO methods, made changes to eliminate
###### warnings and prepare for mod_perl implementation
###### 07/09/01 discontinued the auth_dupe flag per Dick's directive (no more duplicate credit card list)
###### 07/10/01 adjusted the script to accept 9 character IDs, also implemented greater filtering on data input
###### made the name fields non updateable on info change applications
###### made the process ticket based so that info was checked against the DB only once instead of every time
###### 07/11/01 made the app useable by members for info updating
###### 07/12/01 made cbo_country pull from a static file rather than from the DB to increase speed
###### 07/13/01 made the dupe email check part of an external library that performs one query rather than 2
###### 08/23/01 made adjustments for spid not being in vipcalcs or mop
###### 08/30/01 moved the status upgrade flag to exclusively within the upgrade statement block
###### 09/24/01 added site activation notices for partners
###### 11/30/01 added filtering to accept digits only in the method of account fields
###### also added extra handling for deleting temp inv_num records if communications fail during payment authorization
###### 12/06/01 added capturing the account holders infomation to the  table
###### 12/10/01 finished with the account capturing and modified the logic for checking application/membertype
###### in the event that they have upgraded, timed out and then backed up and repressed the submit button
###### 01/02/02 changed the handling of name fields to allow hyphens and periods
###### also added DBI quoting of name values in the mop update query to address update failures
###### regarding apostrophes in these fields
###### 01/03/02 added handling of LOCALE specific data with the anticipation that we will easily accept
###### western european characters
###### 01/18/02 disabled all character filtering for fields other than names & email, has been too much hassle
###### with someone wanting something that is not ordinary alphanumeric
###### 02/07/02 revamped the payment authorization section to call an authorization module instead
###### of having it hard coded in, also removed all start_html calls except the first one with the header
###### 02/20/02 removed all payment functionality from the script and moved it to a separate application
###### also put the start_html's back in place to avoid duplicating html headers when dealing with templates
###### implemented writing of temporary upgrade records for processing by other applications
###### 03/20/02 fixed an uninitialized var error in the check for payment info fields
###### 03/28/02 put the upline list back in for notifications and commented out the get coach
###### 03/29/02 added extra verbiage to the final authorization page, also made the upgrade cause the new VIP
###### to 'jump line' to the next VIP up, this entailed getting sponsor info on two occasions at the final stage
###### it also involved updating the depths table
###### 04/03/02 added admin support to specify a paid thru date when bypassing payment processing
###### 04/10/02 added a hack to prefill the alias into the static upgrade page
###### 05/14/02 added capturing of the IP address of the applicant at time of upgrade
###### 05/15/02 added a check against a table of 'throw away' addresses to force the submission of at least
###### one ISP provided address
###### 05/21/02 finished implementation of CC & eCheck payment templates to have the name passed as hidden
###### making it 'un-changeable'
###### 05/22/02 added an insert into the notifications table on a VIP upgrade
###### 05/23/02 made the mop name changeable when in ADMIN mode
###### 05/28/02 disabled the ISP check in ADMIN mode
###### 05/29/02 fixed a glitch in Jump_Line where a sponsor who was sponsored by 01 returned a null spid
###### 06/25/02 broke the PayPal subscription creation routine out into its own module
###### 07/03/02 changed the max auths to 1 and added a link to the authorizations page in the appropriate error
###### made it possible for ADMIN to enter additional parties on the same method of payment, however
###### 07/16/02 added timestamp pattern matching substitutions
###### 07/18/02 changed the max auths in ADMIN mode to 2 so that the error would come up like before
###### 07/31 - 08/01 changed the upgrade behaviour to not jump line, removed sponsor notifications,
###### made the 'sponsor' in the welcome letter the first upline VIP per Dick, also fixed the behaviour of the
###### mop check to allow a Partner to upgrade to VIP using the account we already have on file
###### 08/09/02 changed the id to spid that we pass to Get_VIP_sponsor in the mailto routine
###### also added the AOL link creation routine
###### 08/28/02 Stephen added the trap for the monthend shutdown during the rollup
###### 09/12/02 made the fraud notifications addresses come in from cgi_objects and added an optional CC
###### 09/16/02 lowered the fraud count to 5 per Dick
###### 09/17/02 bypassed Fraud_Check when in admin mode
###### 11/08/02 pulled the Coach value from the global library
###### 12/05/02 CVV2 processing capability added Stephen Martin
###### 12/06/02 Replacement of the majority of text/html files with template objects
###### 12/17/02 went to the 'use' style for private libs/modules, went to the AuthCustom_Generic cookie,
###### and went with the G_S::cboCountry routine
###### 12/24/02 made the lastname require at least 2 characters per Linda M.
###### 01/08/03 disabled the spam policy mailing
###### 01/17/03 Added separate State / Province INputs to normalize state / territories using FIPS codes
###### 01/28/03 disabled the throw away domain check
###### 03/25/03 added the Trade_Restrictions check
##### 04/06/2003 - modified interval() statements for postgres7.3	
###### 04/10/03 refined the date related SQL for making the paid-thru date in bypass mode
###### 05/29/03 forced '000' into the card code field if they didn't provide one
###### also removed some references to Partner upgrades				
###### revised handling of member info updates, removed some more references to Partners
###### and cleaned up the code a bit for easier readability
###### raised the max mop authorizations for Admin from 2 to 200000
###### added insertion of a CA subscription transaction for upgrades
###### 10/31/03 repointed from vipcalcs to a_level for the a_level value
###### 11/07/03 revised the transaction creation SQL to reflect the correct 'amount' and
###### also to assign 30 PP for an upgrade
###### 12/15/03 added extra notes to the admin MOP screen
###### 12/19/03 added a hook to engage the opt2003 system
###### 02-01-04 unhooked Opt2003
###### 02-05-04 rehooked Opt2003 :)
###### 02/17/04 added checks for blocked domains in an email address
###### 02/27/04 changed the wording for the error message when they don't click the radio button
###### 03/04/04 added handling for a new temporary upgrade option for ex-VIPs
###### 03/05/04 added handling for the utype parameter in the payment form page
###### and also the get Check_Already_Authorized routine
###### 03/10/04 added an aliased 'alias' parameter 'a'
###### 05/26/04 added a check for the length of the submitted CC value
###### also make the default pay param vals '' instead of undef
###### 08/19/04 Added cboLanguage and a call to it in sub Do_Page. Added language_pref to %field.
######		Added language description to the Confirmation page.
###### 10/01/04 added a pre-process to the alias param to eliminate leading & trailing whitespace
###### 11/05/04 dropped the PP % for new upgrades
###### 03/23/05 implemented utf-8 mailings
###### 01/12/06 added a few hacks to work with upgrade_tmp records created by appx
# 02/13/06 changed the redirection on 'imcomplete' upgrades with no temp records
# 06-14-06 opened the updating capability to AGs
# 09/01/06 added filtering on the o parameter
###### 11/16/07 added the excluded_domains check into the param checks
###### 12/17/07 removed/disabled references to phone_ac
