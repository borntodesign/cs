#!/usr/bin/perl -w
###### members (members.cgi)
###### last modified: 03/13/2015	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use XML::local_utils;

###### GLOBALS
my ($db, $member, $path) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $lu = new XML::local_utils;

my %TMPL = (
#	'main' => 11054,
	'main' => 'main.tt',
	'language_pack' => 11055
);

my $MBR_QRY = "
	SELECT m.id,
		m.spid,
		m.alias,
		m.membertype,
		m.status,
		m.firstname,
		m.lastname,
		COALESCE(m.company_name, '') AS company_name,
		COALESCE(m.emailaddress, '') AS emailaddress,
		mt.label AS membertype_label
	FROM members m
	JOIN member_types mt
		ON mt.code=m.membertype
	WHERE ";

###### we need to get rid of the leading forward slash
($path = $q->path_info()) =~ s/^\///;

my ($ID, $page) =  split /\//, $path, 2;
$ID = uc $ID;
#Err("ID=$ID and page=$page");# exit;
###### we have to make this thing recognize a logged in user so that they can click on
###### a generic URL in a control center and have it recognize them
if (! $ID)
{
	my ( $USER_ID, $meminfo ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if ($USER_ID && $USER_ID !~ /\D/)
	{
		$ID = $meminfo->{'alias'};
	}
	
	###### if we were successful, we'll redirect to the correct URL instead of just using it
	###### this way, maybe we can train the user to maybe bookmark the correct URL?
	if ($ID)
	{
		my $red = $q->self_url;
		$red =~ s#(.*members)(.*)#$1/$ID$2#;
		print $q->redirect($red);
		exit;
	}
}

###### we're looking for an 'alias' id beginning with two letters
###### unless we have received a page
###### aliases don't necessarily fit that pattern, especially Affinity Groups
###### but in order to keep robots out who would just follow simple numeric sequences
###### we'll require an alias				

if ($ID && $ID !~ /\D/)
{
	###### doesn't appear to be a valid alias
	print 	$q->header('-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("I'm sorry that is an invalid member number"),
		$q->h4(($ID || 'blank')),
		$q->end_html();
	exit;
}

Exit() unless $db = DB_Connect('members');

unless ($ID)
{
	###### if we don't have an ID we'll check for a refspid cookie and send 'em there
	###### otherwise we'll take 'em for 01
	###### this should really be a rarely used branch since it indicates calling the
	###### script bare ie. no path info
	my $refspid = $q->cookie('refspid') || 1;
	my $referer = ($refspid > 1) ? Lookup_Referer($refspid) : 'DC00001';

	###### we could have a refspid that is no longer a VIP
	###### our referer will be undef in that case
	unless ($referer)
	{
		$refspid = 1;
		$referer = 'DC00001';
	}

	print $q->redirect(
		'-location'=>$q->url . "/$referer",
		'-cookie'=>RefspidCK($refspid)
	);
	Exit();
}

$MBR_QRY .= ($ID =~ /\D/ ) ? 'm.alias= ?' : 'm.id= ?';

my $sth = $db->prepare($MBR_QRY);
$sth->execute($ID);
$member = $sth->fetchrow_hashref;
$sth->finish;

unless ( $member )
{
	Bad_Vip();
	Exit();
}

###### they have been turned off or are suspended
if ( $member->{'membertype'} =~ /v/ && $member->{'status'} ne '1')
{
	print	$q->header('-charset'=>'utf-8', '-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("We're sorry, this site is unavailable."),
		$q->end_html();
}
elsif ( $member->{'membertype'} eq 't')
{
	print	$q->header('-charset'=>'utf-8', '-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("We're sorry, use of this member site has been terminated."),
		$q->end_html();
}
###### if they are cancelled or terminated, we'll make use of any pointer pages
###### they may have out there
elsif( $member->{'membertype'} eq 'c')
{
	###### we'll check to see if they have another refspid cookie
	###### and redirect them there, otherwise we'll take 'em for 01
	my $refspid = $q->cookie('refspid') || 1;
	my $referer = ($refspid > 1) ? Lookup_Referer($refspid) : 'DC00001';

	###### we could have a refspid that is no longer a VIP
	###### our referer will be undef in that case
	unless ($referer)
	{
		$refspid = 1;
		$referer = 'DC00001';
	}
	
	print $q->redirect(
		'-location'=>$q->url . "/$referer",
		'-cookie'=>RefspidCK($refspid)
	);
}
#elsif ( ($member->{'membertype'} =~ /v|ag/ && $member->{'status'} eq '1') || BannerMember() )
# as of Oct 2014 per Dick... affiliates can use this again
elsif ( ($member->{'membertype'} eq 'v' && $member->{'status'} eq '1') || $member->{'membertype'} eq 'tp' || $member->{'membertype'} eq 'm')
{
		my ($tmp, $lb) = ();
		$tmp = $gs->GetObject($TMPL{'language_pack'}) || die "Failed to load language pack: $TMPL{'language_pack'}";

		my $TT = Template->new({'INCLUDE_PATH' => '/home/httpd/cgi-templates/TT/members'});

		$gs->Prepare_UTF8($member, 'encode');
		$member->{'OWNER'} = OWNER();
		
		$TT->process(\$tmp, {'member'=>$member}, \$lb) || Err($TT->error());
		
		$lb = $lu->flatXHTMLtoHashref($lb);
		$gs->Prepare_UTF8($lb,'encode');
		print $q->header(
			'-charset'=>'utf-8',
			'-expires'=>'now',
			'-cookies'=>RefspidCK($member->{'id'})
		);
		
		$TT->process($TMPL{'main'}, {'member'=>$member, 'lb'=>$lb}) || print $TT->error();
}
else
{
	###### 11/23/04 Dick wants all other traffic directed to 01
	print $q->redirect(
			'-location'=> $q->url . "/DC00001",
			'-cookie'=>RefspidCK(1));
}

Exit();

###### start of subs

sub Bad_Vip
{
	print	$q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		$q->h3("I'm sorry that is an invalid ID number"),
		$q->h4($ID), $q->end_html();
}

sub Err
{
	print 	$q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error'),
		$_[0],
		'<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>',
		$q->end_html();
		
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Lookup_Referer
{
#	my $sth = $db->prepare("
#		SELECT alias FROM members
#		WHERE membertype IN ('v','ag','agu') AND id= ?");
# Dick wanted first name/last name which is not viable for Affinity Groups who just have the contact name as that
# plus, AGs cannot refer Trial Partners anyway
	my $sth = $db->prepare("
		SELECT alias FROM members
		WHERE membertype IN ('v') AND id= ?");
	$sth->execute($_[0]);
	my $rv = $sth->fetchrow_array;
	$sth->finish;
	return $rv;
}

sub OWNER
{
#	my $owner = $q->escapeHTML( $member->{'company_name'} || "$member->{'firstname'} $member->{'lastname'}" );
# per Dick: I don't want this page to use/show a company name, unless the membership itself is in the company name.
# I didn't ask for this to happen. If and when we have a name displayed that doesn't work with the 's or s', then we can manually change it. 
	my $owner = $q->escapeHTML( "$member->{'firstname'} $member->{'lastname'}" );
	# per Dick 01/29/14: Is there any way we can put in the "'s" unless the name ends in s, then put the apostrophe after the s' and then be able to manually modify these for any other exceptions?
	if ($owner =~ m/s$/)
	{
		$owner .= "'";
	}
	else
	{
		$owner .= "'s";
	}
	
	return $owner;
}

sub RefspidCK
{
	return $q->cookie(
		'-name'=>'refspid',
		'-value'=>$_[0],
		'-expires'=>'+1y',
		'-domain'=>'.clubshop.com');
}

###### 08/29/03 revised the member query to not rely on a view and to allow greater flexibility
###### in defining the result set particularly for AGs which can now have SRWs
###### 09/17/03 revised to recognize the ID of a properly logged in member
###### and redirect to the correct URL if the script was called with an empty path
###### fixed some uninit'd var errors and disabled the preliminary 'ID' check
###### 02/26/04 added handling for redirects to static pages while dispensing a cookie
###### 11/23/04 revised the default handling for non VIP sites to go to 01
###### also went to utf-8 charset headers and utf-8ing the member data
###### 01/11/05 made the refspid cookie valid for a year
###### 05/19/05 moved the refspid cookie making to a sub for uniformity
# 09/28/05 made the Banner Member routine return true for all 'm' members per Dick
###### 08/03/06 revised the test for a lastname
###### 01/29/14 making this NOT mod-perl... just doesn't need to be, will do redirects on the old URL
###### also major other changes to return to one display template and a language pack
# 10/23/14 made it so that Affiliates (membertype 'm') can use this
# 03/13/15 corrected some encoding issues that cropped up since we moved to the newer server
