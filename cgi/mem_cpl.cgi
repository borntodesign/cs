#!/usr/bin/perl -w
###### mem_cpl.cgi
###### a script to present a 'control panel' of member options	
###### created: 07/13/04	Bill MacArthur
######	last modified: 05/24/05	Bill MacArthur

use strict;
use CGI();
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use Digest::MD5 qw( md5_hex );

use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw( $LOGIN_URL $BUILDER_LEVEL);
use ParseMe;

###### GLOBALS
our ($db, $id, $meminfo, $reportee) = ();
our %TEMPLATES = (main=> 10327);
our $message_1 = '';
our $admin = '';

our $q = new CGI;

###### my parameters are rif (the id of the reportee)
our %param = ();	###### parameter hash

foreach ($q->param())
{
	$param{$_} = G_S::PreProcess_Input($q->param($_));
}

###### if we are looking at this as admin or as upline, there will be a 'rid' param
unless ( $param{admin} )
{
	( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	unless ( $id && $id !~ /\D/ )
	{
		Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
		exit;
	}

	unless ($db = DB_Connect('mem_cpl')){ exit }

	###### hook our 'Dream Team' level into this thing
	my $sth = $db->prepare("SELECT level FROM nbteam_vw WHERE id= $id");
	$sth->execute;
	$meminfo->{nbt_level} = $sth->fetchrow_array || 0;
	$sth->finish;
}

###### THE END of ordinary user authentication
###### now we'll do ADMIN authentication if there was a flag

else
{
	###### when coming in as admin, we need an ID	
	unless ($param{rid})
	{
		Err('Missing a required parameter: rid (the ID)');
		exit;
	}

	
###### here is where we get our admin user and try logging into the DB
	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'mem_cpl', $operator, $pwd )){exit}

	$meminfo = {
		id		=> 1,
		firstname	=> $operator,
		lastname	=> '',
		membertype	=> 'staff',
		alias		=> '01',
		nbt_level	=> 1000
	};

	$admin = 1;
}

$db->{RaiseError} = 1;

###### if there was a parameter 'rid' then we are not doing our own id
if ( $param{rid} )
{
	$reportee = $db->selectrow_hashref("
		SELECT id,spid,alias,firstname,lastname,emailaddress,membertype
		FROM members WHERE ${\($param{rid} =~ /\D/ ? 'alias= ?' : 'id= ?')}
	", undef, $param{rid});

	unless ($reportee->{id})
	{ 
		Err('Invalid information passed<br>Search ID not found.');
		goto 'END';
	}

	###### if we are not admin, we need to check if they are in our downline
	if (	$meminfo->{id} > 1 &&
		! G_S::Check_in_downline($db, 'members', $id, $reportee->{id}))
	{
		Err('Search ID not found in your downline.');
		goto 'END';
	}
}

###### if we don't have a reportee established, I guess it's us!
$reportee ||= $meminfo;

###### we are departing from the 'action' parameter since the HTML uses a lot of
###### javascript and assigning to 'action' confuses the syntax when referring to form
unless ($param{perform})
{
	Output();
}
elsif ($param{perform} eq 'uplaxs')
{
	###### set/reset the option to allow upline to manage ones member transfer app
	###### we don't let anybody do this but the member themselves, though
	if ($meminfo->{id} != $reportee->{id})
	{
		$message_1 = $q->div({-class=>'fp_red'},
			'The action can only be performed by the member themselves.'
		);
		Output();
	}
	elsif (! $param{val})
	{
		Err('Missing the option parameter.');
	}
	elsif (! grep /$param{val}/, ('ONBD','On','Off'))
	{
		Err("Invalid option parameter: $param{val}");
	}
	else
	{
		###### we should now have a valid option to deal with
		###### actually, ONBD should only come through if they are mucking around
		###### but we'll deal with it as if it was 'On'
		my $qry = "
			DELETE FROM member_options
			WHERE option_type= 10 AND id= $reportee->{id};

			INSERT INTO member_options (
				id, option_type, val_bool, val_text
			) VALUES (
				$reportee->{id}, 10, ?,
				? || ' by $meminfo->{alias}'
			);";

		if ($param{val} eq 'Off')
		{
			$db->do($qry, undef, ('FALSE', 'turned off'));
		}
		else
		{
			$db->do($qry, undef, ('TRUE', 'turned on'));
		}

		Output();
	}
}
else
{
	Err('Invalid action');
}

END:
$db->disconnect;
exit;

sub Err
{
	print $q->header(-expires=>'now');
	print $q->start_html(-bgcolor=>'#ffffff'), 'There has been a problem<br /><br />';
	print $q->h4($_[0]), scalar localtime(), $q->end_html();
}

sub Get_Alevel
{
	my $id = shift;
	return $db->selectrow_array("
		SELECT a_level FROM a_level WHERE id= ${id}
	") || 0;
}

sub Output
{
	my $TMPL = G_S::Get_Object($db, $TEMPLATES{main}) ||
		die "Failed to load template: $TEMPLATES{main}";

	$TMPL =~ s/%%admin%%/($meminfo->{id} == 1 ? 1 : '')/eg;
	$TMPL =~ s/%%timestamp%%/scalar localtime()/e;
	$TMPL =~ s/%%mgt_mode%%/($reportee->{id} != $meminfo->{id} ? 1:0)/e;
	$TMPL =~ s/%%def_action%%/$q->script_name/e;
	$TMPL =~ s/%%message_1%%/$message_1/;
	$TMPL =~ s/%%rid%%/($reportee->{id} ? $reportee->{id} : '')/e;
	$TMPL =~ s/%%admin%%/$admin/g;
		
	$TMPL =~ s/%%ap_on_off%%/_ap_on_off()/e;
	$TMPL =~ s/%%as_on_off%%/_as_on_off()/e;
	$TMPL =~ s/%%number_active_autostacks%%/_number_active_autostacks()/e;
	$TMPL =~ s/%%personal_mailings%%/_personal_mailings()/e;
	$TMPL =~ s/%%personal_website_link%%/_personal_website_link()/e;
	$TMPL =~ s/%%activity_rpt_subscription%%/_activity_rpt_subscription()/e;
	$TMPL =~ s/%%uplaxs_mta%%/_uplaxs_mta()/e;
	$TMPL =~ s/%%3_for_free%%/_3_for_free()/e;

	ParseMe::ParseAll(\$TMPL, {reportee=>$reportee});
	print $q->header(-expires=>'now'), $TMPL;
}

###### start of tag specific routines

sub _3_for_free
{
	my $lbl = 'Not Applicable';
	my $class = 'ofbd_ctl';

	if ($reportee->{membertype} eq 'v')
	{
		my $row = $db->selectrow_array("
			SELECT id
			FROM member_options
			WHERE option_type= 4
			AND val_bool= TRUE
			AND id = $reportee->{id}");

		$lbl = ($row) ? 'Yes' : 'No';
		$class = ($row) ? 'on_ctl' : 'off_ctl';
	}
	return $q->textfield(-value=>$lbl, -class=>$class, -size=>(length $lbl) +2);
}

sub _activity_rpt_subscription
{
	my $lbl = 'Not available'; ###### default for non-VIPs
	my $class = 'ofbd_ctl';

	if ($reportee->{membertype} eq 'v')
	{
		my ($row) = $db->selectrow_array("
			SELECT id
			FROM member_options
			WHERE option_type= 5
			AND val_bool= TRUE
			AND id = $reportee->{id}");

		if ($row)
		{
			$lbl = 'Paid Active';
			$class = 'on_ctl';
		}
		else
		{
		###### we need to know their a_level to know if they are entitled
		###### to a free subscription
			$reportee->{a_level} = Get_Alevel($reportee->{id}) unless defined $reportee->{a_level};

			if ($reportee->{a_level} >= 4)
			{
				$lbl = 'Not subscribed';
				$class = 'off_ctl';
			}
			else
			{
				$lbl = 'Free Subscription';
				$class = 'onbd_ctl';
			}
		}
	}

	return $q->textfield(-value=>$lbl, -class=>$class, -size=>(length $lbl) +2);
}

sub _ap_on_off
{
	my $lbl = 'Not Applicable';
	my $style = 'onbd_ctl';

	if ($reportee->{membertype} eq 'v')
	{
		$lbl = 'Off by default';

		my ($id, $row) = $db->selectrow_array("
			SELECT id, val_bool FROM member_options
			WHERE option_type= 12 AND id= $reportee->{id}
		");

		if ($id)
		{
			if ($row)
			{
				$lbl = 'On';
				$style = 'on_ctl';
			}
			else
			{
				$lbl = 'Off';
				$style = 'off_ctl';
			}
		}
	}
	return $q->textfield(-value=>$lbl, -class=>$style, -size=>(length $lbl) +2);
}

sub _as_on_off
{
	my $lbl = 'Not Applicable';
	my $style = 'onbd_ctl';

	if ($reportee->{membertype} eq 'v')
	{
		$lbl = 'On by default';

		my ($row) = $db->selectrow_array("
			SELECT val_bool FROM member_options
			WHERE option_type= 9 AND id= $reportee->{id}
		");

		if ($row)
		{
			if ($row)
			{
				$lbl = 'On';
				$style = 'on_ctl';
			}
			else
			{
				$lbl = 'Off';
				$style = 'off_ctl';
			}
		}
	}
	return $q->textfield(-value=>$lbl, -class=>$style, -size=>(length $lbl) +2);
}

sub _number_active_autostacks
{
	my $number = $db->selectrow_array("
		SELECT COUNT(id) FROM member_options
		WHERE option_type= 7 AND val_bool= TRUE
		AND id= $reportee->{id}");

	my $style = ($number) ? 'on_ctl' :'off_ctl';
	return $q->textfield(
		-value=>$number,
		-class=>$style,
		-size=>(length $number) +1);
}

sub _personal_mailings
{
	my $lbl = 'Not Applicable';
	my $style = 'onbd_ctl';

	if ($reportee->{membertype} eq 'v')
	{
#		$lbl = 'On by default';
	###### since the abscence of a record is currently the only 'ON' method
	###### we'll get rid of the 'by default' concept
		$lbl = 'On';

		my ($row) = $db->selectrow_array("
			SELECT val_bool FROM member_options
			WHERE option_type= 3 AND id= $reportee->{id}
		");

		if (defined $row)
		{
			if ($row)
			{
				$lbl = 'On';
				$style = 'on_ctl';
			}
			else
			{
				$lbl = 'Off';
				$style = 'off_ctl';
			}
		}
	}

	return $q->textfield(-value=>$lbl, -class=>$style, -size=>(length $lbl) +2);
}

sub _personal_website_link
{
	my $onclick = '';
	my $title = '';
	my $style = 'off_ctl';
	my ($row) = $db->selectrow_array("
			SELECT val_text
			FROM member_options
			WHERE option_type= 2
			AND id = $reportee->{id}");

	if ($row)
	{
		$onclick = "window.open('$row')";
		$style = 'on_ctl';
		$title = 'Click to open the URL in a popup window.';
	}

	return $q->textfield(
		-title=>$title,
		-value=>($row || ''),
		-class=>$style,
		-size=>($row ? ((length $row) +4) : 15),
		-onclick=>$onclick);
}

sub _uplaxs_mta
{
	if ($reportee->{membertype} ne 'v')
	{
		return $q->textfield(
			-value=>'Not Applicable',
			-class=>'onbd_ctl',
			-size=>16);
	}

	my @list = ('On', 'Off');
	my $style = 'onbd_ctl';
	my ($row) = $db->selectrow_array("
		SELECT val_bool FROM member_options
		WHERE option_type= 10 AND id= $reportee->{id}");
		
	my $default = 'ONBD';

	if ($row)
	{
		$default = 'On';
		$style = 'on_ctl';
	}
	elsif (defined $row)
	{
		$default = 'Off';
		$style = 'off_ctl';
	}
	else
	{
	###### we'll only include the ONBD item if they are in fact ONBD
		$default = 'ONBD';
		push (@list, 'ONBD');
	}

	return $q->popup_menu(
		-name=>'',
		-default=>$default,
		-values=>\@list,
		-labels=>{'ONBD'=>'On by default', 'On'=>'On', 'Off'=>'Off'},
		-class=>$style,
		-onChange=>'_uplaxs_mta(this)',
		-force=>1);
}

###### 07/16/04 added the 'force' attribute to the drop-down mta_axs
###### 04/28/05 Added the Auto Pilot code.
###### 05/24/05 revised row test in personal mailings
