#!/usr/bin/perl -w
###### cb_card_transfer_plastic.cgi
###### a simple interface for VIPs to transfer sponsorship of cards that they own
# last modified: 02/17/14	Bill MacArthur

=head 4 Basic Flow

	Authenticate the user
	Make sure we have the required parameters if we are not being called empty
	Verify that the recipient of this transfer is in their downline and capable of sponsoring cards
	Verfify the the card count matches the start and ending cards and that all cards are transferrable
	Perform the transfer
=cut

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use G_S qw( $SALT %URLS );
use DB_Connect;
use XML::Simple;
use ParseMe;

my ($db, %p, @errs, @messages, $recipient) = ();
my $q = new CGI;
my $gs = new G_S;

my %TMPL = (
	main => 10678,
	xml => 10683
);

# Get the Login ID and Member info hash from the cookie.
my ( $Login_ID, $mem_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

# If the Login ID doesn't exist or has any non-digits send them to login again.
if (!$Login_ID || $Login_ID =~ /\D/)
{
	print $q->redirect("$URLS{LOGIN}?destination=${\$q->self_url}");
	End();
}
elsif(! grep $_ eq $mem_info->{membertype}, ('v','m','tp')){
	print $q->redirect("$URLS{LOGIN}?destination=${\$q->self_url}");
	End();
}

$p{_action} = $q->param('_action');
End() unless $db = DB_Connect::DB_Connect('cb_card_transfer.cgi');
$db->{RaiseError} = 1;

my $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to retrieve template: $TMPL{xml}\n";
$xml = XMLin($xml,  NoAttr => 1);

unless ($p{_action}){
	DoPage();
	End();
}

# we will only execute each one if the previous one succeeds
if( LoadParams() && ($recipient = ValidateRecipient()) && ValidateTransfer()){

# if we wanted to confirm then to confirm what we are doing, this would be the place

	PerformTransfer();
}

DoPage();

End();

###### start of sub-routines

sub DoPage
{
	my $tmpl = $gs->Get_Object($db, $TMPL{main}) || die "Failed to obtain template: $TMPL{main}\n";
	# put this up here so that any tokens contained in these nodes can be interpolated below
	ParseMe::ParseAll(\$tmpl, {xml=>$xml});

	$tmpl =~ s/%%errors%%/Output_Errs()/e;
	$tmpl =~ s/%%messages%%/Output_Messages()/e;
	$tmpl =~ s/\%\%$_\%\%/$p{$_} || ''/eg foreach (qw/recipient num_of_cards cbid_low cbid_high/);

	print $q->header(-charset=>'utf-8'), $tmpl;
	End();
}

sub End
{
	$db->disconnect if $db;
	exit;
}

sub Err
{
	push @errs, $_[0];
}

sub LoadParams
{
	($p{cbid_low} = $gs->PreProcess_Input($q->param('cbid_low') || '')) =~ s/^DHS//i;	# remove the prepender if they entered it
	($p{cbid_high} = $gs->PreProcess_Input($q->param('cbid_high') || '')) =~ s/^DHS//i;	# remove the prepender if they entered it
	$p{num_of_cards} = $gs->PreProcess_Input($q->param('num_of_cards') || '');
	$p{recipient} = $gs->PreProcess_Input($q->param('recipient') || '');

	Err($xml->{error2}) unless $p{cbid_low} && $p{cbid_low} !~ /\D/;
	Err($xml->{error3}) unless $p{cbid_high} && $p{cbid_high} !~ /\D/;
	Err($xml->{error4}) unless $p{num_of_cards} && $p{num_of_cards} !~ /\D/;
	Err($xml->{error6}) unless $p{recipient};
	if (@errs){
		return 0;
	}
	return 1;
}

sub Message
{
	push @messages, $_[0];
}

sub Output_Errs
{
	my $rv = '';
	$rv .= $q->div({-class=>'error'}, $_) foreach @errs;
	return $rv;
}

sub Output_Messages
{
	my $rv = '';
	$rv .= $q->div({-class=>'message'}, $_) foreach @messages;
	return $rv;
}

sub PerformTransfer
{
	$db->begin_work;
	my $rv = $db->do('
		UPDATE clubbucks_master SET referral_id= ?, "class"=2
		WHERE referral_id = ?
		AND "class" IN (2,7)	-- if we want to allow them to transfer sponsored cards, then make this (2,7)
		AND id BETWEEN ? AND ?', undef, $recipient->{id}, $Login_ID, $p{cbid_low}, $p{cbid_high});
	# our $rv should equal the number of cards
	unless ($rv == $p{num_of_cards}){
		Err($xml->{error5});
		$db->rollback;
		return 0;
	} else {
		Message($xml->{message1});
		$db->commit;
	}
	return 1;
}

sub ValidateRecipient
{
	# does the membership exist in my downline?
	my $qry = 'SELECT id, spid, alias, membertype, firstname, lastname FROM members WHERE ' .
		($p{recipient} =~ /\D/ ? 'alias=?' : 'id=?');
	my $rv = $db->selectrow_hashref($qry, undef, uc($p{recipient}) );
	unless ($rv){
		Err($xml->{error9});
		return 0;
	}

	# am I assigning sponsorship to myself? if so we are done checking in here
	if ($Login_ID != $rv->{id}){

		# for now VIPs can transfer cards to VIPs and merchant affiliates
		unless (grep $_ eq $rv->{membertype}, (qw/v ma ag m tp/)){
			Err($xml->{error7});
			return 0;
		}
		
		unless ($gs->Check_in_downline($db, 'members', $Login_ID, $rv->{id}))
		{
			# certain parties are allowed to transfer out of organization, like Jay McCay, due to the nature of the business environment
			my ($access) = $db->selectrow_array('SELECT id FROM member_options WHERE option_type=15 AND val_bool=TRUE AND id=?', undef, $Login_ID);
			unless ($access)
			{
				Err($xml->{error8});
				return 0;
			}
		}
	}
	return $rv;
}

sub ValidateTransfer
{
	if ($p{cbid_high} < $p{cbid_low}){
		Err($xml->{error10});
		return 0;
	}
	# make sure that the number of cards equals the range submitted
	elsif (($p{cbid_high} - $p{cbid_low} + 1) != $p{num_of_cards}){
		Err($xml->{error11});
		return 0;
	}
	
	# now make sure that all the cards in the range are under the ownership of the user
	my ($rv) = $db->selectrow_array('
		SELECT COUNT(*) FROM clubbucks_master
		WHERE referral_id = ?
		AND "class" IN (2,7)	-- if we want to allow them to transfer sponsored cards, then make this (2,7)
		AND id BETWEEN ? AND ?', undef, $Login_ID, $p{cbid_low}, $p{cbid_high});
	if ($rv != $p{num_of_cards}){
		Err($xml->{error12});
		return 0;
	}
	
	return 1;
}

# 10/29/09 tweaked to allow them to transfer "assigned" cards as well as owned cards
# 01/14/10 made it possible to transfer cards to AG's
# 04/07/11 made it possible to transfer cards to APs (m)
# 05/06/11 made it possible to transfer cards to AMPs (amp)
# 07/01/11 made it possible to use this interface as an AMP
# 09/22/11 made it possible for certain authorized parties to transfer cards out of their organization
# 02/17/14 changed the membership checking lists to replace amp with tp