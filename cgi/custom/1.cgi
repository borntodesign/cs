#!/usr/bin/perl -w
# 1.cgi
# a custom script to do a callback to a lead referral source that does not have an https callback handler
# by calling this custom script in a secure session, we can satisfy our page whilst still performing the callback
# we are hard-coding in the URL at this point as we can write customizations on this theme in the rare cases where they are needed

use strict;
use CGI;

my $cgi = new CGI;

print $cgi->header('image/gif');
open IMAGE, "/home/clubshop.com/html/images/clearpixel.gif";
my ($image, $buff);
while(read IMAGE, $buff, 1024) {
    $image .= $buff;
}
close IMAGE;
print $image;

#print $cgi->redirect('http://www.glocalincome.com/images/clearpixel.gif');
my $ck = $cgi->cookie('inb1');
exit unless $ck;

my $url = 'wget -q --tries=10 --waitretry=5 -b --output-file=/tmp/custom-1.cgi --output-document=/tmp/custom-1.cgi http://www.highpayoutpoker.com/glocalCatcher.php?userId=';
$url .= $ck;
qx($url);
exit;

