#!/usr/bin/perl -w
###### rpc.cgi
###### a routine to provide simple xml data chunks to an active xml page
###### deployed: 07/25/06	Bill MacArthur
###### modified: 06/20/15	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use CGI;
use DB_Connect;
use CGI::Carp qw(fatalsToBrowser);
binmode STDOUT, ":encoding(utf8)";

my (%params, $xml, $db) = ();
our @errs = ();
my $q = new CGI;
my $mod_dir = '/home/httpd/cgi-lib/rpc.cgi';

die "IP access denied!\n" unless IpCk();
###### the '1' says to connect as 'rpccgi'
###### the last arg says tell us what happened via the normal redirect URL
#$db = DB_Connect('rpc.cgi', 1, {return_redirects => 1});
$db = DB_Connect('generic', undef, {'return_redirects' => 1});
die "DB connection failed" unless $db;

my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});

if (! ref $db)
{
	push (@errs, $db);
	undef $db;
}
else
{
	$db->{'RaiseError'} = 1;

	if ( LoadParams( _cfg() ))
	{
		$xml = Run($params{'proc'}) || '';
		$xml .= RollErrs() if @errs;
		print $q->header('-expires'=>'now', '-charset'=>'utf-8', '-type'=>'text/xml');
		print "<$params{root}>$xml</$params{root}>";
	}
	else
	{
		push (@errs, 'Undefined error') unless @errs;
	}
}
Err() if @errs;
$db->disconnect if $db;
exit;

sub Combine
{
	###### roll several small routines result sets into one
	###### this is preferable to making several calls to this script
	###### with all the attendant overhead
	
	###### we need a process list
	return undef unless LoadParams({'proclist' => {
		'req'	=> 1,
		'value'	=> undef,
		'ck'	=> undef}
	});
#	return '<b>b</b>';
	###### our proclist should be a CSV list of available procs
	my @list = split(/,/, $params{'proclist'});
	my $rv = '';
	foreach (@list)
	{
		###### stop processing if we receive an undef
		###### the loginCK  will return undef to stop further processing
		my $rs = Run($_);
		last unless defined $rs;
		$rv .= $rs;
	}
	return $rv;
}

sub Convert2XML
{
	my $hr = shift;
	my $xml = '';
	foreach (keys %{$hr})
	{
		$xml .= "<$_>" . (defined $hr->{$_}? $hr->{$_} : '') . "</$_>\n";
###### I'm hoping not to have to wrestle with this flag again,
###### but I'm leaving this in here for a reminder
#		print "$_ flag: ", utf8::is_utf8($hr->{$_}), "\n";
	}
	###### this returns unbalanced XML, but the caller should expect that
	return $xml;
}

sub Cookie
{
	return undef unless $params{'cookies_as_params'};
	return $q->cookie($_[0]);
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8', '-type'=>'text/xml'), RollErrs();
}

sub IpCk
{
	my $adr = $q->remote_addr;
	return 1 if $adr eq '127.0.0.1';		# our most common scenario
	return 1 if $adr =~ /(^192\.168\.0)/;	# lan access
	return 1 if grep $adr eq $_, ('208.69.229.17', '208.69.229.16', '192.168.250.1', '208.69.229.4', '73.55.2.151');	# that last one was for testing from a comcast account
	return undef;
}

sub List
{
	###### List available routines
	opendir(DIR, $mod_dir) || die "Failed to open module directory: $mod_dir\n";
	
#	my @list = readdir(DIR);
	my $rv = '';
	foreach (sort readdir(DIR))
	{
		next unless s/\.lib$//;
		my $proc = LoadModule($_);
		# eval in case a module author *forgets* to include a Describe routine
		my $prov = eval('$proc->Describe') || 'Module is missing description';
		$rv .= "<$_>\n";
	#	$rv .= "<description>";
		$rv .= $prov;
	#	$rv .= "</description>\n";
		$rv .= "</$_>\n";
	}
	closedir(DIR);
	return $rv;
}

sub LoadModule
{
	my $arg = shift || return;
	
	###### we may have received a filename or just a 'package' name
	$arg =~ /(.+?)(\.lib)?$/;
	my $proclib = $2 ? $arg : "$arg.lib";
	my $proc = "rpccgi::$1";
	
	###### let's see if we have a 'module' by that name
	eval("require 'rpc.cgi/$proclib'");
	if ($@)
	{
		warn "This error was encountered while trying to load proc '$proclib' - $@";
		return undef;
	}
	
	$proc = new $proc;
	return $proc;
}

sub LoadParams
{
	my $cfg = shift || die "Current param hash is undef";
	foreach (keys %{$cfg})
	{
		###### no point in doing it over if it's already been done
		###### this could apply for Combine calls
		next if defined $params{$_};

		$params{$_} = $q->param($_) || Cookie($_) || $cfg->{$_}->{'default'};

		if ($cfg->{$_}->{'req'} && ! defined $params{$_})
		{
			push (@errs, "$_ is required");
			return;
		}
		next unless defined $params{$_};

	###### if any of our checks return true, then we have a problem
		foreach my $ck (@{$cfg->{$_}->{'ck'}})
		{
			if ($ck =~ m#/#)
			{
				my $rv = eval('$params{$_}' . "$ck");
				push (@errs, "$_ = $params{$_}\nfailed validation") if $rv;
				push (@errs, "$@\nvalue=$params{$_}\nname=$_") if $@;
			}
			else	###### we'll handle errors in the respective routines
			{
				my $rv = eval($ck . "($params{$_})");
				push (@errs, "$_ = $params{$_}\nfailed validation") if $rv;
				push (@errs, "$@\nvalue=$params{$_}\nname=$_") if $@;
			}
			return if @errs;
		}
	}
	return 1;
}

sub LoginCK
{
	my ($id, $meminfo) = $gs->authen_ses_key($params{'loginCK'});
	$gs->Prepare_UTF8($meminfo);
	if ($id && $id !~ /\D/)
	{
		$params{'id'} = $id;
		return '<loginck>' . Convert2XML($meminfo) . '</loginck>';
	}
	push (@errs, '<login_invalid>1</login_invalid>');
	return undef;
}

sub RollErrs
{
	my $errs = "<rpc_errors>\n" .
		'<time>Current server time: ' . scalar localtime(). "</time>\n";
	foreach (@errs)
	{
		s/&/&amp;/g;
		$errs .= "<error>$_</error>\n";
	}
	undef @errs;
	return "$errs</rpc_errors>";
}

sub Run
{
	my $proc = shift || return;
	###### special cases
	if (grep $proc eq $_, ('Combine','List', 'LoginCK'))
	{
		my $rv = eval("$proc()");
		push (@errs, $@) if $@;
		return $rv;
	}
	
	if ($proc eq '_proctest')
	{
		$proc = new _proctest;
	}
	else
	{
		$proc = LoadModule($proc);
		return undef unless $proc;
	}

	my $rv = LoadParams($proc->Params) ? $proc->Process($db, \%params) : undef;
	return undef unless defined $rv;
	
	###### We should be receiving a ref to either a hash or a scalar.
	###### If it's a hash, we'll convert it to xml and ref that as a scalar
	###### we could use XML::Simple,\
	###### but then we have to get a root node which we probably don't want
	###### and certainly don't want to have to define by parameters down at this level
	return (ref $rv eq 'HASH') ? ${\Convert2XML($rv)} : $$rv;
}

sub _cfg
{
	return {
		'root'		=> {
		# there has to be a default, otherwise axkit will barf on unbalanced xml
			default	=> 'rpc',
			'req'	=> 0,
			'ck'	=> ['=~m/^\d/']
		},
		'proc'	=> {
			default	=> undef,
			'req'	=> 1,
			'ck'	=> ['=~m/:/']
#			ck	=> ['=~m/\D/', '_ck_proclist']
		},
		'loginCK'	=> {
			'default'	=> undef,
			'req'	=> 0,
			'ck'	=> undef
		},
###### until we deal with issues of privacy and such, we won't really be using cookies
		'cookies_as_params'	=> {
			'default'	=> undef,
			'req'	=> 0,
			'ck'	=> undef
		}
	};
}

###### ###### ###### END OF SUBROUTINES

###### to test this proc out, invoke rpc.cgi?proc=_proctest;id=1
package _proctest;
###### this is a barebones test process
sub new { return bless{} }
sub Params {
	return {'id'=>{'value'=>undef,'req'=>1,'ck'=>['=~m/\D/']}};
}
sub Process {
	my $self = shift;
	my($db, $params) = @_;
	my $rv = '<node1>NODE1</node1>';
	return \$rv;
}
1;

__END__

05/18/15	Added the binmode for STDOUT