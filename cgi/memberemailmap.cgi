#!/usr/local/bin/perl
#
# display email address for a given memberid or a member id for an email address
#
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use LWP::UserAgent;
use URI::Escape;
use G_S;
use ParseMe
   require XML::Simple;
   require XML::local_utils;
use Data::Dumper;
use CGI;
use CGI::Cookie;

my $cgi = new CGI;
my $gs = new G_S;
#
#
my %TMPL = ('xml'=>10897);
my $cgiini = "ipaddress.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
#
# Get the translation hash to remove the text at the end of the location string
#
my $xmt = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to load template: $TMPL{xml}\n";
    $xmt =~ s/\n|\r|^M//g;
my $lu = new XML::Simple;
my $trans = $lu->XMLin($xmt);
chomp($trans->{translate}->{rewardsmap}->{"currentloc.shtml"});
chomp($trans->{translate}->{rewardsmap}->{"defaultloc.shtml"});

my $agent = LWP::UserAgent->new;
my $xml = new XML::Simple;

my $def_location = $cgi->cookie('rewards_def_location');
#
#
#
my $result={};
my $params= {};
my $mlat = 0;
my $mlong = 0;
my $qry = "";
my $def_address = "";
my $ii = 0;
#
# Get The form input (using GET) these are currently location, change, check
#
my @formdata = split (/&/, $ENV{'QUERY_STRING'});
foreach  my $pair (@formdata) {
    my ($key, $value) = split(/=/, $pair);
    $value =~ tr/+/ /;
    $value =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
    $params->{$key} = $value;
}
#
# check for either email address or id number
#
if ($params->{id} 
