#!/usr/bin/perl -w
###### merch-frontline-rpt.cgi
###### started as a replica of the merchant_comm_rpt
###### Display a merchant's personally sponsored members
###### The script is driven by a Member ID which is found in the login cookie.
###### 
###### created: 11/24/09 Bill MacArthur
###### last modified: 03-01-14	Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use G_S qw($LOGIN_URL);
use Template;
use XML::Simple;

###### GLOBALS
my $TT = Template->new();
my $q = new CGI;

###### our page templates
my %TMPL = (
	'rpt' => 10758,
	'xml' => 10759);

my $member_id = '';
my $alias = '';
my $member_info = '';
my $db = DB_Connect('merch-frontline-rpt.cgi') || exit;
$db->{'RaiseError'} = 1;
my $gs = G_S->new({'db'=>$db});


my $xml = $gs->GetObject($TMPL{'xml'}) || die "Couldn't open $TMPL{'xml'}";
$xml = XMLin( $xml );

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

Report();
End();

################################
###### Subroutines start here.
################################

sub End
{
	$db->disconnect if $db;
	exit;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now'), $q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print $q->div($_[0]);
	print $q->end_html();
	End();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# Get the member Login ID and info hash.
	( $member_id, $member_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

	use Data::Dumper;
	warn "\n\nmerch-frontline-rpt: " . $member_id . "---\n" . Dumper($member_info) . "\n";
	# If the Login ID doesn't exist or has any non-digits send them to login again.
	if (!$member_id || $member_id =~ /\D/)
	{
		RedirectLogin();
		End();
	}
	elsif ($member_info->{membertype} ne 'ma')
	{
		Err('This report is only available to logged in merchant affiliate memberships');
	}
}

###### Display the transaction report for the requested period.
sub Report
{
	my $qry = '';
	my $sth = ();
	my $report_body = '';
	my %data = (
		'report'=>[],
		'xml'=> $xml,
		'script_name'=> $q->script_name
	); 

	# Prepare a query to get detailed transactions for the period requested.
	$qry = "SELECT m.alias,
				m.firstname,
				m.lastname,
				m.emailaddress,
				m.address1,
				m.city,
				m.state,
				m.country,
				m.postalcode,
				m.phone

			FROM members m

			WHERE m.spid= ?
			AND m.membertype IN ('m','s','v')

			ORDER BY
					m.lastname";

	$sth = $db->prepare($qry);

	# Get the member's transactions for the period specified.
	$sth->execute( $member_id);
	while ( my $tmp = $sth->fetchrow_hashref)
	{
		push @{$data{'report'}}, $tmp;
	}		

	$sth->finish();

	my $tmpl = $gs->GetObject($TMPL{'rpt'}) || die "Couldn't open $TMPL{'rpt'}";

	my $rv = ();
	$TT->process(\$tmpl, \%data, \$rv);
	print $q->header('-charset'=>'utf-8'), $rv;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
	print $q->header('-expires'=>'now'), $q->start_html('-bgcolor'=>'#ffffff');
	print "Please login <a href=\"$LOGIN_URL?destination=$me\">HERE</a>.<br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

# 03/01/14 tweaked the way the XML was pulled in