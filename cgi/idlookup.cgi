#!/usr/bin/perl -w
# new 'findme.cgi'
###### idlookup.cgi
# last modified 04/17/15	Bill MacArthur

use CGI;
use strict;
use XML::LibXML::Simple;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;

my $supportLink = '<a href="mailto: support1@dhs-club.com">support1@dhs-club.com</a>';

my $q = new CGI;
$q->charset('utf-8');

my ($db, $criteria) = ();
my $gs = G_S->new({'CGI'=>$q, 'db'=>\$db});
my %TMPL = (
	'Success'	=> 10277,
	'NoResult'	=> 10276,
	'Duplicate'	=> 10278,
	'VIP_email'	=> 10279,
	'm_email'	=> 10280,
	'ma_email'	=> 11110,
	'language_pack'	=> 11111
);

exit unless $db = DB_Connect('findme');
$db->{'RaiseError'} = 1;

my $lb = $gs->GetObject($TMPL{'language_pack'}) || Exit();
$lb = XMLin($lb, 'NoAttr'=>1) || Exit();

my %mail = (
	'smtp'		=> '192.168.17.1',
	'Content-Type'=> 'text/plain; charset="utf-8"',
	'From' 		=> 'Clubshop Search Results <idlookup@dhs-club.com>',
	'Subject'	=> $lb->{'cisp'}
);

my $email = lc ( $q->param('email') || $q->param('emailaddress') );
my $id = $gs->PreProcess_Input(uc $q->param('id'));
my $cbid = $gs->PreProcess_Input( $q->param('cbid'));

unless ($email || $id || $cbid)
{
	Err("$lb->{'no_sv_recv'}<br />$lb->{'enter_something'}");
	exit;
}
elsif ($email && $email !~ /@/)
{
	Err(sprintf($lb->{'invalid_email'}, "<b>$email</b>"));
	exit;
}



###### if we are searching by CBID, then we'll have to get the member ID and continue
if ($cbid)
{
	$cbid =~ s/dhs//i;
	if ((! $cbid) || $cbid =~ /\D/ || length $cbid > 10)
	{
		Err($lb->{'backup_and_reenter'});
		Exit();
	}

	my $sth = $db->prepare("
		SELECT id, current_member_id
		FROM clubbucks_master
		WHERE id= $cbid");
	$sth->execute;
	my $tmp = $sth->fetchrow_hashref;
	$sth->finish;

	unless ($tmp->{id})
	{
		Err($lb->{'sorrynocbid_match'});
		Exit();
	}
	elsif (! $tmp->{'current_member_id'})
	{
		Err("$lb->{'sorry_cardnotactivated'} $lb->{'nomemmatch'}");
		Exit();
	}

	$id = $tmp->{'current_member_id'};
}

my $qry = "SELECT m.membertype,
			COALESCE(m.emailaddress, '') AS emailaddress,
			m.firstname,
			m.lastname,
			m.alias,
			CASE
				WHEN m.password > ' ' THEN m.password
				ELSE m.oid::VARCHAR
			END AS password,
			mt.label AS member_description,
			COALESCE(m.company_name,'') AS company_name,
			m.oid,
			COALESCE(ma.id::TEXT,'') AS ma_id,
			COALESCE(ma.username,'') AS ma_username,
			COALESCE(ma.password,'') AS ma_password
			
		FROM members m
		JOIN \"Member_Types\" mt
			ON m.membertype = mt.code
		LEFT JOIN merchant_affiliates_master ma
			ON ma.member_id=m.id
		WHERE m.membertype != 'd'
		";

if ($email)
{
	$qry .= 'AND m.emailaddress= ?';
	$criteria = $email;
}
elsif ($id =~ /\D/)
{
	$qry .= 'AND m.alias= ?';
}
else
{
	$qry .= 'AND m.id= ?';
}
$criteria ||= $id;
	
my $sth = $db->prepare($qry);
my $rv = $sth->execute($criteria);
my $data = $sth->fetchrow_hashref;
$sth->finish;

if ($rv eq '0E0')
{
	Do_Page($TMPL{'NoResult'});
}

###### there should have been only one row of data returned
###### if this statement returns true, that means there was more and we cannot proceed
elsif ($rv > 1)
{
	Do_Page($TMPL{'Duplicate'});
}


###### since we received a spam complaint from someone who received the email notification from a lookup
###### we simply tell them to get in touch with us if they need to
elsif ($data->{'membertype'} eq 'r')
{
	Err(sprintf("$lb->{'memisremoved'}<br />$lb->{'please_contact'}", $supportLink));
}
elsif (! $data->{'emailaddress'})
{
	Err(sprintf("$lb->{'cannotsend'}<br />$lb->{'please_contact'}", $supportLink));
}
else
{
	my $flg = DomainBlocked();
	if ($flg)
	{
		Err(sprintf("$lb->{'domblock'} $lb->{'sendanyway'}", $supportLink));
	}

	if ($data->{'membertype'} eq 'v')
	{
		$rv = Do_Mail($TMPL{'VIP_email'});
	}
	elsif ($data->{'membertype'} eq 'ma')
	{
		$rv = Do_Mail($TMPL{'ma_email'});
	}
	else
	{
		$rv = Do_Mail($TMPL{'m_email'});
	}

	unless ($flg)
	{
		if ($rv)
		{
			Do_Page($TMPL{'Success'});
		}
		else
		{
			Err($lb->{'mailsenderror'});
		}
	}
}

END:
$db->disconnect;
exit;

##############################################

sub Do_Mail
{
	my $tmpl = shift;
	$tmpl = $gs->GetObject($tmpl) || die "Failed to load template: $tmpl\n";

	###### if we have data, we'll put it into our template
	foreach (keys %{$data})
	{
		$tmpl =~ s/%%$_%%/$data->{$_}/g;
	}

	$mail{'To'} = "$data->{'firstname'} <$data->{'emailaddress'}>";
	$mail{'Message'} = $tmpl;

	return sendmail(%mail);
}

sub Do_Page
{
	my $tmpl = shift;
	$tmpl = $gs->GetObject($tmpl) || die "Failed to load template: $tmpl\n";

	###### if we have data, we'll put it into our template
	foreach (keys %{$data})
	{
		$tmpl =~ s/%%$_%%/$data->{$_}/g;
	}
	$tmpl =~ s/%%submission%%/($email || $cbid || $id || '')/e;
	print $q->header, $tmpl;
}

sub DomainBlocked
{
	(my $domain = $data->{'emailaddress'}) =~ s/.*@//;
	return $db->selectrow_array("SELECT \"domain\" FROM domains_blocked WHERE \"domain\" = '$domain'");
}

sub Err
{
	print $q->header, $q->start_html('-title'=>'Error', '-bgcolor'=>'#ffffff');
	print "$_[0]<br /><br />\n";
	print scalar localtime(), ' (PST)';
	print $q->end_html();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

###### 03/19/04 a whole new incarnation for the old findme.cgi
###### 12/02/05 tweaks to discover why a valid email address submitted returned no results
###### the problem was that input preprocessing was removing valid characters from the email
###### address, like _
###### 04/20/06 added handling for blocked domain alerts
# 07/09/07 change the smtp server to mail.dhs-club.com to circumvent the blocks imposed on the web server