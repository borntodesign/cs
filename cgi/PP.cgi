#!/usr/bin/perl -w
###### PP.cgi
######
###### A script that allows that handles PayPal transactions
######
###### created: 03/13	Bill MacArthur
######
###### modified: 
	
use strict;
use Template;
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util::Numeric qw(isnum);
use Mail::Sendmail;
use Data::Dumper;
use lib ('/home/httpd/cgi-lib');
use CS_PayPal;
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use GI;
use XML::local_utils;

###### ###### be sure to set that 'test' value correctly... 0=LIVE 1=TEST .... DEBUG outputs the SOAP XML to stderr if set
my $API = CS_PayPal->API({'test'=>0});
#$Business::PayPal::API::Debug = 1;

my $cspp = new CS_PayPal;
my $TT = new Template;
my $q = new CGI;
my ($db, %params, $errors, $lang_blocks) =();
my $gs = G_S->new({'db'=>\$db});
my %TMPL = (
	'XML' => 11001,
	'ExpressCheckout_Funding_Confirm'	=> 11000,
	'startExpressCheckout_Funding' 		=> 10999
);

my ( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
if (! $member_id || $member_id =~ m/\D/)
{
	print $q->redirect("$LOGIN_URL?destination=" . $q->url);
	Exit();
}

LoadParams();

# if we did not fail at loading params, then we can go ahead and make a DB connection
$db = DB_Connect('PP.cgi');
$db->{'RaiseError'} = 1;
my $GI = GI->new('cgi'=>$q, 'db'=>$db);

($mem_info->{'country'}) = $db->selectrow_array('SELECT country FROM members WHERE id= ?', undef, $member_id);

LoadLangBlocks();

if ($params{'x'} eq 'startExpressCheckout')
{
	if ($params{'sfunc'} eq 'funding')
	{
		startExpressCheckout_Funding();
	}
	elsif ($params{'sfunc'} eq 'funding-confirm')
	{
		ExpressCheckout_Funding_Confirm();
	}
}
elsif ($params{'x'} eq 'GetTransactionDetails')
{
	if ($params{'sfunc'} eq 'ca-funding')
	{
		CaFunding();
	}
}

die "Unexpected logic branch: x:$params{'x'} sfunc:$params{'sfunc'}";

###### start of subroutines

=head3 CaFunding

	Called upon a return from PayPal after an ExpressCheckout call for CA funding.
	The module docs say these fields are available on the %resp handle
	
	as described in the PayPal "Web Services API Reference" document.

Returns a hash with the following keys:

  Token
  TransactionID
  TransactionType
  PaymentType
  PaymentDate
  GrossAmount
  FeeAmount
  SettleAmount
  TaxAmount
  ExchangeRate
  PaymentStatus
  PendingReason
  BillingAgreementID (if BillingType 'MerchantInitiatedBilling'
                      was specified during SetExpressCheckout)
=cut

sub CaFunding
{
	my %CoDetails = $API->GetExpressCheckoutDetails($params{'token'});
#warn Dumper(\%CoDetails);
	my %resp = $API->DoExpressCheckoutPayment(
		'Token'		=> $params{'token'},
		'PayerID'	=> $params{'PayerID'},
		'OrderTotal'	=> $params{'total'},
		'PaymentAction' => 'Sale'
	);
#warn Dumper(\%resp);
	if ($errors = $cspp->HandleErrors(\%resp))
	{
		$errors = $q->p( $lang_blocks->{'paypal_error_1' }) . $errors;
		SimpleError();
	}
	elsif ($resp{'PaymentStatus'} =~ m/^Completed/)	# could be "Completed" -or- "Completed-Funds-Held"
	{
		my ($trans_id) = $db->selectrow_array(q#SELECT nextval(('"soa_trans_id_seq"'::text)::regclass)#);
		unless ($trans_id)
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		my $rv = $db->do("
			INSERT INTO soa (trans_id, trans_type, id, amount, description, reconciled, memo, invoice_number)
			VALUES (
				$trans_id,
				3000,
				$member_id,
				$resp{'GrossAmount'},
				?,
				TRUE,
				?,
				?
			)
			", undef, "PayPal Trans ID: $resp{'TransactionID'}", "Gross: $resp{'GrossAmount'} - Fee: $resp{'FeeAmount'} - Payer: $CoDetails{'Payer'}", $CoDetails{'InvoiceID'});
		
		unless ($rv && $rv eq '1')
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		print $q->redirect('https://www.clubshop.com/cgi/funding.cgi');
		Exit();
	}
	elsif ($resp{'PaymentStatus'} eq 'In-Progress')
	{
		$errors = "$lang_blocks->{'payment_in_progress'} $lang_blocks->{'try_reload'}";
		SimpleError();
	}
	elsif ($resp{'PaymentStatus'} eq 'Failed')
	{
		$errors = $lang_blocks->{'payment_failed'};
		startExpressCheckout_Funding();
	}
	elsif ($resp{'PaymentStatus'} eq 'Pending')
	{
		$errors = $lang_blocks->{'payment_pending'};
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Pending PayPal payment',
			'From' => 'PP.cgi@www.clubshop.com',
			'Message' => 'A PayPal funding request has come through as "Pending". Here are some details on the Checkout:' . "\n" . Dumper(\%CoDetails)
		);
		startExpressCheckout_Funding();
	}
	else
	{
		$errors = "Unexpected PaymentStatus: $resp{'PaymentStatus'}";
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Unexpected PaymentStatus PayPal payment',
			'From' => 'PP.cgi@www.clubshop.com',
			'Message' => "A PayPal funding request has come through as $resp{'PaymentStatus'}. Here are some details on the Checkout:\n" . Dumper(\%CoDetails)
		);
		startExpressCheckout_Funding();
	}
	
	# for PaymentStatus return values, see: https://www.x.com/developers/paypal/documentation-tools/api/doexpresscheckoutpayment-api-operation-soap
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub ExpressCheckout_Funding_Confirm
{
	unless (defined $params{'amount'} && isnum($params{'amount'}) && $params{'amount'} >= 10)
	{
		$errors = $lang_blocks->{'min_deposit_amount'};
		startExpressCheckout_Funding();
	}
	
	my $tmpl = $gs->GetObject($TMPL{'ExpressCheckout_Funding_Confirm'}) || die "Failed to retrieve template: $TMPL{'ExpressCheckout_Funding_Confirm'}";
	my ($fee_rate) = $db->selectrow_array("SELECT fee_label FROM soa_fees WHERE trans_type= 3000");
	my ($fee) = $db->selectrow_array("SELECT calculate_paypal_funding_fee($params{'amount'})");
	unless ($fee)
	{
		$errors = "$lang_blocks->{'notcalc'} $lang_blocks->{'try_again'}";
		startExpressCheckout_Funding();
	}
	
	my $total = $params{'amount'} + $fee;
	my ($invoice) = $db->selectrow_array(q#SELECT nextval(('"inv_num_invoice_seq"'::text)::regclass)#);
	$invoice = "CA-$mem_info->{'alias'}-$invoice";

	my %resp = $API->SetExpressCheckout(
	#	'PaymentType'		=> 'InstantOnly',	# if we need to only accept instant type payments... which we might... this is ignored for recurring payments, though
		'OrderDescription'	=> 'Club Account Funding',
		'OrderTotal'		=> $total,
		'ReturnURL'			=> ($q->url . "?x=GetTransactionDetails;sfunc=ca-funding;total=$total"),
		'CancelURL'			=> 'https://www.clubshop.com/cgi/funding.cgi',
		'NoShipping'		=> 1,
		'cpp-header-image'	=> 'https://www.clubshop.com/images/partner/cs_logo.jpg',
		'cpp-header-back-color'	=> '001A53',
		'currencyID'		=> 'USD',
		'InvoiceID'			=> $invoice
	);
	
	# if we have errors we are done
	startExpressCheckout_Funding() if $errors = $cspp->HandleErrors(\%resp);

	unless (%resp && $resp{'Token'})
	{
		$errors = 'Failed to obtain a checkout token from PayPal';
		startExpressCheckout_Funding();
	}
	
	my $lang_pref = $gs->Get_LangPref;
	my %data = (
		'ME'				=> $q->url . '?x=startExpressCheckout;sfunc=funding',
		'funding_fee_rate'	=> $fee_rate,
		'errors'			=> $errors,
		'amount'			=> sprintf('%.2f', $params{'amount'}),
		'fee'				=> sprintf('%.2f', $fee),
		'total'				=> sprintf('%.2f', $total),
		'token'				=> $resp{'Token'},
		'ppURL'				=> $cspp->destinationURL({'tag'=>'ExpressCheckout'}),
		'lang_blocks'		=> $lang_blocks,
		'ppBtnURL'			=> $cspp->ExpressCheckoutGif($lang_pref, $mem_info->{'country'})
	);
	
	my $tmp = ();
	$TT->process(\$tmpl, \%data, \$tmp) || die $TT->error();
	print $q->header('-charset'=>'utf-8');
	print $GI->wrap({
			'title'=> $lang_blocks->{'title_ca'},
			'style'=> $q->style({'-type'=>'text/css'}, '.number { text-align:right; }'),
#			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/CommStmnt.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$tmp
		});	
	Exit();
}

sub LoadLangBlocks
{
	$lang_blocks = $gs->Get_Object($db, $TMPL{'XML'}) || die "Failed to retrieve language pack template\n";
#	$tmp = decode_utf8($tmp);
	
	$lang_blocks = XML::local_utils::flatXMLtoHashref($lang_blocks);
}

=head 3 LoadParams

	We are accepting certain parameters only and then usually for certain specific values. We will enforce this in here.
	
=cut

sub LoadParams
{
	# if the value is -0-, then we expect that named parameter but the value is freestyle
	my %Allowed = (
		'x' 		=> [qw# VerifyDeposit startExpressCheckout GetTransactionDetails #],
		'dest'		=> [qw# /cgi/funding.cgi #],
		'sfunc'		=> [qw# funding funding-confirm ca-funding #],
		'id'		=> 0,
		'token'		=> 0,
		'amount'	=> 0,
		'PayerID'	=> 0,
		'total'		=> 0,
		'InvoiceID'	=> 0
	);
	
	foreach my $pn ($q->param)
	{
		if (exists $Allowed{$pn})
		{
			if ($Allowed{$pn})	# if there are expected values in the map, make sure the value passed is in the list
			{
				$params{$pn} = $q->param($pn);
				die "Unrecognized parameter value: $pn: $params{$pn}" unless grep $_ eq $params{$pn}, @{$Allowed{$pn}};
			}
			else	# otherwise we can accept whatever value
			{
				$params{$pn} = $q->param($pn);
			}
		}
		else
		{
			# if a parameter comes in that we do not recognize, we will just skip it
		}
	}
}

sub SimpleError
{
	print $q->header('-charset'=>'utf-8'), $q->start_html, $q->div($errors), $q->end_html;
	Exit();
}

sub startExpressCheckout_Funding
{
	my $tmpl = $gs->GetObject($TMPL{'startExpressCheckout_Funding'}) || die "Failed to retrieve template: $TMPL{'startExpressCheckout_Funding'}";
	my ($fee_rate) = $db->selectrow_array("SELECT fee_label FROM soa_fees WHERE trans_type= 3000");
	my %data = (
		'ME'				=> $q->url,
		'funding_fee_rate'	=> $fee_rate,
		'errors'			=> $errors,
		'lang_blocks'		=> $lang_blocks
	);
	
	my $tmp = ();
	$TT->process(\$tmpl, \%data, \$tmp) || die $TT->error();
	print $q->header('-charset'=>'utf-8');
	print $GI->wrap({
			'title'=> $lang_blocks->{'title_ca'},
			'style'=> $q->style({'-type'=>'text/css'}, '.number { text-align:right; }'),
#			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/CommStmnt.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$tmp
		});
	Exit();
}

__END__

This is a sample response from a GetExpressCheckoutDetails call in CaFunding()

$VAR1 = {
           'Street2' => '',
           'FirstName' => 'Dick',
           'PayerID' => 'KTC7EYUQEKGKE',
           'Payer' => 'tester@clubshop.com',
           'Ack' => 'Success',
           'Token' => 'EC-1T144053WD063294W',
           'PayerBusiness' => '',
           'LastName' => 'Burke',
           'AddressStatus' => 'None',
           'CityName' => '',
           'Build' => '5613839',
           'PostalCode' => '',
           'PayerStatus' => 'unverified',
           'Version' => '3.0',
           'Timestamp' => '2013-04-02T19:31:33Z',
           'CorrelationID' => 'cf68f049af821',
           'Street1' => '',
           'Name' => '',
           'StateOrProvince' => ''
         };


This is a sample response from a DoExpressCheckoutPayment call in CaFunding()

$VAR1 = {
           'TaxAmount' => '0.00',
           'PendingReason' => 'none',
           'Ack' => 'Success',
           'Token' => 'EC-1T144053WD063294W',
           'TransactionID' => '25R00520PU072313X',
           'PaymentDate' => '2013-04-02T19:08:00Z',
           'TransactionType' => 'express-checkout',
           'PaymentType' => 'instant',
           'Build' => '5613839',
           'PaymentStatus' => 'Completed',
           'Version' => '3.0',
           'Timestamp' => '2013-04-02T19:08:00Z',
           'GrossAmount' => '10.78',
           'CorrelationID' => '832d7a65a7f1',
           'FeeAmount' => '0.61',
           'ExchangeRate' => ''
         };
