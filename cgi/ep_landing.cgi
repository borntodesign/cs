#!/usr/bin/perl -w
# ep_landing.cgi
# EcoPay uses a fixed return URL, unlike other payment processors where you can pass the URL as a parameter
# So, we will give 'em this one and if we need to differentiate where we want to send somebody, we can deal with it later using a table and logic in here
# May '09... we need to differentiate for VIP upgrades, so we'll rely on a cookie for now and see how that works out

use strict;
use CGI;
 
my $q = new CGI;
# the cookie will not reside on the clubshop domain now with the app running on GI
# so we will simply send them there
print $q->redirect('https://www.glocalincome.com/cgi/ep_landing.cgi');
exit;

my $url = $q->cookie('ep_landing_url');
# we don't have a value in our cookie, the we will default to the VIP CA interface
print $q->redirect('https://www.clubshop.com/cgi/funding.cgi') unless $url;

# our URL should either have our hostname or it should be a URI on our server
# we don't know if a munged cookie will be presented, so let's not make assumptions that it is safe to simpy redirect them
die "Invalid ep_landing_url cookie" unless $url =~ m#^/# || $url =~ m#www\.clubshop\.com/?$#;

# if we receive a URI on this machine, then we need to create a fully qualified URL to redirect to
$url = 'https://' . $q->server_name . $url if $url =~ m#^/#;

print $q->redirect($url);

exit;
