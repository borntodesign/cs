#!/usr/bin/perl -w
###### member_frontline.cgi
###### 
###### Display  a member's frontline members in his/her organization.
###### The script is driven by a Member ID which is found in the login cookie.
###### 
###### Admin access is of the form:
######		http://www.clubshop.com/cgi/member_frontline.cgi?admin=1&id=<member id>
######	
###### dropped this piece on 04/07/04 by allowing admin unrestricted access
######	&mtype=<member type>
###### 
###### Created: 12/01/2003	Karl Kohrt
###### 
###### Last modified: 11/11/2013	Bill MacArthur

use CGI;
use strict;
use XML::Simple;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use XML::local_utils;
use G_S qw($LOGIN_URL);
use DB_Connect;

###### CONSTANTS
my $MTYPES = "'v','s','m','ag','tp'";			# Which members to include in the report. This value is inserted directly into the SQL
my @MEM_LEVELS = qw/v ag m tp/;			# Which members can access this report.
my %TMPL = (
	'report'	=> 10917,
	'member_row'	=> 10916,
	'lang'          => 10918,
	'csv_area'      => 10915
);
my %ICONS = (
	'cb' => qq|<img src="/images/rd-grn-blt-10x10.gif" width="10" height="10" alt="active" />|
		);

###### GLOBALS
my ($db, $admin, $member_id) = ();
my $cgi = new CGI;

my $gs = G_S->new({'db'=>\$db, 'cgi'=>$cgi});
my $ME = $cgi->self_url;

$ME =~ s#member_frontline2#FrontLine#;
print $cgi->redirect($ME);
exit;

my $member_info = {'id'=>'', 'membertype'=>''};
my $login_error = 0;
my $count = 0;
my $class = 'g';					# Start with a gray background for our info row.
my $csv_url = $ME;
my $chng_hdr = 0;
my $page_size = 500;
$csv_url =~ s/csv_file=0/csv_file=1/;
#$csv_url =~ s/;/&/g;

################################
###### MAIN starts here.
################################
#
# Get parameters
#

my $url = $cgi->self_url();
my $indx = 0;
$indx =  $cgi->param('index') || -1;
if ($indx == -1)
{
    $url =~ s/index//;
    $url .="&index=0";
    $indx = 0;
}

my $csv = $cgi->param('csv_file') || 0;
my $rpt_type = $cgi->param('report_type') || '';
if ($rpt_type eq 'm')
{
    $MTYPES = "'s'";			# Which members to include in the report. This value is inserted directly into the SQL
}
elsif ($rpt_type eq 'p')
{
    $MTYPES = "'v'";
}
elsif ($rpt_type eq 'a')
{
    $MTYPES = "'m'";
}
elsif ($rpt_type eq 'mr')
{
    $MTYPES = "'ma'";
}
elsif ($rpt_type eq 'ag')
{
    $MTYPES = "'ag'";
}
elsif ($rpt_type eq 'tp')
{
    $MTYPES = "'tp'";
}
else
{
    $MTYPES = "'m'";
}
    
# Get and test the cookie information for access.
Get_Cookie();	

# Report the member's frontline if we have a good login/cookie.
unless ( $login_error )
{
	# Make sure they are at a high enough level to access the report.
	###### we'll let admin look at anyone's frontline
	unless ( $cgi->param('admin') || (grep $_ eq $member_info->{'membertype'}, @MEM_LEVELS ))
	{
		Err("Frontline Reports are not accessible at your membership level: $member_info->{'membertype'}");
	}
	else
	{
	###### this is our hook for upline to look at a downlines report
		Report_Members() if Privileged();
	}
}

$db->disconnect if defined $db;
exit;

################################
###### Subroutines start here.
################################

sub AliasLink
{
	return $cgi->a({
		'-href' => "http://www.clubshop.com/cgi/vip/member-followup.cgi?rid=$_[0]->{'id'}",
		'-onclick' => 'window.open(this.href);return false;'
	}, $_[0]->{'alias'})
}

##### Prints an error message to the browser.
sub Err
{
	print
		$cgi->header('-expires'=>'now'),
		$cgi->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
	print "<br />$_[0]<br />";
	print $cgi->end_html();
}

# Fill out a member row.
sub Fill_Member_Row
{
	my $one_row = shift;
	my $data = shift;

	# Put in a non-breaking space where we have no values.
	foreach (keys %{$data})
	{
		unless ( defined $data->{$_} )
		{
			$data->{$_} = '&nbsp;';
		}
#		elsif ( $data->{$_} !~ m/\w/ ){
#			$data->{$_} = '&nbsp;';
#		}
	}

	# Put out an icon if there is an active ClubBucks card.
	# 4=Temporary, 5=Fixed

	$one_row =~ s/%%PROSPECTIVE_RANK%%/$data->{'prospective_rank'}/;

	if ($data->{'recent_activity'} eq '1')
	{
	    $one_row =~ s/%%ACTIVITY%%/Y/;
	}
	else
	{
	    $one_row =~ s/%%ACTIVITY%%/N/;
	}
	
	$one_row =~ s/%%PRIOR_MEMBERTYPE%%/$data->{'prior_membertype'}/;
	$one_row =~ s/%%FIRSTNAME%%/$data->{'firstname'}/;
	$one_row =~ s/%%LASTNAME%%/$data->{'lastname'}/;
	$one_row =~ s/%%ALIAS%%/AliasLink($data)/e;
	$one_row =~ s/%%EMAILSTATUS%%/$data->{'mail_option_lvl'}/;
	$one_row =~ s/%%COUNTRY%%/$data->{'country'}/;
	$one_row =~ s/%%PHONE%%/$data->{'phone'}/;
	$one_row =~ s/%%CELLPHONE%%/$data->{'cellphone'}/;
	$one_row =~ s/%%LANGUAGE%%/$data->{'language_pref'}/;
	
	# If the email addresses are blank, then just put out a blank.
	unless ($data->{'emailaddress'} eq '&nbsp;')
	{
		$one_row =~ s/%%EMAILADDRESS%%/<a href="Mailto:$data->{'emailaddress'}">$data->{emailaddress}<\/a>/;
	}
	else
	{
		$one_row =~ s/%%EMAILADDRESS%%/&nbsp;/;
	}

	unless ($data->{'emailaddress2'} eq '&nbsp;')
	{
		$one_row =~ s/%%EMAILADDRESS2%%/<a href="Mailto:$data->{'emailaddress2'}">$data->{emailaddress2}<\/a>/;
	}
	else
	{
		$one_row =~ s/%%EMAILADDRESS2%%/&nbsp;/;
	}

	$one_row =~ s/%%CLASS%%/$class/;
	$class = ($class eq 'w') ? ('g') : ('w');		# Alternate the row's color.

	# Return the new row.
	return $one_row;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# If this is an admin user.		 
	if ( $cgi->param('admin') )
	{
		$admin =  $cgi->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		my $operator = $cgi->cookie('operator');
		my $pwd = $cgi->cookie('pwd');
		require ADMIN_DB;

		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'member_frontline.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}
		unless ( $member_id = $cgi->param('id'))
		{
			Err('Missing a required parameter');
			$login_error = 1;
		}
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect( 'member_frontline.cgi' );
		$db->{'RaiseError'} = 1;

		# Get the member Login ID and info hash.
		( $member_id, $member_info ) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic') );
#warn "member_id=$member_id";
		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			$login_error = 1;
		}
		
		$gs->Prepare_UTF8($member_info);
	}

}

sub Privileged
{
	###### if we don't have a refid param then we do nothing in here
	my $refid = $cgi->param('refid');
	return 1 if (! $refid || $refid == $member_id);

	if ($refid =~ /\D/ || length $refid > 8)
	{
		Err("Bad refid received");
		return;
	}

	###### now we need to verify that the logged in party is really the upline
	###### of the refid
#	if (! $gs->Check_in_downline($db, 'members', $member_id, $refid) )
	unless ($db->selectrow_array("SELECT network.access_ck1($refid, $member_id)"))
	{
		Err("$refid was not found in your downline.");
		return;
	}

	###### change our 'login' info to reflect the refid party
	###### this way when we return true, the rest of this script can proceed
	###### without modification
	$member_info = ();
	$member_id = $refid;
	return 1;
}

###### Display the member's frontline.
sub Report_Members
{
	my $qry = '';
	my $qry2 = '';
	my $sth = '';
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $report_text = '';
	my $firstname = '';
	my $lastname = '';
	my $limits = ';';
	if ($csv eq '0')
	{
            $limits = " OFFSET $indx LIMIT $page_size ;";
	}

	# Prepare a query to get the member's frontline info.
	if ($rpt_type ne 'ma')
	{
	    $qry = "
		SELECT distinct m.id AS id,
			m.alias,
			m.firstname,
			m.lastname,
			m.membertype,
			m.mail_option_lvl,
			COALESCE(m.language_pref,'') AS language_pref,
			mt.display_code,
			COALESCE(m.country, '') AS country,
			COALESCE(m.emailaddress, '') AS emailaddress,
			COALESCE(m.emailaddress2, '') AS emailaddress2,
			COALESCE(m.phone,'') AS phone,
			COALESCE(m.cellphone,'') AS cellphone,
			mrs.last_action_time,
			mrs.current_flag as recent_activity,
			mrs.ranking_summary as prospective_rank,
			COALESCE(mt2.display_code,'N/A') AS prior_membertype
			
		FROM 	members m
		JOIN member_types mt
			ON mt.code=m.membertype
		LEFT JOIN member_ranking_summary mrs
			ON mrs.member_id = m.id
		LEFT JOIN members_prior_membertype mpm
			ON mpm.id=m.id
		LEFT JOIN member_types mt2
			ON mt2.code=mpm.membertype
		WHERE 	m.spid = ?
		AND 	m.membertype IN ($MTYPES)
		ORDER BY recent_activity, prospective_rank DESC, id DESC
                $limits";

	    $qry2 = "
			SELECT COUNT(m.id) AS max_cnt
			FROM 	members m
	       	WHERE 	m.spid = ?
			AND 	m.membertype IN ($MTYPES)";

	    $chng_hdr = 0;
	}
	else	# this is the merchant affiliate query
	{
	    $qry = "
		SELECT distinct m.id AS id,
                        m.company_name,
			m.firstname,
			m.lastname,
			m.membertype,
                        m.mail_option_lvl,
                        COALESCE(m.language_pref,'') AS language_pref,
			mt.display_code,
			COALESCE(m.country, '') AS country,
			COALESCE(m.emailaddress, '') AS emailaddress,
			COALESCE(m.emailaddress2, '') AS emailaddress2,
                        mrs.last_action_time,
                        COALESCE(mrs.current_flag, 2) as recent_activity,
                        mrs.ranking_summary as prospective_rank
		FROM 	members m
		JOIN member_types mt
			ON mt.code=m.membertype
                LEFT JOIN member_ranking_summary mrs
                        ON      mrs.member_id = m.id
                JOIN merchant_affiliates_master ma                                                   
                        ON ma.member_id = m.id                                        
         	WHERE 	m.spid = ?
                AND ma.status > 0
		AND 	membertype IN ($MTYPES)
		ORDER BY mrs.current_flag, mrs.ranking_summary DESC
                $limits";

	    $qry2 = "
			SELECT COUNT(m.id) AS max_cnt
			FROM members m
			JOIN merchant_affiliates_master ma                                                   
				ON ma.member_id = m.id                                        
			WHERE 	m.spid = ?
			AND ma.status > 0
			AND m.membertype IN ($MTYPES)";

	    $chng_hdr = 1;
	}

my $trans_xml = $gs->GetObject($TMPL{'lang'});
my $translation = XML::Simple::XMLin($trans_xml);

	$sth = $db->prepare($qry2);

	# Get the frontline data.
#	$sth->execute($member_id);

#	$data = $sth->fetchrow_hashref;
	my ($maxcnt) = $db->selectrow_array($qry2, undef, $member_id);


	$sth = $db->prepare($qry);

	# Get the frontline data.
	$sth->execute($member_id);

	my $report_row = $gs->GetObject($TMPL{'member_row'}) || die "Couldn't open $TMPL{'member_row'}";

	while ( $data = $sth->fetchrow_hashref )
	{
		$count++;

		# Add the new row to the other text rows.
		if (defined $data->{'prospective_rank'} )
		{
			$report_text .= $data->{'prospective_rank'} . ',';
		}
		else
		{
		    $report_text .= "&nbsp;,";
		}
		
		if (defined $data->{'recent_activity'}  && $data->{'recent_activity'} eq '1')
		{
			$report_text .= 'Y,';
		}
		else
		{
			$report_text .= 'N,';
		}
		
		$report_text .= $data->{'prior_membertype'} . ',';
		$report_text .= $data->{'firstname'} . ',';
		$report_text .= $data->{'lastname'} . ',';
		$report_text .= $data->{'alias'} . ',';
		$report_text .= $data->{'emailaddress'} . ',';
		$report_text .= $data->{'mail_option_lvl'} . ',';
		$report_text .= $data->{'emailaddress2'} . ',';
		$report_text .= $data->{'phone'} . ',';
		$report_text .= $data->{'cellphone'} . ',';
		$report_text .= $data->{'country'} . ',';
		
		if ($data->{'language_pref'} eq '')
		{
			$report_text .= ",\n";
		}
		else
		{
		    $report_text .= $data->{'language_pref'} . ",\n";
		}

		# Add the new row to the other HTML rows.
		$report_body .= Fill_Member_Row($report_row, $data);
	}		

	###### get our reportee info if we don't already have it
	unless ($member_info->{'alias'})
	{
		$sth = $db->prepare("
			SELECT id, alias, membertype, firstname, lastname, emailaddress
			FROM members WHERE id= ?");
		$sth->execute($member_id);
		$member_info = $sth->fetchrow_hashref;
	}

	$sth->finish();

	my $tmpl = '';
	if ($csv eq '0')
	{
	    $tmpl = $gs->GetObject($TMPL{'report'}) || die "Couldn't open $TMPL{'report'}";
	    $tmpl =~ s/%%URL%%/$csv_url/;
	}
	else
	{
	    $tmpl = $gs->GetObject($TMPL{'csv_area'}) || die "Couldn't open $TMPL{'csv_area'}";
	}

	$tmpl =~ s/%%FIRSTNAME%%/$member_info->{firstname}/;
	$tmpl =~ s/%%LASTNAME%%/$member_info->{lastname}/;
	$tmpl =~ s/%%ALIAS%%/$member_info->{alias}/;
	my $ptout = 0;
	
	if ($indx <= 0)
	{
            $tmpl =~ s/%%PREVIOUS%%//;
	}
	else
	{
            my $prev_link = "<a href=\"$url\">Previous</a>";
            my $tmpindx = $indx - $page_size;
            my $chk = "index=$indx";
            $ptout = "index=$tmpindx";
            $prev_link =~ s/$chk/$ptout/;
            $tmpl =~ s/%%PREVIOUS%%/$prev_link/
	}
	
	if ($indx + $page_size >= $maxcnt)
	{
            $tmpl =~ s/%%NEXT%%//
	}
	else
	{
            my $next_link = "<a href=\"$url\">Next</a>";
            my $tmpindx = $indx + $page_size;
            my $chk = "index=$indx";
            $ptout = "index=$tmpindx";
            $next_link =~ s/$chk/$ptout/;
            $tmpl =~ s/%%NEXT%%/$next_link/
	}
	
	my $tmpindx2 = $indx + $page_size;
	
	if ($page_size > $count)
	{
            $tmpindx2 = $indx + $count;
	}
	
	my $range = "Displaying Records $indx to $tmpindx2";
	$tmpl =~ s/%%RANGE%%/$range/g;
	$report_text ||=  'Sorry - No members were found.';

	$tmpl =~ s/%%DISCLAIMER%%/$translation->{disclaimer}/g;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$tmpl =~ s/%%REPORT_TEXT%%/$report_text/g;
	$tmpl =~ s/%%COUNT%%/$count/g;

	if ($chng_hdr == 1)
	{
            $tmpl =~ s|<th align="left">Prospect Rank</th>||;
            $tmpl =~ s|<th align="left">Active</th>||;
            $tmpl =~ s|Trial Pos|Company Name|;
	}
	print $cgi->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $cgi->script_name;
	print $cgi->header('-expires'=>'now'),
		$cgi->start_html('-bgcolor'=>'#ffffff');
	print "Please login <a href=\"$LOGIN_URL?destination=$ME\">HERE</a>.<br /><br />";
	print scalar localtime(), "(PST)";
	print $cgi->end_html;
}


###### 12/31/03 Added the CBID status field to the query and report.
###### 01/05/04 Changed: if ( $data->{cb_class} == 4 || $data->{cb_class} == 5 )
######			to: if ( $data->{cb_class} eq '4' || $data->{cb_class} eq '5' )
######			since cb_class may = '&nbsp;' in some cases.
###### 11/17/04 added several SQL coalesces in the report query
###### 01/25/05 lowered the threshold for access permission
# 08/30/06 added AGs to the list of frontline members AND gave AGs access to the report as well
###### 06/11/09 added the new ma membertype to the viewable ones, also revised the way the list is used in the SQL
# removed the ma membertype as they end up in the mailing list
# 11/12/10 added AMPs to the member list and the access list
###### 04/21/11 added a link on the alias and chanaged a/c to skype name
###### 05/02/11 removed the spaces in the CSV list
# 04/02/12 made it available to the 'm' membertype
# 04/03/12 made the report types align with the current business logic... ie. changed the Member report to just s membertypes and the Affiliate report to m membertypes
###### 10/01/12 ripped out lots of the trial position stuff
###### 10/17/12 Made the default report pull membertype 'm' instead of AGs... geesh
###### Also ripped out a bunch of needless joins in the COUNT queries in Report_Members()
###### 04/09/13 made the access check use the network.access_chk1() routine instead of the G_S method
###### 11/11/13 added the prior membertype into the mix