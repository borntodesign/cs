#!/usr/bin/perl -w
###### ebiz-grading.cgi

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw($LOGIN_URL);
use ebph qw($PASS_HANDLER_ROOT $EBIZ_ROOT);
use DB_Connect;

###### Globals
our $q = new CGI; 
our ($db, $id, $meminfo, %params, @questions, %Grades) = ();
our $correct_list = '';
our $incorrect_list = '';
our $correct = 0;

our %TMPL = (
	default_pass	=> 10290,
	default_fail	=> 10289
);
###### conditional 1 is rendered if they have gotten some correct but have not passed
#our %conditionals = ( 1 => 0 );

###### load up our params - we've got lots of 'em
foreach ($q->param)
{
	###### since we will be comparing answer keys to named parameters
	###### ie. answer->{key} == param{key}
	###### we need to convert our radio answer parameters into the correct key
	if (/^radio/)
	{
		$params{$q->param($_)} = 1;
	}
	else
	{
		$params{$_} = $q->param($_) || '';
	}
}

unless ($params{course_key})
{
	Err('No course key received.');
	exit;
}
elsif ($params{course_key}  =~ /\D/ || $params{course_key} > 2000000000)
{
	Err("Invalid course key received: $params{course_key}");
	exit;
}

( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### the intent is to have this script behind an apache protected directory
###### so login issues will be taken care of by that part
###### we just put this in here for back up and external directory testing
unless ( $id && $id !~ /\D/ )
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
	exit;
}

###### if they are not a VIP, they don't have access to the report
if ( $meminfo->{membertype} !~ 'v' )
{
	Err('Access to the E-Business Materials is restricted to VIPs.');
	exit;
}

unless ($db = DB_Connect('ebiz')){ exit }
$db->{RaiseError} = 1;

###### check to see if they have an existing session under this test number
###### also get the correct answers from any previous test
my $sth = $db->prepare("
	SELECT key,
		CASE
			WHEN correctly_answered > '' THEN correctly_answered
			ELSE ''
		END AS correct_answers,
		grade_date,
		passed,
		grade
	FROM ebiz_sessions
	WHERE 	id= $id AND course_key= $params{course_key}");
$sth->execute;
my $session = $sth->fetchrow_hashref;
$sth->finish;

unless ($session->{key})
{
	Err("We're sorry but it appears that you haven't accessed the course materials.");
	goto 'END';
}

###### let's get some particulars on the course itself
###### they may come in handy for adding to the page
$sth = $db->prepare("
	SELECT key AS course_key,
		active,
		name AS course_name,
		pass_grade,
		course_root,
		pass_handler
	FROM 	ebiz_courses
	WHERE 	key= $params{course_key}");
$sth->execute;
my $course = $sth->fetchrow_hashref;
$sth->finish;

###### this is a long shot, but in the event someone has bookmarked an extinct course
unless ($course->{active})
{
	Err("Course: $params{course_key}, is not an active course.");
	goto 'END';
}									

###### pull up all our questions
$sth = $db->prepare("
	SELECT ebtq.key,
		question_number,
		question,
		COALESCE(ebl.content, '') AS lesson
	FROM 	ebiz_test_questions ebtq
	LEFT JOIN ebiz_lessons ebl
	ON 	ebl.lesson_id=ebtq.refers_to
	AND 	ebl.course_key=ebtq.course_key
	WHERE 	ebtq.course_key= $params{course_key}
--	AND 	ebtq.question_number NOT IN ($session->{correct_answers})
	ORDER BY question_number");
$sth->execute;

###### this is our answer query
my $answer_sth = $db->prepare("
	SELECT key,
		answer_number,
		question_key,
		label,
		COALESCE(value, '') AS value,
		CASE
			WHEN alignment > '' THEN alignment
			ELSE 'v'
		END AS alignment,
		control_type
	FROM ebiz_test_answers WHERE question_key= ?
	ORDER BY answer_number");

while (my $tmp = $sth->fetchrow_hashref)
{
	$answer_sth->execute($tmp->{key});
	while (my $x = $answer_sth->fetchrow_hashref)
	{
		push (@{$tmp->{answers}}, $x);
	}
	push (@questions, $tmp);
}
$answer_sth->finish;
$sth->finish;

###### we should now have our list of questions with the answers in an array off the
###### answers key
foreach my $question (@questions)
{
	###### initialize our question grade
	$Grades{$question->{key}} = 1;

	###### by skipping the evaluation of ones we already got right,
	###### we automatically mark them correct and we'll rebuild our 'correct' list
	###### for insertion into the session record for the next time around if we
	###### still don't pass
	unless ($session->{correct_answers} =~ /(^|,)$question->{question_number}(,|$)/)
	{

	# radio buttons and checkboxes transfer a value only if they are checked
	# so we can look for answers that are correct and see if the corresponding
	# param is received
	# on the contrary, if the param is received and the answer is not correct
	# we know we have an incorrect
	# text boxes can be compared on contents or lack thereof
		foreach my $answer (@{$question->{answers}})
		{
			unless ($answer->{value} eq ($params{$answer->{key}} || ''))
			{
				###### mark it wrong
				$Grades{$question->{key}} = 0;
				last;
			}
		}
	}

	if ($Grades{$question->{key}})
	{
		$correct++;
		$correct_list .= "$question->{question_number},";
	}
	else
	{
		$incorrect_list .= "$question->{question_number},";
	}
}

chop $correct_list;
chop $incorrect_list;

my $num_of_questions = scalar @questions;
my $wrong = $num_of_questions - $correct;
my $grade = int (($correct/$num_of_questions) * 100);

###### update their session record with their score
$db->do("
	UPDATE ebiz_sessions SET
		passed= ${\(($grade >= $course->{pass_grade}) ? 'TRUE' : 'FALSE')},
		grade= $grade,
		grade_date= NOW(),
		correctly_answered= '$correct_list',
		incorrectly_answered= '$incorrect_list',
		ipaddress= '${\$q->remote_addr}'
	WHERE 	key= $session->{key}");
		
if ($grade >= $course->{pass_grade})
{
	Default_Pass_Handler();
}
else
{
	Default_Fail_Handler();
}

END:
$db->disconnect;
exit;

sub Default_Fail_Handler
{
	###### this most likely will be the only fail handler
	###### we'll spit out their score and show them the ones they got wrong
	###### at this time, we'll also refer them to the 'lesson' which covers
	###### that particular question

	my $pass_icon = '<script>document.write(pass_icon)</script>';
	my $fail_icon = '<script>document.write(fail_icon)</script>';
	my ($tbl) = ();

	###### first we'll create our list of questions
	###### at this point, we'll show all of them and mark them appropriately
	###### they might decide to only show the wrong ones later
	foreach my $question (@questions)
	{
		$tbl .= $q->Tr(
			$q->td({-valign=>'top', -align=>'center'},
				$Grades{$question->{key}} ? $pass_icon : $fail_icon) .
			"\n" .
			$q->td({-valign=>'top'},
				$q->a({-href=>"$EBIZ_ROOT/$course->{course_root}/$question->{lesson}",
					-target=>'_blank'},
					$question->{question_number})) .
			"\n" .
			$q->td($question->{question}) .
			"\n"
		) . "\n";
	}
	my $tmpl = G_S::Get_Object($db, $TMPL{default_fail}) ||
		die "Failed to load template: $TMPL{default_fail}\n";

	$tmpl =~ s/%%grade%%/$grade/;

	###### by placing $1 as the last option, we preserve any placeholders
	###### instead of erasing them
	###### by doing so we can put %%tbl%% after, avoiding having to parse all
	###### the text it will insert
	$tmpl =~ s/%%(.*?)%%/($meminfo->{$1} || $course->{$1} || '%%'.$1.'%%')/eg;
	$tmpl =~ s/%%table%%/$tbl/;
	print $q->header(-expires=>'now'), $tmpl;
}

sub Default_Pass_Handler
{
	###### the idea behind this routine is to simply update records
	###### possibly provide congratulations
	###### possibly show what they may have gotten wrong
	###### and then shuttle them onto the 'real' handler which is outside the real
	###### scope of the ebiz system

	if ($course->{pass_handler})
	{
		if ($course->{pass_handler} !~ /^http/)
		{
			$course->{pass_handler} = "$PASS_HANDLER_ROOT/$course->{pass_handler}";
		}
		print $q->redirect($course->{pass_handler});
	}
	else
	{
		my $tmpl = G_S::Get_Object($db, $TMPL{default_pass}) ||
			die "Failed to load template: $TMPL{default_pass}\n";

		$tmpl =~ s/%%grade%%/$grade/;
		$tmpl =~ s/%%(.*?)%%/($meminfo->{$1} || $course->{$1})/eg;
		print $q->header(-expires=>'now'), $tmpl;
	}
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />';
	print $q->h4($_[0]), scalar localtime(), $q->end_html();
}





