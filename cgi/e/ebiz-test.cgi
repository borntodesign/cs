#!/usr/bin/perl -w
###### ebiz-test.cgi
###### originally deployed: 04/29/04	Bill MacArthur
###### last modified: 01/12/06	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw($LOGIN_URL);
use ebph qw($PASS_HANDLER_ROOT $EBIZ_ROOT $ALERT_TEMPLATE);
use EbizPrereq;
use DB_Connect;

###### Globals
our $q = new CGI; 
our ($db, $id, $meminfo, $testnum, @questions, $tbl) = ();

###### unfortunately, we have to use pretest criteria checks
###### these of course, are tied to team levels rather than strictly the course itself
###### for now we'll create a map that relates a particular course code to a level
###### this is kludgy, but it will work for now
###### the hash key is the course key
my %course2level_map = (
	42	=> 10,		# Trainee I
	31	=> 20, 	# Trainee II
	25	=> 30, 	# Trainee III
	1562	=> 40,		# Builder Intern
	2391	=> 100,	# Builder Team Coach
	3214	=> 110,	# Strategist Team Coach
	4105	=> 115,		# Trainer Team Coach
	4294	=> 120,	# Marketing Director
	4834	=> 130,	# Promotions Director
	12966	=> 140	# Network Chief
);

###### conditional 1 is rendered if they have gotten some correct but have not passed
###### 2 is used on a retest alert to render a link to further information if available
our %conditionals = ( 1 => 0, 2 => 0 );

($testnum = $q->path_info) =~ s#^/##;
unless ($testnum)
{
	Err('No test number received.');
	exit;
}
elsif ($testnum  =~ /\D/ || $testnum > 2000000000)
{
	Err("Invalid test number received: $testnum");
	exit;
}

( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### the intent is to have this script behind an apache protected directory
###### so login issues will be taken care of by that part
###### we just put this in here for back up and external directory testing
unless ( $id && $id !~ /\D/ )
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
	exit;
}

###### if they are not a VIP, they don't have access to the report
if ( $meminfo->{membertype} !~ 'v|staff' )
{
	Err('Access to the E-Business Materials is restricted to VIPs.');
	exit;
}

unless ($db = DB_Connect('ebiz')){ exit }
$db->{RaiseError} = 1;

###### check to see if they have an existing session under this test number
###### also get the correct answers from any previous test
my $sth = $db->prepare("
	SELECT key,
		CASE
			WHEN correctly_answered > '' THEN correctly_answered
			ELSE '0'
		END AS correct_answers,
		COALESCE(incorrectly_answered, '') AS wrong_answers,
		grade_date::DATE,
		passed,
		COALESCE(grade, 0)::TEXT AS grade
	FROM ebiz_sessions
	WHERE 	id= $id AND course_key= $testnum");
$sth->execute;
my $session = $sth->fetchrow_hashref;
$sth->finish;

unless ($session->{key})
{
	Err("We're sorry but it doesn't appear that you have accessed the course materials.");
	goto 'END';
}

###### let's get some particulars on the course itself
###### they may come in handy for adding to the page
$sth = $db->prepare("
	SELECT key AS course_key,
		active,
		name AS course_name,
		pass_handler,
		course_root
	FROM 	ebiz_courses
	WHERE 	key= $testnum");
$sth->execute;
my $course = $sth->fetchrow_hashref;
$sth->finish;

###### this is a long shot, but in the event someone has bookmarked an extinct course
unless ($course->{active})
{
	Err("Course: $testnum, is not an active course.");
	goto 'END';
}									

###### allow easy 01 'admin' access by skipping the access checks
###### this may end up having other ramifications - we'll see :)
if ($id > 1)
{
	goto 'END' unless TestACL();
}

goto 'END' unless RetestCK();

$db->do("
	UPDATE ebiz_sessions SET
		test_begun= NOW(),
		correctly_answered= '$session->{correct_answers}',
		incorrectly_answered= '$session->{wrong_answers}',
		ipaddress= '${\$q->remote_addr}'
	WHERE 	key= $session->{key}");

$sth = $db->prepare("
	SELECT ebtq.key,
		ebtq.question_number,
		ebtq.question,
		COALESCE(ebl.content, '') AS lesson
	FROM 	ebiz_test_questions ebtq
	LEFT JOIN ebiz_lessons ebl
	ON 	ebl.lesson_id=ebtq.refers_to
	AND 	ebl.course_key=ebtq.course_key
	WHERE 	ebtq.course_key= $testnum
	AND 	ebtq.question_number NOT IN ($session->{correct_answers})
	ORDER BY question_number");
$sth->execute;

my $answer_sth = $db->prepare("
	SELECT key,
		answer_number,
		question_key,
		label,
		value,
		CASE
			WHEN alignment > '' THEN alignment
			ELSE 'v'
		END AS alignment,
		control_type
	FROM ebiz_test_answers WHERE question_key= ?
	ORDER BY answer_number");

while (my $tmp = $sth->fetchrow_hashref)
{
	$answer_sth->execute($tmp->{key});
	while (my $x = $answer_sth->fetchrow_hashref)
	{
		push (@{$tmp->{answers}}, $x);
	}
	push (@questions, $tmp);
}
$answer_sth->finish;
$sth->finish;

###### we should now have our list of questions with the answers in an array off the
###### answers key
my %ctl_map = (
	text		=> \&Ctl_text,
	radio		=> \&Ctl_radio,
	checkbox	=> \&Ctl_checkbox
);
foreach my $question (@questions)
{
	###### in order to provide a certain amount of layout functionality
	###### we'll start with the question and then determine if our answers
	###### are supposed to be laid out vertically or horizontally
	$tbl .= qq|<tr><td class="n" valign="top" align="center"><a href="$EBIZ_ROOT/$course->{course_root}/$question->{lesson}" target="_blank">$question->{question_number}</a>.&nbsp;</td>
		<td class="q">$question->{question}</td></tr>\n|;

	###### since alignment ought to match across answers as should the control type
	###### we'll base our design logic around our first answer
	my ($align, $ctl, $answer_cell) = ();
	foreach my $answer ( @{$question->{answers}} )
	{
		$align ||= $answer->{alignment};
		$ctl ||= $answer->{control_type};

		if ($align eq 'v')
		{
			$answer_cell .= $q->Tr(
				$q->td(
					{-class=>'a', -align=>'right'},
					$answer->{label}) . "\n" .
						$q->td(
						{-class=>'ctl'},
						$ctl_map{$ctl}($answer))
			) . "\n";
		}
		else
		{
			$answer_cell .= $q->td(
				{-class=>'a', -align=>'center'},
				$answer->{label} . $ctl_map{$ctl}($answer)
			) . "\n";
		}
	}

	###### if alignment was vertical, then we'll get rid of the trailing linebreak
	###### and put a closing td and tr on it
	if ($align ne 'v')
	{
		$answer_cell = "<tr>$answer_cell</tr>\n";
	}

	###### create another row with our answers encapsulated in their own tbl
	#  align="${\($align eq 'v' ? 'right' : 'center')}"
	$tbl .= qq|
		<tr><td align="right" colspan="2">
		<table cellspacing="0">$answer_cell</table></td></tr>\n|;

	###### create a nice thin line spacer
	$tbl .= qq|
		<tr><td class="spacer" colspan="2"><img src="/images/blanklin.gif" alt="" height="1" width="1" /></td>
		</tr>\n|; 
}

my $tmpl = G_S::Get_Object($db, 10287) ||
	die "Failed to load main template: 10287\n";

print $q->header(-expires=>'now');	
$tmpl =~ s/%%table%%/$tbl/;

###### we could write explicit 'course' substitutions, but this works well
###### and retains the flexibility of the one-line system
$tmpl =~ s/%%(.*?)%%/($meminfo->{lc $1} || $course->{lc $1} || $session->{lc $1})/eg;

$conditionals{1} = 1 if $session->{correct_answers};
foreach (keys %conditionals)
{
	unless ($conditionals{$_})
	{
		$tmpl =~ s/conditional-$_-open.+?conditional-$_-close//sg;
	}
}
print $tmpl;

END:
$db->disconnect;
exit;

sub Ctl_checkbox
{
	return $q->checkbox(
		-name=>$_[0]->{key},
		-value=>1,
		-label=>''
	);
}

sub Ctl_radio
{
	###### because radio buttons all need the same name to function correctly
	###### we'll have to assign the answer key to the value of the button
	###### this way we can check the right key for a TRUE on the test grading end
	return qq|<input type="radio" name="radio$_[0]->{question_key}" value="$_[0]->{key}" />\n|;
}

sub Ctl_text
{
	return $q->textfield(-name=>$_[0]->{key});
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />';
	print $q->h4($_[0]), scalar localtime(), $q->end_html();
}

sub RetestCK
{
###### if they have already taken the test, then we'll advise them and verify that
###### they want to take it again
###### if they are retesting, then we'll have to force the correct answer list empty
	if ($q->param('retest'))
	{
		$session->{correct_answers} = 0;

		###### reset their answer lists for the grading script
		$session->{correct_answers} = '0';
		$session->{wrong_answers} = '';
	}

	return 1 if ((! $session->{passed}) || $q->param('retest'));

	my $tmpl = G_S::Get_Object($db, 10286) ||
		die "Failed to load template 10286\n";

	if ($course->{pass_handler})
	{
		###### if they have entered some kind of static page,
		###### it should have been a complete URL
		if ($course->{pass_handler} !~ /^http/)
		{
			$course->{pass_handler} = "$PASS_HANDLER_ROOT/$course->{pass_handler}";
		}
	}

	###### we will not render the link if the pass handler is a static page
	$conditionals{2} = 1 if $course->{pass_handler} =~ m#/cgi/#;

	foreach (keys %conditionals)
	{
		unless ($conditionals{$_})
		{
			$tmpl =~ s/conditional-$_-open.+?conditional-$_-close//sg;
		}
	}
	$tmpl =~ s#%%action%%#($q->script_name . "/$course->{course_key}")#eg;
	$tmpl =~ s/%%(.*?)%%/($session->{lc $1} || $course->{lc $1})/eg;
	print $q->header(-expires=>'now'), $tmpl;
	return;
}

sub TestACL
{
###### if we have any checks before allowing them in, this is the place to put them
	die "Course Key: $course->{course_key} is not mapped\n" unless $course2level_map{$course->{course_key}};
	my $cks = EbizPrereq::Get_By_Level($db, $course2level_map{$course->{course_key}}) ||
		die "Failed to retrieve Criteria Checks";
	my (@failures) = ();

	foreach my $ck (@{$cks})
	{
		my $ck_ = eval $ck->{object};
		my $rv = &$ck_($meminfo);
		push (@failures, $ck->{label}) unless $rv;
	}

	return 1 unless @failures;

	my $tmpl = G_S::Get_Object($db, $ALERT_TEMPLATE) ||
		die "Failed to load template: $ALERT_TEMPLATE\n";

	$tmpl =~ s/%%alert%%/$q->ul( $q->li(\@failures))/e;
	$tmpl =~ s/%%(.*?)%%/($meminfo->{lc $1} || $course->{lc $1} || $session->{lc $1})/eg;
	print $q->header(-expires=>'now'), $tmpl;
	
	return;

}

###### 10/07/04 - major revision
###### moved the criteria check routines from the pass handlers into here so as to do
###### pretest criteria checking
###### 11/18/04 added the Trainer level mapping
###### also added an error check for an undefined course2level map value
###### 12/20/04 added the Marketing Director mapping
###### 09/08/05 added Promotions Director mapping
# 01/12/06 added mapping for Network Chief