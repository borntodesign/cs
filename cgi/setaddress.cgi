#!/usr/local/bin/perl
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use LWP::UserAgent;
use URI::Escape;
use G_S;
use ParseMe
   require XML::Simple;
   require XML::local_utils;
use Data::Dumper;
use CGI;
use CGI::Cookie;

my $cgi = new CGI;
my $gs = new G_S;
#
#
my %TMPL = ('xml'=>10897);
my $cgiini = "ipaddress.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
#
# Get the translation hash to remove the text at the end of the location string
#
my $xmt = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to load template: $TMPL{xml}\n";
    $xmt =~ s/\n|\r|^M//g;
my $lu = new XML::Simple;
my $trans = $lu->XMLin($xmt);
chomp($trans->{translate}->{rewardsmap}->{"currentloc.shtml"});
chomp($trans->{translate}->{rewardsmap}->{"defaultloc.shtml"});

my $agent = LWP::UserAgent->new;
my $xml = new XML::Simple;

my $def_location = $cgi->cookie('rewards_def_location');
#
#
#
my $result={};
my $params= {};
my $mlat = 0;
my $mlong = 0;
my $qry = "";
my $def_address = "";
my $ii = 0;
#
# Get The form input (using GET) these are currently location, change, check
#
my @formdata = split (/&/, $ENV{'QUERY_STRING'});
foreach  my $pair (@formdata) {
    my ($key, $value) = split(/=/, $pair);
    $value =~ tr/+/ /;
    $value =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
    $params->{$key} = $value;
}
warn Dumper($params);
#
# Check if location has our default stuff in it, and if so remove it
#
if ($params->{location}) {
    $params->{location} =~ s/$trans->{translate}->{rewardsmap}->{"currentloc.shtml"}//;
    $params->{location} =~ s/$trans->{translate}->{rewardsmap}->{"defaultloc.shtml"}//;
}

#
# Check for problem with microsoft version 7 & 8 not supporting the button construct(at all)
#
if (!defined $params->{change} && !defined $params->{home}) {
    my $cur_cookie = $cgi->cookie('rewards_cur_cookie');
    my $cooks = split("|", $cur_cookie);
    my $def_address = $params->{location};
    warn "setaddress: " . $def_address . "\n";
}
if (defined $params->{change}) {
    warn "into change section\n";
    $def_address = $params->{location};
#
# Check to see if cur cookie exists
#
    my $cur_cookie = $cgi->cookie('rewards_cur_cookie');
#    warn "Current_cookie is: " . Dumper($cur_cookie);
#
# Convert the location typed into the location box and convert it to longitude/latitude
#
   my $url = 'http://maps.googleapis.com/maps/api/geocode/xml?address=' . $def_address . '&sensor=false';
   my $req = HTTP::Request->new(GET=>$url);
   my $res = $agent->request($req);
   my $km_miles = 'KM';
   if ($res->is_success) {
       $result = $xml->XMLin($res->content);
#       warn Dumper($result);
       if (ref($result->{result}) eq "HASH") {
           $mlat = $result->{result}->{geometry}->{location}->{lat};
           $mlong = $result->{result}->{geometry}->{location}->{lng};
           if (ref($result->{result}->{address_component}) eq "ARRAY") {
	       for ($ii = 0; $ii<length($result->{result}->{address_component}); $ii++) {
		   if ($result->{result}->{address_component}[$ii]->{short_name} eq "US") {
		       $km_miles = 'Miles';
		   }
	       }
	   }
           else {
               if ($result->{result}->{address_component}->{short_name} eq "US") {
	           $km_miles = 'Miles';
	       }
	   }
       }
       elsif (ref($result->{result}) eq "ARRAY") {
           $mlat = $result->{result}[0]->{geometry}->{location}->{lat};
           $mlong = $result->{result}[0]->{geometry}->{location}->{lng};
           if (ref($result->{result}[0]->{address_component}) eq "ARRAY") {
               for ($ii = 0; $ii<length($result->{result}[0]->{address_component}); $ii++) {
	           if ($result->{result}[0]->{address_component}[$ii]->{short_name} eq "US") {
		       $km_miles = 'Miles';
		   }
	       }
	   }
           else {
               if ($result->{result}[0]->{address_component}->{short_name} eq "US") {
                   $km_miles = 'Miles';
	       }
	   }
       }
       else {
           $mlat = 0.0;
           $mlong = 0.0;
           $km_miles = 'KM';
       }
   }
#
# If the saved box is checked, we need to save the information into the default cookie otherwise save it to the current
# location cookie
#
  my $locdata = $def_address . " | ". $mlong . " | ". $mlat . " | " . $params->{range} ." | " . $km_miles ."|Typed IN";
#warn $locdata;
  my $cookie = $cgi->cookie(-name=>"rewards_def_location",-expires=>"+1y", -domain=>"clubshop.com",  -value=>$locdata);
  my $cookie2 = " ";
  if ($params->{check}) {
      $cookie = $cgi->cookie(-name=>"rewards_def_location",-expires=>"+1y", -domain=>"clubshop.com",  -value=>$locdata);
      $cookie2 = $cgi->cookie(-name=>"rewards_cur_location",-expires=>"-1y", -domain=>"clubshop.com",  -value=>$locdata);
  }
  else {
      $cookie = $cgi->cookie(-name=>"rewards_cur_location",-expires=>"+30m",-domain=>"clubshop.com",  -value=>$locdata);
  }
#    print $cgi->trailer();
#
# redirect to the ipaddress page
#    print "Location:http://www.clubshop.com/maps/rewardsdirectory.shtml\n\n";
    if ($cookie2 ne " ") {
         print $cgi->redirect(-URL=> "http://www.clubshop.com/maps/rewardsdirectory.shtml", -cookie=>[$cookie,$cookie2]);
    }
    else {
         print $cgi->redirect(-URL=> "http://www.clubshop.com/maps/rewardsdirectory.shtml", -cookie=>$cookie);
    }
}
#
# Check for home button being clicked
#
elsif (defined $params->{home}) {
    warn "into home section\n";
    my $cookie2 = '';
    my $locdata = '';
    $cookie2 = $cgi->cookie(-name=>"rewards_cur_location",-expires=>"-1y", -domain=>"clubshop.com",  -value=>$locdata);
    print $cgi->redirect(-URL=> "http://www.clubshop.com/maps/rewardsdirectory.shtml", -cookie=>$cookie2);
  }
else {
    warn "Got down to gods country\n";
    print $cgi->redirect(-URL=>"http://www.clubshop.com/maps/rewardsdirectory.shtml");
}
