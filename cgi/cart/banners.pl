#!/usr/bin/perl

$AffPath   = "http://www.clubshop.com/cgi"; # No Trailing Forward Slash!
$AffLink   = "http://www.clubshop.com";
$AffData   = 'aff.dat';
$Banners   = 'banners.txt';
&GetFormInput;
$MemberID  = "$field{'ID'}";
print "Content-type: text/html\n\n";
print "<HTML><HEAD><TITLE>Affiliate Banners</TITLE></HEAD>\n";
print "<BODY><br><br>\n";
print "<div align=\"center\"><table width=\"700\" align=\"center\" border=\"0\">\n";
print "<tr><td><p><font face=\"arial\" size=\"2\"><strong>Affiliate Banners</strong></font></P>\n";
print "<p><font face=\"arial\" size=\"2\">Please copy the link information from within the text box and paste to your site.</font></p>\n";
open(BANR,$Banners);
@LINES = <BANR>;
close(BANR);
 foreach $line (@LINES)
  {
  ($Path,$Desc,$Width,$Height) = split(/\|/,$line);
  if ($Path ne "")
   {
   print "<p><img src=\"$Path\" width=\"$Width\" height=\"$Height\" alt=\"$Desc\" border=\"0\"><br>\n";
   print "<textarea rows=\"4\" cols=\"70\">";
   print "<a href=\"$AffPath/cart.pl\?ID=$MemberID\"><img src=\"$Path\" width=\"$Width\" height=\"$Height\" alt=\"$Desc\" border=\"0\"></a></textarea></p><br>\n";
   }
  }
print "<p><font face=\"arial\" size=\"2\">Or you can use a simple link with no picture:</font></p>\n";
print "<p><textarea rows=\"2\" cols=\"70\">";
print "<a href=\"$AffPath/cart.cgi\?ID=$MemberID\">Store Manager</a></textarea></p><br>\n";
print "<p><a href=\"$AffLink\">Home</a></p>";
print "</td></tr></table></div><br>\n";
print "</BODY></HTML>\n";

sub GetFormInput
 {
 (*fval) = @_ if @_ ;
 local ($buf);
 if ($ENV{'REQUEST_METHOD'} eq 'POST') { read(STDIN,$buf,$ENV{'CONTENT_LENGTH'}); }
 else {	$buf=$ENV{'QUERY_STRING'}; }
 if ($buf eq "") { return 0 ;	}
 else {
 	@fval=split(/&/,$buf);
	foreach $i (0 .. $#fval)
         {
	 ($name,$val)=split (/=/,$fval[$i],2);
	 $val=~tr/+/ /;
	 $val=~ s/%(..)/pack("c",hex($1))/ge;
	 $name=~tr/+/ /;
	 $name=~ s/%(..)/pack("c",hex($1))/ge;
	 if (!defined($field{$name})) {	$field{$name}=$val; }
	 else {	$field{$name} .= ",$val"; }
         }
      }
return 1;
}

exit;

