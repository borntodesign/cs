#!/usr/bin/perl
# StoreCart v.1.0 auth.pl
# Works with Authorize.Net, ECheck, and Pre-Payment accounts.
#
# Major revision 03-15-2006 to allow for DHS Pre-Payment account option.
#
#############################################################################
# ChargeCart By JMS Online Systems                                          #
# (c) 2000,2001 All rights reserved.                                        #
# www.jmsonline.net Custom ecommerce programming is our specialty!          #
#############################################################################
#
###### auth.cgi	last modified 10/18/07	Bill MacArthur
#

use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use CGI;
use G_S;
use CartMail;
our $q = new CGI; 

$| =1;
print $q->header();

our %CARTNAMES = ('cart2'	=> 'Test Cart',
			'cart'	=> 'Merchant Affinity Group Cart',
			'uscart'	=> 'Business Center Cart',
			'benefitscart'=> 'CS Mall Cart' );
my %TEMPLATES = (	payoptions => "$G_S::HTML_TMPL_DIR/cart/payoptions.html");
my $script_name = $q->script_name();
(our $CartDir = $script_name) =~ s/auth\.cgi$/carts/;
$CartDir = '/home/httpd' . $CartDir;
(our $parent_dir = $script_name) =~ s/\/auth\.cgi$//;
$parent_dir =~ s/^\/cgi\///;
our $PP_TRANS_TYPE = 4900;		# Mall Miscellaneous transaction type.
our $PP_ORDER_DESC = $CARTNAMES{$parent_dir};	# Additional description for the Pre-Pay transaction.

my ($success, $auth_problem) = '';
my ($hidden);
foreach ($q->param()){ $hidden .= $q->hidden($_) . "\n" unless /action/ }

###### For troubleshooting.
my $printFields = "NO";

$mail_prog = '/usr/sbin/sendmail' ;
&GetFormInput;

if($printFields eq "YES") { exit; }

$Rate      = '0.10'; # Commission Rate
$AffData   = "affsales.dat";
$OrderFile = "sales.txt";

our $Transactionamount = $field{'x_Amount'};
our $Shipping          = "$field{'Shipping'}";
our $ShipHow           = "$field{'ShipHow'}";
our $Tax               = "$field{'Tax'}";
our $Orderstring       = "$field{'Orderstring'}";
our $SName             = "$field{'x_ship_to_first_name'} $field{'x_ship_to_last_name'}";
our $SAddress          = "$field{'x_ship_to_address'}";
our $SCity             = "$field{'x_ship_to_city'}";
our $SState            = "$field{'x_ship_to_state'}";
our $SZip              = "$field{'x_ship_to_zip'}";
our $Email             = "$field{'x_Email'}";
our $Phone             = "$field{'x_Phone'}";
our $BName             = "$field{'x_First_Name'} $field{'x_Last_Name'}";
our $BAddress          = "$field{'x_Address'}";
our $BCity             = "$field{'x_City'}";
our $BState            = "$field{'x_State'}";
our $BZip              = "$field{'x_Zip'}";
our $Country           = "$field{'x_Country'}";
our $InvID             = "$field{'x_Invoice_Num'}";
our $InvID             = "$field{'x_Invoice_Num'}";
our $Response          = "$field{'x_response_code'}";
our $Reason            = "$field{'x_response_reason_text'}";
our $TransID           = "$field{'x_trans_id'}";
our $AVS               = "$field{'x_avs_code'}";
# Get the member Login ID and info hash from the VIP cookie.
our ( $member_id, $mem_info ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
my $OrderID = $mem_info->{alias} || '';

# If there is no action.
unless ($field{'action'})
{
	###### present our payment template filled out as much as possible
	open (TMPL, "$TEMPLATES{'payoptions'}") || die "Couldn't open $TEMPLATES{'payoptions'}\n";
	my $name = $field{'x_First_Name'} . ' ' . $field{'x_Last_Name'};

	foreach (<TMPL>)
	{
		s/%%hidden%%/$hidden/;
		s/%%action%%/$script_name/g;
		s/%%FirstName%% %%LastName%%/$BName/g;
		s/%%Address1%%/$BAddress/eg;
		s/%%City%%/$BCity/eg;
		s/%%State%%/$BState/eg;
		s/%%Zip%%/$BZip/eg;
		s/%%Country%%/&G_S::cbo_Country($q, 'x_ship_to_country', $field{'x_Country'})/e;
		print;
	}
	close TMPL;
	exit;
}
elsif ($field{'action'} eq 'payment')
{
	my ($err);

	# Make sure we have the required params.
	my @REQ = ('x_Amount', 'x_Invoice_Num', 'x_Cust_ID', 'payhow');
	foreach (@REQ){ unless ($q->param($_)){ $err .= "Missing $_<br />\n" }}
	if ($err)
	{
		$err .= qq|Please back up and try again. <a href="/contact.html">Contact us</a> if the problem persists.|;
		&Err($err);
		exit;
	}
	# Check the credit card required params.
	if ($field{'payhow'} eq 'CC')
	{
		unless ($field{'CARDNUM'} && $field{'EXPDATE'})
		{
			&Err('Please back up and provide a Credit Card number and select an Expiration date.');
			exit;
		}
		# Process the credit card charge.
		else { &Process_Order_CC }
	}
	# Check the ECheck required params.
	elsif ($field{'payhow'} eq 'ECHECK')
	{
		unless ($field{'BANKNAME'} && $field{'ABACODE'} && $field{'ACCTNO'})
		{
			&Err('There is eCheck information missing. We need Bank Name, Account # &amp; ABA Code.<br />Please backup and be sure all fields are supplied.');
			exit;
		}
		# Process the eCheck charge.
		else { &Process_Order_CC }
	}
	# Process the pre-payment charge.
	elsif ($field{'payhow'} eq 'PP') 
	{ 
		&Process_Order_PP;
	}
	else 	{ &Err('Please backup and select a payment method.'); exit; }
	
}
else { &Err('Undefined action'); exit; }

# If the payment processing was successful.
if ($success > 0)
{
	&GetDateTime;

	# Get the admin info.
	open(INFO,"setup.txt");
	while (<INFO>)
	{
 		$Line = $_;
 		chop($Line);
 		($MID,$Us,$Pa,$Comp,$CAddr,$Cit,$Sta,$Zp,$Tx,$Em,$Site,$Home,$Back,$PHead,$PFoot) = split(/\|/,$Line);
	}
	close(INFO);

	&CreateOrderString;	# Create the list of items for email and the sales.txt file.
	&EmailAdmin;		# Email the admin the order.
	&EmailMember;		# Email the member the order.
	&AppendOrder;		# Append the order to the sales.txt file.
#	As of 03/14/06, we are not doing affiliate order reporting.
#	If we decide to do so later, fix the match operator below. 'AF' can't be right.
#	if ( $OrderID =~ /AF/ ) { &AppendAffiliateOrder; }

	# Rename the old cart so it cannot be reused
	if (-e "$CartDir/$OrderID.c") 
	{ 
		my $oldcart = "$CartDir/$OrderID.f." . time();
		my $res = qx(mv $CartDir/$OrderID.c $oldcart);
	}
	$message = "Thank You For Your Order\!<br>Please Check Your Email For A Receipt";
}
else
{
###### Skip over the original, special error message stuff.
#	&GetSpecialErrorMessages;

	$message = qq|<h3>Authorization Problem</h3>
			Sorry, your payment could not be processed for the following reason:
			<br />
			<b>$auth_problem</b>
			|;
}

&printHead;
print "<br><hr width=\"400\" align=\"center\"><p align=\"center\"><font size=\"3\" face=\"arial\">$message</font></p><hr width=\"400\" align=\"center\">\n";
&printFoot;

exit;

#####################################
###### Begin Subroutines here. ######
#####################################

sub AppendOrder
{
	open(ORDERS,">>$OrderFile");
	print ORDERS "$OrderID\|$Email\|$BName\|$BAddress\|$BCity\|$BState\|$BZip\|$field{'x_ship_to_country'}\|$Phone\|$SName\|$SAddress\|$SCity\|$SState\|$SZip\|$field{'x_ship_to_country'}\|$PNPID\|$AccessItems\|";
	print ORDERS "$ShipHow\|$Shipping\|$Tax\|$Transactionamount\|$DateTime\|\n";
	close(ORDERS);
}

sub AppendAffiliateOrder
{
	$AffNumb    = "$OrderID";
	$NonComm    = ($Shipping + $Tax);
	$PreTotal   = ($Transactionamount - $NonComm);
	$SubTotal   = sprintf "%.2f",$PreTotal;
	$PreComm    = ($SubTotal * $Rate);
	$Commission = sprintf "%.2f",$PreComm;
	open (AFF_DATA, ">>$AffData");
	print AFF_DATA "$OrderID\|$SubTotal\|$Commission\|\#$InvID\|$DateTime\|\n";
	close (AFF_DATA);
}

sub CreateOrderString
{
	my $LineItem = '';
	my $count = 0;
	(@Ordered) = split(/\|\|/,$Orderstring);
	foreach $row (@Ordered)
 	{
 		($Item,$Name,$Price,$Qty,$Txable) = split(/\~/,$row);
 		$AdminItems .=  "$Qty - $Item, $Name \$$Price\n";
 		$MemberItems .=  "$Qty - $Name \$$Price\n";
 		$count = length("$Qty,,,$Name,,,\$$Price");
 		$AccessItems .= "$Qty,,,$Name,,,\$$Price";
		# Extend the length of the row for cleaner display - currently 60 characters.
		# Ultimately, we would put in a line feed, but it is unknown what 
		# characters can be put into a text file and imported into Access properly.
		while ( $count <= 60 ) { $AccessItems .= " "; $count++; }
		$AccessItems .= " ";		
 	}
}

sub EmailAdmin
{
	my $mail_msg = '';
	open (MAIL, '>',\$mail_msg) || die "failed to create mail FH\n";
	print MAIL "To: $Em\n"; 
#	print MAIL "Reply-to: $Email\n";
#	print MAIL "From: $Email\n";
	print MAIL "From: $Em\n";
	print MAIL "Subject: WebStore Order $OrderID - $CARTNAMES{$parent_dir}\n";

	print MAIL "Order Number: $InvID\n\n";
	print MAIL "DHS ID Number: ",$OrderID,"\n";

	# Pre-Pay account info is simple.
	if ( $field{'payhow'} eq "PP" )
	{ 
		print MAIL "Payment Type: DHS Club Pre-Payment Account\n";
		print MAIL "Bill To: $mem_info->{firstname} $mem_info->{lastname}\n";
	}
	# Other payment types require more info.
	else
	{
		if ( $field{'payhow'} eq "CC" ) 		{ print MAIL "Payment Type: Credit Card\n"; }
		elsif ( $field{'payhow'} eq "ECHECK" ) 	{ print MAIL "Payment Type: E-check\n"; }

		print MAIL "Bill To: $BName\n";
		print MAIL "Address: $BAddress\n";
		print MAIL "City: $BCity\n";
		print MAIL "State: $BState\n";
		print MAIL "Zip: $BZip\n";
		print MAIL "Country: $field{'x_Country'}\n";
	}
	print MAIL "\n";
	print MAIL "Ship To: $SName\n";
	print MAIL "Address: $SAddress\n";
	print MAIL "City: $SCity\n";
	print MAIL "State: $SState\n";
	print MAIL "Zip: $SZip\n";
	print MAIL "Country: $field{'x_ship_to_country'}\n";
	print MAIL "Phone: $Phone\n";
	print MAIL "\n";

	#print MAIL "AVS: $AVResponse\n";
	#print MAIL "\n";

	print MAIL $AdminItems;
	print MAIL "\n";

	print MAIL "Shipping: \t\$$Shipping\n";
	print MAIL "Tax: \t\t\$$Tax\n";
	print MAIL "\t\t--------\n";
	print MAIL "Total: \t\t\$$Transactionamount\n";
	print MAIL "\n";

	print MAIL "Ship Via: $ShipHow\n\n" if $ShipHow ne "";

	print MAIL "Order Number: $InvID\n";	
 	print MAIL "Affiliate Sales Number: $OrderID\n" if $AffNumb ne "";
	print MAIL "Order Date: $DateTime\n";
	print MAIL "DHS ID Number: ",$OrderID,"\n";
	print MAIL "\n";

	CartMail::mail_fh($mail_msg);
}

sub EmailMember
{
        my $mail_msg = '';
        open (MAIL, '>',\$mail_msg) || die "failed to create mail FH\n";
	print MAIL "To: $Email\n"; 
	print MAIL "Reply-to: $Em\n";
	print MAIL "From: $Em\n";
	print MAIL "Subject: Order From $Comp\n";
	print MAIL "The following information was received for your order:\n" ;
	print MAIL "\n" ;

	print MAIL "Order Number: $InvID\n";
	print MAIL "\n" ;

	if ($field{'payhow'} eq 'PP')
	{
		print MAIL "Bill To: DHS Club Pre-Payment Account\n";
		print MAIL "DHS ID Number: $OrderID\n";
		print MAIL "Name: $mem_info->{firstname} $mem_info->{lastname}\n";
	}
	else
	{
		print MAIL "Bill To: $BName\n";
		print MAIL "Address: $BAddress\n";
		print MAIL "City: $BCity\n";
		print MAIL "State: $BState\n";
		print MAIL "Zip: $BZip\n";
		print MAIL "Country: $field{'x_Country'}\n";
	}
	print MAIL "\n";

	print MAIL "Ship To: $SName\n";
	print MAIL "Address: $SAddress\n";
	print MAIL "City: $SCity\n";
	print MAIL "State: $SState\n";
	print MAIL "Zip: $SZip\n";
	print MAIL "Country: $field{'x_ship_to_country'}\n";
	print MAIL "\n";

	print MAIL $MemberItems;
	print MAIL "\n";
	print MAIL "Shipping: \t\$$Shipping\n";
	print MAIL "Tax: \t\t\$$Tax\n";
	print MAIL "\t\t--------\n";
	print MAIL "Total: \t\t\$$Transactionamount\n";
	print MAIL "\n";
	print MAIL "\n";
	print MAIL "Thank you for your order\!\n";
	print MAIL "\n";
	print MAIL "$Comp\n";
	print MAIL "$Home\n";
	print MAIL "$Em\n";
	print MAIL "\n\n";
        CartMail::mail_fh($mail_msg);

}

sub Err
{
	print $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print qq|<b>Error: $_[0]</b><br>|;
	print $q->end_html();
}

sub GetDateTime
{
 	@Week_Days = ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
 	@Months    = ('January','February','March','April','May','June','July','August','September','October','November','December');
 	($Second,$Minute,$Hour,$Month_Day,$Month,$Year,$Week_Day,$IsDST) = (localtime)[0,1,2,3,4,5,6,8];
 	if ($Second < 10)    {  $Second = "0$Second"; }
 	if ($Minute < 10)    {  $Minute = "0$Minute"; }
 	if ($Hour < 10)      {  $Hour = "0$Hour";     }
 	if ($Month_Day < 10) {  $Month_Day = "0$Month_Day"; }
 	$Year += 1900;
 	$DateTime = "$Months[$Month]\/$Month_Day\/$Year $Hour\:$Minute\:$Second";
}

sub GetFormInput
{
	foreach ($q->param())
	{
		$field{$_} = $q->param($_);
		print "$_ = $field{$_}<br />\n" if $printFields eq "YES";
	}
	return 1;
}

sub GetSpecialErrorMessages
{
	if($AVS eq "A") { $AVResponse = "Address Matches, Zip does not"; }
	if($AVS eq "B") { $AVResponse = "Address information not provided for AVS check"; }
	if($AVS eq "E") { $AVResponse = "AVS error"; }
	if($AVS eq "G") { $AVResponse = "Non U.S. Card Issuing Bank"; }
	if($AVS eq "N") { $AVResponse = "No Match on Address or Zip"; }
	if($AVS eq "P") { $AVResponse = "AVS not applicable for this test transaction"; }
	if($AVS eq "R") { $AVResponse = "System unavailable or timed out"; }
	if($AVS eq "S") { $AVResponse = "Service not supported by issuer"; }
	if($AVS eq "U") { $AVResponse = "Address information is unavailable"; }
	if($AVS eq "W") { $AVResponse = "Zip matches, Address does not"; }
	if($AVS eq "X") { $AVResponse = "Address and Zip Match"; }
	if($AVS eq "Y") { $AVResponse = "Address and Zip Match"; }
	if($AVS eq "Z") { $AVResponse = "Zip matches, Address does not"; }
	
	$message      = "" ;
	$found_err    = "" ;
	$error_number = 1;
	
	$errmsg = "$error_number\. Amount is not valid.<br>\n" ;
	if ($Transactionamount eq "")  { $message = $message.$errmsg ; $found_err = 1 ; $error_number++; }
	if ($Transactionamount =~ /D/) { $message = $message.$errmsg ; $found_err = 1 ; $error_number++; }
	$errmsg = "$error_number\. Card Declined.<br>\n" ;
	if ($Response eq "2")          { $message = $message.$errmsg ; $found_err = 1 ; $error_number++; }
	$errmsg = "$error_number\. Error Processing Card.<br>\n" ;
	if ($Response eq "3")          { $message = $message.$errmsg ; $found_err = 1 ; $error_number++; }
	if ($found_err) { &printError; }
}

sub printError
{
 	&printHead;
 	if($error_number ne "2") { $error_number--; $err = "$error_number Errors While Processing:" } else { $err = "Error While Processing:" }
 	print "<hr width=\"400\" align=\"center\"><p align=\"center\"><font size=\"2\" face=\"arial\" color=\"0080C0\"><strong>$err</strong></font></p><p align=\"center\">$message <br>$Reason</p>\n";
 	print "<p align=\"center\"><font size=\"2\" face=\"arial\" color=\"0080C0\"><strong>Use Your Back Button To Return To The Previous Page</strong></font></p><hr width=\"400\" align=\"center\">";
 	&printFoot;
 	exit 0;
 	return 1;
}

sub printHead
{
 	if (-e "$PHead") { open(HEAD,"$PHead"); while(<HEAD>) { print; } close(HEAD); }
}

sub printFoot
{
 	if (-e "$PFoot") { open(FOOT,"$PFoot"); while(<FOOT>) { print; } close(FOOT); }
}

###### Process a credit card charge (or ECheck).
sub Process_Order_CC
{
	###### form our string of vals to pass to the authorization module
	my %add = ( order_description => 'Shopping Cart Order');
	$add{ amount } = $field{'x_Amount'};
	$add{ invoice_number } = $field{'x_Invoice_Num'};
	$add{ id } = $field{'x_Cust_ID'};
	$add{ payment_method } = $field{'payhow'};
	$add{ emailaddress } = $field{'Email'} if $field{'Email'};
	$add{ ship_address } = $field{'x_ship_to_address'} || '';
	$add{ ship_city } = $field{'x_ship_to_city'} || '';
	$add{ ship_state } = $field{'x_ship_to_state'} || '';
	$add{ ship_zip } = $field{'x_ship_to_zip'} || '';
	$add{ ship_country } = $field{'x_ship_to_country'} if $field{'x_ship_to_country'};
	$add{ ship_firstname } = $field{'x_ship_to_first_name'} || '';
	$add{ ship_lastname } = $field{'x_ship_to_last_name'} || '';
	$add{ ship_company_name } = $field{'ship_company_name'} if $field{'ship_company_name'};
	$add{ phone } = $field{'phone'} if $field{'phone'};
	$add{ fax } = $field{'fax'} if $field{'fax'};
	$add{ account_holder_firstname } = $field{'x_First_Name'} || '';
	$add{ account_holder_lastname } = $field{'x_Last_Name'} || '';
	$add{ account_holder_address } = $field{'x_Address'} || '';
	$add{ account_holder_city } = $field{'x_City'} || '';
	$add{ account_holder_state } = $field{'x_State'} || '';
	$add{ account_holder_zip } = $field{'x_Zip'} || '';
	$add{ account_holder_country } = $field{'x_Country'} || '';
	$add{ bank_account_number } = $field{'ACCTNO'} if $field{'ACCTNO'};
	$add{ bank_name } = $field{'BANKNAME'} if $field{'BANKNAME'};
	$add{ aba_code } = $field{'ABACODE'} if $field{'ABACODE'};
	$add{ cc_number } = $field{'CARDNUM'} if $field{'CARDNUM'};
	$add{ cc_expiration } = $field{'EXPDATE'} if $field{'EXPDATE'};
	$add{ phone } = $field{'x_Phone'} || '';
       $add{ card_code } = $field{'card_code'} if $field{'card_code'};

	require '/home/httpd/cgi-lib/ccauth.lib';

	($success, $auth_problem) = &CCAuth::Authorize(\%add);
}

###### Process a Pre-Pay charge.
sub Process_Order_PP
{
	my $balance = '';
	$success = 0;
	use DB_Connect;
	if ($db = DB_Connect('generic'))
	{
		$success = $db->selectrow_array("	SELECT debit_trans_complete(?,?,?,?,NULL,?);",
								undef,
								( 	$member_id,
									$PP_TRANS_TYPE,
					 				$field{'x_Amount'}*-1,
									$PP_ORDER_DESC,
									$CARTNAMES{$parent_dir}
								) );
		unless ( $success > 0 )
		{
			# Get the balance, since that is likely the cause of the failure.
			if ( $balance = $db->selectrow_array("SELECT debit_balance($member_id);") )
			{
				$auth_problem = "DHS Pre-Pay available balance: US\$$balance";
			}
		}
	}
	# Otherwise, report the database connection failure.
	else
	{
		$auth_problem = "DHS Pre-Pay charge failed due to a database error.";
	}
}


###### 03/28/06 Modified $CartDir substitution. It was trying to sub for cart.cgi instead
######		of auth.cgi because I borrowed that code from the cart.cgi script and forgot
######		to change it. That caused open (*.c) carts to remain open after purchase.
###### 12/14/06 Changed method of mail sending to no longer use the sendmail command.
# 10/18/07 revised EmailAdmin to send from the same as the To house account
