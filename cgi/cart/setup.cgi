#!/usr/bin/perl
##########################################
# StoreClerk v.1.0                       #
# Copyright 2000,2001 JMS Online Systems #
# All Rights Resrved.                    #
##########################################
# Last Revision: 01-25-2001              #
##########################################
###### last modified 06/03/02	Bill MacArthur
use CGI::Carp qw(fatalsToBrowser);

$CCID = "CSCC17";
##### END SETUP #####
use File::Copy;
&GetFormInput;

if ($FORM{'Access'} eq "Login"){ &CheckLogin; }
elsif ($FORM{'CCID'} ne "$CCID"){ &CheckLogin; }
elsif ($FORM{'CCID'} eq ""){ &CheckLogin; }
else{ print "Content-type: text/html\n\n"; }

if ($FORM{'reset'} eq "1")        { &ResetDemo; exit; }
if ($FORM{'backup'} eq "1")       { &BackupDemo; exit; }

if ($FORM{'open'} eq "setup")     { $Title = "Store Setup Information"; &StoreSetup; exit; }
elsif ($FORM{'open'} eq "dept")   { $Title = "Department Names"; &Departments; exit; }
elsif ($FORM{'open'} eq "orders") { &ViewOrders; exit; }
elsif ($FORM{'open'} eq "folder") { &ViewItems; exit; }
if ($FORM{'UpdateDepts'} eq "Y")  { &UpdateDepts; exit; }
if ($FORM{'ChangeSetup'} ne "")   { &ChangeSetup; exit; }
if ($FORM{'CI'} eq "1")           { &ChangeItem; exit; }
if ($FORM{'EI'} eq "1")           { &EditItem; exit; }
if ($FORM{'NI'} eq "1")           { &NewItem; exit; }
if ($FORM{'AI'} eq "1")           { &AddItem; exit; }
if ($FORM{'DI'} eq "1")           { &DelItem; exit; }

&PrintMain;

exit;

sub ResetDemo
 {
 copy("data.bak","data.txt");
 copy("depts.bak","depts.txt");
 copy("sales.bak","sales.txt");
 copy("setup.bak","setup.txt");
 &PrintMain;
 }

sub BackupDemo
 {
 copy("data.txt","data.bak");
 copy("depts.txt","depts.bak");
 copy("sales.txt","sales.bak");
 copy("setup.txt","setup.bak");
 &PrintMain;
 }

sub CheckLogin
 {
 open(SDATA,"setup.txt");
 @LDATA = <SDATA>;
 close(SDATA);
 $Row = $LDATA[0];
 ($ID,$User,$Pass,$Company,$Address,$City,$State,$Zip,$Tax,$Email,$Site,$Home,$Back,$PageHead,$PageFoot) = split(/\|/,$Row);
 if ($FORM{'User'} eq "$User") { $Login = 1; }
 if ($FORM{'Pass'} eq "$Pass") { $Login++; }

	if ($Login ne "2") {
		print "Content-type: text/html\n\n";
		&Login;
	}
	else {
		print "Set-Cookie: Malias=dhsclub; path=/;\n";
		print "Content-type: text/html\n\n";
	}
}

sub Login
 {
 print "<html><head><title>Merchant Login</title></head>\n";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<form action=\"/cgi/cart/setup.cgi\" method=\"post\">";
 print "<div align=\"center\"><br><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"SellCart.com\" border=\"0\"></a></div>\n";
 print "<br><br><div align=\"center\">\n";
 print "<table border=\"0\" cellpadding=\"3\" cellspacing=\"3\">";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Username</strong></font>: <td align=\"left\"> <input name=\"User\" size=\"14\" maxlength=\"20\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Password</strong></font>: <td align=\"left\"> <input name=\"Pass\" size=\"14\" maxlength=\"20\"></td></tr>\n";
 print "<tr><td colspan=\"2\"><br><div align=\"center\">";
 print "<input type=\"hidden\" name=\"CCID\" value=\"T184023\">\n";
 print "<input type=\"submit\" name=\"Access\" value=\"Login\"></div>\n";
 print "</tr></table></div></form></body></html>\n\n";
 exit;
 }

sub PrintMain
 {
 print "<html><head><title>StoreClerk Store Manager</title></head>\n";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\">\n";
 print "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n";
 print "<tr><td colspan=\"5\"><img src=\"/storeclerk/images/toolbar.gif\" border=\"0\" width=\"580\" height=\"30\" usemap=\"\#toolbar\"></td></tr><tr><td>\n";
 print "<table background=\"\" border=\"0\" width=\"580\" cellpadding=\"0\" cellspacing=\"0\">\n";
 open(DEPTM,"depts.txt");
 @DEPM = <DEPTM>;
 close(DEPTM);
 $ColCount = 1;
 $FolderCount = 1;
 foreach $row (@DEPM)
  {
  chomp($row);
  ($DeptNumber,$DeptName)=split(/\|/,$row);
  $DNumber = DEPT . $DeptNumber;
  if ($ColCount eq "1") { print "<tr>"; }
  if ($DeptName ne "")
   {
   print "<td align=\"center\" width=\"116\" valign=\"top\"><a href=\"/cgi/cart/setup.cgi\?open=folder\&Dnumber=$DNumber\&CCID=$CCID\"><img src=\"/storeclerk/images/folder.gif\" border=\"0\" alt=\"\" width=\"100\" height=\"60\" alt=\"Department $DeptNumber\"><br><font face=\"arial\" size=\"1\" color=\"000000\"><strong>$DeptName</strong></font></a></td>\n";
   } else {
   print "<td align=\"center\" width=\"116\" valign=\"top\"><a href=\"/cgi/cart/setup.cgi\?open=dept\&CCID=$CCID\"><img src=\"/storeclerk/images/folder.gif\" border=\"0\" alt=\"\" width=\"100\" height=\"60\" alt=\"Department $DeptNumber\"><br></a></td>\n";
   }
  if ($ColCount eq "5") { print "</tr>"; $ColCount = 0; }
  $ColCount++;
  }
 print "<br><br>\n";
 print "<tr><td colspan=\"5\" align=\"center\"><br><a href=\"https://secure.authorize.net\"><font size=\"1\" face=\"arial\" color=\"000000\"><strong>Virtual Terminal</strong></font></a> - <a href=\"/cgi/cart/afmanage.cgi\"><font size=\"1\" face=\"arial\" color=\"000000\"><strong>Affiliate Manager</strong></font></a><br><br></td></tr>\n";
 print "</table></td></tr></table></div><br>\n";
 print "<map name=\"toolbar\">\n";
 print "<area shape=\"rect\" alt=\"Store Setup\" coords=\"0,0,144,28\" href=\"/cgi/cart/setup.cgi\?open=setup\&CCID=$CCID\" title=\"Store Setup\">\n";
 print "<area shape=\"rect\" alt=\"Departments\" coords=\"147,0,288,27\" href=\"/cgi/cart/setup.cgi\?open=dept\&CCID=$CCID\" title=\"Departments\">\n";
 print "<area shape=\"rect\" alt=\"View Orders\" coords=\"292,1,432,27\" href=\"/cgi/cart/setup.cgi\?open=orders\&CCID=$CCID\" title=\"View Orders\" target=\"_blank\">\n";
 print "<area shape=\"rect\" alt=\"Online Help\" coords=\"435,1,575,28\" href=\"http://www.storeclerk.net/help\" title=\"Online Help\" target=\"_blank\">\n";
 print "<area shape=\"default\" nohref></map>\n";
 print "<div align=\"center\"><font face=\"arial\" size=\"1\"><strong>Powered by:</strong></font><br><br><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/smallcc.jpg\" alt=\"StoreClerk.Net\" border=\"0\"></a></div>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</BODY></HTML>\n";
 }

sub ViewItems
 {
 print "<html><head><title>StoreClerk Shopping Cart Administration</title></head>\n";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\">\n";
 print "<table width=\"580\" cellpadding=\"2\" cellspacing=\"2\" border=\"0\"><tr><td align=\"center\" colspan=\"4\"><br><a href=\"/cgi/cart/setup.cgi\?NI=1\&Dnumber=$FORM{'Dnumber'}\&CCID=$CCID\"><font size=\"1\" face=\"arial\"><strong>ADD AN ITEM TO THIS FOLDER</strong></font></a> | <a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"1\" face=\"arial\"><strong>MAIN MENU</strong></font></a><br><br><hr></td></tr>";
 open(DATA,"data.txt");
 @DBASE = <DATA>;
 close(DATA);
 $BgCount = 1;
 foreach $row (@DBASE)
  {
  if ($BgCount eq "2") { $BG = "\#C0C0C0"; $BgCount = 0; } else { $BG = "\#E1E1E1"; }
  chomp($row);
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  $DNumber = DEPT . $Dept;
  if ($DNumber eq "$FORM{'Dnumber'}")
   {
   print "<tr bgcolor=\"$BG\"><td align=\"left\" valign=\"top\" width=\"80\"><font size=\"1\" face=\"arial\"><strong>$Item</strong></font></td><td width=\"300\"><a href=\"/cgi/cart/cart.cgi\?IT=$Item\" target=\"_blank\"><font size=\"1\" face=\"arial\"><strong>$Name</strong></font></a></td><td align=\"right\" width=\"70\"><font size=\"1\" face=\"arial\"><strong>$Price</strong></font></td>";
   print "<td align=\"center\" width=\"130\"><a href=\"/cgi/cart/setup.cgi\?EI=1\&IT=$Item\&CCID=$CCID\"><font size=\"1\" face=\"arial\"><strong>EDIT</strong></font></a> | ";
   print "<a href=\"/cgi/cart/setup.cgi\?DI=1\&IT=$Item\&Dnumber=$DNumber\&CCID=$CCID\"><font size=\"1\" face=\"arial\"><strong>DELETE</strong></font></a></td></tr>";
   $BgCount++;
   }
  }
 print "<tr><td colspan=\"4\" align=\"center\"><br><hr><br><a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"2\" face=\"arial\"><strong>Return To Main Menu</strong></font></a><br><br></td></tr>";
 print "</table></div><br>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</BODY></HTML>\n";
 exit;
}

sub AddItem
 {
 open(AITM,">>data.txt");
 print AITM "$FORM{'Dept'}\|$FORM{'IT'}\|$FORM{'Name'}\|$FORM{'Desc'}\|$FORM{'Opt1'}\|$FORM{'Opt2'}\|$FORM{'Opt3'}\|$FORM{'Weight'}\|$FORM{'Price'}\|$FORM{'Ship'}\|";
 print AITM "$FORM{'Url'}\|$FORM{'Image'}\|$FORM{'Tax'}\|$FORM{'Invt'}\|\n";
 close(AITM);
 &ViewItems;
}

sub ChangeItem
 {
 open(DAT,"data.txt");
 @DBASE = <DAT>;
 close(DAT);
 sleep(1);
 open(NEWD,">data.txt");
 foreach $row (@DBASE)
  {
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  if ($FORM{'OLDItem'} eq "$Item")
   {
   print NEWD "$Dept\|$FORM{'IT'}\|$FORM{'Name'}\|$FORM{'Desc'}\|$FORM{'Opt1'}\|$FORM{'Opt2'}\|$FORM{'Opt3'}\|$FORM{'Weight'}\|$FORM{'Price'}\|$FORM{'Ship'}\|$FORM{'Url'}\|$FORM{'Image'}\|$FORM{'Tax'}\|$FORM{'Invt'}\|\n";
   } else {  print NEWD "$row";  }
  }
close(NEWD);
&ViewItems;
}

sub DelItem
 {
 open(DAT,"data.txt");
 @DBASE = <DAT>;
 close(DAT);
 sleep(1);
 open(NEWD,">data.txt");
 foreach $row (@DBASE)
  {
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  if ($FORM{'IT'} eq "$Item") { $Gone = 1;  }  else {  print NEWD "$row";  }
  }
close(NEWD);
&ViewItems;
}

sub EditItem
 {
print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n|;
 print "<html><head><title>Edit Item $FORM{'IT'}</title></head>";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\"><form action=\"/cgi/cart/setup.cgi\" method=\"post\"><br>\n";
 print "<table width=\"580\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">\n";
 open(DATA,"data.txt");
 @ROWS = <DATA>;
 close(DATA);
 foreach $row (@ROWS)
  {
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  if ($FORM{'IT'} eq "$Item")
   {
   $Dnumber = DEPT . $Dept;
   if ($Image ne "")
    {
    if (-e "/home/httpd/html/storeclerk/images/$Image") { print "<tr><td colspan=\"2\" align=\"center\" bgcolor=\"FFFFFF\"><br><img src=\"/storeclerk/images/$Image\" border=\"0\" alt=\"$Item $Desc\"><br><br></td></tr>\n"; }
    }
   print "<tr><td align=\"right\" width=\"150\"><br><br><font face=\"arial\" size=\"1\"><strong>Item Number:</strong></font></td><td width=\"430\" align=\"left\"><br><br><input type=\"hidden\" name=\"OLDItem\" value=\"$Item\"><input name=\"IT\" value=\"$Item\" size=\"10\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Item Name:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Name\" value=\"$Name\" size=\"35\" maxlength=\"100\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Description:</strong></font></td><td width=\"430\" align=\"left\"><textarea name=\"Desc\" cols=\"50\" rows=\"5\" wrap=\"soft\">$Desc</textarea></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>URL:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Url\" value=\"$Url\" size=\"35\" maxlength=\"100\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Image:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Image\" value=\"$Image\" size=\"20\" maxlength=\"100\"> <a href=\"/storeclerk/upload.htm\" target=\"\_blank\"><font size=\"1\" face=\"arial\">UPLOAD IMAGE</font></a></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Price:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Price\" value=\"$Price\" size=\"6\" maxlength=\"8\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Shipping:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Ship\" value=\"$Ship\" size=\"6\" maxlength=\"8\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Weight:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Weight\" value=\"$Weight\" size=\"6\" maxlength=\"6\"><font face=\"arial\" size=\"1\"><strong> lbs. </strong></font></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>In Stock:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Invt\" value=\"$Invt\" size=\"6\" maxlength=\"8\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Taxable:</strong></font></td><td width=\"430\" align=\"left\">";
   if ($Tax eq "0")
    {
    print "<input type=\"radio\" name=\"Tax\" value=\"1\"><font face=\"arial\" size=\"1\"><strong> Yes </strong></font><input type=\"radio\" name=\"Tax\" value=\"0\" checked><font face=\"arial\" size=\"1\"><strong> No</strong></font></td></tr>\n";
    } else {
    print "<input type=\"radio\" name=\"Tax\" value=\"1\" checked><font face=\"arial\" size=\"1\"><strong> Yes </strong></font><input type=\"radio\" name=\"Tax\" value=\"0\"><font face=\"arial\" size=\"1\"><strong> No</strong></font></td></tr>\n";
    }
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Option 1:</strong></font></td>     <td width=\"430\" align=\"left\"><input name=\"Opt1\" value=\"$Opt1\" size=\"35\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>2:</strong></font></td>     <td width=\"430\" align=\"left\"><input name=\"Opt2\" value=\"$Opt2\" size=\"35\"></td></tr>\n";
   print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>3:</strong></font></td>     <td width=\"430\" align=\"left\"><input name=\"Opt3\" value=\"$Opt3\" size=\"35\"></td></tr>\n";
   print "<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"CCID\" value=\"$CCID\"><input type=\"hidden\" name=\"CI\" value=\"1\"><input type=\"hidden\" name=\"Dnumber\" value=\"$Dnumber\"><br><input type=\"submit\" name=\"Change\" value=\"Update Item\"><br><br><a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"2\" face=\"arial\"><strong>Return To Main Menu</strong></font></a><br><br></td></tr>\n";
   }
  }
 print "</table></div><br>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</form></BODY></HTML>\n";
exit;
}

sub NewItem
 {
 open(DEPTM,"depts.txt");
 @DEPM = <DEPTM>;
 close(DEPTM);
 $ColCount = 1;
 foreach $row (@DEPM)
  {
  chop($row);
  ($DeptNumber,$DeptName)=split(/\|/,$row);
  $DNumber = DEPT . $DeptNumber;
  if($DNumber eq "$FORM{'Dnumber'}") { $Dept = $DeptNumber; $IName = "$DeptName";}
  }
print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n|;
 print "<html><head><title>Add New Item</title></head>";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\"><form action=\"/cgi/cart/setup.cgi\" method=\"post\"><br>\n";
 print "<table width=\"580\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">\n";
 print "<tr><td align=\"center\" colspan=\"2\"><br><font face=\"arial\" size=\"2\"><strong>Add Item To $IName</strong></font><br></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><br><br><font face=\"arial\" size=\"1\"><strong>Item Number:</strong></font></td>  <td width=\"430\" align=\"left\"><br><br><input type=\"hidden\" name=\"Dept\" value=\"$Dept\"><input name=\"IT\" value=\"$Item\" size=\"10\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Item Name:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Name\" value=\"$Name\" size=\"35\" maxlength=\"100\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Description:</strong></font></td><td width=\"430\" align=\"left\"><textarea name=\"Desc\" cols=\"50\" rows=\"5\">$Desc</textarea></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>URL:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Url\" value=\"$Url\" size=\"35\" maxlength=\"100\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Image:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Image\" value=\"$Image\" size=\"20\" maxlength=\"100\"><a href=\"/storeclerk/upload.htm\" target=\"\_blank\"><font size=\"1\" face=\"arial\">UPLOAD IMAGE</font></a></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Price:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Price\" value=\"$Price\" size=\"6\" maxlength=\"8\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Shipping:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Ship\" value=\"$Ship\" size=\"6\" maxlength=\"8\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Weight:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Weight\" value=\"$Weight\" size=\"6\" maxlength=\"6\"><font face=\"arial\" size=\"1\"><strong> lbs. </strong></font></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>In Stock:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Invt\" value=\"$Invt\" size=\"6\" maxlength=\"8\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Taxable:</strong></font></td><td width=\"430\" align=\"left\"><input type=\"radio\" name=\"Tax\" value=\"1\" checked><font face=\"arial\" size=\"1\"><strong> Yes </strong></font><input type=\"radio\" name=\"Tax\" value=\"0\"><font face=\"arial\" size=\"1\"><strong> No</strong></font></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>Option 1:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Opt1\" value=\"$Opt1\" size=\"35\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>2:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Opt2\" value=\"$Opt2\" size=\"35\"></td></tr>\n";
 print "<tr><td align=\"right\" width=\"150\"><font face=\"arial\" size=\"1\"><strong>3:</strong></font></td><td width=\"430\" align=\"left\"><input name=\"Opt3\" value=\"$Opt3\" size=\"35\"></td></tr>\n";
 print "<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"CCID\" value=\"$CCID\"><input type=\"hidden\" name=\"AI\" value=\"1\"><input type=\"hidden\" name=\"Dnumber\" value=\"$FORM{'Dnumber'}\"><br><input type=\"submit\" name=\"Change\" value=\"Add This Item\"><br><br><a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"2\" face=\"arial\"><strong>Return To Main Menu</strong></font></a><br><br></td></tr>\n";
 print "</table></div><br>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</form></BODY></HTML>\n";
exit;
}

sub UpdateDepts
 {
 $DCount = 1;
 open(DEPS,">depts.txt");
 while ($DCount <= 30)
   {
   $DNumber = DEPT . $DCount;
   print DEPS "$DCount\|$FORM{$DNumber}\|\n";
   $DCount++;
   }
 close(DEPS);
 &PrintMain;
}

sub Departments
 {
print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n|;
 print "<html><head><title>Departments</title></head>\n";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<form action=\"/cgi/cart/setup.cgi\" method=\"post\">\n";
 print "<br><div align=\"center\">\n";
 print "<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\"><tr><td>";
 print "<table width=\"580\" cellpadding=\"1\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" colspan=\"4\"><br><font size=\"2\" face=\"arial\" color=\"000000\"><strong>$Title</strong></font><br><hr width=\"560\"></td></tr>\n";
 open(DEPTM,"depts.txt");
 @DEPM = <DEPTM>;
 close(DEPTM);
 $ColCount = 1;
 foreach $row (@DEPM)
  {
  chomp($row);
  ($DeptNumber,$DeptName)=split(/\|/,$row);
  $DNumber = DEPT . $DeptNumber;
  if ($ColCount eq "1") { print "<tr>"; }
  print "<td width=\"60\" align=\"center\">";
  if ($DeptName ne "")
   {
   print "<a href=\"/cgi/cart/cart.cgi\?DT\=$DeptNumber\" target=\"_blank\"><img src=\"/storeclerk/images/smfolder.gif\" alt=\"Department $DeptNumber\" border=\"0\" width=\"50\" height=\"30\"></a></td>\n";
   } else {
   print "<img src=\"/storeclerk/images/smfolder.gif\" alt=\"Department $DeptNumber\" border=\"0\" width=\"50\" height=\"30\"></td>\n";
   }
  print "<td align=\"left\"><input name=\"$DNumber\" size=\"25\" value=\"$DeptName\"></td>\n";
  if ($ColCount eq "2") { print "</tr>"; $ColCount = 0; }
  $ColCount++;
  }
 if ($ColCount ne "1") { print "<td></td></tr>"; }
 print "<tr><td colspan=\"4\" align=\"center\"><br><input type=\"hidden\" name=\"CCID\" value=\"$CCID\"><input type=\"hidden\" name=\"UpdateDepts\" value=\"Y\"><input type=\"submit\" name=\"Send\" value=\"Submit Changes\"><br><br><a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"2\" face=\"arial\"><strong>Return To Main Menu</strong></font></a><br><br></td></tr>\n";
 print "</table></td></tr></table></div>";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</form></body></html>\n\n";
 exit;
 }

sub StoreSetup
 {
 open(INFO,"setup.txt");
 while (<INFO>)
   {
   $Line = $_;
   chop($Line);
   ($ID,$User,$Pass,$Company,$Address,$City,$State,$Zip,$Tax,$Email,$Site,$Home,$Back,$PageHead,$PageFoot) = split(/\|/,$Line);
   }
 close(INFO);
print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n|;
 print "<html><head><title>Store Setup</title></head>";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\"><form action=\"/cgi/cart/setup.cgi\" method=\"post\"><br>\n";
 print "<table width=\"580\" cellpadding=\"1\" cellspacing=\"1\" border=\"1\"><tr><td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"arial\"><u>$Title</u></font><br><br></td></tr>\n";
 print "<tr><td align=\"right\" width=\"200\"><font face=\"arial\" size=\"1\"><strong>Merchant ID\:</strong></td> <td align=\"left\" width=\"380\"><input name=\"ID\" size=\"15\" value=\"$ID\"><\/td><\/tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Username:</strong><font></td><td align=\"left\"><input name=\"User\" size=\"15\" value=\"$User\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Password:</strong><font></td><td align=\"left\"><input name=\"Pass\" size=\"15\" value=\"$Pass\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Company:</strong><font></td><td align=\"left\"><input name=\"Company\" size=\"30\" value=\"$Company\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Address:</strong><font></td><td align=\"left\"><input name=\"Address\" size=\"30\" value=\"$Address\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>City:</strong><font></td><td align=\"left\"><input name=\"City\" size=\"30\" value=\"$City\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>State:</strong><font></td><td align=\"left\"><input name=\"State\" size=\"2\" value=\"$State\" maxlength=\"2\"> <font face=\"arial\" size=\"1\"><strong>Zip Code:</strong></font> <input name=\"Zip\" size=\"6\" value=\"$Zip\" maxlength=\"6\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Sales Tax:</strong><font></td><td align=\"left\"><input name=\"Tax\" size=\"4\" value=\"$Tax\" maxlength=\"5\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Email:</strong><font></td><td align=\"left\"><input name=\"Email\" size=\"30\" value=\"$Email\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Site Name:</strong><font></td><td align=\"left\"><input name=\"Site\" size=\"30\" value=\"$Site\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Home URL:</strong><font></td><td align=\"left\"><input name=\"Home\" size=\"30\" value=\"$Home\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Continue URL:</strong><font></td><td align=\"left\"><input name=\"Back\" size=\"30\" value=\"$Back\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Page Header:</strong><font></td><td align=\"left\"><input name=\"PageHead\" size=\"30\" value=\"$PageHead\"></td></tr>\n";
 print "<tr><td align=\"right\"><font face=\"arial\" size=\"1\"><strong>Page Footer:</strong><font></td><td align=\"left\"><input name=\"PageFoot\" size=\"30\" value=\"$PageFoot\"></td></tr>\n";
 print "<tr><td align=\"center\" colspan=\"2\"><br><input type=\"hidden\" name=\"CCID\" value=\"$CCID\"><input type=\"submit\" name=\"ChangeSetup\" value=\"Apply Store Setup Information\"><br><br><a href=\"/cgi/cart/setup.cgi\?CCID=$CCID\"><font size=\"2\" face=\"arial\"><strong>Return To Main Menu</strong></font></a><br><br></td></tr>\n";
 print "</table></div></form><br><br>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font></p>\n";
 print "</body></html>";
 exit;
 }

sub ChangeSetup
 {
 if (-e "setup.txt")
  {
  open(INFO,">setup.txt");
  print INFO "$FORM{'ID'}\|$FORM{'User'}\|$FORM{'Pass'}\|$FORM{'Company'}\|$FORM{'Address'}\|$FORM{'City'}\|$FORM{'State'}\|$FORM{'Zip'}\|$FORM{'Tax'}\|$FORM{'Email'}\|$FORM{'Site'}\|$FORM{'Home'}\|$FORM{'Back'}\|$FORM{'PageHead'}\|$FORM{'PageFoot'}\|\n";
  close(INFO);
  } else {  &SetupError('Error locating setup.txt');  exit;  }
 &PrintMain;
}

sub ViewOrders
 {
print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n|;
 print "<html><head><title>View Orders</title></head>";
 print "<BODY bgcolor=\"\#FFFFFF\" text=\"\#000000\" link=\"\#000000\" vlink=\"\#000000\" alink=\"\#000000\">\n";
 print "<div align=\"center\">\n";
 print "<table width=\"580\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" colspan=\"3\"><br><font size=\"3\" face=\"arial\">Online Sales Orders</font><br></td></tr>";
 open(SALES,"sales.txt");
 @ORDERS = <SALES>;
 close(SALES);
 foreach $order (@ORDERS)
  {
  chomp($order);
  ($OrderNumber,$Email,$BName,$BAddr,$BCity,$BState,$BZip,$BCountry,$BPhone,$SName,$SAddr,$SCity,$SState,$SZip,$SCountry,$APNumber,$Items,$ShipBy,$Shipping,$Tax,$Total,$Date)=split(/\|/,$order);
  print "<tr><td align=\"left\" valign=\"top\" width=\"190\"><font size=\"2\" face=\"arial\">Order Number:</font> <b><tt>$OrderNumber</tt></b><br><p align=\"left\"><font size=\"2\" face=\"arial\">";
  print "Sold To:</font><br><a href=\"mailto:$Email\"><font size=\"2\" face=\"arial\">$BName</font></a><br><font size=\"2\" face=\"arial\">";
  print "$BAddr<br>$BCity<br>$BState $BZip $BCountry<br>$BPhone</font></td><td align=\"left\" valign=\"top\" width=\"190\"><font size=\"2\" face=\"arial\">Time:</font> <b><tt>$Date</tt></b><p align=\"left\"><font size=\"2\" face=\"arial\">";
  print "Ship To:</font><br><font size=\"2\" face=\"arial\">$SName<br>";
  print "$SAddr<br>$SCity<br>$SState $SZip $SCountry<br>Ship: $ShipBy</font><br><br></td><td align=\"left\" valign=\"top\" width=\"200\"><font size=\"2\" face=\"arial\">Process ID:</font> <b><tt>$APNumber</tt></b><p align=\"left\"><font size=\"2\" face=\"arial\">";
  print "$Items <br>Sales Tax: $Tax <br>Shipping: $Shipping <br>Total: $Total </font></td></tr>\n";
  print "<tr><td colspan=\"3\"><hr></td></tr>";
  }
 print "</table></div><p align=\"center\"><font size=\"2\" face=\"arial\">Close This Window When Done</font></p>";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">- All Rights Reserved.</font>\n";
 print "</body></html>";
 exit;
 }

sub GetFormInput
 {
 (*fval) = @_ if @_ ;
 local ($buf);
 if ($ENV{'REQUEST_METHOD'} eq 'POST') { read(STDIN,$buf,$ENV{'CONTENT_LENGTH'}); }
 else {	$buf=$ENV{'QUERY_STRING'}; }
 if ($buf eq "") { return 0 ; }
 else {
 @fval=split(/&/,$buf);
 foreach $i (0 .. $#fval){
 ($name,$val)=split (/=/,$fval[$i],2);
 $val=~tr/+/ /;
 $val=~ s/%(..)/pack("c",hex($1))/ge;
 
###### we'll get rid of control character and other newline type characters since they break the data file
$val =~ s/\n|\r|\t|\c[m//g;

###### get rid of leading & trailing spaces
$val =~ s/^\s*|\s*$//g;

$name=~tr/+/ /;
 $name=~ s/%(..)/pack("c",hex($1))/ge;
 if (!defined($FORM{$name})) {	$FORM{$name}=$val; }
 else {	$FORM{$name} .= ",$val"; }
   }
 }
return 1;
}

sub SetupError
 {
 print "<p> $_ </p>";
 exit;
}

###### 06/03/02 pushed the login checks ahead of any other functions since as it was written, the whole thing
###### could have been reset or backed up without being logged in



