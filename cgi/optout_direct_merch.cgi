#!/usr/bin/perl
# last modified: 01/01/16	Bill MacArthur

use strict;
use warnings;
use lib "/home/httpd/cgi-lib";
use CGI;
use Data::Serializer;
use Data::Dumper;

my $obj = Data::Serializer->new();

$obj = Data::Serializer->new(
                         'serializer' => 'Storable',
                         'digester'   => 'MD5',
                         'cipher'     => 'DES',
                         'secret'     => 'PoHsBuLc1955',
                         'compress'   => 1,
		       );

my $cgi = CGI->new();

my $serialized = 0;

if ($cgi->param('mystuff')) {
    $serialized = "^Storable|DES|MD5|hex|Compress::Zlib^" . $cgi->param('mystuff'); 
}

my $params = $obj->deserialize($serialized);

warn Dumper($params);

if (! exists $params->{'username'}) {
    print
          $cgi->header('-expires'=>'now'),
          $cgi->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error');
    print "<br />The Information in you link to this process is corrupted<br />";
    print $cgi->end_html();
    exit;
}

my $url="https://www.clubshop.com/cgi/LoginMerchants.cgi?_action=login&username=$params->{'username'}&password=$params->{'password'}&merchantid=$params->{'merchantid'}&masterbranch=maf&destination=/maf/cancel.shtml";
print $cgi->redirect($url);
exit;

__END__

01/01/16	the functional change was the login string