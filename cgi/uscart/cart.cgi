#!/usr/bin/perl -w
# StoreClerk v.1.0
# Authorize.net and Pre-Payment account Version
#
# Major revision 03-15-2006 to allow for DHS Pre-Payment account option.
#
#############################################################################
# StoreClerk By JMS Online Systems                                          #
# (c) 2000,2001 All rights reserved.                                        #
# www.jmsonline.net Custom ecommerce programming is our specialty!          #
#############################################################################
#
# Last Modified Date: 03/15/06	Karl Kohrt

use CGI::Carp qw(fatalsToBrowser);
use CGI;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;

our $TestMode   = "NO";
our $q = new CGI;
our $script_name = $q->script_name;
(our $auth_gateway = $script_name) =~ s/cart\.cgi$/auth\.cgi/;
(our $ship_script = $script_name) =~ s/cart\.cgi$/ship\.pl/;
$ship_script = '/home/httpd' . $ship_script;
require "$ship_script";
(our $CartDir = $script_name) =~ s/cart\.cgi$/carts/;
$CartDir = '/home/httpd' . $CartDir;
our $WebData = 'setup.txt';
our $mail_prog = '/usr/sbin/sendmail';
our $DataBase = 'data.txt';
our $DeptBase = 'depts.txt';

our ($MID,$Us,$Pa,$Comp,$CAddr,$Cit,$Sta,$Zp,$Tax,$Em,$Site,$Home,$Back,$PHead,$PFoot) = '';
open(CONFIG, $WebData) || die "Couldn't open $WebData\n";
while (<CONFIG>)
{
 	$Line = $_;
 	chop($Line);
 	($MID,$Us,$Pa,$Comp,$CAddr,$Cit,$Sta,$Zp,$Tax,$Em,$Site,$Home,$Back,$PHead,$PFoot) = split(/\|/,$Line);
}
close(CONFIG);

# this action must be 'get' as of 06/03/02, 'post' breaks it
my $ME_FORM = qq|<form action="$Back" method="get">|;

######### End Setup Variables - Do Not Edit Anything Below This Line #########
&ParseData;

###### Substitute the parameters encoded in the path if we have a path from from a login return.
if ($q->path_info())
{
	my $path = $q->path_info();
	$path =~ s#/##;
	my @vals = split(/,/, $path);
	for (@vals)
	{
		my ($key, $val) = split(/=/, $_);
		$FORM{$key} = $val;
	}
}

my ($id, $meminfo) = G_S::authen_ses_key($q->cookie('AuthCustom_Generic'));
my $CustID = $meminfo->{alias} || '';

print $q->header('-expires'=>'now','-charset'=>'utf-8');

# Re-Post the items and the shipping info page.
if ($FORM{'RD'} && $FORM{'RD'} eq "UPDATE") { &RePost; &printShipTo; }
elsif ($FORM{'CK'} && $FORM{'CK'} eq "1")	{ &printOrder; }	# Confirm shipping info, proceed to checkout.
elsif ($FORM{'DT'} && $FORM{'DT'} ne "")	{ &printDept; }
elsif ($FORM{'IT'} && $FORM{'IT'} ne "")	{ &printItem; }
elsif ($FORM{'LK'} && $FORM{'LK'} ne "")	{ &printMenu; }
elsif ($FORM{'OR'} && $FORM{'OR'} eq "1")  			# Show items, enter shipping info and choose payment method.
{ 
	&PostItemOrder; 
	&printShipTo; 
}
elsif ($FORM{'CL'} && $FORM{'CL'} eq "1")   { &ResetCart; }	# Empty the member's cart.
elsif ($FORM{'KeyWord'} && $FORM{'KeyWord'} ne ""){ &printSearch; }
else { &printMenu; }							# Default is to print the menu.

exit;

####################################
###### Subroutines Begin Here ######
####################################

sub cbo_State
{
	my $name = shift;
	my $default_value = shift || '';
	my $country = $FORM{'Country'} || 'US';
	my ( $tmp1,$tmp2,$tmp3,$tmp4,$StateList,$StateHash) = '';
	if ($db = DB_Connect('generic'))
	{
		(	$tmp1,
			$tmp2,
			$tmp3,
			$tmp4,
			$StateList,
			$StateHash
			) = G_S::Standard_State($db,
							'',
							$default_value,
							$country
							);
	}
	return $q->popup_menu(	-name=>$name,
					-values=>$StateList,
					-labels=>$StateHash,
					-default=>$default_value,
					-force=>1);
}

sub NoMatch
{
 	print "<hr width=\"500\" align=\"center\"><p align=\"center\"><font face=\"arial, Helvetica, sans-serif\">\n";
 	print "Sorry, no match was found for <b>$FORM{'KeyWord'}</b><br> Please try another keyword, part of a keyword or item number.</font></p><hr width=\"500\" align=\"center\"><br><br><br><br>";
 	&printFoot;
 	exit;
}

sub ParseData
{
	if ($ENV{'REQUEST_METHOD'} eq "GET")     {  $buffer = $ENV{'QUERY_STRING'}; }
	elsif ($ENV{'REQUEST_METHOD'} eq "POST") {  read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'}); }
	@pairs = split(/&/, $buffer);
	foreach $pair (@pairs)
	{
 		($name, $value) = split(/=/, $pair);
 		$value =~ tr/+/ /;
 		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
 		$value =~ s/~!/ ~!/g;
 		$FORM{$name} = $value;
 	}
}

sub PostItemOrder
{
	&printHead;
	open(DATA,$DataBase) || die "Couldn't open $DataBase\n";
	@ROWS = <DATA>;
	close(DATA);
	if(-e "$CartDir/$CustID.c") 
	{
		open(CART, ">>$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n";
	} 
	else { open(CART, ">$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n"; }

	foreach $row (@ROWS)
 	{
 		($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
 		$ItemQuan = $Item . Q;
 		$Quantity  = $FORM{$ItemQuan};
 		if ($FORM{'IO'} && $FORM{'IO'} eq "$Item")
  		{
  			print CART "$Item\|$Name $FORM{'Opt1'} $FORM{'Opt2'} $FORM{'Opt3'}\|$Desc\|$Price\|$Ship\|$Weight\|$Quantity\|\n";
  		}
 	}
	close(CART);
}

sub printDept
{
 &printHead;
 print "<div align=\"center\"><table border=\"0\" width=\"600\" cellpadding=\"3\" cellspacing=\"3\" align=\"center\">\n";
 print "<tr><td class=\"more_info\" valign=\"top\" nowrap>$ME_FORM Browse: <select name=\"DT\">\n";

 open(DEPT,$DeptBase) || die "Couldn't open $DeptBase\n";
 @LINES = <DEPT>;
 close(DEPT);
 foreach $line (@LINES)
  {
  ($Dept,$Name) = split(/\|/,$line);
  if ($Name ne "") { print "<option value=\"$Dept\">$Name</option>\n"; }
  }
 print "</select>\n<input type=\"image\" name=\"SearchDept\" src=\"/storeclerk/images/go.gif\" alt=\"Go\" border=\"0\" height=\"21\" width=\"50\"></div></form></td>\n";
 print "<td class=\"more_info\" valign=\"top\" >$ME_FORM Search: <input class=\"more_info\" name=\"KeyWord\" size=\"12\" maxlength=\"25\"> <input type=\"image\" name=\"SearchKey\" src=\"/storeclerk/images/go.gif\" border=\"0\" height=\"21\" width=\"50\"></div></form></td></tr>\n";
 print "<tr><td colspan=\"2\"><hr></td></tr>";
 print "</table></div><div align=\"center\"><table border=\"0\" width=\"700\" cellpadding=\"3\" cellspacing=\"3\" align=\"center\">\n";

 open(DATA,$DataBase) || die "Couldn't open $DataBase\n";
 @ROWS = <DATA>;
 close(DATA);
 foreach $row (@ROWS)
  {
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  $ItemQuan = $Item . Q;
  if ($FORM{'DT'} && $FORM{'DT'} eq "$Dept")
   {
   print "<tr><td valign=\"top\"><span class=\"name\">$Name</span>\n";
   print "<p class=\"desc\">$Desc</p>\n";
   if ($Url ne "") { print "<a href=\"$Url\" target=\"_blank\" class=\"more_info\">more info</a>\n"; }
   print "$ME_FORM";
   if ($Opt1 ne "")
    {
    $OptCount = 1;
    @OptionOne   = split(/\,/,$Opt1);
    $OptionTitle = $OptionOne[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt1\">";
    while ($OptionOne[$OptCount] ne "") {  print "<option>$OptionOne[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   if ($Opt2 ne "")
    {
    $OptCount = 1;
    @OptionTwo   = split(/\,/,$Opt2);
    $OptionTitle = $OptionTwo[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt2\">";
    while ($OptionTwo[$OptCount] ne "") {  print "<option>$OptionTwo[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   if ($Opt3 ne "")
    {
    $OptCount = 1;
    @OptionThr   = split(/\,/,$Opt3);
    $OptionTitle = $OptionThr[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt3\">";
    while ($OptionThr[$OptCount] ne "") {  print "<option>$OptionThr[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   print "<p class=\"price\">";
   if ($Invt ne "") { print "In Stock: $Invt <br>"; }
   print "Price: \$$Price\n";
   print "<input type=\"hidden\" name=\"IO\" value=\"$Item\">\n";
   print "<input type=\"hidden\" name=\"$ItemQuan\" value=\"1\">\n";
   print "<input type=\"hidden\" name=\"OR\" value=\"1\">\n";
   print "<input type=\"image\" name=\"OrderNow\" src=\"/storeclerk/images/order.gif\" alt=\"Order Now\" border=\"0\" width=\"101\" height=\"29\" align=\"absmiddle\"></p></form></td>";
   if ($Image ne "")
    {
    if (-e "/home/httpd/html/storeclerk/images/$Image")
     {
     print "<td align=\"center\"><img src=\"/storeclerk/images/$Image\" border=\"0\" alt=\"$Name\"></td></tr>\n";
     } else {
     print "<td>\&nbsp\;</td></tr>\n";
     }
    }
    print "<tr><td colspan=\"2\"><hr></td></tr>";
   }
  }
 print "</table></div><br>\n";
 &printFoot;
}

sub printError
{
	print "<br><hr width=\"400\" align=\"center\">
		<p align=\"center\">
		$ErrorMessage
		</p><hr width=\"400\" align=\"center\">";
	&printFoot;
}

sub printFoot
{
	if (-e "$PFoot") 
	{
		open(FOOT,"$PFoot") || die "Couldn't open $PFoot\n";	
		while(<FOOT>) { print; }
		close(FOOT);
	} 
	else { print "Cannot find page footer"; }
}

sub printHead
{
	if (-e "$PHead") 
	{
		open(HEAD,"$PHead") || die "Couldn't open $PHead\n";
		while(<HEAD>) { print; }
		close(HEAD);
	} else { print "Cannot find page header"; }
}

sub printItem
{
 &printHead;
 print "<div align=\"center\"><table border=\"0\" width=\"600\" cellpadding=\"3\" cellspacing=\"3\" align=\"center\">\n";
 print "<tr><td align=\"right\" valign=\"top\" width=\"300\">$ME_FORM<div align=\"right\"><font size=\"1\" face=\"arial\" color=\"000000\"><strong>Browse: </strong></font><select name=\"DT\">\n";
 open(DEPT,$DeptBase) || die "Couldn't open $DeptBase\n";
 @LINES = <DEPT>;
 close(DEPT);
 foreach $line (@LINES)
  {
  ($Dept,$Name) = split(/\|/,$line);
  if ($Name ne "") { print "<option value=\"$Dept\">$Name</option>"; }
  }
 print "</select> <input type=\"image\" name=\"SearchDept\" src=\"/storeclerk/images/go.gif\" alt=\"Go\" border=\"0\"></div></form></td>";
 print "<td align=\"left\" valign=\"top\" width=\"300\">$ME_FORM<div align=\"left\"><font size=\"1\" face=\"arial\" color=\"000000\"><strong>Search: </strong></font><input name=\"KeyWord\" size=\"12\" maxlength=\"25\"> <input type=\"image\" name=\"SearchKey\" src=\"/storeclerk/images/go.gif\" border=\"0\"></div></form></td></tr>\n";
 print "<tr><td colspan=\"2\"><hr></td></tr>";
 print "</table></div><div align=\"center\"><table border=\"0\" width=\"700\" cellpadding=\"3\" cellspacing=\"3\" align=\"center\">\n";
 open(DATA,$DataBase) || die "Couldn't open $DataBase\n";
 @ROWS = <DATA>;
 close(DATA);
 foreach $row (@ROWS)
  {
  ($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  $ItemQuan = $Item . Q;
  if ($FORM{'IT'} && $FORM{'IT'} eq "$Item")
   {
   print "<tr><td valign=\"top\"><span class=\"name\">$Name</span>\n";
   print "<p class=\"desc\">$Desc</p>\n";
   if ($Url ne "") { print "<a href=\"$Url\" target=\"_blank\" class=\"more_info\">more info</a>\n"; }
   print "</p>$ME_FORM";
   if ($Opt1 ne "")
    {
    $OptCount = 1;
    @OptionOne   = split(/\,/,$Opt1);
    $OptionTitle = $OptionOne[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt1\">";
    while ($OptionOne[$OptCount] ne "") {  print "<option>$OptionOne[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   if ($Opt2 ne "")
    {
    $OptCount = 1;
    @OptionTwo   = split(/\,/,$Opt2);
    $OptionTitle = $OptionTwo[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt2\">";
    while ($OptionTwo[$OptCount] ne "") {  print "<option>$OptionTwo[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   if ($Opt3 ne "")
    {
    $OptCount = 1;
    @OptionThr   = split(/\,/,$Opt3);
    $OptionTitle = $OptionThr[0];
    print "<font face=\"arial\" size=\"1\"><strong>$OptionTitle\:</strong></font> <select name=\"Opt3\">";
    while ($OptionThr[$OptCount] ne "") {  print "<option>$OptionThr[$OptCount]</option>"; $OptCount++; }
    print "</select>\n";
    }
   print "<p class=\"price\">";
   if ($Invt ne "") { print "In Stock: $Invt <br>"; }
   print "Price: \$$Price\n";
   print "<input type=\"hidden\" name=\"IO\" value=\"$Item\">\n";
   print "<input type=\"hidden\" name=\"Name\" value=\"$Name\">\n";
   print "<input type=\"hidden\" name=\"$ItemQuan\" value=\"1\">\n";
   print "<input type=\"hidden\" name=\"OR\" value=\"1\">\n";
   print "<input type=\"image\" name=\"OrderNow\" src=\"/storeclerk/images/order.gif\" alt=\"Order Now\" border=\"0\" width=\"101\" height=\"29\" align=\"absmiddle\"></p></form></td>";
   if ($Image ne "")
    {
    if (-e "/home/httpd/html/storeclerk/images/$Image")
     {
     print "<td align=\"center\"><img src=\"/storeclerk/images/$Image\" border=\"0\" alt=\"$Name\"></td></tr>\n";
     } else {
     print "<td>\&nbsp\;</td></tr>\n";
     }
    }
    print "<tr><td colspan=\"2\"><hr></td></tr>";
   }
  }
 print "</table></div><br>\n";
 &printFoot;
}

sub printMenu
{
	&printHead;
 	print "<div align=\"center\"><table border=\"0\" width=\"600\" cellpadding=\"3\" cellspacing=\"3\">\n";
 	print "<tr><td class=\"more_info\" valign=\"top\" nowrap>$ME_FORM Browse: <select name=\"DT\">\n";
 	open(DEPT,$DeptBase) || die "Couldn't open $DeptBase\n";
 	@LINES = <DEPT>;
 	close(DEPT);
 	foreach $line (@LINES)
  	{
  		($Dept,$Name) = split(/\|/,$line);
  		if ($Name ne "") { print "<option value=\"$Dept\">$Name</option>\n"; }
  	}
 	print "</select>\n<input type=\"image\" name=\"SearchDept\" src=\"/storeclerk/images/go.gif\" alt=\"Go\" border=\"0\" height=\"21\" width=\"50\"></div></form></td>";
 	print "<td class=\"more_info\" valign=\"top\" nowrap>$ME_FORM Search: <input class=\"more_info\" name=\"KeyWord\" size=\"12\" maxlength=\"25\"> <input type=\"image\" name=\"SearchKey\" src=\"/storeclerk/images/go.gif\" border=\"0\" height=\"21\" width=\"50\"></div></form></td></tr>\n";
	print "</table></div><br>";
	&printFoot;
}

###### The form where you confirm shipping info and proceed to checkout.
sub printOrder
{
	&printHead;
	$Blank = 0;
	my $Tweight = 0;
	my $SShip = 0;
	$Hide = "";

	if ($FORM{'FName'} eq "")   { $Blank = "1"; }
	if ($FORM{'LName'} eq "")   { $Blank = "1"; }
	if ($FORM{'Address'} eq "") { $Blank = "1"; }
	if ($FORM{'City'} eq "")    { $Blank = "1"; }
	if ($FORM{'State'} eq "" && 
    		$FORM{'Province'} eq "") { $Blank = "1"; }
	if ($FORM{'x_ship_to_zip'} eq "")     { $Blank = "1"; }
	if ($FORM{'Email'} eq "")   { $Blank = "1"; }
	if ($FORM{'Phone'} eq "")   { $Blank = "1"; }
	if ($Blank eq "1") 
	{ 
		$ErrorMessage = "Please use your back button and enter all shipping information.";
		&printError; 
		exit;
	}

	my $STATE = $FORM{'State'} || $FORM{'Province'};
	if($TestMode eq "YES")
  	{
  		$Hide .= "<input type=\"hidden\" name=\"x_test_request\" value=\"TRUE\">\n";
  	}
 	$InvID = time();
 	$Hide .= "<input type=\"hidden\" name=\"x_Version\"              value=\"3.0\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Receipt_Link_Method\"  value=\"POST\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Login\"                value=\"$MID\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Cust_ID\"              value=\"$CustID\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Invoice_Num\"          value=\"$InvID\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_merchant_email\"       value=\"$Em\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_email_customer\"       value=\"TRUE\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_email_merchant\"       value=\"TRUE\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_first_name\"   value=\"$FORM{'FName'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_last_name\"    value=\"$FORM{'LName'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_address\"      value=\"$FORM{'Address'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_city\" value=\"$FORM{'City'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_state\" value=\"$STATE\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_zip\"          value=\"$FORM{'x_ship_to_zip'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_ship_to_country\"      value=\"$FORM{'x_ship_to_country'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Email\"                value=\"$FORM{'Email'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"x_Phone\"                value=\"$FORM{'Phone'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"Email\"                  value=\"$FORM{'Email'}\">\n";
 	$Hide .= "<input type=\"hidden\" name=\"Phone\"                  value=\"$FORM{'Phone'}\">\n";

	# Establish the authorization script and any special parameters.
	print "<form action=\"$auth_gateway\" method=\"post\">\n";
	if ($FORM{'PayHow'} && $FORM{'PayHow'} eq "PrePay")
	{
 		$Hide .= "<input type=\"hidden\" name=\"payhow\" value=\"PP\">\n";
 		$Hide .= "<input type=\"hidden\" name=\"action\" value=\"payment\">\n";
	}
	# The credit card payment method is the default.
	else
	{
 		$Hide .= "<input type=\"hidden\" name=\"payhow\" value=\"CC\">\n";
	}

	print "<hr align=\"center\" width=\"600\">
	<br>
	<div align=\"center\">
	<table border=\"0\" bgcolor=\"FFFFFF\" cellspacing=\"2\" cellpadding=\"3\" width=\"600\">
	<tr><td align=\"center\" width=\"300\">
	<font size=\"2\" face=\"arial, Helvetica, sans-serif\" color=\"#2626FF\"><strong><u>
	Shipping Information
	</u></strong></font>
	</td>
	<td width=\"300\" align=\"center\">
	<font size=\"2\" face=\"arial, Helvetica, sans-serif\" color=\"#2626FF\"><strong><u>
	Payment Information
	</u></strong></font>
	</td></tr>
	<tr><td valign=\"top\">

	<table width=\"300\"cellspacing=\"0\" cellpadding=\"4\">
	<tr><td><p>
	<font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>
	Ship From:<br>
	$Comp<br>
	$CAddr<br>
	$Cit\, $Sta $Zp<br>
	</strong></font>
	</p></td>
	<td><p>
	<font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>
	Ship To:<br>
	$FORM{'FName'} $FORM{'LName'}<br>
	$FORM{'Address'}<br>
	$FORM{'City'}\, $FORM{'State'} $FORM{'Zip'}<br>
	</strong></font>
	</p></td></tr>
	</table>

	<table width=\"300\"cellspacing=\"2\" cellpadding=\"2\">";

	if(-e "$CartDir/$CustID.c")
	{
 		$Count = 1;
 		open(TEMP, "$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n";
 		@ROWS = <TEMP>;
 		close(TEMP);

 		foreach $row (@ROWS)
  		{
  			($Item,$Name,$Desc,$Price,$Ship,$Weight,$Qty) = split(/\|/,$row);
  			if ($Ship ne "")   { $SShip   += ($Ship * $Qty); }
  			if ($Weight ne "") { $Tweight += ($Weight * $Qty); }
  			$ITotal = ($Price * $Qty);
  			$SubTotal += ($Price * $Qty);
  			$PTotal = sprintf "%.2f",$ITotal;

  			if($Price ne "")
   			{
   				print "<tr><td valign=\"top\" width=\"240\">
   				<font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>
				$Qty \#$Item $Name
				</strong></font>
				</td><td valign=\"top\" width=\"60\" align=\"right\">
				<font size=\"2\" face=\"arial\">
				\$$PTotal
				</font>
				</td></tr>\n";

   				$OrderString .= "$Item\~$Name\~$Price\~$Qty\~N\~||";
   				$OrderDesc   .= "$Qty $Name ";
   				$Count++;
   			}
  		}
	}

	$TShip = 0;

#	###### UPS SHIPPING #######
#	$UPSPickup = "Regular\%20Daily\%20Pickup";
	$ShipHow   = "$FORM{'ShipHow'}";
#	if ($Tweight > 0)
#	{
#  		$TweightX = $Tweight; if ($Tweight < 1) { $TweightX = 1; }
#  		@ShipReturn = &Ship_Module("$ShipHow", "$Zp", "$FORM{'x_ship_to_zip'}", "$FORM{'x_ship_to_country'}", "$TweightX", "$UPSPickup", "$shipextra2", "shipextra3", "$shipextra4", "$shipextra5");
#  		$ModShipAmount = $ShipReturn[0]; $Retrn = $ShipReturn[1];
#  		if ($ModShipAmount eq "")  {   $BackShip = ($Tweight * 3);   }
#  		$SHIP = ($ModShipAmount + $BackShip);
#	}
#	$TShip = ($SHIP + $SShip);
#
#	# add in a fixed $5.00 charge
#	#$TShip += 5;
#
	if ($ShipHow eq "1DA")    { $Method = "Next Day Air"; }
	if ($ShipHow eq "2DA")    { $Method = "2nd Day Air"; }
	if ($ShipHow eq "3DS")    { $Method = "3 Day Select"; }
	if ($ShipHow eq "GND")    { $Method = "Ground"; }
	$ShipType = "$Method";
#
#	if ($Retrn eq "6920")
#	{
# 		($ShipErrorA, $ShipErrorB) = split(/--/,$Results[2]);
# 		print "<p><b>WARNING: $ShipErrorA <br>";
# 		print "Please use the back button on your browser and enter a valid zip or postal code.</b></p>";
# 		exit;
#	}
#	###### END UPS SHIPPING #######

	$Taxble = 0;
	if ($FORM{'State'} && $FORM{'State'} eq "$Sta") { $STax = ($Tax * $SubTotal); $TTax = sprintf "%.2f",$STax; $Taxble++; }
	if ($Taxble eq "0") { $STax = "0.00"; $TTax = "0.00"; }

	$Total  = ($SubTotal + $TShip + $STax);
	$TTotal = sprintf "%.2f",$Total;
	$STotal = sprintf "%.2f",$SubTotal;
	$TTShip = sprintf "%.2f",$TShip;
	print "<tr><td align=\"right\" colspan=\"2\">";
	print "<input type=\"hidden\" name=\"Shipping\"      value=\"$TTShip\">\n";
	print "<input type=\"hidden\" name=\"ShipHow\"       value=\"$ShipType\">\n";
	print "<input type=\"hidden\" name=\"Tax\"           value=\"$TTax\">\n";
	print "<input type=\"hidden\" name=\"x_Amount\"      value=\"$TTotal\">\n";
	print "<input type=\"hidden\" name=\"x_tax\"         value=\"$TTax\">\n";
	print "<input type=\"hidden\" name=\"x_Description\" value=\"$OrderDesc\">\n";
	print "<input type=\"hidden\" name=\"Orderstring\"   value=\"$OrderString\">\n";
	print "$Hide";
	print "<hr align=\"right\"></td></tr>";
	print "<tr><td valign=\"top\" width=\"240\" align=\"right\"><font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>Sub Total:</strong></font><td valign=\"top\" width=\"60\" align=\"right\"><font size=\"2\" face=\"arial\">\$$STotal</font></td></tr>";
#	print "<tr><td valign=\"top\" width=\"240\" align=\"right\"><font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>Shipping:</strong></font><td valign=\"top\" width=\"60\" align=\"right\"><font size=\"2\" face=\"arial\">$TTShip</font></td></tr>";
	print "<tr><td valign=\"top\" width=\"240\" align=\"right\"><font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>Shipping Included</strong></font><td valign=\"top\" width=\"60\" align=\"right\"><font size=\"2\" face=\"arial\">&nbsp;</font></td></tr>";
	print "<tr><td valign=\"top\" width=\"240\" align=\"right\"><font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>Tax:</strong></font><td valign=\"top\" width=\"60\" align=\"right\"><font size=\"2\" face=\"arial\">$TTax</font></td></tr>";
	print "<tr><td valign=\"top\" width=\"240\" align=\"right\"><font size=\"1\" face=\"arial\" color=\"2626FF\"><strong>Total Order:</strong></font><td valign=\"top\" width=\"60\" align=\"right\"><font size=\"2\" face=\"arial\"><strong>\$$TTotal</strong></font></td></tr>";
	print "</td></table>";

	print "<td width=\"300\" valign=\"top\"><table border=\"0\" width=\"300\" cellpadding=\"2\" cellspacing=\"2\" bgcolor=\"FFFFFF\" align=\"center\" vlign=\"top\">\n";

	if ($FORM{'PayHow'} && $FORM{'PayHow'} eq "PrePay")
	{
		print "<tr><td colspan=\"2\" align=\"center\" style=\"font-size: 9pt;\">
				Paying with your DHS Club Pre-Payment Account.</td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>Account Name:</strong></font></td>\n";
		print "<td>$meminfo->{firstname} $meminfo->{lastname}</td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>DHS Club ID:</strong></font></td>\n";
		print "<td>$CustID</td></tr>\n";
  		print "<tr><td colspan=\"2\" align=\"center\">
			<input type=\"submit\" value=\"Submit Payment\"></td></tr>";
	}
	# Credit Card is the default.
	else
	{
		print "<tr><td colspan=\"2\"><span style=\"font-size: 9pt;\">This would be the information as listed on the credit or checking account you will be using.<span></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>First Name:</strong></font></td>\n";
		print "<td><input maxlength=\"50\" name=\"x_First_Name\" size=\"30\" value=\"$FORM{'FName'}\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>Last Name:</strong></font></td>\n";
		print "<td><input maxlength=\"50\" name=\"x_Last_Name\" size=\"30\" value=\"$FORM{'LName'}\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>Address:</strong></font></td>\n";
		print "<td><input maxlength=\"50\" name=\"x_Address\" size=\"30\" value=\"$FORM{'Address'}\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>City:</strong></font></td>\n";
		print "<td><input maxlength=\"50\" name=\"x_City\" size=\"30\" value=\"$FORM{'City'}\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>State/Province:</strong></font></td>\n";
		print "<td><input maxlength=\"6\" name=\"x_State\" size=\"30\" value=\"$STATE\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>Postal Code:</strong></font></td>\n";
		print "<td><input maxlength=\"6\" name=\"x_Zip\" size=\"30\" value=\"$FORM{'x_ship_to_zip'}\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial\"><strong>Country</strong></font></td>\n";
		print "<td>" . &G_S::cbo_Country($q, 'x_Country', 'US') . "</td></tr>\n";
  		print "<tr><td colspan=\"2\" align=\"center\">
			<input type=\"image\" src=\"/storeclerk/images/checkout.gif\" name=\"Checkout\" border=\"0\">
			</td></tr>";
	}

	if($TestMode eq "YES")
	{
  		print "<tr><td><font size=\"1\" face=\"arial\"><strong>\*\*CART IS IN TEST MODE\*\*<br>For testing, enter<br> card number: 5454545454545454<br> and a valid expiration date<br> on the next page.</strong></font></td></tr>";
	} 
	print "</form></td></tr>";
	print "</table></td></tr></table></div><br><hr align=\"center\" width=\"600\"><br>";
	&printFoot;
}

sub printSearch
{
	&printHead;
 	print "<div align=\"center\"><table border=\"0\" width=\"600\" cellpadding=\"3\" cellspacing=\"3\">\n";
 	print "<tr><td class=\"more_info\" valign=\"top\" nowrap>$ME_FORM Browse: <select name=\"DT\">\n";
 	open(DEPT,$DeptBase) || die "Couldn't open $DeptBase\n";
 	@LINES = <DEPT>;
 	close(DEPT);
 	foreach $line (@LINES)
  	{
  		($Dept,$Name) = split(/\|/,$line);
  		if ($Name ne "") { print "<option value=\"$Dept\">$Name</option>\n"; }
  	}
 	print "</select>\n<input type=\"image\" name=\"SearchDept\" src=\"/storeclerk/images/go.gif\" alt=\"Go\" border=\"0\" height=\"21\" width=\"50\"></div></form></td>";
 	print "<td class=\"more_info\" valign=\"top\" nowrap>$ME_FORM Search: <input class=\"more_info\" name=\"KeyWord\" size=\"12\" maxlength=\"25\"> <input type=\"image\" name=\"SearchKey\" src=\"/storeclerk/images/go.gif\" border=\"0\" height=\"21\" width=\"50\"></div></form></td></tr>\n";
 	$Count = 1;
 	$IItems = "0";
 	open(DATA,$DataBase) || die "Couldn't open $DataBase\n";
 	@ROWS = <DATA>;
 	close(DATA);
 	foreach $row (@ROWS)
  	{
  		($Dept,$Item,$Name,$Desc,$Opt1,$Opt2,$Opt3,$Weight,$Price,$Ship,$Url,$Image,$Tax,$Invt) = split(/\|/,$row);
  		$SLine   = $row;
  		$SLine   =~ tr/a-z/A-Z/;
  		$InKey   = "$FORM{'KeyWord'}";
  		$InKey   =~ tr/a-z/A-Z/;
  		if ($SLine =~ /$InKey/)
   		{
   			$IItems++;
   			print "<tr><td colspan=\"2\"><p><a href=\"$script_name\?IT=$Item\"><font face=\"arial\">$Name</font></a><br>\n";
   			print "<font face=\"arial\" size=\"1\"><strong>$Item - $Desc</strong></font></p></td></tr>\n";
   			$Count++;
   		}
  	}
	print "</table></div><br>";
	if($IItems eq "0") { &NoMatch; }
	&printFoot;
}

###### The form where you enter shipping info and choose payment method.
sub printShipTo
{
	$SubTotal = "0.00";
	$ItemCount = "0";
	print "$ME_FORM\n";
	print "<div align=\"center\">";
	print "<table border=\"0\" bgcolor=\"FFFFFF\" cellspacing=\"2\" cellpadding=\"4\" width=\"600\">\n";
	print "<tr><td align=\"center\"><font size=\"2\" face=\"arial, helvetica, sans-serif\"><strong><u>Items Ordered</u></strong></font></td>\n";
	print "<td width=\"300\" align=\"center\"><font size=\"2\" face=\"arial, Helvetica, sans-serif\"><strong><u>Shipping Information</u></strong></font></td></tr>\n";
	print "<tr><td valign=\"top\">";
	if(-e "$CartDir/$CustID.c")
 	{
 		open(TEMP, "$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n";;
 		@ROWS = <TEMP>;
 		close(TEMP);
 		foreach $row (@ROWS)
  		{
  			($Item,$Name,$Desc,$Price,$Ship,$Weight,$Qty) = split(/\|/,$row);
  			$SubTotal += ($Price * $Qty);
  			$Remove   = $Item . R;
  			$Quantity = $Item . Q;
  			if($Price ne "")
   			{
   				print "<p><input type=\"checkbox\" checked name=\"$Remove\" value=\"1\"> <input name=\"$Quantity\" size=\"4\" value=\"$Qty\"> <font size=\"2\" face=\"arial, helvetica, sans-serif\" color=\"#2626FF\"><strong>$Name</strong><br><font size=\"1\" face=\"arial, helvetica, sans-serif\" color=\"#000000\">$Desc<br>Price: <b>\$$Price</b> ea.</font></p>\n";
   				$ItemCount = "1";
   			}
  		}
 	}
	if($ItemCount eq "1")
 	{
 		$STotal = sprintf "%.2f",$SubTotal;
 		print "<p><font size=\"1\" face=\"arial\"><strong>Subtotal: \$$STotal</strong></font></p>\n";
 		print "<p><font size=\"1\" face=\"arial\"><strong>Remove An Item: Uncheck The Item, Click UPDATE<br>Change Quantity: Enter New Quantity, Click UPDATE</strong></font></p>\n";
 		print "<p><input type=\"submit\" name=\"RD\" value=\"UPDATE\" border=\"0\"></p>\n";
 		print "<p align=\"center\"><a href=\"$Back\"><img src=\"/storeclerk/images/continue.gif\" alt=\"Continue Shopping\" border=\"0\"></a></p>\n";
 		$None = 0;
 	} else {
 		print "<p align=\"center\"><font size=\"3\" face=\"arial\"><strong>Your Shopping Cart Is Empty<br></strong></font></p>\n";
 		print "<p align=\"center\"><a href=\"$Back\"><img src=\"/storeclerk/images/continue.gif\" alt=\"Continue Shopping\" border=\"0\"></a></p>\n";
 		$None = 1;
 	}

	if($None eq "1")
 	{
 		print "</td></tr></table></div>\n";
 		print "<br><hr width=\"590\" align=\"center\">\n";
 		&printFoot;
 	} 
	else 
	{
		my $the_value = '';
 		print "</td><td width=\"300\" valign=\"top\"><table border=\"0\" width=\"300\" cellpadding=\"2\" cellspacing=\"2\" bgcolor=\"FFFFFF\" align=\"center\" valign=\"top\">\n";
 		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Your Email:</strong></font></td>\n";
		$the_value = $FORM{'Email'} || '';
  		print "<td><input maxlength=\"50\" name=\"Email\" size=\"30\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>First Name:</strong></font></td>\n";
		$the_value = $FORM{'FName'} || '';
  		print "<td><input maxlength=\"50\" name=\"FName\" size=\"30\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Last Name:</strong></font></td>\n";
		$the_value = $FORM{'LName'} || '';
  		print "<td><input maxlength=\"50\" name=\"LName\" size=\"30\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Address:</strong></font></td>\n";
		$the_value = $FORM{'Address'} || '';
  		print "<td><input maxlength=\"50\" name=\"Address\" size=\"30\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>City:</strong></font></td>\n";
		$the_value = $FORM{'City'} || '';
  		print "<td><input maxlength=\"50\" name=\"City\" size=\"30\" value=\"$the_value\"></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>State:</strong></font></td>\n";
		$the_value = $FORM{'State'} || '';
  		print "<td><font size=\"-1\">" . &cbo_State('State', $the_value) . "</font></td></tr>\n";
		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Province:</strong></font></td>\n";
		$the_value = $FORM{'Province'} || '';
  		print "<td><input maxlength=\"40\" name=\"Province\" size=\"30\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Zip/Postal Code:</strong></font></td>\n";
		$the_value = $FORM{'x_ship_to_zip'} || '';
  		print "<td><input maxlength=\"15\" name=\"x_ship_to_zip\" size=\"12\" value=\"$the_value\"></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Country:</strong></font></td>\n";
		$the_value = $FORM{'x_ship_to_country'} || 'US';
  		print "<td><font size=\"-1\">". &G_S::cbo_Country($q, 'x_ship_to_country', $the_value) . "</font></td></tr>\n";
  		print "<tr><td align=\"right\"><font size=\"1\" face=\"arial, Helvetica, sans-serif\"><strong>Phone:</strong></font></td>\n";
		$the_value = $FORM{'Phone'} || '';
  		print "<td><input maxlength=\"15\" name=\"Phone\" size=\"15\" value=\"$the_value\"><br /><font size=\"1\" face=\"arial\">Include area/country code</font></td></tr>\n";

		# Shipping Info.
#  		print "<tr><td align=\"right\">\n";
# 		print "<font size=\"1\" face=\"arial\"><strong>Ship:</strong></font>";
# 		print "&nbsp;\n";
# 		print "</td>\n";
# 		print "<td>\n";
 		print qq|&nbsp;<input type="hidden" name="ShipHow" value="GND" />\n|;
# 		print "<font size=\"-1\"><select name=\"ShipHow\">\n";
# 		print "<option value=\"GND\" selected>Standard Shipping</option><option value=\"3DS\">3 Day Select</option>";
# 		print "<option value=\"2DA\">2nd Day Air</option><option value=\"1DA\">Next Day Air</option></select></font>\n";
# 		print "</td></tr>";

		# Payment selection.
  		print "<tr><td align=\"center\" colspan=\"2\">\n";
 		print "<font size=\"2\" face=\"arial, helvetica, sans-serif\"><strong><u>Payment Method:</u></strong></font>";
 		print "&nbsp;\n";
 		print "</td></tr>\n";
 		print "<tr><td>\n";
 		print "</td><td>\n";
 		print qq|<input type="radio" name="PayHow" value="AuthorizeNet" checked/>&nbsp;Credit Card<br>\n|;
 		print qq|<input type="radio" name="PayHow" value="PrePay" />&nbsp;Glocal Income ClubAccount<br>\n|;
 		print qq|<span style="font-size: 80%; padding-left: 2em;">
			<a href="https://www.clubshop.com/cgi/funding.cgi?action=report" target="_blank">
			Check Club Account balance here</a></span>\n|;
 		print "</td></tr>";

  		print "<tr><td align=\"center\" colspan=\"2\"><br><div align=\"center\"><input type=\"image\" src=\"/storeclerk/images/checkout.gif\" name=\"Total\" value=\"1\" border=\"0\"></div>";
  		print "<input type=\"hidden\" name=\"CK\" value=\"1\"></td></tr>\n";
  		print "</table></td></tr></table></div></form>\n";
  		print "<br><hr width=\"590\" align=\"center\">\n";
  		&printFoot;
 	}
}

sub RePost
{
	&printHead;
	open(CART,"$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n";
	@ITEMS = <CART>;
	close(CART);
	$NewCart = "";
	foreach $row (@ITEMS)
 	{
 		($Item,$Name,$Desc,$Price,$Ship,$Weight,$Qty) = split(/\|/,$row);
 		$ItemDel  = $Item . R;
 		$ItemQuan = $Item . Q;
 		$Quantity = $FORM{$ItemQuan};
 		$Remove   = $FORM{$ItemDel};
 		if($Remove eq "1")
  		{
  			$NewCart .= "$Item\|$Name\|$Desc\|$Price\|$Ship\|$Weight\|$Quantity\|\n";
  			$FORM{$ItemDel} = "0";
  		}
 	}
	open(NCART,">$CartDir/$CustID.c") || die "Couldn't open cart: $CartDir/$CustID.c\n";
	print NCART "$NewCart";
	close(NCART);
}

###### Empty the member's cart.
sub ResetCart
{
	&printHead;
	if (-e "$CartDir/$CustID.c") { unlink ("$CartDir/$CustID.c"); }
	print "<br><hr width=\"400\" align=\"center\"><p align=\"center\">Your Cart Is Now Empty<br><br>";
	print "<a href=\"$Back\"><img src=\"/storeclerk/images/continue.gif\" alt=\"Continue Shopping\" border=\"0\"></a></p><hr width=\"400\" align=\"center\">\n";
	&printFoot;
}

###### 05/30/02 added country to the billing form
###### 06/07/02 removed the flat rate shipping fee
###### 06/07/02 added handling for path info when calling back after a login redirect
###### 06/27/02 eliminated the shipping value from checkout
###### 07/17/02 changed some HTML templating
###### 07/23/02 disable the 'Quick Links'
###### 01/06/03 made use of the 'use' style for G_S lib and also went to the use of the new cookie
###### installed an expires header as well and straightened up some bad HTML
###### 01/13/02 used the real cookie rather than just the name, DUH
###### 03/08/06 Added the Pre-Payment option as a way to pay.
# 05/28/14 added a charset header for utf-8