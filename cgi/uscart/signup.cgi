#!/usr/bin/perl
# StoreClerk Affiliate Manager
#############################################################################
# StoreClerk By JMS Online Systems                                          #
# (c) 2000,2001 All rights reserved.                                        #
# www.jmsonline.net Custom ecommerce programming is our specialty!          #
# Support: support@jmsonline.net                                            #
#############################################################################
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use CartMail;

$AffPath   = "http://www.clubshop.com/cgi/uscart"; # No Trailing Forward Slash!
$AffData   = 'aff.dat';
$Banners   = 'banners.txt';
$mail_prog = '/usr/sbin/sendmail' ;
##### END SETUP #####
print "Content-type: text/html\n\n";
&GetFormInput;
$CompanyName = $field{'CompanyName'} ;
$WebsiteName = $field{'WebsiteName'} ;
$WebsiteURL  = $field{'WebsiteURL'} ;
$Name        = $field{'Name'} ;
$Email       = $field{'Email'} ;
$Address     = $field{'Address'} ;
$City        = $field{'City'} ;
$State       = $field{'State'} ;
$Zip         = $field{'Zip'} ;
$Country     = $field{'Country'} ;
$PassWord    = $field{'PassWord'} ;
$SignUp      = $field{'SignUp'} ;
$message     = "" ;
$found_err   = "" ;

$errmsg = "Website URL must be filled in.<br>\n";
if ($WebsiteURL eq "")        { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "Name must be filled in.<br>\n";
if ($Name eq "")              { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "Please enter a valid email address<br>\n";
if ($Email !~ /.+\@.+\..+/)   {	$message = $message.$errmsg; $found_err = 1; }
$errmsg = "Address must be filled in.<br>\n";
if ($Address eq "")           { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "City must be filled in.<br>\n";
if ($City eq "")              { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "State must be filled in.<br>\n";
if ($State eq "")             { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "Zip Code must be filled in.<br>\n";
if ($Zip eq "")               { $message = $message.$errmsg; $found_err = 1; }
$errmsg = "PassWord must be filled in.<br>\n";
if ($PassWord eq "")          {	$message = $message.$errmsg; $found_err = 1; }
if ($found_err) { &PrintError; }

$Prefix   = "AF-";
$CurTime  = time();
$MemberID = $Prefix.$CurTime;
chop($MemberID);
chop($MemberID);

open(DATA,">>$AffData");
print DATA "$MemberID\|$PassWord\|$Email\|$Name\|$CompanyName\|$Address\|$City\|$State\|$Zip\|$Country\|$WebsiteName\|$WebsiteURL\|\n";
close(DATA);

open(INFO,"setup.txt");
while (<INFO>)
 {
 $Line = $_;
 chop($Line);
 ($MID,$Us,$Pa,$Comp,$CAddr,$Cit,$Sta,$Zp,$Tax,$Em,$Site,$Home,$Back,$PHead,$PFoot) = split(/\|/,$Line);
 }
close(INFO);

print "<HTML><HEAD><TITLE>Affiliate Confirmation</TITLE></HEAD>\n";
print "<BODY><br><br>\n";
print "<div align=\"center\"><table width=\"700\" align=\"center\" border=\"0\">\n";
print "<tr><td><p><font face=\"arial\" size=\"2\"><strong>Affiliate Confirmation</strong></font></P>\n";
print "<p><font face=\"arial\" size=\"2\">Please print this page. It contains information you may need in the future.</font></p>\n";
print "<p><font face=\"arial\" size=\"2\">Thank you for registering as a <b>$Comp</b> Affiliate, you can begin accumulating your commissions immediately. We issue commission checks each month.</font></p>\n";
print "<p><font face=\"arial\" size=\"2\">We will mail these checks to the following address:<br><br><ul>\n";
print "<b>$Name</b><br>\n";
if ($CompanyName ne "")
 {
 print "<b>$CompanyName</b><br>\n";
 }
print "<b>$Address<br>$City\, $State $Zip</b></ul></P>\n";
print "<p><font face=\"arial\" size=\"2\">Your Affiliate Number is: <b>$MemberID</b></font></p>\n";
print "<p><font face=\"arial\" size=\"2\">Below are some banners and links for use on your website to link to $Comp\.</font></p>\n";
print "<p><font face=\"arial\" size=\"2\">Please copy the link information from within the text box and paste to your site.</font></p>\n";
open(BANR,$Banners);
@LINES = <BANR>;
close(BANR);
 foreach $line (@LINES)
  {
  ($Path,$Desc,$Width,$Height) = split(/\|/,$line);
  if ($Path ne "")
   {
   print "<p><img src=\"$Path\" width=\"$Width\" height=\"$Height\" alt=\"$Desc\" border=\"0\"><br>\n";
   print "<textarea rows=\"4\" cols=\"70\">";
   print "<a href=\"$AffPath/cart.cgi\?ID=$MemberID\"><img src=\"$Path\" width=\"$Width\" height=\"$Height\" alt=\"$Desc\" border=\"0\"></a></textarea></p><br>\n";
   }
  }
print "<p><font face=\"arial\" size=\"2\">Or you can use a simple link with no picture:</font></p>\n";
print "<p><textarea rows=\"2\" cols=\"70\">";
print "<a href=\"$AffPath/cart.cgi\?ID=$MemberID\">$Site</a></textarea></p><br>\n";
print "<p><a href=\"$Home\">Home</a></p>";
print "</td></tr></table></div><br>\n";
print "<p align=\"center\"><font face=\"arial\" size=\"1\">\&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\"> All Rights Reserved.</font></p>\n";
print "</BODY></HTML>\n";

$recip = $Email ;

my $mail_msg = '';
open (MAIL, '>',\$mail_msg) || die "failed to create mail FH\n";
print MAIL "Reply-to: $Em\n";
print MAIL "From: $Em\n";
print MAIL "Subject: $Comp Affiliate Program\n";
print MAIL "To: $Name \n" ;
print MAIL "From: $Comp \n" ;
print MAIL "\n" ;
print MAIL "Thank you for joining our affiliate program!\n" ;
print MAIL "\n" ;
print MAIL "Your affiliate number is: $MemberID\n" ;
print MAIL "Your password is: ".$PassWord."\n" ;
print MAIL "To check the status of your commissions use this page:\n\n";
print MAIL "$AffPath/partner.cgi\n";
print MAIL "\n" ;
print MAIL "The following information was recorded for your account:\n";
print MAIL "\n" ;
print MAIL "".$CompanyName."\n" ;
print MAIL "".$WebsiteName."\n" ;
print MAIL "".$WebsiteURL."\n" ;
print MAIL "".$Name."\n" ;
print MAIL "".$Email."\n" ;
print MAIL "".$Address."\n" ;
print MAIL "".$City." ".$State." ".$Zip."\n" ;
print MAIL "".$Country."\n" ;
print MAIL "\n" ;
print MAIL "Commission checks will be sent to the above address. If the above information is incorrect please contact us at $Em\n" ;
print MAIL "\n" ;
print MAIL "Thank You,\n" ;
print MAIL "\n" ;
print MAIL "$Comp\n" ;
print MAIL "$Site\n" ;
print MAIL "$Home\n" ;
print MAIL "\n" ;
print MAIL "\n\n";
CartMail::mail_fh($mail_msg);

$recip = "$Em" ;

my $mail_msg = '';
open (MAIL, '>',\$mail_msg) || die "failed to create mail FH\n";
print RMAIL "Reply-to: $Email\n";
print RMAIL "From: $Email\n";
print RMAIL "Subject: New Affiliate Sign-up\n";
print RMAIL "New Affiliate Sign-up:\n" ;
print RMAIL "\n" ;
print RMAIL "".$CompanyName."\n" ;
print RMAIL "".$Email."\n" ;
print RMAIL "".$WebsiteName."\n" ;
print RMAIL "".$WebsiteURL."\n" ;
print RMAIL "".$Name."\n" ;
print RMAIL "".$Address."\n" ;
print RMAIL "".$City." ".$State." ".$Zip."\n" ;
print RMAIL "".$Country."\n" ;
print RMAIL "\n" ;
print RMAIL "Affiliate Number: $MemberID\n" ;
print RMAIL "Affiliate Password: ".$PassWord."\n" ;
print RMAIL "\n" ;
print RMAIL "\n\n";
CartMail::mail_fh($mail_msg);

sub PrintError
 {
 print "<br><br><br><br><hr align=\"center\" width=\"600\"><p align=\"center\">$message</p><hr align=\"center\" width=\"600\">\n";
 exit;
}

sub GetFormInput
 {
 (*fval) = @_ if @_ ;
 local ($buf);
 if ($ENV{'REQUEST_METHOD'} eq 'POST') { read(STDIN,$buf,$ENV{'CONTENT_LENGTH'}); }
 else {	$buf=$ENV{'QUERY_STRING'}; }
 if ($buf eq "") { return 0 ;	}
 else {
 	@fval=split(/&/,$buf);
	foreach $i (0 .. $#fval)
         {
	 ($name,$val)=split (/=/,$fval[$i],2);
	 $val=~tr/+/ /;
	 $val=~ s/%(..)/pack("c",hex($1))/ge;
	 $name=~tr/+/ /;
	 $name=~ s/%(..)/pack("c",hex($1))/ge;
	 if (!defined($field{$name})) {	$field{$name}=$val; }
	 else {	$field{$name} .= ",$val"; }
         }
      }
return 1;
}

exit;


###### 12/14/06 Changed method of mail sending to no longer use the sendmail command.

