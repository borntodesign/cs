#!/usr/bin/perl -w
###### TrialPartner.cgi
###### The TrialPartner reporting page, directed towards Trial Partners themselves, rather than Partners
###### in other words, this is more of a marketing report than an informational report
###### created: 10/17/13	Bill MacArthur
###### last modified: 03/17/16	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use JSON;
use lib ('/home/httpd/cgi-lib');
use XML::local_utils;
use G_S qw( $LOGIN_URL );
use DB_Connect;
require TrialPartner;
binmode STDOUT, ":encoding(utf8)";
#use Data::Dumper;	# only for debugging

my $DEBUG = 0;	# set this to -0- to turn off debug statements on STDERR
my $noTrackPartner = 0;	# set this to -0- to create a tracking activity for visiting this report by a Partner who is still in the line
my %TMPL = (
	'language_pack' => 11038,
#	'main'	=> 11037,
	'json_cfg' => 11039,
	'TT' =>{
		'main' => '/home/httpd/cgi-templates/TT/TrialPartner.tt'
	}
);

my ($db, $member, $lb, @errors, @alerts, $id, $data, $json_cfg, %Cache);
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my $TT = Template->new({'ABSOLUTE' => 1});

my ($loginid, $login) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
#debug('login: ' . Dumper($login));

exit unless $db = DB_Connect('TrialPartner');
$db->{'RaiseError'} = 1;

my $TP = TrialPartner->new({'db'=>$db, 'cgi'=>$q, 'gs'=>$gs});

my $ckID = $q->cookie('id') || '';
my $paramID = $q->param('id') || '';
$paramID = $gs->Alias2ID($db, 'members', $paramID) if $paramID && $paramID =~ m/\D/;
my $loginType = 0;

if ($loginid && $loginid !~ m/\D/)	# they are fully logged in
{
	debug("loginid= $loginid");
	$loginType = 2;
	$member = $TP->GetMe($loginid);

	if ($login->{'membertype'} eq 'v')	# they are a Partner
	{
		# check to see if they are still in the line
		if ($member->{'seq'})
		{
			debug("Setting id= $loginid");
			$id = $loginid;
		}
		else
		{
			$id = $paramID;	# we have to identify the party by the id parameter because the simple ID cookie will be the same as the full login cookie
			$member = ();
		}
	}
	elsif ($login->{'membertype'} eq 'tp' || $member->{'seq'})	# they are a Trial Partner even if they don't have the login membertype to prove it...
	{															# they could have just clicked the link and their cookie has not been refreshed yet
		$id = $loginid;
	}
	else
	{
		$id = $paramID;
		$member = ();			# we want this to get reinitialized below on the actual ID
	}
	debug("inside full login... id= $id");
}
elsif ($ckID)	# we are not fully logged in but we do have a simple login
{
	$loginType = 1;
	
	# if we have a report ID parameter and our login ID does not match, we do not have access to this report
	if ($ckID ne $paramID)
	{
		print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
		Exit();
	}
	
	$id = $ckID;
}
elsif ($paramID)
{
	$id = $paramID;
}

unless ($id)
{
	# actually, just creating the page involves doing a lot of stuff that depends on knowing who the viewer is, so...
#	push @errors, 'No membership ID received';
#	Do_Page();
	print $q->redirect("$LOGIN_URL?destination=" . $q->script_name);
	Exit();
}

$member ||= $TP->GetMe($id);	# if we entered the logged in block above we probably already set this up

unless ($member->{'seq'})
{
	push @errors, "That ID is not recognized in the Trial Partner line: " . $q->escapeHTML($id);
	Do_Page();
}

LoadLB();	# we are doing this here since we have to interpolate placeholders in the content of the language pack
			# if we ever end up having to place this above the point of knowing who our member is, then we will have to instead interpolate the $lb keys once we have the needed data

# if they are requesting a Trial Partner transfer, we will take care of that before proceeding
if (($q->param('action') || '') eq 'requestXfer')
{
	if (! $TP->MadeTPxferRequest)
	{
		$db->begin_work;
		$db->do(qq|INSERT INTO notifications (id, notification_type) VALUES ($member->{'id'}, 161)|);
		$db->do(qq|INSERT INTO member_actions (id, "type", ip) VALUES ($member->{'id'}, 15, ?)|, undef, $q->remote_addr);
		$db->commit;
	}
	# whether they have done it before or not, we will give 'em the blurb ;)
	push @alerts, $lb->{'reqsent'};
}

# Dick is not concerned about crossline looking at the report, but if a Partner is logged in, we can at least deny access to a particular report if he would not meet the normal access CK
# ... actually that does not work as it will often be the case that an upline's TP will be at the head of the report and a downline Partner would *NOT* have normal access
# but he still needs to see the report
# ... So instead we will see what the pns is and make sure that the related pool is our pnspid (we are presumably a Partner... why would a non-Partner want to look at somebody else's report)
if ($loginType == 2)
{
	if ($loginid == 1)
	{
		# do nothing, just let 01 look
	}
	elsif ($member->{'id'} != $loginid)	# we are viewing a report that is not ours, so we need to check if we have access
	{
#		my ($rv) = $db->selectrow_array("SELECT network.access_ck1($member->{'id'}, $loginid)");
		my ($rv) = $db->selectrow_array("
			SELECT np.pid
			FROM network.pspids np
			JOIN network.partner_network_separators nps
				ON np.pid=nps.pid
			WHERE nps.pnsid= $member->{'pnsid'}
			AND np.id= $loginid");
		
		unless ($rv)
		{
			$member = ();	# no use in displaying the information in the header
			push @errors, $lb->{'noauth'};
			Do_Page();
		}
	}
}

RecordActivity();	# this is conditional, but the conditions will be evaluated within the function

$data = $TP->GetLineData( UplineCTE() );	# that method will use the ID cached from the last lookup method
$json_cfg = $gs->GetObject($TMPL{'json_cfg'}) || warn "Failed to load json_cfg: $TMPL{'json_cfg'}";

ConfigureReportData() if ($loginid || 0) > 1;

Do_Page();


###### beginning of sub-routines

=comment
{
"startFoldingAt" : 100,	/* reports that have this many lines will enable folding */
"foldCountMax" : 10,	/* The maximum number of rows contained in a fold, or in other words, if we have X or more, we fold */
"foldPartnerMin" : 3,	/* if we have less than this number between partners, we will not fold... set to -0- to always fold between partners */
"howFarFromTop" : 50	/* How many lines will we display at the top of the report... note, if we have a Partner as line #1, then 'foldPartnerMin' will override this value */
}
=cut

sub ConfigureReportData	# establish folding points and such
{
	my $cfg = decode_json($json_cfg);
#warn "rows: ". scalar @{$data};	
	return unless $cfg->{'startFoldingAt'} < scalar @{$data};	# if our configuration specifies x number of rows before we begin folding, and we don't have that many total, we are done

	# we'll process the first row using the "howFarFromTop" setting if it's not a Partner
	my $mark = ${$data}[0];
	my $idx = 1;
	my $endX = ();
	my $_cfg = {'v' => $cfg->{'foldPartnerMin'}, 'tp' => $cfg->{'howFarFromTop'}};
	($endX, $idx) = LoopData($cfg, $idx,  $_cfg);
	
	$mark->{'fold'} = $endX if $endX && $endX >= $_cfg->{ $mark->{'membertype'} };	# no use creating a fold for -0-
#warn "entering loop- endX: $endX, idx: $idx";
	$_cfg = {'v' => $cfg->{'foldPartnerMin'}, 'tp' => $cfg->{'foldCountMax'}};
	while ($idx < scalar @{$data})
	{
		$mark = ${$data}[$idx];	#
		$idx++;
#warn "inside Loop - idx: $idx, markID: $mark->{'alias'}";
		# from here on we will use the regular folding setting
		($endX, $idx) = LoopData($cfg, $idx, $_cfg);
		$mark->{'fold'} = $endX if $endX && $endX >= $_cfg->{ $mark->{'membertype'} };	# no use creating a fold for -0-
#warn "inside Loop - post LoopData - idx: $idx, markID: $mark->{'alias'}";
	}
}

sub debug
{
	return unless $DEBUG;
	warn $_[0];
}

sub Do_Page
{
	print $q->header('-charset'=>'utf-8', '-expires'=>'+1h');
	my $out = ();

	foreach (@{$data})
	{
#		$gs->Prepare_UTF8($_, 'decode');
	}

#	$gs->Prepare_UTF8($member, 'decode');
	
	my $rv = $TT->process($TMPL{'TT'}->{'main'},
		{
			'errors' => \@errors,
			'alerts' => \@alerts,
			'script_name' => $q->script_name,
			'member' => $member,
			'lb' => $lb,
			'data' => $data,
			'json_cfg' => $json_cfg,
			'loginType' => $loginType,
			'login' => $login,
			'upg_price' => UpgradePrice(),
			'actual_language' => $Cache{'actual_language'},
			'links' => Links(),
			'HasPRTPorP' => $TP->HasPRTPorP(),
			'MadeTPxferRequest' => $TP->MadeTPxferRequest()
		}, \$out);
	
	print $rv ? $out : $TT->error;	# $rv should be true if we were successful
	
	Exit();
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Links
{
	return {} unless $id;	# Do_Page() can be called in many cases, at least one without having determined an ID, and then we cannot construct proper links
	return {
		'requestXfer'	=> $q->script_name . "?id=$id;action=requestXfer",
		'tpgroup'		=> "/cgi/tpgroup.cgi?id=$id"
	};
}

sub LoadLB
{
	my $tmp = ();
	($tmp, $Cache{'actual_language'}) = $gs->GetObject($TMPL{'language_pack'});
	warn "Failed to load language pack template: $TMPL{'language_pack'}" unless $tmp;
	$Cache{'actual_language'} ||= 'en';
	$TT->process(\$tmp, {'member'=>$member, 'upg_price'=>UpgradePrice(), 'links'=>Links()}, \$lb);
	my $lu = new XML::local_utils;
	$lb = $lu->flatXHTMLtoHashref($lb);
#	$gs->Prepare_UTF8($lb, 'encode');
}

=comment
{
"startFoldingAt" : 100,	/* reports that have this many lines will enable folding */
"foldCountMax" : 10,	/* The maximum number of rows contained in a fold, or in other words, if we have X or more, we fold */
"foldPartnerMin" : 3,	/* if we have less than this number between partners, we will not fold... set to -0- to always fold between partners */
"howFarFromTop" : 50	/* How many lines will we display at the top of the report... note, if we have a Partner as line #1, then 'foldPartnerMin' will override this value */
}
=cut

sub LoopData
{
	my $cfg = shift;
	my $idx = shift;
	my $endX = shift;
	my $startX = 0;
	while ($idx < scalar @{$data})
	{
		my $tmp = ${$data}[$idx];
		$idx++;
		if ($tmp->{'membertype'} eq 'v')
		{
			$idx--;	# we want this party to become the next mark when the loop above cycles again

			# if we hit a Partner, we are done in here and we need to put him back on the list so that he will be displayed
			last;
		}
		elsif ($tmp->{'membertype'} eq 'mp')
		{
			# we always want a member pool to show at the bottom
			last;
		}

		$startX++;
#warn "proc: $tmp->{'alias'} $tmp->{'membertype'}, cnt4mtype: $endX->{ $tmp->{'membertype'} }, idx: $idx";
		last if $startX == ($endX->{ $tmp->{'membertype'} } || 100);
	}
	return ($startX, $idx);
}

=comment
On 10/18/13, Dick said:
I don't see that a Partner would go to a TP's actual report (starting with their position in the line). I do see them clicking on the link at the bottom of their Team Report to view the TP Team Report.

So as long as we can match the cookie of the accessor with the ID for the report, we can report this as the TP's activity.

Once they upgrade and until they have been moved to the Partner Team Report, we should continue to report their access as activity. 
=cut

sub RecordActivity
{
	# actually, if we are fully logged in, we should have a simple cookie ID as well
	return unless $loginType >= 1 && $ckID eq $id;
	return if $member->{'membertype'} eq 'v' && $noTrackPartner;
	
#	$db->do('INSERT INTO activity_tracking (id, "class") VALUES (?, 51)', undef, $id);
# new tracking mechanism being setup March 2016 that will create a variety of activity types based upon number of visits
	$db->do('INSERT INTO backend_processing (proctype, val_int1) VALUES (10, ?)', undef, $id);
}

sub UpgradePrice
{
	return 0 if $member->{'membertype'} eq 'v';
	
	# if they are a former partner, then just the BASIC renewal, otherwise initial annual, basic monthly initial and basic monthly renewal
	my $stypes = $member->{'vip_date'} ? '41' : '2,40,41';
	
	$Cache{'UpgradePrice'} ||= $db->selectrow_hashref("
		SELECT currency_code, SUM(amount) AS amount FROM subscription_pricing_by_country
		WHERE subscription_type IN ($stypes)
		AND country_code= '$member->{'country'}'
		GROUP BY currency_code
	");
	
	return $Cache{'UpgradePrice'};
}

sub UplineCTE
{
	return undef unless $loginType == 2;	# if they are not fully logged in, there is no upline access
	
	# $loginid at this point should be a strictly numerical value, so it is safe to insert directly into the SQL
	return "
		-- this part takes care of Partners accessing Partners
		SELECT id FROM grp.grp_complete_less_stops WHERE upline= $loginid
		UNION
		-- this part takes care of access parties for whom I am the Partner
		SELECT id FROM my_upline_vip WHERE upline= $loginid
		UNION
		-- this part takes care of accessing non-Partners under Partners for whom I have access
		SELECT muv.id FROM my_upline_vip muv
		JOIN grp.grp_complete_less_stops gc ON gc.id=muv.upline AND gc.upline= $loginid
		JOIN members m ON m.id=muv.id
		-- optimized for the Trial Partner report
		WHERE m.membertype = 'tp'
	";
}

__END__

02/14/14	Added UpgradePrice()
02/21/14	Finally got the actual_language going to tell the template what language we are so that the Google translate can work properly for other than English
08/09/14	Moved the main template out into a file system TT template, as well as changing UpgradePrice() to give the price for a former partner under the Welcome Back Program
08/21/14	Added Links()
11/26/14	Addressed encoding issues, now that we are on a newer platform, in Do_Page()
12/10/14	Made it so that if we cannot identify the viewer, we redirect to login instead of trying to render the page with an error
01/08/15	put an admin override to not ConfigureReportData() if 01 is looking
04/30/15	added an expires http header to the report
05/08/15	Tweaks to deal with encoding issues that arose with the latest upgrade of DBD::Pg
03/17/16	Revised RecordActivity to create a backend_processing record instead of an activity_tracking one