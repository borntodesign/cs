#!/usr/bin/perl -w
######  msg_sub_sys_spawn.cgi
######  This is part of the messaging sub-system
###### originally written by: Stephen Martin
######	last modified 05/30/2003	Bill MacArthur

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use Digest::MD5 qw( md5_hex );
use DBI;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw( $LOGIN_URL );

$| = 1;

use constant IS_MODPERL => $ENV{MOD_PERL};
use subs qw(exit);

# Select the correct exit function
*exit = IS_MODPERL ? \&Apache::exit : sub { CORE::exit };

###### GLOBALS
our($UID, $meminfo) = ();
my $sth = ();
our $db        = ();
our $q         = new CGI;
our $id        = our $firstname = our $lastname = '';

my $qry;
my $TMPL;
my $MSG_TMPL_ID;
my $MEMBER;

my $I;
my $O;
my $MOID;

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

###### this is the threshold below which we look for an upline guide

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
}


###### validate the user
###### we'll skip some stuff if we're ADMIN
unless ( $param{admin} ) {
    ( $UID, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if we decide to exclude access based on an ACCESS DENIED password, we'll do it here

###### if the UID has any non-digits it means that its bad/expired whatever
    if ( !$UID || $UID =~ /\D/ ) { &RedirectLogin(); exit }

    $firstname = $meminfo->{firstname};
    $lastname  = $meminfo->{lastname};
    $id        = $UID;

###### DB_Connect does its own error handling, so we only need exit if it doesn't work
    unless ( $db = DB_Connect( 'msg_sub_sys.cgi') ) { exit }
}

###### now we'll take care of the ADMIN mode
else {
	unless ( $param{id} )
	{
		Err('Missing a required parameter');
		exit;
	}

###### here is where we get our admin user and try logging into the DB
    my $operator = $q->cookie('operator');
    my $pwd      = $q->cookie('pwd');

	unless ($operator && $pwd)
	{
		Err('Missing login credentials');
		exit;
	}
    require '/home/httpd/cgi-lib/ADMIN_DB.lib';
    unless ( $db = &ADMIN_DB::DB_Connect( 'bizreport', $operator, $pwd ) ) {
        exit;
    }

    my $qry =
"SELECT lpad(Id::TEXT,7,'0') as id, FirstName, LastName, membertype, password FROM members WHERE id = '$param{id}'";
    if ( &ID_Ck($qry) == 0 ) { goto 'END' }
}
$db->{RaiseError} = 1;

###### At this point we are verified and can commence operations

$I = $param{I} || '';
$O = $param{O} || '';

###### Collect everything about the member logged in 

$MEMBER = get_info( $UID, "id" );

###### Verify we are who we say we are

my $crc1 = $I ^ $O;
my $crc2 = $MEMBER->{id} ^ $MEMBER->{oid};

unless ( $crc1 == $crc2 )
{
 Err("CRC miss match!");
 goto 'END';
}
 
###### Print the message template.

$MOID = $param{MOID};

$MSG_TMPL_ID=obtain_message($MOID);

$TMPL = &G_S::Get_Object( $db, $MSG_TMPL_ID );
unless ($TMPL) { &Err("Couldn't retrieve the template object. $MSG_TMPL_ID : $MOID"); exit; }

print $q->header( -expires => 'now' );
print $TMPL;

END:
$db->disconnect;
exit;

sub Err {
    print	$q->header( -expires=>'now' ),
		$q->start_html( -bgcolor => '#ffffff' ),
		'There has been a problem<br><br>';
    print $q->h4( $_[0] ), scalar localtime(), ' (PST)';
    print $q->end_html();
}

sub Hidden_Admin {
###### generate hidden tags for use when in admin mode
###### otherwise substitute nothing
    if ( $param{'admin'} && $param{'admin'} eq '1' ) { return '&admin=1' }
    else { return '' }
}

sub ID_Ck {
    my $membertype;    ###### a temporary variable just to test with
    my $sth = $db->prepare( $_[0] );
    $sth->execute;

###### id, firstname, & lastname are globals
    ( $id, $firstname, $lastname, $membertype ) = $sth->fetchrow_array;
    $sth->finish;
    unless ($membertype) {
        &Err('No match on the ID and Password received');
        return 0;
    }
    elsif ( $membertype ne 'v' ) {
        &Err('According to our records, you are not a VIP');
        return 0;
    }
    return 1;
}

sub RedirectLogin {
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' );
    print
"Please login at <a href=\"$LOGIN_URL\">$LOGIN_URL</a><br /><br />";
    print scalar localtime(), "(PST)";
    print $q->end_html;
}

sub get_info {
    my ($id) = @_;
    shift;
    my ($sw) = @_;

    unless ( $id && $sw ) {
        &Err("You have entered get_info with the wrong \# of parameters!");
        exit;
    }

    my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = "SELECT 
          oid,* 
         FROM members
         WHERE $sw = ?";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($id);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data;
}

sub obtain_message
{
 my  ($moid) = @_;

  my ( $sth, $qry, $data, $rv );    ###### database variables

    $qry = "SELECT
             cgi_objects.obj_id as obj_id
            FROM
             cgi_objects
            WHERE 
             ( messages.msg_id = cgi_objects.obj_id ) 
            AND
             ( messages.oid    = ?) ";

    $sth = $db->prepare($qry);

    $rv = $sth->execute($moid);

    $data = $sth->fetchrow_hashref;
    $rv   = $sth->finish();

    return $data->{obj_id}

}

###### 12/18/02 went to the 'use' style for custom modules, also went to the AuthCustom_Generic cookie
###### 05/30/03 changed the DB connection to not connect as the script
###### also revised some logic in the admin DB connect block to exit if parameters were missing
# 02/09/2011 g.baker postgres 9.0 port lpad(text, int, text) casts
