#!/usr/bin/perl -w
###### card-order-reporting.cgi
###### a reporting tool for cards included in orders we have on file
###### this was born out of the need to provide Fab the IDs of members who activated cards associated with his Allegria program
###### deployed: 07/07/11	Bill MacArthur
###### last modified: 

###### we will use TT since at some point we may need to integrate some form of prettiness

use strict;
use Template;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S qw($LOGIN_URL);
use DB_Connect;

###### GLOBALS
my $NOMAIL = 0;	###### set to 1 to not send mail (testing environment only)
my $cgi = new CGI;
my $gs = new G_S;

my $orders = '';
my ($db, @cookies) = ();
													
my ( $id, $meminfo ) = $gs->authen_ses_key( $cgi->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
if ( $id && $id !~ /\D/ )
{
	###### we don't need to do anything if its good
}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . ${\$cgi->script_name()} . "\">$LOGIN_URL</a>");
	exit;
}

exit unless $db = DB_Connect('card-order-reporting');
$db->{'RaiseError'} = 1;

if (($cgi->param('_action') || '') eq 'report')
{
	Report();
}
else
{
	Interface();
}

$db->disconnect;
exit;

sub Err
{
	print $cgi->header(-expires=>'now'),
		$cgi->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$cgi->h4($_[0]),
		scalar localtime(),
		$cgi->end_html();
}

sub Interface
{
	my @orders = ();
	my $sth = $db->prepare('
		SELECT * FROM cbid_orders
		WHERE void=FALSE AND member_id= ?
		ORDER BY cbid_start');
	$sth->execute($id);
	while (my $o = $sth->fetchrow_hashref)
	{
		push @orders, $o;
	}
	my $TT = Template->new('INCLUDE_PATH' => '/home/httpd/cgi-templates/TT/');
	
	print $cgi->header();
	$TT->process('card-order-reporting-interface.html',
		{
			'orders' => \@orders,
			'script_name' => $cgi->script_name
		}
	
	) || print $TT->error();
}

sub Report
{
	my @qry_args = ();
	foreach my $o ( $cgi->param('orderIds') )
	{
		die "Invalid orderID param: $o" if $o =~ m/\D/;
		$orders .= "$o,";
	}
	
	unless ($orders)
	{
		Err('No orders selected for reporting');
		return;
	}
	chop $orders;

	push @cookies,
		$cgi->cookie(
			'-name'=>'card_orders_to_report',
			'-expires'=> '+1y'
		);

	my $qry = "
		SELECT
			m.alias,
			cb.stamp::date AS last_changed,
			m.firstname,
			m.lastname,
			cb.id AS cardnumber
		
		FROM
			cbid_orders co,
			clubbucks_master cb
		JOIN members m
			ON m.id=cb.current_member_id
		WHERE cb.id BETWEEN co.cbid_start AND co.cbid_end
		AND co.order_id IN ($orders)
		AND co.member_id= $id
		AND cb.current_member_id IS NOT NULL\n";
	
	my @date_ranges = ();
	push @date_ranges, $cgi->param('date_start') if $cgi->param('date_start');
	push @date_ranges, $cgi->param('date_end') if $cgi->param('date_end');
	@date_ranges = $db->selectrow_array('SELECT NOW()::DATE') unless @date_ranges;
	
	if (scalar @date_ranges == 2)
	{
		$qry .= "AND cb.stamp::DATE BETWEEN ? AND ?\n";
		push @qry_args, @date_ranges;
	}
	
	my $orderBy = $cgi->param('orderBy') || '';
	if ($orderBy eq 'cbid')
	{
		$qry .= "ORDER BY cb.id\n";
	}
	elsif ($orderBy eq 'stamp')
	{
		$qry .= "ORDER BY cb.stamp";
	}
	
	my $sth = $db->prepare($qry);
	$sth->execute(@qry_args);
	
	print $cgi->header('-type'=>'text/plain', '-cookies'=> \@cookies);
	while (my $row = $sth->fetchrow_hashref)
	{
		print "$row->{'alias'},";
		print "$row->{'last_changed'},";
		print "$row->{'firstname'},";
		print "$row->{'lastname'},";
		print "$row->{'cardnumber'}\n";
	}
}