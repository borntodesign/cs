#!/usr/bin/perl -w
###### member_ppp_rpt.cgi
###### 
###### Display a member's Personal Pay Point totals and referral fees.
###### The script is driven by a Member ID which is found in the login cookie.
###### 
###### Created: 10/13/2003	Karl Kohrt
###### 
###### Last modified: 10/25/13 Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw($LOGIN_URL);
use ADMIN_DB;
use GI;
use cs_admin;

###### our page templates
my $q = new CGI;
my $ME = $q->url;

my $db = ();
my $gs = G_S->new({'db'=>\$db, 'cgi'=>$q});
my $member_id = '';
my $alias = '';
my $member_info = '';
my $admin = '';
my $login_error = 0;
my $period_menu = '';
my %periods_hash = ('' => 'Select a Period');

# the key is the membertype
# the inner keys represent the "template" that will be used to create the data table and individual rows
my %subTMPL = (
	'v' => {
		'tbl' => _table_pp(),
		'row' => _row_pp()
	},
	'm' => {
		'tbl' => _table_nopp(),
		'row' => _row_nopp()
	}
);

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

# Report the transactions for the requested month if we have a good login/cookie.
Get_Periods();
Report_Period();

End();

################################
###### Subroutines start here.
################################

###### Create a combo box based upon the parmeters passed in.
sub Create_Combo{
	my ($name, $list, $labels, $def) = @_;
	return $q->popup_menu(
		'-name'		=> $name,
		'-values'	=> $list,
		'-labels'	=> $labels,
		'-default'	=> $def,
		'-onChange'	=> 'document.forms[0].submit()');
}

sub End
{
	$db->disconnect if $db;
	exit;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'),
		$q->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error', '-encoding'=>'utf-8'),
		$q->h3($_[0]),
		$q->end_html();
	End();
}

sub Fill_Row
{
	my $one_row = shift;
	my $data = shift;
 
	my $commission = $data->{'commission'} || '0.00';
	my $ppp = $data->{'ppp'} || '0.00';
	my $this_id = '';
	($this_id = $data->{'alias'}) =~ s/\D//g;

	if ( $this_id == $member_id )
	{
		$data->{'lastname'} = "<strong>$data->{'lastname'}</strong>";
		$data->{'firstname'} = "<strong>$data->{'firstname'}</strong>";
	}
	$one_row =~ s/%%LASTNAME%%/$data->{'lastname'}/;
	$one_row =~ s/%%FIRSTNAME%%/$data->{'firstname'}/;
	$one_row =~ s/%%ALIAS%%/$data->{'alias'}/;
	$one_row =~ s/%%PPP%%/$ppp/;
 	$one_row =~ s/%%COMMISSION%%/$commission/;

	# Return the new row.
	return $one_row;
}
	
###### Get the info stored in the cookie.
sub Get_Cookie
{
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		unless ( $alias = $member_id = $q->param('id') )
		{
			Err('Missing a required parameter');
		}
		
		$admin =  $q->param('admin') || '';
		 
		# Get the admin user and try logging into the DB
		my $gi = cs_admin->new({'cgi'=>$q});
		my $operator = $gi->authenticate();

		# Check for errors.
		exit unless $db = ADMIN_DB::DB_Connect( 'member_trans_rpt.cgi', $operator->{'username'}, $operator->{'password'} );
		my $qry = "SELECT id, spid, alias, firstname, lastname, emailaddress, membertype FROM members WHERE ";
		$qry .= $member_id =~ /\D/ ? 'alias=?' : 'id=?';
#warn "$qry";
		$member_info = $db->selectrow_hashref($qry, undef, $member_id);
		die "Unable to identify a membership matching: $member_id" unless $member_info;
		$alias = $member_info->{'alias'};
		$member_id = $member_info->{'id'};
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect( 'member_trans_rpt.cgi' );

		# Get the member Login ID and info hash.
		( $member_id, $member_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

		# If we didn't get a member ID, we'll check for a ppreport cookie.
		unless ( $member_id )
		{
			# Get the member ID and the alias.
			$member_id = $gs->PreProcess_Input($q->cookie('id')) || '';
			
			# we also need member information like we would get from a real login cookie
			# access to this report based on a simple ID cookie could be a serious breach of privacy
			# but the powers that be want it accessible without a full login, so...
			if ($member_id)
			{
				$member_info = $db->selectrow_hashref("
					SELECT id, spid, alias, firstname, lastname, emailaddress, membertype
					FROM 	members
					WHERE id = ?", undef, $member_id);
				$alias = $member_info->{'alias'} if $member_info;
			}
		}
		else
		{
			# Assign the alias found in the VIP cookie.
			$alias = $member_info->{'alias'};
		}					

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			RedirectLogin();
			End();
		}
	}
	$db->{'RaiseError'} = 1;
}

###### Get the reporting periods and build a drop-down menu.
sub Get_Periods	
{
	my $qry = '';
	my $sth = '';
	my $data = '';
	my @list = ('');

	$qry = "	SELECT period, open, close
			FROM 	periods
			WHERE --	period <= get_working_period(1,now())
				open <= NOW()
			AND 	type=1
			ORDER BY open DESC";
	$sth = $db->prepare($qry);
	$sth->execute();

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@list, $data->{'period'});
		$data->{close} ||= "current";
		$periods_hash{$data->{'period'}} = "$data->{'open'} to $data->{'close'}";
	}
	$sth->finish;

	$period_menu = qq#<span class="fb_r">Reporting Periods</span><br>\n#;
	$period_menu .= Create_Combo('period', \@list, \%periods_hash);
	$period_menu .= "&nbsp;" . $q->submit('-value'=>'Go');
}

###### Display the transaction report for the requested period.
sub Report_Period
{
	my $qry = '';
	my $sth = '';
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $comm_total = 0;
	my $ppp_total = 0;
	my $year = '';
	my $period = '';

	# Get the period that was passed or default to the current period.
	unless ( $period = $q->param('period') )
	{
		$qry = "
			SELECT period
			FROM periods
			WHERE period = get_working_period(1,now()) and type=1;";
		$sth = $db->prepare($qry);
		$sth->execute();
		$period  = $sth->fetchrow_array;
	}

	# Prepare a query to get detailed transactions for the period requested.
	$qry = "SELECT m.alias,
				m.firstname,
				m.lastname,
				SUM(ta.ppp) AS ppp,
				SUM(ta.commission) AS commission
				
			FROM ta_live ta
			JOIN transactions t
				ON ta.transaction_ref=t.trans_id
			JOIN members m
				ON m.id=t.id
				WHERE ta.period= ?
				AND ta.id = ?
				AND ta.sub_type IS NULL
				AND COALESCE(ta.ppp, ta.commission, 0) <> 0

			GROUP BY
					m.alias,
					m.firstname,
					m.lastname
			ORDER BY
					m.lastname,
					m.firstname,
					m.alias";

	$sth = $db->prepare($qry);

	# Get the member's transactions for the period specified.
	$sth->execute( $period, $member_id);

	my $report_row = $subTMPL{$member_info->{'membertype'}}->{'row'} || $subTMPL{'m'}->{'row'};
	
	while ( $data = $sth->fetchrow_hashref)
	{
		# Add the new row to the other rows.
		$report_body .= Fill_Row($report_row, $data);

		# Add the individual amounts to the aggregate totals.
		$ppp_total += $data->{'ppp'} || 0;
		$comm_total += $data->{'commission'} || 0;
	}		

	$sth->finish();

	my $tmpl = $subTMPL{$member_info->{'membertype'}}->{'tbl'} || $subTMPL{'m'}->{'tbl'};
	
	$tmpl =~ s/%%ME%%/$ME/g;
	$tmpl =~ s/%%MEMBER_ID%%/$alias/g;
	$tmpl =~ s/%%ADMIN%%/$admin/g;
	$tmpl =~ s/%%PERIOD_MENU%%/$period_menu/g;
	$tmpl =~ s/%%PERIOD%%/$periods_hash{$period}/g;
	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$ppp_total = sprintf("%.2f", $ppp_total);
	$comm_total = sprintf("%.2f", $comm_total);
	$tmpl =~ s/%%PPP_TOTAL%%/$ppp_total/;
	$tmpl =~ s/%%COMM_TOTAL%%/$comm_total/;

	my $GI = GI->new;

	my $title = (grep {$_ eq $member_info->{'membertype'} } ('m','tp')) ?
		"Referral Commissions Report - # $alias" :
		"Personal Pay Points/Commissions Report - # $alias";

	my $_tmpl = $GI->wrap({
		'content' => \$tmpl,
		'title' => $title,
		'link' => '<link href="/css/memberinfo.cgi.css" rel="stylesheet" type="text/css" />'
	});
	print $q->header('-expires'=>'now'), $_tmpl;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
	print $q->redirect("$LOGIN_URL?;destination=$me");
}

sub _row_nopp
{
	return <<'EOT';

<tr>
<td align="left" class="data">&nbsp;%%LASTNAME%%</td>
<td align="left" class="data">&nbsp;%%FIRSTNAME%%</td>
<td align="left" class="data">&nbsp;%%ALIAS%%</td>
<td align="right" class="data">%%COMMISSION%%&nbsp;</td>
</tr>

EOT
}

sub _row_pp
{
	return <<'EOT';

<tr>
<td align="left" class="data">&nbsp;%%LASTNAME%%</td>
<td align="left" class="data">&nbsp;%%FIRSTNAME%%</td>
<td align="left" class="data">&nbsp;%%ALIAS%%</td>
<td align="right" class="data">%%PPP%%&nbsp;</td>
<td align="right" class="data">%%COMMISSION%%&nbsp;</td>
</tr>

EOT
}

sub _table_nopp
{
	return <<'EOT';

<form action="%%ME%%">

<input type="hidden" name="admin" value="%%ADMIN%%">
<input type="hidden" name="id" value="%%MEMBER_ID%%">

<table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">

<tr><td align="center">
<table border="1" width="100%" cellpadding="8" cellspacing="0" bgcolor="#FFFFFF">

<tr><td align="center">
<table border="0" width="100%" bgcolor="#FFFFFF">

<tr><td valign="middle" class="heading">
Your Referral Commissions<br>
&nbsp;&nbsp;&nbsp;from&nbsp;%%PERIOD%%.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(#%%MEMBER_ID%%)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

<td align="left" valign="middle" class="heading">

%%PERIOD_MENU%%

</td></tr>
</table>
</td></tr>

<tr><td align="center">
<table border="1" width="100%" cellpadding="3" cellspacing="0" bgcolor="#ffffff">

<tr> 
<td align="center" class="heading">Last Name</td>
<td align="center" class="heading">First Name</td>
<td align="center" class="heading">ID Number</td>
<td align="center" class="heading">Referral Commissions</td>
</tr>

%%REPORT_BODY%%

<tr>
<td colspan="3" align="right" class="heading">
Totals:&nbsp;
</td>

<td align="right" class="heading">
$&nbsp;%%COMM_TOTAL%%&nbsp;
</td>
</tr>

</table>
</td></tr>

</table>
</td></tr>

</table>

</form>

EOT
}

sub _table_pp
{
	return <<'EOT';

<form action="%%ME%%">

<input type="hidden" name="admin" value="%%ADMIN%%">
<input type="hidden" name="id" value="%%MEMBER_ID%%">

<table align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<!-- tr><td>
<img src="/images/index_031803/index2_r1_c1.jpg">
</td></tr -->

<tr><td align="center">
<table border="1" width="100%" cellpadding="8" cellspacing="0" bgcolor="#FFFFFF">

<tr><td align="center">
<table border="0" width="100%" bgcolor="#FFFFFF">

<tr><td valign="middle" class="heading">
Your Personal Pay Points/Commissions<br>
&nbsp;&nbsp;&nbsp;from&nbsp;%%PERIOD%%.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(#%%MEMBER_ID%%)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

<td align="left" valign="middle" class="heading">

%%PERIOD_MENU%%

</td></tr>
</table>
</td></tr>

<tr><td align="center">
<table border="1" width="100%" cellpadding="3" cellspacing="0" bgcolor="#ffffff">

<tr> 
<td align="center" class="heading">Last Name</td>
<td align="center" class="heading">First Name</td>
<td align="center" class="heading">ID Number</td>
<td align="center" class="heading">Personal Pay Points</td>
<td align="center" class="heading">Personal Commissions</td>
</tr>

%%REPORT_BODY%%

<tr>
<td colspan="3" align="right" class="heading">
Totals:&nbsp;
</td>

<td align="right" class="heading">%%PPP_TOTAL%%&nbsp;
</td>

<td align="right" class="heading">
$&nbsp;%%COMM_TOTAL%%&nbsp;
</td>
</tr>

</table>
</td></tr>

</table>
</td></tr>

</table>

</form>

EOT
}

###### 10/15/03 Added access for the free members via the 'pa' cookie.
###### 10/17/03 Copied the $member_id into $alias when coming in via the 'admin' cookie.
###### 02/17/04 added a sort order to the get periods SQL
###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login. Added query to get alias value.
# revised the SQL for obtaining a list of periods
###### 07/01/10 added the GI wrapper did a tiny bit of cleanup
###### 07/07/10 moved the templates from cgi_objects into the script so that we could select 'em quick based on membertype
###### 08/03/10 further embellishments to Get_Cookie to get all the particular member details via a query
###### if they are accessing the report with a simple ID cookie
###### 10/25/13 moving back to clubshop.com... also cleaning up code a bit
