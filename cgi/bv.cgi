#!/usr/bin/perl -w
###### bv Browser View
###### render an HTML "mailing" in the browser
###### created: 11/09	Bill MacArthur
###### last modified: 

use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use G_S;
use DB_Connect;
use HtmlMailings;

my $q = new CGI;

# run in SSL mode only
unless ($q->https){
	my $url = $q->self_url;
	$url =~ s/^http/https/;
	print $q->redirect($url);
	exit;
}
my ($db, $member, %params, $oid) = ();

$db = DB_Connect('bv') || exit;
$db->{'RaiseError'} = 1;
my $HM = HtmlMailings->new({'db'=>$db, 'BrowserView'=>1});

# if we received path info, that would be our info packet
if (my $bvi = $q->path_info)
{
	$bvi =~ s#^/##;
	$oid = ();
	($params{'docid'}, $params{'lp'}, $oid, $params{'id'}) = $HM->decodeBrowserViewIdentity($bvi);
	Err('Invalid token') unless $params{'docid'} && $params{'lp'} && $oid && $params{'id'};
}
else {
	die "bvi token is required";
}
Err('A docid parameter is required') unless $params{'docid'} && $params{'docid'} !~ /\D/;
$HM->init({'docid'=>$params{'docid'}, 'lang_pref'=>($params{'lp'} || 'en')});

my $qry = "
	SELECT members.*, members.oid" .
	($HM->config()->{'sql'}->{'extra_cols'} || '') .
	" FROM members " .
	($HM->config()->{'sql'}->{'extra_tables'} || '') .
	" WHERE members.alias=? AND members.oid=? " .
	($HM->config()->{'sql'}->{'extra_where'} || '') .
	($HM->config()->{'sql'}->{'finish'} || '');

$member = $db->selectrow_hashref($qry, undef, $params{'id'}, $oid);
Err('Member lookup failed') unless $member;
	
unless ($q->param('text')){
	print $q->header(-charset=>'utf-8'), ${$HM->GenerateHtmlDocument({member=>$member}, $params{lp})};
} else {
	print $q->header(-type=>'text-plain', -charset=>'utf-8'), ${$HM->GenerateTextDocument({member=>$member}, $params{lp})};
}
End();

###### ######

sub End
{
	$db->disconnect;
	exit;
}

sub Err
{
	print $q->header, $q->start_html('Error'), $q->div($_[0]), $q->end_html;
	End();
}
