#!/usr/bin/perl -w
###### ap_funding.pl
######
###### A script that receives a transaction for pre-pay funding from Payza, formerly Alert Pay
######
###### this script started as a copy of mb_funding.pl written by Karl Kohrt
###### created: 02/25/09	Bill MacArthur
######
###### modified: 09/10/13	Bill MacArthur
	
use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use DB_Connect;

###### CONSTANTS
my @Inbound_IPs = ('72.52.13.101', '108.163.136.234', '108.163.136.235 ');

# what we will place in front of the transaction ID from Payza to ensure we don't find dupes on simply the number itself
my $FUNDING_TRANS_PREFIX = 'Payza trans ID: ';
my $FUNDING_TRANS_TYPE = 3005;
my $ap_securitycode = 'cI454ykoKuPXczHW';	# this should mirror the number setup in our account
my @numerics = qw/ap_totalamount vip_id/;
my $TRACKING = 1;		# Change to 0 for no logging.
my $PATH = "/home/httpd/cgi-logs";	# Where the logs are kept.
#my $ACCOUNTING_ADDRESS = 'acfspvr@dhs-club.com';
my $ACCOUNTING_ADDRESS = 'webmaster@dhs-club.com';
my @fieldnames = qw/
	ap_referencenumber
	ap_securitycode
	ap_currency
	ap_totalamount
	apc_1
	ap_status/;

###### GLOBALS
my $q = new CGI;
my $gs = new G_S;
my $cgiini = "ap_funding.cgi";
my %EMAIL = (
	'success'	=> '10505',	# these are the MB templates
	'fail'		=> '10531'		# revise if we start sending email
);
my $db = '';
my $mem_info = '';
my $errors = '';
my $xml = '';
my $content_xml = '';
my $params_hash= {};

################################
###### MAIN starts here.
################################

# Open the log files if logging is turned on above.
open (TRACK_LOG, ">>$PATH/payza_tracking.log") if $TRACKING;

my $remote_addr = $q->remote_addr;
unless (grep { $_ eq $remote_addr } @Inbound_IPs)
{
	print TRACK_LOG "Remote address, $remote_addr , does not match our accept list.";
	die "Remote address, $remote_addr , does not match our accept list.";
}

exit unless $db = DB_Connect('generic');
$db->{'RaiseError'} = 1;

# Load the params into a local hash.
Load_Params();

# Log the data sent to us by Alert Pay
Log(scalar(localtime) . " - ");
Log("ID#$params_hash->{'vip_id'}; " . $q->query_string);
print $q->header();

# Unless there are errors in the order, we'll try to place the order.
unless ( Check_Params() )
{
	# Get the member's info.
	$mem_info = $db->selectrow_hashref('SELECT * FROM members WHERE	id = ?', undef, $params_hash->{'vip_id'});

	# If we received anything but a "processed" status back, let the member know it.
#	if ( $params_hash->{status} != 2)
#	{
#		my $email = Member_Message($params_hash->{status});
#		Email_Someone($email);
#	}
	# If we placed the funds in the table, let the member know it.
	if ( $mem_info && (my $trans_num = Insert_Funds()) > 0 )
	{
	# forget about email
#		my $email = Member_Message($trans_num);
#		Email_Someone($email);
	}
	# Otherwise, let accounting know about the problem.
	else
	{
		my $email_message = '';
		unless ($mem_info)
		{
			$email_message = "Cannot find a Partner with ID: $params_hash->{'vip_id'}\n";
		}
		elsif ( $trans_num < 0 )
		{
			my $tmp_trans = -1*$trans_num;
			$email_message = qq!The Payza website submitted a duplicate of transaction #$tmp_trans. The duplicate was not added to any Club Account. Forward this on to IT:\n!;
		}
		else
		{
			$email_message = qq!This Payza funds transfer was completed, but was not credited to the member's Club Account due to a database error. Please credit it to the member's account via the admin interface as soon as possible:\n!;
		}
		$email_message .= qq!
		DHS ID:	$mem_info->{'alias'}
		Name: $mem_info->{'firstname'} $mem_info->{'lastname'}
		Amount:	$params_hash->{ap_totalamount}
		Ref. ID: $params_hash->{ap_referencenumber}!;
		my %email = (
			'to' 		=> $ACCOUNTING_ADDRESS,
			'bcc'	 	=> '',
			'subject' 	=> 'Error - Club Account Funding System - Alert Pay',
			'from' 		=> 'ap_funding.cgi<noreply@clubshop.com>',
			'message' 	=> $email_message);
		Email_Someone(\%email);

		# Log the insertion error.
		Log(" ----- ");
		Log("error= transaction insertion error; ID#$params_hash->{'vip_id'}; amount=$params_hash->{'ap_totalamount'}");
	}
}
# Otherwise, if this transaction's status show's processed on MB's end,
#  but not on ours, let accounting know about the problem.
#  All other status types are logged above, so we'll just skip them here.
elsif ( $params_hash->{'ap_status'} eq 'Success' )
{
	my $email_message = qq!This Alert Pay funds transfer was sent to us with a bad parameter(s), so it was not entered into the Club Account system. Contact IT for assistance:\n\n!;
	$email_message .= qq!$errors\n\nParameters:\n!;
	foreach (sort keys %{$params_hash} )
	{
		$email_message .= "\t$_= " . ($params_hash->{$_} || '') . "\n";
	}
	my %email = (
		'to' 		=> $ACCOUNTING_ADDRESS,
		'bcc'	 	=> '',
		'subject' 	=> 'Error - Club Account Funding System - Payza',
		'from' 		=> 'ap_funding.cgi<noreply@clubshop.com>',
		'message' 	=> $email_message);
	Email_Someone(\%email);

	# Log the insertion error.
	Log(" ----- ");
	Log("error= parameter format error; ID#$params_hash->{'vip_id'}; amount=$params_hash->{'ap_totalamount'}");
}

print "Transaction Acknowledged.";
close TRACK_LOG if $TRACKING;

$db->disconnect if $db;
exit;

################################
###### Subroutines start here.
################################

###### Check the parameters for errors.
sub Check_Params
{
	# Return immediately unless this is a processed transaction.
	$errors .= 'ap_status ne Success' if $params_hash->{'ap_status'} ne 'Success';
	$errors .= 'ap_test indicates TEST mode' if $params_hash->{'ap_test'};

	# Clear the fields of the standard garbage.
	foreach my $key (keys %{$params_hash})
	{
		$params_hash->{$key} = $gs->PreProcess_Input($params_hash->{$key});
		
		if ( (grep { m#$key# } @numerics) && ($params_hash->{$key} !~ m#^\d+\.?\d*$#) )
		{
			$errors .= "The $key field value ($params_hash->{$key}) must be numeric.\n";
		}
	}
	# Check required fields as necessary.
	foreach ( @fieldnames )
	{
		# All fields are required for this script.
		if ( ! defined $params_hash->{$_}
			|| $params_hash->{$_} eq ''
			|| $params_hash->{$_} eq 'blank')
		{
			$errors .= "Missing value for the field: $_\n";
		}
	}

	unless ( $errors )
	{
#		$md5_key = uc(md5_hex( $md5_key ));
#		my $ours = uc(md5_hex( $merchant_id .
# 					$params_hash->{transaction_id} .
# 					$md5_key .
# 					$params_hash->{mb_amount} .
# 					$params_hash->{mb_currency} .
# 					$params_hash->{status} ));
#print TRACK_LOG " ----- theirs= $params_hash->{md5sig}\n -----   ours= $ours\n";

		# Check the values for authenticity
		if ( $ap_securitycode ne $params_hash->{'ap_securitycode'} ) 
		{ 
			$errors .= "The security code data did not match.\n\n";
		}
		
		$errors .= "Currency of transaction is not USD\n" if $params_hash->{'ap_currency'} ne 'USD';
	}

	return $errors;
}

###### Generate e-mail to the appropriate party.
sub Email_Someone
{
	use MIME::Lite;
	my $email_hash = shift;

	# Build the message
	my $message = MIME::Lite->new(
		'From'     => $email_hash->{'from'},
		'To'       => $email_hash->{'to'},
		'Bcc'      => $email_hash->{'bcc'},
		'Subject'  => $email_hash->{'subject'},
		'Type'     => 'text/plain; charset="utf-8"',
		'Encoding' => '7bit',
		'Data'     => $email_hash->{'message'});
	# Send the message
	$message->send('smtp','mail.dhs-club.com');

	return 1;
}

###### Insert funds into their pre-pay account - soa table and return the transaction number.
sub Insert_Funds
{
	# Check to see if this transaction has already been entered.
 	my ($trans_num) = $db->selectrow_array('
 		SELECT trans_id 
		FROM soa 
		WHERE description = ?', undef, ($FUNDING_TRANS_PREFIX . $params_hash->{'ap_referencenumber'}));

	# Build the query if not a duplicate and we can get a transaction number from the soa table.
	if ($trans_num)
	{
		return -1 * $trans_num;	# A duplicate was found.
	}
	elsif ( $trans_num = $db->selectrow_array("SELECT nextval('soa_trans_id_seq')") )
	{
		my $qry = "INSERT INTO soa (
						trans_id,
						trans_type,
						id,
						amount,
						description,
						operator )
				VALUES ( $trans_num, $FUNDING_TRANS_TYPE, ?, ?, ?, ? )";
		my $sth = $db->prepare($qry);
		my $rv = $sth->execute(
						$params_hash->{'vip_id'},
						$params_hash->{'ap_totalamount'},
						($FUNDING_TRANS_PREFIX . $params_hash->{'ap_referencenumber'}),
						$cgiini);

		if ( $rv )
		{
			return $trans_num;
		}
		else 
		{
			# Log the authorization in case we have problems later.
			Log (" ----- ");
			Log("error= transaction insertion error; ID#$mem_info->{'id'}; amount=$params_hash->{'amount'}");

			return 0;
		}
	}
	else { return 0; }					# Couldn't get a transaction number.
}

###### Load the params into a local hash.
sub Load_Params
{
	# Get the submitted parameters.
	foreach ( $q->param() )
	{
		$params_hash->{$_} = $q->param($_) || '';
	}
	# rather than rewriting, we will create the vip_id field from a value we will really receive
	$params_hash->{'vip_id'} = $params_hash->{'apc_1'} || '';
}

sub Log
{
	return unless $TRACKING;
	print TRACK_LOG "$_[0]\n";
}

###### Create an email message for the member.
sub Member_Message
{
	use MailingUtils;
	my $trans_id = shift;
	my $to = '';
	my $from = '';
	my $cc = '';
	my $bcc = '';
	my $subject = '';
	my $mu = new MailingUtils;
	my $type = ($trans_id <= 1) ? 'fail' : 'success';

	# Get the base email template(s).
	my $pc = $mu->Get_Object($db, $EMAIL{$type});

	# Select the template based upon the language preference.
	my $MSG = $mu->Select_Translation($pc, $mem_info->{'language_pref'});
	unless ($MSG) { die "Couldn't load Email Template: $EMAIL{$type}\n"; }

	# remove any comment fields
	$MSG =~ s/(^|\n)(#.*)//g;
	# remove line feeds (these can be caused by windows pasting)
	$MSG =~ s/\r//g;

	# Blank out the new moneybooker form paragraph in the success email.
	$MSG =~ s/%%conditional-moneybookers-start.+?conditional-moneybookers-stop%%//sg if $type eq 'success';

	# Send a Bcc email if an address is defined above for testing/confirmation.
	$MSG =~ s/%%bcc_email%%/$bcc/;

	# The transaction data.
#	if ( $trans_id > 1 )	{ $MSG =~ s/%%trans_id%%/$trans_id/; }
#	else			{$MSG =~ s/%%trans_id%%/$REASONS{$trans_id}/; }
	$MSG =~ s/%%trans_amount%%/$params_hash->{amount}/;
	$MSG =~ s/%%trans_desc%%/Moneybookers Payment/;

	# These are our member's info.
	$MSG =~ s/%%to_alias%%/$mem_info->{'alias'}/g;
	$MSG =~ s/%%to_firstname%%/$mem_info->{'firstname'}/g;
	$MSG =~ s/%%to_lastname%%/$mem_info->{'lastname'}/g;
# This next line's email entry is for testing only.
#	$MSG =~ s/%%to_email%%/$ACCOUNTING_ADDRESS/g;
	$MSG =~ s/%%to_email%%/$mem_info->{'emailaddress'}/g;

	# extract our 'To' line from the template
	$MSG =~ s/(^|\n)(To:.*)//i;
	($to = $2) =~ s/^To:\s*//i;
	# extract our 'From'
	$MSG =~ s/(^|\n)(From:.*)//i;
	($from = $2) =~ s/^From:\s*//i;
	# extract our 'Subject'
	$MSG =~ s/(^|\n)(Subject:.*)//i;
	($subject = $2) =~ s/^Subject:\s*//i;
	# extract our 'Bcc'
	$MSG =~ s/(^|\n)(Bcc:.*)//i;
	($bcc = $2) =~ s/^Bcc:\s*//i if $2;
	# extract our 'Cc'
	$MSG =~ s/(^|\n)(Cc:.*)//i;
	($cc = $2) =~ s/^Cc:\s*//i if $2;

	# remove all remaining leading spaces and newlines
	$MSG =~ s/^(\s|\n)*//;

	if ( $to && $trans_id > 1 )
	{
		$bcc = $ACCOUNTING_ADDRESS;
	}
	# If no email address was found.
	elsif ( $trans_id > 1 )
	{
		$to = $ACCOUNTING_ADDRESS;
		$bcc = '';
	}
	else
	{
		$bcc = '';
	}
	# perform AOL link creation if we are mailing to an AOL address
	if ($to =~ m/\@aol\.com/){
		$MSG =~ s#(http\S*)#<a href="$1">$1</a>#g;
	}
	my %mail = (
		'to' 		=> $to,
		'bcc'	 	=> $bcc,
		'subject' 	=> $subject,
		'from' 		=> $from,
		'message' 	=> $MSG);
	return \%mail;
}

###### 01/03/06 Added the removal of the moneybookers conditional paragraph to the Member_Message.
###### 01/27/06 Change the RaiseError flag to 0 so that email will go to accounting with errors.
###### 01/30/06 Added handling of a duplicate transaction submission which stems from moneybookers.com
######		not receiving an "OK 200" result from the original transaction submission.
###### 02/27/06 Added a quick out in Check_Params and Main 
######		in the cases where the status returned from moneybookers is not processed (2).
# 02/26/07 changed the mail sending to use SMTP instead of directly piping through sendmail like Mime::Lite defaults to
# 08/20/07 changed the logging location from var/log
# 09/30/08 changed the DB connection from the admin module to the regular module
###### 09/10/13 added the range of IP addresses we expect to hear from... also some general code cleanup
