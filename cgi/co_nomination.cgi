#!/usr/bin/perl -w
###### co_nomination.cgi
######
###### A form from which members can submit Charitable Organization nominations.
######
###### created: 02/10/05	Karl Kohrt
######
###### modified: 07/24/05	Bill MacArthur
	
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
require XML::Simple;
require XML::local_utils;
require ParseMe;
use CGI::Carp qw(fatalsToBrowser);
use MailingUtils;
use Mail::Sendmail;
use Solicit;

###### CONSTANTS
our $NOM_AGE = '30 days';	# The age of a nomination before another can be submitted.
				#  A valid PostgreSQL Interval value.
our $TOP_EXEC_ROLE = 120;	# Marketing Director
our $UPLINE_ROLE = 100;	# Builder Team Coach
our $STAFF_EXEC = 12; 	#3127951;	# The default Top Exec in case no other is found.

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our $don_script = "/cgi/co_donate.cgi";
our $xs = new XML::Simple(NoAttr=>1);
our %TMPL = (	xsl		=> 10383,
		content	=> 10384,
		);
our %EMAIL = (co		=> 10387,
		upline		=> 10388
		);
our @field_names = qw/id firstname lastname co_name emailaddress url
			address1 address2 city state country postalcode
			phone fax stamp username password notes fein/;
# Create a string to match the array above for use in queries.
our $field_names_string = join ', ', @field_names;
our $db = '';
our $xml = '';
our $member_id = '';
our $co_id = $q->param('co_id') || '';
our $action = $q->param('action') || '';
our $mem_info = '';
our $nom_details = '';
our $alias = '';
our $membertype = '';
our $access_type = '';
our $operator = '';
our $login_error = 0;
our %co_info = ();
our $messages = '';

################################
###### MAIN starts here.
################################

# Get the id for a logged in member.
Get_Cookie();	

# If they are logged in with a password.
if ( $access_type eq 'auth'
		&& ( ! $action || $action eq 'nominate') )
{
	if ($mem_info->{membertype} ne 'v')
	{
		$messages .= qq#\n<span class="bad">
				<xsl:value-of select="//messages/vips_only" /></span><br />\n#;
	}
	else
	{
	# Unless the member has submitted a nomination less than $NOM_AGE old.
	unless ( Member_Has_Nomination() )
	{
		# If this is a nomination request.
		if ( $action && $action eq 'nominate' )
		{
			# Load the params into a local hash.
			Load_Params();

			# Unless the nomination already exists or is in the do_not_solicit list.
			unless ( CO_Exists() || Check_Do_Not_Solicit($co_info{emailaddress}) )
			{
				# If we created the CO record.
				if ( Create_Nomination() )
				{
					# Email the CO and/or the MD.
					Do_Mail('co');
					# Email the appropriate upline.
					Do_Mail('upline');
				}
			}
		}
	}
	}
}
# If they are updating a nomination.
elsif ( $action && 
		($action =~ m/^confirm/
		|| $action eq 'remove' ) )
{
	# Load the params into a local hash.
	Load_Params();
	if ( $action eq 'confirm' )
	{
		Get_Nomination();
	}
	elsif ( $action eq 'confirm1' || $action eq 'remove')
	{
		Update_Nomination();
		if ( $action eq 'remove' )
		{
			# Get the email address if one exists
			#  and add it to the Do Not Solicit list.
			if ( my $emailaddress = Get_Email() )
			{
				# Include the user's IP address in the record 
				#  and the operator if we have one..
				my $notes = "$operator: " if $operator;
				$notes .= $q->remote_addr . " via co_nomination.cgi removal.";

				# Add them to the Do Not Solicit list.
				#  At this time, I don't care if this succeeds or not,
				#  so $stamp is retrieved, but not used.
				#  FYI - $stamp values:
				#   0=success, -1=DB failure, other=date of existing entry.
				my $stamp = Solicit::Do_Not_Solicit($db, $emailaddress, $notes);
			}
		}
	}
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	$messages .= qq#\n	<span class="bad">
				<xsl:value-of select="//messages/message4" />
				<xsl:text> </xsl:text>
				<a href="$LOGIN_URL?destination=$ME">
				<xsl:value-of select="//messages/message5" />
				</a>
				</span><br/>\n#;
}

# Now build out the page.
Display_Page();

$db->disconnect if $db;
exit(0);

################################
###### Subroutines start here.
################################

sub cbo_Country
{
	require '/home/httpd/cgi-lib/CC.list';
	my $country = '';
	$country = $nom_details->{country} if $nom_details;
	return $q->popup_menu(	-name=>'country',
					-class=>'in',
					-values=>\@CC::COUNTRY_CODES,
					-default=>$country,
					-labels=>\%CC::COUNTRIES,
					-force=>'true',
					-id=>'country',
					-onChange=>'getStates()');
}

###### Check for the pre-existence of this nomination.
sub CO_Exists
{
	my $sth = $db->prepare(	"SELECT *
					FROM co_list_view
					WHERE 	lower(co_name) = ?
					AND 	lower(city) = ?
					AND 	lower(state) = ?
					AND 	lower(country) = ?
					LIMIT 1;");		
	my $rv = $sth->execute(	lc($co_info{co_name}),
					lc($co_info{city}),
					lc($co_info{state}),
					lc($co_info{country}));
	my $exists = '';
	$exists = $sth->fetchrow_hashref() if $rv;

	# The record already exists.
	if ( $exists )
	{
		# Let them know.
		$messages .= qq#\n	<span class="bad">
				<xsl:value-of select="//messages/message6" />
				</span><br/>\n#;
		# Assign the found info to the display variable.
		$nom_details = $exists;
		foreach ( @field_names ) { $nom_details->{$_} = $nom_details->{$_} || '' }
		return 1;
	}
	return 0;
}

###### Check for the email address in the do_not_solicit list.
sub Check_Do_Not_Solicit
{
	my $email = shift;
	my $sth = $db->prepare(	"SELECT *
					FROM do_not_solicit
					WHERE 	emailaddress = ?;");		
	my $rv = $sth->execute($email);
	my $exists = '';
	$exists = $sth->fetchrow_hashref() if $rv;

	# The email is on the list.
	if ( $exists )
	{
		# Let them know.
		$messages .= qq#\n	<span class="bad">
				<xsl:text>'</xsl:text>
				$email
				<xsl:text>' </xsl:text>
				<xsl:value-of select="//messages/no_solicit" />
				</span><br/>\n#;
		# Assign the found info to the display variable.
		$nom_details = \%co_info;
		foreach ( @field_names ) { $nom_details->{$_} = $nom_details->{$_} || '' }
		return 1;
	}
	return 0;
}

###### Create a nomination record.
sub Create_Nomination
{
	my $value_list = '';
	my @values = ();

	# Get the next ID in the sequence.
	my $next_id = $db->selectrow_array("SELECT nextval('co_info_id_seq');");

	if ( $next_id )
	{
		# Create a nomination record.
		my $qry .= "INSERT INTO co_info ( ";
		foreach ( @field_names )
		{
			unless ( $_ eq 'id' || $_ eq 'stamp' || $_ eq 'url')
			{
				$qry .= " $_,";
				$value_list .= " ?,";
				push @values, $co_info{$_};
			}
		}
		# Include the referral_id in the record.
		$qry .= " referral_id, id";
		$value_list .= " ?, ?";
		push @values, $member_id, $next_id;

		$qry .= " ) VALUES ( " . $value_list . " );";
#Err($qry); my $count=0; foreach ( @values ) {++$count;Err("<br>$count) $_<br>")}

		# Now add the co_list info part of the query.
		$qry .= "INSERT INTO co_list ( type, type_id, notes, url )
				VALUES ( 'co', $next_id, 'Initial nomination setup.', ? );";
		push @values, $co_info{url};
#		my $sth = $db->prepare($qry);
		# If we were able to create the nomination.
		if ( $db->do(	$qry, undef, @values ) )
		{					
			$messages .= qq#\n	<span class="good">
						<xsl:value-of select="//messages/message1" /><br/>
						<xsl:value-of select="//messages/pend_note" />
						</span><br/>\n#;
			# Add the info into the CO hash for the nomination accept link.
			$co_info{oid} = $db->selectrow_array("	SELECT oid
								FROM 	co_info
								WHERE 	id = $next_id;");
			$co_info{id} = $next_id;
		}
		# If no nomination was created.
		else
		{					
			$messages .= qq#\n	<span class="bad">
						<xsl:value-of select="//messages/message2" />
						</span><br/>\n#;
			return 0;
		}
	}
	# If no sequence ID could be retrieved.
	else
	{					
		$messages .= qq#\n	<span class="bad">
					<xsl:value-of select="//messages/message2" />
					</span><br/>\n#;
		return 0;
	}
	return 1;
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	my $xsl = $gs->Get_Object($db, $TMPL{xsl}) ||
		die "Failed to load XSL template: $TMPL{xsl}\n";

	# Establish the xml document.
	$xml = "<base>\n";

	# Put the CO info in if authorized to see it.
#	if ( $access_type eq 'auth' && $nom_details )
	# Put the CO info in if we have it.
	if ( $nom_details )
	{
		# utf-8ize our nomination info
		$gs->ConvHashref('utf8', $nom_details);
		$xml .= $xs->XMLout($nom_details, RootName=>'nomination');
	}
	# Add the user info for page personalization.
	if ( $mem_info )
	{
		my $user_xml = $xs->XMLout($mem_info, RootName=>'user') ;
		$xml .= $user_xml;
	}
	my $content = $gs->Get_Object($db, $TMPL{content}) ||
		die "Failed to load template: $TMPL{content}\n";

	# Combine the remaining pieces of the xml document.
	$xml 	.= $content . "\n</base>\n";

	if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
	else
	{
		if ( $messages )
		{
			$messages = qq#<tr><td colspan="2" align="center"><hr width="30%"/>
					$messages
					<hr width="30%"/></td></tr>#;
		}
		$xsl =~ s/%%messages%%/$messages/g;
		$xsl =~ s/%%ME%%/$ME/g;
		$xsl =~ s/%%don_script%%/$don_script/g;
		$xsl =~ s/%%LOGIN_URL%%/$LOGIN_URL/g;
		if ( $access_type eq 'auth'
			|| ($action eq 'confirm' && $nom_details) ) { $xsl =~ s/%%COUNTRIES%%/cbo_Country()/e }
		else { $xsl =~ s/conditional-form-start.*?conditional-form-end//sg }

		print $q->header(-charset=>'utf-8'), XML::local_utils::xslt($xsl, $xml);
	}
}

###### Send an email to the $mail_who party.
sub Do_Mail
{
	my $mail_who = shift;
	my $exec = {};
	my $upline = '';

# As of 08/09/05, all email require the exec info.
	# If this is going to the DHS Club members.
#	if ( $mail_who eq 'upline' )
#	{
		# At this point we have an ID, we can look upline
		#  and select the necessary parties to email.
		my $sth = $db->prepare("
			SELECT m.id, m.spid, m.firstname, m.lastname, m.emailaddress,
				m.alias, m.membertype,
				COALESCE(nbt.level, 0) AS role,
				COALESCE(a.a_level, 0) AS a_level
			FROM 	members m
			LEFT JOIN nbteam_vw nbt
			ON 	m.id=nbt.id
			LEFT JOIN a_level a
			ON 	a.id=m.id
			WHERE 	m.id= ?");
#		my $tmp = {spid => $mem_info->{spid}};
		my $tmp = {spid => $mem_info->{id}};
		while ($tmp->{spid} > 1)
		{
			$sth->execute($tmp->{spid});
			$tmp = $sth->fetchrow_hashref;

			if ($tmp->{role} >= $UPLINE_ROLE && $tmp->{membertype} eq 'v')
			{
				$gs->ConvHashref('utf8', $tmp);
				if ($tmp->{role} >= $TOP_EXEC_ROLE)
				{
					$exec = $tmp;
				}
				else
				{
					$exec = '';
					$upline .= "\"$tmp->{firstname} $tmp->{lastname}\" <$tmp->{emailaddress}>," unless $tmp->{id} == $mem_info->{id};
				}
			}
			last if $tmp->{role} >= $TOP_EXEC_ROLE;
		}
		# If we didn't find an exec to email, email the designated staff exec.
		unless ($exec && $exec->{emailaddress})
		{
#Err("no exec found yet. searching for $STAFF_EXEC now.");
			$sth->execute($STAFF_EXEC);
			$exec = $sth->fetchrow_hashref;
		}
		chop $upline;
		$gs->ConvHashref('utf8', $mem_info);
#Err("exec= $exec->{id}, $exec->{emailaddress}");
#	}

	my $tmpl = $gs->Get_Object($db, $EMAIL{$mail_who}) ||
		die "Failed to retrieve the email template #$EMAIL{'$mail_who'}\n";
	# If we don't have a CO email address, sub in the exec's info in the To: field.
	if ( ! $co_info{emailaddress} && $mail_who eq 'co' )
	{
		$tmpl =~ s/%%co:firstname%%/$exec->{firstname}/;
		$tmpl =~ s/%%co:lastname%%/$exec->{lastname}/;
		$tmpl =~ s/%%co:emailaddress%%/$exec->{emailaddress}/;
	}
	# Now sub in all the remaining fields.
	ParseMe::ParseAll(\$tmpl,{
		member	=> $mem_info,
		exec	=> $exec,
		co	=> \%co_info,
		upline	=> {list => $upline}});
	my $hdr = ParseMe::Grab_Headers(\$tmpl);
	MailingUtils::DrHeaders($hdr);
	my %mail = (
		'Content-Type'=> 'text/plain; charset="utf-8"',
		'Message'	=> $tmpl,
		%{$hdr}
	);

	sendmail(%mail) || die $Mail::Sendmail::error;
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $_[0];
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		$operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'co_nomination.cgi', $operator, $pwd ))
		{
			$login_error = 1;
		}
		# Mark this as an admin user.
		$access_type = 'admin';
	}
	# If this is a member.		 
	else
	{
		unless ($db = DB_Connect('co_nomination.cgi')) {exit}
		$db->{RaiseError} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id && $member_id !~ m/\D/ ) { $access_type = 'auth'; }
		elsif ( $co_id ) {  $access_type = 'co'; }
	}
}

###### Retrieve the pre-existing email address.
sub Get_Email
{
	my $oid = $q->param('oid');
	my $email = $db->selectrow_array(	"SELECT emailaddress 
						FROM 	co_info
						WHERE 	id = ?
						AND 	oid = ?;",
						undef, $co_id, $oid) || '';
	return $email;
}

###### Retrieve the pre-existing nomination.
sub Get_Nomination
{
	my $oid = $q->param('oid');
	# If we have the required query params. 
	if ( $oid && $co_id )
	{
		my $sth = $db->prepare(	"SELECT i.oid, i.confirm_ip, l.* 
						FROM 	co_info i
							INNER JOIN co_list_view l ON i.id = l.type_id
						WHERE 	l.type = 'co'
						AND 	l.type_id = ?
						AND 	i.oid = ?;");		
		my $rv = $sth->execute( $co_id, $oid );
		my $exists = '';
		$exists = $sth->fetchrow_hashref() if $rv;

		# If the record has already been confirmed.
		if ( $exists->{confirm_ip} )
		{
			$messages .= qq#\n	<span class="bad">
					<xsl:text>'</xsl:text>$exists->{co_name}<xsl:text>' </xsl:text>
					<xsl:value-of select="//messages/already_confirmed" />
					</span><br/>\n#;
			return 0;
		}
		# If we found a non-confirmed record.
		elsif ( $exists->{oid} )
		{
			# Display the found info and instructions to confirm.
			$messages .= qq#\n	<span class="good">
						<xsl:value-of select="//messages/nom_confirm" />
						</span><br/>\n#;
			# Assign the found info to the display variable.
			$nom_details = $exists;
			$nom_details->{action} = 'confirm1';
			foreach ( @field_names ) { $nom_details->{$_} = $nom_details->{$_} || '' }
			return 1;
		}
		# Let them know we couldn't find their nomination.
		else
		{
			$messages .= qq#\n	<span class="bad">
						<xsl:value-of select="//messages/not_found" />
						</span><br/>\n#;
			return 0;
		}
	}
	# Let them know we didn't have all the params to find their nomination.
	else
	{
		$messages .= qq#\n	<span class="bad">
					<xsl:value-of select="//messages/bad_link" />
					<br/><xsl:value-of select="//messages/not_found" />
					</span><br/>\n#;
		return 0;
	}
}

###### Load the params into a local hash.
sub Load_Params
{
	foreach ( $q->param() )
	{
		$co_info{$_} = $q->param($_) || '';
		$co_info{$_} =~ s/^http:\/\/// if $_ eq 'url';
	}
}

###### Check to see if the member has a nomination pending.
sub Member_Has_Nomination
{
	# Get the most recent nomination info for the member.
	$nom_details = $db->selectrow_hashref("
				SELECT *
				FROM co_list_view
				WHERE 	referral_id = $member_id
				AND 	stamp >= NOW() - INTERVAL '$NOM_AGE'
				AND 	active IS NULL
				ORDER BY stamp DESC
				LIMIT 1;");		
	# If they have a nomination less than $NOM_AGE old.
	if ( $nom_details->{id} )
	{
		foreach ( @field_names ) { $nom_details->{$_} = $nom_details->{$_} || '' }
		# Let them know.
		$messages .= qq#\n	<span class="bad">
					<xsl:value-of select="//messages/message3" /><br/>
					<xsl:value-of select="//messages/pend_note" />
					</span><br/>\n#;
		return 1;
	}
	return 0;
}

###### Update a nomination record.
sub Update_Nomination
{
	my $oid = $q->param('oid');
	my @values = ();

	if ( $oid )
	{
		# Update a CO nomination record.
		my $qry .= "UPDATE co_info SET ";

		# Unless they asked to be removed from the nomination list.
		unless ( $action eq 'remove' )
		{
			foreach ( @field_names )
			{
				unless ( $_ eq 'id' 
					|| $_ eq 'stamp'
					|| $_ eq 'url'
					|| $_ eq 'notes')
				{
					$qry .= " $_ = ?,";
					push @values, $co_info{$_} || '';
				}
			}
		}
		# Include the user's IP address in the record.
		$qry .= "	confirm_ip = ?,
				stamp = NOW(),
				notes = 'Update via co_nomination.cgi.'";
		push @values, $q->remote_addr;

		$qry .= " WHERE oid = ? AND id = ?;";
		push @values, $oid, $co_id;
		# This is the CO list table update query.
		$qry .= "UPDATE co_list SET ";
		if ( $action eq 'remove' )
		{
			$qry .= " active = 'f', ";
		}
		else
		{	
			$qry .= " url = ?, ";
			push @values, $co_info{url} || '';
		}
		$qry .= "	stamp = NOW(),
				notes = 'Update via co_nomination.cgi.'
				WHERE type = 'co'
				AND type_id = (	SELECT id 
							FROM co_list l 
								INNER JOIN co_info i 
									ON l.type_id = i.id 
							WHERE l.type = 'co'
							AND i.oid = ? 
							AND l.type_id = ?);";
		push @values, $oid, $co_id;
#Err($qry);
#		my $sth = $db->prepare($qry);
		# If we were able to update the nomination.
		if ( $db->do(	$qry, undef, @values ) )
		{					
			$messages .= qq#\n	<span class="good">#;
			if ( $action eq 'remove' )
			{
				$messages .= qq#<xsl:value-of select="//messages/removed" />#;
			}
			else
			{
				$messages .= qq#<xsl:value-of select="//messages/confirmed" />
						<br/><xsl:value-of select="//messages/confirmed2" />#;
			}
			$messages .= qq#</span><br/>\n#;
		}
		# If nomination was not updated.
		else
		{					
			$messages .= qq#\n	<span class="bad">
						<xsl:value-of select="//messages/not_confirmed" />
						</span><br/>\n#;
			return 0;
		}
	}
	# If no OID was submitted.
	else
	{					
		$messages .= qq#\n	<span class="bad">
					<xsl:value-of select="//messages/message2" />
					</span><br/>\n#;
		return 0;
	}
	return 1;
}


###### 06/09/05 Added error handling for those not logged in.
###### 08/11/05 Added online nomination confirmation.
###### 08/17/05 Added the website url to the code.
###### 08/19/05 Added the remove code and Do Not Solicit call.
###### 08/24/05 Added the Do Not Solicit check for new nominations.
###### 09/12/05 Added error handling for an incomplete confirmation link.
###### 02/27/06 rolled the multiple query execution into a do instead of an execute
# 07/24/06 made the application only usable by VIPs