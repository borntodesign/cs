#!/usr/bin/perl

=head1 NAME:
merchant_application.cgi

	This handles the merchant 

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Purpose of this Script:

	Verbose Description of the script

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB_Connect, G_S, MerchantAffiliates, XML::local_utils

=cut

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../cgi-lib";
use DHSGlobals;
use CGI;
use DB_Connect;
use G_S;
use MerchantAffiliates;
use XML::local_utils;
use XML::Simple;
use CreditCard;
use Members;
use MailTools;
use DHS::UrlSafe;
use Locations;
use Data::Dumper;

=head1
GLOBALS

=head2
CLASSES:

	Classes that are instanciated globally
=cut

#our $ObjectName = SomeClass->new();
our $db;
our $CGI = CGI->new();
our $DHS_UrlSafe = DHS::UrlSafe->new();

=head1
SUBROUTINES

=head2
exitScript

=head3
Description:

	This sub came around because Bill was/is using a "goto" statement!
	Basically it disconnects from the database, and exits the script.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub exitScript
{
	$db->disconnect if $db;
	exit;
}


sub displayNotEnoughCards
{
	my $xml = '';
	my $xsl = '';
	my $error_messages = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data'));
	my $errors_xml = XML::Simple::XMLout($error_messages, RootName => "errors" );
	
	$xml = G_S::Get_Object($db, 10680);
	$xsl = G_S::Get_Object($db, 10894);
	
	$xml =<<EOT;

	<base>
		<data>
			<step>1</step>
			$errors_xml
		</data>
		$xml
	</base>

EOT

	print $CGI->header(-charset=>'utf-8');	
	print XML::local_utils::xslt($xsl, $xml);
	exitScript();

}

=head2
displayApplication

=head3
Description:

	Display the application.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub displayApplication
{
	my $merchant_information = shift;
	my $errors = shift;
	my $step = shift;	
	
	$step = 1 if ! $step;
	
	my $xml = '';
	my $xsl = '';
	my $configuration_xml = '';
	
	my $data = '';
	my $errors_xml = '';
	my $step_xml = "<step>$step</step>";
	my $langauges_xml = '';
	my $states_xml = '';
	
	my $params = '';
	my $pricing_xml = '';
	my $cc_expiration_dates_xml = G_S::buildCreditCardExpirationDate();
	
	my $bank_information_xml = '';
	
	my $MerchantAffiliates = {};
	
	
#	use Data::Dumper;
#	warn "\n\nerrors: " . Dumper($errors) . "\n\n";
#	warn "\n\nstep: $step \n\n";
	
	eval{
		$MerchantAffiliates = MerchantAffiliates->new($db);
		$configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
		
		if (! $merchant_information)
		{
			if ($CGI->cookie('AuthCustom_Generic'))
			{
				my ( $id, $meminfo ) = G_S::authen_ses_key( $CGI->cookie('AuthCustom_Generic'));
				$merchant_information->{referral_id} = $id if $meminfo->{membertype} eq 'v';
			}
			elsif($CGI->cookie('refspid'))
			{
				$merchant_information->{referral_id} = $CGI->cookie('refspid');	
			}
					
		}


		
	};
	if($@)
	{
		
		my %error_email = 
								(
									from=>'errors@dhs-club.com',
									to=>'errors@dhs-club.com',
									subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
									text=>"There was an error instanciating the MerchantAffiliates Class, or retrieving the Merchant Config.
											$@
										  "
								);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%error_email);
		
	}
	
	
#	if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20091027)
#	{
		#TODO add the correct object IDs for Production.
		$xml = G_S::Get_Object($db, 10680);
		$xsl = G_S::Get_Object($db, 10894);
		$bank_information_xml = G_S::Get_Object($db, 10714);
#	} else {
#		$xml = G_S::Get_Object($db, 10675);
#		$xsl = G_S::Get_Object($db, 10676);
#		$bank_information_xml = G_S::Get_Object($db, 10706);
#		
#	}
	
	my $countries_xml = G_S::Get_Object($db, 10407);
	


	eval{
		if($merchant_information->{'country'} || $merchant_information->{'cc_country'})
		{
			
			my $Locations = Locations->new($db);
			
			if ($step == 1)
			{
				$states_xml .= $Locations->getXMLProvincesByCountryCode({country_code=>$merchant_information->{'country'}});
			}
			elsif($step == 2 && $merchant_information->{'cc_country'})
			{
				$states_xml .= $Locations->getXMLProvincesByCountryCode({country_code=>$merchant_information->{'cc_country'}});
				$states_xml .= $Locations->getXMLProvincesByCountryCode({country_code=>$merchant_information->{'cc_country'}, node_name=>'cc_state'});
			}
			
		}
	};
	
	if($@)
	{
		
		my $error_message = $@;
		
		#warn $error_message;
		
		my %error_email = 
								(
									from=>'errors@dhs-club.com',
									to=>'errors@dhs-club.com',
									subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
									text=>"There was an error retrieving, the state list.
											$error_message
										  "
								);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%error_email);
		
	}
	
	
	
	
	my $languages_ref = $db->selectall_arrayref("SELECT '<' || code2 || '>' ||  description || '</' || code2 || '>'  FROM language_codes WHERE active = true ORDER BY description");
	
	foreach (@$languages_ref)
	{
	   $langauges_xml .= $_->[0];
	}
	
	$langauges_xml = "<language>$langauges_xml</language>";
	
	if(! $step || $step == 1)
	{
		my $configuration = XML::Simple::XMLin("<base>$configuration_xml</base>");
	
		if($configuration->{areas_available_for_registration})
		{
			my $country_hash = XML::Simple::XMLin("$countries_xml");
			
			my $new_country_hash = {};
			
			foreach (keys %{$configuration->{areas_available_for_registration}})
			{
				$new_country_hash->{lc($_)} = $country_hash->{$_};
			}
			
			$countries_xml = XML::Simple::XMLout($new_country_hash, RootName => "country" );
			
		}
	
	}
	
	if ($errors)
	{
		$errors_xml = XML::Simple::XMLout($errors, RootName => "errors" )
	}

	if($merchant_information)
	{
		
		my $information_hashref_arrayref = {};
		
		#$merchant_information->{biz_description} = $db->selectrow_arrayref('SELECT yp_heading FROM ypcats2naics WHERE pk = ?', undef, ($merchant_information->{business_type})) if $merchant_information->{business_type};
		
		my $language_Preference = G_S::Get_LangPref();
		$merchant_information->{biz_description} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($language_Preference, $merchant_information->{business_type})) if $merchant_information->{business_type}; 
		
		foreach (keys %$merchant_information)
		{
			next if ! $_;
			$information_hashref_arrayref->{$_} = [$merchant_information->{$_}];
		}
		
		$params = XML::Simple::XMLout($information_hashref_arrayref, RootName => "params" )
	}
	
	if($step == 2)
	{
		eval{
			
			#my $exchange_rates = $db->selectrow_hashref("SELECT ern.code, CASE WHEN ern.code = 'EUR' THEN 1 ELSE ern.rate END AS rate FROM currency_by_country cbc	JOIN exchange_rates_now ern ON cbc.currency_code = ern.code WHERE cbc.country_code = '$merchant_information->{country}'");
			my $exchange_rates = $db->selectrow_hashref("SELECT ern.code, ern.rate FROM currency_by_country cbc	JOIN exchange_rates_now ern ON cbc.currency_code = ern.code WHERE cbc.country_code = '$merchant_information->{country}'");	
#			$exchange_rates->{code}
#			$exchange_rates->{rate}
			
			
			my $pricing = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");
			
			my $country_code_lower_case = lc($merchant_information->{country});
			
			
			
#			#If it is a regular price country convert the regular pricing
#			if(exists $pricing->{regular_price_countries}->{$merchant_information->{merchant_type}}->{$merchant_information->{$country_code_lower_case}})
#			{
#				
#				$pricing->{prices}->{converted}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{bronze} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{silver} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{regular}->{gold} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#			} else {
#				
#				$pricing->{prices}->{converted}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{bronze} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{silver} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#				$pricing->{prices}->{converted}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{$merchant_information->{merchant_type}}->{sale}->{gold} * $exchange_rates->{rate};
#				1 while $pricing->{prices}->{converted}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
#				
#			}
		
		
			#If it is a regular price country convert the regular pricing
			if(defined $country_code_lower_case && exists $merchant_information->{$country_code_lower_case} && exists $pricing->{regular_price_countries}->{offline}->{$merchant_information->{$country_code_lower_case}})
			{

						$pricing->{prices}->{converted}->{offline}->{free} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{regular}->{free} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{regular}->{bronze} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{regular}->{silver} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{regular}->{gold} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			} else {

						$pricing->{prices}->{converted}->{offline}->{free} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{sale}->{free} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{sale}->{bronze} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{sale}->{silver} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{sale}->{gold} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			}
			
			if(defined $country_code_lower_case && exists $merchant_information->{$country_code_lower_case} && exists $pricing->{regular_price_countries}->{offline_rewards}->{$merchant_information->{$country_code_lower_case}})
			{
						$pricing->{prices}->{converted}->{offline_rewards}->{free} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{free} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{basic} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{basic} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{bronze} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{silver} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{gold} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{platinum} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{regular}->{platinum} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			} else {
						$pricing->{prices}->{converted}->{offline_rewards}->{free} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{free} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{basic} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{basic} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
												
						$pricing->{prices}->{converted}->{offline_rewards}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{bronze} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{silver} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{gold} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
						$pricing->{prices}->{converted}->{offline_rewards}->{platinum} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{sale}->{platinum} * $exchange_rates->{rate};
						1 while $pricing->{prices}->{converted}->{offline_rewards}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			}
			
			$pricing->{prices}->{converted}->{offline}->{monthly} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline}->{monthly} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline}->{monthly} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{free} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{free} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{free} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{basic} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{basic} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{basic} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
						
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{bronze} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{bronze} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{bronze} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{silver} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{silver} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{silver} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{gold} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{gold} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{gold} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			$pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{platinum} = sprintf "%.2f $exchange_rates->{code}", $pricing->{prices}->{offline_rewards}->{monthly}->{platinum} * $exchange_rates->{rate};
			1 while $pricing->{prices}->{converted}->{offline_rewards}->{monthly}->{platinum} =~ s/^([−+]?\d+)(\d{3})/$1,$2/;
			
			
			$configuration_xml .= XML::Simple::XMLout($pricing->{prices}->{converted}, RootName => "converted", NoAttr => 1);
			
						
		};
		if($@)
		{
			#warn $@;
			
			my $error_message = $@;
			
			#warn $error_message;
			
			my %error_email = 
									(
										from=>'errors@dhs-club.com',
										to=>'errors@dhs-club.com',
										subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
										text=>"There was an error converrting the pricing for an applicants country.\n Error Message: \n $error_message ",
									);
			
			$error_email{text} .= "\n merchant_information: " . Dumper($merchant_information);
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%error_email);
		
		
		}

		
		
	}
	
	
	$xml = <<EOT;
	<base>
		
		<menus>
			$countries_xml
			
			$states_xml
			
			$configuration_xml
			
			$langauges_xml
			
			$cc_expiration_dates_xml
			
		</menus>
		
		<data>
			$step_xml
			$errors_xml
		</data>
		
		$params
		
		$xml
		
		$bank_information_xml
		
	</base>
	
EOT




#print $CGI->header(-type=>'text/xml', -charset=>'utf-8');
#print $xml . "\n\n";

#//data/step
#//params/payment_method
#//params/exception_percentage_of_sale
#//params/ec_number
#//params/ca_number

								
	
	#print $xsl . "\n\n";
	
	#print $params . "\n\n";

#	warn "\n\n $xml \n\n";

#	print $CGI->header(-type=>'text/plain', -charset=>'utf-8');
#	print $xml . "\n\n";

	print $CGI->header(-charset=>'utf-8');	
	print XML::local_utils::xslt($xsl, $xml);
	
	
}


=head2
processApplication

=head3
Description:

	Process the application.


=head3
Params:

	none

=head3
Returns:

	none

=cut
sub processApplication
{
	

	
	my %merchant_information = ();
	
	my %missing_fields = ();
	my $Members = {};
	my $MerchantAffiliates = {};
	my $RewardCards = {};

	my $configuration = {};
	my $number_of_cards_in_a_region_is_ok = 0;
	
	
	eval{
		
		$MerchantAffiliates = MerchantAffiliates->new($db);
		my $configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
		$configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");
		
		$RewardCards = RewardCards->new($db);
		
		
	};
	if($@)
	{
			my $error_message = $@;
			
			#warn $error_message;
			
			my %error_email = 
									(
										from=>'errors@dhs-club.com',
										to=>'errors@dhs-club.com',
										subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
										text=>"There was an error instanciating Classes, or retrieving the merchant config.\n Error Message: \n $error_message ",
									);
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%error_email);
	}
	
	
	
	my %required_fields = 
	(
			1	=>	[
					
					'business_name',
					'firstname1',
					'lastname1',
					'address1',
					'city',
					'state',
					'country',
					'phone',
					'language',
					'emailaddress1',
					#'emailaddress2',
					'username',
					'password',
			],
			2	=>	[
					'merchant_package',
					'business_type',
				]
	);
	
	if($CGI->param('_step') == 2 && $CGI->param('merchant_package') != 15)
	{
		
		push @{$required_fields{2}}, 'payment_method';
		
		if($CGI->param('payment_method') eq 'cc')
		{
			# If the payment method is Credit Card, add the required fields for credit card.
			push @{$required_fields{2}}, 'cc_type', 'cc_number', 'ccv_number', 'cc_expiration_date', 'cc_name_on_card', 'cc_address', 'cc_city', 'cc_country', 'cc_state';
		}
		elsif($CGI->param('payment_method') eq 'ca')
		{
			# If the payment method is Club Account, add the required fields for a Club Account.
			push @{$required_fields{2}}, 'ca_number';
		}
		elsif($CGI->param('payment_method') eq 'bd')
		{
			push @{$required_fields{2}}, 'pay_iban','pay_swift','pay_bank_name','pay_bank_city','pay_bank_address','pay_bank_state','pay_bank_country', 'pay_account_number';
		}
		
	}
	
	
	
	
	# Assign the information the merchant submitted to the "$merchant_information" hash.
	foreach my $key ($CGI->param())
	{
		next if $key =~ /submit|biz_description/i;
				
		$merchant_information{$key} = $CGI->param($key) if $CGI->param($key) && $key !~ /^_|^x$|^y$/;
		
#		$merchant_information{member_id} =~ s/\D//g if($key eq 'member_id' && $merchant_information{member_id});
#		$merchant_information{referral_id} =~ s/\D//g if($key eq 'referral_id' && $merchant_information{referral_id});
		
		$merchant_information{cc_expiration_date} =~ s/^\w//g if($key eq 'cc_expiration_date' && $merchant_information{cc_expiration_date});
		
		$merchant_information{$key} = $CGI->param($key) if $key =~ /^merchant_package$/;
		
	}
	
	
	
	
#	{
#		my $temp = \%merchant_information;
#		warn "\n merchant_information from CGI->param(): " . Dumper($temp) . "\n";
#	}
	
	if($CGI->param('_step') == 3)
	{
		displayApplication(\%merchant_information, undef, 3);	
		exitScript();
	}
	
	$merchant_information{'merchant_type'} = $MerchantAffiliates->getMerchantTypeNameById($merchant_information{'merchant_package'}) if(exists $merchant_information{'merchant_package'} && defined $merchant_information{'merchant_package'} && $merchant_information{'merchant_package'} =~ /^\d+$/ && $CGI->param('_step') == 2);
	
	#Check for missing fields.
	foreach my $key (@{$required_fields{$CGI->param('_step')}})
	{
		
		
		$missing_fields{$key} = ['Missing Field'] if(! defined $merchant_information{$key} || ! $merchant_information{$key});
		
		
	}
	
	#Check their discount options, only one is allowed
#	my $temp = \%merchant_information;
#	warn "\n merchant_information: \n" . Dumper($temp);
	
	
	if(exists $merchant_information{'merchant_type'} && $merchant_information{'merchant_type'} eq 'offline_rewards')
	{
		#Make sure they selected one.
		$missing_fields{'discount_option_is_required'} = ['Missing Field'] if(! $merchant_information{'special'} && ! $merchant_information{'percentage_off'} && ! $merchant_information{'flat_fee_off'});
		
		if (exists $merchant_information{'flat_fee_off'})
		{
			$merchant_information{'flat_fee_off'} =~ s/\ +$//;	
			$merchant_information{'flat_fee_off'} =~ s/^\ +//;	
			if($merchant_information{'flat_fee_off'} =~ /\D/)
			{
				$missing_fields{'flat_fee_off'} = ['Missing Field'] if (exists $merchant_information{'flat_fee_off'} && $merchant_information{'flat_fee_off'} =~ /\D/);
			}
		}
		
		if(! exists $missing_fields{'discount_option_is_required'} && ! exists $missing_fields{'flat_fee_off'} && $merchant_information{'percentage_off'})
		{
			my $package_name = $MerchantAffiliates->getMerchantPackageNameById($merchant_information{'merchant_package'});
			#$missing_fields{'appropriate_discount_option_for_package'} = ['Missing Field'] if ($merchant_information{'percentage_off'} < $configuration->{'min_percentage_off_per_package'}->{$merchant_information{'merchant_type'}}->{$package_name});
			$missing_fields{'discount_option_is_required'} = ['Missing Field'] if ($merchant_information{'percentage_off'} < $configuration->{'min_percentage_off_per_package'}->{$merchant_information{'merchant_type'}}->{$package_name});
		}
	
	}
	elsif(exists $merchant_information{'merchant_type'} && $merchant_information{'merchant_type'} eq 'offline')
	{
		$missing_fields{'discount_option_is_required'} = ['Missing Field'] if(!$merchant_information{'percentage_of_sale'} && !$merchant_information{'exception_percentage_of_sale'});
		$missing_fields{'only_one_discount_option_allowed'} = ['Missing Field'] if($merchant_information{'percentage_of_sale'} && $merchant_information{'exception_percentage_of_sale'});
	}
	
	
	delete($missing_fields{merchant_package}) if defined $merchant_information{merchant_package};
	
	
#	if($@)
#	{
#			warn "Error: $FindBin::RealBin / $FindBin::RealScript - There was an error instanciating the Members Class. " . $@;
#			$missing_fields{script_error} = ['Script Error'];
#	}
	
	
	#Look for missing, or misformatted information pertaining to the first step of the form.
	#TODO: After the data is validated, check the number of Reward Cards in their region, against the number of merchants (Merchants * 50 + 50 <= Reward Cards).
	if($CGI->param('_step') == 1)
	{
		eval{
	
			$Members = Members->new($db);
#			$MerchantAffiliates = MerchantAffiliates->new($db);
			
			my $member_id = '';
			my $referral_id = '';
			
			# If the applicant supplied a Member ID
			if($merchant_information{member_id})
			{
				$member_id = $merchant_information{member_id};
				$member_id =~ s/\D//g;
				$member_id =~ s/^0*//;
				
				$missing_fields{member_id_invalid} = ['Missing Field'] if ($member_id && ! $Members->getMemberInformationByMemberID({member_id=>$member_id, field=>'id'}));
							
				
			}
			
			if ($merchant_information{referral_id})
			{
				
				$referral_id = $merchant_information{referral_id};
				$referral_id =~ s/\D//g;
				$referral_id =~ s/^0*//;
				
				my $referal_member_type = $Members->getMemberInformationByMemberID({member_id=>$referral_id, field=>'membertype'});
			
				$missing_fields{referral_id_wrong_membertype} = ['Missing Field']
					if (! $referal_member_type || ! grep $_ eq $referal_member_type, ('v','amp'));
			}
			
			
			$missing_fields{member_id_referral_id} = ['Missing Field'] if($member_id && $referral_id);	
			
			if (! $missing_fields{emailaddress1})
			{
				
				$missing_fields{emailaddress1_preexisting} = ['Missing Field'] if (! $Members->checkEmailDomain($merchant_information{emailaddress1}));
#				$missing_fields{emailaddress2_preexisting} = ['Missing Field'] if ($merchant_information{emailaddress2} && !$merchant_information{member_id} && ! $Members->checkEmailDuplicates($merchant_information{emailaddress2}) && $Members->checkEmailDomain($merchant_information{emailaddress2}));
				
	 			$missing_fields{emailaddress1_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress1}));
#				$missing_fields{emailaddress2_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{emailaddress2}));
				
#				$missing_fields{emailaddress_should_not_match} = ['Missing Field'] if ($merchant_information{emailaddress1} && $merchant_information{emailaddress2} && $merchant_information{emailaddress1} eq $merchant_information{emailaddress2});
			}

			
#			if (! $merchant_information{exception_percentage_of_sale} && ! $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#			}
#			elsif($merchant_information{exception_percentage_of_sale} && $merchant_information{percentage_of_sale})
#			{
#				$missing_fields{exception_percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale} = ['Missing Field'];
#				$missing_fields{percentage_of_sale_not_both} = ['Missing Field'];
#			}
			
			if(! exists $missing_fields{username})
			{
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingUsernameDuplicate($merchant_information{username}));
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkMasterUsernameDuplicates($merchant_information{username}));
				$missing_fields{username_preexisting} = ['Missing Field'] if (! $MerchantAffiliates->checkLocationUsernameDuplicates($merchant_information{username}));
			}
			
			# If the referral ID is a house account we'll skip the Reward Card Requirment check.
			my $skip = 0;
			foreach (@{$configuration->{'house_accounts'}->{'house_account'}})
			{
	
				$skip = 1 if ($referral_id && $referral_id == $_);
			}
			
			$skip = 1 if ( exists $merchant_information{country} && $merchant_information{country} =~ /^IT$|^italy$|^italia$/i);
			
			#If there are no error messages for the state, country, refferal_id or member_id, and the member_id or refferal_id hold a value we should check for card available
			#If there is no ISO code for their region, or the applicant has Java Script dispabled the accuracy of the cards in an errea is questionable.
#TODO: Add an exclusion here for Italy.
			if (! $skip && ! defined $missing_fields{'state'} && ! defined $missing_fields{'country'} && ! defined $missing_fields{referral_id_wrong_membertype} && ! defined $missing_fields{member_id_invalid} && ($member_id || $referral_id))
			{
				#check the reward card numbers.
				$MerchantAffiliates->setRewardCards();
				my $reward_cards_in_a_province = $MerchantAffiliates->{'RewardCards'}->getRewardsCardInAProvince($merchant_information{'country'}, $merchant_information{'state'});
				
				my $merchants_in_a_province = $MerchantAffiliates->retrieveNumberOfMerchantsInAProvince($merchant_information{'country'}, $merchant_information{'state'});
				
				$merchants_in_a_province = 0 if ! $merchants_in_a_province;
				
				
				#warn "\n\n $merchants_in_a_province && $reward_cards_in_a_province < $merchants_in_a_province * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'} \n\n";
				
				if ($merchants_in_a_province && $reward_cards_in_a_province < $merchants_in_a_province * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'})
				{
					my $sponsor_id = 0;
					
					
					#The Member ID, and Refferal ID should have been checked, and sanitized by now.
					if($member_id)
					{
						my $member = $Members->getMemberInformationByMemberID({member_id=>$member_id});
						$sponsor_id = $member->{'spid'};
					}
					else
					{
						$sponsor_id = $referral_id
					}
					
					#TODO: Add a check for the sponsor_id, so this is skiped if the spid is 1,2, 5, 6, 7, 28 or 29.
					#This is probably the best spot for this.
					
					my $referal_member = $Members->getMemberInformationByMemberID({member_id=>$sponsor_id});
					
					$missing_fields{'not_enough_reward_cards'} = [$merchants_in_a_province * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'} - $reward_cards_in_a_province];
					$missing_fields{'not_enough_reward_cards_sponsors_name'} = ["$referal_member->{'firstname'} $referal_member->{'lastname'}"];
					$missing_fields{'not_enough_reward_cards_sponsors_email'} = [$referal_member->{'emailaddress'}];
					
					#set the notification
					my %notification_info = (
												id => $sponsor_id,
												notification_type => 105,
												spid => $missing_fields{'not_enough_reward_cards'}->[0],
												memo => $merchant_information{'business_name'}
											);
					
					$MerchantAffiliates->createNotification(\%notification_info);
					
					my $url = $CGI->url();
					$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%missing_fields);
					print $CGI->redirect(-uri=>"$url");
					exitScript();
					

				}
			}
		};
		if($@)
		{
			#warn "Error: $FindBin::RealBin / $FindBin::RealScript - There was an error in if(CGI->param('_step') == 1). Eror: " . $@;
			
			my $error_message = $@;
			
			my %error_email = 
									(
										from=>'errors@dhs-club.com',
										to=>'errors@dhs-club.com',
										subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
										text=>"There was an error validating the merchants information, or validating the number of cards in the merchants area.\n Error Message: \n $error_message \n",
									);
			my $temp = \%missing_fields;
			$error_email{text} .= "\n missing_fileds: \n" . Dumper($temp) . "\n";
			
			$temp = \%merchant_information;
			$error_email{text} .= "\n merchant_information: \n" . Dumper($temp) . "\n";
			
			my $MailTools = MailTools->new();
			$MailTools->sendTextEmail(\%error_email);
			
			$missing_fields{script_error} = ['Script Error'];
			
		}
	}
	
	
	
	#Look for missing, or misformatted information pertaining to the second step of the form.	
	if($CGI->param('_step') == 2)
	{
		

		
		#TODO: Add the checking here for valid Club Accounts, if they select that option. This is basically making sure that the Club Account number they supply is an active VIP		
		
		if(exists $merchant_information{payment_method} && $merchant_information{payment_method} eq 'cc')
		{
			#Check to make sure it is a real card number.
			#TODO: Look into a meaningfull error message for this one.
			$missing_fields{cc_number} = ['Missing Field'] if ($merchant_information{cc_number} && ! CreditCard::validate($merchant_information{cc_number}));
		}
		elsif(exists $merchant_information{payment_method} && $merchant_information{payment_method} eq 'ca')
		{
			
			eval{
				my $Members = Members->new($db);
				
				my $ca_number = $merchant_information{ca_number} ? $merchant_information{ca_number}: '';
				$ca_number =~ s/\D//g;
				$ca_number =~ s/^0*//;
				
				my $member_information = $Members->getMemberInformationByMemberID({member_id=>$ca_number});
				
				
				
#				use Data::Dumper;
#				print $CGI->header('text/plain');
#				print Dumper($member_information) . "\n\n";
#				print 'Member Match: ' . $member_information->{membertype} =~ /m|s|v/;
#				exitScript;

				#TODO Add a meaningful error message.				
				$missing_fields{ca_number} = ['Missing Field'] if($member_information && $member_information->{membertype} !~ /m|s|v/ ); # Removed on July 14th per Will || $member_information->{member_type} =~ /v/
				
			};
			if($@)
			{

#				print $CGI->header('text/plain');
#				print 'Error: ' . $@ . "\n";
#				exitScript();

				my $error_message = $@;
				
				my %error_email = 
										(
											from=>'errors@dhs-club.com',
											to=>'errors@dhs-club.com',
											subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
											text=>"There was an error validating the Club Account number for the Payment Method.\n Error Message: \n $error_message ",
										);
				
				my $temp = \%merchant_information;
				
				$error_email{text} .= Dumper($temp);
				
				my $MailTools = MailTools->new();
				$MailTools->sendTextEmail(\%error_email);

				$missing_fields{script_error} = ['Script Error'];
				displayApplication(\%merchant_information, \%missing_fields, $CGI->param('_step'));
				exitScript();
			}
			
		}
		elsif(exists $merchant_information{payment_method} && $merchant_information{payment_method} eq 'if')
		{
			#A Member ID for the International Funds Transfer is 
			#not required, so we skip the checks if one was not specified.
			if($merchant_information{if_number})
			{
				eval{
					my $Members = Members->new($db);
					
					my $if_number = $merchant_information{if_number};
					$if_number =~ s/\D//g;
					$if_number =~ s/^0*//;
					
					my $member_information = $Members->getMemberInformationByMemberID({member_id=>$if_number});
					
					$missing_fields{if_number} = ['Missing Field'] if($member_information->{membertype} !~ /m|s|v/)
					
				};
				if($@)
				{
					
					
					my $error_message = $@;
					
					my %error_email = 
											(
												from=>'errors@dhs-club.com',
												to=>'errors@dhs-club.com',
												subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
												text=>"There was an error validating the member type for an International Funds Trasfer payment option.\n Error Message: \n $error_message ",
											);
					
					my $temp = \%merchant_information;
					
					$error_email{text} .= Dumper($temp);
					
					my $MailTools = MailTools->new();
					$MailTools->sendTextEmail(\%error_email);
					
					$missing_fields{script_error} = ['Script Error'];
					displayApplication(\%merchant_information, \%missing_fields, $CGI->param('_step'));
					exitScript();
				}
			}
		}
#		elsif($merchant_information{payment_method} eq 'ec')
#		{
			#TODO: Decide what to do with Eco Card Validation	
			#TODO Add a meaningful error message.
#			$missing_fields{ec_number} = ['Missing Field'] 
#		}
		
	}
	
	
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
		displayApplication(\%merchant_information, \%missing_fields, $CGI->param('_step'));
		exitScript();
	}
	
		
	if($CGI->param('_step') == 2)
	{
		
		
		my $credit_new_members_club_account = [];
		
		
		if($merchant_information{merchant_package} == 15)
		{
			
			delete $merchant_information{'cc_type'} if exists $merchant_information{'cc_type'};
			delete $merchant_information{'cc_number'} if exists $merchant_information{'cc_number'};
			delete $merchant_information{'ccv_number'} if exists $merchant_information{'ccv_number'};
			delete $merchant_information{'cc_expiration_date'} if exists $merchant_information{'cc_expiration_date'};
			delete $merchant_information{'cc_name_on_card'} if exists $merchant_information{'cc_name_on_card'};
			delete $merchant_information{'cc_address'} if exists $merchant_information{'cc_name_on_card'};
			delete $merchant_information{'cc_city'} if exists $merchant_information{'cc_name_on_card'};
			delete $merchant_information{'cc_country'} if exists $merchant_information{'cc_country'};
			delete $merchant_information{'cc_state'} if exists $merchant_information{'cc_country'};
			
			delete $merchant_information{'ca_number'} if exists $merchant_information{'ca_number'};
			
			delete $merchant_information{'pay_iban'} if exists $merchant_information{'pay_iban'};
			delete $merchant_information{'pay_swift'} if exists $merchant_information{'pay_swift'};
			delete $merchant_information{'pay_bank_name'} if exists $merchant_information{'pay_swift'};
			delete $merchant_information{'pay_bank_city'} if exists $merchant_information{'pay_bank_city'};
			delete $merchant_information{'pay_bank_address'} if exists $merchant_information{'pay_bank_city'};
			delete $merchant_information{'pay_bank_state'} if exists $merchant_information{'pay_bank_city'};
			delete $merchant_information{'pay_bank_country'} if exists $merchant_information{'pay_bank_city'};
			delete $merchant_information{'pay_account_number'} if exists $merchant_information{'pay_account_number'};
			
			delete $merchant_information{'pay_payment_method'} if exists $merchant_information{'pay_payment_method'};
			
			########################################################################
			# If the member made it this far, all the required fields check out.
			# Now we will see about making them a merchant, or putting
			# them in the "merchant_pending" table.
			########################################################################
			#Create the Merchant Member, the Merchant Master record, the Merchant Location record, and the Shopping Member
			
				#sanitize some data
				if($merchant_information{member_id})
				{
					$merchant_information{member_id} =~ s/\D//g;
					$merchant_information{member_id} =~ s/^0*//;
				}
				
				if($merchant_information{referral_id})
				{
					$merchant_information{referral_id} =~ s/\D//g;
					$merchant_information{referral_id} =~ s/^0*//;
				}
				
				# This shouldn't be needed, but it won't hurt.
				if($merchant_information{ca_number})
				{
					$merchant_information{ca_number} =~ s/\D//g;
					$merchant_information{ca_number} =~ s/^0*//;
				}
				
				$merchant_information{status} = 2;
				
				eval{
					
					$MerchantAffiliates = MerchantAffiliates->new($db);
					
					
					#my $temp = \%merchant_information;
					#warn "\nregistration.cgi - the information passed into MerchantAffiliates->createMerchant \n merchant_information: " . Dumper($temp);
					#if($merchant_information{'merchant_type'} eq 'offline_rewards')
					if (! exists $merchant_information{'special'} || ! $merchant_information{'special'})
					{
						if (exists $merchant_information{'flat_fee_off'} || exists $merchant_information{'percentage_off'})
						{
							$merchant_information{'flat_fee_off'} =~ s/\D//g if (exists $merchant_information{'flat_fee_off'} && $merchant_information{'flat_fee_off'} =~ /\D/);
							
							$merchant_information{'special'} = $merchant_information{'flat_fee_off'} ? $merchant_information{'flat_fee_off'} : $merchant_information{'percentage_off'};							
						}
					}
					
#					my $temp = \%merchant_information;
#					warn "\n\n merchant_information: " . Dumper($temp) . "\n\n";
					
					$merchant_information{merchant_master_id} = $MerchantAffiliates->createMerchant(\%merchant_information, $credit_new_members_club_account);
					
				};
				if($@)
				{
					
					my $error_message = $FindBin::RealScript . ' : ' . $@;
					
					#warn $error_message;
					
					my $temp_merchant_information = \%merchant_information;
					
					$error_message .= "\n\n merchant_information: \n" . Dumper($temp_merchant_information);
					
					my %email = 
					(
						from=>'errors@dhs-club.com',
						to=>'errors@dhs-club.com',
						subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript Creating a merchant payment by Credit Card",
						text=>$error_message
					);
					
					
					my $MailTools = MailTools->new();
					$MailTools->sendTextEmail(\%email);
					
					displayApplication(\%merchant_information, {script_error => ['Script Error']}, $CGI->param('_step'));
					exitScript();
				}
			
			
		}
		elsif($merchant_information{payment_method} eq 'cc')
		{
			#For Charging the Credit Card, and Verification the card is valid. 
			require 'ccauth.lib';
			
			
			my %cc_parameters = ();
			
			$cc_parameters{cc_number} = $merchant_information{cc_number};			
			$cc_parameters{card_code} = $merchant_information{ccv_number};
			$cc_parameters{cc_expiration} = $merchant_information{cc_expiration_date};
			$cc_parameters{cc_expiration} =~ s/^d//;
			
			$cc_parameters{account_holder_lastname} = $merchant_information{cc_name_on_card};
			$cc_parameters{account_holder_address} = $merchant_information{cc_address};
			$cc_parameters{account_holder_city} = $merchant_information{cc_city};
			$cc_parameters{account_holder_state} = $merchant_information{cc_state};
			$cc_parameters{account_holder_zip} = $merchant_information{cc_postalcode} if $merchant_information{cc_postalcode};
			$cc_parameters{account_holder_country} = $merchant_information{cc_country};
			
			$cc_parameters{payment_method} = 'CC';
			$cc_parameters{invoice_number} = 'MA' . time;
			$cc_parameters{id} = 'MARegistration';
			
			
			my $returned_auth_info = '';
			
			eval{
				my $number_of_merchants_in_region = $MerchantAffiliates->retrieveNumberOfMerchantsInAProvince($merchant_information{country}, $merchant_information{state});
				my $number_of_cards_in_a_province = $RewardCards->getRewardsCardInAProvince($merchant_information{country}, $merchant_information{state});
				$number_of_cards_in_a_region_is_ok = ($number_of_cards_in_a_province >= $number_of_merchants_in_region * $configuration->{'reward_cards'}->{'cards_to_merchants'} + $configuration->{'reward_cards'}->{'cards_to_merchants'}) ? 1:0;
				
				# This is a hack, but it is needed for Italy.
				# The number of cards to merchant ratio does not matter any longer for Italy.
				$number_of_cards_in_a_region_is_ok = 1 if ( exists $merchant_information{country} && $merchant_information{country} =~ /^IT$|^italy$|^italia$/i);
				
			};
			if($@)
			{
				my $error_message = $FindBin::RealScript . ' : ' . $@;
					
				#warn $error_message;
					
				my %email = 
				(
					from=>'errors@dhs-club.com',
					to=>'errors@dhs-club.com',
					subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript Creating a merchant payment by Credit Card",
					text=>$error_message
				);
				
				
				my $MailTools = MailTools->new();
				$MailTools->sendTextEmail(\%email);
				
				displayApplication(\%merchant_information, {script_error => ['Script Error']}, $CGI->param('_step'));
				exitScript();
					
			}
			
			
			#There should be no need to check if a free package has also selected the exception percentage of sale, that is handled on the front end.
			
			if($merchant_information{merchant_package} != 0)
			{
				
				my $configuration_xml = '';
				
				$configuration_xml = G_S::Get_Object($db, 10681);
				
				
				my $pricing = XML::Simple::XMLin("<base>$configuration_xml</base>");
				
				#Charge the card the appropriate amount.
				#TODO: Add the correct Charge.
				
				my $country = lc($merchant_information{country});
				
				#TODO: This bit should be reworked.  
				#	   We shouldn't need to rely on some of this logic to grab 
				#	   the current pricing for these people, it should be more dynamic
				#	   based on the config file.
				
				my $pricing_type = $pricing->{regular_price_countries}->{$merchant_information{merchant_type}}->{$country} ? 'regular':'sale';
				
#				warn "MerchantAffiliates->getMerchantPackageNameById(merchant_information{merchant_package}: $merchant_information{merchant_package}) \n";
				
				my $package_price_label = $merchant_information{merchant_package} ? $MerchantAffiliates->getMerchantPackageNameById($merchant_information{merchant_package}): '';
				
#				warn "\n\nmerchant_information{merchant_type}: $merchant_information{merchant_type}";
#				warn "pricing_type: $pricing_type";
#				warn "package_price_label: $package_price_label";				
#				warn '$pricing->{prices}->{$merchant_information{merchant_type}}->{$pricing_type}->{$package_price_label}';
#				warn "pricing: " . Dumper($pricing->{prices});
#				warn "\n\n";
				
				
				$cc_parameters{amount} = $pricing->{prices}->{$merchant_information{merchant_type}}->{$pricing_type}->{$package_price_label};
				$cc_parameters{order_description} = 'Merchant Registration Credit Card Charge ';
				
# I'm leaving this here for debugging in the future.
#				use Data::Dumper;
#				print $CGI->header('text/plain');
#				print Dumper(%cc_parameters) . "\n\n";
#				exitScript();
				
				$returned_auth_info = CCAuth::Authorize(\%cc_parameters, {});
				
				if($returned_auth_info eq '1')
				{
					
					$credit_new_members_club_account->[0]->{amount} = $cc_parameters{amount};
					$credit_new_members_club_account->[0]->{description} = 'CC Funding for a Merchant Package';
					$credit_new_members_club_account->[0]->{transaction_type} = 7000;
					$credit_new_members_club_account->[0]->{operator} = $CGI->cookie('operator') ? $CGI->cookie('operator') : 'registration2.cgi' ;
					
				}
				
			} else {
				
				$cc_parameters{amount} = 1.50;
				$cc_parameters{order_description} = 'Merchant Registration Credit Card Ping ';
				
				$returned_auth_info = CCAuth::Authorize(\%cc_parameters, {auth_type=>'AUTH_ONLY'});
				
			}
			
			
			if ($returned_auth_info ne '1')
			{
				
				#warn "/cgi/registration.cgi - Error Message: $returned_auth_info";
				
				my $error_code = $returned_auth_info;
				
				$error_code =~ s/^.*\s(\d+):.*/$1/gi;
				
				$error_code =~ s/\s*//gi;
				
				$returned_auth_info =~ s/^\d\s*|\d+:\s*|\<\/*b\>\s*|\<br\s\/\>//gi;
				
				$missing_fields{"cc_error_$error_code"} = [$returned_auth_info];
				$missing_fields{cc_error} = [$returned_auth_info];
				
				displayApplication(\%merchant_information, \%missing_fields, $CGI->param('_step'));
				exitScript();
				
			} 
			
			
			########################################################################
			# If the member made it this far, all the required fields check out.
			# Now we will see about making them a merchant, or putting
			# them in the "merchant_pending" table.
			########################################################################
			#Create the Merchant Member, the Merchant Master record, the Merchant Location record, and the Shopping Member
			
				#sanitize some data
				if($merchant_information{member_id})
				{
					$merchant_information{member_id} =~ s/\D//g;
					$merchant_information{member_id} =~ s/^0*//;
				}
				
				if($merchant_information{referral_id})
				{
					$merchant_information{referral_id} =~ s/\D//g;
					$merchant_information{referral_id} =~ s/^0*//;
				}
				
				# This shouldn't be needed, but it won't hurt.
				if($merchant_information{ca_number})
				{
					$merchant_information{ca_number} =~ s/\D//g;
					$merchant_information{ca_number} =~ s/^0*//;
				}
				
				$merchant_information{status} = 2;
				
				eval{
					
					$MerchantAffiliates = MerchantAffiliates->new($db);
					
					
					#my $temp = \%merchant_information;
					#warn "\nregistration.cgi - the information passed into MerchantAffiliates->createMerchant \n merchant_information: " . Dumper($temp);
					#if($merchant_information{'merchant_type'} eq 'offline_rewards')
					if (! exists $merchant_information{'special'} || ! $merchant_information{'special'})
					{
						if (exists $merchant_information{'flat_fee_off'} || exists $merchant_information{'percentage_off'})
						{
							$merchant_information{'flat_fee_off'} =~ s/\D//g if (exists $merchant_information{'flat_fee_off'} && $merchant_information{'flat_fee_off'} =~ /\D/);
							
							$merchant_information{'special'} = $merchant_information{'flat_fee_off'} ? $merchant_information{'flat_fee_off'} : $merchant_information{'percentage_off'};							
						}
					}
					
#					my $temp = \%merchant_information;
#					warn "\n\n merchant_information: " . Dumper($temp) . "\n\n";
					
					$merchant_information{merchant_master_id} = $MerchantAffiliates->createMerchant(\%merchant_information, $credit_new_members_club_account);
					
				};
				if($@)
				{
					
					my $error_message = $FindBin::RealScript . ' : ' . $@;
					
					#warn $error_message;
					
					my $temp_merchant_information = \%merchant_information;
					
					$error_message .= "\n\n returned_auth_info: $returned_auth_info \n\n merchant_information: \n" . Dumper($temp_merchant_information);
					
					
					my %email = 
					(
						from=>'errors@dhs-club.com',
						to=>'errors@dhs-club.com',
						subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript Creating a merchant payment by Credit Card",
						text=>$error_message
					);
					
					
					my $MailTools = MailTools->new();
					$MailTools->sendTextEmail(\%email);
					
					displayApplication(\%merchant_information, {script_error => ['Script Error']}, $CGI->param('_step'));
					exitScript();
				}

			
#			my $step = $CGI->param('_step') + 1;
#			$merchant_information{'need_reward_cards'} = 1 if ! $number_of_cards_in_a_region_is_ok;
#			displayApplication(\%merchant_information, undef, $step);	
#			exitScript();
			
			
		}
		else
		{
			#All should be good by now, if it's not a valid club account that would have been taken care of by now, just save the data in the Pending table.
			
			eval{
				
				#$MerchantAffiliates = MerchantAffiliates->new($db);
				
				
				 
				 my %pending_merchant_info = ();
				
				$pending_merchant_info{member_id} = $merchant_information{member_id};
				$pending_merchant_info{referral_id} = $merchant_information{referral_id};
				$pending_merchant_info{business_name} = $merchant_information{business_name};
				$pending_merchant_info{tin} = $merchant_information{tin};
				$pending_merchant_info{firstname1} = $merchant_information{firstname1};
				$pending_merchant_info{lastname1} = $merchant_information{lastname1};
				$pending_merchant_info{address1} = $merchant_information{address1};
				$pending_merchant_info{city} = $merchant_information{city};
				$pending_merchant_info{state} = $merchant_information{state};
				$pending_merchant_info{country} = $merchant_information{country};
				$pending_merchant_info{postalcode} = $merchant_information{postalcode};
				$pending_merchant_info{phone} = $merchant_information{home_phone};
				$pending_merchant_info{cell_phone} = $merchant_information{scanner_number};
				$pending_merchant_info{fax} = $merchant_information{fax};
				
				$pending_merchant_info{language_pref} = $merchant_information{language};
				$pending_merchant_info{cc_type} = $merchant_information{cc_type};
				$pending_merchant_info{business_phone} = $merchant_information{phone};
				
				$pending_merchant_info{payment_method} = $merchant_information{payment_method};
				$pending_merchant_info{merchant_package} = $merchant_information{merchant_package};
				
				$pending_merchant_info{emailaddress1} = $merchant_information{emailaddress1};
#				$pending_merchant_info{emailaddress2} = $merchant_information{emailaddress2};
				$pending_merchant_info{username} = $merchant_information{username};
				$pending_merchant_info{password} = $merchant_information{password};
				$pending_merchant_info{url} = $merchant_information{url};
				$pending_merchant_info{website_language_pref} = $merchant_information{website_language};
				$pending_merchant_info{business_type} = $merchant_information{business_type};
				
				$pending_merchant_info{percentage_of_sale} = $merchant_information{percentage_of_sale};
  				$pending_merchant_info{exception_percentage_of_sale} = $merchant_information{exception_percentage_of_sale};
  				
  				#Special is for merchants who pay a flat monthly fee.
  				$pending_merchant_info{special} = $merchant_information{special};
  				$pending_merchant_info{flat_fee_off} = $merchant_information{flat_fee_off};
  				$pending_merchant_info{percentage_off} = $merchant_information{percentage_off};
  				
  				$pending_merchant_info{discount_blurb} = $merchant_information{discount_blurb};
  				
  				$pending_merchant_info{pay_name} = $merchant_information{cc_name_on_card};
  				$pending_merchant_info{pay_address} = $merchant_information{cc_address};
  				$pending_merchant_info{pay_city} = $merchant_information{cc_city};
  				$pending_merchant_info{pay_state} = $merchant_information{cc_state};
  				$pending_merchant_info{pay_country} = $merchant_information{cc_country};
  				$pending_merchant_info{pay_postalcode} = $merchant_information{cc_postalcode};
  				$pending_merchant_info{pay_cardnumber} = $merchant_information{cc_number};
  				$pending_merchant_info{pay_cardcode} = $merchant_information{ccv_number};
  				$pending_merchant_info{pay_cardexp} = $merchant_information{cc_expiration_date};
  				$pending_merchant_info{eco_card_number} = $merchant_information{ec_number};
  				$pending_merchant_info{ca_number} = $merchant_information{ca_number};
  				$pending_merchant_info{if_number} = $merchant_information{if_number};
  				
  				##################
  				# Bank Draft
  				##################
#  				$pending_merchant_info{pay_bank_name} = $merchant_information{pay_bank_name};
#				$pending_merchant_info{pay_swift} = $merchant_information{pay_swift};
#				$pending_merchant_info{pay_iban} = $merchant_information{pay_iban};
#				$pending_merchant_info{pay_account_number} = $merchant_information{pay_account_number};
#				$pending_merchant_info{pay_routing_number} = $merchant_information{pay_routing_number};
#				$pending_merchant_info{pay_bank_address} = $merchant_information{pay_bank_address};
#				$pending_merchant_info{pay_bank_city} = $merchant_information{pay_bank_city};
#				$pending_merchant_info{pay_bank_state} = $merchant_information{pay_bank_state};
#				$pending_merchant_info{pay_bank_country} = $merchant_information{pay_bank_country};
#				$pending_merchant_info{pay_bank_postalcode} = $merchant_information{pay_bank_postalcode};

  				
  				
  				$pending_merchant_info{status} = 0;
  				$pending_merchant_info{merchant_type} = $merchant_information{merchant_type};
				
				
					#sanitize some data
					if($pending_merchant_info{member_id})
					{
						$pending_merchant_info{member_id} =~ s/\D//g;
						$pending_merchant_info{member_id} =~ s/^0*//;
					}
					
					if($pending_merchant_info{referral_id})
					{
						$pending_merchant_info{referral_id} =~ s/\D//g;
						$pending_merchant_info{referral_id} =~ s/^0*//;
					}
					
					# This shouldn't be needed, but it won't hurt.
					if($pending_merchant_info{ca_number})
					{
						$pending_merchant_info{ca_number} =~ s/\D//g;
						$pending_merchant_info{ca_number} =~ s/^0*//;
					}
					
					if($pending_merchant_info{if_number})
					{
						$pending_merchant_info{if_number} =~ s/\D//g;
						$pending_merchant_info{if_number} =~ s/^0*//;
					}
					
				$MerchantAffiliates->insertPendingMerchantInformation(\%pending_merchant_info);
				
			};
			if($@)
			{
				
				#warn $FindBin::RealScript . ' : ' . $@;
				
				my $error_message = $@;
				
				my %error_email = 
										(
											from=>'errors@dhs-club.com',
											to=>'errors@dhs-club.com',
											subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
											text=>"There was an error creating a pending merchant.\n Error Message: \n $error_message ",
										);
				
				my $temp = \%merchant_information;
				$error_email{text} .= "\n merchant_information: \n" . Dumper($temp);
				
				my $MailTools = MailTools->new();
				$MailTools->sendTextEmail(\%error_email);
				
				displayApplication(\%merchant_information, {script_error => ['Script Error']}, $CGI->param('_step'));
				exitScript();
				
			}
			
			
			
		}
		
	}
	
	
	
	eval{
			
			#delete($merchant_information{ec_number}) if (defined $merchant_information{ec_number} && $merchant_information{ec_number} && $CGI->param('_step') == 2);
			
			my $step = $CGI->param('_step') + 1;
			
			if($step == 3)
			{
				my $url = $CGI->url();
				
				use URI::Escape;
				
				my @parameters = ();
				
				$merchant_information{_step} = 3;
				
				$merchant_information{'need_reward_cards'} = 1 if ! $number_of_cards_in_a_region_is_ok;
				
				#$merchant_information{merchant_type} = $MerchantAffiliates->getMerchantTypeNameById($merchant_information{merchant_package});
				
				foreach (qw(merchant_type merchant_package payment_method exception_percentage_of_sale percentage_of_sale ec_number ca_number _step need_reward_cards merchant_master_id username password))
				{
					if($merchant_information{$_})
					{
						my $encoded = URI::Escape::uri_escape($_);
						my $value = URI::Escape::uri_escape($merchant_information{$_});
						push @parameters, "$encoded=$value";
					}
				}
				
				$url .= '?' . join('&', @parameters);
							
				print $CGI->redirect(-uri=>$url);
				exitScript();
			}
			
			displayApplication(\%merchant_information, undef, $step);
			
	};
	if($@)
	{
		
		#warn "$FindBin::RealBin $FindBin::RealScript " . $@;
		
		
		
		my $error_message = $@;
		
		my %error_email = 
						(
							from=>'errors@dhs-club.com',
							to=>'errors@dhs-club.com',
							subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript :: processApplication()",
							text=>"There was an redirecting a merchant to step 3 of the registration from.\n Error Message: \n $error_message ",
						);
				
		my $temp = \%merchant_information;
		$error_email{text} .= "\n merchant_information: \n" . Dumper($temp);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%error_email);
		
		
		$missing_fields{'fatal_error'} = 'Fatal Error';
		
		displayApplication(\%merchant_information, \%missing_fields, $CGI->param('_step'));
		
	}
	
	
}

####################################################
# Main
####################################################

if (! $CGI->https())
{
	my $url = $CGI->self_url();
	$url =~ s/http:/https:/;
	print $CGI->redirect($url);	
	exitScript();
}



unless ($db = DB_Connect('merchant_applications')){exit}
$db->{RaiseError} = 1;

if($CGI->param('data'))
{
	displayNotEnoughCards();
}
elsif($CGI->param())
{
	processApplication();	
} else {
	displayApplication();
}




exitScript();

__END__

=head1
CHANGE LOG

	2009 July 14	Keith	Removed the checking of VIP status when an applicant specified a 
							Club Account to Charge.  Now all that is required is a Valid ClubShop Membership.
	
	2009 July 17	Keith	A Credit Card Zip Code is not required, due to some international customers
							not having zip codes, Curacao for example.
							
	2009 July 17	Keith	Home phone is no longer required.
	
	2009 August 17	Keith	Added International Funds Transfer Information.

	2009 September 14	Keith	Added the XML International Funds Transfer Information, 
								and made some bug fixes to the checking of a member account 
								when one opts to pay by Club Account.
								
	2009 September 16	Keith	Added information for crediting a merchants Club Account when 
								a merchant has paid for their package by Credit Card.
	
	2009 October 27		Keith	If the "refspid" is present, and there is no query string in the URL
								the "referral_id" field is populated with the value from the
								"refspid" cookie.
	
	2010 Mar 12		Keith		If the "AuthCustom_Generic" cookie is present, and there is no query 
								string in the URL the "referral_id" field is populated with the value 
								from the "AuthCustom_Generic" cookie.  The VIP were having issues,
								so hopefully this will settle it.  When VIP have been on our site,
								and logged into the VIP control center the referral_id was not 
								getting their ID number, instead is was getting what ever was
								set in the refspid cookie.
								
								The referral_id field being population goes in this order
								
								1) If the referral_id parameter is specified in the query string
									that is used.
								2) If the "AuthCustom_Generic" cookie is set, and the member type is "v"
									the ID is used to set the referral_id.
								3) If the refspid cookie is set the referral_id is set by the value of the
									refspid cookie.
	
	
	2010 Mar 18		Keith		Modified the displayApplication function so if a user is returned to the
								display, and they have selected a country with provinces in our states table,
								they will get a drop down with the province to choose.
	
	2010 May 03		Keith		Modified the processApplication function, I added a check if someone uses a
								house account as the referring_id the card requirement check is bypassed.
	
	2010 Jun 16		Keith		Modified the processApplication function, I added checks, and the specials
	 							field for the flat monthly fee Merchants.

	2010 Jul 13		Keith		Made many changes to this application to meet the requirements for version 3.0 of the Merchant Interface Website.
								Moved package options to step two, and expanded the pricing structure to include monthly subscriptions.

=cut
