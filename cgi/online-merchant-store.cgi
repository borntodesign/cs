#!/usr/bin/perl
# the merchant application for entrance into our online Mall
# started by George as a copy of the offline merchant registration
# heavily revised by Bill 05/16/12 for release
# last modified: 09/17/12	Bill MacArthur

use strict;
use warnings;
use lib '/home/httpd/cgi-lib';
use DHSGlobals;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use G_S;
require MerchantAffiliates;
use XML::local_utils;
use XML::Simple;
use Merch_Tools;
use Members;
use MailTools;
require DHS::UrlSafe;
use Locations;
use Data::Dumper;
require CreditCard;
require ccauth;
require ccauth_parse;
use Template;

my $SKIP_AUTHORIZATION = (); #'1,,,PAYMENT BYPASSED,PAYMENT BYPASSED,,TESTING';	# for normal authorization, this should be empty, but save the test result as it represents part of what could be an Authnet response
our $db = ();
my $gs = G_S->new({'db'=>\$db});
our $CGI = CGI->new();
our $DHS_UrlSafe = DHS::UrlSafe->new();
my $MailTools = MailTools->new();
my $ME = $CGI->script_name();
my $HQ_CONTACT = 'clubshoprewards@clubshop.com';
my %TMPL = (
	'xml'	=> 10680,# test 10955, # previous 10928
	'html'	=> 10927,
	'select_ctls'	=> 10931,		# an HTML snippet of online mall categories
	'config1'	=> 10681,				# not really sure this configuration applies to the online app, but George included it
	'merch_email_1'	=> 10935,			# a rudimentary assembly document for the email sent to a merchant when the application is entered
	'online_activate_xml'	=> 10933,	# language pack for online merchant activation
	'merch_sponsor_email_1'	=> 10936,	# a rudimentary assembly document for the email sent to a merchant sponsor when the application is entered
	'finish_page_html'	=> 10956		# the HTML template rendered when they complete the application
);
my %missing_fields = ();
my $TT = Template->new();

sub AuthorizeCard
{
	my $merchant_information = shift;
	my $translation = shift;
	my $credit_new_members_club_account = shift;
	
	$merchant_information->{'cc_number'} = $merchant_information->{'pay_cardnumber'};
	$merchant_information->{'ccv_number'} = $merchant_information->{'pay_cardcode'};
	$merchant_information->{'cc_expiration_date'} = $merchant_information->{'pay_cardexp'};
	$merchant_information->{'cc_name_on_card'} = $merchant_information->{'pay_name'};
	$merchant_information->{'cc_address'} = $merchant_information->{'pay_address'};
	$merchant_information->{'cc_city'} = $merchant_information->{'pay_city'};
	$merchant_information->{'cc_country'} = $merchant_information->{'pay_country'};
	$merchant_information->{'cc_state'} = $merchant_information->{'pay_state'};
	$merchant_information->{'cc_postalcode'} = $merchant_information->{'pay_postalcode'};
	$merchant_information->{'payment_method'} = 'CC';

	my %cc_parameters = ();

	$cc_parameters{'cc_number'} = $merchant_information->{'cc_number'};
	$cc_parameters{'card_code'} = $merchant_information->{'ccv_number'};
	$cc_parameters{'cc_expiration'} = $merchant_information->{'cc_expiration_date'};
	$cc_parameters{'cc_expiration'} =~ s/^d//;
	$cc_parameters{'account_holder_lastname'} = $merchant_information->{'cc_name_on_card'};
	$cc_parameters{'account_holder_address'} = $merchant_information->{'cc_address'};
	$cc_parameters{'account_holder_city'} = $merchant_information->{'cc_city'};
	$cc_parameters{'account_holder_state'} = $merchant_information->{'cc_state'};
	$cc_parameters{'account_holder_zip'} = $merchant_information->{'cc_postalcode'} if $merchant_information->{'cc_postalcode'};
	$cc_parameters{'account_holder_country'} = $merchant_information->{'cc_country'};
	$cc_parameters{'payment_method'} = 'CC';
	$cc_parameters{'invoice_number'} = 'MA' . time;
	$cc_parameters{'id'} = 'MARegistration';
	    		
	if ($merchant_information->{'discount_type'} == 20)
	{
		$cc_parameters{'amount'} = $translation->{'merchant_types'}->{'online_mall'}->{'bronze'}->{'initial_fee_bronze'};
	}
	elsif ($merchant_information->{'discount_type'} == 21)
	{
		$cc_parameters{'amount'} = $translation->{'merchant_types'}->{'online_mall'}->{'silver'}->{'initial_fee_silver'};
	}
	elsif ($merchant_information->{'discount_type'} == 22)
	{
		$cc_parameters{'amount'} = $translation->{'merchant_types'}->{'online_mall'}->{'gold'}->{'initial_fee_gold'};
	}
	elsif ($merchant_information->{'discount_type'} == 23)
	{
		$cc_parameters{'amount'} = $translation->{'merchant_types'}->{'online_mall'}->{'platinum'}->{'initial_fee_platinum'};
	}
	elsif ($merchant_information->{'discount_type'} == 24)
	{
		$cc_parameters{'amount'} = $translation->{'merchant_types'}->{'online_mall'}->{'basic'}->{'initial_fee_basic'};
	}
	$cc_parameters{'order_description'} = 'Online Merchant Registration Credit Card Charge ';

###### ###### Credit card authorization here ###### ######
#warn 'online mall: '.Dumper($translation->{'merchant_types'}->{'online_mall'});
#warn Dumper(\%cc_parameters);
                
	# Tell Authorize that we want the whole enchilada instead of a simple digit code result
	# and that we only want to do an authorization.... we will capture later after creating the records
	#{'raw'=>1, 'auth_type'=>'AUTH_ONLY'};
	my $returned_auth_info_raw = $SKIP_AUTHORIZATION || CCAuth::Authorize(\%cc_parameters, {'raw'=>1, 'auth_type'=>'AUTH_ONLY'});
				
	# this value is a hashref... see the module for the details
	my $returned_auth_info = ccauth_parse::Parse($returned_auth_info_raw);
	
	if ($returned_auth_info->{'resp_code'} eq '1')
	{
		$credit_new_members_club_account->[0]->{'amount'} = $cc_parameters{'amount'};
		$credit_new_members_club_account->[0]->{'description'} = "Funding for an Online Merchant Package\nAuthnet Trans ID: $returned_auth_info->{'transaction_id'}";
		$credit_new_members_club_account->[0]->{'transaction_type'} = 7000;
		$credit_new_members_club_account->[0]->{'operator'} = $CGI->cookie('operator') || $ME;
		
		# by placing the response hash in here, we have the details later on wherever we need them
		$credit_new_members_club_account->[0]->{'returned_auth_info'} = $returned_auth_info;
		return 1;
	}
	else
	{
		# I don't know what is up with all the regex's but whatever... Bill 05/12
		my $error_code = $returned_auth_info->{'resp_code'};
		$error_code =~ s/^.*\s(\d+):.*/$1/gi;
		$error_code =~ s/\s*//gi;
		$returned_auth_info->{'resp_code'} =~ s/^\d\s*|\d+:\s*|\<\/*b\>\s*|\<br\s\/\>//gi;
		$missing_fields{"cc_error_$error_code"} = [$returned_auth_info->{'resp_code'}];
		$missing_fields{'cc_error'} = [$returned_auth_info->{'resp_code'}];

		displayApplication($merchant_information, \%missing_fields, $CGI->param('_step'));
		exitScript();
	}
}

sub ctlPayCardExp
{
	my $arg = shift || '';
	my $rv = q|<select name="pay_cardexp" id="pay_cardexp" class="registration_dropmenu"><option value='ba'> </option>|;
	my @dateparts = localtime();
	my $currmonth = $dateparts[4] + 1;	# month from localtime is 0-11
	my $year = sprintf("%02d", $dateparts[5] % 100);

	# we need to finish off our current year with the months remaining
	for (my $month = $currmonth; $month < 13; $month++)
	{
		$month = '0' . $month if length($month) == 1;	# we are going to want our month to be 01-12
		my $val = $month . $year;
		
		my $args = {'-value'=>$val};
		$args->{'-selected'} = 'selected' if $val eq $arg;
		$rv .= $CGI->option($args, $val);
	}
	
	# now fill in full years ahead 10 years
	for (my $ny = $year; $ny < ($year +10); $ny++)
	{
		for (my $month = 1; $month < 13; $month++)
		{
			$month = '0' . $month if length($month) == 1;	# we are going to want our month to be 01-12
			my $val = $month . $ny;
			
			my $args = {'-value'=>$val};
			$args->{'-selected'} = 'selected' if $val eq $arg;
			$rv .= $CGI->option($args, $val);
		}
	}
	
	return $rv . '</select>';
}

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub displayApplication
{
	my $merchant_information = shift; warn 'Entering displayApplication with this data: '.Dumper($merchant_information);
	my $errors = shift;
	my $step = shift;	
	
	$step = 1 if ! $step;
	
	my $html = '';
	my $xml = '';
	my $transl = '';
	my $configuration_xml = '';
	
	my $data = '';
	my $errors_xml = '';
	my $langauges_xml = '';
	my $states_xml = '';
	
	my $params = '';
	
	my $bank_information_xml = '';
	
	my $MA = {};
	my $translation_xml = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}";
	my $translation = XML::Simple::XMLin($translation_xml, 'NoAttr'=>1);

	$MA = MerchantAffiliates->new($db);
	$configuration_xml = $MA->retrieveMerchantConfig();
		
	if (! $merchant_information)
	{
		if ($CGI->cookie('AuthCustom_Generic'))
		{
			my ( $id, $meminfo ) = $gs->authen_ses_key( $CGI->cookie('AuthCustom_Generic'));

			# a real login will simply be digits in the $id
			if ($id && $id !~m /\D/ && $meminfo->{'membertype'} eq 'v')
			{
				$merchant_information->{'referral_id'} = $id;
			}
		}
		elsif($CGI->cookie('refspid'))
		{
			$merchant_information->{'referral_id'} = $CGI->cookie('refspid');	
		}
				
	}

	$html = $gs->Get_Object($db, $TMPL{'html'}) || die "Failed to load template: $TMPL{'html'}";
#
# Get the translated countries
#
	my $language_Preference = $gs->Get_LangPref() || 'en';
	my $sth = $db->prepare("SELECT country_code, country_name FROM country_names_translated_no_restrictions(?)");
	my (%countries, @countries) = ();
	$sth->execute($language_Preference);
	while (my ($cc, $cn) = $sth->fetchrow_array)
	{
		push @countries, $cc;
		$countries{$cc} = $cn;
	}

#
# If we are coming back in to get the state or provence
#
	if($merchant_information->{'country'})
	{
		
		my $Locations = Locations->new($db);
		
		if ($step == 1)
		{
			$states_xml .= $Locations->getXMLProvincesByCountryCode({'country_code'=>$merchant_information->{'country'}});
		}
		elsif($step == 2 && $merchant_information->{'cc_country'})
		{
			$states_xml .= $Locations->getXMLProvincesByCountryCode({'country_code'=>$merchant_information->{'cc_country'}});
			$states_xml .= $Locations->getXMLProvincesByCountryCode({'country_code'=>$merchant_information->{'cc_country'}, 'node_name'=>'cc_state'});
		}
		
	}
	
# Convert Countries/States XML to HTML
#
	foreach my $placeholder (qw/countries pay_countries/)
	{
		my $param_name = $placeholder eq 'countries' ? 'country' : 'pay_country';
		my $countries_html = '';
		if ($merchant_information->{$param_name}) 
		{
			$countries_html = '<option value=""> </option>';
		}
		else
		{
			$countries_html = '<option value="" selected="selected"> </option>';
		}
	
		foreach my $key (@countries)
		{
			if (($merchant_information->{$param_name} || '') eq $key)
			{
				$countries_html .= qq#<option value ="$key" selected="selected">$countries{$key}</option>\n#;
			}
			else
			{
				$countries_html .= qq#<option value ="$key">$countries{$key}</option>\n#;
			}
		}
	
		$html =~ s/\%\%$placeholder\%\%/$countries_html/g;
	}

#
# Get Languages
#
	my $languages = $db->selectall_hashref("SELECT code2, description FROM language_codes WHERE code2 IS NOT NULL AND active = TRUE",
                                               'description', undef) ||       
                                               die "Failed to obtain a set of language names"; 
	my $language_html = "";
	if ($merchant_information->{'language'})
	{
		$language_html = '<option value=""> </option>';
	}
	else
	{
		$language_html = '<option value="" selected="selected"> </option>';
	}

	foreach my $lang (sort(keys %{$languages}))
	{
		if (($merchant_information->{'language'} || '') eq $languages->{$lang}->{'code2'})
		{
	        $language_html .= "<option value=\"" . $languages->{$lang}->{'code2'} . "\" selected=\"selected\" >" . $languages->{$lang}->{'description'} . "</option>\n";
	    }
	    else
	    {
	        $language_html .= "<option value=\"" . $languages->{$lang}->{'code2'} . "\">" . $languages->{$lang}->{'description'} . "</option>\n";
	    }
	}

	$html =~ s/%%languages%%/$language_html/g;
	
#
# Convert Languages
#
	my $strng = $gs->Get_Object($db, $TMPL{'select_ctls'}) || die "Failed to load template: $TMPL{'select_mall_cats'}";
	my @selects = split(/<!-- -->/,$strng);
#
# create the select statements.
#
	my $tmpstmt = {
		'select_stmt' => HackSelectCtl($selects[0], ($merchant_information->{'online_mall_category'} || $merchant_information->{'main_mall_category'}))
		,'payment_method_select' => HackSelectCtl($selects[1],  $merchant_information->{'pay_payment_method'})
#		,'bogo_amt_select' => $selects[2],
		,'cc_type_select' => HackSelectCtl($selects[3], $merchant_information->{'cc_type'})
		,'pay_cardexp_select' => ctlPayCardExp($merchant_information->{'pay_cardexp'})# HackSelectCtl($selects[4], $merchant_information->{'pay_cardexp'})
	};

	$html = Merch_Tools::addLanguage($html, $tmpstmt);
	
	my $tmp = ();
	my $rz = $TT->process(\$html, {'lang_blocks'=>$translation, 'merchant_information'=>$merchant_information, 'CGI'=>$CGI, 'ME'=>$ME}, \$tmp);
	die $TT->error() unless $rz;
	$html = $tmp;
	$html = Merch_Tools::addLanguage($html, $translation->{'merchant_types'}->{'online_mall'});

	$merchant_information->{'checkedm3'} = "checked=\"checked\"";

	if (! $merchant_information->{'member_id'})
	{
	    $merchant_information->{'member_id'} = '';
		$merchant_information->{'checkedm1'} = "";
	}
	else
	{
		$merchant_information->{'checkedm1'} = "checked=\"checked\"";
		$merchant_information->{'checkedm3'} = "";
	}

	if (! $merchant_information->{'referral_id'})
	{
	    $merchant_information->{'referral_id'} = '';
		$merchant_information->{'checkedm2'} = "";
	}
	else
	{
		$merchant_information->{'checkedm2'} = "checked=\"checked\"";
		$merchant_information->{'checkedm3'} = "";
	}

	if (! $merchant_information->{'business_name'})
	{
		$merchant_information->{'business_name'} = "";
	}

	if (! $merchant_information->{'address1'}) {
		$merchant_information->{'address1'} = "";
	}

	if (! $merchant_information->{'state'}) {
		$merchant_information->{'state'} = "";
	}
	
	if (! $merchant_information->{'city'}) {
		$merchant_information->{'city'} = "";
	}
	if (! $merchant_information->{'postalcode'}) {
            $merchant_information->{'postalcode'} = "";
	}
	if (! $merchant_information->{'phone'}) {
            $merchant_information->{'phone'} = "";
	}
	if (! $merchant_information->{'emailaddress1'}) {
            $merchant_information->{'emailaddress1'} = "";
	}
	if (! $merchant_information->{'firstname1'}) {
            $merchant_information->{'firstname1'} = "";
	}
	if (! $merchant_information->{'lastname1'}) {
            $merchant_information->{'lastname1'} = "";
	}
	if (! $merchant_information->{'url'}) {
            $merchant_information->{'url'} = "";
	}
	if (! $merchant_information->{'username'}) {
            $merchant_information->{'username'} = "";
	}
	if (! $merchant_information->{'password'}) {
            $merchant_information->{'password'} = "";
	}
	if (! $merchant_information->{'discount_blurb'}) {
            $merchant_information->{'discount_blurb'} = "";
	}
	if (! $merchant_information->{'pay_cardnumber'}) {
            $merchant_information->{'pay_cardnumber'} = "";
	}
	if (! $merchant_information->{'pay_cardcode'}) {
            $merchant_information->{'pay_cardcode'} = "";
	}
	if (! $merchant_information->{'pay_name'}) {
            $merchant_information->{'pay_name'} = "";
	}
	if (! $merchant_information->{'pay_address'}) {
            $merchant_information->{'pay_address'} = "";
	}
	if (! $merchant_information->{'pay_city'}) {
            $merchant_information->{'pay_city'} = "";
	}
	if (! $merchant_information->{'pay_state'}) {
            $merchant_information->{'pay_state'} = "";
	}
	if (! $merchant_information->{'pay_postalcode'}) {
            $merchant_information->{'pay_postalcode'} = "";
	}
#warn "Online-test2: " . Dumper($merchant_information);
#warn "Html stuff: " . $html . "\n";
	$html = Merch_Tools::addLanguage($html, $merchant_information, "dat");
#
# display any errors if any
#
	if ($errors)
	{
		$html =~ s/NONE/block/;
		my $errhtml = '';
		foreach my $key (keys %{$errors})
		{
			if (ref($translation->{'errors'}->{$key}) eq 'HASH')
			{
				$errhtml .= $translation->{'errors'}->{$key}->{'content'} . "<br/>";
			}
			else
			{
				$errhtml .= $translation->{'errors'}->{$key} . "<br/>";
			}
		}

		$html =~ s/%%errors_out%%/$errhtml/g;
	}

#
# business description information
#
	my $information_hashref_arrayref = {};
	$information_hashref_arrayref->{'pay_cardnumber'} = "";
	$information_hashref_arrayref->{'discount_blurb'} = "";

	if($merchant_information)
	{
		$merchant_information->{'biz_description'} = $db->selectrow_arrayref('SELECT description FROM business_codes(?,?)', undef, ($language_Preference, $merchant_information->{'business_type'}))
			if $merchant_information->{'business_type'}; 
		
		foreach (keys %$merchant_information)
		{
			next if ! $_;
			$information_hashref_arrayref->{$_} = [$merchant_information->{$_}];
		}

		$params = XML::Simple::XMLout($information_hashref_arrayref, 'RootName' => "params" )
	}
	
			
	my $country_code_lower_case = lc($merchant_information->{'country'});
			
	
	my $config_xml = $gs->Get_Object($db, $TMPL{'config1'}) || die "Failed to load template: $TMPL{'config1'}";
	my $config = XML::Simple::XMLin("<base>" . $config_xml . "</base>");
	$html = Merch_Tools::addLanguage($html, $config);

	if (! $information_hashref_arrayref->{'discount_blurb'})
	{
		$information_hashref_arrayref->{'discount_blurb'} = "";
	}

	$html = Merch_Tools::addLanguage($html, $information_hashref_arrayref, "params");
	print $CGI->header('-charset'=>'utf-8');
	print $html;
}


=head2
processApplication

=head3
Description:

	Process the application.

=head3
Params:

	none

=head3
Returns:

	none

=cut
sub processApplication
{
	my %merchant_information = ();
	my $Members = {};
	my $MerchantAffiliates = {};
	my $credit_new_members_club_account = [];

	$MerchantAffiliates = MerchantAffiliates->new($db);
	my $configuration_xml = $MerchantAffiliates->retrieveMerchantConfig();
	my $configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef(); #XML::Simple::XMLin("<base>$configuration_xml</base>");

	my $translation_xml = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}";
	my $translation = XML::Simple::XMLin($translation_xml, 'NoAttr'=>1);

	my @required_fields = (
		'business_name',
		'firstname1',
		'lastname1',
		'address1',
		'city',
		'state',
		'country',
		'phone',
		'language',
		'emailaddress1',
		'username',
		'password',
		'merchant_package',
		'percentage_off',
		'discount_type',
		'discount_subtype',
		'cc_type',
		'pay_cardnumber',
		'pay_cardcode',
		'pay_cardexp',
		'idnum_enter',
		'idnum_receipt'
	);
	
	# Assign the information the merchant submitted to the "$merchant_information" hash.
	foreach my $key ($CGI->param())
	{
		next if $key =~ /submit|biz_description/i;
				
		$merchant_information{$key} = $CGI->param($key) if $CGI->param($key) && $key !~ /^_|^x$|^y$/;
		
		$merchant_information{$key} = $CGI->param($key) if $key =~ /^merchant_package$/;
	}
	
	$merchant_information{'merchant_type'} = 'online_mall';
	$merchant_information{'merchant_package'} = $merchant_information{'discount_type'};

#
# Set the address entered to be pay_address if box is checked
#
	if ($merchant_information{'pay_address_same'})
	{
		$merchant_information{'pay_address'} = $merchant_information{'address1'};
		$merchant_information{'pay_city'} = $merchant_information{'city'};
		$merchant_information{'pay_country'} = $merchant_information{'country'};
		$merchant_information{'pay_state'} = $merchant_information{'state'};
		$merchant_information{'pay_postalcode'} = $merchant_information{'postalcode'};
	}
#
# check to see if credit card is covered over. If it is, the card information has not changed
#
	if (exists $merchant_information{'pay_cardnumber'})
	{
		if ($merchant_information{'pay_cardnumber'} =~ /"XXXXXXXXXXXX"/)
		{
			delete($merchant_information{'pay_cardnumber'}) if $merchant_information{'pay_cardnumber'};
            delete($merchant_information{'pay_cardcode'}) if $merchant_information{'pay_cardcode'};
            delete($merchant_information{'pay_cardexp'}) if $merchant_information{'pay_cardexp'};
			delete($merchant_information{'pay_name'}) if $merchant_information{'pay_name'};
            delete($merchant_information{'pay_address'}) if $merchant_information{'pay_address'};
            delete($merchant_information{'pay_city'}) if $merchant_information{'pay_city'};
            delete($merchant_information{'pay_country'}) if $merchant_information{'pay_country'};
            delete($merchant_information{'pay_state'}) if $merchant_information{'pay_state'};
            delete($merchant_information{'pay_postalcode'}) if $merchant_information{'pay_postalcode'};
#warn "online-test: Deleting CC: ";
		}
        else
        {
        	my $rv = CreditCard::validate($merchant_information{'pay_cardnumber'});

#            if ($rv)
#            {
#				the CC ping stuff used to be here... now it's in AuthorizeCard
#	        }
#	        else
			unless ($rv)
	        {
		    	$missing_fields{'cc_number'} = ['Missing Field'];
	        }
	    }
	}
	else
	{
	    	$missing_fields{'cc_number'} = ['Missing Field'];
	}
         
#
# Check for missing fields.
#
	foreach my $key (@required_fields)
	{
	    if (! defined $merchant_information{$key} || ! $merchant_information{$key})
	    {
#warn "online-test: Missing: " . $key;
			$missing_fields{$key} = ['Missing Field'];
	    }
	}
	
#
# check to see in the number of characters in the blurb is less than or - to 100
#
	if (defined $merchant_information{'discount_blurb'} && $merchant_information{'discount_blurb'})
	{
		if (length($merchant_information{'discount_blurb'}) > 100 )
		{
			$merchant_information{'discount_blurb'} = substr($merchant_information{'discount_blurb'},0,100);
			$missing_fields{'blurb_adjusted'} = ['Longer than 100'];
	    }
	}
#
# check to see if the url starts with http: if it doesn't, add it
#
	if ($merchant_information{'url'} =~ /^http:/)
	{
	}
	else
	{
		my $tmpurl = $merchant_information{'url'};
		$merchant_information{'url'} = "http://" . $tmpurl;
	}
	
	$Members = Members->new($db);
#	my $member_id = '';
#	my $referral_id = '';
				#sanitize some data
	if($merchant_information{'member_id'})
	{
		$merchant_information{'member_id'} =~ s/\D//g;
		$merchant_information{'member_id'} =~ s/^0*//;
	}
			
	if($merchant_information{'referral_id'})
	{
		$merchant_information{'referral_id'} =~ s/\D//g;
		$merchant_information{'referral_id'} =~ s/^0*//;
	}

# If the applicant supplied a Member ID
	if($merchant_information{'member_id'})
	{
#		$member_id = $merchant_information{'member_id'};
#		$member_id =~ s/\D//g;
#		$member_id =~ s/^0*//;
				
#		$missing_fields{'member_id_invalid'} = ['Missing Field'] if ($member_id && ! $Members->getMemberInformationByMemberID({'member_id'=>$member_id, 'field'=>'id'}));
		$missing_fields{'member_id_invalid'} = ['Missing Field'] unless $Members->getMemberInformationByMemberID({'member_id'=>$merchant_information{'member_id'}, 'field'=>'id'});			
	}
			
	if ($merchant_information{'referral_id'})
	{
#		$referral_id = $merchant_information{'referral_id'};
#		$referral_id =~ s/\D//g;
#		$referral_id =~ s/^0*//;
		
#		my $referal_member_type = $Members->getMemberInformationByMemberID({'member_id'=>$referral_id, 'field'=>'membertype'});
		my $referal_member_type = $Members->getMemberInformationByMemberID({'member_id'=>$merchant_information{'referral_id'}, 'field'=>'membertype'});
		$missing_fields{'referral_id_wrong_membertype'} = ['Missing Field'] if (! $referal_member_type || ! grep $_ eq $referal_member_type, ('v'));
	}
			
			
	$missing_fields{'member_id_referral_id'} = ['Missing Field'] if ($merchant_information{'referral_id'} && $merchant_information{'member_id'});	
			
	if (! $missing_fields{'emailaddress1'})
	{
				
		$missing_fields{'emailaddress1_preexisting'} = ['Missing Field'] if (! $Members->checkEmailDomain($merchant_information{'emailaddress1'}));
		$missing_fields{'emailaddress1_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingEmailDuplicates($merchant_information{'emailaddress1'}));
        }
			
	if(! exists $missing_fields{'username'})
	{
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkPendingUsernameDuplicate($merchant_information{'username'}));
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkMasterUsernameDuplicates($merchant_information{'username'}));
		$missing_fields{'username_preexisting'} = ['Missing Field'] if (! $MerchantAffiliates->checkLocationUsernameDuplicates($merchant_information{'username'}));
	}
			
	# If there are errors, or missing fields redisplay the form so the Applicant can correct the issues.
	if(scalar (keys %missing_fields))
	{	
		displayApplication(\%merchant_information, \%missing_fields);
		exitScript();
	}

#
# For this page, only a percentage off will be made
#
	$merchant_information{'reward_discount'} = 'pctoff';
	$merchant_information{'discount_subtype'} = 2;
	
	# when the special is set, the discount is set to -0- in the merchant location creation method in the MerchantAffiliates class
	# we need the discount to be set since that is what will be used to determine how much our commission will be on sales entered
#	$merchant_information{'special'} = $merchant_information{'percentage_off'};
	$merchant_information{'percentage_of_sale'} = $merchant_information{'percentage_off'};
	
########################################################################
# If the member made it this far, all the required fields check out.
# Now we will see about making them a merchant, or putting
# them in the "merchant_pending" table.
########################################################################
#Create the Merchant Member, the Merchant Master record, the Merchant Location record, and the Shopping Member
			
	$merchant_information{'business_type'} = 9999;
	$merchant_information{'online_mall_category'} = $merchant_information{'main_mall_category'};
	$merchant_information{'primary_mallcat'} = $merchant_information{'main_mall_category'};
	$merchant_information{'status'} = 2;
	delete ($merchant_information{'main_mall_category'});

	$MerchantAffiliates = MerchantAffiliates->new($db);
					
#warn "online-test: create";

	# if the user supplied a member ID, then this should be the referral ID... shouldn't it?
	$merchant_information{'referral_id'} ||= $merchant_information{'member_id'};

	# let's ping the for the full price before creating any records
	# if the card does not authorize, then AuthorizeCard will redisplay the app with errors and exit the script... regardless we will do an explicit exitScript just in case
	exitScript() unless AuthorizeCard(\%merchant_information, $translation, $credit_new_members_club_account);

	eval {
		$merchant_information{'merchant_master_id'} = $MerchantAffiliates->createMerchant(\%merchant_information, $credit_new_members_club_account);
	};

	my $err = $@;	# need to do this because if we enter the IF block and perform anything before referencing $@, we lose the current value
	if ($err)
	{
		CCAuth::Void($credit_new_members_club_account->[0]->{'returned_auth_info'});
		die $err;
	}

	# at this point we have created all the records -AND- the Club Account entry, so let's CAPTURE our prior authorization... there should be only one element in the arrayref
	my $rv = $SKIP_AUTHORIZATION ? 1 : CCAuth::Capture($credit_new_members_club_account->[0]->{'returned_auth_info'});
	
	# if our capture did not return properly, alert HQ and die
	unless ($rv and $rv eq '1')
	{
		my $msg = "Capture of CC transaction failed. Authnet transaction ID: $credit_new_members_club_account->[0]->{'returned_auth_info'}->{'transaction_id'}\nMerchant ID: $merchant_information{'merchant_master_id'}";
		my %email =
		(
			'from'=>"$ME <clubshop\@dhs-club.com>",
			'to'=>$HQ_CONTACT,
			'subject'=>'Merchant Registration CC Capture Failure',
			'text'=>$msg
		);

		$MailTools->sendTextEmail(\%email);
		die $msg;
	}
	else
	{
		$MerchantAffiliates->updateVendorStatusByMerchantID($merchant_information{'merchant_master_id'} , 2);
	}
	
#
# Send Email to Carina
#
	my $qry = "SELECT * FROM merchant_affiliate_locations WHERE merchant_id = $merchant_information{'merchant_master_id'};";
	my $merch_locs = $db->selectrow_hashref($qry);
	
	my %email =
	(
		'from'=>"$ME <clubshop\@dhs-club.com>",
		'to'=>'clubshoprewards@clubshop.com',
		'subject'=>'New Online Merchant Registration',
		'text'=>"Merchant ID: $merchant_information{'merchant_master_id'}\nVendor_id: $merch_locs->{'vendor_id'}",
	);

	$MailTools->sendTextEmail(\%email);

#
# Send email to merchant
#

	my $html = $gs->Get_Object($db, $TMPL{'merch_email_1'}) || die "Failed to load template: $TMPL{'merch_email_1'}";
	
	# when called this way, it will default to the language_pref indicated by the user's browser
	$translation_xml = $gs->Get_Object($db, $TMPL{'online_activate_xml'}) || die "Failed to load template: $TMPL{'online_activate_xml'}";
	$translation = XML::Simple::XMLin($translation_xml);

	$html = Merch_Tools::addLanguage($html, $translation);
	$html =~ s/%%params_firstname%%/$merchant_information{'firstname1'}/g;
	$html =~ s/%%params_username%%/$merchant_information{'username'}/g;
	$html =~ s/%%params_password%%/$merchant_information{'password'}/g;
	$html =~ s/%%params_merchant_id%%/$merchant_information{'merchant_master_id'}/g;
	%email =
	(
		'from'=>'Clubshop Rewards <Info@ClubShop.com>',
		'to'=>"$merchant_information{'emailaddress1'}",
		'subject'=>$translation->{'email1'}->{'subject'},
		'text'=>$html,
	);

	$MailTools->sendTextEmail(\%email);

#
# Now for the email for the sponser
#
	unless ($merchant_information{'spid'})
	{
		# get the merchant record
		my $tmp = $MerchantAffiliates->retrieveMerchantMasterRecordByID( $merchant_information{'merchant_master_id'} );
		
		# we can get the merchant ma membership's spid
		$merchant_information{'spid'} = $Members->getMemberInformationByMemberID({'member_id'=>$tmp->{'member_id'}, 'field'=>'spid'});
	}

	my $spid_info = $Members->getMemberInformationByMemberID({'member_id'=>$merchant_information{'spid'}});
	
	$html = $gs->Get_Object($db, $TMPL{'merch_sponsor_email_1'}, $spid_info->{'language_pref'});
	
	# when called this way, it will specify the language preferred by the sponsor
	$translation_xml = $gs->Get_Object($db, $TMPL{'online_activate_xml'}, $spid_info->{'language_pref'}) || die "Failed to load template: $TMPL{'online_activate_xml'}";
	$translation = XML::Simple::XMLin($translation_xml);
	$html = Merch_Tools::addLanguage($html, $translation->{'email2'});
	$html =~ s/%%params_firstname%%/$spid_info->{'firstname'}/g;
	$html =~ s/%%merch_store%%/$merchant_information{'business_name'}/g;
	$html =~ s/%%merch_email%%/$merchant_information{'emailaddress1'}/g;
	%email =
	(
		'from'=>'Clubshop Rewards <Info@ClubShop.com>',
		'to'=>"$spid_info->{'emailaddress'}",
		'subject'=>$translation->{'email2'}->{'subject'},
		'text'=>$html,
	);

	$MailTools->sendTextEmail(\%email);

#
# Load the mall_vendor table
#
	$qry = "SELECT * from malls WHERE country_code = ? AND language_code = ?";
	my $mall = $db->selectrow_hashref($qry, undef, $merchant_information{'country'}, $merchant_information{'language'});
#warn "from mall select: " . Dumper($mall);
	if ($mall)
	{
		$qry="
			INSERT INTO mall_vendors (
				vendor_id,
				mall_id,
				url,
				flag_sale,
				flag_coupon,
				flag_shipping_incentive,
				active,
				notes,
				flag_payout_details
			) VALUES (
				?, ?, ?, ?, ?, ?, ?, ?, ?);";
            my @values = ();
            push @values, $merch_locs->{'vendor_id'};
            push @values, $mall->{'mall_id'};
            push @values, $merchant_information{'url'};
            push @values, 'false';
            push @values, 'false';
            push @values, 'false';
            push @values, 'false';
            push @values, "Added by online mall registration: $ME";
            push @values, 'false';
            $db->do($qry, undef, @values);
	}
#
#	warn "online-test:proc: 17" . Dumper(\%missing_fields) . Dumper(\%merchant_information);;
#
# Send out final page Convert Languages
#
	$translation_xml = $gs->Get_Object($db, $TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}";
	$translation = XML::Simple::XMLin($translation_xml, 'NoAttr'=> 1);

	$html = $gs->Get_Object($db, $TMPL{'finish_page_html'}) || die "Failed to load template: $TMPL{'finish_page_html'}";

	print $CGI->header('-charset'=>'utf-8');	
	$TT->process(\$html, {'lang_blocks'=>$translation, 'merchant_information'=>\%merchant_information}) || print $TT->error();
	exitScript();
}

####################################################
# Main
####################################################

#
# Make sure data comming from secure source
#
if (! $CGI->https())
{
	my $url = $CGI->self_url();
	$url =~ s/http:/https:/;
	print $CGI->redirect($url);	
	exitScript();
}

unless ($db = DB_Connect('merchant_applications')){exit}
$db->{'RaiseError'} = 1;

if($CGI->param())
{
	processApplication();	
} 
else
{
	displayApplication();
}

exitScript();

# hack, hack, hack... :(
sub HackSelectCtl
{
	my $txt = shift;
	my $val = shift || '';
#warn "val: $val";
	my $rv = '';
	foreach my $n (split(/\n/, $txt))
	{
		# we looking to determine the value of this option... the value may be encapsulated in either single or double quotes.... why? I do not know
		$n =~ m/<option value=('|")(.*?)('|")/;
		my $nval = $2 || '';
#warn "matched: $nval";
		$n =~ s/<option/<option selected="selected"/ if $nval eq $val;
		$rv .= $n;
	}
	
	return $rv;
}

__END__

09/17/12
	So many changes have been made that I will not begin to itemize them all :-(
	