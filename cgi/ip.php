<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Google Maps Plotting  User By Refrence Of IP Address | Subesh.com.np</title>
<?php
/*
Subesh Pokhrel
subesh.com.np
Mapping The IP Address to Latitude and Longitude.
The function getRealIpAddr() is taken from roshanbh.com.np
*/
function getRealIpAddr()

{
  if (!empty($_SERVER['HTTP_CLIENT_IP']))
  //check ip from share internet
  {
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
  //to check ip is pass from proxy
  {
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else
  {
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

function IPtoLatLng($ip)
{
	$latlngValue=array();
	$dom = new DOMDocument();
	$ipcheck = ip2long($ip);

    if($ipcheck == -1 || $ipcheck === false){
		echo "ERROR: INVALID IP";
		exit;
	}
	else
		$uri = "http://api.hostip.info/?ip=$ip&position=true";
 
	$dom->load($uri);
	$name=$dom->getElementsByTagNameNS('http://www.opengis.net/gml','name')->item(1)->nodeValue;
	$coordinates=$dom->getElementsByTagNameNS('http://www.opengis.net/gml','coordinates')->item(0)->nodeValue;
	$temp=explode(",",$coordinates);
	$latlngValue['LNG']=$temp[0];
	$latlngValue['LAT']=$temp[1];
	$latlngValue['NAME']=$name;
	return $latlngValue;
	
}
$IP=getRealIpAddr();
$latlng=IPtoLatLng($IP);
?>
 <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAPHLcOOGHX2-uLk3K8q1nMRSFS3Iba-Yq79Uuy07kI14Q-o9MOhQTADZYj-oIAYvmRBVx8i9NqeBSjg"
      type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[

    var iconBlue = new GIcon(); 
    iconBlue.image = 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
    iconBlue.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

	var point = new GLatLng(parseFloat('<?=$latlng['LAT']?>'),parseFloat('<?=$latlng['LNG']?>'));
    
		function load() {
		  if (GBrowserIsCompatible()) {
			var map = new GMap2(document.getElementById("map"));
			map.setCenter(new GLatLng(parseFloat('<?=$latlng['LAT']?>'),parseFloat('<?=$latlng['LNG']?>')),3);
			var marker = createMarker(point);
            map.addOverlay(marker);
		}
	}
	
	 function createMarker(point) {
      var marker = new GMarker(point, iconBlue);
      var html = "<h1>Gothcha! HA HA </h1> <br>You Are Here !<br><b><?=$latlng['NAME']?></b> ";
      GEvent.addListener(marker, 'mouseover', function() {
        marker.openInfoWindowHtml(html);
      });
      return marker;
    }
 //]]>
	
  </script>
 </head> 
  <body onload="load()" onunload="GUnload()">
  <h1>Subesh.com.np: Maps Example</h1>
   <div id="map" style="width: 500px; height: 300px"></div>
</body>
</html>