#!/usr/bin/perl -w
######  SOAreport.cgi
######	written by:	Stephen Martin
######  This utility prints of report of the statement of accounts for a VIP
###### last modified 05/30/03	Bill MacArthur

use strict;

use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw( md5_hex );
use DBI;
use locale;
use Time::JulianDay;

$| = 1;
use constant IS_MODPERL => $ENV{MOD_PERL};
use subs qw(exit);

# Select the correct exit function
*exit = IS_MODPERL ? \&Apache::exit : sub { CORE::exit };

###### GLOBALS
our( $UID, $meminfo ) = ();
my $sth = ();
our $db        = ();
our @cookieSET = ();
our $q         = new CGI;
our $id        = our $firstname = our $lastname = '';
our $data;
our $html;
our $rc;

my $qry;
my $qualifyingSQL;
my $qualifyingSQL2;
my $self = $q->url();
my $MON;
my $NOW;
my $MENU;
my $PERIOD;

my @MONTHS = (
    '_UNDEF_', 'January', 'February', 'March',     'April',   'May',
    'June',    'July',    'August',   'September', 'October', 'November',
    'December'
);

###### my parameters alias, pwd
our %param = ();    ###### parameter hash

foreach ( $q->param() ) {
    $param{$_} = $q->param($_);
}
our %cookies = ();    ###### these are the cookies passed
foreach ( $q->cookie() ) {
    $cookies{$_} = $q->cookie($_);
}

###### validate the user
###### we'll skip some stuff if we're ADMIN
###### Admin mode condition commented out this program does not use an admin mode.

##### unless ( $param{admin} ){
##### Uncomment the line above to implement an admin mode

( $UID, $meminfo ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if we decide to exclude access based on an ACCESS DENIED password, we'll do it here

###### if the UID has any non-digits it means that its bad/expired whatever
if ( !$UID || $UID =~ /\D/ ) { &RedirectLogin(); exit }

if ( $meminfo->{membertype} ne 'v' ) {
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' ),
      $q->h3('Business Reports are for VIP members only'), $q->end_html();
    exit;
}
$firstname = $meminfo->{firstname};
$lastname  = $meminfo->{lastname};
$id        = $UID;

###### DB_Connect does its own error handling, so we only need exit if it doesn't work
unless ( $db = DB_Connect( 'SOAreport.cgi') ) { exit }
###### }
###### Uncomment the '}' line above to implement admin mode

###### Uncomment code below to implement admin mode.
###### Admin begin 

###### now we'll take care of the ADMIN mode
###### we need an ID and first & lastnames to proceed

###### else{

###### here is where we get our admin user and try logging into the DB
#	my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
#	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
#	unless ($db = &ADMIN_DB::DB_Connect( 'bizreport', $operator, $pwd )){exit}
#
#	unless ($param{id}){&Err('Missing a required parameter'); goto 'END'}
#	my $qry = "SELECT lpad(Id,7,0) as id, FirstName, LastName, membertype, password FROM members WHERE id = '$param{id}'";
#	if ( &ID_Ck($qry) == 0 ){ goto 'END' };
# }

###### Admin end

$db->{RaiseError} = 1;

###### Trap action and amend SQL as appropriate 

$NOW = get_slice();

if ( $param{action} eq "SMON" ) {
    ###### build query extension to retrieve within this date range

    $MON           = $param{ym};
    $qualifyingSQL = gen_SQL($MON);
    $qualifyingSQL .= " ORDER BY soa.entered ASC";
    $qualifyingSQL2 = gen_SQL($MON);

}
else {
    ###### by default use the current month
    $MON           = $NOW;
    $qualifyingSQL = gen_SQL($NOW);
    $qualifyingSQL .= " ORDER BY soa.entered ASC";
    $qualifyingSQL2 = gen_SQL($NOW);
}

$MENU = gen_dropdown($MON);

$PERIOD = get_period($MON);

###### get current statement of accounts for the id # 

$qry = "SELECT 
        soa.trans_id, 
        soa.id, 
        soa.amount, 
        soa.description as tdesc, 
        soa.memo, 
        soa.entered, 
        soa.batch, 
        soa.reconciled, 
        soa.operator, 
        soa_transaction_types.name as trans_type_desc
      FROM 
        soa
      LEFT OUTER JOIN soa_transaction_types ON soa.trans_type = soa_transaction_types.id
      WHERE 
       ( soa.id = ? )";

$qry .= $qualifyingSQL;

$sth = $db->prepare($qry);
$sth->execute($UID);

$html = "";

$rc = 0;

while ( $data = $sth->fetchrow_hashref ) {
    $html = $html . "<tr>";
    $html = $html . "<td class=\"col1\" width=\"25%\">" . $data->{trans_type_desc} . "</td>";
    $html = $html . "<td class=\"col2\" width=\"25%\">" . $data->{amount} . "</td>";
    $html = $html . "<td class=\"col3\" width=\"50%\">" . $data->{tdesc} . "</td>";
    $html = $html . "</tr>";
$rc++;
}

$sth->finish;

if ( $rc == 0 ) 
{
$html = <<EOH1;
<tr>
 <td class="nores" colspan="3" nowrap align="center">*** There are no transactions for this month ***</td>
</tr>
EOH1
}

###### Get the total

$qry = "SELECT 
         SUM(soa.amount) as total
        FROM 
         soa
        WHERE
       soa.id = ?";

$qry .= $qualifyingSQL2;

$sth = $db->prepare($qry);
$sth->execute($UID);

$data = $sth->fetchrow_hashref;

$sth->finish;

###### Get Template 

my $TMPL = &G_S::Get_Object( $db, 10115 );
unless ($TMPL) { &Err("Couldn't retrieve the template object."); return }

if (@cookieSET) {
    print $q->header( -expires => 'now', -cookie => \@cookieSET );
}
else { print $q->header( -expires => 'now' ) }

$data->{total} = sprintf( "%.2f", $data->{total} );

$TMPL =~ s/%%SOA_ROWS%%/$html/g;
$TMPL =~ s/%%FIRSTNAME%%/$firstname/g;
$TMPL =~ s/%%LASTNAME%%/$lastname/g;
$TMPL =~ s/%%TOTAL%%/$data->{total}/g;
$TMPL =~ s/%%SELF%%/$self/g;
$TMPL =~ s/%%MONTHS%%/$MENU/;
$TMPL =~ s/%%PERIOD%%/$PERIOD/g;

print $TMPL;

END:
$db->disconnect;
exit;

sub Err {
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' ),
      'There has been a problem<br><br>';
    print $q->h4( $_[0] ), scalar localtime(), ' (PST)';
    print $q->end_html();
}

sub Hidden_Admin {
###### generate hidden tags for use when in admin mode
###### otherwise substitute nothing
    if ( $param{'admin'} && $param{'admin'} eq '1' ) { return '&admin=1' }
    else { return '' }
}

sub ID_Ck {
    my $membertype;    ###### a temporary variable just to test with
    my $sth = $db->prepare( $_[0] );
    $sth->execute;

###### id, firstname, & lastname are globals
    ( $id, $firstname, $lastname, $membertype ) = $sth->fetchrow_array;
    $sth->finish;
    unless ($membertype) {
        &Err('No match on the ID and Password received');
        return 0;
    }
    elsif ( $membertype ne 'v' ) {
        &Err('According to our records, you are not a VIP');
        return 0;
    }
    return 1;
}

sub RedirectLogin {
    my $me = $q->script_name;
    print $q->header( -expires => 'now' ),
      $q->start_html( -bgcolor => '#ffffff' );
    print
"Please login at <a href=\"$LOGIN_URL?destination=$me\">$LOGIN_URL</a><br /><br />";
    print scalar localtime(), "(PST)";
    print $q->end_html;
}

sub get_slice {
    my $mday;
    my $mon;
    my $year;

    ( $mday, $mon, $year ) = (localtime)[ 3, 4, 5 ];

    $mon++;

    $year = $year + 1900;

    return $year . "-" . sprintf( "%02d", $mon );
}

sub gen_dropdown {
    ###### Generate a dropdown of -12 months to now defaulted to whatever was selected in $MON

    my ($m) = @_;    # MON selected value

    my $cy;
    my $cm;

    my $py;
    my $pm;

    my $i;

    my $html;

    my $selected;

    ####### Find now -12 months 

    ( $cy, $cm ) = split ( /-/, $NOW );

    $html = "";

    for ( $i = 0 ; $i < 12 ; $i++ ) {
        $pm = $cm - $i;
        $py = $cy;

        if ( $pm <= 0 ) {
            $pm = $pm + 12;
            $py = $py - 1;
        }

        $pm = sprintf( "%02d", $pm );

        if ( $py . "-" . $pm eq $m ) { $selected = " SELECTED "; }
        else { $selected = ""; }

        $html = $html
          . "<option value=\""
          . $py . "-"
          . $pm
          . "\" $selected >"
          . $MONTHS[$pm] . " - "
          . $py
          . "</option>\n";
    }
    return $html;
}

sub get_period {

    my ($m) = @_;

    my $cy;
    my $cm;

    ( $cy, $cm ) = split ( /-/, $m );

    return $MONTHS[$cm] . " - " . $cy;

}

sub gen_SQL {
    my ($m) = @_;

    my $cy;
    my $cm;
    my $sql;

    ( $cy, $cm ) = split ( /-/, $m );

    $sql =
" AND ( date_part('years',entered) = $cy )  AND ( date_part('months',entered) = $cm ) ";

    return $sql;

}

###### 05/30/03 changed the DB connection to NOT connect on its own account