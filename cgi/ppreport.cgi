#!/usr/bin/perl -w
###### ppreport.cgi
###### originally named: mpp.cgi
###### The portal page for Associate Members.
######
###### Created 04/28/05 Karl Kohrt
######
###### Last modified: 03/02/16	Bill MacArthur

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib('/home/httpd/cgi-lib');
use G_S qw(%MEMBER_TYPES);
use DB_Connect;
use MSYS;
require ClubCash;
require XML::Simple;
require XML::local_utils;
#use Data::Dumper;

###### GLOBALS
my $ACTIVATE_MESSAGING = 1;		# set to zero to deactivate the messaging system hook.
my ($db, $data, $ME) = ();
my $q = new CGI;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
my %TEMPLATES = (
	'm' 	=> 10433,
	's' 	=> 10433,
	'r' 	=> 10433,
	'tp'	=> 10433,
	'content'=> 10432,
	'v'		=> 10114
	);

my %ATmtypes = (	# the activity we will trigger based upon the membertype since at least affiliates and trial partners currently use this as their business center 03/02/16
	'm' => 104,
	'tp' => 105
);
my %conditionals = ('langpref'=>0);

my $upline_contact = '';

# if we have a VIP accessing this report, we'll place their cookie hash in here
my $VIP = ();#VipCK();

# in order to allow VIPs to check these and have it work right for them we'll have
#  to go through hoops in establishing some identity.
# first we'll check for explicit parameters which will supercede any other identity check.
# in order to shorten the links with an embedded emailaddress, we'll look for a parameter
#  called 'a' as well as 'alias' - we can eliminate the alias parameter later on.
my $alias = $q->param('a') || $q->param('alias') || $q->param('id');

exit unless $db = DB_Connect('mpp.cgi');	###### DB_Connect takes care of its own error handling.
$db->{'RaiseError'} = 1;

# in order to allow them to use their ClubBucks ID we'll have to make some assumptions
#  about the format of the 'alias' submitted
#  if it begins with 'dhs' or it is all numeric, we'll assume it is a CBID
$alias = Convert_CBID() if ($alias && ($alias =~ /^dhs/i || $alias !~ /\D/));

my $id = $q->cookie('id');

# Unless they're logged in, go to the login page.
unless ($alias || $id)
{
	Bad_Login();

	Exit();
}


#$emailaddress = lc $emailaddress;

# get the record on our reportee
# we will let a parameter override a cookie
my $qry = ();

if ($alias)
{
	$alias =~ s/\s*//g;
	$alias = uc $alias;
	$qry = Generate_Query('m.alias');
}
else
{
	$qry = Generate_Query('m.id');
	$alias = $id;
}

my $sth = $db->prepare( $qry );
$sth->execute($alias);
$ME = $sth->fetchrow_hashref;
$sth->finish;

unless ($ME->{'id'})
{
	Bad_Login();
	Exit();
}
#warn "id= $ME->{'id'}";

# alias should equal members.alias since that will be the expected value in the XSL
$alias = $ME->{'alias'};

Cleanup_XML($ME);	# Escape any characters that might break the xml/xsl model.
#unshift (@$data, $ME);

# we allow reports only to members
#  unless they have certain other cookies
unless ((grep $_ eq $ME->{'membertype'}, (qw/m tp/) ) || ( $ME->{'membertype'} eq 'r' && Bypass_OK() ))
{

	my $TMPL = $gs->GetObject($TEMPLATES{'v'}) || die "Failed to load template: $TEMPLATES{'v'}";

	$TMPL =~ s/%%MEMBER%%/$MEMBER_TYPES{$ME->{'membertype'}}/g;
	$TMPL =~ s/%%TZ%%/${\(scalar localtime())}/g;

	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $TMPL;

	Exit();
}

# Get the rewards points total for display.
Reward_Balances();

# this will return NULL in cases where the member is in a coop without a designated pool ID
($ME->{'my_tp_line_destination'}) = $db->selectrow_array("SELECT od.my_tp_line_destination($ME->{'id'})") if $ME->{'membertype'} ne 'tp';

# now get the upline contact.
$qry = Generate_Query('m.spid');
$sth = $db->prepare( $qry );
$sth->execute($ME->{'spid'});
$upline_contact = $sth->fetchrow_hashref();
$sth->finish;

Cleanup_XML($upline_contact);

# we'll place a cookie so that if we pull this script up again from a raw link, we can render the report
#  without having to require the parameter
our @cookie = ();

# we won't set the cookies if they are logged in as a VIP or admin
unless (Bypass_OK())
{
	# we'll also add a numeric ID cookie that will be identical to the one setup
	#  if they fully 'login' :)
	push (@cookie, $q->cookie('-name'=>'id', '-value'=>$ME->{'id'}, '-expires'=>'+1y', '-domain'=>'.clubshop.com'));

	if ($ATmtypes{$ME->{'membertype'}})
	{
		# create an activity tracking entry
		eval { $db->do('INSERT INTO activity_tracking(id, "class") VALUES (?, ?)', undef, $ME->{'id'}, $ATmtypes{$ME->{'membertype'}}); };
	}
}

Do_Page();

Exit();

#####################################
###### Subroutines start here. ######
#####################################

sub Bad_Login
{
#		print $q->redirect('http://www.clubshop.com/ppreport.html');
	print $q->redirect('https://www.clubshop.com/cs/login.shtml?destination=/cgi/apbc');
}

sub Bypass_OK
{
###### we'll check for admin or VIP login
	return ($q->cookie('operator') || $VIP) ? 1 : undef;
}

###### Clean up any characters that might break the xml/xsl later.
sub Cleanup_XML
{
	my $info = shift;
	foreach ( keys %{$info} )
	{
		if ( defined $info->{$_} )
		{
			$info->{$_} =~ s/\</\&lt\;/g;
			$info->{$_} =~ s/\>/\&gt\;/g;
			$info->{$_} =~ s/\&/\&amp\;/g;
			$info->{$_} =~ s/\"/\&quot\;/g;
		}
		else { $info->{$_} = '' }
	}
}
sub Convert_CBID
{
	my $id = $alias;
	$id =~ s/^dhs//i;

	###### if there are any other non-digit characters we won't proceed
	return $id if $id =~ /\D/;

	my $sth = $db->prepare("
		SELECT m.alias
		FROM clubbucks_master cb
		JOIN members m
			ON m.id=cb.current_member_id
		WHERE cb.id= ?::BIGINT");
	$sth->execute($id);

	###### if we return empty, then the cookie test will run
	###### which will not give the desired result of informing the user that the value they submitted is invalid
	$id = $sth->fetchrow_array;
	$sth->finish;
	return $id || $alias;
}

###### Put the page out to the browser.
sub Do_Page
{

	# Get the membertype appropriate xsl template.
	my $xsl = $gs->GetObject($TEMPLATES{ $ME->{'membertype'} });
	unless ($xsl)
	{
		Err("Couldn't retrieve template: $TEMPLATES{ $ME->{'membertype'} }");
		return 0;
	}

	if ($q->param('xsl'))
	{
		print "Content-type: text/plain\n\n$xsl";
		exit;
	}


	my $activities = {};
	my $running_out = 0;
	my $began = 0;
	my $show_report = 0;

	my $formatted_activities = {};

	foreach my $keys (sort keys %{$activities})
	{
		my $formatted = {};

		push @{$formatted->{date}}, $keys;

		$formatted->{'activities'}->{'activity'} = $activities->{$keys}->{'activity'};

		push @{$formatted_activities->{'activities'}->{'days_activity'}}, $formatted;

	}

	if ($show_report)
	{
		push @{$formatted_activities->{'report_available'}}, 1 ;
	}
	else
	{
		push @{$formatted_activities->{'report_available'}}, 0 ;
	}

	push @{$formatted_activities->{'show_report'}}, 1 if (($q->param('activity_report') || 0) == 1);

	my $report_information = XML::Simple::XMLout($formatted_activities, 'RootName' => "report");

	# Get the xml content.
	my $xml = $gs->GetObject($TEMPLATES{'content'}) || die "Failed to load XML template: $TEMPLATES{'content'}\n";

	$xml =~ s/<lang_blocks>/<root><lang_blocks>/;
	$xml .= XML::Simple::XMLout($ME, 'rootname'=>'member', 'noescape'=>1);
	$xml .= $report_information . '</root>';


	if ($q->param('xml'))
	{
		print "Content-type: text/plain\n\n$xml";
	}
	else
	{
		# Eliminate the language selector section for admin or VIP usage
		$conditionals{'langpref'} = 0 if (($VIP->{'id'} && $VIP->{'id'} ne $ME->{'id'}) || $q->cookie('operator'));

		# We only have one conditional, but leave this here for future growth since it was already here.
		foreach my $c (keys %conditionals)
		{
			unless ($conditionals{$c})
			{
				$xsl =~ s/conditional-$c-open.+?conditional-$c-close/ /sg;
			}
		}

		my $reward_total = $ME->{'redeem_bal'} + $ME->{'pending_bal'};
#		$xsl =~ s/%%pending_points%%/$ME->{pending_points}/;
		$xsl =~ s/%%cc_currency%%/$ME->{'cc_currency'}/;
		$xsl =~ s/%%reward_total%%/$reward_total/;
		$xsl =~ s/%%script_name%%/$q->script_name/eg;
		$xsl =~ s/%%name%%/$ME->{name}/g;
		$xsl =~ s/%%firstname%%/$ME->{firstname}/g;
		$xsl =~ s/%%alias%%/$alias/g;
		$xsl =~ s/%%id%%/$ME->{id}/g;
		$xsl =~ s/%%membertype%%/$MEMBER_TYPES{$ME->{'membertype'}}/g;
		$xsl =~ s/%%raw_membertype%%/$ME->{'membertype'}/g;
		$xsl =~ s/%%upline_name%%/$upline_contact->{name}/g;
		$xsl =~ s/%%upline_emailaddress%%/$upline_contact->{emailaddress}/g;
		$xsl =~ s/%%MESSAGING%%/Messaging_Hook()/e;
		$xsl =~ s/%%browser_language_pref%%/$gs->Get_LangPref()/eg;
		$xsl =~ s/%%my_tp_line_destination%%/($ME->{'my_tp_line_destination'} || '')/eg;

		print $q->header('-cookies'=>\@cookie, '-charset'=>'utf-8'), XML::local_utils::xslt($xsl, $xml);

	}

	return 1;
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-encoding'=>'utf-8', '-bgcolor'=>'#ffffff');
	print "$_[0]<br /><br />", scalar localtime();
	print $q->end_html();
}

sub Exit
{
	$db->disconnect if $db;

	exit;
}

sub Generate_Query
{
	my $SEARCH_COL = shift;
	return "SELECT DISTINCT
    		m.id,
			m.spid,
			m.emailaddress,
			m.alias,
			m.membertype,
			m.join_date,
			CASE WHEN pv.id IS NOT NULL THEN TRUE::BOOLEAN ELSE FALSE END AS priorv,
			CASE
				WHEN m.membertype = 'ag' THEN m.company_name
				ELSE m.firstname ||' '|| m.lastname
				END AS name,
			CASE
				WHEN m.membertype = 'ag' THEN m.company_name
				ELSE m.firstname
				END AS firstname
			FROM   members m
			LEFT JOIN priorvs pv
				ON pv.id=m.id
			WHERE 	$SEARCH_COL = ?";
}

sub Messaging_Hook
{
	return '' unless $ACTIVATE_MESSAGING;

	my $MOBJ = new MSYS;
	my $MBOX = $MOBJ->message_Check( $ME->{'id'}, $q->cookie('msysm'), $db ) || '';
	return $MBOX
}

sub Reward_Balances
{
	my $rv = ClubCash::Get_ClubCash_Amounts($ME->{'id'}, undef, {'db'=>$db});
	$ME->{'redeem_bal'} = sprintf('%.2f', $rv->{'redeem'});
	$ME->{'pending_bal'} = sprintf('%.2f', $rv->{'pending'});
	$ME->{'cc_currency'} = $rv->{'currency'};
}

#sub VipCK
#{
#	my $cookie = $q->cookie('AuthCustom_Generic');
#	return unless $cookie;
#
#	my ($id, $meminfo) = $gs->authen_ses_key( $cookie );
#	return $meminfo if $meminfo && $meminfo->{'membertype'} eq 'v';
#	return;
#}

###### 06/09/05 added a check for a persistent 'alias' cookie
###### 06/10/05 Added the Cleanup_XML sub to take care of &, <, and > in the member fields.
###### 	Added the Reward_Balances sub to get the reward points info for display.
###### 01/27/06 Added -charset=>'utf-8' to the header statements for the 8.1 DB upgrade.
###### 09/14/06 added pull-in and display of 'pending_points'
###### 08/04/10 added recognition of the alias parameter as well as 'a'
# 03/15/11 added the tracking of this page visit
# 04/13/11 added the ClubCash module and moved the acquisition of the RP data to using that
###### 05/09/12 added the od.pool stats to the member's mix of data
###### 07/10/12 added the language_pref to the list of interpolated placeholders in Do_Page
###### 09/09/13 porting back over to Clubshop from GI... also ripping out some cruft in the process
###### 11/18/13	modified the cookie creation to make the domain .clubshop instead of leaving it to default to www.clubshop
###### 08/07/14 changed language_pref to browser_language_pref, also added priorv to the data mix, added $ME to the XML data as <member />