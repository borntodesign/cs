#!/usr/bin/perl -w
###### mp_remove.cgi
######
###### Accepts a user emailaddress to effect a removal from the members_purge table.
######
######	created 12/16/2003	Karl Kohrt
######
######	last modified 07/02/2004	Karl Kohrt

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use DBI;
use lib ('/home/httpd/cgi-lib');
use G_S;
use DB_Connect;

$| = 1;

###### GLOBALS
our %TBL = (	'members'	=> 'members_purge');
our %TMPL = (	'form'	=> '10253',
		'confirm' => '10252');
our $template = 'form';	# The default template is the form.
our $redirect_link = "http://www.clubshop.com/cgi-bin/memberapp.cgi/001?membertype=m";

our $message = '';		# The message holder for the templates.
our $db = '';
our $q = new CGI;

our $action = $q->param('action') || $q->param('a') || '';
our $emailaddress = lc($q->param('emailaddress') || $q->param('e') || '');
$emailaddress =~ s/^\s*//;
$emailaddress =~ s/\s*$//;

unless ($db = DB_Connect('mp_remove.cgi')){exit}
$db->{RaiseError} = 1;

# Unless we have an email address.
unless ($emailaddress)
{
	$message .= "To process your removal, enter your email address below.\n";
}
# If we have a bad email address.
elsif (G_S::Check_Email($emailaddress) != 1)
{
	$message .= "The email address you submitted doesn't appear to be valid.\n";
}
# If we have a good email address.
elsif ( $action )
{
	# If they have clicked the application link from the email.
	if ( $action eq 'a' )
	{
		if (my ($firstname, $lastname, $emailaddress) = Get_Purge_Record())
		{
			$redirect_link .= "&firstname=$firstname";
			$redirect_link .= "&lastname=$lastname";
			$redirect_link .= "&email=$emailaddress";
			$redirect_link .= "&email_check=$emailaddress";
		}						
		$template = '';	
	}
	# If they have clicked the remove link from the email 
	# or clicked the remove button on the mp_remove form.
	elsif ( $action eq 'r' )
	{
		$template = 'confirm';	
	}
	Delete_Purge_Record();
}
# If there is a template - either the form or the confirmation.
if ( $template )
{
	Do_Page($template);
}
# Otherwise we are passing through to the member application.
else
{
	 print $q->redirect($redirect_link);
}
$db->disconnect;

exit;

#################################
###### Start subroutines here.
#################################

###### Delete the record from the members_purge table.
sub Delete_Purge_Record
{
	my $qry = "	DELETE FROM $TBL{members}
			WHERE emailaddress = ?";

	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($emailaddress);

	if ( $rv >= 1 )
	{
		$message = "We have removed your contact information from our system.";
	}
	else
	{
		$message = "The address '$emailaddress' was not found in our system.<br>
				It may have already been removed or may be spelled differently.<br>";
	}
	$sth->finish();
}

###### Display the form.
sub Do_Page
{
	my $template = shift;
	my $PAGE = G_S::Get_Object($db, $TMPL{$template}) || die "Couldn't retrieve the template: $TMPL{$template}\n";
	$PAGE =~ s/%%emailaddress%%/$emailaddress/;
	$PAGE =~ s/%%message%%/$message/;
	print $q->header(), $PAGE;	
}

###### Get the data from the members_purge table.
sub Get_Purge_Record
{
	my $qry = "	SELECT firstname,
				lastname,
				emailaddress
			FROM $TBL{members}
			WHERE emailaddress = ?";
	my $sth = $db->prepare($qry);
	my $rv = $sth->execute($emailaddress);
	my @data = $sth->fetchrow_array();
	return @data;
}

###### 07/02/04 Added a conditional to the Get_Purge_Record call so that we would avoid uninitialized
######  		errors in the case where the member had already been removed from members_purge.
