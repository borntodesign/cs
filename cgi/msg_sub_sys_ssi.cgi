#!/usr/bin/perl -w
###### original design by Stephen Martin
###### last modified 10/21/04	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use MSYS;
use CGI;

my $q  = new CGI;
my $path = $q->path_info;
my $MBOX = '';

###### validate the user
my ($UID) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### if the UID has any non-digits it means that its bad/expired whatever
if ( !$UID || $UID =~ /\D/ )
{
	###### do nothing but neutralize the value if any
	$UID = '';
}
else
{
	###### get the member pa cookie?
}

if ($UID)
{	
	my $MOBJ = new MSYS;
	$MBOX = $MOBJ->message_Check( $UID, $q->cookie('msysm') ) || '';
}

if ($q->path_info)
{
	$MBOX =~ s#"#\\"#g;
	$MBOX =~ s#\n#\\n#g;
	print $q->header('text/javascript'), "var _msg = \"$MBOX\"";
}
else
{
	print $q->header, $MBOX;
}

exit;

1;

###### 05/30/03 altered the DB connection parameter to NOT connect under its own name
###### also ripped out the admin stuff since it could never have worked because the hash used was never initialized
###### see the .1 version for the original
###### 05/25/04 revised to spit out a javascript portion if called with a path
###### this will allow the system to be embedded in static pages using javascript
###### also ripped out all the extra stuff that had no real value
###### 10/21/04 I cannot believe that this thing has been in place since may without 'use CGI;'
###### and there haven't been any complaints