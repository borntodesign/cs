#!/usr/bin/perl -w
###### md_research.cgi
######
###### A script to generate a submission form for Marketing Directors to submit research
######  selections to the database. Also generates an email to the admin of this research program.
######
###### created: 04/07/06	Karl Kohrt
######
###### last modified: 

use strict;
use DBI;
use CGI;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw ($LOGIN_URL);
require XML::Simple;
require XML::local_utils;

###### CONSTANTS
our $ADMIN_ADDRESS = "mallsales\@dhs-club.com";
our %TMPL = (	'xml'		=> 10558,
		'md_form'	=> 10559
		);	

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $ME = $q->script_name;
our $xs = new XML::Simple(NoAttr=>1);
our $db = '';
our $messages = '';
our $member_id = '';
our $mem_info = '';
our $member_details = ();
our @vendor_types = ();
our $action = ( $q->param('submit') || '' );
our $cgiini = 'md_research.cgi';
our $login_type = '';
our $login_error = 0;
our $no_form = 0;		# A 0 indicates we'll show the selection part of the form.
our $cats_for_email = '';

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

# Get the xml right away because we need the $MIN_CATS value.
our $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to load template: $TMPL{xml}\n";
our $MIN_CATS = $xml;
$MIN_CATS =~ s/^(.*?)min_num_cats>|<\/min_num_cats(.*?)$//gs;

# If they are logged in with a password.
if ( $login_type eq 'auth' )
{
	$member_details = Get_Member_Details();
	# If this is an update.
	if ( $q->param('action') && $q->param('action') eq 'update' )
	{
		if ( Update_Preferences() ) 
		{
			$no_form = 1;
			Email_Admin();
		}
	}
}
# If they are not logged in.
else
{
	# Let them know they must be logged in to use this form.
	$messages .= qq#\n	<span class="bad">
				<xsl:value-of select="//messages/must_login" />
				<xsl:text> </xsl:text>
				<a href="$LOGIN_URL?destination=$ME">
				<xsl:value-of select="//messages/login_here" />
				</a>
				</span><br/>\n#;
	$no_form = 1;
}

Do_Page();

$db->disconnect if $db;

exit;

#####################################
###### Begin Subroutines Here. ######
#####################################

###### Display record(s) to the browser.
sub Do_Page
{
	# Add the user info for page personalization.
	if ( $mem_info )
	{
		my $user_xml = $xs->XMLout($mem_info, RootName=>'user') ;
		$xml .= $user_xml;
	}

	my $xsl = G_S::Get_Object($db, $TMPL{md_form}) || die "Couldn't open $TMPL{md_form}";
	# If flagged as no form, comment out the submission form.
	if ( $no_form  )
	{
		$xsl =~ s/conditional-form-start.+?conditional-form-stop//sg;
	}

	# Build out the xml.
	$xml = "<base>\n"
		. $xml . "\n"
		. "\n</base>\n";
	if ($q->param('xml')){ print "Content-type: text/plain\n\n$xml" }
	else
	{
		if ( $messages )
		{
			$messages = qq#<p>$messages</p>#;
		}
		$xsl =~ s/%%messages%%/$messages/g;
		$xsl =~ s/%%ME%%/$ME/g;
		$xsl =~ s/%%LOGIN_URL%%/$LOGIN_URL/g;

		my $xml = XML::local_utils::xslt($xsl, $xml);

		print $q->header(-charset=>'utf-8'), $xml;
	}
}

##### Prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), "<br/>$_[0]<br/>";
}

###### Generate e-mail to the Admin.
sub Email_Admin
{
	use MIME::Lite;
	my $tmp_mall = $q->param('mall');

	# Build the message.
	my $email_message = qq!These are the mall category selections for:\n
	Name:\t\t$mem_info->{firstname} $mem_info->{lastname}
	DHS ID:\t\t$mem_info->{alias}
	Email:\t\t$mem_info->{emailaddress}
	Country:\t$mem_info->{country_name}

	Mall:\t\t$tmp_mall
	Categories:\n$cats_for_email!;

	# Build the email.
	my $message = MIME::Lite->new(
           	From     => 'md_research.cgi',
           	To       => $ADMIN_ADDRESS,
           	Subject  => 'Marketing Director - Product Venues',
           	Type     => "text/plain",
           	Encoding => '8bit',
           	Data     => $email_message);
	$message->attr("content-type"         => "text/plain");
    	$message->attr("content-type.charset" => "utf-8");

	# Send the message
	$message->send;

	return 1;
}

###### Get the info stored in the cookie.
sub Get_Cookie
{	
	# If this is an admin user.		 
	if ( $q->param('admin') )
	{
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator'); my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
		{
			$login_error = 1;
		}
		# Mark this as an admin user.
		$login_type = 'admin';
	}
	# If this is a member.		 
	else
	{
		unless ($db = DB_Connect($cgiini)) {exit}
		$db->{RaiseError} = 1;

		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

		if ( $member_id && $member_id !~ m/\D/ )
		{
			$login_type = 'auth';
		}
	}
}

###### Get the member details to pre-populate the form.
sub Get_Member_Details
{
	my $tmp_hash = $db->selectrow_hashref("	SELECT r.cats,
								r.stamp AS cats_stamp,
								c.country_name
							FROM 	members m
								INNER JOIN tbl_countrycodes c
									ON c.country_code = m.country
								LEFT JOIN md_research r
									ON r.id = m.id
							WHERE m.id = $member_id") || ();
	foreach ( keys %{$tmp_hash} )
	{
		$mem_info->{$_} = $tmp_hash->{$_};
	}
	return 1;
}

###### Update the MD's preferences.
sub Update_Preferences
{
	my $cats = '';
	my $num_cats = 0;

	# Get the submitted categories.
	foreach ( $q->param() )
	{
		if ( $_ =~ m/t\d+$/ )
		{
			$num_cats++;
			$cats .= $_ . ',';
			$cats_for_email .= "\t\t". $q->param($_) . "\n";
		}
	}
	chop $cats if $cats;	# Remove the trailing comma.

	# If not enough choices were made, error out.
	if ( $num_cats < $MIN_CATS )
	{ 
		$messages .= qq#\n	<hr/><span class="bad">
					<xsl:value-of select="//messages/no_selections" />
					</span><br/><hr/>\n#;
		return 0;
	}

	my $qry = '';
	# If we found an existing md_research record.
	if ( $mem_info->{cats_stamp} )
	{
		$qry = "	UPDATE md_research
				SET	cats = ?,
					mall = ?,
					stamp = NOW()
				WHERE 	id = ?;";
	}
	else
	{
		# If we didn't find a record , insert one.
		$qry = "	INSERT INTO 	md_research (
					cats,
					mall,
					stamp,
					id )
				VALUES (
					?,?,
					NOW(),
					? );";
	}
	my $sth = $db->prepare($qry);
	my $rv = 0;

	# If we were able to create the record.
	if ( $sth->execute($cats, $q->param('mall'), $member_id) )
	{					
		$rv = 1;
		$messages .= qq#\n	<span class="good">
					<xsl:value-of select="//messages/success" />
					</span>
					<br/><br/>
					<span class="link"> 
					<a href="/cgi/sales-rpt.cgi">
					<xsl:value-of select="//lang_blocks/current"/>.
					</a>
					</span>\n#;
	}
	# If no record was created.
	else
	{					
		$messages .= qq#\n	<br/><span class="bad">
					<xsl:value-of select="//messages/failure" />
					</span>
					<br/><br/>
					<span class="link"> 
					<a href="/cgi/sales-rpt.cgi">
					<xsl:value-of select="//lang_blocks/current"/>.
					</a>
					</span><br/>\n#;
	}
	$sth->finish;
	return $rv;
}


