#!/usr/bin/perl -w
###### 11/01/2002	Stephen Martin
###### (basically rewritten) 09/07/12	Bill MacArthur
###### last modified: 05/09/13	Bill MacArthur

use Data::Recursive::Encode;
use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use Mail::Sendmail;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S;
use Template;
require XML::LibXML::Simple;

###### GLOBALS
my $TT = Template->new();
my %TMPL = (
	'login' => 10890,
	'welcome' => 10889,
	'upline-notice'=> 10043,
	'lang_blocks'=> 10967,
	'welcome-email'=> 10039
);
my $q = new CGI;
my $MYURL = $q->script_name();
my ($MEMBER, @errors, $db, $lang_blocks) = ();
my $gs = G_S->new({'db'=>\$db});
my $alias = uc( $q->param('alias') ) || '';
my $password = $q->param('password') || '';

my $action = $q->param('action') || '';

exit unless $db = DB_Connect('NetShopUpgrade.cgi');
$db->{'RaiseError'} = 1;

LoadLangBlocks();

if ( $action eq "Upgrade" )
{
	process_upgrade();
	exit;
}

Login();

###### ###### ###### ###### ######

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub process_upgrade
{
    unless ($alias)
    {
    	push @errors, $lang_blocks->{'err_idreq'};
    }
    unless ( $password )
    {
    	push @errors, $lang_blocks->{'err_pwreq'};
    }

	Login() if @errors;
	
	$MEMBER = get_member_details($alias, $password);

    unless ($MEMBER->{'id'})
    {
    	push @errors, $lang_blocks->{'err_badlogin'};
    	Login();
    }

    unless ( $MEMBER->{'membertype'} eq "s" )
    {
        push @errors, $lang_blocks->{'err_incorrect_mtype'};
        Login();
    }

    _perform_upgrade();

    my $TMPL = $gs->GetObject( $TMPL{'welcome'} ) || die "Couldn't retrieve the template object: $TMPL{'welcome'}";

    $TMPL =~ s/%%alias%%/$MEMBER->{'alias'}/g;
    print $q->header('-charset'=>'utf-8'), $TMPL;

    $db->disconnect;

    exit;

}

sub get_member_details
{
    my ($id, $password) = @_;
	my @args = $id;
	
	my $qry = "SELECT* from members WHERE " . ($id =~ m/\D/ ? 'alias=?' : 'id=?');
	if ($password)
	{
		$qry .= ' AND password=?';
		push @args, $password
	}
	

    return $db->selectrow_hashref($qry, undef, @args);
}

sub _perform_upgrade
{
	# make sure their current sponsor can still be there sponsor once their membertype changes
	my ($spid) = $db->selectrow_array("SELECT find_qualified_sponsor($MEMBER->{'spid'}, 'm')");
	
	my $SPONSOR = get_member_details( $spid );
	
    $db->do("UPDATE members SET spid=$spid, membertype='m', memo= ? WHERE id = ?", undef, $MYURL, $MEMBER->{'id'} );

	###### Find 1st Partner sponsor of member
	my $VIP    = $gs->GetPartnerSponsor($spid);
#	my $COACH  = $gs->GetCoach($spid);
	my $COACH  = $db->selectrow_hashref("SELECT * FROM my_coach($MEMBER->{'id'})");

	MailUpline($VIP, $COACH);

    my $MEMBERLETTER = $gs->GetObject( $TMPL{'welcome-email'} ) || warn "Couldn't retrieve the template object.";
	if ($MEMBERLETTER)
	{
		my $SPNAME = $VIP->{'firstname'} . " " . $VIP->{'lastname'};
		my $TC = "$COACH->{'firstname'} $COACH->{'lastname'} $COACH->{'emailaddress'}";
		$MEMBERLETTER =~ s/%%firstname%%/$MEMBER->{'firstname'}/g;
		$MEMBERLETTER =~ s/%%alias%%/$MEMBER->{alias}/g;
		$MEMBERLETTER =~ s/%%spname%%/$SPNAME/g;
		$MEMBERLETTER =~ s/%%emailaddress%%/$SPONSOR->{'emailaddress'}/g;
		$MEMBERLETTER =~ s/%%TC%%/$TC/g;
		mailto( "$MEMBER->{'firstname'} <$MEMBER->{'emailaddress'}>", $MEMBERLETTER, $lang_blocks->{'email_subj1'} );
	}
}

sub LoadLangBlocks
{
	$lang_blocks = $gs->GetObject($TMPL{'lang_blocks'}) || die "Failed to load template: $TMPL{'lang_blocks'}";
	$lang_blocks = Data::Recursive::Encode->encode_utf8( XML::LibXML::Simple::XMLin($lang_blocks, 'NoAttr'=>1) );
}

sub Login
{
	###### Obtain Login Template
	
	my $TMPL = $gs->GetObject( $TMPL{'login'} ) || die "Couldn't retrieve the template object: $TMPL{'login'}";
	my $data = {
		'SELF'=>$MYURL,
		'alias'=>$alias,
		'password'=>$password,
		'errors'=>\@errors,
		'lang_blocks'=>$lang_blocks
	};

	print $q->header('-charset'=>'utf-8');
	$TT->process( \$TMPL, $data) || print $TT->error;
	Exit();
}

sub mailto
{
    my ($email, $content, $subject, $args) = @_;
#warn "email: $email";
#warn "content: $content";
#warn "sub: $subject";
#warn "cc: $args->{'cc'}" if $args->{'cc'};
	return unless ($email && $content && $subject);

    ###### send the email.

    my %mail = (
        'To'      => $email,
        'Subject' => $subject,
        'From'    => 'Membership Support <support@dhs-club.com>',
        'Message' => $content,
        'Content-Type' => 'text/plain; charset="utf-8"'
    );

	$mail{'cc'} = $args->{'cc'} if $args->{'cc'};
	
	unless ( sendmail(%mail) )
	{
 		warn "Unable to send email from $0 to $email : reason $Mail::Sendmail::error\n";
	}
}

sub MailUpline
{
	my ($VIP, $COACH) = @_;
	
	##### Retrieve and construct vip / team coach letter
	my $VIPLETTER = $gs->GetObject( $TMPL{'upline-notice'} ) || warn "Couldn't retrieve the template object: $TMPL{'upline-notice'}.";

     $VIPLETTER =~ s/%%vipfirstname%%/$VIP->{'firstname'}/g;
     $VIPLETTER =~ s/%%mid%%/$MEMBER->{'alias'}/g;
     $VIPLETTER =~ s/%%mfirst%%/$MEMBER->{'firstname'}/g;
     $VIPLETTER =~ s/%%mlast%%/$MEMBER->{'lastname'}/g;
     $VIPLETTER =~ s/%%memail%%/$MEMBER->{memail}/g;

     mailto("$VIP->{'firstname'} <$VIP->{'emailaddress'}>", $VIPLETTER, $lang_blocks->{'email_subj2'}, {'cc'=>"$COACH->{'firstname'} <$COACH->{'emailaddress'}>"});
}

# 07/20/12 basically a rewrite in place
###### 05/09/13 employed Data::Recursive::Encode->encode_utf8() to turn off the lang pack's UTF-8 flags so that TT can properly render it
