#!/usr/bin/perl -w
###### gainTP.cgi
###### A simple interface for gaining a Trial Partner position
###### Although we could be called as a standalone page, we are expecting to be pulled into a "popup" box of some sort, like greybox or something like that
###### See an overview of this script functionality at the bottom...
###### created: 10/15/13	Bill MacArthur
###### last modified: 12/22/14	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Template;
use JSON;
use XML::LibXML::Simple;
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;
use wrap;
require TrialPartner;

my %TMPL = (
	'wrapper' => 10962,
	'toLogin' => 11033,
	'language_pack' => 11034,
	'congrats' => 11035,
	'error'	=> 11036
);

my ($db, $member, $lb, @cookies);

my $gs = G_S->new({'db'=>\$db});
my $q = new CGI;
my $TT = new Template;

my $json = BuildJSON();

exit unless $db = DB_Connect('gainTP.cgi');
$db->{'RaiseError'} = 1;

$db->{'AutoCommit'} = 1;	###### be sure this is set to 1 for normal operation... having it off makes it easy to verify template presentations without making permanent data changes

my $TP = TrialPartner->new({'db'=>$db, 'cgi'=>$q});
LoadLB();

my $wrap = wrap->new({
	'template'=> $TMPL{'wrapper'},
	'db'=> $db,
	'gs'=> $gs
});


my ($id, $mem_info) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
unless ( $id && $id !~ /\D/ )
{
	$wrap->lang_nodes({'title'=>$lb->{'title'}});
	Do_Page($TMPL{'toLogin'});
}
$member = $TP->GetMe($id);
if (($member->{'membertype'} ne 'm' && $member->{'membertype'} ne 's') || $member->{'seq'})	# Dick wanted to treat these differently and so we have a generic error page :(
{														# if they are not an Affiliate, or they are already a Trial Partner (the seq is already set)
	Do_Page($TMPL{'error'});
}

# special pre-handling for those in the 01 container
#PreProcess01() if $member->{'spid'} == 1;
$TP->PreProcess($member);

$member = $TP->CreateTrialPartnerPosition($id);					# this method will have the fresh membertype in it
Err($lb->{'error1'}) unless $member;

push @cookies, $gs->Create_AuthCustom_Generic_cookie($member);	# we will create a new full login cookie with the fresh membertype
$wrap->lang_nodes({'title'=>$lb->{'congrats_title'}});
Do_Page($TMPL{'congrats'});


###### beginning of sub-routines

sub BuildJSON
{
	my $j = {'LOGIN_URL'=>$LOGIN_URL, 'apbc'=>'/cgi/apbc'};
	return encode_json($j);
}

sub Do_Page
{
	my $tmpl = shift;
	$tmpl = $gs->GetObject($tmpl) || warn "Failed to load template: $tmpl";
	print $q->header('-charset'=>'utf8', '-cookies'=>\@cookies);				# we will give 'em a new login cookie with the right membertype in it if they have gone TP
	my $tmp = ();
	my $rv = $TT->process(
		\$tmpl, {
			'LOGIN_URL' => $LOGIN_URL,
			'script_name' => $q->script_name,
			'urls' => $json,
			'member' => $member,
			'lb' => $lb
		},
		\$tmp);
	$tmp = $TT->error unless $rv eq '1';
	print $wrap->wrap({'content'=>$tmp});
	
	Exit();
}

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-bgcolor'=>'#ffffff', '-encoding'=>'utf-8');
	print $_[0];
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub LoadLB
{
	$lb = $gs->GetObject($TMPL{'language_pack'}) || warn "Failed to load language pack template: $TMPL{'language_pack'}";
	$lb = XMLin($lb);
}

#sub PreProcess01
#{
#	# look up the container we need to transfer them to and make the transfer
#	my ($nuspid) = $db->selectrow_array("SELECT coop.tp_container_assignment($member->{'id'})") || Err($lb->{'error1'}); # I do not see any cases where this could happen, but go figure....
#	$db->do("UPDATE members SET spid= $nuspid, memo= 'Moved to this container for Trial Partner position by gainTP.cgi' WHERE id= $member->{'id'}");
#}

=head5 Overview

	The idea is that we are called and we try to identify the party by means of the full login cookie.
	If they are not logged in, then we will load the 'toLogin' template.
			
	Little can go wrong if we are logged in, so upon success, we will present a congratulations screen, which should be very minimal since we may be loaded in a popup.
	If we already have a position or do not qualify, then we will load an alternate text screen.
	
	If we have links in the congratulations content, they should be activated through javascript so that we can determine if we are in a popup or not.
	If so, we need to do something besides load new content into the popup.
	
=cut

__END__

# 01/06/14 made it so that Shoppers can do this too
# 12/09/14 revised PreProcess01 to simply PreProcess since we now have coops outside of 01 that need to reassign TP upgrades prior to making them TPs
12/22/14	moved PreProcess() into the TrialPartner class for use by appx