#!/usr/bin/perl -w
################################################################
# marketing_questionnairs.cgi
#
# The interface for Marketing Questionnairs
# This script started as a copy of "pwp-edit.cgi" and was stripped
# and modified to handle Marketing Questionnairs (survey questions).
#
# Deployed: 06/08
# Author: Keith Hasely
# Version: 0.1
# last updated: 05/18/15	Bill MacArthur
#               
################################################################

################################################################
# Includes
################################################################
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use CGI;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use ADMIN_DB;
use XML::Simple;
use XML::local_utils;
use CGI::Carp qw(fatalsToBrowser set_message);
require Specials;
use MailTools;
use Data::Dumper;
binmode STDOUT, ":encoding(utf8)";

################################################################
# Object Instanciations, Variables, Arrays, and Hashes
################################################################
our $q = CGI->new();
$q->charset('utf-8');
our $xs = XML::Simple->new('NoAttr'=>1);
our $gs = new G_S;

our $ME = $q->url;
$ME =~ s/_submit//;

our	$userid = 0;
our $spid = 0;
our $membertype = '';
our $db = ();

our $prefered_language = $q->param('lang')?$q->param('lang'):'en';

my $questionnaires_id = defined $q->param('questionnaires_id')?$q->param('questionnaires_id'):1;

my $step = defined($q->param('step'))? $q->param('step'):'';
my $mdata = '';


###############################
# '$xml_configs'
# 'content_xml' Initial value only
# 'language_pref_xml' The Language Code
# 'xml_err' A simple error handler template (has ClubAdvantage graphic embedded :(
#
# NOTE: We my change this in the near
#		future.  Maybe have the
#		Information serverd up from
#		a configuation file, or the DB.
###############################
my $xml_configs = {

	'1'=>{
		'questionnaires_admin'	=> 0,
		'questionnaires_id'	=> 1,
		'content_xml'		=> 10600,
		'xsl'			=>	10603
	}

};

################################################################
# Subroutine(s)
################################################################

sub __availableLanguages
{



}
###############################
# Prepare the page and outputs
# it to the browser.
#
# Param		hash
# 			int		xsl						The style sheet.
#			int		content_xml				The content XML file.
#			int		questionnaires_id		The questionnaires ID
#			bool	questionnaires_admin	A flag for the admin display.
# Return	string
###############################
sub displayPage
{
	my $args = shift || {};

	my $return = '';
	my $xsl = '';
	my $xml = '';
	my $profile_xml = '';
	my $content_xml = '';
	my $member_xml = '';
	my $admin_xml = '';
	my $questions_xml = '';
	my $language_preference = $prefered_language;
	my $query_mall_categories = '';
	my %users_answers = ();
	my %users_answered_questions = ();
	my $questionnaire_incomplete = 0;
	my $notice_xml = '';
        my $member_info = $mdata;
#	my $member_info = $db->selectrow_hashref("
#				SELECT
#					alias,
#					firstname,
#					lastname,
#					address1,
#					address2,
#					city,
#					state,
#					postalcode,
#					country,
#					emailaddress,
#					other_contact_info
#				FROM
#					members
#				WHERE 	id = ?", undef, $userid);

	$member_info->{other_contact_info} =~ s/&/&amp;/;


	# Create the XML nodes for the user information.
	$member_xml = $xs->XMLout($member_info, RootName=>'user');



	# Get the enterpreted questionnaire language
	$content_xml = $gs->Get_Object($db, $args->{content_xml}, $prefered_language) ||
		die "Failed to load XML template: $args->{content_xml}\n";

	# This query checks for questions that have not been answered, and returns
	# a resultset of question ids that have not been answered
	my $sth = $db->prepare("
							SELECT
								qq.id
							FROM
								questionnaires_questions AS qq
								JOIN
								questionnaires_user_status AS qus
								ON
								qq.questionnaires_id = qus.questionnaires_id
							WHERE
								qq.questionnaires_id = ?
								AND
								qus.userid = ?
								AND
								qq.id NOT IN (
											SELECT
												DISTINCT(qa.questionnaires_questions_id) AS questionnaires_questions_id
											FROM
												questionnaires_user_answers AS qua
												JOIN
												questionnaires_answers AS qa
												ON
												qa.id = qua.questionnaires_answers_id
												JOIN
												questionnaires_questions AS qq
												ON
												qq.id = qa.questionnaires_questions_id
											WHERE
												qq.questionnaires_id =  ?
												AND
												qua.members_id = ?
											)
							");

	$sth->execute($args->{questionnaires_id}, $userid, $args->{questionnaires_id}, $userid);
	##################################################
	# Check for incompleted questions, and add the "answered='no'" attribute
	# to the element.
	# NOTE: We may want to use the XML::DOM library in the furure instead of
	#		regular expressions.
	##################################################
	while (my $row = $sth->fetchrow_hashref)
	{
		$questionnaire_incomplete = 1;
		$content_xml =~ s/(<question\s+?question_id="$row->{id}".*?)>/$1 answered="no">/;
	}




	$sth = $db->prepare("
						SELECT
							qua.questionnaires_answers_id,
							qua.value,
							qa.questionnaires_questions_id
						FROM
							questionnaires_user_answers AS qua
							LEFT JOIN
							questionnaires_answers AS qa
							ON
							qua.questionnaires_answers_id = qa.id
							JOIN
							questionnaires_questions AS qq
							ON
							qq.id = qa.questionnaires_questions_id
						WHERE
							qq.questionnaires_id = ?
							AND
							qua.members_id = ?
							");


	$sth->execute($args->{questionnaires_id}, $userid);
	##################################################
	# Check for answered questions, and mark them to be checked in the interface
	# by adding the "default='yes'" attribute to the element.
	# NOTE: We may want to use the XML::DOM library in the furure instead of
	#		regular expressions.
	##################################################
	CHECK_ANSWERS: while (my $row = $sth->fetchrow_hashref)
	{

		$users_answers{$row->{questionnaires_answers_id}} = $row->{value};

		if (! $row->{questionnaires_questions_id})
		{
			next CHECK_ANSWERS;
		}

		$users_answered_questions{$row->{questionnaires_questions_id}} = $row->{questionnaires_questions_id};
		$content_xml =~ s/(<answer\d+\s*key="$row->{questionnaires_answers_id}"\s*question_id="$row->{questionnaires_questions_id}".*?)>/$1 default="yes">/ if ($row->{value});
	}

	#$content_xml = decode_utf8($content_xml) unless Encode::is_utf8($content_xml);

	# This handles questionnaire specific tasks, for instance questionnaire 1 needs the
	# categories in the mall, so they are dynamically generated.
	if($args->{content_xml} eq "10600")
	{


						##################################################
						# If this is questionnaire 1 insert the mall
						# categories as a child of the "answers" node
						# question_id 4.
						# Query the database for the store categories
						# Build the xml
						# Search for the null node to replace
						# Replace the null node for question id "4".
						##################################################
						$query_mall_categories =<<EOS;
										SELECT
											COALESCE(mct.catname,mc.catname) AS catname,
											qa.id AS catid
										FROM
											mallcats AS mc
											JOIN
											questionnaires_answers AS qa
											ON
											mc.catid = qa.mallcats_catid
											JOIN
											questionnaires_questions AS qq
											ON
											qq.id = qa.questionnaires_questions_id
											LEFT JOIN
											mallcats_translated AS mct
											ON
											mct.catid = qa.mallcats_catid
											AND
											mct.language_code = '$language_preference'
										WHERE
											qq.questionnaires_id = 1
											AND
											qa.questionnaires_questions_id = 4
											AND
											qa.mallcats_catid <> 0
										ORDER BY
											mct.catname

EOS


						my $store_categories =qq(<answers question_id="4">\n);


						$sth = $db->prepare($query_mall_categories);
						$sth->execute;
						my $counter =1;

						while( my $row = $sth->fetchrow_hashref ())
						{
							$row->{catname} =~ s/&/&amp;/g;
							$store_categories .= qq(\t\t\t\t<answer$counter key="$row->{catid}" question_id="4");
							$store_categories .= qq( default="yes" ) if (defined $users_answers{$row->{catid}});
							$store_categories .= ">";
							$store_categories .= $row->{catname};
							$store_categories .= qq(</answer$counter>\n);
							$counter++;
						}


						$store_categories .= qq(\t\t\t</answers>);


						$content_xml =~ s/<answers\s+question_id="4"\s*\/>/$store_categories/;


						##################################################



						##################################################
						# If this is questionnaire 1 insert the hobby
						# categories as a child of the "answers" node
						# question_id 8 (Hobbies).
						# Query the database for the hobby categories
						# Build the xml
						# Search for the null node to replace
						# Replace the null node for question id "8".
						##################################################
						$query_mall_categories =<<EOS;
										SELECT
											COALESCE(mct.catname,mc.catname) AS catname,
											qa.id AS catid
										FROM
											mallcats AS mc
											JOIN
											questionnaires_answers AS qa
											ON
											mc.catid = qa.mallcats_catid
											JOIN
											questionnaires_questions AS qq
											ON
											qq.id = qa.questionnaires_questions_id
											LEFT JOIN
											mallcats_translated AS mct
											ON
											mct.catid = qa.mallcats_catid
											AND
											mct.language_code = '$language_preference'
										WHERE
											qq.questionnaires_id = 1
											AND
											qa.questionnaires_questions_id = 8
											AND
											qa.mallcats_catid <> 0
										ORDER BY
											mct.catname
EOS


						$store_categories =qq(<answers question_id="8">\n);


						$sth = $db->prepare($query_mall_categories);
						$sth->execute;
						$counter =1;

						while( my $row = $sth->fetchrow_hashref ())
						{
							$row->{catname} =~ s/&/&amp;/g;
							$store_categories .= qq(\t\t\t\t<answer$counter key="$row->{catid}" question_id="8" js="uncheckNone('50050')");
							$store_categories .= qq( default="yes" ) if (defined $users_answers{$row->{catid}});
							$store_categories .= ">";
							$store_categories .= $row->{catname};
							$store_categories .= qq(</answer$counter>\n);
							$counter++;
						}

						$store_categories .= qq(\t\t\t\t<answer$counter key="50050" question_id="8"  element_id="50050" js="uncheckMyFriends(this)");
						$store_categories .= qq( default="yes" ) if (defined $users_answers{50050});
						$store_categories .= qq(>None</answer$counter>\n);

						$store_categories .= qq(\t\t\t</answers>);


						$content_xml =~ s/<answers\s+question_id="8"\s*\/>/$store_categories/;



						##################################################


						my @status = __updateUsersPoints({questionnaires_id=>$args->{questionnaires_id}});

						if (!$status[1]){
							if($member_info->{other_contact_info})
							{
								$content_xml =~ s/<answer1 key="50086" question_id="9" type="radio">/<answer1 key="50086" question_id="9" type="radio" default="yes">/;
							} else {
								$content_xml =~ s/<answer2 key="50087" question_id="9" type="radio">/<answer2 key="50087" question_id="9" type="radio" default="yes">/;
							}
						}

						$store_categories = qq(<answers question_id="10">\n);
						$store_categories .= qq(\t\t\t\t<answer1 key="$member_info->{other_contact_info}" question_id="10_50090" type="text"></answer1>);
						$store_categories .= qq(</answers>\n);

						$content_xml =~ s/<answers\s+question_id="10"\s*\/>/$store_categories/;



						if($questionnaire_incomplete)
						{
							$notice_xml =<<EOS;
											<user_notices>
												<incomplete>yes</incomplete>
											</user_notices>
EOS

						}

		if($args->{questionnaires_admin})
		{


			 my $users_completion_times = $db->selectrow_hashref("
							SELECT
								CASE WHEN qus.completed=NULL THEN NULL ELSE q.rewards_points END AS rewards_points,
								to_char(qus.completed, 'Month DD, YYYY') AS completed,
								to_char(qus.updated, 'Month DD, YYYY') AS updated,
								to_char(qus.created, 'Month DD, YYYY') AS created,
								'NO' AS submit
							FROM
								questionnaires AS q
								JOIN
								questionnaires_user_status AS qus
								ON
								q.id = qus.questionnaires_id
							WHERE
								qus.userid = ?", undef, $userid);

			if (!$users_completion_times)
			{
				$users_completion_times = {
											'created' => '',
											'rewards_points' => 'The member has not filled out this survey.',
											'submit' => 'NO',
											'updated' => '',
											'completed' => ''
											};
			}

			$admin_xml = $xs->XMLout($users_completion_times, RootName=>'admin');

		}


	}




	if(scalar(__updateUsersPoints({questionnaires_id=>$args->{questionnaires_id}})))
	{
		$notice_xml .= "<display_no_points_message>yes</display_no_points_message>";
	}

	###############################
	# Combine the applicatble
	# pieces of the XML document.
	###############################
	$xml =<<EOT;
<?xml version="1.0" encoding="UTF-8" ?>
		<base>
			$admin_xml
			$member_xml
			$content_xml
			$notice_xml
			<timestamp>${\scalar localtime()}</timestamp>
			$profile_xml
		</base>
EOT

#	$xsl = $gs->Get_Object($db, $args->{xsl}) ||
#		die "$@\nFailed to load XSL template: $xsl\n";

	#Output the XML  Document
	#$return = $q->header(-charset=>'utf-8', -expires=>'now');
	#$return .= XML::local_utils::xslt($xsl, $xml);

	#$return = $q->header(-type=>'text/html');
	$return = $q->header(-charset=>'UTF-8', -expires=>'now', -type=>'text/xml');
	$return .= $xml;

	return $return;

}
###############################

sub exitScript
{
	$db->disconnect if ($db);
	exit;
}
###############################

###############################
# Output an error message for the user
# then exit the script cleanly
#
# Param		hash
# 			string message			The mesage to output before exiting.
# Return	null
###############################
sub errorExitScript
{
	my $message = shift || {};
	print $q->header(-charset=>'utf-8', -expires=>'now');

	print '<error>' . $message . '</error>';

	exitScript();
}
###############################

###############################
# Insert the answers into the
# database.
#
# Param		hash
# 			int xsl					The style sheet.
#			int content_xml			The content XML file.
#			int questionnaires_id	The questionnaires ID
# Return	null
###############################
sub insertUpdateUserAnswers
{
	#If the user already has answers
	my $args = shift || {};
	my @users_submitted_questions_answers = undef;
	my $rewards_points = 0;
	my $completed_questionnare_flag = 0;
	my $query_insert_points = '';
	my $url = '';
	my $Specials = Specials->new($db);

	my $temporary_for_checking_errors = '';

	my $query_questions = <<EOS;
		SELECT
			qq.id
		FROM
			questionnaires_questions AS qq
		WHERE
			qq.questionnaires_id = $args->{questionnaires_id}
			AND
			qq.active = TRUE
EOS


	my $delete_query = <<EOS;
						DELETE
						FROM
						questionnaires_user_answers
						WHERE
						members_id = $userid
						AND
						questionnaires_answers_id IN (
							SELECT
							qa.id
							FROM
							questionnaires_answers AS qa
							JOIN
							questionnaires_questions AS qq
							ON
							qq.id = qa.questionnaires_questions_id
							AND
							qq.questionnaires_id = $args->{questionnaires_id}
						)
EOS

#		DELETE FROM
#			questionnaires_user_answers
#		WHERE
#			members_id = $userid
#			AND
#			questionnaires_id = $args->{questionnaires_id}
#

	my $insert_query = <<EOS;
		INSERT INTO
			questionnaires_user_answers
			(
				members_id,
				questionnaires_answers_id,
				value
			)
			VALUES
			(
				$userid,
				?,
				TRUE
			)
EOS

	 #my $results = $db->selectall_arrayref($query_questions);

	#Delete the users previous entries for this questionnaire.
	$db->do($delete_query);

	#Prepare the query for inserting the users answers
	my $sth = $db->prepare($insert_query);


		if($args->{questionnaires_id} eq '1')
		{
				my $question_id = 10;
				my $key_id = 50090;

				my $skype_name = $q->param(9) == 50087 ? '' : $q->param('10_50090');

				my $skye_name_update = <<EOS;
					UPDATE
					members
					SET
						other_contact_info = ?
					WHERE
						id = $userid
EOS

				my $skype_sth = $db->prepare($skye_name_update);

				$skype_sth->execute($skype_name);

				$skype_sth = undef;

				if((!$skype_name && $q->param(9) == 50087) || ($skype_name && $q->param(9) == 50086) )
				{
					$sth->execute(50090);
					$users_submitted_questions_answers[10] = ['50090'];
				}


		}



 	PARAMS: foreach my $key ($q->param())
	{
		next PARAMS if $key =~/\D/;

		#put the answered questions in a multidimentsional array keyed by the question id.
		# $users_submitted_questions_answers[key][0] = answer
		# $users_submitted_questions_answers[key][1] = answer
		# $users_submitted_questions_answers[key][2] = answer
		$users_submitted_questions_answers[$key] = [$q->param($key)];

		foreach my $answer ($q->param($key))
		{
			$sth->execute($answer);


		}
	}


	#Query for the questions to check for the completion of the questionnaire
	my $question_ids = $db->selectall_hashref($query_questions, "id",);

	COMPLETED_QUESTIONNARE: foreach my $answer_id (keys %$question_ids)
	{
			if(defined($users_submitted_questions_answers[$answer_id]))
			{
				$completed_questionnare_flag = 1;
			} else {
				#If this questionnare was not completed set the flag, and exit the loop.
				$completed_questionnare_flag = 0;
				last COMPLETED_QUESTIONNARE;
			}
	}



	#Insert, or update the questionnaires_user_status table

	$rewards_points = updateUsersQuestionnaireStatus({'status'=>$completed_questionnare_flag, 'questionnaires_id'=>$args->{'questionnaires_id'}});

	# create a special notification if this party is a pool member
#	if ($rewards_points->{'reward_points'} || $rewards_points->{'pay_points'})
#	{
#		if ($db->selectrow_array('SELECT is_pool_member(?)', undef, $userid))
#		{
#			$db->do('INSERT INTO notifications (id, spid, notification_type) VALUES (?, ?, 124)', undef, $userid, $spid);
#		}
#	}
	
	if($rewards_points->{'reward_points'})
	{
		# assign rewards points
		# someFunction($userid, $rewards_points);
		# reward_trans
		# reward_trans_types


		$query_insert_points = "
		INSERT
		INTO
			reward_transactions
		(
			id,
                        trans_id,
                        reward_points,
			reward_type,
                        cur_xchg_rate,
                        currency,
			notes,
                        date_redeemable,
                        date_reported,
                        vendor_id,
			processed,
                        total_pending_rewards,
                        staff_notes
		)
		VALUES
		(
		  $userid,
                  NULL,
		  $rewards_points->{'reward_points'}/100.00,
		  6,
                  $mdata->{rate},
                  '$mdata->{currency}',
		  'Points for Questionnare ID $args->{'questionnaires_id'}',
		  NOW()::DATE,
		  NOW()::DATE,
                  0,
		  TRUE,
                  0,
                  NULL
		)
";

			$db->do($query_insert_points);
			
			$temporary_for_checking_errors .= $query_insert_points . "\n\n";

	}

	if($rewards_points->{'pay_points'})
	{

		$query_insert_points = "
		INSERT INTO
			transactions
			(
				id,
				spid,
				membertype,
				description,
				trans_type,
				vendor_id,
				amount,
				trans_date,
				discount_amt,
				cv_mult,
				reb_mult,
				comm_mult,
				pp
			) VALUES (
				$userid,
				$spid,
				'$membertype',
				'Points for Questionnare ID $args->{'questionnaires_id'}',
				12,
				663,
				0,
				NOW(),
				1,
				4,
				0,
				0,
				$rewards_points->{'pay_points'}
			)
";

		eval{
			$db->do($query_insert_points);
			
			$temporary_for_checking_errors .= $query_insert_points . "\n\n";
			
		};
		if($@)
		{
			my $MailTools = MailTools->new();
			
			my $message =<<EOT;
			
There was an error inserting information into the transactions table. Assigning a pay point to someone who has completed a questionnaire.

Query:
$query_insert_points

Error:

EOT

			$message .= $@;

			$message .= "\n\nrewards_points:\n";
			
			$message .= Dumper($rewards_points);
			
			$message .= "\n\n\n" . $temporary_for_checking_errors . "\n\n";
			
			$MailTools->sendTextEmail({'to'=>'errors@dhs-club.com', 'subject'=>'Error: marketing_questionnaires.cgi', 'text'=>$message});
			
		}


	}

	eval{
		$Specials->processUserSpecialPreferences($userid);
	};
	if($@)
	{
		$completed_questionnare_flag = 0;
	}

	if($completed_questionnare_flag)
	{
		$url = qq(step=) . $q->escape('thank_you') . '&points=' . $q->escape($rewards_points->{'reward_points'});
	} else {
		$url = qq(step=) . $q->escape('do_over');
	}


#	my $MailTools = MailTools->new();
#	my $message =<<EOT;
#Member $userid has just updated their Personal Preferences Questionnaire (Top Picks Questionnaire.).
#rewards_points:
#EOT
#	$message .= Dumper($rewards_points);
#	$message .= "\n\n\n" . $temporary_for_checking_errors . "\n\n";
#	$MailTools->sendTextEmail({to=>'errors@dhs-club.com', subject=>"Notice: marketing_questionnaires.cgi, User ID: $userid", text=>$message});




	print $q->redirect(-URL =>"/mall/other/questionnaire.php?$url");
	
	#$url


}
###############################

###############################
# Check to see if the user is
# logged in, if they are
# return their userid.
#
# Param		userdata	hash
# Return	int
###############################
sub loginCheck
{

	my $args = shift || {};
	
	my $auth_custom_generic = $args->{AuthCustomGeneric}?$args->{AuthCustomGeneric}:$q->cookie('AuthCustom_Generic');
	
	my ( $id, $meminfo ) = G_S::authen_ses_key( $auth_custom_generic );
	my $content_xml = '';
	my $xml = '';
	my $xsl = '';
	my $return = {id=>$id,spid=>$meminfo->{spid},membertype=>$meminfo->{membertype}};
	my $step_xml = <<EOS;
						<step>
							<login>yes</login>
							<url>$LOGIN_URL?destination=$ME</url>
						</step>
EOS

	return $return if $id && $id !~ /\D/;





	$content_xml = $gs->Get_Object($db, $args->{content_xml}, $prefered_language) ||
		die "Failed to load XML template: $args->{content_xml}\n";



	###############################
	# Combine the applicatble
	# pieces of the XML document.
	###############################
	$xml =<<EOS;
		<base>
			$step_xml
			$content_xml
			<timestamp>${\scalar localtime()}</timestamp>
		</base>
EOS


	$xsl = $gs->Get_Object($db, $args->{xsl}) ||
		die "$@\nFailed to load XSL template: $xsl\n";



	#Output the XML  Document
	print $q->header(-charset=>'utf-8', -expires=>'now');
	print $xml; #XML::local_utils::xslt($xsl, $xml);

#	print $q->redirect( $LOGIN_URL . "?destination=" .$ME );
	exitScript();

}
###############################

###############################
# Display the thank you page.
#
# Param		hash	$args
# 			int 	xsl					The style sheet.
#			int 	content_xml			The content XML file.
#			int 	questionnaires_id	The questionnaires ID
# Return	string
###############################
sub thankYouPage
{
	my $args = shift || {};
	my $points = defined($q->param('points'))? $q->param('points'):0;
	my $return = '';
	my $xml = '';
	my $xsl = '';
	my $content_xml = '';
	my $step_xml = <<EOS;
						<step>
							<thank_you>yes</thank_you>
							<points>$points</points>
						</step>
EOS


	$content_xml = $gs->Get_Object($db, $args->{content_xml}, $prefered_language) ||
		die "Failed to load XML template: $args->{content_xml}\n";

	###############################
	# Combine the applicatble
	# pieces of the XML document.
	###############################
	$xml =<<EOS;
		<base>
			$step_xml
			$content_xml
			<timestamp>${\scalar localtime()}</timestamp>
		</base>
EOS

	$xsl = $gs->Get_Object($db, $args->{xsl}) ||
		die "$@\nFailed to load XSL template: $xsl\n";

	#Output the XML  Document
	$return = $q->header(-charset=>'utf-8', -expires=>'now');
	$return .= $xml; #XML::local_utils::xslt($xsl, $xml);

	return $return;

}
###############################

###############################
# Update the users questionnaires status.
#
# Param		hash
#			int		questionnaires_id
# Return	int		The amount of rewards point to assign.
###############################
sub __updateUsersPoints
{
	my $args = shift || {};
	##############################################################
	# NOTE: If they need to add time ranges for limiting the assignment of
	# 		reward points the questionnaires table should
	# 		have a couple of fields stating start, and end dates for point assignment
	# 		then in the query where the alias field "assign_rewards_points" is,
	# 		add a decision if the current date is between the start, and end dates
	# 		and "NOW() >= qus.completed + q.point_assignment_interval" return "TRUE".
	##############################################################
	my $query_check_existence =<<EOS;
		SELECT
			qus.completed,
			qus.updated,
			q.rewards_points,
			COALESCE(NOW() >= qus.completed + q.point_assignment_interval,TRUE) AS assign_rewards_points,
			CASE
				WHEN
				NOW() >= (CASE WHEN qus.completed IS NULL THEN NOW() ELSE qus.completed + q.pay_point_assignment_interval END) AND (SELECT COUNT(qus_inside.ip) FROM questionnaires_user_status qus_inside WHERE qus_inside.questionnaires_id = qus.questionnaires_id AND qus_inside.ip = qus.ip) = 1 AND (SELECT count(pnspc.country_code) FROM personal_nonfee_sales_prerequisite_countries pnspc WHERE pnspc.country_code = m.country) > 0
				THEN TRUE
				ELSE FALSE
			END AS assign_pay_points,
			q.pay_points
		FROM
			questionnaires_user_status qus
			JOIN
			questionnaires q
			ON
			qus.questionnaires_id =  q.id
			JOIN
			members m
			ON
			m.id = qus.userid
		WHERE
			qus.questionnaires_id = $args->{questionnaires_id}
			AND
			qus.userid = $userid

EOS

	# If the user already has a record Update otherwise Insert
	return $db->selectrow_array($query_check_existence);


}

###############################
# Update the users questionnaires status.
#
# Param		hash
# 			bool	status
#			int		questionnaires_id
# Return	int		The amount of rewards point to assign.
###############################
sub updateUsersQuestionnaireStatus
{
	my $args = shift || {};
	my $status = (defined($args->{'status'}) && $args->{'status'}) ? 'NOW()':'NULL';
	my $ip = $q->remote_host();
	my $return = {'reward_points'=>0, 'pay_points'=>0};
	my $ip_information = '';
	my $updated_status = '';

	# If the user already has a record Update otherwise Insert
	my @results = __updateUsersPoints({questionnaires_id=>$args->{questionnaires_id}});

	if(scalar(@results))
	{
		#If the status for this questionnare is completed, and the it is time to
		# earn rewards points go ahead, and update the completed field.
		if ($status ne 'NULL' && $results[3] ne '0')
		{
			$updated_status = qq(completed = $status,);
			$ip_information = qq(ip = '$ip',);
		}

		$db->do(
			"UPDATE
				questionnaires_user_status
			SET
				$updated_status
				$ip_information
				updated = NOW()
			WHERE
				questionnaires_id = $args->{questionnaires_id}
				AND
				userid = $userid"
		);

	} else {

		$db->do(
			"INSERT INTO
			questionnaires_user_status
			(
				completed,
				updated,
				created,
				questionnaires_id,
				userid,
				ip
			)

			VALUES
			(
				NULL,
				NOW(),
				NOW(),
				$args->{'questionnaires_id'},
				$userid,
				'$ip'
			)"
		);

		@results = __updateUsersPoints({'questionnaires_id'=>$args->{'questionnaires_id'}});

		$db->do(
			"UPDATE
				questionnaires_user_status
			SET
				completed = $status,
				updated = NOW()
			WHERE
				questionnaires_id = $args->{'questionnaires_id'}
				AND
				userid = $userid
			"
		) if $status ne 'NULL';

		# Set the "assign_rewards_points" field to true as
		# long as the questionnare is completed.
		$results[3] = 0 if $status eq 'NULL';
		$results[4] = 0 if $status eq 'NULL';
		
	}

	#If the "$status" is not "NULL" and the

	$return->{'reward_points'} = ($status ne 'NULL' && $results[3] ne '0')? $results[2]:0;
	$return->{'pay_points'} = ($status ne 'NULL' && $results[4] ne '0')? $results[5]:0;

	return $return;

}
###############################

################################################################
# Main
################################################################
set_message($ERR_MSG);


if(!$xml_configs->{$questionnaires_id})
{
	errorExitScript('The requested questionnaire is not available at this time.');
}


###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exitScript() unless $db;
$db->{RaiseError} = 1;
#$db->{pg_enable_utf8} = 1;


# if the "id" parameter is passed in the query string, check if the user is admin
if ( defined($q->param('id')) && ($q->cookie('operator') && $q->cookie('pwd')) && ADMIN_DB::DB_Connect( 'pwp', $q->cookie('operator'), $q->cookie('pwd') ))
{
	$userid = $q->param('id');
	$xml_configs->{$questionnaires_id}->{'questionnaires_admin'} = 1;

	#errorExitScript({message=>Dumper($xml_configs->{$questionnaires_id})});
}
elsif (defined($q->param('id')))
{
	errorExitScript('Your not supposed to do that!');
}
else
{
	$xml_configs->{$questionnaires_id}->{'AuthCustomGeneric'} = $q->param('AuthCustomGeneric');

	my $temp = loginCheck($xml_configs->{$questionnaires_id});
	
	undef $xml_configs->{$questionnaires_id}->{'AuthCustomGeneric'};
	
	$userid = $temp->{'id'};
	$spid = $temp->{'spid'};
	$membertype = $temp->{'membertype'};
#	my $mdata = $db->selectrow_hashref("
#				SELECT
#					m.alias,
#					m.firstname,
#					m.lastname,
#					m.address1,
#					m.address2,
#					m.city,
#					m.state,
#					m.postalcode,
#					m.country,
#					m.emailaddress,
#					m.other_contact_info,
#                                        cu.currency_code as currency,
#                                        cr.rate
#				FROM
#					members m
#                                LEFT JOIN currency_by_country cu
#                                      ON (m.country = cu.country_code)
#                                LEFT JOIN exchange_rates_now cr
#                                      ON (cu.currency_code = cr.code)
#
#				WHERE 	id = ?", undef, $userid);

}

if (!$userid)
{
	errorExitScript('The users information is not available at this time.');
}


   $mdata = $db->selectrow_hashref("
		SELECT
 	            m.alias,
		    m.firstname,
		    m.lastname,
		    m.address1,
		    m.address2,
		    m.city,
		    m.state,
		    m.postalcode,
		    m.country,
		    m.emailaddress,
		    m.other_contact_info,
                    cu.currency_code as currency,
                    cr.rate
		FROM
		    members m
                LEFT JOIN currency_by_country cu
                    ON (m.country = cu.country_code)
                LEFT JOIN exchange_rates_now cr
                    ON (cu.currency_code = cr.code)
		WHERE 	id = ?", undef, $userid);

if ($step eq "submit")
{
	insertUpdateUserAnswers($xml_configs->{$questionnaires_id});
}
elsif ($step eq "thank_you")
{
	print thankYouPage($xml_configs->{$questionnaires_id});
} else {
    # this doubles as case "do_over" and else
    print displayPage($xml_configs->{$questionnaires_id});
}

exitScript();

__END__

#CHANGE LOG:
#
#	2008-10-23	Keith	Added some error trapping in the "insertUpdateUserAnswers" subroutine
#						If the inser query fails for assigning pay points an email is sent to
#						"errors@dhs-club.com".
#
#	2008-11-12	Keith	Modified this script so it only returns an XML document, now it can
#						be used with XSP pages.
#
#	02/02/11	Added the setting of a notification record when the survey taker is a pool member
#
#       02/23/11        g.baker updated to use the new reward_transactions table
# 01/03/12 disabled the pool sensitive notification
# 11/18/14 changed the redirect URL to the php script
05/18/15	Added binmode to STDOUT