#!/usr/bin/perl -w
###### contest.pl
###### a routine to select a list of member_id's from the rewards_transactions file from a specific period and randomly pick a winner
###### 
###### last modified: 01/05/12 George Baker

use strict;
use lib "/home/httpd/cgi-lib";
use DB_Connect;
use CGI;

###### GLOBALS

my $log = '';      #### this will be oiut logging variable (email or log file)
my $db = '';
my $start_date = $ARGV[0];
my $end_date = $ARGV[1];
my $cgi = CGI->new();
$start_date = $cgi->param('start_date');
$end_date = $cgi->param('end_date');
#
# IF this script is to run on a "time" basis, the admin crontab needs to be edited and cgi should be the database user
# otherwise I'm running it as me.
#
my $id2 = 0;
$db = DB_Connect('merchant_applications') || die "Could not connect to db";

###### 

my $tdata = $db->prepare("
select id from reward_transactions 
          where reward_type >= 15 and 
                reward_type < 19 and 
                (\'$start_date\'::date, \'$end_date\'::date) overlaps (date_reported, date_reported) 
;");
my $rv = $tdata->execute;

my @member_list = ();
my $counter = 0;
#while (my $tmp = $tdata->fetchrow_hashref) {
#    $member_list[$counter++] = $tmp->{id};
#}
open(OTHERS, "/home/httpd/html/contest/others.csv") or die("Unable to open file");
my @others_list = <OTHERS>;
close(OTHERS);
foreach $id2  (@others_list) {
    $member_list[$counter++] = $id2;
}

my $winner = $member_list[ int(rand($counter)) ];
my $xml = "<base>\n<winner>". $winner . "</winner></base>";
print $cgi->header(-type=>'text/xml', -charset=>'utf-8'), $xml;
#print $cgi->header(-type=>'text/text', -charset=>'utf-8'), $winner;
#print $winner;
my $qry = "insert into contest_winners values ($winner, '$start_date', '$end_date', now());";
$tdata = $db->prepare($qry);
$rv = $tdata->execute; 
