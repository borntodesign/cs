#!/usr/bin/perl -w
# getcsck
# Get ClubShop Cookie
# for the time being, we are offering a sort of dual login from GI
# by sending the new login over to CS with their cookie value to have a CS cookie set as well
# created: June 2010	Bill MacArthur
# last modified: 10/19/10

use strict;
use CGI;
#use Template;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
use Data::Dumper;
use Apache::GI_Authen;

my $cgi = new CGI;
#my $TT = Template->new();
my $gs = new G_S;
my (@cookies) = ();

# we are expecting a "destination" and a "cv" (Cookie Value) parameters
my $dest = $cgi->param('destination') || '/';

# we could be receiving a destination that resides on CS or GI
# if it is on CS, we will have a fully qualified URI
$dest = "https://www.glocalincome.com$dest" unless $dest =~ m/^http/;

my $cv = $cgi->param('cv') || die "No cv parameter received";

# validate the cv value
my ($id, $member) = GI_Authen::authen_ses_key(undef, undef, $cv);
unless ($id && $id !~ /\D/){
	warn "Expired or invalid cv value: id= $id member= $member";
	die "Expired or invalid cv value";
}

push @cookies, $gs->Create_AuthCustom_Generic_cookie($member);
push @cookies, $cgi->cookie(-name=>'id', -domain=>'.clubshop.com', -expires=>'1y', -value=>$member->{id});
print $cgi->header(-charset=>'utf-8', -cookies=>\@cookies);
print qq#<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>Login Success</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript">
var dest = "$dest";
if (top.location){
	top.location = dest;
}
else {
	window.location = dest;
}
</script>

</head>
<body>
<p>Login Successful. Redirecting.</p>
<p>If you are not redirected, please
<a href="https://www.glocalincome.com$dest" target="_top">click here</a>.</p>
</body></html>#;

# 10/19/10 added the "warn" if the cv was bad to see what the ID value was for debugging
