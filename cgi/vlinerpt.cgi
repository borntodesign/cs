#!/usr/bin/perl -w
###### vlinerpt.cgi	Vertical Line Report
###### will output a list of personally sponsored free members who themselves have members sponsored
###### last modified 12/27/2010	Bill MacArthur

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;

###### in order to accept params included in a path, like when we are coming in from a redirect
###### we'll assign the params from the path info, that way we don't have to rewrite because of calls to q->param
###### this of course overwrites any existing params in a query string, but there shouldn't be any anyway
($ENV{QUERY_STRING} = $ENV{PATH_INFO}) =~ s#/## if $ENV{PATH_INFO};

###### CONSTANTS
our $MEMBERS_TBL = 'members';
our %TEMPLATES = (
	list => 10261,
	tree => 10262
);
#my $ALIAS_LINK = '/cgi/member_detail.cgi?refid';	# the link we put on the aliases in the report
my $ALIAS_LINK = 'http://www.glocalincome.com/cgi/vip/member-followup.cgi?rid';	# the link we put on the aliases in the report

###### GLOBALS
our ($db, %DATA, $id, $meminfo, $Member) = ();
our $q = new CGI;
our $SCRIPT = $q->script_name();

our $ADMIN = $q->param('admin');

our $Rid = $q->param('rid') || '';		###### this is the ID of the VIP we are Reporting on
our $Mid = $q->param('mid') || '';		###### the member we are reporting on
our $Jump2 = uc $q->param('jump2') || '';	###### the ID we want to Jump to :)

unless ($Rid)
{
	Err('Missing report ID');
	exit;
}

###### neither our rid or our mid should contain any letters
if($Rid =~ /\D/)
{
	Err("Bad Rid passed: $Rid<br />This report works only on numeric IDs.");
	exit;
}

if($Mid =~ /\D/)
{
	Err("Bad Mid passed: $Mid<br />This report works only on numeric IDs.");
	exit;
}

###### we want to allow transparent access for HQ use without logging in as the VIP
if ($ADMIN)
{
	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	unless ($db = ADMIN_DB::DB_Connect( 'vlinerpt', $operator, $pwd )){exit}
}
else
{
###### we'll first verify the identity of our user
	($id, $meminfo) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
	if ( !$id || $id =~ /\D/ )
	{
		my $return_link = "$LOGIN_URL?destination=$SCRIPT/" . CGI::escape($q->query_string());
		Err("Your login has expired.<br />Please login again <a href=\"$return_link\">here</a>.");
		exit;
	}

###### if they are not a VIP, they don't have access to the report
	elsif ( $meminfo->{membertype} !~ 'v' )
	{
		Err('Vertical Line Reports are for VIPs only.');
		exit;
	}
	else
	{
		exit unless $db = DB_Connect('vlinerpt');
	}
}

###### let's report DB errors on the spot
$db->{RaiseError} = 1;

###### our hack to build in 'Jump to' functionality
if ($Jump2)
{
	my $jumpto = $Jump2;	###### retain this value for error message if needed

	$Member = Get_member_info($Jump2);
	unless ($Member)
	{
		Err("Cannot find a membership for: $Jump2");
		goto 'END';
	}

###### we will need numeric IDs to continue, so we'll handle aliases accordingly
	my $Jump2 = $Member->{id};

###### make sure they are in my downline
	unless( G_S::Check_in_downline($db, $MEMBERS_TBL, $id, $Jump2) == 1 )
	{
		Err("Cannot find $jumpto in your downline.");
		goto "END";
	}

###### if the Jump2 is a VIP we need to define a list report, otherwise.......
	if ($Member->{membertype} eq 'v')
	{
		$Rid = $Jump2;
#		$REPORT_TYPE = 'list';
	}
	elsif ($Member->{membertype} =~ /m|p/)
	{
		$Mid = $Jump2;
#		$REPORT_TYPE = 'tree';
	}
	else {
		my $msg = "The member you want to jump to: $jumpto <br />is not the correct member type.<br />
			They are: $G_S::MEMBER_TYPES{$Member->{membertype}}\n";
		Err($msg);
		goto 'END';
	}
}
	###### end of 'Jump To' hack

$SCRIPT .= "?rid=$Rid";
$SCRIPT .= ';admin=1' if $ADMIN;
our $REPORT_TYPE = ($Mid) ? ('tree') : ('list');

###### verify that the person we are pulling info on is in our downline as long as we aren't in ADMIN mode
###### or doing a Jump to since we would have already checked that
###### of course if we are reporting on ourselves, we don't need to check
unless ($ADMIN || $Jump2 || ($Rid == $id))
{
	my $tmp = ($REPORT_TYPE eq 'list') ? ($Rid) : ($Mid);
	unless( G_S::Check_in_downline($db, $MEMBERS_TBL, $id, $tmp) == 1)
	{
		Err("Cannot find $tmp in your downline.");
		goto "END";
	}
}

if ($REPORT_TYPE eq 'list')
{
	$Member = Get_member_info($Rid) unless $Member;

###### since we don't have a Member ID to report on, we must be doing a simple list
###### we'll get the list of members under our Report ID that have members sponsored
	my $qry = "
		SELECT m.id,
			m.spid,
			m.alias,
			CASE WHEN m.membertype = 'ag' THEN m.company_name
				ELSE m.firstname
			END AS firstname,
			CASE WHEN m.membertype = 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			CASE
				WHEN m.membertype = 'v' THEN ''
				ELSE COALESCE(time_left_in_queue(mt.entered)::TEXT, '')
			END AS time_left,
			CASE
				WHEN m.membertype = 'v' THEN ''
				ELSE COALESCE(TO_CHAR(expire_from_queue(m.id)::DATE, 'Mon DD, YYYY'), '')
			END AS cutoff_date,				
			COALESCE(m.language_pref,'') AS language_pref,
			COALESCE(m.country,'') AS country_code
		FROM $MEMBERS_TBL m
		LEFT JOIN member_transfer_recipients mt
		ON mt.id=m.id
		WHERE (
			SELECT COUNT(id)
			FROM $MEMBERS_TBL
			WHERE spid = m.id
			AND membertype ~ 'm|v'
		) > 0
		AND spid = $Rid
		AND membertype !~ 'v|p'
		ORDER BY m.id";

	my ($tmp, $tbl) = ();
	my $sth = $db->prepare($qry);
	$sth->execute;
	while ($tmp = $sth->fetchrow_hashref)
	{
		$DATA{$tmp->{id}} = $tmp;
	}
	
	$sth->finish;
	if (%DATA)
	{
	###### now we'll begin forming our data table to be returned
		$tbl = qq#<table border="0" cellspacing="0" cellpadding="3">#;
		my $class = 'w';

	###### loop through our rows of data,
	###### flip-flopping between classes to alternate row colors
		foreach (sort {$a<=>$b} keys %DATA)
		{
			$tbl .= qq#<tr class="$class"><td><a href="$SCRIPT&mid=$DATA{$_}->{id}">$DATA{$_}->{alias}, $DATA{$_}->{firstname} $DATA{$_}->{lastname}</a></td></tr>\n#;
			$class = ($class eq 'w') ? ('g') : ('w');
		}

	###### now close the table and output our page
		$tbl .= '</table>';
	}
	else
	{
		$tbl = $q->div({-style=>"font-weight: bold;"}, "No vertical member lines to report on at this time.");
	}

	$_ = G_S::Get_Object($db, $TEMPLATES{'list'}) || die "Couldn't open template: $TEMPLATES{'list'}";

	s/%%firstname%%/$Member->{firstname}/g;
	s/%%lastname%%/$Member->{lastname}/g;
	s/%%alias%%/$Member->{alias}/g;
	s/%%emailaddress%%/$Member->{emailaddress}/g;
	s/%%TABLE%%/$tbl/;
	s/%%URL%%/$q->script_name()/eg;
	s/%%Rid%%/$Rid/g;
	s/%%HIDDEN_ADMIN%%/Create_hidden_admin()/eg;
	print $q->header(-charset=>'utf-8'), $_;

}
else
{
###### since it's not a list, it must be a report on a member of the list
	require '/home/httpd/cgi-lib/vertical_drill-v2.lib';
	$Member = Get_member_info($Mid) unless $Member;
	unless ($Member)
	{
		Err("Cannot find a membership for: $Jump2");
		goto 'END';
	}

###### thwart attempts by VIPs to use this report to create a complete genealogy from a VIP
###### the system load consequences could be severe
	if ($Member->{membertype} =~ /v/)
	{
		Err("These reports are designed for Free Member Vertical Lines.<br />$Member->{alias} is a: $G_S::MEMBER_TYPES{$Member->{membertype}}.");
		goto 'END';
	}

	my $length = my $tmp2 = ();
	my $linecnt = 1;
	$Member->{cutoff_date} =~ s/,//;
	my $vlist = ListItem($Member); 
	my $maxlength = length $vlist;
	my $email_list = "$Member->{firstname} $Member->{lastname} <$Member->{emailaddress}>,\n";

	my ($data, $max_level, $fam, $time) = Vertical::VList($db, $Mid, $MEMBERS_TBL);

###### now we have our data, lets format it for the report
###### we can apply different styles to the data to accomodate the different member types
	for (my $x=0; $x < scalar @$data; $x++)
	{
		if ($data->[$x]{membertype} eq 'v')
		{
		###### I don't remember why the 'cut-off' date was included
#			$data->[$x]{DATA} = $q->a({-class=>'vip', -href=>"mailto:$data->[$x]{emailaddress}"}, "$data->[$x]{alias}, $data->[$x]{firstname} $data->[$x]{lastname}&nbsp;&nbsp;** <i>$data->[$x]{cutoff_date}</i>");
		###### but nonetheless, Dick now wants a link to the network report for this VIP
			$data->[$x]{DATA} = $q->a({-class=>'vip', -href=>"mailto:$data->[$x]{emailaddress}"}, "$data->[$x]{alias}, $data->[$x]{firstname} $data->[$x]{lastname}");
			$data->[$x]{DATA} .= "&nbsp;&nbsp;<i>" . $q->a({
				-href=>"/cgi/genealogy.cgi?r=$data->[$x]{id}" . ($ADMIN ? ";admin=1" : ''),
				-target=>'_blank'},
				'Network Report') . "</i>";
		}
		elsif ($data->[$x]{membertype} eq 'm' || $data->[$x]{membertype} eq 'amp')
		{
			$data->[$x]{DATA} = 
				$q->a({-href=>"$ALIAS_LINK=$data->[$x]{id}", -onclick=>'return nuwin(this.href)'}, $data->[$x]{alias}) .
				' ' .
				$q->a({-href=>"mailto:$data->[$x]{emailaddress}"}, "$data->[$x]{firstname} $data->[$x]{lastname}") .
				($data->[$x]{cutoff_date} ? " - <i>$data->[$x]{cutoff_date}</i>" : '');
		}
###### if they are not a valid member, we won't show them
		else
		{
			$data->[$x]{DATA} = "$data->[$x]{alias} <i style=\"color: #cc0000;\">- not Associate or VIP -</i>";
		}

###### Create a link to see the VIPs vertical lines without visiting the network rpt.
		if ($data->[$x]{membertype} =~ /ag|v/)
		{
			my $lines = $db->selectrow_array("Select count(m.id)
				FROM members m, members m2
				WHERE m.spid= $data->[$x]{id}
				AND m2.spid=m.id AND m.membertype IN ('ag','m')
				AND m2.membertype IN ('m','ag','v')
				HAVING COUNT(m2.id)>0");

			$data->[$x]{DATA} .= "&nbsp;&nbsp;<i>" . $q->a({
				-href=>$q->script_name . "?rid=$data->[$x]{id}" . ($ADMIN ? ";admin=1" : '')
				},
				'Vertical Line Reports') . "</i>" if $lines;
		}

###### now we'll create the two lists we'll provide in the report
		if ($data->[$x]{membertype} =~ /m|v/)
		{
			###### we have two columns related to the queue, cutoff_date & time_left
			$data->[$x]{cutoff_date} =~ s/,//;
			$tmp2 = ListItem($data->[$x]);
			$vlist .= $tmp2;

			$email_list .= "$data->[$x]{firstname} $data->[$x]{lastname} <$data->[$x]{emailaddress}>,\n";

###### we want to create the textarea wide enough later rather than doing a default size
###### so we'll keep track of the longest line and the number of lines
			$length = length $tmp2;
			$maxlength = $length if $maxlength < $length;
			$linecnt++;
		}
	}

###### remove the trailing newline and comma
#	$vlist =~ s/,\\n$//;
	$email_list =~ s/,\n$//;

###### make a textarea form element out of our list
	$email_list = $q->textarea(
		-cols=>($maxlength +3),
		-rows=>($linecnt +2),
		-name=>'linelist',
		-value=>$email_list);

###### if for some reason there is no data passed to this routine, then it will return NULL,
###### so we'll set the var to something
	my $Block = Vertical::TableOUT($q, {LvlCols=>'', max_level=>$max_level}, $data, $fam) || '';
	my $Table = Tbl_Header($max_level, $Member) . $Block . '</table>';

	$_ = G_S::Get_Object($db, $TEMPLATES{'tree'}) || die "Couldn't open template: $TEMPLATES{'tree'}";

	s/%%firstname%%/$Member->{firstname}/g;
	s/%%lastname%%/$Member->{lastname}/g;
	s/%%alias%%/$Member->{alias}/g;
	s/%%emailaddress%%/$Member->{emailaddress}/g;
	s/%%CUTOFF_DATE%%/$Member->{cutoff_date}/;
	s/%%TIME_LEFT%%/$Member->{time_left}/;
	s/%%TREE_TABLE%%/$Table/;
	s/%%VLINE_LIST%%/$vlist/;
	s/%%LISTBOX%%/$email_list/;
	s/%%URL%%/$q->script_name()/eg;
	s/%%Rid%%/$Rid/g;
	s/%%HIDDEN_ADMIN%%/Create_hidden_admin()/eg;

	print $q->header(-charset=>'utf-8'), $_;

}

END:
$db->disconnect;
exit;

sub Create_hidden_admin
{
	return ($ADMIN) ? ($q->hidden(-name=>'admin', -value=>1)) : ('')
}

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print $q->h4($_[0]);
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST.</tt>';
	print $q->end_html();
}

sub Get_member_info
{
	my $id = shift;
	my $qry = "
		SELECT m.id,
			m.spid,
			m.alias,
			CASE WHEN m.membertype = 'ag' THEN m.company_name
				ELSE m.firstname
			END AS firstname,
			CASE WHEN m.membertype = 'ag' THEN ''
				ELSE m.lastname
			END AS lastname,
			m.membertype,
			COALESCE(m.emailaddress, '') AS emailaddress,
			CASE
				WHEN m.membertype = 'v' THEN ''
				ELSE COALESCE(time_left_in_queue(mt.entered)::TEXT, '')
			END AS time_left,
			CASE
				WHEN m.membertype = 'v' THEN ''
				ELSE COALESCE(TO_CHAR(expire_from_queue(m.id)::DATE, 'Mon DD, YYYY'), '')
			END AS cutoff_date,
			COALESCE(m.language_pref,'') AS language_pref,
			COALESCE(m.country,'') AS country_code
		FROM $MEMBERS_TBL m
		LEFT JOIN member_transfer_recipients mt
		ON mt.id=m.id
		WHERE ";
	$qry .= ($id =~ /\D/) ? ("m.alias= ?") : ("m.id= ?");
	my $sth = $db->prepare($qry);
	$sth->execute($id);
	my $Member = $sth->fetchrow_hashref;
	$sth->finish;
	return $Member;
}

sub ListItem
{
	my $Member = shift;
	my $rv = '';
	foreach (qw/alias firstname lastname emailaddress/){
		my $tmp = $Member->{$_};
		# escape bogus data for javascript
		$tmp =~ s/(;|")/\\$1/g;
		$rv .= "$tmp,";		
	}
	return $rv . "$Member->{time_left},$Member->{cutoff_date},$Member->{country_code},$Member->{language_pref}" . '\n';
}
sub Tbl_Header
{
	my ($max_level, $Member) = @_;
	my $tmp = qq#<table border="0" cellspacing="0" cellpadding="0"><tr>\n#;

	for (my $x = 0; $x < ($max_level + 1); $x++)
	{
		$tmp .= qq#<td><img src="/images/blanklin.gif" alt="" width="15" height="1" /></td>\n#;
	}

	$tmp .= qq#<td>&nbsp;</td></tr>\n#;
	$tmp .= $q->Tr( $q->td({-colspan=>($max_level + 2)},
			$q->a({-href=>"$ALIAS_LINK=$Member->{id}", -onclick=>'return nuwin(this.href)'}, $Member->{alias}) .
			qq#, <a href="mailto:$Member->{emailaddress}">$Member->{firstname} $Member->{lastname}</a>
			&nbsp;&nbsp;- $Member->{cutoff_date} -
			<br /><br />\n#
	));

#	$tmp .= $q->Tr({-class=>'report_on'},
#		$q->td({-class=>'img'}, "<img src=\"/images/angle2.gif\" width=\"15\" height=\"20\">" ) . $q->td({-colspan=>($max_level +1), -class=>'report_on'}, $Member->{alias}));
}

###### 04/04/02 turned warnings back on, created error trapping for bogus rid & mid values
###### 04/19/02 removed the space after the colon in the mailto links and added the names after the aliases
###### 04/24/02 added functionality to include lists in the report, also changed the behaviour for displaying removes
###### added functionality for Jump To and disabled the ability to pull a Tree report on a VIP
###### 06/27/02 added the server time to the error message output.
###### 07/15/02 made the reports available even if the reportee had only 1 'removed' person sponsored
###### 07/31/02 made the report available on a Partner line
###### (it will not show Partners as having reports available, but will generate a report on a Partner)
###### 08/14/02 added additional asterisks to help vision impaired folks identify VIPs and Partners in a line
###### 12/18/02 went to the 'use' style for custom modules, also went to AuthCustom_Generic cookie
###### 01/30/03 fixed an uninit'd var error by removing the 'r' membertype from the count query when determining
###### whether there was a report available under an ID
###### 01/29/04 added handling for the member_transfer_queue info
###### 02/05/04 revised to use member transfer recipients table instead
###### also removed the date display for VIPs
###### 06/14/04 added the network report link to the VIP listings in these reports
###### 08/24/05 changed the drilling .lib module to modify the report to stop at the
###### first VIP/AG in a branch
# 07/06/06 revised the SQL to display the company_name for AGs
# 08/09/06 minor tweaks
###### 12/06/06 broke out the alias for a different link on members
###### 02/26/07 added the language pref to the mix in the vline var (used to produce the javascript array)
###### 03/26/09 moved the URL to the report on the alias into a 'constant'
# 12/27/10 changed the alias link URL