#!/usr/bin/perl -w

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use CGI;
use Specials;

our %param = ();
our $rv = ();

our $CGI = CGI->new();
our $db = '';
our $member_id = '';

sub Get_Cookie
{
    my $login_type = '';
    # Get the member Login ID and info hash from the VIP cookie. 
    $member_id = $CGI->cookie('id');
    if ( ! $member_id || $member_id =~ m/\D/ ) {
        my $minidee = $CGI->cookie('minidee');
        my @tokes = split(/|/,$minidee);
        $member_id = $tokes[0];
        if ( ! $member_id || $member_id =~ m/\D/ ) {
            print $CGI->redirect("/mall/other/login2.shtml?destination=http://www.clubshop.com/mall/US");
            die;
	}
    }
    if ($member_id =~ /^[0-9]+$/) { 
    }
    else {
        print $CGI->redirect("/mall/other/login2.shtml?destination=http://www.clubshop.com/mall");
        die;
    }
    $param{member_id} = $member_id;
}

sub exitScript
{
	$db->disconnect if ($db);
	exit;
}

sub ampCK {
	my $amp = shift;
	return $amp if $amp eq '&amp;';
	$amp =~ s/&/&amp;/;
	return $amp;
}


sub Err
{
	my $params = shift;

	print $CGI->header('text/xml');
	print "<error>$params</error>";
	exitScript();
}

sub EscapeEntities {
	my $arg = shift;
	$arg =~ s/\ &\ /\ &amp;\ /g;
	return $arg;
}

sub LoadParams
{
	%param = ();
	###### we are only expecting these arguments
	my @expected = qw/mall_id member_id sort_by_date/;
	foreach(@expected){
		$param{$_} = $CGI->param($_);
	}

	return ('bad or missing mall_id param:' .  $param{mall_id} || '') if (! $param{mall_id}) or $param{mall_id} =~ /\D/;

        if (!defined $param{member_id} || (defined $param{member_id} && $param{member_id} =~ /\D|^$/)) {
	    Get_Cookie();
	}

	return undef;
}

sub _default_handler
{
	my $xml = '';

	eval{
		my $Specials = Specials->new($db);
		$xml = $Specials->getMemberTopPicksXML($param{member_id},$param{mall_id}, $param{sort_by_date});
		
		$xml = EscapeEntities($xml);
		
	};
	if($@)
	{
		Err($@);
		return;
	}

	my $sort_by = $param{sort_by_date}?0:1;

	print $CGI->header('text/html');
        $xml =~ s/vendors/table/g;
        $xml =~ s|<none>There are no results</none>|<p>There are no Top Picks available based on your survey preferences at this time.<br /> Please check back later.</p>|g;
	print("<html><header></header><body>$xml</body></html>");
}


################################################################
# Main
################################################################
###############################
# Connect to the Database.
###############################
$db = DB_Connect('questionnairs');
exit unless $db;
$db->{RaiseError} = 1;

# we'll get textual URls back with the invocation method above, so we can test
# for a valid DB handle
if (! ref $db)
{
	Err("<error>$db</error>");
}
else
{
	if ( $rv = LoadParams() )
	{
		Err($rv);
		exitScript();
	}

 	_default_handler();
}

exitScript();


