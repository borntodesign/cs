#!/usr/bin/perl -w
# LoginMember.cgi
# created: 09/16/13	Bill MacArthur
# last modified: 06/17/15	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use G_S;
#use Data::Dumper;
require Login::csLogin;
binmode STDOUT, ":encoding(utf8)";

my $cgi = new CGI;
my $gs = G_S->new();
my ($params, $tmpl, $language_pack, $member);

# this is a map of error codes that we can expect in here as they map to textual errors
my %errors_map = (
	1 => 'na4partnertype',
	2 => 'login_expired',
	3 => 'incorrect_pwd',
	4 => 'nologin',
	5 => 'code5',
	6 => 'code5',
	7 => 'code7',
	8 => 'code7',
	9 => 'must_login'
);

# be sure to add any login form template IDs to @valid_login_form_ids in Login::Login.pm
my %TMPL = (
	'ag_login'			=> 546,
	'default_login' 	=> 545,		# default login form
	'default_success'	=> 544		# this renders a simple window with a javascript that causes the top window to load the destination URL
);

my $csL = Login::csLogin->new({'cgi'=>$cgi, 'gs'=>$gs});
$gs->db( $csL->db );


$params = $csL->LoadParams();
$params->{'destination'} ||= '/cs/';

$language_pack = $csL->LoadLanguagePack();
$gs->Prepare_UTF8($language_pack,'encode');
unless ($params->{'_action'})
{
	# we may have received an error code and an error string
	# for now, we will just place our translated error if we got one
	$csL->errors( $language_pack->{ $errors_map{$params->{'err'}} } ) if $params->{'err'} && $errors_map{ $params->{'err'} };
	LoginForm();
}
elsif ($params->{'_action'} eq 'login')
{
	Login();
}
elsif ($params->{'_action'} eq 'ckonly')
{
	CookieOnly();
}
elsif ($params->{'_action'} eq 'logout')
{
	$csL->Logout();
}
else
{
	$csL->Error("Unrecognized _action parameter: $params->{'_action'}");
}

$csL->Exit();

###### ######

sub CookieOnly
{
	# since we are only interested in setting a cookie, we will not be doing the whole friendly error thing
	if (! $params->{'username'})
	{
		$csL->Error('no username provided');
	}
	
	if (! $params->{'password'})
	{
		$csL->Error('no password provided');
	}

	my $error;
	($member, $error) =  $csL->authenticate($params->{'username'}, $params->{'password'});

	if ($error)
	{
		$csL->Error("could not authenticate with the username and password provided: $error");
	}
	CreateCookies($member);
	print
		$cgi->header('-charset'=>'utf-8', '-cookies'=>$csL->setHeaderCookie),
		$cgi->start_html,
		$cgi->p('A CS cookie should have been successfully set.'),
		$cgi->end_html;
}

sub CreateCookies
{
	my $member = shift;
	$csL->setHeaderCookie( $csL->Generate_AuthCustom_Generic_Cookie( $member ) );
	$csL->setHeaderCookie( $cgi->cookie('-name'=>'id', '-value'=>$member->{'id'}, '-expires'=>'1y', '-domain'=>'.clubshop.com', '-path'=>'/') );
	$csL->setHeaderCookie( $cgi->cookie('-name'=>'pa', '-value'=>$member->{'alias'}, '-expires'=>'1y', '-domain'=>'.clubshop.com', '-path'=>'/') );
	$csL->setHeaderCookie( $cgi->cookie('-name'=>'country', '-value'=>$member->{'country'}, '-expires'=>'1y', '-domain'=>'.clubshop.com', '-path'=>'/') );
	$csL->setPnsidCookie($member->{'id'});
}

sub Login
{
	if (! $params->{'username'})
	{
		$csL->errors( $language_pack->{'nousername'} );
	}
	
	if (! $params->{'password'})
	{
		$csL->errors( $language_pack->{'nopwd'} );
	}

	if ($csL->errors)
	{
		LoginForm();	# LoginForm() exits when done
	}
	else
	{
		my $error = ();
		($member, $error) =  $csL->authenticate($params->{'username'}, $params->{'password'});

		if ($error)
		{
			$csL->errors( $language_pack->{ $errors_map{$error} } );
			
			LoginForm();
		}

		CreateCookies($member);
		$csL->db->do("INSERT INTO membership_logins (id, ipaddr) VALUES (?,?)", undef, $member->{'id'}, $cgi->remote_addr);
		
		$csL->Display($TMPL{'default_success'}, $member);
	}
}

=head3 LoginForm

Render a login form.
We will use the default login form template unless we have a valid formID HTTP parameter

=cut

sub LoginForm
{
	$csL->NegateCookies() if $csL->errors;
#	$csL->Display($TMPL{$params->{'formID'} || 'default_login'});

	# as long as Login->LoadParams is being used, the formID parameter will be checked for conformity to expected template IDs
	$csL->Display($params->{'formID'} || $TMPL{'default_login'});
}

__END__

Change log:

03/13/15
	Corrected an encoding issue that was observed, by doing Prepare_UTF8 on the $language_pack

05/11/15	Added the binmode on stdout
06/11/15	Added the pnsid cookie creator in CreateCookies()
06/17/15	Added the pa cookie