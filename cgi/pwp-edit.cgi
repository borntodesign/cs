#!/usr/bin/perl -w
###### pwp-edit.cgi
######
###### the interface for building Personalized Web Pages
###### this script started as an exact copy of appx and was dissected for this purpose
###### deployed: 01/08	Bill MacArthur
######
###### modified: 01/18/12	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($SALT $LOGIN_URL $ERR_MSG);
use PartnerSubscriptionAccess;
use CGI;
use DB_Connect;
use DBD::Pg qw(:pg_types);
use Encode;
use String::Random;													
use Data::Dumper;
require XML::Simple;
require XML::local_utils;
require XML::Validate::LibXML;
use CGI::Carp qw(fatalsToBrowser set_message);
set_message($ERR_MSG);

###### CONSTANTS
our $APP_AGE = "30 days";	# An SQL Interval - how long we'll hold the process_state record.

# the role a VIP must be at to approve organizational member's pages ( the interface in general)
our $COACH_APPROVAL_LEVEL = 11;

###### GLOBALS
our $q = new CGI;
our $gs = new G_S;
our $script_num = 2;		# The number of the application for the process_state table.
our $ME = $q->script_name;
our $xs = new XML::Simple('NoAttr'=>1);
our %TMPL = (
	'config_xml'		=> 10587,	# the parameter definitions are in there for one thing
	'content_xml'		=> 10585,	# initial value only, see Content_XML()
	'language_pref_xml'	=> 10431,	# this is fine for the language_code of the PWP
	'xml_err'		=> 10435	# a simple error handler template (has ClubAdvantage graphic embedded :(
);
###### only used in Content_XML to determine if a template load/reload is necessary
###### needs to retain value between calls
our $xml_tmpl = ();

our $mbr_qry = "
	SELECT m.id, m.alias, m.spid, m.membertype,
		m.firstname, m.lastname,
		COALESCE(m.country, '') AS country,
		COALESCE(m.emailaddress, '') AS emailaddress,
		COALESCE(m.password, '') AS password,
		COALESCE(m.language_pref, '') AS language_pref,
		COALESCE(m.cellphone, '') AS cellphone,
		COALESCE(m.phone, '') AS phone,
		m.memo,
		m.oid,
		COALESCE(m.company_name, '') AS company_name
	FROM members m WHERE ";
our $config_xml = our $data_xml = our $content_xml = '';
our ($db, $config_hash, $app_type, @errors, $app_step, $pwp_record) = ();
our %params = ();
our $state = ();		# our session state data

our $member = ();		# this will be a hashref of our 'new' member data at some points
our $RedoFlag = ();		# a shared flag to restart our main 'loop' - *careful now*
our $SkipDisplay = ();	# a shared flag to bypass the Display routine
				# this would typically be used after a trapped error

###### rather than stuffing extra bits of data into the param hash
###### we'll include an extra node under 'data' called 'extras'
###### this hash will be the pot for those bits
# we are also going to initialize a 'random' value to embed in some URLs
#(like images so that the browser will refresh them instead of caching them)
our %extras = ('random'=>time);

our $ADMIN = Admin_Mode_Ck({'ck_only'=>1});

###### we won't waste our time doing VIP cookies if we are in Admin mode
###### an id parameter will be required in that case and will be used to fill $mem_info
our $mem_info = $ADMIN ? {} : UserCK();	# a hashref of our logged in user (or undef if none)

our $VIP_login = 1 if $mem_info && $mem_info->{'membertype'} eq 'v';

###### available to modules as necessary rather than having to do it each time in those
###### extract our path information
# remember to use natural placeholder selectors ie. (1) is the first path element
# the first (0) value is undef since the path starts with a /
our @path_info = split /\//, $q->path_info;

################################
###### MAIN starts here.
################################

# Connect to the db.
$db ||= DB_Connect('pwp');	###### we may be connected as ADMIN
exit unless $db;
$db->{'RaiseError'} = 1;
$db->{'pg_enable_utf8'} = 1;

my $psa = PartnerSubscriptionAccess->new($q, {'db'=>$db, 'G_S'=>$gs});
Exit() unless $psa->CGI_AuthorizeOrRedirect(2);	# "2" is the resourceID

# Get the application config xml and put it in a hash for processing.
$config_xml = $gs->Get_Object($db, $TMPL{'config_xml'}) || die "Failed to load XML template: $TMPL{'config_xml'}\n";
$config_hash = $xs->XMLin("<opt>$config_xml</opt>", 'NoAttr'=>0);

###### we should be able to determine the app_type at this time
die "Failed to determine application type" unless $app_type = DetermineAppType();
$params{'app_type'} = $app_type;

###### pull in any 'track' specific routines
###### they should be available in the cgi-lib/pwp directory as track-name .pm
###### by using 'require' instead of 'use', we avoid crashing the app if the module
###### is non-existent or broken
# (it'll only be broken for that track if the module is broken)
eval ("require 'pwp/$app_type" . '.pm\'');

###### ###### only VIPs and admin allowed ###### ######
unless ($ADMIN || $VIP_login)
{
	Err($q->a({'-href'=>"$LOGIN_URL?destination=" . $q->script_name . $q->path_info},
	"Login is required to use this track"));
	Exit();
}
	
###### we should be able to determine the app_step at this time as well
die "Failed to determine application step" unless DetermineAppStep();

our $cfg = new _my_private_;

# Load the params into a local hash.
Load_Params();

# call our module init block to initialize our module global vars, etc.
eval("$app_type" .'::init()');
warn $@ if $@ && $@ !~ /Undefined subroutine &\w+::init/;

	###### We will start a 'loop' here.
{	###### If we want to restart processing, we can issue a 'redo'.
	###### More elegant than a 'goto'. If necessary, we can move this up higher.
$RedoFlag = undef;	# careful now, we don't want to get in an infinite loop
#die "in errors\n";
###### we are doing these before checking params, so keep that in mind
###### we could receive some bogus stuff
foreach(@{$cfg->operations('stage0')})
{
	my $brk = eval $_;
	die $@ if $@;
	Exit() unless $brk;
}

redo if $RedoFlag;

Check_Params();

unless (@errors)
{
	foreach(@{$cfg->operations('pre')})
	{
		my $brk = eval $_;
		die $@ if $@;
		Exit() unless $brk;
	}
}

if (@errors)
{
	# if there are errors in the application, we don't want to move to the next step
	# by setting the next step value, to our current step, we'll be sure to come back
	###### This same functionality may come in handy for other things later on :)
	$params{'next_step'} = $app_step;

	# This is a hack, but I think it will work out OK.
	# If not then we may have to include error_comp or other mechanisms
	# to ensure that the data structures are in place when needed.
	# The idea is that the current step may not have the 'vars' defined, so we need
	# to define them. Otherwise, the needed data won't be loaded.
	$cfg->vars('load_languages', 1) if defined $params{'language_pref'};
	$cfg->vars('load_countries', 1) if (defined $params{'country'} || defined $params{'mop_country'});
	$cfg->vars('load_states', 1) if defined $params{'state'};

	foreach(@{$cfg->operations('error_comp')})
	{
		my $brk = eval $_;
		die $@ if $@;
 		Exit() unless $brk;
	}
}
else
{
	foreach(@{$cfg->operations('post')})
	{
		my $brk = eval $_;
		die $@ if $@;
		Exit() unless $brk;
	}
}

redo if $RedoFlag;

# Now build out the page.
Display_Page() unless $SkipDisplay;

last;	###### out-of-control LOOP buster
}	###### the end of our 'loop' :)

Exit();

################################
###### Subroutines start here.
################################

sub AccessCK
{
	###### make sure not just any Tom, Dick or Harry can access this
	if ($ADMIN){
		die "pk or id parameter is required in admin mode\n" unless $params{'pk'} or $params{'id'};
		return 1;
	}
	# no profile record, then they can start a new page
	return 1 if ! $pwp_record;
	# for now only the VIP themselves can edit
	return 1 if $pwp_record->{'id'} == $mem_info->{'id'};
	Err("You do not have permission to access this document\n");
	return undef;
}

sub Admin_mem_info_ck
{
	return 1 unless $ADMIN;

	###### when accessing the default (user page list) track in admin, we need an 'id' parameter
	###### that we can populate $mem_info from if it's empty
	###### (like it would be unless we are also logged in as a VIP)
#	return 1 if $mem_info;	###### actually we need to override any VIP login 
	die "id parameter is necessary when in Admin mode\n" unless $params{'id'};
	$mem_info = $db->selectrow_hashref("$mbr_qry id=?", undef, $params{'id'}) ||
		die "Failed to find a member record matching: $params{'id'}\n";
	return 1;
}

sub Admin_Mode_Ck
{	###### are we staff logged into the database
	my $args = shift;
	require '/home/httpd/cgi-lib/ADMIN_DB.pm';
	###### we'll swap out DB connections if this is admin
	if ( ($q->cookie('operator') && $q->cookie('pwd')) &&
		(my $dbh = ADMIN_DB::DB_Connect( 'pwp', $q->cookie('operator'), $q->cookie('pwd') )))
	{
		$db->disconnect if defined $db;
		$db = $dbh;
		$extras{'admin'} = 1;	###### something to carry through into the data/extras XML
		return { 'operator' => $q->cookie('operator'), 'pwd' => $q->cookie('pwd') };
	}
	Err("Database login required for this track.") unless exists $args->{'ck_only'};
	return undef;
}

sub ApprovalCK
{
	return 'TRUE';	# Jan. 2012 per Dick... make 'em all self-approved
	return 'TRUE' if $ADMIN;
	my ($role) = $db->selectrow_array('SELECT a_level FROM a_level WHERE id= ?', undef, $mem_info->{'id'});
	return ($role || 0) >= $COACH_APPROVAL_LEVEL ? 'TRUE' : 'NULL';
}

sub AdjustCompletion
{
	###### we will perform a very simple update on the completion flag
	my $complete = $params{'complete'} ? 'TRUE':'FALSE';
	my $rv = $db->do("UPDATE pwp_pages SET complete= $complete WHERE pk= ?", undef, $params{'pk'});
	if ($rv eq '1')
	{
		$extras{'messages'} = $params{'complete'} ? 'mark-complete' : 'mark-incomlete';
		Get_Page_Record();
	}
	###### as of 01/08 this is called as a 'post' routine,
	###### so if the update fails they will know by the status at the top of the page
	return 1;
}

###### Build and combine the different menus.
sub Build_Menus
{
	my $menus = '';

	# Load the language menu into an xml block.
	$menus .= ($gs->Get_Object($db, $TMPL{'language_pref_xml'}) ||
		die "Failed to load XML template: $TMPL{language_pref_xml}\n")
		if $cfg->vars('load_languages');

	return \$menus;
}

sub Check_Params
{
	###### create a unique list of required params and submitted params
	my %uniq = map {$_ => 1} ((keys %params), $cfg->req);

	###### by keeping all our checks within the same loop
	###### we can keep our errors grouped and sorted together
	foreach (sort {$cfg->sort($a) <=> $cfg->sort($b)} keys %uniq)
	{
	###### do our 'required' checks first
		if ($params{$_} eq '')
		{
			if ($cfg->req($_))
			{
				die "A required parameter is missing: $_"
					if $cfg->req_err_level($_) eq 'die';
				push @errors, [$_, 'missing_error']
					if $cfg->req_err_level($_) eq 'warn';
			}
			next;	###### no use continuing without a value
		}

	###### perform max-length validations
		if ( $cfg->length($_) && (length($params{$_}) > $cfg->length($_)) )
		{
			push @errors, [$_, 'length_error']	;
			next;
		}

	###### perform our field specific validation
		my $checks = $cfg->check($_); # we are receiving an arrayref or undef
		# if there is no defined validation or no param value, then we're done
		next unless $checks;
		foreach my $ck_item (@$checks)
		{
			# we can perform simple matches
			# if we don't care about setting up error messages
			# like if we are going to 'die' on error
			# For these we will be specifying the correct condition.
			# If that doesn't match, then the 'error condition' will be triggered.
			if ($ck_item =~ m#/#)
			{
				my $rv = eval('$params{$_}' . "$ck_item");
				if (! $rv)
				{
					die "A required parameter is invalid: $_\n" .
						"Value submitted: $params{$_}\n"
						if $cfg->error_level($_) eq 'die';
					push @errors, ['generic_error', "Invalid $_ parameter"]
						if $cfg->error_level($_) eq 'warn';
				}
			}
			else	###### we'll handle errors in the respective routines
			{
			###### we will look for names that begin with 'Ck_' and divert
			###### those accordingly
				if ($ck_item =~ /^Ck_/){ Ck_($ck_item, $params{$_}, $_) }
				else
				{
					eval($ck_item . '($_)');
					die "$@\nsub=$ck_item\nvalue=$params{$_}\nname=$_\n" if $@;
				}
			}
		}

		###### we don't want the same first and last names
		###### by pattern matching on a possible prefix, we'll be covered
		###### on a direct to VIP signup where we'll be asking for a co-applicant
		if (/(.*)lastname$/ && $params{($1 || '') . 'firstname'})
		{
			push @errors, [$_, 'name_match_disallowed']
				if $params{($1 || '') . 'firstname'} eq $params{($1 || '') . 'lastname'};
		}
	}
}

sub Ck_	###### a catchall validation routine
{
	my $sub = shift;
	my $value = shift;
	my $name = shift || '';
	die "Missing validation identifier" unless $sub;

	###### I guess we'll return FALSE if there is nothing to check (for now anyway)
	return unless $value;

	# rv is our flag that we caught something
	# err_code is an error defined in our master xml document under the errors node
	# in the abscence of a predefined code, this could be some text to give some
	# kind of intelligible error

	###### we may sometimes want to pass some textual values in as well, like for Ck_Invalid_Characters
	###### since this has proven impossible due to the environment, we will improvise by extracting
	###### the text and creating an argument to pass in after the regular $value argument
	my $xargs = ();
	if ($sub =~ s/\((.*)\)//){	# we are looking for a set of parenthesis with some text inside
		$xargs = $1;
		###### get rid of leading and trailing single/double quotes
		$xargs =~ s/^('|")|('|")$//g if $xargs;
	}
	my ($rv, $err_code, $err_txt) = eval($sub . '($value, $xargs)');
	die "$@\nsub=$sub\nvalue=$value\nname=$name\n" if $@;
	if ($rv)
	{
		if ((! $name) || $cfg->error_level($name) eq 'die')
		{
			my $errstr = "A parameter is invalid: ${\$name || 'name not defined'}\n";
			$errstr .= "Value submitted: $value\n";
			###### a special case for Ck_Invalid_Characters which will report what characters were bad
			$errstr .= 'rv Error reported: ' . $q->escapeHTML($rv) . "\n" if $rv ne '1';
			$errstr .= "Error code reported: $err_code\n" if $err_code;
			$errstr .= "Error reported: $err_txt\n" if $err_txt;
			die $errstr;
		}
		elsif ($cfg->error_level($name) eq 'warn')
		{
			if ($err_code){ push @errors, [$name, $err_code] }
			elsif ($err_txt){ push @errors, ['generic_error', $err_txt] }
			elsif ($rv ne '1'){ push @errors, [$name, $q->escapeHTML($rv)]}
			else {
			# theoretically, this case should not normally happen
			# but if there is a configuration setup error it could
				my $errstr = "Validation failed for $name: " . $q->escapeHTML($value);
				push @errors, ['generic_error', $errstr];
			}
		}
		else { # I guess we do nothing
		}
	}
}

sub Ck_Digits_Only
{
	my $rv = $_[0] =~ /\D/;
	return ($rv) ? (1, 'digits_only') : undef;
}

sub Ck_Invalid_Characters
{
	###### our second argument should be a list of invalid characters
	my $rv = $_[0] =~ /($_[1])/;
	return $rv ? "Invalid Characters: $1" : undef;
}

sub Ck_No_Digits
{
	my $rv = $_[0] =~ /\d/;
	return ($rv) ? (1, 'alpha_error') : undef;
}

sub Ck_page_name_dupe
{
	my ($dupe) = $db->selectrow_array('
		SELECT pk FROM pwp_pages WHERE pk != ? AND page_name= ?', undef, ($params{'pk'} || 0), $params{page_name});
	return ($dupe) ? (1, 'duplicate_page_name') : undef;
}

sub Ck_page_name_valid
{
	###### we only want letters, digits and - _ .
#	my $test = $params{page_name};
###### presumably due to utf8 encoding issues,
###### this value cannot be used as it goes through the regex engine even with upper ascii characters
###### so we will try the actual CGI.pm value
	my $test = $q->param('page_name');
	
	###### we'll strip out what we will allow and then check to see if anything is left
	###### I don't feel like figuring out a complex regex with assertions
	$test =~ s#\w|\-|\.##g;
	return ($test) ? (1, 'invalid_page_name_chars') : undef;
}

sub CoachUpdatePageStatus
{
	###### either approve or disapprove a page after making sure the party is in our organization
	die "That page is not in waiting status\n" if length($pwp_record->{approved});
	my ($rv) = $db->selectrow_array("
		SELECT upline FROM relationships WHERE id= $pwp_record->{'id'} AND upline=?", undef, $mem_info->{'id'}) ||
		die "The page doesn't seem to belong to a member of your organization\n";
	die "Invalid 'work' parameter\n" unless $params{work} =~ m/^approve$|^disapprove$/;

	my $status = $params{work} eq 'approve' ? 'TRUE' : 'FALSE';
	$db->do("UPDATE pwp_pages SET stamp=NOW(), approved= $status, coach=?
		WHERE void=FALSE AND approved IS NULL AND pk= $pwp_record->{'pk'}", undef, $mem_info->{'id'});
	return 1;
}

sub CountMetaTags
{
	###### since we want all 10 tags filled (as of 01/08),
	###### and we only want the one error identified with the label
	###### we'll mark them as optional in our config and do our real checking here
	my $cnt = 0;
	foreach (keys %params)
	{
		$cnt++ if /meta_tag\d/ && $params{$_};
	}
#die "cnt=$cnt\n";
	return 1 if $cnt == 10;
	push @errors, ['meta_tags', 'missing_error'];
	return 1;
}

sub DetermineAppStep
{
	my $arg = shift;	
	my $rv = $arg || $q->param('next_step') || 'step0';
	###### make sure we have a pre-defined app_step
	my $match = ();
	foreach (keys %{$config_hash->{$app_type}})
	{
		###### we are only looking for nodes matching 'stepxx'
		next if $_ !~ '^step\d+';
		if ($_ eq $rv)
		{
			$match = $_;
			last;
		}
	}
	die "Invalid app_step: $rv\n" if ($rv && !$match);
	$params{'app_step'} = $app_step = $match;
	return 1;
}

sub DetermineAppType
{
	###### Initially we should have an app type embedded in the path after the
	###### 'sponsor' or 'first' path component. Alternatively, we could receive an app_type param
	my $rv = $q->param('app_type') || $path_info[2];

	# some parties and maybe some browsers were confusing the //upg and similar URLs to have redundant
	# // , thereby taking matters into their own hands and breaking the URL in the process
	# we'll try to accomodate them by recognizing an alpha path component in the first slot if all else fails
	# with the exception of default_01, there won't be any tracks with a digit in them and it won't be broken ;)
	$rv ||= $path_info[1] if $path_info[1] && $path_info[1] !~ /\d/;

	$rv ||= 'default';
	###### make sure we have defined the app_type
	my $match = ();
	foreach (keys %{$config_hash})
	{
		###### besides our fields key the only nodes in our root
		###### (on 03/30/05) are app_type nodes
		next if $_ eq 'fields';
		if ($_ eq $rv)
		{
			$match = $_;
			last;
		}
	}
	die "Invalid app_type" if ($rv && !$match);
	if (! $config_hash->{$match}{'active'})
	{
		Err("This application type is not active: $match");
		Exit();	###### crude, but effective
	}

	###### since we have a valid app type,
	###### let's see if we can load the configuration for it
	$/ = undef;	# slurpy
	open (CFG, "/home/httpd/cgi-templates/xml/pwp/$match.xml") || die "Failed to load app type xml\n";
	my $xml = <CFG>;
	close CFG;
	$/ = "\n";
	$config_hash->{$match} = $xs->XMLin($xml, 'NoAttr'=>0);
	###### merge this node into the master document
	$config_xml =~ s#<$match.*?/>#$xml#;
	return $match;			
}

###### Prepare the page and display it to the browser.
sub Display_Page
{
	###### accept some stuff if we don't want/need the default
	# for now we'll handle:
	# xsl		- a replacement template ID
	# xml		- a replacement for all the normal stuff generated
	# extra_xml	- you got it, some extra to roll in with the regular
	my $args = shift || {};
	my $xsl = $args->{'xsl'} || $cfg->xslt;
	my $xml = $args->{'xml'};
	my $profile_xml = '';
	if ($pwp_record)
	{
		$profile_xml = $pwp_record->{'profile_xml'} || '';
		delete $pwp_record->{'profile_xml'};
	}
	my $page_xml = $xs->XMLout($pwp_record, 'RootName'=>'page') || '';
	Roll_Data_XML() unless $xml;
	Load_Content_XML();
	# Combine the remaining pieces of the xml document if applicable.
	$xml ||=  	"<base>\n" .
			$config_xml .
			$data_xml .
			$content_xml .
			($args->{'extra_xml'} || '') .
			"<timestamp>${\scalar localtime()}</timestamp>" .
			$page_xml .
			$profile_xml .
			"\n</base>";

	$xsl = eval ('$gs->Get_Object($db, $xsl)') || die "$@\nFailed to load XSL template: $xsl\n";

	if ($q->param('xml') && $ADMIN)
	{
		if ($q->param('xml') =~ '^all$|^1$'){ EmitXML($xml) }
		elsif ($q->param('xml') eq 'xsl'){ EmitXML($xsl) }
		elsif ($q->param('xml') eq 'config'){ EmitXML($config_xml) }
		elsif ($q->param('xml') eq 'content'){ EmitXML($content_xml) }
		elsif ($q->param('xml') eq 'data'){ EmitXML($data_xml) }
		elsif ($q->param('xml') eq 'page'){ EmitXML("<r>$page_xml\n$profile_xml</r>") }
		else {	###### reroll data_xml into just what we want and use that
			Roll_Data_XML([$q->param('xml')]);
			EmitXML($data_xml)
		}
	}
	else
	{
		print $q->header(-charset=>'utf-8', -expires=>'now');
#		print $q->header('text/plain');
		print XML::local_utils::xslt($xsl, $xml);
	}
}

sub Dump_Errors
{
	return 1 unless @errors;
	
	###### a rather crude to way to handle 'errors' that shouldn't be
	print "Content-type: text/plain\n\n";
	print "Error: $_\n" foreach @errors;
	return;
}

sub EmitXML
{
	print $q->header(-charset=>'utf-8', -type=>'text/xml');
	#print $q->header(-type=>'text/plain', -charset=>'utf-8');
	print $_[0];
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html('Error'), $q->h3($_[0]), $q->p,
		$q->p('Current server time: ' . scalar localtime()), $q->end_html;
}

sub Err_xml
{	###### Instead of an rough english error message, we may want to use
	###### a translatable message. In that case we can put it/them
	###### into a simple stylesheet.
	my $args = shift;	# for now we expect a hashref with a key called 'list'
				# which points to an arrayref
	my $errata = shift || '';
	my $xml = "<error_errata>$errata</error_errata><error_list>\n";
	$xml .= ('<' . $_ . " />\n") foreach @{$args->{list}};
	$xml .= "</error_list>\n";
	Display_Page({xsl=>$TMPL{xml_err}, extra_xml=>$xml});
}

sub Get_Page_Record
{
	my $rv = {};
	###### we could come in without a pk on a new page creation
	if ($params{'pk'})
	{
		my @cols = @_;
		my $cols = '';
		if (@cols){
			$cols .= "p.$_," foreach @cols;
			chop $cols;
		} else{
			$cols = 'p.*';
		}
		my @args = $params{'pk'};
		my $qry = "
			SELECT $cols, pi.pk AS image_pk
			FROM pwp_pages p
			LEFT JOIN pwp_images pi ON pi.pk=p.pk
			WHERE p.void=FALSE AND p.pk= ? ";
		if ($params{'id'}){
			$qry .= ' AND p.id=?';
			push @args, $params{'id'};
		}
		my $rv = $db->selectrow_hashref($qry, undef, @args) ||
				die "Failed to retrieve a record with the supplied parameters: pk:$params{'pk'} id:$params{'id'}\n";
		# let's make all our values empty rather than uninitialized
		foreach (keys %$rv){ $rv->{$_} = '' unless defined $rv->{$_} }

		$pwp_record = $rv;
	}
	###### now for an ADMIN hack
	###### we don't need an ID as long as we have a PK, but we need identification information at the top of the page
	###### and when in ADMIN mode, we don't have that info because $mem_info is empty
	###### so...
	$mem_info ||= $db->selectrow_hashref("$mbr_qry id= ?", undef, ($rv->{'id'} || $params{'id'}));
	return 1;
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Get_Coach_Page_List
{
	###### get a list of 'downline' pages for this Coach
	# we are going to use a 'qry' parameter to determine the type of report
	# to start with we are only going to recognize a default of "mine" meaning pages that awaiting MY approval
	# later we can define other kinds of lists with different requirements
	# we will setup the various query pieces and assemble them to create the whole
	# this should simplify satisfying various requirements
	$params{'qry'} ||= 'mine';
	my $Tables = q|
		pwp_pages pwp
		INNER JOIN members m ON pwp.id=m.id AND m.membertype='v'
		LEFT JOIN pwp_images pwi ON pwp.pk=pwi.pk|;

	my $Body = q|m.alias, m.id, pwp.pk, pwp.page_name, pwp.stamp,
			SUBSTR((CASE WHEN pwp.stamp >= COALESCE(pwi.stamp, pwp.stamp) THEN pwp.stamp
				ELSE pwi.stamp
			END)::TEXT, 0, 20) AS last_modified,
			CASE WHEN pwp.approved IS NULL THEN 'null'::TEXT
				WHEN pwp.approved=FALSE THEN 'false'::TEXT
				ELSE 'true'::TEXT
			END AS approval_status|;

	my $Criteria = q|pwp.void=FALSE AND pwp.approved IS NULL AND pwp.coach=?|;
	my $Sort = 'ORDER BY last_modified';
	my @Args = ();

	if ($params{'qry'} eq 'my-team')
	{
		$Tables .= q|
			LEFT JOIN members mc ON mc.id=pwp.coach
			INNER JOIN relationships r ON r.id=pwp.id|;
			
		$Body .= q!,mc.alias AS coach_alias, mc.firstname||' '||mc.lastname AS coach_name!;
		$Criteria = q|pwp.void=FALSE AND pwp.approved IS NULL AND r.upline=?|;
	}
	
	my $sth = $db->prepare("SELECT $Body FROM $Tables WHERE complete=TRUE AND $Criteria $Sort");
	$sth->execute($mem_info->{'id'}, @Args);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @{$extras{page_list}}, $tmp;
	}
	return 1;
}

sub Get_User_Page_List
{
	###### get a list of pages for this user
	my $sth = $db->prepare("
		SELECT
			pwp.pk,
			pwp.id,
			pwp.complete,
			CASE WHEN pwp.approved IS NULL THEN 'null'::TEXT
				WHEN pwp.approved=FALSE THEN 'false'::TEXT
				ELSE 'true'::TEXT
				END AS approval_status,
			pwp.page_name,
			COALESCE(r.alias,'') AS coach_alias,
			COALESCE((r.firstname||' '||r.lastname),'') AS coach_name,
			SUBSTR((CASE WHEN pwp.stamp >= COALESCE(pwi.stamp, pwp.stamp) THEN pwp.stamp
				ELSE pwi.stamp
			END)::TEXT, 0, 20) AS last_modified
		FROM pwp_pages pwp
		LEFT JOIN pwp_images pwi
			ON pwp.pk=pwi.pk
		LEFT JOIN members r
			ON r.id=pwp.coach
		WHERE pwp.void=FALSE
		AND pwp.id= ?");
	$sth->execute($mem_info->{'id'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @{$extras{page_list}}, $tmp;
	}
	return 1;
}

sub Load_Content_XML
{
	###### conditionally pull in the correct XML doc (global lang_blocks)
	###### on a first run through, the var will be undef
	###### in rare cases it may on a redo require a different doc than the first time
	if (! defined $xml_tmpl || 
		$xml_tmpl != ((@errors ? $cfg->vars('err_xml') : '') ||
				$cfg->vars('xml') ||
				$TMPL{'content_xml'})
	)
	{
		$xml_tmpl = ((@errors ? $cfg->vars('err_xml') : '') || $cfg->vars('xml') || $TMPL{'content_xml'});
		$content_xml = $gs->Get_Object($db, $xml_tmpl) ||
		die "Failed to load XML template: $xml_tmpl\n";
		$content_xml = decode_utf8($content_xml) unless Encode::is_utf8($content_xml);
	}
}

###### Load the params into a local hash.
sub Load_Params
{
	###### we will only load params that we are expecting
	###### in other words, if they aren't in the configuration, we don't want 'em
	foreach ( keys %{$cfg->{fields}} )
	{
		###### turn on the utf8 flag for our params
		$params{$_} = decode_utf8( defined($q->param($_)) ? $q->param($_) : '');

		###### we have to treat some fields special
		###### like emails (PreProcess removes underscores)
###### actually for this interface and the result, we want minimal filtering
###### so we will not run through the pre-processor at all
#		if (/^email/)
		if (1)
		{
			###### remove all control characters (below ASCII 32) * except newlines
			###### and other characters that should never be needed in the data
			###### like ? % ^ ~ | * ! =
			$params{$_} =~ s/[\x00-\x09]|[\x0B-\x1F]|\^|~|\||\*|=|\+//g;

			###### remove leading & trailing whitespace
			$params{$_} =~ s/^\s*|\s*$//g;
		}
		elsif (/^redurl/)	###### and '=' and '%'
		{	# in this case we don't care what we get
			# because will not be processing it
			# - do nothing -
		}
		else
		{
			$params{$_} = $gs->PreProcess_Input($params{$_});
		}
	}
}

sub Make_Connection_SSL
{
	###### since people will be prompted to change their password
	###### we may have those paranoid about the connection not being secure
	###### can call this routine from any 'track'
	###### or we may just end up calling it in all cases at some point
	return 1 if $q->https;
	(my $url = $q->self_url) =~ s/^http/https/;
	print $q->redirect($url);
	return;
}

sub PageCreation_AccessCK
{
	###### currently, 01/08, VIPs are allowed one page... we will enforce that here
	return 1 if $ADMIN;	# admin can create as many pages for whomever they choose

	###### let's see how many pages they have already
	my ($cnt) = $db->selectrow_array('SELECT COUNT(id) FROM pwp_pages WHERE id=?', undef, $mem_info->{'id'});
	return 1 unless $cnt;
	if ($cnt > 0 && ! $pwp_record){
		Err('VIPs are currently limited to 1 Personalized Web Page each');
		return undef;
	}
	return 1;
}

sub RecordImage
{
	###### this sets the max upload size
	$CGI::POST_MAX = 100000;
	require Image::Magick;

#	print $q->header('text/plain'), "$params{filename}\n";
	###### for some reason, the hash value for filename loses it's capability to dual server
	###### as a textual name and also as a file handle like CGI.pm intends
	###### so we will will grab the real deal (the script has already ensured it's presence and what not)
	my $file = $q->param('filename');
	my $submit_mime = $q->uploadInfo($file)->{'Content-Type'};
		
	my $image = Image::Magick->new;
	###### Internet Explorer sends the complete path and filename, so we have to implement this like so
	###### rather than like the regular documentation
	$image->Read(file=>\*$file);
	my ($width, $height, $mime) = $image->Get('width','height','mime');
	die "Failed to receive uploaded image\n" unless $width && $height;
#	print "specs b4: $height, $width, $mime\n";
	###### Internet Explorer currently sends pjpeg and x-png... go figure
	if (	($mime =~ m#^image/(jpg|jpeg)$# && $submit_mime !~ m#^image/.*(jpg|jpeg)$#) ||
		($mime =~ m#^image/png$# && $submit_mime !~ m#^image/.*png$#) ||
		($mime =~ m#^image/gif$# && $submit_mime !~ m#^image/gif$#))
	{
		die "Submitted mime type doesn't match calculated mime type\n";
	}
	elsif ($mime !~ m#image/(png|jpeg|jpg|gif)$#){
		die "Invalid mime type calculated: $mime\nOnly these types are allowed: jpg gif png\n";
	}

	if ($width > $height){
		$height = int($height * (100/$width));
		$width = 100;
	}
	elsif ($width < $height){
		$width = int($width * (100/$height));
		$height = 100;
	}
	$image->Resize('height'=> $height, 'width'=> $width);
	($width, $height, $mime) = $image->Get('width','height','mime');
#	print "specs after: $height, $width, $mime\n";

#$rv = $image->Write(filename=>'/home/httpd/html/temp/newlogo.gif');
#print "write results: $rv\n";
#	my $blob = $image->ImageToBlob();
#	print "blob: $blob\n";
	my ($sth, $qry) = ();
	if ($pwp_record->{image_pk}){
		$sth = $db->prepare("UPDATE pwp_images SET image= ?, mime_type= ? WHERE pk= $pwp_record->{image_pk}");
		$sth->bind_param(1, $image->ImageToBlob(), { 'pg_type' => DBD::Pg::PG_BYTEA });
		$sth->bind_param(2, $image->Get('mime'));
	}
	else{
		$sth = $db->prepare("INSERT INTO pwp_images (image, mime_type, pk) VALUES (?,?,?)");
		$sth->bind_param(1, $image->ImageToBlob(), { 'pg_type' => DBD::Pg::PG_BYTEA });
		$sth->bind_param(2, $image->Get('mime'));
		$sth->bind_param(3, $params{'pk'});
		$pwp_record->{'image_pk'} = $params{'pk'};
	}
	my $rv = $sth->execute;
	die "Failed to save picture: $rv\n" unless $rv eq '1';

	###### now update the page itself to require upline approval if necessary and to reflect the 'coach'
	my $coach = 'NULL';
	my $status = 'TRUE';
	if (ApprovalCK() ne 'TRUE')
	{
		($coach) = $db->selectrow_array("
			SELECT id FROM vip_select_by_level(?, $COACH_APPROVAL_LEVEL)", undef, $mem_info->{'id'});
			$status = 'NULL';
	}
	###### this may only happen to 12, but why take the chance
	$coach ||= 'NULL';

	$db->do("
		UPDATE pwp_pages
		SET approved= $status,
			coach= $coach
		WHERE pk= ? AND id= ?", undef, $pwp_record->{'image_pk'}, $mem_info->{'id'});
	return 1;
}

sub RecordPage
{
	my @profile = qw(where_going fav_clubshop_mall_store farthest_north children fav_movie skype_name telephone locality
		countries_visited meta_tag8 overcome_concerns fav_city farthest_south birth_place age_range name meta_tag1
		fav_vacation_spot join_date meta_tag6 meta_tag5 marital_status speak_languages meta_tag7 fav_book meta_tag4 meta_tag2
		fav_tv_show findout school like_about fav_song initial_concerns fav_music fav_sporting_team meta_tag9 meta_tag10
		neighboring_towns meta_tag3 bio_brief foods hobbies holidays college_teams);
	my $profile_xml = '<profile>';
	foreach (sort @profile)
	{
		my $tmp = $q->escapeHTML($params{$_});
		###### now convert newlines to HTML breaks
	# let's not - we can just do it on the way out to presentation instead
	#	$tmp =~ s#\n#<br />#g;
		$profile_xml .= "<$_>$tmp</$_>" if $params{$_};
	}
	$profile_xml .= '</profile>';

	###### now let's make sure the profile document is valid XML, otherwise their web page will give ugly errors
	###### we will go with a simple die as the errors could be too verbose and by dieing they go in the logs
	my $validator = new XML::Validate::LibXML;
	unless ($validator->validate($profile_xml))
	{
		die "A valid XML document could not be formed using your responses:\n\n
			${\$validator->last_error()}->{'message'}\n\n$profile_xml";
	}
	###### if the work is done by ADMIN, then we should be OK,
	###### otherwise we will require upline approval if we don't meet muster
	my $Approved = ApprovalCK();
	my ($coach) = $Approved eq 'TRUE' ? 'NULL' : $db->selectrow_array("
			SELECT id FROM vip_select_by_level(?, $COACH_APPROVAL_LEVEL)", undef, $mem_info->{'id'});

	###### this may only happen to 12, but why take the chance
	$coach ||= 'NULL';

	my ($qry, @args) = ();
	if ($params{'pk'})
	{
		$qry = "UPDATE pwp_pages
			SET operator=current_user, stamp=NOW(), approved= $Approved, profile_xml=?,
				language_code=?, page_name=?, coach= $coach
			WHERE pk= ?";
		push @args, ($profile_xml, $params{'language_code'}, $params{'page_name'}, $params{'pk'});
	}
	else
	{
		($params{'pk'}) = $db->selectrow_array("SELECT nextval('pwp_pages_pk_seq')") ||
			die "Failed to obtain a new record pk\n";

		$qry = "INSERT INTO pwp_pages (pk, coach, approved, id, profile_xml, profile_version, language_code, page_name)
			VALUES ($params{'pk'}, $coach, $Approved, ?, ?, 1, ?, ?)";
		push @args, ($mem_info->{'id'}, $profile_xml, $params{'language_code'}, $params{'page_name'});
	}
#	print $q->header('text/plain'), $profile_xml;
#	foreach (keys %params){print "$_ ";}
	my $rv = $db->do($qry, undef, @args);
	if ($rv eq '1')
	{
		$extras{'messages'} = 'record-page-success';
		Get_Page_Record();
		return 1;
	}
	else
	{
		push @errors, ['generic',"Recording of page failed: $DBI::errstr"];
		return undef;
	}
}

sub Redirect_User_First_Page
{
	###### if they don't have any pages on file,
	###### we may as well send them directly to the editor rather than to an empty list
	###### unless of course we are in ADMIN mode, then an empty list is preferable
	return 1 if $ADMIN;
	return 1 if $extras{'page_list'};
	print $q->redirect($q->url . '/0/editor');
	return undef;
}

sub RestoreState
{
	my $args = shift || {};
	###### move the saved parameters into the live hash with the exception
	###### of the ones that are pre-existent
	# unless we receive a 'force' argument
	my $rv = RetrieveState();
	die "Failed to retrieve process data.\n" unless $rv;

	###### the default root of the dumped data is $state, our global
	eval $rv;
	###### for now we have to do the same decoding thing here as we do with params
	###### once the DB is unicode we can drop this
#	$gs->Prepare_UTF8($state);

	foreach (keys %{$state})
	{
		$params{$_} = $state->{$_} unless defined $params{$_} && ! $args->{'force'};
	}
	return 1;
}

###### Retrieve the state of the app process from a dump
###### or simply retrieve some simple data elements.
###### Referenced by a pk value and its equiv encrypted value for security reasons.
###### The idea is that some component will know what to do with what comes back.
# this routine is called by other modules besides RestoreState()
sub RetrieveState
{
	my $data = ();
	# Find the primary key of the process state record using the encrypted value, if one exists.
	my $pk = CryptString('decrypt', $params{'pkc'});

	# If the crypted key matches the unencrypted key that was passed.
	if ( $pk && $pk eq $params{'pk'} )
	{
		# Get the params xml block using the provided pk value.
		$data = $db->selectrow_array("
			SELECT data
			FROM 	process_state
			WHERE 	pk = $pk
			AND 	application = 1");
	}
	# let the caller perform error handling if $data is undef
	# it may be just checking for existence and not want to die
	return $data;
}

sub RoleCK
{
	###### let's get the role for the 'user'
	return 1 if $ADMIN;
	($mem_info->{'role'}) =	$db->selectrow_array('SELECT a_level FROM a_level WHERE id=?', undef, $mem_info->{'id'}) || 0;
	if ($mem_info->{'role'} < $COACH_APPROVAL_LEVEL)
	{
		Err('Your pay level is insufficient to use this interface');
		return undef;
	}
	return 1;
}

sub Roll_Errors
{
	return '' unless @errors;
	###### combine all of our errors into an XML block
	my $rv = "<errors>\n";
	foreach (@errors){
		$rv .= "<$_->[0]";
		$rv .= ($_->[1]) ? ">$_->[1]</$_->[0]>\n" : "/>\n";
	}
	$rv .= "</errors>\n";
	return $rv;	
}

sub Roll_Data_XML
{
	my $arg = shift || 'data';
	$arg = [qw(member errors menus user sponsor params sponsor extras)] if $arg eq 'data';
	# arg can be an arrayref of *Just* the components we want instead of the default whole enchilada
	$data_xml = "<data>\n<script_name>$ME</script_name>\n";
	foreach (@$arg)
	{
		if ($_ eq 'params'){ $data_xml .= $xs->XMLout(\%params, 'RootName'=>'params') || '' }
		elsif ($_ eq 'errors'){ $data_xml .= Roll_Errors() }
		elsif ($_ eq 'menus'){ $data_xml .= '<menus>' . $${\Build_Menus()} . '</menus>' }
		elsif ($_ eq 'user'){ $data_xml .= $xs->XMLout($mem_info, 'RootName'=>'user') || '' }
		elsif ($_ eq 'member')
		{
			$gs->Null_to_empty($member) if $member;
			$data_xml .= $xs->XMLout($member, 'RootName'=>'member') || '';
		}
		elsif ($_ eq 'extras')
		{
			warn Dumper(\%extras);
			$data_xml .= %extras ? $xs->XMLout(\%extras, 'RootName'=>'extras') : '';
		}
	}
	$data_xml .= "</data>";
}

sub SaveState
{
	# we'll either save the raw stuff we've been passed or the params
	my $data = shift;
	unless ($data)
	{
	# Data Dumper recognizes the utf-8 flag and converts on the fly to ascii.
	# So, we need to turn off the flag for it work as we want it to.
		my %local_params = %params;
		_utf8_off(\%local_params);
		$data = Data::Dumper->Dump([\%local_params],['state']);
	}

	# Find the primary key of the process state record, if one exists.
	my $pk;
	$pk = CryptString('decrypt', $params{'pkc'}) if $params{'pkc'};

	# If the crypted key matches the unencrypted key that was passed.
	if ( $pk && $pk eq $params{'pk'} )
	{
		# Update the process state record.
		die "Failed to update process state\n" unless $db->do("
			UPDATE process_state
			SET 	application = $script_num,
				data = ?,
				expires = NOW() + INTERVAL '$APP_AGE'
			WHERE 	pk = ?", undef, $data, $pk);
	}
	else
	{
		$pk = $db->selectrow_array("SELECT NEXTVAL('process_state_pk_seq');");
	# Insert a new process state record.
		die "Failed to preserve process state\n" unless $db->do("
			INSERT INTO process_state (pk, application, data, expires)
			VALUES (?, $script_num, ?, NOW() + INTERVAL '$APP_AGE')",
		undef, $pk, $data);

	# Put the pk and pk encrypted into the params hash.
		$params{'pk'} = $pk;
		$params{'pkc'} = CryptString('encrypt', $pk);
	}
	return 1;
}

sub UserCK
{
	###### we want to determine if a logged in party is doing this
	my $ck = $q->cookie('AuthCustom_Generic');
	return unless $ck;

	###### if they have a good cookie we'll capture their info
	my ($id, $meminfo) = $gs->authen_ses_key( $ck );
	if ($id && $meminfo && $id !~ /\D/)
	{
		$gs->Prepare_UTF8($meminfo);
		return $meminfo;
	}
	return;
}

###### ###### ###### END OF Functional Subroutines
###### infrastructure routines below
sub _utf8_off
{
	my $hr = shift;
	foreach (keys %{$hr})
	{
		if (ref $hr->{$_} eq 'HASH'){ _utf8_off($hr->{$_}) }
		else { Encode::_utf8_off($hr->{$_}) }
	}
}

###### ###### ###### END OF SUBROUTINES

package _my_private_;
use Data::Dumper;

sub cfg
{
	my ($self, $arg) = @_;
	###### extract the value of a non-field item
	return $self->{$arg};
}

sub check
{
	###### return a 'list' of validation checks
	###### we'll start our list with 'pre_validation' checks
	###### then we'll add our global checks and finally tack on 'post_validation'
	my (@list, $s);
	foreach my $type ('pre_validation', 'validation', 'post_validation')
	{
		$s = $_[0]->{'fields'}{$_[1]}{$type};
		push @list, (ref $s ? @$s : ($s))
			if ($s && (! ref $s || ref $s eq 'ARRAY'));
	}
	return @list ? \@list : undef;
}

sub error_level { return $_[0]->{'fields'}{$_[1]}{'error_level'} || '' }

sub length { return $_[0]->{'fields'}{$_[1]}{'max'} }

sub new
{
	# we are assuming a one time call to this, although, I suppose it could
	# be reinitialized
	# we are also assuming that the config hash, app_type & app_step are initialized
	# we'll create our 'private' data structure from the existing ones
	die "Necessary contructs are not initialized"
		unless $config_hash && $app_type && $app_step;
	my %h = %{$config_hash->{$app_type}{$app_step}};

	foreach (keys %{$h{required}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>1,
			'optional'=>0
			),
			%{$config_hash->{'fields'}{$_}},	# pick up our global config directives
			%{$h{'required'}{$_}}			# pick up our additional config directives
		);
	}

	foreach (keys %{$h{optional}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>0,
			'optional'=>1
			),
			%{$config_hash->{'fields'}{$_}},
			%{$h{'optional'}{$_}}
		);
	}

	# roll in the 'global' param reqs and optionals
	foreach (keys %{$config_hash->{$app_type}{'required'}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>1,
			'optional'=>0
			),
			%{$config_hash->{'fields'}{$_}},	# pick up our global config directives
			%{$config_hash->{$app_type}{'required'}{$_}}			# pick up our additional config directives
		);
	}

	foreach (keys %{$config_hash->{$app_type}{'optional'}})
	{
		%{$h{fields}{$_}} = (
			(
			'required'=>0,
			'optional'=>1
			),
			%{$config_hash->{'fields'}{$_}},
			%{$config_hash->{$app_type}{'optional'}{$_}}
		);
	}

	# we want to allow some vars that are global to the app_type
	foreach (keys %{$config_hash->{$app_type}{vars}})
	{
		$h{'vars'}{$_} = $config_hash->{$app_type}{'vars'}{$_};
	}

	###### if our xslt node is a 'text' node,
	###### we'll convert it to a parent with a 'default' child
	$h{'xslt'} = { 'default' => $h{'xslt'} } unless ref $h{'xslt'};

	# we want to do the same for some xsl templates that are global to the app_type
	# this would be the case for perhaps some specific 'error' or special cases
	foreach (keys %{$config_hash->{$app_type}{'xslt'}})
	{
		$h{'xslt'}{$_} = $config_hash->{$app_type}{'xslt'}{$_};
	}

	return bless \%h;
}

sub operations
{
	# create a list of operations that have to be performed
	my ($self, $arg) = @_;
	my $ops = $self->cfg('operations');
	my $rv = $ops->{$arg};
	# if we have an empty node in our XML, we'll have an empty hashref here
	return [] unless defined $rv && ref $rv ne 'HASH';
	# if we have a list, it should be represented as an arrayref
	return (ref $rv) ? $rv : [$rv];
}

sub opt{ return _flip_req('optional', @_) }

sub req { return _flip_req('required', @_) }

sub req_err_level { return $_[0]->{'fields'}{$_[1]}{'req_err_level'} || '' }
sub sort { return $_[0]->{'fields'}{$_[1]}{'sort_order'} || 100 }

sub value
{
	return $params{$_[1]};
}

sub vars
{
	my ($self, $arg, $newval) = @_;
	$self->{'vars'}{$arg} = $newval if defined $newval;
	return $self->{'vars'}{$arg};
}

sub xslt
{
	my ($self, $arg) = @_;
	# if we are looking for XSLT, then we'll apply a bit of logic
	# we may have an error condition requiring a different template
	# or we may not have any conditional templates
	# (if we're doing conditionals, then we'd best have an error template)
	if ($arg){ return $self->{'xslt'}{$arg} }
	elsif (@errors){ return $self->{'xslt'}{'error'} || $self->{'xslt'}{'default'} }
	return $self->{'xslt'}{'default'};
}

sub _flip_req
{
	my ($what, $self, $key, $value) = @_;
	my $not_what = ($what eq 'required') ? 'optional' : 'required';
	unless ($key)		###### I guess we want the whole enchilada
	{
		my @list;
		foreach (keys %{$self->{fields}}){
			push @list, $_ if $self->{'fields'}{$_}{$what};
		}
		return @list
	}
	elsif ($value)	###### we want to change one
	{			###### we should receive a hashref
				###### like {param_name=>1} to make it required
		$self->{'fields'}{$key}{$what} = $value;
		$self->{'fields'}{$key}{$not_what} = ('1' cmp $value);
		return $value;
	}
		
	return $self->{'fields'}{$key}{$what};
}

1;

###### ###### ###### END OF _my_private_ PACKAGE

__END__

01/18/12	removed role related stuff in favor of pay level... however it is basically unneeded
