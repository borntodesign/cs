#!/usr/bin/perl -w
# clubcash_monthly_detail.cgi
# create the necessary XML file for * given id and a given month/months
# last modified: 03/16/16	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw/$LOGIN_URL/;
use ParseMe
require XML::Simple;
require XML::local_utils;
use CGI::Carp qw(fatalsToBrowser);
use CGI;
use DB_Connect;
use ClubCash;
#use Data::Dumper;

binmode STDOUT, ":encoding(utf8)";

###### GLOBALS/Cookies

my $q = new CGI;
$q->charset('utf-8');
my $db = '';
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});
our $ME = $q->script_name;
our $xs = new XML::Simple('NoAttr'=>1);
our $cgiini = 'clubcash_monthly_detail.cgi';
our $messages = '';
my  $member_id = 0;
my  $mem_info = '';
our $member_details = ();
our $login_error = 0;
our $login_type = '';
our %TMPL = (
	'xml'=> 10853,  
);
my $rowcount = 0;
my $admin = 0;
my $report_id = 0;

# Get and test the cookie information for access.

exit unless Get_Cookie();

# Get the xml language for putting out the page and convert to hash

my $xmt = $gs->GetObject($TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}\n";
my $lu = new XML::local_utils;
my $xml = $lu->flatXMLtoHashref($xmt);

### Get parameters from the web page

my $default = 0;
my %FORM = ();
    foreach ($q->param()){
	$FORM{$_} = $q->param($_);
    }
if ($FORM{disp_month}) {
    $default = $FORM{disp_month};
}
if ($FORM{report_id}) {
    $member_id = $FORM{report_id};
    $report_id = $member_id;
}
if ($FORM{id}) {
    $member_id = $FORM{id};
    $report_id = $member_id;
}
if ($FORM{admin}) {
    $admin = 1;
}
#warn "Clubcash_monthly_detail started, id = $member_id";
#$member_id = 4822841;
#    if ($member_id == 12) { $member_id = 3218569;}

##############################
# Check For ID Number or Die #
##############################

unless ($member_id)
{
	Err('Access Denied! No ID Number Submitted.');
}
elsif ($member_id =~ m/\D/)
{
	Err('Sorry, we have received an invalid ID value: ' . $q->escapeHTML($member_id));
}
elsif ($login_type eq 'admin')
{
	# nothing to be done
}
elsif ($login_type eq 'auth')
{
	# up until March 2016, this report was open for any logged in party to look at any other member's balances
	# per Dick March 2016: "...it is important for the Partner Sponsor and/or Coach to be able to see and report the ClubCash balances for their memberships.
	# So continue to allow them the privilege of gaining this info."
	#
	# So... make sure the logged in party is the same as the report ID -or- that the viewer is sponsor or coach of the report ID
	if ($member_id != $mem_info->{'id'})
	{
		my $tmp = $db->selectrow_hashref("SELECT sponsor, coach FROM my_sponsor_coach WHERE id= $member_id");
		if ($tmp && ($mem_info->{'id'} == ($tmp->{"sponsor"} || 0) || $mem_info->{'id'} == ($tmp->{"coach"} || 0)))
		{
			# we are good
		}
		else
		{
			Err($xml->{'notindn'});
		}
	}
}
#
# Get the members info for the top of the page
#

my $qry = "SELECT firstname,
                  lastname,
                  alias,
                  id
           FROM members
           WHERE id = ?";
my $mdata = $db->prepare($qry);
my $rv = $mdata->execute($member_id);
my $tmp2 = $mdata->fetchrow_hashref;

my $lang = $gs->Get_LangPref();

#
## get the number of months of data out there
#

$qry = "SELECT count(*) as num_months
        FROM reward_transactions 
        WHERE id=$member_id and reward_type = 1;";
my $trdata = $db->prepare($qry);
$rv = $trdata->execute;
my $tmp3 = $trdata->fetchrow_hashref;
#
# Add a month incase they started in the middle of the month
$tmp3->{num_months} ++;

#warn "Num Months: $tmp3->{num_months}";
my $dates = ClubCash::GetNeededDates($tmp3->{num_months});

#
# Get the data for the desired time period
#

$qry = "SELECT rt.id, 
                  rt.reward_points, 
                  rt.reward_type,
                  rt.currency,
                  rt.cur_xchg_rate,
                  coalesce(rt.notes,' ') as notes,
                  rt.created::DATE,
                  rt.date_redeemable,
                  rt.date_reported,
                  rt.vendor_id,
                  rt.total_pending_rewards,
                  v.vendor_name,
                  coalesce(t.native_currency_amount,t.amount,0) as amount,
                  coalesce(t.receivable_currency_code,'USD') as ncurrency,
                  coalesce(rttt.description,rtt.description,'Unknown') as description
           FROM reward_transactions rt
           LEFT JOIN vendors v ON
                (v.vendor_id = rt.vendor_id)
           LEFT JOIN transactions t ON
                (t.trans_id = rt.trans_id)
           LEFT JOIN reward_trans_types rtt ON
                (rt.reward_type = rtt.type)
           LEFT JOIN reward_trans_type_translated rttt ON
                (rttt.orig_type = rtt.type and rttt.lang_id = '$lang')
           WHERE rt.id = $tmp2->{id} AND
                 rt.date_reported >= '$dates->{som}[$default]'::DATE AND
                 rt.date_reported <= '$dates->{eom}[$default]'::DATE
           ORDER BY rt.date_reported, rt.pk";
#warn $qry."\n";
my $tdata = $db->prepare($qry);
$rv = $tdata->execute;
my $pending = 0;
my $redeem = 0;
my $tot_pending = 0;
my $tot_redeem = 0;
my $pending_date = ' ';
my $description = ' ';
my $report_date = ' ';
my $purchase_amt = ' ';
my $purchase_cur = '';
#my $current_date = $dates->{TODAY};
my $current_date = $dates->{eom}[$default];
my $redeem_date = ' ';
my $bgcolor = '';

print $q->header();
print $q->start_html(-title=>$xml->{title}, 
                 -style=>{-src=>'/css/reward_details.css'});
print "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
print "   <tr>\n";
print "   <td align='left'> <div align='center'><span class='Blue_Large_Bold'>$xml->{page_header}</span></div></td></tr>\n";
print "   <tr><td><div align='center'><span class='Orange_Large_11'>$xml->{for} $tmp2->{firstname} $tmp2->{lastname}, $tmp2->{alias}</span></div></td></tr>\n";
print "<tr><td><div align='center'><span class='Orange_Large_11'>";
print "<form action='/cgi/clubcash_monthly_detail.cgi' method='POST'>\n";
if ($admin) {
    print "<input type='hidden' id='admin' name='admin' value='1' />\n";
}
if ($report_id) {
    print "<input type='hidden' id='report_id' name='report_id' value='$report_id' />\n";
}
print "<select name='disp_month' onchange='this.form.submit()'>\n";
my $selected = '';
my $ii=0;
for ($ii=0; $ii<$tmp3->{num_months}; $ii++)
{
    if ($ii == $default) {
		$selected = 'selected';
    }
    else {
        $selected = '';
    }
    print "<option $selected value='$ii'>$dates->{som}[$ii] - $dates->{eom}[$ii]</option>\n";
}
print "</select>\n";
print "</form>\n";
print "   </span></div></td></tr>\n";
print "   <tr> <td>";
print "<table border='0' width='100%' cellpadding='0',cellspacing='0' bgcolor='#EAEAEA'>\n";
print "   <tr class='b'>\n";
print "      <th class='Blue_Bold'>$xml->{tdate}</th>\n";
print "      <th class='Blue_Bold'>$xml->{rdate}</th>\n";
print "      <th class='Blue_Bold'>$xml->{ttype}</th>\n";
print "      <th class='Blue_Bold'>$xml->{desc}</th>\n";
print "      <th class='Blue_Bold'>$xml->{pamt}</th>\n";
print "      <th class='Blue_Bold'>$xml->{pend}</th>\n";
print "      <th class='Blue_Bold'>$xml->{reddate}</th>\n";
print "      <th class='Blue_Bold'>$xml->{red}</th>\n";
print "   </tr>\n";

### Loop through reward transactions

my $trclass = '';
my $tdclass = '';

while (my $tmp = $tdata->fetchrow_hashref) {

## Determine row color and background

    if ($tmp->{reward_points} < 0) {
	$tdclass = " class='neg'";
    }
    else {
	$tdclass = '';
    }
    if ($rowcount % 2 == 0) {
       $trclass="class='a'";
    }
    else {
       $trclass = "class='b'";
    }
    $rowcount ++;

    if ($tmp->{reward_type} == 1) {
        if ($trclass eq "class='a'") {
	    $trclass = "class='c'";
        }
	$redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate} - 
                  $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
        $pending = $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
        if ($tmp->{total_pending_rewards} == 0) {
            $pending_date = ' ';
        }
	else {
	    $pending_date = $tmp->{date_redeemable};
	}
	$report_date = ' ';
        $description = $tmp->{notes};
        $redeem_date = $tmp->{date_redeemable};
        $purchase_amt = '';
    }
    elsif ($tmp->{reward_type} == 21) {
        $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate} -
 	          $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
        $pending = $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
        $report_date = ' ';
        $description = $xml->{ptor};
        $purchase_amt = '';
        $redeem_date = $tmp->{date_redeemable};
    }
    elsif ($tmp->{reward_type} == 2 ||
           $tmp->{reward_type} == 8 || $tmp->{reward_type} == 9 ||
           $tmp->{reward_type} == 10 || $tmp->{reward_type} == 13) {
        $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate};
        $pending = 0;
	$report_date = $tmp->{date_reported};
	$purchase_amt = '';
        $redeem_date = $tmp->{date_redeemable};
	$description = $tmp->{notes};
    }
    elsif ($tmp->{reward_type} >= 15 && $tmp->{reward_type} <= 19) {
        if ($tmp->{date_redeemable} gt $current_date) {
	    $pending = $tmp->{reward_points} * $tmp->{cur_xchg_rate};
            $redeem = 0;
	}
	else {
	    $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate};
            $pending = 0;
	}
        $description = $tmp->{vendor_name};
        $redeem_date = $tmp->{date_redeemable};
        $purchase_amt = $tmp->{amount};
    }
    else {
        if ($tmp->{reward_type} == 3 || $tmp->{reward_type} == 4 || $tmp->{reward_type} == 20) {
           $description = $tmp->{notes};
       }
        else {
           $description = $tmp->{vendor_name};
       }
        $purchase_amt = $tmp->{amount};
        $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate} - $tmp->{total_pending_rewards}*$tmp->{cur_xchg_rate};
        $pending = $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
        if ($tmp->{date_redeemable} gt $current_date) {
            $pending = $redeem;
            $redeem = 0;
            $redeem_date = $tmp->{date_redeemable};
	}
        else {
            $redeem_date = ' ';
        }
    }

## Set the currency code for the purchase amount

    if ($tmp->{currency} ne $tmp->{ncurrency}) {
	$purchase_cur = $tmp->{ncurrency};
    }
    else {
        $purchase_cur = ' ';
    }

## Get the total pending and redeemable

    $tot_pending += $pending;
    $tot_redeem += $redeem;
    print "<tr $trclass>\n";
    print "<td $tdclass>$tmp->{created}</td>\n";
    print "<td $tdclass>$tmp->{date_reported}</td>\n";
    print "<td $tdclass>$tmp->{description}</td>\n";
    print "<td $tdclass>$description</td>\n";
    if (!$purchase_amt) {
	print "<td>&nbsp;</td>\n";
    }
    else {
        printf ("<td align='right' $tdclass>%.2f %s</td>\n",$purchase_amt,$purchase_cur);
    }
    printf ("<td align='right' $tdclass>%.2f</td>\n",$pending);
    print "<td align='center' $tdclass>$redeem_date</td>\n";
    printf ("<td align='right' $tdclass>%.2f</td>\n",$redeem);
    print "</tr>\n";
}
my $today = `date +%F`;
my $sum = `date +%B --date=$dates->{eom}[$default]` . `date +%G --date=$dates->{eom}[$default]`;
   $trclass = "class='c'";
$tdclass='';
print "<tr $trclass>\n";
print "<td $tdclass>$today</td>\n";
print "<td $tdclass>&nbsp;</td>\n";
print "<td $tdclass>$xml->{bal}</td>\n";
print "<td $tdclass>$sum Summary</td>\n";
print "<td $tdclass>&nbsp;</td>\n";
printf ("<td align='right' $tdclass>%.2f</td>\n",$tot_pending);
print "<td align='center' $tdclass>&nbsp;</td>\n";
printf ("<td align='right' $tdclass>%.2f</td>\n",$tot_redeem);
print "</tr>\n";
print "</table>\n";
print "</td></tr>\n";
print "<tr><td>$xml->{dis}";
print "</td></tr></table>\n";
print "</body>\n";
print "</html>\n";

### Internal functions

sub Err
{
	print $q->header('-expires'=>'now', '-charset'=>'utf-8'), $q->start_html('-encoding'=>'utf-8', '-bgcolor'=>'#ffffff');
	print "$_[0]<br /><br />", scalar localtime();
	print $q->end_html();
	Exit();
}

sub Exit
{
	$db->disconnect if $db;

	exit;
}

sub Get_Cookie
{
    # If this is an admin user. 
    if ( $q->param('admin') )
    {
		# Get the admin user and try logging into the DB
		my $operator = $q->cookie('operator'); 
		my $pwd = $q->cookie('pwd');
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( $cgiini, $operator, $pwd ))
		{
		    $login_error = 1;
		    die "admin login error $cgiini - $operator - $pwd \n";
		}
		# Mark this as an admin user.
		$login_type = 'admin';
		return 1;
    }
    # If this is a member. 
    else
    {
		exit unless $db = DB_Connect($cgiini);
		$db->{'RaiseError'} = 1;
	
		# Get the member Login ID and info hash from the VIP cookie.
		( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
	
		if ( $member_id && $member_id !~ m/\D/ )
		{
		    $login_type = 'auth';
		}
		else
		{
		    print $q->redirect("$LOGIN_URL?destination=" . $q->escape('/cgi/clubcash_monthly_detail.cgi'));
	        return 0;
		}
		return 1;
    }
}

__END__

11/14/14	Implemented the $LOGIN_URL from G_S as well as some other code cleanup
11/18/15	Added binmode STDOUT, ":encoding(utf8)"; to deal with encoding issues