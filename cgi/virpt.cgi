#!/usr/bin/perl -w
###### virpt.cgi
###### Vested Income Report (graphically report residual income earnings potential)
###### created: 12/08/04	Bill MacArthur
###### last modified: 01/25/06	Karl Kohrt

use strict;
use lib('/home/httpd/cgi-lib');
use G_S qw/$LOGIN_URL $EXEC_BASE/;
use DB_Connect;
use DBI;
use CGI;			
use CGI::Carp('fatalsToBrowser');
use XML::local_utils;
use Digest::MD5 qw( md5_hex );

###### GLOBALS
our $key = 'onykey!23#';
our ($id, $meminfo, $db, $rid, $vested_lines) = ();
our $q = new CGI;
our $gs = new G_S;
our $xmlu = new XML::local_utils;
our %TMPL = (
	xsl		=> 10352,
	xml		=> 10353,
	email		=> 10354,
	mail_sent	=> 10355
);
###### since I fully expect that this report will be used as a marketing tool for free
###### members, I will build to that model even though it is only for VIPs at this point

###### we need some kind of ID to proceed
###### we'll look for an ID parameter, a VIP cookie or a member cookie in that order
our $ADMIN = AdminCK();
$id = $q->param('id');
if ($id)
{
	if (! $ADMIN)
	{
		Err('Your administrative login is invalid');
		exit;
	}
	else
	{
		$meminfo = GetMemInfo($id);
	}
}
elsif (($id, $meminfo) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic')) )
{
###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
	if ( $id && $id !~ /\D/ )
	{
		###### do nothing - they're all set
	}

###### if the cookie test returns anything other than digits, then they are not logged in or its expired
	else
	{
		Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
		exit;
	}

###### if they are not a VIP, they don't have access to the report
###### for now anyway
	if ( $meminfo->{membertype} !~ 'v' )
	{
		Err('Vested Income Reports are for VIP members only'),
		exit;
	}
}
else
{
	Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
	exit;
}
$db ||= DB_Connect('virpt.cgi') || exit;

###### we will branch here for the email sending routine since we are not generating
###### a report and we don't need the stuff below
if ($q->param('email') && $q->param('email') eq 'send')
{
	Do_Mail();
	goto 'END';
}

$rid = $q->param('rid');
###### we'll handle 'errors' in the sub
goto 'END' unless ValidateRid($rid);

$meminfo->{active_lines} = GetVestedLines();

if ($rid)
{
	GetRidDetails() if ($rid);
}
elsif ($meminfo->{active_lines} > 0)
{
	$rid = $meminfo->{vested_lines}->{line}->[0];
}

###### move our hashref'd rid into our main hash
$meminfo->{rid} = $rid;

our $xsl = $gs->Get_Object($db, $TMPL{xsl}) || die "Failed to retrieve XSL\n";
our $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Failed to retrieve XML\n";

###### perform our data insertions
$xsl =~ s/%%script_name%%/$q->script_name/e;
$xsl =~ s/%%admin%%/($ADMIN ? ";id=$id" : '')/e;
$xsl =~ s/%%temperature%%/CalcTemp($rid->{npp})/e;
$xml =~ s/%%user%%/$xmlu->hashref2xml({user=>$meminfo})/e;
#Err($xml);goto 'END';
print $q->header(-charset=>'utf-8', -expires=>'now'), $xmlu->xslt($xsl,$xml);
						
END:
$db->disconnect;
exit;

sub AdminCK
{
	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return unless $operator && $pwd;

	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	return ($db = ADMIN_DB::DB_Connect( 'virpt.cgi', $operator, $pwd ));
}

sub CalcTemp
{
	my $npp = shift;
	return 0 unless $npp;
	return 230 if $npp > 7000;
	my $low = 0;
	my $step = -1;
	my $high = 0;
	foreach(qw/75 150 300 600 1000 1500 2500 3500 4500 6000 7000/)
	{
		$high = $_;
		last if $npp <= $high;
		$low = $_;
		$step++;
	}
	my $range_diff = ($high - $low);
	my $diff = ($npp - $low);
	my $height = 0;
	if ($low == 0)
	{
		$height = int(($diff/$range_diff) * 10);
	}
	else
	{
		$height = int(($diff/$range_diff) * 20) + ($step * 20) + 10;
	}
}

sub Do_Mail
{
	require MailingUtils;
	require ParseMe;
	use Mail::Sendmail;

	###### at this point we have an ID, we can look upline
	###### and select the necessary parties to email
	my $coach = my $upline = '';
	my $sth = $db->prepare("
		SELECT m.id, m.spid, m.firstname, m.lastname, m.emailaddress,
			m.alias, m.membertype,
			COALESCE(nbt.myrole, 0) AS role,
			COALESCE(a.a_level, 0) AS a_level
		FROM 	members m
		LEFT JOIN nbteam_vw nbt
		ON 	m.id=nbt.id
		LEFT JOIN a_level a
		ON 	a.id=m.id
		WHERE 	m.id= ?");
	my $tmp = {spid => $meminfo->{spid}};
	while ($tmp->{spid} > 1)
	{
		$sth->execute($tmp->{spid});
		$tmp = $sth->fetchrow_hashref;

		if ($tmp->{role} >= 100 && $tmp->{membertype} eq 'v')
		{
#			$gs->ConvHashref('utf8', $tmp);
			if (! $coach)
			{
				$coach = $tmp;
			}
#			else
			elsif ($tmp->{role} >= 110)	# Strategist
			{
				$upline .= "\"$tmp->{firstname} $tmp->{lastname}\" <$tmp->{emailaddress}>,";
			}
		}
#		last if $tmp->{a_level} >= $EXEC_BASE;
		last if $tmp->{role} >= 110;	# Strategist
	}

	unless ($coach)
	{
		Err('We couldn`t locate the proper upline to contact.');
		return;
	}
	chop $upline;
	my $tmpl = $gs->Get_Object($db, $TMPL{email}) ||
		die "Failed to retrieve the email template\n";
#	$gs->ConvHashref('utf8', $meminfo);
	ParseMe::ParseAll(\$tmpl,{
		user	=> $meminfo,
		coach	=> $coach,
		upline	=> {list => $upline}
	});
#Err($tmpl);return;
	my $hdr = ParseMe::Grab_Headers(\$tmpl);
	MailingUtils::DrHeaders($hdr);
	my %mail = (
		'Content-Type'=> 'text/plain; charset="utf-8"',
		'Message'	=> $tmpl,
		%{$hdr}
	);

	sendmail(%mail) || die $Mail::Sendmail::error;

	###### prepare our user feedback/confirmation
	$tmpl = $gs->Get_Object($db, $TMPL{mail_sent}) ||
		die "Failed to retrieve mail confirmation template\n";
	ParseMe::ParseAll(\$tmpl,{
		user	=> $meminfo,
		coach	=> $tmp
	});
	print $q->header(-charset=>'utf-8'), $tmpl;
}

sub Err
{
	print $q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		'There has been a problem<br /><br />',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Generate_Rid
{
	my $r = shift;
	return unless $r;
	return md5_hex($r . $key . $id) . "\%7C$r";
}
	
sub GetMemInfo
{
	my $id = shift;
	my $mem = $db->selectrow_hashref("
		SELECT id,spid,alias,firstname,lastname
		FROM members WHERE id=?", undef, $id) ||
	die "Failed to retrieve a member record for $id\n";
	return $mem;
}

sub GetRidDetails
{
	###### lets populate our rid record
	my $r = $db->selectrow_hashref("
		SELECT m.id, m.spid, m.alias,
			m.firstname||' '||m.lastname AS name,
			COALESCE(nc.npp, 0) AS npp
		FROM members m, network_calcs_vw nc
		WHERE m.id=nc.id
		AND 	m.id= ?", undef, $rid);
	$rid = $r;
}

sub GetVestedLines
{
	###### let's see who we for VIPs frontline generating paypoints
	$meminfo->{vested_lines}->{line} = [];
	my $cnt = 0;
	my $sth = $db->prepare("
		SELECT m.id, m.spid, m. alias, m.firstname||' '||m.lastname AS name,
			COALESCE(nc.npp, 0) AS npp
		FROM 	members m, network_calcs_vw nc
		WHERE 	m.id=nc.id
	-- we need more than just VIPs
	--	AND 	membertype='v'
		AND 	nc.npp > 0
		AND 	m.spid=$id");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		###### convert our ID to a hashkey so we don't have to
		###### validate the line of sponsorship each time
		$tmp->{hash} = Generate_Rid($tmp->{id});
		push (@{$meminfo->{vested_lines}->{line}}, $tmp);
		$cnt++;
	}
	$sth->finish;
	return $cnt;
}

sub ValidateRid
{
	my $r = shift;
	return 1 unless $r;
	my @vals = split /\|/, $r, 2;
	if (md5_hex($vals[1] . $key . $id) eq $vals[0])
	{
		$rid = $vals[1];
		return 1;
	}
	Err('Invalid report ID parameter received');
	return;
}

###### 12/10/04 removed the membertype criteria in the GetVestedLines routine
###### since it then ignored VIPs in depth under free members -duh-
###### 01/10/05 changed the SQL to look at the network_calcs view instead of the live
###### 		so as to preserve the reports during the beginning of the month
###### 		adjusted the mail routine to stop at the strategist
###### 01/25/06 Commented out ConvHashref references
