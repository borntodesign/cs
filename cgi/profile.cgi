#!/usr/bin/perl -w
###### Stripe.cgi
######
###### A script that allows that handles Stripe transactions
###### Originally copied from PP.cgi
######
###### created: 05/18	Bill MacArthur
######
###### modified: 
	
use strict;
use Template;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw ($LOGIN_URL);
use CGI;
use DB_Connect;
use GI;
use Clubshop::Session;

my $TT = new Template;
my $q = new CGI;
my ($db, %params, $errors, $lang_blocks) =();
my $gs = G_S->new({'db'=>\$db});
my %TMPL = (
	'XML' => 11141,
	'view'                    => 'projects/profile/view.tt',
	'startStripeCheckout_Funding' 		=> 11140
);

my $app_id = 407;


# my ( $member_id, $mem_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );
# if (! $member_id || $member_id =~ m/\D/)
# {
# 	print $q->redirect("$LOGIN_URL?destination=" . $q->url);
# 	Exit();
# }

LoadParams();

# if we did not fail at loading params, then we can go ahead and make a DB connection
$db = DB_Connect('Stripe.cgi');
$db->{'RaiseError'} = 1;
my $GI = GI->new('cgi'=>$q, 'db'=>$db);
my $session = Clubshop::Session->new('db' => $db, 'app_id' => $app_id, 'expiry' => $expiry);

($mem_info->{'country'}) = $db->selectrow_array('SELECT country FROM members WHERE id= ?', undef, $member_id);

LoadLangBlocks();

my @path_parts = split(/\//, $q->path_info);
shift @path_parts;  # the first element is empty because the path starts with a backslash

# path0=action (view,delete,csv,remove)
# path1=oid

unless ($path_parts[0] and $path_parts[1] and $params{'email'})	# require at least these three bits
{
	SimpleError('We are missing some information to process this request');
}

my $browserLang = $gs->Get_langPref();

if ($path_parts eq 'view')
{
	ShowProfile();
}


die "Unexpected logic branch: x:$params{'x'} sfunc:$params{'sfunc'}";

###### start of subroutines

sub CaFunding
{
        my $process_state = $session->RetrieveState($params{'pk'}, $params{'pkc'}, 1);
        unless ($process_state)
        {
		$errors = $lang_blocks->{'something_went_wrong'};
		startStripeCheckout_Funding();
        }
        
	my $resp = $cspp->API->charges_create(
            'amount'    => $params{'amount'} * 100,
            'card'      => $params{'stripeToken'},
            'description'   => $params{'description'},
            'metadata[pk]'  => $params{'pk'},
            'metadata[pkc]'  => $params{'pkc'},
        );

	if (! $resp)
	{
		$errors = $lang_blocks->{'paypal_error_1' } . ' ' . $cspp->API->error->{'message'};
		startStripeCheckout_Funding();
	} else {
                $resp = $cspp->API->success;
        }

	if ($resp->{'status'} eq 'succeeded')	# could be "Completed" -or- "Completed-Funds-Held"
	{
            # at this point our pk & pkc params should match the metadata values we passed to Stripe
            # so also should the amount in the Stripe response match the value we passed to it
            # If these do not match, then that means that some tampering have taken place with params.
            # So abort.
                unless ($params{'pk'} == $resp->{'metadata'}{'pk'} && $params{'pkc'} eq $resp->{'metadata'}{'pkc'} && ($params{'amount'} * 100) == $resp->{'amount'})
                {
                        $errors = $lang_blocks->{'something_went_wrong'};
                        startStripeCheckout_Funding();
                }
                
		my ($trans_id) = $db->selectrow_array(q#SELECT nextval(('"soa_trans_id_seq"'::text)::regclass)#);
		unless ($trans_id)
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		my $rv = $db->do("
			INSERT INTO soa (trans_id, trans_type, id, amount, description, reconciled, memo)
			VALUES (
				$trans_id,
				$txnType,
				$member_id,
				?,
				?,
				TRUE,
				?
			)
			", undef, $params{'amount'}, "Stripe Trans ID: $resp->{'id'}", "Gross: $params{'amount'} - Payer: $resp->{'source'}{'name'} - Fee: " . $resp->{'application_fee'} || 0);
		
		unless ($rv && $rv eq '1')
		{
			$errors = "$lang_blocks->{'db_error_recording'} $lang_blocks->{'noreload'}";
			$errors .= $q->p($db->errstr) if $db->errstr;
			SimpleError();
		}
		
		print $q->redirect('https://www.clubshop.com/cgi/funding.cgi');
		Exit();
	}

=comment
	elsif ($resp{'PaymentStatus'} eq 'In-Progress')
	{
		$errors = "$lang_blocks->{'payment_in_progress'} $lang_blocks->{'try_reload'}";
		SimpleError();
	}
	elsif ($resp{'PaymentStatus'} eq 'Failed')
	{
		$errors = $lang_blocks->{'payment_failed'};
		startExpressCheckout_Funding();
	}
	elsif ($resp{'PaymentStatus'} eq 'Pending')
	{
		$errors = $lang_blocks->{'payment_pending'};
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Pending PayPal payment',
			'From' => 'PP.cgi@www.clubshop.com',
			'Message' => 'A PayPal funding request has come through as "Pending". Here are some details on the Checkout:' . "\n" . Dumper(\%CoDetails)
		);
		startExpressCheckout_Funding();
	}
=cut
	else
	{
		$errors = "Unexpected PaymentStatus: " . $cspp->API->success->{'status'};
		
		# let somebody know about this
		sendmail(
			'To' => 'webmaster@clubshop.com',
			'Subject' => 'Unexpected PaymentStatus Stripe payment',
			'From' => 'Stripe.cgi@www.clubshop.com',
			'Message' => "A Stripe funding request has come through as ${$cspp->API->success->{'status'}}. Here are some details on the Checkout:\n" . Dumper($cspp->API->success)
		);
		startExpressCheckout_Funding();
	}
	# for PaymentStatus return values, see: https://stripe.com/docs/api
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

=head 3 LoadParams

	We are accepting certain parameters only and then usually for certain specific values. We will enforce this in here.
	
=cut

sub LoadParams
{
	# if the value is -0-, then we expect that named parameter but the value is freestyle
	my %Allowed = (
		'id'		=> 0,
		'token'		=> 0,
		'pk'		=> 0,
		'pkc'	        => 0
	);
	
	foreach my $pn ($q->param)
	{
		if (exists $Allowed{$pn})
		{
			if ($Allowed{$pn})	# if there are expected values in the map, make sure the value passed is in the list
			{
				$params{$pn} = $q->param($pn);
				die "Unrecognized parameter value: $pn: $params{$pn}" unless grep $_ eq $params{$pn}, @{$Allowed{$pn}};
			}
			else	# otherwise we can accept whatever value
			{
				$params{$pn} = $q->param($pn);
			}
		}
		else
		{
			# if a parameter comes in that we do not recognize, we will just skip it
		}
	}
}

sub SimpleError
{
	print $q->header('-charset'=>'utf-8'), $q->start_html, $q->div($errors), $q->end_html;
	Exit();
}

sub ShowProfile
{
	# we should have an oid in our path and an email as a param
	my $meminfo = $db->selectrow_hashref(q#
		SELECT alias, membertype, firstname, lastname, address1, address2, city, state, postal_code, country, phone, language_pref
		FROM members
		WHERE membertype IN ('m','s','v','tp','ag')
		AND members.oid= ?
		AND members.emailaddress= ?
		#, undef, $path_info[1], $params{'email'});

	my $tmpl = $gs->GetTemplate($TMPL{'view'}, $browserLang) || SimpleError("Failed to obtain content for display");
	my $tmp;

	$TT->process(\$tmpl, {'meminfo'=>$meminfo, 'lang_pref'=>$browserLang}, \$tmp);
}

__END__

