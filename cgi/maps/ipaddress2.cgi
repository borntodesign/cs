#!/usr/local/bin/perl
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use CGI;
use CGI::Cookie;

my $PI = 4*atan2(1,1);    # pi to max digits of the system
my $earth_radius_mi = 3960.0;
my $deg2rad = $PI/180.0;
my $rad2deg = 180/$PI;
my @box_size_mi = (5.0, 10.0, 25.0, 50.0, 100.0, 20000.0);
my $cgi = new CGI;


my $ip = $ENV{'REMOTE_ADDR'};
my $ip = "208.78.129.189";


my @tokes = split('\.',, $ip);

my $nip = @tokes[3] + @tokes[2] * 256 + @tokes[1] * 256 * 256 + @tokes[0] * 256 * 256 * 256;

#print "Tokens: @tokes[3] . @tokes[2] . @tokes[1]. @tokes[0] ---- $nip<br>";

my $cgiini = "clubcash_monthly_detail.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
my $qry = "SELECT *
        FROM ip_latlong_lookup
        WHERE $nip <= ipTO and $nip >= ipFROM
        LIMIT 1;";
#warn $qry."\n";
#print "$qry<br/>\n";
my $tdata = $db->prepare($qry);
my $rv = $tdata->execute;
my @box = ();
my $cookiedata = "";
while (my $tmp = $tdata->fetchrow_hashref) {

    $cookiedata = "\"You Are Here, $tmp->{iplongitude}, $tmp->{iplatitude} |";
    my $box_lat_chg = ((@box_size_mi[5]/2.0)/$earth_radius_mi) * $rad2deg;
    my $box_lon_chg = ((@box_size_mi[5]/2.0)/($earth_radius_mi * cos($tmp->{iplatitude}*$deg2rad)))*$rad2deg;
    @box[0] = $tmp->{iplongitude}+$box_lon_chg;
    @box[1] = $tmp->{iplatitude}+$box_lat_chg;
    @box[2] = $tmp->{iplongitude}-$box_lon_chg;
    @box[3] = $tmp->{iplatitude}-$box_lat_chg;
#    $cookiedata .= "Box top, @box[0], @box[1] |";
#    $cookiedata .= "Box Bottom, @box[2], @box[3] |";
    $qry = "Select mal.*, mam.business_type, yp.yp_heading from merchant_affiliate_locations mal
            left join merchant_affiliates_master mam
                 on (mam.id = mal.merchant_id)
            left join ypcats2naics yp
                 on (yp.pk = mam.business_type)
            where mal.longitude <= @box[0] and mal.longitude >= @box[2] and
                  mal.latitude <= @box[1] and mal.latitude >= @box[3];";
    warn $qry . "\n";
    my $tdata2 = $db->prepare($qry);
    my $rv = $tdata2->execute;
    while (my $tmp2 = $tdata2->fetchrow_hashref) {
        $tmp2->{location_name} =~ s/\"/\'/g;
        $cookiedata .= "$tmp2->{location_name}, $tmp2->{longitude}, $tmp2->{latitude}, $tmp2->{business_type}, $tmp2->{yp_heading} |";
    }
    $cookiedata .= "\";";
    print $cgi->header();
    print "<html>\n";
    print "<head>\n";
    print "<title>IP Address Display2</title>\n";
    print <<"EOT";
<style type="text/css">
    html { height: 100% }
body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=true">
</script>
<script type="text/javascript" src="http://www.clubshop.com/mall/_includes/js/javacookie.js"></script>
<script type="text/javascript">
    function initialize() {
        var cook = $cookiedata;
        var cookies = cook.split("|");
        var location = cookies[0].split(",");
        var latlng = new google.maps.LatLng(location[2],location[1]);
	var myOptions = {
			      zoom: 13,
			      center: latlng,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
	                 };
        var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        var marker = new google.maps.Marker({position:latlng, title:location[0], icon:"/maps/markers/blue_MarkerA.png"});
        marker.setMap(map);
        for (ii=1; ii<cookies.length; ii++) {
	    location = cookies[ii].split(",");
            latlng = new google.maps.LatLng(location[2],location[1]);
            if (location[3] == 2795) {
               var mcolor = "/maps/markers/green_MarkerR.png";
               marker = new google.maps.Marker({position:latlng, title:location[0], icon:mcolor});
            }
	    else if (location[3] == 2218 || location[3] == 335 || location[3] == 2031) {
               var mcolor = "/maps/markers/purple_MarkerS.png";
               marker = new google.maps.Marker({position:latlng, title:location[0], icon:mcolor});
            }
	    else if (location[3] == 2581 || location[3] == 1102 || location[3] == 2372 || location[3] == 1426) {
               var mcolor = "/maps/markers/brown_MarkerC.png";
               marker = new google.maps.Marker({position:latlng, title:location[0], icon:mcolor});
            }
	    else if (location[3] == 750) {
               var mcolor = "/maps/markers/yellow_MarkerB .png";
               marker = new google.maps.Marker({position:latlng, title:location[0], icon:mcolor});
            }
	    else {
               marker = new google.maps.Marker({position:latlng, title:location[0]});
	    }

	    marker.setMap(map);
       	}
    }

</script>
EOT
    print "</head>\n";
    print "<body onload=\"initialize()\">\n";
    print "<h1>IP Address Display</h1>\n";
    print "You are using ip address $ip<br/>\n";
    print "Your Longitude == Latitude is $tmp->{iplongitude} == $tmp->{iplatitude} <br/>\n";
    print "your city-region-country is : $tmp->{ipcity} - $tmp->{ipregion} - $tmp->{countrylong}<br/>\n";
    print "your 5 mile box size is @box[0] , @box[1] --- @box[2] . @box[3] \n";
    print "<div id=\"map_canvas\" style=\"width:100%; height:100%;\"></div>";
print "<p>\n";
print "</body></html>\n";
}
