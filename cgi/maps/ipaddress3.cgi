#!/usr/local/bin/perl
use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use DB_Connect;
use CGI::Cookie;

my $PI = 4*atan2(1,1);    # pi to max digits of the system
my $earth_radius_mi = 3960.0;
my $deg2rad = $PI/180.0;
my $rad2deg = 180/$PI;
my @box_size_mi = (5.0, 10.0, 25.0, 50.0, 100.0);
my $cgi = new CGI;


my $ip = $ENV{'REMOTE_ADDR'};
my $ip = "208.78.129.189";

my @tokes = split('\.',, $ip);

my $nip = @tokes[3] + @tokes[2] * 256 + @tokes[1] * 256 * 256 + @tokes[0] * 256 * 256 * 256;

#print "Tokens: @tokes[3] . @tokes[2] . @tokes[1]. @tokes[0] ---- $nip<br>";

my $cgiini = "clubcash_monthly_detail.cgi";
my $db = '';
unless ($db = DB_Connect($cgiini)) {print"DB did not connect";exit}
my $qry = "SELECT *
        FROM ip_latlong_lookup
        WHERE $nip <= ipTO and $nip >= ipFROM
        LIMIT 1;";
#warn $qry."\n";
#print "$qry<br/>\n";
my $tdata = $db->prepare($qry);
my $rv = $tdata->execute;
my @box = ();
my $cookiedata = "";
while (my $tmp = $tdata->fetchrow_hashref) {

    $cookiedata = "You Are Here, $tmp->{iplongitude}, $tmp->{iplatitude} |";
    my $box_lat_chg = ((@box_size_mi[0]/2.0)/$earth_radius_mi) * $rad2deg;
    my $box_lon_chg = ((@box_size_mi[0]/2.0)/($earth_radius_mi * cos($tmp->{iplatitude}*$deg2rad)))*$rad2deg;
    @box[0] = $tmp->{iplongitude}+$box_lon_chg;
    @box[1] = $tmp->{iplatitude}+$box_lat_chg;
    @box[2] = $tmp->{iplongitude}-$box_lon_chg;
    @box[3] = $tmp->{iplatitude}-$box_lat_chg;
    $cookiedata .= "Box top, @box[0], @box[1] |";
    $cookiedata .= "Box Bottom, @box[2], @box[3] |";
    $qry = "Select * from merchant_affiliate_locations 
            where longitude <= @box[0] and longitude >= @box[2] and
                  latitude <= @box[1] and latitude >= @box[3];";
    my $tdata2 = $db->prepare($qry);
    my $rv = $tdata2->execute;
    while (my $tmp2 = $tdata2->fetchrow_hashref) {
        $cookiedata .= "$tmp2->{location_name}, $tmp2->{longitude}, $tmp2->{latitude} |";
    }
    my $cooklen = length($cookiedata);
    my $cookie = CGI::Cookie->new(-name=>"loc", -value=>$cookiedata);
    print $cgi->header(-cookie=>$cookie);
    print "<html>\n";
    print "<head>\n";
    print "<title>IP Address Display3</title>\n";
    print "</head>\n";
    print "<body>\n";
    print "<h1>IP Address Display</h1>\n";
    print "You are using ip address $ip<br/>\n";
    print "Your Longitude == Latitude is $tmp->{iplongitude} == $tmp->{iplatitude} <br/>\n";
    print "your city-region-country is : $tmp->{ipcity} - $tmp->{ipregion} - $tmp->{countrylong}<br/>\n";
    print "your 5 mile box size is @box[0] , @box[1] --- @box[2] . @box[3] \n";
    print "your cookie length: $cooklen\n";
    print "<div id=\"map_canvas\" style=\"width:100%; height:100%;\"></div>";
print "<p>\n";
print "</body>\n</html>\n";
}
