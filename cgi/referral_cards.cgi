#!/usr/bin/perl -w
###### referral_cards.cgi
###### Written: 10/03/2003	Karl Kohrt
######
###### This script will display the member's current "referred to" ClubBucks cards.
######
###### Last modified: 02/23/05 Karl Kohrt
######

use strict;
use lib ('/home/httpd/cgi-lib');
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S;
use DB_Connect;

###### CONSTANTS

###### GLOBALS
our $LOGIN_URL = "http://www.clubshop.com/login.html";
our %TMPL = (	Report	=> 10225,
		Row	=> 10226);
our $q = new CGI;
our $db = '';
our $member_id = '';
our $meminfo = '';
our $how_many = 0;
our $how_many_activated = 0;
our $cardrows = '';

##########################################
###### MAIN program begins here.
##########################################

# Get the member info from the VIP cookie.
( $member_id, $meminfo ) = &G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

# If we didn't get a member ID, we'll check for a ppreport cookie.
unless ( $member_id )
{
	# Get the alias, then remove all letters to get the member ID.
	$member_id = G_S::PreProcess_Input($q->cookie('id')) || '';
}	

# if the member id has any non-digits it means that its bad/expired whatever
if (!$member_id || $member_id =~ /\D/){ &RedirectLogin(); exit }

# DB_Connect does its own error handling, so we only need exit if it doesn't work
unless ($db = DB_Connect('referral_cards.cgi')){exit}

Build_Rows();		# Build out the rows of cards found.
Display_List();	# Display the page.

exit;

##########################################
###### Subroutines start here.
##########################################
			
###### Build out the display rows from the cards found in the database.
sub Build_Rows
{
	my $master_row = &G_S::Get_Object($db, $TMPL{'Row'}) || die "Failed to load template: $TMPL{'Row'}";
	my $this_row = '';
	my $row_color = '#ffffff';
	my $class_description = '';
	my ($sth, $qry, $rv, $data) = ();

	# Get the CBID's from the database.
	$qry = "	SELECT id,
				class,
				current_member_id,
				label
			FROM clubbucks_master
			WHERE	referral_id = ?
			ORDER BY current_member_id,
					id;";
	$sth = $db->prepare($qry);
	$rv = $sth->execute($member_id);
	defined $rv or die $sth->errstr;

	while ( $data = $sth->fetchrow_hashref )
	{
		++$how_many;
		$this_row = $master_row;

		$data->{current_member_id} ||= "&nbsp;";
		$data->{referral_clubbucks_id} ||= "&nbsp;";
		$data->{label} ||= "&nbsp;";
		if ( $data->{class} == 4 || $data->{class} == 5 )
		{
			$class_description = "YES";
			++$how_many_activated;
		}
		else
		{
			$class_description = "No";
		}
						

		# Fill out the current row info.
		$this_row =~ s/%%cbid%%/$data->{id}/;
		$this_row =~ s/%%class%%/$data->{class}/;
		$this_row =~ s/%%class_description%%/$class_description/;
		$this_row =~ s/%%member_id%%/$data->{current_member_id}/;
		$this_row =~ s/%%referral_cbid%%/$data->{referral_clubbucks_id}/;
		$this_row =~ s/%%label%%/$data->{label}/;

		# Alternate row color.
		($row_color eq '#ffffff') ? ($row_color = '#eeeeee') : ($row_color = '#ffffff');
		$this_row =~ s/%%row_color%%/$row_color/;

		# Add a row to the list of cards.
		$cardrows .= $this_row;
	}
	$sth->finish();
}

###### Display the page showing ClubBucks cards referred to by this member.
sub Display_List
{
	my $tmpl = &G_S::Get_Object($db, $TMPL{'Report'}) || die "Failed to load template: $TMPL{'Report'}";

	$tmpl =~ s/%%how_many%%/$how_many/g;
	$tmpl =~ s/%%how_many_activated%%/$how_many_activated/g;
	$tmpl =~ s/%%member_id%%/$member_id/g;
	$tmpl =~ s/%%cardrows%%/$cardrows/;

	# Print out the page to the browser.
	print $q->header(-expires=>'now'), $tmpl;
}

##########################################
sub Err{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print "*<BR><B>", $_[0], "</B><BR>*";
	print '<p>Current server time: <tt>' . scalar localtime() . ' PST</tt>';
	print $q->end_html();
}

##########################################
sub RedirectLogin{
	my $me = $q->script_name;
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print "Please login <a href=\"$LOGIN_URL?destination=$me\">here</a><br /><br />.";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

###### 10/13/03 Removed two un-used, global variables ($ME, $tmpl).
###### 02/23/05 Switched from the 'pa' cookie to the 'id' cookie in the case
###### 	of an associate member login.
