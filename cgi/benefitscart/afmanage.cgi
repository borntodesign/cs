#!/usr/bin/perl

use CGI::Carp qw(fatalsToBrowser);

&GetFormInput;
print "Content-type: text/html \n\n";

$Search    = "$field{'Search'}";
$Manager1  = $field{'Manager1'};
$Manager2  = $field{'Manager2'};
$Manager3  = $field{'Manager3'};
$AffInfo   = $field{'AffInfo'};
$MonthYear = $field{'MonthYear'};
$Month     = "$field{'Month'}";
$Year      = "$field{'Year'}";

if($MonthYear eq "1") { &ListComms; }
if($Manager1 ne "")   { &ListMembers; }
if($Manager2 ne "")   { &ShowSales; }
if($Manager3 ne "")   { &Commissions; }
if($Search ne "")     { &DoSearch; }
if($AffInfo ne "")    { &GetAffInfo; }

&ShowMain;

sub ListComms
 {
 $results = "";
 $results .= "<HTML><HEAD><TITLE>StoreClerk Affiliate Sales For $Month $Year</TITLE></HEAD>\n";
 $results .= "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\">\n";
 $results .= "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"StoreClerk Affiliate Manager\" border=\"0\"></a></div>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>Affiliate Sales For $Month $Year</strong></font></p>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<div align=\"center\"><table border=\"0\" cellpadding=\"6\" cellspacing=\"6\" width=\"600\">\n";
 $results .= "<tr><td colspan=\"4\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 $results .= "<tr><td colspan=\"4\" align=\"center\"><hr></td></tr>\n";
 $results .= "<tr><td>Affiliate ID</td><td>Name - Address</td><td align=\"right\">Sales</td><td align=\"right\">Commission</td></tr>\n";
 open(AFFS,"affsales.dat");
 @SALES = <AFFS>;
 close(AFFS);
 open(AFFL,"aff.dat");
 @LINES = <AFFL>;
 close(AFFL);
 foreach $line (@LINES)
  {
  $Match = 0;
  $Gross = 0;
  $Comms = 0;
  ($AF,$PS,$EM,$NM,$CM,$AD,$CY,$ST,$ZP,$CT,$WS,$URL) = split(/\|/,$line);
  foreach $row (@SALES)
   {
   ($AFN,$GRO,$CMS,$TRN,$DTM) = split(/\|/,$row);
   ($MTH,$DY,$YR) = split("/",$DTM);
   ($ZYR,$HMS) = split(/ /,$YR);
   if($MTH eq "$Month") { $Match++; }
   if($ZYR eq "$Year")  { $Match++; }
   if($AF eq "$AFN")    { $Match++; }
   if($Match eq "3")
    {
    $Gross += $GRO;
    $Comms += $CMS;
    }
    $Match = 0;
   }
   if($Gross ne "0")
    {
    $GrossSales = sprintf "%.2f",$Gross;
    $CommSales  = sprintf "%.2f",$Comms;
    $results .= "<tr><td valign=\"top\"><a href=\"/cgi/benefitscart/afmanage.cgi\?AffInfo=$AF\"><font face=\"arial\" size=\"2\">$AF</font></a></td>";
    $results .= "<td valign=\"top\"><p><a href=\"mailto:$EM\"><font face=\"arial\" size=\"2\">$NM</font></a><br><a href=\"$URL\"><font face=\"arial\" size=\"2\">$CM</font></a><br><font face=\"arial\" size=\"2\">$AD<br>$CY\, $ST $ZP</font></td><td align=\"right\" valign=\"top\"><font face=\"arial\" size=\"2\">$GrossSales</font></td><td align=\"right\" valign=\"top\"><font face=\"arial\" size=\"2\">$CommSales</font></td></tr>\n";
    }
  }
 $results .= "<tr><td colspan=\"4\" align=\"center\"><hr><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></form></td></tr>\n";
 $results .= "</table></div><br>";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 print "$results\n";
 exit;
 }

sub ShowSales
 {
 $results = "";
 $results .= "<HTML><HEAD><TITLE>StoreClerk Affiliate Sales Log</TITLE></HEAD>\n";
 $results .= "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\">\n";
 $results .= "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"StoreClerk Affiliate Manager\" border=\"0\"></a></div>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>AFFILIATE SALES LOG</strong></font></p>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<div align=\"center\"><table border=\"0\" cellpadding=\"6\" cellspacing=\"0\" width=\"650\">\n";
 $results .= "<tr><td colspan=\"5\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 $results .= "<tr><td colspan=\"5\" align=\"center\"><hr></td></tr>\n";
 $results .= "<tr><td>Affiliate</td><td align=\"right\">Sale Amount</td><td align=\"right\">Commission</td><td align=\"right\">Tracking ID</td><td align=\"center\">Month/Date/Year Time</td></tr>\n";
 open(AFFS,"affsales.dat");
 @LINES = <AFFS>;
 close(AFFS);
 foreach $line (@LINES)
  {
  ($AF,$SA,$CS,$TN,$DT) = split(/\|/,$line);
  $results .= "<tr><td><a href=\"/cgi/benefitscart/afmanage.cgi\?AffInfo=$AF\"><font face=\"arial\" size=\"2\">$AF</font></a></td><td align=\"right\">$SA</td><td align=\"right\">$CS</td><td align=\"right\">$TN</td><td align=\"center\">$DT</td></tr>\n";
  }
 $results .= "<tr><td colspan=\"5\" align=\"center\"><hr><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 $results .= "</table></div><br>";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 print "$results\n";
 exit;
 }

sub Commissions
 {
 $results = "";
 $results .= "<HTML><HEAD><TITLE>StoreClerk Monthly Affiliate Commissions</TITLE></HEAD>\n";
 $results .= "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\"><br><br>\n";
 $results .= "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"StoreClerk Affiliate Manager\" border=\"0\"></a></div>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<div align=\"center\"><table border=\"0\" cellpadding=\"6\" cellspacing=\"0\" width=\"350\">\n";
 $results .= "<tr><td colspan=\"2\" align=\"center\"><p align=\"center\"><font face=\"arial\" size=\"2\"><strong>Select Month and Year:</strong></font></p><form action=\"/cgi/benefitscart/afmanage.cgi\" method=\"post\"><input type=\"hidden\" name=\"MonthYear\" value=\"1\"></td></tr>\n";
 $results .= "<tr><td align=\"right\"><select name=\"Month\"><option>January</option><option>February</option><option>March</option><option>April</option><option>May</option>\n";
 $results .= "<option>June</option><option>July</option><option>August</option><option>September</option><option>October</option><option>November</option><option>December</option></select></td>\n";
 $results .= "<td align=\"left\"><div align=\"left\"><select name=\"Year\"><option>2001</option><option>2002</option><option>2003</option><option>2004</option><option>2005</option><option>2006</option></select> <input type=\"submit\" value=\"GO\"></div></td></tr>\n";
 $results .= "<tr><td colspan=\"2\" align=\"center\"><hr><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></form></td></tr>\n";
 $results .= "</table></div><br>";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 print "$results\n";
 exit;
 }

sub DoSearch
 {
 $results = "";
 $results .= "<HTML><HEAD><TITLE>Affiliate Search</TITLE></HEAD>\n";
 $results .= "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\">\n";
 $results .= "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"StoreClerk Affiliate Manager\" border=\"0\"></a></div>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>SEARCH RESULTS</strong></font></p>\n";
 $results .= "<hr width=\"350\" align=\"center\">\n";
 $results .= "<div align=\"center\"><table border=\"0\" cellpadding=\"6\" cellspacing=\"0\" width=\"650\">\n";
 $results .= "<tr><td colspan=\"3\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 $results .= "<tr><td colspan=\"3\" align=\"center\"><hr></td></tr>\n";
 $Count1 = 0;
 $Count2 = 0;
 $IItems = "0";
 open(AFFL,"aff.dat");
 @LINES = <AFFL>;
 close(AFFL);
 foreach $line (@LINES)
  {
  ($AF,$PS,$EM,$NM,$CM,$AD,$CY,$ST,$ZP,$CT,$WS,$URL) = split(/\|/,$line);
  $SLine   = $line;
  $SLine   =~ tr/a-z/A-Z/;
  $InKey   = "$Search";
  $InKey   =~ tr/a-z/A-Z/;
  $Count2++;
  if ($SLine =~ /$InKey/)
   {
   $IItems++;
   $results .= "<tr><td><a href=\"/cgi/benefitscart/afmanage.cgi\?AffInfo=$AF\" target=\"\_blank\"><font face=\"arial\" size=\"2\">$AF</font></a></td><td><font face=\"arial\" size=\"2\">$NM</font> <a href=\"mailto:$EM\"><font face=\"arial\" size=\"2\">$EM</font></a></td><td><a href=\"$URL\" target=\"\_blank\"><font face=\"arial\" size=\"2\">$WS</font></a></td></tr>\n";
   $Count1++;
   }
  }
 $results .= "<tr><td colspan=\"3\" align=\"center\"><font size=\"2\" face=\"arial\">$Count1 records located from a total of $Count2</font><hr></td></tr>\n";
 $results .= "<tr><td colspan=\"3\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 $results .= "</table></div><br>";
 $results .= "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 if($IItems eq "0") { &NoMatch; } else { print "$results"; }
 exit;
 }

sub ListMembers
 {
 print "<html><head><title>Affiliate Members</title></head>\n";
 print "<body>\n";
 print "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"StoreClerk Affiliate Manager\" border=\"0\"></a></div>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>AFFILIATE MEMBERS</strong></font></p>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<div align=\"center\"><table border=\"0\" cellpadding=\"6\" cellspacing=\"0\" width=\"650\">\n";
 print "<tr><td colspan=\"3\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
 print "<tr><td><font face=\"arial\" size=\"2\"><strong>Affiliate ID</strong></font></td><td><font face=\"arial\" size=\"2\"><strong>Contact Name</strong></font></td><td><font face=\"arial\" size=\"2\"><strong>Website Name</strong></font></td></tr>\n";
 print "<tr><td colspan=\"3\" align=\"center\"><hr></td></tr>\n";
 open(AFFL,"aff.dat");
 @LINES = <AFFL>;
 close(AFFL);
 @ROWS = sort(@LINES);
 foreach $line (@ROWS)
  {
  ($AF,$PS,$EM,$NM,$CM,$AD,$CY,$ST,$ZP,$CT,$WS,$URL) = split(/\|/,$line);
  if ($AF ne "")
   {
   print "<tr><td><a href=\"/cgi/benefitscart/afmanage.cgi\?AffInfo=$AF\" target=\"\_blank\"><font face=\"arial\" size=\"2\">$AF</font></a></td><td><font face=\"arial\" size=\"2\">$NM</font> <a href=\"mailto:$EM\"><font face=\"arial\" size=\"2\">$EM</font></a></td><td><a href=\"$URL\" target=\"\_blank\"><font face=\"arial\" size=\"2\">$WS</font></a></td></tr>\n";
   }
  }
  print "<tr><td colspan=\"3\" align=\"center\"><hr></td></tr>\n";
  print "<tr><td colspan=\"3\" align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
  print "</table></div><br>";
  print "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 exit;
 }

sub GetAffInfo
 {
 $TotSales = 0;
 print "<html><head><title>Affiliate Information</title></head>\n";
 print "<body>\n";
 print "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"Affiliate Manager\" border=\"0\"></a></div>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>AFFILIATE EARNINGS</strong></font></p>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<br><div align=\"center\"><table border=\"1\" cellpadding=\"6\" cellspacing=\"0\" width=\"650\">\n";
 print "<tr bgcolor=\"FEB767\">";
 print "<td align=\"left\" valign=\"top\"><font face=\"arial\"><strong>Affiliate</strong></font></td>";
 print "<td align=\"left\" valign=\"top\"><font face=\"arial\"><strong>Address</strong></font></td>";
 print "<td align=\"left\" valign=\"top\"><font face=\"arial\"><strong>Web Site</strong></font></a></td></tr>";
 open (AFFS,"aff.dat");
 while (<AFFS>)
  {
  $Signin = 0;
  $Line = $_;
  chop($Line);
  @Fields = split(/\|/,$Line);
  $Col1   = $Fields[0];
  $Col2   = $Fields[1];
  if ($AffInfo eq "$Col1")
   {
   $Email   = $Fields[2];
   $Name    = $Fields[3];
   $Company = $Fields[4];
   $Address = $Fields[5];
   $City    = $Fields[6];
   $State   = $Fields[7];
   $Zip     = $Fields[8];
   $WebName = $Fields[10];
   $WebURL  = $Fields[11];
   $Good = 1;
   print "<tr bgcolor=\"D2DDFD\">";
   print "<td align=\"left\" valign=\"top\"><font face=\"arial\">$Name <br>$Email <br>$Col1</font></td>";
   print "<td align=\"left\" valign=\"top\"><font face=\"arial\">$Company <br>$Address <br>$City $State $Zip</font></td>";
   print "<td align=\"left\" valign=\"top\"><a href=\"$WebURL\"><font face=\"arial\">$WebName</font></a></td></tr>";
   print "<tr bgcolor=\"FEB767\">";
   print "<td align=\"center\" colspan=\"3\"><font face=\"arial\" size=\"2\"><strong>Affiliate Commission Log</strong></font></td></tr>";
   print "<tr bgcolor=\"D2DDFD\">";
   print "<td align=\"center\" valign=\"top\"><font face=\"arial\" size=\"1\"><strong>Order \| Sale</strong></font></td>";
   print "<td align=\"center\" valign=\"top\"><font face=\"arial\" size=\"1\"><strong>Commission</strong></font></td>";
   print "<td align=\"center\" valign=\"top\"><font face=\"arial\" size=\"1\"><strong>Date</strong></font></td></tr>";
   open (SALES,"affsales.dat");
   while (<SALES>)
    {
    $SLine = $_;
    chop($SLine);
    @Fields = split(/\|/,$SLine);
    $AffID  = $Fields[0];
    if ($AffInfo eq "$AffID")
     {
     $SubTot = $Fields[1];
     $Sub   += $SubTot;
     $ComIsh = $Fields[2];
     $Com   += $ComIsh;
     $OrdNum = $Fields[3];
     $OrdDate = $Fields[4];
     $TotSales++;
     print "<tr bgcolor=\"FFFFFF\">";
     print "<td align=\"right\" valign=\"top\"><font face=\"arial\">$OrdNum \$";
     printf "%.2f",$SubTot;
     print "</font></td><td align=\"right\" valign=\"top\"><font face=\"arial\">\$";
     printf "%.2f",$ComIsh;
     print "</font></td><td align=\"center\" valign=\"top\"><font face=\"arial\">$OrdDate</font></td></tr>";
     }
    }
    if ($Sub eq "") { $Sub = "0.00"; }
    if ($Com eq "") { $Com = "0.00"; }
    print "<tr bgcolor=\"D2DDFD\">";
    print "<td align=\"right\" valign=\"top\"><font face=\"arial\">Gross Sales: \$";
    printf "%.2f",$Sub;
    print "</font></td><td align=\"right\" valign=\"top\"><font face=\"arial\">Commissions: \$";
    printf "%.2f",$Com;
    print "</font></td><td align=\"center\" valign=\"top\"><font face=\"arial\">Total Sales: $TotSales</font></td></tr>";
    print "<tr><td colspan=\"3\" align=\"center\"><br><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></td></tr>\n";
    print "</table></div><br>";
    print "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
    print "</body></html>\n";
   }
  }
 close(AFFS);
 close(SALES);
 exit;
 }

sub ShowMain
 {
 print "<HTML><HEAD><TITLE>StoreClerk Affiliate Manager</TITLE></HEAD>\n";
 print "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\">\n";
 print "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"Affiliate Manager\" border=\"0\"></a></div>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>AFFILIATE MANAGER</strong></font></p>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<div align=\"center\">\n";
 print "<TABLE BORDER=\"0\" WIDTH=\"500\" CELLPADDING=\"10\">\n";
 print "<TR><TD ALIGN=\"CENTER\"><div align=\"center\"><font face=\"arial\" size=\"2\">SEARCH:</font> <INPUT NAME=\"Search\" SIZE=\"12\"><INPUT TYPE=\"SUBMIT\" NAME=\"Find\" VALUE=\"GO\"></div></TD></TR>\n";
 print "<TR><TD ALIGN=\"CENTER\"><INPUT TYPE=\"SUBMIT\" NAME=\"Manager1\" VALUE=\"AFFILIATE MEMBERS\"></TD></TR>\n";
 print "<TR><TD ALIGN=\"CENTER\"><INPUT TYPE=\"SUBMIT\" NAME=\"Manager2\" VALUE=\" TRANSACTION LOG  \"></TD></TR>\n";
 print "<TR><TD ALIGN=\"CENTER\"><INPUT TYPE=\"SUBMIT\" NAME=\"Manager3\" VALUE=\" COMMISSIONS DUE  \"></TD></TR>\n";
 print "</TABLE></div>\n";
 print "<br>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 print "</FORM></BODY></HTML>\n";
 exit;
 }

sub NoMatch
 {
 print "<HTML><HEAD><TITLE>Affiliate Search</TITLE></HEAD>\n";
 print "<BODY bgcolor=\"\#FFFFFF\"><FORM METHOD=\"POST\" ACTION=\"/cgi/benefitscart/afmanage.cgi\">\n";
 print "<div align=\"center\"><a href=\"http://www.storeclerk.net\"><img src=\"/storeclerk/images/ccart.jpg\" alt=\"Affiliate Manager\" border=\"0\"></a></div>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"2\"><strong>NOT FOUND</strong><br>$Count2 Affiliate Records Searched</font></p>\n";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"2\">Sorry, no match was found for <b>$Search</b><br> Please try another keyword or associate number.</font></p>";
 print "<hr width=\"350\" align=\"center\">\n";
 print "<p align=\"center\"><a href=\"/cgi/benefitscart/afmanage.cgi\"><font face=\"arial\" size=\"2\">Return To Main Menu</font></a></p>\n";
 print "<br><br>\n";
 print "<p align=\"center\"><font face=\"arial\" size=\"1\">StoreClerk \&copy\; 2001</font> <a href=\"http://www.jmsonline.net\"><font face=\"arial\" size=\"1\">JMS Online Systems</font></a> <font face=\"arial\" size=\"1\">All Rights Reserved</font></p>\n";
 print "</FORM></BODY></HTML>\n";
 exit;
 }

sub GetFormInput
 {
 (*fval) = @_ if @_ ;
 local ($buf);
 if ($ENV{'REQUEST_METHOD'} eq 'POST') { read(STDIN,$buf,$ENV{'CONTENT_LENGTH'}); }
 else {	$buf=$ENV{'QUERY_STRING'};	}
 if ($buf eq "") {	return 0 ;	}
 else {
 @fval=split(/&/,$buf);
 foreach $i (0 .. $#fval)
  {
  ($name,$val)=split (/=/,$fval[$i],2);
  $val=~tr/+/ /;
  $val=~ s/%(..)/pack("c",hex($1))/ge;
  $name=~tr/+/ /;
  $name=~ s/%(..)/pack("c",hex($1))/ge;
  if (!defined($field{$name})) { $field{$name}=$val; } else { $field{$name} .= ",$val";	}
  }
 }
return 1;
}


