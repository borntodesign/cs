#!/usr/bin/perl
###### benefits_redirect.cgi
###### last modified 06/02/2003	Bill MacArthur

use CGI ':all';
use CGI::Carp 'fatalsToBrowser';
use DBI;
my ($dbuser, $dbpasswd, $dbflag, $key, $val, $myflg, $db, $dbserver);

open (INI, '/home/httpd/cgi-lib/cgi.ini') || die "Couldn't open ini file cgi.ini\n";
foreach (<INI>){
	if ($_ =~ m/^#/){next}
	chomp;
	($key, $val) = split /:/;
	if ($key eq 'username'){$dbuser = $val}
	elsif ($key eq 'password'){$dbpasswd = $val}
	elsif ($key eq 'benefits_redirect.cgi'){$myflg = $val}
	elsif ($key eq 'database'){$dbflag = $val}
	elsif ($key eq 'dbserver'){$dbserver = $val}
}
close INI;

if ($myflg ne 'on'){print redirect('http://www.clubshop.com/errors/scriptoff.html'); exit}
elsif ($dbflag ne 'on'){print redirect('http://www.clubshop.com/errors/dbmaint.html'); exit}
elsif (!($db= DBI->connect("DBI:Pg:dbname=network;host=$dbserver", "$dbuser", "$dbpasswd"))){
	print redirect('http://www.clubshop.com/errors/nodb.html');
	die "Database is down as of ".scalar localtime;
}

foreach (param()){
	$FORM{$_} = param($_);
}
##############################
# Check For ID Number or Die #
##############################

if ($FORM{sponsorID} eq '') {
	print header(), start_html();
	print "<P>Access Denied! No ID Number Submitted.<P>\n";
	print end_html();
	exit;
}

###### first we'll truncate the .html if they have submitted it so we can easily just upper case the thing
$FORM{sponsorID}=~ s/\.html//;
$FORM{sponsorID}= uc $FORM{sponsorID};
my $sth = $db->prepare("SELECT membertype FROM members WHERE Alias = '$FORM{sponsorID}'");
$sth->execute;
my $title = $sth->fetchrow_array;
$sth->finish;
if (!(defined $title )){	###### there was no record returned
	print header(-expires=>'-1d'), start_html(), h3("Invalid ID number");
	print h4("$FORM{sponsorID}<br>Back up and try again."), end_html();
}
elsif($title ne 'v'){
	print "Location: http://www.clubshop.com/benefits_access.html\n\n\n";
}
else{
	print "Location: http://www.clubshop.com/benefits/front.html\n\n\n";
}
$db->disconnect;
exit;

###### 06/02/03 revised to look at the cgi.ini in cgi-lib