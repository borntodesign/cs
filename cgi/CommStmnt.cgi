#!/usr/bin/perl -w
###### CommStmnt.cgi
###### Commission Statements
###### this script started as a direct copy of pireport.cgi
###### newly released:
###### last changed: 07/01/15	Bill MacArthur

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use Template;
use Encode;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use ADMIN_DB;
use XML::LibXML::Simple;
use G_S qw/$EXEC_BASE $LOGIN_URL/;
use XML::local_utils;
use GI;
binmode STDOUT, ":encoding(utf8)";

#use Data::Dumper;	# testing only

my %TMPL = (
	'xhtml'		=> 10979,
	'TT'		=> 10980,
	'months'	=> 10572
);

my %Links = (
	'compplan' => 'http://www.clubshop.com/manual/compensation/partner.html'
);

my $q = new CGI;
my $TT = Template->new();

my ($db, $lang_blocks, %params, $USER_ID, $meminfo, @periods, %Periods, $StatementDate, $CommMonthEnding);
my $gs = G_S->new({'db'=>\$db});

###### for now we'll accept a simple member ID
###### and if it turns out to be a Partner, we'll require a login so as to protect
###### the privacy of the Partners

my $ADMIN = AdminCK();
	
my $id = $q->cookie('id');
die "Bad ID cookie" if $id && $id =~ /\D/;

LoadParams();

my $rid = uc($params{'rid'});
$rid ||= ($q->cookie('id') || '') unless $ADMIN;	# if in admin mode, we will only accept a parameter for identification

unless ($rid)
{
	my $error = $ADMIN ? 'No rid parameter (rid=ID OF THE PARTNER). Necessary in ADMIN mode.' : 'You must be ' .
		$q->a({'-href'=>"$LOGIN_URL?destination=" . $q->script_name}, 'logged in') .
		' to use this report.';
	Err($error);
}

DB();

my $GI = GI->new('cgi'=>$q, 'db'=>$db);
LoadLangBlocks();

my $rpte = $db->selectrow_hashref("
	SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype,
		cbc.currency_code,
		exr.rate,
		cc.exponent AS currency_exponent,
		(my_exec(m.id)).id AS my_exec,
		a_level_stable.a_level
	FROM members m
	JOIN currency_by_country cbc
		ON cbc.country_code=m.country
	JOIN exchange_rates_now exr
		ON exr.code=cbc.currency_code
	JOIN currency_codes cc
		ON cbc.currency_code=cc.code
	LEFT JOIN a_level_stable
		ON a_level_stable.id=m.id
	WHERE ${\($rid =~ m/\D/ ? 'm.alias':'m.id')} = ?", undef, $rid);

unless ($rpte)
{
	Err("No matching records for: $rid");
	Exit();
}

###### are they a valid membership?
unless ((grep $_ eq $rpte->{'membertype'}, qw/v/) || $ADMIN)
{
	Err( $gs->ParseString($lang_blocks->{'no_rpt_available'}, $rpte->{'membertype'}) );
}

if ($rpte->{'membertype'} eq 'v' && ! $ADMIN)
{
	###### since this is a VIP we are reporting on we will do the following
	# make sure the user is logged in
	# make sure the user, if not the reportee, is upline
	( $USER_ID, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if (! $USER_ID || $USER_ID =~ /\D/)
	{
		Err(qq|A valid <a href="$LOGIN_URL?destination=${\$q->script_name()}">login</a> is required to view Partner reports.|);
	}

	###### make sure that the reportee is our downline... and not an Exec
	if ($USER_ID != $rpte->{'id'})
	{
		if (! $db->selectrow_array("SELECT id FROM relationships WHERE id= ? AND upline= ?", undef, $rpte->{'id'}, $USER_ID))
		{
			Err( $gs->ParseString($lang_blocks->{'not_in_dn'}, $rid) );
		}

		# get rid of the second conditional to simply block upline access to even a frontline Exec's report
		# if you do, you may as well, comment out the 'my_exec' portion of the SQL where it is called
		if ($rpte->{'a_level'} >= $EXEC_BASE && $rpte->{'my_exec'} != $USER_ID)
		{
			Err($lang_blocks->{'no_see_exec'});
		}
	}
}

CommStatementDates();	# load values, if available into @periods and %Periods

Output();

Exit();


###### ###### Start of Sub Routines ###### ######

sub AdminCK
{
#	return 0;	# uncomment to mimic ordinary operation even by Admin

	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return 0 unless $operator && $pwd;

	$db = ADMIN_DB::DB_Connect( 'CommStmnt.cgi', $operator, $pwd) ||	die "Failed to connect to the database with staff login";
	$db->{'RaiseError'} = 1;
	return $operator;
}

sub cboStatementDates
{
	return $q->popup_menu(
		'-class' => 'cbocomm',
		'-name' => 'period',
		'-values' => \@periods,
		'-labels' => \%Periods,
		'-default' => $params{'period'},
		'-onchange' => 'this.form.submit()'
	);
}

sub CommStatementDates
{
	# the following query will give us at least one entry with the latest commission statement date regardless of whether the individual involved had one then or not
	# this helps us to have a default statement date
	my $sth = $db->prepare("
		WITH latest AS (
			SELECT p.period, p.comm_statement_date
			FROM periods p
			WHERE comm_statement_date IS NOT NULL
			ORDER BY p.comm_statement_date DESC
			LIMIT 1
		)
		SELECT latest.period, latest.comm_statement_date FROM latest
		UNION
		SELECT DISTINCT p.period, p.comm_statement_date
		FROM periods p
		JOIN soa
			ON soa.comm_statement_period=p.period
			AND soa.id= $rpte->{'id'}
		ORDER BY 2 DESC
	");
	$sth->execute;
	while (my ($period, $date) = $sth->fetchrow_array)
	{
		push @periods, $period;
		$Periods{$period} = $date;
	}
	
	$StatementDate = $Periods{$params{'period'}} || $Periods{$periods[0]};
}

sub DB
{
	return if $db;
	$db = DB_Connect::DB_Connect('CommStmnt.cgi') || exit;
	$db->{'RaiseError'} = 1;
}

sub EmitXML
{
	print "Content-type: text/plain\n\n$_[0]\n";
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'<h4>There has been a problem</h4>',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
		
	Exit();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub Get_Mbr_Stats
{
	# if we have nothing in @periods, then there is nothing to show regardless of whether we received a parameter or not
	return () unless @periods;

	my $period = $Periods{ $params{'period'} } ? $params{'period'} : $periods[0];	# if the parameter does not match one of the periods available, take the most recent

	my $sth = $db->prepare("
		SELECT
	        COALESCE(soa.amount::TEXT, '') AS amount,
	        soa.trans_type,
	        SUBSTR(COALESCE(soa.description, soa_transaction_types.name, ''), 0, 35) as tdesc, 
	        DATE_TRUNC('second', soa.entered) AS entered,
	        soa_transaction_types.name AS trans_type_desc,
	        CASE WHEN soa.comm_statement_item=TRUE THEN COALESCE(soa.display_amount, soa.amount)::TEXT ELSE '' END AS csi_display_amount,
	        COALESCE(p.close::TEXT,'') AS comm_month_ending,
	        COALESCE(p.comm_statement_date::TEXT,'') AS statement_date

			
		FROM soa
		JOIN soa_transaction_types
			ON soa_transaction_types.id=soa.trans_type
		JOIN periods p
			ON p.period=soa.comm_statement_period
		WHERE soa.id= $rpte->{'id'}
		AND soa.void=FALSE
		AND soa.comm_statement_period= $period
		ORDER BY soa.entered ASC, soa.trans_id ASC");

	$sth->execute;
	my $rv = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		# there are occasions where there are multiple items with the same trans_type, so...
		# we will create an arrayref for the key value
		push @{$rv->{ $tmp->{'trans_type'} }}, $tmp;
	}
	
	$CommMonthEnding = '';
	if ($rv)
	{
		# get an element off our hash and get the comm month
		my ($i) = keys %{$rv};
		$CommMonthEnding = $rv->{$i}->[0]->{'comm_month_ending'};
	}
	else
	{
		return $rv;
	}

	# this will obtain the most recent exchange preceeding the exact date if there is nothing for the exact date
	# we are going to get the run date for the paid commission run first as that is the same date the income report uses for the exchange rate...
	# makes sense to use that date since processing was performed on that day
	my ($run_date) = $db->selectrow_array("SELECT run_date FROM archives.refcomp_me_dates WHERE paid=TRUE AND period= $period") || die "Failed to get run_date for period: $period";

	($rpte->{'rate'}) = $db->selectrow_array(qq|
		SELECT "rate"
		FROM exchange_rates
		WHERE "date"::DATE <= '$run_date'
		AND "code"= '$rpte->{'currency_code'}'
		ORDER BY "date" DESC
		LIMIT 1|) || die "Failed to obtain an exchange rate for $rpte->{'currency_code'} ON $run_date";

	# now create a localized version of the USD value for the actual commission amount
	foreach my $item (@{$rv->{'9000'}})
	{
		$item->{'csi_display_amount_localized'} = sprintf "\%\.$rpte->{'currency_exponent'}f", ($rpte->{'rate'} * $item->{'csi_display_amount'});
	}

	return $rv;
}

sub LoadLangBlocks
{
	my $tmp = $gs->Get_Object($db, $TMPL{'xhtml'}) || die "Failed to retrieve language pack template\n";
#	$tmp = decode_utf8($tmp);
	
	$lang_blocks = XML::local_utils::flatXHTMLtoHashref($tmp);
}

sub LoadParams
{
	foreach (qw/period rid/)
	{
		$params{$_} = $q->param($_) || '';
	}
}

sub MonthName
{
	my $date = shift;
	
	my %Months = (
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November',
		'12' => 'December'
	);
	my $months = $gs->Get_Object($db, $TMPL{'months'}) || die "Failed to retrieve the months template\n";
	$months = XML::LibXML::Simple::XMLin($months);
	$months = $months->{'months'}->{'month'};

	my @d = split(/-/, $date);

	return $months->{ $Months{$d[1]} }->{'content'};
}

sub Output
{
	my $stats = Get_Mbr_Stats();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}) ||	die "Failed to retrieve main template\n";
	my $tmpl = ();

	ParseLangBlocks({
		'rpte' => $rpte,
		'statement_date' => $StatementDate,
		'month_name' => MonthName($StatementDate),
		'year' => (split(/-/, $StatementDate))[0]
	});

	my $rv = $TT->process(\$tmp,
		{
			'rpte' => $rpte,
			'lang_blocks' => $lang_blocks,
			'stats' => $stats,
			'cboStatementDates' => cboStatementDates(),
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo,
			'statement_date' => $StatementDate,
			'comm_month_ending' => $CommMonthEnding
		}, \$tmpl);

	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'),
		$GI->wrap({
			'title'=>$lang_blocks->{'title'},
#			'style'=> $q->style({'-type'=>'text/css'}, '.cbocomm {font-size:100%;} a {color:#2A85E8; text-decoration:none;}'),
			'style'=>$GI->StyleLink('/css/pireport.css') . $GI->StyleLink('/css/CommStmnt.css'),
			#style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
			'content'=>\$tmpl
		});	
}

sub ParseLangBlocks
{
	my $args = shift;
	foreach my $key (keys %{$lang_blocks})
	{
		my $tmp = ();
		my $rv = $TT->process(\$lang_blocks->{$key}, $args, \$tmp);
		unless ($rv)
		{
			die $TT->error();
		}
		$lang_blocks->{$key} = $tmp;
	}
}

__END__

07/01/15	Added the binmode to deal with improper encoding