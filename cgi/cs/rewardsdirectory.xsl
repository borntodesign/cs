<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output
		method="xml" encoding="utf-8"
		omit-xml-declaration="yes"
		media-type="text/html"
	/>
	
	<xsl:template match="/" >
		
		
	<div>		
		<h1><a href="/business/" class="style12"><xsl:value-of select="//lang_blocks/heading" /></a></h1>
		<span class="Text_Body_Content">
			<br />
	    	<xsl:value-of select="//lang_blocks/introduction" />
	    	<br />
	    	<br />
		</span>
	</div>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FCBF12">
		<tr class="heading_two">
			<td class="heading_two_row" colspan="3" align="center" valign="top"><h2 class="style13"><xsl:value-of select="//lang_blocks/sub_heading" /></h2></td>
		</tr>
		<tr>
			<td colspan="3" align="left" valign="top" bgcolor="#FFFFFF" class="Text_Body_Content">
				<div class="search_container">
					<ul>
						<li class="country_search">
							<span class="style14"><xsl:value-of select="//lang_blocks/main_search/search_blurb" />:</span>
							<br />
							<span class="text_orange"><xsl:value-of select="//lang_blocks/main_search/country_label" /></span>
							<!--  Use JQuery to add a click event to this div -->
							<!--  a onclick="$('#country_scroll').toggle(); $('#province_scroll').hide(); $('#city_scroll').hide(); return false;" href="#">Italy</a -->
							<div class="scrollingSelected select_a_country">
								<xsl:choose>
									<xsl:when test="//data/country_label">
										<xsl:value-of select="//data/country_label" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="//lang_blocks/main_search/select_country" />
									</xsl:otherwise>
								</xsl:choose>
							</div>
							<div class="country_scroll scrollingSelections">
								<ul>
									<xsl:for-each  select="//data/countries/*">
										<li>
											<xsl:if test="selected"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
											<a>
												<xsl:attribute name="href"><xsl:value-of select="link" /></xsl:attribute>
												<xsl:value-of select="name" />
											</a>
										</li>
									</xsl:for-each>
								</ul>
							</div>
						</li>
						<li class="province_search">
							<span class="style14"><xsl:value-of select="//lang_blocks/main_search/search_blurb" />:</span>
							<br />
							<span class="text_orange"><xsl:value-of select="//lang_blocks/main_search/province_label" /></span>
							<!--  Use JQuery to add a click event to this div -->
							<!--  a onclick="$('#country_scroll').toggle(); $('#province_scroll').hide(); $('#city_scroll').hide(); return false;" href="#">Italy</a -->
							<xsl:if test="///data/provinces/province">
								<div class="select_a_province scrollingSelected">
									<xsl:choose>
										<xsl:when test="//data/province_label">
											<xsl:value-of select="//data/province_label" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="//lang_blocks/main_search/select_province" />
										</xsl:otherwise>
									</xsl:choose>
									
								</div>
								<div class="province_scroll scrollingSelections">
									<ul>
										<xsl:for-each  select="//data/provinces/*">
											<li>
												<xsl:attribute name="class"><xsl:value-of select="selected" /></xsl:attribute>
												<a>
													<xsl:attribute name="href"><xsl:value-of select="link" /></xsl:attribute>
													<xsl:value-of select="name" />
												</a>
											</li>
										</xsl:for-each>
									</ul>
								</div>
							</xsl:if>
						</li>
						<li class="city_search">
							<span class="style14"><xsl:value-of select="//lang_blocks/main_search/search_blurb" />:</span>
							<br />
							<span class="text_orange"><xsl:value-of select="//lang_blocks/main_search/city_label" /></span>
							<!--  Use JQuery to add a click event to this div -->
							<!--  a onclick="$('#country_scroll').toggle(); $('#province_scroll').hide(); $('#city_scroll').hide(); return false;" href="#">Italy</a -->
							<xsl:if test="//data/cities/city">
								<div class="select_a_city scrollingSelected">
									<xsl:choose>
										<xsl:when test="//data/city_label">
											<xsl:value-of select="//data/city_label" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="//lang_blocks/main_search/select_city" />
										</xsl:otherwise>
									</xsl:choose>
									
								</div>
								<div class="city_scroll scrollingSelections">
									<ul>
										<xsl:for-each  select="//data/cities/*">
											<li>
												<xsl:attribute name="class"><xsl:value-of select="selected" /></xsl:attribute>
												<a>
													<xsl:attribute name="href"><xsl:value-of select="link" /></xsl:attribute>
													<xsl:value-of select="name" />
												</a>
											</li>
										</xsl:for-each>
									</ul>
								</div>
							</xsl:if>
						</li>
					</ul>
				</div>
				<div class="report_container">
					<xsl:choose>
						<xsl:when test="//data/merchants">
						<input class="print_button" alt="Print" src="/images/btn/btn_print-88x22.png" type="image" />
						<div class="keyword_container">
							<ul>
								<li>
									<span class="style14"><xsl:value-of select="//lang_blocks/keyword_label" /></span>
									<br />
									<form name="keyword_form" class="keyword_form" method="get">
											<xsl:attribute name="action"><xsl:value-of select="//data/form/action" /></xsl:attribute>
										<input type="text" name="keywords"  maxlength="30" class="registration_textbox">
											<xsl:attribute name="value"><xsl:value-of select="//data/search_keywords" /></xsl:attribute>
										</input>
										
										<input type="image" name="keyword_submit" class="keyword_submit" align="absmiddle" src="/images/btn/btn_search-91x27.png" alt="Search" />
									</form>
								</li>
							</ul>
						
						
						</div>


							<ul>
								<xsl:for-each select="//data/merchants/*">
									<li>
										<xsl:if test="city_label != ''"><p><b class="city"><xsl:value-of select="city_label" /></b></p></xsl:if>
										<xsl:if test="category_description_label != ''"><p><b class="category"><xsl:value-of select="category_description_label" /></b></p></xsl:if>
										<xsl:if test="coupon"><div class="coupon">
																				<img>
																					<xsl:attribute name="src"><xsl:value-of select="coupon/src" /></xsl:attribute>
																					<xsl:attribute name="width"><xsl:value-of select="coupon/width" /></xsl:attribute>
																					<xsl:attribute name="height"><xsl:value-of select="coupon/height" /></xsl:attribute>
																					<xsl:attribute name="alt"><xsl:value-of select="location_name" /></xsl:attribute>
																				</img>
															</div></xsl:if>
										<xsl:if test="blurb or discount_blurb or URL">
										<img border="0" src="/images/icons/icon_info_15x15.png" class="info_icon">
											<xsl:attribute name="title"><xsl:value-of select="//lang_blocks/more_information" /></xsl:attribute>
											<xsl:attribute name="alt"><xsl:value-of select="//lang_blocks/more_information" /></xsl:attribute>
											<xsl:attribute name="id">_<xsl:value-of select="id" /></xsl:attribute>
										</img>
										<xsl:text> </xsl:text>
										</xsl:if>
										<b class="name">
											<xsl:choose>
												<xsl:when test="print_page_link">
													<a>
														<xsl:attribute name="href"><xsl:value-of select="print_page_link" /></xsl:attribute>
														<xsl:value-of select="location_name" />
													</a>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="location_name" />
												</xsl:otherwise>
											</xsl:choose>
										</b>
										<br />
			
										<span class="discount">
											<xsl:choose>
												<xsl:when test="special">
														<xsl:if test="special/bogo"><xsl:value-of select="//lang_blocks/specials/bogo" /><br /></xsl:if>
														<xsl:if test="special/bogoh"><xsl:value-of select="//lang_blocks/specials/bogoh" /><br /></xsl:if>
														<xsl:if test="special/amount_off"><xsl:value-of select="//lang_blocks/specials/amount_off" />: <xsl:value-of select="special/amount_off" /><br /></xsl:if>
														<xsl:if test="special/percentage_off"><xsl:value-of select="//lang_blocks/specials/percentage_off" />: <xsl:value-of select="special/percentage_off" /><br /></xsl:if>
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="discount/reward_points"><xsl:value-of select="//lang_blocks/discounts/reward_points" />: <xsl:value-of select="discount/reward_points" /><br /></xsl:if>
													<xsl:if test="discount/pay_points"><xsl:value-of select="//lang_blocks/discounts/pay_points" />: <xsl:value-of select="discount/pay_points" /><br /></xsl:if>
													<xsl:if test="discount/referral_fee"><xsl:value-of select="//lang_blocks/discounts/referral_fee" />: <xsl:value-of select="discount/referral_fee" /><br /></xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</span>
										<xsl:if test="blurb or discount_blurb or URL">
										<p class="extra_information">
											<xsl:attribute name="id">extra_information_<xsl:value-of select="id" /></xsl:attribute>
											<xsl:if test="blurb"><span class="blurb"><xsl:value-of select="blurb" /><br /><br /></span></xsl:if>
											<xsl:if test="discount_blurb"><span class="blurb"><xsl:value-of select="discount_blurb" /><br /></span></xsl:if>
											<xsl:if test="url"><a target="_blank" class="blurb"><xsl:attribute name="href"><xsl:value-of select="url" /></xsl:attribute><xsl:value-of select="url" /></a><br /></xsl:if>
										</p>
										</xsl:if>
										<p class="address_phone_container">
											<span class="address1"><xsl:value-of select="address1" /><br /></span>
											<span class="phone"><xsl:value-of select="phone" /></span>
										</p>
										<hr/>
									</li>
								</xsl:for-each>
							</ul>
						</xsl:when>
						<xsl:when test="//data/cities/city">
							<input id="print_button" alt="Print" src="/images/btn/btn_print-88x22.png" onclick="print(); return false;" type="image" />
							<div class="keyword_container">
								<ul>
									<li>
										<span class="style14"><xsl:value-of select="//lang_blocks/keyword_label" /></span>
										<br />
										<form name="keyword_form" class="keyword_form" method="post">
											<input type="text" name="keywords"  maxlength="30" class="registration_textbox">
												<xsl:attribute name="value"><xsl:value-of select="//data/search_keywords" /></xsl:attribute>
											</input>
											
											<input type="image" name="keyword_submit" class="keyword_submit" src="/images/btn/btn_search-91x27.png" align="absmiddle" alt="Search" />
										</form>
									</li>
								</ul>
							</div>
							<ul>
								<li><strong><xsl:value-of select="//lang_blocks/no_search_results" /></strong></li>
							</ul>
						</xsl:when>
						<xsl:when test="//data/provinces/province">
							<center><strong><xsl:value-of select="//lang_blocks/main_search/select_province" /></strong></center>
						</xsl:when>
						<xsl:otherwise>
							<center><strong><xsl:value-of select="//lang_blocks/main_search/select_country" /></strong></center>
						</xsl:otherwise>
					</xsl:choose>
				</div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			</td>
		</tr>
	</table>
	</xsl:template>
