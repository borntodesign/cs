#!/usr/bin/perl

=head1 NAME:
rewardsdirectory.cgi

	Display a list of links to and individual store listings

=head1
VERSION:

	0.1

=head1
PREREQUISITS:

	Other classes, or packages that are used

	strict, CGI, DB, DB_Connect, G_S, Sys::Hostname, DHS::UrlSafe

=cut

use strict;
use warnings;
use FindBin; # $FindBin::Bin / $FindBin::Script Should be used when creating error emails, or writing to error logs (this will give you the location, and name of the script).
#use lib "$FindBin::Bin/../../cgi-lib";
use lib '/home/httpd/cgi-lib';
use DHSGlobals;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DB;
use DB_Connect;
use G_S;
use DHS::UrlSafe;
use MerchantAffiliates;
use URI::Escape;
use DHS::Templates;
use XML::local_utils;
use XML::LibXML::Simple;
use Lingua::EN::NameCase 'NameCase';
use Locations;
use Sanitize;
use Encode;
use Data::Dumper;
use File::Temp;

# doing that on the newer server totally goobers up the rendition
#binmode(STDOUT, ":utf8");
#binmode(STDERR, ":utf8");
#use Sys::Hostname; #This is used to retrieve the systems hostname.  This should mainly be used with error emails, or writting to an error log (Sys::Hostname::hostname()).

my $DEBUG = 0;	# change to -0- for production... look for calls to debug() to see what is being called
my $do_tmp_files = 0;	# for some reason when this was developed, files were being created for each call to the script... set this to -0- to disable this behavior

my ($db, %params) = ();
my $CGI = CGI->new();
my $DHS_UrlSafe = DHS::UrlSafe->new();
my $DHS_Templates = DHS::Templates->new();
my $MerchantAffiliates = {};
my $G_S = G_S->new({'db'=>\$db, 'CGI'=>$CGI});
my $XML_Simple = XML::Simple->new();
my $Locations = {};
my $Sanitize = {};

=head2
VARIABLES, HASHES, and ARRAYS:

	Variables, Hashes, and Arrays that are used globally throughout the script.

=cut

my %page_title = ('country'=>'', 'state'=>'','city'=>'', 'location_cities'=>'', 'location_categories'=>'', 'location_names'=>'');
my $xml = '';
my $menus_xml = '';
my $merchants_xml = '';
my $xsl = '';
my $html_template = '';
my $countries_translated_xml = '';
my $countries_translated_hash = {};
my %template_values = ();
my $messages = {};
my $messages_xml = '';
my $params = '';
our @fields_to_build_store_url = ('country', 'state', 'city', 'location_name_url');
my $skip_processing_html = 0;
my $control = '';
my $page_content = '';
my $application_configuration = {};
my $url = $CGI->url('-absolute'=>1);
#debug(__LINE__ . ' utf8:'.Encode::is_utf8($url));

my @language_preferences = $CGI->http('HTTP_ACCEPT_LANGUAGE') ? split(/,/, $CGI->http('HTTP_ACCEPT_LANGUAGE')) : 'en';

$language_preferences[0] =~ s/^(\w\w).*/$1/ if exists $language_preferences[0] && $language_preferences[0];

my $correct_name_case = (exists $language_preferences[0] && $language_preferences[0] && $language_preferences[0] =~ /en|es/) ? 1:0;

$Lingua::EN::NameCase::SPANISH = 1 if exists $language_preferences[0] && $language_preferences[0] && $language_preferences[0] =~ /es/;

my $member_type = undef;

####################################################
# Main
####################################################

LoadParams();

exit unless $db = DB_Connect('merchants');
$db->{'RaiseError'} = 1;
#$db->{'pg_enable_utf8'} = 1;
#
# The following is for keeping track of the number of time this function is started
#
open TIMES_STARTED, "<", "/home/httpd/html/stats/started" or die $!;
my $num_times = <TIMES_STARTED>;
$num_times ++;
close (TIMES_STARTED);
open TIMES_STARTED, ">", "/home/httpd/html/stats/started" or die $!;
print TIMES_STARTED $num_times;
close (TIMES_STARTED);
#
# take above out when stats are finished. Also take out TIMES_SEARCHED in here as well
#
#eval{
	
	$MerchantAffiliates = MerchantAffiliates->new($db);
	$Locations = Locations->new($db);
	$Sanitize = Sanitize->new($db, {});
	
	$Sanitize->sanitizeForXML(\$url);
#debug(__LINE__ . ' utf8:'.Encode::is_utf8($url));
	# I should retrieve their cookie, and if they are a VIP show the Pay Points in the listing.
	
	if(DHSGlobals::PRODUCTION_SERVER || DHSGlobals::DB_LAST_UPDATED > 20100817)
	{
		$xml = $G_S->GetObject(10822);

		$xsl = $G_S->GetObject(10891);
		$countries_translated_xml = $G_S->GetObject(10407);
	}
	else
	{
		$xml = $G_S->GetObject(10815);
		$xsl = $G_S->GetObject(10816);
		$countries_translated_xml = $G_S->GetObject(10407);
	}
#
# Whe countries_translated_xml is retrieved from the database. turn off the utf8 flag on the text so the
# XMLin doesn't barf on it
#
$countries_translated_xml = $db->selectrow_array('SELECT value from object_translations where pk=1202') if $DEBUG;	# this will give us the English list

#ScreenDebug($countries_translated_xml);

	$countries_translated_xml = encode_utf8($countries_translated_xml);
#	$xml = encode_utf8($xml);
	
	$html_template = $DHS_Templates->retrieveShtmlTemplate('/cs/_includes/templates/merchant_shell.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'), undef, undef, 1);
	
#debug("HTML template\n$html_template", 'die');

	$application_configuration = $MerchantAffiliates->retrieveMerchantConfigHashRef();
	
#	warn "done getting everything";
	my $packages_allowed_a_url = {};
	foreach (@{$application_configuration->{'url'}->{'allowed_package'}})
	{
		$packages_allowed_a_url->{$_} = $_;
	}

	$countries_translated_hash = $XML_Simple->XMLin($countries_translated_xml, 'NoAttr'=>1);
	
	$G_S->Prepare_UTF8($countries_translated_hash);
#debug(__LINE__ . ' utf8:'.Encode::is_utf8($xml));
	$xml = encode_utf8($xml);
	$page_content = $XML_Simple->XMLin($xml, 'NoAttr' => 1);
	$xml = decode_utf8($xml);
	$G_S->Prepare_UTF8($page_content);

#ScreenDebug($page_content->{'main_search'}->{'select_city'});
	my @search_parameters = $DHS_UrlSafe->processURL($CGI->url('-absolute'=>1), 1);

	# the old mall invokes the URL with named parameters instead of path elements
	# this means one always enters the directory having to select a country and so on
	# instead of having the country and state selected directly from the mall
	if ($CGI->param('country'))
	{
		$search_parameters[2] = $CGI->param('country');
		$search_parameters[3] = $CGI->param('state');
	}
	
	my $temp = \@search_parameters;
	
# this is a load of crap	
#	$template_values{'CONTROL'} = $control = "$search_parameters[0]/$search_parameters[1]";
# I (Bill) is hard-coding it so that it works if invoked directly as a script instead of a rewrite within Apache
	$template_values{'CONTROL'} = $control = "cs/rewardsdirectory";
#	$Sanitize->sanitizeData(\$template_values{'CONTROL'}) if $template_values{'CONTROL'};
	$Sanitize->sanitizeData(\$control) if $control;

	
	shift(@search_parameters);
	shift(@search_parameters);
	#$search_parameters[0] country
	#$search_parameters[1] province/state
	#$search_parameters[2] city/town/village
	#$search_parameters[3] store
#warn "b4 $search_parameters[2]" if $search_parameters[2];
#warn Encode::is_utf8($search_parameters[2]);
	$search_parameters[2] = decode_utf8($search_parameters[2]) if $search_parameters[2];
#warn Encode::is_utf8($search_parameters[2]);
#warn $search_parameters[2] if $search_parameters[2];
#if ($search_parameters[2]){print STDERR "print $search_parameters[2]";}

	my %location_params = ();
	
	#If the country code doesn't meet the minimum requirements why bother.
	if($search_parameters[0] && $search_parameters[0] =~ /^\w{2}$/)
	{
		$location_params{$fields_to_build_store_url[0]} = $search_parameters[0] if $search_parameters[0]; # Country
		$location_params{$fields_to_build_store_url[1]} = $search_parameters[1] if $search_parameters[1]; # Province/State
		$location_params{$fields_to_build_store_url[2]} = $search_parameters[2] if $search_parameters[2]; # City
		$location_params{$fields_to_build_store_url[3]} = $search_parameters[3] if $search_parameters[3];
		
		$Sanitize->sanitizeData(\$location_params{$fields_to_build_store_url[0]}) if $location_params{$fields_to_build_store_url[0]};
		$Sanitize->sanitizeData(\$location_params{$fields_to_build_store_url[1]}) if $location_params{$fields_to_build_store_url[1]};
		$Sanitize->sanitizeData(\$location_params{$fields_to_build_store_url[2]}) if $location_params{$fields_to_build_store_url[2]};
		$Sanitize->sanitizeData(\$location_params{$fields_to_build_store_url[3]}) if $location_params{$fields_to_build_store_url[3]};
		
	}
	
	my @reward_merchant_package_ids = ();
	my $reward_merchant_package_ids_csv = undef;
	
	
	@reward_merchant_package_ids = keys %{$MerchantAffiliates->{__merchant_type_by_package}};
	
	$reward_merchant_package_ids_csv = join(' , ', @reward_merchant_package_ids);
	
	
	my @countries = ();
	my @provinces = ();
	my @cities = ();
	my @locations = ();
	my $provinces_translated_hash = {};
		
	#TODO: Use the cache for these
	@countries = $MerchantAffiliates->retrieveMerchantLocationCountries('active', $reward_merchant_package_ids_csv);
	@provinces = $MerchantAffiliates->retrieveMerchantLocationProvinces('active', $location_params{$fields_to_build_store_url[0]}, $reward_merchant_package_ids_csv) if(exists $location_params{$fields_to_build_store_url[0]});		
	@cities = $MerchantAffiliates->retrieveMerchantLocationCities('active', $location_params{$fields_to_build_store_url[0]}, $location_params{$fields_to_build_store_url[1]}, $reward_merchant_package_ids_csv) if(exists $location_params{$fields_to_build_store_url[0]} && exists $location_params{$fields_to_build_store_url[1]});

#
# @countries is an array of all countries that have merchants
#
	#TODO: Check the query string, and if countries, provinces, or cities are null take them back a step.  I should ask about this.
	
	#Create the Country Links.
	if(scalar(@countries) > 0)
	{
		my $country_selected = (exists $location_params{$fields_to_build_store_url[0]}) ? $location_params{$fields_to_build_store_url[0]}: undef;
		$menus_xml = createMenuList(\@countries, $countries_translated_hash, $country_selected, 'countries', 'country', $correct_name_case);
debug(__LINE__ . ' utf8:'.Encode::is_utf8($menus_xml));
#print STDERR $menus_xml;
#
# $menu_xml contains the html necessary to cresate the countries list it will be scalar and not have the utf8 flag set
#

		#TODO Use the correct language pieces here if the country, province, city is not selected.
		my $country_label = '';
		
		if(exists $countries_translated_hash->{lc($country_selected)})
		{
			$country_label = $correct_name_case ? Lingua::EN::NameCase::NameCase($countries_translated_hash->{lc($country_selected)}): $countries_translated_hash->{lc($country_selected)};
		}
		else
		{
			$country_label = $page_content->{'main_search'}->{'select_country'};
		}
			
			
		$country_label = "<country_label>$country_label</country_label>\n";
		$Sanitize->convertToUtf8(\$country_label);
			
		$menus_xml .= $country_label;

# that whole concept of loading up the title with every country is ludicrous and not the least bit helpful in SE rankings now, so axe it
		$page_title{'country'} = '';
		
		if($country_selected && exists $countries_translated_hash->{lc($country_selected)})
		{
			$page_title{'country'} = $correct_name_case ? Lingua::EN::NameCase::NameCase($countries_translated_hash->{lc($country_selected)}) : $countries_translated_hash->{lc($country_selected)};
			$Sanitize->convertToUtf8(\$page_title{'country'});			
		}
#		else
#		{
#			my @title_country_names = ();
#			foreach (@countries)
#			{
#				my $temp = $countries_translated_hash->{lc($_->{'country'})};
#				#$Sanitize->convertToUtf8(\$temp);
#				next unless $temp;	# don't ask my why we end up with an undef for one of the keys, but we will skip it if we do
#				push @title_country_names, $temp;
#			}
#			
#			$page_title{'country'} = join(' ', @title_country_names);
#			
#			#$Sanitize->convertToUtf8(\$page_title{'country'});
#		
#		}

			
			#Create the Province Links.
			if(scalar(@provinces) > 0)
			{
				$provinces_translated_hash = $Locations->getProvincesByCountryCode({'country_code'=>$country_selected}) if $country_selected;
				
				foreach ($provinces_translated_hash)
				{
					my $temp = $provinces_translated_hash->{$provinces_translated_hash};
					#$Sanitize->convertToUtf8(\$temp);
					$provinces_translated_hash->{lc($_)} = $temp;
					delete $provinces_translated_hash->{$_};
					
				}
				$G_S->Prepare_UTF8($provinces_translated_hash->{$_}) foreach keys %{$provinces_translated_hash};
# the following produced proper characters in a browser with the above line
#print $CGI->header('-charset'=>'utf-8');
#foreach my $c (keys %{$provinces_translated_hash}){ print "$c : $provinces_translated_hash->{$c}->{'name'}\n";
#	# warn "$_" foreach keys %{$provinces_translated_hash->{$c}}
#}
#		exitScript();		
				my $province_selected = (exists $location_params{$fields_to_build_store_url[1]}) ? $location_params{$fields_to_build_store_url[1]}: undef;
				$Sanitize->convertToUtf8(\$province_selected);

# if ScreenDebug is done on the function call, the result is well formed characters... and so is menus_xml
#debug(__LINE__ . ' utf8:'.Encode::is_utf8($menus_xml));

				$menus_xml .= createMenuList(\@provinces, $provinces_translated_hash, $province_selected, 'provinces', 'province', $correct_name_case);
#				ScreenDebug($menus_xml);
				#TODO: I should check to see if @locations is populated, if it is I should should just use $location_params{$fields_to_build_store_url[1]} for the $province_label.
				my $province_label = '';
				
				if(defined $province_selected)
				{
					$province_label = (exists $provinces_translated_hash->{$province_selected} && $provinces_translated_hash->{$province_selected}->{'name'}) ? $provinces_translated_hash->{$province_selected}->{'name'} : $province_selected;
					
					$province_label = Lingua::EN::NameCase::NameCase($province_label) if $correct_name_case;
#warn "province label 1: $province_label";
				}
				else
				{
					$province_label = $page_content->{'main_search'}->{'select_province'};
#warn "province label 2: $province_label";
				}  
#warn "province label 3: $province_label - utf8 flag:" . Encode::_utf8_off($province_label);
#Encode::_utf8_off($province_label);
				$province_label = "<province_label>$province_label</province_label>\n";
#				$Sanitize->convertToUtf8(\$province_label);
				
				$menus_xml .= $province_label;

				if (defined $province_selected && exists $provinces_translated_hash->{$province_selected} && $provinces_translated_hash->{$province_selected}->{'name'})
				{
					$page_title{'state'} = $correct_name_case ? Lingua::EN::NameCase::NameCase($provinces_translated_hash->{$province_selected}->{'name'}) : $provinces_translated_hash->{$province_selected}->{'name'};
				}
				else
				{
					my @title_provinces = ();
					foreach (@provinces)
					{
						my $temp = $correct_name_case ? Lingua::EN::NameCase::NameCase($_->{'state_label'}) : $_->{'state_label'};
						push @title_provinces, $temp;
					}
					
					$page_title{'state'} = join(' ', @title_provinces);
				}

				my $city_selected = undef;
				#Create the City Links.
				if(scalar(@cities) > 0)
				{
					$city_selected = (exists $location_params{$fields_to_build_store_url[2]}) ? $location_params{$fields_to_build_store_url[2]}: undef;

					$Sanitize->convertToUtf8(\$city_selected);
					
					$menus_xml .= createMenuList(\@cities, undef, $city_selected, 'cities', 'city', $correct_name_case);

					my $city_label = $city_selected ? $city_selected : $page_content->{'main_search'}->{'select_city'};
					
					$city_label = Lingua::EN::NameCase::NameCase($city_label) if $correct_name_case;
					
					$city_label = "<city_label>$city_label</city_label>\n";

					$menus_xml .= $city_label;
					
					if ($city_selected)
					{
						$page_title{'city'} =  $correct_name_case ? Lingua::EN::NameCase::NameCase($city_selected): $city_selected ;						
					}
				}

				# When we want to just retrieve the merchants after a city has been selected move the "retrieveLocations" call, and the
				# following code block into the "#Create the City Links" code block above
				
				
				#TODO: If there are keywords, retrieve the locations based on the key words.
				my $cleaned_keywords = $CGI->param('keywords');
				
				if ($cleaned_keywords)
				{
					$Sanitize->sanitizeData(\$cleaned_keywords);
					#$Sanitize->convertToUtf8(\$cleaned_keywords);
				}
						
				my @temp_keywords = $cleaned_keywords ? split(/\s/, $cleaned_keywords): ();
				
				my @keywords = ();
				
				if ($cleaned_keywords)
				{
					my $temp = "<search_keywords>$cleaned_keywords</search_keywords>";
					#$Sanitize->convertToUtf8(\$temp);
					$menus_xml .= $temp;
				}
				
				
				foreach (@temp_keywords)
				{
					$_ =~ s/\s//g;
					push @keywords, uc($_) if $_;	
				}

#
# this is for stats for searching
#				
                                if (@temp_keywords) {
				    open TIMES_SEARCHED, "<", "/home/httpd/html/stats/searched" or die $!;
				    my $num_times = <TIMES_SEARCHED>;
				    $num_times ++;
				    close (TIMES_SEARCHED);
				    open TIMES_SEARCHED, ">", "/home/httpd/html/stats/searched" or die $!;
				    print TIMES_SEARCHED $num_times;
				    close (TIMES_SEARCHED);
                                    open SEARCHED_FOR, ">>/home/httpd/html/stats/searched_for" or die $!;
                                    foreach (@temp_keywords) {
					print SEARCHED_FOR $_ . "\n";
				    }
                                    close (SEARCHED_FOR);
				}
#
# take out above when stats are removed
#

			        my $language_pref = $G_S->Get_LangPref;
				@locations = $MerchantAffiliates->retrieveLocations(\%location_params, $reward_merchant_package_ids_csv, $language_pref, \@keywords) if $city_selected;
				if(scalar(@locations) > 0)
				{
					
					my $city = '';
					my $merchant_category_description = '';
					
					my $exchange_rate_query =<<EOT;

					SELECT 
						ern.rate, 
						cc.description
					FROM 
						currency_by_country cbc
					JOIN 
					exchange_rates_now ern
						ON 
						ern."code" = cbc.currency_code
					JOIN 
					currency_codes cc
						ON 
						cc."code" = cbc.currency_code
					WHERE 
						cbc.country_code= ?

EOT

#					my $exchange_rate = 1;
#					my $currency_name = '';
					
					my $country_code = uc($country_selected);
					my ($exchange_rate, $currency_name) = $db->selectrow_array($exchange_rate_query, undef, $country_code);
					$exchange_rate = sprintf("%.3f", $exchange_rate);
					
					#If the person has the ID cookie set check their member type.
					if($CGI->cookie('id'))
					{
						my $id = $CGI->cookie('id');
						
						$id =~ s/\D//;
						
						$MerchantAffiliates->setMembers();
						$member_type = $MerchantAffiliates->{'Members'}->getMemberInformationByMemberID({'member_id'=>$id, 'field'=>'membertype'});
						
					}
					
					
					
					my $is_vip_flag = (defined $member_type && $member_type eq 'v') ? 1:0;
					
					#Loop through each location.
					#I need some template for the stores, I should probably do an XSL XML conversion
#######################################################################################################################################
					foreach (@locations)
					{
						
						
						# city, The city is a heading if the city hasn't been specified in the query string.
						$_->{'city'} = $correct_name_case ? Lingua::EN::NameCase::NameCase($_->{'city'}): $_->{'city'};
						$Sanitize->convertToUtf8(\$_->{'city'});
						$_->{'city_label'} = $city = $_->{'city'} if (!$city || $city ne $_->{'city'});
						
						$page_title{'location_cities'} .=  " $_->{city_label}" if (exists $_->{'city_label'} && $page_title{'city'} ne $city);
						
						# Merchant Business Type (most of this is translated now :D )
						# I'll need to make a call for the information, or create a join when the location information is retrieved
						$_->{'category_description'} = $correct_name_case ? Lingua::EN::NameCase::NameCase($_->{'category_description'}) : $_->{'category_description'};
						##$Sanitize->convertToUtf8(\$_->{'category_description'});
						if (!$merchant_category_description || $merchant_category_description ne $_->{'category_description'})
						{
							$_->{'category_description_label'} = $merchant_category_description = $_->{'category_description'};
							$page_title{'location_categories'} .= " $_->{'category_description'}";
						}
						
						
						$page_title{'location_names'} .= " $_->{location_name}";
						
						# Determin what benefits the merchant gets, do some formatting, and make the hash XML safe
						# state/province, description, location_name, 
						
						# The location name will be a link to the print page
						# if($MerchantAffiliates->{__package_upgrade_values}->{$_->{discount_type}} > 9)
						# I should keep the location of the business listing in a config.
						my @url_for_individual_location = ('/business');
						foreach my $index (@fields_to_build_store_url)
						{
							last if ! $_->{$index};
							Encode::_utf8_off($_->{$index});
							push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($_->{$index});
						}
						#TODO: add an if here for the print page.
						
						$_->{print_page_link} = join('/', @url_for_individual_location) if ($MerchantAffiliates->{__package_upgrade_values}->{$_->{discount_type}} > 9);
						
						delete $_->{url};
						
						#Coupon holds the merchants banner.  I believe it holds HTML, so we have to deal with that.
						
						if (! exists $_->{'discount_type'} || $_->{'discount_type'} < $application_configuration->{'coupon'}->{'minimum_package'} || !  $_->{'coupon'})
						{
							delete $_->{'coupon'};
						}
						else
						{
							$_->{'coupon'} = $XML_Simple->XMLin($_->{'coupon'});
						}

						#delete $_->{'blurb'} if (! $_->{'blurb'} || ($_->{discount_type} <= $application_configuration->{'blurb'}->{'minimum_package'}));
						delete $_->{'blurb'} if (! $_->{'blurb'});
						
						#@locations is going to be transformed int XML, so we don't want any empty nodes, or invalid characters.
						foreach my $key (keys %{$_})
						{
							if (! $_->{$key})
							{
								delete $_->{$key};
								next;
							}
							
							next if $key eq 'category_description_label'; 
						}
						
						
						if($MerchantAffiliates->getMerchantTypeNameById($_->{'discount_type'}) eq 'offline')
						{
							# Specials are for 'offline_reward' merchants
							delete $_->{'special'};
							my $temp_discount = $_->{'discount'};
							$_->{'discount'} = {};
							$_->{'discount'}->{'reward_points'} = sprintf("%.2f ", ( $temp_discount - ( $temp_discount * .6 ) ) * 100 ) . " %";
							$_->{'discount'}->{'pay_points'} = sprintf("%.2f", ( $temp_discount * 20) / $exchange_rate) . ' %' if $is_vip_flag;
							#$_->{'discount'}->{referral_fee} = sprintf("%.2f", ( $temp_discount * 20) / $exchange_rate) . ' %' if $is_vip_flag;

#						    warn "Offline: " . Dumper($_);
						}
						else
						{
						    # warn "Otherwise: " . Dumper($_);
							# Discount is for 'offline' merchants
							delete $_->{'discount'};
							my $temp_special = $_->{'special'} || '';
							$_->{'special'} = {};
							# Specials are for 'offline_reward' merchants
							if ($temp_special =~ /half|free/)
							{
								
								if($temp_special eq 'free')
								{
									$_->{'special'}->{'bogo'} = 'bogo';
								}
								else
								{
									$_->{'special'}->{'bogoh'} = 'bogoh';
								}
								
							}
							elsif($temp_special =~ /^\d+$/)
							{
								$_->{'special'}->{'amount_off'} = sprintf("%.2f", $temp_special );
							}
							else #elsif($_->{'special'})
							{
								$_->{'special'}->{'percentage_off'} =  sprintf("%.0f", $temp_special * 100) . ' %';
							}
							
						    } # end of main else for not targeted merchants
						
						
					}# end foreach (@locations)
					
					my $tmp = ();
					$tmp = File::Temp->new( 'UNLINK' => 0, 'TEMPLATE' => 'tmpXXXXX', 'DIR' => '/tmp/', 'SUFFIX' => '.csv') if $do_tmp_files;
					my $csvfile = $tmp->filename if $do_tmp_files;
					print $tmp "Category, Store Name, Percent off, Discount Description, phone, address,  city, Provence/State, Postal Code\n" if $do_tmp_files;
					my $cat = ' ';
					foreach my $merch (@locations)
					{
						$G_S->Null_to_empty($merch);
					    if ($cat ne $merch->{'category_description'})
					    {
							$cat = $merch->{'category_description'};
                            print $tmp "$cat,,,,,,,,,\n" if $do_tmp_files;
					    }
					    
						my $dblurb = " ";
						my $pctamt = " ";
						
						if ($merch->{'discount_blurb'})
						{
							$dblurb = $merch->{'discount_blurb'};
					    }

						if (exists $merch->{'discount'})
						{
						#$pctamt .= "PP: " . $merch->{'discount'}->{pay_points};
                                                #$pctamt .= " RF: " . $merch->{'discount'}->{referral_frr};
                                                $pctamt .= " CC: " . $merch->{'discount'}->{'reward_points'};
					    }
					    elsif (exists $merch->{'special'} && exists $merch->{'special'}->{'percentage_off'} && $merch->{'special'}->{'percentage_off'})
					    {
							$pctamt .= " PO: " . $merch->{'special'}->{'percentage_off'};
					    }
					    
					    my $special = "";
					    if (exists $merch->{'special'})
					    {
							$special = $merch->{'special'}->{'percentage_off'};
					    }
                                        
						print $tmp "\" \" ,\"$merch->{'location_name'}\",\"$special\",\"$dblurb\",\"$pctamt\",\"$merch->{phone}\",\"$merch->{'address1'}\",\"$merch->{'city'}\",\"$merch->{'state'}\",\"$merch->{'postalcode'}\"\n" if $do_tmp_files;
#					    if (! exists $merch->{'special'} && $merch->{id} == 12939) {
#						warn "Special doesn't exist 12939" . Dumper($merch);
#					    }
#					    elsif ($merch->{id} == 12939) {
#						warn "Special does exist 12939" . Dumper($merch);
#					    }
					} #Close for foreach my merch (@locations})

					close($tmp) if $do_tmp_files;
					chmod(0666, $csvfile) if $do_tmp_files;
					
					$merchants_xml = $XML_Simple->XMLout({'merchant'=>[@locations]}, 'NoAttr' => 1, 'RootName'=>'merchants');
					
					substr($merchants_xml, 11, 0,  "<cvsdir>$csvfile</cvsdir>") if $do_tmp_files;
#					warn "Merchants xml:". Dumper($merchants_xml);
					
				}# end if(scalar(@locations) > 0)
				
				
			}# end if(scalar(@provinces) > 0)
			
		}
		else
		{
			if(scalar(@search_parameters) > 0)
			{
				unshift @search_parameters, $control;
				pop(@search_parameters);
				
				my @redirect_url_parts = ();
				foreach my $index (@search_parameters)
				{
					last if ! $index;
					
					push @redirect_url_parts, $DHS_UrlSafe->encodeUrlPart($index);
				}
				
				my $new_search_url_for_location = join('/', @redirect_url_parts);
				
				#warn "\n\n redirect /$new_search_url_for_location \n";
				print $CGI->redirect('-uri'=>"/$new_search_url_for_location");
				
				$skip_processing_html = 1;
				
			}
		}# end if(scalar(@countries) > 0)

	
	if(! $skip_processing_html)
	{
		$template_values{'TITLE'} = "$page_title{'country'} $page_title{'state'} $page_title{'city'} $page_title{'location_cities'} $page_title{'location_names'} $page_title{'location_categories'}";
		
		# get rid of leading and trailing whitespace
		$template_values{'TITLE'} =~ s/^\s*|\s*$//g;
		
		if ($template_values{'TITLE'})
		{
			$template_values{'TITLE'} = $page_content->{'title'} . ' - ' . $template_values{'TITLE'};
		}
		else
		{
			$template_values{'TITLE'} = $page_content->{'title'};
		}
		
		
		$template_values{'TITLE'} = encode_utf8($template_values{'TITLE'});
#		Encode::_utf8_off($template_values{'TITLE'});
		
		$template_values{'META_DESCRIPTION'} = "$page_content->{'heading'} $page_content->{'introduction'}";
#		ScreenDebug('utf8:'.Encode::is_utf8($template_values{'META_DESCRIPTION'}).'  ' . $template_values{'META_DESCRIPTION'});
#		$template_values{'META_DESCRIPTION'} =~ s/\W/ /g if $template_values{'META_DESCRIPTION'};
#		$template_values{'META_DESCRIPTION'} =~ s/\ \_+/ /g if $template_values{'META_DESCRIPTION'};
		
		$template_values{'META_DESCRIPTION'} = encode_utf8( qq(<meta name="description" content="$template_values{'META_DESCRIPTION'}" />) );
		
		if(! $template_values{'META_KEYWORDS'})
		{
			my $keywords = $template_values{'TITLE'};
			$keywords =~ s/,|"|\s-\s/ /g; #"
			$keywords =~ s/\s\s/ /g;
			$template_values{'META_KEYWORDS'} = qq(<meta name="keywords" content="$keywords" />);			
		}

#		Encode::_utf8_off($template_values{'META_DESCRIPTION'});# = encode_utf8($template_values{'META_DESCRIPTION'});
		
#######
my $menus2_xml = $menus_xml;#decode_utf8($menus_xml);
my $menus3_xml = $menus2_xml;#encode_utf8($menus2_xml);
debug(__LINE__ . ' utf8:'.Encode::is_utf8($menus2_xml));
debug(__LINE__ . ' utf8:'.Encode::is_utf8($menus3_xml));

# this stuff will not/should not have any special characters in it
#debug(__LINE__ . ' utf8:'.Encode::is_utf8($url));
#debug(__LINE__ . " $url");
				my $output_xml = <<EOT;

<root>

$xml

<data>

<form>
	<action>$url</action>
</form>

$menus3_xml

$merchants_xml

</data>

</root>

EOT
	debug(__LINE__ . ' utf8 flag:' . Encode::is_utf8($output_xml));
	$output_xml = encode_utf8($output_xml);
#ScreenDebug(__LINE__ . "\n$output_xml");

#		$template_values{'PRINT_CONTENT'} = $template_values{'PAGE_CONTENT'} = decode_utf8( XML::local_utils::xslt($xsl, $output_xml) );
		$template_values{'PRINT_CONTENT'} = $template_values{'PAGE_CONTENT'} = XML::local_utils::xslt($xsl, $output_xml);
	debug(__LINE__ . ' utf8 flag:' . Encode::is_utf8($template_values{'PRINT_CONTENT'}));
#		$template_values{'PRINT_CONTENT'} = decode_utf8($template_values{'PRINT_CONTENT'});
#ScreenDebug(__LINE__ . "\n$template_values{'PRINT_CONTENT'}");
		#This is here so we don't have duplicate IDs for the more information icon, and extra_information areas.
		$template_values{'PRINT_CONTENT'} =~ s/"_(\d+)/"_print$1/g;
		$template_values{'PRINT_CONTENT'} =~ s/"extra_information_(\d+)/"extra_information_print$1/g;
		$template_values{'PRINT_CONTENT'} =~ s/liclass/prntliclass/g;

#		my $tmpval = $template_values{'PRINT_CONTENT'};
#		$template_values{'PRINT_CONTENT'} = decode_utf8($tmpval);
#		$template_values{'PAGE_CONTENT'} = $template_values{'PRINT_CONTENT'};
#		$template_values{'TITLE'} = decode_utf8($template_values{'TITLE'});

if ($params{'output'} eq 'template_keys')
{
	print $CGI->header('-type'=>'text/plain', '-charset'=>'utf-8');
	foreach my $k (keys %template_values)
	{
	#	debug("$k utf8:".Encode::is_utf8($template_values{$k}));
	#next if $k eq 'PRINT_CONTENT';
	#Encode::_utf8_on($template_values{$k}) if grep $_ eq $k, (qw/TITLE META_KEYWORDS/);
	print "$k utf8:".Encode::is_utf8($template_values{$k})."\n$template_values{$k}\n\n";
	}
	exitScript();
}


#debug(__LINE__ . Encode::is_utf8($html_template));

#utf8 flag is on at this point for $html_template, regardless of the translation
#$html_template = encode_utf8($html_template);
debug(__LINE__ . ' utf8: ' . Encode::is_utf8($html_template));
#$html_template = decode_utf8($html_template);
#debug(__LINE__ . ' utf8: ' . Encode::is_utf8($html_template));
		my $output = $DHS_Templates->processHtmlTemplate(\%template_values, $html_template);
#		$output = encode_utf8($output);
	debug(__LINE__ . ' utf8 flag:' . Encode::is_utf8($output));
		
		if ($params{'output'} eq 'html_template')
		{
			print $CGI->header('-type'=>'text/html', '-charset'=>'utf-8');
			print $html_template;
		}
		elsif ($params{'output'} eq 'page_content')
		{
			print $CGI->header('-type'=>'text/plain', '-charset'=>'utf-8');
			print $template_values{'PAGE_CONTENT'};
		}
		elsif ($params{'output'} eq 'plain')
		{
			print $CGI->header('-type'=>'text/plain', '-charset'=>'utf-8');
			print $output;
		}
		elsif ($params{'output'} eq 'xml')
		{
			print $CGI->header('-type'=>'text/plain', '-charset'=>'utf-8');
			print $output_xml;
		}
		elsif ($params{'output'} eq 'processed_xml')
		{
			print $CGI->header('-type'=>'text/html', '-charset'=>'utf-8');
			print XML::local_utils::xslt($xsl, $output_xml);
		}
		else
		{
			print $CGI->header('-type'=>'text/html', '-charset'=>'utf-8');
			print $output;
		}
		#warn "\n\n Done Processing Template \n" if ! DHSGlobals::PRODUCTION_SERVER;
	}	# end of if(! $skip_processing_html)
	
#	$params = XML::Simple::XMLout($data, RootName => "params" );
#	$messages_xml = XML::Simple::XMLout($messages, RootName => 'data', NoAttr => 1) if (ref($messages) eq 'HASH');

#};
if($@)
{
	my $url = $CGI->url('-full'=>1, '-query'=>1);
	my $remote_ip = $CGI->remote_addr();
	my $user_agent = $CGI->user_agent();
	my $referer = $CGI->referer();
	my $http_environment_variables = {};
	my $parameters = {};
	
	my $error = $@;
	
	
	foreach ($CGI->param())
	{
		$parameters->{$_} = $CGI->param($_);
	}
	
	foreach ($CGI->http())
	{
		 $http_environment_variables->{$_} = $CGI->http($_);
	}
	
	
	my %email = 
	(
		'from'=>'errors@dhs-club.com',
		'to'=>'errors@dhs-club.com',
		'subject'=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . " $FindBin::RealScript",
		'text'=>"A fatal error occured.  

Error: $error

URL: $url
Remote IP: $remote_ip
User Agent: $user_agent
Referring Site: $referer

HTTP Environment Variables: 

",
	);


	$email{'text'} .= Dumper($http_environment_variables);

	$email{'text'} .= "\n\n CGI->params: \n" . Dumper($parameters);



	
		
	use MailTools;
	my $MailTools = MailTools->new();
	$MailTools->sendTextEmail(\%email);
	
}


exitScript();

=head2
createMenuList

=head3
Description:

	


=head3
Params:

	results_arrayref
	ARRAYREF of HASHREFS
	
	
	proper_names_hashref
	HASHREF
	{
		lower case key => value
	}

=head3
Returns:

	ARRAY
	

=cut

sub createMenuList
{
	
	my $results_arrayref = shift;
	my $proper_names_hashref = shift;
	my $selected = shift;
	my $root_node_name = shift;
	my $node_name = shift;
	my $fix_name_case = shift;
	my %menu = ($node_name=>{});
	my @result_links = ();
	my $active = '';
	
	foreach (@$results_arrayref)
	{
		my @url_for_individual_location = ($control);
		# by placing a space in there we get the XMLout to render a proper <a> tag, otherwise it creates it as a self-closing tag and that is invalid XHTML
		my $url_name = ' ';
		
		foreach my $index (@fields_to_build_store_url)
		{
			last if ! $_->{$index};
			if (ref($proper_names_hashref) eq 'HASH' && exists $proper_names_hashref->{lc($_->{$index})})
			{
#				$url_name = $fix_name_case ? Lingua::EN::NameCase::NameCase($proper_names_hashref->{lc($_->{$index})}) : $proper_names_hashref->{lc($_->{$index})};
				$url_name = $proper_names_hashref->{lc($_->{$index})};
			}
			else
			{
				$url_name = $_->{$index . '_label'} ? $_->{$index . '_label'}: $_->{$index};
				
#				$url_name =  Lingua::EN::NameCase::NameCase($url_name) if $fix_name_case;

			}
			
			my $temp = $_->{$index};
			
			$active = 'selected' if ($selected && !$active && $selected eq $temp);
			
#			push @url_for_individual_location, $DHS_UrlSafe->encodeUrlPart($_->{$index});
			push @url_for_individual_location, $_->{$index};
		}
		
		
		my $url_for_location = '/' . join('/', @url_for_individual_location);
		
		$menu{$node_name}->{$url_name}->{'link'} = $url_for_location;
		$menu{$node_name}->{$url_name}->{'selected'} = $active if $active;

		#push @result_links, qq(<li $active><a href="/$url_for_location">$url_name</a></li>);
		
		$active = '';
		
	}
#print STDERR Dumper(\%menu);	
	my $return = $XML_Simple->XMLout(\%menu, 'NoAttr' => 1, 'RootName'=>$root_node_name);

# utf8 flag is on at this point
	return $return;
}

sub debug
{
	return unless $DEBUG;
	
	my $content = shift;
	my $action = shift || 'warn';
	
	if($action eq 'die')
	{
		die $content;
	}
	elsif($action eq 'warn')
	{
		warn $content;
	}
	else
	{
		die "Unrecognized debug type: $action\n" . $content;
	}
}

sub exitScript
{
	$db->disconnect if $db;
	exit;
}

sub LoadParams
{
	# this routine was not part of the original script and is being added to facilitate debugging at this time, but could be used throughout if desired
	# the key is the param name and the value is the default applied if there is no parameter value passed
	my %recognized = (
		'output'	=> ''
	);
	foreach (keys %recognized)
	{
		$params{$_} = $CGI->param($_);
		$params{$_} = $recognized{$_} unless defined $params{$_};
	}
}

sub ScreenDebug
{
	return unless $DEBUG;
	print $CGI->header('-charset'=>'utf-8','-type'=>'text/plain');
	print $_[0];
	exitScript();
}

__END__

	Date	Made By	Description of the Change
	03/21/11	Bill MacArthur	added $db->{'pg_enable_utf8'}=1 to fix the character encoding issues
	04/15/15	Bill MacArthur	several non-functional changes to mitigate undef errors
	11/12/15	Bill MacArthur	Disabled the generation of a page title and keywords when all it was was a list of all the countries.... ugh


