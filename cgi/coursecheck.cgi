#!/usr/bin/perl -w
# coursecheck.cgi
# originally written by:	Chris Handley
# Script to verify if the user has taken pre-requisite tests for this course
###### if so, slurp in the page and spit it out
###### last modified 12/17/02	Bill MacArthur

###### set this value to (1) to debug, otherwise = 0
my $debug = 1;

use DBI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use lib ('/home/httpd/cgi-lib');
use G_S qw( $LOGIN_URL );
use DB_Connect;

$| = 1;   # do not buffer error messages

my $maxtest = 0;
my (%FORM, %cookies, $db) = ();
my $id = '';

if (exists $ENV{'HTTP_COOKIE'}){ &getCookies() }
else {
	my $msg = qq|This application depends on cookies. None were received from your browser. 
<br />Please check your browser's cookie settings.<br />
Help can be found at the <a href=\"$LOGIN_URL\">login page</a>.\n|;
	&Err($msg); exit
}

&parse_form();

my $courseno = $FORM{'C'};
my $pagename = $FORM{'page'};
unless ($courseno && $pagename){ &Err('Missing parameters'); exit}

my $MTYPE = $cookies{'Vmtype'} || '';

($id) = &G_S::authen_ses_key( $cookies{'AuthCustom_Generic'} ) if ($cookies{'AuthCustom_Generic'});
###### a numeric ID should be returned if they have a valid cookie
###### if they are staff, it will be their username
###### we'll give staff free view of the tests
unless ( $MTYPE eq 'staff' || ($id && $id !~ /\D/) ){
	my $msg = "You must be logged in to use this function.<br><a href=\"$LOGIN_URL\">Click here</a> to login.";
	if ($debug){
		$msg .= "<br /><br />We have received the following cookies from you -<br />\n";
		foreach (keys %cookies){ $msg .= "$_ : $cookies{$_}<br />\n" if $cookies{$_}; }
		$msg .= "<br />The most significant cookie gives us the following as your ID -<br />\n";
		$msg .= "<br>USER = $id\n";
	}
	&Err($msg);
	exit;
}

if ($MTYPE eq 'p' and $courseno > 1) { 
	&Err("<h1>Message:</h1><p>You must upgrade to VIP to have access to all of the e-Business Institute");
	exit;
}

$pagename = $pagename . $courseno . '.html';

unless ($db = DB_Connect('ebiz')){exit}
$db->{RaiseError} = 1;

###### we'll give staff free access to all the tests
if ($MTYPE eq 'staff'){ $maxtest = 999 }
else{
###### we want to get some kind of result besides a NULL so we'll use coalesce
	my $sth = $db->prepare("SELECT COALESCE(MAX(testno), 0) FROM test_results WHERE id = '$id'");
	$sth->execute;
	$maxtest = $sth->fetchrow_array;
	$sth->finish;
}
if ($courseno <= ($maxtest + 1)){  
	unless (open(INF, "/home/httpd/html/vip/ebusiness/$pagename")){
		&Err('No test by that number or test template missing');
	}else{
		my @webp = <INF>;
		close(INF);
		print qq|Expires: Tue, 12 Mar 2002 00:00:00 GMT
Date: Tue, 12 Mar 2002 00:00:00 GMT
Content-Type: text/html; charset=ISO-8859-1\n\n|;
		print @webp;
	}
}else{  
	&Err("You cannot study course $courseno until you have passed all of the tests associated with earlier courses");
}

$db->disconnect;
exit;

sub Err{
	print qq|Expires: Tue, 12 Mar 2002 00:00:00 GMT
Date: Tue, 12 Mar 2002 00:00:00 GMT
Content-Type: text/html; charset=ISO-8859-1\n\n|;
	print qq|<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title></title>
</head><body bgcolor="#ffffff">\n|;

	print $_[0];
	print "<br /><br />The current server time (Pacific Time) is: " . scalar localtime();
	print "</body></html>\n";
}

sub getCookies {
        # cookies are separated by a semicolon and a space, this will split
        # them and return a hash of cookies
        my @rawCookies = split /; /, $ENV{'HTTP_COOKIE'};

        foreach(@rawCookies){
            my ($key, $val) = split /=/;
            $cookies{$key} = $val;
        } 
}

sub parse_form {
	my $buffer = '';
	if ($ENV{'REQUEST_METHOD'} eq 'POST'){
		read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	}
	elsif ($ENV{'REQUEST_METHOD'} eq 'GET'){
		$buffer = $ENV{QUERY_STRING};
	}
 
  my @pairs = split(/&/, $buffer);
   foreach (@pairs) {
      my ($name, $value) = split /=/;

      $value =~ tr/+/ /;
      $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

      $FORM{$name} = $value;
   }
}

###### 01/09/02 instituted 'use strict' and redesigned to use global DB connection module
###### also made all the courses available to all the staff
###### 01/17/02 made the application available to 'staff'
###### removed CGI.pm functionality in an attempt to circumvent the repeated problems that some VIPs
###### seem to encounter when using the script, also enabled detailed output of their cookies
###### for helping us debug what the trouble may be
###### 12/17/02 went to the 'use' method for custom libs/modules, also went to the AuthCustom_Generic cookie
