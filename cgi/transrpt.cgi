#!/usr/bin/perl -w
###### transrpt.cgi
###### Transaction Report for Referral Commissions & Training Bonuses
###### show the related transactions that generated the PP one is getting paid on
###### released: 02/09/12	Bill MacArthur
###### last modified: 07/16/14	Bill MacArthur

use strict;
use CGI;	
use CGI::Carp qw(fatalsToBrowser);
use Template;
use Encode;
use lib ('/home/httpd/cgi-lib');
use DB_Connect;
use G_S qw/$EXEC_BASE $LOGIN_URL/;
use XML::local_utils;
use Refcomp::Analysis;

#use Data::Dumper;	# for debugging only

my $REFCOMP_TBL = 'refcomp';
my %TMPL = (
	'TT' => {
		'rc'=>10946,
		'tb'=>10921,
		'tc'=>10997,
		'pt'=>10929,
		'ppp'=>10926,
		'xpp'=>10944,
		'frpp'=>10968,
		'powerpp'=>10973
	},
	'xml'=>10974
);

my %Links = (
	'compplan' => 'http://www.clubshop.com/manual/compensation/partner.html'
);

my $q = new CGI;
my $TT = Template->new();
my $RA = Refcomp::Analysis->new();
my %CachedVals = ();	# put things in here and reuse them instead of looking them up again

my ($db, $lang_blocks, %params, $USER_ID, $meminfo, $period, $mTbl) = ();
my $gs = G_S->new({'db'=>\$db});

###### for now we'll accept a simple member ID
###### and if it turns out to be a VIP, we'll require a login so as to protect
###### the privacy of the VIPs

my $ADMIN = AdminCK();
	
my $id = $q->cookie('id');
die "Bad ID cookie" if $id && $id =~ /\D/;

LoadParams();

my $rid = uc($params{'rid'}) || $q->cookie('id') || '';
unless ($rid)
{
#	Err('No ID number received');
	Err('You must be ' .
		$q->a({'-href'=>"$LOGIN_URL?destination=" . $q->script_name}, 'logged in') .
		' to use this report.');
	exit;
}

$db ||= DB_Connect::DB_Connect('transrpt') || exit;
$db->{'RaiseError'} = 1;

my $level_values = $db->selectall_hashref('SELECT * FROM level_values', 'lvalue');
my $rpte = $db->selectrow_hashref("
	SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.emailaddress, m.membertype,
		cbc.currency_code,
		cc.exponent AS currency_exponent
	FROM members m
	JOIN currency_by_country cbc
		ON cbc.country_code=m.country
	JOIN currency_codes cc
		ON cbc.currency_code=cc.code
	WHERE ${\($rid =~ m/\D/ ? 'm.alias':'m.id')} = ?", undef, $rid);

unless ($rpte)
{
	Err("No matching records for: $rid");
	Exit();
}

###### are they a valid membership?
unless ((grep $_ eq $rpte->{'membertype'}, qw/v/) || $ADMIN)
{
#	Err( $gs->ParseString($lang_blocks->{'no_rpt_available'}, $rpte->{'membertype'}) );
	Err( "Reports are unavailable for this membertype: $rpte->{'membertype'}" );
	Exit();
}
elsif ($rpte->{'membertype'} eq 'v' && ! $ADMIN)
{
	###### since this is a VIP we are reporting on we will do the following
	# make sure the user is logged in
	# make sure the user, if not the reportee, is upline
	( $USER_ID, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );
	if ($USER_ID =~ /\D/)
	{
		Err("A valid <a href=\"$LOGIN_URL\">login</a> is required to view Partner reports.");
		Exit();
	}
	
	###### make sure that the reportee is our downline
	if ($USER_ID != $rpte->{'id'} && ! $db->selectrow_array("
		SELECT network.access_ck1($rpte->{'id'}, $meminfo->{'id'})"))
#		SELECT id FROM my_team(?) WHERE id= ?", undef, $USER_ID, $rpte->{'id'}))
	{
#		Err( $gs->ParseString($lang_blocks->{'not_in_dn'}, $rid) );
		Err( "Your pay level or coaching position are insufficient to gain access to this information." );
		Exit();
	}
}

# we seem to need this everywhere, so why not just always pull it
$period = GetPeriod();

# although nobody ever mentioned it up until 07/16/14, we were using the live members table for the name and membertype data even when looking at months back
# by doing the following, we can refer to archived data if it exists, presenting a more accurate picture of what really happened back then
# literal_close will be null for the current period or any other non-existant period/schema matches
if ($period->{'literal_close'})
{
	my $schema = 'cgi_' . $period->{'close'};
	$schema =~ s/-/_/g;
	($mTbl) = $db->selectrow_array('SELECT nspname FROM pg_catalog.pg_namespace WHERE nspname = ?', undef, $schema);
	$mTbl .= '.members' if $mTbl;
#	warn '#'.$mTbl.'#';
}
$mTbl ||= 'members';
# as it turns out, our archive tables do not contain the alias, and so the use of this methodology has been restricted to certain report types
# where the SQL has been revised to join the live table as well to obtain the alias
# 12/04/14... actually the archived table is really only useful for membertype as proper names are not stored in the archived versions of the members table

if (grep $_ eq $params{'rptype'}, (qw/ppp rpp qpp fpp plpp gpp/))
{
	Output_3();
	Exit();
}
elsif ($params{'rptype'} eq 'fpp_rpp')
{
	Output_4();
	Exit();
}
elsif ($params{'rptype'} eq 'frpp')
{
	Output_5();
	Exit();
}
elsif ($params{'rptype'} eq 'powerpp')
{
	Output_6();
	Exit();
}
elsif ($params{'rptype'} eq 'rc' || $params{'rptype'} eq 'tb' || $params{'rptype'} eq 'tc')
{
	# do nothing... these are handled below, but we need to trap these here before we do the next evaluation
}
elsif ($params{'rptype'})
{
	die "Not yet configured for report type: $params{'rptype'}";
}

# we do not need currency formatting for our xPP reports, so we will only do this stuff down here
my $format_string = '%.' . $rpte->{'currency_exponent'} .'f';

# now get the exchange rate in effect for a given paid commission run,
# or today's if we are looking at current OR prior month data
if ($params{'month'})
{
	my $tmp = $db->selectrow_hashref('SELECT run_date, paid FROM archives.refcomp_me_dates WHERE commission_month=?',
		undef, $params{'month'}) || die "Failed to obtain a run date for: $params{'month'}";

	if ($tmp->{'paid'})
	{
		($rpte->{'rate'}) = $db->selectrow_array('SELECT "rate" FROM exchange_rate_by_date(?,?)',
			undef, $tmp->{'run_date'}, $rpte->{'currency_code'});
	}
}

# if we did not get a rate for a paid month, then get today's rate
($rpte->{'rate'}) ||= $db->selectrow_array('SELECT "rate" FROM exchange_rates_now WHERE "code"=?',
		undef, $rpte->{'currency_code'});

if ($params{'rptype'} eq 'rc' || $params{'rptype'} eq 'tb')
{
	Output_1();
}
elsif ($params{'rptype'} eq 'tc')
{
	Output_TC();
}
else
{
	Output_2();
}

Exit();


###### ###### Start of Sub Routines ###### ######

sub AdminCK
{
	my $operator = $q->cookie('operator');
	my $pwd = $q->cookie('pwd');
	return 0 unless $operator && $pwd;

	require '/home/httpd/cgi-lib/ADMIN_DB.lib';
	$db = ADMIN_DB::DB_Connect( 'transrpt', $operator, $pwd ) || die "Failed to connect to the DB with staff login";
	return $operator;
}

sub Err
{
	print $q->header('-expires'=>'now'),
		$q->start_html('-bgcolor'=>'#ffffff'),
		'<h4>There has been a problem</h4>',
		$q->h4($_[0]),
		scalar localtime(),
		$q->end_html();
}

sub Exit
{
	$db->disconnect if $db;
	exit;
}

sub GetData
{
	
	my (@rv, $comm_ttl, $dn_pay_ttl, $ttl, $qry, $potential_pay_ttl) = ();
	my $data = GetRefcomp();

#	my $xml = XML::LibXML::Simple::XMLin($data->{'analysis'}, 'KeyAttr'=>{'dn'=>'id'}, 'ForceArray'=>['dn']);
	$RA->init($data->{'analysis'});
#warn Dumper($xml);

=pod

	The XML looks like this as far as referral commissions go
	<m50>
		<dn id="4220435" fpp="39.00" rpp="0" actual_pay="19.50" frpp="477.00"/>
		<dn id="2630080" fpp="35.04" rpp="0" actual_pay="17.52" frpp="0"/>
		<dn id="5381455" fpp="28.00" rpp="0" actual_pay="14.00" frpp="0"/>....
	
	For the training bonus it looks like this:
	<m25>
		<dn id="5392151" fpp="40.00" rpp="0" potential_pay="10.00" actual_pay="10.00"/>
		<dn id="5357211" fpp="57.09" rpp="0" potential_pay="14.27" actual_pay="14.27"/>....
	
	This ends up looking like this:
	$VAR1 = {
	          'my25' => '1831.88',
	          'frontline_income' => '909.66',
	          'm50' => {
	                   'dn' => {
	                           '2630080' => {
	                                        'actual_pay' => '17.52',
	                                        'fpp' => '35.04',
	                                        'rpp' => '0'
	                                      },
	                           '5381455' => {
	                                        'actual_pay' => '14.00',
	                                        'ppp' => '28.00'
	                                        'rpp' => '0'
	                                      },...
	                   }
=cut

	my $key = $params{'rptype'} eq 'tb' ? 'm25' : 'm50';
	my $xml = $RA->dnNodeList($key);
	my ($transition) = $db->selectrow_array("
		SELECT CASE WHEN COALESCE(?::DATE, NOW()::DATE) < '2012-11-01' THEN FALSE ELSE TRUE END", undef, ($params{'month'} || undef));

	foreach (sort {$a<=>$b} keys %{ $xml })
	{
		my $tmp = $xml->{$_};
		my $local_fpp = 0;

		if (! $transition)
		{
			next unless $tmp->{'fpp'} || $tmp->{'rpp'};
			$local_fpp = $tmp->{'fpp'} + $tmp->{'rpp'};

		}
		else
		{
			next unless $tmp->{'ppp'} || $tmp->{'gpp'};
			$local_fpp = $tmp->{'ppp'} + $tmp->{'gpp'};
		}
		
		next unless $local_fpp > 0;
		
		$ttl += $local_fpp;
		$tmp->{'potential_pay'} = sprintf($format_string, (($tmp->{'potential_pay'} || 0) * $rpte->{'rate'}));
		$potential_pay_ttl += $tmp->{'potential_pay'};
		my $c = sprintf($format_string, (($tmp->{'actual_pay'} || 0) * $rpte->{'rate'}));
		$comm_ttl += $c;
		
		my $dn_pay = sprintf($format_string, ($local_fpp * 0.5 * $rpte->{'rate'}));
		$dn_pay_ttl += $dn_pay;
		
		push @rv, $db->selectrow_hashref("
			SELECT id, alias, firstname, lastname, ($local_fpp)::NUMERIC(8,2) AS fpp,
				$c AS commission,
				$dn_pay AS dn_pay,
				$tmp->{'potential_pay'} AS potential_pay
			FROM members m
			WHERE id= $_");
	}
	return \@rv, sprintf('%.2f', $ttl), $comm_ttl, $dn_pay_ttl, sprintf('%.2f', $potential_pay_ttl);
}

sub GetFPP_PPData
{
	my $period = GetPeriod();
	my @rv = ();
	my $sth = $db->prepare("
		SELECT
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			ta.period,
			(COALESCE(SUM(ta.fpp),0) + COALESCE(SUM(ta.rpp),0)) AS pp
		FROM ta_live ta
		JOIN transactions t
			ON t.trans_id=ta.transaction_ref
		JOIN $mTbl m
			ON m.id=t.id
		JOIN members ml
			ON ml.id=t.id
			
		WHERE ta.id= ?
		AND ta.period= $period->{'period'}
--		AND (ta.\$colname IS NOT NULL

		GROUP BY
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			ta.period
			
		ORDER BY pp DESC, m.id ASC
	");
	$sth->execute($rpte->{'id'});
	my $ttl = 0;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
		$ttl += $tmp->{'pp'};
	}

	return (\@rv, sprintf('%.2f', $ttl));
}

sub GetFrppData
{
	my @rv = ();
	my $ttl = 0;
	my $data = GetRefcomp();
	$RA->init($data->{'analysis'});
	my $xml = $RA->dnNodeListM50;
	foreach (sort {$a<=>$b} keys %{ $xml })
	{
		next unless $xml->{$_}->{'frpp'}; # no use adding in zeroes and creating line entries for them
		$ttl += $xml->{$_}->{'frpp'};
		push @rv, $db->selectrow_hashref("
			SELECT id, alias, firstname, lastname, ($xml->{$_}->{'frpp'})::NUMERIC(8,2) AS frpp
			FROM members
			WHERE id= $_");
	}
	
	return \@rv, sprintf('%.2f', $ttl);
}

sub GetMemberTypeCodes
{
	my %rv = ();
	my $sth = $db->prepare('SELECT "code", display_code FROM member_types');
	$sth->execute;
	while (my ($code, $dc) = $sth->fetchrow_array)
	{
		$rv{$code} = $dc;
	}
	return \%rv;
}

sub GetPeriod
{
	# if we got a "month" paramter, we can get the period for that
	# otherwise we will use the current period
	unless ($CachedVals{'period'})
	{
		my $date = $params{'month'} ? $db->quote($params{'month'}) : 'NOW()::DATE';
		$CachedVals{'period'} = $db->selectrow_hashref(qq'
			SELECT period, "open", COALESCE("close", NOW()::DATE) AS "close", "close" AS literal_close
			FROM periods
			WHERE "type"= 1
			AND $date BETWEEN open AND COALESCE("close", NOW()::DATE)') || die "Failed to obtain a period for: $date";
	}
	return $CachedVals{'period'};
}

# get the xPP data for those reports
sub GetPowerPPData
{
	my %rv = ();

	my $sth = $db->prepare("
		SELECT
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			ta.ppp AS pp,
			ta.raw_power_pp AS raw_power_pp,
			ta.multiplier,
			ta.cap
		FROM power_pp_vw ta
		JOIN transactions t
			ON t.trans_id=ta.transaction_ref
		JOIN $mTbl m
			ON m.id=t.id
		JOIN members ml
			ON ml.id=t.id
			
		WHERE ta.id= ?
		AND ta.period= $period->{'period'}
		AND ta.ppp <> 0
		ORDER BY pp DESC, m.id ASC
	");
	$sth->execute($rpte->{'id'});
	my %ttls = ();
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$ttls{'cap'} ||= $tmp->{'cap'};	# we set this once and then we're done
		push @{ $rv{$tmp->{'id'}} }, $tmp;
		$ttls{'pp'} += $tmp->{'pp'};
		$ttls{'raw_power_pp'} += $tmp->{'raw_power_pp'};
	}
	
	if (%rv)
	{
		# if we had some data, then the cap should be the same for all rows
		($ttls{'power_pp'}) = $db->selectrow_array("SELECT power_pp_credit($ttls{'pp'}, $ttls{'raw_power_pp'}, ($ttls{'cap'})::SMALLINT)");
	}
#die Dumper(\%rv);
	foreach my $k (keys %ttls)
	{
		$ttls{$k} = sprintf('%.2f', $ttls{$k});
	}
	return (\%rv, \%ttls);
}

# get the xPP data for those reports
sub GetPPData
{
	my $colname = shift;
	
	# since we are using this directly in SQL and we cannot use DB quote on it, we will validate the value
	die "Invalid colname: $colname" unless grep $_ eq $colname, (qw/ppp rpp qpp fpp plpp/);
	my $period = GetPeriod();
	
	my @rv = ();
	my $sth = $db->prepare("
		SELECT
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			ta.period,
			SUM(ta.$colname) AS pp
		FROM ta_live ta
		JOIN transactions t
			ON t.trans_id=ta.transaction_ref
		JOIN $mTbl m
			ON m.id=t.id
		JOIN members ml
			ON ml.id=t.id
			
		WHERE ta.id= ?
		AND ta.period= $period->{'period'}
		AND ta.$colname IS NOT NULL

		GROUP BY
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			ta.period
			
		ORDER BY pp DESC, m.id ASC
	");
	$sth->execute($rpte->{'id'});
	my $ttl = 0;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
		$ttl += $tmp->{'pp'};
	}

	return (\@rv, sprintf('%.2f', $ttl));
}

sub GetRefcomp
{
	return $db->selectrow_hashref("SELECT * FROM $REFCOMP_TBL WHERE id= ?", undef, $rpte->{'id'}) unless $params{'month'};

	my ($run_date) = $db->selectrow_array('
		SELECT MAX(run_date)
		FROM archives.refcomp_me_dates
		WHERE commission_month= ?', undef, $params{'month'});
	return () unless $run_date;
	
	return $db->selectrow_hashref("
		SELECT *
		FROM archives.refcomp_me_batches
		WHERE run_date='$run_date' AND id= ?", undef, $rpte->{'id'});
}

sub GetTcData
{
	
	my (@rv) = ();
	my $comm_ttl = 0;
	my $pp_basis_ttl = 0;
	my $data = GetRefcomp();

#	my $xml = XML::LibXML::Simple::XMLin($data->{'analysis'}, 'KeyAttr'=>{'dn'=>'id'}, 'ForceArray'=>['dn']);
	$RA->init($data->{'analysis'});

=pod

	For the Team Commissions XML data looks like this:
	<m25>
		<dn id="5552771" pp_basis="23.00" total_shares="248" my_shares="20" potential_pay="6.9" actual_pay="0.56"/>
		<dn id="5548912" pp_basis="17.00" total_shares="248" my_shares="20" potential_pay="5.1" actual_pay="0.41"/>
	
	This ends up looking like this:
	$VAR1 = {
	           '5375666' => {
	                        'rpp' => '',
	                        'fpp' => '',
	                        'pp_basis' => '37.50',
	                        'actual_pay' => '0.05',
	                        'ppp' => '',
	                        'gpp' => '',
	                        'potential_pay' => '11.25'
	                      },

=cut

	my $xml = $RA->dnNodeListM25();
#warn Dumper($xml);
	foreach (sort {$a<=>$b} keys %{ $xml })
	{
		my $tmp = $xml->{$_};
		$pp_basis_ttl += $tmp->{'pp_basis'};
#		my $c = sprintf($format_string, (($tmp->{'actual_pay'} || 0) * $rpte->{'rate'}));
		# because the actual pay is so low, we cannot convert it in many cases and have it add up as the other currency at the end
		my $c = sprintf('%.2f', ($tmp->{'actual_pay'} || 0));
		$comm_ttl += $c;
		
		push @rv, $db->selectrow_hashref("
			SELECT id, alias, firstname, lastname,
				$c AS commission,
				$tmp->{'pp_basis'} AS pp_basis
			FROM members
			WHERE id= $_");
	}
	return \@rv, sprintf($format_string, ($comm_ttl * $rpte->{'rate'})), $pp_basis_ttl;
}

sub GetTransData
{
	my @arglist = ();
	my $qry_cols = "
			m.id,
			ml.alias,
			ml.firstname,
			ml.lastname,
			m.membertype,
			t.amount,
			t.native_currency_amount,
			t.currency_code,
			t.description AS purchase_description,
			t.trans_date,
			t.entered,
			v.vendor_name,
			mal.location_name AS ma_location_name,
			tt.description AS tt_description
	";
	
	my $qry_from = '			
		FROM transactions t
		JOIN transaction_types tt
			ON tt.trans_type=t.trans_type
		JOIN $mTbl m
			ON m.id=t.id
		JOIN members ml
			ON ml.id=t.id
		LEFT JOIN vendors v
			ON v.vendor_id=t.vendor_id
		LEFT JOIN merchant_affiliate_locations mal
			ON mal.vendor_id=v.vendor_id
		LEFT JOIN ta_live ta
			ON ta.transaction_ref=t.trans_id
	';

	my $period = GetPeriod();
	my $qry_where = '';
	if ($params{'rptype'} eq 'pt')
	{
		$qry_cols = 'DISTINCT ' . $qry_cols;
		$qry_from .= "
			JOIN sales_requirement_items srs
				ON srs.trans_id=t.trans_id
		";
		
		$qry_where = "
			WHERE srs.id= ?
			AND srs.credit_date BETWEEN '$period->{'open'}' AND '$period->{'close'}' 
		";
		push @arglist, $rpte->{'id'};
	}
	elsif ($params{'rptype'} eq 'ppp')
	{
		$qry_cols .= ",ta.ppp AS pp";
		$qry_where = "
			WHERE ta.id= ?
			AND ta.ppp IS NOT NULL
			AND ta.period= $period->{'period'}
		";
		push @arglist, $rpte->{'id'};
	}
	
	my @rv = ();
	my $sth = $db->prepare("SELECT $qry_cols $qry_from $qry_where ORDER BY m.id, t.trans_date, t.entered");
	$sth->execute(@arglist);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	
	return \@rv;
}

sub LoadLanguagePack
{
	my $TmplData = shift;
#	my $xml = $gs->GetObject($TMPL{'xml'}) || die "Failed to load language pack: $TMPL{'xml'}";
	my $xml = $gs->GetObject(10974) || die "Failed to load language pack: $TMPL{'xml'}";
	if ($TmplData)
	{
		my $tmp = ();
		$TT->process(\$xml, $TmplData, \$tmp);
		$xml = $tmp;
	}
	# we don't always need this as the requirement for a language pack did not come around until 11/12... so why load the library everytime
	eval('use XML::LibXML::Simple;');
	return XMLin($xml, 'NoAttr'=>1);
}

sub LoadParams
{
	foreach (qw/rptype month rid hc/)	# hc: highlight column
	{
		$params{$_} = $q->param($_) || '';
	}

	$params{'rptype'} ||= 'rc';	# default to the referral commission report
}

sub Output_1
{
	my ($stats, $ttl, $comm_ttl, $dn_pay_ttl, $potential_pay_ttl) = GetData();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{$params{'rptype'}}) || die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'member'=>$rpte,
			'stats'=>$stats,
			'transaction_count'=>(scalar @{$stats}),
			'ttl'=>$ttl,
			'dn_pay_ttl'=>$dn_pay_ttl,
			'comm_ttl'=>$comm_ttl,
			'potential_pay_ttl'=>$potential_pay_ttl,
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

sub Output_2
{
	my ($stats) = GetTransData();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{$params{'rptype'}}) ||	die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'member'=>$rpte,
			'stats'=>$stats,
			'transaction_count'=>(scalar @{$stats}),
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

sub Output_3
{
	my $colname = uc $params{'rptype'};
	my ($stats, $ttl) = GetPPData($params{'rptype'});
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{'xpp'}) ||	die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'colname'=>$colname,
			'stats'=>$stats,
			'ttl'=>$ttl,
			'mtCodes'=>GetMemberTypeCodes()
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

sub Output_4
{
	my $colname = uc $params{'rptype'};
	my ($stats, $ttl) = GetFPP_PPData($params{'rptype'});
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{'xpp'}) ||	die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'colname'=>'RPP',
			'stats'=>$stats,
			'ttl'=>$ttl,
			'mtCodes'=>GetMemberTypeCodes()
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

# our Frontline RPP (FRPP) report

sub Output_5
{
	my ($stats, $ttl) = GetFrppData();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{$params{'rptype'}}) || die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'stats'=>$stats,
			'ttl'=>$ttl
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

sub Output_TC
{
	my ($stats, $comm_ttl, $pp_basis_ttl) = GetTcData();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{$params{'rptype'}}) || die "Failed to retrieve template for rptype: $params{'rptype'}\n";
	my $tmpl = ();
	
	my $rv = $TT->process(\$tmp,
		{
			'member'=>$rpte,
			'stats'=>$stats,
#			'transaction_count'=>(scalar @{$stats}),
			'comm_ttl'=>$comm_ttl,
			'me' => $q->script_name(),
			'params' => \%params,
			'user' => $meminfo
		}, \$tmpl);
	unless ($rv)
	{
		die $TT->error();
	}

	print $q->header('-charset'=>'utf-8'), $tmpl;
}

sub roundup
{
# we need to do this since perl rounds .105 down to .10, but Postgres rounds it to .11
# that 1 cent can make our math look bad sometimes
        my $val = shift;
        return 0 unless $val;
        my $h = chop $val;
        $val += .01 if $h >= 5;
        return $val;
}

sub Output_6
{
	my ($stats, $ttls) = GetPowerPPData();
	my $tmp = $gs->Get_Object($db, $TMPL{'TT'}->{'powerpp'}) ||	die "Failed to retrieve template for rptype: $params{'rptype'}\n";

	print $q->header('-charset'=>'utf-8');
	$TT->process(\$tmp,
		{
			'stats'=>$stats,
			'ttls'=>$ttls,
			'mtCodes'=>GetMemberTypeCodes(),
			'lang_blocks'=>LoadLanguagePack({'ttls'=>$ttls})
		}) ||
		die $TT->error();
}

sub SQL_rc
{
	return "
		SELECT
			m.id, m.alias, m.firstname, m.lastname,
			SUM(ta.npp) AS fpp,
			((SUM(ta.npp) * .5)::NUMERIC(7,2) * $rpte->{'rate'})::NUMERIC(7, $rpte->{'currency_exponent'}) AS commission
		
		FROM ta_live ta
		JOIN transactions t
			ON t.trans_id=ta.transaction_ref
		JOIN members m
			ON t.id=m.id
		WHERE ta.id= ?
		AND ta.period= ?
		AND ta.npp <> 0
		GROUP BY m.id, m.alias, m.firstname, m.lastname
		ORDER BY fpp DESC";
}

#sub SQL_tb
#{
#	return "
#		SELECT
#			m.id, m.alias, m.firstname, m.lastname,
#			SUM(ta.npp) AS fpp,
#			((SUM(ta.npp) * .25)::NUMERIC(7,2) * $rpte->{'rate'})::NUMERIC(7, $rpte->{'currency_exponent'}) AS commission
#			
#		FROM ta_live ta
#		JOIN members ms
#			ON ms.id=ta.id
#		JOIN transactions t
#			ON t.trans_id=ta.transaction_ref
#		JOIN $mTbl m
#			ON t.id=m.id
#		WHERE ms.spid= ?
#		AND ta.period= ?
#		AND ta.npp <> 0
#		GROUP BY m.id, m.alias, m.firstname, m.lastname
#		ORDER BY fpp DESC";
#}

__END__

02/29/12 had to revise the options to XMLin for the analysis data
		as it was not converting correctly if there was say only one node within a m25 node
05/02/12 Dick wants the membertype on some reports, so revised the SQL to obtain that
05/04/12 continuing with changes, made the rc report use a separate template as it had to be different :(
08/14/12
	Added handling for the frpp report with Output_5
	Moved to Refcomp::Analysis for data instead of using XML::LibXML::Simple against analysis
11/09/12
	Added handling for the new Power PP report
11/27/12
	Tweaked the downline pay calculation in GetData
02/12/13
	Added the report for Team Commissions Breakdown
07/16/14
	Provided a mechanism in GetPPData() to use an archived members table when viewing archived reports