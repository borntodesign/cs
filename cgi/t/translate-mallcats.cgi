#!/usr/bin/perl -w
###### translate-mallcats.cgi
###### an application to allow parties to translate the mall categories
###### created: 02/08	Bill MacArthur
###### this script was started from translation-editor.cgi by Karl Kohrt
###### last modified: 05/09/08	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
use DB_Connect;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

###### Globals
my ($db, %params, $list) = ();
our $q = new CGI;
our $ME = $q->url;
our $gs = new G_S;

exit unless my $user = LoginCK();

# A reg. ex. list of translation languages(2-letter codes) for this translator.
our $language_list = $db->selectrow_array("
	SELECT languages 
	FROM translators 
	WHERE name = '$user->{alias}'") || '';
# DC00001 was used up to this point for translator table simplification (only one record
#   for all admins). If this is an admin, get the operator for record keeping from here on.
$user->{alias} = $user->{name} if $user->{alias} eq 'DC00001';

LoadParams();

unless ($params{_action})
{
	Display_Page();
}
elsif ($params{_action} eq 'search')
{
	die "Either a keyword or a letter is required\n" unless $q->param('letter') || $q->param('keyword');
	CkRequired('language_code');

	Build_List();
	Display_Page();
}
elsif ($params{_action} eq 'update')
{
	CkRequired('language_code','catname','catname_translated');
	UpdateResource();
}
else
{
	Err('This undefined action.');
}

END:
$db->disconnect;
exit;

##########################################
###### Begin subroutines here.
##########################################

###### Build out the list for display.		
sub Build_List
{
	my @args = ($params{language_code}, $params{language_code});
	my $qry = "
		SELECT DISTINCT mc.catname, COALESCE(mct.catname,'') AS catname_translated
		FROM mallcats mc
		LEFT JOIN mallcats_translated mct ON mc.catid=mct.catid AND mct.language_code= ?
              LEFT JOIN (
			SELECT DISTINCT malls.language_code, vc.catid
			FROM malls
			INNER JOIN mall_vendors mv ON mv.mall_id=malls.mall_id
			INNER JOIN vendor_categories vc ON vc.vendor_id=mv.vendor_id
			WHERE -- malls.active=TRUE AND
				 malls.language_code= ?
		) AS tmp ON tmp.catid=mc.catid
		WHERE mc.catname ";
	###### category name criteria
	if ($params{letter}){
		$qry .= 'LIKE ? ';
		push @args, "$params{letter}\%";
	} else {
		$qry .= '~* ? ';
		push @args, $params{keyword};
	}
	###### active criteria
	if ($params{active} =~ /\d/){
		my $active = $params{active} == 1 ? 'NOT NULL' : 'NULL';
		$qry .= " AND tmp.catid IS $active\n";
	}
	###### translated? criteria
	if ($params{translated} =~ /\d/){
		my $t = $params{translated} == 1 ? 'NOT NULL' : 'NULL';
		$qry .= " AND mct.catname IS $t\n";
	}

	$qry .= "ORDER BY mc.catname\n";
	$qry .= "LIMIT $params{limit}\n" if $params{limit};
	$qry .= "OFFSET $params{offset}\n" if $params{offset};
	my $sth = $db->prepare($qry);
	$sth->execute(@args);
	my $class = 'a';
	my $frm = 1;
	while (my $tmp = $sth->fetchrow_hashref){
		$tmp->{catname} = $q->escapeHTML($tmp->{catname});
		$tmp->{catname_translated} = $q->escapeHTML($tmp->{catname_translated});
# 		$list .= qq|<tr class="$class">
# 			<td>$tmp->{catid}</td><td>$tmp->{catname}</td>
# 			<td><form action="#" id="f$tmp->{catid}">
# 				<input type="text" value="$tmp->{catname_translated}" />
# 			</form></td></tr>\n|;
		$list .= qq|<form action="#" id="f$frm" class="$class">
			<input type="text" class="catname" name="catname" readonly="readonly" value="$tmp->{catname}" />| .
			$q->textfield(
				-class=>'catname_translated',
				-name=>'catname_translated',
				-value=>$tmp->{catname_translated},
				-tabindex=>$frm) . qq|
			<input type="submit" value="OK" class="submit" />
			<input type="reset" value="R" class="submit"/></form>\n|;
#			<input type="image" src="/images/checkmark4" class="submit" /></form>\n|;

		$class = $class eq 'a' ? 'b':'a';
		$frm++;
	}
	unless ($list){
		$list = "No results returned: keyword=$params{keyword}, letter=$params{letter}, limit=$params{limit}, offset=$params{offset}\n";
	} else {
	#	$list = qq|<table id="list_tbl">$list</table>|;
		$list = qq|<div id="frmlist">$list</div>|;
	}
}

sub cboLanguage
{
	my (@vals, %labels) = ();
	my $qry ='SELECT code2, description FROM language_codes WHERE active=TRUE AND code2 ~ ? ';
	if ($language_list =~ /\*/){
		$qry .= "AND code2 != 'en' ";
	}
	$qry .= 'ORDER BY description';
	my $sth = $db->prepare($qry);
	$sth->execute($language_list);
	while (my ($code, $name) = $sth->fetchrow_array){
		push @vals, $code;
		$labels{$code} = $name;
	}
	return $q->popup_menu(
		-name=>'language_code',
		-values=>\@vals,
		-labels=>\%labels
	);
}

sub CkRequired
{
	foreach (@_){
		die "$_ is a required parameter\n" unless $params{$_} gt '';
	}
}

sub Err
{
	my $msg = shift || '';

	print 	$q->header(-expires=>'now', -charset=>'utf-8', -encoding=>'utf-8'),
		$q->start_html(-encoding=>'utf-8', -bgcolor=>'#ffffff', -title=>'Error'),
		"<br />$msg<b r/><br />\n", scalar localtime(),
		$q->end_html();
}

sub Display_Page
{
	print $q->header(-expires=>'now', -charset=>'utf-8'),
		_pageHead(),
		'<div id="flipflop"></div>',
		_searchForm(),
		###### these next items are the hidden form that will do the work of submitting translations
		$q->start_form(-action=>$q->url, -id=>'submit_form', -method=>'get'),
		$q->hidden(-name=>'language_code'),
		$q->hidden(-name=>'catname'), $q->hidden(-name=>'catname_translated'),
		$q->hidden(-name=>'_action', -value=>'update', -force=>1), $q->end_form;
	print $q->hr({-style=>'clear:both'}), $list;
	print $q->end_html;
}

sub LoadParams
{
	foreach(qw/_action language_code catname catname_translated active limit offset letter keyword translated/){
		$params{$_} = defined $q->param($_)? $q->param($_) : '';
	}
	###### some cursory checks
	for (grep {$params{$_} =~ /\D/} (qw/limit offset/)){
		die "Invalid numeric parameter: $params{$_}\n";
	}
}

sub LoginCK
{
	###### for now we will make sure they are a VIP and that they are in the
	###### translators table
	
	if ((my $operator = $q->cookie('operator')) && (my $pwd = $q->cookie('pwd')))
	{
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		return unless $db = ADMIN_DB::DB_Connect(
			'translation_editor',
			$operator,
			$pwd );
		$db->{RaiseError} = 1;

		return {
			id		=> 1,
			alias		=> 'DC00001',
			firstname	=> $operator,
			lastname	=> 'The DHS Club',
			name		=> $operator
		};
	}
	else
	{
		my($id, $realOP) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
		if ((! $id) || $id =~ /\D/ )
		{
			Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
			return;
		}

	###### since admin use is above, we will prohibit 01 use without it
	###### this will keep ex-employees who know the login credentials from
	###### being able to access this function
		if ($id == 1)
		{
			Err('To use this interface as 01, you must be logged into the database.');
			return;
		}

		return unless $db = DB_Connect('translation_editor');
		$db->{RaiseError} = 1;
	
		unless ($realOP->{membertype} eq 'v')
		{
			Err('This resource is for the exclusive use of VIPs.');
			return;
		}

		$id = $db->selectrow_array("
			SELECT id FROM translators WHERE name= '$realOP->{alias}'");
		unless ($id)
		{
			Err("We're sorry. You are not found on the list of translators.");
			return;
		}

		$realOP->{name} = "$realOP->{firstname} $realOP->{lastname}";
		return $realOP;
	}
}

sub UpdateResource
{
	###### first we will get every row that matches the submitted catname
	###### then we will update/insert for each one
	my $sth = $db->prepare('
		SELECT mc.catid, mct.language_code
		FROM mallcats mc
		LEFT JOIN mallcats_translated mct ON mc.catid=mct.catid AND mct.language_code=?
		WHERE mc.catname = ?');
	$sth->execute( $params{language_code}, $params{catname} );
	my $cnt = 0;
	while (my $tmp = $sth->fetchrow_hashref){
		if ($tmp->{language_code}){
			$cnt += $db->do("
				UPDATE mallcats_translated SET catname= ?
				WHERE catid= $tmp->{catid} AND language_code= '$tmp->{language_code}'",
				undef, $params{catname_translated});
		} else {
			$cnt += $db->do("
				INSERT INTO mallcats_translated (catid, catname, language_code)
				VALUES ($tmp->{catid}, ?, ?)", undef, ($params{catname_translated}, $params{language_code}));
		}
	}
	print $q->header('text/plain'), $cnt;
}

sub _pageHead
{
	return q|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Mall Category Translation</title>
<script src="/js/yui/yahoo-min.js" type="text/javascript"></script>
<script src="/js/yui/event-min.js" type="text/javascript"></script>
<script src="/js/yui/connection-min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/cat-translate.js"></script>
<style type="text/css">
#frmlist form {
	display:block;
	padding: 0;
	margin-bottom: .2em;
	vertical-align: middle;
}
#frmlist input {
	font-size: 75%;
	width: 20em;
}
#frmlist input.submit {
	width:2em;
	background-color: inherit;
	border-width: 1px;
}
#frmlist input.catname {
	background-color:inherit;
	border: 1px solid silver;
	padding-left: 0.1em;
}
form.a{ background-color: #eee; }
form.b{ background-color: rgb(255,255,240); }
#search_tbl {
	border: 1px inset silver;
	background-color: rgb(224,255,224);
	border-collapse: collapse;
}
#search_tbl td {
	font: normal 90% sans-serif;
}
td.ctl {
	padding-right: 1em;
	padding-left: 0.2em;
}
input.btn { font-size:70%; }
span.please_wait {
	margin-left: 1em;
	color: red;
	background-color:yellow;
	border: 1px solid #d80000;
	padding: 0.2em 0.4em;
}
span.update_success {
	margin-left: 1em;
	color: green;	
}
#divhelp {
	font: normal 70% sans-serif;
	border-left: 1px solid silver;
	border-bottom: 1px solid silver;
	float:right;
/*	margin-top:0;
	margin-right: 0;*/
}
body { margin-top: 0; margin-right:0; }
caption {
	font: bold 75% sans-serif;
	text-align: left;
	padding-bottom: 0.2em;
}
#search_form { float:left; }
td.search {
	background-color: rgb(240,255,240);
/*	padding: .2em .3em;*/

}
</style>
<script type="text/javascript">
function searchFrmSubmit(frm){
	if (frm.keyword.value=='' && frm.letter.selectedIndex==0){
		alert('Either a letter must be selected or a keyword submitted');
		return false;
	}
	return true;
}
</script>
</head>

<body onload="init();">
<div id="divhelp">Tip: Hit the "Enter" key to submit your words.
<br />The "R" button is a reset button to restore the field to it's original state.<br />
To actually record a reset value, you must submit it.</div>
|;
}

sub _searchForm
{
	return $q->start_form(-action=>$q->url, -id=>'search_form', -onsubmit=>'return searchFrmSubmit(this)'),
		$q->hidden(-name=>'_action', -value=>'search'), '<table id="search_tbl">',
		$q->caption('Mall Category Translation Interface'),
		'<tr><td colspan="5" style="font: normal 70% sans-serif; color: blue;">
			Select categories "Starting with" a letter or use a keyword.</td></tr>
			<tr><td class="search">Starting with:</td><td class="ctl search">',
		$q->popup_menu(
			-name=>'letter',
			-values=>['', 'A'..'Z'],
			-default=>'',
			-onchange=>"document.getElementsByName('keyword')[0].value=''"
		), '</td><td>Language:</td><td class="ctl">', cboLanguage(),
		'</td><td rowspan="3" class="ctl" valign="bottom">',
		$q->submit(-value=>'Submit', -class=>'btn'), ' ', $q->reset(-class=>'btn'),
		'</td></tr>',
		'<tr><td class="search">Keyword:</td><td class="ctl search">',
		$q->textfield(-name=>'keyword', -onchange=>"document.getElementsByName('letter')[0].selectedIndex=0"),
		'</td><td>Translated:</td><td class="ctl">',
		$q->popup_menu(
			-name=>'translated',
			-values=>[0,1,''],
			-default=>0,
			-labels=>{1=>'Yes', 0=>'No', ''=>'Both'}
		),'</td></tr><tr><td>Status:</td><td class="ctl">',
		$q->popup_menu(
			-name=>'active',
			-values=>[1,0,''],
			-default=>1,
			-labels=>{1=>'Active', 0=>'Inactive', ''=>'Both'}
		), '</td><td>Limit:</td><td class="ctl">',
		$q->popup_menu(
			-name=>'limit',
			-values=>[25,50,100,''],
			-labels=>{25=>'25', 50=>'50', 100=>'100', ''=>'All'},
			-default=>25
		), '</td></tr></table>',
		'</form>';
		
}





