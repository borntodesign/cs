#!/usr/bin/perl -w
###### translation_editor.cgi
###### an application to allow parties to edit documents
###### created: 09/02/04	Bill MacArthur
###### primary author: Karl Kohrt
###### last modified: 05/22/15	Bill MacArthur

use strict;
use XML::LibXML ();
use CGI::Pretty;
use Encode;
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
use DB_Connect;
require XML_Editor;	# that is a house package

binmode STDOUT, ":encoding(utf8)";

###### Globals
my $SEND_ERRORS_TO = 'l.young@dhs-club.com,webmaster@dhs-club.com';	###### if we bomb on saving, let us know
our $lock_period = "24 hours";	# An SQL interval representing the amount of time locked for editing().
our ($db) = ();
our $q = new CGI;
$q->charset('utf-8');
my $ME = $q->url;
my $gs = G_S->new({'db'=>\$db, 'CGI'=>$q});

our %TMPL = ( 'main'	=> 10350 );
our @result_fields = qw/pk full_lang stamp operator complete locked/;
our $result_header = qq#<tr>
	<td class="head">Resource Name</td>
	<td class="head">Preview</td>
	<td class="head">Language</td>
	<td class="head">Last modified:</td>
	<td class="head">By:</td>
	<td class="head">Complete:</td>
	<td class="head">Locked:</td></tr>#;
our $header = '';
our $message = '';
our $body = '';
our $otype = 'standard';	# The catchall object type for describing the editor to be used.

exit unless my $user = LoginCK();

# A reg. ex. list of translation languages(2-letter codes) for this translator.
our $language_list = $db->selectrow_array("
	SELECT languages 
	FROM translators 
	WHERE name = '$user->{'alias'}'") || '';
# DC00001 was used up to this point for translator table simplification (only one record
#   for all admins). If this is an admin, get the operator for record keeping from here on.
$user->{'alias'} = $user->{'name'} if $user->{'alias'} eq 'DC00001';

my $rid = $q->param('rid');
my $action = $q->param('action');

unless ($action)
{
	# If there is an $rid being passed, we'll try to unlock it.
	#  This happens when coming back to the list from an editing session.
	Unlock() if $rid;

	# Show the resource list.
	if ( $body = Build_List() )
	{
		$message= "<span class=\"b\">Click a Resource Name to open the editor.</span>";
	}
	else
	{
		$message= "	<span class=\"b\">No resources are available for translation at this time.</span>";
	}	
	Display_Page($message, $body);
}
elsif ($action eq 'edit')
{
	# Get the resource
	###### we'll die in the sub if we cannot find it
	my $resource = LockCK();
	
	# If the resource is not locked and not marked as complete.
	if (! $resource->{'complete'} && ! $resource->{'islocked'})
	{
		# Lock the resource.
		if ( Lock_Resource($resource) )
		{
			my $edit_part = '';
			my $xml_head = '';
			my $xml_foot = '';

			# Build the resource header info.
			$header = Build_Header($resource);
			
			# If this is a properly formatted XML resource.
			if ( $resource->{'resource_value'} =~ m/<lang_blocks>.*<\/lang_blocks>/sg )
			{
				# Save the header and footer info from the xml file if any exists.
				# We will remove it later in XML_Translate this resource and the English version.
				($xml_head = $resource->{'resource_value'}) =~ s/<lang_blocks>.*?$//sg;
				($xml_foot = $resource->{'resource_value'}) =~ s/^.*?(<\/lang_blocks>)//sg;

				# Build the xml editing interface.
				$edit_part = XML_Editor::XML_Edit($db, 'edit', $resource);
				$otype = 'xml';	# An xml object.
			}
			# If this is an non-XML resource or XML not properly formatted.
			else
			{
				# We just put the whole thing into a text area for editing.
				$edit_part = $q->textarea(
					-id=>'object',
					'-name'=>'object',
					'-value'=>$resource->{'resource_value'},
					-rows=>24,
					-cols=>120,
					-wrap=>'off')
			}
			
			# Build the editing interface.
			$body = Build_Editor($resource, $gs->Prepare_UTF8($edit_part), $xml_head, $xml_foot);
		}
		else
		{
			$body = Build_List();
			$message = $q->span({-class=>'r'},
				'The resource you selected can`t be locked for editing at this time.');
		}
		Display_Page($message, $body, $header);
	}
	# Otherwise, display the list of resources with a message.		
	else
	{
		ShowLockProblem($resource);
		$body = Build_List();
		Display_Page($message, $body);
	}	
}
elsif ($action eq 'update')
{
	my $resource = LockCK();
	# If the resource is not locked and not marked as complete.
	if ($resource->{'complete'} || $resource->{'islocked'})
	{
		ShowLockProblem($resource);
	}
	else
	{
		my $valid_file_format = 1;		# Default is a valid file format.
		# Load up all the params
		my @params = $q->param;
		my %param_hash = ();
		foreach (@params)
		{
			$param_hash{$_} = $q->param($_);
		###### filter out leading and trailing whitespace
			$param_hash{$_} =~ s/^\s*//;
			$param_hash{$_} =~ s/\s*$//;
			
			$param_hash{$_} = decode_utf8($param_hash{$_});
		}
	
		my $object = $param_hash{'object'} || '';
		$otype = $param_hash{'otype'} if $param_hash{'otype'};
	
		# If this is an xml editing session, we must parse the document first.
		if ( $otype eq 'xml' )
		{
			# The XML_Editor needs the RID in order to find the English version.
			$param_hash{'resource_identifier'} = Get_Resource_ID();

			# Parse the document.
			$object = XML_Editor::XML_Edit($db, 'update', \%param_hash);
	
			if ( $object =~ m/<lang_blocks><\/lang_blocks>/ )
			{
				$valid_file_format = 0;
				$message = "<span class=\"r\">No update completed. An empty object was returned from XML_Editor::XML_Edit.<br />\n</span>";
			}
	
			# Get and attach any header or footer info that was passed.
			my $xml_head = $param_hash{'xml_head'} || '';
			my $xml_foot = $param_hash{'xml_foot'} || '';
			$object = $xml_head . $object . $xml_foot;
	
			# Check the validity of the newly created xml document.
			my $xmlDom = undef;
			eval { $xmlDom = XML::LibXML->new()->parse_string($object); } if $valid_file_format;
	
	 		if ( $@ && $valid_file_format )
			{
				$valid_file_format = 0;
				$message = "<span class=\"r\">No update completed. The following created an invalid document.<br />\nError: $@<br />\n</span>";
			}
		}
	
		# Update the resource if it is a valid file format.
		$message = UpdateResource($object) if $valid_file_format;
	} ###### end of the update process on an incomplete, unlocked document

	# Show the resource list.
	$body = Build_List();
	Display_Page($message, $body);
}
elsif ($action eq 'preview')
{
	Display_Page( 'literal', Get_Resource_Value() );
}
else
{
	Err('This action is not currently allowed.');
}

END:
$db->disconnect;
exit;

##########################################
###### Begin subroutines here.
##########################################
sub Err
{
	my $msg = shift || '';

	print 	$q->header(-expires=>'now', -charset=>'utf-8', -encoding=>'utf-8'),
		$q->start_html(-encoding=>'utf-8', -bgcolor=>'#ffffff', -title=>'Error'),
		"<br />$msg<b r/><br />\n", scalar localtime(),
		$q->end_html();
}

###### Build out the list for display.		
sub Build_List
{
	my $list = '';
	my $result = '';
 	my $result_fields = '';
	# Retrieve the current list of resources in the queue based upon the language list.
	my $obj = $db->selectall_hashref("
		SELECT teq.*,
			lc.description AS full_lang,
			NOW()::timestamp(0) AS time_of_day
		FROM 	translation_edit_queue teq
		LEFT JOIN language_codes lc
		ON teq.language::VARCHAR = lc.code2::VARCHAR
		WHERE 	teq.language ~ '$language_list'
		ORDER BY teq.entered", 'pk');
	# If we have records to display.
	if ( $obj )
	{
		my $time_of_day = '';
		$list .= '<table border="1" cellspacing="0" cellpadding="3">';
		$list .= $result_header;

		# Display the various lines of data found above.
		foreach $result (sort {$obj->{$a}{language} cmp $obj->{$b}{language}} keys %{$obj} )
		{
			# The name of the item and the editing link.
			my $action_link = $q->td({-class=>'fp'},
				$q->a({-href=>"$ME?action=edit;rid=$obj->{$result}{pk}"},
					$obj->{$result}{name}));
												
			# The preview link.
			my $preview_link = $q->td({-class=>'fp'},
				$q->a({-href=>"$ME?action=preview;rid=$obj->{$result}{pk}",
					-onclick=>"window.open(this.href, 'preview');return false;"},
					'Preview'));
												
			# The rest of the fields.
			my $result_list = '';
			foreach ( @result_fields )
			{
				unless ( $_ eq 'pk' || $_ =~ m/value$/ )
				{
					if ( $_ eq 'complete' )
					{
						if ( $obj->{$result}{complete} eq '1' )
						{
							$obj->{$result}{complete} = 'Yes';
						}
						else 
						{
							$obj->{$result}{complete} = 'No';
						}
					}
					elsif ( $_ eq 'locked')
					{
						unless ( $obj->{$result}{locked} )
						{
							$obj->{$result}{locked} = "No";
						}
					}
					else
					{					
#if ($_ =~ m/lang/) {Err("$_= $obj->{$result}{$_}") }
						$obj->{$result}{$_} = CGI::escapeHTML($obj->{$result}{$_}) || '&nbsp;';
					}
					$result_list .= qq#<td CLASS="fp">$obj->{$result}{$_}</td>\n#;
				}
			}				
			$list .= "<tr>\n"
				. $action_link
				. $preview_link
				. $result_list
				. "</tr>\n";
			# Mark the time of day for the list.
			unless ( $time_of_day ) { $time_of_day = $obj->{$result}{time_of_day} }
		}
		$list .= "</table>";
		$list .= "<div align=\"right\">* This list was created on $time_of_day. All times are Pacific Time.</div>";
	}
	return $list;
}

###### Build the resource editor.		
sub Build_Editor
{
	my ($obj, $edit_part, $xml_head, $xml_foot) = @_;

	my $body = $q->start_form('-action'=>$q->script_name,
					'-method'=>'post')
		. $q->hidden('-name'=>'rid', '-value'=>$obj->{'pk'})
		. $q->hidden('-name'=>'otype', '-value'=>$otype, '-force'=>1)
		. $q->hidden('-name'=>'action', '-value'=>'update', '-force'=>1) . "\n"
		. $q->hidden('-name'=>'xml_head', '-value'=>$xml_head, '-force'=>1) . "\n"
		. $q->hidden('-name'=>'xml_foot', '-value'=>$xml_foot, '-force'=>1) . "\n"
		. $edit_part
		. $q->br
		. $q->submit('-value'=>'Update')
		. "&nbsp;&nbsp;"
		. $q->checkbox(
			'-value'=>'true',
			'-name'=>'complete',
			'-label'=>'Check here before clicking Update if this resource is complete.',
			'-force'=>1)
		. $q->end_form;

	return $body;
}

###### Build the standard resource header.		
sub Build_Header
{
	my $obj = shift;

	my $hdr = "<table><tr><td bgcolor=\"#ffffff\">Resource Name: $obj->{name}</td>\n"
		. "<td bgcolor=\"#ffffff\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n"
		. "<td bgcolor=\"#ffffff\">Last modified by: $obj->{operator}</td></tr>\n"
		. "<tr><td bgcolor=\"#eeeeee\">Resource ID: $obj->{pk}
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Resource Language: $obj->{full_lang}
						</td>\n"
		. "<td bgcolor=\"#eeeeee\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n"
		. "<td bgcolor=\"#eeeeee\">On: $obj->{stamp}</td></tr>\n"
		. "<tr><td bgcolor=\"#ffffff\">Current edit started at: $obj->{current_time} Pacific Time</td>\n"
		. "<td bgcolor=\"#ffffff\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n"
		. "<td bgcolor=\"#ffffff\">Update changes before: $obj->{lock_time} Pacific Time</td></tr>\n"
		. "<tr><td bgcolor=\"#ffffee\"><a href='$ME?rid=$rid'>Return to List</a></td></tr>\n"
		. "</table>\n"
		. "<hr />\n";

	return $hdr;
}

sub Display_Page
{
	my $message = shift || '';
	my $body = shift || '';
	my $header = shift || '';
	my $tmpl = '';
	my $type = 'text/html';	###### default header content type
	if ( $message eq 'literal' )
	{
		$tmpl = $body;
		###### for xml resources with a lang_blocks node,
		###### they only need to see what is in that node
		if ($tmpl =~ /<lang_blocks>/)
		{
			$type = 'text/xml';
			$tmpl =~ s#.*?(<lang_blocks>.*</lang_blocks>).*#$1#s;
		}
		###### are we looking at an XML document without lang_blocks?
		elsif ($tmpl =~ m#</# && $tmpl !~ /<html/)
		{
			$type = 'text/xml';
		}
		elsif ($tmpl !~ m#</#){ $type = 'text/plain' }
		###### we'll leave regular HTML alone
	}
	else
	{
		$tmpl = G_S::Get_Object($db, $TMPL{'main'}) || die "Couldn't open $TMPL{'main'}";

		$tmpl =~ s/%%message%%/$message/g;
		$tmpl =~ s/%%body%%/$body/g;
		$tmpl =~ s/%%header%%/$header/g;
		$tmpl =~ s/%%otype%%/$otype/g;

		# A couple of substitutions that need to be done for the xml editor.
		$tmpl =~ s/%%rid%%/$rid/g;
		$tmpl =~ s/%%ME%%/$ME/g;
	}

	# Display the page to the browser.
	print $q->header('-expires'=>'now',	'-type'=>$type, '-charset'=>'utf-8'),
		$tmpl;
}

###### Get the resource ID from the translation objects table.
sub Get_Resource_ID
{
	our $tmp = '';
	our $id_number = $q->param('rid');
	# Retrieve the current value from the translation queue.
	$tmp = $db->selectrow_array("
		SELECT resource_identifier
		FROM 	translation_edit_queue
		WHERE 	pk = $id_number") ||
		die "Cannot find the original Translation Resource: $id_number";

	return $tmp;
}

###### Get the resource value from the translation queue table.
sub Get_Resource_Value
{
	our $TMPL = '';
	our $id_number = $q->param('rid');
	# Retrieve the current value from the translation queue.
	$TMPL = $db->selectrow_array("
		SELECT resource_value
		FROM 	translation_edit_queue
		WHERE 	pk = $id_number") ||
		die "Cannot open the requested Translation Resource: $id_number";

	###### if we are showing a text page, we'll change some common textual items
	###### to their HTML equivalent
###### we're gonna try just detecting plain text in Display and then sending
###### the appropriate content header
# 	if ($TMPL !~ m#</|<br|<p|<body#)
# 	{
# 		$TMPL =~ s/  /&nbsp;&nbsp;/g;
# 		$TMPL =~ s/</&lt;/g;
# 		$TMPL =~ s/>/&gt;/g;
# 		$TMPL =~ s#\n#<br />#g;
# 	}
	return $TMPL;
}

# Get the resource if it is not locked by someone else.
sub LockCK
{
	# Retrieve the current info about the resource if this is the lock owner,
	#  the lock has expired, or there is no lock.
	my $rv = $db->selectrow_hashref("
		SELECT teq.*, lc.description AS full_lang,
			NOW()::timestamp(0) AS current_time,
			NOW()::timestamp(0) + INTERVAL '$lock_period' AS lock_time,
			CASE WHEN (
				teq.lock_owner= '$user->{'alias'}' OR
				teq.locked < (NOW() - INTERVAL '$lock_period') OR
				teq.locked IS NULL) THEN FALSE
				ELSE TRUE
			END AS islocked
		FROM 	translation_edit_queue teq
		LEFT JOIN language_codes lc
		ON teq.language::VARCHAR = lc.code2::VARCHAR
		WHERE 	teq.pk= ?", undef, $rid);
	die "Failed to find resource for: $rid\n" unless $rv;
	return $rv;
}

###### Lock the resource for editing.		
sub Lock_Resource
{
	my $obj = shift;

	# Lock the record for editing.
	my $rv = $db->do("
		UPDATE translation_edit_queue
		SET	locked = NOW(),
			lock_owner = '$user->{'alias'}'
		WHERE 	pk = $obj->{pk} AND (locked ISNULL OR lock_owner = '$user->{'alias'}')");
	return $rv;
}

sub LogAndAlert
{
	my $object = shift;
	my $filepath = '/home/httpd/html/errors/translation/';
	my $filename = time . '.txt';
	unless (open (OUT, ">$filepath$filename")){ return "Failed to open $filename for saving after DB update failure\n"; }
	else {
		print OUT "User: $user->{name}\n";
		print OUT "DB error: " . $db->errstr . "\n\n";
		print OUT "The content that failed to update:\n\n$object\n";
		close OUT;
		use Mail::Sendmail;
		my $rv = sendmail(
			To => $SEND_ERRORS_TO,
			From => 'translation_editor.cgi@www.clubshop.com',
			Subject => 'Transalation save failure',
			Message => "$user->{name} submitted a document that failed to update.\n" .
				"The document can be found in: $filepath$filename on the web server\n" .
				"This link should bring it up as well: " .
				"http://www.clubshop.com/errors/translation/$filename\n"
		);
		return ($rv ? "Admin has been notified" : '') . "\nDocument has been saved as $filename";
	}
}

sub LoginCK
{
	###### for now we will make sure they are a VIP and that they are in the
	###### translators table
	
	if ((my $operator = $q->cookie('operator')) && (my $pwd = $q->cookie('pwd')))
	{
		require '/home/httpd/cgi-lib/ADMIN_DB.lib';
		return unless $db = ADMIN_DB::DB_Connect(
			'translation_editor',
			$operator,
			$pwd );
		$db->{RaiseError} = 1;

		return {
			id		=> 1,
			alias		=> 'DC00001',
			firstname	=> $operator,
			lastname	=> 'The DHS Club',
			name		=> $operator
		};
	}
	else
	{
		my($id, $realOP) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### a valid id will not have anything besides digits
###### if it matches non-digits it may just be an expired cookie
###### once we rely on the AuthCookie only we will provide error handling at that time
###### if they don't have the cookie we'll just proceed to the other tests since empty strings will be passed
		if ((! $id) || $id =~ /\D/ )
		{
			Err("You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>");
			return;
		}

	###### since admin use is above, we will prohibit 01 use without it
	###### this will keep ex-employees who know the login credentials from
	###### being able to access this function
		if ($id == 1)
		{
			Err('To use this interface as 01, you must be logged into the database.');
			return;
		}

		return unless $db = DB_Connect('translation_editor');
		$db->{RaiseError} = 1;
	
		unless ($realOP->{membertype} eq 'v')
		{
			Err('This resource is for the exclusive use of VIPs.');
			return;
		}

		$id = $db->selectrow_array("
			SELECT id FROM translators WHERE name= '$realOP->{'alias'}'");
		unless ($id)
		{
			Err("We're sorry. You are not found on the list of translators.");
			return;
		}

		$realOP->{'name'} = "$realOP->{'firstname'} $realOP->{'lastname'}";
		return $realOP;
	}
}

sub mceFixup
{
	# use of the tinyMCE editor for full HTML documents introduces a few quirks
	# such as script tags being converted to mce:script tags
	# we need to reverse that stuff
	my $content = shift;
	$$content =~ s#(</*)mce:script#$1script#g;
}

sub SearchPage
{
	my $msg = shift || '';
					
	print $q->header('-expires'=>'now'),
		$q->start_html('-encoding'=>'utf-8', '-bgcolor'=>'#ffffff', '-title'=>"Translation Edit Queue Lookup"),
		$q->span({'-style'=>'color:#cc0000'}, $msg),
		$q->start_form('-action'=>$q->script_name, '-method'=>'get'),
		$q->hidden('-name'=>'action', '-value'=>'edit', '-force'=>1),
		"<h3>Translation Edit Queue Lookup</h3>",
		"Enter the Resource ID\n",
		$q->textfield('-name'=>'rid'), $q->submit, $q->end_form;

#	print "<br /><br />$AddNew\n" if $user->{id} eq '1';

	print $q->br, $q->br, scalar localtime(),
		$q->end_html;
}

sub ShowLockProblem
{
	my $resource = shift;
	$message = "<span class=\"r\">The resource you selected is ";
	if ( $resource->{complete} ) { $message .= "marked complete at this time.</span>" }
	else { $message .= "locked by another translator at this time: $resource->{lock_owner}</span>" }
}

# Unlock the queue resource if possible.
sub Unlock
{
	my $rv = $db->do("
		UPDATE translation_edit_queue SET
			locked = NULL,					
			lock_owner = NULL
		WHERE 	pk= $rid
		AND 	lock_owner= '$user->{'alias'}'				
		AND 	locked > NOW() - INTERVAL '$lock_period'") ;
}

sub UpdateResource
{
	my $object = shift;
	mceFixup(\$object);

	my $COMPLETE = $q->param('complete') ? 'TRUE' : 'FALSE';
	###### if it has been marked complete then it will no longer be locked
	###### otherwise we will update the lock
	my $LOCKED = $q->param('complete') ? 'NULL' : 'NOW()';
	my $LOCKO = $q->param('complete') ? 'NULL' : $db->quote($user->{'alias'});
	return "<span class=\"r\">Resource Value is empty or undefined.</span>" unless $object;
	return "<span class=\"r\">Resource ID is empty or undefined.</span>" unless $rid;

#	Stopped this on 03/27/06 due to a Greek translation having been submitted in escaped HTML characters.
# 	We'll deal with instances of these in the editor or production as they appear from now on.
#	$object =~ s/&#(\d*);/$1/g;
	# Convert the resource value to Unicode.
#	$object =~ s/&#(\d*);/Unicode_Convert($1)/ge;
#return "Not updating while testing";
	my $rv = $db->do("
		UPDATE translation_edit_queue SET
			resource_value= ?,
			complete= $COMPLETE,
			stamp= NOW(),
			locked = $LOCKED,					
			lock_owner = $LOCKO,					
			operator= '$user->{'alias'}'
		WHERE 	pk= $rid", undef, $object);

	my $msg = '';
	if ( $rv eq '1' )
	{
		$msg = "<span class=\"b\">Update successful.</span>";
	}
	else
	{
		$msg = LogAndAlert($object) || '';

	   $msg = $q->span({'-class'=>'r', 'style'=>'font-weight:bold'}, $db->errstr . qq|
		$msg
		The update was not successful!<br />
		Press the 'Back' button and save your changes in Notepad (or a similar program) until you have discovered the reason.
		|) .
		q|<script type="text/javascript">alert('The update was NOT successful!')</script>|;
	}
	return $msg;
}

__END__

###### 11/22/04 added the utf-8 charset to the display http header
###### 12/01/04 Added change of $user->{'alias'} to $user->{name} after language retrieval
###### 	if DC00001, so that we can see which admin made changes to a resource.
###### 12/06/04 Added the 'Preview' link to each row.
###### 03/04/05 Moved Lock_Resource out of Build_Resource in preparation for xml handling.
###### 03/17/05 Added the calls to XML_Editor::XML_Edit in the case of xml docs with the proper
######		<lang_blocks> formatting.
###### 03/18/05 Added a check for an empty $object being returned from XML_Editor::XML_Edit
######		prior to updating the resource in the queue.
###### 07/07/05 Added sub Get_Resource_ID for processing the updates using the newly modified
######		XML_Editor::XML_Edit module.
###### 01/25/06 Commented out ConvHashref references and sub Unicode_Convert
###### 03/27/06 Commented out the removal of escaped HTML character sequences (&#...;)
###### 04/27/06 added filtering of leading and trailing whitespace from params
# 07/21/06 removed the reset button from the interface
###### also made some other HTML tweaks and fixes as well as changing the preview stuff
###### so as to render xml and text documents better
###### 07/03/07 add capture of a submitted document if the DB update failed
###### also created a notification of the same to 'admin'
###### 07/16/07 revised the lock handling stuff and the update SQL
###### 09/02/09 added fixups for mce editor quirks
###### 08/20/13 Somewhere along the line we started getting goobered up characters in the DB... so had to do decode_utf8() on the params as they were coming in
05/22/15	Added Prepare_UTF8() to the value being passed into Build_Editor