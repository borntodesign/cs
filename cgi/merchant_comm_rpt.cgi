#!/usr/bin/perl -w
###### merchant_comm_rpt.cgi
###### started as a replica of the member_ppp_rpt
###### Display a merchant's referral commissions
###### The script is driven by a Member ID which is found in the login cookie.
###### 
###### Original script Created: 10/13/2003	Karl Kohrt
###### 
###### this version created: 11/24/09 Bill MacArthur
###### last modified: 03/01/14	Bill MacArthur

use lib ('/home/httpd/cgi-lib');

use CGI;
use strict;
use CGI::Carp qw(fatalsToBrowser);
use DB_Connect;
use G_S qw($LOGIN_URL);
use Template;
use XML::Simple;

###### GLOBALS
my $TT = Template->new();
my $q = new CGI;

###### our page templates
our %TMPL = (
	'rpt' => 10756,
	'xml' => 10757);

our $member_id = '';
our $alias = '';
our $member_info = '';
my $db = DB_Connect('merchant_comm_rpt.cgi') || exit;
$db->{'RaiseError'} = 1;
my $gs = G_S->new({'db'=>$db});

my $xml = $gs->Get_Object($db, $TMPL{xml}) || die "Couldn't open $TMPL{xml}";
$xml = XMLin( $xml );

################################
###### MAIN starts here.
################################

# Get and test the cookie information for access.
Get_Cookie();	

Report();
End();

################################
###### Subroutines start here.
################################

###### Create a combo box based upon the parmeters passed in.
sub Create_Combo
{
	my $def = shift;
	my $data = '';
	my @list = ();
	my %labels = ();

	# at this point we only want periods which potentially still contain data in ta_live
	my $qry = "SELECT period, open, close
			FROM 	periods
			WHERE
				open BETWEEN (SELECT open FROM oldest_available_ta_period) AND NOW()
			AND 	type=1
			ORDER BY open DESC";
	my $sth = $db->prepare($qry);
	$sth->execute();

	while ( $data  = $sth->fetchrow_hashref )
	{
		push (@list, $data->{period});
		$data->{close} ||= "current";
		$labels{$data->{period}} = "$data->{open} to $data->{close}";
	}
	$sth->finish;
	return $q->popup_menu(
		-name => 'period',
		-values	=> \@list,
		-labels	=> \%labels,
		-default => $def,
		-onChange => 'document.forms[0].submit()');
}

sub End
{
	$db->disconnect if $db;
	exit;
}

##### prints an error message to the browser.
sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff', -title=>'Error');
	print $q->div($_[0]);
	print $q->end_html();
	End();
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	# Get the member Login ID and info hash.
	( $member_id, $member_info ) = $gs->authen_ses_key( $q->cookie('AuthCustom_Generic') );

	# If the Login ID doesn't exist or has any non-digits send them to login again.
	if (!$member_id || $member_id =~ /\D/)
	{
		RedirectLogin();
		End();
	}
	elsif ($member_info->{membertype} ne 'ma')
	{
		Err('This report is only available to logged in merchant affiliate memberships');
	}
}

###### Display the transaction report for the requested period.
sub Report
{
	my $qry = '';
	my $sth = ();
#	my $one_row = '';
	my $report_body = '';
	my %totals = (commission=>0, purchase_totals=>0);
	my $ppp_total = 0;
	my $year = '';
	my $period = '';
	my %data = (
		report=>[],
		totals=>{
			commission=>0,
			purchase_totals=>0},
		xml=> $xml,
		script_name=> $q->script_name
	); 

	# Get the period that was passed or default to the current period.
	unless ( $period = $q->param('period') )
	{
		$qry = "SELECT period
			FROM periods
			WHERE period = get_working_period(1,NOW()) and type=1;";
		$sth = $db->prepare($qry);
		$sth->execute();
		$period  = $sth->fetchrow_array;
	}
	$data{periods} = Create_Combo($period);

	# Prepare a query to get detailed transactions for the period requested.
	$qry = "SELECT m.alias,
				m.firstname,
				m.lastname,
				--COALESCE(mal.location_name,v.vendor_name) AS merchant_name,
				SUM(t.amount) AS purchase_total,
				SUM(ta.commission) AS commission

			FROM ta_live ta
			JOIN transactions t
				ON t.trans_id=ta.transaction_ref
			JOIN members m
				ON m.id=t.id
			JOIN vendors v
				ON v.vendor_id=t.vendor_id
			LEFT JOIN merchant_affiliate_locations mal
				ON mal.vendor_id=v.vendor_id

			WHERE ta.period= ?
			AND ta.id= ?
			AND ta.commission IS NOT NULL
			GROUP BY
					m.alias,
					m.firstname,
					m.lastname
					--,merchant_name

			ORDER BY
					m.alias";

	$sth = $db->prepare($qry);

	# Get the member's transactions for the period specified.
	$sth->execute( $period, $member_id);
	while ( my $tmp = $sth->fetchrow_hashref)
	{
		push @{$data{report}}, $tmp;

		# Add the individual amounts to the aggregate totals.
		$totals{purchase_totals} += $tmp->{purchase_total} || 0;
		$totals{commission} += $tmp->{commission} || 0;
	}		

	$sth->finish();

	my $tmpl = $gs->Get_Object($db, $TMPL{rpt}) || die "Couldn't open $TMPL{rpt}";

	$totals{purchase_totals} = sprintf("%.2f", $totals{purchase_totals});
	$totals{commission} = sprintf("%.2f", $totals{commission});

	my $rv = ();
	$TT->process(\$tmpl, \%data, \$rv);
	print $q->header(-charset=>'utf-8'), $rv;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $me = $q->script_name;
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff');
	print "Please login <a href=\"$LOGIN_URL?destination=$me\">HERE</a>.<br /><br />";
	print scalar localtime(), "(PST)";
	print $q->end_html;
}

# 03/01/14 changed the way the XML template was pulled in a processed