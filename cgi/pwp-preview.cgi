#!/usr/bin/perl -w
# pwp-preview.cgi (the Personal Web Page) previewer
# this frontend is similar to the pwp script for glocalgeneration
# this one works of the PK instead of the page_name however, and includes other tweaks
# created 01/08	Bill MacArthur

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use lib '/home/httpd/cgi-lib';
use DB_Connect;
require pwp_presentation;

my $q = new CGI;

my $pk = $q->param('pk') || die "Failed to receive pk param\n";
die "Invalid pk param\n" if $pk =~ /\D/;

###### make sure we are at least admin or a logged VIP for previewing
###### this will keep them from advertising these pages if they are not approved
if (my $login = LoginCK())
{
	my $db = DB_Connect('pwp') || exit;
	$db->{RaiseError} = 1;

	###### admin login returns '2' and we may want to refer to a voided entry
	###### so we'll only disallow voids to VIPs
	my $xc = $login == 1 ? 'void=FALSE AND' : '';

	my ($page) = $db->selectrow_hashref("
		SELECT ipp.*, EXTRACT(epoch FROM ipp.stamp)::INT AS perltime FROM pwp_pages ipp
		WHERE $xc pk= ?", undef, $pk) ||
		die "Failed to find a matching record for pk: $pk\n";

	my ($member) = $db->selectrow_hashref('
		SELECT id, alias FROM members WHERE id= ?', undef, $page->{id});

###### if we wanted to put access controls in, this would be a good spot

	pwp_presentation::Render($db, $q, $page, $member);
	$db->disconnect;
}
exit;

sub LoginCK
{
	my ( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

	return 1 if $id && $id !~ /\D/ && $meminfo->{membertype} eq 'v';

	###### admin can use this thing too
	###### this is not the real deal, but this is not a very important interface either
	return 2 if $q->cookie('operator') && $q->cookie('pwd');

	print 	$q->header(-expires=>'now'),
		$q->start_html(-bgcolor=>'#ffffff'),
		$q->h3('This facility requires SSL and VIP login'),
		$q->end_html();

	return undef;
}

