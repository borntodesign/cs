package FreePartnerStatus;
use strict;
# created 03/27/14	Bill MacArthur

=head1 FreePartnerStatus

A class for managing the "Free Partner Status" of memberships.

=head2 new()

Create a new instance for a given membership.

=head3 Arguments

Arguments should come in as a hashref. These keys are currently recognized and required:
	'db'=>an active DBH
	'id'=>the member ID of the party we are working with

=head3 Returns

A new instance of FreePartnerStatus if successful. Otherwise, undef (like if instantiated without the required arguments)

=cut

sub new
{
	my ($class, $args) = @_;
	unless ($args->{'db'} && $args->{'id'})
	{
		warn "The argument key/value pairs must have at least a valid DB handle on 'db' and an integer ID on 'id'";
		return undef;
	}
	
	_loadCurrentRecord($args);
	return bless $args, $class;
}

=head2 createFPS

Create a new free_partner_status record -if- one does not already exist.

=head3 Arguments

A hashref with the following keys
	Required:
		'expiration'=>the date this freebie should expire
	
	Optional:
		'notes'=>notes about this record
		'operator'=>who should be recorded as the record creator (default will be current_user)

=head3 Returns

True if record creation was successful, False otherwise.
If unsuccessful, a warning will be thrown and a textual error will be set that can be retrieved on ->{'errorStr'}

=cut

sub createFPS
{
	my ($self, $args) = @_;
	$self->_resetErrStr();
	
	# first off, let's see if we have an active record already
	if ($self->isFree())
	{
		$self->{'errorStr'} = 'This membership is already at free status';
		warn $self->{'errorStr'};
	}
	elsif (! $args->{'expiration'})
	{
		$self->{'errorStr'} = 'An expiration date is required to create a new record';
		warn $self->{'errorStr'};
	}
	
	return 0 if $self->{'errorStr'};
	
	my $op = $self->_setOperator($args->{'operator'});
	my $rv = $self->{'db'}->do("INSERT INTO network.free_partner_status (id, expiration, operator, notes) VALUES (?,?, $op, ?)", undef, $self->{'id'}, $args->{'expiration'}, $args->{'notes'});
	
	# now load the new record into our active record
	$self->_loadCurrentRecord();
	return $rv;
}

=head2 createFreePartner

"Upgrade" a membership to Partner and create a free_partner_status record to go along with it.
If the membership is already at Partner, then the membership will remain untouched and only a free_partner_status record creation will be attempted.

=head3 Arguments

An hashref with these keys:
	Required:
		'expiration'=>the date when the freebie is over
		
	Optional
		'notes'=>notes about this transaction
		'operator'=>who is initiating this

=head3 Returns

A myRecord hash or undef if unsuccessful.
If unsuccessful, ->{'errorStr'} will be set.

=cut

sub createFreePartner
{
	my ($self, $args) = @_;
	$self->_resetErrStr();
	
	unless ($args && $args->{'expiration'}) {
		$self->_setErrAndWarn('An expiration date must be passed in on the arguments hashref');
		return undef;
	}
	
	$args->{'notes'} ||= 'Action performed by FreePartnerStatus->createFreePartner';

	my ($ck, $xmtype) = $self->{'db'}->selectrow_array('
		SELECT mt.can_upgrade_to_v, mt.code
		FROM members m
		JOIN member_types mt
			ON mt.code=m.membertype
		WHERE m.id= ?', undef, $self->{'id'});

	my $dbstate = $self->{'db'}->{'RaiseError'};	# just in case it is not set to 1, we'll save it
	$self->{'db'}->{'RaiseError'} = 0;
	$self->{'db'}->begin_work;

	my $rv = 0;
	my $localerr = '';
	if ($ck) {
		my $updateSQL = "UPDATE members SET vip_date=NOW(), status=1, membertype='v', ";
		$updateSQL .= "memo= " . $self->{'db'}->quote($args->{'notes'}) . ', ';
		$updateSQL .= 'operator= ' . $self->_setOperator($args->{'operator'}) . ' ';
		$updateSQL .= 'WHERE id= ?';
		
		$rv = $self->{'db'}->do($updateSQL, undef, $self->{'id'});	# at this point, we are resetting $rv to either true or false
		
		# various scenarios exist
		# first off, if I was already a Partner, I am not down in this code path because $ck is null, so there is nothing that we need to do in that case
		# 1. If I was a TP, I have not moved in network position
		# 2. If I was down under a pool, I have been moved out into the bottom of the TP line, with the pool alongside me
		# 3. If I was not in a pool, then I need to be moved to the taproot above the pool
		# cases 2 & 3 are addressed simply by network.move_to_taproot_above_pool
		if ($ck eq 'tp') {
			# nothing to do
		} else {
			# is my sponsor in the TP line?
			my $rv = $self->{'db'}->selectrow_array('SELECT network.move_to_taproot_above_pool(?)', undef, $self->{'id'});
			$self->_setErrAndWarn('Failed to move the new Partner to the taproot above the pool: ' . $self->{'db'}->errstr);
		}
	} elsif ($xmtype eq 'v') {
		$rv = 1;
	} else {
		$localerr = "$self->{'id'} is neither a Partner nor a membertype that can be upgraded to Partner";
	}

	if ($rv) {
		$rv = $self->createFPS($args);
		if ($rv) {
			$self->{'db'}->commit;
			$self->{'db'}->{'RaiseError'} = $dbstate;
			$self->_loadCurrentRecord();
			return $self->myRecord();
		}
	}
	# apparently something went wrong
	$self->_setErrAndWarn($localerr || $self->{'db'}->errstr);
	$self->{'db'}->rollback;
	$self->{'db'}->{'RaiseError'} = $dbstate;
	return undef;
}

=head2 Expiration

A simple method call that returns the expiration date of the current record, or an empty string if a record does not exist.

=cut

sub Expiration
{
	my $self = shift;
	return $self->{'active_record'}->{'expiration'} || '';
}

=head2 Expire

Make the current record expire as of now() or as of an arbitrary date

=head3 Arguments

An optional hashref with these optional keys:
	'expiration'=>a valid date in the format of YYYY-MM-DD (in the absence of a value, this will default to NOW())
	'operator'=>who is doing this (defaults to "current_user"())
	'notes'=>notes about why this is happening

=head3 Returns

True upon success, False otherwise.

=cut

sub Expire
{
	my $self = shift;
	my $args = shift;
	$args->{'expiration'} = $self->_validateOrSetExpirationDate($args->{'expiration'});
	return $self->_update($args);
}

=head2 Extend

Make the current record expire as of an arbitrary date (presumably beyond where it is already at)

=head3 Arguments

A hashref with these keys:
	Required:
		'expiration'=>a valid date in the format of YYYY-MM-DD
		
	Optional:
		'operator'=>who is doing this (defaults to "current_user"())
		'notes'=>notes about why this is happening

=head3 Returns

True upon success, False otherwise.

=cut

sub Extend
{
	my $self = shift;
	my $args = shift;
	$self->_resetErrStr();
	
	unless ($args->{'expiration'}) {
		$self->_setErrAndWarn('An expiration date is required to extend a record');
	} elsif (! $self->_validateExpirationDate($args->{'expiration'})) {
		# errorStr has already been set
	} elsif (! $self->{'active_record'}->{'pk'}) {
		$self->_setErrAndWarn('An active record does not exist to extend');
	}
	
	return 0 if $self->{'errorStr'};
	
	return $self->_update($args);
}

=head2 historyRecIsCurrent

Tell me if this record is the same as my "current" record.

=head3 Arguments

A hashref containing a record of the type retured by myHistory()

=head4 Returns

If the pk and the expiration dates are the same, then returns True, otherwise False

=cut

sub historyRecIsCurrent
{
	my ($self, $rec) = @_;
	
	# if our "current" record is empty, then we cannot evaluate anything
	if (! ($self->{'active_record'}->{'expiration'} && $self->{'active_record'}->{'pk'})) {
		return 0;
	}
	elsif (! ($rec->{'expiration'} && $rec->{'pk'})) {
		$self->_setErrAndWarn('The record passed into historyRecIsCurrent seems to be incomplete. It should contain both a pk and expiration.');
		return 0;
	}
	
	return 1 if $rec->{'expiration'} eq $self->{'active_record'}->{'expiration'} && $rec->{'pk'} == $self->{'active_record'}->{'pk'};
	return 0;
}

=head2 myHistory

A simple method to return a set of free_partner_status records pertaining to this individual.

=head3 Arguments

None

=head3 Returns

An array reference with each element of the array being a hashref of free_partner_status records.
These will be sorted from oldest to newest.
The last record will contain the same data that can be obtained by calling ->myRecord, with the exception of the _active flag which is created for ->myRecord
If there are no records, the array reference will be empty.

=cut

sub myHistory
{
	my $self = shift;
	my @rv = ();
	my $sth = $self->{'db'}->prepare('
		SELECT *,
			CASE WHEN expiration < NOW()::DATE THEN FALSE::BOOLEAN ELSE TRUE::BOOLEAN END AS _active,
			stamp::TIMESTAMP(0) AS _stamp
		FROM network.free_partner_status WHERE id= ? ORDER BY expiration');
	$sth->execute($self->{'id'});
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	return \@rv;
}

=head2 myRecord

A simple method that returns the current record. If there is no current record, the result is undef.
This record should not be confused with the concept that they are actively a free partner,
but it merely means that this is the most recent record on file and the one that will be acted upon if any action methods are invoked.

=cut

sub myRecord
{
	my $self = shift;
	return $self->{'active_record'};
}

=head2 isFree

Just a simple method that returns True(1) if the member has a free Partner status, False(0) otherwise;

=head3 Arguments

None

=cut

sub isFree
{
	my $self = shift;
	return $self->{'active_record'}->{'_active'} ? 1:0;
}

=head2 _loadCurrentRecord

A private method to populate our {'active_record'}

=head3 Returns

A hashref of the most recent network.free_partner_status record, with these added "columns"
	_active (a boolean value indicating if the expiration date is past or not)
	_stamp (the record timestamp value as a timestamp(0))
	
=cut

sub _loadCurrentRecord
{
	my $self = shift;
	$self->{'active_record'} = $self->{'db'}->selectrow_hashref("
		SELECT *,
			CASE WHEN expiration < NOW()::DATE THEN FALSE::BOOLEAN ELSE TRUE::BOOLEAN END AS _active,
			stamp::TIMESTAMP(0) AS _stamp
		FROM network.free_partner_status WHERE id= ?
		ORDER BY expiration DESC LIMIT 1", undef, $self->{'id'});
}

sub _resetErrStr
{
	my $self = shift;
	$self->{'errorStr'} = '';
}

sub _setErr
{
	my ($self, $err) = @_;
	$self->{'errorStr'} .= $err;
}

sub _setErrAndWarn
{
	my ($self, $err) = @_;
	$self->_setErr($err);
	warn $err;
}

=head2 _setOperator

A "private" method for creating a value to insert directly into the SQL for the operator column

=head3 Arguments

An optional text string indicating who is doing this operation

=head3 Returns

If an argument is supplied, the return value will be a db->quote() version of the argument.
Otherwise, is will be "current_user"()

=cut

sub _setOperator
{
	my $self = shift;
	my $operator = shift;
	return '"current_user"()' unless $operator;
	return $self->{'db'}->quote($operator);
}

sub _update
{
	my $self = shift;
	my $args = shift;
	
	# if $args is undef or empty, they we have nothing to do in here
	return 0 unless $args && keys %{$args};
	
	my (@vals) = ();
	my $sql = 'UPDATE network.free_partner_status SET';
	foreach (qw/expiration operator notes/)
	{
		next unless $args->{$_};
		
		if ($_ eq 'operator'){
			my $op = $self->_setOperator($args->{$_});
			$sql .= " operator = $op,";
		} elsif ($_ eq 'expiration' && $args->{$_}) {
			# let's make sure it is a least a valid value since it has to be inserted directly
			return 0 unless $self->_validateExpirationDate($args->{$_});
			$sql .= " expiration = ?,";
			push @vals, $args->{$_};
		} else {
			$sql .= " $_ = ?,";
			push @vals, $args->{$_};
		}
	}
	
	chop $sql;	# get rid of the trailing ','
	$sql .= "WHERE pk= $self->{'active_record'}->{'pk'}";
	return $self->{'db'}->do($sql, undef, @vals);
}

sub _validateExpirationDate
{
	my ($self, $exp) = @_;
	if ($exp && $exp !~ m/^\d{4}-\d{2}-\d{2}$/) {
		$self->_setErrAndWarn('The expiration date should either be empty or in the format of YYYY-MM-DD');
		return 0;
	}
	return 1;
}

sub _validateOrSetExpirationDate
{
	my ($self, $exp) = @_;
	return if $exp && ! $self->_validateExpirationDate($exp);

	return $exp if $exp;
	return $self->{'db'}->selectrow_array('SELECT NOW()::DATE');
}

1;