package odp;
use strict;
use CGI;
use Data::Dumper qw(Dumper);
$Data::Dumper::Indent=0;
$Data::Dumper::Terse=1;

# last modified 04/05/12	Bill MacArthur

# these are just in case we do not receive a DB handle
use lib '/home/httpd/cgi-lib';
use DB_Connect;

my $Q = ();

=head2 odp

	A class to provide some methods for getting certain bits of information regarding the "One Downline Pool"
	
	Some efforts have been made to reuse already looked up data in the event that this class's methods are reused during a session.
	That tends to make the code a bit more complex that it would ordinarily be.
	
=head3 new

	Instantiate a new 'odp' object
	
=head4 Arguments

	Pass in arguments on a hashref like this:
		{'db'=>active DB handle, 'cgi'=>CGI object...}
	
	DB handle will be created if one is not passed in and it is needed for completing execution of the requested method.
	You can also pass in a CGI object on the 'cgi' key. If you don't a new local copy will be initialized.
	
=cut

sub new
{
	my $args = shift;
	$args ||= {};
	$args = shift if $args && ref $args ne 'HASH';
	return bless ($args || {});
}

sub cgi
{
	my $self = shift;
	$self->{'cgi'} ||= new CGI;
	return $self->{'cgi'};
}

=head3 coach

	Get our coach either from the cookie info... or look it up
	Besides the return value, the 'coach' key is set on $self->{'me'}

=head4 Arguments

	A numeric member ID

=head4 Returns

	A hashref of the coach data:
		{'name'=>'Harry Finkler','emailaddress'=>'hj@finkler.me'}

=cut

sub coach
{
	my ($self, $id) = @_;
	$id ||= $self->WhoAmI();
	return {} if $id eq 'DEMO';
	
	# has 'me' been setup and is it really ME
	my $coach = $self->{'me'}->{'coach'} if $self->{'me'}->{'coach'} && $self->{'me'}->{'id'} == $id;
	unless ($coach)
	{
		if (my $ck = $self->HasCookie())
		{
			my $me = $self->GetMeFromCookie();
			$coach = $me->{'coach'} if $me->{'id'} == $id
			# the coach can end up undef here... think if I have another cookie which does not match my ID
			# oh the joys of having everybody and their brother looking at everybody's pages ;(
		}

		if (! $coach)
		{
# 12/13/11... for now we have to give 'em their virtual coach based on the upcoming lines of sponsorship
#			$coach = $self->db->selectrow_hashref("
#				SELECT firstname, lastname, emailaddress, other_contact_info AS skype
#				FROM my_upline_vip($id)");
#			$coach = $self->_tmpCoach($id);
			$coach = $self->db->selectrow_hashref("
				SELECT firstname, lastname, emailaddress, other_contact_info AS skype
				FROM my_coach($id)");

		}
	}
	$self->{'me'}->{'coach'} = $coach;
	return $coach;
}

=head3 cgiCookie

	Returns a CGI cookie to stuff into a header.

=cut

sub cgiCookie
{
	my $self = shift;
	unless ($self->{'me'})
	{
		$self->{'me'} = $self->GetMe();
	}

	unless ($self->{'me'}->{'coach'})
	{
		$self->{'me'}->{'coach'} = $self->coach();
	}
	
	unless ($self->{'me'}->{'counts'})
	{
		$self->{'me'}->{'counts'} = $self->GetPoolCounts();
	}
	
	return $self->cgi->cookie(
		'-name'=>'myodp',
		'-value'=>$self->cookie(),
		'-expires'=>'1h',
		'-path'=>'/',
		'-domain'=>'.clubshop.com'
		);
}

=head3 cookie

	Returns a | delimited set of values that comprises the 'myodp' cookie

=cut

sub cookie
{
	my $self = shift;
	my $rv = '';
	if ($self->{'me'})
	{
		$rv = Dumper($self->{'me'});
#			"$self->{'me'}->{'id'}|" .
#			"$self->{'me'}->{'alias'}|" .
#			"$self->{'me'}->{'firstname'}|" .
#			"$self->{'me'}->{'lastname'}|" .
#			"$self->{'me'}->{'membertype'}|" .
#			"$self->{'me'}->{'deadline'}|" .
#			"$self->{'me'}->{'seq'}|" .
#			"$self->{'me'}->{'days_left'}|" .
#			"$self->{'me'}->{'counts'}->{'partner_count'}|" .
#			"$self->{'me'}->{'counts'}->{'member_count'}|" .
#			"$self->{'me'}->{'coach'}->{'firstname'}|" .
#			"$self->{'me'}->{'coach'}->{'lastname'}|" .
#			"$self->{'me'}->{'coach'}->{'emailaddress'}|" .
#			"$self->{'me'}->{'coach'}->{'skype'}"
#		;
	}
	return $rv;
}

# this is just an internal routine to make it possible to do $self->db->method instead of $self->{'db'}->method
sub db
{
	my $self = shift;
	$self->{'db'} ||= DB_Connect::DB_Connect('odp.pm');
	return $self->{'db'};
}

=head3 GetMe

	Given a numeric ID, returns a hashref that is a blend of some members data and some od.pool data.
	Currently:
		{
			m.alias,
			m.id,
			m.firstname,
			m.lastname,
			od.pool.deadline,
			od.pool.seq
		}
	
=cut

sub GetMe
{
	my ($self, $id) = @_;
	$id ||= $self->WhoAmI();
	return $self->{'me'} if $self->{'me'} && $self->{'me'}->{'id'} == $id;
	
	# if we haven't been called before, maybe we can get the data from the cookie?
	my $me = $self->GetMeFromCookie();
	if ($me && $me->{'id'} && $me->{'id'} == $id)
	{
		$self->{'me'} = $me;
	}
	else
	{
		$self->{'me'} = $self->db->selectrow_hashref("
			SELECT
				m.alias, m.id, m.firstname, m.lastname, m.membertype,
				COALESCE(m.country,'US') AS country,
			--	od.pool.deadline::DATE AS deadline,
				last_of_month() AS deadline,
			--	od.pool.deadline AS deadline_timestamp,
				last_of_month() + INTERVAL '23 hours 59 min 59 sec' AS deadline_timestamp,
				od.pool.seq,
			--	(od.pool.deadline::DATE - NOW()::DATE) AS days_left,
				(last_of_month() - NOW()::DATE) AS days_left,
				COALESCE(cbc.currency_code,'USD') AS currency_code
				
			FROM members m
			LEFT JOIN od.pool
				ON od.pool.id=m.id
			LEFT JOIN currency_by_country cbc
				ON cbc.country_code=m.country
			WHERE m.id= ?
		", undef, uc($id)) if $id;
		
		# if we did not have an ID to lookup
		$self->{'me'} ||= $self->meSkel();
		
		# now get our subscription pricing
		# we will default to USD if we do not have a country
#		my $c = $self->{'me'}->{'country'} || 'US';
		$self->SubscriptionPricing( $self->{'me'}->{'country'} );
	}
	return $self->{'me'};
}

=head3 GetMeFromCookie

	If the 'myodp' cookie is available, parse it out and return it as a hashref.
	If we don't have a cookie or we cannot parse it, we'll return an empty hashref.
	
=cut

sub GetMeFromCookie
{
	my $self = shift;

	return {} unless my $ck = $self->HasCookie();
	$ck = $self->cgi->unescape($ck);
	$ck = eval( $ck );
	return $ck || {};
}

=head3 GetPoolCounts

	Pass in a numeric member ID and get back a hashref like this:
		{'partner_count'=>xxx, 'member_count'=>yyyy}
	Where partners are what is now known as VIPs and members are what are now known as non-VIPs
	The 'counts' key is also set on $self->{'me'}
	
=cut

sub GetPoolCounts
{
	my ($self, $id) = @_;
	$id ||= $self->WhoAmI();

	# has 'me' been setup and is it really ME
	my $counts = $self->{'me'}->{'counts'} if defined $self->{'me'}->{'counts'} && $self->{'me'}->{'id'} == $id;
	return $counts if $counts;

	# if they are not in a pool they will not have a 'seq' value
	if ($self->{'me'}->{'seq'})
	{
#		$counts = $self->db->selectrow_hashref("
#			SELECT
#				SUM( CASE WHEN m.membertype='v' THEN 1 ELSE 0 END ) AS partner_count,
#				SUM( CASE WHEN m.membertype <> 'v' THEN 1 ELSE 0 END ) AS member_count
#			FROM members m
#			JOIN od.pool
#				ON m.id=od.pool.id
#			WHERE od.pool.seq > $self->{'me'}->{'seq'}
#		");
		$counts = $self->db->selectrow_hashref("
			SELECT v AS partner_count, m AS member_count
			FROM od.pool_counts
			WHERE id= ?", undef, $id);
	}

	$counts ||= {'partner_count'=>0, 'member_count'=>0};

	# at least put something in there if there are the bottom most person
	$counts->{'member_count'} ||= 0;
	$counts->{'partner_count'} ||= 0;

	$counts->{'member_count'} = '1000+' if $counts->{'member_count'} >= 1000;
	$self->{'me'}->{'counts'} = $counts;
	return $counts;
}

=head3 HasCookie

	If the 'myodp' cookie is available, returns the cookie value, otherwise returns undef

=cut

sub HasCookie
{
	my $self = shift;
	return $self->cgi->cookie('myodp');
}

sub meSkel
{
	my $self = shift;
	return {
		'id'=>0,
		'alias'=>'',
		'membertype'=>'',
		'country'=>'US',
		'counts'=>{'partner_count'=>0,'member_count'=>0},
		'coach'=>{},
		'currency_code'=>'USD'
	};
}

=head3 SubscriptionPricing

	Get the subscription pricing information from the DB

=head4 Arguments

	A 2 character country code

=head4 Returns

	Sets and returns this hashref: $self->{'me'}->{'subscription_pricing'}
	
=cut

sub SubscriptionPricing
{
	my ($self, $c) = @_;
	($c) = $self->db->selectrow_array("
		SELECT currency_code FROM currency_by_country WHERE country_code=?", undef, $c);
	my $sth = $self->db->prepare("
		SELECT subscription_type, amount FROM subscription_pricing_current
		WHERE currency_code='$c' AND subscription_type IN (1,2,3)");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$self->{'me'}->{'subscription_pricing'}->{ $tmp->{'subscription_type'} } = $tmp->{'amount'};
	}
	$self->{'me'}->{'subscription_pricing'}->{'currency_code'} = $c;
	return $self->{'me'}->{'subscription_pricing'};
}

=head3 SubscriptionPrice

	We are expecting that GetMe has been called and so that the subscription pricing
	information is available.
	
=head4 Arguments

	Expects to receive of of two arguments: initial -or- monthly

=head4 Returns

	A numeric value prepended with the currency code.

=cut

sub SubscriptionPrice($$)
{
	my ($self, $arg) = @_;
	my $rv = ''; #$self->{'me'}->{'subscription_pricing'}->{'currency_code'};
	if ($arg eq 'monthly')
	{
		$rv .= "$self->{'me'}->{'subscription_pricing'}->{'3'}";
	}
	elsif ($arg eq 'initial')
	{
		my $price = $self->{'me'}->{'subscription_pricing'}->{'1'} + $self->{'me'}->{'subscription_pricing'}->{'2'};
		$rv .= "$price";
	}
	return $rv;
}

=head3 WhoAmI

	Determines who we are based upon the ID parameter or the ID cookie, in that order.
	If neither of these values are available, then returns 0

=cut

sub WhoAmI
{
	my $self = shift;
	my $rv = $self->cgi->param('id') || $self->cgi->cookie('id') || 0;
	($rv) = $self->db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, $rv) if $rv =~ /\D/;
	return $rv;
}

# this will have to do until we *finally* get the new virtual genealogy data into the live dataset
sub _tmpCoach
{
	my ($self, $id) = @_;
	# let's see if we have a virtual sponsor record...if not we are done
	# we are only going to look 4 up as we should find a VIP by then, if not there is some kind of problem
	my $rv = ();
	my $sth = $self->db->prepare('
		SELECT m.id, m.firstname, m.lastname, m.emailaddress, m.other_contact_info AS skype, membertype
		FROM original_vip_spid ovs
		JOIN members m
			ON m.id=ovs.spid
		WHERE ovs.id=?
		');
	my $cnt = 0;
	while ($cnt < 4)
	{
		$sth->execute($id);
		$rv = $sth->fetchrow_hashref;
		last unless $rv;
		last if $rv->{'membertype'} eq 'v';
		$id = $rv->{'id'};
		$cnt++;
	}
	return $rv;
}

1;

__END__

04/05/12	changed the GetPoolCounts method to look at the counts table instead of doing counting explicitly
