package LastLineActivity;
use strict;

=head1 LastLineActivity

	A class for obtaining the appropriate last purchase most appropriate for a given party in an nspid line
	-and/or- the last upgrade under me
	
=head2 new

	Instantiate a new object.

=head3 Arguments

	Pass these as a hash ref.
	This is the minimal requirement: {'db'=> "an-active-DB-handle", 'id'=>the numeric member ID}
	You can also pass in 'seq' and 'pnsid' key values on the hashref to avoid having to look them up again.

=cut

my %Cache = ();

sub new
{
	my $args = shift;
	$args = shift if ! ref $args;	# if we were called as a static function we could have the args as the first argument
	die "Arguments should be passed in a hashref like this: {'db'=> an-active-DB-handle, 'id'=>the numeric member ID}" unless ref($args) eq 'HASH';

	# we need a DB handle... we will not validate it, it just better be good when we go to use it
	die "We need an DB handle on the 'db' key" unless $args->{'db'};
	die "We need a numeric ID on the 'id' key" unless $args->{'id'} && $args->{'id'} !~ m/\D/;
	
	my $self = bless $args;
	
	# if we are provided with this data upon instantiation, then init the cache so we don't have to look it up again
	$self->myPosition({'seq'=>$args->{'seq'}, 'pnsid'=>$args->{'pnsid'}}) if $args->{'seq'} && $args->{'pnsid'};
	
	return $self;
}

=head2 LLP

	Ya, not very creative, but good enough to indicate the Last Line Purchase.

=head3 Arguments

	An optional
	If the network.positions seq & pnsid values are already available and can be passed in, then add keys for those values.
	If the network.positions values are not already available, they will be looked up and made available using the Position() method
	{'seq'=>45678901, 'pnsid'=>4987654}
	
=head3 Returns

	A hashref of data on the Last Line Purchase purchaser
	-or-
	undef if one is not available and/or applicable to the ID passed
	
=cut

sub LLP
{
	my ($self, $args) = @_;
	
	my $position = $self->myPosition($args);
	
	# for now there is no more logic needed than simply pulling a record and returning the result
	# this will likely change soon after rollout
	return $self->{'db'}->selectrow_hashref("
		SELECT
			m.id,
			m.alias,
			m.emailaddress,
			SUBSTR(m.firstname, 1, 1) AS first_initial,
			m.firstname,
			m.lastname,
			m.firstname||' '||m.lastname AS name,
			m.country AS country_code
		FROM members m
		JOIN mviews.last_line_purchase llp
			ON llp.id=m.id
		WHERE llp.seq > $position->{'seq'}
		AND llp.pnsid = $position->{'pnsid'}
		ORDER BY seq
		LIMIT 1
	");
}

=head2 LLU

	Ya, not very creative, but good enough to indicate the Last Line Upgrade.

=head3 Arguments

	None
	
=head3 Returns

	A hashref of data on the Last Line Upgrade
	-or-
	undef if one is not available and/or applicable to the ID passed
	
=cut

sub LLU
{
	my ($self) = @_;
	
	my $position = $self->myPosition();
	
	# for now there is no more logic needed than simply pulling a record and returning the result
	# this will likely change soon after rollout
	return $self->{'db'}->selectrow_hashref("
		SELECT
			m.id,
			m.alias,
			m.emailaddress,
			SUBSTR(m.firstname, 1, 1) AS first_initial,
			m.firstname,
			m.lastname,
			m.firstname||' '||m.lastname AS name,
			m.country AS country_code
		FROM members m
		JOIN mviews.last_line_upgrade llp
			ON llp.id=m.id
		WHERE llp.seq > $position->{'seq'}
		AND llp.pnsid = $position->{'pnsid'}
		ORDER BY seq
		LIMIT 1
	");
}

=head2 myPosition

	A method for obtaining/caching a network.position record
	If we receive a hashref like this, {'id'=>1234567, 'seq'=>45678901, 'pnsid'=>4987654}, we will simply cache those values.
	Otherwise, we will look them up and cache them.
	If the lookup returns empty, we will cache that so as to not bother looking again.

=head3 Arguments

	Arguments should be passed in a hashref with minimally an 'id' key pointing the party we are looking up.
	
=head3 Returns

	A hashref like this: {'id'=>1234567, 'seq'=>45678901, 'pnsid'=>4987654}
	-or- undef

=cut

sub myPosition
{
	my $self = shift;
	my $args = shift || {};	# do that so that the evaluation below doesn't choke on an undef being represented as a hashref
	
	if (exists $Cache{'position'})
	{
		return $Cache{'position'};
	}
	elsif ($args->{'seq'} && $args->{'pnsid'})
	{
		$Cache{'position'} = $args;
	}
	else
	{
		$Cache{'position'} = $self->{'db'}->selectrow_hashref("SELECT id, pnsid, seq FROM mviews.positions WHERE id= ?", undef, $self->{'id'});
	}
	
	return $Cache{'position'};
}

=head2 myTPP

	A method for obtaining/caching what is currently known as TPP since that is needed to determine if we are going to return something from LLP()
	
=head3 Arguments

	An optional hashref with a key 'tpp', like what this returns ;-)
	
=head3 Returns

	A hashref like this: {'tpp'=>0}
	-or- undef

=cut

sub Refcomp
{
	my $self = shift;
	
	if (exists $Cache{'refcomp'} && exists $Cache{'refcomp'}->{'tpp'})
	{
		return $Cache{'refcomp'};
	}
	else
	{
		$Cache{'refcomp'} = $self->{'db'}->selectrow_hashref("SELECT opp AS tpp FROM refcomp WHERE id= ?", undef, $self->{'id'});
	}
	
	return $Cache{'refcomp'};
}

1;

__END__

03/31/15	Created by Bill MacArthur
