package Merch_Tools;
use strict;
use Data::Dumper;
=head1
NAME: Merch_Tools

	This class is ment to replace use of XSL XST calls

=head1
VERSION:

	1.0

=head1
=head2 CONSTRUCTOR

=head3
new

=head4
Params:

	db obj to a database connection

=head4
Returns:

	obj $this

=cut

sub new
{
	
   my $class = shift;
   my $this = {
   			'__name' => 'Merch_Tools',
   			'__version' => 1.0,
	};
		
	bless($this, $class);
	
	
   return $this;
}


=head3 AddLanguage

=head4
Description:

	Given and html or other language snippet, replace all %%variable%% with corresponding data from hash

=cut

sub addLanguage
{
    my $html = shift;
    my $trans = shift;
    my $parent = shift;

    foreach my $key (keys %{$trans})
    {
        if (ref($trans->{$key}) eq 'HASH')
        {
			if (exists $trans->{$key}->{'content'})
			{
				if (defined $parent)
				{
					my $newkey = $parent . "_" . $key;
					if ($html =~ /%%$newkey%%/)
					{
						$html =~ s/%%$newkey%%/$trans->{$key}->{content}/g;
		    		}
					else
					{
						$html =~ s/%%$key%%/$trans->{$key}->{content}/g;
					}
	        }
	        else
	        {
				$html =~ s/%%$key%%/$trans->{$key}->{'content'}/g;
	        }
	    }
	    else
	    {
	        $html = addLanguage($html, $trans->{$key}, $key);
	    }
        }
        else 
        {
            if (defined $parent)
            {
				my $newkey = $parent . "_" . $key;
				if ($html =~ /%%$newkey%%/)
				{
					$html =~ s/%%$newkey%%/$trans->{$key}/g;
				}
				else
				{
					$html =~ s/%%$key%%/$trans->{$key}/g;
				}
            }
            else
            {
				$html =~ s/%%$key%%/$trans->{$key}/g;
            }
        }
    }
    return $html;
}



1;

__END__

=head1 SEE ALSO: 

=head1 CHANGE LOG:

	2012-05-14 Bill
		No significant changes... just reformatting to make it easier to read and figure out what is going on
	
=cut
