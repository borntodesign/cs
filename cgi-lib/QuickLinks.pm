package QuickLinks;

=head1
	QuickLinks
=head2
	Description
=head4
	Instead of having to create lists of quick links at various resources,
	we will abstract this stuff out to this Class and call the desired method to get the links back
	complete with translated labels (if available), in whatever format we may need them.
	
	This will keep all the URLs in one place and make it a simple affair to simply pass in a list of link IDs to get our list

	Created: 03/16/09	Bill MacArthur
	Last modified: 02/04/15	Bill MacArthur
=cut

use strict;
use XML::Simple;
use lib ('/home/httpd/cgi-lib');
use G_S;

my $TMPL = 10667;	# this would be the XML list of labels
our %XML = ();

=head4
	Be sure to pass in an active database handle and the language pref to new so that they will be available across the board
	my $mf = MemberFollowup->new($dbh);
=cut

sub new
{
	my ($self, $dbh, $lang_pref) = @_;
	$self = bless {'dbh' => $dbh}, __PACKAGE__;
	$self->_loadXML($lang_pref);
	return $self;
}

=head5
	compile_data
	get all of our resources and return a standard dataset, an arrayref of hashrefs
=cut

sub compile_data
{
	my ($self, $member, $list) = @_;
	my @rv = ();
	foreach (@{$list})
	{
		my $tmp = eval("\$self->_$_(\$member)");
		print "$@\n" if $@;
		next unless $tmp;	# some of the routines are disabled by returning undef
		push @rv, $tmp;
	}
	return \@rv;
}

=head5
	BuildOptionList
	Requires a hashref of a member record and an arrayref of report items desired
	Returns an option list which can be inserted directly into an HTML <select> tag
=cut

sub BuildOptionsList
{
	my ($self, $member, $list) = @_;
	my $rv = '';
	$list = $self->compile_data($member, $list);
	foreach ( @{$list} )
	{
		# ordinary options will return a hashref
		# and they should have a href... but then there are the personal website links which may end up empty
		if (ref $_)
		{
			$rv .= qq|<option value="$_->{'href'}">$_->{'label'}</option>\n| if $_->{'href'};
		}
		# the new _15 returns an entire optgroup set as text
		else
		{
			$rv .= $_;
		}
	}
	return $rv;
}

# the following are internal functions

sub _defaultNode
{
	my $node = shift;
	return $XML{'native'}->{$node} || $XML{'en'}->{$node};	
}

sub _loadXML
{
	my $self = shift;
	my $lang_pref = shift;
	my $gs = new G_S;
	my $xml = $gs->Get_Object($self->{'dbh'}, $TMPL, 'en') || die "Failed to load 'en' lang_blocks XML: $TMPL \n";
	$XML{'en'} = XMLin($xml, 'NoAttr' => 1);

	$xml = $self->{'lang_pref'} ? $gs->Get_Object($self->{'dbh'}, $TMPL, $lang_pref) : ();
	$XML{'native'} = $xml ? XMLin($xml, 'NoAttr' => 1) : {};
}

=head5
	1 - Team Report
=cut

sub _1
{
	return undef; # disabled 11/16/15
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r1'),
		'href'	=> "//www.clubshop.com/cgi/vip/tree.cgi/team?id=$member->{'id'}"
	};
}

=head5
	2 - Organization Report
=cut

sub _2
{
	return undef; # disabled 03/09/12
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r2'),
		'href'	=> "//www.glocalincome.com/cgi/organization.cgi?r=$member->{'id'}"
	};
}

=head5
	3 - Transaction Report
=cut

sub _3
{
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r3'),
		'href'	=> "/cgi/member_trans_rpt.cgi?id=$member->{'id'}"
	};
}

=head5
	4 - Income Report
=cut

sub _4
{
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r4'),
		'href'	=> "//www.clubshop.com/cgi/pireport.cgi?rid=$member->{'id'}"
	};
}

=head5
	5 - Auto Stackers
=cut

sub _5
{
	return undef; # disabled 12/02/11
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r5'),
		'href'	=> "//www.clubshop.com/cgi/vip/arsmgt.cgi?rid=$member->{'id'}"
	};
}

=head5
	6 - Member Transfer interface
=cut

sub _6
{
	return undef; # disabled 12/02/11
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r6'),
		'href'	=> "//www.clubshop.com/cgi/mv-members.cgi?rid=$member->{'id'}"
	};
}

=head5
	7 - Activity Report
=cut

sub _7
{
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r7'),
		'href'	=> "//www.clubshop.com/cgi/vip/activity_rpt?rid=$member->{'id'}"
	};
}

=head5
	8 - eBiz Course History
=cut

sub _8
{
	return undef; # disabled 12/02/11
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r8'),
		'href'	=> "//www.clubshop.com/cgi/ebiz_sessions.cgi?search_id=$member->{'id'}"
	};
}

=head5
	9 - eBiz Course History
=cut

sub _9
{
	return undef; # disabled 12/02/11
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r9'),
		'href'	=> "//www.clubshop.com/cgi/nbteam_quals.cgi?nextrole=1;report_id=$member->{'id'}"
	};
}

=head5
	10 - Personal Web Site
=cut

sub _10
{
	my ($self, $member) = @_;
	my ($url) = $self->{'dbh'}->selectrow_array('
			SELECT val_text FROM member_options
			WHERE option_type=2 AND id= ?', undef, $member->{'id'});
	return {
		'label'	=> _defaultNode('r10'),
		'href'	=> $url
	};
}

=head5
	11 - Network Pay Calculations
=cut

sub _11
{
	return undef; # disabled 03/09/12
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r11'),
		'href'	=> "//www.clubshop.com/cgi/net_calcs.cgi?rid=$member->{'id'}"
	};
}

=head5
	12 - Upline Report
=cut

sub _12
{
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r12'),
		'href'	=> "//www.clubshop.com/cgi/vip/line_rpt.cgi?mid=$member->{'id'}"
	};
}

=head5
	13 - Trial Partner Group Report
=cut

sub _13
{
#	return undef;	# disabled 10/01/12
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r13'),
		'href'	=> "//www.clubshop.com/cgi/tpgroup.cgi?id=$member->{'id'}"
	};
}

=head5
	14 - Trial Partner Report
=cut

sub _14
{
#	return undef;	# disabled 10/01/12
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r14'),
		'href'	=> "http://www.clubshop.com/cgi/TrialPartner.cgi?id=$member->{'id'}"
	};
}

=head5
	15 - Frontline Lists Group
=cut

sub _15
{
	my ($self, $member) = @_;
	my $rv = '<optgroup label="' . _defaultNode('r15') . '">';
	$rv .= qq|
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=m;refid=$member->{'id'}">Members</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=a;refid=$member->{'id'}">Affiliates</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=mr;refid=$member->{'id'}">Merchants</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=ag;refid=$member->{'id'}">Affinity Groups</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=p;refid=$member->{'id'}">Partners</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=tp;refid=$member->{'id'}">Trial Partners</option>
	|;
	$rv .= '</optgroup>';
	return $rv;
}

=head5
	16 - Group Report
=cut

sub _16
{
	my ($self, $member) = @_;
	return {
		'label'	=> _defaultNode('r16'),
		'href'	=> "//www.clubshop.com/cgi/vip/tree.cgi/grp?id=$member->{'id'}"
	};
}

=head5
	17 - Frontline Lists Group for Affinity Groups
=cut

sub _17
{
	my ($self, $member) = @_;
	my $rv = '<optgroup label="' . _defaultNode('r15') . '">';
	$rv .= qq|
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=m;refid=$member->{'id'}">Members</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=a;refid=$member->{'id'}">Affiliates</option>
		<option value="http://www.clubshop.com/cgi/member_frontline2.cgi?csv_file=0;report_type=tp;refid=$member->{'id'}">Trial Partners</option>
	|;
	$rv .= '</optgroup>';
	return $rv;
}

1;

# 12/02/11 disabled several of the options
###### 03/09/12 added _15 and handling for it
###### 08/27/12 made it so that _X routines that return undef do not get put on the stack of options to process
###### 09/03/14 repurposed 13 and 14 and reactivated for the latest version of Trial Partners
# 02/04/15 added _17 for the specific lists applicable to an AG