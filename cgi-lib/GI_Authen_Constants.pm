package GI_Authen_Constants;

use strict;
use Crypt::CBC;
use Exporter qw( import );

our @EXPORT_OK = qw/$secret_key cipher $LoginScript/;

our $secret_key = '9(7^%#=|';
our $LoginScript = 'https://glocalincome.glocalincome.com/home/login.xml';

sub cipher {
	return Crypt::CBC->new('-key'=>$secret_key, '-cipher'=>'DES' );
}
1;