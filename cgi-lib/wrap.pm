package wrap;
use strict;
use Template;

=head2 wrap

A class to wrap some content in a standard look.
Ideal for something like appx where there are several steps which all need the same look from start to finish.
This class uses Template Toolkit for rendering.

=head4 Usage

$wrap = wrap->new({
	template=>'template ID of the wrapper',
	lang_nodes=> {a hashref of language content used in the wrapper},
	db=> {an active DB handle},
	gs=> {a G_S object}
});

Other items can be added to the arg hashref and will be made available to TT on the key.

my $output = $wrap->wrap( {content=> "the content I want wrapped"} );

Our wrapper template should at least have a TT placeholder of [% content %] where the content is supposed to go.

=cut

=head2 lang_nodes

Retrieve the current value of the lang_nodes value
-or-
set our lang_nodes hashref subsequent to calling new or to replace a previous value

=head3 Params

	When setting, {a hashref of language content used in the wrapper}

=head3 Returns

	The current lang_nodes value. If this value has not been set, the return value will be an empty hashref.
	
=cut

sub lang_nodes
{
	my $self = shift;
	my $ref = shift;
	$self->{'lang_nodes'} = $ref if $ref;
	return $self->{'lang_nodes'} || {};
}

sub new
{
	my $args = shift;
	# if we received a class name as if we were invoked as wrap->new , we don't really need that
	$args = shift if $args eq 'wrap';
	die "Arguments should be passed in a hash reference\n" unless ref $args eq 'HASH';
	
	# let's try to get our wrapper document right off the bat
	$args->{'wrapper'} = $args->{'gs'}->Get_Object( $args->{'db'}, $args->{'template'} ) ||
		die "Failed to load template: $args->{'template'}";
	return bless $args;
}

sub wrap
{
	my $self = shift;
	my $args = shift;
	my $TT = Template->new();
	my $rv = ();
	my $data = {'lang_nodes'=>$self->lang_nodes, 'content'=>$args->{'content'}};
	# provide a mechanism to include other items at the time "new" is called
	foreach my $k (keys %{$self}){
		next if grep $_ eq $k, (qw/gs db lang_nodes template/);
		$data->{$k} = $self->{$k};
	}
	$TT->process(\$self->{'wrapper'}, $data, \$rv);
	return $rv;
}

1;

__END__

10/14/10 added the lang_nodes method
