package PotentialMerchantAffiliates;
use strict;
use Members;
use MailTools;
use G_S;


=head1
NAME: PotentialMerchantAffiliates

	This class is ment to manage information pertaining
	to preregistered merchants

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

	my $returned_hashref = {};
	my %submitted_data
	$submitted_data{pk}	= $CGI->param('pk') ? $CGI->param('pk') : '';
	$submitted_data{id}	= $CGI->param('id') ? $CGI->param('id') : '';

	eval{
		my $PotentialMerchantAffiliates = PotentialMerchantAffiliates->new($DB);
		$returned_hashref = $PotentialMerchantAffiliates->updatePotentialMerchantInformation($submitted_data);
	};
	if($@)
	{
		#How ever you want to handle the error.
	}

=cut

=head1 DESCRIPTION

=cut

=head2 CONSTRUCTOR

=head3
new

=head4
Params:

	db obj to a database connection

=head4
Returns:

	obj $this

=cut

sub new
{
	use XML::Simple;
	
    my $class = shift;
    my $this = {
    			name => 'PotentialMerchantAffiliates',
    			version => 0.1,
    			potential_merchant_affiliates_info_by_pk => {},
    };

    $this->{db} = shift;
	
	bless($this, $class);
	
	eval{
		
		$this->setG_S();
		
		my $standard_language = $this->{_G_S}->Get_Object($this->{db}, 10633);
		$this->{standard_language} = XMLin($standard_language);
		
	};
	if($@)
	{
		die 'The standard language is not available. ' . $@	
	}
	
    
    return $this;
}


=head2
METHODS

=head3
PRIVATE

=cut

=head3 __updateCache

=head4
Description:

	Get the users information by the "Member ID"
	or "Alias" and assign the data to the cache

=head4
Params:

	hashref
		member_id	bool	If true use the 'id' else use 'pk'
		id		string	The 'Members ID', or the 'pk'

=head4
Returns:

	string	on error
=cut

sub __updateCache
{
	my $this = shift;
	my $args = shift;

	if (! exists $args->{pk} || $args->{pk} !~ /^\d+$/)
	{
		return "The 'pk' parameter is required, and must be an integer.";
	}


	eval
	{
		my $sth = $this->{db}->prepare("SELECT * FROM potential_merchant_affiliates WHERE pk = ?");

		$sth->execute($args->{pk});

		my $rows = $sth->fetchrow_hashref;

		$this->{potential_merchant_affiliates_info_by_pk}->{$rows->{pk}} = $rows;


	};
	if ($@)
	{
		return "The cache was not updated due to a database error. <br />Query: SELECT * FROM potential_merchant_affiliates WHERE pk = ? <br />pk: $args->{pk} <br />" . $@;
	}

	return;
}

=head3
PUBLIC

=head3
setStandardLanguage

=head4
Description:

	Ideally the constructor should take this into 
	account, but it's not bad having a public method 
	as well.  For those instances when you need 
	to change languages mid stream.

=cut

sub setStandardLanguage
{
	use XML::Simple;
	
	my $this = shift;
	my $language_code = shift;
	
	die 'The ISO language code must be specified' if (! defined $language_code || $language_code !~/^\w\w$/);
	
	my $standard_language = $this->{_G_S}->Get_Object($this->{db}, 10633, $language_code);
	$this->{standard_language} = XMLin($standard_language);	
	
	return;
	
}




=head3
setMembers

=head4
Description:

	Instanciates the "Members" subclass, if it is not already instanciated

=cut

sub setMembers
{
	my $this = shift;

	eval{
		$this->{Members} = Members->new($this->{db}) if ! exists $this->{Members};
	};
	if($@)
	{
		die 'There was an issue instanciating Members. Error: ' . $@;
	}
	
	
}

=head3
PUBLIC

=head3
setG_S

=head4
Description:

	Instanciates the "_G_S" subclass, if it is not already instanciated

=cut

sub setG_S
{
	my $this = shift;

	eval{
		$this->{_G_S} = G_S->new() if ! exists $this->{_G_S};
	};
	if($@)
	{
		die 'There was an issue instanciating Members. Error: ' . $@;
	}
	
	
}

=head3 updateMemberId

=head4
Description:

	Update the potential_merchant_affiliates
	table so the id is set for this merchant.

=head4
Params:

	hashref
		pk	int	The PK from the potential_merchant_affiliates"
				table for this Potential Merchant

		id	int	The merchants member ID.

=head4
Returns:

	string on failure

=cut

sub updateMemberId
{
	my $this = shift;
	my $params = shift;

	if (! exists $params->{id} || ! exists $params->{pk} || $params->{id} !~ /^\d+$/ || $params->{pk} !~ /^\d+$/)
	{
		return "The members 'id', and 'pk' are required parameters, and they must be integers (numbers).";
	}

	my @vendor_information = ($params->{id}, $params->{pk});

	my $update_query = 'UPDATE potential_merchant_affiliates SET id=? WHERE pk =?';

	eval{
		$this->{db}->do($update_query, undef, @vendor_information);
	};
	if($@)
	{
		die "There was an error executing the query. <br />Query: $update_query <br />params->{id}: $params->{id} <br />params->{pk}: $params->{pk} <br />" . $@;
	}

	####################################################
	# This may be needed later on.
	#
	# $this->__updateCache({pk=>$params->{pk}});
	####################################################

	return;


}

=head3 getPotentialMerchant

=head4
Params:

	hashref The key is the field to search, and the
			value is the value to search for. If
			one of the keys are "pk" one row is
			returned from the cache.

=head4
Returns:

	hashref on failure return null

=cut

sub getPotentialMerchant
{
	my $this = shift;
	my $params = shift;
	my $query = 'SELECT * FROM potential_merchant_affiliates WHERE ';

	my $fields = '';
	my @values = ();
	my $return_arrayref = ();

	if(exists $params->{pk})
	{
		if(exists $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}})
		{
			push @$return_arrayref, $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}};
			return $return_arrayref
		} else {
			my $error = $this->__updateCache({pk=>$params->{pk}});
			die $error if($error);

			push @$return_arrayref, $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}};
			return $return_arrayref;
		}
	}

	########################################
	# Build the fields to query with.
	########################################
	foreach my $key (keys %$params)
	{
		next if ! $params->{$key};

		$fields .= "$key ILIKE ? AND ";
		push @values, $params->{$key};
	}

	$fields =~ s/AND\s$//;

	return if !$fields;

	$query .= $fields;

	$query .= ' ORDER BY company_name';

	eval{

		my $STH = $this->{db}->prepare($query);

		$STH->execute(@values);

		while(my $rows = $STH->fetchrow_hashref())
		{
			push @$return_arrayref, $rows;
		}


	};
	if($@)
	{
		die "There was an issue with retreiving the information.  <br />Query: $query <br /> " .  $@;
	}


	# At some point we may want to loop through "$return_arrayref",
	# and set the cache for each pk, but there is no need at this point.

	return $return_arrayref;


}

=head3 deleteMerchantRecord

=head4
Description:

	This PERMANENTLY deletes the Potential Merchants information.

=head4
Params:

	pk	int	The primary key from the potential_merchant_affiliates table.

=head4
Return:

	undef/string	undefefined on sucess, and a message on error.

=cut


sub deleteMerchantRecord
{
	my $this = shift;
	my $pk = shift;
	
	my $deleted_rows = 0;
	
	die 'The required parameter was not passed, or is in an inncorrect format. It must be an integer. ' if $pk !~ /^\d+$/;
	
	my $query  = 'DELETE FROM potential_merchant_affiliates WHERE pk = ?';
	
	eval{
		$deleted_rows = $this->{db}->do($query, undef, ($pk));
	};
	if($@)
	{
		return 'The potential merchant record was not deleted.  PK:' . $pk . ' Error: ' . $@;
	}
	
	if($deleted_rows == 1)
	{
		return;
	} 
	elsif(!$deleted_rows)
	{
		return 'The Pre-registered Merchant was not removed. PK: ' . $pk;
	}
	
	return 'More than one Pre-registered Merchant record was deleted.  Number of rows deleted: ' . $deleted_rows . ' PK: ' . $pk;
	
}

=head3 updatePotentialMerchantInformation

=head4
Description:

	This updates the Potential Merchants information.

=head4
Params:

	hashref
		should be a hashref where the keys
		are named the same as the fields in
		the potential_merchant_affiliates.
		If there is an accepted key in the
		hashref set to true the accepted
		field will be updated to the current
		date.  If the "member_id_assigned",
		or "id" field are missing a new "x"
		member will be created for this
		potential merchant.

=head4
Return:

	the number of rows effected.

=cut

sub updatePotentialMerchantInformation
{
	my $this = shift;
	my $params = shift;
	my $return_value = {updated=>0, new_member_info=>{}};

	if (! $params)
	{
		die 'No parameters were passed.';
	}


	if(! exists $params->{pk} || $params->{pk} !~ /^\d*$/)
	{
		die 'The primary key is required to make the update, and it must be an integer (number).';
	}

	my $update_query = 'UPDATE potential_merchant_affiliates SET ';

	my $insert_notification_type_query =<<EOT;
			INSERT INTO notifications (id, notification_type, memo) VALUES (?,101, 'New Merchant from the Merchant Pre-registration process.')

EOT

	my $set_notification = 0;
	


	my $set_parameters = '';
	my @values = ();


		# If there is no Member ID is or no Member ID Assigned a new membership is created.
		#TODO we should probably add some member id checking to make sure the one provided is accurate
		if($params->{member_id_assigned} =~ /^NULL$|^\s*$/ && $params->{id} =~ /^NULL$|^\s*$/)
		{
			my $member_information = {};

			#No "id" should be needed it is generated in the Members object if one is not specified.
			#$member_information->{id} 					= ;
			$member_information->{spid}					= $params->{referral} ? $params->{referral}: 1;
			$member_information->{membertype}			= 'x';
			#$member_information->{prefix}				= $params->{} ? $params->{}: ;
			$member_information->{firstname}			= $params->{firstname} ? $params->{firstname}: '';
			#$member_information->{mdname}				= $params->{} ? $params->{}: ;
			$member_information->{lastname}				= $params->{lastname} ? $params->{lastname}: '';
			#$member_information->{suffix}				= $params->{} ? $params->{}: ;
			#$member_information->{secprefix}			= $params->{} ? $params->{}: ;
			#$member_information->{secfirstname}			= $params->{} ? $params->{}: ;
			#$member_information->{secmdname}			= $params->{} ? $params->{}: ;
			#$member_information->{seclastname}			= $params->{} ? $params->{}: ;
			#$member_information->{secsuffix}			= $params->{} ? $params->{}: ;
			#$member_information->{company_name}			= $params->{} ? $params->{}: ;
			$member_information->{emailaddress}			= $params->{emailaddress2} ? $params->{emailaddress2}: '';
			#$member_information->{emailaddress2}		= $params->{} ? $params->{}: ;

			#No "alias" should be needed it is generated in the Members object if one is not specified.
			#$member_information->{alias}				=  ;
			#$member_information->{join_date}			= $params->{} ? $params->{}: ;
			$member_information->{address1}				= $params->{address1} ? $params->{address1}: '';
			#$member_information->{address2}				= $params->{} ? $params->{}: ;
			$member_information->{city}					= $params->{city} ? $params->{city}: '';
			$member_information->{state}				= $params->{state} ? $params->{state}: '';
			$member_information->{postalcode}			= $params->{postalcode} ? $params->{postalcode}: '';
			$member_information->{country}				= $params->{country} ? $params->{country}: '';
			$member_information->{phone}				= $params->{phone} ? $params->{phone}: '';
			$member_information->{fax_num}				= $params->{fax_num} ? $params->{fax_num}: '';
			#$member_information->{vip_date}				= $params->{} ? $params->{}: '';

			#No "password" should be needed it is generated in the Members object if one is not specified.
			#$member_information->{"password"}			= $params->{} ? $params->{}: '';
			#$member_information->{stamp}				= $params->{} ? $params->{}: '';
			$member_information->{memo}					= 'Member automatically created from the Potential Merchant Affiliate Application';
			#$member_information->{status}				= $params->{} ? $params->{}: '';
			$member_information->{mail_option_lvl}		= 2;
			$member_information->{language_pref}		= $params->{language_pref} ? $params->{language_pref}: 'en';
			#$member_information->{cellphone}			= $params->{} ? $params->{}: ;
			#$member_information->{other_contact_info}	= $params->{} ? $params->{}: ;
			
			$member_information->{ipaddress}			= $params->{ipaddress} ? $params->{ipaddress}: '';
			$member_information->{signup_date}			= $params->{signup_date} ? $params->{signup_date}: '';
			$member_information->{landing_page}			= $params->{landing_page} ? $params->{landing_page}: '';
			
			eval{

				if(!defined $this->{Members})
				{
					$this->setMembers();
				}

				$return_value->{new_member_info} = $this->{Members}->createNewMember($member_information);

				#die 'The member record could not be created.' if(!$return_value->{new_member_info});

				die 'The member record could not be created. ' . ref($return_value->{new_member_info}) . ' ' . $return_value->{new_member_info} if (ref($return_value->{new_member_info}) ne 'HASH');
				
				$params->{member_id_assigned} = $return_value->{new_member_info}->{member_id};
				
				

			};
			if($@)
			{
				
				die "We could not create a membership for this potential merchant. Error: " .$@;
			}
		}

	foreach my $key (keys %$params)
	{
		next if ($key =~ /^pk$|^ipaddress$|^signup_date$|^landing_page$/);

		if ($key ne 'accepted')
		{

			if ($params->{$key} =~/^NULL$|^\s*&/)
			{
				$set_parameters .=  " $key=NULL, ";
			} else {
				$set_parameters .=  " $key=?, ";
				push @values, $params->{$key};
			}
		} else {

			if(! exists $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}})
			{
				my $errors = $this->__updateCache({pk=>$params->{pk}});
				if($errors)
				{
					die 'There was an issue retrieving the Potential Merchant Affiliates information for updating. PotentialMerchantAffilates::updatePotentialMerchantInformation.  Error: ' . $errors;
				}
			}

			#skip to the next itteration if the merchants accepted status will not change.
			next if ($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{accepted} && $params->{accepted});

			if(! $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{accepted} && $params->{accepted})
			{
				$set_parameters .= " accepted=NOW(), ";
				#set the notification here!!
				$set_notification = 1;
				
				
			} else {
				$set_parameters .= " accepted=NULL, ";
			}


		}

	}











	$set_parameters =~ s/,\s$//;

	return 0 if ! $set_parameters;

	$update_query .= $set_parameters . ' WHERE pk =?';

	push @values, $params->{pk};

	eval{


		$return_value->{updated} =  $this->{db}->do($update_query, undef, @values);
		$this->__updateCache({pk=>$params->{pk}});
		
		
	};
	if($@)
	{
		die "There was an issue updateing the information.  <br />Query: $update_query <br /> " . $@;
	}
	
		if($set_notification)
		{
			
			if(($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{member_id_assigned} || $this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{id}))
			{
				
				@values = ();
				
				if($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{member_id_assigned})
				{
					@values = ($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{member_id_assigned});
				} else {
					if($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{id} =~ /^\d+$/)
					{
						@values = ($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{id});	
					} else {
				
					$this->setMembers() if(!defined $this->{Members});
						
						my $member_hashref = $this->{Members}->getMemberInformationByMemberAlias($this->{potential_merchant_affiliates_info_by_pk}->{$params->{pk}}->{id});
						
						if($member_hashref)
						{
							@values = ($member_hashref->{alias});
						}
					}
				}
				
				
				die 'The Potential merchant did not get a notification email.  ' if (! $values[0]);
				
				eval{
					
					$this->{db}->do($insert_notification_type_query, undef, @values);
				};
				if($@)
				{
					die "There was an issue updateing the information.  <br />Query: $insert_notification_type_query <br /> " . $@;
				}
			} else {				
				die "There was an issue with the merchants member id, ";
			}
		}


	return $return_value;
}

=head3 retrieveFlier

=head4
Description:

	This takes a location, as a 
	string, and displayes the 
	potential merchant flier.

=head4
Prerequisits:

	This method uses the XML::local_utils module.

=head4
Params:

	location	string	The location to be displayed.

=head4
Return:

	string	Returns the xml file.

=head4
Error:

	An exection is thrown if the 
	parameter is not specified, or
	a fatal error occurs.

=cut

sub retrieveFlier
{
	use XML::local_utils;
	my $this = shift;
	my $location = shift;
	my $xsl_object_id = shift;
	my $xml_object_id = shift;
	
	$xsl_object_id = 10632 if !$xsl_object_id;
	
	$xml_object_id = 10631 if !$xml_object_id;
		
	my $mixed_case_location = $location;
	
	$location = uc($location);
	
	$location =~ s/&/&amp;/g;
	$mixed_case_location =~ s/&/&amp;/g;
	
	my $return_xhtml = '';
	
	my $xsl = '';
	my $xml = '';
	
	die 'A location must be specified. ' if(! $location);
	
	eval{
		
		$this->setG_S();
		$xml = $this->{_G_S}->Get_Object($this->{db}, $xml_object_id);
		
		$xsl = $this->{_G_S}->Get_Object($this->{db}, $xsl_object_id);
		
		$return_xhtml = XML::local_utils::xslt($xsl, $xml);
		
	};
	if($@)
	{
		die 'There was an error retrieving the template, and stylesheet. Error: ' . $@;
	}
	
	$return_xhtml =~ s/%%DO_NOT_TRANSLATE_THIS_COUNTRY_CITY_PLACE_HOLDER%%/$location/;
		
	$return_xhtml =~ s/%%DO_NOT_TRANSLATE_THIS_COUNTRY_CITY_PLACE_HOLDER%%/$mixed_case_location/g;
	
	$return_xhtml =~ s/www\.clubshop\.com/<a href="http:\/\/www.clubshop.com" target="_blank" class="nav_footer_black">www.clubshop.com<\/a>/g;
	
	$return_xhtml =~ s/CLUBSHOP\ REWARDS/<span class="Text_Content_bold">ClubShop\ Rewards<\/span>/g;
	
	$return_xhtml =~ s/CLUBSHOP/<span class="Text_Content_bold">ClubShop<\/span>/g;

	$return_xhtml =~ s/REWARD\ POINTS/<span class="Text_Content_bold">Reward\ Points<\/span>/g;
	
	
	return $return_xhtml;
	
	
}


=head3 retrieveFlier

=head4
Description:

	This takes a location, as a 
	string, and displayes the 
	potential merchant flier.

=head4
Prerequisits:

	This method uses the XML::local_utils module.

=head4
Params:

	location	string	The location to be displayed.

=head4
Return:

	string	Returns the xml file.

=head4
Error:

	An exection is thrown if the 
	parameter is not specified, or
	a fatal error occurs.

=cut

sub retrieveFlierLocalMarketing
{
	use XML::local_utils;
	my $this = shift;
	my $location = shift;
	my $xsl_object_id = shift;
	my $xml_object_id = shift;
	
	$xsl_object_id = 10632 if !$xsl_object_id;
	
	$xml_object_id = 10631 if !$xml_object_id;
		
	my $mixed_case_location = $location;
	
	$location = uc($location);
	
	$location =~ s/&/&amp;/g;
	$mixed_case_location =~ s/&/&amp;/g;
	
	my $return_xhtml = '';
	
	my $xsl = '';
	my $xml = '';
	
	die 'A location must be specified. ' if(! $location);
	
	eval{
		
		$this->setG_S();
		$xml = $this->{_G_S}->Get_Object($this->{db}, $xml_object_id);
		$xsl = $this->{_G_S}->Get_Object($this->{db}, $xsl_object_id);
		
		$return_xhtml = XML::local_utils::xslt($xsl, $xml);
		
	};
	if($@)
	{
		die 'There was an error retrieving the template, and stylesheet. Error: ' . $@;
	}
	
	$return_xhtml =~ s/%%DO_NOT_TRANSLATE_THIS_COUNTRY_CITY_PLACE_HOLDER%%/$location/;
		
	$return_xhtml =~ s/%%DO_NOT_TRANSLATE_THIS_COUNTRY_CITY_PLACE_HOLDER%%/$mixed_case_location/g;
	
	if ($xml_object_id == 10662)
	{
		$return_xhtml =~ s/www\.clubshop\.com/<a href="http:\/\/www.clubshop.com" target="_blank" class="nav_footer_black">www.clubshop.com<\/a>/g;
		$return_xhtml =~ s/CLUBSHOP\ REWARDS/<span class="Text_Content_bold">ClubShop\ Rewards<\/span>/g;
		$return_xhtml =~ s/CLUBSHOP/<span class="Text_Content_bold">ClubShop<\/span>/g;
		$return_xhtml =~ s/REWARD\ POINTS/<span class="Text_Content_bold">Reward\ Points<\/span>/g;
	}
	
	return $return_xhtml;
	
	
}


=head3 listPreregisteredMerchants

=head4
Description:

	This takes the County, and State/Provice
	and displays the pre-registered merchants 
	in that area.

=head4
Params:

	hashref	
	{
		country	string	The ISO country code,
		state	string	The ISO state code	
	}

=head4
Return:

	hashref	Returns the store list in a hashref ready to be made into XML.

=head4
Error:

	An exection is thrown if the 
	parameter is not specified, or
	a fatal error occurs.

=cut

sub listPreregisteredMerchants
{
	my $this = shift;
	my $params = shift;
	
	
	die 'There are required parameters, and they were not specified.' if ! $params;
	die 'A country must be specified.' if ! $params->{country};
	die 'A state must be specified.' if ! $params->{state};
	
	$params->{state} =~ s/___/\ /g;
	
	my $stores = {};
	
	
	my $query =<<EOT;
		SELECT
			y2n.yp_heading AS naics_text,
			pma.business_type,
			pma.company_name,
			pma.address1,
			pma.city,
			pma.state,
			pma.country
		FROM
			potential_merchant_affiliates AS pma
			JOIN
			ypcats2naics AS y2n
			ON
			y2n.pk = pma.business_type
		WHERE
			pma.accepted IS NOT NULL

EOT


	my %query_conditions = (
								where=>{
										country=>$params->{country},
										state=> $params->{state}
								},
								'sort'=>'pma.business_type, pma.company_name, pma.address1'
							);

	$query_conditions{'sort'} = $params->{sort_by} if $params->{sort_by};
	
	
	my @query_parameters = $this->buildMerchantListQueryConditions(\%query_conditions);

	my $counter = 0;
	my @categories = ();

#	use Data::Dumper;
#	die Dumper(@query_parameters);
	
	
	eval{
		
		$query .= shift @query_parameters;
		
		my $STH = $this->{db}->prepare($query);
	
		$STH->execute(@query_parameters);
	
		my $previous_category = '';
	
	
		my $td_class = '';
	
		@{$stores->{tr}} = {th=>[$this->{standard_language}->{type_of_business}, $this->{standard_language}->{name_of_business}, $this->{standard_language}->{address_of_business}]};
	
		my $counter = 0;
		while(my $row = $STH->fetchrow_hashref())
		{
			$counter++;
	
			if($previous_category eq $row->{naics_text})
			{
				push @{$stores->{tr}}, {td=>['',$row->{company_name},$row->{address1} . ', ' . $row->{city}], class=>$td_class};
				$td_class = ($td_class eq 'grey') ?'light_grey':'grey';
			} else {
				push @{$stores->{tr}}, {td=>[$row->{naics_text},'',''],class=>'store_type'};
				$td_class = 'light_grey';
				push @{$stores->{tr}}, {td=>['',$row->{company_name},$row->{address1} . ', ' . $row->{city}], class=>$td_class};
				$td_class = 'grey';
			}
	
	
			$previous_category = $row->{naics_text};
		}
	
		if (!$counter)
		{
			push @{$stores->{tr}}, {td=>[$this->{standard_language}->{no_stores}], class=>'light_grey'};
		}
	
	};
	if($@)
	{
		die '<error>' . $@ . " $query</error>\n";
	}
	
	
	return $stores;



}


=head3 buildMerchantListQueryConditions

=head4
Description:

	This buils the where, and sort
	parts for the merchantList method

=head4
Params:

	hashref	
	{
		where hashref {
			country	string	The ISO country code,
			state	string	The ISO state code	
		},
		sort	string	The sort parameters	
	}

=head4
Return:

	string	Returns query parameters, a blank
			string is returned if no 
			parameters are set.

=head4
Error:

	An exection is thrown if a fatal error occurs.

=cut

sub buildMerchantListQueryConditions
{
	my $this = shift;
	my $parameters = shift;
	
	
	my @return = ();

	if (! exists $parameters->{where} && ! exists $parameters->{'sort'} )
	{
		return '';
	}

	my $query = '';

	my $where_counter = 0;
	if(exists($parameters->{where}))
	{
		
		foreach my $key (keys %{$parameters->{where}})
		{

			next if (!$parameters->{where}->{$key});

			$where_counter++;
			$query .= " AND pma.$key = ? ";

			push @return, $parameters->{where}->{$key};
		}

		$query =~ s/\sAND$//;
	}



	if (!$where_counter)
	{
		$query .= ' AND pma.country = ? AND pma.state=?';

		push @return, 'AN';
		push @return, 'CUR';
	}

	if(exists($parameters->{'sort'}) && $parameters->{'sort'} =~ /address1|company_name|business_type/)
	{
		$query .= ' ORDER BY ' . $parameters->{'sort'};
	}

	if ($query !~ /\sORDER\sBY\s/)
	{
		$query .= ' ORDER BY pma.business_type, pma.company_name, pma.address1';
	}

	return ($query, @return);
}

=head3 selectState

=head4
Description:

	This builds a state combo box based
	on states of a country that has
	Potential Merchant Affiliates that
	have been accepted.

=head4
Params:

	$country	string	The ISO country code

=head4
Return:

	string	Returns the state combo box,
			based on country.

=head4
Error:

	An exection is thrown if a fatal error occurs, 
	or the country is not specified.

=cut

sub selectState
{
	my $this = shift;
	my $country = shift;
	my $CGI = shift;
	
	die 'Please select a Country' if $country !~ /\w+/;
	
	my (%hash, $tmp) = ();

	my @list = ('0');
	
	$hash{0} = $this->{standard_language}->{select_state};


	my $sth = $this->{db}->prepare("
					SELECT 
						DISTINCT(m.state) AS state,
						COALESCE(sc.name, m.state) AS state_name
 					FROM
						potential_merchant_affiliates m
						LEFT JOIN 
							state_codes sc
							ON 	
								sc.code=m.state
							AND
								sc.country = m.country
					WHERE 	
						m.accepted IS NOT NULL
						AND
						m.country = ?
					ORDER BY 
						state
	");
	
	$sth->execute($country);
	
	while ($tmp = $sth->fetchrow_hashref)
	{
		my $state_abbreviation = $tmp->{state};
		
		$state_abbreviation =~ s/\s/___/g;
		
		push (@list, $state_abbreviation);
		$hash{$state_abbreviation} = $tmp->{state_name};
	}
	
	$sth->finish;
	
	
	return "<select_label>$this->{standard_language}->{select_state}</select_label>" . $CGI->popup_menu(	
					-name		=> 'state',
					-values		=> \@list,
					-labels		=> \%hash,
					-default	=> undef,
					-onChange	=> 'document.forms[0].submit()'
			);

}


=head3 selectCountry

=head4
Description:

	This takes the ISO State Code and return the country name

=head4
Params:

	$CGI	obj	The CGI object.

=head4
Return:

	string	Returns the state name.

=head4
Error:

	An exection is thrown if a fatal error occurs.

=cut

# TODO: 

sub selectCountry
{
	my $this = shift;
	my $CGI = shift;
	
	my ($tmp) = ();

	# create the top of our list
# for now we only have the US, so we won't prepopulate the list with it
#	my @list = ('', 'US');
	my @list = ('');

	my %hash = ('US' => 'United States');

	my $sth = $this->{db}->prepare("

					SELECT 
						DISTINCT(m.country) AS code,
						c.country_name
 					FROM
						potential_merchant_affiliates m,
						tbl_countrycodes c
					WHERE
						c.country_code=m.country
						AND
						m.accepted IS NOT NULL
					ORDER BY 
					c.country_name

	");

	$sth->execute;
	while ($tmp = $sth->fetchrow_hashref)
	{
		push (@list, $tmp->{code});
		$hash{$tmp->{code}} = $tmp->{country_name};
	}
	$sth->finish;
	
	
	return "<select_label>$this->{standard_language}->{select_country}</select_label>" . $CGI->popup_menu(	
					-name		=> 'country',
					-values		=> \@list,
					-labels		=> \%hash,
					-default	=> undef,
					-onChange	=> 'document.forms[0].submit()'
			);

}

=head3 retrieveCountryName

=head4
Description:

	This takes the ISO County Code and return the country name

=head4
Params:

	$country_code	string	The country code of the country name
							you want to retrieve.

=head4
Return:

	string	Returns the country name.

=head4
Error:

	An exection is thrown if the 
	parameter is not specified, or
	a fatal error occurs.

=cut

sub retrieveCountryName
{
	my $this = shift;
	my $country_code = shift;
	
	die 'A country code must be specified. ' if ($country_code !~ /^\w+$/);

	my $country_name = '';

	my $query =<<EOT;

				SELECT 
					tcc.country_name
				FROM
					tbl_countrycodes AS tcc
				WHERE
					tcc.country_code = ?

EOT

	eval{
		
		my $sth = $this->{db}->prepare($query);
		$sth->execute($country_code);
		$country_name = $sth->fetchrow_hashref;
		$sth->finish();
	
	};
	if($@)
	{
		return '';	
	}
	
	return $country_name->{country_name};

}

=head3 retrieveStateName

=head4
Description:

	This takes the ISO State Code and return the country name

=head4
Params:

	$state_code	string	The state code of the state name
							you want to retrieve.

=head4
Return:

	string	Returns the state name.

=head4
Error:

	An exection is thrown if the 
	parameter is not specified, or
	a fatal error occurs.

=cut

sub retrieveStateName
{
	my $this = shift;
	my $state_code = shift;
	
	die 'A state/province code must be specified. ' if ($state_code !~ /^\w+$/);

	my $state_name = '';

	my $query =<<EOT;

					SELECT 
						DISTINCT(m.state),
						COALESCE(sc.name, m.state) AS state_name
 					FROM
						potential_merchant_affiliates m
						LEFT JOIN 
							state_codes sc
							ON 	
								sc.code=m.state
							AND
								sc.country = m.country
					WHERE 	
						m.state = ?

EOT

	eval{
		
		my $sth = $this->{db}->prepare($query);
		$sth->execute($state_code);
		$state_name = $sth->fetchrow_hashref;
		$sth->finish();
	
	};
	if($@)
	{
		return '';	
	}
	
	return $state_name->{state_name};

}


1;

__END__


=head1 CHANGE LOG:

	2008-10-22	Keith	The query in getPotentialMerchant has been 
						updated to search case insensitive, and the
						results are orderd by the merchants store name.

	2008-10-30	Keith	The retrieveFlier method was added.						
=cut


	
	
