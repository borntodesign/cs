package FTA;

my @approvals = ();
my $count = 0;
#my $nonFTAlimit = 3;	# if they are not registered as a Fees Transmission Agent, they can pay a max of this many monthly fees/upgrades
my $nonFTAlimit = 999999;	# 05/04/15 still trying to put a nail in this coffin

sub new
{
	my $class = shift;
	my $id = shift;
	my $db = shift;
	my $sth = $db->prepare('
		SELECT
			id,
			country_code,
			downline_auth,
			crossline_auth,
			fee_authorized
		FROM fee_transmission_agents_by_country WHERE id=?');
	$sth->execute($id);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @approvals, $tmp;
	}
	
	($count) = $db->selectrow_array("
		SELECT COUNT(DISTINCT s.member_id)
		FROM soa
		JOIN subscriptions s
			ON soa.trans_id=s.soa_trans_id
		JOIN subscription_types st
			ON s.subscription_type=st.subscription_type
		WHERE s.member_id != soa.id
		AND soa.entered >= first_of_month()
	-- we will only check for monthly transactions... they can pay all the annuals they want :)
		AND st.sub_class= 'VM'
		AND s.void=FALSE
		AND soa.void=FALSE
		AND soa.id= ?", undef, $id) || 0;
	
	return bless {'db'=>$db, 'id'=>$id, 'max_count'=>$nonFTAlimit, 'initial_count'=>$count};
}

sub approved
{
	my $me = shift;
	return 1 if @approvals;
	return 1 if $count <= $me->{'max_count'};

	return 0;
}

sub count
{
	return $count;
}

#sub country_authorized_crossline
#{
#	my ($me, $ccode) = @_;
#	my ($rv) = $me->{'db'}->selectrow_array('
#		SELECT country_code
#		FROM fee_transmission_agent_countries
#		WHERE crossline_auth=TRUE
#		AND country_code=?
#		AND id= ?',
#		undef,
#		$ccode,
#		$me->{'id'});
#	return $rv;
#}

sub country_authorized_crossline
{
	my ($me, $ccode) = @_;
	foreach my $row (@approvals)
	{
		return $ccode if $ccode eq $row->{'country_code'};
	}
	return undef;
}

sub update_count
{
	my $me = shift;
	my $inc = shift;
	$count += $inc;
}

1;

__END__

04/08/15	changed from the old VIEW fee_transmission_agents_vw to fee_transmission_agents_by_country