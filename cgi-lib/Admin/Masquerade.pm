package Admin::Masquerade;
use strict;
use CGI::Session;

=head1 Admin::Masquerade

=description

	Created as a more seemless solution to needing to act as a certain party when logged in as admin.
	Instead, we will store that party's info and give it back in place of a regular full login cookie would provide
	
=head2 new

A standard constructor

=head3 Arguments

Arguments should be passed as key/value pairs.
Currently recognized keys are:
	Optional: 'dbh' => a valid DB handle (or reference to scalar that will become one) is required is we are creating a session, otherwise it is not

=head3 Returns

	A new Admin::Masquerade object
	
=cut

sub new
{
	my $class = shift;
	my %args = @_;
	
	my $self = {
		'db'=>$args{'dbh'},
		'Session'=>($args{'Session'} || CGI::Session->new())
	};
	return bless ($self, $class);
}

=head2 Active

=head3 Returns

	True/False (1/0) depending on whether we have an active session or not.
	
=cut

sub Active
{
	my $self = shift;
	return ($self->authen_ses_key_id) ? 1 : 0;
}

=head2 authen_ses_key

	Meant to mimic the function of the G_S version of this method.
	
=head3 Arguments

	None
	
=head3 Returns

	If a valid session exists, returns ($id, $member_hashref), otherwise undef,{}
	
	Note: the *_hash version of this just returns the hashref
		  the *_id version of this just returns the numeric ID

=cut

sub authen_ses_key
{
	my $self = shift;
	my $id = ();
	my $hashref = {};
	$hashref = $self->Session->param('masq_id');
	$id = $hashref->{'id'};
	return ($id, $hashref);
}

sub authen_ses_key_hash
{
	my $self = shift;
	return $self->Session->param('masq_id');
}

sub authen_ses_key_id
{
	my $self = shift;
	return ($self->authen_ses_key())[0];
}

=head2 Clear

	Simply clean out the session

=cut

sub Clear
{
	my $self = shift;
	$self->Session->delete();
	$self->Session->flush();
}

sub db
{
	my $self = shift;

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

=head2 MasqueradeAs

	Setup a session with the particulars of the ID.
	
	Note: new() must have been called with a 'dbh' argument to provide the DB lookup capability.
	
=head3 Arguments

	An ID is required as the first argument.
	The admin user (DB login username) is required as the second argument.
	
=head3 Returns

	True (1) if setup is successfull.
	False (0) if setup is unsuccessfull, like if the ID is missing and/or the admin user is missing
	
	Note: in order for the session to really be setup, the cookie needs to be set. So Session->header should be used in place of CGI->header to ensure that this takes place.
		The cookie expiration is set for 30 minutes, which should be adequate for a masquerading session.
=cut

sub MasqueradeAs
{
	my ($self, $id, $user) = @_;
	unless ($id && $user)
	{
		warn "id or user are missing";
		return 0;
	}
	my $qryend = ($id =~ m/\D/) ? 'alias=?' : 'id=?';
	
	my $h = $self->db->selectrow_hashref("
		SELECT id, spid, alias, emailaddress, firstname, lastname, membertype
		FROM members
		WHERE $qryend
	", undef, $id);
	
	unless ($h)
	{
		warn "No membership returned for ID: $id";
		return 0;
	}
	
	$self->Session->param('masq_id', $h);
	$self->Session->param('masq_user', $user);
	$self->Session->expire('30m');
	return 1;
}

=head2 NoticeBlock

	Render an HTML notice of who we are masquerading as
	
=head3 Returns

	A <table> block with the information if we have a valid session, otherwise an empty string.
	A table is used so as not to spread across the screen.
	
=cut

sub NoticeBlock
{
	my $self = shift;
	return '' unless $self->authen_ses_key_hash();

	return qq|<table><tr><td style="border:1px solid red; padding:1em; background-color:#eee;">
		${\$self->User} is masquerading as ${\$self->authen_ses_key_hash->{'alias'}}, ${\$self->authen_ses_key_hash->{'firstname'}} ${\$self->authen_ses_key_hash->{'lastname'}}
		<div style="text-align:right; font-size:85%;"><a href="/cgi/admin/masq.cgi?action=setmasq;clearmasq=1">Clear Masquerade</a></div>
		</td></tr></table>|;
}

=head2 PrependMasqInfo

=head3 Arguments

	A string, presumably a note/memo that will become part of a record.

=head3 Returns

	The argument string prepended with something like this:
	"${\$self->User} is masquerading as ${\$self->authen_ses_key_hash->{'alias'}} :"

=cut

sub PrependMasqInfo
{
	my $self = shift;
	my $txt = shift || '';
	return $txt unless $self->Active;
	return "${\$self->User} is masquerading as ${\$self->authen_ses_key_hash->{'alias'}} : $txt";
}

=head2 Session

=head3 Arguments

	None

=head3 Returns

	The attached Session object. If this was received in the call to new(), that is the returned object, otherwise, it is a new object.
	
=cut

sub Session { return $_[0]->{'Session'}; }

=head2 User

=head3 Arguments

	None

=head3 Returns

	The Session username or undef if there is none
	
=cut

sub User
{
	return $_[0]->Session->param('masq_user') || '';
}

1;