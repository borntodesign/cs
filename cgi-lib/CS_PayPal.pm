package CS_PayPal;

=head2 CS_PayPal

	Currently a simple wrapper around Business::PayPal::API and friends.
	If we ever want to migrate to another package, we can hopefully do so without missing a beat by remapping the method calls used with Business::PayPal to another package's versions.
	
	One additional thing we keep in here is the authentication credentials.
	
=cut

use strict;
use Business::PayPal::API qw( ExpressCheckout ); 

my $urlFlag = 'test';
my %destinationURLs = (
	'ExpressCheckout' => {
		'test' => 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout',
		'live' => 'https://www.paypal.com/webscr?cmd=_express-checkout&token='
	}

);

=head3 API

	Currently you can just call API and get back a Business::PayPal::API object and use the methods provided by it and helper modules
	
=head4 Arguments

	We are feeding the real arguments to Business::PayPal::API->new() from within,
	but we do accept one argument to new() here, a hashref with one key/value, ie. {test=>1} or {test=>0}
	If test=>1 then we will use the PayPal sandbox and our sandbox credentials, otherwise will be using all live
	
=head4 Returns

	A Business::PayPal::API object
	
=cut
	
sub API
{
	my ($class, $args) = @_;
	my %creds = ();
	if (! $args || ! $args->{'test'})	# we are doing live
	{
		%creds = (
			'Username'   => 'info_api1.proprofit.co.uk',
			'Password'   => 'DZGY3H8GWC69CRZF',
			'Signature'  => 'AFcWxV21C7fd0v3bYYYRCpSSRl31ANhtlEOJeXOl2NryZEds8gZQd-gQ',
			'sandbox'    => 0
		);
		$urlFlag = 'live';
	}
	else
	{
		%creds = (
			'Username'   => 'acfspvr-facilitator_api1.dhs-club.com',
			'Password'   => 'EBKQYBHWMQ4QDTG7',
			'Signature'  => 'ABudjSyOGracBlij6qUr9PbqVOxmANhgK2SBmjyR-T7yuNFqqXKmTTc3',
			'sandbox'    => 1
		);
	}
	
	$Business::PayPal::API::Debug = 1 if $args->{'DEBUG'};
	
	return new Business::PayPal::API( %creds );
}

=head3 new()

	Create a CS_PayPal object for utility methods like getting destination URLs.

=head4 Arguments

	None. We are expecting that you will be calling API() at some point before calling any methods of this object in order to determine if we are using sandbox or live.

=head4 Returns

	A CS_PayPal object;
	
=cut

sub new
{
	my $class = shift;
	return bless {}, $class;
}

sub destinationURL
{
	my ($self, $args) = @_;
	
	die unless $args && $args->{'tag'};
	
	if ($args->{'tag'} eq 'ExpressCheckout')
	{
#		die "A token is required for this URL" unless $args->{'token'};
#		return $destinationURLs{$args->{'tag'}}->{$urlFlag} . $args->{'token'};
		return $destinationURLs{$args->{'tag'}}->{$urlFlag};
	}
	
	die "Unexpected tag: $args->{'tag'}";
}

=head3 ExpressCheckoutGif

	In order to provide multiple language buttons, we have to use a map as it doesn't seem that PayPal offers buttons based upon language negotiation.
	This method will provide the appropriate URL for the button based upon the language pref and the country, if we have it mapped.

=head4 Arguments

	A language code
	A country code

=head4 Returns

	A URL something like this: https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif where the 'en_US' will represent a language/country specific button
	
=cut

sub ExpressCheckoutGif
{
	my ($self, $lang_pref, $country) = @_;

	# these are the images we are choosing to display
	my @map = (
		'de_DE',
		'en_AU',
		'en_US',
		'en_GB',
		'it_IT',
		'fr_CA',
		'fr_FR',
		'nl_NL',
		'es_ES',
		'ru_RU'
	);
	
	my %langMap = (
		'en' => 'en_US',
		'fr' => 'fr_FR',
		'it' => 'it_IT',
		'es' => 'es_ES',
		'nl' => 'nl_NL',
		'de' => 'de_DE',
		'ru' => 'ru_RU'
	);
	
	my $fc = ();

	if ($country && $lang_pref)
	{
		my $lang_country = $lang_pref . '_' . $country;
		$fc = $lang_country if grep $_ eq $lang_country, @map;
	}

	if ((! $fc) && $lang_pref)
	{
		$fc = $langMap{$lang_pref};
	}

	if ((! $fc) && $country)
	{
		$fc = $langMap{ lc($country) } || $langMap{ $country };
	}
	
	$fc ||= 'en_US';	# our default
	
	# the base URL as of April 2013 is something like this: https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif , where the language code and country are part of the URL
	return "https://www.paypal.com/$fc/i/btn/btn_xpressCheckout.gif";
}

sub HandleErrors
{
	my ($self, $resp) = @_;
	return undef if $resp->{'Ack'} eq 'Success';
	my $rv = '';
	foreach my $err (@{$resp->{'Errors'}})
	{
		$rv .= "Error code: $err->{'ErrorCode'} - $err->{'LongMessage'}<br />";
	}
	return $rv;
}
1;
