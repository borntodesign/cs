package MerchantAffiliates;
use strict;
use Members;
use MailTools;
require G_S;
use Vendors;
use XML::Simple;
use DHSGlobals;
use Lingua::EN::NameCase 'NameCase';
require RewardCards;
use Encode;
use Data::Dumper;

# all typo errors courtesy of Keith Hasely, the original author

###### starting on 08/26/11 we are revising this package to begin removing eval blocks and 'nice' error handling
###### instead of trying to make friendly errors, we are going to DIE
###### if the calling code is evaled itself, then it can deal with the die, otherwise we will die

#At some point we may want to enable this option so cities like el Serrania will be El Serrania.
#Ideally we would detect a countries predominant language, or determin if a country is heavily
#influence by the spanish language move this option to the methods that are using NameCase(), 
#and enable this option when necessary.
#$Lingua::EN::NameCase::SPANISH = 1;

=head1
NAME: MerchantAffiliates

	This class is ment to manage information pertaining
	to Merchant Affiliates

=head1
VERSION:

	.6

=head1
SYNOPSIS:

	my $returned_hashref = {};
	my %submitted_data = ();
	$submitted_data{pk}	= $CGI->param('pk') ? $CGI->param('pk') : '';
	$submitted_data{'id'}	= $CGI->param('id') ? $CGI->param('id') : '';

	eval{
		my $MerchantAffiliates = MerchantAffiliates->new($DB);
		$returned_hashref = $MerchantAffiliates->updateMerchantInformation($submitted_data);
	};
	if($@)
	{
		#How ever you want to handle the error.
	}


=head1 DESCRIPTION

	This class is ment to manage information pertaining
	to Merchant Affiliates.  Creating, updateing, etc., using the Vendors,
	and Members classes as sub classes to handle Vendor, and Member Information.

=head2 CONSTRUCTOR

=head3
new

=head4
Params:

	db obj to a database connection
	An optional G_S object.
		This was added years after original development, but it will help mitigate operational costs for greater use of G_S for UTF8 stuff
		by reusing an existing object instead of G_S having to recreate one if used functionally.

=head4
Returns:

	obj $this

=cut

sub new
{
	
   my $class = shift;
   my $this = {
   			'__name' => 'MerchantAffiliates',
   			'__version' => 0.2,
   			'merchant_affiliates_info_by_id' => {},
   			'__merchant_package_names_by_id' =>['free','bronze','silver','gold','platinum'],# There are more "__merchant_package_names_by_id" after the $this declaration
   			'__merchant_package_id_by_name' => {'free'=>0,'bronze'=>1,'silver'=>2,'gold'=>3,'platinum'=>13},
   			'__discount_type__subscription_type__mapping' => {
				0=>6,#Free
				1=>7,#Bronze
				2=>8,#Silver
				3=>9,#Gold
				#4=>10,
				#5=>11,
				#6=>12,
				#7=>13,
				#8=>14,
				#9=>15
				10=>7,#Bronze 
				11=>8,#Silver
				12=>9,#Gold
				13=>26,#Platinum
				14=>31,#Basic Change the subscription ID
				15=>6,#Free Change the subscription ID
                                20=>7,#online_mall bronze
                                21=>8,#online_mall silver
                                22=>9,#online_mall gold
                                23=>26,#online_mall platinum
                                24=>31,#online_mall basic
   			},
   			'__merchant_type_by_package' =>
   			{
					0=>'offline',
					1=>'offline',
					2=>'offline',
					3=>'offline',
					10=>'offline_rewards',#Bronze 
					11=>'offline_rewards',#Silver
					12=>'offline_rewards',#Gold
					13=>'offline_rewards',#Platinum
					14=>'offline_rewards',#Basic
					15=>'offline_rewards',#Free
					20=>'online_mall',#Bronze
					21=>'online_mall',#Silver
					22=>'online_mall',#Gold
					23=>'online_mall',#Platinum
					24=>'online_mall'#Basic
   			},
   			'__package_upgrade_values' =>
#This is used to compare packages to see if the merchant is upgrading their package, or just changing 
#to the non tracking option.
#These values are also hard coded in the Offline Merchant Listing in the Mall, so if you update these the query for the
#mall listing should also be updated.
				{
					0=>1, # Tracking Free
					1=>20, # Tracking Bronze
					2=>30, # Tracking Silver
					3=>40, # Tracking Gold
					15=>1, # Free
					14=>10, # Basic
					10=>20, # Bronze 
					11=>30, # Silver
					12=>40, # Gold
					13=>50, # Platinum
				},
				#_keyword_length is the length of the "keyword" field in the "merchant_keywords" table.
   			'_keyword_length' => 20
	};
	
	$this->{'__merchant_package_names_by_id'}->[10] = 'bronze';
	$this->{'__merchant_package_names_by_id'}->[11] = 'silver';
	$this->{'__merchant_package_names_by_id'}->[12] = 'gold';
	$this->{'__merchant_package_names_by_id'}->[13] = 'platinum';
	$this->{'__merchant_package_names_by_id'}->[14] = 'basic';
	$this->{'__merchant_package_names_by_id'}->[15] = 'free';
	$this->{'__merchant_package_names_by_id'}->[20] = 'bronze';
	$this->{'__merchant_package_names_by_id'}->[21] = 'silver';
	$this->{'__merchant_package_names_by_id'}->[22] = 'gold';
	$this->{'__merchant_package_names_by_id'}->[23] = 'platinum';
	$this->{'__merchant_package_names_by_id'}->[24] = 'basic';

	
	$this->{'db'} = shift;
	$this->{'G_S'} = shift;
	
	bless($this, $class);
	
	
   return $this;
}


=head2 METHODS

=head3 PRIVATE

=cut

=head3 __updateCache

=head4
Description:

	Get the merchants information by the Merchants ID,
	and assign the data to the cache ($this->{'merchant_affiliates_info_by_id'}->{'merchant_id'}).
	Ideally if the Cache needs to be update, just delete the Merchants Cache
	(delete $this->{'merchant_affiliates_info_by_id'}->{'merchant_id'}), and the
	next time you call $this->getMerchantMaster(MERCHANT_ID) the updated information
	will be cached.

=head4
Params:

	hashref
		id		string	The 'Merchants ID'

=head4
Returns:

	string	on error

=cut

sub __updateCache
{
	my $this = shift;
	my $args = shift;

	if (! exists $args->{'id'} || $args->{'id'} !~ /^\d+$/)
	{
		die "The 'id' parameter is required, and must be an integer.";
	}

	my $sth = $this->{'db'}->prepare("
		SELECT mam.*, mpp.pay_payment_method AS pending_payment_method
		FROM merchant_affiliates_master mam
		LEFT JOIN merchant_payment_pending mpp
			ON mam.id = mpp.merchant_id
		WHERE mam.id = ?");

	$sth->execute($args->{'id'});

	my $rows = $sth->fetchrow_hashref;

	$this->{'merchant_affiliates_info_by_id'}->{$rows->{'id'}} = $rows;

	return;
}


=head3 PUBLIC

=head3 setMembers

=head4
Description:

	Instanciates the "Members" as a subclass, if it is not already instanciated

=cut

sub setMembers
{
	my $this = shift;

	eval{
		$this->{'Members'} = Members->new($this->{'db'}) if ! exists $this->{'Members'};
	};
	if($@)
	{
		die 'There was an issue instanciating Members. Error: ' . $@;
	}
	
	
}

=head3 setVendors

=head4
Description:

	Instanciates the "Vendors" class as a subclass, if it is not already instanciated.

=cut

sub setVendors
{
	my $this = shift;

	eval{
		$this->{'Vendors'} = Vendors->new($this->{'db'}) if ! exists $this->{'Vendors'};
	};
	if($@)
	{
		die 'There was an issue instanciating Vendors. Error: ' . $@;
	}
	
	
}

=head3 setRewardCards

=head4
Description:

	Instanciates the "RewardCards" class as a subclass, if it is not already instanciated.

=cut

sub setRewardCards
{
	my $this = shift;

	eval{
		$this->{RewardCards} = RewardCards->new($this->{'db'}) if ! exists $this->{RewardCards};
	};
	if($@)
	{
		die 'There was an issue instanciating Vendors. Error: ' . $@;
	}
	
	
}

=head3 updateMasterMemberId

=head4
Description:

	Update the merchant_affiliates_master
	table so the member_id is set for this merchant.

=head4
Params:

	hashref
		id	int	The id (primary key) from the merchant_affiliates_master
				table for this Merchant Master Record

		member_id	int	The merchants member ID.

=head4
Returns:

	null

=cut

sub updateMasterMemberId
{
	my $this = shift;
	my $params = shift;

	if (! exists $params->{'member_id'} || ! exists $params->{'id'} || $params->{'member_id'} !~ /^\d+$/ || $params->{'id'} !~ /^\d+$/)
	{
		return "The members 'member_id', and 'id' are required parameters, and they must be integers (numbers).";
	}
	
	$params->{'member_id'} =~ s/\D//g;
	$params->{'member_id'} =~ s/^0*//g;
	
	$params->{'id'} =~ s/\D//g;
	$params->{'id'} =~ s/^0*//g;
	
	my @merchant_information = ($params->{'member_id'}, $params->{'id'});

	my $update_query = 'UPDATE merchant_affiliates_master SET member_id=? WHERE id =?';

	eval{
		$this->{'db'}->do($update_query, undef, @merchant_information);
	};
	if($@)
	{
		die "MerchantAffiliates::updateMasterMemberId({member_id=>$params->{'member_id'}, id=>$params->{'id'}}) - There was an error executing the query. <br />Query: $update_query <br />params->{'member_id'}: $params->{'member_id'} <br />params->{'id'}: $params->{'id'} <br />" . $@;
	}

	return;
}

=head3 updateVendorStatusByMerchantID

=head4
Description:



=head4
Params:


	id	int	The id (primary key) from the merchant_affiliates_master table for this Merchant Master Record

	status	int	The status to change to.

=head4
Returns:

	null

=cut

sub updateVendorStatusByMerchantID
{
	my $this = shift;
	my $id = shift;
	my $status = shift;

	return "The 'id' is a required parameters, and must be an integers (numbers)." if (! $id || $id !~ /^\d+$/);

	my @merchant_information = ($status, $id);

	my $update_query = 'UPDATE vendors SET status= ? WHERE vendor_id IN ( SELECT mal.vendor_id FROM merchant_affiliate_locations mal WHERE mal.merchant_id = ?)';

	eval{
		$this->{'db'}->do($update_query, undef, @merchant_information);
	};
	if($@)
	{
		die "MerchantAffiliates::updateVendorStatusByMerchantID($id, $status) - There was an error executing the query. <br />Query: $update_query <br />id: $id <br />status: $status <br />" . $@;
	}

	return;
}

=head3 updateVendorBannerByMerchantID

=head4
Description:

	Updates the Merchants banner by the Merchants ID.
	The Merchant ID is used to retrieve the Merchants Location
	ID, then the coupon field in the Vendor table is updated
	for all the Merchants Locations.

=head4
Params:


	id	int	The Merchant ID

	coupon	string	The coupon field is used for the merchant banner.

=head4
Returns:

	null

=cut

sub updateVendorBannerByMerchantID
{
	my $this = shift;
	my $id = shift;
	my $coupon = shift;

	return "The 'id' is a required parameters, and must be an integers (numbers)." if (! $id || $id !~ /^\d+$/);

	my @merchant_information = ($coupon, $id);

	my $update_query = 'UPDATE vendors SET coupon= ? WHERE vendor_id IN ( SELECT mal.vendor_id FROM merchant_affiliate_locations mal WHERE mal.merchant_id = ?)';

	eval{
		$this->{'db'}->do($update_query, undef, @merchant_information);
	};
	if($@)
	{
		die "MerchantAffiliates::updateVendorBannerByMerchantID($id, $coupon) - There was an error executing the query. <br />Query: $update_query <br />id: $id <br />coupon: $coupon <br />" . $@;
	}

	####################################################
	# This may be needed later on.
	#
	# $this->__updateCache({id=>$params->{'id'}});
	####################################################

	return;


}

=head3 checkPendingEmailDuplicates

=head4
Description:

	Check to see if the email address is used by a Pending Merchant.

=head4
Params:

	email					string	The email address to check.
	unapproved_merchants	bool	(Optional) Set to 1 if you only want to check Pending Merchants who have not been approved yet.
												This is here so if a Merchant is updateing their information, and they at one time
												had a pending application we can over look it.

=head4
Returns:

	int	0 if the email address already exists in the merchant_pending table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=head4
TODO:

	Check to see if this method is used any where.  This should be in the PotentialMerchantAffilates Class.

=cut

sub checkPendingEmailDuplicates
{
	my $this = shift;
	my $email = shift;
	my $unapproved_merchants = shift;
	
	die 'The email address is a required parameter. ' if (! defined $email || ! $email);
	
	if(!$unapproved_merchants)
	{
		return 0 if $this->{'db'}->selectrow_array('SELECT id FROM merchant_pending WHERE emailaddress1= ? OR emailaddress2= ?', undef, $email, $email);
	}
	else
	{
		return 0 if $this->{'db'}->selectrow_array('SELECT id FROM merchant_pending WHERE (status IS NULL OR status = 0) AND (emailaddress1= ? OR emailaddress2= ?)', undef, $email, $email);
	}
	return 1;

}

=head3 checkLocationUsernameDuplicates

=head4
Description:

	Check the "merchant_affiliate_locations" table to see if the supplied username is already in use.

=head4
Params:

	username	string	The username to check.

=head4
Returns:

	int	0 if the username address already exists in the merchant_affiliate_locations table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=cut

sub checkLocationUsernameDuplicates
{
	my $this = shift;
	my $username = shift;
	
	die 'The username address is a required parameter. ' if (! defined $username || ! $username);
	
	return 0 if $this->{'db'}->selectrow_array('SELECT id FROM merchant_affiliate_locations WHERE username ILIKE ?', undef, $username);

	return 1;

}

=head3 checkMasterUsernameDuplicates

=head4
Description:

	Check the "merchant_affiliates_master" table to see if the username is already in use.

=head4
Params:

	username	string	The username to check.

=head4
Returns:

	int	0 if the username address already exists in the merchant_affiliates_master table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=cut

sub checkMasterUsernameDuplicates
{
	my $this = shift;
	my $username = shift;
	
	die 'The username address is a required parameter. ' if (! defined $username || ! $username);
	
	return 0 if $this->{'db'}->selectrow_array('SELECT id FROM merchant_affiliates_master WHERE username ILIKE ?', undef, $username);

	return 1;

}


=head3 checkLocationNameUrlDuplicates

=head4
Description:

	Check the "merchant_affiliate_locations" table to see if the "location_name_url" is already in use.

=head4
Params:

	location_name_url
	string
	The "location_name_url" to check.
	
	country
	string
	The country code.
	
	state
	string
	The province (this can be the ISO code, or spelt out)
	
	city
	string
	The city
	
	merchant_location_id (Optional)
	int
	The ID for the merchant location record, if this is specified the merchants location record is skipped.

=head4
Returns:

	int	The number of "location_name_url" already exists in the "merchant_affiliate_locations" table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=cut

sub checkLocationNameUrlDuplicates
{
	my $this = shift;
	my $location_name_url = shift;
	my $country = shift;
	my $state = shift;
	my $city = shift;
	my $merchant_location_id = shift;
	
	my $exclude_merchant_location_query_part = undef;
	
	die 'MerchantAffiliates::checkLocationNameUrlDuplicates - The location_name_url is a required parameter. ' if (! defined $location_name_url || ! $location_name_url);
	die 'MerchantAffiliates::checkLocationNameUrlDuplicates - The country is a required parameter. ' if (! defined $country || ! $country);
	die 'MerchantAffiliates::checkLocationNameUrlDuplicates - The state is a required parameter. ' if (! defined $state || ! $state);
	die 'MerchantAffiliates::checkLocationNameUrlDuplicates - The city is a required parameter. ' if (! defined $city || ! $city);
	die "MerchantAffiliates::checkLocationNameUrlDuplicates - The merchant_location_id was specified, but it must be an integer. merchant_location_id: $merchant_location_id" if (defined $merchant_location_id && $merchant_location_id !~ /^\d+$/);
	
	
	my @values = ($country, $state, $city, $location_name_url);
	
	
	if($merchant_location_id)
	{
		$exclude_merchant_location_query_part = ' AND id <> ? ';
		push @values, $merchant_location_id;
	}
	
	
	
	my $query_location_name_count =<<EOT;

SELECT
	COUNT(mal.location_name_url) AS url_name_count
FROM
	merchant_affiliate_locations mal
WHERE 
	mal.country = ?
 	AND
	mal.state = ?
	AND
	mal.city = ?
	AND
	mal.location_name_url = ?
	
	$exclude_merchant_location_query_part

EOT
	
	my $row = $this->{'db'}->selectrow_hashref($query_location_name_count, undef, @values);
	
	return $row->{'url_name_count'};


}

			
=head3 checkPendingUsernameDuplicate

=head4
Description:

	Check to see if the "username" is used in the "merchant_pending" table

=head4
Params:

	username	string	The username to check.

=head4
Returns:

	int	0 if the username address already exists in the merchant_pending table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=head4
TODO:

	Check to see if this method is used any where.  This should be in the PotentialMerchantAffilates Class.

=cut

sub checkPendingUsernameDuplicate
{
	my $this = shift;
	my $username = shift;
	
	die 'The username address is a required parameter. ' if (! defined $username || ! $username);
	
	return 0 if $this->{'db'}->selectrow_array('SELECT id FROM merchant_pending WHERE username ILIKE ?', undef, $username);

	return 1;

}

=head3 =head3 getMerchantMaster

=head4
Description:

	Returns the maximum length of a keyword.  Ideally this would
	be extracted from the table this information is stored in, but
	there is no database cache right now, so well hard code it.

=head4
Params:

	none

=head4
Returns:

	int

=cut

sub getKeywordLength
{
	my $this = shift;
	
	return $this->{'_keyword_length'};
	
}

=head3 getMerchantDiscounts

=head4
Description:
	
	Load the records from merchant_discounts into $this->{'merchant_discounts'} if they have not already been loaded.
	The data will be loaded using selectall_hashref using the "type" as the hash key.

=head4
Returns:

	For now, we will just return $this->{'merchant_discounts'};

=cut

sub getMerchantDiscounts
{
	my $this = shift;
	if (! $this->{'merchant_discounts'})
	{
		$this->{'merchant_discounts'} = $this->{'db'}->selectall_hashref('
			SELECT "type", "name", discount, rebate, reb_com, cap_amount, notes, pp, sub_type, tracking FROM merchant_discounts', 'type');
	}
	return $this->{'merchant_discounts'};
}

=head3 getMerchantMaster

=head4
Description:

	If the Merchants Id is passed in as a param,
	and the information is in the cache return 
	the cached information, otherwise update the cache, 
	then return the merchant information.
	
	If the Merchant ID is passed in only one record
	will be returned as an arrayref of hashref ([{id=>,etc=>}]).
	
	If a parameter besides the Merchant ID is pased 
	in, any of the fields in the "merchant_affiliates_table" 
	may be used, those fields will be used to narrow the
	search, and multiple merchants may be returned.
	
	The search uses the ILIKE statement so the "%" wildcard
	can be used.

=head4
Params:

	hashref The key is the field to search, and the
			value is the value to search for. If
			one of the keys are "id" one row is
			returned from the cache.

=head4
Returns:

	hashref on failure return null

=cut

sub getMerchantMaster
{
	my $this = shift;
	my $params = shift;
	my $query = '
		SELECT mam.*, mpp.pay_payment_method AS pending_payment_method
		FROM merchant_affiliates_master mam
		LEFT JOIN merchant_payment_pending mpp ON mam.id = mpp.merchant_id
		WHERE ';

	my $fields = '';
	my @values = ();
	my $return_arrayref = ();
	
	if(exists $params->{'id'} && $params->{'id'})
	{
		if(exists $this->{'merchant_affiliates_info_by_id'}->{$params->{'id'}})
		{
			push @$return_arrayref, $this->{'merchant_affiliates_info_by_id'}->{$params->{'id'}};
			return $return_arrayref
		}
		else
		{
			my $error = $this->__updateCache({'id'=>$params->{'id'}});
			die $error if($error);

			push @$return_arrayref, $this->{'merchant_affiliates_info_by_id'}->{$params->{'id'}};
			return $return_arrayref;
		}
	}

	########################################
	# Build the fields to query with.
	########################################
	foreach my $key (keys %$params)
	{
		
		next if (! $key || ! exists $params->{$key} || $params->{$key} =~ /^\s*$/);

		if($key eq 'status')
		{
			
			my @split_values = split(/,/, $params->{$key});
			
			my @place_holders = ();
			
			foreach (@split_values)
			{
				push @place_holders, '?';
				push @values, $_;	
			}
			
			$fields .= "mam.$key IN (" . join(',',@place_holders) . ") AND ";
			
		}
		else
		{
			$fields .= "mam.$key ILIKE ? AND ";
			push @values, $params->{$key};
		}
	}
	
	$fields =~ s/AND\s*$//;
	
	return if !$fields;
	
	$query .= $fields;

	$query .= ' ORDER BY mam.business_name';
	
	eval {
#		warn "Merch Line 859: " . $query;
		
		my $STH = $this->{'db'}->prepare($query);
		
		$STH->execute(@values);
		
		while(my $rows = $STH->fetchrow_hashref())
		{
			push @$return_arrayref, $rows;
			
			#Add the returned merchants to the cache.
			$this->{'merchant_affiliates_info_by_id'}->{$rows->{'id'}} = $rows;
			
		}
	};

	if($@)
	{
		die "There was an issue with retreiving the information.  <br />Query: $query <br /> " .  $@;
	}

	return $return_arrayref;
}

=head3 deleteMerchantRecord

=head4
Description:

	This PERMANENTLY deletes the Merchants information.
	* This is untested, and we may not be able to remove all merchants.  If a merchant has transactions our system wont allow the merchant to be deleted.

=head4
Params:

	id	int	The primary key from the merchant_affiliates_master table.

=head4
Return:

	undef/string	undefefined on sucess, and a message on error.

=head4
TODO:

	Look into adding a delete for all locations associating with the master record.  
	We will also need a method for deleteing locations, and marking locations as inactive (in the event a merchant has a fire, or some other reson to temporarily close )
	The marchants Banner should be deleted, when the merchant account is deleted.

=cut

sub deleteMerchantRecord
{
	my $this = shift;
	my $id = shift;
	
	my $deleted_rows = 0;
	
	die 'The required parameter was not passed, or is in an inncorrect format. It must be an integer. ' if $id !~ /^\d+$/;
	
	my $query  = 'DELETE FROM merchant_affiliates_master WHERE id = ?';
	
	eval {
		$deleted_rows = $this->{'db'}->do($query, undef, ($id));
	};
	
	if($@)
	{
		return 'The merchant record was not deleted.  ID:' . $id . ' Error: ' . $@;
	}
	
	if($deleted_rows == 1)
	{
		return;
	} 
	elsif(!$deleted_rows)
	{
		return 'The Merchant was not removed. ID: ' . $id;
	}
	
	return 'More than one Merchant record was deleted.  Number of rows deleted: ' . $deleted_rows . ' ID: ' . $id;
	
}


=head3 deleteMerchantPaymentPendingRecord

=head4
Description:

	This PERMANENTLY deletes the Merchants Payment Pending information.
	The Merchants Payment Pending record is used when they are requesting
	to upgrade their package, but have not transfered the funds yet.

=head4
Params:

	id	int	The Merchants ID.

=head4
Return:

	int	The number of rows deleted.

=cut

sub deleteMerchantPaymentPendingRecord
{
	my $this = shift;
	my $id = shift;
	
	my $deleted_rows = 0;
	
	die 'The required parameter was not passed, or is in an inncorrect format. It must be an integer. ' if $id !~ /^\d+$/;
	
	my $query  = 'DELETE FROM merchant_payment_pending WHERE merchant_id = ?';
	
	eval{
		$deleted_rows = $this->{'db'}->do($query, undef, ($id));
	};
	if($@)
	{
		return 'The merchant_payment_pending record was not deleted.  Merchant ID:' . $id . ' Error: ' . $@;
	}
	
	return $deleted_rows;
	
}

=head3 updateMerchantMasterInformation

=head4
Description:

	This updates the Merchants Master Record information.

=head4
Params:

	merchant_master_id	int	The Merchants Master ID
	
	params	hashref
				should be a hashref where the keys
				are named the same as the fields in
				the merchant_affiliates_master table.
				If there is an accepted key in the
				hashref set to true the accepted
				field will be updated to the current
				date.  If the "member_id_assigned",
				or "id" field are missing a new "x"
				member will be created for this
				potential merchant.
				
				If the "tin" key is defined the
				'merchants_member_id' key must also exist.
				The 'tin' field is in the mmebers
				table, so the merchants_member_id is needed
				to update the tin field.
	
	merchants_member_id	int	(optional) This is not currently being used
	
	master_location_id	int	(optional) The Master Location ID, if it does not exist we 
						   will use the location id that was created first.
	
	subscription	hashref	(optional) 
	     		{
				status	string 'UPGRADE', 'DOWNGRADE', or 'UPDATE'
				description	string This is used only if status is 
				set to downgrade, this should be a description of 
				the downgrade.
			 					
			}if the Merchant updates/changes their merchant package, 
			this should be set to true.
			If you add a subscription the params->{'member_id'}, 
			and params->{'discount_type'} need 
			to be included in the params parameter.
											
	debit_club_account	arrayref of hashrefs
		[ 
			{
				transaction_type	Int	This should be a value from the 
				                                "trans_type" field in the 
								"transaction_types" table,
			       	amount	Double (IN USD)	The amount to credit the new members club account,
			        description	String	A description of the credit to the club account of the new member,
				operator	String	The name of the person, or process creating this,
				member_id	Int	(Optional) If a Members ID is specified 
							the transaction is created 
							for that member, if it is not 
							specified the transaction is 
							made for the MA Membership 
							that was created.
			  },
		   ]
							

=head4
Return:

	the number of rows effected.

=cut
#TODO: Look into the subscription parameter, and see if we need to change anything here so if a ca_number is available we need to debit that club account.
sub updateMerchantMasterInformation($$$;$$$$)
{
	my $this = shift;
	my $merchant_master_id = shift;
	my $params = shift;
	my $merchants_member_id = shift;
	my $master_location_id = shift;
	my $subscription = shift;
	my $debit_club_account = shift;
	
	
	#warn "debit_club_account: \n" . Dumper($debit_club_account);

	my $return = 0;
	
	die 'No parameters were passed.' if ! $params;

	die 'The merchant_master_id is a required parameter.' if ! $merchant_master_id;
	
	die "'subscription->{'status'}' must be set as 'UPGRADE', 'DOWNGRADE', 'UPDATE', or the 'subscription' parameter should be set to undef." if (exists $subscription->{'status'} && $subscription->{'status'} !~ /UPGRADE|DOWNGRADE|UPDATE/i);
	
	if (exists $subscription->{'status'} && $subscription->{'status'})
	{
		die 'The member_id, and discount type are required to create a subscription, but they do not exist.' if ( ! exists $params->{'member_id'} || ! exists $params->{'discount_type'} || $params->{'member_id'} !~ /^\d+$/ || $params->{'discount_type'} !~ /^\d+$/);
		
		$subscription->{'status'} = uc($subscription->{'status'});
	}
	
	
	if(ref($debit_club_account) eq 'ARRAY' && $debit_club_account->[0])
	{
		foreach (@$debit_club_account)
		{
			die "MerchantAffiliates::updateMerchantMasterInformation - The debit_club_account->{'transaction_type'} parameter was specified, but did not hold the correct information, only Integers ar allowed for this parameter.  $_->{'transaction_type'} " if ($_->{'transaction_type'} !~ /\d+/);
			#TODO: Add a check here for a data type of double
			#die "MerchantAffiliates::updateMerchantMasterInformation - The debit_club_account->{'amount'} ($_->{'amount'}) parameter was specified, but did not hold the correct information, only posative Doubles are allowed. " if ($_->{'amount'} );
			die "MerchantAffiliates::updateMerchantMasterInformation - The debit_club_account->{'description'} parameter was specified, but did not hold the correct information." if ! $_->{'description'};
			die "MerchantAffiliates::updateMerchantMasterInformation - The debit_club_account->{'operator'} parameter was specified, but did not hold the correct information, this should hold the name of the person, or process calling this method." if ! $_->{'operator'};
			die "MerchantAffiliates::updateMerchantMasterInformation - The debit_club_account->{'member_id'} parameter was specified, but did not hold the correct information, only integers are allowed." if(exists $_->{'member_id'} && $_->{'member_id'} !~ /^\d+$/);
			
		}
	}
	
	my $set_parameters = '';
	my %merchant_master_info = ();
	my %merchant_location_info = ();
	my %merchant_vendor_discount_info = ();
	my %merchant_banner = ();
	
#	$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
#	my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");
	
#	warn "Merch line 1116: " . Dumper($params);
	my $merchant_config = $this->retrieveMerchantConfigHashRef();
#	warn Dumper($merchant_config);
	$params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};
	
	($merchant_master_info{'member_id'} = $params->{'member_id'}) =~ s/\D//g if $params->{'member_id'};
	$merchant_master_info{'member_id'} =~ s/^0*//g if defined $merchant_master_info{'member_id'};
	
	($merchant_master_info{'referral_id'} = $params->{'referral_id'}) =~ s/\D//g if $params->{'referral_id'};
	$merchant_master_info{'referral_id'} =~ s/^0*//g if defined $merchant_master_info{'referral_id'};
	
	#TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Business Name.
	
	my $string_length = (defined $params->{'business_name'} && $params->{'business_name'}) ? length($params->{'business_name'}):0;
	if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
	{
		my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
		$params->{'business_name'} = substr($params->{'business_name'}, 0, - $characters_to_remove);
	}

        
	
	
	$merchant_location_info{'location_name'} = $merchant_master_info{'business_name'} = $params->{'business_name'} if $params->{'business_name'};
	
	$merchant_master_info{'firstname1'} = $params->{'firstname1'} if $params->{'firstname1'};
	$merchant_master_info{'lastname1'} = $params->{'lastname1'} if $params->{'lastname1'};
	
	$merchant_location_info{'address1'} = $merchant_master_info{'address1'} = $params->{'address1'} if $params->{'address1'};
	
	$merchant_location_info{'address2'} = $params->{'address2'} if defined $params->{'address2'};
	$merchant_location_info{'address3'} = $params->{'address3'} if defined $params->{'address3'};
 
	$merchant_location_info{'city'} = $merchant_master_info{'city'} = $params->{'city'} if $params->{'city'};
	$merchant_location_info{'state'} = $merchant_master_info{'state'} = $params->{'state'} if $params->{'state'};
	$merchant_vendor_discount_info{'country'} = $merchant_location_info{'country'} = $merchant_master_info{'country'} = $params->{'country'} if $params->{'country'};
	$merchant_location_info{'postalcode'} = $merchant_master_info{'postalcode'} = $params->{'postalcode'} if $params->{'postalcode'};
	$merchant_location_info{'longitude'} = $merchant_master_info{'longitude'} = $params->{'longitude'} if $params->{'longitude'};
	$merchant_location_info{'latitude'} = $merchant_master_info{'latitude'} = $params->{'latitude'} if $params->{'latitude'};
	
	$merchant_master_info{'emailaddress1'} = lc($params->{'emailaddress1'}) if defined $params->{'emailaddress1'};
	$merchant_master_info{'emailaddress2'} = lc($params->{'emailaddress2'}) if defined $params->{'emailaddress2'};
	
	$merchant_location_info{'phone'} = $merchant_master_info{'phone'} = $params->{'phone'} if $params->{'phone'};
	$merchant_master_info{'fax'} = $params->{'fax'} if defined $params->{'fax'};
	$merchant_location_info{'scanner'} = $merchant_master_info{'scanner'} = $params->{'scanner'} if defined $params->{'scanner'};

	($merchant_location_info{'username'} = $merchant_master_info{'username'} = $params->{'username'}) =~ s/\s//g  if $params->{'username'};
	($merchant_location_info{'password'} = $merchant_master_info{'password'} = $params->{'password'}) =~ s/\s//g  if $params->{'password'};

	$merchant_vendor_discount_info{'rebate'} = $merchant_master_info{'rebate'} = $params->{'rebate'} if $params->{'rebate'};
	$merchant_vendor_discount_info{'reb_com'} = $merchant_master_info{'reb_com'} = $params->{'reb_com'} if $params->{'reb_com'};
	
	if (exists $params->{'cap'} && $params->{'cap'} =~ /\D/)
	{
   	$params->{'cap'} =~ s/\.\d*$//;
   	$params->{'cap'} =~ s/\D//;
	}
	
	$merchant_vendor_discount_info{'cap'} = $merchant_master_info{'cap'} = $params->{'cap'};

	#TODO Since we are doing away with the tracking merchants we should change this at some point to just use discount.
#       warn "Line 1178: " . Dumper($params);
	if(exists $params->{'percentage_of_sale'} || exists $params->{'exception_percentage_of_sale'})
	{
		$merchant_master_info{'discount'} = $params->{'percentage_of_sale'} ? $params->{'percentage_of_sale'} : $params->{'exception_percentage_of_sale'};
		$merchant_vendor_discount_info{'discount'} = $merchant_master_info{'discount'};
	}
	elsif(exists $params->{'discount'})
	{
		$merchant_vendor_discount_info{'discount'} = $merchant_master_info{'discount'} = $params->{'discount'};
	}
	
	if (defined $params->{'discount_type'})
	{
		$merchant_master_info{'discount_type'} = $params->{'discount_type'};
		$merchant_vendor_discount_info{'discount_type'} = $merchant_master_info{'discount_type'};
	}
	
        $merchant_master_info{'discount_subtype'} = $params->{'discount_subtype'} if defined $params->{'discount_subtype'};

	$merchant_master_info{'special'} = $params->{'special'} if exists $params->{'special'};

	$merchant_master_info{'blurb'} = $params->{'blurb'} if exists $params->{'blurb'};
	
	$merchant_master_info{'discount_blurb'} = $params->{'discount_blurb'} if exists $params->{'discount_blurb'};
	
	$merchant_vendor_discount_info{'pp'} = $merchant_master_info{'pp'} = $params->{'pp'} if $params->{'pp'};
	
	$merchant_vendor_discount_info{'url'} = $merchant_master_info{'url'} = $params->{'url'} if defined $params->{'url'};
	
	#TODO: I should probably set this to be an empty string if Determin if the master info should be notes, blurb, or not at all.
	$merchant_vendor_discount_info{'blurb'} = $params->{'blurb'} if exists $params->{'blurb'};
	
	$merchant_master_info{'url_lang_pref'} = $params->{url_language_pref} if defined $params->{url_language_pref};
	
	$merchant_master_info{'business_type'} = $params->{'business_type'} if $params->{'business_type'};
	
	$merchant_master_info{'pay_name'} = $params->{'pay_name'} if defined $params->{'pay_name'};
	$merchant_master_info{'pay_address'} = $params->{'pay_address'} if defined $params->{'pay_address'};
	$merchant_master_info{'pay_city'} = $params->{'pay_city'} if defined $params->{'pay_city'};
	$merchant_master_info{'pay_state'} = $params->{'pay_state'} if defined $params->{'pay_state'};
	$merchant_master_info{'pay_country'} = $params->{'pay_country'} if defined $params->{'pay_country'};
	$merchant_master_info{'pay_postalcode'} = $params->{'pay_postalcode'} if defined $params->{'pay_postalcode'};
	
	$merchant_master_info{'pay_cardnumber'} = $params->{'pay_cardnumber'} if (defined $params->{'pay_cardnumber'} && $params->{'pay_cardnumber'} !~ /x/gi);
	
	$merchant_master_info{'pay_cardexp'} = $params->{'pay_cardexp'} if defined $params->{'pay_cardexp'};
	
 
	
	$merchant_master_info{pay_routing_number} = $params->{pay_routing_number} if defined $params->{pay_routing_number};
	$merchant_master_info{pay_account_number} = $params->{pay_account_number} if defined $params->{pay_account_number};
	
#	$merchant_master_info{pay_iban} = $params->{pay_iban} if defined $params->{pay_iban};
#	$merchant_master_info{pay_swift} = $params->{pay_swift} if defined $params->{pay_swift};
	$merchant_master_info{pay_bank_name} = $params->{pay_bank_name} if defined $params->{pay_bank_name};
	$merchant_master_info{pay_bank_city} = $params->{pay_bank_city} if defined $params->{pay_bank_city};
	$merchant_master_info{pay_bank_address} = $params->{pay_bank_address} if defined $params->{pay_bank_address};
	$merchant_master_info{pay_bank_state} = $params->{pay_bank_state} if defined $params->{pay_bank_state};
	$merchant_master_info{pay_bank_country} = $params->{pay_bank_country} if defined $params->{pay_bank_country};
	$merchant_master_info{pay_bank_postalcode} = $params->{pay_bank_postalcode} if defined $params->{pay_bank_postalcode};
	$merchant_master_info{pay_account_number} = $params->{pay_account_number} if defined $params->{pay_account_number};
	$merchant_master_info{pay_routing_number} = $params->{pay_routing_number} if defined $params->{pay_routing_number};
	
	
	
	
	$merchant_master_info{'pay_payment_method'} = $params->{'pay_payment_method'} if defined $params->{'pay_payment_method'};
	
	$merchant_master_info{'pay_cardcode'} = $params->{'pay_cardcode'} if (defined $params->{'pay_cardcode'} && $params->{'pay_cardcode'} =~ /^\d+$/);
	$merchant_master_info{'eco_card_number'} = $params->{'eco_card_number'} if defined $params->{'eco_card_number'};
	
	
	($merchant_master_info{'ca_number'} = $params->{'ca_number'}) =~ s/\D//g if defined $params->{'ca_number'};
	($merchant_master_info{'ca_number_annual'} = $params->{'ca_number_annual'}) =~ s/\D//g if defined $params->{'ca_number_annual'};
	
	if(uc($merchant_master_info{'pay_payment_method'}) eq 'CA')
	{
		#These zero length strings will be set to null before the table is updated.
		$merchant_master_info{'ca_number'} = '' if ! $merchant_master_info{'ca_number'};
		$merchant_master_info{'ca_number_annual'} = '' if ! $merchant_master_info{'ca_number_annual'};
		
	}
	
#	if($params->{'discount_type'} == 15)
#	{
#		#If it is a free merchant let's set all the payment information to null.
#		$merchant_master_info{'pay_name'} = undef;
#		$merchant_master_info{'pay_address'} = undef;
#		$merchant_master_info{'pay_city'} = undef;
#		$merchant_master_info{'pay_state'} = undef;
#		$merchant_master_info{'pay_country'} = undef;
#		$merchant_master_info{'pay_postalcode'} = undef;
#		
#		$merchant_master_info{'pay_cardnumber'} = undef;
#		
#		$merchant_master_info{'pay_cardexp'} = undef;
#		
#		
#		
#
##		$merchant_master_info{pay_bank_name} = undef;
##		$merchant_master_info{pay_bank_city} = undef;
##		$merchant_master_info{pay_bank_address} = undef;
##		$merchant_master_info{pay_bank_state} = undef;
##		$merchant_master_info{pay_bank_country} = undef;
##		$merchant_master_info{pay_bank_postalcode} = undef;
##		$merchant_master_info{pay_account_number} = undef;
##		$merchant_master_info{pay_routing_number} = undef;
#		
#		
#		
#		
#		$merchant_master_info{'pay_payment_method'} = undef;
#		
#		$merchant_master_info{'pay_cardcode'} = undef;
#		$merchant_master_info{'eco_card_number'} = undef;
#		
#		
#		$merchant_master_info{'ca_number'} = undef;
#		$merchant_master_info{'ca_number_annual'} = undef;
#		
#	}
	
	
	
	
	
	$merchant_master_info{'notes'} = $params->{'notes'} if $params->{'notes'};
	$merchant_master_info{'contact_info'} = $params->{'contact_info'} if $params->{'contact_info'};
	
	$merchant_vendor_discount_info{'operator'} = $merchant_location_info{'operator'} = $merchant_master_info{'operator'} = $params->{'operator'} if $params->{'operator'};
	
	#TODO: We may need to add this later $merchant_vendor_discount_info{'status'} = 
	$merchant_vendor_discount_info{'status'} = $merchant_master_info{'status'} = $params->{'status'} if exists $params->{'status'};
	
	$merchant_banner{'banner'} = $params->{'banner'} if (exists $params->{'banner'} && $params->{'banner'});
	$merchant_banner{'delete_banner'} = $params->{'delete_banner'} if (exists $params->{'delete_banner'} && $params->{'delete_banner'});
#
# If discount_type is not bronze or higher then that should not get a banner (type 1-3 && 10-14)
#
        if (!($merchant_master_info{'discount_type'} >= 1 && $merchant_master_info{'discount_type'} <= 3) &&
		!($merchant_master_info{'discount_type'} >= 10 && $merchant_master_info{'discount_type'} <= 13)) {
	    $merchant_banner{'delete_banner'} = 1;
	}
        if (!($merchant_master_info{'discount_type'} >= 2 && $merchant_master_info{'discount_type'} <= 3) &&
	      !($merchant_master_info{'discount_type'} >= 11 && $merchant_master_info{'discount_type'} <= 13)) {
	    $merchant_vendor_discount_info{'url'} = $merchant_master_info{'url'} = undef;
	}

#	$merchant_master_info{activated} = $params->{} if $params->{};

# Updating the Member ID associated with the Merchant Record is currently handled by a trigger in the DB
# I'm leaving this here in case we need it in the future.
	my %merchant_member_info = ();

	$merchant_member_info{'tin'} 					= $params->{'tin'} if ($merchants_member_id && exists $params->{'tin'});

#	$merchant_member_info{'spid'} 				= $params->{'referral_id'} if $params->{'referral_id'};
#	$merchant_member_info{'firstname'} 			= $params->{'firstname1'} if $params->{'firstname1'};
#	$merchant_member_info{'lastname'} 			= $params->{'lastname1'} if $params->{'lastname1'};
#	$merchant_member_info{'company_name'} 		= $params->{'business_name'} if $params->{'business_name'};
#	$merchant_member_info{'emailaddress'} 		= $params->{'emailaddress1'} if $params->{'emailaddress1'};
#	$merchant_member_info{'address1'} 			= $params->{'address1'} if $params->{'address1'};
#	$merchant_member_info{'city'} 				= $params->{'city'} if $params->{'city'};
#	$merchant_member_info{'state'} 				= $params->{'state'} if $params->{'state'};
#	$merchant_member_info{'postalcode'} 			= $params->{'postalcode'} if $params->{'postalcode'};
#	$merchant_member_info{'country'} 				= $params->{'country'} if $params->{'country'};
#	$merchant_member_info{'phone'} 				= $params->{'home_phone'} if $params->{'home_phone'};
#	$merchant_member_info{'fax_num'} 				= $params->{'fax'} if $params->{'fax'};
#	$merchant_member_info{'password'} 			= $params->{'password'} if $params->{'password'};
#	$merchant_member_info{'memo'} 				= $params->{'memo'} if $params->{'memo'};
#	$merchant_member_info{'mail_option_lvl'} 		= $params->{'mail_option_lvl'} if $params->{'mail_option_lvl'};
#	$merchant_member_info{'language_pref'} 		= $params->{'language'} if $params->{'language'};
#	$merchant_member_info{'cellphone'} 			= $params->{'scanner_number'} if $params->{'scanner_number'};
	
	
	my $update_query  = ' UPDATE merchant_affiliates_master SET ';
	my @fields = ();

	my @values = ();
	my $return_value = 0;
	########################################
	# Build the fields to insert into the
	# "members" table, and "original_spid" table.
	########################################
	foreach (keys %merchant_master_info)
	{
		next if ($_ eq 'id');
		
		$merchant_master_info{$_} = undef if (defined $merchant_master_info{$_} && $merchant_master_info{$_} =~ /^\s*$/);
		
		push @fields, "$_ = ?";
		push @values, $merchant_master_info{$_};
		
	}
	
	push @values, $merchant_master_id;
	
	my $set_fields = join(', ', @fields);
	
	$update_query .= " $set_fields WHERE id = ? ";
	
	
	eval{
		
		$this->{'db'}->{'AutoCommit'} = 0;
		
		$return = $this->{'db'}->do($update_query, undef, @values);
		
		if (exists $subscription->{'status'} && $subscription->{'status'})
		{
		
		    if( ref($debit_club_account) eq 'ARRAY' && $debit_club_account->[0])
		    {
			
				foreach (@$debit_club_account)
				{
					my @debit_club_account_data = (	$_->{'member_id'}, 
									$_->{'transaction_type'}, 
									$_->{'amount'}, 
									$_->{'description'},
									$_->{'operator'} );
					
	#				warn "debit_club_account_data: @debit_club_account_data";
					my $transaction_id = $this->{'db'}->selectrow_array('
						SELECT debit_trans_complete(?,?,?,?,NULL,?)',
							undef, @debit_club_account_data);
				
					die 'There was an error crediting the members Club Account. @debit_club_account_data: ' . Dumper(@debit_club_account_data) if (! $transaction_id || $transaction_id eq '0E0');
				}
		    }

		    if($subscription->{'status'} eq 'UPGRADE')
		    {
		        @values = ($merchant_master_info{'member_id'}, $this->{'__discount_type__subscription_type__mapping'}->{$merchant_master_info{'discount_type'}});
		        my $create_merchant_subscription_returned_pk = $this->{'db'}->selectrow_hashref('SELECT create_merchant_subscription(?, ?, NOW())', undef, @values);
				
		        die 'The subscription could not be upgraded @values = (' . $merchant_master_info{'member_id'} . ',' . $this->{'__discount_type__subscription_type__mapping'}->{$merchant_master_info{'discount_type'}} . ')' if ! $create_merchant_subscription_returned_pk->{'create_merchant_subscription'};
				
		    }
		    elsif($subscription->{'status'} eq 'DOWNGRADE' || $subscription->{'status'} eq 'UPDATE')
		    {
		        @values = ($merchant_master_info{'member_id'}, $this->{'__discount_type__subscription_type__mapping'}->{$merchant_master_info{'discount_type'}}, $subscription->{'description'});
#warn "MerchantAffiliates::updateMerchantMasterInformation::downgrade_mercant_subscription - Values (member_id, discount_type, description): @values \n";
		        my $downgrade_merchant_subscription_returned_pk = $this->{'db'}->selectrow_hashref('SELECT downgrade_merchant_subscription(?, ?, ?)', undef, @values);
				
		        die "We could not $subscription->{'status'} the subscription. " if ! $downgrade_merchant_subscription_returned_pk->{'downgrade_merchant_subscription'};
		    }
		    else
		    {
		        die "The subscription->{'status'} parameter was set, but nothing was done. The subscription->{'status'} must have not been set correctly. subscription->{'status'}: " . $subscription->{'status'};
		    }
		}
#
# G.baker commented out to only update the merchant_affiliates_master and leave the locations alone
#		
		#If we want the merchant to update their master location seperately, this code block needs to be removed, or commented out.
##		if(scalar (keys %merchant_location_info))
##		{
##			my $merchant_location_id = $this->retrieveMerchantMasterLocationByMerchantID($merchant_master_id);
##			$merchant_vendor_discount_info{'vendor_id'} = $merchant_location_id->{'vendor_id'};
##			
##			if (! $this->updateMerchantLocationInformation($merchant_location_id->{'id'}, \%merchant_location_info,\%merchant_vendor_discount_info))
##			{
##				
##				my $error_message = " The location information was not updated \n merchant_location_id->{'id'}: $merchant_location_id->{'id'} \n";
##				
##				
##				use Data::Dumper;
##				my $temp = \%merchant_location_info;
##				$error_message .= 'merchant_location_info: ' . Dumper($temp);
##				
##				$temp = \%merchant_vendor_discount_info;
##				$error_message .= "\n merchant_vendor_discount_info: " . Dumper($temp) . "\n";
##				
##				
##				die $error_message;
##				
##			}
##			
##			if($merchant_location_info{'location_name'})
##			{
##				#warn "this->updateMerchantLocationsBusinessName($merchant_master_id, $merchant_location_info{'location_name'}, $merchant_location_id->{'id'})";
##				my $number_of_rows_updated = $this->updateMerchantLocationsBusinessName($merchant_master_id, $merchant_location_info{'location_name'}, $merchant_location_id->{'id'});
##				die 'The location name was not updated - number_of_rows_updated: ' . $number_of_rows_updated if $number_of_rows_updated !~ /^\d+$/;
##			}
##			
##		}
		
		
		            if(scalar (keys %merchant_banner))
			    {
		
				my $banner_info = $merchant_banner{delete_banner} ? undef : $merchant_banner{banner};
				
				$this->updateVendorBannerByMerchantID( $merchant_master_id, $banner_info );
				
				if ($merchant_banner{delete_banner})
				{
					if(! unlink <$merchant_banner{delete_banner}>)
					{
						#TODO: Send an email for some clean up.
					}
				}
		
			    }
		
		
		if($merchants_member_id && exists $merchant_member_info{'tin'})
		{
			$this->setMembers() if(!defined $this->{'Members'});
#			
#			if (! $merchants_member_id)
#			{
#				my $temp_merchant_information = $this->retrieveMerchantMasterRecordByID($merchant_master_id);
#				
#				$merchants_member_id = $temp_merchant_information->{'member_id'};
#			}
#			
			$this->{'Members'}->updateTin($merchants_member_id, $merchant_member_info{'tin'});
		}
		
		# The merchant master information was just updated, so lets delete the cache if it exists so old information isn't returned when
		# this->getMerchantMaster is called.
		delete $this->{'merchant_affiliates_info_by_id'}->{$merchant_master_id} if exists $this->{'merchant_affiliates_info_by_id'}->{$merchant_master_id};
		
		$this->{'db'}->commit();	
		$this->{'db'}->{'AutoCommit'} = 1;
		
		
	};
	if($@)
	{
		use Data::Dumper;
		my $error = $@;
		#warn 'MerchantAffiliates::updateMerchantMasterInformation - There was an error updateing the Merchants Master Record, the Merchants Location Record, the Locations Vendor Record, or the Merchant Banner Information. Error: ' . $error;
		my $MailTools = MailTools->new();
		
		my $message = "\n MerchantAffiliates::updateMerchantMasterInformation - There was an error updateing the Merchants Master Record, the Merchants Location Record, the Locations Vendor Record, or the Merchant Banner Information. \n";
		$message .= " Error: \n $error \n";
		$message .= " merchant_master_id: $merchant_master_id \n ";
		$message .= " params: \n " . Dumper($params) . "\n";
		$message .= " merchants_member_id: $merchants_member_id \n ";
		$message .= " master_location_id: $master_location_id \n ";
		$message .= " subscription: \n " . Dumper($subscription) . "\n";
		$message .= " debit_club_account: \n " . Dumper($debit_club_account) . "\n";

		$MailTools->sendTextEmail({
									'from'=>'errors@dhs-club.com',
									'to'=>'errors@dhs-club.com',
									'subject'=> DHSGlobals::CLUBSHOP_DOT_COM . " MerchantAffiliates::updateMerchantMasterInformation ",
									'text'=>$message
								});
		
		
		$this->{'db'}->rollback();
		$this->{'db'}->{'AutoCommit'} = 1;
		
		$return = 0;
	}

	
	return $return;
	
}


=head3 updateMerchantLocationInformation

=head4
Description:

	This updates a Merchants Location Record information.
	(We should make the querries work in a transaction block
	when/if we allow the merchant to modify their discount by location.
	Right now they are updateing their discount information from
	the Master Information screen.)

=head4
Params:

	merchant_location_id	int	The primary key for the record you want to update in the merchant_affiliate_locations table.
	
	params					hashref
								should be a hashref where the keys
								are named the same as the fields in
								the merchant_affiliate_locations.
								If there is an accepted key in the
								hashref set to true the accepted
								field will be updated to the current
								date.  If the "member_id_assigned",
								or "id" field are missing a new "x"
								member will be created for this
								potential merchant.
	
	discount_params			hashref	The indexes should match the fields in the "vendor" table, 
									and the "merchant_affiliate_discount" table.

=head4
Return:

	the number of rows effected.

=cut

sub updateMerchantLocationInformation
{
	my $this = shift;
	my $merchant_location_id = shift;
	my $params = shift;
	my $discount_params = shift;

	my $return = 0;


	if (! $params)
	{
		die 'No parameters were passed.';
	}

	if (! $merchant_location_id)
	{
		die 'The merchant_location_id is a required parameter.';
	}

	my $set_parameters = '';
	my %merchant_location_info = ();
	
#	$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
#	my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");
	
	my $merchant_config = $this->retrieveMerchantConfigHashRef();
	
	$params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};

#	warn "Into update Location: " . Dumper($params);
	my $coords = $this->getMerchantLocationCoordinates($params);
	if ($coords->{'error'} eq ' ')
	{
	    $params->{'longitude'} = $coords->{'longitude'};
	    $params->{'latitude'} = $coords->{'latitude'};
	}

	#TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Location Name.
	#my $length = 35;
	my $string_length = length($params->{'location_name'});
	if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
	{
		my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
		$params->{'location_name'} = substr($params->{'location_name'}, 0, - $characters_to_remove);
	}
	
	$merchant_location_info{'location_name'} = $params->{'location_name'} if $params->{'location_name'};
	$merchant_location_info{'address1'} = $params->{'address1'} if $params->{'address1'};
	$merchant_location_info{'address2'} = $params->{'address2'} ? $params->{'address2'} : undef;
	$merchant_location_info{'address3'} = $params->{'address3'} ? $params->{'address3'} : undef;
	$merchant_location_info{'city'} = $params->{'city'} if $params->{'city'};
	$merchant_location_info{'state'} = $params->{'state'} if $params->{'state'};
	$merchant_location_info{'country'} = $params->{'country'} if $params->{'country'};
	$merchant_location_info{'postalcode'} = $params->{'postalcode'} ? $params->{'postalcode'} : undef;
	$merchant_location_info{'phone'} = $params->{'phone'} if $params->{'phone'};
	($merchant_location_info{'username'} = $params->{'username'}) =~ s/\s//g if $params->{'username'};
	($merchant_location_info{'password'} = $params->{'password'}) =~ s/\s//g if $params->{'password'};

	$merchant_location_info{'scanner'} = $params->{'scanner'} ? $params->{'scanner'}: undef;
	
	$merchant_location_info{'operator'} = $params->{'operator'} if $params->{'operator'};

	$merchant_location_info{'notes'} = $params->{'notes'} ? $params->{'notes'}: undef;
#TODO: Fix this!
	$merchant_location_info{'location_name_url'} = $this->createLocationNameUrlFromLocationName($merchant_location_info{'location_name'}, $merchant_location_info{'country'},$merchant_location_info{'state'},$merchant_location_info{'city'},$merchant_location_id);

	# we do not want to change the DB information to null if for some reason the "params" are missing
	# so instead we simply will not create a key
	# theoretically this should not be necessary,
	# but there have been cases where the existing info was overwritten with a NULL ;(
	$merchant_location_info{'longitude'} = $params->{'longitude'} if $params->{'longitude'};
	$merchant_location_info{'latitude'} = $params->{'latitude'} if $params->{'latitude'};
	
	my $update_query  = ' UPDATE merchant_affiliate_locations SET ';
	my @fields = ();
	my @values = ();
	
	########################################
	# Build the fields to insert into the
	# "members" table, and "original_spid" table.
	########################################
	foreach (keys %merchant_location_info)
	{
		next if ($_ eq 'id');
		
		$merchant_location_info{$_} = undef if (defined $merchant_location_info{$_} && $merchant_location_info{$_} =~ /^\s*$/);
		
		push @fields, "$_ = ?";
		push @values, $merchant_location_info{$_};
		
	}
	
	push @values, $merchant_location_id;
	
	my $set_fields = join(', ', @fields);
	
	$update_query .= " $set_fields WHERE id = ? ";
	
	
	eval{
		
#	    warn "MerchantAffiliates::UpdateMerchantLocationInformation: " . $update_query . "\n" . Dumper(@values) . "\n";
		$return = $this->{'db'}->do($update_query, undef, @values);
		
		if (scalar (keys %$discount_params))
		{
			die ' The discount information was not updated ' if ! $this->updateMerchantDiscountInformation($merchant_location_id, $discount_params);
		}	
		
	};
	if($@)
	{
		die 'MerchantAffiliates::updateMerchantLocationInformation - There was an error updateing the Merchants Location Record. Query: ' . $update_query . 'Error: ' . $@;
		
		$return = 0;
	}
	
	return $return;
	
}


=head3 updateMerchantLocationsBusinessName

=head4
Description:

	This updates all the Merchants Location Records with the current 
	business/location name being used.

=head4
Params:

	merchant_master_id
	int
	The Merchants Master ID.
	
	business_name
	STRING
	The business name
	
	location_to_skip
	INT
	(Optional) If a location ID is included the business_name will not be updated for this location
	You want to use this if you update a particular location, and also need to update the remaining
	locations for a merchant.  If you do not use this in that cenario an error may be thrown the next
	time the location record is updated.  It's wierd, but it's how it is for now.

=head4
Return:

	the number of rows effected.

=cut

sub updateMerchantLocationsBusinessName
{
	my $this = shift;
	my $merchant_master_id = shift;
	my $location_name = shift;
	my $location_to_skip = shift;
	
	my $return = 0;

	if ($merchant_master_id !~ /^\d+$/)
	{
		die 'The merchant_master_id is a required parameter.';
	}

	if (! $location_name)
	{
		die 'The location_name was not defined.';
	}

	if ($location_to_skip && $location_to_skip !~ /^\d+$/)
	{
		die 'The location_to_skip parameter must be an integer. location_to_skip: ' . $location_to_skip;
	}
	
	
	my $set_parameters = '';
	my %merchant_location_info = ();
	
	my $update_query  = ' UPDATE merchant_affiliate_locations SET location_name = ? WHERE merchant_id = ?';
	my @query_values = ();
	
#	$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
#	my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");
	
	my $merchant_config = $this->retrieveMerchantConfigHashRef();
	
	#TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Location Name.
	#my $length = 35;
	my $string_length = length($location_name);
	if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
	{
		my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
		$location_name = substr($location_name, 0, - $characters_to_remove);
	}
	


	
	if ($location_to_skip)
	{
		$update_query  .= ' AND id <> ?';
		@query_values = ($location_name, $merchant_master_id, $location_to_skip);
	}
	else
	{
		@query_values = ($location_name, $merchant_master_id);
	}
	
	eval{
		
		$return = $this->{'db'}->do($update_query, undef, @query_values);
		
		$return = 0 if $return eq '0E0';
		
		#TODO: At some point decide what to do with the returned results, $number_of_locations_updated.
		my $number_of_location_urls_updated = $return ? $this->updateAllMerchantLocationNameUrl($merchant_master_id, $location_to_skip):0;
		
	};
	if($@)
	{
		
#		warn "MerchantAffiliates::updateMerchantLocationsBusinessName - There was an error updateing the Merchants Location Name Record. update_query: $update_query - query_values: ($location_name, $merchant_master_id, $location_to_skip) - Error: " . $@;
		
		$return = 0;
	}
	
	return $return;
	
}

=head3 updateAllMerchantLocationNameUrl

=head4
Description:

	This updates all the Merchants Location Records with the current 
	location_name_url.

=head4
Params:

	merchant_master_id
	int
	The Merchants Master ID.
	
	business_name
	STRING
	The business name
	
	location_to_skip
	INT
	(Optional) If a location ID is included the business_name will not be updated for this location
	You want to use this if you update a particular location, and also need to update the remaining
	locations for a merchant.  If you do not use this in that cenario an error may be thrown the next
	time the location record is updated.  It's wierd, but it's how it is for now.

=head4
Return:

	the number of rows effected.

=cut

sub updateAllMerchantLocationNameUrl
{
	
	my $this = shift;
	my $merchant_master_id = shift;
	my $location_id_to_skip = shift;
	
	die "MerchantAffiliates::updateMerchantLocationNameUrl - The merchant_master_id is a required parameter, it was not specified or did not hold the correct information.  Only integers are allowed. merchant_master_id: $merchant_master_id " if (! defined $merchant_master_id || $merchant_master_id !~ /^\d+$/);
	die "MerchantAffiliates::updateMerchantLocationNameUrl - The location_id_to_skip was specified, but it did not hold the correct information.  Only integers are allowed. location_id_to_skip: $location_id_to_skip " if (defined $location_id_to_skip && $location_id_to_skip !~ /^\d+$/);
	
	my $return_value = 0;
	
	my $update_query = 'UPDATE merchant_affiliate_locations SET location_name_url = ? WHERE id = ?';

	my $merchant_locations = $this->retrieveAllMerchantLocationsByMerchantID($merchant_master_id);
	
	foreach (@$merchant_locations)
	{
	
		next if $location_id_to_skip == $_->{'id'};
		
		my $new_location_name_url = $this->createLocationNameUrlFromLocationName($_->{'location_name'},$_->{'country'},$_->{'state'},$_->{'city'},$_->{'id'});
		
		
		if ($new_location_name_url ne $_->{'location_name_url'})
		{
			
			my @values = ($new_location_name_url, $_->{'id'});
			my $rows_updated = $this->{'db'}->do($update_query, undef, @values);

			
			$return_value += $rows_updated if ($rows_updated =~ /^\d+$/);
		}
		
		
	}
	
	return $return_value;
	
}


=head3 updateMerchantDiscountInformation

=head4
Description:

	This updates a Merchants Discount Record, and their Vendor Record.
	The Discount is kept in a Master Record, and the Vendor Record 
	(the vendor record is associated with a location not the master record).

=head4
Params:

	merchant_location_id	int	The Location ID for the record you want to update in the "merchant_affiliate_discounts" table
					and the "vendors" table.  "merchant_affiliate_discounts" table has a pk (Primary Key) field,
					and can hold multiple disocunts for a location with begin, and end dates, but it hasn't been
					used yep.  I'm assuming there was once a plan to take advantage of this, but it hasn't been 
					done yet so well ignore it, and just update the information based on the Location ID.'
	
	
	params			hashref
							should be a hashref where the keys
							are named the same as the fields in
							the "merchant_affiliate_discounts" table, and the "vendors" table.
							If the "vendor_id" index is not defined it will be looked up using the 
							"merchant_location_id"

=head4
Return:

	the number of rows effected.

=cut

sub updateMerchantDiscountInformation
{
	my $this = shift;
	my $merchant_location_id = shift;
	my $params = shift;

	my $return = 0;


	if (! $params)
	{
		die 'MerchantAffiliates::updateMerchantDiscountInformation - No parameters were passed.';
	}

	if (! $merchant_location_id)
	{
		die 'MerchantAffiliates::updateMerchantDiscountInformation - The merchant_location_id is a required parameter.';
	}
	
	
	my $set_parameters = '';
	my %merchant_discount_info = ();
	my %vendor_params = ();
	
	$merchant_discount_info{'rebate'} = $params->{'rebate'} if exists $params->{'rebate'};
	$vendor_params{'rebate'} = $params->{'rebate'} if exists $params->{'rebate'};
	
	$merchant_discount_info{'reb_com'} = $params->{'reb_com'} if exists $params->{'reb_com'};
	$vendor_params{'reb_com'} = $params->{'reb_com'} if exists $params->{'reb_com'};
	
	$merchant_discount_info{'pp'} = $params->{'pp'} if exists $params->{'pp'};
	$vendor_params{'pp'} = $params->{'pp'} if exists $params->{'pp'};
	
	
	if (exists $params->{'cap'} && $params->{'cap'} =~ /\D/)
	{
		$params->{'cap'} =~ s/\.\d*$//;
		$params->{'cap'} =~ s/\D//g;
	}
	
	$params->{'cap'} = undef if (exists $params->{'cap'} && ! $params->{'cap'});
	
	$merchant_discount_info{'cap'} = $params->{'cap'} if exists $params->{'cap'};
	$vendor_params{'cap'} = $params->{'cap'} if exists $params->{'cap'};
	
	#Be carefull with this field, it is a numeric data type in the "merchant_affiliate_discount" table, but a real in the "vendor" table.
	$merchant_discount_info{'discount'} = $params->{'discount'} if exists $params->{'discount'};
	$vendor_params{'discount'} = $params->{'discount'} if exists $params->{'discount'};
	
	$merchant_discount_info{'operator'} = $params->{'operator'} if exists $params->{'operator'};
	$vendor_params{'operator'} = $params->{'operator'} if exists $params->{'operator'};
	
	$merchant_discount_info{'discount_type'} = $params->{'discount_type'} if defined $params->{'discount_type'};
	$vendor_params{'vendor_name'} = $params->{'vendor_name'} if $params->{'vendor_name'};
	$vendor_params{'payout'} = $params->{'payout'} if $params->{'payout'};
	$vendor_params{'vendor_group'} = $params->{'vendor_group'} if $params->{'vendor_group'};
	$vendor_params{'status'} = $params->{'status'} if $params->{'status'};
	$vendor_params{'url'} = $params->{'url'} if $params->{'url'};
	$vendor_params{'country'} = $params->{'country'} if $params->{'country'};
	$vendor_params{'primary_mallcat'} = $params->{'primary_mallcat'} if $params->{'primary_mallcat'};
	
	$vendor_params{'blurb'} = $params->{'blurb'} if exists $params->{'blurb'};
	
	$vendor_params{'discount_blurb'} = $params->{'discount_blurb'} if exists $params->{'discount_blurb'};
	
	my $update_end_date_query = '
		UPDATE merchant_affiliate_discounts
		SET end_date = NOW()
		WHERE end_date IS NULL
		AND location_id IN (
			SELECT id FROM merchant_affiliate_locations WHERE merchant_id IN (
				SELECT merchant_id FROM merchant_affiliate_locations WHERE id = ?
			)
		)';

	my $update_query = ' UPDATE merchant_affiliate_discounts SET ';

	my $check_update_or_insert=<<EOT;

		SELECT
			CASE
				WHEN mad.start_date >= NOW()
				THEN
					TRUE
				ELSE
					FALSE
			END AS update,
			mad.discount
		FROM
			merchant_affiliate_discounts mad
		WHERE
			mad.location_id = ?
			AND
			mad.end_date IS NULL

EOT


	eval
	{
		my $resuults = $this->{'db'}->selectrow_hashref($check_update_or_insert, undef, ($merchant_location_id));

		if(! $resuults->{'update'})
		{
			my $merchant_locations = $this->retrieveAllMerchantLocationsByLocationID($merchant_location_id);
			
			$return = $this->{'db'}->do($update_end_date_query, undef, ($merchant_location_id));

			#If there is no discount field they must be updating their discount_type
			$merchant_discount_info{'discount'} = $resuults->{'discount'} if ! $merchant_discount_info{'discount'};
						
			foreach (@$merchant_locations)
			{
				$this->createMerchantDiscount($_->{'id'}, \%merchant_discount_info, 1);		
			}
		}
		else
		{
			
			my @fields = ();
			my @values = ();
			my @place_holders = ();
			
			########################################
			# Build the fields to insert into the
			# "members" table, and "original_spid" table.
			########################################
			foreach (keys %merchant_discount_info)
			{
				next if ($_ eq 'id');
				
				$merchant_discount_info{$_} = undef if (defined $merchant_discount_info{$_} && $merchant_discount_info{$_} =~ /^\s*$/);
				
				push @fields, "$_ = ?";
				push @values, $merchant_discount_info{$_};
				
			}
			
			push @values, $merchant_location_id;
			
			my $set_fields = join(', ', @fields);
			
			#This is a little ugly, but it's necessary for now.
			#This is what you want to use to complete the query when we are managing the discounts and cap by location, and not the master info. #$update_query .= " $set_fields WHERE location_id = ? ";
			$update_query .= "
				$set_fields
				WHERE end_date IS NULL
				AND location_id IN (
					SELECT id FROM merchant_affiliate_locations WHERE merchant_id IN (
						SELECT merchant_id FROM merchant_affiliate_locations WHERE id = ?
					)
				)";
	
#			my $temp = \%merchant_discount_info;
#			warn "\nmerchant_discount_info:\n" . Dumper($temp) . "\n\n";
			
			
			$return++ if $this->{'db'}->do($update_query, undef, @values);
			
		}

		if(scalar (keys %vendor_params))
		{
			
#			if(! $vendor_params{'vendor_id'})
#			{
#				#TODO: Change this so all the vendor records for the multiple locations are updated.
#				my $merchant_location_record = $this->retrieveMerchantLocationByLocationID($merchant_location_id);
#				
#				$vendor_params{'vendor_id'} = $merchant_location_record->{'vendor_id'};
#			}
#			
#			my $vendor_id = $vendor_params{'vendor_id'};
#			
#			delete $vendor_params{'vendor_id'};
#			
#			if (scalar (keys %vendor_params))
#			{
#			
#				$this->setVendors() if ! $this->{'Vendors'};
#				
#
#				die ' The vendor record was not updated ' if ! $this->{'Vendors'}->updateVendorInformation($vendor_id, \%vendor_params);
#			}	

			delete $vendor_params{'vendor_id'} if exists $vendor_params{'vendor_id'};
			
			if (scalar (keys %vendor_params))
			{
			
				$this->setVendors() if ! $this->{'Vendors'};
				
#				my $temp = \%vendor_params;
#				warn "vendor_params:\n" . Dumper($temp);
				
				die ' The vendor record was not updated ' if ! $this->{'Vendors'}->updateMerchantVendorInformationByMerchantLocationId($merchant_location_id, \%vendor_params);
			}	
		}
	};
	if($@)
	{
		
		use Data::Dumper;
		
#		warn "MerchantAffiliates::updateMerchantDiscountInformation - There was an error updateing the Merchants Discount Record, or the Vendor Record. merchant_location_id: $merchant_location_id - params: " . Dumper($params) . ' - Error: ' . $@;
		
		$return = 0;
		
	}
	
	return $return;
}

=head3 createMerchantMasterInformation

=head4
Description:

	This creates the Merchants Master Record information, and 
	creates a Subscription (Annual Billing) entry.

=head4
Params:

	params
	HASHREF
	A hashref where the keys are named the same as the fields in the 
	"merchant_affiliates_master" table.  If an 'id' index exists 
	an exception will be thrown.  The "member_id", and "discount_type" 
	are required parameters.
	
	exceptions
	HASHREF
		{
			skip_subscription
			BOOL
			If set to true we will skip the creation of a subscription.
		}

=head4
Return:

	returns	int	"merchant_master_record" Primary Key on sucess, 0 on failure.  

Exceptions:

	An exception is thrown if there is an issue with inserting the data,
	'id' is inclueded in the params, 'member_id' is missing or non numberic,
	or the 'discount_type' is missing or non numberic.

=cut

sub createMerchantMasterInformation
{
	
	my $this = shift;
	my $params = shift;
	my $exceptions = shift;
	
	die 'The ID field is not valid, this method is for creating the Master Record, not updateing it.' if (exists $params->{'id'} && $params->{'id'});	
	die 'The member_id is a required parameter. The Merchant Master record could not be created.' if (exists $params->{'member_id'} && $params->{'member_id'} !~ /^\d+$/);	
	die 'The discount_type is a required parameter. The Merchant Master record could not be created.' if (exists $params->{'discount_type'} && $params->{'discount_type'} !~ /^\d+$/);	
	
	my $insert_query = 'INSERT INTO merchant_affiliates_master ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	
	eval{
		my @primary_key_from_merchant_affiliates_master_table = $this->{'db'}->selectrow_array("SELECT NEXTVAL('merchant_affiliates_master_id_seq')");
		
		die 'We were not able to generate the primary key. ' if ! $primary_key_from_merchant_affiliates_master_table[0];
		
		$params->{'id'} = $primary_key_from_merchant_affiliates_master_table[0];
		
		#TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Business Name.
		#my $length = 100;
		
#		$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
#		my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");

		my $merchant_config = $this->retrieveMerchantConfigHashRef();
	
		my $string_length = length($params->{'business_name'});
		if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
		{
			my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
			$params->{'business_name'} = substr($params->{'business_name'}, 0, - $characters_to_remove);
		}
		
		if($params->{'discount_blurb'})
		{
			
			$string_length = length($params->{'discount_blurb'});
			if ($string_length > $merchant_config->{'discount_blurb'}->{'maximum_characters'})
			{
				my $characters_to_remove = $string_length - $merchant_config->{'discount_blurb'}->{'maximum_characters'};
				$params->{'discount_blurb'} = substr($params->{'discount_blurb'}, 0, - $characters_to_remove);
			}
			
		}
		
		foreach my $key (keys %$params)
		{
			next if (! $params->{$key} && $key ne 'discount_type');
			
			push @fields, $key;
			push @place_holders, '?';
			push @values, $params->{$key};
		        if ($key eq 'discount_type') {
                            if ($params->{$key} == 15 || $params->{$key} == 0) {
				push @fields, "activated";
				push @place_holders, '?';
				push @values, 'now()';
			    }
                            elsif ($params->{$key} >= 20 && $params->{'status'} == 3) {
				push @fields, "activated";
				push @place_holders, '?';
				push @values, 'now()';
			    }
			}
			
		}
		
		
		$insert_query .= '(' . join(',', @fields) . ') VALUES(' . join(',', @place_holders) . ') ';
		use Data::Dumper;
#		warn "insert query MerchantAffiliates: " . $insert_query . " Data: " . Dumper(@values);
		
		$return_value = $this->{'db'}->do($insert_query, undef, @values) ? $primary_key_from_merchant_affiliates_master_table[0]: 0;
		
		# Recycle the @values array.
		
		if (defined $exceptions->{skip_subscription} && ! $exceptions->{skip_subscription})
		{
			die 'The subscription could not be created' if ! $this->createMerchantSubscription($params);
		}
		
	};
	if($@)
	{
		
		my %email = 
		(
			from=>'errors@dhs-club.com',
			to=>'errors@dhs-club.com',
			subject=>'Error: ' . DHSGlobals::CLUBSHOP_DOT_COM . ' MerchantAffiliates::createMerchantMasterInformation',
			text=>'MerchantAffiliates::createMerchantMasterInformation  Query: ' . $insert_query . ' - Error: ' . $@,
		);
		
		my $MailTools = MailTools->new();
		$MailTools->sendTextEmail(\%email);
	
		die $email{text};
	}
	
	return $return_value;
	
}

=head3 createMerchantLocationInformation

=head4
Description:

	This inserts the Merchants Location information, this will create a new location record.

=head4
Params:

	params	hashref
			should be a hashref where the keys
			are named the same as the fields in
			the merchant_affiliate_locations.  If
			an 'id' index exists an exception 
			will be thrown.
	status	int	(Optional) The status default is 0

=head4
Return:

	returns	int	1 on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=cut

sub createMerchantLocationInformation
{
	
	my $this = shift;
	my $params = shift;
	my $status = shift;
	
	die '' if (exists $params->{'id'} && $params->{'id'});	
	
	
	
	my $insert_query = 'INSERT INTO merchant_affiliate_locations ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	
	eval{
		
		$params->{'location_name_url'} = $this->createLocationNameUrlFromLocationName($params->{'location_name'}, $params->{'country'},$params->{'state'},$params->{'city'});
		
		my @primary_key_from_merchant_affiliate_locations_table = $this->{'db'}->selectrow_array("SELECT NEXTVAL('merchant_affiliate_locations_id_seq')");
		
		die 'We were not able to generate the primary key. ' if ! $primary_key_from_merchant_affiliate_locations_table[0];
		
		$this->setVendors() if ! $this->{'Vendors'};

#warn "Into createMerchantLocationInformation: " . Dumper($params);
		
		my %vendor_information = ();
		my %merchant_affiliate_discounts = ();
		
		$vendor_information{'vendor_name'} = "MA Location: $primary_key_from_merchant_affiliate_locations_table[0]";
		$vendor_information{'status'} = $status ? $status: 0;
		$vendor_information{'vendor_group'} = 5;
		if ($params->{'discount_type'} >= 20)
		{ #for online mall
			$vendor_information{'vendor_group'} = 6;
		}
		
		$vendor_information{'url'} = $params->{'url'} if $params->{'url'};
		$vendor_information{'country'} = $params->{'country'} if $params->{'country'};
		$vendor_information{'discount'} = $params->{'discount'} if (exists $params->{'discount'} && defined $params->{'discount'});
		
		$vendor_information{'coupon'} = $params->{'coupon'} if $params->{'coupon'};
		
		$merchant_affiliate_discounts{'discount'} = $params->{'discount'} if (exists $params->{'discount'} && defined $params->{'discount'});
		$merchant_affiliate_discounts{'discount_type'} = $params->{'discount_type'} if $params->{'discount_type'};
		
		$vendor_information{'blurb'} = $merchant_affiliate_discounts{'notes'} = $params->{'notes'} if $params->{'notes'};
		$vendor_information{'primary_mallcat'} = $params->{'primary_mallcat'};
		delete($params->{'discount_type'});
		delete($params->{'discount'});
		delete($params->{'url'});
		delete($params->{'coupon'});
		delete($params->{'primary_mallcat'});

		#delete($params->{'vendor_group'}); # This may be needed later.
		
#		use Data::Dumper;
#		my $temp = \%vendor_information;
#		warn Dumper($temp);
		
		my $vendor_id = $this->{'Vendors'}->createVendorInformation(\%vendor_information);
		
		#TODO: Add some checking, and trouble shooting here. for the $vendor_id;
		
		$params->{'vendor_id'} = $vendor_id;
		
		$params->{'id'} = $primary_key_from_merchant_affiliate_locations_table[0];
		
		#At some point we may want to check for the countries default language, and if it is Spanis set this $Lingua::EN::NameCase::SPANISH = 1;
		#so El, and La stay in that format instead of the default el, or la.
		$params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};
		
		my $coords = $this->getMerchantLocationCoordinates($params);
		if ($coords->{'error'} eq ' ')
		{
		    $params->{'longitude'} = $coords->{'longitude'};
		    $params->{'latitude'} = $coords->{'latitude'};
		}

		 foreach my $key (keys %$params)
		 {
			 next if (! $params->{$key} || $key =~/^_/);

			 push @fields, $key;
			 push @place_holders, '?';
			 push @values, $params->{$key};

		 }

		 $insert_query .= '(' . join(',', @fields) . ') VALUES(' . join(',', @place_holders) . ') ';

		 $return_value = $this->{'db'}->do($insert_query, undef, @values) ? $primary_key_from_merchant_affiliate_locations_table[0]:0;


		 $this->createMerchantDiscount($primary_key_from_merchant_affiliate_locations_table[0], {'discount_type' => $merchant_affiliate_discounts{'discount_type'}, 'discount' => $merchant_affiliate_discounts{'discount'}}) if $return_value;

	 };
	 if($@)
	 {
		 die 'MerchantAffiliates::createMerchantLocationInformation: ' . $@;
	 }

	 return $return_value;

}

=head3 createMerchantDiscount

=head4
Description:
=head4
Params:

	 location_id	INT	
	 params	HASHREF	The params should match the fields in the merchant_affiliates_discount table.
	 start_date_tomorrow	BOOL	(optional) Set to TRUE if you want the start_date to be "NOW() + interval '1 day'"

=head4
Return:

	 returns	INT Primary Key for the new row.

=cut

sub createMerchantDiscount
{
	 my $this = shift;
	 my $location_id = shift;
	 my $params = shift;
	 my $start_date_tomorrow = shift;

#warn "in createMerchantDiscount: " . Dumper($params);

	 die 'MerchantAffiliates::createMerchantDiscount - The location_id parameter is required, but it did not hold the correct data - location_id: ' . $location_id if $location_id !~ /^\d+$/;
	 die "MerchantAffiliates::createMerchantDiscount - The params->{'discount'} parameter is required, but it did not hold the correct data - params->{'discount'}: " . $params->{'discount'} if ($params->{'discount'} !~ /^\d?\.?\d*$/ || $params->{'discount'} < 0);

	 my @fields = ();
	 my @place_holders = ();
	 my @values = ();

	 foreach (keys %$params)
	 {
		 next if (! $_ || ! defined $params->{$_});
		 push @fields, $_;
		 push @place_holders, '?';
		 push @values, $params->{$_};
	 }

	 push @fields, 'location_id';
	 push @place_holders, '?';
	 push @values, $location_id;

	 push @fields, 'start_date';
	 push @place_holders, $start_date_tomorrow ? "(NOW() + interval '1 day')": 'NOW()';

	 my $discount_insert_query = 'INSERT INTO merchant_affiliate_discounts (' . join(', ', @fields) . ') VALUES (' . join(', ', @place_holders) . ')';

	 my $return_value = undef;

	 eval{
#die "$discount_insert_query\n" . Dumper(\@values);
		 $this->{'db'}->do($discount_insert_query, undef, @values);
		 $return_value = $this->{'db'}->last_insert_id('network', 'public', 'merchant_affiliate_discounts', undef);

	 };
	 if($@)
	 {
		 die 'MerchantAffiliates::createMerchantDiscount - There was an error inserting the record into the merchant_affiliate_discounts table.  Error: ' . $@;
	 }

	 return $return_value;		

}

=head3 createNotification

=head4
Description:

	 This inserts a notification record.

=head4
Params:

	 hashref
		 should be a hashref where the keys
		 are named the same as the fields in
		 the notifications table.

=head4
Return:

	 returns	int Primary Key for the new notification record on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=cut

sub createNotification
{

	 my $this = shift;
	 my $params = shift;

	 my $insert_query = 'INSERT INTO notifications ';

	 my @values = ();
	 my @fields = ();
	 my @place_holders = ();
	 my $return_value = 0;


	 eval{
		 my @primary_key_from_notifications_table = $this->{'db'}->selectrow_array("SELECT NEXTVAL('notifications_pk_seq')");

		 die 'We were not able to generate the primary key. ' if ! $primary_key_from_notifications_table[0];

		 $params->{pk} = $primary_key_from_notifications_table[0];

		 foreach my $key (keys %$params)
		 {
			 next if (! $params->{$key});

			 push @fields, $key;
			 push @place_holders, '?';
			 push @values, $params->{$key};

		 }

		 $insert_query .= '(' . join(',', @fields) . ') VALUES(' . join(',', @place_holders) . ') ';


		 $return_value = $this->{'db'}->do($insert_query, undef, @values) ? $primary_key_from_notifications_table[0]: 0;

	 };
	 if($@)
	 {
		 use Data::Dumper;
		 my $temp = \@values;
		 die "\nMerchantAffiliates::createNotification -- Query: $insert_query -- Values: " . Dumper($temp) . " -- Error: " . $@ . "\n";
	 }

	 return $return_value;

}

=head3 insertPendingMerchantInformation

=head4
Description:

	 This insert the Pending Merchants information, this will create a new Pending Merchant record.

=head4
Params:

	 hashref
		 should be a hashref where the keys
		 are named the same as the fields in
		 the merchant_pending table.  If
		 an 'id' index exists an exception 
		 will be thrown.

=head4
Return:

	 returns	int	1 on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=head4
TODO:

	 Determin if this is needed, or used.  This should be handled by the "PotentialMerchantAffiliates" Class.

=cut

sub insertPendingMerchantInformation
{

	 my $this = shift;
	 my $params = shift;

	 die '' if (exists $params->{'id'} && $params->{'id'});	

	 my $insert_query = 'INSERT INTO merchant_pending ';

	 my @values = ();
	 my @fields = ();
	 my @place_holders = ();
	 my $return_value = 0;

	 my %notification_values = ();

	 eval{

		 my @primary_key_from_merchant_pending_table = $this->{'db'}->selectrow_array("SELECT NEXTVAL('merchant_pending_id_seq')");

		 die 'We were not able to generate the primary key. ' if ! $primary_key_from_merchant_pending_table[0];

		 $params->{'id'} = $primary_key_from_merchant_pending_table[0];

		 $params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};

#		$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
#		my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");

		 my $merchant_config = $this->retrieveMerchantConfigHashRef();

		 #TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Business Name.
		 #my $length = 31;
		 my $string_length = length($params->{'business_name'});
		 if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
		 {
			 my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
			 $params->{'business_name'} = substr($params->{'business_name'}, 0, - $characters_to_remove);
		 }

		 foreach my $key (keys %$params)
		 {
			 next if (! $params->{$key});

			 push @fields, $key;
			 push @place_holders, '?';
			 push @values, $params->{$key};
		 }

		 $insert_query .= '(' . join(',', @fields) . ') VALUES (' . join(',', @place_holders) . ') ';

		 $return_value = $this->{'db'}->do($insert_query, undef, @values);

		 $notification_values{'id'} = $params->{'id'};
		 $notification_values{'notification_type'} = 104;
		 $notification_values{'memo'} = 'Pending Merchant Application.';



		 $this->createNotification(\%notification_values);
	 };

	 if($@)
	 {
		 die 'Error: ' . $@;
	 }

	 return $return_value;
}

=head3 insertMerchantPendingPaymentInformation

=head4
Description:

	 This first deletes the Merchants Payment Pending information 
	 then inserts the Merchants Payment Pending information, this 
	 will create a new Pending Merchant record.

=head4
Params:

	 hashref
		 should be a hashref where the keys
		 are named the same as the fields in
		 the merchant_payment_pending table.  If
		 a 'merchant_id' index does not exist an 
		 exception will be thrown.

=head4
Return:

	 returns	int	1 on sucess, 0 on failure.  An exception is thrown 
	 if there is an issue with deleting, or inserting the data.

=cut

sub insertMerchantPendingPaymentInformation
{

	 my $this = shift;
	 my $params = shift;

	 die 'MerchantAffiliates::insertMerchantPendingPaymentInformation - No Merchant ID was supplied, it is a required field' if (! exists $params->{'merchant_id'} || ! $params->{'merchant_id'});	

	 my $insert_query = 'INSERT INTO merchant_payment_pending ';

	 my @values = ();
	 my @fields = ();
	 my @place_holders = ();
	 my $return_value = 0;


	 $params->{'pay_payment_method'} = lc($params->{'pay_payment_method'}) if defined $params->{'pay_payment_method'};

	 eval{

			 foreach my $key (keys %$params)
			 {
				 next if (! $params->{$key});

				 push @fields, $key;
				 push @place_holders, '?';
				 push @values, $params->{$key};
			 }

			 $insert_query .= '(' . join(',', @fields) . ') VALUES (' . join(',', @place_holders) . ') ';

			 $this->deleteMerchantPaymentPendingRecord($params->{'merchant_id'});

			 $return_value = $this->{'db'}->do($insert_query, undef, @values);

	 };
	 if($@)
	 {
		 die 'MerchantAffiliates::insertMerchantPendingPaymentInformation() - Error: ' . $@;
	 }

	 return $return_value;

}

=head3 createLocationNameUrlFromLocationName

=head4
Description:

	 The location_name is stripped of all character other than a-z, and 0-9,
	 then the name is checked against other store names in that area.  If a 
	 match is found the number 2 is appended to the end of that name, and rechecked the
	 appended number is increased until no matches are found.

=head4
Params:

	 location_name
	 string
	 The store name, as defined in "merchant_affiliates_locations::location_name".

	 country
	 string
	 The country code.

	 state
	 string
	 The state, or province.

	 city
	 string
	 The city.

	 merchant_location_id
	 int
	 The ID for the merchant location record, if this is specified the merchants location record is skipped.

=head4
Return:

	 string
	 The location name

=cut

sub createLocationNameUrlFromLocationName
{
	 my $this = shift;
	 my $location_name = shift;
	 my $country = shift;
	 my $state = shift;
	 my $city = shift;
	 my $merchant_location_id = shift;

	 die 'MerchantAffiliates::createLocationNameUrlFromLocationName - The location_name is a required parameter. ' if (! defined $location_name || ! $location_name);
	 die 'MerchantAffiliates::createLocationNameUrlFromLocationName - The country is a required parameter. ' if (! defined $country || ! $country);
	 die 'MerchantAffiliates::createLocationNameUrlFromLocationName - The state is a required parameter. ' if (! defined $state || ! $state);
	 die 'MerchantAffiliates::createLocationNameUrlFromLocationName - The city is a required parameter. ' if (! defined $city || ! $city);
	 die "MerchantAffiliates::createLocationNameUrlFromLocationName - The merchant_location_id was specified, but it must be an integer. merchant_location_id: $merchant_location_id" if (defined $merchant_location_id && $merchant_location_id !~ /^\d+$/);

	 my $location_name_url = lc($location_name);

	 $location_name_url =~ s/\W//g;

	 my $flag = 0;
	 my $name_counter = '';

	 my $location_name_url_modified = '';

	 while($flag == 0)
	 {

		 $location_name_url_modified = $location_name_url . $name_counter;

		 my $number_of_matches = $this->checkLocationNameUrlDuplicates($location_name_url_modified, $country, $state, $city, $merchant_location_id);

		 $flag = 1 if ($number_of_matches == 0);

		 $name_counter = $name_counter ? $name_counter +1 : 2 ;


	 }

	 return $location_name_url_modified;

}

=head3 createMerchant

=head4
Description:

	 This method creates a Merchant Affiliate 'MA' member record moves 
	 the shopper member below the merchant, creates a Merchant Master 
	 record, a Merchant location Record, the Vendor Record, and debits
	 the Club Account (if the Club Account transaction information is
	 passed in).

=head4
Params:

	 params	hashref
				 {
					 merchant_type	
					 member_id		
					 referral_id		
					 business_name	
					 firstname1		
					 lastname1		
					 address1		
					 city			
					 country			
					 state			
					 postalcode		
					 longitude
					 latitude
					 phone			
					 home_phone		
					 scanner_number	
					 fax				
					 language		
					 emailaddress1	
					 emailaddress2	
					 username		
					 password		
					 url				
					 website_language	
					 business_type	
					 percentage_of_sale	
					 exception_percentage_of_sale	
					 payment_method	
					 ca_number	
					 ec_number	
					 cc_type		
					 cc_number	
					 ccv_number	
					 cc_expiration_date	
					 cc_name_on_card	
					 cc_address		
					 cc_city			
					 cc_country		
					 cc_state		
					 cc_postalcode	
					 merchant_package	
				 }


	 debit_club_account	arrayref of hashrefs
							 [ 
								 {
									 transaction_type	Int	This should be a value from the "trans_type" field in the "transaction_types" table,
									 amount	Double (IN USD)	The amount to credit the new members club account,
									 description	String	A description of the credit to the club account of the new member,
									 operator	String	The name of the person, or process creating this,
									 member_id	Int	(Optional) If a Members ID is specified the transaction is created for that member, if it is not specified the transaction is made for the MA Membership that was created.
								 },

							 ]

=head4
Return:

	 returns	int	Merchant Master Record ID on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=cut

sub createMerchant
{
	 my $this = shift;
	 my $params = shift;
	 my $debit_club_account = shift;

	 my %merchant_master_record_info = ();
	 my %merchant_location_record_info = ();
	 my %merchant_member_info = ();
	 my %shopper_member_info = ();
	 my $new_merchant_member_id = {};
	 my $new_merchant_shopper_member_id = {};
#	 warn "createMerchant 1 \n";
	 my $merchant_master_record_id = 0;

#	 warn "CreateMerchant: " . Dumper($params);

	 if( ref($debit_club_account) eq 'ARRAY')
	 {
		 foreach (@$debit_club_account)
		 {
			 die "MerchantAffiliates::createMerchant - The debit_club_account->{'transaction_type'} parameter was specified, but did not hold the correct information, only Integers ar allowed for this parameter.  $_->{'transaction_type'}" if ($_->{'transaction_type'} !~ /\d+/);
			 #TODO: Add a check here for double data type
			 #die "MerchantAffiliates::createMerchant - The debit_club_account->{'amount'} ($_->{'amount'}) parameter was specified, but did not hold the correct information, only posative Doubles are allowed. " if ($_->{'amount'} );
			 die "MerchantAffiliates::createMerchant - The debit_club_account->{'description'} parameter was specified, but did not hold the correct information." if ! $_->{'description'};
			 die "MerchantAffiliates::createMerchant - The debit_club_account->{'operator'} parameter was specified, but did not hold the correct information, this should hold the name of the person, or process calling this method." if ! $_->{'operator'};
			 die "MerchantAffiliates::createMerchant - The debit_club_account->{'member_id'} parameter was specified, but did not hold the correct information, only integers are allowed." if(exists $_->{'member_id'} && $_->{'member_id'} !~ /^\d+$/);

		 }
	 }
#warn "createMerchant 2 \n";

	 $params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};

	 ($merchant_member_info{'spid'} 				= $params->{'referral_id'} ? $params->{'referral_id'}: 1) =~ s/\D//g; #remove all non numeric data
	 $merchant_member_info{'spid'} 				=~ s/^0*//g;

	 $merchant_member_info{'membertype'} 			= 'ma';
	 $merchant_member_info{'firstname'} 			= $params->{'firstname1'} ? $params->{'firstname1'}: '';
	 $merchant_member_info{'lastname'} 			= $params->{'lastname1'} ? $params->{'lastname1'} : '';

	 #$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
	 #my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");

	 my $merchant_config = $this->retrieveMerchantConfigHashRef();

#warn "createMerchant 3 \n";
	 #TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Business Name.
	 #my $length = 31;
	 my $string_length = length($params->{'business_name'});

	 if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
	 {
		 my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
		 $params->{'business_name'} = substr($params->{'business_name'}, 0, - $characters_to_remove);
	 }

# these all used to read like this.... but why insert empty values instead of simply leaving the field NULL
#	 $merchant_member_info{'company_name'} 		= $params->{'business_name'} ? $params->{'business_name'}: '';
	 $merchant_member_info{'company_name'}	= $params->{'business_name'} if $params->{'business_name'};
	 $merchant_member_info{'emailaddress'}	= $params->{'emailaddress1'} ? $params->{'emailaddress1'}: '';
	 #$merchant_member_info{'emailaddress2'}	 	= $params->{'emailaddress2'} ? $params->{'emailaddress2'}: '';
	 $merchant_member_info{'address1'}		= $params->{'address1'} if $params->{'address1'};
	 $merchant_member_info{'city'}			= $params->{'city'} if $params->{'city'};
	 $merchant_member_info{'state'} 		= $params->{'state'} if $params->{'state'};
	 $merchant_member_info{'postalcode'} 	= $params->{'postalcode'} if $params->{'postalcode'};
	 $merchant_member_info{'country'} 		= $params->{'country'} if $params->{'country'};

	 $merchant_member_info{'phone'} 		= $params->{'home_phone'} if $params->{'home_phone'};
	 $merchant_member_info{'phone'}			||= $params->{'phone'} if $params->{'phone'};

	 $merchant_member_info{'fax_num'} 		= $params->{'fax'} if $params->{'fax'};
#	 ($merchant_member_info{'password'} 	= $params->{'password'} if $params->{'password'} : '') =~ s/\s//g;
	 $merchant_member_info{'password'} 	= $params->{'password'} if $params->{'password'};
	 $merchant_member_info{'memo'} 			= 'The membership was created by the online Merchant Registration form.';
	 $merchant_member_info{'mail_option_lvl'} 		= 2;
#	 $merchant_member_info{'language_pref'} = $params->{'language'} if $params->{'language'}:'en';
	 $merchant_member_info{'language_pref'} = $params->{'language'} if $params->{'language'};
	 $merchant_member_info{'cellphone'} 	= $params->{'scanner_number'} if $params->{'scanner_number'};
	 $merchant_member_info{'tin'} 			= $params->{'tin'} if $params->{'tin'};


	 #If you are going to turn on transactions this is where to start.
	 $this->{'db'}->{'AutoCommit'} = 0;

#	 warn "createMerchant 5 \n";

	 eval {
		 $this->setMembers() unless $this->{'Members'};

#	 warn "createMerchant 6 \n";

		 #TODO: If the Applicant supplies a Member ID use the Members 'my_upline_vip' for the Merchant Associate SPID, 
		 #if a Referral ID is supplied, and it is not a VIP use my_upline_vip for the SPID.
#		 if((defined $params->{'member_id'} && $params->{'member_id'}) || (defined $params->{'referral_id'} && $params->{'referral_id'}))
		if($params->{'member_id'} || $params->{'referral_id'})
		{

#			 my $id_to_check = (defined $params->{'member_id'} && $params->{'member_id'}) ? $params->{'member_id'} : $params->{'referral_id'};
			my $id_to_check = $params->{'member_id'} || $params->{'referral_id'};

			my $member_information = $this->{'Members'}->getMemberInformationByMemberID({'member_id'=>$id_to_check});

#	 warn "createMerchant 7 \n";

			if (grep $_ eq $member_information->{'membertype'}, (qw/v ag/) )
			{
				 $merchant_member_info{'spid'} = $member_information->{'id'};

				 #If the Member ID was passed in, and it is being used as a sponsor remove the index, it is not needed.  It will create a loop in the member upline.
				 delete $params->{'member_id'} if defined $params->{'member_id'};

			}
			else
			{

				 my $first_vip_upline = $this->{'db'}->selectrow_hashref("SELECT upline FROM my_upline_vip WHERE id = ?", undef, ($id_to_check));
#	 warn "createMerchant 8 \n";
#				 $merchant_member_info{'spid'} = $first_vip_upline->{'upline'} ? $first_vip_upline->{'upline'} : 1;
				 $merchant_member_info{'spid'} = $first_vip_upline->{'upline'} || 1;

				 delete $params->{'member_id'} if (defined $params->{'member_id'} && $member_information->{'membertype'} =~ /m/);
			}

		 }

#	 warn "createMerchant 9 \n";

		 $new_merchant_member_id = $this->{'Members'}->createNewMember(\%merchant_member_info, 1, 1); 
#	 warn "createMerchant 10 \n";


		 if(ref($new_merchant_member_id) ne 'HASH')
		 {
			 #TODO: Add some more here.
			 die 'MerchantAffiliates::createMerchant - The Merchant member record could not be created. ' . $new_merchant_member_id;
		 }

	 if($params->{'member_id'})
	 {

#		 warn "Update member $params->{'member_id'} spid to $new_merchant_member_id->{'member_id'} \n";

		 #Check to see if this is a valid member ID, and what thier spid is.
		 #If they have a referral_id check to make sure the referral_id is valid, and is a VIP
		 #if their spid matches the referral_id it's all good, and if not what then???
		 #TODO: What should I do if their member type is VIP?
		 $this->{'Members'}->updateMember($params->{'member_id'}, {'spid'=>$new_merchant_member_id->{'member_id'}, 'memo'=>'The SPID was updated when a Merchant Affiliate Membership was created.'});

#		 warn "Updated $params->{'member_id'} spid \n";

	 }
	 else
	 {

		 $shopper_member_info{'spid'}				= $new_merchant_member_id->{'member_id'};
		 $shopper_member_info{'membertype'} 		= 's';
		 $shopper_member_info{'firstname'} 		= $params->{'firstname1'} ? $params->{'firstname1'}: '';
		 $shopper_member_info{'lastname'} 			= $params->{'lastname1'} ? $params->{'lastname1'} : '';
		 $shopper_member_info{'company_name'} 		= $params->{'business_name'} ? $params->{'business_name'}: '';
		 $shopper_member_info{'emailaddress'} 		= $params->{'emailaddress2'} ? $params->{'emailaddress2'}: '';
		 #$shopper_member_info{'emailaddress2'}	 	= $params->{'emailaddress2'} ? $params->{'emailaddress2'}: '';
		 $shopper_member_info{'address1'} 			= $params->{'address1'} ? $params->{'address1'}: '';
		 $shopper_member_info{'city'} 				= $params->{'city'} ? $params->{'city'} : '';
		 $shopper_member_info{'state'} 			= $params->{'state'} ? $params->{'state'} : '';
		 $shopper_member_info{'postalcode'} 		= $params->{'postalcode'} ? $params->{'postalcode'}: '';
		 $shopper_member_info{'country'} 			= $params->{'country'} ? $params->{'country'}: '';
		 $shopper_member_info{'phone'} 			= $params->{'home_phone'} ? $params->{'home_phone'}: '';
		 $shopper_member_info{'fax_num'} 			= $params->{'fax'} ? $params->{'fax'}: '';
		 ($shopper_member_info{'password'} 			= $params->{'password'} ? $params->{'password'} : '') =~ s/\s//g;
		 $shopper_member_info{'memo'} 				= 'The membership was created by the Merchant Registration from.';
		 $shopper_member_info{'mail_option_lvl'} 	= 2;
		 $shopper_member_info{'language_pref'} 	= $params->{'language'} ? $params->{'language'}:'en';
		 $shopper_member_info{'cellphone'} 		= $params->{'scanner_number'} ? $params->{'scanner_number'}: '';

		 #$shopper_member_info{other_contact_info} 	= $params->{} ? $params->{}: ''; #skype name

		 #Maybe add these?
		 #$shopper_member_info{ipaddress} 			= $params->{ipaddress} ? $params->{ipaddress}: '';
		 #$shopper_member_info{signup_date} 			= $params->{signup_date} ? $params->{signup_date}: '';
		 #$shopper_member_info{coop_name} 			= $params->{coop_name} ? $params->{coop_name}: '';
		 #$shopper_member_info{landing_page}			= $params->{landing_page} ? $params->{landing_page}: '';
	 }

	 #Prep the info for creating the Merchant Master Record	
	 $merchant_master_record_info{'member_id'} = $new_merchant_member_id->{'member_id'};

	 #TODO: Change this, the referral ID should be from the $new_merchant_member_id->{'member_id'} member record spid
	 ($merchant_master_record_info{'referral_id'} = $params->{'referral_id'} ? $params->{'referral_id'} : 1) =~ s/\D//g;;
	 $merchant_master_record_info{'referral_id'} =~ s/^0*//g;

	 $merchant_master_record_info{'business_name'} = $params->{'business_name'};
	 $merchant_master_record_info{'firstname1'} = $params->{'firstname1'};
	 $merchant_master_record_info{'lastname1'} = $params->{'lastname1'};
	 $merchant_master_record_info{'address1'} = $params->{'address1'};
	 $merchant_master_record_info{'city'} = $params->{'city'};
	 $merchant_master_record_info{'country'} = $params->{'country'};
	 $merchant_master_record_info{'state'} = $params->{'state'};
	 $merchant_master_record_info{'postalcode'} = $params->{'postalcode'};
	 $merchant_master_record_info{'phone'} = $params->{'phone'};
	 $merchant_master_record_info{'fax'} = $params->{'fax'};
	 $merchant_master_record_info{'emailaddress1'} = $params->{'emailaddress1'};
	 $merchant_master_record_info{'emailaddress2'} = $params->{'emailaddress2'};
	 ($merchant_master_record_info{'username'} = $params->{'username'}) =~ s/\s//g;
	 ($merchant_master_record_info{'password'} = $params->{'password'}) =~ s/\s//g;
	 $merchant_master_record_info{'url'} = $params->{'url'};

	 $merchant_master_record_info{'business_type'} = $params->{'business_type'};

	 $merchant_master_record_info{'discount_subtype'} = $params->{'discount_subtype'};

	 $merchant_master_record_info{'online_mall_category'} = $params->{'online_mall_category'};
	 $merchant_master_record_info{'idnum_enter'} = $params->{'idnum_enter'};
	 $merchant_master_record_info{'idnum_receipt'} = $params->{'idnum_receipt'};

	 $merchant_master_record_info{'discount_type'} = (defined $params->{'merchant_package'} && $params->{'merchant_package'} =~ /^\d+$/) ? $params->{'merchant_package'}:0;

	 if ($merchant_master_record_info{'discount_subtype'} == 3) # percentage of sale
	 {
		 $merchant_master_record_info{'discount'} = $params->{'percentage_of_sale'} || $params->{'exception_percentage_of_sale'};
		 
		 # adding this in because at the moment (June 2015) the rewards directory is looking at the "special" field in the merchant_affiliates_master table for a value
		 $merchant_master_record_info{'special'} = $merchant_master_record_info{'discount'};
	 }
	 elsif ($merchant_master_record_info{'discount_subtype'} == 1) # flat fee has special but no discount
	 {
		 $merchant_master_record_info{'special'} = $params->{'special'};
		 $merchant_master_record_info{'discount'} = 0.00;
	 }
	 elsif ($merchant_master_record_info{'discount_subtype'} == 2)  # bogo by one get one type of deal
	 {
		 $merchant_master_record_info{'special'} = $params->{'special'};
		 $merchant_master_record_info{'discount'} = 0.0;
	 }
	 else
	 {
	 	die "Unrecognized discount_subtype: $merchant_master_record_info{'discount_subtype'}";
	 }

	 $merchant_master_record_info{'blurb'} = $params->{'blurb'} if exists $params->{'blurb'};

	 $merchant_master_record_info{'discount_blurb'} = $params->{'discount_blurb'} if exists $params->{'discount_blurb'};

#TODO: Decide what to do here at some point.  Right now all the merchant applications are for offline.
#	$merchant_master_record_info{} = $params->{'merchant_type'};

	 #some notes if need be.
	 $merchant_master_record_info{'notes'} = $params->{'notes'} ? $params->{'notes'} : '';

	 # Add some logic, so we are only insert the payment method they specify, or should I put the onous on the programmer before the method is called
	 ($merchant_master_record_info{'pay_cardnumber'} = $params->{'cc_number'}) =~ s/\D//g if $params->{'cc_number'};
	 $merchant_master_record_info{'pay_cardcode'} = $params->{ccv_number};
	 $merchant_master_record_info{'pay_cardexp'} = $params->{cc_expiration_date};
	 $merchant_master_record_info{'pay_name'} = $params->{cc_name_on_card};
	 $merchant_master_record_info{'pay_address'} = $params->{cc_address};
	 $merchant_master_record_info{'pay_city'} = $params->{cc_city};
	 $merchant_master_record_info{'pay_country'} = $params->{cc_country};
	 $merchant_master_record_info{'pay_state'} = $params->{cc_state};
	 $merchant_master_record_info{'pay_postalcode'} = $params->{cc_postalcode};
	 $merchant_master_record_info{'pay_payment_method'} = uc($params->{'payment_method'});

	 $params->{'ca_number'} =~ s/\D//g if $params->{'ca_number'};

	 if($params->{'ca_number'})
	 {
		 my $temporary_member_information_from_club_account_number = $this->{'Members'}->getMemberInformationByMemberID({'member_id'=>$params->{'ca_number'}}); 
		 $merchant_master_record_info{'ca_number'} = $params->{'ca_number'} if ($temporary_member_information_from_club_account_number->{'membertype'} eq 'v' && $temporary_member_information_from_club_account_number->{'status'} == 1);

		 if (defined $merchant_master_record_info{'ca_number'})
		 {
			 $merchant_master_record_info{'ca_number'} =~ s/\D//g;
			 $merchant_master_record_info{'ca_number'} =~ s/^0*//g;
		 }
	 }

	 if($params->{'ca_number_annual'})
	 {
		 my $temporary_member_information_from_club_account_number = $this->{'Members'}->getMemberInformationByMemberID({'member_id'=>$params->{'ca_number_annual'}}); 
#warn "createMerchant 11 \n";
		 $merchant_master_record_info{'ca_number_annual'} = $params->{'ca_number_annual'} if ($temporary_member_information_from_club_account_number->{'membertype'} eq 'v' && $temporary_member_information_from_club_account_number->{'status'} == 1);

		 if (defined $merchant_master_record_info{'ca_number_annual'})
		 {
			 $merchant_master_record_info{'ca_number_annual'} =~ s/\D//g;
			 $merchant_master_record_info{'ca_number_annual'} =~ s/^0*//g;
		 }
	 }


	 $merchant_master_record_info{'eco_card_number'} = $params->{'ec_number'};

	 $merchant_master_record_info{'url_lang_pref'} = $params->{'website_language'};

	 $merchant_master_record_info{'status'} = $params->{'status'} ? $params->{'status'}: 2;

#warn "createMerchant 12 \n";

# ???????
#	$merchant_master_record_info{'pay_payment_method'} = $params->{'payment_method'};
#	$merchant_master_record_info{} = $params->{'cc_type'}; #Does this matter?

#  status smallint,
#  contact_info character varying,
#  activated date,


	 #Create the Merchant Record
#warn "createMerchant 13 \n";

		 $merchant_master_record_id = $this->createMerchantMasterInformation(\%merchant_master_record_info, {'skip_subscription'=>1});
#	 warn "createMerchant 14 \n";

		 #TODO: Move the clubaccount transactions here, so we can include the Merchant ID in the descriptions.
		 #TODO: Move the subscription creation here

		 if(ref($debit_club_account) eq 'ARRAY')
		 {

			 foreach (@$debit_club_account)
			 {
				 #There should be no need to check the information, if the correct parameters were not passed we shouldn't have gotten this far.

				 my $member_to_create_the_transaction_for = defined $_->{'member_id'} ? $_->{'member_id'} : $merchant_master_record_info{'member_id'};

				 my @debit_club_account_data = (
													 $member_to_create_the_transaction_for, 
													 $_->{'transaction_type'}, 
													 ($_->{'amount'}), 
													 "$_->{'description'} - For Merchant ID: $merchant_master_record_id",
													 $_->{'operator'}
												 );

				 my $transaction_id = $this->{'db'}->selectrow_array(
																	 'SELECT debit_trans_complete(?,?,?,?,NULL,?)',
																	 undef,
																	 @debit_club_account_data
																 );

#warn "createMerchant 15 \n";

				 die 'There was an error crediting the members Club Account. @debit_club_account_data: ' . Dumper(@debit_club_account_data) if ! $transaction_id;
			 }
		 }
#warn "createMerchant 16 \n";

		 $this->createMerchantSubscription(\%merchant_master_record_info);


#warn "createMerchant 17 \n";



		 $merchant_location_record_info{'merchant_id'} = $merchant_master_record_id;

		 #$merchant_location_record_info{'vendor_id'} = '';$this is added in the createMerchantLocationInformation method.
		 $merchant_location_record_info{'location_name'} = $params->{'business_name'};
		 $merchant_location_record_info{'address1'} = $params->{'address1'};
		 $merchant_location_record_info{'city'} = $params->{'city'};
		 $merchant_location_record_info{'state'} = $params->{'state'};
		 $merchant_location_record_info{'postalcode'} = $params->{'postalcode'};
		 $merchant_location_record_info{'longitude'} = $params->{'longitude'};
		 $merchant_location_record_info{'latitude'} = $params->{'latitude'};
		 $merchant_location_record_info{'country'} = $params->{'country'};
		 $merchant_location_record_info{'phone'} = $params->{'phone'};
		 ($merchant_location_record_info{'username'} = $params->{'username'}) =~ s/\s//g;
		 ($merchant_location_record_info{'password'} = $params->{'password'}) =~ s/\s//g;

		 #This is a little rediculous, but there is a blurb field in the "venor" table which is created when the
		 #Merchant Affiliate Location Record is created, the location record has a notes field, but no blurb field.
		 #The blurb field in the Vendor table is used to display special notes in the Mall listing.
		 #I believe the notes in the Merchant Location Record is for internal use, but the blurb field in the
		 #Vendors record is used for display in the Mall, this needs to be straightened out at some point.
		 $merchant_location_record_info{'notes'} = $params->{'blurb'};

		 # Information needed to create the Vendor Record. 
		 $merchant_location_record_info{'url'} = $params->{'url'};

		 if(exists $params->{'special'} && $params->{'special'})
		 {
			 $merchant_location_record_info{'discount'} = 0.00;
		 }
		 else
		 {
			 #$merchant_location_record_info{'discount'} = $params->{'percentage_of_sale'} ? $params->{'percentage_of_sale'} : $params->{'exception_percentage_of_sale'};
			 $merchant_location_record_info{'discount'} = $params->{'percentage_of_sale'} || $params->{'exception_percentage_of_sale'} || 0;
		 }


		$merchant_location_record_info{'discount_type'} = $params->{'merchant_package'};
		$merchant_location_record_info{'scanner'} = $params->{'scanner_number'};
		$merchant_location_record_info{'primary_mallcat'} = $params->{'primary_mallcat'};

		 #Create the Merchant Location Record.
#warn "createMerchant 18 \n";
		my $merchant_location_id = $this->createMerchantLocationInformation(\%merchant_location_record_info);

#warn "createMerchant 19 \n";

#
# Create merchant notification for approval if not an online merchant
#

		if ($params->{'merchant_package'} < 20)
		{
		     my %notification_values = ();

		     $notification_values{'id'} = $merchant_master_record_id;
		     $notification_values{'notification_type'} = 103;
		     $notification_values{'memo'} = 'Approved Merchant Application.';

		     $this->createNotification(\%notification_values);
		 }
#warn "createMerchant 20 \n";

	 };

	 if($@)
	 {
		 $this->{'db'}->rollback();
		 $this->{'db'}->{'AutoCommit'} = 1;

		 die 'MerchantAffiliates::createMerchant: ' . $@;
	 }

	 $this->{'db'}->commit();	
	 $this->{'db'}->{'AutoCommit'} = 1;

#warn "createMerchant 21 \n";
	 $new_merchant_member_id->{'confirmation'} = $this->{'Members'}->confirmMembership({'id'=>$new_merchant_member_id->{'member_id'}});

	 if (! $new_merchant_member_id->{'confirmation'})
	 {
		 #TODO: Decide what to do if the merchant membership is not confirmed.
		 use Data::Dumper;
		 die 'The Merchant Membership could not be confirmed.' . Dumper($new_merchant_member_id);
	 }

	 #TODO: Check to make sure all is good. Ideally transactions will be used.
	 return $merchant_master_record_id;
}

=head3 retrieveMerchantCookie

	 Retrieve the Merchant Cookie, decode it, then return the Location IDs as an array.
	 If no cookie is set, they have not logged in yet.

=head4
Params:

	 $CGI_cookie			ref		A reference to the CGI::cookie sub (sub{$CGI->cookie(@_)})
	 $G_S_authen_ses_key	ref 	A reference to the G_S::authen_ses_key sub (sub{$G_S->authen_ses_key(@_)})

=head4
Returns:

	 hashref	
			 login_id	int	The login ID
			 ma_info	hashref	
			 {
				 maflocations	string	A CSV of location IDs
				 There is more I'll add them here as I find out what they are.
			 }

=head4
Errors:

	 0 is returned, and a message is written to the error log. 

=cut

sub retrieveMerchantCookie
{
	 my $this = shift;
	 my $CGI_cookie = shift;
	 my $G_S_authen_ses_key = shift;


	 if( ref $CGI_cookie ne 'CODE')
	 {
		 warn 'MerchantAffiliates::retrieveMerchantCookie - Error the $CGI_cookie param should be a code reference to CGI::cookie, but it isnt.';

		 return 0;
	 }

	 if( ref $G_S_authen_ses_key ne 'CODE')
	 {
		 warn 'MerchantAffiliates::retrieveMerchantCookie - Error the $G_S_authen_ses_key param should be a code reference to G_S::authen_ses_key, but it isnt.';

		 return 0;
	 }


	 my $cookie_info = {};

	 # Get the Login ID and Merchant Affiliate info hash.

	 eval{
		 ( $cookie_info->{'login_id'}, $cookie_info->{'ma_info'} ) = &$G_S_authen_ses_key(&$CGI_cookie('AuthCustom_maf'), 'maf' );
	 };
	 if($@)
	 {
		 warn 'MerchantAffiliates::retrieveMerchantCookie - Error retrieving the cookie.  Error: ' . $@;
		 return 0;
	 }
	 # If the Login ID doesn't exist or has any non-digits send them to login again.
	 if (!$cookie_info->{'login_id'} || $cookie_info->{'login_id'} =~ /\D/)
	 {
		 return 0;
	 }

	 return $cookie_info;

}

=head3 retrieveMerchantMasterLocationByMerchantID

=head4
Description:

	 Get the Merchants Master Location information by a Merchants ID.
	 The location that is returned is the location with the lowest
	 ID, since that is the first one that was created.

=head4
Params:

	 hash
		 merchant_id	int		Merchant Master ID

=head4
Returns:

	 hashref or null		A hashref containing the 
						 merchants master location record is returned,
						 if there is no data null is returned.

=cut

sub retrieveMerchantMasterLocationByMerchantID
{
	 my $this = shift;
	 my $merchant_id = shift;

	 $merchant_id =~ s/\s+//g if $merchant_id;

	 my $query = <<EOT;

		 SELECT
			 mal.*
		 FROM
			 merchant_affiliate_locations mal
		 WHERE
			 mal.merchant_id = ?
		 ORDER BY id
		 LIMIT 1

EOT

	 my $return_hashref = '';

	 if (!$merchant_id)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterLocationByMerchantID - The 'merchant_id' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($merchant_id);
		 $return_hashref = $sth->fetchrow_hashref;

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterLocationByMerchantID - An error occured retrieving the merchant master record.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;

}

=head3 retrieveAllMerchantLocationsByLocationID

=head4
Description:

	 Get all the Merchants locations by passing in one Location ID.

=head4
Params:

	 location_id	int		Merchant Master ID


=head4
Returns:

	 arrayref of hashrefs or null	An arrayref of hashref containing the 
									 merchants location records,
									 if there is no data null is returned.

=cut

sub retrieveAllMerchantLocationsByLocationID
{
	 my $this = shift;
	 my $location_id = shift;

	 $location_id =~ s/\s+//g if $location_id;

	 my $query = <<EOT;

		 SELECT
			 mal.*,
			 v.status,
			 v.url,
			 v.blurb,
			 v.coupon
		 FROM
			 merchant_affiliate_locations mal
			 LEFT JOIN
			 vendors v
			 ON
			 mal.vendor_id = v.vendor_id
		 WHERE
			 mal.merchant_id = (SELECT merchant_id FROM merchant_affiliate_locations WHERE id = ?)
		 ORDER BY id

EOT

	 my $return_arrayref_hashref = [];

	 if (!$location_id)
	 {
		 warn "MerchantAffiliates::retrieveAllMerchantLocationsByLocationID - The 'location_id' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($location_id);
		 #$return_arrayref_hashref = $sth->fetchrow_hashref;

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @$return_arrayref_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveAllMerchantLocationsByLocationID - An error occured retrieving the merchant location records.  Error: " .  $@;
		 return;
	 }

	 return $return_arrayref_hashref;

}

=head3 retrieveAllMerchantLocationsByMerchantID

=head4
Description:

	 Get all the Merchants locations by Merchant ID.

=head4
Params:

	 merchant_id	int		Merchant Master ID

	 bool	set this to one to skip the Merchants Master Location

=head4
Returns:

	 arrayref of hashrefs or null	An arrayref of hashref containing the 
									 merchants location records,
									 if there is no data null is returned.

=cut

sub retrieveAllMerchantLocationsByMerchantID
{
	 my $this = shift;
	 my $merchant_id = shift;
	 my $skip_master_location_flag = shift;

	 $merchant_id =~ s/\s+//g if $merchant_id;

	 my $query = <<EOT;

		 SELECT
			 mal.*,
			 v.status,
			 v.url,
			 v.blurb,
			 v.coupon
		 FROM
			 merchant_affiliate_locations mal
			 LEFT JOIN
			 vendors v
			 ON
			 mal.vendor_id = v.vendor_id
		 WHERE
			 v.status <> 0
			 AND
			 mal.merchant_id = ?
		 ORDER BY id

EOT

	 my $return_arrayref_hashref = [];

	 if (!$merchant_id)
	 {
		 warn "MerchantAffiliates::retrieveAllMerchantLocationsByMerchantID - The 'merchant_id' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

#	     warn $query . "---" . $merchant_id . "\n";
		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($merchant_id);
		 #$return_arrayref_hashref = $sth->fetchrow_hashref;
		 my $ii = 0;
		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 $ii++;
			 if ($skip_master_location_flag)
			 {
				 $skip_master_location_flag = 0;
				 next;
			 }
			 push @$return_arrayref_hashref, $rows;
		 }
#                warn "George5 " . $ii ."--". Dumper($return_arrayref_hashref);

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveAllMerchantLocationsByMerchantID - An error occured retrieving the merchant master record.  Error: " .  $@;
		 return;
	 }

	 return $return_arrayref_hashref;

}

=head3 retrieveMerchantMasterRecordByLocationID

=head4
Description:

	 Get the Merchants Master information by a Location ID

=head4
Params:

	 hash
		 location_id	int		Location ID

=head4
Returns:

	 hashref or null		A hashref containing the 
						 merchants master record is returned,
						 if there is no data null is returned.

=cut

sub retrieveMerchantMasterRecordByLocationID
{
	 my $this = shift;
	 my $location_id = shift;

	 $location_id =~ s/\s+//g if $location_id;

	 return if (! $location_id);




	 my $query = <<EOT;

		 SELECT
			 mam.*
		 FROM
			 merchant_affiliates_master mam
			 JOIN
			 merchant_affiliate_locations mal
			 ON
			 mam.id = mal.merchant_id
		 WHERE
			 mal.id = ?

EOT

	 my $return_hashref = '';

	 if (!$location_id)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordByLocationID - The 'location_id' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($location_id);
		 $return_hashref = $sth->fetchrow_hashref;

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordByLocationID - An error occured retrieving the merchant master record.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;

}


=head3 retrieveLocations

=head4
Description:

	 Retrieve merchants locations for a Country, Province, City, or a specified merchant location.

=head4
Params:

	 hashref
			 {

				 country =>	string,
				 state =>	string,
				 city =>	string,
				 #TODO: Add the stores unique identifier =>	string,
			 }

	 discount_types
	 STRING
	 A CSV string of discount types

	 language_code
	 STRING
	 A two letter language code

	 keywords
	 ARRAY REF
	 The keywords to use when retrieving merchant locations.

=head4
Returns:

	 array of hashrefs or null		

=cut

sub retrieveLocations
{
	 my $this = shift;
	 my $params = shift;
	 my $discount_types = shift;
	 my $language_code = shift;
	 my $keywords = shift;

	 die 'MerchantAffiliates::retrieveLocations - A hash is expected to be passed in. ' if ($params && ref($params) ne 'HASH');


	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*,//;
		 $discount_types =~ s/,\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }

	 if (defined $language_code &&  $language_code)
	 {

		 $language_code =~ s/\s//g;
		 $language_code =~ s/\d//g;
		 $language_code =~ s/(..).+/$1/g;

#		warn "\n language_code: $language_code \n";
#		warn CGI::url();


		 $language_code = 'en' if (length($language_code) < 2 || length($language_code) > 2 || $language_code !~ /\D\D/);

	 }
	 else
	 {
		 $language_code = 'en';
	 }






	 my $where = '';
	 my @where_fields = ();
	 my @where_values = ();
	 my @locations = ();
	 my $category_description_sort = 'category_description,';

	 my $keyword_placeholders = '';
	 my $keyword_join = '';
	 my $keyword_where = '';



	 if ($params)
	 {
		 foreach (keys %$params)
		 {
			 next if ! $_;

			 push @where_fields, " mal.$_ = ? ";
			 push @where_values, $params->{$_};
		 }
	 }


	 if(ref($keywords) eq 'ARRAY' && scalar(@$keywords))
	 {

		 my @place_holders = ();

		 foreach (@$keywords)
		 {
			 next if ! $_;

			 push @place_holders, '?';

			 push @where_values, $_;
		 }


		 $keyword_placeholders = join(',', @place_holders);

		 $category_description_sort = '';

	 }


	 $where = join(' AND ', @where_fields);

	 $where = " AND $where " if $where;

	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? " $where AND mam.discount_type IN ($discount_types) ": " AND mam.discount_type IN ($discount_types) ";
	 }

	 if($keyword_placeholders)
	 {
		 $keyword_join = 'JOIN merchant_keywords__merchant_master__links mkmml ON mkmml.merchant_id = mam.id';
		 $keyword_where = "AND mkmml.merchant_keywords_id IN ( SELECT mk.merchant_keywords_id FROM merchant_keywords mk WHERE mk.keyword IN ($keyword_placeholders) )";
	 }



	 my $query =<<EOT;

			 SELECT
				 DISTINCT
				 mal.*,
				 COALESCE(mam.url,'') AS url,
				 mam.discount_type,
				 COALESCE(mam.special,'') AS special,
				 COALESCE(mam.discount_blurb,'') AS discount_blurb,
				 mam.business_type,
				 d.cap,
				 d.discount,
				 v.blurb,
				 COALESCE(v.coupon,'') AS coupon,
				 bc.description AS category_description,
				 CASE 
					 WHEN 
						 mam.discount_type =  13
					 THEN 
						 1
					 WHEN 
						 mam.discount_type IN (12, 3)
					 THEN 
						 2
					 ELSE 
						 0 
				 END AS preference
			 FROM
				 merchant_affiliate_locations mal
				 JOIN
				 merchant_affiliates_master mam
					 ON
					 mal.merchant_id = mam.id
				 $keyword_join
				 JOIN
				 merchant_affiliate_discounts d
					 ON
					 mal.id = d.location_id
				 JOIN
				 merchant_discounts md
					 ON
					 md.type = mam.discount_type
				 JOIN
				 vendors v
					 ON 
					 v.vendor_id = mal.vendor_id
				 JOIN
				 business_codes('$language_code') bc
					 ON
					 mam.business_type = bc.code
			 WHERE
				 v.vendor_group = 5 
				 AND
				 NOW()::date >= d.start_date
				 AND
				 (
					 NOW()::date <= d.end_date 
					 OR 
					 d.end_date IS NULL
				 )
				 $where
				 AND
				 v.status= 1
				 -- exclude our house testing account
				 AND
				 mam.id != 41
				 $keyword_where
			 ORDER BY 
				 mal.city,
				 $category_description_sort
				 preference DESC,
				 mal.location_name

EOT


	 use Data::Dumper;

	 eval{
		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute(@where_values);

		 while(my $row = $sth->fetchrow_hashref())
		 {

			 push @locations, $row;
		 }

	 };
	 if ($@)
	 {
		 my $error_dumps = '';

		 $error_dumps .= "\n params: " . Dumper($params) . "\n" if $params;
		 $error_dumps .= ' discount_types: ' . Dumper($discount_types) . "\n" if $discount_types;
		 $error_dumps .= ' language_code: ' . Dumper($language_code) . "\n" if $language_code;
		 $error_dumps .= ' keywords: ' . Dumper($keywords) . "\n" if $keywords;

		 die "MerchantAffiliates::retrieveLocations - An error occured retrieving the merchant location.  Error: " .  $@ . $error_dumps;

	 }
	 return @locations;

}


=head3 retrieveMerchantLocationByLocationID

=head4
Description:

	 retrieve a Merchants location information by a Location ID

=head4
Params:

		 location_id	int		Merchants Location ID

=head4
Returns:

	 hashref or null		A hashref containing the 
						 merchants location record is returned,
						 if there is no data null is returned.

=cut

sub retrieveMerchantLocationByLocationID
{
	 my $this = shift;
	 my $location_id = shift;

	 $location_id =~ s/\s+//g if $location_id;

	 return if (! $location_id);

	 my $query = <<EOT;

		 SELECT
			 mal.*,
			 v.status,
			 v.url,
			 v.blurb,
			 v.coupon
		 FROM
			 merchant_affiliate_locations mal
			 LEFT JOIN
			 vendors v
			 ON
			 mal.vendor_id = v.vendor_id
		 WHERE
			 mal.id = ?

EOT

	 my $return_hashref = '';



	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($location_id);
		 $return_hashref = $sth->fetchrow_hashref;

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantLocationByLocationID - An error occured retrieving the merchant location record.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;

}


=head3 retrieveMerchantMallLocationByLocationID

=head4
Description:

	 retrieve a Merchants location information by a Location ID.
	 The difference between this method and the "retrieveMerchantLocationByLocationID" method
	 is the information returned here is the same as you would get in the Mall Listing.

=head4
Params:

		 location_id	int		Merchants Location ID

=head4
Returns:

	 hashref or null		A hashref containing the 
						 merchants location record is returned,
						 if there is no data null is returned.

=head4
TODO: 

	 When we redo the script that outputs the mall listing, and add methods to this class
	 for the store listings we should centralize the query so depending on the parameters 
	 the mall info is put out as many stores, or just one.  This way we wont have the same 
	 query in multiple locations.

=cut

sub retrieveMerchantMallLocationByLocationID
{
	 my $this = shift;
	 my $location_id = shift;

	 $location_id =~ s/\s+//g if $location_id;

	 return if (! $location_id);

	 my $query = <<EOT;

	 SELECT
		 mal.*,
		 yp.yp_heading AS description,
		 COALESCE(mam.url,'') AS url,
		 v.coupon,
		 mam.discount_type,
		 mam.special,
		 d.cap,
		 d.discount,
		 v.blurb
	 FROM
		 merchant_affiliate_locations mal
		 JOIN
		 merchant_affiliates_master mam
			 ON mal.merchant_id = mam.id
		 JOIN
		 merchant_affiliate_discounts d
			 ON mal.id = d.location_id
		 JOIN
		 vendors v
			 ON v.vendor_id = mal.vendor_id
		 JOIN
		 ypcats2naics yp
			 ON mam.business_type = yp.pk
	 WHERE
		 v.vendor_group = 5
		 AND
		 NOW()::date >= d.start_date
		 AND
		 (
			 NOW()::date <= d.end_date
			 OR 
			 d.end_date IS NULL
		 )
		 AND
		 mam.status != 0
		 AND
		 mam.status IS NOT NULL
		 AND
		 v.status= 1
		 -- exclude our house testing account
		 AND
		 mam.id != 41
		 AND
		 mal.id = ?
	 ORDER BY
		 mal.city,
		 description,
		 d.discount_type DESC, -- preference DESC, -- This should be d.discount_type
		 d.discount DESC,
		 mal.location_name

EOT

	 my $return_hashref = '';



	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($location_id);
		 $return_hashref = $sth->fetchrow_hashref;

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantLocationByLocationID - An error occured retrieving the merchant location record.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;

}


=head3 retrieveMerchantMasterRecordByID

=head4
Description:

	 Get the Merchants Master information by ID

=head4
Params:

		 id	int		The Merchants ID
		 exceptions	hashref	
							 {
								 with_pending_payment_information	bool	If set the merchants Pending Payment 
																			 information will be included with the 
																			 returned hashref.
							 }

=head4
Returns:

	 hashref or null		A hashref containing the 
						 merchants master record is returned,
						 if there is no data null is returned.

=cut

sub retrieveMerchantMasterRecordByID
{
	 my $this = shift;
	 my $id = shift;
	 my $exceptions = shift;

	 $id =~ s/\s+//g if $id;

	 return if (! $id);

	 my $merchant_master_information_with_pending_payment_query = <<EOT;

	 SELECT
		 mam.*,
		 mpp.id AS mpp_id,
		 mpp.pay_payment_method AS mpp_pay_payment_method,
		 mpp.ca_number AS mpp_ca_number,
		 mpp.status AS mpp_status,
		 mpp.discount_type AS mpp_discount_type,
		 mpp.discount AS mpp_discount,
		 mpp.special AS mpp_special,
		 mpp.discount_blurb AS mpp_discount_blurb
	 FROM
		 merchant_affiliates_master mam
		 LEFT JOIN
		 merchant_payment_pending mpp
		 ON
		 mam.id = mpp.merchant_id
		 AND
		 mpp.status = 0

	 WHERE
		 mam.id = ?

EOT



	 my $return_hashref = '';

	 if (!$id)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordByID - The 'id' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

		 #TODO: I should change this so it uses the getMerchantMaster method, then queries for the pending payment info, and combines the data.
		 if (defined $exceptions->{'with_pending_payment_information'})
		 {

			 my $sth = $this->{'db'}->prepare($merchant_master_information_with_pending_payment_query);
			 $sth->execute($id);
			 $return_hashref = $sth->fetchrow_hashref;

		 }
		 else
		 {

			 $return_hashref = $this->getMerchantMaster({'id'=>$id});

			 $return_hashref = $return_hashref->[0] if ref($return_hashref) eq 'ARRAY';
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordByID - An error occured retrieving the merchant master record.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;

}

=head3 retrieveMerchantMasterRecordsWithSponsorInfo

=head4
Description:

	 Get an array of the Merhcants who have joined most recently

=head4
Params:

		 limit
		 INT
		 The number of merchants to return.

=head4
Returns:

	 arrayref or null
	 An arrayref containing the merchants master records, and the 
	 firstname, lastname, and member id of their sponsor.

=cut

sub retrieveMerchantMasterRecordsWithSponsorInfo
{
	 my $this = shift;
	 my $limit = shift;
	 my $exceptions = shift;

	 $limit =~ s/\s+//g if $limit;

	 return if (! $limit);

	 my $merchant_config = $this->retrieveMerchantConfigHashRef();
	 my @values = ();
	 my $not_in_spid = '';

	 foreach (@{$merchant_config->{'house_accounts'}->{'house_account'}})
	 {
		 push @values, $_;
	 }

	 $not_in_spid = join(',',@values);

	 my $query = <<EOT;

	 SELECT
		 m.*,
		 sponsor_member.sponsors_id,
		 sponsor_member.sponsors_firstname,
		 sponsor_member.sponsors_lastname
	 FROM
		 members m
		 LEFT JOIN
		 (
			 SELECT
				 sm.id AS sponsors_id,
				 sm.firstname AS sponsors_firstname,
				 sm.lastname AS sponsors_lastname
			 FROM
				 members sm
			 WHERE
				 sm.membertype = 'v'

		 ) sponsor_member
		 ON
		 m.spid = sponsor_member.sponsors_id
	 WHERE
		 m.membertype = 'ma'
		 AND
		 m.spid NOT IN ($not_in_spid)
	 ORDER BY
		 m.join_date DESC
	 LIMIT ?

EOT

	 my $return_arrayref = [];

	 if (!$limit)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordsWithSponsorInfo - The 'limit' is a required parameter, but it was not specified.";
		 return;
	 }

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($limit);

		 while (my $row = $sth->fetchrow_hashref)
		 {
			 push @$return_arrayref, $row;
		 }
	 };

	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantMasterRecordsWithSponsorInfo - An error occured retrieving the merchant master record.  Error: " .  $@;
		 return;
	 }

	 return $return_arrayref;

}


=head3 retrieveMerchantConfig

=head4
Description:

	 Get the Merchant Config file, and return it as an XML Document.

=head4
Params:

	 dont_return_config	BOOL	TRUE to return null (Yes this was an after thought).

=head4
Returns:

	 string or null		The Merchant Config file as an XML Document.

=cut

sub retrieveMerchantConfig
{
	 my $this = shift;
	 my $dont_return_config = shift;

	 return $this->{'_configuration_xml'} if defined $this->{'configuration_xml'};

	 my $config_id = 10681;

	 $this->{'_configuration_xml'} = G_S::Get_Object($this->{'db'}, $config_id);

	 return $dont_return_config ? undef : $this->{'_configuration_xml'};

}

=head3 retrieveMerchantConfigHashRef

=head4
Description:

	 Get the Merchant Config file, then convert it into a HASHREF,
	 and return it.

=head4
Params:

	 None

=head4
Returns:

	 string		The Merchant Config file as an XML Document.

=cut

sub retrieveMerchantConfigHashRef
{
	 my $this = shift;

	 return XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>") if defined $this->{'configuration_xml'};

	 $this->retrieveMerchantConfig(1);

	 return XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>") 

}

=head3 retrieveMerchantPackageCost

=head4
Description:

	 Retrieve the cost of a merchant package.

=head4
Params:

	 merchant_type	string online, or offline.  offline is the default.
	 discount_type	int	The merchant package ID

=head4
Returns:

	 double	The cost of the Merchant Package.

=cut

sub retrieveMerchantPackageCost
{
	 my $this = shift;
	 my $merchant_type = shift; #offline, or online
	 my $discount_type = shift; # The merchant package ID
	 my $merchants_country = shift;

	 $merchants_country = lc($merchants_country);
	 $merchant_type = $merchant_type ? lc($merchant_type) : 'offline';

	 my $package_price_label = '';

	 my $pricing = $this->retrieveMerchantConfigHashRef(); 

	 # The discount type should match the "type" field in the "merchant_discounts" table.

	 if ($discount_type)
	 {

		 $package_price_label = $this->getMerchantPackageNameById($discount_type);

	 }

	 my $pricing_type = $pricing->{'regular_price_countries'}->{$merchant_type}->{$merchants_country} ? 'regular':'sale';

	 return $discount_type ? $pricing->{'prices'}->{$merchant_type}->{$pricing_type}->{$package_price_label} : 0;
}

=head3 retrieveMerchantSeedCost

=head4
Description:

	 Retrieve the minimum amount needed in a 
	 Merchant Members Club Account to use their 
	 Club Account as a method of payment.

=head4
Params:

	 merchant_type	string online, or offline.  offline is the default.
	 discount_type	int	The merchant package ID

=head4
Returns:

	 double	The cost of the Merchant Package.

=cut

sub retrieveMerchantSeedCost
{
	 my $this = shift;
	 my $merchant_type = shift; #offline, or online

	 $merchant_type = $merchant_type ? lc($merchant_type): 'offline';

	 my $package_price_label = 'seed';

	 my $pricing = $this->retrieveMerchantConfigHashRef();

	 return $pricing->{'prices'}->{$merchant_type}->{'seed'};

}

=head3 retrievePendingMerchant

=head4
Description:

	 Retrieve the pending information for a
	 Pending Merchant.

=head4
Params:

	 hashref The key is the field to search, and the
			 value is the value to search for. If
			 one of the keys are "id" one row is
			 returned from the cache.

=head4
Returns:

	 arrayref of hashrefs on failure return null

=head4
NOTE:

	 This method is only used in one place, and is not called very often.
	 If is starts being used heavily we may want to remove the subquery
	 that retrieves the number of merchants in a location, and make
	 that a separate method, then mashup the the pending merchants
	 with the number of active merchants in an area.

=head4
TODO:

	 Check to see if this is used any where, this should 
	 be in the PotentialMerchantAffiliates Class.

=cut

sub retrievePendingMerchant
{
	 my $this = shift;
	 my $params = shift;

	 my $query = <<EOT;

					 SELECT
						 mp.*,
						 mc.number_of_merchants_in_a_province
					 FROM
						 merchant_pending mp
						 LEFT JOIN
						 (
							 SELECT
								 COUNT(DISTINCT mam.id) AS number_of_merchants_in_a_province,
								 mal.state,
								 mal.country
							 FROM 
								 merchant_affiliate_locations mal
								 JOIN 
									 merchant_affiliates_master mam
									 ON 
									 mal.merchant_id = mam.id
								 JOIN 
									 merchant_affiliate_discounts d
									 ON 
									 mal.id = d.location_id
								 JOIN 
									 vendors v
									 ON 
									 v.vendor_id = mal.vendor_id
								 JOIN 
									 ypcats2naics yp
									 ON 
									 mam.business_type = yp.pk
							 WHERE 
								 v.vendor_group = 5 
								 AND 
								 NOW()::date >= d.start_date
								 AND	
								 (NOW()::date <= d.end_date OR d.end_date IS NULL)
								 AND	
								 mam.status != 0 
								 AND	
								 mam.status IS NOT NULL 
								 AND	
								 v.status IN (1,2) -- This is Active, and Approved
							 -- exclude our house testing account
								 AND 
								 mam.id != 41
							 GROUP BY
								 mal.country,
								 mal.state
						 ) mc
						 ON
						 mc.country = mp.country
						 AND
						 mc.state = mp.state
					 WHERE

EOT

	 my $fields = '';
	 my @values = ();
	 my $return_arrayref = ();

	 ########################################
	 # Build the fields to query with.
	 ########################################
	 foreach my $key (keys %$params)
	 {
		 next if ! defined $params->{$key};

		 if($key eq 'status')
		 {
			 if($params->{$key} =~ /\d+/ && $params->{$key} == 0)
			 {
				 $fields .= "(mp.status = ? OR mp.status is NULL) AND ";
				 push @values, $params->{$key};
			 }
			 elsif($params->{$key} =~ /\d+/ && $params->{$key} == 1)
			 {
				 $fields .= "mp.status = ? AND ";
				 push @values, $params->{$key};
			 }
		 }
		 elsif($params->{$key} ne 'NULL')
		 {
			 $fields .= "CAST(mp.$key AS text) ILIKE CAST(? AS text) AND ";
			 push @values, $params->{$key};
		 } else {
			 $fields .= "(mp.$key IS NULL OR mp.$key = 0 ) AND ";
		 }

	 }

	 $fields =~ s/AND\s$//;

	 return if !$fields;

	 $query .= $fields;

	 $query .= ' ORDER BY mp.id DESC';

	 eval{

#		warn "n\n query: ";
#		warn $query;
#		
#		warn "\n\n \@values: ";
#		my $temp = \@values;
#		use Data::Dumper;
#		warn Dumper($temp);

		 my $STH = $this->{'db'}->prepare($query);

		 $STH->execute(@values);

		 if(defined $params->{'id'} && $params->{'id'})
		 {
			 my $rows = $STH->fetchrow_hashref();

			 push @$return_arrayref, $rows;

		 } else {
			 while(my $rows = $STH->fetchrow_hashref())
			 {
				 push @$return_arrayref, $rows;
			 }

		 }


	 };
	 if($@)
	 {
		 die "There was an issue with retreiving the information.  <br />Query: $query <br /> " .  $@;
	 }


	 return $return_arrayref->[0] if(defined $params->{'id'} && $params->{'id'});

	 # At some point we may want to loop through "$return_arrayref",
	 # and set the cache for each pk, but there is no need at this point.

	 return $return_arrayref;


}

=head3 retrieveNumberOfMerchantsInAProvince

=head4
Description:

	 Retrieve the number of Merchants in a province,
	 merchants that have an approved, or active status

=head4
Params:

	 country
	 STRING
	 The ISO Country Code

	 province
	 STRING
	 The ISO Province Code, or the province name if an ISO code does not exist.

=head4
Returns:

	 INT	The number of Approved, and Active merchants in a Province.

=cut

sub retrieveNumberOfMerchantsInAProvince
{

	 my $this = shift;
	 my $country = shift;
	 my $province = shift;
	 my $results = {number_of_merchants_in_a_province=>0};

	 #TODO:  Create a cache for the number of merchants in a province, and add a force parameter

	 die 'The country is a required parameter but it was not provided. ' if ! $country;

	 die 'The province is a required parameter but it was not provided. ' if ! $province;

	 my $query=<<EOT;

							 SELECT
								 COUNT(DISTINCT mam.id) AS number_of_merchants_in_a_province
							 FROM 
								 merchant_affiliate_locations mal
								 JOIN 
									 merchant_affiliates_master mam
									 ON 
									 mal.merchant_id = mam.id
								 JOIN 
									 merchant_affiliate_discounts d
									 ON 
									 mal.id = d.location_id
								 JOIN 
									 vendors v
									 ON 
									 v.vendor_id = mal.vendor_id
								 JOIN 
									 ypcats2naics yp
									 ON 
									 mam.business_type = yp.pk
							 WHERE 
								 v.vendor_group = 5 
								 AND 
								 NOW()::date >= d.start_date
								 AND	
								 (NOW()::date <= d.end_date OR d.end_date IS NULL)
								 AND	
								 mal.country= ?
								 AND	
								 mal.state= ?
								 AND	
								 mam.status != 0 
								 AND	
								 mam.status IS NOT NULL 
								 --AND	
								 --v.status IN (1,2) -- This is Active, and Approved
							 -- exclude our house testing account
								 AND 
								 mam.id != 41

EOT

	 eval{
		 $results = $this->{'db'}->selectrow_hashref($query, undef, ($country, $province));
	 };
	 if($@)
	 {
		 die 'MerchantAffiliates::retrieveNumberOfMerchantLocationsInAProvince($country, $province) - There was an error retrieving the number of Merchants in a province. - Error: ' . $@;
	 }



	 return $results->{'number_of_merchants_in_a_province'}

}


=head3 updatePendingMerchantStatus

=head4
Params:

	 id	int	The ID of the record you want to update.

=head4
Returns:

	 number of rows affected.

=cut

sub updatePendingMerchantStatus
{
	 my $this	=	shift;
	 my $id		=	shift;
	 my $number_of_rows = 0;

	 return 0 if ! $id;

	 my $update_query = 'UPDATE merchant_pending SET status= 1 WHERE id =?';

	 eval{
		 $number_of_rows = $this->{'db'}->do($update_query, undef, ($id));
	 };
	 if($@)
	 {
		 die "There was an error executing the query. <br />Query: $update_query <br />id: $id - Error: " . $@;
	 }

	 return $number_of_rows;

}

=head3 deletePendingMerchant

=head4
Params:

	 id	int	The ID of the record you want to delete.

=head4
Returns:

	 int	The number of rows affected, this should not be more than 1.

=cut

sub deletePendingMerchant
{
	 my $this	=	shift;
	 my $id		=	shift;
	 my $number_of_rows = 0;

	 return 0 if ! $id;

	 my $delete_query = 'DELETE FROM merchant_pending WHERE id = ?';

	 eval{
		 $number_of_rows = $this->{'db'}->do($delete_query, undef, ($id));
	 };
	 if($@)
	 {
		 die "There was an error executing the query. <br />Query: $delete_query <br />id: $id - Error: " . $@;
	 }

	 return $number_of_rows;

}

=head3 updatePendingMerchantInformation

=head4
Description:

	 This updates a Pending Merchants Record.  This method does not update the payment information due to the current inplementation of the JavaScript getstates 
	 Library. The library only allows for one dynamic state listing on a page, where we need two on the Pending Merchant Update screens.

=head4
Params:

	 pending_merchant_id	int	The primary key for the record you want to update in the pending_merchants table.

	 params					hashref
								 should be a hashref where the keys
								 are named the same as the fields in
								 the pending_merchants table.

=head4
Return:

	 the number of rows effected.

=cut

sub updatePendingMerchantInformation
{
	 my $this = shift;
	 my $pending_merchant_id = shift;
	 my $params = shift;

	 my $return = 0;


	 if (! $params)
	 {
		 die 'No parameters were passed.';
	 }

	 if (! $pending_merchant_id)
	 {
		 die 'The pending_merchant_id is a required parameter.';
	 }


	 $params->{'city'} = Lingua::EN::NameCase::NameCase($params->{'city'}) if $params->{'city'};

	 my %pending_merchant_info = ();

					 $pending_merchant_info{'member_id'} = $params->{'member_id'} ? $params->{'member_id'}:undef;
					 $pending_merchant_info{'referral_id'} = $params->{'referral_id'} ? $params->{'referral_id'}:undef;

					 #TODO: When the Sanitize class is available, use one of the methods there to adjust the length of the Location Name.
					 #my $length = 31;
					 #$this->retrieveMerchantConfig() if ! defined $this->{'_configuration_xml'};
					 #my $merchant_config = XML::Simple::XMLin("<base>$this->{'_configuration_xml'}</base>");
					 my $merchant_config = $this->retrieveMerchantConfigHashRef();

					 my $string_length = length($params->{'business_name'});
					 if ($string_length > $merchant_config->{'business_name'}->{'max_length'})
					 {
						 my $characters_to_remove = $string_length - $merchant_config->{'business_name'}->{'max_length'};
						 $params->{'business_name'} = substr($params->{'business_name'}, 0, - $characters_to_remove);
					 }

					 $pending_merchant_info{'business_name'} = $params->{'business_name'} ? $params->{'business_name'}:undef;
					 $pending_merchant_info{'firstname1'} = $params->{'firstname1'} ? $params->{'firstname1'}:undef;
					 $pending_merchant_info{'lastname1'} = $params->{'lastname1'} ? $params->{'lastname1'}:undef;
					 $pending_merchant_info{'address1'} = $params->{'address1'} ? $params->{'address1'}:undef;
					 $pending_merchant_info{'city'} = $params->{'city'} ? $params->{'city'}:undef;
					 $pending_merchant_info{'state'} = $params->{'state'} ? $params->{'state'}:undef;
					 $pending_merchant_info{'country'} = $params->{'country'} ? $params->{'country'}:undef;
					 $pending_merchant_info{'postalcode'} = $params->{'postalcode'} ? $params->{'postalcode'}:undef;
					 $pending_merchant_info{'longitude'} = $params->{'longitude'} ? $params->{'longitude'}:undef;
					 $pending_merchant_info{'latitude'} = $params->{'latitude'} ? $params->{'latitude'}:undef;
					 $pending_merchant_info{'phone'} = $params->{'phone'} ? $params->{'phone'}:undef;
					 $pending_merchant_info{'cell_phone'} = $params->{'cell_phone'} ? $params->{'cell_phone'}:undef;
					 $pending_merchant_info{'fax'} = $params->{'fax'} ? $params->{'fax'}:undef;

					 $pending_merchant_info{'language_pref'} = $params->{'language_pref'} ? $params->{'language_pref'}:undef;
					 $pending_merchant_info{'cc_type'} = $params->{'cc_type'} ? $params->{'cc_type'}:undef;
					 $pending_merchant_info{'business_phone'} = $params->{'business_phone'} ? $params->{'business_phone'}:undef;


					 $pending_merchant_info{'merchant_package'} = $params->{'merchant_package'} ? $params->{'merchant_package'}:undef;

					 $pending_merchant_info{'emailaddress1'} = $params->{'emailaddress1'} ? $params->{'emailaddress1'}:undef;
#					$pending_merchant_info{'emailaddress2'} = $params->{'emailaddress2'} ? $params->{'emailaddress2'}:undef;
					 $pending_merchant_info{'username'} = $params->{'username'} ? $params->{'username'}:undef;
					 $pending_merchant_info{'password'} = $params->{'password'} ? $params->{'password'}:undef;
					 $pending_merchant_info{'url'} = $params->{'url'} ? $params->{'url'}:undef;
					 $pending_merchant_info{'website_language_pref'} = $params->{'website_language_pref'} ? $params->{'website_language_pref'}:undef;
					 $pending_merchant_info{'business_type'} = $params->{'business_type'} ? $params->{'business_type'}:undef;
					 $pending_merchant_info{'percentage_of_sale'} = $params->{'percentage_of_sale'} ? $params->{'percentage_of_sale'}:undef;
					 $pending_merchant_info{'exception_percentage_of_sale'} = $params->{'exception_percentage_of_sale'} ? $params->{'exception_percentage_of_sale'}:undef;

					 $pending_merchant_info{'special'} = $params->{'special'} ? $params->{'special'}:undef;
					 $pending_merchant_info{'percentage_off'} = $params->{'percentage_off'} ? $params->{'percentage_off'}:undef;
					 $pending_merchant_info{'flat_fee_off'} = $params->{'flat_fee_off'} ? $params->{'flat_fee_off'}:undef;

					 $pending_merchant_info{'discount_blurb'} = $params->{'discount_blurb'} ? $params->{'discount_blurb'}:undef;



					 # We may want to uncomment this in the near future when we have a JavaScript Country Province library that allows for more than one country province select boxes on the same page.
#	  				$pending_merchant_info{'payment_method'} = $params->{'payment_method'} ? $params->{'payment_method'}:undef;
#	  				$pending_merchant_info{'pay_name'} = $params->{cc_name_on_card} ? $params->{cc_name_on_card}:undef;
#	  				$pending_merchant_info{'pay_address'} = $params->{cc_address} ? $params->{cc_address}:undef;
#	  				$pending_merchant_info{'pay_city'} = $params->{cc_city} ? $params->{cc_city}:undef;
#	  				$pending_merchant_info{'pay_state'} = $params->{cc_state} ? $params->{cc_state}:undef;
#	  				$pending_merchant_info{'pay_country'} = $params->{cc_country} ? $params->{cc_country}:undef;
#	  				$pending_merchant_info{'pay_postalcode'} = $params->{cc_postalcode} ? $params->{cc_postalcode}:undef;
#	  				$pending_merchant_info{'pay_cardnumber'} = $params->{'cc_number'} ? $params->{'cc_number'}:undef;
#	  				$pending_merchant_info{'pay_cardcode'} = $params->{ccv_number} ? $params->{ccv_number}:undef;
#	  				$pending_merchant_info{'pay_cardexp'} = $params->{cc_expiration_date} ? $params->{cc_expiration_date}:undef;
#	  				
#	  				$pending_merchant_info{'eco_card_number'} = $params->{'ec_number'} ? $params->{'ec_number'}:undef;
#	  				
#	  				$pending_merchant_info{'ca_number'} = $params->{'ca_number'} ? $params->{'ca_number'}:undef;
#	  				
#	  				$pending_merchant_info{'if_number'} = $params->{'if_number'} ? $params->{'if_number'}:undef;


					 $pending_merchant_info{'status'} = $params->{'status'} ? $params->{'status'}:0;
					 $pending_merchant_info{'merchant_type'} = $params->{'merchant_type'} ? $params->{'merchant_type'}:'offline';


					 #sanitize some data
					 if($pending_merchant_info{'member_id'})
					 {
						 $pending_merchant_info{'member_id'} =~ s/\D//g;
						 $pending_merchant_info{'member_id'} =~ s/^0*//;
					 }

					 if($pending_merchant_info{'referral_id'})
					 {
						 $pending_merchant_info{'referral_id'} =~ s/\D//g;
						 $pending_merchant_info{'referral_id'} =~ s/^0*//;
					 }

					 # This shouldn't be needed, but it won't hurt.
					 if($pending_merchant_info{'ca_number'})
					 {
						 $pending_merchant_info{'ca_number'} =~ s/\D//g;
						 $pending_merchant_info{'ca_number'} =~ s/^0*//;
					 }

					 if($pending_merchant_info{'if_number'})
					 {
						 $pending_merchant_info{'if_number'} =~ s/\D//g;
						 $pending_merchant_info{'if_number'} =~ s/^0*//;
					 }


	 my $update_query  = ' UPDATE merchant_pending SET ';
	 my @fields = ();

	 my @values = ();

#	 warn "Into update for pending merchant\n";
	 ########################################
	 # Build the fields to insert into the
	 # "members" table, and "original_spid" table.
	 ########################################
	 foreach (keys %pending_merchant_info)
	 {
		 next if ($_ eq 'id');

		 $pending_merchant_info{$_} = undef if (defined $pending_merchant_info{$_} && $pending_merchant_info{$_} =~ /^\s*$/);

		 push @fields, "$_ = ?";
		 if ($this->{'G_S'})
		 {
		 	push @values, $this->{'G_S'}->Prepare_UTF8($pending_merchant_info{$_});
		 }
		 else
		 {
		 	push @values, G_S::Prepare_UTF8($pending_merchant_info{$_});
		 }
#		 push @values, decode_utf8($pending_merchant_info{$_});	# this did not properly deal with cases where the data already had the utf-8 flag set
	 }

	 push @values, $pending_merchant_id;

	 my $set_fields = join(', ', @fields);

	 $update_query .= " $set_fields WHERE id = ? ";

	 eval{

		 $return = $this->{'db'}->do($update_query, undef, @values);

	 };
	 if($@)
	 {
		 warn 'MerchantAffiliates::updatePendingMerchantInformation - There was an error updating the Pending Merchant Record. Error: ' . $@;

		 $return = 0;
	 }

	 return $return;
}

=head3 updatePendingMerchantPaymentInformation

=head4
Description:

	 This updates a Pending Merchants Record.  This method only updates the payment information due to the current inplementation of the JavaScript getstates 
	 Library. The library only allows for one dynamic state listing on a page, where we need two on the Pending Merchant Update screens.

=head4
Params:

	 pending_merchant_id	int	The primary key for the record you want to update in the pending_merchants table.

	 params					hashref
								 should be a hashref where the keys
								 are named the same as the fields in
								 the pending_merchants table.

=head4
Return:

	 the number of rows effected.

=cut

sub updatePendingMerchantPaymentInformation
{
	 my $this = shift;
	 my $pending_merchant_id = shift;
	 my $params = shift;


	 my $return = 0;


	 if (! $params)
	 {
		 die 'No parameters were passed.';
	 }

	 if (! $pending_merchant_id)
	 {
		 die 'The pending_merchant_id is a required parameter.';
	 }


	 my %pending_merchant_info = ();

				$pending_merchant_info{'payment_method'} = $params->{'payment_method'} ? $params->{'payment_method'}:undef;
				$pending_merchant_info{'cc_type'} = $params->{'cc_type'} ? $params->{'cc_type'}:undef;
				$pending_merchant_info{'pay_name'} = $params->{'pay_name'} ? $params->{'pay_name'}:undef;
				$pending_merchant_info{'pay_address'} = $params->{'pay_address'} ? $params->{'pay_address'}:undef;
				$pending_merchant_info{'pay_city'} = $params->{'pay_city'} ? $params->{'pay_city'}:undef;
				$pending_merchant_info{'pay_state'} = $params->{'pay_state'} ? $params->{'pay_state'}:undef;
				$pending_merchant_info{'pay_country'} = $params->{'pay_country'} ? $params->{'pay_country'}:undef;
				$pending_merchant_info{'pay_postalcode'} = $params->{'pay_postalcode'} ? $params->{'pay_postalcode'}:undef;
				$pending_merchant_info{'pay_cardnumber'} = $params->{'pay_cardnumber'} ? $params->{'pay_cardnumber'}:undef;
				$pending_merchant_info{'pay_cardcode'} = $params->{'pay_cardcode'} ? $params->{'pay_cardcode'}:undef;
				$pending_merchant_info{'pay_cardexp'} = $params->{'pay_cardexp'} ? $params->{'pay_cardexp'}:undef;
				$pending_merchant_info{'eco_card_number'} = $params->{'eco_card_number'} ? $params->{'eco_card_number'}:undef;
				$pending_merchant_info{'ca_number'} = $params->{'ca_number'} ? $params->{'ca_number'}:undef;
				$pending_merchant_info{'if_number'} = $params->{'if_number'} ? $params->{'if_number'}:undef;

	 my $update_query  = ' UPDATE merchant_pending SET ';
	 my @fields = ();

	 my @values = ();


	 ########################################
	 # Build the fields to insert into the
	 # "members" table, and "original_spid" table.
	 ########################################
	 foreach (keys %pending_merchant_info)
	 {
		 next if ($_ eq 'id');

		 $pending_merchant_info{$_} = undef if (defined $pending_merchant_info{$_} && $pending_merchant_info{$_} =~ /^\s*$/);

		 push @fields, "$_ = ?";
		 push @values, $pending_merchant_info{$_};

	 }

	 push @values, $pending_merchant_id;

	 my $set_fields = join(', ', @fields);

	 $update_query .= " $set_fields WHERE id = ? ";


	 eval{

		 $return = $this->{'db'}->do($update_query, undef, @values);

	 };
	 if($@)
	 {
		 warn 'MerchantAffiliates::updatePendingMerchantPaymentInformation - There was an error updateing the Pending Merchant Record. Error: ' . $@;

		 $return = 0;
	 }

	 return $return;

}

=head3 getMerchantPackageNameById

=head4
Description:

	 This returns the name of the Merchant Package, by the merchant package ID as defined in the "merchant_discounts" table.

=head4
Params:

	 package_id	int	The id of the package name you want returned, as defined in the "merchant_discounts" table.

	 format_in_sentence_case	Bool	Setting this to true will formmat the name in Sentence Case, "Gold" instead of "gold" will be returned.

=head4
Return:

	 the number of rows effected.

=cut

sub getMerchantPackageNameById
{
	 my $this = shift;
	 my $package_id = shift;
	 my $format_in_sentence_case = shift;

	 $package_id = 0 if ! $package_id;

	 die 'The Package ID is in the wrong format, it needs to be an integer' if $package_id !~ /^\d+$/;

	 return $format_in_sentence_case ?  Lingua::EN::NameCase::NameCase($this->{__merchant_package_names_by_id}->[$package_id]) : $this->{__merchant_package_names_by_id}->[$package_id];

}

=head3 getMerchantTypeNameById

=head4
Description:

	 This returns the name of the Merchant Type of the Merchant (offline, offline_rewards), by the merchant package ID as defined in the merchant config.

=head4
Params:

	 package_id	int	The id of the package name you want returned, as defined in the merchant config.

=head4
Return:

	 string (offline, or offline_rewards)

=cut

sub getMerchantTypeNameById
{
	 my $this = shift;
	 my $package_id = shift;
	 my $format_in_sentence_case = shift;

	 $package_id = 0 if ! $package_id;

	 die 'The Package ID is in the wrong format, it needs to be an integer' if $package_id !~ /^\d+$/;

	 return $this->{__merchant_type_by_package}->{$package_id};

}

=head3 getPackageUpgradeValues

=head4
Description:

	 This returns a number used to determin if the merchant is upgrading.
	 #TODO: Add a description that is useful to other people.

=head4
Params:

	 package_id	int	The id of the package upgrade ID you want returned.

=head4
Return:

	 integer

=head4
TODO:

	 Modify this method so if the "__package_upgrade_values" attribute does not exist it is created using the
	 values from the "merchant_discounts" table (type, and package_status fields).

=cut

sub getPackageUpgradeValues
{
	 my $this = shift;
	 my $package_id = shift;

	 $package_id = 0 if ! $package_id;

	 die 'The Package ID is in the wrong format, it needs to be an integer' if $package_id =~ /\D/;

	 return $this->{'__package_upgrade_values'}->{$package_id};

}

=head3 getMerchantPackageIdByName

=head4
Description:

	 This returns the ID of the Merchant Package, by the merchant package name as defined in the "merchant_discounts" table.

=head4
Params:

	 package_name	string	The name of the package ID you want returned, as defined in the "merchant_discounts" table.

	 format_in_sentence_case	Bool	Setting this to true will formmat the name in Sentence Case, "Gold" instead of "gold" will be returned.

=head4
Return:

	 the number of rows effected.

=cut

sub getMerchantPackageIdByName
{

	 my $this = shift;
	 my $package_name = shift;

	 my %email = 
	 (
		 from=>'errors@dhs-club.com',
		 to=>'errors@dhs-club.com',
		 subject=>'OBSOLETE: ' . DHSGlobals::CLUBSHOP_DOT_COM . ' MerchantAffiliates::getMerchantPackageIdByName',
		 text=>'MerchantAffiliates::getMerchantPackageIdByName  : This method is obsolete, and should not be in used.',
	 );

	 my $MailTools = MailTools->new();
	 $MailTools->sendTextEmail(\%email);

	 $package_name = lc($package_name);

	 die 'The Package Name is in the wrong format, it must only contain characters' if $package_name !~ /^\w+$/;

	 return $this->{__merchant_package_id_by_name}->{$package_name};

}

=head3 retrieveMerchantMasterVendorInformationByMerchantID

=head4
Description:

	 Retrieve the Merchant Vendor record for their
	 master location.

=head4
Params:

	 id	INT	The Merchants Master ID

=head4
Return:

	 hashref	The Merchant Vendor information 

=cut

sub retrieveMerchantMasterVendorInformationByMerchantID
{
	 my $this = shift;
	 my $id = shift;

	 my $vendor_info = {};

	 my $merchant_location_info = $this->retrieveMerchantMasterLocationByMerchantID($id);

	 eval{

		 $this->setVendors();

		 $vendor_info = $this->{'Vendors'}->getVendorInformation($merchant_location_info->{'vendor_id'});

	 };
	 if($@)
	 {
		 die 'MerchantAffiliates::retrieveMerchantMasterVendorInformationByMerchantID() - There was an error instanciating the Vendors class as a sub class, or retrieving the Merchants Master Vendor ID.  ' . $@;
	 }

	 return $vendor_info;

}


=head3 createMerchantSubscription

=head4
Description:

	 This create the Merchants Subscription (Annual Billing) entry.

=head4
Params:

	 params
	 HASHREF
	 {
		 member_id
		 INT
		 A Member ID

		 discount_type
		 INT
		 A merchants discount type
	 }

=head4
Return:

	 returns
	 INT
	 1 on sucess, 0 on failure as returned from dbi::do.  

Exceptions:

	 An exception is thrown if there is an issue with inserting the data.

=cut

sub createMerchantSubscription
{

	 my $this = shift;
	 my $params = shift;

	 die 'A valid Member ID is required to create a subscription for a merchant' if (! defined $params->{'member_id'} || ! $params->{'member_id'} || $params->{'member_id'} !~ /\d/);
	 die 'A valid Discount type is required to create a subscription for a merchant' . $params->{'discount_type'} if (! defined $params->{'discount_type'} || ! defined $this->{'__discount_type__subscription_type__mapping'}->{$params->{'discount_type'}} ||  $params->{'discount_type'} !~ /\d/);


	 if(uc($params->{'pay_payment_method'}) eq 'IF')
	 {

		 my @values = ($params->{'member_id'},6);
		 my $number_of_rows_created = $this->{'db'}->do('SELECT create_merchant_subscription(?, ?, NOW())', undef, @values);

		 die "MechantAffiliates::createMerchantSubscription The merchant subscription could not be created: SELECT create_merchant_subscription(?, ?, NOW()), undef, ($params->{'member_id'}, 6)" if (! $number_of_rows_created || $number_of_rows_created eq '0E0');

		 @values = ($params->{'member_id'},$this->{'__discount_type__subscription_type__mapping'}->{$params->{'discount_type'}}, "Subscription for Member $params->{'member_id'}");
		 $number_of_rows_created = $this->{'db'}->do('SELECT downgrade_merchant_subscription(?, ?, ?)', undef, @values);

		 die "MechantAffiliates::createMerchantSubscription The merchant subscription could not be created: SELECT downgrade_merchant_subscription(?, ?, ?), undef, ($params->{'member_id'},$this->{'__discount_type__subscription_type__mapping'}->{$params->{'discount_type'}}, 'Subscription for Member $params->{'member_id'}')" if (! $number_of_rows_created || $number_of_rows_created eq '0E0');

		 return $number_of_rows_created;
	 }
	 else
	 {

		 my @values = ($params->{'member_id'},$this->{'__discount_type__subscription_type__mapping'}->{$params->{'discount_type'}});

		 my $number_of_rows_created = $this->{'db'}->do('SELECT create_merchant_subscription(?, ?, NOW())', undef, @values);

		 die "MechantAffiliates::createMerchantSubscription The merchant subscription could not be created: SELECT downgrade_merchant_subscription(?, ?, ?), undef, ($params->{'member_id'},$this->{'__discount_type__subscription_type__mapping'}->{$params->{'discount_type'}}, 'Subscription for Member $params->{'member_id'}')" if (! $number_of_rows_created || $number_of_rows_created eq '0E0');

		 return $number_of_rows_created;

	 }
}


=head3 retrieveMerchantKeywordsByID

=over

=item I<Description:>

	 Retrieve the Merchants Keywords by their Merchant ID.


=item I<Params:>

	 merchant_id
	 INT
	 The Merchants Master ID

=item I<Return:>

	 ARRAYREF
	 The Merchants Keywords

=back

=cut

sub retrieveMerchantKeywordsByID
{
	 my $this = shift;
	 my $merchant_id = shift;

	 $merchant_id =~ s/\s+//g if $merchant_id;

	 die 'The Merchant ID is a required parameter.' if ! $merchant_id;

	 my $query = <<EOT;

			 SELECT  
				 mk.keyword
			 FROM
				 merchant_keywords__merchant_master__links mkmml
			 JOIN
				 merchant_keywords mk
				 ON
				 mk.merchant_keywords_id = mkmml.merchant_keywords_id
			 WHERE
				 mkmml.merchant_id = ?

EOT

	 my $return_arrayref = [];

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute($merchant_id);

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @$return_arrayref, $rows->{keyword};
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantKeywordsByID - An error occured retrieving the merchants ($merchant_id) keywords record.  Error: " .  $@;
		 return;
	 }

	 return $return_arrayref;


}


=head3 retrieveMerchantKeywordIDsByKeywords

=over

=item I<Description:>

	 Retrieve the Keyword IDs, and the Keywords by keywords.


=item I<Params:>

	 keywords
	 ARRAYREF
	 An Array Ref of the keywords.

	 index_by_keyword
	 BOOl
	 Set to true if you want the returned HASHREF indexed by the Keyword, instead of the Keyword ID

=item I<Return:>

	 HASHREF
	 The Merchants Keyword IDs, and the Keywords

=back

=cut

sub retrieveMerchantKeywordIDsByKeywords
{
	 my $this = shift;
	 my $keywords = shift;
	 my $index_by_keyword = shift;

	 die "'keywords' needs to be defined as an Array Ref, and is a required parameter." if (ref($keywords) ne 'ARRAY' || ! scalar(@$keywords));

	 my @placeholders = ();

	 foreach (@$keywords)
	 {
		 push @placeholders, '?';
	 }

	 my $in_placeholders = join(',', @placeholders);


	 my $query = <<EOT;

			 SELECT
				 mk.merchant_keywords_id,
				 mk.keyword
			 FROM
				 merchant_keywords mk
			 WHERE
				 mk.keyword IN ($in_placeholders)

EOT

	 my $return_hashref = {};

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute(@$keywords);


		 if ($index_by_keyword)
		 {
			 while (my $rows = $sth->fetchrow_hashref())
			 {
				 $return_hashref->{$rows->{keyword}} = $rows->{merchant_keywords_id};
			 }
		 }
		 else
		 {
			 while (my $rows = $sth->fetchrow_hashref())
			 {
				 $return_hashref->{$rows->{merchant_keywords_id}} = $rows->{keyword};
			 }
		 }


	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantKeywordsByKeywords - An error occured retrieving the keywords.  Error: " .  $@;
		 return;
	 }

	 return $return_hashref;


}


=head3 retrievePendingMerchantCountries

=over

=item I<Description:>

	 Retrieve a country list of countries in the "pending_merchants" table.

=item I<Params:>

	 status
	 SCALAR
	 pending, active (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Pending Merchants countries.
	 (
		 0=>
		 {
			 country_label => 'Australia',
			 country => 'AU'
		 }
		 1=>
		 {
			 country_label => 'United Kingdom',
			 country => 'UK'
		 },
		 2=>
		 {
			 country_label => 'United States of America',
			 country => 'US'
		 },
	 )

=back

=cut

sub retrievePendingMerchantCountries
{
	 my $this = shift;
	 my $status = shift;

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label ';

	 my @return_array_hashref = ();

	 if($status eq 'pending')
	 {
		 $where = ' WHERE mp.status = 0 OR mp.status IS NULL ';
	 }
	 elsif($status eq 'active')
	 {
		 $where = ' WHERE mp.status = 1 ';		
	 }

	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mp.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mp.country
			 ) AS country_label
	 FROM
		 merchant_pending mp
		 LEFT JOIN
		 tbl_countrycodes tcc
		 ON
		 tcc.country_code = mp.country

	 $where

	 $order_by

EOT


	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrievePendingMerchantCountries - An error occured retrieving the pending merchant countries.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 retrieveMerchantCountries

=over

=item I<Description:>

	 Retrieve a country list from the "merchant_affiliates_master" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both (approved and active) (leave this parameter null for all)

=item I<Return:>

	 Array of Hashrefs
	 The Merchants countries.
	 (
		 0=>
		 {
			 country_label => 'Australia',
			 country => 'AU'
		 }
		 1=>
		 {
			 country_label => 'United Kingdom',
			 country => 'UK'
		 },
		 2=>
		 {
			 country_label => 'United States of America',
			 country => 'US'
		 },
	 )

=back

=cut

sub retrieveMerchantCountries
{
	 my $this = shift;
	 my $status = shift;
	 my $discount_types = shift;

	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }


	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 ';		
		 }
		 elsif($status eq 'both')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';
		 }
	 }


	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? $where . " AND mam.discount_type IN ($discount_types) ": " WHERE  mam.discount_type IN ($discount_types) ";
	 }

	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mam.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mam.country
			 ) AS country_label
	 FROM
		 merchant_affiliates_master mam
		 LEFT JOIN
		 tbl_countrycodes tcc
		 ON
		 tcc.country_code = mam.country

	 $where

	 $order_by

EOT


	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantCountries - An error occured retrieving the merchant countries.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 retrieveMerchantLocationCountries

=over

=item I<Description:>

	 Retrieve a country list from the "merchant_affiliate_locations" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both (approved and active) (leave this parameter null for all)

=item I<Return:>

	 Array of Hashrefs
	 The Merchants countries.
	 (
		 0=>
		 {
			 country_label => 'Australia',
			 country => 'AU'
		 }
		 1=>
		 {
			 country_label => 'United Kingdom',
			 country => 'UK'
		 },
		 2=>
		 {
			 country_label => 'United States of America',
			 country => 'US'
		 },
	 )

=back

=cut

sub retrieveMerchantLocationCountries
{
	 my $this = shift;
	 my $status = shift;
	 my $discount_types = shift;

	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }


	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 AND v.status= 1 ';		
		 }
		 elsif($status eq 'both')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';
		 }
	 }


	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? " $where AND mam.discount_type IN ($discount_types) ": " WHERE  mam.discount_type IN ($discount_types) ";
	 }


	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mal.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mal.country
			 ) AS country_label
	 FROM
		 merchant_affiliates_master mam
		 JOIN
		 merchant_affiliate_locations mal
			 ON
			 mal.merchant_id = mam.id
		 JOIN
		 vendors v
			 ON 
			 v.vendor_id = mal.vendor_id
		 LEFT JOIN
		 tbl_countrycodes tcc
			 ON
			 tcc.country_code = mal.country

	 $where

	 $order_by

EOT

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantLocationCountries - An error occured retrieving the merchant countries.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;
}


=head3 retrievePendingMerchantProvinces

=over

=item I<Description:>

	 Retrieve a province list of provinces in the "pending_merchants" table.

=item I<Params:>

	 status
	 SCALAR
	 pending, active (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Pending Merchants countries.
	 (
		 0=>
		 {
			 province_label => 'California',
			 province => 'CA'
		 }
		 1=>
		 {
			 province_label => 'Montana',
			 province => 'MT'
		 },
		 2=>
		 {
			 province_label => 'North Carolina',
			 province => 'NC'
		 },
	 )

=back

=cut

sub retrievePendingMerchantProvinces
{
	 my $this = shift;
	 my $status = shift;

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY state_label ';

	 my @return_array_hashref = ();

	 if($status eq 'pending')
	 {
		 $where = ' WHERE mp.status = 0 OR mp.status IS NULL ';
	 }
	 elsif($status eq 'active')
	 {
		 $where = ' WHERE mp.status = 1 ';		
	 }

	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mp.state,
			 COALESCE
			 (
				 sc.name, 
				 mp.state
			 ) AS state_label
	 FROM
		 merchant_pending mp
		 LEFT JOIN
		 state_codes sc
		 ON
		 sc.code = mp.state
		 AND
		 sc.country = mp.country
	 $where

	 $order_by

EOT

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrievePendingMerchantProvinces - An error occured retrieving the pending merchant province.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;
}


=head3 retrieveMerchantProvinces

=over

=item I<Description:>

	 Retrieve a province list from the "merchant_affiliates_master" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both(approved, active) (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Pending Merchants countries.
	 (
		 0=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'California',
			 province => 'CA'
		 }
		 1=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'Montana',
			 province => 'MT'
		 },
		 2=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'North Carolina',
			 province => 'NC'
		 },
	 )

=back

=cut

sub retrieveMerchantProvinces
{
	 my $this = shift;
	 my $status = shift;
	 my $country = shift;
	 my $discount_types = shift;


	 #die "MerchantAffiliates::retrieveMerchantCities - The country parameter is a required field, but you did not supply one. Country: $country " if(! defined $country || ! $country);

	 if($country)
	 {
		 $country =~ s/\s*//g;
		 $country = uc($country);
	 }

	 die "MerchantAffiliates::retrieveMerchantCities - The country parameter is a required field, but it did not hold the correct information.  Only ISO country codes are allowed. Country: $country " if($country && $country !~ /^\w{2}$/);




	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label, state_label ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2 ';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 ';		
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';		
		 }
	 }

	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? $where . " AND mam.discount_type IN ($discount_types) ": " WHERE  mam.discount_type IN ($discount_types) ";
	 }



	 if($country)
	 {
		 $where = $where ? $where . " AND mam.country = ": " WHERE mam.country = ";
		 $where .= $this->{'db'}->quote($country) . ' ';
	 }


	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mam.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mam.country
			 ) AS country_label,
			 mam.state,
			 COALESCE
			 (
				 sc.name, 
				 mam.state
			 ) AS state_label
	 FROM
		 merchant_affiliates_master mam
		 LEFT JOIN
		 tbl_countrycodes tcc
			 ON
			 tcc.country_code = mam.country
		 LEFT JOIN
		 state_codes sc
			 ON
			 sc.code = mam.state
		 AND
		 sc.country = mam.country

	 $where

	 $order_by

EOT


	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantProvinces - An error occured retrieving the merchant province.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 retrieveMerchantLocationProvinces

=over

=item I<Description:>

	 Retrieve a province list from the "merchant_affiliates_master" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both(approved, active) (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Pending Merchants countries.
	 (
		 0=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'California',
			 province => 'CA'
		 }
		 1=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'Montana',
			 province => 'MT'
		 },
		 2=>
		 {
			 country_label => 'United States',
			 country => 'US',
			 province_label => 'North Carolina',
			 province => 'NC'
		 },
	 )

=back

=cut

sub retrieveMerchantLocationProvinces
{
	 my $this = shift;
	 my $status = shift;
	 my $country = shift;
	 my $discount_types = shift;


	 #die "MerchantAffiliates::retrieveMerchantLocationProvinces - The country parameter is a required field, but you did not supply one. Country: $country " if(! defined $country || ! $country);

	 if($country)
	 {
		 $country =~ s/\s*//g;
		 $country = uc($country);
	 }

	 die "MerchantAffiliates::retrieveMerchantLocationProvinces - The country parameter is a required field, but it did not hold the correct information.  Only ISO country codes are allowed. Country: $country "
		 if ($country && $country !~ /^\w{2}$/);

	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label, state_label ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2 ';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 AND v.status= 1 ';		
		 }
		 elsif($status eq 'both')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';		
		 }
	 }

	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? " $where AND mam.discount_type IN ($discount_types) ": " WHERE  mam.discount_type IN ($discount_types) ";
	 }


	 if($country)
	 {
		 $where = $where ? " $where AND mal.country = ": " WHERE mal.country = ";
		 $where .= $this->{'db'}->quote($country) . ' ';
	 }


	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mal.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mal.country
			 ) AS country_label,
			 mal.state,
			 COALESCE
			 (
				 sc.name, 
				 mal.state
			 ) AS state_label
	 FROM
		 merchant_affiliates_master mam
		 JOIN
		 merchant_affiliate_locations mal
			 ON
			 mal.merchant_id = mam.id
		 JOIN
		 vendors v
			 ON 
			 v.vendor_id = mal.vendor_id
		 LEFT JOIN
		 tbl_countrycodes tcc
			 ON
			 tcc.country_code = mal.country
		 LEFT JOIN
		 state_codes sc
			 ON
			 sc.code = mal.state
		 AND
		 sc.country = mal.country

	 $where

	 $order_by

EOT


#	eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

#	};
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantLocationProvinces - An error occured retrieving the merchant province.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;
}


=head3 retrieveMerchantCities

=over

=item I<Description:>

	 Retrieve a list of cities from the "merchant_affiliates_master" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both(approved, active) (leave this parameter null for both)

	 country
	 string
	 The ISO Country Code

	 province
	 string
	 The province, either and ISO code, or whatever the Merchant entered

	 discount_types
	 string
	 A CSV of merchant discount_types (the code for the merchant packages).

=item I<Return:>

	 Array of Hashrefs
	 The Merchants Cities.
	 (
		 0=>
		 {
			 country => 'US',
			 state => 'FL',
			 city => 'Venice',
		 }
		 1=>
		 {
			 country => 'US',
			 state => 'FL',
			 city => 'Nokomis',
		 },
		 2=>
		 {
			 country => 'US',
			 country_label => 'United States',
			 state => 'FL',
			 state_label => 'Florida',
			 city => 'Osprey',
		 },
	 )

=back

=cut

sub retrieveMerchantCities
{
	 my $this = shift;
	 my $status = shift;
	 my $country = shift;
	 my $province = shift;
	 my $discount_types = shift;

	 #die "MerchantAffiliates::retrieveMerchantCities - The country parameter is a required field, but you did not supply one. Country: $country " if(! defined $country || ! $country);

	 if($country)
	 {
		 $country =~ s/\s*//g;
		 $country = uc($country);		
	 }

	 die "MerchantAffiliates::retrieveMerchantCities - The country parameter is a required field, but it did not hold the correct information.  Only ISO country codes are allowed. Country: $country " if( $country && $country !~ /^\w{2}$/);

	 if($province)
	 {
		 $province =~ s/^\s*//;
		 $province =~ s/\s*$//;
	 }

	 #die "MerchantAffiliates::retrieveMerchantCities - The province parameter is a required field, but you did not supply one. Province: $province " if(! defined $province || ! $province);

	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label, state_label, mam.city ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2 ';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 ';		
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';		
		 }
	 }

	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? $where . " AND mam.discount_type IN ($discount_types) " : " WHERE  mam.discount_type IN ($discount_types) ";
	 }


	 if($country)
	 {
		 $where = $where ? $where . " AND mam.country = " : " WHERE mam.country = ";
		 $where .= $this->{'db'}->quote($country) . ' ';
	 }

	 if($province)
	 {
		 $where = $where ? $where . " AND mam.state = " : " WHERE mam.state = ";
		 $where .= $this->{'db'}->quote($province) . ' ';
	 }

	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mam.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mam.country
			 ) AS country_label,
			 mam.state,
			 COALESCE
			 (
				 sc.name, 
				 mam.state
			 ) AS state_label,
			 mam.city
	 FROM
		 merchant_affiliates_master mam
		 LEFT JOIN
		 tbl_countrycodes tcc
			 ON
			 tcc.country_code = mam.country
		 LEFT JOIN
		 state_codes sc
			 ON
			 sc.code = mam.state
			 AND
			 sc.country = mam.country
	 $where

	 $order_by

EOT

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantCities - An error occured retrieving the merchant city.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 retrieveMerchantLocationCities

=over

=item I<Description:>

	 Retrieve a list of cities from the "merchant_affiliates_location" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both(approved, active) (leave this parameter null for both)

	 country
	 string
	 The ISO Country Code

	 province
	 string
	 The province, either and ISO code, or whatever the Merchant entered

	 discount_types
	 string
	 A CSV of merchant discount_types (the code for the merchant packages).

=item I<Return:>

	 Array of Hashrefs
	 The Merchants Cities.
	 (
		 0=>
		 {
			 country => 'US',
			 state => 'FL',
			 city => 'Venice',
		 }
		 1=>
		 {
			 country => 'US',
			 state => 'FL',
			 city => 'Nokomis',
		 },
		 2=>
		 {
			 country => 'US',
			 country_label => 'United States',
			 state => 'FL',
			 state_label => 'Florida',
			 city => 'Osprey',
		 },
	 )

=back

=cut

sub retrieveMerchantLocationCities
{
	 my $this = shift;
	 my $status = shift;
	 my $country = shift;
	 my $province = shift;
	 my $discount_types = shift;

	 #die "MerchantAffiliates::retrieveMerchantLocationCities - The country parameter is a required field, but you did not supply one. Country: $country " if(! defined $country || ! $country);

	 if($country)
	 {
		 $country =~ s/\s*//g;
		 $country = uc($country);		
	 }

	 die "MerchantAffiliates::retrieveMerchantCities - The country parameter is a required field, but it did not hold the correct information.  Only ISO country codes are allowed. Country: $country " if( $country && $country !~ /^\w{2}$/);

	 if($province)
	 {
		 $province =~ s/^\s*//;
		 $province =~ s/\s*$//;
	 }

	 #die "MerchantAffiliates::retrieveMerchantLocationCities - The province parameter is a required field, but you did not supply one. Province: $province " if(! defined $province || ! $province);

	 if (defined $discount_types && $discount_types)
	 {
		 $discount_types =~ s/^\s*//;
		 $discount_types =~ s/\s*$//;

		 $discount_types = undef if ! $discount_types;
	 }

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY country_label, state_label, mal.city ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2 ';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 AND v.status = 1 ';		
		 }
		 elsif($status eq 'both')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';		
		 }
	 }

	 if(defined $discount_types && $discount_types)
	 {
		 $where = $where ? " $where AND mam.discount_type IN ($discount_types) ": " WHERE  mam.discount_type IN ($discount_types) ";
	 }


	 if($country)
	 {
		 $where = $where ? " $where AND mal.country = '$country' ": " WHERE mal.country = '$country' ";
	 }

	 if($province)
	 {
		 $where = $where ? " $where AND mal.state = ": " WHERE mal.state = ";
		 $where .= $this->{'db'}->quote($province) . ' ';
	 }


	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 mal.country,
			 COALESCE
			 (
				 tcc.country_name, 
				 mal.country
			 ) AS country_label,
			 mal.state,
			 COALESCE
			 (
				 sc.name, 
				 mal.state
			 ) AS state_label,
			 mal.city
	 FROM
		 merchant_affiliates_master mam
		 JOIN
		 merchant_affiliate_locations mal
			 ON
			 mal.merchant_id = mam.id
		 JOIN
		 vendors v
			 ON 
			 v.vendor_id = mal.vendor_id
		 LEFT JOIN
		 tbl_countrycodes tcc
			 ON
			 tcc.country_code = mal.country
		 LEFT JOIN
		 state_codes sc
			 ON
			 sc.code = mal.state
			 AND
			 sc.country = mal.country
	 $where

	 $order_by

EOT

	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantLocationCities - An error occured retrieving the merchant city.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;
}


=head3 retrievePendingMerchantPackages

=over

=item I<Description:>

	 Retrieve a list of Packages in the "pending_merchants" table.

=item I<Params:>

	 status
	 SCALAR
	 pending, active (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Pending Merchants countries.
	 (
		 0=>
		 {
			 package_label => 'free',
			 package_type => 'offline',
			 package => 0
		 }
		 1=>
		 {
			 package_label => 'free',
			 package_type => 'offline_rewards',
			 package => 15
		 },
	 )

=back

=cut

sub retrievePendingMerchantPackages
{
	 my $this = shift;
	 my $status = shift;

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY md.name ';

	 my @return_array_hashref = ();

	 if($status eq 'pending')
	 {
		 $where = ' WHERE mp.status = 0 OR mp.status IS NULL ';
	 }
	 elsif($status eq 'active')
	 {
		 $where = ' WHERE mp.status = 1 ';		
	 }



	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 md.name,
			 mp.merchant_package
	 FROM
		 merchant_pending mp
		 LEFT JOIN
		 merchant_discounts md
		 ON
		 md.type = mp.merchant_package

	 $where

	 $order_by

EOT


	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {

			 $rows->{'package_type'} = $this->getMerchantTypeNameById($rows->{'merchant_package'});
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrievePendingMerchantPackages - An error occured retrieving the pending merchant packages.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 retrieveMerchantPackages

=over

=item I<Description:>

	 Retrieve a list of Packages in the "merchant_affiliates_master" table.

=item I<Params:>

	 status
	 SCALAR
	 inactive, approved, active, both(approved, active) (leave this parameter null for both)

=item I<Return:>

	 Array of Hashrefs
	 The Merchants countries.
	 (
		 0=>
		 {
			 name => 'free',
			 package_type => 'offline',
			 discount_type => 0
		 }
		 1=>
		 {
			 name => 'free',
			 package_type => 'offline_rewards',
			 discount_type => 15
		 },
	 )

=back

=cut

sub retrieveMerchantPackages
{
	 my $this = shift;
	 my $status = shift;

	 my $query = '';
	 my $where = '';
	 my $order_by = ' ORDER BY md.name ';

	 my @return_array_hashref = ();

	 if(defined $status)
	 {
		 if($status eq 'approved')
		 {
			 $where = ' WHERE mam.status = 2 ';
		 }
		 elsif($status eq 'active')
		 {
			 $where = ' WHERE mam.status = 1 ';		
		 }
		 elsif($status eq 'both')
		 {
			 $where = ' WHERE mam.status IN (1,2) ';		
		 }
	 }


	 $query =<<EOT;

	 SELECT
		 DISTINCT
			 md.name,
			 mam.discount_type
	 FROM
		 merchant_affiliates_master mam
		 LEFT JOIN
		 merchant_discounts md
		 ON
		 md.type = mam.discount_type

	 $where

	 $order_by

EOT


	 eval{

		 my $sth = $this->{'db'}->prepare($query);
		 $sth->execute();

		 while (my $rows = $sth->fetchrow_hashref())
		 {

			 $rows->{'package_type'} = $this->getMerchantTypeNameById($rows->{'discount_type'});
			 push @return_array_hashref, $rows;
		 }

	 };
	 if ($@)
	 {
		 warn "MerchantAffiliates::retrieveMerchantPackages - An error occured retrieving the merchant packages.  Error: " .  $@;
		 return;
	 }

	 return @return_array_hashref;



}


=head3 insertMerchantKeywords

=over4

=item
Description:

	 This first checks for existing keywords, 
	 retrieved their keyword ID, retrieves the 
	 merchants exisiting keyword ID, and if they 
	 arent' used by other merchants deletes the 
	 keywords, then inserts the new keywords, and 
	 inserts the keyword Id for the merchant keyword 
	 links.

=item
Params:

	 merchant_id	INT The Merchants ID
	 keywords	ARRAYREF	The keywords

=item
Return:

	 returns	undef on sucess, and a hashref on partial success or failure,
	 letting you know wich words were not deleted, or inserted.
	 {
		 delete=>[
					 ';(DELETE',
					 'bakery',
					 'cookie'
				 ],
		 insert=>[
					 ';(SELECT',
					 '*',
					 'members('
				 ]

	 }

=item
Exceptions:

	 An exception is thrown if a fatal error is encountered.


=back


=cut

sub insertMerchantKeywords
{

	 my $this = shift;
	 my $merchant_id = shift;
	 my $keywords = shift;

	 die 'MerchantAffiliates::insertMerchantKeywords - No Merchant ID was supplied, it is a required field' if ( ! $merchant_id);	

	 return if ! scalar(@$keywords);

	 my $return_value = undef;

	 eval{

		 my $current_keywords = $this->retrieveMerchantKeywordsByID($merchant_id);

		 #Here we organize which keywords the Merchant already has, and which ones need to be added.
		 my @keywords_to_keep = ();
		 my @new_key_words = ();

		 foreach (@$keywords)
		 {
			 $_ =~ s/\*//g;

			 my $keyword = uc($_);
			 if(scalar(grep(/\b$keyword\b/,@$current_keywords)))
			 {
				 #Check to see if the key word is already used.
				 push @keywords_to_keep, $keyword if(! scalar(grep(/\b$keyword\b/,@keywords_to_keep)));
			 }
			 else
			 {
				 #Not found, and the keyword isn't already in use;
				 push @new_key_words, $keyword if(! scalar(grep(/\b$keyword\b/,@new_key_words)));
			 }
		 }

		 #Check for existing Keywords, extract their Keyword ID, and make ready for insertion into the link table (@new_key_words).
		 my $merchants_existing_keywords_with_ids = scalar(@keywords_to_keep) ? $this->retrieveMerchantKeywordIDsByKeywords(\@keywords_to_keep, 1) : {};

		 #Extract the Keyword IDs for prexisting keywords, and make ready for insertion into the link table (@keywords_to_keep).
		 my $existing_keywords_with_ids = scalar(@new_key_words) ? $this->retrieveMerchantKeywordIDsByKeywords(\@new_key_words, 1) : {};

		 #combine the merchants pre-existing keywords with new keywords that already exist, and extract the keywords that don't yet exist in the system.
		 my $combined_existing_keywords = $merchants_existing_keywords_with_ids;
		 my @keywords_to_create = ();
		 foreach (@new_key_words)
		 {
			 if (exists $existing_keywords_with_ids->{$_})
			 {
				 $combined_existing_keywords->{$_} = $existing_keywords_with_ids->{$_};
			 }
			 else
			 {
				 push @keywords_to_create, $_;
			 }
		 }

		 #Delete the merchants current links, so we can safely delete the keyword 
		 #this merchant is no longer using, we will re-establish the relationship 
		 #to non deleted keywords later.
		 #TODO: This will return the number of rows deleted, decide if we should check this for any reason.
		 $this->deleteMerchantKeywordLinks($merchant_id);


		 #Extract the keywords this merchant is no longer using.
		 my @keywords_to_delete = ();
		 foreach (@$current_keywords)
		 {
			 if (! defined $merchants_existing_keywords_with_ids->{$_})
			 {
				 push @keywords_to_delete, $_;
			 }

		 }

		 #Clean up keywords that are not being used by this merchant any longer.		
		 $this->deleteMerchantKeywords(\@keywords_to_delete) if(scalar(@keywords_to_delete));


		 #TODO: Insert the new keywords, and retrieve their new IDs so we can
		 # create the relation ship in the next codeblock.
		 my $temp = \@keywords_to_create;

		 my $returned_created_keywords = $this->insertKeywords(\@keywords_to_create);

		 if($returned_created_keywords && ref($returned_created_keywords) eq 'HASH')
		 {

				 foreach (keys %{$returned_created_keywords->{'new_words'}})
				 {
					 $combined_existing_keywords->{$_} = $returned_created_keywords->{'new_words'}->{$_};
				 }

			 $return_value->{'insert'} = $returned_created_keywords->{'errors'} if (exists $returned_created_keywords->{'errors'} && scalar(@{$returned_created_keywords->{'errors'}}));
		 }		



		 #TODO: Insert the merchants keyword IDs into the link table
		 my $returned_link_errors = $this->insertKeywordMerchantLink($merchant_id,$combined_existing_keywords);
		 if(exists $return_value->{'insert'})
		 {
			 $return_value->{'insert'} .= $returned_link_errors if (scalar(@$returned_link_errors));
		 }
		 else
		 {
			 $return_value->{'insert'} = $returned_link_errors if (scalar(@$returned_link_errors));
		 }



	 };
	 if($@)
	 {
		 die "MerchantAffiliates::insertMerchantKeywords($merchant_id, [$keywords]) - Error: " . $@;
	 }


	 return $return_value;

}


=head3 insertKeywordMerchantLink

=over4

=item
Description:

	 This will create a link between a merchant, and a keyword.
	 pass in the Mercahant ID, and a hashref keyed by the keyword, 
	 and the value containing the keyword primary key.

=item
Params:

	 merchant_id	
	 INT	
	 The merchant ID

	 keywords
	 HASHREF
	 The keywords where the index is the keyword, and the 
	 value is the keywords primary key.

=item
Return:

	 returns	
	 ARRAYREF
	 If the keyword link is not created, the keyword will be 
	 returned in this arrayref.

=back

=cut

sub insertKeywordMerchantLink
{
	 my $this = shift;
	 my $merchant_id = shift;
	 my $combined_existing_keywords = shift;

	 my $return_errors = [];

	 foreach (keys %$combined_existing_keywords)
	 {
		 eval{
			 die "MerchantAffiliates::insertKeywordMerchantLink($merchant_id, $combined_existing_keywords->{$_})" if !$this->{'db'}->do("INSERT INTO merchant_keywords__merchant_master__links (merchant_id, merchant_keywords_id) VALUES (?,?)", undef, ($merchant_id, $combined_existing_keywords->{$_}));
		 };
		 if($@)
		 {
			 push @$return_errors, $_;
			 warn "MerchantAffiliates::insertKeywordMerchantLink - INSERT INTO merchant_keywords__merchant_master__links (merchant_id, merchant_keywords_id) VALUES ($merchant_id, $combined_existing_keywords->{$_}) " . $@;
		 }
	 }

	 return $return_errors;

}		


=head3 insertKeywords

=over4

=item
Description:

	 This will insert the keyword in the merchants_keywords table,
	 returning a hashref keyed with 'new_words', and 'errors'.
	 The 'new_words' keys a hashref that is keyed by the keyword,
	 and the value is the keywords primary key.  The 'errors' key
	 is an arrayref of the keywords that could not be created.

=item
Params:

	 $keywords_to_create	
	 ARRAYREF
	 The keywords where the index is the keyword, and the 
	 value is the keywords primary key.

=item
Return:

	 returns	
	 HASHREF
	 The 'new_words' index contains a HASHREF where the keyword is 
	 the key, and the value is the keywords primary key,
	 the 'errors' index contains an ARRAYREF of the keywords that 
	 could not be inserted into the merchant_keywords table.

=back

=cut

sub insertKeywords
{
	 my $this = shift;
	 my $keywords_to_create = shift;

	 my @error_keywords = ();
	 my $keywords = {};

		 foreach (@$keywords_to_create)
		 {
			 eval{

				 die "MerchantAffiliates::insertKeywords($_)" if(! $this->{'db'}->do("INSERT INTO merchant_keywords (keyword) VALUES (?)", undef, ($_)));
				 $keywords->{'new_words'}->{$_} = $this->{'db'}->last_insert_id(undef,'public', 'merchant_keywords','merchant_keywords_id');
			 };
			 if($@)
			 {
				 push @{$keywords->{'errors'}}, $_;
				 warn "MerchantAffiliates::insertKeywords - INSERT INTO merchant_keywords (keyword) VALUES ($_) - " . $@ . "\n";
			 }
		 }

	 return $keywords;

}


=head3 deleteMerchantKeywords

=over4

=item
Description:

	 This will delete keyword that are not associated with other merchants.
	 When you call this method you need to delete the assocation to the 
	 keyword in the "merchant_keywords__merchant_master__links" table with
	 the merchant you are removing the key word(s) from, 
	 $this->deleteMerchantKeywordLinks(MERCHANT_ID) will do the trick.

=item
Params:

	 keywords	ARRAYREF	The keywords

=item
Return:

	 returns	arrayref If there are DB errors the array will hold the keywords 
						 that could not be deleted due to fatal db errors.
						 use if(scalar(@$returned_arrayref)) to determin if it
						 was completely sucessfull or not.

=back

=cut

sub deleteMerchantKeywords
{
	 my $this = shift;
	 my $keywords = shift;

	 my $return_error_keywords = [];

	 die 'The keyword param should be passed in as an ARRAY Ref.' if ref($keywords) ne 'ARRAY';

	 my $delete_query =<<EOT;

					 DELETE
					 FROM
						 merchant_keywords
					 WHERE
					 keyword IN (
							 SELECT
								 mk.keyword
							 FROM
								 merchant_keywords mk
								 LEFT JOIN
								 merchant_keywords__merchant_master__links mkmml
								 ON
								 mk.merchant_keywords_id = mkmml.merchant_keywords_id
							 WHERE
								 mk.keyword IN (?)
								 AND
								 mkmml.merchant_keywords_id IS NULL
							 )

EOT


	 foreach (@$keywords)
	 {
		 eval{
			 $this->{'db'}->do($delete_query, undef, ($_));
		 };
		 if($@)
		 {
			 warn "MerchantAffiliates::deleteMerchantKeywords - The keyword '$_' was not deleted from the merchant_keywords table. " . $@;
			 push @$return_error_keywords, $_;
		 }
	 }	

	 return $return_error_keywords;
}


=head3 deleteMerchantKeywordLinks

=head4
Description:

	 This will delete the keyword links for a merchant.  This should be 
	 called before $this->deleteMerchantKeywords(['keyword','keyword']);

=head4
Params:

	 merchant_id	INT	The merchants ID

=head4
Return:

	 returns	int The number of rows deleted.

=cut

sub deleteMerchantKeywordLinks
{
	 my $this = shift;
	 my $merchant_id = shift;

	 die "The merchant id is a required field. " if $merchant_id !~ /^\d+$/;

	 return $this->{'db'}->do('DELETE FROM merchant_keywords__merchant_master__links WHERE merchant_id = ?', undef, ($merchant_id));	


}


=head3 adjustMerchantBlurbForCurrentPackageBenefits

=head4
Description:

	 This will modify the Merchants Blurb length for their Current Package

=head4
Params:

	 merchant_id	INT	The merchants ID

=head4
Return:

	 int	The number of rows affected as returned from this->updateMerchantMasterInformation, or undef if there is nothing to update.

=cut

sub adjustMerchantBlurbForCurrentPackageBenefits
{

	 my $this = shift;
	 my $merchant_id = shift;

	 die "The merchant id is a required field. " if $merchant_id !~ /^\d+$/;

	 my $config_hashref = $this->retrieveMerchantConfigHashRef();
	 my $merchant_master_record_hashref = $this->getMerchantMaster({'id'=>$merchant_id});
	 my %merchant_master_info = ();


	 foreach (keys %{$merchant_master_record_hashref->[0]})
	 {
		 $merchant_master_info{$_} = $merchant_master_record_hashref->[0]->{$_};
	 }

	 #get the maximum number of keyword for their package.
	 $merchant_master_info{'merchant_type'} = $this->getMerchantTypeNameById($merchant_master_info{'discount_type'});				
	 my $merchants_package_name = $this->getMerchantPackageNameById($merchant_master_info{'discount_type'});

	 return if (! exists $merchant_master_info{'blurb'} || ! $merchant_master_info{'blurb'});

	 my $length = $config_hashref->{'blurb'}->{'max_characters_per_package'}->{$merchant_master_info{'merchant_type'}}->{$merchants_package_name} if (exists $config_hashref->{'blurb'}->{'max_characters_per_package'}->{$merchant_master_info{'merchant_type'}} && exists $config_hashref->{'blurb'}->{'max_characters_per_package'}->{$merchant_master_info{'merchant_type'}}->{$merchants_package_name} &&  $config_hashref->{'blurb'}->{'max_characters_per_package'}->{$merchant_master_info{'merchant_type'}}->{$merchants_package_name});

	 $length++;

	 my $original_value = $merchant_master_info{'blurb'};

	 $original_value =~ s/\s+$//;
	 $original_value =~ s/;|\(|\)//g;

	 $merchant_master_info{'blurb'} =~ s/\s+$//;
	 $merchant_master_info{'blurb'} =~ s/;|\(|\)//g;


	 my $string_length = length($merchant_master_info{'blurb'});

	 if ($string_length > $length)
	 {
		 my $characters_to_remove = $string_length - $length;
		 $merchant_master_info{'blurb'} = substr($merchant_master_info{'blurb'}, 0, - $characters_to_remove);
	 }

	 if (length($original_value) != length($merchant_master_info{'blurb'}))
	 {
		 return $this->updateMerchantMasterInformation($merchant_id, \%merchant_master_info);
	 }

	 return undef;

}


=head3 adjustMerchantKewordsForCurrentPackageBenefits

=head4
Description:

	 This will modify the Merchants Keywords for the Current Package

=head4
Params:

	 merchant_id	INT	The merchants ID

=head4
Return:

	 returns	HASH 
					 {
						 notices	HASHREF If the maximum number of keywords were exceded for the merchant package this will be set.
						 {
							 max_number_of_keywords_exceeded	STRING
						 },
						 warnings	HASHREF	If any of the keywords were modified, the will be listed with as "orignal value - the modified keyword \n" in modified_keywords
						 {
							 keywords	string	If this is set it should only hold the value modified
							 modified_keywords	string "original keyword - modified keyword \n" . "original keyword - modified keyword \n"
						 },
						 errors	HASHREF	Errors from the insertMerchantKeywords method
						 {
							 delete=>[
										 ';(DELETE',
										 'bakery',
										 'cookie'
									 ],
							 insert=>[
										 ';(SELECT',
										 '*',
										 'members('
									 ]
						 }
					 }

=cut

sub adjustMerchantKewordsForCurrentPackageBenefits
{

	 my $this = shift;
	 my $merchant_id = shift;

	 die "The merchant id is a required field. " if $merchant_id !~ /^\d+$/;

	 my $config_hashref = $this->retrieveMerchantConfigHashRef();
	 my $merchant_master_record_hashref = $this->getMerchantMaster({id=>$merchant_id});
	 my %merchant_master_info = ();

	 foreach (keys %{$merchant_master_record_hashref->[0]})
	 {
		 $merchant_master_info{$_} = $merchant_master_record_hashref->[0]->{$_};
	 }

	 #get the maximum number of keyword for their package.
	 $merchant_master_info{'merchant_type'} = $this->getMerchantTypeNameById($merchant_master_info{'discount_type'});				
	 my $merchants_package_name = $this->getMerchantPackageNameById($merchant_master_info{'discount_type'});
	 my $max_number_of_keywords = $config_hashref->{'number_of_keywords'}->{$merchant_master_info{'merchant_type'}}->{$merchants_package_name};
	 my $keywords = $this->retrieveMerchantKeywordsByID($merchant_id);
	 my @merchant_keywords = ();
	 my %return = ();

	 foreach (@$keywords)
	 {
		 chomp($_);

		 next if ! $_;


		 #Check here for the maximum number of keywords, if they have reached their maximum call "last".
		 if (scalar(@merchant_keywords) >= $max_number_of_keywords)
		 {
			 $return{notices}->{max_number_of_keywords_exceeded} = 'The maximum number of keywords for your package has been exceeded.';
			 last;
		 }

		 my $original_value = $_;

		 $original_value =~ s/\s+$//;
		 $_ =~ s/\s+$//;
		 $_ =~ s/;|\(|\)//g;

		 $original_value =~ s/^\s+//;

		 $_ =~ s/\s//g;
		 $_ = uc($_);
		 my $string_length = length($_);

		 if ($string_length > $this->{'_keyword_length'})
		 {
			 my $characters_to_remove = $string_length - $this->{'_keyword_length'};
			 $_ = substr($_, 0, - $characters_to_remove);
		 }

		 if (length($original_value) != length($_))
		 {
			 if(exists $return{'warnings'}->{'modified_keywords'})
			 {
				 $return{'warnings'}->{'modified_keywords'} .= "$original_value - $_ \n";
			 }
			 else
			 {
				 $return{'warnings'}->{'keywords'} = "Modified";
				 $return{'warnings'}->{'modified_keywords'} = "$original_value - $_ \n";
			 }
		 }

		 push @merchant_keywords, $_; 

	 }

	 $return{'errors'} = $this->insertMerchantKeywords($merchant_id, \@merchant_keywords);

	 return %return;
}

#
# Take address entered and do a geocode to get the lat/lng of the address
#
sub getMerchantLocationCoordinates
{
    use LWP::UserAgent;
    use URI::Escape;
    use XML::Simple;

    my $this = shift;
    my $params = shift;
    my $agent = LWP::UserAgent->new;
    my $xml = new XML::Simple;
    my $merchant_location_info = {};
    my $chng = 0;
    my $returnhash = {};
#    warn "getMerchantLocationCoordicnates" . Dumper($params);
    if (exists $params->{'id'})
    {
         $merchant_location_info = $this->retrieveMerchantLocationByLocationID($params->{'id'});
         if ($params->{'address1'} ne $merchant_location_info->{'address1'} ||
             $params->{'city'} ne $merchant_location_info->{'city'} ||
             $params->{'state'} ne $merchant_location_info->{'state'} ||
             $params->{'country'} ne $merchant_location_info->{'country'})
		{
             $chng = 1;
		}
		if ($chng == 0 && ($params->{'longitude'} != $merchant_location_info->{'longitude'} ||
             $params->{'latitude'} != $merchant_location_info->{'latitude'})) {
             $chng = 2;
		}
   }
    else
    {
        $chng = 3;
    }
#
# IF $chng is 2 the lat/lng is different than in the database
#
   if ($chng == 2)
   {
       $returnhash->{'latitude'} = $params->{'latitude'};
       $returnhash->{'longitude'} = $params->{'longitude'};
       return $returnhash;
   } 

   my $googaddr = "$params->{'address1'}, $params->{'city'}, $params->{'state'}, $params->{'country'}";
   my $result = {};

   $returnhash->{'error'} = " ";

   if (defined $googaddr)
   {
		my $count = $googaddr =~ tr/,//;
		if ($count <= 1)
		{
			$returnhash->{'error'} = "Address1, City, State/Provence, Country does not return valid coordinates";
			return $returnhash;
		}
		my $addrs = uri_escape_utf8($googaddr);
		my $url = 'http://maps.googleapis.com/maps/api/geocode/xml?address=' . $addrs . '&sensor=false';
		my $req = HTTP::Request->new('GET'=>$url);
		my $res = $agent->request($req);
		if (! $res->is_success)
		{
			$returnhash->{'error'} = $res->status_line;
			return $returnhash;
		}
		else
		{
			$result = $xml->XMLin($res->content);
		}

		if (ref($result->{'result'}) eq "HASH")
		{
			$returnhash->{'latitude'} = $result->{'result'}->{'geometry'}->{'location'}->{'lat'};
			$returnhash->{'longitude'} = $result->{'result'}->{'geometry'}->{'location'}->{'lng'};
		}
		elsif (ref($result->{'result'}) eq "ARRAY")
		{
			$returnhash->{'latitude'} = $result->{'result'}[0]->{'geometry'}->{'location'}->{'lat'};
			$returnhash->{'longitude'} = $result->{'result'}[0]->{'geometry'}->{'location'}->{'lng'};
		}

		return $returnhash;
   }

   $returnhash->{'error'} = "address1, city, state/region, country does not compute";
   return $returnhash;
}

1;

__END__

=head1 SEE ALSO: 

L<Members>, L<MailTools>, L<G_S>, L<Vendors>, XML::Simple, L<DHSGlobals>, Lingua::EN::NameCase


=head1 CHANGE LOG:

	2009-07-16
	Keith
	I removed the creation of a Shopper Member when a Merchant is created.

	2009-08-05
	Keith
	I updated the "updateMerchantMasterInformation" method to allow
						removal of unused information.
	2009-08-24
	Keith
	In "retrieveMerchantPackageCost" I am now forcing the "merchant_type" 
	parameter to lowercase.

	2009-09-09
	Keith
	Added a "notes" parameter to the "updateMerchantLocationInformation" 
	method, and the "createMerchantLocationInformation" method.

	2009-09-11
	Keith
	Inclued the "Lingua::EN::NameCase" library, and implemented 
	"Lingua::EN::NameCase::NameCase()" sub where city names are 
	inserted, or updated.
						
	2009-09-16
	Keith
	In the createMerchant method I added the "debit_club_account" 
	parameter so if a merchants MA memberships club account should 
	be credited it will be.
						
	2009-09-18
	Keith
	Added the getMerchantPackageIdByName method, and the 
	getMerchantPackageNameById methods.
	
	2009-10-22
	Keith
	Add the updateMerchantDiscountInformation, it is being 
	used in the updateMerchantLocationInformation method.
	
	2009-11-04
	Keith
	Added the "retrieveAllMerchantLocationsByMerchantID" method.
	
	2009-12-12
	Keith
	Added the "updateVendorBannerByMerchantID" method, and modified 
	the "updateMerchantMasterInformation" method so merchants can 
	add, and delete their banner to the merchant listing.
	
	2010-01-07
	Keith
	Added the "createMerchantSubscription" method, modified 
	the "createMerchant" method, and modified the 
	"createMerchantMasterInformation" so they utilize 
	the "createMerchantSubscription".

	2010-01-07
	Keith
	Modified the "createMerchant" method so the club account
	transaction entries include the Merchant ID.

	2010-01-13
	Keith
	Modified the following methods to truncate the Business Name field,
	if it is more than 30 characters long.
	
	createMerchant
	createMerchantLocationInformation
	createMerchantMasterInformation
	insertPendingMerchantInformation
	updateMerchantLocationInformation
	updateMerchantMasterInformation
	updatePendingMerchantInformation
	
	2010-02-22
	Keith
	I added the createMerchantDiscount Method,
	and modified the updateMerchantDiscount Method
	so when the Discount Information is updated a new
	discount record is created, and the previous discount
	records end_date field is set.
	
	2010-03-02
	Keith
	I added some error trapping to the createMerchantDiscount Method,
	for the discount parameter.

	2010 Mar 10	
	Keith
	Modified the createMerchant method, so if a merchant has applied
	and they supply a member ID that is an Associate Member, we use the
	associates SPID for the Merchant Affiliate memberships SPID

	2010 Jun 29
	Keith
	I added the "getPackageUpgradeValues" method to help determin if a 
	merchant is upgrading, downgrading, or staying at the same level.
	
	2011 Feb 9
	Bill
	revised various SQL modifiers to employ db->quote instead of directly inserting the string into the SQL

	2011 Feb 25
	Bill
	added 'amp' to the list of valid merchant sponsors
	
    2011 Sept 22
    George
    getMerchantLocationCoordinates  added to get lat/long coordinates from google geocoder

	2012-05-03
	Bill
	Did some mass substitutions of hash keys like this {key} to {'key'}
	Also revised the way the phone of the member record being created was determined to use home_phone if available.
	If not fall back to the business phone.
=cut
