package EcoCard;
use strict;
use CGI;

my %EcoPay = (
        Password => 'wd3mHRtk7CF1',
        Merchantaccountnumber => '106884',
        PaymentpageID => '1059'
);

=head2 package EcoCard

=head4 
	For starters, we want a standard outbound link for members to fund their Club Account
	
=head3 Usage:

=head4
	my $ec = new EcoCard($dbh);

	FundingRequest will return a well formed URL suitable for a redirect or as a clickable link given the appropriate argument

	my $outbound_link = $ec->FundingRequest({
		member_id	=> xxx,
		amount		=> xxx,
		description	=> 'some description'
		,currency	=> 'XXX'	# this argument is optional
		,html_safe	=> 1		# optional
	});
	
	An active database handle is required for the FundingRequest as an ecopay_temp record is created.
	member_id is an integer member record ID value
	amount is the transaction amount, which should be in USD as of May '09
	description is a textual description that will get recorded with the transaction in the members EcoCard account.
		The description will be URL escaped before being inserted into the final URL
	currency is the 3 character ISO currency code and currently will be overridden as we are only doing USD as of May '09
	html_safe will apply CGI::htmlEscape to the returned URL to make it XHTML safe
=cut

sub dbh { return $_[0]->{dbh}; }

sub FundingRequest
{
	my ($self, $args) = @_;
	# first we will record the outbound request
	# then we can build a redirection URL
	my ($ecotemp) = $self->dbh->selectrow_array("SELECT nextval('ecopay_temp_pk_seq')") ||
		die "Failed to obtain an ecotemp transaction ID";
	$self->dbh->do("INSERT INTO ecopay_temp (pk, id, amount) VALUES ($ecotemp, ?, ?)", undef,
		$args->{member_id}, $args->{amount});

	$args->{currency} = 'USD';
	die "member_id argument must present and be an integer value\n" unless $args->{member_id} && $args->{member_id} !~ m/\D/;
	die "amount argument must present and be a numeric value\n" unless $args->{amount} && $args->{amount} =~ m/^\d+\.?\d{0,2}$/;
	my $url =
		'https://secure.ecopayz.com/PrivateArea/WithdrawOnlineTransfer.aspx?' .
		"Password=$EcoPay{Password}&" .
		"Merchantaccountnumber=$EcoPay{Merchantaccountnumber}&" .
		"PaymentpageID=$EcoPay{PaymentpageID}&" .
		"Currency=$args->{currency}&" .
		"Amount=$args->{amount}&" .
		"ClientAccountNumberAtMerchant=$args->{member_id}&" .
		"TxID=$ecotemp&" .
		"Merchantfreetext=" . CGI->escape($args->{description} || '');
	return $args->{html_safe} ? CGI->escapeHTML($url) : $url;
}

sub new
{
	my ($package, $dbh) = @_;
	die "An active database handle is required\n" unless $dbh;
	return bless {dbh=>$dbh};
}

1;

=head3 Test Use:

=head4
	We have test accounts with EcoCard. This module can be copied and the EcoCard test credentials enabled.
	Then the copied module can be used. We have a test user with EcoCard which can send our test account virtual funds.
	
For testing please use the following details:

Casino login: Clubshop_test/clubshop_test

USD account: 106889

EUR account: 106887

GBP account: 106888

 

Payment page ID: 1061

Payment page pwd: rTel3wQWvYDa

 Merchant password (for automated payouts): cZYf6Z0zZZNS

 

Test user account: dhs_test/asdasd
=cut