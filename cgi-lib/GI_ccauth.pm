package CCAuth;
###### module for performing CC and echeck auths with authorize.net
###### last modified 09/08/15	Bill MacArthur

use strict;
use DHSGlobals;

###### set the following value to bypass actual transactions at authorize.net
our $TEST_MODE = 0;

our %CUSTOM_ERRS = (
	44 =>	'CVV2 (Card Code) rejection',
	45 =>	'CVV2 (Card Code) and AVS rejection',
	65 =>	'CVV2 (Card Code) mismatch');

sub Authorize($;$)
{

    return 1 if (! DHSGlobals::PRODUCTION_SERVER);
    #return '0<b> 2: This transaction has been declined. </b><br />' if (! DHSGlobals::PRODUCTION_SERVER);


###### $data is a hashref containing the necessary data for the transaction
###### 
	my $data = shift;
###### we will recognize 'TEST_MODE', 'auth_type', 'raw', and member args
# the member argument should be a hashref of a member record that can be used to enforce the
# rule requiring the card holder to be the member unless already preauthorized
	my $arg = shift || {};
	$TEST_MODE = $arg->{'TEST_MODE'} if $arg->{'TEST_MODE'};
	my $Auth_Type = $arg->{'auth_type'} || 'AUTH_CAPTURE';	# use AUTH_ONLY to ping the account - see the API for other values

	use LWP::UserAgent;
	use CGI::Util qw(escape);

	my $URL = 'https://secure2.authorize.net/gateway/transact.dll';

#	my $LOGIN = "7hg3Eca8MC";
#	my $LOGIN_PWD = '7c3gnJ85448u3MAY';
#	my $company_name = escape('ClubShop.com.com');
    my $LOGIN = '2U7R7kg4';
    my $LOGIN_PWD = '27mfG4jpHE7NL87w';
    my $company_name = escape('The DHS Club, Inc.');

	my $ACCOUNTING_CONTACT = 'acf1@clubshop.com';

	my $problem = '';
	my $authorization = '';
	my $add = '';
	my $res = ();
	my @REQUIRED_DATA_VALUES = qw/amount order_description id invoice_number payment_method/;

###### check to make sure we have at least these values passed
	foreach (@REQUIRED_DATA_VALUES){ unless (defined $data->{$_}){ return(0, "Missing $_")}}
	
	# lets branch here to do our member/card holder comparison
	# if we get an empty response continue with authorization, otherwise we'll jump to the end to return our response
	$authorization = NameCK($data, $arg);

	unless ($authorization)
	{
###### let's try escaping our values before creating the string that way we can be sure we're covered beyond spaces
#	foreach (keys %{$data}){ $data->{$_} = uri_escape_utf8($data->{$_}) }
# that ^^^ doesn't work right for data that is already utf-8
	foreach (keys %{$data}){ $data->{$_} = escape($data->{$_}) }

	$add = "x_ADC_Delim_Data=TRUE&x_ADC_URL=FALSE&x_Version=3.1&x_Login=$LOGIN&x_Password=$LOGIN_PWD";
	$add .= "&x_Type=$Auth_Type";
	$add .= "&x_Description=$company_name%20$data->{'order_description'}";
	$add .= "&x_Amount=$data->{'amount'}";
######	$add .= "&x_Amount=1.00";
	$add .= "&x_Invoice_num=$data->{'invoice_number'}";
	$add .= "&x_Cust_ID=$data->{'id'}";
	$add .= "&x_Method=$data->{'payment_method'}";
	$add .= "&x_Email=$data->{'emailaddress'}" if defined $data->{'emailaddress'};
	$add .= "&x_Ship_To_Address=$data->{'ship_address'}" if defined $data->{'ship_address'};
	$add .= "&x_Ship_To_City=$data->{'ship_city'}" if defined $data->{'ship_city'};
	$add .= "&x_Ship_To_State=$data->{'ship_state'}" if defined $data->{'ship_state'};
	$add .= "&x_Ship_To_Zip=$data->{'ship_zip'}" if defined $data->{'ship_zip'};
	$add .= "&x_Ship_To_Country=$data->{'ship_country'}" if defined $data->{'ship_country'};
	$add .= "&x_Ship_To_First_Name=$data->{'ship_firstname'}" if defined $data->{'ship_firstname'};
	$add .= "&x_Ship_To_Last_Name=$data->{'ship_lastname'}" if defined $data->{'ship_lastname'};
	$add .= "&x_Ship_To_Company=$data->{'ship_company_name'}" if defined $data->{'ship_company_name'};
	$add .= "&x_Phone=$data->{'phone'}" if defined $data->{'phone'};
	$add .= "&x_Fax=$data->{'fax'}" if defined $data->{'fax'};
	$add .= "&x_First_Name=$data->{'account_holder_firstname'}" if defined $data->{'account_holder_firstname'};
	$add .= "&x_Last_Name=$data->{'account_holder_lastname'}" if defined $data->{'account_holder_lastname'};
	$add .= "&x_Address=$data->{'account_holder_address'}" if defined $data->{'account_holder_address'};
	$add .= "&x_City=$data->{'account_holder_city'}" if defined $data->{'account_holder_city'};
	$add .= "&x_State=$data->{'account_holder_state'}" if defined $data->{'account_holder_state'};
	$add .= "&x_Zip=$data->{'account_holder_zip'}" if defined $data->{'account_holder_zip'};
	$add .= "&x_Country=$data->{'account_holder_country'}" if defined $data->{'account_holder_country'};
	$add .= "&x_Bank_Acct_Num=$data->{'bank_account_number'}" if defined $data->{'bank_account_number'};
	$add .= "&x_Bank_Name=$data->{'bank_name'}" if defined $data->{'bank_name'};
	$add .= "&x_Bank_ABA_Code=$data->{'aba_code'}" if defined $data->{'aba_code'};
	$add .= "&x_Card_Num=$data->{'cc_number'}" if defined $data->{'cc_number'};
	$add .= "&x_Exp_Date=$data->{'cc_expiration'}" if defined $data->{'cc_expiration'};
	$add .= "&x_customer_ip=$data->{'ipaddr'}" if defined $data->{'ipaddr'};
	$add .= (defined $data->{'card_code'}) ? "&x_Card_Code=$data->{'card_code'}" :"&x_Card_Code=";

###### eCheck specific requirements
	if ($data->{'payment_method'} =~ /echeck/i)
	{
		$add .= '&x_Echeck_Type=WEB&x_Bank_Acct_Type=CHECKING';
		my $name = '';
		$name .= $data->{'account_holder_firstname'} if (defined $data->{'account_holder_firstname'});
	
	###### now add a URI escaped space if we are ADDING a lastname to a firstname
		$name .= '%20' if ($name && defined $data->{'account_holder_lastname'});
		$name .= "$data->{'account_holder_lastname'}" if (defined $data->{'account_holder_lastname'});
		
		$add .= "&x_Bank_Account_Name=$name";
	}

###### escaping the whole thing broke it because the _ & = were escaped as well
###### unless we have problems with other characters, we'll leave it at escaping spaces only
#	$add =~ s/ /%20/g;

	$add .= '&x_Test_Request=TRUE' if $TEST_MODE;
#print "$add\n";
#return 0;

	my $ua = LWP::UserAgent->new;
	my $req = HTTP::Request->new('POST' => $URL);
	$req->header('charset'=>'UTF-8');
	$req->content_type('application/x-www-form-urlencoded');
	$req->content($add);
#warn $req->as_string();
###### start our authorization

	$res = $ua->request($req);
	$authorization = $res->as_string;

###### chop out the headers
	$authorization =~ s/^.*\n\n//s;
	}
	# END OF unless ($authorization)

# uncomment only for testing
#print CGI::header('text/plain');
#print "authorize.net returned:\n". $authorization ."\n";
#open(FL, ">test"); print FL $res->as_string; close FL;
#return;
# end of uncomment only for testing

	my $success = 0;
	
###### there have been times where either the connection died
###### or authorize.net simply did not respond
###### in those cases there are no results in $authorization
	if ($authorization && $authorization =~ /^\-?\d{1},/)
	{
		# callers that request the raw response intend to handle parsing on their own, probably using ccauth_parse
		return $authorization if $arg->{'raw'};

		my @transaction = split /,/, $authorization;

		if ($transaction[0] == 1)
		{
			$success = 1;
		}
		else
		{
		
###### in some cases we want to replace the generic 'transaction declined' with a more concise reason
###### we'll use the reason code to determine this
			if ($transaction[0] =~ m/2|3/)
			{
				foreach (keys %CUSTOM_ERRS)
				{
					$transaction[3] = $CUSTOM_ERRS{$_} if $transaction[2] eq $_;
				}
			}

###### concatenate the reason code onto the reason for future reference
			$transaction[3] = "$transaction[2]: $transaction[3]";

			if ($transaction[0] == 3 || $transaction[0] == 2 ||  $transaction[0] == -1)
			{
				$problem .= "<b> $transaction[3] </b><br />";
			}
			else
			{
				$problem .= "There has been an authorization error.<br />
				Please check your email for a receipt.<br />
				Try again only if you have received no receipt.<br />\n";
			}
		}

###### there were no results or they were not consistent with what we were expecting
	}
	else
	{
		$problem .= "There has been an authorization error.<br />
			Please check your email for a receipt.<br />
			Try again only if you have received no receipt.<br />\n";

		###### we'll send an email to accounting so that they can check on the status of the order
		my $subject = 'Incomplete: ';
		my $msg = "An authorization has timed out and could possibly have been approved.
The transaction details are included below:\n\n";

		$add =~ s/%20/ /g;
		foreach (split /&/, $add){
			if (/x_Description/)
			{
				$subject .= (split /=/)[1];
			}
			s/=/ = /; $msg .= "$_\n";
		}
		$msg .= "\nHere was the complete response from authorize.net:\n\n" . $res->as_string;

		use Mail::Sendmail;
		my %mail = (
				'To'	=> $ACCOUNTING_CONTACT,
				'From'	=> 'system-authorizations@www.clubshop.com',
				'Subject'=> $subject,
				'Message'=> $msg);

		sendmail(%mail);
	}

	if($success != 1) {
###### we don't want to keep track of failed authorizations
		return (0, $problem);
	}
	return 1
}

sub NameCK($$)
{
	my $data = shift;
	my $arg = shift;
	return undef unless $arg->{'member'};
	die "Value of member argument must be a hashref\n" unless ref $arg->{'member'} eq 'HASH';
	
	# Dick has specified that the card holder must match the lastname of the member
	# if we don't have a field named 'lastname' we're up the creek
	die "member hashref argument must contain a 'lastname' field\n" unless exists $arg->{'member'}->{'lastname'};
	
	# if the value of lastname is empty, then there is nothing to check and we don't necessarily want to bomb for that
	return undef unless $arg->{'member'}->{'lastname'};
	
	# if our member lastname matches the account holder's 'first' or 'last' names, we'll call it good
	my $card_holder = lc("$data->{'account_holder_firstname'} $data->{'account_holder_lastname'}");
	my $lname = lc($arg->{'member'}->{'lastname'});
	return undef if $card_holder =~ m/$lname/;
	
	# if we didn't match, then we have to look to see if they have a card preauthorized for use by another party
	# since we will need a DB connection to look it up, we'll look in main and quit if there isn't one
	#return undef unless $main::db;
	# until that part is hammered out, we'll return a decline
	# we will mock an authnet response for the benefit of callers that intend to parse it and provide translated responses
	# if they simply are looking for a textual response, Authorize will parse out the proper components
	return '-1,,-1,The Card Holder must match the lastname of the membership using the card,,,,';
}

1;

###### 06/05/02 added a routine to send a mail if authorization connection dies before we receive an answer
###### 06/06/02 altered the subject in the alert email to reflect the transaction description
###### 06/18/02 changed over to LWP for making our requests
###### 06/20/02 added the login password as will soon be required
###### 08/08/02 changed the authnet password
###### 09/11/02 changed the authnet password again
###### 10/21/02 changed the authnet password again
###### 11/22/02 added the cvv2 handling and changed the version number from 3.0 to 3.1, also added the following
###### x_Type, x_Echeck_Type, x_Bank_Acct_Type, x_Bank_Account_Name
###### 12/11/02 revised some uri escaping, changed some error reporting, added a TEST_MODE flag,
###### implemented 'strict'
###### 12/20/02 changed the response code match to an exact comparison
###### 08/20/03 changed the gateway password
###### 11/11/03 changed the gateway password
###### 05/12/05 changed LOGIN id
###### 12/13/05 added handling for args to allow testing from the caller
###### and also to request a 'complete' response return instead of a doctored one
# 02/28/06 changed the uri_escape call on data to uri_escape_utf8
# 03/01/06 the _utf8 thing doesn't work right with data that is already utf-8
# instead went from URI::Escape to importing escape() from CGI::Utils 
# 10/08/08 added the NameCK routine and the necessary code in Authorize to use it
# 05/29/09 added handling for the auth_type key/value pair in the Authorize routine
# 09/21/10 synchronized live/dev versions as far as functionality goes
# 09/22/10 this file is now named GI_ccauth.pm although we are retaining the package name of ccauth so it can be used interchangably
# we have customized the accounting contact and the company name
###### 07/14/11 adde handling for an ip address to submit to Authnet
# 07/15/14 changed the company name and the accounting contact email address
# 09/08/15 changed the destination URL from secure to secure2
