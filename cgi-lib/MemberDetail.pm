package MemberDetail;
use strict;
use lib ('/home/httpd/cgi-lib');
use ParseMe;
use XML::Simple;
use G_S;

=head1
	MemberDetail

=head4
	Creates a Standard Member Detail HTML table of member details for Partners and other membership types
	
	Call MemberDetail::Generate with these arguments:
		database handle
		the numeric ID of the member
=cut

our %TMPL = ('xml'=>10664, 'tbl'=>10663);

sub Generate($$;$)
{
	my $db = shift || die '$db is a required argument';
	my $id = shift;
	my $gs = shift || G_S->new({'db'=>$db});
	
	my %conditionals = (1 => 0, 2 => 0, 3 => 0);	# 1 is for the VIP date, so the default is off unless they are a VIP
													# 2 is for merchant information
													# 3 former partner date, only shows for non-partners who have a vip_date
	
	my $sth = $db->prepare("
		SELECT m.*,
			COALESCE(lc.description, '') AS language_preference,
			member_origin(m.id) AS member_origin,
			mfn.val_text AS vipnotes,
			mt.display_code,
			mop.payment_method,
			COALESCE(pm.description,'') AS pay_method_description
		FROM members m
		JOIN member_types mt
			ON mt.code=m.membertype
		LEFT JOIN language_codes lc
			ON lc.code2=m.language_pref
		LEFT JOIN member_followup_notes mfn
			ON mfn.id=m.id
		LEFT JOIN mop
			ON mop.id=m.id
		LEFT JOIN payment_methods pm
			ON pm.method=mop.payment_method
		WHERE m.id= ?");
	$sth->execute($id);
	my $member = $sth->fetchrow_hashref;
	$sth->finish;
	unless ($member->{'id'})
	{
		Err("Failed to retrieve member record for: $id");
		return;
	}

	foreach (keys %{$member})
	{
		$member->{$_} .= '' unless defined $member->{$_};
	}

	my $xml = $gs->GetObject($TMPL{'xml'}) || die "Failed to load template: $TMPL{'xml'}";
	my $lb = XMLin($xml, 'NoAttr'=>1);
	
#	$gs->Prepare_UTF8($lb, 'encode');
	
	my $tmpl = $gs->GetObject($TMPL{'tbl'}) || die "Failed to load template: $TMPL{'tbl'}";
	my $ma = {};
	
	if ($member->{'membertype'} eq 'v')
	{
		# if they are a Partner, we need the subscription type label
		($member->{'stype'}) = $db->selectrow_array("
			SELECT st.label
			FROM partner_subscription_monthly psm
			JOIN subscription_types st
				ON st.subscription_type=psm.subscription_type
			WHERE psm.member_id=$member->{'id'}") || '';
		
		# certain particulars are only shown for Partners
		$conditionals{1} = 1;
	}
	elsif ($member->{'membertype'} eq 'ma')
	{
		$ma = $db->selectrow_hashref("
			SELECT mam.business_name, mad.tracking, st.description AS stype, vst.status
			FROM merchant_affiliates_master mam
			JOIN merchant_discounts mad
				ON mad.type=mam.discount_type
			JOIN subscriptions s
				ON s.member_id=mam.member_id
			JOIN subscription_types st
				ON st.subscription_type=s.subscription_type
				AND st.sub_class IN ('MM','RM')
			JOIN vendor_status_types vst
				ON vst.status_type=mam.status
			WHERE mam.member_id= $member->{'id'}
		");
		
		$ma->{'tracking'} = $ma->{'tracking'} ? 'Tracking' : 'Direct Discount';
		
		# certain particulars are only shown for Merchants
		$conditionals{2} = 1;
	}
	elsif ($member->{'membertype'} eq 'tp')
	{
		# we need to add the Trial Partner expiration to the end of the display code per this request
		# 12/17/13 per Dick: can you add a hyphen then the deadline date to the Trial Partner (Member Type) designation in the MFUR (TP - 12/25/2013)
		$member->{'country'} ||= 'US';	# theoretically the country should not be NULL, but we don't want to chance bombing the function below
		my ($deadline) = $db->selectrow_array("SELECT date_fmt_by_country(deadline, '$member->{'country'}') FROM od.pool WHERE id= $member->{'id'}") || '';
		$member->{'display_code'} .= " - $deadline";
	}
	
	if ($member->{'membertype'} ne 'v' && $member->{'vip_date'})
	{
		$conditionals{3} = 1;
	}
	
#	my $coach = $db->selectrow_hashref("SELECT * FROM network.my_coach($member->{'id'}, '$member->{'membertype'}')");
	my $coach = $db->selectrow_hashref("SELECT * FROM my_coach($member->{'id'})");
	
	foreach (keys %conditionals)
	{
		$tmpl =~ s/conditional-$_-open.+?conditional-$_-close/ /sg unless $conditionals{$_};
	}
	
	if ($member->{'mail_option_lvl'} > 0)
	{
		#$member->{'mailflag'} = '<span class="emailgood">&#10004;</span>';
		$member->{'mailflag'} = '<img src="/images/icons/check-mark.png" height="14" width="14" alt="" />';
	}
	else
	{
		$member->{'mailflag'} = '<span class="emailbad">X</span>';
	}
	
	###### convert newlines to HTML
	$member->{'member_origin'} =~ s#\n|\r#<br />#g;
	$member->{'vipnotes'} =~ s#\n|\r#<br />#g;
	ParseMe::ParseAll(\$tmpl, {'member'=>$member, 'lb'=>$lb, 'coach'=>$coach, 'ma'=>$ma});
	return $tmpl;
}

1;

__END__

01/31/11 added the member_types.display_code to the mix
03/09/12 added payment method data to the mix
###### 02/12/13 added the TNT coach to the mix
04/07/15 added the mailFlag
04/15/15 revised the call to network.my_coach to simply my_coach()