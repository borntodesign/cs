package TrialPartner;

# started as a direct copy of odp.pm since there are similarities... and why start from scratch

use strict;
use CGI;
use JSON;
use Crypt::CBC;
#use Data::Dumper qw(Dumper);
#$Data::Dumper::Indent=0;
#$Data::Dumper::Terse=1;

# these are just in case we do not receive a DB handle
use lib '/home/httpd/cgi-lib';
use DB_Connect;

my $jsonFormat = {'utf8' => 1, 'pretty' => 1};	# for production we can get rid of 'pretty'
my $Q = ();	# our queue
my $e = ();	# will become our encryption object if we need it

my $NewTrialPartnerNotificationType = 157;

=head2 TrialPartner

	A class to provide some methods for getting certain bits of information regarding the Trial Partner "system"
	
	Some efforts have been made to reuse already looked up data in the event that this class's methods are reused during a session.
	That tends to make the code a bit more complex that it would ordinarily be.
	
=head3 new

	Instantiate a new 'TrialPartner' object
	
=head4 Arguments

	Pass in arguments on a hashref like this:
		{'db'=>active DB handle, 'cgi'=>CGI object...}
	
	DB handle will be created if one is not passed in and it is needed for completing execution of the requested method.
	You can also pass in a CGI object on the 'cgi' key. If you don't a new local copy will be initialized.
	
=cut

sub new
{
	my $args = shift;
	$args ||= {};
	$args = shift if $args && ref $args ne 'HASH';
	return bless ($args || {});
}

sub cgi
{
	my $self = shift;
	$self->{'cgi'} ||= new CGI;
	return $self->{'cgi'};
}

sub e
{

	$e ||= Crypt::CBC->new('-key' => 'TPformer0DP', '-cipher' => 'Blowfish');
	return $e;
}

#TODO change this to reflect the current state of who one's coach is.... if we really even need this function anymore

=head3 coach

	Get our coach either from the cookie info... or look it up
	Besides the return value, the 'coach' key is set on $self->{'me'}

=head4 Arguments

	A numeric member ID

=head4 Returns

	A hashref of the coach data:
		{'name'=>'Harry Finkler','emailaddress'=>'hj@finkler.me'}

=cut

sub coach
{
	my ($self, $id) = @_;
	$id ||= $self->WhoAmI();
	
	# has 'me' been setup and is it really ME
	my $coach = $self->{'me'}->{'coach'} if $self->{'me'}->{'coach'} && $self->{'me'}->{'id'} == $id;
	unless ($coach)
	{
		if (my $ck = $self->HasCookie())
		{
			my $me = $self->GetMeFromCookie();
			$coach = $me->{'coach'} if $me->{'id'} == $id
			# the coach can end up undef here... think if I have another cookie which does not match my ID
			# oh the joys of having everybody and their brother looking at everybody's pages ;(
		}

		if (! $coach)
		{
# 12/13/11... for now we have to give 'em their virtual coach based on the upcoming lines of sponsorship
#			$coach = $self->db->selectrow_hashref("
#				SELECT firstname, lastname, emailaddress, other_contact_info AS skype
#				FROM my_upline_vip($id)");
			$coach = $self->_tmpCoach($id);
		}
	}
	$self->{'me'}->{'coach'} = $coach;
	return $coach;
}

=head3 cgiCookie

	Returns a CGI cookie to stuff into a header.

=cut

sub cgiCookie
{
	my $self = shift;
	unless ($self->{'me'})
	{
		$self->{'me'} = $self->GetMe();
	}

#	unless ($self->{'me'}->{'coach'})
#	{
#		$self->{'me'}->{'coach'} = $self->coach();
#	}
	
	unless ($self->{'me'}->{'counts'})
	{
		$self->{'me'}->{'counts'} = $self->GetPoolCounts();
	}
	
	return $self->cgi->cookie(
		'-name'=>'myodp',
		'-value'=>$self->cookie(),
		'-expires'=>'1h',
		'-path'=>'/',
		'-domain'=>'.clubshop.com'
		);
}

=head3 cookie

	Returns an the JSON converted string of the Trial Partner position, encrypted and ready for a cookie value

=cut

sub cookie
{
	my $self = shift;
	return '' unless $self->{'me'};
	return $self->encrypt( $self->MeToJSON() );
}

=head3 CreateTrialPartnerPosition

	First off, see if the ID is already in a position.
	If not place them in a position.
	Return the particulars of the position.

=head4 Arguments

	A numeric ID.
	
=head4 Returns

	A scalar hashref.

=head4 Note:

	A requirement for gaining a Trial Partner position is that the identity of the member is verified by 2 factor authentication.
	It is presumed that the caller is handling this check and that is why we are requiring an ID parameter instead of determining it by simple parameter/cookie methods.
	
	Also, this method will overwrite any cached value of "me"
	
=cut

sub CreateTrialPartnerPosition
{
	my ($self, $id) = @_;
	unless ($id)
	{
		warn "A numeric ID must be passed to this method";
		return undef;
	}
	
	$self->{'me'} = $self->GetMe($id) || return undef;
	
	unless ($self->{'me'}->{'seq'})	# if we got an 'seq' from "me" then I am already registered in the system
	{
		# we are only inserting Affiliates... bahahaha sure, as of 01/06/14 Dick is asking if we allowed for "Members" to gain a TP position
		if ($self->{'me'}->{'membertype'} eq 'm' || $self->{'me'}->{'membertype'} eq 's')
		{
			my $ACstate = $self->db->{'AutoCommit'};	# begin_work turns AutoCommit off, so we cannot evaluate it after our transactions
			$self->db->begin_work if $ACstate;
			$self->db->do("UPDATE members SET membertype='tp', memo='making Trial Partner' WHERE id= $self->{'me'}->{'id'}");
			$self->db->do("INSERT INTO activity_rpt_data (id, spid, activity_type) VALUES ($self->{'me'}->{'id'}, $self->{'me'}->{'spid'}, 39)");	# create an activity record
			my ($poolid) = $self->db->selectrow_array("SELECT network.move_to_taproot_above_pool($self->{'me'}->{'id'})");	# make them the sponsor of the pool... put them into a vertical line above it
			unless ($poolid)
			{
				warn "This statement returned empty: SELECT network.move_to_taproot_above_pool($self->{'me'}->{'id'})";
				$self->{'me'} = undef;
				return undef;
			}
			else
			{
				# create/update their refcomp record so that they instantly have TPP ;-)
				my ($tpp) = $self->db->selectrow_array("SELECT opp FROM refcomp WHERE id= $poolid") || 0;
				my $ck = $self->db->do("UPDATE refcomp SET opp= $tpp WHERE id= $self->{'me'}->{'id'}");
				$self->db->do("INSERT INTO refcomp (opp, id) VALUES ($tpp, $self->{'me'}->{'id'})") unless $ck eq '1';
			}
			$self->db->do("INSERT INTO od.pool (id, pnsid) SELECT $self->{'me'}->{'id'}, pnsid FROM network.partner_network_separators WHERE pid= $poolid");
			$self->db->do("INSERT INTO notifications (id, spid, notification_type) VALUES ($self->{'me'}->{'id'}, $self->{'me'}->{'spid'}, $NewTrialPartnerNotificationType)");
			$self->db->commit if $ACstate;
			
			# refresh the values now that we are in
			$self->{'me'} = $self->GetMeFromID($id);
		}
	}
	
	return $self->{'me'};
}

# this is just an internal routine to make it possible to do $self->db->method instead of $self->{'db'}->method
sub db
{
	my $self = shift;
	$self->{'db'} ||= DB_Connect::DB_Connect('TrialPartner.pm');
	return $self->{'db'};
}

sub decrypt
{
	return e->decrypt($_[0]);
	
}

sub encrypt
{
	return e->encrypt($_[0]);
	
}

=head3 GetLineData

	Get all of the parties from me down to the pool.
	
=head4 Arguments

	Although this method uses the cached value of the member for the starting ID/SEQ, in order to provide an embedded flag for determining if another party has access,
	We optionally need a valid SQL statement to use in the CTE which will be left joined to the result set.
	The SQL should have a column "id" for matching the member.id in the main query
	An optional 3rd argument is a single quote encapsulated, comma delimited list of membertypes. This will default to 'tp','v','mp'.

=head4 Returns

	A reference to an array of hashrefs.
	The order of the array elements is descending from the ID.
	There is a flag in each row named "upline_access". If a CTE is passed to the method and the upline has access, this field will be populated, otherwise it will be NULL.
	
=cut
 
sub GetLineData
{
	my $self = shift;
	my $cte = shift || 'SELECT NULL::INTEGER AS id';
	my $mtype_list = shift || "'tp','v','mp'";
	
	return $self->{'gs'}->select_array_of_hashrefs("
		WITH perms AS ($cte)
		SELECT
			m.id,
			m.spid,
			m.alias,
			m.membertype,
			m.emailaddress,
			m.firstname,
			m.lastname,
			m.country,
			m.vip_date,
			COALESCE(mc.alias,'') AS coach_alias,
			COALESCE(mc.emailaddress,'') AS coach_emailaddress,
			perms.id AS upline_access,	-- that will end up being null except when there is a match on the CTE
			COALESCE(r.opp,0) AS tpp
			
		FROM members m
		JOIN od.pool op
			ON op.id=m.id
		LEFT JOIN my_coach myc	-- unfortunately brand new members do not yet have a coach assigned and so if they go to look at their report *they* are absent from the report
			ON myc.id=m.id
		LEFT JOIN members mc
			ON mc.id=myc.upline
		LEFT JOIN perms
			ON perms.id=m.id
		LEFT JOIN refcomp r
			ON r.id=m.id
			
		WHERE op.seq >= $self->{'me'}->{'seq'}
		AND op.pnsid = $self->{'me'}->{'pnsid'}
		AND m.membertype IN ($mtype_list)			-- when somebody becomes removed, they do not lose their od.pool record right away, so we need this membertype filter
		ORDER BY op.seq
	");
}

=head3 GetMe

	Given a numeric ID, returns a hashref that is a blend of some members data and some od.pool data.
	See GetMeFromID()
	
=cut

sub GetMe
{
	my ($self, $id) = @_;
	$id ||= $self->WhoAmI();
	return $self->{'me'} if $self->{'me'} && $self->{'me'}->{'id'} == $id;
	
	# if we haven't been called before, maybe we can get the data from the cookie?
	my $me = $self->GetMeFromCookie();
	if ($me && $me->{'id'} && $me->{'id'} == $id)
	{
		$self->{'me'} = $me;
	}
	else
	{
		$self->GetMeFromID($id) if $id;	# that will set the cached value of $self->{'me'}
		
		# if we did not have an ID to lookup
		$self->{'me'} ||= $self->meSkel();
		
		# now get our subscription pricing
		# we will default to USD if we do not have a country
#		my $c = $self->{'me'}->{'country'} || 'US';
#		$self->SubscriptionPricing($c);
	}
	return $self->{'me'};
}

=head3 GetMeFromCookie

	If the 'myodp' cookie is available, parse it out and return it as a hashref.
	If we don't have a cookie or we cannot parse it, we'll return an empty hashref.
	
=cut

sub GetMeFromCookie
{
	my $self = shift;

	return {} unless my $ck = $self->HasCookie();
	$ck = $self->cgi->unescape($ck);
	$ck = $self->JSONtoMe($ck);
	return $ck || {};
}

=head3 GetMeFromID

	Given a numeric ID, returns a hashref that is a blend of some members data and some od.pool data.
	Currently:
		{
			m.alias,
			m.id,
			m.firstname,
			m.lastname,
			m.membertype,
			m.country,
			m.language_pref,
			m.vip_date,
			od.pool.deadline AS a date,
			deadline_timestamp,
			od.pool.seq,
			od.pool.pnspid,
			days_left
		}
		
	This will not return any cached value but will rewrite any cached value.
	If ID does not match a member record, the return value will be undef.

=cut

sub GetMeFromID
{
	my $self = shift;
	my $id = shift || return undef;
	
	$self->{'me'} = $self->db->selectrow_hashref("
		SELECT
			tp.alias,
			tp.username,
			tp.id, tp.spid, tp.firstname, tp.lastname, tp.membertype,
			tp.country,
			tp.vip_date,
			tp.deadline,
			tp.deadline_timestamp,
			tp.seq,
			tp.pnsid,
			tp.days_left
			
		FROM od.trial_partners tp
		WHERE tp.id= ?
	", undef, $id);
	
	return $self->{'me'};
}

#=head3 GetPoolCounts
#
#	Pass in a numeric member ID and get back a hashref like this:
#		{'partner_count'=>xxx, 'member_count'=>yyyy}
#	Where partners are what is now known as VIPs and members are what are now known as non-VIPs
#	The 'counts' key is also set on $self->{'me'}
#	
#=cut
#
#sub GetPoolCounts
#{
#	my ($self, $id) = @_;
#	$id ||= $self->WhoAmI();
#
#	# has 'me' been setup and is it really ME
#	my $counts = $self->{'me'}->{'counts'} if defined $self->{'me'}->{'counts'} && $self->{'me'}->{'id'} == $id;
#	return $counts if $counts;
#
#	# if they are not in a pool they will not have a 'seq' value
#	if ($self->{'me'}->{'seq'})
#	{
#		$counts = $self->db->selectrow_hashref("
#			SELECT
#				SUM( CASE WHEN m.membertype='v' THEN 1 ELSE 0 END ) AS partner_count,
#				SUM( CASE WHEN m.membertype <> 'v' THEN 1 ELSE 0 END ) AS member_count
#			FROM members m
#			JOIN od.pool
#				ON m.id=od.pool.id
#			WHERE od.pool.seq > $self->{'me'}->{'seq'}
#		");
#	}
#	else
#	{
#		$counts = {'partner_count'=>0, 'member_count'=>0};
#	}
#	# at least put something in there if there are the bottom most person
#	$counts->{'member_count'} ||= 0;
#	$counts->{'partner_count'} ||= 0;
#
#	$counts->{'member_count'} = '1000+' if $counts->{'member_count'} >= 1000;
#	$self->{'me'}->{'counts'} = $counts;
#	return $counts;
#}

=head3 HasCookie

	If the 'myodp' cookie is available, returns the cookie value, otherwise returns undef

=cut

sub HasCookie
{
	my $self = shift;
	return $self->cgi->cookie('myodp');
}

=head3 HasPRTPorP

	If this party has a personally referred Trial Partner or Partner, this method returns true, otherwise false (0).

=head4 Arguments

	None

=cut

sub HasPRTPorP
{
	my $self = shift;
	return $self->db->selectrow_array("SELECT TRUE FROM members WHERE membertype IN ('tp','v') AND spid= $self->{'me'}->{'id'} LIMIT 1") || 0;
}

=head3 LineCounts

	Get the number of Trial Partners/Partners under me in the line.
	
=head4 Arguments

	None
	
=head4 Returns

	A hashref like this {'tp'=>100, 'v'=>2}
	
=cut

sub LineCounts
{
	my $self = shift;
	return $self->db->selectrow_hashref("
		SELECT COALESCE(op.tp, 0) AS tp, COALESCE(op.v, 0) AS v
		FROM dummy
		LEFT JOIN od.pool_counts op
			ON op.id= $self->{'me'}->{'id'}");
}

=head3 JSONtoMe

	Import a JSONized hashref into "me"
	If successful, cache the JSON
	
=head4 Arguments

	A json string that should evaluate to a hashref
	
=head4 Returns

	A hashref if successful, otherwise undef.
	
=cut

sub JSONtoMe
{
	my $self = shift;
	my $json = shift || return undef;

	$self->{'me'} = from_json($json, $jsonFormat);
	
	# queue up the json if it was successfully extracted
	if (ref $self->{'me'} eq 'HASH' && $self->{'me'}->{'id'})
	{
		$Q->{'json'} = $json;
	}
	else
	{
		$self->{'me'} = undef;
	}
	
	return $self->{'me'};
}

=head3 MadeTPxferRequest

	If they have made a Trial Partner transfer request within the last 30 days, this method returns true, otherwise false (0).
	
=head4 Arguments

	None
	
=cut

sub MadeTPxferRequest
{
	my $self = shift;
	return $self->db->selectrow_array(qq|SELECT TRUE FROM member_actions WHERE "type"=15 AND stamp >= NOW() - INTERVAL '30 days' AND id= $self->{'me'}->{'id'}|) || 0;	
}

sub meSkel
{
	my $self = shift;
	return {
		'id'=>0,
		'alias'=>'',
		'membertype'=>'',
		'country'=>'US',
		'counts'=>{'partner_count'=>0,'member_count'=>0},
		'coach'=>{}
	};
}

sub MeToJSON
{
	my $self = shift;
	return undef unless $self->{'me'};
	
	unless ($Q->{'json'})
	{
		$Q->{'json'} = to_json($self->{'me'}, $jsonFormat);
	}
	
	return $Q->{'json'};
}

=head3 PreProcess

	For parties that are in co-ops, we sometimes need to move them into a different co-op so that they will be placed into the proper line.
	This method takes care of that.

=head4 Arguments

	A member hashref with at least id and spid key/values

=head4

	Nothing of value.
	In most cases this method is a no-op

=cut

sub PreProcess
{
	my $self = shift;
	my $member = shift;

	# look up the container we need to transfer them to and make the transfer
	my ($nuspid) = $self->db->selectrow_array("SELECT coop.tp_container_assignment($member->{'id'})");
	return unless $nuspid;
	return if $nuspid == $member->{'spid'};
	
	$self->db->do("UPDATE members SET spid= $nuspid, memo= 'Moved to this container for Trial Partner position by TrialPartner::PreProcess()' WHERE id= $member->{'id'}");
}

#TODO this is obviously antiquated... so if we need it, we need to redefine it

=head3 SubscriptionPricing

	Get the subscription pricing information from the DB

=head4 Arguments

	A 2 character country code

=head4 Returns

	Sets and returns this hashref: $self->{'me'}->{'subscription_pricing'}
	
=cut

sub SubscriptionPricing
{
	my ($self, $c) = @_;
	($c) = $self->db->selectrow_array("
		SELECT currency_code FROM currency_by_country WHERE country_code=?", undef, $c);
	my $sth = $self->db->prepare("
		SELECT subscription_type, amount FROM subscription_pricing_current
		WHERE currency_code='$c' AND subscription_type IN (1,2,3)");
	$sth->execute;
	while (my $tmp = $sth->fetchrow_hashref)
	{
		$self->{'me'}->{'subscription_pricing'}->{ $tmp->{'subscription_type'} } = $tmp->{'amount'};
	}
	$self->{'me'}->{'subscription_pricing'}->{'currency_code'} = $c;
	return $self->{'me'}->{'subscription_pricing'};
}

=head3 SubscriptionPrice

	We are expecting that GetMe has been called and so that the subscription pricing
	information is available.
	
=head4 Arguments

	Expects to receive of of two arguments: initial -or- monthly

=head4 Returns

	A numeric value prepended with the currency code.

=cut

#TODO this is obviously antiquated... so if we need it, we need to redefine it
sub SubscriptionPrice($$)
{
	my ($self, $arg) = @_;
	my $rv = $self->{'me'}->{'subscription_pricing'}->{'currency_code'};
	if ($arg eq 'monthly')
	{
		$rv .= " $self->{'me'}->{'subscription_pricing'}->{'3'}";
	}
	elsif ($arg eq 'initial')
	{
		my $price = $self->{'me'}->{'subscription_pricing'}->{'1'} + $self->{'me'}->{'subscription_pricing'}->{'2'};
		$rv .= " $price";
	}
	return $rv;
}

=head3 WhoAmI

	Determines who we are based upon the ID parameter or the ID cookie, in that order.
	If neither of these values are available, then returns 0

=cut

sub WhoAmI
{
	my $self = shift;
	my $rv = $self->cgi->param('id') || $self->cgi->cookie('id') || 0;
	($rv) = $self->db->selectrow_array('SELECT id FROM members WHERE alias=?', undef, $rv) if $rv =~ /\D/;
	return $rv;
}

1;


