package storeInfoFeed;
# this class is used by more than one resource, so you cannot modify the type of the return values without breaking something

# this is just an internal routine to make it possible to do $self->db->method instead of $self->{'db'}->method -as- well as to deal with various types of DB arguments to new()
sub db
{
	my $self = shift;
	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

=head3 ExtensionsToItems

=head4 Arguments

	vendor_id, mall_id, exttype
		
=head4 Returns

	A hashref with a single key 'item' which contains a block of items of the specified extension type: <item></item>
	
=cut

sub ExtensionsToItems
{
	my $self = shift;
	
	my $xml = ();
	my $tmp = $self->GetExtensions($_[0],$_[1],$_[2]);
	foreach (@{$tmp})
	{
		$xml->{'item'} .= "<item>$_->{'val_text'}</item>";
	}
	return $xml;
}

=head3 GetExtensions

=head4 Arguments

	Required: vendor_id, mall_id, exttype
	Optional: translation flag (boolean)
		
=head4 Returns

	An array ref with hashref elements consisting of pk and val_text values from mall_vendor_extensions that are valid according to the arguments
	
=cut

sub GetExtensions
{
	my $self = shift;
	
	# our arguments: vendor_id, mall_id, exttype, translation flags
	###### if we want translation flags, then we are not concerned with evaluating translation logic issues
	###### otherwise, we will not pull items that don't satisfy the business logic
	my $qry = 'SELECT pk, val_text';
	$qry .= ', translate, ignore_translate_for_display' if $_[3];
	$qry .= "
		FROM mall_vendor_extensions
		WHERE exttype= $_[2]
		AND void=FALSE
		AND NOW()::DATE BETWEEN start_date AND COALESCE(end_date, NOW()::DATE)
		AND vendor_id=?
		AND mall_id= ?
	";
	$qry .= 'AND (translate=FALSE OR ignore_translate_for_display=TRUE)' unless $_[3];
	$qry .= ' ORDER BY end_date';
	my $sth = $self->db->prepare($qry);
	$sth->execute($_[0], $_[1]);
	my @rv = ();
	while(my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	return \@rv;
}

=head3 new

	Instantiate a new 'storeInfoFeed' object
	
=head4 Arguments

	Pass in arguments on a hashref like this:
		{'db'=>active DB handle, 'cgi'=>CGI object...}
	
	A DB handle is required even if it is not active until the time we use it.
	
=cut

sub new
{
	my $args = shift;
	$args ||= {};
	$args = shift if $args && ref $args ne 'HASH';
	return bless ($args || {});
}

1;