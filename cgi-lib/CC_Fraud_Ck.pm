package CC_Fraud_Ck;
use 5.010;	# needed for state variables
use strict;
use G_S;

my %TMPL = ( 'lp'=>10896 );

=head2 CC_Fraud_Ck

The beginning of a package to perform some CC fraud mitigation.

=cut

=head3 new(\$)

Your typical object creator.

=head4 Arguments

Requires a hash reference with these key value pairs:
	{
		'db'=> an open database handle
	}

=head4 Returns

A CC_Fraud_Ck object

=cut

sub new
{
	my ($class, $args) = @_;
	die "An active DB handle is required on the 'db' key of the argument hashref" unless $args->{'db'};
	return bless $args;
}

=head3 Check_Country_And_Ensure_Card_Authorized(\$)

The logic goes like this:
	Check the country for the IP address provided.
	If the country is not on our fraud alert list, then we are good.
	If the country is on our fraud alert list, check that the credit card has been authorized for use.
	If so, then we are good. Otherwise, return the translated text message that has been defined.

=head4 Arguments

Requires a hash reference with these keys:
	{
		'ipaddr'=> the ip address we need to check against
		-OR-
		'country_code'=> 2 character country code
		'ccnum'=> the credit card number we will need to check
	}

=head4 Returns

Undef if we are in the clear, otherwise returns a textual value to be used in whatever error handling is desired.

=head4 Notes

	As of March 2015, our IP address data is stale and unreliable and so Dick wants to rely on the country they submit.
	So, this method is moving to work with either an ipaddr or a country code, in the event this class is used by more than appx.

=cut

sub Check_Country_And_Ensure_Card_Authorized
{
	my ($self, $args) = @_;
	if ($args->{'country_code'})
	{
		return undef unless $self->Check_Country($args);
	}
	else
	{
		return undef unless $self->Check_IP_Addr($args);
	}

	return undef if $self->Check_CC_Number($args);

	return $self->ErrorNotice();
}

=head3 Check_CC_Number(\$)

Determine if the CC number provided is on our authorized list

=head4 Arguments

Requires a hash reference with these keys:
	{
		'ccnum'=> the credit card number we will need to check
	}

=head4 Returns

Undef if no match, otherwise a true value

=cut

sub Check_CC_Number
{
	my ($self, $args) = @_;
	die "A CC number should be provided on the 'ccnum' key of the argument hashref" unless $args->{'ccnum'};
	return $self->{'db'}->selectrow_hashref('
		SELECT acc.*
		FROM fraud_control.authorized_cc acc
		WHERE ccnum= ?
		AND auth_revoked=FALSE', undef, $args->{'ccnum'});
}

=head3 Check_Country

Is the country argument on our list of fraud alert countries?

=head4 Arguments

Requires a hash reference with these keys:
	{
		'country_code'=> a valid 2 character country code
	}

=head4 Returns

Undef if no match, otherwise a true value

=cut

sub Check_Country
{
	my ($self, $args) = @_;
	die "A valid country code key is missing on the argument hashref" unless $args->{'country_code'};
	return $self->{'db'}->selectrow_hashref("SELECT * FROM fraud_control.controlled_cc_countries WHERE country_code= ?", undef, $args->{'country_code'});
}

=head3 Check_IP_Addr(\$)

Determine if the IP address argument is in a country we have placed on our fraud alert list

=head4 Arguments

Requires a hash reference with these keys:
	{
		'ipaddr'=> the ip address we need to check against
	}

=head4 Returns

Undef if no match, otherwise a hashref to the matching ip_latlong_lookup record.

=cut

sub Check_IP_Addr
{
	my ($self, $args) = @_;
	die "An IP address should be provided on the 'ipaddr' key of the argument hashref" unless $args->{'ipaddr'};
	
	# in the event we are called more than once, only do the lookup once
	state $cache;
	$cache->{ $args->{'ipaddr'} } = $self->{'db'}->selectrow_hashref('
		SELECT ipl.*
		FROM ip_latlong_lookup ipl
		JOIN fraud_control.controlled_cc_countries fcc
			ON fcc.country_code=ipl.countryshort
		WHERE ? BETWEEN inet_from AND inet_to', undef, $args->{'ipaddr'});
	return $cache->{$args->{'ipaddr'}};
}

=head3 Check_Taproot_Line(\$)

As of 05/05/15 Dick wants to ensure every party/credit card used in the Shili line is approved, so we are going to check the line a party is from based upon their pnsid.

=head4 Arguments

Requires a hashref with these keys:
	{
		'id'=> the numeric ID of the member we are checking
	}

=head4 Returns

Undef if no match, otherwise the mviews.positions_freshest.pnsid if there is a match on the Shili line.

=cut

sub Check_Taproot_Line
{
	my ($self, $args) = @_;
	die "A member 'id' key is missing on the argument hashref" unless $args->{'id'};

	# in the event we are called more than once, only do the lookup once
	state $cache;
	$cache->{ $args->{'id'} } = $self->{'db'}->selectrow_array('SELECT pnsid FROM mviews.positions_freshest WHERE pnsid= 5603104 AND id=?', undef, $args->{'id'});
	return $cache->{$args->{'id'}};
}

=head3 Check_Member_Authorized(\$)

Dick wants to be able to authorize arbitrary member IDs to use the upgrade app regardless of their country.
This intention most likely extends to the use of credit cards at the (Club Accounts) funding.cgi interface.
So this method will check the existence of the id in the appropriate table.

=head4 Arguments

Requires a hash reference with these keys:
	{
		'id'=> the numeric ID of the member we are checking
	}

=head4 Returns

Undef if no match, otherwise the matching ID.

=cut

sub Check_Member_Authorized
{
	my ($self, $args) = @_;
	die "A member 'id' key is missing on the argument hashref" unless $args->{'id'};

	# in the event we are called more than once, only do the lookup once
	state $cache;
	$cache->{ $args->{'id'} } = $self->{'db'}->selectrow_array('SELECT id FROM fraud_control.authorized_members WHERE id=?', undef, $args->{'id'});
	return $cache->{$args->{'id'}};
}

=head3 ErrorNotice()

Initially created to centralize this code block when it needed to be conditionally called from more than one routine

=head4 Arguments

An optional textual xpath node location in the language pack for this module, currently 10896. It will default to //card_auth_req

=head4 Returns

The value of the node if successfully extracted, otherwise "An error has occurred.\nCannot identify $nodeName"

=cut

sub ErrorNotice
{
	my ($self, $nodeName) = @_;
	$nodeName ||= '//card_auth_req';
	my $doc = G_S::Get_Object($self->{'db'}, $TMPL{'lp'}) || die "Cannot identify a template with ID: $TMPL{'lp'}";
	require XML::local_utils;
	my $lu = XML::local_utils->new();
	return $lu->extractNodeFromXML($nodeName, \$doc) || "An error has occurred.\nCannot identify $nodeName";
}

1;