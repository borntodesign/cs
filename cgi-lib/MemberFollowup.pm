package MemberFollowup;
use strict;
use Digest::MD5 qw(md5_hex);

=head1
	MemberFollowup
	methods for creating records in the "Member Followup System"

=head4
	created:		03/11/09	Bill MacArthur
	last modified:	11/25/14	Bill MacArthur
=cut

=head4
	my $mf = MemberFollowup->new(<database handle>)
=cut
our $salt = 'xpl94857c';

=head4
	Be sure to pass in an active database handle to new so that it will be available across the board
	my $mf = MemberFollowup->new($dbh);
=cut

sub new {
	my ($self, $dbh) = @_;
	return bless {dbh => $dbh}, __PACKAGE__;
}

=head4
	CreateMemberFollowup
	Requires a member ID, upline ID, the followup text and...
		an optional arrayref pointing to a list of activity_rpt_data pk values (or a single value).
	Creates a member_followup_text record and associated member_followup_key records pointing to the activity keys passed.
	Returns nothing as it will die if there are any unexpected DB results
=cut

sub CreateMemberFollowup
{
	my ($self, $member_id, $upline_id, $text, $list_arrayref) = @_;

# Somehow, the VIPs end up submitting duplicate followups on activities from the activity reports
# Since it is conceivable that they could submit some dupes and some non-dupes, we will do this
# if they do not submit any activities, then we will allow the text insert to commit
# if they submit a mix of dupes and non-dupes, we will allow the text insert to commit
# if they submit all dupes, we will rollback the text insert to avoid duplicate unlinked followup items at the member profile
	my $_commit = $self->dbh->{'AutoCommit'}; # in case we are not auto-committing for some reason coming in

	$self->dbh->{'AutoCommit'} = 0;
	my ($followup) = $self->dbh->selectrow_array("SELECT nextval('member_followup_text_pk_seq')") || die "Failed to obtain a followup record id\n";
	
	my $rv = $self->dbh->do("INSERT INTO member_followup_text (pk, id, upline_id, val_text) VALUES ($followup,?,?,?)", undef, $member_id, $upline_id, $text);
	die "Failed to create a followup record\n" unless $rv eq '1';

	my $proceed = 1;
	my $skip = my $keep = 0;
# next, create key records with which to link the followup record with zero or more activities
	if ($list_arrayref)
	{
		# if we only passed a single scalar value, then we'll change it to an arrayref
		$list_arrayref = [$list_arrayref] unless ref $list_arrayref;

		foreach (@{$list_arrayref})
		{
			# first check to see if we already exist
#			my ($ck) = $self->dbh->selectrow_array("SELECT mft_pk FROM member_followup_key WHERE mft_pk= $followup AND ard_pk= ?", undef, $_);

			# regardless of whether we match on the mft_pk, we can only have one record with a given ard_pk
			my ($ck) = $self->dbh->selectrow_array("SELECT mft_pk FROM member_followup_key WHERE ard_pk= ?", undef, $_);
			
			# if we get a result, then we already have a key
			if ($ck)
			{
				$skip++;
				next;
			}
			$keep++;
			$self->dbh->do("INSERT INTO member_followup_key (mft_pk, ard_pk) VALUES ($followup,?)", undef, $_);
		}
	}

	# if we didn't skip any we're all set, if we skipped 'em all we won't commit
	$proceed = 0 if $skip && ! $keep;
	
	if ($proceed > 0)
	{
		$self->dbh->commit;
	}
	else
	{
		$self->dbh->rollback;
	}
	
	$self->dbh->{'AutoCommit'} = $_commit;
}

=head4
	CreateReminder
	Requires a member ID, upline (should be a hashref, minimally with an ID and alias),
		the reminder text and the reminder date
	Creates a type 33 activity with some text for coming up at a later date
	Returns the number of rows created, 1 or 0 (-0- will be returned if the reminder is today's date)
=cut

sub CreateReminder
{
	my ($self, $member_id, $upline, $text, $date) = @_;

	# we should never have a reminder for today ;)
	my $rv = $self->dbh->do('INSERT INTO activity_rpt_data (activity_type, id, spid, rpt_date, val_text) SELECT 33, ?, ?, ?, ? WHERE ? > NOW()::DATE', undef,
		($member_id, $upline->{'id'}, $date, "$upline->{'alias'}: $text", $date));
	return $rv;
}

=head4
	$mf->dbh returns the database handle passed into new()
=cut

sub dbh { return $_[0]->{'dbh'} }

=head4
	DecodeActivities
	Take an array ref and perform DecodeActivity on each item in it
=cut

sub DecodeActivities
{
	my ($self, $ref) = @_;
	return unless $ref && scalar @{$ref};
	# convert our activity param values like: 13333953:d7a7b26a7f76d572909dfb4892f0f3ce
	# to the simple activity_rpt_data pk value after we ensure that the data is not munged
	for (my $x=0; $x < scalar @{$ref}; $x++)
	{
		${$ref}[$x] = $self->DecodeActivity( ${$ref}[$x] );
	}
}

=head4
	DecodeActivity
	Take a previously encoded parameter and see if it is valid
=cut

sub DecodeActivity
{
	my ($self, $val) = @_;
	my @a = split(/:/, $val);

	return $a[1] eq md5_hex($a[0] . $salt) ? $a[0] : undef;
}

=head4
	EncodeActivity
	Take an activity_rpt_pk value and create a "self-checking" parameter value like 12345:<md5 hash>
=cut

sub EncodeActivity
{
	my ($self, $val) = @_;
	return "$val:" . md5_hex($val . $salt);
}

1;

__END__

11/25/14	Hopefully rectified an ongoing problem with primary key errors on the member_followup_key table by altering our record check in CreateMemberFollowup()
