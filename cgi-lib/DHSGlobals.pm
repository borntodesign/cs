package DHSGlobals;

=head1 NAME:
DHSGlobals

    This module holds constants that indicate whether or not you are on the production server, or development server

=head1
VERSION:

    0.1

=head1
SYNOPSIS:

=head2
How To Use This Module:

    This module holds constants that indicate whether or not you are on the production server, or development server.

=head2
Code Example

    use DHSGlobals;
if(DHSGlobals::PRODUCTION_SERVER)
{
    #Do for Production Server.
} else {
    #Do for Development Server.
}

=head1
PREREQUISITS:

    None

=cut

    use strict;

=head1 DESCRIPTION

=head2 Constants
=cut

    use constant DB_LAST_UPDATED => 10012008;
    use constant PRODUCTION_SERVER => 1;
    use constant CLUBSHOP_DOT_COM => 'www.clubshop.com';
    use constant GLOCALINCOME_DOT_COM => 'www.glocalincome.com';

1;

__END__

=head1
CHANGE LOG

    Date What was changed

=cut

