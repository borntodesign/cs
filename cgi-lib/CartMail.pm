package CartMail;

##
## Created 12/14/06 Shane Partain
##
## This module was written to prvide a simpler way of converting
## some existing scripts from using the sendmail command, namely,
## auth.cgi and signup.cgi in the shopping cart system.
##

use strict;
use MailingUtils;
use ParseMe;

sub mail_fh
{
	my $mu = new MailingUtils;
	my $msg = shift;
	my %mail = ();
        my $hdr = ParseMe::Grab_Headers(\$msg);
        $mu->DrHeaders($hdr);
        $mail{headers} = $hdr;

        $mail{txt} = \$msg;
        unless ( $mu->Mail(%mail) )
        {
                print "$MailingUtils::error\n";
                return;
        }

}

1; 
