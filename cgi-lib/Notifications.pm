package Notifications;

=head1 NAME:
Notifications

	Create a notification in the notification table.  We primarily use notifications for emails.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	This class manages notifications

=head2
Code Example

	my $Notifications = Notifications->new($db);

=head1
PREREQUISITS:

	Other classes, or packages that are used

=cut

use strict;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	$attribute_name	string	What this attributes purpose is.

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $self			= {};
    $self->{db} = shift;
    
    bless($self, $class);
    $self->_init();
    return $self;
}

=head2
METHODS

=head2
PRIVATE

=head3
_init

=head4
Description:

	Description of what is set in _init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $self = shift;

    $self->{version}	= '0.1';
    $self->{name}		= 'Notifications';

}





=head2
PUBLIC

=head3 createNotification

=head4
Description:

	This inserts a notification record.

=head4
Params:

	hashref
		should be a hashref where the keys
		are named the same as the fields in
		the notifications table.

=head4
Return:

	returns	int Primary Key for the new notification record on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=cut

sub createNotification
{

	my $this = shift;
	my $params = shift;

	my $insert_query = 'INSERT INTO notifications ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	
	eval{
		my @primary_key_from_notifications_table = $this->{db}->selectrow_array("SELECT NEXTVAL('notifications_pk_seq')");
		
		die 'We were not able to generate the primary key. ' if ! $primary_key_from_notifications_table[0];
		
		$params->{pk} = $primary_key_from_notifications_table[0];
		
		foreach my $key (keys %$params)
		{
			next if (! $params->{$key});
			
			push @fields, $key;
			push @place_holders, '?';
			push @values, $params->{$key};
			
		}
		
		$insert_query .= '(' . join(',', @fields) . ') VALUES(' . join(',', @place_holders) . ') ';
		
		my $status = $this->{db}->do($insert_query, undef, @values);
		$return_value = ($status && $status ne '0E0') ? $primary_key_from_notifications_table[0]: 0;
		
	};
	if($@)
	{
		use Data::Dumper;
		my $temp = \@values;
		die "\nNotifications::createNotification -- Query: $insert_query -- Values: " . Dumper($temp) . " -- Error: " . $@ . "\n";
	}
	
	return $return_value;
	
}




1;

__END__

=head1
SEE ALSO

L<LinkedPackage>, UnlinkedPackage

=head1
CHANGE LOG

	Date	What was changed

=cut

