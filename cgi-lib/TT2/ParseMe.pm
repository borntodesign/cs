package TT2::ParseMe;
use Encode;

=head2 new

Create a new instance of TT2::ParseMe

=head3 Arguments

Any arguments passed should be passed via hash reference.
The only key currently recognized is 'TT' => a valid TT object

=cut

sub new
{
	my $class = shift;
	my $args = shift || {};
	die 'Arguments must be supplied in a hashref' if (defined $args) && ref $args ne 'HASH';
	return bless $args;
}

=head2 Parse

=head3 Description

Pass in a scalar or a hash reference
Apply use TT to process the data supplied to the scalar or each element of the hashref

=head3 Arguments

The scalar or hashref which needs parsing
A hashref of data that will be passed to TT
An optional reference to a TT object. If one is not provided, a new object will be created.

=cut

sub Parse
{
	my ($self, $arg, $data, $TT) = _self_or_default(@_);
	$TT ||= $self->{'TT'} || TT();

	###### we can accept a simple value which we will convert and return
	###### or a hashref, in which case we'll convert every key in place
	my $hashref = ref $arg eq 'HASH' ? $arg : {'key'=>$arg};

	foreach (keys %{$hashref})
	{
		if (not defined $hashref->{$_})
		{
			next;
		}
		elsif (ref $hashref->{$_})	# this is to deal with multi-level hashrefs
		{
			Parse($hashref->{$_}, $data, $TT);
		}
		else
		{
			my $tmp = ();
			$TT->process(\$hashref->{$_}, $data, \$tmp);
			$hashref->{$_} = $tmp;
		}
	}
	return (ref $arg eq 'HASH') ? undef : $hashref->{'key'};
}

sub TT
{
	my $TT = ();
	eval {
		require Template;
		$TT = new Template;
	};
	return $TT;
}

sub _self_or_default
{
	return @_ if defined($_[0]) && (!ref($_[0])) && ($_[0] eq __PACKAGE__);

	###### I clipped this from CGI.pm
	unless (defined($_[0]) && (ref($_[0]) eq __PACKAGE__))
	{
		$Q ||= new(__PACKAGE__);
		unshift(@_, $Q);
	}
	return wantarray ? @_ : $Q;
}

1;