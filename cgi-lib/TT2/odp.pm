package Template::Plugin::Local::odp;
use strict;

use lib '/home/httpd/cgi-lib';
use base qw(Template::Plugin odp);
#use odp;	# that would be the generic odp class

=head2 Template::Plugin::Local::odp

	A plugin for Template pages that need data from the One Downline Pool system

=cut

sub debug
{
	my ($self, $args) = @_;
	return $args;
}
sub new
{
	my ($class, $context, @params) = @_;
    return bless {
         '_CONTEXT' => $context
    }, $class;
}

sub GetSetCookie
{
	my $self = shift;

	my $id = $self->WhoAmI();

	my $_id = $self->GetMeFromCookie->{'id'} || 0;

	# WhoAmI will always return an ID parameter before a cookie ID
	# if the values differ, we know we need to setup a new cookie value
	# which has to be done after we allow the new cookie value to be created by GetMe()
	my $flg = ($_id == $id) ? 1:0;
	
	delete $self->{'me'};
	$self->GetMe();
	$self->coach();
	$self->GetPoolCounts();
	
	# if we already have the cookie, we do not want to recreate it as it will end up never expiring
	# if we leave it alone it will expire and we can recreate it at that point
	return $flg ? '' : $self->jsCookie();
}

sub jsCookie
{
	my $self = shift;
	
	# that is 3600000 milliseconds (1 hour)
	return qq#
		var today = new Date();
		var expires_date = new Date( today.getTime() + (3600000) );
		document.cookie = "myodp=" + escape("# .
		$self->cookie() .
		q#") + ";expires=" + expires_date.toGMTString() + ";path=/;domain=.clubshop.com";#;
}

1;

