###### last modified 10/21/02	Bill MacArthur
###### 10/09/02 changed the behaviour or the session cookie setter to add rather than SET a cookie
###### setting seemed to blow away any other cookies that were set earlier in the authentication procedure
###### 10/10/02 reworked authenticate to pass along any errors from authen_ses_key if there were any
###### and to allow retention of their current cookies if they were otherwise good
###### 10/16/02 also enabled handling of auth_user errors
###### 10/21/02 changed package name

package mod_perl;

use strict;


1;

__END__

 
