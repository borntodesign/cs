package Solicit;
###### a routine that inserts an email address into the do_not_solicit table
######
###### Requires a database handle, valid email address, and any notes.
###### Returns 0 for successful insert, a time stamp of an existing record, and -1 for DB failure.
######
###### 08/18/05	Karl Kohrt
######
###### Modified: 
		
###### Create a Do_Not_Solicit record.
sub Do_Not_Solicit($$$)
{
	my ($db, $emailaddress, $notes) = @_;

	# Check to see if the email address is already in the table.
	my $when = $db->selectrow_array("SELECT stamp::date
						FROM do_not_solicit
						WHERE emailaddress = ?",
						undef, $emailaddress)
						|| 0;
	unless ( $when )
	{
		my $qry .= "INSERT INTO do_not_solicit 	( emailaddress, notes )
					VALUES 	( ?, ? );";
		my $sth = $db->prepare($qry);
		# Create the record.
		unless ( $sth->execute( $emailaddress, $notes ) )
		{
			# Flag it if the record didn't get created.
			$when = -1;
		}
	}
	return $when;
}

1;
