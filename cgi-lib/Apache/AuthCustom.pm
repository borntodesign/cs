###### once upon a time this was originally Apache::AuthCookieDBIRadius

###### this has been stripped and heavily modified to accomodate our needs
###### 05/05/03 uppercased the user var before entering the first query in authen_cred
###### this allowed the upper function to be removed from the query, which let the DB use the alias index
###### 05/30/03 added a special authentication for MAF's
###### 08/15/03 added restrictions to MA logins so that they have to be active in order to login unless they are in the process of signing up
###### 10/28/03 added error handling for invalid Merchant ID entries
###### 01/13/04 added uninit'd var handling for no hash returned from a cookie
###### 06/07/04 added passing back of the spid which was already in the cookie
###### 12/09/04 disabled all the extraneous cookies and added a simple 'id' cookie
###### 04/17/06 added handling for staff md5 passwords
###### 07/25/06 changed the membertype check to an equivalency rather than a regex
###### 01/21/09 tweaked the authentication of staff to use the MD5 check properly
###### 08/11/09 added the creation of a cookie for the 'ma' membership of a maf login
# 05/26/10 added an instruction to turn off the utf8 flag in CreateTicket because when creating a ticket from a caller outside the regular login environment
# if the utf8 flag was on for any of the data and there were wide characters, as there are in Greek, apparently md5_hash doesn't recognize the flag and barfs
# by turning the flag off, it stays happy

package AuthCustom;

use Carp;
use Encode;
use Apache2::RequestRec;
use Apache2::Connection;
use APR::URI;
use strict;
#use Apache2;
#use Apache::DBI;	###### since we use many different ID when connecting, this is not a practical option
use Apache2::Const;
#use Apache::File;
#use CGI::Cookie;		###### added for extra functionality
require Apache2::Cookie;
use Digest::MD5 qw( md5_hex );
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );
# Also uses Crypt::CBC if you're using encrypted cookies.
use Crypt::CBC;
use Tie::IxHash;

use lib ('/home/clubshop.com/cgi-lib');
use DB_Connect;	###### this is a custom module in cgi-lib
use Apache::AuthCookieCustom;

###### our CUSTOM AuthCookie

use vars qw( @ISA );
@ISA = qw( Apache::AuthCookieCustom );

#===============================================================================
# F U N C T I O N   D E C L A R A T I O N S
#===============================================================================

sub _log_not_set($$);
sub _dir_config_var($$);
sub _dbi_config_vars($);
sub _now_year_month_day_hour_minute_second();
sub _percent_encode($);
sub _percent_decode($);

sub authen_cred($$\@);
#sub authen_ses_key($$$;$$);
sub group($$\@);

#===============================================================================
# P A C K A G E   G L O B A L S
#===============================================================================

use vars qw( %CIPHERS );
# Stores Cipher::CBC objects in $CIPHERS{ idea:AuthName },
# $CIPHERS{ des:AuthName } etc.

use vars qw( %SECRET_KEYS );
# Stores secret keys for MD5 checksums and encryption for each auth realm in
# $SECRET_KEYS{ AuthName }.
	
#===============================================================================
# S E R V E R   S T A R T   I N I T I A L I Z A T I O N
#===============================================================================

BEGIN {

###### we'll just hard code it right in since we have only one auth realm
	$SECRET_KEYS{'Generic'} = '9(7^%#=|';
	$SECRET_KEYS{'maf'} = 'onb8%^gt';

###### if for some reason we got none we're gonna have a problem
	unless(defined %SECRET_KEYS){ Apache::log_error( "No secret keys gathered." ); return 0 }
}

#===============================================================================
# P R I V A T E   F U N C T I O N S
#===============================================================================

#-------------------------------------------------------------------------------
# _log_not_set -- Log that a particular authentication variable was not set.

sub _log_not_set($$)
{
###### we are not passing this value
# 	my( $r, $variable ) = @_;
# 	my $auth_name = $r->auth_name;
# 	$r->log_error( "Apache::AuthCookieDBIRadius: $variable not set for auth realm
# $auth_name", $r->uri );
}

#-------------------------------------------------------------------------------
# _dir_config_var -- Get a particular authentication variable.

sub _dir_config_var($$)
{
	my( $r, $variable ) = @_;

	return unless $r;

	my $auth_name = $r->auth_name;
	return $r->dir_config( "$auth_name$variable" );
}

#-------------------------------------------------------------------------------
# _dbi_config_vars -- Gets the config variables from the dir_config and logs
# errors if required fields were not set, returns undef if any of the fields
# had errors or a hash of the values if they were all OK.  Takes a request
# object.

sub _dbi_config_vars($)
{
###### since $r may be empty, we have to test for it before trying its functions
	my( $r ) = @_;

	my %c; # config variables hash

	#<WhatEverDBI_CryptType>
	#What kind of hashing is used on the password field in the database.  This can
	#be 'none', 'crypt', or 'md5'.  This is not required and defaults to 'none'.

	$c{ DBI_crypttype } = ($r) ? (_dir_config_var( $r, 'DBI_CryptType' ) || 'none') : 'none';

	#<WhatEverDBI_SecretKeyFile>
	#The file that contains the secret key (on the first line of the file).  This
	#is required and has no default value.  This key should be owned and only
	#readable by root.  It is read at server startup time.
	#The key should be long and fairly random.  If you want, you
	#can change it and restart the server, (maybe daily), which will invalidate
	#all prior-issued tickets.

###### we have this hardcoded at the top
# 	unless ( $c{ DBI_secretkeyfile } = _dir_config_var $r, 'DBI_SecretKeyFile' )
# 	{
# 		_log_not_set $r, 'DBI_SecretKeyFile';
# 		return undef;
# 	}

	#<WhatEverDBI_EncryptionType>
	#What kind of encryption to use to prevent the user from looking at the fields
	#in the ticket we give them.  This is almost completely useless, so don't
	#switch it on unless you really know you need it.  It does not provide any
	#protection of the password in transport; use SSL for that.  It can be 'none',
	#'des', 'idea', 'blowfish', or 'blowfish_pp'.
	#This is not required and defaults to 'none'.'

	$c{ DBI_encryptiontype } = 'des'; #($r) ? (_dir_config_var( $r, 'DBI_EncryptionType' ) || 'none') : 'none';

	# If we used encryption we need to pull in Crypt::CBC. (we are doing it at the top)
# 	if ( $c{ DBI_encryptiontype } ne 'none' ) 
# 	{
# 		require Crypt::CBC;
# 	}

	#<WhatEverDBI_SessionLifetime>
	#How long tickets are good for after being issued.  Note that presently
	#Apache::AuthCookie does not set a client-side expire time, which means that
	#most clients will only keep the cookie until the user quits the browser.
	#However, if you wish to force people to log in again sooner than that, set
	#this value.  This can be 'forever' or a life time specified as:
	#DD-hh-mm-ss -- Days, hours, minute and seconds to live.
	#This is not required and defaults to '00-12-00-00' or 12 hours.
	$c{ DBI_sessionlifetime } = ($r) ? (_dir_config_var( $r, 'DBI_SessionLifetime' ) || '00-12-00-00') : '00-12-00-00';

	# Custom variables from httpd.conf.
# 	$c{ DBI_a }    			  = _dir_config_var( $r, 'DBI_a' ) || 'off';
# 	$c{ DBI_b }     			  = _dir_config_var( $r, 'DBI_b' ) || 'off';
#  	$c{ DBI_c }    			  = _dir_config_var( $r, 'DBI_c' ) || 'off';
#  	$c{ DBI_d }    			  = _dir_config_var( $r, 'DBI_d' ) || 'off';
#  	$c{ DBI_e }    			  = _dir_config_var( $r, 'DBI_e' ) || 'off';
#  	$c{ DBI_f }    			  = _dir_config_var( $r, 'DBI_f' ) || 'off';
#  	$c{ DBI_g }    			  = _dir_config_var( $r, 'DBI_g' ) || 'off';
  	$c{ REQ_mtype } = ($r) ? (_dir_config_var( $r, 'REQ_mtype' ) || 'm v s ag agu r staff maf mal ma amp') : 'm v s ag agu r staff maf mal ma amp';

	# other fields from httpd.conf.	
#	$c{ DBI_activeuser }      = _dir_config_var( $r, 'DBI_activeuser' ) || 'off';
 	$c{ DBI_activeuser } = 'off';
#	$c{ DBI_log_field } 	  	  = _dir_config_var( $r, 'DBI_log_field' ) || 'last_access';
	$c{ DBI_log_field } = 'last_access';

	return %c;
}

#-------------------------------------------------------------------------------
# _now_year_month_day_hour_minute_second -- Return a string with the time in
# this order separated by dashes.

sub _now_year_month_day_hour_minute_second()
{
	return sprintf '%04d-%02d-%02d-%02d-%02d-%02d', Today_and_Now;
}

#-------------------------------------------------------------------------------
# _percent_encode -- Percent-encode (like URI encoding) any non-alphanumberics
# in the supplied string.

sub _percent_encode($)
{
	my( $str ) = @_;
	$str =~ s/([^\w])/ uc sprintf '%%%02x', ord $1 /eg;
	return $str;
}

#-------------------------------------------------------------------------------
# _percent_decode -- Percent-decode (like URI decoding) any %XX sequences in
# the supplied string.

sub _percent_decode($)
{
	my( $str ) = @_;
#	$str =~ s/%([0-9a-fA-F]{2})/ pack( "c",hex( $1 ) ) /ge;
	$str =~ s/%([0-9a-fA-F]{2})/ pack( "U",hex( $1 ) ) /ge;
	return $str;
}

#===============================================================================
# P U B L I C   F U N C T I O N S
#===============================================================================

#-------------------------------------------------------------------------------
# Take the credentials for a user and check that they match; if so, return
# a new session key for this user that can be stored in the cookie.
# If there is a problem, return a bogus session key.

sub authen_cred($$\@)
{
	my ($row, $result, $g, $staff_flag) = ();
		   
	###### these are the membertype that are not allowed to login
	my $noLogins = 't c d';

	# our basic member query
	my $qry = q|
		SELECT CASE
					WHEN password IS NOT NULL AND password > '' THEN password::TEXT
					ELSE oid::TEXT
				END AS password,
				alias AS username,
				m.id,
				COALESCE(firstname, '') AS firstname,
				COALESCE(lastname, '') AS lastname,
				emailaddress,
				membertype,
				spid
		FROM members m|;

	my( $self, $r, @credentials ) = @_;

###### @credentials currently has a maximum of 5 parameters
###### 0 username
###### 1 password
###### 2 merchant affiliate/location ID
###### 3 affiliate/location distinguisher
###### 4 special flag that will allow a login for a Merchant Affiliate even though he is not activated
 
#	my $auth_name = ($r) ? ( $r->auth_name || '' ) : 'Generic';
	my $auth_name = $r->auth_name || '';

	my $auth_type = $r->auth_type || '';
	my $debug = $r->dir_config("AuthCookieDebug") || 0;

	# Username goes in credential_0
	my $user = $credentials[ 0 ];
#	unless ( $user =~ /^.+$/ ) 
	unless ( $user ) 
	{
		$r->log_reason( "AuthCustom: no username supplied for auth realm $auth_name", $r->uri ) if $debug > 0;
		return 'ERROR! No ID Supplied';
	}

	# Password goes in credential_1
	my $password = $credentials[ 1 ];

	# create $temp for error messages.
	my $temp = $password;
    
#	unless ( $password =~ /^.+$/ ) 
	unless ( $password ) 
	{
		$r->log_reason( "AuthCustom: no password supplied for auth realm $auth_name", $r->uri ) if $debug > 0;
		return 'ERROR! No Password Supplied';
	}

	# get the configuration information.
	my %c = _dbi_config_vars $r;

	# Look up user in database.
	my $dbh = DB_Connect::DB_Connect('GLOBAL_LOGIN');
	$dbh->{RaiseError} = 1;

#	my $dbh = DBI->connect( $c{ DBI_DSN }, $c{ DBI_user }, $c{ DBI_password } );
	unless ( defined $dbh ) 
	{
		$r->log_reason( "AuthCustom: couldn't connect to database for auth realm $auth_name", $r->uri );
		return 'ERROR! Internal Server Error (111).  Please contact us immediately so we can fix this problem.';
		#return 'bad';
	}

###### we'll branch here if we are doing a MAF login
   if ($auth_name eq 'maf')
   {
	###### for MAF authentications, an ID (either master or location) will be needed as well as a username and password
	###### we won't be able to distinguish between a master or location ID by looks, we have to specify what we are looking for in another param
	my $id = $credentials[2];
	unless ( $id ) 
	{
		$r->log_reason( "AuthCustom: no MAF ID supplied for auth realm $auth_name", $r->uri ) if $debug > 0;
		$dbh->disconnect;
		return 'ERROR! No ID Supplied';
	}

	###### this should be a numeric only value
	if ( $id =~ /\D/ )
	{
		$r->log_reason( "AuthCustom: alphanumeric Merchant Master or Location ID submitted", $r->uri ) if $debug > 0;
		$dbh->disconnect;
		return 'ERROR! The Master or Location ID is a strictly numeric value.';
	}

	my $maf_type = $credentials[3];
	unless ( $maf_type ) 
	{
		$r->log_reason( "AuthCustom: no MAF type supplied for auth realm $auth_name", $r->uri ) if $debug > 0;
		$dbh->disconnect;
		return 'ERROR! No MAF login type supplied';
	}
	
	if ($maf_type eq 'mal')
	{	###### login for a Merchant Affiliate Store Location
		$result = $dbh->prepare("
			SELECT ma.password,
					ma.username,
					ma.id,
					ma.location_name AS firstname,
					ma.id AS lastname,
					'' AS emailaddress,
					'$maf_type'::TEXT AS membertype,
					''::TEXT AS spid,
					v.status,
					''::TEXT AS member_id
			FROM merchant_affiliate_locations ma
			LEFT JOIN vendors v
				ON ma.vendor_id=v.vendor_id
			WHERE ma.id= ?");
		$result->execute($id);
		$row = $result->fetchrow_hashref;
		$result->finish;
		if ( ! $row )
		{
	#		$r->log_reason("AuthCustom authentication failed for MAF Location ID: $id", $r->uri);
			$dbh->disconnect;

			return 'ERROR! No match for that Location ID.';
		}

		if ($row->{'username'} ne $user)
		{
	#		$r->log_reason("AuthCustom authentication failed for MAF Location user: $user", $r->uri);
			$dbh->disconnect;

			return 'ERROR! No match for that username.';
		}

		if (! defined $row->{status} || (defined $row->{status} && $row->{status} eq ''))
		{
			$dbh->disconnect;

			return 'ERROR! There is no active vendor associated with this location.';
		}
		elsif ( $row->{status} ne '1')
		{
			$dbh->disconnect;

			return 'ERROR! This location is not <i>active</i>.';
		}
	}
	else
	{	###### we'll do our Merchant Affiliate Master login here
		$result = $dbh->prepare("
			SELECT password,
					username,
					id,
					business_name AS firstname,
					''::TEXT AS lastname,
					''::TEXT AS emailaddress,
					'$maf_type'::TEXT AS membertype,
					''::TEXT AS spid,
					status,
					member_id
			FROM merchant_affiliates_master
			WHERE id= ?");
		$result->execute($id);
		$row = $result->fetchrow_hashref;
		$result->finish;
		if ( ! $row )
		{
	#		$r->log_reason("AuthCustom authentication failed for MAF Master ID: $id", $r->uri);
			$dbh->disconnect;

			return 'ERROR! No match for that Merchant Affiliate ID.';
		}

		if ($row->{'username'} ne $user)
		{
	#		$r->log_reason("AuthCustom authentication failed for MAF Master user: $user", $r->uri);
			$dbh->disconnect;

			return 'ERROR! No match for that username.';
		}

		if (! defined $row->{status} || (defined $row->{status} && $row->{status} eq ''))
		{
			###### we'll receive a special parameter to allow a 'Merchant login' during the application process
			###### in order to allow them to use the login protected location application
			unless ($credentials[4])
			{
				$dbh->disconnect;

				return 'ERROR! This Merchant account has not yet been activated.';
			}
		}
#		elsif ($row[8] ne '1')
#               elsif ($row->{status} !~ /1|2/)
               elsif ($row->{status} < 1)

		{
			$dbh->disconnect;

			return 'ERROR! This Merchant account is not <i>active</i>.';
		}

		###### now we'll pull in all the locations under this MA
		$result = $dbh->prepare("
			SELECT id
			FROM merchant_affiliate_locations
			WHERE merchant_id= ?");
		$result->execute($id);
		while ($_ = $result->fetchrow_array)
		{
			$row->{'lastname'} .= "$_,";
		}
		chop $row->{'lastname'};
		###### get our 'ma' membership
		$row->{'ma_membership'} = $dbh->selectrow_hashref("$qry WHERE id= $row->{'member_id'}") if $row->{'member_id'};
	}
   }
   else
   {
###### in order to make the login a simple two field approach, we'll try a total of three different queries
###### to determine if we have a valid ID

###### the first will be the ordinary ID/Password combo
	my $uc_user = uc $user;
	my $cmd = "$qry WHERE alias = ?";

#$r->log_reason( "my DB query: $cmd" );
	$result = $dbh->prepare($cmd);
	$result->execute($uc_user);
	$row = $result->fetchrow_hashref;
	$result->finish;

	if (! $row){
###### this one is our staff matcher
# postgres stores MD5 passwords with an 'md5' prefix
		$cmd = "
			SELECT SUBSTR(passwd,4) AS password,
					usename AS username,
					'01'::TEXT AS id,
					'staff' AS firstname,
					'staff' AS lastname,
					''::TEXT AS emailaddress,
					'staff'::TEXT AS membertype,
					''::TEXT AS spid
			FROM staff_vw
			WHERE usename = ?";
				
#$r->log_reason( "my DB query: $cmd" );
		$result = $dbh->prepare($cmd);
		$result->execute($user);
		$row = $result->fetchrow_hashref;
		$result->finish;
		$staff_flag = 1 if $row;
	}

	# debug line.
#	$r->log_reason( "AuthCustom:  results from database query: row = @row for user $user for auth realm $auth_name", $r->uri ) if $debug > 2;

	#unless ( defined $crypted_password ) 
	if ( ! $row )
	{
#		$r->log_reason("AuthCustom authentication failed for user $user and password $password", $r->uri);
		$dbh->disconnect;

		return 'ERROR! No match for that ID.';
	}
   }	###### end of regular member authentication
	   
#	my $crypted_password = $row->{'password'};
#	my $activeuser = $row->{'username'};
# 	my $a = $row[2];	###### ID (numeric)
# 	my $b = $row[3];	###### firstname
# 	my $c = $row[4];	###### lastname
# 	my $d = $row[5];	###### emailaddress
# 	my $e = $row[6];	###### membertype (the code)
# 	my $f = $row[7];	###### spid


###### we have to go through the password check since we don't want error messages coming up for valid
###### memberships for which an invalid password has been submitted
###### that could be a breach of privacy
###### do our password encryption if we have it
# our staff logins are MD5 on the password out of the DB
	if ( lc $c{ DBI_crypttype } eq 'none' && ! $staff_flag ) 
	{
	###### if the passwords are not equal, they may be md5 so we won't bail yet
		if ( $password ne $row->{'password'} ) 
		{
			$r->log_reason( "AuthCustom: plaintext passwords didn't match for user $user, password = $password, crypted_password = $row->{'password'} for auth realm $auth_name", $r->uri );
			$dbh->disconnect;
			return 'ERROR! Password did not match.';
			#return 'bad';
		}
	} 
	elsif ( lc $c{ DBI_crypttype } eq 'crypt' && ! $staff_flag )
	{
		my $salt = substr $row->{'password'}, 0, 2;
		unless ( crypt( $password, $salt ) eq $row->{'password'} ) 
		{
			$r->log_reason( "AuthCustom: crypted passwords didn't match for user $user, password supplied = $temp for auth realm $auth_name", $r->uri );
			$dbh->disconnect;
			return 'ERROR! Password did not match.';
			#return 'bad';
		}
	}
###### we don't have md5 passwords for members so we won't be defining apache to
	elsif ( lc $c{ DBI_crypttype } eq 'md5' )
	{
		unless ( md5_hex( $password ) eq $row->{'password'} ) 
		{
			$r->log_reason( "AuthCustom: MD5 passwords didn't match for user $user for auth realm $auth_name", $r->uri );
			$dbh->disconnect;
			return 'ERROR! Password did not match.';
			#return 'bad';
		}
	}
	elsif ( $staff_flag )
	{
                unless ( md5_hex( $password . $user ) eq $row->{'password'} )
                {
                        $r->log_reason( "AuthCustom: DB MD5 passwords didn't match for user $user for auth realm $auth_name", $r->uri );
                        $dbh->disconnect;
                        return 'ERROR! Password did not match.';
                }
        }

###### password checking complete

###### we'll exclude login for certain membertypes here
	if( $noLogins =~ /$row->{'membertype'}/ ){
 		$result = $dbh->prepare("SELECT label FROM \"Member_Types\" WHERE code= ?");
		$result->execute($row->{'membertype'});
		my $membertype = $result->fetchrow_array;

 		$result = $dbh->prepare("SELECT obj_value FROM cgi_objects WHERE obj_id= 520");
		$result->execute;
		my $msg = $result->fetchrow_array;
		$result->finish;

		$msg =~ s/%%membertype%%/$membertype/;
		$dbh->disconnect;
		return "ERROR! $msg";
	 }

###### now we'll check for for any special handling for potential problem members
###### we could have done a left join in the queries above, but they are very expensive since our members
###### query is on the alias and the join would be on the ID
###### any results returned will resemble 'CustomCode:510' where the numbers will equate to a template obj_id
	if ($auth_name ne 'maf' && $auth_name ne 'mal')
	{
		$result = $dbh->prepare("SELECT special_handling(?)");
		$result->execute($row->{'id'});
 		$g = $result->fetchrow_array || '';
		$result->finish;

###### we should not have a value in $g, if we do it is because of a member problem
		if( $g )
		{
			$dbh->disconnect;
			return "ERROR! $g";
		}

	}

# create a record of our membership login
	if ($auth_name eq 'maf' && $row->{'member_id'})
	{
		$dbh->do("INSERT INTO membership_logins (id,ipaddr) VALUES (?, ?)", undef,
			$row->{'member_id'}, ${\ $r->connection->remote_ip()} );
	}
	elsif ($auth_name eq 'Generic')
	{
		$dbh->do("INSERT INTO membership_logins (id,ipaddr) VALUES (?, ?)", undef,
			$row->{'id'}, ${\ $r->connection->remote_ip()} );
	}		

	$dbh->disconnect if defined $dbh;

###### create all of our other cookies here rather than in AuthCookie
	my @cookies = ();

###### we want to create a 'Generic' cookie for the network membership login for a merchant here as well
	if ($auth_name eq 'maf' && $row->{'ma_membership'})
	{
		push (@cookies, Apache2::Cookie->new($r, '-domain'=> '.clubshop.com', '-name'=>'id', '-value'=>$row->{'ma_membership'}->{'id'}, '-path'=> '/', '-expires'=>'+1y'));
		push (@cookies, Apache2::Cookie->new($r, '-domain'=> '.clubshop.com', '-name'=>'AuthCustom_Generic',
			'-value'=> CreateTicket($r, \%c, $row->{'ma_membership'}, 'Generic'),
			'-path'=> '/'));
	}
	
	if ($auth_name eq 'Generic')
	{
		push (@cookies, Apache2::Cookie->new($r, '-domain'=> '.clubshop.com', '-name'=> "id", '-value'=>$row->{'id'} , '-path'=> '/', '-expires'=>'+1y'));
	}

	foreach (@cookies)
	{
		$_->bake($r);
	}

	return CreateTicket($r, \%c, $row, $auth_name);
}


# Take a session key and check that it is still valid; if so, return the user.
###### this is modified to work under a regular cgi environment as well
###### and will return a user hash as well as the user (ID)
###### we will have to pass our auth_name through a regular variable instead of $r when in
###### regular CGI mode
sub authen_ses_key($$$;$$)
{
###### when called from a regular cgi environment, $self & $r will be empty

	my( $self, $r, $encrypted_session_key, $hash_flag, $auth_name ) = @_;
	my $debug = ($r) ? ( $r->dir_config("AuthCookieDebug") || 0 ) : 0;

	return undef unless $encrypted_session_key;

###### if our authorization passes a user 'ERROR! ' or 'CODE: ' we want to send that through as is
###### since it will fail any real tests below
	return $encrypted_session_key if ( $encrypted_session_key =~ /ERROR! |CODE:/);

	$auth_name ||= ($r) ? $r->auth_name : 'Generic';

	# Get the configuration information.
	my %c = _dbi_config_vars $r;

	# Get the secret key.
	my $secret_key = $SECRET_KEYS{ $auth_name };

	unless ( defined $secret_key ) {
		if ( $r ){
			if ($debug > 0){ $r->log_reason( "AuthCustom: didn't get the secret key from for auth realm $auth_name", $r->uri ) }
		}
		return "AuthCustom: missing secret key";
	#	return undef;
	}
	
	# Decrypt the session key.
	my $session_key = $encrypted_session_key;

	# Check that this looks like an encrypted hex-encoded string.
	unless ( $encrypted_session_key =~ /^[0-9a-fA-F]+$/ ){
		if ( $r ){
			if ($debug > 0){ $r->log_reason( "AuthCustom: encrypted session key doesn't look like it's properly hex-encoded for auth realm $auth_name", $r->uri ) }
		}
		return "ERROR - AuthCustom: encrypted session key doesn't look like it's properly hex-encoded";
	#	return undef;
	}
	# Get the cipher from the cache, or create a new one if the
	# cached cipher hasn't been created, & decrypt the session key.
	my $cipher;
	$cipher = $CIPHERS{ "des:$auth_name" } || Crypt::CBC->new(-key=>$secret_key, -cipher=>'DES' );
	$session_key = $cipher->decrypt_hex( $encrypted_session_key );
	# Break up the session key.
	my( $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $supplied_hash ) = split( /:/, $session_key);


	# Let's check that we got passed sensible values in the cookie.
	unless ( $enc_user =~ /^[a-zA-Z0-9_\%]+$/ ){
		if ( $r ){
			if ($debug > 0){ $r->log_reason( "AuthCustom: bad percent-encoded user recovered from session ticket for auth_realm $auth_name", $r->uri ) }
		}
		warn "AuthCustom.pm: session key: ";
                warn $session_key;	
		return "ERROR - AuthCustom: bad percent-encoded user recovered from session ticket";
	}

	# decode the user
#	my $user = _percent_decode $enc_user;
	my $user = $enc_user;

	unless ( $issue_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		if ( $r && $debug > 0)
		{
			$r->log_reason( "AuthCustom: bad issue time $issue_time recovered from ticket for user $user for auth_realm $auth_name", $r->uri );
		}
		return "ERROR - AuthCustom: bad issue time $issue_time recovered from ticket for user $user";
	}
	unless ( $expire_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		if ( $r && $debug > 0)
		{
			$r->log_reason( "AuthCustom: bad expire time $expire_time recovered from ticket for user $user for auth_realm $auth_name", $r->uri );
		}
		return "ERROR - AuthCustom: bad expire time $expire_time recovered from ticket for user $user";
	}
	unless ( $supplied_hash && $supplied_hash =~ /^[0-9a-fA-F]{32}$/ )	
	{
		###### initialize the value if NULL
		$supplied_hash ||= 'UNDEFINED';
		
		if ( $r && $debug > 0)
		{
			$r->log_reason( "AuthCustom: bad hash $supplied_hash recovered from ticket for user $user for auth_realm $auth_name", $r->uri );
		}
		return "ERROR - AuthCustom: bad hash $supplied_hash recovered from ticket for user $user";
	}

	# Calculate the hash of the user, issue time, expire_time and
	# the secret key and then the hash of that and the secret key again.
	my $hash = md5_hex( join ':', $secret_key, md5_hex(
      join ':', $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $secret_key
	) );

	# Compare it to the hash they gave us.
	unless ( $hash eq $supplied_hash ){
		if ( $r ){
			if ($debug > 0){ $r->log_reason( "AuthCustom: hash in cookie did not match calculated hash of contents for user $user for auth realm $auth_name", $r->uri ) }
		}
		return "ERROR - AuthCustom: hash in cookie did not match calculated hash of contents for user $user";
	#	return undef;
	}

	# Check that their session hasn't timed out.
	if ( _now_year_month_day_hour_minute_second gt $expire_time ){
		if ( $r ){
			if ($debug > 0){ $r->log_reason( "AuthCustom: expire time $expire_time has passed for user $user for auth realm $auth_name", $r->uri ) }
		}
		return "Your login has expired.<br />Please login again.";
	}

	# If we're being paranoid about timing-out long-lived sessions,
	# check that the issue time + the current (server-set) session lifetime
	# hasn't passed too (in case we issued long-lived session tickets
	# in the past that we want to get rid of). *** DEBUG ***
	# if ( lc $c{ DBI_AlwaysUseCurrentSessionLifetime } eq 'on' ) {

	# check the directory to see if user has correct permissions here.
 	#$auth_name = $r->auth_name;

   # Get the configuration information
   %c = _dbi_config_vars $r;

#debug
#if ($r){
#$r->log_reason("e=$e");
#foreach (keys %c){ $r->log_reason( "$_: $c{$_}" ) }
#}
	unless (grep $e eq $_, split( / /, $c{REQ_mtype}))
#    if ( $c{REQ_mtype} !~ /$e/ )
#    if ( $c{REQ_mtype} ne $e )
    {
		if ( $r )
		{
       		if ($debug > 0){ $r->log_reason( "AuthCustom required mtype= $c{REQ_mtype} but this user is: $e, for auth realm $auth_name", $r->uri) }
		}
       	return "Based on your membertype, this resource is not available.";
    }

	# They must be okay, so return the user.
	$r->subprocess_env('TICKET', $user) if $r;
	
	$activeuser = _percent_decode( $activeuser );
	$b = _percent_decode( $b );
	$c = _percent_decode( $c );
	$d = _percent_decode( $d );
	if ($hash_flag)
	{
	###### this will pass back a hash ref to some core values that we hav carried in our cookie
	###### in addition to the user ID
		if ( $auth_name eq 'Generic' )
		{
			return ($user, {
					id=>$a,
					alias=>$activeuser,
					firstname=>$b,
					lastname=>$c,
					emailaddress=>$d,
					membertype=>$e,
					spid=>$f});
		}
		elsif ( $auth_name eq 'maf')
		{
			return ($user, {
					id=>$a,
					username=>$activeuser,
					mafname=>$b,
					maflocations=>$c,
				#	emailaddress=>$d,
					membertype=>$e,
					})
		}
	}
	else
	{
		return $user;
	}
}

sub CreateTicket
{
	# this routine may be called outside of a mod_perl environment, in that case both $r and $c may come in undef/empty
	my $r = shift;
	my $c = shift;
	my ($row, $auth_name) = @_;
#	print STDERR "auth_name: $auth_name ... row: $row\n";
	unless ($c)
	{
		my %h = _dbi_config_vars(undef);
		$c = \%h;
	}
	# Create the expire time for the ticket.
	my $expire_time;
	# expire time in a zillion years if it's forever.
	if ( lc $c->{ DBI_sessionlifetime } eq 'forever' )
	{
		$expire_time = '9999-01-01-01-01-01';
	}
	else
	{
		my ( $deltaday, $deltahour, $deltaminute, $deltasecond ) = split( /-/, $c->{ DBI_sessionlifetime });
		# Figure out the expire time.
		$expire_time = sprintf(
			'%04d-%02d-%02d-%02d-%02d-%02d',
			Add_Delta_DHMS( Today_and_Now,
			$deltaday, $deltahour,
			$deltaminute, $deltasecond )
		);
	}

	# Now we need to %-encode non-alphanumberics in the username so we
	# can stick it in the cookie safely. 
#	my $enc_user = _percent_encode $row->{'username'};

	# OK, now we stick the username and the current time and the expire
	# time together to make the public part of the session key:
	my $current_time = _now_year_month_day_hour_minute_second;

#warn "fn flag: " . Encode::is_utf8($row->{'firstname'});

	my $public_part = "$row->{'id'}:$current_time:$expire_time:";
	$public_part .= _percent_encode( $row->{'username'} || $row->{'alias'} );
	$public_part .= ":$row->{'id'}:";
	$public_part .= _percent_encode( $row->{'firstname'} ) . ":";
   	$public_part .= _percent_encode( $row->{'lastname'} ) . ":";
   	$public_part .= _percent_encode( $row->{'emailaddress'} );
   	$public_part .= ":$row->{'membertype'}:$row->{spid}:$row->{status}";

#$r->log_reason( "public part: $public_part" ); 

	# Now we calculate the hash of this and the secret key and then
	# calculate the hash of *that* and the secret key again.
	my $secret_key = $SECRET_KEYS{ $auth_name };

	unless ( defined $secret_key ) 
	{
		$r->log_reason( "AuthCustom: didn't have the secret key for auth realm $auth_name", $r->uri );
		return 'ERROR! Internal Server Error (333).  Please contact us immediately so we can fix this problem.';
		#return 'bad';
	}
#warn "public part: $public_part\n";
#warn "public_part flag: " . Encode::is_utf8($public_part);
#warn "turning flag off\n";
Encode::_utf8_off($public_part) if Encode::is_utf8($public_part);
#warn "public part: $public_part\n";
#warn "public_part flag: " . Encode::is_utf8($public_part);

	my $hash = md5_hex( join ':', $secret_key, md5_hex(
		join ':', $public_part, $secret_key
	) );

	# Now we add this hash to the end of the public part.
	my $session_key = "$public_part:$hash";

#$r->log_reason( "enc type: $c{ DBI_encryptiontype }");

	# Now we encrypt this and return it.
	my $encrypted_session_key;
#print STDERR "enctype: $c->{ DBI_encryptiontype }\n";
	if ( $c->{ DBI_encryptiontype } eq 'none' ) 
	{
		$encrypted_session_key = $session_key;
	} 
	elsif ( lc $c->{ DBI_encryptiontype } eq 'des' ) 
	{
		my $cipher = $CIPHERS{ "des:$auth_name" } || Crypt::CBC->new( '-key'=>$secret_key, '-cipher'=>'DES' );
		$encrypted_session_key = $cipher->encrypt_hex( $session_key );
	} 
	return $encrypted_session_key;
}

#-------------------------------------------------------------------------------
# Take a list of groups and make sure that the current remote user is a member
# of one of them.

#sub group($$\@)
#{
#	my( $self, $r, @groups ) = @_;
#
#	my $auth_name = $r->auth_name;
#
#	# Get the configuration information.
#	my %c = _dbi_config_vars $r;
#
#	my $user = $r->connection->user;
#
#$r->log_reason('checking group');
#return OK;
#
#	# Now loop through all the groups to see if we're a member of any:
## 	my $result = $dbh->prepare( <<"EOS" );
## SELECT $c{ DBI_groupuserfield }
## FROM $c{ DBI_groupstable }
## WHERE $c{ DBI_groupfield } = ?
## AND $c{ DBI_groupuserfield } = ?
## EOS
## 	foreach my $group ( @groups ) {
## 		$result->execute( $group, $user );
## 		return OK if ( $result->fetchrow_array );
## 	}
## 	$r->log_reason( "Apache::AuthCookieDBIRadius: user $user was not a member of any of the required groups @groups for auth realm $auth_name", $r->uri );
#	return FORBIDDEN;
#}

1;

__END__

=head1 NAME

   Apache::AuthCookieDBIRadius - An AuthCookie module backed by a DBI database, and an optional Radius server.

=head1 SYNOPSIS

   # In httpd.conf or .htaccess

	############################################
	#     AuthCookie                           #
	#                                          #
	# PortalDBI_CryptType                      #
	# PortalDBI_GroupsTable                    #
	# PortalDBI_GroupField                     #
	# PortalDBI_GroupUserField                 #
	# PortalDBI_EncryptionType none|crypt|md5  #
	# PortalDBI_a on|off                       #
	# PortalDBI_b on|off                       #
	# PortalDBI_c on|off                       #
	# PortalDBI_d on|off                       #
	# PortalDBI_e on|off                       #
	# PortalDBI_f on|off                       #
	# PortalDBI_g on|off                       #
	# PortalDBI_useracct on|off                #
	# PortalDBI_log_field last_access          #
	# PortalDBI_Radius_host none               #
	# PortalDBI_Radius_port 1645               #
	# PortalDBI_Radius_secret none             #
	# PortalDBI_Radius_timeout 45              #
	# AuthCookieDebug 0,1,2,3                  #
	# PortalDomain .yourdomain.com             #
	#                                          #
	############################################

	# key line must come first
	PerlSetVar PortalDBI_SecretKeyFile /usr/local/apache/conf/site.key

	PerlModule Apache::AuthCookieDBIRadius
	PerlSetVar PortalPath /
	PerlSetVar PortalLoginScript /login.pl
	PerlSetVar AuthCookieDebug 1
	PerlSetVar PortalDBI_DSN 'dbi:Pg:host=localhost port=5432 dbname=mydatabase'
	PerlSetVar PortalDBI_User "database_user"
	PerlSetVar PortalDBI_Password "database_password"
	PerlSetVar PortalDBI_UsersTable "users"
	PerlSetVar PortalDBI_UserField "userid"
	PerlSetVar PortalDBI_PasswordField "password"
	PerlSetVar PortalDBI_SessionLifeTime 00-24-00-00

	<FilesMatch "\.pl">
 	 AuthType Apache::AuthCookieDBIRadius
 	 AuthName Portal
 	 SetHandler perl-script
 	 PerlHandler Apache::Registry
 	 Options +ExecCGI
	</FilesMatch>

	# login.pl
	<Files LOGIN>
 	 AuthType Apache::AuthCookieDBIRadius
 	 AuthName Portal
 	 SetHandler perl-script
 	 PerlHandler Apache::AuthCookieDBIRadius->login
	</Files>

	#######################################
	#                                     #
	# Begin websites                      #
	#                                     #
	#######################################

	# private
	<Directory /home/httpd/html/private>
 	 AuthType Apache::AuthCookieDBIRadius
 	 AuthName Portal
 	 PerlSetVar PortalDBI_b on
 	 PerlAuthenHandler Apache::AuthCookieDBIRadius->authenticate
 	 PerlAuthzHandler Apache::AuthCookieDBIRadius->authorize
 	 require valid-user
	</Directory>

	# calendar
	<Directory /home/httpd/html/calendar>
 	 AuthType Apache::AuthCookieDBIRadius
 	 AuthName Portal
  	 PerlSetVar PortalDBI_a on
 	 PerlAuthenHandler Apache::AuthCookieDBIRadius->authenticate
 	 PerlAuthzHandler Apache::AuthCookieDBIRadius->authorize
 	 require valid-user
	</Directory>


=head1 DESCRIPTION

This module is an authentication handler that uses the basic mechanism provided
by Apache::AuthCookie with a DBI database for ticket-based protection.  It
is based on two tokens being provided, a username and password, which can
be any strings (there are no illegal characters for either).  The username is
used to set the remote user as if Basic Authentication was used.

On an attempt to access a protected location without a valid cookie being
provided, the module prints an HTML login form (produced by a CGI or any
other handler; this can be a static file if you want to always send people
to the same entry page when they log in).  This login form has fields for
username and password.  On submitting it, the username and password are looked
up in the DBI database.  The supplied password is checked against the password
in the database; the password in the database can be plaintext, or a crypt()
or md5_hex() checksum of the password.  If this succeeds, the user is issued
a ticket.  This ticket contains the username, an issue time, an expire time,
and an MD5 checksum of those and a secret key for the server.  It can
optionally be encrypted before returning it to the client in the cookie;
encryption is only useful for preventing the client from seeing the expire
time.  If you wish to protect passwords in transport, use an SSL-encrypted
connection.  The ticket is given in a cookie that the browser stores.

After a login the user is redirected to the location they originally wished
to view (or to a fixed page if the login "script" was really a static file).

On this access and any subsequent attempt to access a protected document, the
browser returns the ticket to the server.  The server unencrypts it if
encrypted tickets are enabled, then extracts the username, issue time, expire
time and checksum.  A new checksum is calculated of the username, issue time,
expire time and the secret key again; if it agrees with the checksum that
the client supplied, we know that the data has not been tampered with.  We
next check that the expire time has not passed.  If not, the ticket is still
good, so we set the username.

Authorization checks then check that any "require valid-user" or "require
user jacob" settings are passed.  Finally, if a "require group foo" directive
was given, the module will look up the username in a groups database and
check that the user is a member of one of the groups listed.  If all these
checks pass, the document requested is displayed.

If a ticket has expired or is otherwise invalid it is cleared in the browser
and the login form is shown again.

=head1 APACHE CONFIGURATION DIRECTIVES

All configuration directives for this module are passed in PerlSetVars.  These
PerlSetVars must begin with the AuthName that you are describing, so if your
AuthName is PrivateBankingSystem they will look like:

	PerlSetVar PrivateBankingSystemDBI_DSN "DBI:mysql:database=banking"

See also L<Apache::Authcookie> for the directives required for any kind
of Apache::AuthCookie-based authentication system.

In the following descriptions, replace "WhatEver" with your particular
AuthName.  The available configuration directives are as follows:

=over 4

=item C<WhatEverDBI_DSN>

Specifies the DSN for DBI for the database you wish to connect to retrieve
user information.  This is required and has no default value.

=item C<WhatEverDBI_User>

The user to log into the database as.  This is not required and
defaults to undef.

=item C<WhatEverDBI_Password>

The password to use to access the database.  This is not required
and defaults to undef.

=item C<WhatEverDBI_UsersTable>

The table that user names and passwords are stored in.  This is not
required and defaults to 'users'.

=item C<WhatEverDBI_UserField>

The field in the above table that has the user name.  This is not
required and defaults to 'user'.

=item C<WhatEverDBI_PasswordField>

The field in the above table that has the password.  This is not
required and defaults to 'password'.

=item C<WhatEverDBI_CryptType>

What kind of hashing is used on the password field in the database.  This can
be 'none', 'crypt', or 'md5'.  This is not required and defaults to 'none'.

=item C<WhatEverDBI_GroupsTable>

The table that has the user / group information.  This is not required and
defaults to 'groups'.

=item C<WhatEverDBI_GroupField>

The field in the above table that has the group name.  This is not required
and defaults to 'grp' (to prevent conflicts with the SQL reserved word 'group').

=item C<WhatEverDBI_GroupUserField>

The field in the above table that has the user name.  This is not required
and defaults to 'user'.

=item C<WhatEverDBI_SecretKeyFile>

The file that contains the secret key (on the first line of the file).  This
is required and has no default value.  This key should be owned and only
readable by root.  It is read at server startup time.
The key should be long and fairly random.  If you want, you
can change it and restart the server, (maybe daily), which will invalidate
all prior-issued tickets.

=item C<WhatEverDBI_EncryptionType>

What kind of encryption to use to prevent the user from looking at the fields
in the ticket we give them.  This is almost completely useless, so don't
switch it on unless you really know you need it.  It does not provide any
protection of the password in transport; use SSL for that.  It can be 'none',
'des', 'idea', 'blowfish', or 'blowfish_pp'.

This is not required and defaults to 'none'.

=item C<WhatEverDBI_SessionLifetime>

How long tickets are good for after being issued.  Note that presently
Apache::AuthCookie does not set a client-side expire time, which means that
most clients will only keep the cookie until the user quits the browser.
However, if you wish to force people to log in again sooner than that, set
this value.  This can be 'forever' or a life time specified as:

	DD-hh-mm-ss -- Days, hours, minute and seconds to live.

This is not required and defaults to '00-24-00-00' or 24 hours.

=back

=head1 COPYRIGHT

Copyright (C) 2000 SF Interactive, Inc.  All rights reserved.

=head1 LICENSE

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of

ERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=head1 AUTHOR

Author: Charles Day <BarracodE@s1te.com>
Original Author: Jacob Davies <jacob@sfinteractive.com> <jacob@well.com>


=head1 SEE ALSO

Apache::AuthCookie(1)
Apache::AuthCookieDBI(1)

=cut



















