package Apache::EbizAccess;

use strict;
use Apache::Constants qw(:common);
use Apache::Cookie;
use Apache::Request;
use G_S;

our $ebiz_root = '/vip/ebusiness';
our $access_handler = '/cgi-bin/ebzaxs/';

sub handler
{
	my $r = Apache::Request->new(shift);

	$r->uri =~ m#$ebiz_root/(.*?)(/.*)#;
	###### we shouldn't be called unless there is something after our ebiz_root
	###### but just in case
	unless ($1)
	{
		return OK;
	}

	my %ck = Apache::Cookie->new($r)->parse;
	unless ($ck{"EBIZ_$1"})
	{
		$r->internal_redirect($access_handler . $1 . $2);
		return DONE;
	}
	
																		
#return OK;
#$r->print('EBIZ_$1: ' . $ck{"EBIZ_$1"}->value . "\n");
#$r->print ("decoding returned: " . G_S::Ebiz_Decode_Cookie($ck{"EBIZ_$1"}->value, 'id') . "\n");
	my $rv = G_S::Ebiz_Decode_Cookie($ck{"EBIZ_$1"}->value, 'id');

	###### if it isn't we'll be doing the redirect and we still need to return OK
	unless ($rv)
	{
		$r->internal_redirect($access_handler . $1 . $2);
		return DONE;
	}
	return OK;
}

1;

__END__

