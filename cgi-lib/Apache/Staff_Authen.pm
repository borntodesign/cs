package Staff_Authen;

use strict;
use Apache2::Const qw(FORBIDDEN);
use parent qw( Apache2::AuthCookie );

use lib '/home/httpd/cgi-lib';
use cs_admin;

sub authen_ses_key($$$)
{
	###### when called from a regular cgi environment, $self may & $r will be empty
	my( $self, $r, $encrypted_session_key ) = @_;

	unless ($encrypted_session_key)
	{
		return FORBIDDEN;
	}
	
	# that function dies in all cases except success when it passes back a hashref containing username/password key/values
	# unless that flag is passed on the end, in which case it will pass back undef
	my $rv = cs_admin::authenticate($encrypted_session_key, 1);

	return $rv ? $rv : FORBIDDEN;
}

1;