package CS_Authen;

use strict;
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );
use URI::Escape qw(uri_escape uri_unescape);
use Apache2::Const qw(:common REDIRECT);
use Apache2::Request;
use Apache2::Util;
use Digest::MD5 qw( md5_hex );
use parent qw( Apache2::AuthCookie );

use lib '/home/httpd/cgi-lib';
use CS_Authen_Constants qw/$secret_key cipher $LoginScript/;

# Take a session key and check that it is still valid; if so, return the user.
###### this is modified to work under a regular cgi environment as well
###### and will return a user hash as well as the user (ID)
###### we will have to pass our auth_name through a regular variable instead of $r when in
###### regular CGI mode
sub authen_ses_key($$$)
{
	###### when called from a regular cgi environment, $self may & $r will be empty
	my( $self, $r, $encrypted_session_key ) = @_;

	unless ($encrypted_session_key)
	{
		return $r ? FORBIDDEN : undef;
	}
	
	# Decrypt the session key.
	my $session_key = $encrypted_session_key;

	# Check that this looks like an encrypted hex-encoded string.
	unless ( $encrypted_session_key =~ /^[0-9a-fA-F]+$/ )
	{
		my $msg = "encrypted session key doesn't look like it's properly hex-encoded: ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		debug($r, 1, ($msg . $r->uri ));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}

	$session_key = cipher->decrypt_hex( $encrypted_session_key );
	
	# Break up the session key.
	my( $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $supplied_hash ) = split( /:/, $session_key);

	# Let's check that we got passed sensible values in the cookie.
	unless ( $enc_user =~ /^[a-zA-Z0-9_\%]+$/ )
	{
		my $msg = "bad percent-encoded user recovered from session ticket: $enc_user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}

	my $user = $enc_user;

	unless ( $issue_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		my $msg = "bad issue time $issue_time recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}
	unless ( $expire_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		my $msg = "bad expire time $expire_time recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}
	unless ( $supplied_hash && $supplied_hash =~ /^[0-9a-fA-F]{32}$/ )	
	{
		###### initialize the value if NULL
		$supplied_hash ||= 'UNDEFINED';
		my $msg = "bad hash $supplied_hash recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>"bad hash $supplied_hash recovered from ticket for user $user"});
	}

	# Calculate the hash of the user, issue time, expire_time and
	# the secret key and then the hash of that and the secret key again.
	my $hash = md5_hex( join ':', $secret_key, md5_hex(
      join ':', $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $secret_key
	) );

	# Compare it to the hash they gave us.
	unless ( $hash eq $supplied_hash )
	{
		my $msg = "hash in cookie did not match calculated hash of contents for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}

	# Check that their session hasn't timed out.
	if ( _now_year_month_day_hour_minute_second() gt $expire_time )
	{
#		my $msg = "expire time $expire_time has passed for user $user : ";
		my $msg = "Your login has expired";
		return (undef, {'err'=>2, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri ));
		return (FORBIDDEN, {'err'=>2, 'errstr'=>$msg});
	}

	$activeuser = uri_unescape( $activeuser );
	$b = uri_unescape( $b );
	$c = uri_unescape( $c );
	$d = uri_unescape( $d );

	my $user_hashref = {
				'id'=>$a,
				'alias'=>$activeuser,
				'firstname'=>$b,
				'lastname'=>$c,
				'emailaddress'=>$d,
				'membertype'=>$e,
				'spid'=>$f
				};
	# $g is simply an empty set of {} at this time (06/10)
	# if we decide to stuff some extra bits of inforation into the cookie, we can serialize into those braces somehow
	# and not have to worry about breaking the cookie structure
	
	if ($r && (($r->dir_config("CS_Authen_Debug") || 0) >= 3))
	{
		foreach (keys %{$user_hashref})
		{
			#	debug($r, 3, "$_ : $user_hashref->{$_}");	### this ends up boogering up UTF-8 characters
			warn "$_ : $user_hashref->{$_}\n";
		}
	}

# set the hashref in pnotes for later authorization if necessary
	$r->pnotes('user_hashref', $user_hashref) if $r;

# if being called in a mod_perl environment
# when called in the mod_perl environment, and we have been successful at validating the cookie, we only want to send the user
# AuthCookie recogizes 2 return arguments as an error condition
	return $r ? $user : ($user, $user_hashref);
}

# this authorize method was taken directly from AuthCookie and then tweaked slightly
# so that custom "requires" would be handled by our custom_errors as well as authentication errors
sub authorize ($$)
{
  my ($auth_type, $r) = @_;

  debug($r, 3, $r->uri);
  return OK unless $r->is_initial_req; #only the first internal request
  
  if ($r->auth_type ne $auth_type)
  {
	debug($r, 3, ($auth_type . " auth type is " . $r->auth_type));
    return DECLINED;
  }
  
  my $reqs_arr = $r->requires or return DECLINED;
  
#  my $user = $r->connection->user;
  my $user = $r->user;
  unless ($user)
  {
    debug($r, 1, ("No user authenticated: " . $r->uri));
    return FORBIDDEN;
  }
  
  my $satisfy = $auth_type->get_satisfy($r);
  return SERVER_ERROR unless $auth_type->satisfy_is_valid($r, $satisfy);
  my $satisfy_all = $satisfy eq 'all';
  
  my ($forbidden);
  foreach my $req (@$reqs_arr)
  {
    my ($requirement, $args) = split( /\s+/, $req->{'requirement'}, 2 );
    $args = '' unless defined $args;
    debug($r, 2, "$requirement $args");
    
    if ( lc($requirement) eq 'valid-user' ) {
      if ($satisfy_all) {
        next;
      } else {
        return OK;
      }
    }

    if($requirement eq 'user')
    {
      if ($args =~ m/\b$user\b/)
      {
        next if $satisfy_all;
        return OK; # satisfy any
      }
     
      $forbidden = 1;
      next;
    }

    # Call a custom method
    my $ret_val = $auth_type->$requirement($r, $args);
    debug($r, 3, "$auth_type->$requirement returned $ret_val");
    if ($ret_val == OK)
    {
      next if $satisfy_all;
      return OK; # satisfy any
    }
    else
    {
    	return $ret_val;
    }

    # Nothing succeeded, deny access to this user.
    $forbidden = 1;
  }

  return $forbidden ? FORBIDDEN : OK;
}

# create a "destination" parameter value
sub CreateDestination($$)
{
	my ($self, $r) = @_;
	my $apr = Apache2::Request->new($r);
	my $rv = '';
	my @pairs =();
	
	foreach my $param ($apr->param)
	{
		my @vals = $apr->param($param);
		foreach my $val (@vals)
		{
			push @pairs, $param . '=' . $val;
		}
	}
	$rv = '?' . (join ';', @pairs) if scalar(@pairs) > 0;
	$rv = Apache2::Util::escape_path($r->uri . $rv, $r->pool);
	debug($r, 3, "destination param: $rv");
	return $rv;
}

# custom_errors
# AuthCookie's authenticate will call this method if it receives more than one return value from authen_ses_key
# in that case, @args will contain something like this:
# ('ERROR', {'err'=>0, 'errstr'=>"hash in cookie did not match calculated hash of contents for user $user"})
# an err code of -0- would refer to an error which we don't intend to translate
# other values can be used on the other end for identifying specific errors and providing customized handling
#
# custom_errors can also be called from our authorize method which overrides AuthCookie's method
# this is only because we want to provide custom error handling instead of simply returning to the login screen
# in that case, @args will contain the error code from the authorization method
sub custom_errors
{
	my ($self, $r, @args) = @_;
	debug($r, 3, "Error: $args[0]");
	my $url = $LoginScript . '?';
	my $qs = '';
	if ($args[0])
	{
		my $hr = $args[1] || {};
		foreach (keys %{$args[1]})
		{
#			$qs .= escape_uri($_) . '=' . escape_uri( $hr->{$_} ) . ';';
			$qs .= uri_escape($_) . '=' . uri_escape( $hr->{$_} ) . ';';
		}
	}
	$url .= $qs . 'destination=' . $self->CreateDestination($r);
	debug($r, 3, "redirect URL: $url");

	$r->custom_response($args[0], $url);
	return $args[0];
}

# just a method to override AuthCookie's version since we want to actually use custom_errors
sub login_form($$)
{
	my $self = shift;
	my $r = shift;
	return $self->custom_errors($r, FORBIDDEN);
}

# check to see if we are in the specified membertype list
# this will be invoke by AuthCookie when a require directive is setup in httpd.conf
# require membertype s m v
sub membertype($$$)
{
	my ($self, $r, $args) = @_;
	unless ($args)
	{
		$r->custom_response(SERVER_ERROR, "No required member types specified");
		return SERVER_ERROR;
	}
	my @mtypes = split(/ /, $args);
	my $user_hashref = $r->pnotes('user_hashref');
	return OK if grep $_ eq $user_hashref->{'membertype'}, @mtypes;
	debug($r, 3, "Membertype $user_hashref->{'membertype'} not in list: $args");
	return $self->custom_errors($r, AUTH_REQUIRED, {'err'=>1, 'errstr'=>"Membertype $user_hashref->{'membertype'} not in list: $args"});
}

#-------------------------------------------------------------------------------
# _now_year_month_day_hour_minute_second -- Return a string with the time in
# this order separated by dashes.
sub _now_year_month_day_hour_minute_second()
{
	return sprintf '%04d-%02d-%02d-%02d-%02d-%02d', Today_and_Now;
}

sub debug($$$)
{
	my ($r, $log_level, $msg) = @_;
	return unless $r;
	$r->log_error( (caller(1))[3] . " : $msg") if ($r->dir_config("CS_Authen_Debug") || 0) >= $log_level;
}

1;