package CS_Merchant_Authen;

use strict;
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );
use URI::Escape qw(uri_unescape);
use Apache2::Const qw(:common REDIRECT);
use Apache2::Request;
use Apache2::Util;
use Digest::MD5 qw( md5_hex );

use lib qw(/home/clubshop.com/cgi-lib);
use base qw( CS_Authen );
use CS_Authen_Constants qw/$secret_key cipher $LoginScript/;

# Take a session key and check that it is still valid; if so, return the user.
###### this is modified to work under a regular cgi environment as well
###### and will return a user hash as well as the user (ID)
###### we will have to pass our auth_name through a regular variable instead of $r when in
###### regular CGI mode
sub authen_ses_key($$$)
{
	###### when called from a regular cgi environment, $self may & $r will be empty
	my( $self, $r, $encrypted_session_key ) = @_;
	
	unless ($encrypted_session_key){
		return $r ? FORBIDDEN : undef;
	}
	
	my $auth_name ||= ($r) ? $r->auth_name : 'General';
	
	# Decrypt the session key.
	my $session_key = $encrypted_session_key;

	# Check that this looks like an encrypted hex-encoded string.
	unless ( $encrypted_session_key =~ /^[0-9a-fA-F]+$/ )
	{
		my $msg = "encrypted session key doesn't look like it's properly hex-encoded: ";
		if ($r)
		{
			debug($r, 1, ($msg . $r->uri ));
			return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
		}
		
		return (undef, {'err'=>0, 'errstr'=>$msg});
	}

	$session_key = cipher->decrypt_hex( $encrypted_session_key );
	
	# Break up the session key.
	my( $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $supplied_hash ) = split( /:/, $session_key);

	# Let's check that we got passed sensible values in the cookie.
	unless ( $enc_user =~ /^[a-zA-Z0-9_\%]+$/ )
	{
		my $msg = "bad percent-encoded user recovered from session ticket: $enc_user : ";
		if ($r)
		{
			debug($r, 1, ($msg . $r->uri));
			return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
		}
		
		return (undef, {'err'=>0, 'errstr'=>$msg});
	}

	my $user = $enc_user;

	unless ( $issue_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		my $msg = "bad issue time $issue_time recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}
	unless ( $expire_time =~ /^\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/ )
	{
		my $msg = "bad expire time $expire_time recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}
	unless ( $supplied_hash && $supplied_hash =~ /^[0-9a-fA-F]{32}$/ )	
	{
		###### initialize the value if NULL
		$supplied_hash ||= 'UNDEFINED';
		my $msg = "bad hash $supplied_hash recovered from ticket for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}

	# Calculate the hash of the user, issue time, expire_time and
	# the secret key and then the hash of that and the secret key again.
	my $hash = md5_hex( join ':', $secret_key, md5_hex(
      join ':', $enc_user, $issue_time, $expire_time, $activeuser, $a, $b, $c, $d, $e, $f, $g, $secret_key
	) );

	# Compare it to the hash they gave us.
	unless ( $hash eq $supplied_hash )
	{
		my $msg = "hash in cookie did not match calculated hash of contents for user $user : ";
		return (undef, {'err'=>0, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri));
		return (FORBIDDEN, {'err'=>0, 'errstr'=>$msg});
	}

	# Check that their session hasn't timed out.
	if ( CS_Authen::_now_year_month_day_hour_minute_second() gt $expire_time )
	{
		my $msg = "Your login has expired: expire time $expire_time has passed for user $user : ";
		return (undef, {'err'=>2, 'errstr'=>$msg}) unless $r;
		
		debug($r, 1, ($msg . $r->uri ));
		return (FORBIDDEN, {'err'=>2, 'errstr'=>$msg});
	}

	$activeuser = uri_unescape( $activeuser );
	$b = uri_unescape( $b );
	$c = uri_unescape( $c );
	$d = uri_unescape( $d );

# this format is defined in csmLogin.pm in the Create_CS_Authen_maf_Ticket() routine

	my $user_hashref = {
				'id'=>$a,
				'username'=>$activeuser,
				'mafname'=>$b,
				'maflocations'=>$c,
				'emailaddress'=>$d,
				'membertype'=>$e
				};
	# $g is simply an empty set of {} at this time (06/10)
	# if we decide to stuff some extra bits of inforation into the cookie, we can serialize into those braces somehow
	# and not have to worry about breaking the cookie structure
	
	if ($r && (($r->dir_config("CS_Authen_Debug") || 0) >= 3))
	{
		foreach (keys %{$user_hashref})
		{
			#	debug($r, 3, "$_ : $user_hashref->{$_}");	### this ends up boogering up UTF-8 characters
			warn "$_ : $user_hashref->{$_}\n";
		}
	}

# set the hashref in pnotes for later authorization if necessary
	if ($r)
	{
		$r->pnotes('user_hashref', $user_hashref) if $r;

# if being called in a mod_perl environment
# when called in the mod_perl environment, and we have been successful at validating the cookie, we only want to send the user
# AuthCookie recogizes 2 return arguments as an error condition
		return $user;
	}
	
	return ($user, $user_hashref);
}

1;

__END__

01/29/15	Revised the return values of authen_ses_key to return a hashref as the second value in all cases instead of just when called in a mod-perl environment
