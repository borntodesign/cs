package Clubshop::Session;
use strict;
require Crypt::CBC;
use JSON::XS;
use G_S qw($SALT);

=head1 Session:

    simple storing and retrieving of statefulness

=head3 New

=head4 Required Arguments

    A hash with these keys:
        db: A database handle
        app_id: the application ID
        expiry: the date the session data expires
    
=cut

sub new
{
    my ($class, %args) = @_;
    return bless \%args, $class;
}

sub CryptString
{
###### Encrypt/decrypt a text string.
    my ($self, $action, $value) = @_;
    
    my $cipher = Crypt::CBC->new({ 'key'=>$SALT, 'cipher'=>'DES'} );

	if ( $action eq 'encrypt' )
	{
		$value = $cipher->encrypt_hex($value);
	}
	elsif ( $action eq 'decrypt' )
	{
		$value = $cipher->decrypt_hex($value);
	}

	return $value;
}

=head3 RetrieveState

    Retrieve a session based upon an app_id and an encrypted key

=head4 Arguments

    parameter 'pk' value
    parameter 'pkc' value
    Boolean: true will decode the json data package / null/false will return the raw value

=head4 Returns

    With a true argument, whatever sort of data structure was originally serialized in.
    Otherwise, the exact value that was stored.
    
=cut

sub RetrieveState
{
    my $self = shift;
    my $pk_param = shift;
    my $pkc = shift;
    my $decode = shift;
    
	my $data = ();
	
	if ($pk_param && $pkc)
	{
    	# Find the primary key of the process state record using the encrypted value, if one exists.
	    my $pk = $self->CryptString('decrypt', $pkc);

	    # If the crypted key matches the unencrypted key that was passed.
	    if ( $pk && $pk eq $pk_param )
	    {
	    	# Get the params xml block using the provided pk value.
	    	$data = $self->{'db'}->selectrow_array("
	    		SELECT data
	    		FROM 	process_state
	    		WHERE 	pk = $pk
	    		AND 	application = $self->{'app_id'}");
	    }
    }
	return ($decode && $data) ? decode_json($data) : $data;
}

=head3 SaveState

    parameter 'pk' value
    parameter 'pkc' value
    Save whatever session information you want

=head4 Arguments

    The data structure you want to retrieve later.

=cut

sub SaveState
{
    my $self = shift;
    my $pk_param = shift;
    my $pkc = shift;
    my $data = shift;

#		my %local_params = %{$self->{'params'}};
#		_utf8_off(\%local_params);	# disabling this 06/05/15 to try and fix some bad encoding corruption that has begun to occur since the DBD::Pg upgrade
		$data = encode_json($data);

	# Find the primary key of the process state record, if one exists.
	my $pk;
	$pk = $self->CryptString('decrypt', $pkc) if $pkc;

	# If the crypted key matches the unencrypted key that was passed.
	if ( $pk && $pk_param && $pk == $pk_param )
	{
	# Update the process state record.
		die "Failed to update process state\n" unless $self->{'db'}->do("
			UPDATE process_state
			SET application = $self->{'app_id'},
				data = ?,
				expires = NOW() + ?::INTERVAL
			WHERE pk = ?", undef, $data, $self->{'expiry'}, $pk);
	}
	else
	{
		$pk = $self->{'db'}->selectrow_array("SELECT NEXTVAL('process_state_pk_seq');");
	# Insert a new process state record.
		die "Failed to preserve process state\n" unless $self->{'db'}->do("
			INSERT INTO process_state (pk, application, data, expires)
			VALUES (?,  ?, ?, NOW() + ?::INTERVAL)",
		undef, $pk, $self->{'app_id'}, $data, $self->{'expiry'});
	}
	
	$pkc ||= $self->CryptString('encrypt', $pk);
	return $pk, $pkc;
}

1;

