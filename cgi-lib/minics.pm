package minidee;
###### minidee for MiniDetail
###### the intent at this point is to maintain some basic personal data in a cookie
###### to avoid doing DB lookups on every page hit
###### created: 03/07/11	G.Baker
###### last modified: 03/08/11	G.Baker

use strict;
use CGI::Cookie;
use lib qw(/home/httpd/cgi-lib);
use DB_Connect;
use ClubCash;

our @FIELDS = qw/id spid alias firstname lastname emailaddress reward_points country membertype top_picks/;

sub combine_fields
{
	my $data = shift;
	return join '|', (map {$data->{$_} || ''} @FIELDS);
}

sub create_cookie
{
	###### this routine returns a cookie ready to set into headers
	###### we overload it to accept an ID or a recordset in a hashref
	my $arg = shift || return ();
	###### if my arg is a ref, then I should have all I need in the hashref
	my $data = (ref $arg ? combine_fields($arg) : create_cookie_value($arg)) || return ();
	return new CGI::Cookie(
		-name    =>  'minics',
		-value   =>  $data,
		-expires =>  '+1h',
		-domain  =>  '.clubshop.com',
		-path    =>  '/',
		-secure  =>  0
	);
}

sub create_cookie_value
{
	###### this routine returns a cookie ready to set into headers
	###### we overload it to accept an ID or a recordset in a hashref
	my $arg = shift || return ();
	my $data = (ref $arg ? $arg : get_data($arg)) || return ();
	return combine_fields($data);
}

sub get_data
{
	###### must have something to do something
	my $id = shift || return ();

	###### we will do the typical search on ID or Alias even though I expect
	###### to only receive numeric IDs
	###### no ticky no washy, no DB no cookie
	my $db = DB_Connect::DB_Connect('generic', undef, {fail_silently=>1}) || return ();
	$db->{RaiseError} = 1;
	my $rv = $db->selectrow_hashref("
		SELECT m.id, m.spid, m.alias, m.firstname, m.lastname, m.membertype,
			COALESCE(m.emailaddress, '') AS emailaddress,
			COALESCE(m.country, '') AS country
		FROM members m
		WHERE ${\($id =~ /\D/ ? 'm.alias' : 'm.id')} = ?", undef, $id) || return ();
        my $trd = ClubCash::Get_ClubCash_Amounts($rv->{id},'generic');
        my $tmpt = sprintf("%.2f %s",$trd->{redeem},$trd->{currency});
        $rv->{reward_points} = $tmpt;
 	$rv->{top_picks} = $db->selectrow_array("
 		SELECT DISTINCT('1') AS top_picks
 		FROM member_shopping_preferences AS msp
 	        JOIN mall_vendor_extensions_links AS mvel
                     ON (mvel.incentive_mallcat_id = msp.incentive_mallcat_id AND
                         mvel.category=msp.category)
 	        JOIN mall_vendor_extensions AS mve
		     ON (mve.pk = mvel.pk)
		JOIN malls AS mall
		     ON (mall.mall_id = mve.mall_id AND
			 mall.country_code = '$rv->{country}')
 		WHERE
                     mve.exttype IN (3,4,5) AND
                     NOW() between mve.start_date and mve.end_date AND
                     msp.member_id = $rv->{id}") || 0;

	$db->disconnect;
	return $rv;
}

sub parse
{
	###### I'm expecting a cookie value of the correct format
	###### I will return a hashref
	my $ck = shift || return ();
	my %h=();
	my @vals =();
	###### wrap in evals to silence uninit'd var warnings
	eval (@vals = split /\|/, $ck);
	eval (q!for(my $x=0; $x < scalar @FIELDS; $x++){$h{$FIELDS[$x]} = $vals[$x] || ''}!);
	return \%h;
}

1;

# 03/07/11 Created from minidee for use with new clubshop pages
