package XML_Editor;
######
###### An application to allow parties to edit xml documents. It requires two parameters:
######		1.	'edit' - To build out an editing interface using the supplied object.
######		2.	A hashref of the Translation Edit Queue item including
######			the xml object used to build out the html editing interface
######			and the name of the file (so we can locate the english version).
######		- or -
######		1. 	'update' - To parse the xml document from the supplied object.
######		2.	An html string from which we can parse the finished xml document. 
######
###### created: 03/04/05	Karl Kohrt
######
###### last modified: 05/16/12 Bill MacArthur
	
use lib ('/home/httpd/cgi-lib');
use strict;
use Carp qw/croak/;
use CGI;
use G_S;
require XML::Simple;
require XML::local_utils;
use Data::Dumper;	# testing only

###### Globals
our $q = new CGI;
our $body = '';
our $english_xml = '';
our %XSL = (
	'rewrite'	=> '10403',
	'editor'	=> '10401',
	'paths'	=> '10460',
	'recover'	=> '10461' );
our $xs = new XML::Simple('NoAttr'=>1);
our $db = '';
our $action = ''; 
our $object = ''; 
our $language_list = '';

sub XML_Edit($$$)
{	
	( $db, $action, $object ) = @_;

	# A reg. ex. list of active translation languages(2-letter codes).
	my $sth = $db->prepare("
		SELECT code2
		FROM 	language_codes 
		WHERE 	active = TRUE");
	$sth->execute();
	$language_list = $sth->fetchrow_array;
	while ( my $lang = $sth->fetchrow_array ) { $language_list .= "|$lang" }
	$sth->finish;

	# Initial error handling here.
	if ( $action !~ /^update$|^edit$/ ) { croak("The action '$action' is not allowed in package XML_Editor.pm.") }
	unless ( $object ) { croak("An empty or non-existent object was passed into XML_Editor.pm.") }

	# Get the English version of the XML document.
	$english_xml = Get_English();
#die Dumper(XML::Simple::XMLin($english_xml));
	if ($action eq 'edit')
	{
		# Build out the editor page.
		$body = Build_Editor($object->{'resource_value'}, $english_xml);
	}
	elsif ($action eq 'update')
	{
		# Parse the xml translation data out of the editor params.
		$body = Parse_XML_Object($object);
	}
	return $body;
}

###### Display the editor.		
sub Build_Editor
{
	(my $obj, my $eng) = @_;

	# Remove the header and footer info from the xml file if any exists.
	# We can simply delete it since we've already captured it in translation_editor.cgi.
	$obj =~ s/^.*?(<lang_blocks>)/$1/sg;
	$obj =~ s/(<\/lang_blocks>).*?$/$1/sg;

	# Now let's combine the two separate xml documents into one.
	my $new_xml = qq#<base><english>$eng</english><trans>$obj</trans></base>#;
#warn "blended XML: $new_xml";
	# Get the xsl rewrite stylesheet.
	my $xsl = G_S::Get_Object($db, $XSL{'rewrite'}) || die "Couldn't open $XSL{'rewrite'}";

	# Rewrite the xml data to add tracking attributes to the tags.
	$new_xml = XML::local_utils::xslt($xsl, $new_xml);
#warn "\nrewritten XML: $new_xml";
	# Get the xsl editor stylesheet.
	$xsl = G_S::Get_Object($db, $XSL{'editor'}) || die "Couldn't open $XSL{editor}";

	# Create the actual xml editor form.
	my $edit_form = XML::local_utils::xslt($xsl, $new_xml);

	# Pass the editor form back to the editor script.
	return $edit_form;
}

# Get the English version of the XML document.
sub Get_English
{
	my $filename = '';
	my $base_file = '';
	my $lang_ext =  '';
	my $xml =  '';

	# If this is a file.
	if ( $object->{'resource_identifier'} =~ m/\.xml/ )
	{
		$filename = $object->{'resource_identifier'};
		$base_file = '';

		# Check for the language extension on the filename.
		$lang_ext = substr($filename, -2);

		# If in the allowed language list, remove the extension to get the base filename.
		if ( $lang_ext =~ m/$language_list/ )
		{
			$base_file = substr($filename, 0, -3);
		}
		else
		{
			croak("The file you are trying to edit ($base_file.$lang_ext) is not in your approved language.\n");
		}

		# Put the file's contents into a variable.
		$/ = undef;					
		$filename = $base_file;
		unless ( open(DATA, $filename) )
		{
			$filename = $base_file . ".en";
			unless ( open(DATA, $filename) )
			{
				$filename = $base_file . ".xml";
				unless ( open(DATA, $filename) )
				{
					croak("The file you are trying to edit ($base_file.$lang_ext) does not exist in English.\n")
				}
			}
		}
		while ( <DATA> ) { $xml = $_ }
		close DATA;
	}
	# Otherwise, this is an object translation from the DB.
	else
	{
		# If the ID is negative, this is a new object and the ID is the English version.
		if ( $object->{resource_identifier} < 0 )
		{
			$xml = $db->selectrow_array("SELECT value
								FROM object_translations
								WHERE pk
									= $object->{resource_identifier}*-1;");
		}
		# Otherwise, this is an existing, translated object, 
		#  so find the corresponding English version.
		else
		{
			$xml = $db->selectrow_array("SELECT o2.value
								FROM object_translations o1
								INNER JOIN object_translations o2
									ON o1.obj_key = o2.obj_key
								WHERE o1.pk = $object->{resource_identifier}
								AND o2.language = 'en';");
		}
	}
	# Remove the header and footer info from the xml file if any exists.
	# We can simply delete it since we've already captured it in translation_editor.cgi.
	$xml =~ s/^.*?(<lang_blocks>)/$1/sg;
	$xml =~ s/(<\/lang_blocks>).*?$/$1/sg;

	return $xml;
}

# Parse the original translation object out of the editor page.
sub Parse_XML_Object
{
	my $param_hash = shift;

	# turn our English document into a hashref
	# ForceContent ensures that root nodes in the hashref will end up really being root nodes when the hashref is processed by XMLout later
	# it also avoids having to determine if we have attributes or not as all attributes will have their own key as will the content
	# the ForceArray is necessary to explicitly identify a node instead of an attribute to XMLout
	# without it text nodes down in depth often end up as attribute nodes of the parent...
	# that makes it more complex to work with the resultant reference, but at least we get consistent output later on
	my $lang_blocks = XML::Simple::XMLin($english_xml, 'ForceContent'=>1, 'ForceArray'=>1);

	foreach my $key ( keys %{$param_hash} )
	{
		# All our params that matter have lang_blocks in the name.
		if ( $key =~ m/lang_blocks\// )
		{
			# Remove the lang_blocks/ translation identifier and build the hash variable.
			(my $tag = $key) =~ s/lang_blocks\///g;
			
			# This creates a string of the form $lang_blocks{'key'}->[0]->{'next key'}, with the key values
			#  quoted to allow for special characters in the eval.
			$tag =~ s#\/#'}->[0]->{'#g;

			# Assign the value to the hash variable.
			eval("\$lang_blocks->{'$tag'}->[0]->{'content'} = \$param_hash->{'$key'}");
			die $@ if $@;
			
			# we do not need to carry the "new" attributes
			eval("delete \$lang_blocks->{'$tag'}->[0]->{'new'}");
			die $@ if $@;
		}
	}

	# Turn the hash into xml
	my $main_xml = XML::Simple::XMLout($lang_blocks, 'RootName' => 'lang_blocks' );

#	# Combine the newly formed translation with the English document
#	$main_xml = "<base><english>$english_xml</english><trans>$main_xml</trans></base>";
#
#	# Get the path attribute re-writer stylesheet.
#	my $xsl = G_S::Get_Object($db, $XSL{'paths'}) || die "Couldn't open $XSL{'paths'}";
#
#	# Re-write the combined doc with pathnames as attributes throughout.
#	$main_xml= XML::local_utils::xslt($xsl, $main_xml);
#print STDERR "mail_xml after path recovery:\n$main_xml";
#
#	# Get the attribute recovery stylesheet.
#	$xsl = G_S::Get_Object($db, $XSL{'recover'}) || die "Couldn't open $XSL{'recover'}";
#
#	# Insert the English doc's attributes into the translation,
#	#  leaving only the translation nodes in the final document.
#	$main_xml= XML::local_utils::xslt($xsl, $main_xml);
#print STDERR "mail_xml after attribute recovery:\n$main_xml";
#
#	# Remove the header and footer info from the xml file if any exists.
#	# We can simply delete it since we've already captured it in translation_editor.cgi.
#	$main_xml =~ s/^.*?(<lang_blocks>)/$1/sg;
#	$main_xml =~ s/(<\/lang_blocks>).*?$/$1/sg;

#	not sure if this still needs to be done now that we are not doing all the XSL processing
#	$main_xml = CGI::unescapeHTML($main_xml);

	return $main_xml;
}

1;

###### 06/13/05 Added single quotes around the hash key names in the Parse_XML_Object subroutine
###### 	prior to the eval statement.
###### 07/07/05 Added attribute re-writer and recovrye code to the Parse_XML_Object sub.
###### 05/16/12 a radical change from the way Parse_XML_Object works... departing from all the XSL conversions
###### to simply working with the document in memory starting with the English and replacing the actual content.
######