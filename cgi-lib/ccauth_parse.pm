package ccauth_parse;
###### a routine to provide extended error reporting and help upon failed CC authorizations
###### the common textual errors are translatable and additional instructions are provided
###### released 12/05	Bill MacArthur

use lib ('/home/httpd/cgi-lib');
require XML::Simple;
require XML::local_utils;

sub Parse($)
{
	my @res = split /,/, $_[0], 8;
	###### map out what the elements are returned
	return {
		resp_code		=> $res[0],
		resp_subcode		=> $res[1],
		resp_reason_code	=> $res[2],
		resp_reason_text	=> $res[3],
		approval_code		=> $res[4],
		avs_result_code	=> $res[5],
		transaction_id	=> $res[6]
	};
}

sub Report($)
{
	my $hr = shift;
	return '' unless $hr;
	if (! ref $hr){ $hr = Parse($hr) }
	elsif(ref $hr ne 'HASH'){ die "Argument must be a scalar or hashref\n" }

	$/ = undef;	###### allow us to suck in entire files in a single bound :)
	open (INP, '/home/httpd/cgi-templates/xsl/auth_errors.xsl') ||
		die "Failed to open auth_errors.xsl\n";
	my $xsl = <INP>;
	close INP;
	open (INP, '/home/httpd/cgi-templates/xml/auth_errors.xml') ||
		die "Failed to open auth_errors.xml\n";
	my $xml = <INP>;
	close INP;

	my $xs = new XML::Simple(NoAttr=>1);
	$xml = '<base>' . $xml . $xs->XMLout($hr) . '</base>';
	my $rv = XML::local_utils::xslt($xsl, $xml);
	return wantarray ? ($rv, $hr) : $rv;
}

1;


