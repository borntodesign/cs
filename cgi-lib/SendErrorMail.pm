package SendErrorMail;

=head1 SendErrorEmail

	A Class for sending trappable script errors and sending them to a standardized email box
	
=cut

=head2 SendMail
	
	use lib '/usr/local/scripts'; # or wherever you keep this module
	use SendErrorMail;
	....
	SendErrorEmail::SendMail(<error message>, <subject>);
	
	That's it.
	
=cut

use strict;
use Mail::Sendmail;
use FindBin qw/$RealBin $RealScript/;

my $emailaddr = 'system-alerts@dhs-club.com';	# where the mail is going
my $hostname = qx/hostname/;
chomp $hostname;
my $subject_prefix = "$RealBin/$RealScript on $hostname :";	#what will be prepended to our subject argument

sub SendMail
{
	my $msg = shift || '';
	my $subject = shift || '';
	sendmail(
		To			=> $emailaddr,
		From		=> qq|"$RealScript" <$<\@$hostname>|,
		Subject		=> "$subject_prefix $subject",
		Message		=> $msg,
		'Content-Type'	=> 'text/plain; charset="utf-8"'
	) || print $Mail::Sendmail::error;
}

1;