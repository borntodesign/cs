package Coop;
use strict;

=head1
NAME: Coop

	Coop is an Abstraction class that should use "Coop::Factory->new(WHATEVER_INTERFACE_YOU_WANT_TO_USE, $db)".
	This holds most of the standard routines for importing, and purchasing Co-op Members.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

	eval{
		my $Coop = Coop::Factory->new(WHATEVER_INTERFACE_YOU_WANT_TO_USE, $db);
	};
	if($@)
	{
		How ever you want to handle your fatal error.
	}

=head1
PREREQUISITS:

	Members, Text::CSV::Slurp

=cut

use Members;
use Text::CSV::Slurp;
use MailTools;

=head1
DESCRIPTION

=head2
CONSTRUCTOR

	The constructor calles an '__init' that is only available
	in the interfaces associated with this class.

=head3
new

=head4
Params:

	db obj to a database connection

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $this			= {};
    
    $this->{db} = shift;
    die 'The db Parameter is required to instanciate the Coop Class. ' if ! defined $this->{db};
    
    bless($this, $class);

    $this->__init();
    return $this;
}


=head2 METHODS

=head3
PRIVATE

=cut


=head3
_setMembers

=head4
Description:

	Instanciates the "Members" subclass, if it is not already instanciated

=cut

sub _setMembers
{
	my $this = shift;

	eval{
		$this->{Members} = Members->new($this->{db}) if ! exists $this->{Members};
	};
	if($@)
	{
		die 'There was an issue instanciating Members. Error: ' . $@;
	}
	
}


=head3
_setMembers

=head4
Description:

	Instanciates the "Text::CSV::Slurp" subclass, if it is not already instanciated.

=head4
TODO:

	This should be an abstract factory method for those instances when we start
	using XML instead of CSVs.

=cut

sub _setTextCSVSlurp
{
	my $this = shift;
	
	eval{
		$this->{Text_CSV_Slurp} = Text::CSV::Slurp->new() if ! exists $this->{Text_CSV_Slurp};
	};
	if($@)
	{
		die 'There was an issue instanciating Members. Error: ' . $@;
	}
	
}

=head2
PUBLIC

=head3
getOriginalHeaderMapping

=head4
Description:

	This parses the '_csv_mapping' hash, 
	and returns an arrayref of the headers
	that should be used with the CSV file
	that is being passed in.

=head4
Parameters:

	None

=head4
Return:

	array

=cut

sub getOriginalHeaderMapping
{
	my $this = shift;
	
	my $original_headers = [];
	foreach my $hashref (@{$this->{_csv_mapping}})
	{
		my @value = values %{$hashref};		
		push @$original_headers, $value[0];
		
	}
	
	return $original_headers;
	
	
}

=head3
getVersion

=head4
Description:

	Returns the version of the class.

=cut

sub getVersion
{
	my $this = shift;
	
	return $this->{_version};
}

=head3
getName

=head4
Description:

	Returns the name of the class.

=cut

sub getName
{
	my $this = shift;
	
	return $this->{_name};
}

=head3
getDbFields

=head4
Description:

	Returns the name of the fields 
	in the database you will be 
	inserting into.

=cut

sub getDbFields
{
	my $this = shift;
	
	my $original_headers = [];
	foreach my $hashref (@{$this->{_csv_mapping}})
	{
		my @keys = keys %{$hashref};		
		push @$original_headers, $keys[0];
		
	}
	
	return $original_headers;
}


=head3
getImportCoops

=head4
Description:

	Returns the an array of hashrefs that contain the 
	interfaces to use when calling "Coops::Factory(INTEFACE_NAME, $db)"

=cut

sub getImportCoops
{
	my $this = shift;
	my @return = (
					{provider_name=>'Default', provider_id=>'Default'},
					{provider_name=>'MediaWiz', provider_id=>'MediaWiz'},
		                        {provider_name=>'MediaWizUS', provider_id=>'MediaWizUS'},
					{provider_name=>'Egos', provider_id=>'Egos'},
				);
	return @return;
	
}


=head3
createMemberships

=head4
Description:

	This loops through an arrayref of hashrefs containing the potential members,
	by this point all fields should be correctly formatted, and ready to create members.

=head4
Parameters:

	potential_members_arrayref_hashref	arrayref of hashrefs	The list of potential users, and their registration information
	invalid_potential_members_arrayref_hashref	referance to an array	The members who could not be created are added here.

=head4
Return:

	int	Number of members created.
=cut

sub createMemberships
{

	my $this = shift;
	
	my $potential_members_arrayref_hashref = shift;

	my $invalid_potential_members_arrayref_hashref = shift;
	


	my $number_of_members_created = 0;
	
	$this->_setMembers() if ! exists $this->{Members};
	
	foreach my $potential_member_hashref (@$potential_members_arrayref_hashref)
	{
		eval{
			
			$potential_member_hashref->{notification_type} = 102;
			$potential_member_hashref->{notification_memo} = 'NEW_COOP_MEMBER';
			
			my $return_value = $this->{Members}->createNewMember($potential_member_hashref);
			
			undef $potential_member_hashref->{notification_type};
			undef $potential_member_hashref->{notification_memo};
			
			if (ref($return_value) ne 'HASH')
			{
				$potential_member_hashref->{'warn'} = $return_value;
				push @$invalid_potential_members_arrayref_hashref, $potential_member_hashref;
			} else {
				$number_of_members_created++;
			}
		};
		if($@)
		{
			undef $potential_member_hashref->{notification_type};
			undef $potential_member_hashref->{notification_memo};
			
			$potential_member_hashref->{error} = 'An error occured creating this member: ' . $@;
			push @$invalid_potential_members_arrayref_hashref, $potential_member_hashref;
		}
		
	}

	return $number_of_members_created;

}


=head3
getTotalMembersForSale

=head4
Description:

	Get the total number of users for sale in the Co-op.

=head4
Params:

	$donor_accounts		string	CSV of Co-op IDs

=head4
Return:

	int	number of members for sale, or string on error
=cut

sub getTotalMembersForSale
{
	my $this = shift;
	my $donor_accounts = shift;
	my $total_members_for_sale = 0;

	if (! defined $donor_accounts)
	{
		die "The 'donor_accounts' is a required parameter.";
	}
	
	my $query =<<EOT;
	
	SELECT
		COUNT(m.id) AS total_members_for_sale
	FROM
		members m
	WHERE
		m.mail_option_lvl > 0
		AND
		m.emailaddress IS NOT NULL
		AND
		m.emailaddress <> ''
		AND
		m.membertype IN ('s', 'm')
		AND
		m.spid IN ($donor_accounts)
		AND
		m.stamp < NOW() - INTERVAL '$this->{_members_age}'
EOT

	eval{

		my $sth = $this->{db}->prepare($query);
		$sth->execute();
		my $rows = $sth->fetchrow_hashref;

		if($rows)
		{
					
			$total_members_for_sale = $rows->{total_members_for_sale};
		} else {
			die "The total number of members could not be determined. ";
		}

	};
	if ($@)
	{
		return "The total number of members could not be determined. Error: " .  $@;
	}

	return $total_members_for_sale;
}

=head3
getCountriesWhereMembersAreAvailable

=head4
Description:

	Get the countries where members are available.

=head4
Params:

	$donor_accounts		string	CSV of Co-op IDs

=head4
Return:

	array	an array containing the countries where members are available.
=cut

sub getCountriesWhereMembersAreAvailable
{
	my $this = shift;
	my $donor_accounts = shift;
	my @countries = ();

	if (! defined $donor_accounts)
	{
		die "The 'donor_accounts' is a required parameter.";
	}
	
	my $query =<<EOT;
	
	SELECT
		DISTINCT(m.country)
	FROM
		members m
	WHERE
		m.mail_option_lvl > 0
		AND
		m.emailaddress IS NOT NULL
		AND
		m.emailaddress <> ''
		AND
		m.membertype IN ('s', 'm')
		AND
		m.country IS NOT NULL
		AND
		m.country <> ''
		AND
		m.spid IN ($donor_accounts)
		AND
		m.stamp < NOW() - INTERVAL '$this->{_members_age}'
EOT

	eval{
		@countries = map { @$_ } @{ $this->{db}->selectall_arrayref($query) }; 
	};
	if ($@)
	{
		return "The countries where members are available could not be determined. Error: " .  $@;
	}

	

	return @countries;
}


=head3
insertCoopOrder

=head4
Description:

	Insert a new COOP Order into the coop_orders table.

=head4
Params:

	$params	hashref	The hashref keys should be the same 
					as the fields in the "coop_orders" table.
					The id field uses members.id as a "foreign key".

=head4
Return:

	int	Should return 1 on sucess, and null or 0 on failure.

=cut


sub insertCoopOrder
{
	my $this = shift;
	my $params = shift;
	
	my @query_fields = ();
	my @query_place_holders = ();
	my @query_place_holder_values = ();
	my $query = 'INSERT INTO coop_orders ';
	my $return_value = 0;
	
	my %required_cc_fields = (id=>0, quantity=>0, price=>0, pay_type=>0, order_type=>0, acct_holder=>0, addr=>0, city=>0, state=>0, postalcode=>0, country=>0, cc_number=>0, cc_cvv2=>0, cc_exp=>0);
	my %required_pp_fields = (id=>0, quantity=>0, price=>0, pay_type =>0, order_type=>0);
	
	
	
#	return 0 if ! exists $params->{id};
	die "params->{id} is undefined" unless $params->{id};	
	
	foreach my $key (keys %$params)
	{
		next if (! $params->{$key} || ! $key);
		
		push @query_fields, $key;
		push @query_place_holders, '?';
		push @query_place_holder_values, $params->{$key};
		
		$required_cc_fields{$key} = 1 if (exists $required_cc_fields{$key} && $params->{$key});
		$required_pp_fields{$key} = 1 if (exists $required_pp_fields{$key} && $params->{$key});
		
	}
	

	
	
	if ($params->{pay_type} =~ /CC/i)
	{
		foreach my $key (keys %required_cc_fields)
		{
#			return 0 if ! $required_cc_fields{$key};
			die "required CC field: $key is undefined" unless $required_cc_fields{$key};
		}
	} 
	elsif ($params->{pay_type}  =~ /PP/i)
	{
		foreach my $key (keys %required_pp_fields)
		{
#			return 0 if ! $required_pp_fields{$key};
			die "required pay type field: $key is undefined" unless $required_pp_fields{$key};
		}
	} 
	else 
	{
#		return 0;
		die "pay type is neither CC nor PP";
	}
	
	
#	return 0 if ! scalar @query_fields;
	die "query_fields is undefined" unless scalar @query_fields;
	
	
	$query .='(' . join(',' , @query_fields) . ') VALUES (' . join(',' , @query_place_holders) . ')';
	
	eval{
		$return_value = $this->{db}->do($query, undef, @query_place_holder_values);
	};
	if($@)
	{
		#warn "Coop.pm->insertCoopOrder: Query: $query Error: ". $@;

	    my $MailTools = MailTools->new();
	    
	    my $message =<<EOT;
	    
There was an error with Coop.pm->insertCoopOrder

Query: $query 

Error:

EOT

$message .= $@;

	    $MailTools->sendTextEmail({to=>'errors@dhs-club.com', subject=>'Error: Coop.pm', text=>$message});


		return 0;
	}
	
	return $return_value;
}

1;

__END__

=head1
SEE ALSO

L<Coop::Factory>, L<Coop::MediaWiz>, L<Coop::Default>, L<Members>, L<Text::CSV::Slurp>

=head1
CHANGE LOG:

	Date	Who	Description

=cut
