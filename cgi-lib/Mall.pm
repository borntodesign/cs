package Mall;
use strict;
use warnings;

=head1 Mall

	A class that is being created 09/19/12, initially to provide a drop down list of top level mall categories
	
=head3 new

	A standard object instanciator.
	
=head4 Arguments

	An optional hashref can be passed with the following key/value pair that will be used:
	{
		'db'=> a active DB handle -or- a reference to an active DB handle -or- a code reference to a function that will return an active DB handle,
		'CGI'=> a CGI object
	}

	Naturally if a method is called in this class and the necessary resources are not available, like CGI for example, it will fail miserably.
	
=head4 Returns

	A new Mall object
	
=cut

sub new($;$)
{
	my ($class, $args) = @_;
	my %h = ();
	$h{'db'} = $args->{'db'} if $args->{'db'};
	$h{'CGI'} = $args->{'CGI'} if $args->{'CGI'};
	return bless \%h, $class;
}

sub CGI
{
	my $self = shift;
	die "A CGI object is being requested but it apperently has not been setup when new() was called." unless $self->{'CGI'};
	return $self->{'CGI'};
}

=head3 db

	Set/Reset/Obtain the available DB handle.
	
=head4 Arguments

	An optional DB handle.
	If one is provided, the existing one will be replaced with the new one.
	
=head4 Returns

	The available DB handle.
	
=cut

sub db
{
	my ($self, $db) = @_;
	$self->{'db'} = $db if $db;

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

=head3 GetMallCatNames

	Pass in a language preference. Get back a hashref of translated mall categories.

=head4 Arguments

	An optional hashref. The following keys are recognized.
	{
		'language_pref' => A language code,
		'catid' => integer,
		'parent' => integer -OR- * if you really want the whole enchilada
	}
	
	Mote, if there is not a language_pref defined, English 'en' will be the default. If there is not a parent defined, -0- will be the default.
	
=head4 Returns

	A hashref that looks like this: (catid => catname)
	{
		'hash' => {
			1 => 'Libri & Riviste',
			2 => 'Fiori',
			...
		},
		'array' => ['Fiori','Libri & Riviste'],	# note the sort order of these elements
		'keys_sorted' => [2,1]					# note the keys are sorted by the values
	}

=cut

sub GetMallCatNames
{
	my ($self, $args) = @_;
	$args->{'language_pref'} ||= 'en';
	$args->{'parent'} = 0 unless defined $args->{'parent'} || $args->{'catid'};
	
	my $qry = 'SELECT catid, catname FROM mallcats_translated(?) ';
	my @qryArgs = $args->{'language_pref'};
	my $where = 'WHERE';
	
	if (defined $args->{'parent'} && $args->{'parent'} ne '*')
	{
		$qry .= "WHERE parent=? ";
		push @qryArgs, $args->{'parent'};
		$where = 'AND';
	}
	
	if ($args->{'catid'})
	{
		push @qryArgs, $args->{'catid'};
		$qry .= "$where catid=?";
	}
	
	my %rv = ();
	my $sth = $self->db->prepare("$qry ORDER BY catname");
	$sth->execute(@qryArgs);
	while (my ($catid, $catname) = $sth->fetchrow_array)
	{
		$rv{'hash'}->{$catid} = $catname;
		push @{$rv{'array'}}, $catname;
		push @{$rv{'keys_sorted'}}, $catid;
	}
	return \%rv;
}

=head3 selectCtlPrimaryMallCats

	Create an HTML <select> element with the translated primary mall category names as the options and the catids as the values.
	The primary mall categories are those who have a parent of -0-
	
=head3 Arguments

	A language preference
	
	An optional hashref of arguments to define aspects of the select control.
	The hashref key/values will be passed directly to CGI to create the control, so use the same syntax that you would use to define a popup_menu with CGI.
	
	An optional hashref of other arguments that are not passed to CGI. The following key/value pairs are recognized:
	{
		'no_first_elem_empty' => 1,			# setting this to a true value will disable the default behavior to create an option at the top of the list with an empty value
		'first_elem_label' => 'some value'	# this will change the default empty option at the top of the list and set it's label to this value
	}

=head3 Returns

	An HTML <select> element populated with alphabetically sorted options whose values are the catids of the related category.
	
=cut

sub selectCtlPrimaryMallCats
{
	my ($self, $language_pref, $ctlargs, $args) = @_;
	$ctlargs ||= {};
	$args ||= {};
	my $data = $self->GetMallCatNames({'language_pref'=>$language_pref, 'parent'=>0});
	
	# note that by working on these references, if we ever begin to use cached versions of them, we will be affecting the cached version as well
	unless ($args->{'no_first_elem_empty'})
	{
		unshift @{$data->{'keys_sorted'}}, '';
		$data->{'hash'}->{''} = $args->{'first_elem_label'} || '';
	}
	
	return $self->CGI->popup_menu(
		%{$ctlargs},
		'-values' => $data->{'keys_sorted'},
		'-labels' => $data->{'hash'}
	);
}

1;