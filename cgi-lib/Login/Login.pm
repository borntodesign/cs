package Login::Login;

=head1 Login::Login

A base class for use by the login scripts.

=cut

use strict;
use Exporter 'import'; # gives you Exporter's import() method directly
our @EXPORT_OK = qw(%errors_map);
use Template;
use XML::Simple;
use lib '/home/httpd/cgi-lib';
use DB_Connect;
use XML::local_utils;
#use Data::Dumper;

my (@errors, $language_pack, %params, @header_cookies) = ();
my %TMPL = (
	'language_pack_html' 	=> 542,
	'language_pack_xml'		=> 543
);
my @valid_login_form_ids = (545, 546);	# be sure to add any other expected value to this list

# this is a map of error codes that we can expect in here as they map to textual errors in the two language pack templates
our %errors_map = (
	1 => 'na4partnertype',
	2 => 'login_expired',
	3 => 'incorrect_pwd',
	4 => 'nologin',
	5 => 'code5',
	6 => 'code5',
	7 => 'code7',
	8 => 'code7',
	9 => 'must_login',
	10 => 'incorrect_malid',
	11 => 'incorrect_mid',
	12 => 'err_noactive_vendor',
	13 => 'location_inactive',
	14 => 'not_yet_activated',
	15 => 'merchant_inactive'
);

my $TT = Template->new();

sub cgi
{
	my $self = shift;
	return $self->{'cgi'};
}

# this is a private method
sub Connect2DB
{
	my $self = shift;
	
	my $db = DB_Connect('GLOBAL_LOGIN', undef, {'return_redirects'=>1});
	$self->Error("Undefined DB connection failure") unless $db;
	
	###### if DB_Connect was successful, then $db will hold a ref,
	###### otherwise it should hold a textual URL which we will use
	if (ref $db)
	{
		$db->{'RaiseError'} = 1;
		return $db;
	}

	print $self->cgi->redirect($db);
	$self->Exit();
}

sub db
{
	my ($self, $db) = @_;
	$self->{'db'} = $db if $db;
	
	# where we were not initialized with a DB handle of any sort, we will look for one in main ;)
	$self->{'db'} ||= $self->Connect2DB();

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

sub debug
{
	my ($self, $msg) = @_;
	print $self->cgi->header('text/plain'), $msg;
	$self->Exit();
}

sub Display
{
	my $self = shift;
	my $tmplID = shift;
	my $member = shift;
	my $destination = shift || '/';
	
	my $tmpl = $self->{'gs'}->GetObject($tmplID) || die "Failed to load template: $tmplID\n";
	my $rv = ();
	my $data = $self->RollData($member);

	$data->{'destination_calculated'} = $params{'destination'} || $destination;
	$data->{'destination_encoded'} = $self->cgi->escape($data->{'destination_calculated'});
#$self->debug(Dumper($data));
	$TT->process(\$tmpl, $data, \$rv);

	$tmpl = ();
	# our errors or our language pack nodes may contain TT placeholders themselves,
	# these are not parsed on the first pass through
	$TT->process(\$rv, $data, \$tmpl); 

	print $self->cgi->header('-charset'=>'utf-8', '-cookies'=>\@header_cookies), $tmpl;
	$self->Exit();
}

=head2 Error

A basic method of returning an error to the browser.

=head3 Arguments

Whatever text should be returned to the browser.

=head3 Returns

Nothing is returned. In fact exit() will ultimately be called, so do not use this method unless script termination is expected.

=cut

sub Error
{
	my ($self, $error) = @_;
	print $self->cgi->header('-charset'=>'utf-8'),
		$self->cgi->start_html('-encoding'=>'utf-8', '-title'=>'Error'),
		$self->cgi->h4($error),
		$self->cgi->end_html;
	$self->Exit();
}

=head2 errors

Optionally set an error in the @errors list.
Retrieve the list.

=head3 Arguments

Optional: a string that should get pushed on the error stack

=head3 Returns

If called in a scalar contect, returns a reference to @errors or undef if @errors is undef, otherwise @errors is returned as a list.

=cut

sub errors
{
	my ($self, $error) = @_;
	push @errors, $error if defined $error;
	return wantarray ? @errors : (@errors ? \@errors : undef);
}

sub Exit
{
	my $self = shift;
	$self->db->disconnect if $self->db;
	exit;
}

=head2 LoadLanguagePack

Pull in our HTML & XML language packs, merge them and return a hashref to the caller. We will also store the hashref in our local $language_pack

=head3 Arguments

None

=head3 Returns

A hashref of the language pack.

=cut

sub LoadLanguagePack
{
	my $self = shift;
	$language_pack = $self->{'gs'}->GetObject($TMPL{'language_pack_html'}) || die "Failed to load html language pack\n";
	my $html = XML::local_utils->flatXHTMLtoHashref($language_pack) || die "Failed to convert html language pack\n";
	
	$language_pack = $self->{'gs'}->GetObject($TMPL{'language_pack_xml'}) || die "Failed to load xml language pack\n";
    my $xml = XMLin($language_pack, 'NoAttr' => 1);
    
	# now merge the two hashrefs together... we'd better be sure all of our nodes are uniquely named in our docs ;)
	$language_pack = {%{$xml}, %{$html}};
	return $language_pack;
}

=head2 LoadParams

Pull in our parameters and doctor them as necessary. These will be stored locally as %params

=head3 Arguments

None

=head3 Returns

The a reference to the %params list.
Note, this enables you to tweak the parameters from the caller and have the changes be seen in this class.

=cut

sub LoadParams
{
	my $self = shift;
	foreach ($self->cgi->param)
	{
		$params{$_} = $self->{'gs'}->Prepare_UTF8( $self->cgi->escapeHTML($self->cgi->param($_) || '') );
		$params{$_} =~ s/^\s+|\s+$//g;
	}
	
	# validate the formID parameter
	if ($params{'formID'})
	{
		$params{'formID'} = () unless grep {$params{'formID'} eq $_} @valid_login_form_ids;
	}
	
	# initialize these to empty whether we received them or not
	foreach(qw/_action/)
	{
		$params{$_} = '' unless defined $params{$_};
	}
	
	return \%params;
}

sub Logout
{
	my $self = shift;
	# we are simply going to negate the cookies and redirect them to the specified page or a default
	NegateCookies();
	my $dest = $self->cgi->param('destination') || 'http://www.clubshop.com/cs/';
	print $self->cgi->redirect('-location'=>$dest, '-cookies'=>\@header_cookies);
}

sub NegateCookies
{
	my $self = shift;
	# if we have failed a login, we are going to negate any cookies that the user may have had from a previous successful login
	push @header_cookies, $self->cgi->cookie('-name'=>'AuthCustom_Generic', '-value'=>'', 'expires'=>'-1d');
	push @header_cookies, $self->cgi->cookie('-name'=>'AuthCustom_maf', '-value'=>'', 'expires'=>'-1d');
	push @header_cookies, $self->cgi->cookie('-name'=>'id', '-value'=>'', '-expires'=>'-1d');
	push @header_cookies, $self->cgi->cookie('-name'=>'pa', '-value'=>'', '-expires'=>'-1d');
	push @header_cookies, $self->cgi->cookie('-name'=>'minidee', '-value'=>'', '-expires'=>'-1d');
	push @header_cookies, $self->cgi->cookie('-name'=>'PHPSESSID', '-domain'=>'clubshop.com', '-expires'=>'-1d', '-value'=>'');
}

=head3 new

=head4 Arguments

Parameters should be passed in as a hash reference.
A G_S object is required
An active DB handle is required if authentication is to occur against the DB.

example: my $giL = csLogin->new({'db'=>$dbh, 'gs'=>a G_S object});

=cut

sub new
{
	my ($class, $args) = @_;
	$args ||= {};
	return bless $args, $class;
}

sub param
{
	my ($self, $pn) = @_;
	return $params{$pn};
}

sub Redirect
{
	my ($self, $url) = @_;
	$url ||= $params{'destination'} || '/';	# if we have received a URL argument, then we will fallback to the regular parameter

	print $self->cgi->redirect('-cookies'=>\@header_cookies, '-location'=>$url);
	$self->Exit();
}

sub RollData
{
	my ($self, $member) = @_;
	return {
		'errors' => \@errors,
		'params' => \%params,
		'language_pack' => $language_pack,
		'member' => $member,
		'xdestination' => $self->cgi->escape($params{'destination'} || '/'),
		'script_name' => $self->cgi->script_name,
		'xpassword' => $self->cgi->escape($member->{'password'})
	};
}

=head2 setHeaderCookie

Add a cookie to the list and get the list back.
If no argument is passed, the list will still be returned.

=head3 Arguments

A prepared cookie like so: $self->cgi->cookie('-name'=>'minidee', '-value'=>'', '-expires'=>'-1d');

=head3 Returns

A scalar reference to the local @header_cookies

=cut

sub setHeaderCookie
{
	my ($self, $ck) = @_;
	push @header_cookies, $ck if defined $ck;
	return \@header_cookies;
}

=head2 setPnsidCookie

Look up the position.pnsid for the given member ID and set the pnsid cookie.
This is being created to help facilitate the Shili comp plan hack of June 2015 which will require knowing what "group" one is in.

=head3 Arguments

A numeric member ID.

=head3 Returns

Nothing, but the pnsid cookie will be added to the list of @header_cookies if we receive a valid member ID and find a matching record

=cut

sub setPnsidCookie
{
	my ($self, $id) = @_;
	return unless ($id || 'a') !~ m/\D/;	# if we receive an empty id argument the default will cause us to return which is also what we need to do if the id is not plain digits
	my ($pnsid) = $self->db->selectrow_array("SELECT pnsid FROM mviews.positions_freshest WHERE id= $id");
	push (@header_cookies, $self->cgi->cookie('-name'=>'pnsid', '-domain'=>'.clubshop.com', '-expires'=>'+1y', '-value'=>$pnsid)) if $pnsid;
}
1;

__END__

11/25/14	added trimming of whitespace at the ends of the parameters
01/12/15	added $data->{'destination_encoded'} to Display()
03/13/15	added escapeHTML to load params.. I don't think there is any case where we need to allow embedded HTML
05/11/15	Added Prepare_UTF8() to param loading to deal with the recent changes to DBD::Pg
06/11/15	Added setPnsidCookie()