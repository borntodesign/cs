package Login::csmLogin;

use strict;
use CGI;
use Digest::MD5 qw( md5_hex );
use URI::Escape qw( uri_escape );
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );
use CS_Authen_Constants qw/$secret_key cipher $LoginScript/;
use parent qw(Login::csLogin);
#use Data::Dumper;	# for testing only

=head2 csLogin

A class to handle authentication and cookie management for Merchants

=cut

# our basic member query
my $qry = q|
	SELECT CASE
				WHEN password IS NOT NULL AND password > '' THEN password::TEXT
				ELSE oid::TEXT
			END AS password,
			alias AS username,
			m.id,
			COALESCE(firstname, '') AS firstname,
			COALESCE(lastname, '') AS lastname,
			emailaddress,
			membertype,
			spid,
			country
	FROM members m|;

=head3 authenticate($$)

=head4 Arguments

	username (ID/alias)
	password
	merchantid (would be the master merchant ID or a location ID)
	masterbranch (should be either 'maf' or 'mal')
	
=head Returns

	Hash Reference to the merchant record and other profile aspects as to be determined
	If this is a master login, then a key, 'ma_membership', will be created on the hashref
	
	This method also returns an error code such as when authentication fails.
	The caller can handle this situation on it's own, but a standard error code will be available.
	
=cut

sub authenticate($$)
{
	my ($self, $username, $pwd, $mid, $maf_type) = @_;
	my ($row) = ();
	if ($maf_type eq 'mal')
	{	###### login for a Merchant Affiliate Store Location
		my $sth = $self->db->prepare("
			SELECT ma.password,
					ma.username,
					ma.id,
					ma.location_name AS firstname,
			--		ma.id AS location_list,
					ma.id AS maflocations,
					'' AS emailaddress,
					'$maf_type'::TEXT AS membertype,
					''::TEXT AS spid,
					v.status,
					''::TEXT AS member_id
			FROM merchant_affiliate_locations ma
			LEFT JOIN vendors v
				ON ma.vendor_id=v.vendor_id
			WHERE ma.id= ?");
		$sth->execute($mid);
		$row = $sth->fetchrow_hashref;
		$sth->finish;
		if ( ! $row )
		{
			return undef, 10; #'ERROR! No match for that Location ID.';
		}

		if (($row->{'username'} ne $username) || ($row->{'password'} ne $pwd))
		{
			return undef, 3;	#'ERROR! No match for that username.';
		}

		if (! defined $row->{'status'} || (defined $row->{'status'} && $row->{'status'} eq ''))
		{
			return undef, 12;	#'ERROR! There is no active vendor associated with this location.';
		}
		elsif ( $row->{'status'} ne '1')
		{
			return undef, 13;	#'ERROR! This location is not <i>active</i>.';
		}
	}
	else
	{	###### we'll do our Merchant Affiliate Master login here
		my $sth = $self->db->prepare("
			SELECT password,
					username,
					id,
					business_name AS firstname,
					''::TEXT AS location_list,
					''::TEXT AS emailaddress,
					'$maf_type'::TEXT AS membertype,
					''::TEXT AS spid,
					status,
					member_id
			FROM merchant_affiliates_master
			WHERE id= ?");
		$sth->execute($mid);
		$row = $sth->fetchrow_hashref;
		$sth->finish;
		if ( ! $row )
		{
			return undef, 11;	# 'ERROR! No match for that Merchant Affiliate ID.';
		}

		if (($row->{'username'} ne $username) || ($row->{'password'} ne $pwd))
		{
			return undef, 3;
		}

		if (! defined $row->{'status'} || (defined $row->{'status'} && $row->{'status'} eq ''))
		{
#TODO revise to use the parameter submitted by the application process
			###### we'll receive a special parameter to allow a 'Merchant login' during the application process
			###### in order to allow them to use the login protected location application
			unless ($self->param(''))
			{
				return undef, 14;	#'ERROR! This Merchant account has not yet been activated.';
			}
		}
		elsif (! ($row->{'status'} == 1 || $row->{'status'} == 2))
		{
			return undef, 15;	#'ERROR! This Merchant account is not <i>active</i>.';
		}

		###### now we'll pull in all the locations under this MA
		$sth = $self->db->prepare("
			SELECT id
			FROM merchant_affiliate_locations
			WHERE merchant_id= ?");
		$sth->execute($mid);
		while ($_ = $sth->fetchrow_array)
		{
#			$row->{'lastname'} .= "$_,";
			$row->{'maflocations'} .= "$_,";
		}
#		chop $row->{'lastname'};
		chop $row->{'maflocations'};
		
		###### get our 'ma' membership
		$row->{'ma_membership'} = $self->db->selectrow_hashref("$qry WHERE id= $row->{'member_id'}") if $row->{'member_id'};
		
		$row->{'spid'} = $row->{'ma_membership'}->{'spid'};
	}

	my $error = undef;
	unless ($row)
	{
		warn "Login failed for merchant/location id: $mid , pwd: $pwd\n";
		return ($row, 3);
	}
	

	# we will add in our other things here when they are defined
	return ($row, $error); 
}

=head3 Create_CS_Authen_maf_Ticket($)

Take a member hash reference and create a GI_Authen_General cookie using CGI::Cookie

=head4 Arguments

	A hash reference the likes of that returned by authenticate()

=head4 Returns

	A cookie string ready for insertion in an HTTP header

=head4 Note

	This cookie format is digested in CS_Merchant_Authen.pm in authen_ses_key()
	
=cut

sub Create_CS_Authen_maf_Ticket($)
{
	my ($self, $row, $error) = @_;
	return ($row, $error) unless $row && ! $error;

	# Create the expire time for the ticket.
	my $expire_time = sprintf(
			'%04d-%02d-%02d-%02d-%02d-%02d',
			Add_Delta_DHMS( Today_and_Now, '00', '12', '00', '00'));

	# OK, now we stick the username and the current time and the expire
	# time together to make the public part of the session key:
	my $current_time = $self->_now_year_month_day_hour_minute_second();

	my $public_part = "$row->{'id'}:$current_time:$expire_time:";
	$public_part .= uri_escape( $row->{'username'} || $row->{'alias'} );
	$public_part .= ":$row->{'id'}:";
	$public_part .= uri_escape( $row->{'firstname'} ) . ":";
#   	$public_part .= uri_escape( $row->{'location_list'} ) . ":";
   	$public_part .= uri_escape( $row->{'maflocations'} ) . ":";
   	$public_part .= uri_escape( $row->{'emailaddress'} );
   	$public_part .= ":$row->{'membertype'}";
   	$public_part .= ":$row->{'spid'}";
   	
   	# we could put serialized data in here if a flexible sore if we want to later on
   	# and not have to worry about breaking the cookie structure
	$public_part .= ":{}";

	# Now we calculate the hash of this and the secret key and then
	# calculate the hash of *that* and the secret key again.
	my $hash = md5_hex( join ':', $secret_key, md5_hex(	join ':', $public_part, $secret_key	) );

	# Now we add this hash to the end of the public part.
	my $session_key = "$public_part:$hash";

	# Now we encrypt this and return it.
	my $encrypted_session_key = cipher->encrypt_hex( $session_key );
 
	return $encrypted_session_key;
}

=head3 Generate_AuthCustom_maf_Cookie($)

Take a member hash reference and create an AuthCustom_maf cookie using CGI::Cookie

=head4 Arguments

	A hash reference the likes of that returned by authenticate()

=head4 Returns

	A cookie string ready for insertion in an HTTP header

=cut

sub Generate_AuthCustom_maf_Cookie($)
{
	my ($self, $hr) = @_;
#	my $ck = $self->{'cgi'}->cookie('-name'=>'AuthCustom_maf', '-value'=>$self->Create_CS_Authen_General_Ticket($hr), '-domain'=>'.clubshop.com', '-path'=>'/');
	my $ck = $self->{'cgi'}->cookie('-name'=>'AuthCustom_maf', '-value'=>$self->Create_CS_Authen_maf_Ticket($hr), '-domain'=>'.clubshop.com', '-path'=>'/');
	return $ck;
}

#-------------------------------------------------------------------------------
# _now_year_month_day_hour_minute_second -- Return a string with the time in
# this order separated by dashes.

sub _now_year_month_day_hour_minute_second()
{
	return sprintf '%04d-%02d-%02d-%02d-%02d-%02d', Today_and_Now;
}

1;
