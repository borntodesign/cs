package IncomeReport::Base;

use strict;
use Carp qw( croak );

my %TMPL = (
	'TT'		=> '/home/httpd/cgi-templates/TT/pireport/live.tt',	# the main template
	'TPTT'		=> '/home/httpd/cgi-templates/TT/pireport/live_trial_partner.tt',
	'archives'	=> {
		'thru0712' => 10233,
		'thru0912' => 10970,
		'thru1012' => 10237,
		'thru0113' => 10994,
		'thru0213' => 10971,
		'thru0713' => 10998,
		'thru0813' => 11030,
		'thru0214' => 11032,
		'thru0614' => 11063,
		'thru0914' => 11086,
		'thru0815' => '/home/httpd/cgi-templates/TT/pireport/thru0815.tt'
	}
);

=head1 Pireport

With the breakup of the numerous income reports, we need a common code base for certain methods. This class will provide that.

=head2 new

=head3 Arguments

Pass arguments in on a hash.
Required key/values are:
	'db' which should evaluate to a valid DB handle by the time any DB methods are called in here.
	'cgi' which should be a valid CGI object.

If the required arguments are not provided, the method will croak.

=head4 Returns

A blessed hash.

=cut

sub new
{
	my ($class, %args) = @_;
	
	croak "Both a valid 'db' handle and valid 'cgi' object are required" unless $args{'db'} && $args{'cgi'};
	
	return bless \%args, $class;
}

sub db
{
	my $self = shift;

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

sub q { return $_[0]->{'cgi'}; }

sub cboCommissionMonths
{
	my $self = shift;
	croak "'rpte' has not been properly set" unless $self->rpte;
	
	my %keys = (''=>''); # put a value in to make the first line of the combo box have a label
	my @vals = '';
	my $sth = $self->db->prepare("
		SELECT DISTINCT rd.commission_month
		FROM archives.refcomp_me_dates rd
		JOIN archives.refcomp_me_batches rb
			ON rb.run_date=rd.run_date
		WHERE rd.commission_month < first_of_month()
		AND rd.commission_month > '2012-03-31'			-- enough already with supporting what is coming up on 3 years ago
		AND rb.id= ?
		ORDER BY rd.commission_month DESC
	");
	$sth->execute($self->rpte->{'id'});
	while (my $tmp = $sth->fetchrow_array)
	{
		push @vals, $tmp;
		$keys{$tmp} = $tmp;
	}
	return $self->q->popup_menu(
		'-class' => 'cbocomm',
		'-name' => 'month',
		'-values' => \@vals,
		'-labels' => \%keys,
		'-default' => ''
	);
}

sub GetReportAndTemplate
{
	my ($self, $month) = @_;
	
	my $tmpl= $TMPL{'TT'};
	
	# if this is a Trial Partner, then we will change the template
	if ($self->rpte && $self->rpte->{'membertype'} eq 'tp')
	{
		$tmpl= $TMPL{'TPTT'};
	}
	
	my $rpt = '/cgi/pireport.cgi';	# the default
	
	# these just require a different template
	if (! $month)
	{
		# do nothing... we'll just return the defaults at the end
	}
	elsif ( grep $_ eq $month, ('2012-04-30', '2012-05-31', '2012-06-30', '2012-07-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0712'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2012-08-31', '2012-09-30') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0912'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2012-10-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru1012'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2012-11-30', '2012-12-31', '2013-01-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0113'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2013-02-28') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0213'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2013-03-31', '2013-04-30', '2013-05-31', '2013-06-30', '2013-07-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0713'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2013-08-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0813'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2013-09-30', '2013-10-31', '2013-11-30', '2013-12-31', '2014-01-31', '2014-02-28') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0214'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2014-03-31', '2014-04-30', '2014-05-31', '2014-06-30') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0614'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2014-07-31', '2014-08-31', '2014-09-30') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0914'};
		$rpt = '/cgi/pireport/a2.cgi';
	}
	elsif ( grep $_ eq $month, ('2014-10-31', '2014-11-30', '2014-12-31', '2015-01-31', '2015-02-28', '2015-03-31', '2015-04-30', '2015-05-31', '2015-06-30', '2015-07-31', '2015-08-31') )
	{
		$tmpl= $TMPL{'archives'}->{'thru0815'};
		$rpt = '/cgi/pireport.cgi';
	}
	return ($tmpl, $rpt)
}

sub Redirect
{
	my ($self, $dest) = @_;
	
	# this version of the report is only for certain archives
	my $redirect = 'https://www.clubshop.com' . $dest;
	$redirect .= '?' . $self->q->query_string if $self->q->query_string;
	print $self->q->redirect($redirect);
}

=head2 rpte

A setter/getter for the reportee. That needs to be set for certain methods.

If called with no argument, this method will return the previously set value, which could be undef if it was not set.
If called with an argument, which should be a hashref, the reportee will be set to that and this method will return the previously set value.

=cut

sub rpte
{
	my ($self, $rpte) = @_;
	my $rv = $self->{'rpte'};
	$self->{'rpte'} = $rpte if $rpte;
	return $rv;
}

1;
