package Members;
use strict;
use DHSGlobals;
use Encode;

=head1
NAME: Members

	This class is ment to manage information pertaining to members,
	such as name, password, address, email Skype name, etc.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

	my $new_member_id = '';
	my %member_information = ();
	$member_information{firstname} = 'John';

	eval{
		my $Members = Members->new({db=>$db});
		$new_member_id = $Members->createNewMember(\%member_information);
	};
	if($@)
	{
		#How ever you want to handle the error.
	}


=head1
PREREQUISITS:

	String::Random	This is declared, and used in createNewMember 
					for creating the members password.


=cut


=head1
DESCRIPTION

=head2
CONSTRUCTOR

=head3
new

=head4
Params:

	db obj to a database connection

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class = shift;
    my $this = {};

    $this->{'db'} = shift;

    bless($this, $class);
    
    $this->_init();
    
    return $this;
}


=head2 METHODS

=head3
PRIVATE

=cut



=head3
_init

=head4
Description:

	Description of what is set in _init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $this = shift;

    $this->{'version'}					= '2';
    $this->{'name'}						= 'Members';
    $this->{'member_info_by_id'}			=  {},
    $this->{'member_info_by_alias'}		=  {}, # This is a hashref of references when using it's values use ${$this->{'member_info_by_alias'}}->{FIELD_NAME},
    $this->{'member_mop_info_by_id'}		=  {},
    $this->{'member_info_by_spid'}		=  {},

}



=head3
__updateSponsorCache

=head4
Description:

	Get the users information by the "Sponsor ID"
	and assign the data to the "member_info_by_spid" cache,
	the "member_info_by_id" cache and the "member_info_by_alias" cache.

=head4
Params:

	hashref
		sponsor_id	bool	if true use the 'id' else use 'alias'

=head4
Return:

	string	on error
=cut

sub __updateSponsorCache
{
	my $this = shift;
	my $sponsor_id = shift;

	my $query =<<EOT;

SELECT
	m.*,
	mt.display_code
FROM
	members m
	JOIN
	\"Member_Types\" mt
	ON
	m.membertype=mt.code
WHERE 
	m.spid = ? 
ORDER BY 
	m.join_date DESC

EOT


	my $return_message = '';
	
	$sponsor_id =~ s/\D//;
	
	return "The 'sponsor_id' was not defined a required parameter. sponsor_id: $sponsor_id " if (!defined $sponsor_id || $sponsor_id !~ /^\d+$/);
	
	eval{

		my $sth = $this->{'db'}->prepare($query);
		$sth->execute($sponsor_id);
		
		$this->{'member_info_by_spid'}->{$sponsor_id} = [];
		
		while (my $rows = $sth->fetchrow_hashref)
		{
			
			$this->{'member_info_by_id'}->{$rows->{'id'}} = $rows;
			
			push @{$this->{'member_info_by_spid'}->{$sponsor_id}}, $rows;
			
			############################
			# Here we use a reference
			# so we don't turn into a
			# memory hog.
			############################
			$this->{'member_info_by_alias'}->{$rows->{'alias'}} = \$this->{'member_info_by_id'}->{$rows->{'id'}};
			
			
		}
		
		delete $this->{'member_info_by_spid'}->{$sponsor_id} if ! scalar(@{$this->{'member_info_by_spid'}->{$sponsor_id}});

	};
	return "$this->{name}::__updateSponsorCache - The cache was not updated due to an error. Query: $query -- sponsor_id: $sponsor_id -- " .  $@ if ($@);
	
	return $return_message;
}




=head3
__updateCache

=head4
Description:

	Get the users information by the "Member ID" or "Alias"
	and assign the data to the "member_info_by_id" cache
	and the "member_info_by_alias" cache.

=head4
Params:

	hashref
		member_id	bool	if true use the 'id' else use 'alias'
		id		string	MemberID

=head4
Return:

	string	on error
=cut

sub __updateCache
{
	my $this = shift;
	my $args = shift;
	my $field = $args->{'member_id'} ?'id':'alias';

	my $query = "SELECT m.oid, m.* FROM members AS m WHERE m.$field = ?";

	my $return_message = '';
	
	if (!defined $args->{'id'})
	{
		return "The 'id' is a required parameter.";
	}
	
	eval{

		my $sth = $this->{'db'}->prepare($query);
		$sth->execute($args->{'id'});
		my $rows = $sth->fetchrow_hashref;

		if($rows)
		{
			
			$this->{'member_info_by_id'}->{$rows->{'id'}} = $rows;
			
			############################
			# Here we use a reference
			# so we don't turn into a
			# memory hog.
			############################
			$this->{'member_info_by_alias'}->{$rows->{'alias'}} = \$this->{'member_info_by_id'}->{$rows->{'id'}};
			
		}
		else
		{
			die "The member was not found. The Member ID, or the Member Alias may be incorrect. ";
		}

	};
	if ($@)
	{
		return "The cache was not updated due to an error. Query: $query -- ID: $args->{'id'} -- " .  $@;
	}

	return $return_message;
}


=head3
__updateMOPCache

=head4
Description:

	Get the users MOP information by the "Member ID"
	and assign the data to the "member_mop_info_by_id" cache.

=head4
Params:

	id		string	MemberID

=head4
Return:

	string	on error
=cut

sub __updateMOPCache
{
	my $this = shift;
	my $args = shift;

	my $query = "SELECT m.* FROM mop AS m WHERE m.id = ?";

	my $return_message = '';
	
	if (!defined $args->{'id'})
	{
		return "The 'id' is a required parameter.";
	}
	
	eval{

		my $sth = $this->{'db'}->prepare($query);
		$sth->execute($args->{'id'});
		my $rows = $sth->fetchrow_hashref;

		if($rows)
		{
					
			$this->{'member_mop_info_by_id'}->{$rows->{'id'}} = $rows;
		} else {
			die "The member was not found. The Member ID may be incorrect. ";
		}

	};
	if ($@)
	{
		return "The cache was not updated due to an error. Query: $query -- ID: $args->{'id'} -- " .  $@;
	}

	return $return_message;
}



=head3
PUBLIC

=cut

=head3
getMemberInformationByMemberID

=head4
Description:

	Get the users information by the Member ID and assign
	the data to the cache

=head4
Params:

	hash
		member_id	int	MemberID
		field		string	Field Name
		force		bool	(Optional) Set to true if you want 
							the latest information from the database

=head4
Returns:

	string, hashref, or null	If the field parameter is specified a string 
						will be returned.  A hashref containing the 
						member record is returned if no field parameter 
						is set, if there is no data null is returned.

=cut

sub getMemberInformationByMemberID
{
	my $this = shift;
	my $params = shift;

	$params->{'force'} = 0 if !defined $params->{'force'};

	return if (! exists $params->{'member_id'} || ! $params->{'member_id'});

	$params->{'member_id'} =~ s/\s+//g;

	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	
	if (defined $params->{'member_id'} && exists $this->{'member_info_by_id'}->{$params->{'member_id'}} && !$params->{'force'})
	{
				return (exists $params->{'field'} && $params->{'field'}) ? $this->{'member_info_by_id'}->{$params->{'member_id'}}->{$params->{'field'}} : $this->{'member_info_by_id'}->{$params->{'member_id'}};
	}

	# If the member cache does not exist, or the data needs to be refreshed
	if(! $this->__updateCache({'member_id'=>1, 'id'=>$params->{'member_id'}}))
	{
		return (exists $params->{'field'} && $params->{'field'}) ? $this->{'member_info_by_id'}->{$params->{'member_id'}}->{$params->{'field'}} : $this->{'member_info_by_id'}->{$params->{'member_id'}};
				
	}

	return;

}




=head3
getMembersBySponsorID

=head4
Description:

	Get all the members sponsored by a Member

=head4
Params:

	sponsor_id	int	MemberID
	force		bool	(Optional) Set to true if you want 
						the latest information from the database

=head4
Returns:

	arrayref, or null	A arrayref containing the 
						member records is returned, if there is no data 
						null is returned.

=cut

sub getMembersBySponsorID
{
	
	my $this = shift;
	my $sponsor_id = shift;
	my $force = shift;
	
	$force = 0 if !defined $force;
	
	return if (! defined $sponsor_id || ! $sponsor_id);
	
	$sponsor_id =~ s/\D//g;
	
	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	
	return $this->{'member_info_by_spid'}->{$sponsor_id} if (defined $sponsor_id && exists $this->{'member_info_by_spid'}->{$sponsor_id} && !$force);
	
	# If the sponsor cache does not exist, or the data needs to be refreshed
	if(! $this->__updateSponsorCache($sponsor_id))
	{
		return $this->{'member_info_by_spid'}->{$sponsor_id} if exists $this->{'member_info_by_spid'}->{$sponsor_id};
	}

	
	return;
	
}




=head3
getMemberInformationByMemberAlias

=head4
Description:

	Get the Member information by the Member Alias and assign
	the data to the cache

=head4
Params:

	hash
		alias	string	Member Alias
		field		string	(Optional)	The Field Name you want retrieved
							from the members information
		force		bool	(Optional) Set to true if you want 
							the latest information from the database

=head4
Returns:

	string or hashref	If the field parameter is specified a string 
						will be returned.  A hashref containing the 
						member record is returned if no field parameter 
						is set.

=cut

sub getMemberInformationByMemberAlias
{
	my $this = shift;
	my $params = shift;

	$params->{'force'} = 0 if (!exists $params->{'force'} && !$params->{'force'});

	return if (!exists $params->{'alias'} || !  $params->{'alias'});

	$params->{'alias'} =~ s/\s+//g;
	
	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	
	
	if ($this->{'member_info_by_alias'}->{$params->{'alias'}} && !$params->{'force'})
	{
		return (exists $params->{'field'} && $params->{'field'}) ? ${$this->{'member_info_by_alias'}->{$params->{'alias'}}}->{$params->{'field'}} : ${$this->{'member_info_by_alias'}->{$params->{'alias'}}};
	}

	# If the member cache does not exist, or the data needs to be refreshed
	
	if(! $this->__updateCache({'member_id'=>0, 'id'=>$params->{'alias'}}))
	{
		return (exists $params->{'field'} && $params->{'field'}) ? ${$this->{'member_info_by_alias'}->{$params->{'alias'}}}->{$params->{'field'}} : ${$this->{'member_info_by_alias'}->{$params->{'alias'}}};
	}

	return;


}


=head3
getSkypeNameByMemberID

=head4
Description:

	Get the users Skype name by their Member ID

=head4
Params:

	int	$member_id	MemberID

=head4
Returns:

	string skype name

=cut

sub getSkypeNameByMemberID
{
	my $this = shift;
	my $member_id = shift;

	return $this->getMemberInformationByMemberID({'member_id'=>$member_id, 'field'=>'other_contact_info', 'force'=>0});

}


=head3
checkEmailDuplicates

=head4
Params:

	email		string	The email address to check.
	member_id	int		(Optional) If this parameter is included the member, whos ID is specified, is excluded from the check
									This means the email address will be compared to with all member email addresses except 
									for the the email address whos Member ID is specified

=head4
Returns:

	int	0 if the email address already exists in the members table.

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=cut

sub checkEmailDuplicates
{
	my $this = shift;
	my $email = shift;
	my $member_id = shift;
	
	die 'The email address is a required parameter. ' if (! defined $email || ! $email);
	
	$email = lc($email);
	
	if(! $member_id)
	{
		return 0 if $this->{'db'}->selectrow_array('SELECT id FROM members WHERE emailaddress= ? OR emailaddress2= ?', undef, $email, $email);
	}
	else 
	{
		return 0 if $this->{'db'}->selectrow_array('SELECT id FROM members WHERE (emailaddress= ? OR emailaddress2= ?) AND id <> ?', undef, $email, $email, $member_id);
	}
	return 1;

}


=head3
checkEmailDomain

=head4
Params:

	email	string	The email address to check.

=head4
Returns:

	int	0 if the email is in a blocked domain, and 1 if the email address is safe.

=head4
Errors:

	An exception is thrown if "excluded_domains.lib" is not available, or a fatal error occurs.

=cut

sub checkEmailDomain
{
	my $this = shift;
	my $email = shift;
	
	die 'The email address is a required parameter. ' if (! defined $email || ! $email);
	
	if (! exists $this->{domains_to_exclude})
	{
		eval{
			require 'excluded_domains.lib';
		};
		if($@)
		{
			return 0;
		}
		
		$this->{domains_to_exclude} = [ @excluded_domains::LIST ];
	}
	
	return 0 if grep $email =~ /$_$/i, @{$this->{domains_to_exclude}};
	
	return 0 if $this->{'db'}->selectrow_array("SELECT domain FROM domains_blocked WHERE ? LIKE ('\%\@' || domain)", undef, $email);
	
	return 0 if $this->{'db'}->selectrow_array("SELECT domain FROM domains_bogus WHERE ? LIKE ('\%\@' || domain)", undef, $email);
	
	return 1;

}


=head3
createNewMember

=head4
Params:

	hashref
		The hashref should be created with the keys
		being field names, and the value being what
		is ment to be placed in that field.  If you
		include the member "id" you must also
		include the "alias".  Also if you include
		"ipaddress","signup_date","coop_name","landing_page"
		as indexes the "original_spid" table will be updated.
		Also if you include "notification_type", "notification_memo"
		as indexes a record will be created in the 
		"notification" table.
		
	no_transaction	bool	(Optional)	Set this to true if you not 
										want the member creation to 
										be in a Transaction Block.  
		
	do_not_check_email_duplicates	bool	(Optional) Set to true if 
											you do not want to require 
											a unique email address.  
											We normally check to make 
											sure we do not have a 
											preexisting email address 
											in the system.

=head4
Returns:

	hash	{member_id=>$params->{'id'}, alias=>$params->{'alias'}}, or a string on error.  
			Use ref() eq 'HASH' to determin if there was an error or not.

=cut

sub createNewMember
{

	use String::Random;
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
	
	my $this = shift;
	my $params = shift;
	my $no_transaction = shift;
	my $do_not_check_email_duplicates = shift;
	
	
	########################################
	# If no sponsor ID exists use 1.
	########################################
	if (! exists $params->{'spid'} || ! $params->{'spid'})
	{
		$params->{'spid'} = 1;
	}
	
	$params->{'spid'} =~ s/\s+//g;
	
	if($params->{'spid'} !~ /^\d+$/)
	{
		if($params->{'spid'} =~ /^\w\w\d+$/)
		{
			my $original_spid = $params->{'spid'};
			
			$params->{'spid'} = uc($params->{'spid'});
			
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
			$params->{'spid'} = $this->getMemberInformationByMemberAlias({'alias'=>$params->{'spid'}, 'field'=>'id'});
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
			
			if (! $params->{'spid'})
			{
				$params->{'spid'} = 1;
				
				if(exists $params->{'memo'} && $params->{'memo'})
				{
					$params->{'memo'} .= " - The Sponsor ID $original_spid was not recognized so we used 1.";
				} else {
					$params->{'memo'} = "The Sponsor ID $original_spid was not recognized so we used 1.";
				}
				
			}			
			
			
		} else {
			
			if(exists $params->{'memo'} && $params->{'memo'})
			{
				$params->{'memo'} .= " - The Sponsor ID $params->{'spid'} was not recognized so we used 1.";
			} else {
				$params->{'memo'} = "The Sponsor ID $params->{'spid'} was not recognized so we used 1.";
			}
			
			$params->{'spid'} = 1;
		}
	} else {
		
		my $original_spid = $params->{'spid'};
#	warn __FILE__ . "Line: " . __LINE__ . "\n";

		$params->{'spid'} = $this->getMemberInformationByMemberID({'member_id'=>$params->{'spid'}, 'field'=>'id'});
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
		
		if (! $params->{'spid'})
		{
			$params->{'spid'} = 1;
				
			if(exists $params->{'memo'} && $params->{'memo'})
			{
				$params->{'memo'} .= " - The Sponsor ID $original_spid was not recognized so we used 1.";
			} else {
				$params->{'memo'} = "The Sponsor ID $original_spid was not recognized so we used 1.";
			}
			
		}
		
	}

	########################################
	# If no "membertype" exists use "x".
	########################################
	if (! exists $params->{'membertype'})
	{
		$params->{'membertype'} = 'x';
	}
	
	
	if (exists $params->{'emailaddress'} && $params->{'emailaddress'} && ! $do_not_check_email_duplicates)
	{
		$params->{'emailaddress'} = lc($params->{'emailaddress'});
		
		return 'The potential members email address is from a domain on our black list (we do not allow email addresses from the blacklist to join). ' if ! $this->checkEmailDomain($params->{'emailaddress'});
		return 'The potential members email address already exists. ' if ! $this->checkEmailDuplicates($params->{'emailaddress'});
	}
	
	
	if (exists $params->{'emailaddress2'} && $params->{'emailaddress2'} && ! $do_not_check_email_duplicates)
	{
		$params->{'emailaddress2'} = lc($params->{'emailaddress2'});
		
		return 'The potential members email address two is from a domain on our black list (we do not allow email addresses from the blacklist to join). ' if ! $this->checkEmailDomain($params->{'emailaddress2'});
		return 'The potential members email address two already exists. ' if ! $this->checkEmailDuplicates($params->{'emailaddress2'});
	}
	
	
	
	my $insert_query = 'INSERT INTO members ';
	my $fields = '';
	my $query_markers = '';
	my @values = ();
	my $String_Random = String::Random->new();

	my $original_spid_update_query = 'UPDATE original_spid SET ';
	my $original_spid_set = '';
	my @original_spid_values = ();
	
	my $notification_query = 'INSERT INTO notifications ';
	my $notification_fields = '';
	my $notification_field_markers = '';
	my @notification_query_values = ();
	
	########################################
	# Build the fields to insert into the
	# "members" table, and "original_spid" table.
	########################################
	foreach my $key (keys %$params)
	{
		next if ($key eq 'id' || $key eq 'alias' || ! $params->{$key});

		# info for the "original_spid" table.
		if ($key =~ /^ipaddress$|^signup_date$|^coop_name$|^landing_page$/ && ! defined $params->{'id'})
		{
			$original_spid_set .= " $key = ?, ";
			push @original_spid_values, $params->{$key};
			next;
		}
		
		# info for the "notification" table.
		if ($key =~ /^notification_type$|^notification_memo$/ && ! defined $params->{'id'})
		{
			$notification_fields .= $key=~ /^notification_memo$/ ? " memo, " : " $key, ";
			
			$notification_field_markers .= '?, ';
			push @notification_query_values, $params->{$key};
			next;			
		}
		
		
		$fields .= "$key, ";
		$query_markers .= '?, ';
		push @values, $params->{$key};
		
	}
	
	$original_spid_set =~ s/,\s*$/\ WHERE id = ?/;
#	warn "Query so far: " . $fields . "\n";
		
 eval{

		########################################
		# If the members ID is specified strip
		# the extra commas, otherwise add the
		# id field, and the place holdher for
		# use later.
		########################################
		if (exists $params->{'id'})
		{
			########################################
			# If the ID is set make sure it is numberic
			# otherwise return with the value of null.
			########################################
			if ($params->{'id'} !~/^\d+$/)
			{
				#warn "The 'id' parameter is not in the correct format, it should be an integer (number).";
				die "The 'id' parameter is not in the correct format, it should be an integer (number).";
			}
			
			push @original_spid_values, $params->{'id'};
			
		} else {
			$fields .= 'id,';
			$query_markers .= '?,';

			########################################
			# If no ID was specified retrieve the
			# next value to use.
			########################################
			my @primary_key_from_members_table = $this->{'db'}->selectrow_array("SELECT NEXTVAL('members_id_seq')");

			$params->{'id'} = $primary_key_from_members_table[0];
			push @values, $params->{'id'};
			push @original_spid_values, $params->{'id'};
		}

		my $first_letter_firstname = $params->{'firstname'}? uc(substr($params->{'firstname'}, 0, 1)):'A';
		$first_letter_firstname = 'A' unless $first_letter_firstname =~ m/[A-Z]/;

		my $first_letter_lastname = $params->{'lastname'}? uc(substr($params->{'lastname'}, 0, 1)):'A';
		$first_letter_lastname = 'A' unless $first_letter_lastname =~ m/[A-Z]/;

		if(! exists $params->{'alias'})
		{

			$fields .= 'alias,';
			$query_markers .= '?,';


			$params->{'alias'} = $first_letter_firstname . $first_letter_lastname . $params->{'id'};
			push @values, $params->{'alias'};

		}

		if(! exists $params->{'password'} || ! $params->{'password'})
		{
			$fields .= '"password",';
			$query_markers .= '?,';
			
			push @values, $first_letter_firstname . $first_letter_lastname . $String_Random->randpattern("ccnn");
			
			
		}

		$fields =~ s/,\s*$//;
		$query_markers =~ s/,\s*$//;

		$insert_query .= " ($fields) VALUES ($query_markers)";

#	warn __FILE__ . "Line: " . __LINE__ . "\n";

		$this->{'db'}->{'AutoCommit'} = 0 if ! $no_transaction;
		
                my @fixed_values = ();
                foreach (@values) {
                    my $tmp = Encode::is_utf8($_) ? $_ : decode_utf8($_);
                    push @fixed_values, $tmp
                }
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
		if(! $this->{'db'}->do($insert_query, undef, @fixed_values))
		{
			#warn 'The member record was not created.';
			die 'The member record was not created.';
		}
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
		

		if ($original_spid_set)
		{
			$original_spid_update_query .= $original_spid_set;

#	warn __FILE__ . "Line: " . __LINE__ . "\n";
			if(! $this->{'db'}->do($original_spid_update_query, undef, @original_spid_values))
			{
				#warn 'The original_spid table was not updated.';
				die 'The original_spid table was not updated.';
			}
			
#	warn __FILE__ . "Line: " . __LINE__ . "\n";

		
		}
#	warn __FILE__ . "Line: " . __LINE__ . "\n";

		if ($notification_fields)
		{
			
			$notification_query .= " ($notification_fields id, spid) VALUES ($notification_field_markers ?, ?) ";
			
			push @notification_query_values, $params->{'id'};
			push @notification_query_values, $params->{'spid'};

#	warn __FILE__ . "Line: " . __LINE__ . "\n";
			
			if(! $this->{'db'}->do($notification_query, undef, @notification_query_values))
			{
				#warn 'The member record was not created.';
				die 'The notification record was not created.';
			}
#	warn __FILE__ . "Line: " . __LINE__ . "\n";
			
			
		}
		
		# $return_value->{'id'}, $potential_member_hashref->{'spid'}, 'Co-op Import';
		
		
		$this->{'db'}->commit() if ! $no_transaction;
		$this->{'db'}->{AutoCommit} = 1 if ! $no_transaction;
		
	};
	if($@)
	{
		$this->{'db'}->rollback() if ! $no_transaction;
		$this->{'db'}->{AutoCommit} = 1 if ! $no_transaction;
		
		die "There was an error creating the new member. <br />Error: " . $@;
		
	}
	
	####################################################
	# This may be needed later on.
	#
	#$this->__updateCache({member_id=>1, id=>$primary_key_from_members_table[0]});
	####################################################
	
	return {member_id=>$params->{'id'}, alias=>$params->{'alias'}};
}


=head3
confirmMembership

=head4
Description:

	This method is #@&*, but a bunch of work has
	already been put into 'appx' so I don't see a reason
	to duplicate it at this point.

=head4
Params:

	hashref
		'id' int The member id from the 'members' table.

=head4
Returns:

	bool 1 on succes, 0 on failure


=head4
Errors:

	An exception will be thrown if the "id" parameter is not specified.

=cut

sub confirmMembership
{
	use LWP::Simple;
	use URI::URL;


	my $this = shift;
	my $params = shift;

	die "The 'id' parameter is required, and must be an integer (number)." if (! defined $params->{'id'} || $params->{'id'} !~ /^\d+$/);


	my $content = '';

	my $Url = {};

	eval{

		$Url = url('http://' . DHSGlobals::CLUBSHOP_DOT_COM . '/cgi/appx.cgi/c?');

		if(! exists $this->{'member_info_by_id'}->{$params->{'id'}})
		{
			if($this->__updateCache({'member_id'=>1, 'id'=>$params->{'id'}}))
			{
				die 'There was an error retrieving the members information.';
			}
		}

		if ($this->{'member_info_by_id'}->{$params->{'id'}}->{'alias'} && $this->{'member_info_by_id'}->{$params->{'id'}}->{oid})
		{
			$Url->query_form('mail_option_lvl'=>1, 'membertype'=>'s', 'app_step'=>'step1', 'app_type'=>'c', 'alias'=>$this->{'member_info_by_id'}->{$params->{'id'}}->{'alias'}, 'oid'=>$this->{'member_info_by_id'}->{$params->{'id'}}->{'oid'}, 'temp_password'=>$this->{'member_info_by_id'}->{$params->{'id'}}->{'password'}, 'password'=>$this->{'member_info_by_id'}->{$params->{'id'}}->{'password'}, next_step=>'step2');

			$content = get($Url);
		}

	};
	if ($@)
	{
		die 'There was an error confirming the membership.  Error: ' . $@;
	}

	if (! $content || $content !~ /<title>Discount\s+Home\s+Shoppers\s+Club\s+-\s+DHS\s+Club\s+Free\s+Membership<\/title>/)
	{
		return 1;
	}

	return;
}


=head3
getMemberMOPInformation

=head4
Description:

	Get the users MOP (Method of Payment) information by the Member ID.

=head4
Params:

	hash
		member_id	int	MemberID
		field		string	Field Name
		force		bool	(Optional) Set to true if you want 
							the latest information from the database

=head4
Returns:

	string, hashref, or null	If the field parameter is specified a string 
						will be returned.  A hashref containing the 
						member record is returned if no field parameter 
						is set, if there is no data null is returned.

=cut

sub getMemberMOPInformation
{
	my $this = shift;
	my $params = shift;

	$params->{'force'} = 0 if !defined $params->{'force'};

	return if (! exists $params->{'member_id'} || ! $params->{'member_id'});

	$params->{'member_id'} =~ s/\s+//g;

	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	if ($this->{'member_mop_info_by_id'}->{$params->{'member_id'}} && !$params->{'force'})
	{
				return (exists $params->{'field'} && $params->{'field'}) ? $this->{'member_mop_info_by_id'}->{$params->{'member_id'}}->{$params->{'field'}} : $this->{'member_mop_info_by_id'}->{$params->{'member_id'}};
	}

	# If the member cache does not exist, or the data needs to be refreshed
	if(! $this->__updateMOPCache({'member_id'=>1, 'id'=>$params->{'member_id'}}))
	{
		return (exists $params->{'field'} && $params->{'field'}) ? $this->{'member_mop_info_by_id'}->{$params->{'member_id'}}->{$params->{'field'}} : $this->{'member_mop_info_by_id'}->{$params->{'member_id'}};
	}

	return;

}


=head3
updateMember

=head4
Description:

	Update fields in the members table.

=head4
Params:

		member_id	int	MemberID
		params		hashref	A hashref where the 
							key is the field you 
							want to update, and 
							the value is the data 
							to use.

=head4
Returns:

	int	returns the number of rows affected.

=head4
Errors:

	An exception is thrown if the member_id was not specified, 
	or there are no parameters specified.  An exception is also 
	thrown if there is an error with the update query.

=cut

sub updateMember
{
	
	my $this = shift;
	my $member_id = shift;
	my $params = shift;
	
	die 'The member ID is a required parameter.' if !$member_id;
	die 'There were not any fields specified to update.' if !$params;
	
	my $update_query  = ' UPDATE members SET ';
	my @fields = ();

	my @values = ();
	my $return_value = 0;
	########################################
	# Build the fields to insert into the
	# "members" table, and "original_spid" table.
	########################################
	foreach my $key (keys %$params)
	{
		next if ($key eq 'id' || $key eq 'alias' || ! $params->{$key});
		
		push @fields, "$key = ?";
		push @values, $params->{$key};
		
	}
	
	push @values, $member_id;
	
	my $set_fields = join(', ', @fields);
	
	$update_query .= " $set_fields WHERE id = ? ";
	
#	use Data::Dumper;
#	my $temp = \@values;
#	warn "\n\n update_query: \n $update_query \n\n values: \n " . Dumper($temp);
	
	return $this->{'db'}->do($update_query, undef, @values);
	
}


=head3
updateTin

=head4
Description:

	Update the "tin" field in the members table.

=head4
Params:

		member_id	int	MemberID
		tin		SCALAR	The members Tax ID

=head4
Returns:

	int	returns the number of rows affected.

=head4
Errors:

	An exception is thrown if the member_id was not specified. 
	An exception is also thrown if there is an error with the 
	update query.

=cut

sub updateTin
{
	
	my $this = shift;
	my $member_id = shift;
	my $tin = shift;
	
	die 'The member ID is a required parameter.' if !$member_id;
	
	my $update_query  = ' UPDATE members SET tin = ? WHERE id = ?';
	
	$tin = undef if ! $tin;
	
	my @value = ($tin, $member_id);
	
	return $this->{'db'}->do($update_query, undef, @value);
	
}


1;

__END__

=head1
CHANGE LOG:

	2008-10-21	Keith	Changed the way the passwords were being generated.  
						The are now being generated using 
						String::Random->randpattern("ccnn"), I didn't like 
						the results rand was giving.
	
	2008-10-23	Keith	Added the "getMembersInformationByMembersAlias", and
						added the "member_info_by_alias" cache.
						
	2009-01-08	Keith	Added the "getMemberMOPInformation", "__updateMOPCache", 
						and the "member_mop_info_by_id" cache.
	
	2009-06-06	Keith	Added the use of DHSGlobals::CLUBSHOP_DOT_COM to the
						"confirmMembership" Method.
	2009-06-07	Keith	Added the "no_transactions" option to "createNewMember".
	
	2009-06-07	Keith	Added the "updateMember" method.
	
	2009-06-07	Keith	Added the "do_not_check_email_duplicates" to the createNewMember Method

=cut

