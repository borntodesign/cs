package cs_admin;

=head1 cs_admin

To start with, just a few admin cookie related methods.

=cut

use strict;
use Digest::SHA1 qw(sha1_hex);
use URI::Escape qw(uri_escape uri_unescape);
use Crypt::CBC;
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );

my $secret = 'dpe094jfdu&&6';
my $cipher = Crypt::CBC->new('-key'=>$secret, '-cipher'=>'DES' );

=head3 authenticate

	Accepts a ticket value created by CreateTicket(), and determines it's authenticity.
	If a ticket value is not received, it will look to see if a CGI object is available on the blessed reference to "self".
	If it is available, it will try to obtain the cookie value from that.
	
	Returns a hashref, minimally with the username and password contained in the ticket.

=cut
 
sub authenticate
{
	my ($me, $ticket, $flag) = self_or_default(@_);

	# if we did not receive a ticket, maybe we can look it up?
	unless ($ticket)
	{
		my $msg = "No ticket value received.";
		die $msg unless ref $me eq __PACKAGE__;
		die "$msg No CGI object passed to new()." unless $me->{'cgi'};
		$ticket = $me->{'cgi'}->cookie('cs_admin');
		
		unless ($ticket)
		{
			Dye("$msg. No cs_admin cookie received", $flag);
			return undef;
		}
	}
	$ticket = $cipher->decrypt_hex($ticket, $secret);
	my ($issue_time, $expire_time, $username, $password, $hr, $hash) = split(/\:/, $ticket);
	
	unless ($issue_time && $expire_time && $username && $password && $hr && $hash)
	{
		Dye("We are missing something in that ticket", $flag);
		return undef;
	}

	my $test = "$issue_time:$expire_time:$username:$password:$hr";
	unless (sha1_hex($test, $secret) eq $hash)
	{
		Dye("Ticket does not authenticate", $flag);
		return undef;
	}
	
	if ( _now_year_month_day_hour_minute_second() gt $expire_time )
	{
		Dye("Expire time $expire_time has passed for user $username... You must login again ;-)", $flag);
		return undef;
	}

	return {
		'username' => uri_unescape($username),
		'password' => uri_unescape($password)
	};
}

=head3 CreateCookie

	Requires a username and password arguments.
	Returns a cookie string suitable for outputting in an HTTP header.
	
=cut

sub CreateCookie
{
	my ($me, $username, $password) = self_or_default(@_);
	my $tk = $me->CreateTicket($username, $password);
	return undef unless $tk;
	return "cs_admin=$tk; path=/";
}

=head3 CreateTicket

	Create the value which will be used in the cs_admin cookie
	We are looking for username & password arguments CreateTicket('pops','dldkk')
	A warning will be issued and an undef returned if both values are not received.
	
	Returns a hex encoded DES encrypted ticket suitable for direct placement into a cookie.
	
=cut 

sub CreateTicket
{
	my ($me, $username, $password) = self_or_default(@_);
	unless ($username && $password){
		warn "username and password are required\n";
		return undef;
	}

	# Create the expire time for the ticket.... currently 12 hours
	my $expire_time = sprintf(
			'%04d-%02d-%02d-%02d-%02d-%02d',
			Add_Delta_DHMS( Today_and_Now, '00', '12', '00', '00'));
	my $current_time = _now_year_month_day_hour_minute_second();
	
	my $ticket = "$current_time:$expire_time:";
	$ticket .= uri_escape($username) . ':';
	$ticket .= uri_escape($password) . ':';
	# for future use if we need to serialize some data into "hashref"
	$ticket .= '{}';
	$ticket = "$ticket:" . sha1_hex($ticket, $secret);
	return $cipher->encrypt_hex($ticket);
}

=head3 Dye

This was originally written so that the authenticate method died in all cases of failure, which was fine for use in scripts as it rendered the needed message.
However, with this module being used in Apache authentication, it just throws an internal server error and the message is lost and the default "error" page is never rendered.
So, we are substituting the "die" statements with this one. If the flag is present, as it will be for callers that do not want to die, then we will simply warn the message.

=cut

sub Dye
{
	my ($me, $msg, $flag) = self_or_default(@_);
	
	if ($flag)
	{
		warn $msg;
	}
	else
	{
		die $msg;
	}
}

=head3 new

	This method is just being provided for starters as a general OO way of doing things.
	The methods/functions in here will generally work as either.
	
	new() will accept arguments in a hashref which will become the blessed object.
	
	If a CGI object is passed into new() like this: ->new({cgi=>CGI Object})
	then we can omit passing a cookie value into the authenticate() method as it will try to obtain it directly from CGI.
	
	If a ticket value cannot be determined, this method will die with a textual error message.
	
=cut

sub new
{
	my ($class, $args) = @_;
	return bless ($args || {});
}

sub self_or_default
{
	# we received a class name, that'll do
	return @_ if defined($_[0]) && (! ref $_[0]) && $_[0] eq __PACKAGE__;

	unshift @_, __PACKAGE__->new() unless defined $_[0] && ref $_[0] eq __PACKAGE__;

	return @_;
}

sub _now_year_month_day_hour_minute_second()
{
	return sprintf '%04d-%02d-%02d-%02d-%02d-%02d', Today_and_Now;
}

1;
