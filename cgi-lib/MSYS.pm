##++
##     MSYS.pm
##     Last modified: 05/26/04	Bill MacArthur
##
##     Written by Stephen Martin
##
##     This module is the API for the msg_sub_sys 
##     Messaging Sub System used to communicate with 
##     dhs-club.com members, when they enter the VIP website.
##--

=head1 NAME

	MSYS Perl Module API for Messaging Sub System.

=head1 SYNOPSIS

	use MSYS;
 
	$m =  new MSYS;

        $response = $m->send_txt_Msg(<ID>,"MESSAGE",[expiration date --default now() + 10 days]);

        $response = $m->send_Tmpl_Msg(<ID>,$obj_id,[expiration date --default now() + 10 days]);

        $response = $m->expire_Messages(<ID>,[expiration date --default now()]);

        $html     = $m->message_Check(<ID>,$cookie);

=head1 DESCRIPTION

        This module allows server based programs to send 
        individual users messages, that are either
        free text or point to a Template Object.

        An optional expiration date can also be set.
        If no expiration date is provided a default of 
        now() + 10 days.
    
        An expiration date is always entered, to avoid 
        messages that cannot be house cleaned.

        The methods return undefined if there is a problem,
        otherwise a value of true or HTML content will be
        returned.

        This module requires the presence of perl DBI/DBD.

        Here is some example code

        #!/usr/bin/perl

        use MSYS;

	my $m = new MSYS;
	my $id = "SM2994131";
	my $TMPL = "10047";

	my $response;

	$response = $m->send_txt_Msg($id,"This was created by $0" )||
 	 defined $response or die "Error invoking.. send_txt_Msg $! \n";

	$response = $m->send_txt_Msg($id,"This was created by $0", "now()" )||
 	 defined $response or die "Error invoking.. send_txt_Msg $! \n";

	$response = $m->send_Tmpl_Msg($id, $TMPL ) ||
 	 defined $response or die "Error invoking.. send_Tmpl_Msg $! \n";

	my $html = $m->message_Check( $id ,$cookie) ||
 	 defined $html or die "Error invoking.. >message_Check $! \n";

	$response = $m->expire_Messages( $id ) ||
 	 defined $response or die "Error invoking.. expire_Messages $!\n";

	print $html;

=head1 COPYRIGHT INFORMATION

 	Permission to use, copy, and  distribute  is  hereby granted,
 	providing that the above copyright notice and this permission
 	appear in all copies and in supporting documentation.

=cut

package        MSYS;
require Exporter;
@ISA    = qw(Exporter);
@EXPORT =
  qw(send_txt_Msg send_Tmpl_Msg message_Check expire_Messages new version);
@EXPORT_OK =
  qw(send_txt_Msg send_Tmpl_Msg message_Check expire_Messages new version);

use DBI;
use strict;

###### we'll use our standard custom libraries to connect to the DB rather than hard-coded here
use lib ('/home/httpd/cgi-lib');
use DB_Connect;

my ($db, $dbh) = ();
my $qry;
my $sth;
my $rv;
my $data;
my $VERSION = "1.17";

sub new {
    my $object = {};
    bless $object;
    return $object;
}

sub version {
    return "1.01";
}

sub send_txt_Msg {
    shift;
    my ($I) = @_;
    shift;
    my ($M) = @_;
    shift;
    my ($E) = @_;

    unless ($I) { return undef; }

    unless ($M) { return undef; }

    $I =~ s/\D//g;

    $dbh = DB_Connect::DB_Connect('msg_sub_sys') || die "Cannot connect to the Database: $dbh->errstr $!\n";

    if ( !$E ) { $E = "now() + \'10 days\'"; }
    else {
        $E = "\'" . $E . "\'";
        $E = " cast($E AS TIMESTAMP) ";
    }

    $qry = "INSERT 
             INTO 
              messages 
              ( id, msg, msg_expires) VALUES ( ?, ?, $E )";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute( $I, $M );
    defined $rv or die "$!\n";

    $rv = $sth->finish();

    $dbh->disconnect;

    return 1;
}

sub send_Tmpl_Msg {

    shift;
    my ($I) = @_;
    shift;
    my ($T) = @_;
    shift;
    my ($E) = @_;

    unless ($I) { return undef; }

    unless ($T) { return undef; }

    $I =~ s/\D//g;

    $dbh = DB_Connect::DB_Connect('msg_sub_sys') || die "Cannot connect to the Database: $dbh->errstr $!\n";

    if ( !$E ) { $E = "now() + \'10 days\'"; }
    else {
        $E = "\'" . $E . "\'";
        $E = " cast($E AS TIMESTAMP) ";
    }

    $qry = "INSERT
             INTO
              messages
              ( id, msg_id, msg_expires) VALUES ( ?, ?, $E )";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute( $I, $T );
    defined $rv or return undef;

    $rv = $sth->finish();

    $dbh->disconnect;

    return 1;

}

sub expire_Messages {
    shift;
    my ($I) = @_;
    shift;
    my ($E) = @_;

    unless ($I) { return undef; }

    $I =~ s/\D//g;

    $dbh = DB_Connect::DB_Connect('msg_sub_sys') || die "Cannot connect to the Database: $dbh->errstr $!\n";

    if ( !$E ) { $E = "now()"; }
    else {
        $E = "\'" . $E . "\'";
        $E = " cast($E AS TIMESTAMP) ";
    }

    $qry = "DELETE 
             FROM
              messages 
            WHERE ( id = ? ) AND ( msg_expires < $E )";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute($I);
    defined $rv or return undef;

    $rv = $sth->finish();

    $dbh->disconnect;

    return 1;

}

sub message_Check
{
	shift;
    my ($I, $C, $db) = @_;

    my ($html, $unread, $mtotal,  @SEEN_SYS_MESSAGES, $unseen_sys_msgs) = ();

    return undef unless $I;

    if ( $C )
    {
     @SEEN_SYS_MESSAGES = split ( /X/, $C );

     $unseen_sys_msgs = get_unseen_sys_messages( $I, \@SEEN_SYS_MESSAGES );
    }
     else 
    {
     $unseen_sys_msgs = 0;
    }    

    $I =~ s/\D//g;

	###### if we received a DB handle, we won't bother making a new connection
	if ($db)
	{
		$dbh = $db;
	}
	else
	{
		$dbh = DB_Connect::DB_Connect('msg_sub_sys') ||
			die "Cannot connect to the Database: $dbh->errstr $!\n";
	}

    ###### Find unread (unviewed) records

    $qry = "	SELECT COUNT(*) AS unread
		FROM messages
		WHERE id = ? AND msg_viewed ISNULL";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute($I);
	unless (defined $rv)
	{
		$dbh->disconnect unless $db;
		return undef;
	}

    $data = $sth->fetchrow_hashref();

    $rv = $sth->finish();

    $unread = $data->{unread};

    ###### Find total messages waiting 

    $qry = "	SELECT COUNT(*) AS mtotal
		FROM messages
		WHERE id = ? OR id = 0";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute($I);
	unless (defined $rv)
	{
		$dbh->disconnect unless $db;
		return undef;
	}

    $data = $sth->fetchrow_hashref();

    $rv = $sth->finish();

    $mtotal = $data->{mtotal};

	$dbh->disconnect unless $db;

    $unread = $unread + $unseen_sys_msgs;

    if ( $unread > 0 ) {
        $html = <<E1;
<!-- Message Sub System  $VERSION -->
 <a href="/cgi/msg_sub_sys.cgi" target="_MBOX_"><img src="/images/envelope_an.gif" border="0" alt="$unread Messages Outstanding" width="21" height="20" /></a>
<!-- Message Sub System  $VERSION -->
E1
    }
    elsif ( $mtotal > 0 ) {
        $html = <<E2;
<!-- Message Sub System  $VERSION -->
 <a href="/cgi/msg_sub_sys.cgi" target="_MBOX_"><img src="/images/envelope.gif" border="0" alt="$mtotal Messages Present" width="21" height="20" /></a>
<!-- Message Sub System  $VERSION -->
E2
    }
    else {
        $html = <<E3;
<!-- Message Sub System  $VERSION -->
<!-- 0 -->
<!-- Message Sub System  $VERSION -->
E3
    }
    return $html;
}

1;

sub get_unseen_sys_messages {
    my ($I) = $_[0];
    my $arrayref = $_[1];

    my @array = @$arrayref;

    my @SYS;

    my $count;
    
    $count = 0;

    $dbh = DB_Connect::DB_Connect('msg_sub_sys') || die "Cannot connect to the Database: $dbh->errstr $!\n";

    ###### Find the outstanding system message oid's 

    $qry = "SELECT oid AS oid FROM messages WHERE ( id = 0)";

    $sth = $dbh->prepare($qry);

    $rv = $sth->execute;
    defined $rv or return undef;

    while ( $data = $sth->fetchrow_hashref() ) {
        push @SYS, $data->{oid};
        $count++;
    }

    $rv = $sth->finish();

    $dbh->disconnect;

    foreach my $o (@SYS) {
        foreach my $c (@array) {
            if ( $o == $c ) {
                $count = $count - 1;
            }
        }
    }

    return $count;
}

###### changed over to the global custom method for connecting to the database
###### fixed some incorrect tests for NULL '= null' has been replaced with ISNULL :)
###### 12/03/03 removed the alignment attribute from the envelope graphic definition
###### 05/26/04 added handling for receiving a DB handle instead of creating a new one
###### in the message_Check routine - also properly handled DB disconnects when returning
###### undef instead of just bailing out	
