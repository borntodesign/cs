package Authen_Constants;

use strict;
use Crypt::CBC;
use Exporter qw( import );

our @EXPORT_OK = qw/$secret_key cipher/;

our $secret_key = '9(7^%#=|';

sub cipher
{
	return Crypt::CBC->new('-key'=>$secret_key, '-cipher'=>'DES' );
}

1;