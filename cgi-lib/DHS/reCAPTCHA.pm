package DHS::reCAPTCHA;

use strict;
use LWP::Simple;
use JSON::Parse 'parse_json';

# public key is 6LeiWAETAAAAAKhK3DR-DNx2uXgrlYNtLe8e3KHo
my $secretKey = '6LeiWAETAAAAACdg0HWLvK_uXvXPk45LyGad7SMK';

my $SELF = ();

sub new
{
	if (! $SELF)
	{
		$SELF = bless {'error'=>undef, 'response'=>undef};
	}
	return $SELF;
}

sub check_answer
{
	my ($self, $resp, $remote_addr, $debug) = _self_or_default(@_);
	warn "response: $resp\nremote_addr: $remote_addr" if $debug;
	
	$self->{'raw_response'} = get("https://www.google.com/recaptcha/api/siteverify?secret=6LeiWAETAAAAACdg0HWLvK_uXvXPk45LyGad7SMK&response=$resp&remoteip=$remote_addr");
	warn $self->{'raw_response'} if $debug;
	
	unless ($self->{'raw_response'})
	{
		$self->{'error'} = 'No response from Google';
		return undef;
	}
	
	$self->{'response'} = parse_json($self->{'raw_response'});
	return 1 if $self->{'response'}->{'success'};
	return undef;
}

sub check_answer_using_cgi
{
	my ($self, $cgi) = _self_or_default(@_);
	if ((! $cgi) && ! $self->{'cgi'})
	{
		eval {
			use CGI;
			$self->{'cgi'} = new CGI;
		};
		
		if ($@)
		{
			$self->{'error'} = 'Failed to initialize an instance of CGI';
			return undef;
		}
	}
	elsif (! $self->{'cgi'})	# we have a $cgi argument
	{
		$self->{'cgi'} = $cgi;
	}
	return $self->check_answer($self->{'cgi'}->param('g-recaptcha-response'), $self->{'cgi'}->remote_addr);
}

sub error
{
	my $self = _self_or_default(@_);
	return $self->{'error'};
}

sub is_valid
{
	my $self = _self_or_default(@_);
	return 1 if $self->{'response'}->{'success'};
	return undef;
}

sub raw_response
{
	my $self = _self_or_default(@_);
	return $self->{'raw_response'};
}

sub _self_or_default
{
	return @_ if defined($_[0]) && (!ref($_[0])) && ($_[0] eq __PACKAGE__);

	###### I clipped this from CGI.pm
	unless (defined($_[0]) && (ref($_[0]) eq __PACKAGE__))
	{
		$SELF ||= new(__PACKAGE__);
		unshift(@_, $SELF);
	}
	return wantarray ? @_ : $SELF;
}

1;

=comment

The following comes off of Google's site

API Response

The response is a JSON object:

{
  "success": true|false,
  "error-codes": [...]   // optional
}

Error code reference
Error code 	Description
missing-input-secret 	The secret parameter is missing.
invalid-input-secret 	The secret parameter is invalid or malformed.
missing-input-response 	The response parameter is missing.
invalid-input-response 	The response parameter is invalid or malformed.

=cut