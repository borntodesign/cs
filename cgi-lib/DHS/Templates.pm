package DHS::Templates;

=head1 NAME:
ClassName

	Here we manage the retrieval, and processing our SHTML templates.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	This class uses the LWP library to fetch an SHTML page from 
	the path you provide, then process the template and it 
	should be ready to send to the screen.
	
	The templates place holders should be in upper case, and
	appended with '%%' and prepended with '%%' (ex. %%TITLE%%).

=head2
Code Example

	use DHS::Templates;
	use CGI;
	
	eval{
		my $CGI = CGI->new();
		my $DHS_Templates = DHS::Templates->new();
		
		my %template_values = (
									title=>"The title of the page", 
									page_content=>"Whatever the content of the page is.",
								);
		
		my $html_template = $DHS_Templates->retrieveShtmlTemplate('/path/to/the/template.shtml', $CGI->http('HTTP_ACCEPT_LANGUAGE'));
		
		print $CGI->header('text/html');
		print $DHS_Templates->processHtmlTemplate(\%template_values, $html_template);
	
	};
	if($@)
	{
		# However you are going to handle your errors.
		print CGI::header('text/html');
		print There was an error $@;
	}

=head1
PREREQUISITS:

	LWP::UserAgent

=cut

use strict;
use LWP::UserAgent;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	This is the constructor

=head4
Params:

	none

=head4
Returns:

	obj $self

=cut

sub new
{
    my $class			= shift;
    my $self			= {};
    bless($self, $class);
    $self->_init();
    return $self;
}

=head2
METHODS

=head2
PRIVATE

=head3
_init

=head4
Description:

	This sets the version, and the name.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $self = shift;

    $self->{VERSION}	= '0.1';
    $self->{NAME}		= 'Templates';

}

=head3
setLWPUserAgent

=head4
Description:

	This sets the LWP_UserAgent sub class, if it has not already been set.

=head4
Parameters:

	none

=head4
Return:

	undef

=cut

sub setLWPUserAgent
{

	my $self = shift;
	
	#TODO: Determin is I should use ref here if(!$self->{'LWP_UserAgent'} || ref($self->{'LWP_UserAgent'}) ne 'WHAT EVER THIS SHOULD BE')
	if (!$self->{'LWP_UserAgent'})
	{
	    $self->{'LWP_UserAgent'}	= LWP::UserAgent->new();
		$self->{'LWP_UserAgent'}->agent("DHS_Template_Retriever/0.1 ");
	}

	return;
	
}


=head2
PUBLIC

=head3
retrieveShtmlTemplate

=head4
Description:

	Pass in the Path, and the Language Preference, and
	this will return the SHTML template in the requested
	language, if that language is not available an English
	will be returned.

=head4
Parameters:

	path
	string
	the location, and name of the file to retrieve ex. "cs/_includes/templates/cs_template.shtml"
	
	language_pref
	string
	(optional) The language preference as defined in CGI::http('HTTP_ACCEPT_LANGUAGE'), 
	or just pass in the language code you desire in the order you desire them 'en,es,it', etc.
	
	host
	string
	(optional) the default is localhost
	
	protocol
	string
	(optional) http or https, http is the default

=head4
Return:

	string
	The page should be returned, if an error occured the error message will be returned ex. "404 Not Found".
	It's best to check the returned result for an error code ex. $return =~ /^\d+/

=cut

sub retrieveShtmlTemplate
{

	my $self = shift;
	my $path = shift;
	my $language_pref = shift;
	my $host = shift;
	my $protocol = shift;
	my $raw_content_flag = shift;
	
#	$host = 'localhost' if !$host;
	$host = '192.168.250.1' if !$host;
	$protocol = 'http' if !$protocol;
	
	$path =~ s/^\/// if $path;
	
	die "Templates::retrieveShtmlPage The path parameter is a required field, but it was not specified. \n" if ! $path;
	
	$protocol =~ s/^\///;
	$protocol =~ s/\/+$//;
	$protocol =~ s/:+//;
	
	$host =~ s/^\///;
	$host =~ s/\/+$//;
	
	
	
	$self->setLWPUserAgent();
	
	
	$self->{'LWP_UserAgent'}->default_header('Accept-Language' => $language_pref) if $language_pref;

	my $HTTP_Request = HTTP::Request->new('GET' => "$protocol://$host/$path");
	
	my $results = $self->{'LWP_UserAgent'}->request($HTTP_Request);

	# Check the outcome of the response
#	return $results->is_success ? $results->decoded_content(raise_error => 1): $results->status_line;
			
	if ($results->is_success)
	{
		return $results->content if $raw_content_flag;
		return $results->decoded_content('raise_error' => 1);

		my $content = $results->decoded_content(raise_error => 1 );
		use Encode;
		warn "is utf8: " . Encode::is_utf8($content, 1);
		return $content;
		return $results->decoded_content((charset => 'utf8', raise_error => 1 ));
	}
	else
	{
		return $results->status_line;
	}
	
}


=head3
processHtmlTemplate

=head4
Description:

	Replace the place holders with content.

=head4
Parameters:

	content
	hash
	The content where the key is the name of the place holder, and the value is the content to write.
	The place holders should be all uppercase, ex. %%PLACE_HOLDER%%.
	
	template
	string
	The template.
	
	skip_cleanup
	bool
	Set this to 1 if you want to skip the final removal of all unused Place Holders.

=head4
Return:

	string
	The page should be returned, if an error occured the error message will be returned ex. "404 Not Found".
	It's best to check the returned result for an error code ex. $return =~ /^\d+/

=cut

sub processHtmlTemplate
{

	my $self = shift;
	my $content = shift;
	my $template = shift;
	my $skip_cleanup = shift;
	
	die "Templates::processHtmlTemplate - The 'content' parameter is a required field, but it was not specified, or is not in the correct format. \n" if (! $content || ref($content) ne 'HASH');
	die "Templates::processHtmlTemplate - The 'template' parameter is a required field, but it was not specified. \n" if ! $template;
	

	foreach (keys %$content)
	{
		my $key = uc($_);
		$template =~ s/%%$key%%/$content->{$_}/g;
	}
	
	#Clean up any left over place holders.
	$template =~ s/%%.+%%//g if !$skip_cleanup;


	return $template;
}




1;

__END__

=head1
SEE ALSO

LWP::UserAgent

=head1
CHANGE LOG

	Date	What was changed

=cut

