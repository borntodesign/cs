package DHS::ConvertCurrency;

=head1 DHS::ConvertCurrency

Currency conversion/manipulation/display methods

The methods are named just like the DB functions they use.

=cut

use strict;
use Encode;

=head2 new

Instantiate a new DHS::ConvertCurrency object

=head3 Arguments

Arguments should be passed as key/value pairs.
A 'db' key is required which points to what is/will become an active DB handle

Optionally a country code, a language code and a currency code can be passed on the 'ccode' and 'lcode' keys respectively.
If these arguments are not passed, they will default to 'US', 'en', 'USD' respectively.
These can be reset using the ccode(), lcode() and currcode() methods.

=head2 Returns

A blessed object

=cut

sub new
{
	my ($class, %args) = @_;
	die "A 'db' argument is required" unless $args{'db'};
	$args{'ccode'} ||= 'US';
	$args{'lcode'} ||= 'en';
	return bless \%args, $class;
}

=head2 ccode() and lcode() and currcode()

Methods to set/retrieve the country code and the language code

=head3 Arguments

Pass in a new value to set the respective code to that value.

=head3 Returns

The existing value, before applying any new argument.

=cut

sub ccode
{
	my ($self, $newval) = @_;
	
	return $self->{'ccode'} unless $newval;
	my $rv = $self->{'ccode'};
	$self->{'ccode'} = $newval;
	return $rv;
}

sub lcode
{
	my ($self, $newval) = @_;
	
	return $self->{'lcode'} unless $newval;
	my $rv = $self->{'lcode'};
	$self->{'lcode'} = $newval;
	return $rv;
}

=head2 currcode()

Method to set the currency code.
Ordinarily you would do this in coordination with the country code as the currency formatting will be specific for a country, which has a particular currency.
This method is here more for internal use when doing exchanges and the currency code is not provided.

=head3 Arguments

Pass in a new value to set the respective code to that value.

=head3 Returns

The existing value, before applying any new argument.

=cut

sub currcode
{
	my ($self, $newval) = @_;
	
	return $self->{'currcode'} unless $newval;
	my $rv = $self->{'currcode'};
	$self->{'currcode'} = $newval;
	return $rv;
}

sub format_currency_by_locale
{
	my ($self, $val) = @_;
	my $rv = ();
	eval{ ($rv) = $self->db->selectrow_array('SELECT format_currency_by_locale(?,?,?)', undef, $val, $self->ccode, $self->lcode) };
	warn $@ if $@;
#	$rv = decode_utf8($rv); disabled 05/08/15
	return $rv;
}

sub format_number_by_locale
{
	my ($self, $val) = @_;
	my $rv = ();
	eval{ ($rv) = $self->db->selectrow_array('SELECT format_number_by_locale(?,?,?)', undef, $val, $self->ccode, $self->lcode) };
	warn $@ if $@;
	return $rv;
}

sub format_exchanged_currency
{
	my ($self, $val) = @_;
	$self->set_currcode_by_country() unless $self->currcode;
	my ($xr) = $self->db->selectrow_array('SELECT "rate" FROM exchange_rates_now WHERE "code"=?', undef, $self->currcode);
	return $self->format_currency_by_locale($xr * $val);
}

sub set_currcode_by_country
{
	my $self = shift;
	my $countrycode = shift;
	$self->ccode($countrycode) if $countrycode;
	my ($cc) = $self->db->selectrow_array('SELECT currency_code FROM currency_by_country WHERE country_code= ?', undef, $self->ccode);
	$self->currcode( $cc );
}

=head2 db

A simple routine to return the DBH

=cut

sub db
{
	my ($self, $db) = @_;

	$self->{'db'} = $db if $db;

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

1;