package DHS::CreditCard;
use strict;

=head2 DHS::CreditCard

	Begun as a simple class to provide a drop-down menu of credit card dates.

=head3 new

	A standard object instanciator.
	
=head4 Arguments

	An optional hashref can be passed with the following key/value pair that will be used:
	{
		'db'=> a active DB handle -or- a reference to an active DB handle -or- a code reference to a function that will return an active DB handle,
		'CGI'=> a CGI object
	}

	Naturally if a method is called in this class and the necessary resources are not available, like CGI for example, it will fail miserably.
	
=head4 Returns

	A new DHS::CreditCard object
	
=cut

sub new($;$)
{
	my ($class, $args) = @_;
	my %h = ();
	$h{'db'} = $args->{'db'} if $args->{'db'};
	$h{'CGI'} = $args->{'CGI'} if $args->{'CGI'};
	return bless \%h, $class;
}

=head3 cboCardExpiry

	Create an HTML <select> element with a set of date options 10 years into the future starting with this month.
	This method requires that you call new() with a CGI object
	
=head3 Arguments

	An optional hashref of arguments to define aspects of the select control.
	The hashref key/values will be passed directly to CGI to create the control, so use the same syntax that you would use to define a popup_menu with CGI.
	
	An optional hashref of other arguments that are not passed to CGI. The following key/value pairs are recognized:
	{
		'no_first_elem_empty' => 1,			# setting this to a true value will disable the default behavior to create an option at the top of the list with an empty value
		'first_elem_label' => 'some value'	# this will change the default empty option at the top of the list and set it's label to this value
	}

=head3 Returns

	An HTML <select> element populated with date options 10 years into the future starting with this month
	
=cut

sub cboCardExpiry
{
	my ($self, $ctlargs, $args) = @_;
	$ctlargs ||= {};
	$args ||= {};
	
	my @dateparts = localtime();
	my $currmonth = $dateparts[4] + 1;	# month from localtime is 0-11
	my $year = sprintf("%02d", $dateparts[5] % 100);

	my @vals = ();
	my %labels = ();
	
	unless ($args->{'no_first_elem_empty'})
	{
		push @vals, '';
		$labels{''} = $args->{'first_elem_label'} || '';
	}
	
	# we need to finish off our current year with the months remaining
	for (my $month = $currmonth; $month < 13; $month++)
	{
		$month = '0' . $month if length($month) == 1;	# we are going to want our month to be 01-12
		push @vals, ($month . $year);
		$labels{$month . $year} = $month . '/' . $year;
	}
	
	# now fill in full years ahead 10 years
	for (my $ny = $year; $ny < ($year +10); $ny++)
	{
		for (my $month = 1; $month < 13; $month++)
		{
			$month = '0' . $month if length($month) == 1;	# we are going to want our month to be 01-12
			push @vals, ($month . $ny);
			$labels{$month . $ny} = $month . '/' . $ny;
		}
	}
	
	return $self->CGI->popup_menu(
		%{$ctlargs},
		'-values' => \@vals,
		'-labels' => \%labels
	);
}

sub CGI
{
	my $self = shift;
	die "A CGI object is being requested but it apperently has not been setup when new() was called." unless $self->{'CGI'};
	return $self->{'CGI'};
}

1;
