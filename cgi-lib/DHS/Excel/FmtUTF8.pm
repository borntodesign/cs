package DHS::Excel::FmtUTF8;

use strict;
use parent 'Spreadsheet::ParseExcel::FmtDefault';
use Encode qw(decode encode);

# the super-class isn't very friendly to sub-classing, so we have to override this to make
# sure it's blessed into the right class
sub new {
       my $class = shift;
       my $enctype = shift || 'encode';
       return bless {'enctype'=>$enctype}, $class;
}

# the only other method we need to override...
sub TextFmt {
       my ($self,$data,$encoding) = @_;

       # Spreadsheet::ParseExcel will pass in the encoding to us!
       # or, it passes nothing in if it's iso-8859-1
#       $encoding ||= 'iso-8859-1';

       # we perform the decoding in a "fatal" manner so that if it fails,
       # we'll just pass the data back as-is
       my $decoded = eval { $self->{'enctype'} eq 'encode' ? encode('utf-8',$data,1) : decode('utf-8',$data,1) } || $data;
#       my $decoded = encode_utf8($data);
       return $decoded;
}

1;