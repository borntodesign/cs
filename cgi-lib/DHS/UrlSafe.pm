package DHS::UrlSafe;

=head1 NAME:
DHS::UrlSafe

	Make data safe for placing in a URL Query string, encode, and decode data

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	This class is originally created to handle passing a URL Safe Base 64 encoded Serialized Hash, or Array between pages.

=head2
Code Example

	#########
	# The Submit Page
	#########
	use DHS::UrlSafe;
	use CGI;
	
	my $CGI = {};
	my $DHS_UrlSafe = {};
	
	my %merchant_location_info = ();
	my $url = '';
	
	eval{
		
		$CGI = CGI->new();
		$DHS_UrlSafe = DHS::UrlSafe->new();
		
		
		$url = $CGI->url();
		$url =~ s/_submit\.cgi/\.cgi/;
		
		$merchant_location_info{address1} = $CGI->param('address1') if $CGI->param('address1');
		$merchant_location_info{address2} = $CGI->param('address2') if $CGI->param('address2');
		$merchant_location_info{address3} = $CGI->param('address3') if $CGI->param('address3');
		$merchant_location_info{city} = $CGI->param('city') if $CGI->param('city');
		$merchant_location_info{state} = $CGI->param('state') if $CGI->param('state');
		$merchant_location_info{country} = $CGI->param('country') if $CGI->param('country');
		$merchant_location_info{postalcode} = $CGI->param('postalcode') if $CGI->param('postalcode');
		$merchant_location_info{phone} = $CGI->param('phone') if $CGI->param('phone');
		($merchant_location_info{username} = $CGI->param('username')) =~ s/\s//g if $CGI->param('username');# A username is required
		($merchant_location_info{password} = $CGI->param('password')) =~ s/\s//g if $CGI->param('password');# A password is required
		
		$url .= "?data=" . $DHS_UrlSafe->encodeHashOrArray(\%merchant_location_info);
		print $CGI->redirect(-uri=>"$url");
		
	};
	if($@)
	{
		# How ever you want to handle any potential exceptions.
	}


	#########
	# The Submit Page
	#########
	use DHS::UrlSafe;
	use CGI;
	use Data::Dumper;
	
	
	my $CGI = {};
	my $DHS_UrlSafe = {};
	
	
	my $merchant_location_info = {};
	
	
	eval{
		
		$CGI = CGI->new();
		$DHS_UrlSafe = DHS::UrlSafe->new();
		
		$merchant_location_info = $DHS_UrlSafe->decodeHashOrArray($CGI->param('data')) if $CGI->param('data');
		
		print $CGI->header('text/plain');
		print "merchant_location_info: \n" . Dumper($merchant_location_info);
		
	};
	if($@)
	{
		# How ever you want to handle any potential exceptions.
	}

	

=head1
PREREQUISITS:

	strict, Storable, MIME::Base64::URLSafe

=cut

use strict;
use Storable;
use MIME::Base64::URLSafe;
use URI::Escape;
use Encode qw(decode_utf8 encode_utf8);

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	$attribute_name	string	What this attributes purpose is.

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $this			= {};
    bless($this, $class);
    $this->_init();
    return $this;
}

=head2
METHODS

=head2
PRIVATE

=head3
_init

=head4
Description:

	Set some standard Attributes.

=cut

sub _init
{

	my $this = shift;

    $this->{version}	= '0.1';
    $this->{name}		= 'UrlSafe';

}


=head2
PUBLIC

=head3
decode

=head4
Description:

	Decode the passed in URL Safe Base 64 encoded scalar into a human readable scalar, or serialized data

=head4
Parameters:

	data	scalar	A scalar with the data to decode.

=head4
Return:

	Scalar, or serialized data

=cut

sub decode
{

	my $this = shift;
	my $data = shift;
	
	return MIME::Base64::URLSafe::decode($data);
	
}


=head3
encode

=head4
Description:

	Encode the passed in scalar, or serialized data into a URL Safe Base 64 encoded scalar

=head4
Parameters:

	data	scalar	A scalar with the data to encode.

=head4
Return:

	URL Safe string

=cut

sub encode
{

	my $this = shift;
	my $data = shift;
	
	return MIME::Base64::URLSafe::encode($data);
	
}

=head3
encodeUrlPart

=head4
Description:

	This is ment to be run on part of a url not the whole URL.
	
	my $DHS_UrlSafe = DHS::UrlSafe->new();
	my @url = ();
	push @url, $DHS_UrlSafe->encodeUrlPart('What ever you have');
	push @url, $DHS_UrlSafe->encodeUrlPart('What ever you have next');
	
	my $url_for_location = join('/', @url); # "What_ever_you_have/What_ever_you_have_next"

=head4
Parameters:

	data	scalar	A scalar with the data to encode.

=head4
Return:

	URL Safe string

=cut

sub encodeUrlPart
{

	my $this = shift;
	my $data = shift;
	
	$data =~ s/\ /_/g;

	if (Encode::is_utf8($data) )
	{
#warn "encodeUrlPart utf8? $data : ".Encode::is_utf8($data);
#warn "length: " .length($data);
		my $d = encode_utf8($data);
#warn "after encoding: $d : ".Encode::is_utf8($d);
#warn "length: " .length($d);
my $t = $data;
Encode::_utf8_off($t);
#warn "turn of flag: $t , length: " . length($t);
#		return URI::Escape::uri_escape($d) ; #URI::Escape::uri_escape($data , "\0-\377");
	}
	my $rv = ();
	eval { $rv = URI::Escape::uri_escape($data) };
	if ($@)
	{
		warn "error for uri_escape: $@ /nrv: $rv";
		$rv = URI::Escape::uri_escape_utf8($data);
		warn "rv after: $rv";
	}
	return $rv;
}

=head3
decodeUrlPart

=head4
Description:

	This is ment to be run on part of a url not the whole URL.
	
	my $DHS_UrlSafe = DHS::UrlSafe->new();
	$CGI->url(-absolute=>1);
	
	my @url = ();
	@url, $DHS_UrlSafe->decodeUrlPart('What ever you have');

	
	my $url_for_location = join('/', @url); # "What_ever_you_have/What_ever_you_have_next"

=head4
Parameters:

	data	scalar	A scalar with the data to encode.

=head4
Return:

	URL Safe string

=cut

sub decodeUrlPart
{

	my $this = shift;
	my $data = shift;
	
	
	$data = URI::Escape::uri_unescape($data);
	$data =~ s/_/\ /g;
	
	return $data;
	
}

=head3
encodeHashOrArray

=head4
Description:

	Encode a Hash Reference, or an Array Reference and return URL safe data.
	First the Hash Reference, or Array Reference is Serialized, then the 
	Serialized data is run through a URL Safe Base 64 encoding algorith.
	To decode the output, an put it back into it's original structure use 
	"decodeHashOrArray".

=head4
Parameters:

	hashref_or_arrayref_to_encode	hashref, or arrayref	A Hash Reference, or an Array Reference to encode.

=head4
Return:

	URL Safe string

=cut

sub encodeHashOrArray
{

	my $this = shift;
	my $hashref_or_arrayref_to_encode = shift;
	
	my $return = '';
	
	
	die 'Only ARRAY References, or Hash References are accepted as a parameter. $hashref_or_arrayref_to_encode: ' . $hashref_or_arrayref_to_encode . ' - ref($hashref_or_arrayref_to_encode): ' . ref($hashref_or_arrayref_to_encode) if ref($hashref_or_arrayref_to_encode) !~ /ARRAY|HASH/;
	
	return $this->encode(Storable::freeze($hashref_or_arrayref_to_encode));
	
}

=head3
decodeHashOrArray

=head4
Description:

	Decode URL Safe Base 64 encoded Serialized Array Reference, or Hash Reference
	as returned by "encodeHashOrArray".

=head4
Parameters:

	base64_encoded_serialized_data	string	URL Safe Base 64 Encoded Serialized Array Reference, or Hash Reference.

=head4
Return:

	An Array Reference, or a Hash Reference

=cut

sub decodeHashOrArray
{

	my $this = shift;
	my $base64_encoded_serialized_data = shift;
	
	die 'A URL Safe Base 64 encoded Serialized Array Reference, or Hash Reference was expected, but nothing was passed in.' if ! $base64_encoded_serialized_data;
	
	return Storable::thaw($this->decode($base64_encoded_serialized_data));
	
}



=head3
parseURL

=head4
Description:

	Describe what this method does

=head4
Parameters:

	unprocessed_url
	string
	The URL as provided by "CGI::url(-absolute=>1)".
	
	substitute_underscore
	bool
	Set to 1 if you want all the underscores in the URL to converted to a space.

=head4
Return:

	hash	(
				module	string,
				control	string,
				params	hashref	{
										key => value,
										or
										key => [
													value,
													value,
													value
												]
									}
			)

=cut

sub parseURL
{
	
	my $self = shift;
	my $unprocessed_url = shift; #$self->{CGI}->url(-absolute=>1);
	my $substitute_underscore = shift;
	my %url_parts = ();
	my @params = ();
	
	
	@params = $self->processURL($unprocessed_url, $substitute_underscore);
	
	$url_parts{module} = shift(@params);
	$url_parts{module} = $url_parts{module};
	
	$url_parts{control} = shift(@params);
	$url_parts{control} = $url_parts{control};
	
	$url_parts{params} = $self->processParams(\@params);
	
	return %url_parts;
	
}

=head3
parseURL

=head4
Description:

	Describe what this method does

=head4
Parameters:

	unprocessed_url
	string
	Description of what this holds
	
	substitute_underscore
	bool
	Set to 1 if you want all the underscores in the URL to converted to a space

=head4
Return:

	string on error, or null if all goes well.
=cut

sub processURL
{
	
	my $self = shift;
	my $unprocessed_url = shift; #$self->{CGI}->url(-absolute=>1);
	my $substitute_underscore = shift;
	my %url_parts = ();
	my @params = [];
	
#warn "processURL the URL: $unprocessed_url";
	
	$unprocessed_url =~ s/^\///;
	$unprocessed_url =~ s/\/$//;
	$unprocessed_url =~ s/\/{2,}/\//;
	$unprocessed_url =~ s/_/\ /g if($substitute_underscore);
	
	$unprocessed_url = URI::Escape::uri_unescape($unprocessed_url);
	@params = split(/\//, $unprocessed_url);

# disabled this on 10/02/15... not sure why we would want to dick around with the data instead of passing it through as received
# whatever the case may be, encoding utf-8 data, that does not have the utf-8 flag on since it came through a URL (already encoded),
# breaks the ability to reconstitute it later unless you know it is doubly encoded ;-<
# if I did anything, I would decode_utf8 that data, thereby turning the flag on, but not today, don't know what it might break..... Bill
#	for (my $x=0; $x < scalar @params; $x++)
#	{
#		$params[$x] = encode_utf8($params[$x]);
#	}
	
	return @params;
}


=head3
processParams

=head4
Description:

	Describe what this method does

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub processParams
{
	my $self = shift;
	my $params = shift;
	
	my $processed_params = {};
	
	my $key = undef;
	
	foreach (@$params)
	{
		next if (!defined $key && $_ =~ /^\s*$/);
		
		if (! defined $key)
		{
			$key = $_;
			$processed_params->{$key} = undef if(! exists $processed_params->{$key});
		}
		else
		{
			if($processed_params->{$key} eq undef)
			{
				$processed_params->{$key} = $_;				
			}
			elsif(ref($processed_params->{$key}) eq 'ARRAY')
			{
				push(@{$processed_params->{$key}}, $_);
			}
			else
			{
				$processed_params->{$key} = [$processed_params->{$key}];
				push(@{$processed_params->{$key}}, $_);
			}

			$key = undef;	
		}
		
	}

	return $processed_params;
	
}




1;

__END__

=head1
SEE ALSO

strict, Storable, MIME::Base64::URLSafe, URI::Escape

=head1
CHANGE LOG

	Date	What was changed
	03/24/11	Bill MacArthur
		Added encode_utf8 to processURL as the rewardsdirectory script places the URL encoded cities
		in the path as single characters which then confuse the module that is used somewhere in the Sanitize class
		This whole thing may need some real attention if something else breaks because of this change.... :(

=cut

