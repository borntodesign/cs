package csmLogin;

use strict;
use CGI;
use Date::Calc qw( Today_and_Now Add_Delta_DHMS );
use Digest::MD5 qw( md5_hex );
use URI::Escape qw(uri_escape);
use CS_Authen_Constants qw/$secret_key cipher $LoginScript/;
#use Data::Dumper;	# only for debugging

=head2 csLogin

A class to handle authentication and cookie management

=cut

=head3 authenticate($$)

=head4 Arguments

	username (ID/alias)
	password
	
=head Returns

	Hash Reference to the members record and other profile aspects as to be determined
	Of course, if this is a staff login, the hash will largely be fabricated.
	It should nonetheless mimic the standard hash returned.
	
	This method also returns an error code such as when authentication fails.
	The caller can handle this situation on its own, but a standard error code will be available.
	
=cut

sub authenticate($$$$)
{
	my ($self, $user, $pwd, $merchid, $store) = @_;
	my $qry = "";
	#$user = uc($user);
	if ($store eq "maf")
	{
		$qry = "SELECT ma.password, ma.username, ma.id as merch_id, ma.business_name as firstname, me.lastname, me.emailaddress,
					me.membertype, me.id, me.spid, ma.status, ma.member_id from merchant_affiliates_master ma
				JOIN members me on me.id = ma.member_id
				WHERE ma.username = ? AND ma.password = ? and ma.id = ?;";
	}
	else
	{
		$qry = "SELECT ma.password, ma.id, ma.location_name as firstname, ma.id as lastname, ''::TEXT as emailaddress, 
					'$store'::TEXT as membertype, ''::TEXT as spid, v.status, ''::TEXT as member_id 
				FROM merchant_affiliate_locations ma
				LEFT JOIN vendors v
					ON ma.vendor_id = v.vendor_id
				WHERE ma.username = ?
				AND ma.password = ?
				AND ma.merchant_id = ?;";
	}
#	warn "Authenticate query: " . $qry;
	my $rv = $self->{'db'}->selectrow_hashref($qry, undef, $user, $pwd, $merchid);
	my $error = undef;
	unless ($rv)
	{
		$error = 3;
		warn "Login failed for id: $user , pwd: $pwd, \n";
		return ($rv, $error);
	}
	
	$rv->{'store'} = $store;
	$rv->{'merch_id'} = $merchid;
	# we will add in our other checks here when they are defined
	return ($rv, $error); 
}

=head3 Create_CS_Authen_General_Ticket($)

Take a member hash reference and create a ticket useful in a AuthCustom_Generic or AuthCustom_maf cookie

=head4 Arguments

	A hash reference the likes of that returned by authenticate()

=head4 Returns

	A ticket string ready for insertion in an HTTP header cookie

=cut

sub Create_CS_Authen_General_Ticket($$;$)
{
	my ($self, $row, $error) = @_;
#warn "Create_CS_Authen_General_Ticket: " . Dumper($row);
	return ($row, $error) unless $row && ! $error;

	# Create the expire time for the ticket.
	my $expire_time = sprintf(
			'%04d-%02d-%02d-%02d-%02d-%02d',
			Add_Delta_DHMS( Today_and_Now, '00', '12', '00', '00'));

	# OK, now we stick the username and the current time and the expire
	# time together to make the public part of the session key:
	my $current_time = _now_year_month_day_hour_minute_second();

	my $public_part = "$row->{id}:$current_time:$expire_time:";
	$public_part .= uri_escape( $row->{'username'} || $row->{'alias'} );
	$public_part .= ":$row->{id}:";
	$public_part .= uri_escape( $row->{'firstname'} ) . ":";
   	$public_part .= uri_escape( $row->{'lastname'} ) . ":";
   	$public_part .= uri_escape( $row->{'emailaddress'} );
   	$public_part .= ":$row->{'membertype'}";
   	$public_part .= ":$row->{'spid'}";
   	
   	# we could put serialized data in here if a flexible sore if we want to later on
   	# and not have to worry about breaking the cookie structure
	$public_part .= ":{}";

	# Now we calculate the hash of this and the secret key and then
	# calculate the hash of *that* and the secret key again.
	my $hash = md5_hex( join ':', $secret_key, md5_hex(
		join ':', $public_part, $secret_key
	) );

	# Now we add this hash to the end of the public part.
	my $session_key = "$public_part:$hash";

	# Now we encrypt this and return it.
	my $encrypted_session_key = cipher->encrypt_hex( $session_key );
#warn "encrypted_session_key: $encrypted_session_key"; 
	return $encrypted_session_key;
}

=head3 Generate_CS_Authen_General_Cookie($)

Take a member hash reference and create a CS_Authen_General cookie using CGI::Cookie

=head4 Arguments

	A hash reference the likes of that returned by authenticate()

=head4 Returns

	A cookie string ready for insertion in an HTTP header

=cut

sub Generate_CS_Authen_General_Cookie($)
{
	my ($self, $hr) = @_;
	my $ck = $self->{'cgi'}->cookie('-name'=>'AuthCustom_Generic', '-value'=>$self->Create_CS_Authen_General_Ticket($hr),  '-path'=>'/', '-domain'=>'.clubshop.com');
#warn "created cookie: $ck";
	return $ck;
}

sub Generate_CSM_Authen_MAF_Cookie($)
{
	my ($self, $hr) = @_;
	my $ck = $self->{'cgi'}->cookie('-name'=>'AuthCustom_maf', '-value'=>$self->Create_CS_Authen_General_Ticket($hr), '-path'=>'/');
#warn "created maf cookie: $ck";
	return $ck;
}

=head3 new

=head4 Arguments

Parameters should be passed in as a hash reference.
An active DB handle is required if authentication is to occur against the DB.
Furthermore a CGI object should be passed in if wanting to create cookies or utilize other methods that require it.
This can be passed in a 'cgi' or 'CGI' key

example: my $giL = csLogin->new({'db'=>$dbh, 'CGI'=>$cgi});

=cut

sub new
{
	my $args = shift;
	$args = shift unless ref $args;	# if we receive a classname as the first argument, skip it
	$args->{'cgi'} = $args->{'CGI'};
	return bless $args || {};
}

=head3 StaffLogin

=head4 Arguments

	username
	password

=head4 Returns

	A hash reference that mimics the return of the authenticate method

=cut

sub StaffLogin($$)
{
	my ($self, $id, $pwd) = @_;
	my $rv = $self->{'db'}->selectrow_hashref(q|
		SELECT
				usename AS username,
				'01'::TEXT AS id,
		--		?::TEXT AS firstname,
				'staff' AS firstname,
				'staff' AS lastname,
				''::TEXT AS emailaddress,
				'staff'::TEXT AS membertype,
				''::TEXT AS spid
		FROM staff_vw
		WHERE usename = ?
		AND SUBSTR(passwd,4) = ?|, undef, $id, $pwd);
	
	# if we need to add other 'profile' stuff in, it will go here
	return ($rv, ($rv ? undef : 3));
}

#-------------------------------------------------------------------------------
# _now_year_month_day_hour_minute_second -- Return a string with the time in
# this order separated by dashes.

sub _now_year_month_day_hour_minute_second()
{
	return sprintf '%04d-%02d-%02d-%02d-%02d-%02d', Today_and_Now;
}

sub _v
{
	my ($self, $member) = @_;
	
	# we really shouldn't be called unless there is a member record, so we won't test for it's validity
	my ($status) = $self->{'db'}->selectrow_array("SELECT status FROM cancellations WHERE id= $member->{id}");
	return ($member, undef) unless $status;
	my %map = (
		'sp' => 5,
		'sa' => 6,
		'tp' => 7,
		'tw' => 8
	);
	# the map value would be undefined for a non-existent key, but by doing the OR, we avoid an uninit'd var error
	return ($member, $map{$status} || ());
}

1;
