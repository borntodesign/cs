package ActivityReport::ar12;
use base ActivityReport::AR;

my $tmpl = 11027;

sub ReturnText($$$$)
{
	my ($self, $me, $up, $lang_pref) = @_;

	my $node = 	
		$self->isMyCoach($me, $up) ? 'coach' :
		$self->isMySponsor($me, $up) ? 'sponsor' :
		$self->isMyExec($me, $up) ? 'exec' :
		$self->isMyDirector($me, $up) ? 'director' :
		$self->isOtherDirector($me, $up) ? 'other_director' : 'default';

	return ('text/html', $self->GetNodeFromTemplate($tmpl, $lang_pref, $node) );	# this will return an empty string if the node is not found
}

1;