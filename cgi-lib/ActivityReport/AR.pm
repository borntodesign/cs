package ActivityReport::AR;
use strict;
require XML::local_utils;

my (%NRCache) = ();

=head2 new

	Instantiate a new ActivityReport::AR object

=head3 Arguments

	Expects a hashref with at least a 'db' key pointing to an active DB handle, and a 'cgi' key pointing to a CGI object
	
=cut

sub new
{
	my ($class, $args) = @_;
	die "A hashref with 'db', 'cgi' and 'G_S' keys, which must have values, must be passed as an argument to new()" unless $args->{'db'} && $args->{'cgi'} && $args->{'G_S'};

	my $self = bless $args, $class;
	
	return $self;
}

sub cgi
{
	return $_[0]->{'cgi'};
}

sub db
{
	return $_[0]->{'db'};
}

sub GetNodeFromTemplate($$$$)
{
	my ($self, $tmpl, $lang_pref, $nodename) = @_;

	my $xhtml = $self->{'G_S'}->GetObject($tmpl, $lang_pref) || die "Failed to obtain template: $tmpl";

	$xhtml = XML::local_utils::flatXHTMLtoHashref($xhtml);

	return \$xhtml->{$nodename} || '';
}

sub NRCache($$$)
{
	my ($self, $id, $key) = @_;
	
	return $NRCache{$id}->{$key} if $NRCache{$id}->{$key};
	
	if ($key eq 'mycoach')
	{
		($NRCache{$id}->{'mycoach'}) = $self->db->selectrow_array('SELECT upline FROM my_coach WHERE id= ?', undef, $id);
	}
	elsif ($key eq 'mysponsor')
	{
		($NRCache{$id}->{'mysponsor'}) = $self->db->selectrow_array('SELECT spid FROM members WHERE id= ?', undef, $id);
	}
	elsif ($key eq 'myexec')
	{
		($NRCache{$id}->{'myexec'}) = $self->db->selectrow_array('SELECT id FROM my_exec(?)', undef, $id);
	}
	elsif ($key eq 'myteam')
	{
		($NRCache{$id}->{'myteam'}) = $self->db->selectall_arrayref('SELECT * FROM network.my_partner_team(?)', undef, $id);
	}
	elsif ($key eq 'isa_director')
	{
		($NRCache{$id}->{'isa_director'}) = $self->db->selectrow_array('SELECT id FROM a_level_stable WHERE a_level >= 8 AND id= ?', undef, $id);
	}
	else
	{
		warn "NRCache key: $key is unrecognized";
	}
	
	return $NRCache{$id}->{$key};
}

sub isMyCoach($$$)
{
	my ($self, $me, $up) = @_;
	return $self->NRCache($me, 'mycoach') == $up ? 1 : ();
}

sub isMyDirector($$$)	#  (Coach's coach if not the Sponsor or Coach) 
{
	# remember that if my coach is a director, we will have been satisfied already by isMyCoach
	my ($self, $me, $up) = @_;
	return undef if $self->isMySponsor($me, $up) || $self->isMyCoach($me, $up);
	
	# first we get the coach of the downline and then we get that party's coach
	return $self->NRCache( $self->NRCache($me, 'mycoach'), 'mycoach') == $up ? 1 : ();
}

sub isMyExec($$$)
{
	my ($self, $me, $up) = @_;
	return $self->NRCache($me, 'myexec') == $up ? 1 : ();
}

sub isOtherDirector($$$)
{
	my ($self, $me, $up) = @_;
	
	# naturally I must be a director to start with... then I must not be my coach, sponsor or exec, or I wouldn't be "other"
	return () if (
		(! $self->NRCache($up, 'isa_director')) ||
		$self->isMyCoach($me, $up) ||
		$self->isMyExec($me, $up) ||
		$self->isMySponsor($me, $up));
	return (grep {$_->[0] == $me} @{ $self->NRCache($up, 'myteam') }) ? 1 : ();
}

sub isMySponsor($$$)
{
	my ($self, $me, $up) = @_;
	return $self->NRCache($me, 'mysponsor') == $up ? 1 : ();
}

1;