#!/usr/bin/perl -w
use DBI;
use strict;
use CGI;
use Test::More qw(no_plan);
use lib ('c:/temp/clubshop/cgi-lib', 'c:/temp', '/home/httpd/cgi-lib');
use G_S;

BEGIN { use_ok('ActivityReport::AR') };
can_ok('ActivityReport::AR', ('new'));

my $q=new CGI;
my $db= DBI->connect("DBI:Pg:dbname=network", "bill","annexoria") || die "DB connect failed";
my $gs = G_S->new({'db'=>$db});
my $acttype = 12;
eval( "use ar$acttype;" );
my $ar = ();
$ar = "ActivityReport::ar$acttype"->new({'db'=>$db, 'cgi'=>$q, 'G_S'=>$gs});

isa_ok($ar, 'ActivityReport::AR');

my %scenarios = scenarios();

#foreach my $k (keys %scenarios)
#{
#	foreach my $ok (keys %{$scenarios{$k}->{'testsOK'}})
#	{
#		my $up = $scenarios{$k}->{'testsOK'}->{$ok};
#		is($ar->$ok($k, $up), 1, "$ok : testsOK: $k - $up");
#	}
#
#	foreach my $ok (keys %{$scenarios{$k}->{'testsNotOK'}})
#	{
#		my $up = $scenarios{$k}->{'testsNotOK'}->{$ok};
#		is($ar->$ok($k, $up), undef, "$ok : testsNotOK: $k - $up");
#	}
#}

#$ar->SetActtype(12);

can_ok("ActivityReport::ar12", ('ReturnText'));
print 'ReturnText: ' . $ar->ReturnText(4985683,111,'it') ."\n";
$db->disconnect;

sub scenarios
{
	return
	(
	4985683 => {
		'testsOK' => {
			'isMySponsor' => 111,
			'isMyCoach' => 111,
			'isMyDirector' => 3077498,
			'isMyExec' => 3077498,
			'isOtherDirector' => 5803
		},
		'testsNotOK' => {
			'isMySponsor' => 810096,
			'isMyCoach' => 810096,
			'isMyDirector' => 810096,
			'isMyExec' => 810096,
			'isOtherDirector' => 810096
		}
	},
	5115502 => {
		'testsOK' => {
			'isMySponsor' => 4736384,
			'isMyCoach' => 5137489,
			'isMyDirector' => 4709268,
			'isMyExec' => 4709268,
			'isOtherDirector' => 5172773
		},
		'testsNotOK' => {
			'isMySponsor' => 810096,
			'isMyCoach' => 4736384,
			'isMyDirector' => 810096,
			'isMyExec' => 810096,
			'isOtherDirector' => 4709268
		}
	}
);
}