package UseLexicon;
###### last modified: 11/16/04	Bill MacArthur
											
use strict;
use base 'Locale::Maketext';
use Locale::Maketext::Lexicon::Gettext;
use Locale::Maketext::Lexicon {en => ['Auto']};
require Exporter;

our @ISA = qw/Exporter Locale::Maketext/;
our @EXPORT_OK = qw/print_/;
my $DEFAULTclass = 'UseLexicon';

our ($Q, %lexicon, $lh, $language) = ();

sub new
{
	my $class = shift;
	return bless ({}, $class);
}

sub init
{
	###### reinitialize - this is important for the mod_perl environment
	($Q, %lexicon, $lh, $language) = ();

	my ($self, $db, $G_S, $lang_spec) = _self_or_default(@_);

	die "DB handle required" unless $db;

	###### if $lang_spec is not defined then we need G_S to determine it
	###### if G_S is undefined, then we need to create a new instance
	if ((! $lang_spec) && (! $G_S))
	{
		use lib('/home/httpd/cgi-lib');
		use G_S;
		$G_S = new G_S;
	}

	my $rv = $db->selectall_hashref("
		SELECT language, pk FROM lexicons WHERE active= TRUE
	", 'language');

	my ($pk) = ();

	if ($lang_spec)
	{
		###### if we have received a language specification
		###### we will not rely on browser settings
		$pk = $rv->{$lang_spec}{pk} || $rv->{en}{pk};
	}
	else
	{

	###### first loop through the language prefs and see if we have a match
	###### since English will be our default using 'AUTO'
	###### we'll jump out even if we don't have a lexicon that matches
	###### this is assuming that the base document is not simply tags for replacement
		foreach (@{$G_S->{language_prefs}})
		{
			$pk = $rv->{$_->[0]}{pk};
			$lang_spec = $_->[0];
			last if ($pk || $_->[0] eq 'en');
		}

	###### failing the primary's let's look through the primary portions of the prefs
		unless ($pk)
		{
			foreach (@{$G_S->{language_prefs}})
			{
				$pk = $rv->{$_->[1]}{pk};
				$lang_spec = $_->[1];
				last if ($pk || $_->[1] eq 'en');
			}
		}

	###### if we got to here without a $pk value,
	###### I guess we don't have foreign translations for this object
		unless ($pk)
		{
			$pk = $rv->{en}{pk};
			$lang_spec = '' unless $lang_spec eq 'en';
		}
	}

	if ($pk)
	{
		$rv = $db->selectrow_array("
			SELECT value FROM lexicons WHERE pk= $pk
		");

		%lexicon = %{ Locale::Maketext::Lexicon::Gettext->parse(split /\n/, $rv) };
		Locale::Maketext::Lexicon->import({$lang_spec => ['Simple' => [\%lexicon]]});
		$lh = __PACKAGE__->get_handle($lang_spec) or die "failed to create lh\n";

		$lh->fail_with( '_lex_fail' );
	}
	else
	{
		$lang_spec = '' unless $lang_spec eq 'en';
	}

	$language = $lang_spec,
	return;
}

sub print_
{
	my ($self, @txt) = _self_or_default(@_);
	foreach (@txt){ print $self->translate($_) }
}

sub translate
{
	my ($self, $_txt) = _self_or_default(@_);

	###### if we received a ref, then we are translating the value in place
	###### since we don't want to deref our argument until we are operating on it,
	###### we'll convert a non ref to a ref and deal with the ref below uniformly
	my $txt = (ref $_txt) ? $_txt : \$_txt;
	if ($$txt)
	{
	###### if there is no language spec, then we are done
	###### we may as well do the same for an 'en' lang_spec
		if ($language && $language ne 'en')
		{
			foreach (sort {length $b <=> length $a} keys %lexicon)
			{
				$$txt =~ s/($_)/${\$lh->maketext($1)}/gs;
			}
		}
	}
	return (ref $_txt) ? undef : $$txt
}

sub _lex_fail
{
	my($failing_lh, $key, @params) = @_;

#    print LEX_FAIL_LOG scalar(localtime), "\t",
#    print scalar(localtime), "\t",
#       ref($failing_lh), "\t", $key, "\n";
#    return $lh->maketext($key,@params);
	return $key;
}

sub _self_or_default
{
	return @_ if defined($_[0]) && (!ref($_[0])) && ($_[0] eq $DEFAULTclass);

	###### I clipped part of this from CGI.pm
	unless (defined($_[0]) && (ref($_[0]) eq $DEFAULTclass))
	{
		$Q ||= new("$DEFAULTclass");
		unshift(@_, $Q);
	}
	return @_;
}
						
1;

###### 11/05/04 made the init routine reinitialize the globals
###### made the translate routine able to handle scalar refs to avoid the penalty
###### of passing large amounts of data back and forth
###### 11/09/04 changed the return handling in translate to return the arg if it was empty
###### 11/16/04 added a test for 'ACTIVE' lexicons