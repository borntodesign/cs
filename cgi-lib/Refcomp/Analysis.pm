package Refcomp::Analysis;
use strict;
use base 'Refcomp';
require XML::LibXML;

# last modified: 10/10/14	Bill MacArthur

=head2 Refcomp::Analysis

	This class started out of the need to obtain and use data contained within the analysis column in the refcomp table.
	Refer to the current overview of the XML document at the end of this file.

=head3 init

	Setup our dataset from the XML.
	Better to do this once and reuse rather than have to parse it everytime.
	Of course, if this method is called more than once, the data will be reinitialized.
		
=head4 Arguments

	An XML string.
	
=head4 Returns

	A XML::LibXML->new->parse_string object of the XML passed in
	
=cut

my %Cache = ();

sub init($)
{
	my ($self, $xml) = @_;
	warn 'XML argument is undefined' unless $xml;
	$xml ||= '<root/>';

	# reinitialize the Cache totally
	%Cache = ();

	$Cache{'LibXML'} = XML::LibXML->new->parse_string($xml);
	return $Cache{'LibXML'};
}

=head3 ckXmlCache

	See if the cache has been populated and if not, provide a warning.
	
=head4 Arguments

	None
	
=head4 Returns

	1 for True -or- 0 for False
	
=cut

sub CkXmlCache()
{
	return 1 if $Cache{'LibXML'};
	warn 'Our XML hashref cache is undefined. Did you call init()?';
	return 0;
}

=head3 dnIdList

	Get a list of IDs under the specified node
	
=head4 Arguments

	A text argment to specify the node under which to gather the IDs... currently 'm25' or 'm50'
	
=head4 Returns

	A reference to an array of IDs
	
=cut

sub dnIdList
{
	my ($self, $nodeName) = @_;
	unless ( $Cache{'dnIdList'}->{$nodeName} )
	{
		my @rv = ();
		foreach my $node ($Cache{'LibXML'}->findnodes("/me/$nodeName/dn"))
		{
			push @rv, $node->getAttribute('id');
		}
		
		$Cache{'dnIdList'}->{$nodeName} = \@rv;
	}
	
	return $Cache{'dnIdList'}->{$nodeName};
}

=head3 dnNodeList

	Just a wrapper around the other dnNodeListMxx methods to facilitate being able to call the correct function by virtue of an argument name instead of explicitly

=head4 Arguments

	The node name you want the nodes from... that would be either m25 or m50
	
=head4 Returns

	A hashref of nodes resembling the hashref that can be seen in the comments at the end

=cut

sub dnNodeList($)
{
	my ($self, $nodeName) = @_;
	
	return
		$nodeName eq 'm50' ? $self->dnNodeListM50 :
		$nodeName eq 'm25' ? $self->dnNodeListM25 :
		die "Invalid argument. Valid arguments are 'm50' or 'm25'";
}

=head3 dnNodeListM25

	Get the "dn" (downline) nodes under the m25 node

=head4 Arguments

	None

=head4 Returns

	A hashref of nodes resembling the hashref that can be seen in the comments at the end

=cut

sub dnNodeListM25()
{
	my ($self) = @_;
	return $Cache{'m25'} if $Cache{'m25'};
	
	my %rv = ();
	foreach my $node ($Cache{'LibXML'}->findnodes("/me/m25/dn"))
	{
		my $id = $node->getAttribute('id');
		foreach my $n (qw/fpp rpp potential_pay actual_pay gpp ppp pp_basis/)
		{
			$rv{$id}->{$n} = $node->findvalue("\@$n");
		}
	}
	
	$Cache{'m25'} = \%rv;
	return $Cache{'m25'};
}

=head3 dnNodeListM50

	Get the "dn" (downline) nodes under the m50 node

=head4

	None
	
=head4 Returns

	A hashref of nodes resembling the hashref that can be seen in the comments at the end

=cut

sub dnNodeListM50()
{
	my ($self) = @_;
	return $Cache{'m50'} if $Cache{'m50'};
	
	my %rv = ();
	foreach my $node ($Cache{'LibXML'}->findnodes("/me/m50/dn"))
	{
		my $id = $node->getAttribute('id');
		foreach my $n (qw/fpp rpp actual_pay frpp gpp ppp/)
		{
			$rv{$id}->{$n} = $node->findvalue("\@$n");
		}
	}
	
	$Cache{'m50'} = \%rv;
	return $Cache{'m50'};
}

=head3 myGRP_PP

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "grp_pp" node off of the "me" root node.
	This value represents the new concept (March 2014) of "group" pay points for the actual analysis period.
	See the comp plan for the actual definition of this value.
		
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub myGRP_PP
{
	return 0 unless CkXmlCache();
	return sprintf('%.2f', ($Cache{'LibXML'}->findvalue('/me/grp_pp') || 0));
}

=head3 LoadRefcompRecord

	Load a record into the Cache for further use via _SUPER_->LoadRefcompRecord.

	Note, the Cache will be completely reinitialized prior to obtaining the new data.
	Also, you should have called new() with an active DB handle or set it with a call to DB().
	We will take care of calling init() using the analysis data returned.

=head4 Arguments

	A numeric ID
	An optional hashref. This key/value pair is currently recognized:
	{
		'tbl'=> 'the table name we should query'	# the default is 'refcomp'
	}
	
=head4 Returns

	A hashref of the record.
	
=cut

sub LoadRefcompRecord
{
	my ($self, $id, $args) = @_;
	my $rv = $self->SUPER::LoadRefcompRecord($id, $args);
	$self->init($rv->{'analysis'});
	return $rv;
}

=head3 my25PctCommissionIdList

	A list of IDs from whom I am receiving a 25% commission... formerly called a Training Bonus... currently called a Coaches Commission
	This is essentially the same list of IDs as can be derived from dnNodeListM25 with the ones whom I receive no commission from excluded.
	
=head4 Arguments

	None
	
=head4 Returns

	A reference to an array of IDs
	
=cut

sub my25PctCommissionIdList()
{
	my $self = shift;
	
	unless ($Cache{'my25PctcommissionIdList'})
	{
		my $tmp = $self->dnNodeListM25();
		foreach (keys %{$tmp})
		{
			push(@{ $Cache{'my25PctcommissionIdList'} }, $_) if ($tmp->{$_}->{'actual_pay'} || 0) > 0;
		}
	}
	
	return $Cache{'my25PctcommissionIdList'};
}

=head3 my25PctCommissionNodeList

	A list of nodes from whom I am receiving a 25% commission... formerly called a Training Bonus... currently called a Coaches Commission
	This is essentially the same list as can be derived from dnNodeListM25 with the ones whom I receive no commission from excluded.
	
=head4 Arguments

	None
	
=head4 Returns

	A hashref of nodes resembling the hashref that can be seen in the comments at the end
	
=cut

sub my25PctCommissionNodeList()
{
	my $self = shift;
	
	unless ($Cache{'my25PctCommissionNodeList'})
	{
		my $tmp = $self->dnNodeListM25();
		foreach (keys %{$tmp})
		{
			$Cache{'my25PctCommissionNodeList'}->{$_} = $tmp->{$_} if $tmp->{$_}->{'actual_pay'} > 0;
		}
	}
	
	return $Cache{'my25PctCommissionNodeList'};
}

=head3 my25PctPotentialCommissionIdList

	A list of IDs from whom I could be receiving a 25% commission... formerly called a Training Bonus... currently called a Coaches Commission
	This is essentially the same list of IDs as can be derived from dnNodeListM25 with the ones whom I receive commissions from, as well as the ones with no potential commission, excluded.
	
=head4 Arguments

	None
	
=head4 Returns

	A reference to an array of IDs
	
=cut

sub my25PctPotentialCommissionIdList()
{
	my $self = shift;
	
	unless ($Cache{'my25PctPotentialCommissionIdList'})
	{
		my $tmp = $self->dnNodeListM25();
		foreach (keys %{$tmp})
		{
			push(@{ $Cache{'my25PctPotentialCommissionIdList'} }, $_) if $tmp->{$_}->{'potential_pay'} > 0 && ! $tmp->{$_}->{'actual_pay'};
		}
	}
	
	return $Cache{'my25PctPotentialCommissionIdList'};
}

=head3 myPPP

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "ppp" node off of the "me" root node.
	This value represents the real PPP for the actual analysis period.
	Unfortunately the ppp column in refcomp was repurposed to be power pay points so there is no slot for real PPP any more.
	... And naturally, we need to display this value since there is now a PPP sales requirement again...
	
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub myPPP
{
	return 0 unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/ppp') || 0;
}

=head3 myPRP_PP

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "prp_pp" node off of the "me" root node.
	This value represents the new concept (March 2014) of "personal" pay points for the actual analysis period.
	See the comp plan for the actual definition of this value.
		
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub myPRP_PP
{
	return 0 unless CkXmlCache();
	return sprintf('%.2f', ($Cache{'LibXML'}->findvalue('/me/prp_pp') || 0));
}

=head3 prpCountNew

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "new_prps" node off of the "me" root node.
	This value represents the number of "new" PRPs for the actual analysis period.
	
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub prpCountNew
{
	return 0 unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/new_prps') || 0;
}

=head3 numOfExecs

	How many Execs do I have?

=head4 Arguments

	None
	
=head4 Returns

	A numeric value that is a count of the Exec nodes in the XML.
	
=cut

sub numOfExecs
{
	return 0 unless CkXmlCache();
	my $rv = 0;
	$rv++ foreach $Cache{'LibXML'}->findnodes('/me/exec');
	return $rv;
}

=head3 frontlineRppTotal

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "frpp" attribute of the "me" root node.
	
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub frontlineRppTotal
{
	return 0 unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/@frpp') || 0;
}

=head3 periodPriorMonth

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "period_prior_month" node off of the "me" root node. This value represents the period prior to the actual analysis period.
	
	Note: If you have not called init(), this method will produce a warning and return an undefined value
	
=cut

sub periodPriorMonth
{
	return 0 unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/period_prior_month');
}

=head3 prpCountPriorMonth

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	None

=head4 Returns

	A numeric value that is the contained in the "prp_cnt_lastmonth" node off of the "me" root node.
	This value represents the number of PRPs for period prior to the actual analysis period.
	
	Note: If you have not called init(), this method will produce a warning and return a value of -0-
	
=cut

sub prpCountPriorMonth
{
	return 0 unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/prp_cnt_lastmonth') || 0;
}

=head3 xAttribute

	init() should have been called prior to calling this method as it relies upon the hashref cached from the XML passed into init()

=head4 Arguments

	The XPath off of the root node. In other words, assuming a node /me/exec_bonus_share_stats, this argument should be 'exec_bonus_share_stats'
	The attribute name
	Optional: a default value such as -0- if you do not want undefs
	
=head4 Returns

	The value that is the contained in the attribute of the designated node off of the root node.
	
	Note: If you have not called init(), this method will produce a warning and return the default
	
=cut

sub xAttribute
{
	my ($self, $nodeName, $attname, $default) = @_;
	return $default unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/' . $nodeName . '/@' . $attname) || $default;
}

=head3 xNode

	Instead of specifying each particular value we want and creating a method, we will create this method which can take an XPATH argument and return that.
	This will require diligence on the part of the caller to not pass in strings that will break the processor as we are not going to attempt to deal with those issues.

=head4 Arguments

	The XPath off of the root node. In other words, assuming a node /me/exec_bonus_share_stats, this argument should be 'exec_bonus_share_stats'
	Optional: a default value such as -0- if you do not want undefs
	
=head4 Returns

	The value that is the contained in the designated node off of the root node.
	
	Note: If you have not called init(), this method will produce a warning and return the default
	
=cut

sub xNode
{
	my ($self, $nodeName, $default) = @_;
	return $default unless CkXmlCache();
	return $Cache{'LibXML'}->findvalue('/me/' . $nodeName) || $default;
}

1;

__END__

	The XML looks something like this: (this, of course, has been modified/extended much over time from the original layout)

###### This section was added to the end in October 2014 to support the new Side Volume requirement
<sv><grp>23</grp><grp_pp>525</grp_pp>
<large_leg><id>5196425</id><grp>21</grp><grp_pp>562.7</grp_pp></large_leg>
</sv>

###### this is the version as of March 2014
<me><period>459</period><period_prior_month>456</period_prior_month>
<ppp>3.26</ppp><tcpp>0</tcpp><prp_pp>423.60</prp_pp><grp_pp>1722.08</grp_pp>
<m50><dn id="2376070" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="3077498" ppp="1.98" gpp="0" actual_pay="0.79" power_pp="19.80"/>
<dn id="3312445" ppp="0.32" gpp="0" actual_pay="0.13" power_pp="3.20"/>
<dn id="1361451" ppp="0" gpp="18.00" actual_pay="7.20" power_pp="0"/>
<dn id="4506638" ppp="0" gpp="17.00" actual_pay="6.80" power_pp="0"/>
<dn id="2808778" ppp="0" gpp="21.00" actual_pay="8.40" power_pp="0"/>
<dn id="111" ppp="0" gpp="17.00" actual_pay="6.80" power_pp="0"/>
<dn id="4639633" ppp="0" gpp="24.00" actual_pay="9.60" power_pp="0"/>
<dn id="3275364" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="5426553" ppp="0" gpp="12.00" actual_pay="4.80" power_pp="0"/>
<dn id="41173" ppp="0" gpp="22.00" actual_pay="8.80" power_pp="0"/>
<dn id="1041155" ppp="0" gpp="18.00" actual_pay="7.20" power_pp="0"/>
<dn id="1530830" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="5487859" ppp="0" gpp="17.00" actual_pay="6.80" power_pp="0"/>
<dn id="4660736" ppp="0" gpp="17.00" actual_pay="6.80" power_pp="0"/>
<dn id="3125044" ppp="0" gpp="11.00" actual_pay="4.40" power_pp="0"/>
<dn id="215438" ppp="0" gpp="19.00" actual_pay="7.60" power_pp="0"/>
<dn id="553714" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="67490" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="5507197" ppp="0" gpp="17.00" actual_pay="6.80" power_pp="0"/>
<dn id="466040" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="3206660" ppp="0" gpp="23.00" actual_pay="9.20" power_pp="0"/>
<dn id="81630" ppp="0.96" gpp="0" actual_pay="0.38" power_pp="9.60"/>
</m50><m25 share_value="4.50" my_shares="75" />
</me>
###### this is a former version
<me id="3698903" fpp="495.74" rpp="0.00" plpp="535.74 frontline_income="247.87" my25="336.67" frpp="992.58" orpp="1642.98">
	<period>405</period><period_prior_month>423</period_prior_month><prp_cnt_lastmonth>6</prp_cnt_lastmonth>
	
	<!-- The new layout as of November 2012 -->
	<me><period>414</period>
	
<!--For the referral commissions it looks like this:-->
	<m50>
		<dn id="4220435" fpp="39.00" rpp="0" actual_pay="19.50" frpp="477.00"/>
		<dn id="2630080" fpp="35.04" rpp="0" actual_pay="17.52" frpp="0"/>
		<dn id="5381455" fpp="28.00" rpp="0" actual_pay="14.00" frpp="0"/>....
	
	<!-- As of November 2012 the XML changes to this to reflect the point allocations that align with the revised comp plan -->
	<me><period>414</period><m50>
		<dn id="159991" ppp="0" gpp="56.00" actual_pay="28.00" pmpp="859.00"/>
		<dn id="3396683" ppp="1.00" gpp="0" actual_pay="0.50" pmpp="10.00"/>
		<dn id="5514870" ppp="4.00" gpp="0" actual_pay="2.00" pmpp="0"/>....

<!--For the training bonus it looks like this:-->
	<m25>
		<dn id="392151" fpp="140.00" rpp="0" potential_pay="35.00"/>
		<dn id="5392151" fpp="40.00" rpp="0" potential_pay="10.00" actual_pay="10.00"/>
		<dn id="5357211" fpp="57.09" rpp="0" potential_pay="14.27" actual_pay="14.27"/>....
	
	<!-- As of November 2012 the XML changes to this -->
	<m25>
		<dn id="5508474" ppp="0" gpp="12.00" potential_pay="3.00" actual_pay="3.00"/>
		<dn id="5526150" ppp="0" gpp="12.00" potential_pay="3.00" actual_pay="3.00"/>....

	<dividends _2="0.00" _3="0.00" _4="2.83" _5="5.54" _6="4.59" _7="7.30" _8="17.85" _9="11.47" _10="14.60" _11="309.36" _12="571.12"/>
 	<exec>3843547</exec>
</me>

	This ends up looking like this if run through XML::LibXML::Simple : (of course we have moved away from that for now)
	$VAR1 = {
	          'my25' => '1831.88',
	          'frontline_income' => '909.66',
	          'm50' => {
	                   'dn' => {
	                           '2630080' => {
	                                        'actual_pay' => '17.52',
	                                        'fpp' => '35.04',
	                                        'rpp' => '0'
	                                      },
	                           '5381455' => {
	                                        'actual_pay' => '14.00',
	                                        'ppp' => '28.00'
	                                        'rpp' => '0'
	                                      },...
	                   }


As of February 2013 and TNT 2.0, the 25% commissions are now called Team Commissions and the way they are calculated is entirely different. Here is the current structure:
<m25>
<dn id="5552771" pp_basis="23.00" total_shares="248" my_shares="20" potential_pay="6.9" actual_pay="0.56"/>
<dn id="5548912" pp_basis="17.00" total_shares="248" my_shares="20" potential_pay="5.1" actual_pay="0.41"/>
