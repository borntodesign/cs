package Filter1;
use minidee;

sub handler {
my $r = shift;
  $r = $r->filter_register();  # Required
  my $fh = $r->filter_input(); # Optional (you might not need the input FH)
	print '<base>';
  while (<$fh>) {
	next if /^<!DOCTYPE/;
	s/^<html(.+?)>/<html>/;
  #  s/ something / something else /;
    print;
  }
	print user($r), '</base>';
}

sub user
{
        my $r = shift;
        my $ck = Apache::Cookie->new(AxKit::Apache->request)->parse;
        my $minideeCK = $ck->{'minidee'} ? $ck->{'minidee'}->value : undef;
        my $id = $ck->{'id'} ? $ck->{'id'}->value : undef;
        my $minidee = {};

        if ($id || $minideeCK)
        {
                unless ($minideeCK)
                {
                        $minidee = minidee::get_data($id);
                        $r->headers_out->set('Set-Cookie' => minidee::create_cookie($minidee));
                }
                else
                {
                        $minidee = minidee::parse($minideeCK);
                }
        }
        return '' unless $minidee;
	my $rv =  '<minidee>';
	$rv .= "<$_>$minidee->{$_}</$_>\n" foreach keys %{$minidee};
	return "$rv</minidee>";
}

1;
