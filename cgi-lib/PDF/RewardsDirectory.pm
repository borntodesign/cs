package PDF::RewardsDirectory;

=head2 PDF::RewardsDirectory

A class for creating PDF lists of Rewards Merchants

=cut

require PDF::API2::Simple;
my $pdf = ();
my $language_pack = ();
my %pdfdefs = (
	'margin_left' => 20,	# actually the default within PDF::API2
	'line_height' => 11		# 10 is the PDF::API2 default
);
=head3 InitLanguagePack

A public method that should be called with an appropriate language pack,
either an XML document that can be converted to a hashref or a ready made hashref.

If this method is not called explicitly with a language pack, or if the required tokens cannot be identified,
English defaults will be used for all tokens required.

=head4 Arguments

Either a scalar value that represents an XML language pack or a hashref lexicon

=head4 Returns the language pack hashref, useful for inspection to determine if the required nodes are set to what is expected.

=cut

sub InitLanguagePack($$)
{
	my ($self, $lp) = @_;
	my $xml = ();
	if (ref $lp)	# we will assume it is a hashref ;)
	{
		$language_pack = $lp
	}
	elsif ($lp)		# no bother trying to convert an empty/undef
	{
		eval q|
			use XML::LibXML::Simple qw(XMLin);
			$xml = XMLin($args->{'xml'});
		|;
		warn "Errors encountered during language pack creation: $@\n" if $@;
	}
	
	# now fill in our defaults if we cannot identify that we have what is needed in what was provided
	# these keys are based upon the current XML language pack for the rewards directory
	# template 10822
	$language_pack->{'specials'}->{'percentage_off'} ||= (
		$xml->{'specials'}->{'percentage_off'} ||
		'Percentage Off');
	
	$language_pack->{'specials'}->{'amount_off'} ||= (
		$xml->{'specials'}->{'amount_off'} ||
		'Amount Off');

	$language_pack->{'specials'}->{'bogoh'} ||= (
		$xml->{'specials'}->{'bogoh'} ||
		'Buy One Get One 50% Off');

	$language_pack->{'specials'}->{'bogo'} ||= (
		$xml->{'specials'}->{'bogo'} ||
		'Buy One Get One Free');

	$language_pack->{'discounts'}->{'reward_points'} ||= (
		$xml->{'discounts'}->{'reward_points'} ||
		'ClubCash');

	$language_pack->{'discounts'}->{'pay_points'} ||= (
		$xml->{'discounts'}->{'pay_points'} ||
		'Pay Points');

	$language_pack->{'discounts'}->{'referral_fee'} ||= (
		$xml->{'discounts'}->{'referral_fee'} ||
		'Referral Fee');

	return $language_pack;
}

=head3 InitPDF()

A private method to set/reset the internal state of the $pdf document

=cut

sub InitPDF
{
	$pdf->end if $pdf;
	$pdf = PDF::API2::Simple->new();
	$pdf->add_font('Helvetica');
	$pdf->add_font('Verdana');
	$pdf->add_font('Arial');
	$pdf->add_page();
}

=head3 MerchantListings()

Creates an internal PDF document and returns a scalar reference to the stringified output.
Each time this method is called, the internal PDF document is destroyed and a new one created.
To reuse the internal document, call one of the other extraction methods instead.

head4 Arguments
	A reference to an array of locations blended with certain category and locality information.
	This array is currently (06/11) provided by Keith code MerchantAffiliates->retrieveLocations

	An optional hashref of other arguments. Keys recognized:
	page_title => the title for the page

=cut

sub MerchantListings($\@;$)
{
	my ($self, $locations, $args) = @_;
	$self->InitLanguagePack({}) unless $language_pack;

	InitPDF();

	if ($args->{'page_title'})
	{
		$pdf->set_font('Helvetica', 16);
		Text($args->{'page_title'});
		Text('');
	}
	
	my $cat = '';
#	$pdf->line_height(10);
	foreach my $merch (@{$locations})
	{
		# reset the "margin" to the default
		$pdf->x( $pdfdefs{'margin_left'} );

		if ($cat ne $merch->{'category_description'})
		{
			$cat = $merch->{'category_description'};
			$pdf->set_font('Helvetica', 12);
			$pdf->current_fill_color('%0009');
			Text($cat);
			$pdf->next_line;
		}

		$pdf->current_fill_color('%000f');
		$pdf->x( $pdfdefs{'margin_left'} +3 );
		$pdf->set_font('Arial', 10);
		Text($merch->{'location_name'});
		$pdf->x( $pdfdefs{'margin_left'} +7 );
		
		if (defined $merch->{'discount'}>{'reward_points'})
		{
			$pdf->set_font('Arial', 7);
			Text("$language_pack->{'discounts'}->{'reward_points'}: " . $merch->{'discount'}->{'reward_points'});
		}
		elsif (defined $merch->{'special'}->{'percentage_off'})
		{
			$pdf->set_font('Arial', 7);
			Text("$language_pack->{'specials'}->{'percentage_off'}: " . $merch->{'special'}->{'percentage_off'});
		}
		elsif (defined $merch->{'special'}->{'bogo'})
		{
			$pdf->set_font('Arial', 7);
			Text("$language_pack->{'specials'}->{'bogo'}: " . $merch->{'special'}->{'bogo'});
		}
		elsif (defined $merch->{'special'}->{'bogoh'})
		{
			$pdf->set_font('Arial', 7);
			Text("$language_pack->{'specials'}->{'bogoh'}: " . $merch->{'special'}->{'bogoh'});
		}

		if ($merch->{'discount_blurb'})
		{
			$pdf->set_font('Arial', 6);
			Text($merch->{'discount_blurb'});
		}

		$pdf->set_font('Arial', 9);
		Text($merch->{'address1'}) if $merch->{'address1'};
		Text($merch->{'phone'}) if $merch->{'phone'};
		
		# a blank line
		$pdf->set_font('Arial', 20);
		Text('');
#print $tmp "\" \" ,\"$merch->{location_name}\",\"$special\",\"$dblurb\",\"$pctamt\",\"$merch->{phone}\",\"$merch->{address1}\",\"$merch->{city}\",\"$merch->{state}\",\"$merch->{postalcode}\"\n";
	}

	$pdf->set_font('Arial', 8);
	Text('List date: ' . scalar localtime());
	
	$pdf->next_line;
#	Text('');	# a blank line

	return $pdf->stringify;
	return \${ $pdf->stringify };			
}

=head3 MerchantListingsText()

A simple method that returns the stringified version of a previously created PDF document

=cut

sub MerchantListingsText
{
	my $self = shift;
	unless ($pdf)
	{
		warn "Internal PDF document is empty/uninitialized\n";
		return ();
	}
	return \${ $pdf->stringify };
}

=head3 new()

Returns a PDF::RewardsDirectory object

=cut

sub new
{
	return bless {

	};
}

=head3 Text()

A private method to invoke $pdf->text('some text', 'autoflow' => 'on') without having to specify 'autoflow' => 'on' every time

=cut

sub Text($)
{
	$pdf->text($_[0], 'autoflow' => 'on');
}

1;
