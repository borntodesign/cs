package ADMIN_DB;
###### package of admin DB tools
###### 05/12/02	Bill MacArthur
###### original DB_Connect derived from DB_Connect.pl
# last modified: 11/05/13	Bill MacArthur

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;

# the 'public' flag is not necessary for strictly admin resources
# the 'restricted' should be used to denote the DB group/s with access to the resource
# sorting will normally occur on the label, but we may want to coerce another behavior, like making #1 #1
# 'sort' can be ignored unless coercion is necessary
our %AppMap = (
	1 => {
		label => 'Member Records',
		url => '/cgi/admin/memberinfo.cgi?id=%%id%%',
		sort => 100
	},
	2 => {
		label => 'Club Account Reports',
		url => '/cgi/admin/SOAadmin.cgi?id=%%id%%',
	},
	3 => {
		label => 'Login as member',
#		url => '/LOGIN?destination=%%default_destination%%;action=login;credential_0=%%alias%%;credential_1=%%password%%'
		'url' => ''
	},
	4 => {
		label => 'Upline Line Report',
		url => '/cgi/vip/line_rpt.cgi?mid=%%id%%;admin=1;rid=1'
	},
	5 => {
		label => 'Personal Web Site Link',
		url => '/cgi/admin/admin-rd-pwl.cgi?id=%%id%%'
	},
	6 => {
		label => 'TMS Records',
		url => '/cgi/admin/tms/AOL.cgi?alias=%%id%%'
	},
	7 => {
		label => 'Personalized Web Pages',
		url => '/cgi/pwp-edit.cgi/0/default?id=%%id%%'
	},
	8 => {
		label => 'Subscriptions',
		url => '/cgi/admin/subscriptions.cgi?id=%%id%%'
	},
	9 => {
		label => 'Upline Line Report (NSPID)',
		url => '/cgi/vip/nline_rpt.cgi?mid=%%id%%;admin=1;rid=1'
	},
);

###### generate a database handle based on the calling scripts name and the users identity
###### we will provide error handling here with appropriate messages so we can return null
###### without having the caller be concerned about it
sub DB_Connect($$$){
###### first we'll get our database connection info
	my ($name, $dbuser, $dbpasswd) = @_;

###### if we don't have all the parameters, we may as well quit right away
	unless ($name && $dbuser && $dbpasswd)
	{
		Err("<h3>Sorry missing 1 or more of resource name, DB user or DB password.<br />You may need to login again.</h3><p>" .
			'Resource name: ' . ($name || '') . '<br />' .
			'DB user: ' . ($dbuser || '') . '<br />' .
			'DB password: ' . ($dbpasswd || '') . '</p>'
		);
		return;
	}

	my ($val, $myflg, $dbflag, $dbserver, $key, $db);
	open (INI, '/home/httpd/cgi-lib/cgi.ini') || die "Couldn't open ini file cgi.ini\n";
	foreach (<INI>){

###### ignore all lines that don't contain a colon, our delimiter
		unless ($_ =~ m/:/){next}
		chomp;
		($key, $val) = split /:/;
		if ($key eq $name){$myflg = $val}
		elsif ($key eq 'database'){$dbflag = $val}
		elsif ($key eq 'dbserver'){$dbserver = $val}
	}
	close INI;

	unless( $myflg )
	{
		die "No entry for for this script in the INI file";
	}

	elsif ($myflg ne 'on'){print $q->redirect('http://www.clubshop.com/errors/scriptoff.html'); return}
	elsif ($dbflag ne 'on'){print $q->redirect('http://www.clubshop.com/errors/dbmaint.html'); return}
	elsif (!($db= DBI->connect("DBI:Pg:dbname=network;host=$dbserver", "$dbuser", "$dbpasswd"))){
		Err("<h3>Sorry your authorization could not be validated.<br />Please login again.</h3><p>"  .
                        'DB user: ' . ($dbuser || '') . '<br />' .
                        'DB password: ' . ($dbpasswd || '') . '</p>');

###### we'll log our connection errors, they may come in useful sometime
		open (LOG, '>>/home/httpd/cgi-logs/dbconnect.errors') || die "Couldn't open the DB connection error log\n";
		print LOG scalar localtime() . "\nOperator: $dbuser , Password: $dbpasswd\n";
		print LOG "Query string = " . $q->query_string() . "\n" . $q->remote_addr() . "\n\n";
		close LOG;
		return;
	}
	my $script = $0;
	$script =~ s#.*/##;
	my ($rv) = $db->selectrow_array("
		SELECT odbu.rolname
		FROM odb.odb_users odbu
		JOIN odb.odb_user_acl odbacl
			ON odbu.pk=odbacl.odb_users_pk
			AND odbacl.has_access=TRUE
		JOIN odb.odb_resources odbr
			ON odbacl.odb_resources_pk=odbr.pk
		WHERE odbu.rolname= ?
		AND (odbr.resource_name= ? OR odbr.resource_name='*')", undef, $dbuser,$script);
	unless ($rv){
		Err('<h3>Sorry, you do not have access to that resource</h3>');
		return;
	}
	return $db;
}

###### this routine just validates whether or not the user is a member of the selected group
sub CheckGroup($$$)
{
	my ($db, $operator, $groupname) = @_;
	my $sth = $db->prepare("SELECT usesysid FROM pg_user WHERE usename = ?");
	$sth->execute($operator);
	my $sid = $sth->fetchrow_array;

	return 0 unless $sid;
	
	$sth = $db->prepare("SELECT grolist FROM pg_group WHERE groname = ?");
	$sth->execute($groupname);
	my $res = $sth->fetchrow_array;	# this returns an array reference with current versions of DBD::Pg as that is what the data type is in the database
	$sth->finish;
	foreach (@{$res})
	{
		return 1 if $_ == $sid
	}
	return 0;
}

sub Err
{
	print $q->header(-expires=>'now'), $q->start_html(-bgcolor=>'#ffffff'), $_[0], $q->end_html();
}

1;

###### 04/04/02 revised the connection routine to abort if all the params are not received
###### 05/13/02 changed the cgi reading parsing to exclude all lines without the delimiter
###### this will allow comments and blank lines without testing for them implicitly
###### 09/09/02 altered variable handling in CheckGroup to return empty if the query returned empty
###### 05/12/03 added a handler for the case when a script is not found in the .ini file
# 07/31/06 added an expires header to the Err routine
# June added the app map functionalities
# 03/02/11 added the %%default_destination%% placeholder to the login as URL
# 08/29/14 modified CheckGroup to handle the array passed from grolist
