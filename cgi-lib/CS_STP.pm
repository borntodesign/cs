package CS_Stripe;

=head2 CS_Stripe.

	Introducing the Stripe implementation on clubshop

	Currently a simple wrapper around Net::Stripe and friends.
	If we ever want to migrate to another package, 
	we can hopefully do so without missing a beat by remapping t
	he method calls used with Business::PayPal to another package's versions.
	
	One additional thing we keep in here is the authentication credentials.
	
=cut

use strict;
use Business::Stripe;

=head3 new()

	Create a CS_Stripe object for utility methods like getting destination URLs.

=head4 Arguments

	We are feeding the real arguments to Business::Stripe->new() from within,
	but we do accept one argument to new() here, a hashref with one key/value, ie. {test=>1} or {test=>0}
	If test=>1 then we will use the Stripe test credentials, otherwise will be using all live
	
=cut

sub new
{
	my $class = shift;
	my $args = shift;
	my %creds = ();
	if (! $args || ! $args->{'live'})	# we are doing live
	{
		%creds = (
			'publicKey'   => 'pk_live_iTbuu6qjSQKyqSZsF9wvuMyH',
			'secretKey'   => 'sk_live_awZmvhrunrg1mv7wbK80Kr6I'
		);
	}
	else
	{
		%creds = (
			'publicKey'   => 'pk_test_vo4yOiWbda9lHh85mZ2W55SH',
			'secretKey'   => 'sk_test_BhQqYNv4Xz5uVoHY3ZzeqBlk'
		);
	}
	return bless {'creds' => \%creds}, $class;
}

=head3 API

    This method returns the actual Business::Stripe object.
	Refer to that documentation.
=cut

sub API
{
    my $self = shift;
    $self->{'API'} = Business::Stripe->new( -api_key => $self->{'creds'}{'secretKey'} ) unless $self->{'API'};
    return $self->{'API'};
}

sub HandleErrors
{
	my ($self, $resp) = @_;
	return undef if $resp->{'Ack'} eq 'Success';
	my $rv = '';
	foreach my $err (@{$resp->{'Errors'}})
	{
		$rv .= "Error code: $err->{'ErrorCode'} - $err->{'LongMessage'}<br />";
	}
	return $rv;
}

1;
