# DB_Connect.pm
package DB_Connect;
###### generate a database handle based on the calling scripts name
###### last modified: 10/28/08	Bill MacArthur

use strict;
use DBI;
use base 'Exporter';

our @EXPORT = qw(DB_Connect);
our $redirect_base = "Status: 302 Moved\nLocation: ";

sub DB_Connect($;$$)
{
	my ($appname, $flag, $args) = @_;
	return unless $appname;

###### at this time we will recognize the following arg values
###### 'fail_silently'		- don't do our redirects
###### 'return_redirects'		- send our redirect URL back to the caller
######			(this means the caller has to know that it is not a DB handle)
###### 'enable_lockout' to lookup the system status and lockout the application accordingly

###### first we'll get our database connection info
	my ($val, $dbuser, $dbpasswd, $myflg, $dbflag, $dbserver, $key, $db);

	open (INI, '/home/httpd/cgi-lib/cgi.ini') || die "Couldn't open ini file cgi.ini\n";
	foreach (<INI>)
	{
	###### ignore all lines that don't contain a colon, our delimiter
		unless ($_ =~ m/:/){ next }
		chomp;
		($key, $val) = split /:/;
		if ($key eq 'username'){$dbuser = $val}
		elsif ($key eq 'password'){$dbpasswd = $val}
		elsif ($key eq $appname){$myflg = $val}
		elsif ($key eq 'database'){$dbflag = $val}
		elsif ($key eq 'dbserver'){$dbserver = $val}
	}
	close INI;

##### we'll reassign the appname to our login var if we got the flag to do so
	$dbuser = $appname if $flag;

	unless( $myflg )
	{
		die "No entry for this script in the INI file: $appname";
	}

	elsif ($myflg ne 'on')
	{
		return _redirect('http://www.clubshop.com/errors/scriptoff.html', $args);
	}
		
	elsif ($dbflag ne 'on')
	{
		return _redirect('https://www.clubshop.com/errors/dbmaint.html', $args);
	}
	
	elsif (!($db= DBI->connect("DBI:Pg:dbname=network;host=$dbserver", "$dbuser", "$dbpasswd")))
	{
		return _redirect('https://www.clubshop.com/errors/nodb.html', $args);
	}
	
	if ($args->{'enable_lockout'})
	{
		my ($rv) = $db->selectrow_array('SELECT locked FROM system_status WHERE locked=TRUE');
		if ($rv)
		{
			$db->disconnect;
			return _redirect('https://www.clubshop.com/errors/lockdown.xml');
		}
	}
			
	return $db;
}

sub _redirect($$)
{
	my ($url, $args) = @_;
	if ($args->{'fail_silently'}){ return }
	elsif ($args->{'return_redirects'}){ return $url }
	else
	{
		print "$redirect_base $url\n";	# beware, for some mysterious reason, this otherwise working piece of code does not always generate a redirect as it seems Apache never sees it
		return undef;					# instead we get the typical Premature end of script headers error
	}									# in those cases it is best to use the 'return_redirects' flag from the caller and trap the return string if it appears to do the redirect there
}
1;

###### added a package name
###### 03/29/02 added logging in as the script rather than as the generic user 'cgi if a flag is passed
###### this is to help identify who is calling the database when debugging connection problems
###### 05/13/02 changed the cgi reading parsing to exclude all lines without the delimiter
###### this will allow comments and blank lines without testing for them implicitly
###### 12/06/02 added the Exporter module and explicit export of the subroutine
###### 05/12/03 added special handling for a missing 'appname' in the .ini file
###### 10/20/04 changed to simple textual redirects instead of using CGI.pm
###### 10/21/04 added handling for conditional redirects
###### 10/28/08 added the 'enable_lockout' argument and handling
