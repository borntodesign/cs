package EventLogin;

###### some simple routines to begin an 'Event' login/function system
###### created: 09/12/07	Bill MacArthur

require Crypt::CBC;
use CGI;
#my $cipher = new Crypt::DES 'ckdj*&87';
my $cipher = Crypt::CBC->new(-key=>'ckdj*&87', -cipher=>'DES');
my $salt = 'ptj9*&+{}';

sub CreateTicket($)
{
	###### for now we'll create a ticket that is good for -4- hours (4 x 3600 seconds)
	###### later on we can access some configuration documents to determine this stuff

	###### we are expecting some data (wrapped in a hashref) to put on the end of our cookie
	###### for now we'll just expect a hashref to an event record
	return undef unless my $data = shift;
	my $time = time + (4 * 3600);
	my $enctime = $cipher->encrypt($time);
	return CGI::cookie(
		-name=>'EventLogin',
		-value=>"$enctime," . $cipher->encrypt($time . $salt) . ",name|$data->{name}\teventname|$data->{eventname}",
		-secure=>1);
}

sub ValidateTicket($)
{
	return undef unless my $cookie = shift;
	die "Invalid cookie format: $cookie\n" unless $cookie =~ m/(.+?),(.+?),(.+)/s;
	my $time = $cipher->decrypt($1);
	unless ("$time$salt" eq $cipher->decrypt($2)){
		warn "EventLogin cookie time+salt check value failed: $time$salt != " .$cipher->decrypt($2);
		return undef;
	}
	unless ($time > time){
		warn 'EventLogin cookie time is in the past';
		return undef;
	}

	my %h = ();
	foreach my $tmp (split /\t/, $3){
		my ($key, $val) = split /\|/, $tmp;
		$h{$key} = $val;
	}
	return \%h;
}

1;
__END__
###### HISTORY ######


###### Ticket Structure ######
EncTime,Hash,Data

EncTime = DES encrypted Expiration time
Hash = DES encrypted EncTime+Salt
Data = fluff as of now

