package AxKitPostFilters;
###### post-content generation filter/s

sub conv2js											
{
	my $data = shift;
	###### remove the top & bottom of the document
	$data =~ s#^.*<body.*?>|</body.*##sg;

	###### change all double quotes to an escaped version
	$data =~ s#"#\\"#g;

	###### for javascript, we need to insert continuation marks at the end of lines
	$data =~ s#\n#\\\n#g;

	return "document.write(\"$data\");";
}

1;

