package ParseWebinarXLS;

use Spreadsheet::ParseExcel;
use DHS::Excel::FmtUTF8;

# this is version 2 as the spreadsheet format changed considerably in Nov 2014

=head2 ParseWebinarXLS

	Take an Excel spreadsheet and parse it out to render a hashref of hashrefs containing the required data.
	This is all assuming a predetermined document layout, of course ;)
	
=head3 new()

	Create a new instance of ParseWebinarXLS

=cut

sub new($$)
{
	my ($class, $filehandle) = @_;
#	die "new() requires a valid filehandle as the first argument. Argument ref: " . (ref $filehandle) unless $filehandle && ref $filehandle eq 'GLOB';

	my $parser = Spreadsheet::ParseExcel->new();
	my $formatter = DHS::Excel::FmtUTF8->new('encode');
	my $workbook = $parser->parse($filehandle, $formatter);
#	my $workbook = $parser->parse($filehandle);
	return bless {'workbook'=>$workbook};
}

=head3 Parse()

	Parse the spreadsheet and return a hashref of hashrefs.
	Note, if there are errors, a hashref will be returned with a key, 'errors', which will contain a textual error message.
	
	Also note that the column indexes are 0 based. In other words, cell(1,0) is cell A,1
=cut

sub Parse
{
	my $self = shift;
	my $ws = $self->{'workbook'}->worksheet(0);
	my ($cell, %rv) = ();
	
	###### rows/cells start at -0-
	my ( $row_min, $row_max ) = $ws->row_range();
	if ($row_max < $row_min)
	{
		$rv{'error'} = "Either parsing failed to see any data or the sheet is empty";
		return \%rv;
	}
	
	unless ($cell = $ws->get_cell(0,1))
	{
		$rv{'error'} = "Failed to get_cell(5,0) which should equate to the Webinar Name";
		return \%rv;
	}
	
	$rv{'webinar_name'} = $cell->value();
	
	$cell = $ws->get_cell(4,0);
	$rv{'webinar_id'} = $cell->unformatted();
	if ($rv{'webinar_id'} !~ m/^\d+\-\d+\-\d+$/)
	{
		$rv{'error'} = 'There seems to be a problem with the format of this document. The webinar ID is not numeric. ';
		$rv{'error'} .= "These are the parsed values: Webinar Name (expected row/column 1,2): $rv{'webinar_name'}  Webinar ID (expected row/column 5,1): $rv{'webinar_id'}";
		return \%rv;
	}
	
	$cell = $ws->get_cell(4,1);
	$rv{'webinar_date_time'} = $cell->unformatted(8,1);
#warn "These are the parsed values: Webinar Name (expected row/column 1,2): $rv{'webinar_name'}  Webinar ID (expected row/column 5,1): $rv{'webinar_id'} Date (expected B5) $rv{'webinar_date_time'}";	
	# remember we start at -0- so col_min should =0 and col_max will be 1 less than a visual inspection would indicate
	# with the new format in Nov 2014, the number of columns varies depending on how many comments we have by the participants
#	my ( $col_min, $col_max ) = $ws->col_range();
#	if ($col_max != 12)
#	{
#		$col_max++;
#		$rv{'error'} = "There seems to be a problem with the format of this document. We are expecting 13 columns, but we seem to have $col_max";
#		return \%rv;
#	}
	
	for (my $x=8; $x <= $row_max; $x++)
	{
		# first we want to verify that they actually entered and left the meeting
#		$cell = $ws->get_cell($x, 6);
#		my $entered = $cell->unformatted;
#		$cell = $ws->get_cell($x, 7);
#		my $left = $cell->unformatted;
		$cell = $ws->get_cell($x, 0);
		my $entered = $cell->unformatted;
#		next unless $entered && $left;
		next unless $entered eq 'Yes';
		
		# now get the ID column
		$cell = $ws->get_cell($x, 11);
		my $id = $cell->unformatted;
		
		next unless $id gt '-';
		
		# try to reduce what we are looking for to plain number IDs or aliases
		$id =~ m/^(\d+)|([a-z|A-Z]{2}\d+)$/;
		$id = $1 || $2;
		next unless $id;
		push @{$rv{'ids'}}, $id;
	}
	return \%rv;
}

1;