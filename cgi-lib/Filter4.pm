package Filter4;
use minidee;

sub handler {
	my $r = shift;
	my $SomeObject = $r;
	$r = $r->filter_register();  # Required
	my $fh = $r->filter_input(); # Optional (you might not need the input FH)
	my $redirect = 0;
	my $print_base_flag = 1;
	while (<$fh>)
	{
		#If the script sends a redirect header
		if(/Status:\ 302\ Moved/)
		{
	  		print;
			$redirect =1;
			Apache::exit();
			next;

		}
		elsif ($redirect)
		{
			chomp;
			s/^Location:\ //;
			$SomeObject->internal_redirect($_);
			Apache::exit();
		}

		print '<base>' if $print_base_flag;
		$print_base_flag = 0;

		next if /^<!DOCTYPE/;
		s/^<html(.+?)>/<html>/;
		print;

	}

	print user($r), '</base>';
}

sub user
{
        my $r = shift;
        my $ck = Apache::Cookie->new(AxKit::Apache->request)->parse;
        my $minideeCK = $ck->{'minidee'} ? $ck->{'minidee'}->value : undef;
        my $id = $ck->{'id'} ? $ck->{'id'}->value : undef;
        my $minidee = {};

        if ($id || $minideeCK)
        {
                unless ($minideeCK)
                {
                        $minidee = minidee::get_data($id);
                        $r->headers_out->set('Set-Cookie' => minidee::create_cookie($minidee));
                }
                else
                {
                        $minidee = minidee::parse($minideeCK);
                }
        }
        return '' unless $minidee;
	my $rv =  '<minidee>';
	$rv .= "<$_>$minidee->{$_}</$_>\n" foreach keys %{$minidee};
	return "$rv</minidee>";
}

1;
