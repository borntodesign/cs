package CoopsParser;
###### a routine to take a list of data from which to create member records
###### it will also send them a specialized welcome email
###### and return lists of successes and failures
###### created: 12/14/04	Bill MacArthur
######
###### Modified: 03/27/06	Karl Kohrt
		
use strict;
use Carp;
use Mail::Sendmail;
use lib '/home/httpd/cgi-lib';
use MailingUtils;
use ParseMe;
require 'excluded_domains.lib';

our $mu = new MailingUtils;
our ($db, $tmpl, @columns, $col_cnt, $list, $spid, %col_layout) = ();
our $OUTPUT_DIRECTORY = '/home/httpd/archives/coop';
our $SMTP_SERVER = 'mail.clubshop.com';

###### we are anticipating templates differentiated by the spid
our %TMPL = (
	1	=> '',
	2	=> '',
	3	=> 10360,
	4	=> 10402,
	5	=> 10413,
	6	=> 10424,
	7	=> 10449,
	12	=> 10360,
	3434833 	=> 10513,
	3434835 	=> 10516,
	3434837 	=> 10517, 
	3434839 	=> 10518,       
	3434841 	=> 10519, 
	3434843 	=> 10520,     
	3434845 	=> 10521, 
	3434847 	=> 10522,     
	3434854 	=> 10523,  
	3434856 	=> 10524
);
our $success = our $fails = '';

sub Logging
{
	my $suffix = time();
	if ($success)
	{
		unless (open (SUCCESS, ">$OUTPUT_DIRECTORY/success.$suffix"))
		{
			warn "Failed to open $OUTPUT_DIRECTORY/success.$suffix for writing\n";
		}
		else
		{
			print SUCCESS $success;
			close SUCCESS;
		}
	}

	if ($fails)
	{
		unless (open (FAIL, ">$OUTPUT_DIRECTORY/fail.$suffix"))
		{
			warn "Failed to open $OUTPUT_DIRECTORY/fail.$suffix for writing\n";
		}
		else
		{
			print FAIL $fails;
			close FAIL;
		}
	}
}

sub Mailem
{
	my $row = shift;
	my $tm = $tmpl->{en};

	ParseMe::ParseAll(\$tm, { new => $row });
	my $hdr = ParseMe::Grab_Headers(\$tm);
	$mu->DrHeaders($hdr);
	my %mail = ( %{$hdr}, message => $tm, smtp=>$SMTP_SERVER );

	return $Mail::Sendmail::error unless sendmail(%mail);
	return;
}

sub Parse_List
{
	###### we are expecting a scalar ref for our list and a scalar value for the spid
	###### the list should be double quote encapsulated, comma separated
	($list, $spid, $db) = @_;
	croak "List is empty" unless $list;
	croak "Spid is empty" unless $spid;

	###### the first line of our list should be the column names
	my ($firstline, @lst) = split /\n/, $$list;
	@columns = _parse_line($firstline);
	$col_cnt = scalar @columns;

	###### this will be much easier than trying to remember the position of the vals
	for (my $x=0; $x < scalar @columns; $x++){ $col_layout{$columns[$x]} = $x }

	foreach my $tmp qw(firstname lastname emailaddress memo)
	{
		croak "Missing a required column: $tmp" unless grep $tmp, @columns;
	}

	$tmpl = $mu->Get_Object($db, $TMPL{$spid}) ||
		die "Failed to load mail template: $TMPL{$spid}, for spid: $spid\n";

	foreach (@lst)
	{
		my @cols = _parse_line($_);
		if ($col_cnt != scalar @cols)
		{
			$fails .= $_ .
				",\"Column data count mismatch. Cols should be $col_cnt but was ${\scalar @cols}\"\n";
			print "<br/>" . $_ . ",\"Column data count mismatch. Cols should be $col_cnt but was ${\scalar @cols}\"<br/>\n";
			next;
		}

		###### create an easy to use hashref of these values
		my $tmp = {};
		for (my $x=0; $x < $col_cnt; $x++)
		{
			###### we only want to deal with lower case email addresses
			$cols[$x] = lc $cols[$x] if $columns[$x] eq 'emailaddress';
			$tmp->{$columns[$x]} = $cols[$x];
		}
		$tmp->{emailaddress} =~ s/\s//g;

		(my $match, my $reason) = _BlankEmail($tmp);			# Check for a blank email address.
		($match, $reason) = _DupeMatch($tmp) unless $match;		# Check for a duplicate email address.
		($match, $reason) = Ck_Blocked_Domains($tmp) unless $match;	# Check for a domain that is obviously bad/forbidden.

		if ($match)
		{
			$fails .= $_ .
				",\"$reason: $match\"\n";
			print "<br/>" . $_ . ",\"$reason: $match\"<br/>\n";
			next;
		}
		###### I guess we are ready to create a new membership
		my ($rv, $msg) = _create_mbr(\$tmp, \@cols);
		unless ($rv)
		{
			$fails .= "$_,\"" . ($msg || 'undefined error') . "\"\n";
			print "<br/>" . "$_,\"" . ($msg || 'undefined error') . "\"\n";
			next;
		}
		else
		{
			$success .= "$_,\"$tmp->{alias}\"\n";
			print "*";
			my $rv = Mailem($tmp);
			$success .= ",\"$rv\"\n" if $rv;
		}
	}
	Logging();
	return ($success, $fails);
}

sub Ck_Blocked_Domains
{
	my $row = shift;
	# had to wrap the grep in an eval to silence warnings about using the LIST only once
    	return ($row->{emailaddress} =~ /abuse/i ||
        		eval 'grep $row->{emailaddress} =~ /$_$/i, @excluded_domains::LIST') 
    			? (1, 'This email address is using a blocked domain') : undef;
}

sub _BlankEmail
{
	my $row = shift;
	# checking if the email address is blank
	my $match = 1 if ( ! $row->{emailaddress} || $row->{emailaddress} eq '');
	my $reason = ( $match ) ? 'No email address was provided' : undef;
	return ($match, $reason);
}

sub _create_mbr
{
	my $row = shift;	###### this is a reference to the hashref in the caller
				###### the benefit is that we can added keys & values here
				###### and they will be available to the caller
#$$row->{id}=1;$$row->{alias}=1;$$row->{oid}=1; return 1;
	my $array_ref = shift;
	my $id = $db->selectrow_array("SELECT nextval('members_id_seq')");
	return (0, 'Failed to get the next numeric ID') unless $id;
	my $alias = uc substr( $$row->{ firstname }, 0, 1) .
			uc substr( $$row->{ lastname }, 0, 1) .
			$id;

	###### now we need to stuff our id and alias into our 'dataset' for use later
	$$row->{id} = $id;
	$$row->{alias} = $alias;

	my $qry = "INSERT INTO members ( id, alias, spid, membertype,";
	$qry .= " $_," foreach (@columns);
	chop $qry;
	$qry .= ") VALUES ( $id, ?, $spid, 'm',";
	for (my $x=0; $x < $col_cnt; $x++){ $qry .= " ?," }
	$qry =~ s/,$/)/;

	if ($db->do($qry, undef, ($alias, @{$array_ref})) == 1)
	{
		###### get the oid for the password
		$$row->{oid} = $db->selectrow_array("
			SELECT oid FROM members WHERE id= $id");
		return 1;
	}
	return (0, $DBI::errstr);
}

sub _DupeMatch
{
	my $row = shift;
	###### at this time we are only checking on primary email address
	my $match = $db->selectrow_array("
		SELECT alias FROM members WHERE emailaddress = ?", undef,
		$row->{emailaddress}
	);
	my $reason = ( $match ) ? 'Another membership was found with this email address' : undef;
	return ($match, $reason);
}

sub _parse_line
{
	local $_ = shift;
	s/^"|"$//g;	###### get rid of leading and trailing quotes
	my @cols = split /","/;

	###### convert our encoded newlines to the real McCoy
	$cols[$col_layout{memo}] =~ s/%12/\n/g if $col_layout{memo};
	return @cols;
}		
1;


###### 03/11/05 Added an email template for imports into member #04.
###### 04/01/05 Moved the lower casing of the emailaddress up a few lines so that 
######		it lower cases the original as well as the $tmp hash version. Note
######		that the original $_ of @lst still shows uppercase and will display
######		in the success/fail files, even though the db record is lower case.
######		Fixing that would require more processing time - we don't need that.
###### 04/07/05 Added an email template for imports into member #05.
###### 04/11/05 Added individual print lines to each success(* only) and failure(error message).
###### 04/22/05 Added an email template for imports into member #06.
###### 05/05/05 Added Ck_Blocked_Domains sub to fail certain email addresses.
###### 05/16/05 Added an email template for imports into member #07.
###### 11/23/05 Added an email template for imports into member #3434833.
###### 11/28/05 Added an email template for imports into member #3434835.
###### 12/01/05 Added email templates for imports into member #s:
###### 		3434837, 3434839, 3434841, 3434843, 3434845, 3434847,       
###### 		3434854, 3434856, 3434858   
###### 12/01/05 Removed 3434858 since it will hold members w/purchases.
###### 03/27/06 Added _BlankEmail sub to stop entry of members with no email 
######			and white-space removal from email.





