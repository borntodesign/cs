package XML::local_utils;
use strict;
use Carp;
use XML::LibXML;
use XML::LibXSLT;

###### some locally used routines
###### created: 12/07/04	Bill MacArthur
###### last modified: 11/19/10	Bill MacArthur

use constant INDENT => '   ';		# this is currently 3 spaces
my $RECURSION_LIMIT = 10;			# we shouldn't have any even this deep		
my $me = new XML::local_utils;
my %XMLdom = ();

=head2 new

	Simply creates a new instance of this class.

=head3 Params

	None

=head3 Returns

	An XML::local_utils object
=cut

sub new
{
	my $class = shift;
	return bless ({}, $class);
}

=head2 extractNodeFromXML

	Extract and serialize a node from an XML document.
	The DOM that is extracted upon the first call for a particular XML document is cached in a global package variable
	so as to avoid having to reparse the document on every call to this method.
	
=head3 Params

	A scalar XPATH representation of the node we want to extract, like: /root/node1
	A scalar reference to the XML document

=head3 Returns

	The string value contained within the node of the XML document

=cut

sub extractNodeFromXML
{
	my ($self, $node, $xmlref) = self_or_default(@_);
	die "The XML should be passed as a scalar reference" unless $xmlref && ref $xmlref eq 'SCALAR';
	
	# $ref should have a name like 'HASH {cm039ido}',
	# this will uniquely identify it and cache it in the package var so we don't have to reparse the DOM every time
	$XMLdom{$xmlref} ||= XML::LibXML->new->parse_string(${$xmlref});
	my $results = $XMLdom{$xmlref}->findnodes($node)->string_value();

#	print "node: $results\n";
	return $results;
}

sub hashref2xml
{
	###### convert a hashref of varying levels to nested XML
	my ($self, $ref, $indent, $depth) = self_or_default(@_);
	$indent ||= '';
	$depth ||= 0;

	die "Recursion exceeded $RECURSION_LIMIT levels" if $depth > $RECURSION_LIMIT;

	unless ($ref)
	{
		croak "Arg is undef";
	}
	###### our first time through we need a key to set the name for the outer node
	elsif ($depth == 0)
	{
		croak "Initial arg must be a hashref\n".
			"Ref = " . ref $ref
			unless ref $ref eq 'HASH';
	}
	else
	{
		croak "Args in depth must be arrays or hashrefs.\n" .
			"Depth = $depth\n".
			"Ref = " . ref $ref
			unless ((ref $ref eq 'HASH') || ref $ref eq 'ARRAY');
	}
	my $rv = '';

	###### we have to handle an array of hashes differently than a hash of hashes
	###### for an array, the enclosing node will already have been handled
	###### in the parent process
	foreach my $key (keys %{$ref})
	{
		if (ref $ref->{$key} eq 'HASH')
		{
			$rv .= "$indent<$key>\n";
			$rv .= hashref2xml($ref->{$key}, $indent.INDENT, ($depth + 1));
			$rv .= "$indent</$key>\n";
		}
		elsif (ref $ref->{$key} eq 'ARRAY')
		{
			foreach (@{$ref->{$key}})
			{
				$rv .= "$indent<$key>\n";
				$rv .= hashref2xml($_, $indent.INDENT, ($depth + 1));
				$rv .= "$indent</$key>\n";
			}
		}
		else
		{
			$rv .= "$indent<$key>${\quote_xml_chars(defined $ref->{$key} ? $ref->{$key} : '')}</$key>\n";
		}
	}
	return $rv;
}

sub quote_xml_chars
{
	###### borrowed from XML::Dumper
	local $_ = shift;
	return $_ if not defined $_;
	s/&/&amp;/g;
	s/</&lt;/g;
	s/>/&gt;/g;
	s/'/&apos;/g;
	s/"/&quot;/g;
	s/([\x80-\xFF])/&XmlUtf8Encode(ord($1))/ge;
	return $_;
}

=head2 flatXHTMLtoHashref

Initially this will be written to take a "flat" group of XHTML nodes which could have HTML interspersed through the content
and convert it to a hashref similar to how XML::Simple renders with XMLin.
The problem with XML::Simple is that it will not render a mixed node simply as serialized text,
it will render it as a DOM representation which is not exactly what we want when we have HTML mixed with text.

Later we may embellish this method to deal with a document with a true heirarchy,
but for now we are looking for all the child nodes off of "body".
For example, the following document will resolve to a hashref with two keys.
The key to creating a valuable hash are the ID attributes on the tags.
These should be compliant XML ids: unique throughout the document and starting with a letter or underscore.

	n1 => 'line 1 ITALIAN text',
	n2 => 'line 2 <strong>should be bold ITALIAN text</strong>'

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
	<title>Untitled document</title>
	</head>
	<body>
	<p id="n1" class="p">line 1 ITALIAN text</p>
	<p id="n2">line 2 <strong>should be bold ITALIAN text</strong></p>
	... other nodes on the body, node type not significant
	</body>
	</html>

=head3
Params:

	XHTML document as a scalar

=head3
Returns:

	hash reference

=cut

sub flatXHTMLtoHashref
{
	my ($self, $html, $args) = self_or_default(@_);
	return {} unless $html;
	my $dom = XML::LibXML->new->parse_string($html);
	my $results = $dom->findnodes('/html/body/*');
	my %rv = ();
	foreach my $context ($results->get_nodelist) {
		my $id = $context->getAttribute('id');
               unless ($id){
                       warn "The following node is missing the ID attribute\n";
                       warn $context->serialize;
                       next;
               }

		$rv{$id} = $context->serialize;
		$rv{$id} =~ s#^<.*?>(.*)</.*?>$#$1#s;
	}
	return \%rv;
}

=head2 flatXMLtoHashref
This method was the next logical progression from flatXHTMLtoHashref as it does not require node IDs
and the XML document can be translated in the XML interface, which is preferable in many ways.

Initially this will be written to take a "flat" group of XHTML nodes which could have HTML interspersed through the content
and convert it to a hashref similar to how XML::Simple renders with XMLin.
The problem with XML::Simple is that it will not render a mixed node simply as serialized text,
it will render it as a DOM representation which is not exactly what we want when we have HTML mixed with text.

Later we may embellish this method to deal with a document with a true heirarchy,
but for now we are looking for all the child nodes off of the root node, typically /lang_blocks/.
For example, the following document will resolve to a hashref with two keys.

	n1 => 'line 1 ITALIAN text',
	n2 => 'line 2 <strong>should be bold ITALIAN text</strong>'

	<lang_blocks>
	<n1>line 1 ITALIAN text</n1>
	<n2>line 2 <strong>should be bold ITALIAN text</strong></n2>
	</lang_blocks>

=head3
Params:

	XML document as a scalar

=head3
Returns:

	hash reference

=cut

sub flatXMLtoHashref
{
	my ($self, $xml, $args) = self_or_default(@_);
	return {} unless $xml;
	my $dom = XML::LibXML->new->parse_string($xml);
	my $results = $dom->findnodes('/*/*');
	my %rv = ();
	foreach my $context ($results->get_nodelist) {
		my $id = $context->getName;
		$rv{$id} = $context->serialize;
	# unfortunately I cannot find a method in the XML libraries to do this
		$rv{$id} =~ s#^<.*?>(.*)</.*?>$#$1#s;
	}
	return \%rv;
}

sub self_or_default
{
	my $self = shift;
	return ($self, @_) if ref $self && $self =~ /XML::local_utils/;	# we received a blessed reference
	return ($me, @_) if $self eq 'XML::local_utils';				# we received a class name
	return ($me, $self, @_);										# we were purely a function call
}

sub XmlUtf8Encode
{
# ============================================================
# borrowed from XML::DOM (actually from XML::Dumper)
    my $n = shift;
    if ($n < 0x80) {
	return chr ($n);
    } elsif ($n < 0x800) {
        return pack ("CC", (($n >> 6) | 0xc0), (($n & 0x3f) | 0x80));
    } elsif ($n < 0x10000) {
        return pack ("CCC", (($n >> 12) | 0xe0), ((($n >> 6) & 0x3f) | 0x80),
                     (($n & 0x3f) | 0x80));
    } elsif ($n < 0x110000) {
        return pack ("CCCC", (($n >> 18) | 0xf0), ((($n >> 12) & 0x3f) | 0x80),
                     ((($n >> 6) & 0x3f) | 0x80), (($n & 0x3f) | 0x80));
    }
    return $n;
}

=head2 xslt

A simple wrapper for xslt transformations in case we want to globally  move to a different method.

=head3
Params:

	We are expecting a ref to an xsl document as the first arg
	Aand a ref to the xml as the 2nd
	We may accept a hashref of optional other args if needed

=head3
Returns:

	The transformed document.

=cut

sub xslt($$;$)
{
	###### we are expecting a ref to an xsl document as the first arg
	###### and a ref to the xml as the 2nd.
	###### we may accept a hashref of optional other args if needed
	my ($self, $xsl, $xml, $opts) = self_or_default(@_);

	croak "XSL is empty or null" unless $xsl;
	croak "XML is empty or null" unless $xml;

	my $parser = XML::LibXML->new('huge'=>1);
	my $xslt = XML::LibXSLT->new();
	$xslt->max_depth(1750);	# the default for this value is 250... but it had to be increased to fix the partner/"genealogy" reports with the taproots in TNT 2.0
	my $source = $parser->parse_string($xml);
	my $style_doc = $parser->parse_string($xsl);
	my $stylesheet = $xslt->parse_stylesheet($style_doc);
	my $results = $stylesheet->transform($source);
	return $stylesheet->output_string($results);
}

1;
				
###### 12/09/04 removed the self->{depth} var in place of a passed argument
###### 11/19/10 added the flatXMLtoHashref method
###### 11/17/14 increased the $xslt->max_depth from 500 to 1000 for the team reports again... yes this is bad, but there is no interest from Dick in really addressing the looming issues
###### 01/02/15 increaded it again from 1000 to 1500... sigh...
