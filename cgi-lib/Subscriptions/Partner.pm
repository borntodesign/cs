package Subscriptions::Partner;
use strict;
use parent qw(Subscriptions::General);

=head1 Subscriptions::Partner
=head2 Description

Started as a collection of general methods, pertaining to Partner subscriptions, that will be reused at least once

=cut

my $Cache = Subscriptions::General->Cache;

sub GetAnnualRenewal
{
	my ($self, $id) = @_;
	
	$Cache->{'AnnualRenewal'} ||= $self->db->selectrow_hashref("
		SELECT s.*
		FROM annual_vip_subscription_renewals s
		WHERE s.member_id= ?", undef, $self->idOrCached($id));
	
	return $Cache->{'AnnualRenewal'};
}

sub GetMonthlyRenewal
{
	my ($self, $id) = @_;
	
	$Cache->{'MonthlyRenewal'} ||= $self->db->selectrow_hashref("
		SELECT s.*
		FROM monthly_vip_subscription_renewals s
		WHERE s.member_id= ?", undef, $self->idOrCached($id));
	
	return $Cache->{'MonthlyRenewal'};
}

=head2 AnnualReplacements

Provided the member does not have an active annual subscription, generate a list of annual subscriptions of the 'renewal' type... in other words, not the initial type
For example: type 4 instead of type 2

=head3 Arguments

A hashref with at least a country code on the 'country_code' key.
An optional key 'id' with a numeric ID. If this value is not provided, the cached value stored by setMemberID will be used.

=head3 Returns

An array reference with a list of subscriptions of the partner_annual_subscription_replacements_by_country type

=cut

sub AnnualReplacements
{
	my ($self, $args) = @_;
	my $rv = ();
	
	# first check to see if they have a live monthly record that is paid and active... if they do, then we cannot do a new subscription
	# we should be doing an upgrade in that case, if we want more, or we should be creating a downgrade order, otherwise
	if (! $self->hasActiveAnnual($self->idOrCached($args->{'id'})))
	{
		$rv = $self->getAnnualSubscriptionReplacements($args->{'country_code'});
	}
	
	return $rv;
}

=head2 MonthlyReplacements

Provided the member does not have an active monthly subscription, generate a list of monthly subscriptions of the 'renewal' type... in other words, not the initial type
For example: type 41 instead of type 40, type 3 instead of type 1

=head3 Arguments

A hashref with at least a country code on the 'country_code' key.
An optional key 'id' with a numeric ID. If this value is not provided, the cached value stored by setMemberID will be used.

=head3 Returns

An array reference with a list of subscriptions of the partner_monthly_subscription_replacements_by_country type

=cut

sub MonthlyReplacements
{
	my ($self, $args) = @_;
	my $rv = ();
	
	# first check to see if they have a live monthly record that is paid and active... if they do, then we cannot do a new subscription
	# we should be doing an upgrade in that case, if we want more, or we should be creating a downgrade order, otherwise
	if (! $self->hasAnyMonthly($self->idOrCached($args->{'id'})))
	{
		$rv = $self->getMonthlySubscriptionReplacements($args->{'country_code'});
	}
	
	return $rv;
}

sub getAnnualSubscriptionReplacements
{
	my ($self, $country_code) = @_;
	my @rv = ();
	my $sth = $self->db->prepare('SELECT * FROM partner_annual_subscription_replacements_by_country WHERE country_code=?');
	$sth->execute($country_code);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	return \@rv;
}

sub getMonthlySubscriptionReplacements
{
	my ($self, $country_code) = @_;
	my @rv = ();
	my $sth = $self->db->prepare('SELECT * FROM partner_monthly_subscription_replacements_by_country WHERE country_code=?');
	$sth->execute($country_code);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}
	return \@rv;
}

sub hasActiveAnnual
{
	my ($self, $id) = @_;
	foreach ( @{$self->Live($self->idOrCached($id))} )
	{
		# a -0- means that the anniversary is approaching, but it is at least still active at the moment
		return 1 if (defined $_->{'status_flag'}) && $_->{'sub_class'} eq 'VA';
	}
	return undef;
}

sub hasActiveMonthly
{
	my ($self, $id) = @_;
	foreach ( @{$self->Live($self->idOrCached($id))} )
	{
		return 1 if $_->{'status_flag'} && $_->{'sub_class'} eq 'VM';
	}

	return undef;
}

sub hasAnyMonthly
{
	my ($self, $id) = @_;
	foreach ( @{$self->Live($self->idOrCached($id))} )
	{
		return 1 if $_->{'sub_class'} eq 'VM';
	}

	return undef;
}

1;