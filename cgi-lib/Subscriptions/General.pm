package Subscriptions::General;
use strict;

=head1 Subscriptions::General
=head2 Description

Started as a collection of general methods, pertaining to subscriptions, that will be reused at least once.
This will also serve as a base class for the other classes under Subscriptions::

=cut

my (%Cache) = ();

=head2 new()

=head3 Arguments

Arguments should be passed in as key/value pairs
Recognized keys:
	'db'=> an active DB handle

=cut

sub new
{
	my ($class, %args) = @_;
	die "We minimally need an active DB handle to instantiate this class" unless $args{'db'};
	
	return bless \%args, $class;
}

sub db
{
	my $self = shift;

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

=head2 Cache

A static method that simply returns a reference to the %Cache

=cut

sub Cache { return \%Cache; }

=head2 FlushCache

A static method that reinitializes the %Cache to undef.

=cut

sub FlushCache { %Cache = (); }

=head2 Live

Get a list of live subscriptions, currently in the form of the VIEW my_live_subscriptions.
The results are cached for reuse.

=head3 Arguments

A numeric member ID

=head3 Returns

An array reference where each element of the array represents a record from the DB

=cut

sub Live
{
	my ($self, $id) = @_;
	$id ||= $Cache{'id'};
	die "A numeric ID is required for Live()" unless $id;
	
	$Cache{'Live'} ||= $self->getLive($id);
	return $Cache{'Live'};
}

sub getLive
{
	my ($self, $id) = @_;
	my @rv = ();
	my $sth = $self->db->prepare('SELECT s.* FROM my_live_subscriptions s WHERE s.member_id= ?');
	$sth->execute($id);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		push @rv, $tmp;
	}

	return \@rv;
}

=head2 idOrCached

Methods requiring an ID can call this method with their ID argument and if their argument is undef, then they will get back the cached value of the ID set by setMemberID()

=head3 Arguments

Optional: a numeric ID

=head3 Returns

	The argument supplied, or the cached ID value

=cut

sub idOrCached
{
	my ($self, $id) = @_;
	return $id || $Cache{'id'};
}

=head2 setMemberID

Cache this member ID for all calls to this instance of the class.
Note, this method does not have to be called explicitly, but if it is called, the cached value will be used if an ID is not supplied in the arguments to one of the other methods.

=head3 Arguments

A numeric ID

=head3 Returns

The previous value.

=cut

sub setMemberID
{
	my ($self, $id) = @_;
	my $rv = $Cache{'id'};
	$Cache{'id'} = $id;
	return $rv;
}

1;