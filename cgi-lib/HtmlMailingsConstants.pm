package HtmlMailingsConstants;
require Exporter;
@ISA = qw/Exporter/;
@EXPORT = qw/
	_5pxrow
	_page_head_gi_1
	_page_head_cr_1
	_page_end_1
	_10pxrow
	_15pxrow
	_20pxrow
	_tr_td_ac_vt
	_tr_td_gi_1
	_tr_td_gi_2
	_td_tr
	_unsubscribe_url
	_downgrade_url
	_nl
	_nlx2
	_facebook_only
	_new_cs_content_wrap_open
	_new_cs_content_wrap_close
	/;

use constant _nl => "\n";
use constant _nlx2 => "\n\n";
use constant _5pxrow => '<tr><td style="font-size:5px">&nbsp;</td></tr>';
use constant _10pxrow => '<tr><td style="font-size:10px">&nbsp;</td></tr>';
use constant _15pxrow => '<tr><td style="font-size:15px">&nbsp;</td></tr>';
use constant _20pxrow => '<tr><td style="font-size:20px">&nbsp;</td></tr>';
use constant _page_end_1 => '</tbody></table></td></tr></tbody></table></body></html>';
use constant _tr_td_ac_vt => '<tr><td align="center" valign="top">';
use constant _tr_td_gi_1 => '<tr><td align="center" valign="top" style="font-size: 11px; color: rgb(255, 255, 255); font-family: Arial,Helvetica,sans-serif;">';
use constant _tr_td_gi_2 => '<tr><td align="left" valign="top" style="font-size: 11px; color: rgb(255, 255, 255); font-family: Arial,Helvetica,sans-serif;">';
use constant _td_tr => '</td></tr>';
use constant _unsubscribe_url => 'http://www.clubshop.com/cgi/m?e=[%member.emailaddress%];o=[%member.oid%]';
use constant _downgrade_url => 'http://www.clubshop.com/cgi/m?e=[%member.emailaddress%];o=[%member.oid%]';
use constant _new_cs_content_wrap_open => '<table width="100%" border="0" cellpadding="10" cellspacing="1" bgcolor="#FCA640"><tr><td bgcolor="#00376F"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_header.png" width="650" height="122"></td></tr><tr><td align="center" bgcolor="#FFFFFF"><table width="580" border="0" cellspacing="0" cellpadding="0"><tr><td>';
use constant _new_cs_content_wrap_close => '</td></tr></table></td></tr><tr><td><img src="http://www.clubshop.com/cs/mail/en/images/cs_email_generic_footer.png" width="650" height="74"></td></tr></table></td></tr></table>';

use constant _page_head_gi_1 => '<html>
<head>
<title>[%Subject%]</title>
</head>
<body marginwidth="0" marginheight="0" bgcolor="#163817" topmargin="0" leftmargin="1" body="">
<table height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#163817" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table cellspacing="0" cellpadding="0" border="0" width="650">
<tbody>';

use constant _page_head_cr_1 => '<html>
<head>
<title>[%Subject%]</title>
</head>
<body marginwidth="0" marginheight="0" bgcolor="#00376f" topmargin="0" leftmargin="1" body="">
<table height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#00376f" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<table cellspacing="0" cellpadding="0" border="0" width="650">
<tbody>';

use constant _facebook_only => '<a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank"><img src="http://www.clubshop.com/clubrewards/email/images/logos/facebook_logo.png" width="41" align="absmiddle" border="0" height="37"></a>
	<span style="font-size: 8px; padding-left: 10px;"><a href="http://www.facebook.com/pages/ClubShop-Rewards/75003391133" target="_blank" style="font-weight: bold; font-size: 11px; color: rgb(254, 188, 34); font-family: Arial,Helvetica,sans-serif; text-decoration: underline;">Become a fan on Facebook</a></span>
    <fb:fan profile_id="75003391133" stream="" connections="" width="300"></fb:fan>';
1;
