package MailTools;

=head1 NAME: MailTools

	This class handles the sending of Multi
	part text, and HTML EMails with attachments
	using the MIME::Lite Class.

=head1 VERSION:

	0.2

=head1 AUTHOR:

	Keith

=head1 SYNOPSIS:

	my $email_hashref = {
		to=>'someone@someplace.com',
		subject=>'Hey',
		from=>'me@me.com',
		text=>"Hey,\nIt's me.\n\nKind Regards,\nMe\n",
		html=>
		{
			data=>"<html>
					<body>
						<p>Hey,<p/>
						<p>It's me.</p>
						<p>Kind Regards,
						<br />
						Me</p>
					</body>
					</html>",
			attatchments=>
			[
				'/home/me/someimage.gif'
			]
		}
	};

	eval{
		my $MailTools = MailTools->new();
		$MailTools->sendMultiPartEmail($email_hashref);
	}

=head1
PREREQUISITS:

	MIME::Lite, and MIME::Types must be installed
	MIME::Types is used to automatically
				determin an attachments mime type.

=cut

use strict;
use MIME::Lite;
use MIME::Types;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Set the attributes, instanciate the
	MIME_Types object, and the Mime_Lite object.

=head4
Params:

	None

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $this			= {};
    bless($this, $class);

    $this->__init();
    return $this;
}

=head2
METHODS

=head2
PRIVATE

=head3
__init

=head4
Description:

	This sets the default Subject, From
	address, and instanciates the
	MIME::Lite class into the 'Mime_Lite'
	attribute.

=cut

sub __init
{

	my $this = shift;

    $this->__resetMailHeaders();
    $this->{version}	= '0.2';
    $this->{name}		= 'MailTools';

    $this->{MIME_Types} = MIME::Types->new();
	$this->__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}});

}

=head3
__init

=head4
Description:

	This sets the default Mail_Headers.

=cut
sub __resetMailHeaders
{
	my $this = shift;
	
	$this->{mail_headers} = {};
	$this->{mail_headers}->{Subject} = qq(ClubShop Mall :: "Where The World Shops!");
    $this->{mail_headers}->{From} = 'announcements@clubshop.com';
}

=head3
__factories

=head4
Description:

	A version of the factories pattern is used here to
	instanciate the MIME::Lite class into
	different private subclasses.  Instanciating
	multiple instances of the class is necesarry
	for constructing emails with multiple types
	of content.

=head4
Params:

	hashref
		object	string	The attribute to instanciate
				into.

		mail_headers	hash_ref	The headers to
						set when instanciating
						the new object

=head4
Example:

	__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}})
	__factories({object=>'HTML_Mime_Lite', mail_headers=>{Type => 'multipart/related'}})
	__factories({object=>'Multipart_Mime_Lite', mail_headers=>{Type => 'multipart/alternative'}})

=head4
Errors:

	An exception is thrown if no object,
	or mail_headers parameters are specified.

=cut
sub __factories
{
	my $this = shift;
	my $params = shift;

	if(! defined $params)
	{
		die 'No parameters were set for __factories.';
	}

	if(exists $params->{object} && $params->{object} && exists $params->{mail_headers} && $params->{mail_headers})
	{
		eval{
			$this->{$params->{object}} = MIME::Lite->new(%{$params->{mail_headers}});
			$this->{$params->{object}}->attr('content-type.charset' => 'UTF-8'); 
		};
		if($@)
		{
			die "There was an error instanciating the $params->{object} object, in $this->{name}::__factories(). " . $@;
		}
	}
	else
	{
		die 'The class, and mail_headers parameters must be defined';
	}

}

=head3
__sendMail

=head4
Description:

	Sends the mail calling the send method.
=cut

sub __sendMail
{
	my $this = shift;
	
	$this->{Mime_Lite}->send('smtp','localhost');
	
	#TODO modify the calling methods to take this into account on success 1 is returned, I'm assuming 0 on failure.
	#return $this->{Mime_Lite}->last_send_successful();
	
}

=head3
__setAttatchments

=head4
Description:

	The attachments are set with this method

=head4
Params:

	hash
		html_attatchments	bool
		attatchments	array of hash_refs

=head4
Returns:

	string or null, a string is returned if an error occures.

=cut

sub __setAttatchments
{
	my $this = shift;
	my $params = shift;
	my $errors = '';

	my $ObjectToAttachTo = exists $params->{html_attatchments} ? $this->{HTML_Mime_Lite}:$this->{Mime_Lite};

	if(exists $params->{attatchments} && $params->{attatchments})
	{
		foreach my $hash_ref (@{$params->{attatchments}})
		{

			if( exists $hash_ref->{path} && $hash_ref->{path} !~ /^\s*$/ && exists $hash_ref->{id} && $hash_ref->{id} !~ /^\s*$/)
			{
				if (-f $hash_ref->{path} && -R $hash_ref->{path})
				{

					eval{

						if(!exists $hash_ref->{type} or !$hash_ref->{type})
						{
							my $mime_type = $this->{MIME_Types}->mimeTypeOf($hash_ref->{path});
							$hash_ref->{type} = $mime_type->{MT_type};
						}

						$ObjectToAttachTo->attach(
													Type	=> $hash_ref->{type},
													Id		=> $hash_ref->{id},
													Path	=> $hash_ref->{path},
												);
					};
					if($@)
					{
						$errors .= $@ . "\n";
					}
				}
				else
				{
					$errors .= "Attatchment ID: $hash_ref->{id} was not included in the email.  It was not available.";
				}
			}
			else
			{
				return 'The path, and id parameters must be set.'
			}
		}
	}
	else
	{
		return 'There were no attatchments to attatch, but __setAttatchments was called...';
	}

	return $errors;
}


=head2
PUBLIC

=head3
sendTextEmail

=head4
Description:

	Send a plain text email, and if there are
	attatchments include them.

=head4
Parameters:

	hashref
		to	string This is required. This can
				be a comma separated list
				for multiple recipients.

		cc	string This is Optional. This
					can be a comma separated
					list for multiple recipients.

		bcc	string This is Optional. This
					can be a comma separated
					list for multiple recipients.

		subject	string
		type	string This should not be specified
				except in special cases.

		text	string The text to send as a message.
		attatchments	array of hash_refs
		{
			id=>'the id for the image',
			path=>'The local path for the image.'
		}, etc., etc.


=head4
Return:

	string on error, or null if all goes well.
=cut

sub sendTextEmail
{
	my $this = shift;
	my $params = shift;

	if(! defined $params)
	{
		die 'No parameters were set.';
	}

	my $errors = '';
	$this->__resetMailHeaders();
	$this->{mail_headers}->{Subject} = $params->{subject} if ( exists $params->{subject} && $params->{subject});
	$this->{mail_headers}->{To} = $params->{to} if ( exists $params->{to} && $params->{to});
	$this->{mail_headers}->{Cc} = $params->{cc} if ( exists $params->{cc} && $params->{cc});
	$this->{mail_headers}->{Bcc} = $params->{bcc} if ( exists $params->{bcc} && $params->{bcc});
	$this->{mail_headers}->{From} = $params->{from} if ( exists $params->{from} && $params->{from});

	if ( exists $params->{type} && $params->{type})
	{
		$this->{mail_headers}->{Type} = $params->{type};
	}
	elsif(exists $params->{attatchments})
	{
		$this->{mail_headers}->{Type} = 'multipart/mixed';
	}
	else
	{
		$this->{mail_headers}->{Type} = 'text/plain';
		$this->{mail_headers}->{Data} = $params->{text};
	}


	$this->__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}});


	# Set the text message in the body of the email.
	if($this->{mail_headers}->{Type} ne 'text/plain')
	{
		$this->{Mime_Lite}->attach(
										Type	=> 'text/plain',
										Data	=> $params->{text},
									);
	}

	if(exists $params->{attatchments})
	{
		# If there are any attatchments set them.
		$errors = $this->__setAttatchments({attatchments => $params->{attatchments}});
	}

	# Send the email.
	$this->__sendMail;

	return $errors;

}

=head3
sendHTMLEmail

=head4
Description:

	Send an HTML email, and if there
	are attatchments include them.

=head4
Parameters:

	hashref
		to		string, This is required.
						This can be a comma
						separated list for
						multiple recipients.

		cc	string This is Optional. This
					can be a comma separated
					list for multiple recipients.

		bcc	string This is Optional. This
					can be a comma separated
					list for multiple recipients.

 		subject	string
		type	string, This should not be
						specified except in
						special cases.

		html	hash_ref
		{
			data => 'HTML Content',
			attatchments	array of hash_refs
			{
				type=>'The attachment MIME Type, this can be
						left off if the atachment ends with
						a standard extension type, but if it
						is a language code you must specify
						this option.'

				id=>'the id for the image, this is used when
						referencing the image in the HTML of
						the email',

				path=>'The local path for the image.'
			}, etc., etc.
		}
		attatchments	array of hash_refs
		{
			type=>'The attachment MIME Type, this can be
					left off if the atachment ends
					with a standard extension type, but
					if it is a language code you must
					specify this option.',

			id=>'the id for the image',

			path=>'The local path for the attatchment.'
		}, etc., etc.

=head4
Return:

	string on error, or null if all goes well.

=cut

sub sendHTMLEmail
{
	my $this = shift;
	my $params = shift;

	if(! defined $params)
	{
		die 'No parameters were set.';
	}

	my $errors = '';
	$this->__resetMailHeaders();
	$this->{mail_headers}->{Subject} = $params->{subject} if ( exists $params->{subject} && $params->{subject});
	$this->{mail_headers}->{To} = $params->{to} if ( exists $params->{to} && $params->{to});
	$this->{mail_headers}->{Cc} = $params->{cc} if ( exists $params->{cc} && $params->{cc});
	$this->{mail_headers}->{Bcc} = $params->{bcc} if ( exists $params->{bcc} && $params->{bcc});
	$this->{mail_headers}->{From} = $params->{from} if ( exists $params->{from} && $params->{from});

	if ( exists $params->{type} && $params->{type})
	{
		$this->{mail_headers}->{Type} = $params->{type};
	}
	elsif(exists $params->{attatchments} || exists $params->{html}->{attatchments})
	{
		$this->{mail_headers}->{Type} = 'multipart/mixed';
	}
	else
	{
		$this->{mail_headers}->{Type} = 'text/html';
		$this->{mail_headers}->{Data} = $params->{html}->{data};
	}

	$this->__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}});

	if($this->{mail_headers}->{Type} ne 'text/html')
	{

		$this->__factories({object=>'HTML_Mime_Lite', mail_headers=>{Type => 'multipart/related'}});

		$this->{HTML_Mime_Lite}->attach(
										Type	=> 'text/html',
										Data	=> $params->{html}->{data}
									);

		if(exists $params->{html}->{attatchments})
		{
			# If there are any attatchments set them.
			$this->__setAttatchments({attatchments => $params->{html}->{attatchments}, html_attatchments=>1});
		}


		$this->{Mime_Lite}->attach($this->{HTML_Mime_Lite});
	}



	if(exists $params->{attatchments})
	{
		# If there are any attatchments set them.
		$errors = $this->__setAttatchments({attatchments => $params->{attatchments}});
	}

	# Send the email.
	$this->__sendMail;

	return $errors;
}

=head3
sendMultiPartEmail

=head4 Description:

	Send an email with HTML, plain text,
	and if there are attatchments include
	them.  When you are including images
	in the HTML use the id (src="the id
	you specify") you assign otherwise
	the image will not show up. The images
	that are included in the
	html->{attatchments} array will not
	show up as attatchments in the email,
	but will be displayed in the HTML of
	the email.

=head4
Parameters:

	hashref

		to	string This is required. This
					can be a comma separated
					list for multiple recipients.

		cc	string This is Optional. This
					can be a comma separated
					list for multiple recipients.
					
 		subject	string
		type	string This should not be specified
						except in special cases.

		text	string the text for the message
		html	hash_ref
		{
			data => 'HTML Content',
			attatchments	array of hash_refs
			{
				id=>'the id for the image, this is
					 used when referencing the image
					 in the HTML of the email',

				path=>'The local path for the image.'

			}, etc., etc.
		}
		attatchments	array of hash_refs
		{
			id=>'the id for the image',
			path=>'The local path for the attatchment.'
		}, etc., etc.

=head4 Return:

	string on error, or null if all goes well.

=head4
Errors:


	An exception is thrown if the content type of the email
	can not be determined.

=cut

sub sendMultiPartEmail
{
	my $this = shift;
	my $params = shift;

	if(! defined $params)
	{
		die 'No parameters were set.';
	}

	my $errors = '';

	$this->__resetMailHeaders();
	
	$this->{mail_headers}->{Subject} = $params->{subject} if ( exists $params->{subject} && $params->{subject});
	$this->{mail_headers}->{To} = ( exists $params->{to} && $params->{to}) ? $params->{to} : undef;
	$this->{mail_headers}->{From} = $params->{from} if ( exists $params->{from} && $params->{from});
	$this->{mail_headers}->{Cc} = $params->{cc} if ( exists $params->{cc} && $params->{cc});
	$this->{mail_headers}->{Bcc} = $params->{bcc} if ( exists $params->{bcc} && $params->{bcc});
	
	if ( exists $params->{type} && $params->{type})
	{
		$this->{mail_headers}->{Type} = $params->{type};
	}
	elsif(exists $params->{attatchments} || exists $params->{html}->{attatchments})
	{
		$this->{mail_headers}->{Type} = 'multipart/mixed';
	}
	elsif(exists $params->{html} && $params->{html} && exists $params->{text} && $params->{text})
	{
		$this->{mail_headers}->{Type} = 'multipart/alternative';
	}
	elsif(exists $params->{html}->{data} && $params->{html}->{data})
	{
		$this->{mail_headers}->{Type} = 'text/html';
		$this->{mail_headers}->{Data} = $params->{html}->{data};
	}
	elsif (exists $params->{text} && $params->{text})
	{
		$this->{mail_headers}->{Type} = 'text/plain';
		$this->{mail_headers}->{Data} = $params->{text};
	}
	else
	{
		die ' The type of email is unrecognizable, the passed in parameters were not valid, or none were specified.';
	}

	$this->__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}});

	$this->__factories({object=>'Multipart_Mime_Lite', mail_headers=>{Type => 'multipart/alternative'}});

	if($this->{mail_headers}->{Type} ne 'text/plain' && exists $params->{text} && $params->{text})
	{
		$this->{Multipart_Mime_Lite}->attach(
										Type	=> 'text/plain',
										Data	=> $params->{text},
									);
	}

	if($this->{mail_headers}->{Type} ne 'text/html' && exists $params->{html}->{data} && $params->{html}->{data})
	{
		$this->__factories({object=>'HTML_Mime_Lite', mail_headers=>{Type => 'multipart/related'}});
		$this->{HTML_Mime_Lite}->attach(
										Type	=> 'text/html',
										Data	=> $params->{html}->{data}
									);

#		$this->{HTML_Mime_Lite}->attr('content-type.charset' => 'UTF-8');

		if(exists $params->{html}->{attatchments})
		{
			# If there are any attatchments set them.
			$errors .= $this->__setAttatchments({attatchments => $params->{html}->{attatchments}, html_attatchments=>1});
		}

		$this->{Multipart_Mime_Lite}->attach($this->{HTML_Mime_Lite});
	}


	$this->{Mime_Lite}->attach($this->{Multipart_Mime_Lite});

	if(exists $params->{attatchments})
	{
		# If there are any attatchments set them.
		$errors .= $this->__setAttatchments({attatchments => $params->{attatchments}});
	}

	# Send the email.
	$this->__sendMail;
	#if(! $this->__sendMail)
	#{
	#	$errors .= ' The email was not sent successfully. ';
	#}

	return $errors;

}


1;

__END__

=head1
SEE ALSO

L<MailTools>

=head1
CHANGE LOG

	06/19/2009	Keith	Added the ability to CC, and BCC emails.

	10/26/2009	Keith	Added the "__resetMailHeaders" subroutine, and added it to the "sendHTMLEmail",	"sendTextEmail", and the "sendMultiPartEmail"

	12/06/2009	Keith	Added "$this->{$params->{object}}->attr('content-type.charset' => 'UTF-8');" to the __factories method.  
						We were having issues with mail clients recogniziong the correct Character Encoding.

=cut