package Sanitize;

=head1 NAME:
Sanitize

	This Class is ment to sanitize data based on PostgreSQLs Data Type specifications.
	
	THIS CLASS IS NOT FINISHED, BUT WE NEED SOMETHING REALLY BAD THAT SANITIZES DATA,
	OS IT IS IS WORK IN PROGRESS.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Module:

	This Class is meant to sanitize data before being put into the database, but after it has been validated.
	If a data type is to long for the field it is being inserted into it will be truncated.
	If it contains potentially harmfull data it will be made safe, or removed
		Example:
			#TODO: decide what to do in these instances, and document them here
			"Tommy; SELECT password FROM members;" will be converted into ""
			
	This should work with the database cache that is created to grab field data types, and sanitize the data .
	You should check for validity of the data, and if it isn't valid you should throw an error, or sanitize the data
	Ideally in the controll you would check, and give the user an option to correct the data, in the module you should just sanitize before insertion.

=cut

use strict;
use Data::Validate;
use Data::Validate::Email;

sub new
{
    my $class			= shift;
    my $self			= {};
    
    $self->{'db'} = shift;
    #TODO: If the database cache is so integrated, I should just instanciate it instead of passing it in.
    $self->{'DatabaseCache'} = shift;
    
    die 'A Database connection is required. ' if ! $self->{db};
    die 'A reference to the DatabaseCache is required. ' if ! $self->{'DatabaseCache'};
	
    bless($self, $class);
    
    $self->_init();
    return $self;
}


=head2
METHODS

=head2
PRIVATE

=head3
_init

=head4
Description:

	Description of what is set in _init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{
	
	my $self = shift;
	
    $self->{'VERSION'}	= '0.1';
    $self->{'NAME'}		= 'Sanitize';
    $self->{'data_lengths'} = {
    							'text'=>3000
    						};
	
}


=head3
_adjustLength

=head4
Description:

	Truncate the length of the string, if the string is longer than the allowed length the information at the end of the string is truncated.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _adjustLength
{
	
	my $self = shift;
	my $data_ref = shift;
	my $length = shift;
	
	my $characters_to_remove = 0;
	
	die 'Sanitize::_adjustLength - The "data_ref" parameter is a required parameter, but it is not a valid Scalar Ref, or something is just wrong with it. ref(data_ref): ' . ref($data_ref) if ref($data_ref) ne 'SCALAR';
	
	my $string_length = length($$data_ref);
	
	if ($string_length > $length)
	{
		$characters_to_remove = $string_length - $length;
		$$data_ref = substr($$data_ref, 0, - $characters_to_remove);
	}
	
}


sub sanitizeByDbData
{
	
	my $errors = '';
	my $self = shift;
	my $data_ref = shift;
	my $table_name = shift;
	my $field_name = shift;
	my $schema_name = shift;
	
	my $length = undef;
	
	die 'The "data_ref" parameter is a required fields, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	die 'The correct parameter pairs were not defined.  The "table_name", and "field_name" need to be defined.' if( ! $table_name && ! $field_name );
	
	$schema_name = 'public' if ! $schema_name;
	
	$self->{'DatabaseCache'}->setSchema($schema_name) if $schema_name ne 'public';
	
	my $table_info = $self->{'DatabaseCache'}->getTableCache($table_name);
	
	$table_info = $self->{'DatabaseCache'}->getTableCache($table_name, 1) if ! exists $table_info->{$field_name};
	
	die 'The data cannot be sanitized, the requested field does not exist.' if ! exists $table_info->{$field_name};
	
	#TODO: Determin if this is needed, it may be over kill.
	die 'This field is empty, but it is a required field.' if (! $$data_ref && ! $table_info->{$field_name}->{NULLABLE});
	
	if ($table_info->{$field_name}->{TYPE_NAME} =~ /^character|text/)
	{
		#TODO: Decide what to do with the "character" data type.
		$length = $table_info->{$field_name}->{TYPE_NAME} =~ /text/ ? $self->{data_lengths}->{text} : $table_info->{$field_name}->{COLUMN_SIZE};
		
	}
	#TODO: Decide if I should treat "boolean" as a number
	#TODO: Decide what to do with serial
	elsif($table_info->{$field_name}->{TYPE_NAME} =~ /smallint|integer|bigint|decimal|numeric|real|double\ precision|double|serial|bigserial/)
	{
		#Get the specs for the length of the different data types.
		#TODO: see how we can work with this numeric(5,4), and other defined sizes.
		#$length
		
		
		$length = $table_info->{$field_name}->{COLUMN_SIZE} if $table_info->{$field_name}->{COLUMN_SIZE};
		
		
#		if($table_info->{$field_name}->{TYPE_NAME} =~ /decimal/)
		
		
		if($table_info->{$field_name}->{TYPE_NAME} =~ /smallint/)
		{
			#-32768 to +32767
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /integer/)
		{
			#-2147483648 to +2147483647
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /bigint/)
		{
			#-9223372036854775808 to 9223372036854775807
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /numeric/)
		{
			#TODO:  To check the length I should probably just remove the decimal, and see if there are more than 1000 digits.
			#The type numeric can store numbers with up to 1000 digits of precision and perform calculations exactly AND So the number 23.5141 has a precision of 6 and a scale of 4. NUMERIC(precision, scale)
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /real/)
		{
			#6 decimal digits precision
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /double\ precision|double/)
		{
			#15 decimal digits precision
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /serial/)
		{
			#1 to 2147483647
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /serial/)
		{
			#1 to 9223372036854775807
		}
		
	}
	elsif($table_info->{$field_name}->{TYPE_NAME} =~ /date|timestamp/)
	{
		#Get the specs for the length of the different dates.
		#$length
	}
	



}


=head2
PUBLIC

=head3
sanitize

=head4
Description:

	Sanitize the data based on the data type.  If a table, and field 
	name are passed in the data type will extracted from the 
	database, or database cache.  If it is an email specify so in
	the data_type parameter.  When a table, and field are specified
	the data will also be truncated to fit the size of the field
	in the database.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.

=cut

sub sanitize
{

	my $errors = '';
	my $self = shift;
	my $data_ref = shift;
	my $data_type = shift;
	my $length = shift;
	my $table_name = shift;
	my $field_name = shift;
	my $schema_name = shift;
	
	
	die 'The "data_ref" parameter is a required fiels, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	die 'The correct parameter pairs were not defined.  Either "data_type", and "length" need to be defined, or "table_name", and "field_name" need to be defined.' if((!$data_type && !$length) || (!$table_name && !$field_name));
	
	$schema_name = 'public' if ! $schema_name;
	
	#Lots, and lots of code
	
	#If the "table_name", and "field_name" are passed in we adjust the data to the specifics of that field in the database.
	#size, is nullable, datatype, etc.
	if ($table_name)
	{
		
		if ($schema_name ne 'public')
		{
			my $current_schema = $self->{'DatabaseCache'}->getSchema();
			$self->{'DatabaseCache'}->setSchema($schema_name) if ($schema_name ne $current_schema);	
		}
		
		my $table_info = $self->{'DatabaseCache'}->getTableCache($table_name);
		
		$table_info = $self->{'DatabaseCache'}->getTableCache($table_name, 1) if ! exists $table_info->{$field_name};
		
		die 'The data cannot be sanitized, the requested field does not exist.' if ! exists $table_info->{$field_name};
		
		#TODO: Determin if this is needed, it may be over kill.
		die 'This field is empty, but it is a required field.' if (! $$data_ref && ! $table_info->{$field_name}->{NULLABLE});
		
		if ($table_info->{$field_name}->{TYPE_NAME} =~ /^character|text/)
		{
			#TODO: Decide what to do with the "character" data type.
			$length = $table_info->{$field_name}->{TYPE_NAME} =~ /text/ ? $self->{data_lengths}->{text} : $table_info->{$field_name}->{COLUMN_SIZE};
			
		}
		#TODO: Decide if I should treat "boolean" as a number
		#TODO: Decide what to do with serial
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /smallint|integer|bigint|decimal|numeric|real|double\ precision|double|serial|bigserial/i)
		{
			#Get the specs for the length of the different data types.
			#TODO: see how we can work with this numeric(5,4), and other defined sizes.
			#$length
			
			if ($table_info->{$field_name}->{COLUMN_SIZE})
			{
				$length = $table_info->{$field_name}->{COLUMN_SIZE};
			}
			else
			{
				if($table_info->{$field_name}->{TYPE_NAME} =~ /smallint/)
				{
					#-32768 to +32767
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /integer/)
				{
					#-2147483648 to +2147483647
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /bigint/)
				{
					#-9223372036854775808 to 9223372036854775807
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /decimal/)
				{
					
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /numeric/)
				{
					#The type numeric can store numbers with up to 1000 digits of precision and perform calculations exactly AND So the number 23.5141 has a precision of 6 and a scale of 4. NUMERIC(precision, scale)
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /real/)
				{
					#6 decimal digits precision
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /double\ precision|double/)
				{
					#15 decimal digits precision
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /serial/)
				{
					#1 to 2147483647
				}
				elsif($table_info->{$field_name}->{TYPE_NAME} =~ /serial/)
				{
					#1 to 9223372036854775807
				}


			}
			
			
		}
		elsif($table_info->{$field_name}->{TYPE_NAME} =~ /date|timestamp/)
		{
			#Get the specs for the length of the different dates.
			#$length
		}
		
	}
	
	
	
	
	$$data_ref = $self->_adjustLength($$data_ref, $length);
	
	
	
	
	
	return $errors;

}

sub adjustLengthString
{

	my $errors = '';
	my $self = shift;
	my $data_ref = shift;
	my $data_type = shift;
	my $length = shift;
	my $table_name = shift;
	my $field_name = shift;
	my $schema_name = shift;
	
	
	die 'The "data_ref" parameter is a required fiels, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	die 'The correct parameter pairs were not defined.  Either "data_type", and "length" need to be defined, or "table_name", and "field_name" need to be defined.' if((!$data_type && !$length) || (!$table_name && !$field_name));
	
	$schema_name = 'public' if ! $schema_name;
	
	#Lots, and lots of code
	
	#If the "table_name", and "field_name" are passed in we adjust the data to the specifics of that field in the database.
	#size, is nullable, datatype, etc.
	if ($table_name)
	{
		if ($schema_name ne 'public')
		{
			my $current_schema = $self->{'DatabaseCache'}->getSchema();
			$self->{'DatabaseCache'}->setSchema($schema_name) if ($schema_name ne $current_schema);	
		}
		
		my $table_info = $self->{'DatabaseCache'}->getTableCache($table_name);
		
		$table_info = $self->{'DatabaseCache'}->getTableCache($table_name, 1) if ! exists $table_info->{$field_name};
		
		die 'The data cannot be sanitized, the requested field does not exist.' if ! exists $table_info->{$field_name};
		
		#TODO: Determin if this is needed, it may be over kill.
		die 'This field is empty, but it is a required field.' if (! $$data_ref && ! $table_info->{$field_name}->{NULLABLE});
		
		if ($table_info->{$field_name}->{TYPE_NAME} =~ /^character|text/)
		{
			#TODO: Decide what to do with the "character" data type.
			$length = $table_info->{$field_name}->{TYPE_NAME} =~ /text/ ? $self->{data_lengths}->{text} : $table_info->{$field_name}->{COLUMN_SIZE};
			
		}
	}
	
	$$data_ref = $self->_adjustLength($$data_ref, $length);
	
	
	#TODO: Sanitize the data
	
	
	return $errors;

}

=head3
convertToUtf8

=head4
Description:

	Accept a data reference, then check to see if it is utf8,
	if it isn't Encode::Guess is used to convert it to utf8,
	if it is utf8 the character encoding is checked, and the
	utf8 flag is checked.  If it is valid utf8 the utf8 flag
	is turned on if it is off.

=head4
Parameters:

	data_ref
	Reference
	The data to be converted into utf8.

=head4
Return:

	BOOL
	TRUE on sucess

=cut

sub convertToUtf8
{

	use Encode;
	use Encode::Guess;

	my $self = shift;
	my $data_ref = shift;
	
	die 'Sanitize::convertToUtf8 - The "data_ref" parameter is a required fiels, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	return if ! $$data_ref;
	
	my $enc = Encode::Guess::guess_encoding($$data_ref);
	
	if ($enc->name ne 'utf8')
	{
#warn 'Encoding: ' . $enc->name . "   and the data is:$$data_ref" .':';
#		$$data_ref = Encode::Guess::decode("Guess", $$data_ref);
		$$data_ref = $enc->decode($$data_ref);
	}
	else
	{
		#If the encoding is utf8, and is valid utf8 
		Encode::_utf8_on($$data_ref) if ! Encode::is_utf8($$data_ref, 1);
		# the following was a test for a specific scenario by Bill
#		$$data_ref = decode_utf8($$data_ref) unless Encode::is_utf8($$data_ref, 1);
	}
	
	return Encode::is_utf8($$data_ref, 1);
	
}

=head3
sanitizeAmount

=head4
Description:

	Remove all non numeric characters.  If the amount is in a European format "1.234,56" it will be changed to "1234.56"

=head4
Parameters:

	data	integer The data to be sanatized, if any characters besides numbers, and decimals are present they are removed.

=head4
Return:

	string on error, or null if all goes well.

=cut

sub sanitizeAmount
{
	my $self = shift;
	my $data = shift;
	
	if ($data =~ /(\D)(\d{2})$/)
	{
		$data =~ s/(\D)(\d{2})$/.$2/;
		my ($integer, $decimal) = split(/\.\d{2}/, $data);
	} 
	else
	{
		$data =~ s/\D//;
	}
	
}


		
=head3
sanitizeData

=head4
Description:

	Remove questionable characters, and questionable patterns.
	We also remove spaces from the beginning, and end of the string.

=head4
Parameters:

	data_ref
	String Ref
	Any data type is accepted, it is treated as a string, and we remove questionable patterns.

=head4
Return:

	string on error, or null if all goes well.

=cut

sub sanitizeData
{
	my $self = shift;
	my $data_ref = shift;
	
	
	die 'Sanitize::sanitizeData - The "data_ref" parameter is a required parameter, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	$$data_ref =~ s/&/\ /g;
	$$data_ref =~ s/\ \ +/\ /g;
	$$data_ref =~ s/^\ +//;
	$$data_ref =~ s/\ +$//;
	$$data_ref =~ s/;//g;
	$$data_ref =~ s/#!//g;
	$$data_ref =~ s/\/bin\/perl//g;
	$$data_ref =~ s/\/bin\///g;
	$$data_ref =~ s/\/sbin//g;
	$$data_ref =~ s/\/usr\///g;
	$$data_ref =~ s/<.*>//g;
	$$data_ref =~ s/\{.*\}//g;
	$$data_ref =~ s/\[.*\]//g;
	$$data_ref =~ s/\(.*\)//g;
	$$data_ref =~ s/<//g;
	$$data_ref =~ s/>//g;
	
	$self->_adjustLength(\$$data_ref, $self->{'data_lengths'}->{'text'});
	
	 
}


		
=head3
sanitizeForXML

=head4
Description:

	Remove, or replace questionable characters, and questionable patterns that may 
	cause errors when doing XML XSL transformations.

=head4
Parameters:

	data_ref
	String Ref
	Any data type is accepted, it is treated as a string, and we remove questionable patterns.

=head4
Return:

	string on error, or null if all goes well.

=cut

sub sanitizeForXML
{
	my $self = shift;
	my $data_ref = shift;
	
	
	die 'Sanitize::sanitizeForXML - The "data_ref" parameter is a required parameter, but it is not a valid Scalar Ref, or something is just wrong with it. ' if ref($data_ref) ne 'SCALAR';
	
	$$data_ref =~ s/&/&amp;/g;
}


sub getTextLength
{
	my $self = shift;
	
	return $self->{'data_lengths'}->{'text'};	
}

sub setTextLength
{
	my $self = shift;
	my $length = shift;
	
	die 'The length parameter must be defined, and it can only contain numbers.' if (! $length || $length =~ /\D/);
	
	$self->{'data_lengths'}->{'text'} = $length;
		
}


1;

__END__

=head1
SEE ALSO

L<LinkedPackage>, UnlinkedPackage

=head1
CHANGE LOG

	Date	What was changed

=cut

