package ebph;
###### the ebusiness pass handler frontend
###### rather than duplicating a bunch of code multiple times
###### we'll use a module across scripts, we can perform basic authorizations
###### the idea at this time is to pass a DB handle, a CGI handle & member info
###### & course and session info back to the main routine
###### after we are done with authenticating the user and their desired access point
###### last modified: 05-13-04	Bill MacArthur

use strict;
use lib ('/home/httpd/cgi-lib');
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use G_S qw($LOGIN_URL);
use DB_Connect;
use base 'Exporter';

###### Globals
our @EXPORT_OK = qw($ALERT_TEMPLATE $EBIZ_ROOT $PASS_HANDLER_ROOT);
our $ALERT_TEMPLATE = 10292;
our $EBIZ_ROOT = 'http://www.clubshop.com/vip/ebusiness';
our $PASS_HANDLER_ROOT = 'http://www.clubshop.com/cgi/e/ph';

our $q = new CGI; 
our ($db, $id, $meminfo, $course_key) = ();
our $ph_root = 'http://www.clubshop.com/cgi/e/ph';

sub default($)
{
$course_key = shift;
unless ($course_key)
{
	return 'No course key received.', $q;
}
elsif ($course_key  =~ /\D/ || $course_key > 2000000000)
{
	return "Invalid course key received: $course_key", $q;
}

( $id, $meminfo ) = G_S::authen_ses_key( $q->cookie('AuthCustom_Generic') );

###### the intent is to have this script behind an apache protected directory
###### so login issues will be taken care of by that part
###### we just put this in here for back up and external directory testing
unless ( $id && $id !~ /\D/ )
{
	return "You need to login again at <a href=\"$LOGIN_URL?destination=" . $q->script_name . "\">$LOGIN_URL</a>", $q;
}

###### if they are not a VIP, they don't have access to the report
if ( $meminfo->{membertype} !~ 'v|staff' )
{
	return 'Access to the E-Business Materials is restricted to VIPs.', $q;
}

unless ($db = DB_Connect('ebiz')){ return 0}
$db->{RaiseError} = 1;

###### check to see if they have an existing session under this test number
###### also get the correct answers from any previous test
my $sth = $db->prepare("
	SELECT key,
		grade_date,
		passed,
		grade
	FROM ebiz_sessions
	WHERE 	id= $id AND course_key= $course_key");
$sth->execute;
my $session = $sth->fetchrow_hashref;
$sth->finish;

unless ($session->{key})
{
	$db->disconnect;
	return "We're sorry but it appears that you haven't accessed the course materials.", $q;
}

###### let's get some particulars on the course itself
###### they may come in handy for adding to the page
$sth = $db->prepare("
	SELECT key AS course_key,
		active,
		name AS course_name,
		pass_grade,
		course_root,
		active
	FROM 	ebiz_courses
	WHERE 	key= $course_key");
$sth->execute;
my $course = $sth->fetchrow_hashref;
$sth->finish;

###### this is a long shot, but in the event someone has bookmarked an extinct course
unless ($course->{active})
{
	$db->disconnect;
	return "Course: $course_key, is not an active course.", $q;
}									

return undef, $q, $db, $meminfo, $course, $session;
}
1;

###### gave a return value of Zero if our DB connect failed
