package ClubCash;
=head1 NAME:
ClubCash

    General Description

=head1
VERSION:

    0.1

=head1
SYNOPSIS:

=head2
How to Use This Class:

    Verbose Description on how to use this class

=head2
Code Example

    Code Example Goes Here

=head1
PREREQUISITS:

    Other classes, or packages that are used

=cut

use strict;
use DBI;
use DB_Connect;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

    Set the attributes, instanciate the
    MIME_Types object, and the Mime_Lite object.

=head4
Params:

    None

=head4
Returns:

    obj $this

=cut

sub new
{
    my $class= shift;
    my $this= {};
    bless($this, $class);

    $this->__init();
    return $this;
}
=head2
METHODS

=head2
PRIVATE

=head3
__init

=head4
Description:

    This sets the default Subject, From
    address, and instanciates the
  MIME::Lite class into the 'Mime_Lite'
    attribute.

=cut

sub __init
{
}
sub IsLeapYear ($)
{
    my $year = shift;
    return 0 if $year % 4;
    return 1 if $year % 100;
    return 0 if $year % 400;
    return 1;
}
sub GetNeededDates ($)
{
    my $num = shift @_;
    my $DATES={};
    my $year = `date +%Y`;
    my $month = `date +%m`;
    my $day = `date +%d`;
    my $sdat = '';
    my $edat = '';
    my @dayz = (0,31,28,31,30,31,30,31,31,30,31,30,31);
    if (IsLeapYear($year)) {
	$dayz[2] += 1;
    }
    my $cmonth=$month;
    my $cyear=$year;

    chomp($cyear);
    chomp($cmonth);

    $DATES->{TODAY} = "$cyear"."-"."$cmonth"."-"."$day";

    if ($month == 1) {
	my $syear = $cyear - 1;
	$sdat = "$syear"."-12-01";
	$edat = "$syear"."-12-31";
    }
    else {
	my $smonth = $cmonth - 1;
	$sdat = "$cyear"."-"."$smonth"."-01";
	$edat = "$cyear"."-"."$smonth"."-"."$dayz[$smonth]";
    }
    $DATES->{STARTOFMONTH} = "$cyear"."-"."$cmonth"."-01";
    $DATES->{ENDOFMONTH} = "$cyear"."-"."$cmonth"."-"."$dayz[$cmonth]";
    $DATES->{STARTPREVMONTH} = $sdat;
    $DATES->{ENDPREVMONTH} = $edat;
    $DATES->{CURYEAR} = $cyear;
#    warn "som: $DATES->{STARTOFMONTH}, eom: $DATES->{ENDOFMONTH}\n";
#    warn "spm: $DATES->{STARTPREVMONTH}, epm: $DATES->{ENDPREVMONTH}\n";
#    warn "Number of months: $num\n";
    $cmonth *=1.0; ## Turns $cmonth into numeric
    for (my $ii=0; $ii<$num; $ii++) {
        if ($cmonth < 10) {
           $DATES->{som}[$ii] = "$cyear"."-0"."$cmonth"."-01";
           $DATES->{eom}[$ii] = "$cyear"."-0"."$cmonth"."-"."$dayz[$cmonth]";
        }
        else {
           $DATES->{som}[$ii] = "$cyear"."-"."$cmonth"."-01";
           $DATES->{eom}[$ii] = "$cyear"."-"."$cmonth"."-"."$dayz[$cmonth]";
        }
        $cmonth --;
        if ($cmonth <= 0) {
	    $cmonth=12;
            $cyear --;
        }
        if (IsLeapYear($cyear)) {
            $dayz[2] = 29;
	}
	else {
	    $dayz[2] = 28;
	}
    }
    return $DATES;
}
sub Get_ClubCash_Amounts ($$;$)
{
#
# Return a hash of pending, redeemable, and currency of the given id
#
    my $id = shift @_;
    my $cgiini = shift @_;
    my $args = shift;
    my $amt = {};
    my $db = $args->{'db'} || DB_Connect($cgiini);
    $amt->{pending} = 0;
    $amt->{redeemable} = 0;
    $amt->{currency} = ' ';
#
# Start By getting some needed Dates
#
    my $dt = GetNeededDates(1);
#
# Get the data for the current time period
#

    my $query = "SELECT rt.id, 
                  rt.reward_points, 
                  rt.reward_type,
                  rt.currency,
                  rt.cur_xchg_rate,
                  rt.date_redeemable,
                  rt.date_reported,
                  rt.notes,
                  rt.total_pending_rewards
           FROM reward_transactions rt
           WHERE rt.id = $id AND
                 rt.date_reported >= '$dt->{som}[0]'::DATE AND
                 rt.date_reported <= '$dt->{eom}[0]'::DATE
           ORDER BY rt.date_reported, rt.reward_type;";
#warn $query."\n";
    my $td = $db->prepare($query);
    my $rv = $td->execute;
    my $redeem = 0;
    my $pending = 0;

### Loop through reward transactions

    while (my $tmp = $td->fetchrow_hashref) {

        if ($tmp->{date_redeemable} le $dt->{TODAY}) {
# type 4 with 0 points and pending points>0 is a points from pending to redeemable
           if ($tmp->{reward_type} == 21) {
		$pending = $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
		$redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate} - $pending;
           }
	   else {
                $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate};
  	        $pending = 0;
	   }
	}
        else {
	    if ($tmp->{reward_type} == 1) {
                $pending = $tmp->{total_pending_rewards} * $tmp->{cur_xchg_rate};
                $redeem = $tmp->{reward_points} * $tmp->{cur_xchg_rate} - $pending;
	    }
	    else {
                $pending = $tmp->{reward_points} * $tmp->{cur_xchg_rate};
                $redeem = 0;
	    }

	}
        $amt->{pending} += $pending;
	$amt->{redeem} += $redeem;
        $amt->{currency} = $tmp->{currency};
    }

    my $soy = $dt->{CURYEAR} . "-01-01";
    $query = "SELECT rt.id, 
                  rt.reward_points, 
                  rt.reward_type,
                  rt.currency,
                  rt.cur_xchg_rate,
                  rt.date_redeemable,
                  rt.date_reported,
                  rt.total_pending_rewards
           FROM reward_transactions rt
           WHERE rt.id = $id AND
                 rt.date_reported >= '$soy'::DATE AND
                 rt.reward_type in (2,5,8,9,10,13)
           ORDER BY rt.date_reported, rt.reward_type;";
#warn $query."\n";
    $td = $db->prepare($query);
    $rv = $td->execute;
    my $amt_redeemed = 0;
    while (my $tmp = $td->fetchrow_hashref) {
	$amt_redeemed += $tmp->{reward_points} * $tmp->{cur_xchg_rate};
    }
    $amt->{ytd_redeemed} = $amt_redeemed;
    if (!defined($amt->{redeem})) {$amt->{redeem} = 0.0;}
    if (!defined($amt->{pending})) {$amt->{pending} = 0.0;}
if (!defined($amt->{currency})) {$amt->{currency} = " ";}
    return $amt;
}

1;
