package EmailTemplates::Master;

=head1 EmailTemplates::Master

The class that runs the EmailTemplates.cgi script.

=head3 new

Instantiate a new instance of an EmailTemplates object.

=head4 Arguments

Arguments should be passed in on a hashref, currently with these required/optional key/value pairs:
Required	'db'=>an active DB handle
			'gs'=>a G_S object
			'cgi'=>a CGI object
			'tmpl'=>a hashref of templates as email ID/template ID key/value pairs

Optional	'lb'=>a hashref of the language pack (marked optional, but if there are errors that should come from the language pack and it is not there....)
			'e'=>a numeric template ID
			'r'=> a numeric member ID representing the recipient (if this is not provided here, ->r() must be called to set it later on)
			'u'=> a numeric member ID representing the user (if this is not provided here, ->u() must be called to set it later on)

A call to the script should look like this:
EmailTemplates.cgi/email-ID-number/recipient-numeric-ID-number

Alternatively we can also use parameterized values on the 'e' parameter amd the 'r' parameter
EmailTemplates.cgi?e=email-ID-number;r=recipient-numeric-ID-number

=cut

use strict;
use Template;
use parent 'EmailTemplates::Config';

my %TMPL = (
	'wrapper'		=> 11079
);

my $TT = new Template;

my $errstr = '';
my $u = ();			# this will hold the "user" of the interface
#my $r = ();			# this will hold the recipient of the email
my $rcoach = ();	# this will hold the recipient's coach
my $_self = ();

my %LINKS = (
	'home'			=> sub {return 'http://www.clubshop.com/'},
	'survey'		=> sub {return 'http://www.clubshop.com/mall/other/questionnaire.xsp'},
	'toppicks'		=> sub {return 'http://www.clubshop.com/'},
	'gainTP'		=> sub {return 'http://www.clubshop.com/cgi/gainTP.cgi'},
	'apbc'			=> sub {return _interpolateLinks('http://www.clubshop.com/cgi/apbc?alias=[% r.alias %]') },
	'TrialPartner'	=> sub {return _interpolateLinks('http://www.clubshop.com/cgi/TrialPartner?id=[% r.id %]') },
	'TNT_mkt_plan'	=> sub {return 'http://www.clubshop.com/present/tp_present.xml'},
	'mall'			=> sub {return 'http://www.clubshop.com/mall/'},
	'rewardsdirectory'	=> sub {return 'http://www.clubshop.com/cs/rewardsdirectory'},
	's2m'			=> sub { return 'http://www.clubshop.com/become_an_affiliate.html'},
	'PartnerUpgrade'	=> sub {return _interpolateLinks('http://www.clubshop.com/cgi/appx/upg?alias=[% r.alias %]') },
	'TrialPartnerFAQ'	=> sub {return 'http://www.clubshop.com/affiliate/TP.html'},
	'PartnerFAQ'	=> => sub {return 'http://www.clubshop.com/affiliate/becomingapartner.xml'},
	'MarketingWebsite'	=> sub {return _interpolateLinks('http://www.clubshop.com/cgi/members/[% r.alias %]') },
	'AffiliateCompPlan'	=> sub {return 'http://www.clubshop.com/manual/compensation/affiliate.html'},
);

sub new
{
	my ($class, $args) = @_;
	
	die "A DB handle should be passed to new on the 'db' key" unless $args->{'db'};
	die "A G_S object should be passed to new on the 'gs' key" unless $args->{'gs'};
	die "A CGI object should be passed to new on the 'cgi' key" unless $args->{'cgi'};
	warn "The optional 'lb' argument is missing and may be needed" unless $args->{'lb'};
	my $self = bless $args, $class;
	$self->u($args->{'u'});
	$self->r($args->{'r'});
	
	# and now to determine if the user has access to the recipient
	my ($rv) = $args->{'db'}->selectrow_array("SELECT network.access_ck1(?::INTEGER, ?::INTEGER)", undef, $self->r()->{'id'}, $self->u()->{'id'});
	if (! $rv) {
		$errstr = $args->{'lb'}->{'noaccess'};
		return undef;
	}
	
	$self->{'tmpl'} = EmailTemplates::Config::initCfg($args->{'gs'});
	$self->{'abc'}='pop';
	$_self = $self;
	return $self;
}

sub convertToHtml
{
	my ($self, $c) = @_;
	
	# first convert all newlines to br tags
	$c =~ s#\n#<br />#g;
	
	# next convert URLs into anchor tags
	# but only if they are not already in an anchor tag... we use the negative lookbehind for that
	$c =~ s#(?<!href=")(https?://[a-zA-Z0-9_&;\?\.\/=%]*)#<a href="$1">$1</a>#g;

	# convert emailaddress into mailto anchor tags
	$c =~ s#([A-Z0-9._%+-]+@[A-Z0-9.-]+)#<a href="mailto:$1">$1</a>#ig;
	
	return $c;
}

=head3 Display

Display the email template.
Do not be confused, the actual email itself is not parsed in here, only the final wrapper.
The email content itself is parsed in ExtractParts()

=head4 Arguments

None

=head4 Returns

Returns true if Template was successful in rendering the template.
Othewise, it returns false and ->errstr() is set.

=cut

sub Display
{
	my $self = shift;
	_resetErrstr();
	
	my $tmplID = $self->displayTemplateID();
	return undef unless $tmplID;
	
	my $tmpl = $self->_loadTemplate($tmplID);	# a special version to select first by recipient language pref, then by user language pref, then defaulting to English
	my $mailing = $self->ExtractParts($tmpl);
	
	$tmpl = $self->{'gs'}->GetObject($TMPL{'wrapper'}) || die "Failed to load template wrapper";
	
	print $self->{'cgi'}->header('-charset'=>'utf8');

	my $data = {
		'r' => $self->r(),
		'script_name' => $self->{'cgi'}->script_name,
		'mailing' => $mailing,
		'lb' => $self->{'lb'},
		'links' => \%LINKS
	};

	my $rv = $TT->process(\$tmpl, $data);

	return $rv if $rv;
	
	$errstr = $TT->error;
	return $rv;
}

=head3 displayTemplateID

Figure out what template to render.
This method can be sub-classed.

=head4 Arguments

None

=head4 Returns

A template ID if successful, otherwise undef and ->errstr is set

=cut

sub displayTemplateID
{
	_resetErrstr();
	my $self = shift;
	my $rv = $self->getTmplID( $self->{'e'} );

	return $rv if $rv;
	if (defined $rv)	# rv equals -0-
	{
		$errstr = $self->{'lb'}->{'nodefault'};
	}
	else
	{
		$errstr = "Failed to identify a template for email ID: " . $self->{'e'};
	}
	
	return undef;
}

=head3 errstr

Returns any error string related to the last method call

=head4 Arguments

None

=cut

sub errstr
{
	return $errstr;
}

=head3 ExtractParts

Take the contents of a mailing template and create the parts that we will need/could need for displaying the mailing

=head4 Arguments

A template as a string

=head4 Returns

A hashref with these key/value pairs:

	raw		=>the content of the template argument, with all the placeholders interpolated
	body 	=>the raw content stripped of the subject line (first line) and any other blank lines directly beneath the subject (first line)
	subject	=>the content of the subject line (first line), stripped of the 'Subject' term, the following colon and the immediately following whitespace
	xbody	=>the 'body' escaped for use in a mailto link
	xsubject	=>the 'subject' escaped for use in a mailto link

=cut

sub ExtractParts
{
	my ($self, $tmpl) = @_;
	$tmpl =~ s/\r//g;
	my %rv = ('raw'=>''); 
	$TT->process(
		\$tmpl,
		{
			'r' => $self->r(),
			'u' => $self->u(),
			'rcoach' => sub { return $self->rcoach() },	# no point calling this in every case
			'lb' => $self->{'lb'},
			'links' => \%LINKS,
			'package' => $self
		},
		\$rv{'raw'}
	);
	
	# we are expecting the first line to be the subject -and- that the word "Subject" may be translated and will be followed by a colon and a space
	$rv{'raw'} =~ s/^(.*)\n+//;
	$rv{'subject'} = $1;
	$rv{'subject'} =~ s/^.+?:\s*//;
	$rv{'xsubject'} = $self->{'cgi'}->escape($rv{'subject'});
	$rv{'body'} = $rv{'raw'};
	$rv{'xbody'} = $self->{'cgi'}->escape($rv{'body'});
	
	$rv{'lines'} = () = $rv{'raw'} =~ m/\n/g;
	$rv{'lines_padded'} = int($rv{'lines'} * 1.4);
	
	$rv{'as_html'} = $self->convertToHtml($rv{'body'});
	
	return \%rv;
}

sub _interpolateLinks
{
	my $arg = shift;
	$arg = shift if ref $arg;	# this would happen if we are called as a method instead of as a function
	my $rv = ();
	$TT->process(
		\$arg,
		{
			'r' => $_self->r(),
			'u' => $_self->u()
		},
		\$rv
	);

	return $rv;
}

sub _resetErrstr
{
	$errstr = '';
}

sub rcoach
{
	return $rcoach if $rcoach;
	my ($self) = @_;

	# now get some basic information on our party
	$rcoach = $self->{'db'}->selectrow_hashref("
		SELECT m.alias, m.id, m.spid, m.firstname, m.lastname,
			m.firstname ||' '|| m.lastname AS fullname,
			m.emailaddress, m.membertype, m.language_pref
		FROM members m
		JOIN my_coach mc
			ON mc.upline=m.id
		WHERE mc.id= ?
	", undef, $self->r()->{'id'});

	return $rcoach;
}

=head3 u

A method to set/get the "user" of this session.

=head4 Arguments

If being used to set the user, then a numeric ID should be passed in.

=head4 Returns

If the user can successfully be identified, this method returns a hashref of particular details about the user -or- undef.

The current fields returned from this method are:
	alias
	id
	spid
	firstname
	lastname
	fullname (a concatenation of firstname, space, lastname)
	emailaddress
	membertype
	language_pref
	other_contact_info AS skype_name
	
=cut

sub u
{
	return $u if $u;
	my $self = shift;
	my $id = shift;
	
	if ($id)
	{
		# now get some basic information on our party
		$u = $self->{'db'}->selectrow_hashref("
			SELECT
				m.alias,
				m.id,
				m.spid,
				m.firstname,
				m.lastname,
				m.firstname ||' '|| m.lastname AS fullname,
				m.emailaddress,
				m.membertype,
				m.language_pref,
				m.other_contact_info AS skype_name
			FROM members m WHERE id= ?
		", undef, $id);
	}
	
	return $u;
}

sub _loadTemplate
{
	my ($self, $tmplID) = @_;
	my @rv = $self->{'gs'}->GetObject($tmplID, $self->r()->{'language_pref'});
	warn "Failed to load template: $tmplID" unless $rv[0];
	
	# if we did not get the recipients language preference, we probably got English, so we will try again with the users language preference
	if ($rv[1] ne $self->r()->{'language_pref'}){
		@rv = $self->{'gs'}->GetObject($tmplID, $self->u()->{'language_pref'});
	}
	
	return $rv[0];
}

1;
