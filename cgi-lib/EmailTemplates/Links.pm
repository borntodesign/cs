package EmailTemplates::Links;

=head1 EmailTemplates::Links

The class for generating icons and/or links to call the EmailTemplates.cgi resource.

=head3 new

Instantiate a new EmailTemplates::Links object

=head4 Arguments

A hashref argument can be passed in. The following key/values are recognized:

Required	'gs' => A G_S object that has been initialized with a DB handle
Optional	'recipientID' => integer value representing the ID of the email recipient
	 
=head4 Returns

A new EmailTemplates::Links object

=cut

use strict;
use CGI;
use parent 'EmailTemplates::Config';#require EmailTemplates::Config;

my $q = new CGI;
my $baseURL = 'http://www.clubshop.com/cgi/EmailTemplates.cgi/';

sub new
{
	my $class = shift;
	my $args = shift || {};
	$args->{'cfg'} = EmailTemplates::Config::initCfg($args->{'gs'});
	return bless $args, $class;
}

sub anchor
{
	my ($self, $acttype, $attrs, $icon) = @_;
	return unless $self->{'cfg'}->{$acttype};	# if we do not have that activity type defined in our configuration, we are done
	
	my %attrs = (
		'href' => $baseURL . $acttype . '/' . $self->setRecipientID()
	);
	$attrs{'onclick'} ||= 'window.open(this.href); return false;';
	
	foreach (qw/title class style/)
	{
		$attrs{$_} = $attrs->{$_} if $attrs->{$_};
	}
	return $q->a(\%attrs, $icon);
}

sub anchorIcon
{
	my ($self, $acttype, $attrs) = @_;
	return unless $self->{'cfg'}->{$acttype};	# if we do not have that activity type defined in our configuration, we are done
	
	$attrs->{'title'} ||= $attrs->{'alt'};	# if either one is empty, and the other one has something, set it's companion
	$attrs->{'alt'} ||= $attrs->{'title'};
	
	return $self->anchor($acttype, $attrs, $self->icon($attrs));
}

sub icon
{
	my ($self, $attrs) = @_;
	my %attrs = ();
	foreach (qw/src alt border height width class style/)
	{
		$attrs{$_} = $attrs->{$_} if $attrs->{$_};
	}
	$attrs{'alt'} ||= '';
	$attrs{'class'} ||= 'emtIcon';
	return $q->img(\%attrs);
}

sub anchorIconBlack
{
	my ($self, $acttype, $attrs) = @_;
	$attrs->{'src'} = "/images/icons/black-envelope-16x12.png";
	$attrs->{'width'} = 16;
	$attrs->{'height'} = 12;
	return $self->anchorIcon($acttype, $attrs);
}

sub anchorIconLight
{
	my ($self, $acttype, $attrs) = @_;
	$attrs->{'src'} = "/images/icons/envelope-16x12.png";
	$attrs->{'width'} = 16;
	$attrs->{'height'} = 12;
	return $self->anchorIcon($acttype, $attrs);
}

=head3 setRecipientID

When iterating over a list of recipient IDs we will not be instantiating this class with an ID. We will need to provide a new ID for each iteration.
-Or- we may just not have instantiated it with an ID because it was not available and subsequently becomes so.

=head4 Arguments

An integer ID for a recipient.
If the argument is not an integer, the value or abscence thereof will be ignored.

=head4 Returns

The new/current value of the recipientID

=cut

sub setRecipientID
{
	my ($self, $id) = @_;
	$self->{'recipientID'} = $id if $id and $id !~ m/\D/;
	return $self->{'recipientID'};
}

1;