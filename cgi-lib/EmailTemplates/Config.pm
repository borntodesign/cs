package EmailTemplates::Config;

=head1 EmailTemplates::Config

The configuration class for EmailTemplates related stuff

=head3 initCfg

Pulls in the JSON configuration template and decodes it into a hashref

Call it something like this:

$cfg = EmailTemplates::Config::initCfg({'gs'=>G_S object});

=head4 Arguments

A G_S object that has been instantiated with a valid DB handle

=head4 Returns

A hashref derived from the configuration template, currently 11080

As of 04/14/14 that looks like this where the keys are an activity type and the values are template IDs for the particular email

{
	"1":"11078",
	"2":"11081",
	"39":"11082"
}

As of 04/23/14... (I know that lasted long didn't it...)
We have to support the concept of further granularity with these keys
And now 05/14/15, membertype of the recipient
{
	"1":"11078",
	"2":"11081",
	"39":"11082",
	"49":{
		"default":"123",
		"sponsor":"456",
		"coach":"789",
		"exec":"45678"
	},
	"59":{
		"sponsor":"456",
		"coach":"789",
		"exec":"45678"
	},
	"999":{"mtypes":{
		"s":{"coach":"11112","sponsor":12345,"exec":"0"},
		"m":"11113",
		"tp":{"default":"11114"},
		"v":{"default":"11115"}
	}}
}

=cut

use strict;
use JSON;
my $cfg = ();

=head2 getTmplID

Based upon the email ID and the relationship of the "user" to the recipient, return a template ID.
This is meant to be called as an object method.

=head3 Arguments

An "email" ID as an integer.

=head3 Returns

If a template matches the email ID and either 'default' or one of the relationship keys, then that template ID will be returned.
If a particular email configuration does not have any keys specified, then the 'default' is considered the actual template for that email ID.
In other words, if the email ID were '1' like in the example above, 11078 would be returned in all cases.
For email ID 49, 123 would be returned and undef would be returned for email 59 unless one of the relationships was defined.
If a particular email ID is not defined, undef will be returned.
If a particular email ID is defined, but a matching relationship cannot be found and there is no default defined,
then -0- will be returned to differentiate between no email configuration and no matching relationship configuration.

Note: If the relationships have not been setup via setRelationships(), the only template IDs that are returned from here will be the 'default'... naturally

=cut

sub getTmplID
{
	my ($self, $e) = @_;
	return undef unless exists $cfg->{$e};
	
	# if our value is not a reference, we can return it directly as it should be a template ID
	return $cfg->{$e} unless ref $cfg->{$e};
	
	my $mycfg = ();
	if ($cfg->{$e}->{'mtypes'})
	{
		# if we do not have a configuration under the mtypes key for the recipients, then we are done
		return undef unless exists $cfg->{$e}->{'mtypes'}->{($self->{'recipient'}->{'membertype'} || '')};
		return $cfg->{$e}->{'mtypes'}->{ $self->{'recipient'}->{'membertype'} } unless ref $cfg->{$e}->{'mtypes'}->{ $self->{'recipient'}->{'membertype'} };
		$mycfg = $cfg->{$e}->{'mtypes'}->{ $self->{'recipient'}->{'membertype'} };
	}
	else
	{
		$mycfg = $cfg->{$e};
	}
	
	my $rv = ();
	
	if (! $self->{'relationships'})
	{
		warn "Apparently you did not perform a call to setRelationships()";
		$self->setRelationships();
	}

	foreach my $rel (qw/exec coach sponsor/)
	{
		$rv = $mycfg->{$rel} if $self->{'relationships'}->{$rel} && exists $mycfg->{$rel};
		last if $rv;
	}
	
	# if I came out of the loop without a return value, we'll take one last stab to see if there is a default setup amongst the relationships
	$rv ||= $mycfg->{'default'};# if (! $rv);# && exists $mycfg->{'default'};

	return $rv || 0;
}

=head2 initCfg

Meant to be called as a static method to load the configuration JSON and setup the static variable that will hold the hashref representation

=head3 Arguments

A G_S object that has been initialized with an active DB handle

=head Returns

The configuration as a hashref

=cut

sub initCfg($)
{
	if (! $cfg) {
		my $gs = shift;
		
		my $json = $gs->GetObject('11080') || die "Failed to load configuration template 11080";
		$cfg = decode_json($json);
	}
	return $cfg;
}

sub formatR
{
	my $self = shift;
	
	# for now we will stick with simply the email address, but we could do a firstname <emailaddress> or anything if we wanted, and/or we could subclass and change it up based upon the mailing
	return $self->{'recipient'}->{'emailaddress'};
}

=head3 r

A method to set/get the recipient of this session's email

=head4 Arguments

If being used to set the recipient, then a numeric member ID

=head4 Returns

If the recipient can successfully be identified, this method returns a hashref of particular details about the user.
Otherwise, it returns undef.

The current fields returned from this method are:
	alias
	id
	spid
	firstname
	lastname
	fullname (a concatenation of firstname, space, lastname)
	emailaddress
	membertype
	language_pref
	
=cut

sub r
{
	my ($self, $id) = @_;
	return $self->{'recipient'} if $self->{'recipient'};
	
	if ($id)
	{
		# now get some basic information on our party
		$self->{'recipient'} = $self->{'db'}->selectrow_hashref("
			SELECT
				m.alias,
				m.id,
				m.spid,
				m.firstname, m.lastname,
				m.firstname ||' '|| m.lastname AS fullname,
				m.emailaddress,
				m.membertype,
				m.language_pref,
				m.country,
				COALESCE(r.opp, 0) AS tpp,
				COALESCE(cnt.country_name, cc.country_name, '') AS country_name,
				COALESCE(date_fmt_by_country(odp.deadline, m.country)::TEXT,'') AS tp_deadline
			FROM members m
		/* unfortunately there is always a chance that they won't have their country set and so we can't risk a straight join that could render nothing */
			LEFT JOIN country_names_translated cnt
				ON cnt.country_code=m.country
				AND cnt.\"language\" = m.language_pref
			LEFT JOIN tbl_countrycodes cc
				ON cc.country_code=m.country
			LEFT JOIN refcomp r
				ON r.id=m.id
			LEFT JOIN od.pool odp
				ON odp.id=m.id
			WHERE m.id= ?
		", undef, $id);
	
		$self->{'recipient'}->{'formatted'} = $self->formatR();
	}
	
	return $self->{'recipient'};
}

=head2 setRelationships

In order to select a template based upon certain user/recipient relationships, we need to know these relationships.
This method provides a common interface for injecting that data.
This method is intended to called as an object method.

=head3 Arguments

A relationship hashref representation that has these keys, with the setting of the keys indicating that the relationship exists.

{
	'sponsor'=>t/f
	'coach'=>t/f
	'exec'=>t/f
}

=head3 Returns

undef

=cut

sub setRelationships
{
	my ($self, $rh) = @_;
	$self->{'relationships'} = $rh || {};	# if we are called with a null value, we will create an empty hashref
	return ();
}

=head2 new

Basically an instantiator that is never intended for use...

=cut

sub new
{
	return bless {};
}

1;