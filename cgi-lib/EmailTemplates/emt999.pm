package EmailTemplates::emt999;

use parent 'EmailTemplates::Master';
use strict;

sub minClubCashRedemption
{
	my $self = shift;
	return $self->{'db'}->selectrow_array('
		SELECT format_currency_by_locale(exchange_usd_for_cbc(val_num1,?),?,?)
		FROM configurations.general WHERE pk=1
	', undef, $self->r->{'country'}, $self->r->{'country'}, $self->r->{'language_pref'});
}

1;