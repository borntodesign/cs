package PartnerSubscriptionAccess;
use strict;
#use Data::Dumper;
use Data::Serializer;
use lib ('/home/httpd/cgi-lib');
use G_S;

my $CookieName = 'PartnerSubscription';

=head1 PartnerSubscriptionAccess

	A class for managing the PartnerSubscription cookie and access to the various resources regulated by the level of the subscription.

=head2 new()

	A standard constructor.

=head3 Arguments

	A CGI object
	A hashref of optional arguments:
		A G_S object on the 'G_S' key.
		
		A DB handle on the 'db' key. Alternatively, you can provide a reference to a local function that provides a cached DB handle.
		Of course, if you need to create a cookie, you'd better supply a DB handle or we will die.
		
=head3 Returns

	A blessed PartnerSubscriptionAccess object.

=cut

sub new
{
	my ($class, $cgi, $args) = @_;
	my $SERIALIZER = Data::Serializer->new(
		'serializer'		=> 'Storable',
		'digester'   		=> 'MD5'
		#'cipher'     		=> 'Blowfish',
		#'secret'     		=> 'eojh574#@!',
		,'serializer_token'	=> 0
	);
	
	my $rv = {
		'cgi'=>$cgi,
		'SERIALIZER'=>$SERIALIZER,
		'cache'=>{}
	};
	
	foreach (qw/G_S/)
	{
		$rv->{$_} = $args->{$_};
	}
	
	if ($args->{'db'})	# we need to initialize this using our method in case we have received a code reference
	{
		db($rv, $args->{'db'});
	}
	
	return bless $rv;
}

sub cgi
{
	return $_[0]->{'cgi'};
}

=head2 createCookieFromValue

	Primarily intended as an internal method call to create the actual CGI cookie object. It will also cache the object.

=head3 Arguments

	The actual cookie value as a text string.

=head3 Returns

	A CGI cookie object.
	
=cut

sub createCookieFromValue
{
	my ($self, $val) = @_;
	return $self->Cache('cookie.object', $self->cgi->cookie('-name'=>$CookieName, '-value'=>$val, '-expires'=>'+3h', '-path'=>'/'));
}

=head2 db

	Primarily intended as an internal method call to get the DB handle using $self->db instead of $self->{'db'}... lazy I know
	However, we can also accept an argument to set the DB handle subsequent to the call to new().
	This could be useful if we want to initialize the object for certain uses only to find out that we need a DB handle later on to use certain methods where it is required.

=head3 Arguments

	An optional DB handle if we need to set the DB handle for the class after the call to new().
	This argument could also be a reference to a subroutine which will itself return a DB handle.

=head3 Returns

	A DB handle if one is available or undef if not.
	
=cut

sub db
{
	my ($self, $db) = @_;
	if ($db)
	{
		$self->{'db'} = (ref $db eq 'CODE') ? &$db : $db; # a regular DBI handle will be a blessed reference
	}
	return $self->{'db'};
}

sub G_S
{
	my ($self) = @_;
	$self->{'G_S'} ||= new G_S;
	return $self->{'G_S'};
	
}

sub Cache
{
	my ($self, $item, $value) = @_;
	$self->{'cache'}->{$item} = $value if defined $value;
	return $self->{'cache'}->{$item};
}

###### the methods below are meant for general public consumption

=head2 ACL

	For now we will just pull a particular resource from the DB.
	However, we may at some point decide to extract the DB data on a routine basis to a complex hash to be imported by a use or require.

=head3 Arguments

	A numeric resource ID
	
=head3

	A hashref of the resource if available. undef otherwise.
	
=cut

sub ACL
{
	my ($self, $resource) = @_;
	return undef unless $resource;
	die "resource ID must be numeric\n" if $resource =~ m/\D/;
	
	my $rv = $self->Cache("resource.$resource");
	unless ($rv)
	{
		$rv = $self->db->selectrow_hashref("SELECT * FROM subscription_pkg_acl WHERE resource_id= $resource");
		$rv = $self->Cache("resource.$resource", $rv);
	}
	return $rv;
}

=head2 Authorized

	Determine if our party is authorized for a particular resource
	
=head3 Arguments

	A numeric resource ID
	
=head3 Returns

	1 (true) -or- 0 (false)
	
=cut

sub Authorized
{
	my ($self, $resource) = @_;
	
	# if a resource ID is not specified, then they are good to go :-)))
	return 1 unless $resource;
	
	# if the specified resource ID does not exist, then they are good to go
	my $acl = $self->ACL($resource) || return 1;
	
	# now we really need to check on their subscription level
	my $ckval = $self->GetRawCookieValue();
	unless ($ckval)
	{
		$self->CreateCookieFromLoginCookie();
		$ckval = $self->GetCookedCookieValue();
	}
	
	my $s = $self->CookieValueToHashref($ckval);
	
	# if we ended up with nothing, then create a dummy record just for doing our comparison
	$s ||= {'weight'=>0};

	return ($s->{'weight'} >= $acl->{'required_weight'}) ? 1:0;
}

=head2 CGI_AuthorizeOrRedirect

	A method to call from a CGI script that will see if the visitor is authorized for the particular resource and if not perform the redirect.
	
=head3 Arguments

	An integer representing the resource ID
	
=head3 Returns

	A CGI cookie object if authorized. This can be stuffed into the header by the caller if desired.
	undef if not authorized. Since a redirect would have been issued, it is expected that the caller will exit upon a false return value.

=cut

sub CGI_AuthorizeOrRedirect
{
	my ($self, $resourceID) = @_;
	
	my $authorized = $self->Authorized($resourceID);
	my $ck = $self->GetRawCookieValue();
	
	# they could be authorized by virtue of the fact that we looked them up and determined they were, even if they did not already have the cookie
	# in that case we should have created one which we can set when we send the data back
	if ($authorized && ! $ck)
	{
		$ck = $self->GetCookedCookieObject();
	}
	# if they already had a cookie, we will refresh it
	else
	{
		$ck = $self->createCookieFromValue($ck);
	}
	
	if ($authorized)
	{
		return $ck;
	}
	
	print $self->cgi->redirect("https://www.clubshop.com/cgi/PSAx?resourceID=$resourceID");
	return undef;
}

=head2 CookieValueToHashref

	Convert a textual value from our cookie to it's original hash reference
	
=head3 Arguments

	The textual value of our cookie
	
=head3 Returns

	A hashref that should be identical to what we originally stuffed into the cookie
	
=cut

sub CookieValueToHashref
{
	my ($self, $raw) = @_;
	return undef unless $raw;
	return $self->Cache('cookie.deserialized') || $self->Cache('cookie.deserialized', $self->{'SERIALIZER'}->deserialize($raw));
}

=head2 CreateCookieFromMemberID

	The method to be called when a PartnerSubscription cookie is desired and we have to work from a member ID.

=head3 Arguments

	A numeric member ID

=head3 Returns

	A CGI "cookie" ready to stuff into a header.

=cut

sub CreateCookieFromMemberID
{
	my ($self, $member_id) = @_;
	
	# no use going through all the subsequent motions if we already created a value
	my $rv = $self->Cache('cookie.object');
	return $rv if $rv;
	
	$rv = $self->CreateCookieValue($member_id);
	return $self->CreateExpiredCookie() unless $rv;
	return $self->createCookieFromValue($rv);
}

=head2 CreateCookieFromLoginCookie

	The method to be called when a PartnerSubscription cookie is desired and we have to work from a login cookie.

=head3 Arguments

	None

=head3 Returns

	A CGI "cookie" ready to stuff into a header.
	As long as we have a valid AuthCustom_Generic cookie available, the cookie will be a "good" one.
	If we do not have a valid AuthCustom_Generic cookie, then the cookie returned will actually be one that tells the browser to invalidate it... ie. we set the expired date in the past

=cut

sub CreateCookieFromLoginCookie
{
	my ($self) = @_;
	
	# no use going through all the subsequent motions if we already created a value
	my $rv = $self->Cache('cookie.object');
	return $rv if $rv;
	
	my ( $member_id, $mem_info ) = $self->G_S->authen_ses_key($self->cgi->cookie('AuthCustom_Generic'));
	
	# a valid login will will return a numeric ID
	unless ($member_id && $member_id !~ m/\D/ && $mem_info->{'membertype'} eq 'v')
	{
		return $self->CreateExpiredCookie();
	}

	return $self->CreateCookieFromMemberID($member_id);
}

=head2 CreateCookieValue

	The method to be called when you just want the value for a PartnerSubscription cookie.
	This should not be confused with extracting the value from the cookie which will in fact get you a hashref.
	See ExtractCookieValue() for that.

	If the cookie value is not already cached, this method will look the subscription up in the DB, create the value, cache it and return it.
	
=head3 Arguments

	A member ID integer.
	
=head Returns

	A string which can be used as the value of the PartnerSubscription cookie.
	
=cut

sub CreateCookieValue
{
	my ($self, $id) = @_;
	die "The ID argument must be an integer" if $id =~ m/\D/;
	
	my $rv = $self->Cache("cookie.value");
	return $rv if $rv;

	my $data = $self->db->selectrow_hashref("
			SELECT
				s.member_id,
				s.subscription_type,
				st.label,
				st.weight
			FROM partner_subscription_monthly s
			JOIN subscription_types st
				ON st.subscription_type=s.subscription_type
			WHERE s.member_id= $id
		");
	return undef unless $data;
	return $self->Cache("cookie.value", $self->{'SERIALIZER'}->serialize($data));
}

=head2 CreateExpiredCookie

	There are cases where we need to expire the cookie.
	Calling this function returns a cookie.object that when sent to the browser will cause it to delete the cookie.
	
=cut

sub CreateExpiredCookie
{
	my $self = shift;
	return $self->Cache('cookie.object', $self->cgi->cookie('-name'=>$CookieName, '-value'=>'', '-expires'=>'-3h', '-path'=>'/'));
}

=head2 ExtractCookieValue

	Get the deserialized variable from the PartnerSubscription cookie.
	
=head3 Arguments

	None
	
=head3 Returns

	A hashref of values as long as there is a valid cookie available.
	Otherwise undef.

=cut

sub ExtractCookieValue
{
	my ($self) = @_;
	return $self->CookieValueToHashref( $self->GetRawCookieValue() );
}

=head2 GetCookedCookieObject

	Simply call this method with no arguments to get the cookie.object that is cached.
	
=cut

sub GetCookedCookieObject
{
	return $_[0]->Cache('cookie.object');
}

=head3 GetCookedCookieValue

	Call this to see if one of our CreateCookieXXX methods were successful.

=head3 Arguments

	None

=head3 Returns

	The textual value that has been in placed in the cookie object or undef if empty
	
=cut

sub GetCookedCookieValue
{
	my $self = shift;
	return $self->Cache("cookie.value");
}

=head2 GetRawCookieValue

	Get the textual value from our PartnerSubscription cookie.
	
=head3 Arguments

	None

=head3 Returns

	The textual value of the cookie
	
=cut

sub GetRawCookieValue
{
	my ($self) = @_;
	return $self->cgi->cookie($CookieName);
}

1;