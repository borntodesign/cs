package Vendors;

=head1 NAME:
Vendors

	This routine manages information pertaining to the Vendors, 
	specificaly information we keep in the Vendor table

=head1
VERSION:

	0.2

=head1
SYNOPSIS:

=head2
How To Use This Class:

	Verbose Description of the class, including how to use it without code examples.

=head2
Code Example

	eval{
		my $Vendors = Vendors->new($db);
		if( ! $Vendors->createVendorInformation(\%vendor_information_hashref))
		{
			#Nothing was done.
		}
	};
	if($@)
	{
		# How ever you want to handle the fatal error.
	}

=head1
PREREQUISITS:

	DHSGlobals

=cut

use strict;
use DHSGlobals;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	db	OBJ	The database handler, as returned from DBI::connect().

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $this			= {
    						name=>'Vendors',
    						version=>0.2
    };
    
    $this->{db} = shift;
    
    $this->{vendors_cache} = {};
    
    bless($this, $class);
    
	return $this;

}


=head2
METHODS

=head2
PRIVATE

=head3 __updateCache

=head4
Description:

	Get the Vendor record Vendor ID,
	and assign the data to the cache

=head4
Params:

	hashref
		id		string	The Vendor ID

=head4
Returns:

	string	on error
=cut

sub __updateCache
{
	my $this = shift;
	my $id = shift;

	die "The 'id' parameter is required, and must be an integer." if (! $id || $id !~ /^\d+$/);


	eval
	{
		my $sth = $this->{db}->prepare("SELECT * FROM vendors WHERE vendor_id = ?");

		$sth->execute($id);

		my $rows = $sth->fetchrow_hashref;

		$this->{vendors_cache}->{$rows->{vendor_id}} = $rows;


	};
	
	die "Vendors::__upcateCache - The cache was not updated due to a database error. <br />Query: SELECT * FROM vendors WHERE vendor_id = ? <br />vendor_id: $id <br />" . $@ if ($@);

	return;
	
}

=head2
PUBLIC


=head3 createVendorInformation

=head4
Description:

	This inserts the Vendor information.

=head4
Params:

	hashref
		should be a hashref where the keys
		are named the same as the fields in
		the vendor table.  If
		an 'id' index exists an exception 
		will be thrown.

=head4
Return:

	returns	int Primary Key for the new vendor record on sucess, 0 on failure.  An exception is thrown if there is an issue with inserting the data.

=cut

sub createVendorInformation
{

	my $this = shift;
	my $params = shift;

	die 'The ID field is not valid, this method is for creating the vendor Record, not updateing it.' if (exists $params->{vendor_id} && $params->{vendor_id});	

	my $insert_query = 'INSERT INTO vendors ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	
	eval{
		my @primary_key_from_vendors_table = $this->{db}->selectrow_array("SELECT NEXTVAL('vendor_id_seq')");
		
		die 'We were not able to generate the primary key. ' if ! $primary_key_from_vendors_table[0];
		
		$params->{vendor_id} = $primary_key_from_vendors_table[0];
		
		foreach my $key (keys %$params)
		{
			next if (! $params->{$key});
			
			push @fields, $key;
			push @place_holders, '?';
			push @values, $params->{$key};
			
		}
		
		$insert_query .= '(' . join(',', @fields) . ') VALUES(' . join(',', @place_holders) . ') ';
		use Data::Dumper;
#warn "Vendors.pm: " . Dumper(@values) . Dumper(@fields);
		
		$return_value = $this->{db}->do($insert_query, undef, @values) ? $primary_key_from_vendors_table[0]: 0;
		
	};
	if($@)
	{
		die 'Error: Vendors::createVendorInformation - ' . $@;
	}
	
	return $return_value;
	
}

=head3 updateVendorInformation

=head4
Description:

	This updates the Vendor information.

=head4
Params:

	id	int	The vendor_id as it is in the vendors table.
	
	params	hashref
					should be a hashref where the keys
					are named the same as the fields in
					the vendor table. Only the fields specified
					will be updated.  If the key 'vendor_id'
					is included and exception is thrown, the 'vendor_id'
					should be the first parameter passed when
					calling this method.

=head4
Return:

	returns	int The number of records effected on sucess, 0 on failure.  An exception is thrown if there is an issue with updateing the data.

=cut

sub updateVendorInformation
{

	my $this = shift;
	my $id = shift;
	my $params = shift;

	die 'The ID filed is not valid, it must be an integer.' if $id !~ /^\d+$/;
	die 'The ID field is not valid, this method is for updateing the vendor Record' if (exists $params->{vendor_id});	

	my $update_query = 'UPDATE vendors SET ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	

		
		foreach (keys %$params)
		{
			next if (! exists $params->{$_});
			
			push @fields, "$_ = ?";
			push @values, $params->{$_};
			
		}
		
		push @values, $id;
		
		$update_query .= ' ' . join(',', @fields) . ' WHERE vendor_id = ? ';
		
	eval{
		
		$return_value = $this->{db}->do($update_query, undef, @values);
		
		delete $this->{vendors_cache}->{$id} if exists $this->{vendors_cache}->{$id};
	};
	if($@)
	{
		die 'Error: Vendors::updateVendorInformation - ' . $@ . "\n$update_query \n @values \n";
	}
	
	return $return_value;
	
}


=head3 updateMerchantVendorInformationByMerchantLocationId

=head4
Description:

	This updates the Vendor information for a merchant based on a Location ID.  
	Pass in the Location ID, and all the Vendor Information associated with the Merchants Master ID is Updated.

=head4
Params:

	id	int	The vendor_id as it is in the vendors table.
	
	params	hashref
					should be a hashref where the keys
					are named the same as the fields in
					the vendor table. Only the fields specified
					will be updated.  If the key 'vendor_id'
					is included and exception is thrown, the 'vendor_id'
					should be the first parameter passed when
					calling this method.

=head4
Return:

	returns	int The number of records effected on sucess, 0 on failure.  An exception is thrown if there is an issue with updateing the data.


TODO: This method should use the "updateVendorInformation" method to update the information instead of a query.  This method should also be located in the "MerchantAffilates" class.

=cut

sub updateMerchantVendorInformationByMerchantLocationId
{

	my $this = shift;
	my $id = shift;
	my $params = shift;

	die 'The ID filed is not valid, it must be an integer.' if $id !~ /^\d+$/;
	die 'The ID field is not valid, this method is for updateing the vendor Record' if (exists $params->{vendor_id});	

	my $update_query = 'UPDATE vendors SET ';
	
	my @values = ();
	my @fields = ();
	my @place_holders = ();
	my $return_value = 0;
	
	

		
		foreach (keys %$params)
		{
			next if (! exists $params->{$_});
			
			push @fields, "$_ = ?";
			push @values, $params->{$_} ? $params->{$_}: undef;
			
		}
		
		push @values, $id;
		
		$update_query .= ' ' . join(',', @fields) . ' WHERE vendor_id IN (SELECT DISTINCT(vendor_id) FROM merchant_affiliate_locations WHERE merchant_id =(SELECT merchant_id FROM merchant_affiliate_locations WHERE id = ?)) ';
		
		delete $this->{vendors_cache}->{$id} if exists $this->{vendors_cache}->{$id};
		
	eval{
		
		$return_value = $this->{db}->do($update_query, undef, @values);
	};
	if($@)
	{
		
		die 'Error: Vendors::updateVendorInformation - ' . $@ . "\n$update_query \n @values \n";
	}
	
	
	
	return $return_value;
	
}


=head3 getVendorInformation

=head4
Params:

	id	INT	The Vendor ID, as defined in the Vendors table.

=head4
Returns:

	hashref on failure return null

=cut

sub getVendorInformation
{
	my $this = shift;
	my $id = shift;
	
	die 'The ID filed is not valid, it must be an integer.' if $id !~ /^\d+$/;
	
	my $fields = '';
	my @values = ();
	my $return_arrayref = ();
	
	return $this->{vendors_cache}->{$id} if exists $this->{vendors_cache}->{$id};
	
	$this->__updateCache($id);
	
	return exists $this->{vendors_cache}->{$id} ? $this->{vendors_cache}->{$id}: undef;
	
}



1;

__END__

=head1
SEE ALSO

L<DHSGlobals>

=head1
CHANGE LOG

	Date	What was changed

=cut

