package RewardCards;

=head1 NAME:
RewardCards

	General descripton of the Class.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	Verbose Description of the class, including how to use it without code examples.

=head2
Code Example

	Code Example Goes Here

=head1
PREREQUISITS:

	Other classes, or packages that are used

=cut

use strict;
use Storable;
use DHSGlobals;
use MIME::Base64::URLSafe;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	$attribute_name	string	What this attributes purpose is.

=head4
Returns:

	obj $self

=cut

sub new
{
    my $class			= shift;
    my $self			= {};
    $self->{db} = shift;
    
    die 'A database handler is required, but it was not supplied.' if ! $self->{db};
    
    bless($self, $class);
    $self->_init();
    return $self;
    
}

=head2
METHODS

=head2
PRIVATE

=head3
privateMethodName

=head4
Description:

	Description of what is set in __init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $self = shift;

    $self->{'version'}	= '0.1';
    $self->{'name'}		= 'RewardCards';
    $self->{'reward_cards_by_location'} = undef;
    $self->{'minimum_number_of_reward_cards_to_cache'} = 50;
    

}


=head2
PUBLIC

=head3
createRewardCardsCache

=head4
Description:

	Describe what this method does

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	BOOLEAN	True on success.


=head4
NOTE:

	This method calls a fairly expensive query (the cost is currently: 112,919),
	and should increase as more reward cards are purchased.

=cut

sub createRewardCardsCache
{


	my $self = shift;
	
	#TODO: The case statement in the query shouldn't be needed once we clean up the data, There are a lot more than just Curacao with this issue.
	
	my $query =<<EOT;

			SELECT
				tc.country,
				tc.state,
				SUM(tc.total) AS total
			FROM
			(
					SELECT
						upper(m.country) AS country,
						upper(m.state) AS state,
						COUNT(m.id) AS total
					FROM
						members m
						JOIN
						clubbucks_master AS cm
						ON
						COALESCE(cm.current_member_id, cm.referral_id ) = m.id
					WHERE
						cm.id < 500000000
						AND
						m.country IS NOT NULL
						AND
						m.state IS NOT NULL
						AND
						m.membertype IN ('m','s','v','ma', 'ag')
					GROUP BY
						m.country,
						m.state
				UNION
					SELECT
						upper(afm.country) AS country,
						upper(afm.state) AS state,
						COUNT(afm.id) * 500 AS total -- Was 50
					FROM
						members afm
					WHERE
						afm.country IS NOT NULL
						AND
						afm.state IS NOT NULL
						AND
						afm.membertype = 'ag'
					GROUP BY
						afm.country,
						afm.state
		) tc
		
		GROUP BY
			tc.country,
			tc.state

EOT


#TODO: Make a cache class. to manage the cache.
	my $insert_query =<<EOT;

					UPDATE
					data_cache
					SET
						data = ?
					WHERE
						id = 1

EOT

	my %card_counts = ();
	my $results = undef;
	
	#Lots, and lots of code
	
	eval{
		
		my $sth = $self->{db}->prepare($query);
		
		$sth->execute();
		
		$self->{'reward_cards_by_location'} = {};
		
		while(my $rows = $sth->fetchrow_hashref)
		{
			
			next if($rows->{'total'} <= $self->{'minimum_number_of_reward_cards_to_cache'} || ! $rows->{'state'} || ! $rows->{'country'});
			
			if (! $self->{'reward_cards_by_location'}->{$rows->{'country'}}->{$rows->{'state'}})
			{
				$self->{'reward_cards_by_location'}->{$rows->{'country'}}->{$rows->{'state'}} = $rows->{'total'};
			}
			else
			{
				$self->{'reward_cards_by_location'}->{$rows->{'country'}}->{$rows->{'state'}} += $rows->{'total'};
			}
			

		}
		
		my $serialized_data = MIME::Base64::URLSafe::encode(Storable::freeze($self->{'reward_cards_by_location'}));
		
		$results = $self->{'db'}->do($insert_query, undef, ($serialized_data))
		
	};
	if($@)
	{
		die "RewardCards::_retrieveRewardCardTotals - There was an error retrieving the card counts, serializing the data, or inserting the serialized data into the data_cache table. Error: $@";
	}
	
	#TODO: Check $results, and if it is false I should send an email.
	return $results;

}

=head3
retrieveRewardsCardCache

=head4
Description:

	Describe what this method does

=head4
Parameters:

	force	BOOL	If this is TRUE the cache will be rebuilt.

=head4
Return:

	BOOLEAN	True on success.

=cut

sub retrieveRewardsCardCache
{
	my $self = shift;
	my $force = shift;
	
	return 1 if (ref($self->{'reward_cards_by_location'}) eq 'HASH' && ! $force);
	
	return 1 if($force && $self->createRewardCardsCache() && ref($self->{'reward_cards_by_location'}) eq 'HASH');
	
	#TODO: Check to see if the file exists, and is accessible by the current process owner, if it doesn't exist call $self->createRewardCardsCache;
	

	my $serialized_data = $self->{'db'}->selectrow_hashref('SELECT data FROM data_cache WHERE id = 1');

	$self->{'reward_cards_by_location'} = Storable::thaw(MIME::Base64::URLSafe::decode($serialized_data->{data})) ;
	
	return 1 if (ref($self->{'reward_cards_by_location'}) eq 'HASH');
	
}


=head3
getRewardsCardCache

=head4
Description:

	Describe what this method does

=head4
Parameters:

	force	BOOL	If this is TRUE the cache will be rebuilt.

=head4
Return:

	HASHREF	An anonymous hashref 
								{
									COUNTRY_CODE => {
														PROVINCE => NUMBER_OF_CARDS_IN_THE_LOCATION,
													},
								}

=cut

sub getRewardsCardCache
{
	my $self = shift;
	my $force = shift;
	
	return $self->{'reward_cards_by_location'} if (ref($self->{'reward_cards_by_location'}) eq 'HASH' && !$force);
	
	return $self->{'reward_cards_by_location'} if $self->retrieveRewardsCardCache($force);
	
}

=head3
getRewardsCardInAProvince

=head4
Description:

	Describe what this method does

=head4
Parameters:

	country_code	STRING	The ISO Country Code.
	state_code		STRING	The ISO Province Code, or the Provice Name if that's how it was stored.
	force			BOOLEAN	If you want to rebuild the cache, this is presently a query of 80,000

=head4
Return:

	HASHREF	An anonymous hashref 
								{
									COUNTRY_CODE => {
														PROVINCE => NUMBER_OF_CARDS_IN_THE_LOCATION,
													},
								}

=cut

sub getRewardsCardInAProvince
{
	my $self = shift;
	my $country_code = shift;
	my $state_code = shift;
	my $force = shift;
	
	$country_code = uc($country_code);
	$state_code = uc($state_code);
	
	if (ref($self->{'reward_cards_by_location'}) eq 'HASH' && ! $force)
	{
		return $self->{'reward_cards_by_location'}->{$country_code}->{$state_code} ? $self->{'reward_cards_by_location'}->{$country_code}->{$state_code}:0;	
	}
	
	if ($self->retrieveRewardsCardCache($force))
	{
			return $self->{'reward_cards_by_location'}->{$country_code}->{$state_code} ? $self->{'reward_cards_by_location'}->{$country_code}->{$state_code} :0;		
	}

	
	return 0;
	
	
}

1;

__END__

=head1
SEE ALSO

L<DHSGlobals>, Storable, MIME::Base64::URLSafe

=head1
CHANGE LOG

	JUN 22 2010	Keith	The query in the createRewardCardsCache method was modified
						so we add 50 cards per Affinity group for an area.


=head1
TODO

	Look into the Cache::File module, we may want to use
	that to manage the cache at some time.

=cut

