package Members;
use strict;
##########################################################
#
# This class is ment to manage information pertaining
# to members, such as name, password, address, email
# Skype name, etc.
#
##########################################################

#############################################
#
# Params db a reference to a database connection
# returns obj $this
#############################################
sub new
{
    my $invocant = shift;
    my $class = ref($invocant) || $invocant;
    my $this = {
    			name => 'members',
    			version => 0.1,
    			member_info_by_id => {}
    };

    $this->{db} = shift;

    bless($this, $class);
    return $this;
}

#############################################
# Get the users information by the "Member ID"
# or "Alias" and assign the data to the cache
# Params hash
#		 member_id bool
#		 id string MemberID
# returns string
#############################################
sub __updateCache
{
	my $this = shift;
	my $args = shift;
	my $field = $args->{member_id}?'id':'alias';


	return if !defined $args->{id};

	eval
	{
		my $sth = $this->{db}->prepare("SELECT * FROM members WHERE $field = ?");
		$sth->execute($args->{id});
		my $rows = $sth->fetchrow_hashref;

		$this->{member_info_by_id}->{$rows->{id}} = $rows;


	};
	if ($@)
	{
		return $@ . ' The cache was not updated due to a database error';
	}

	return;
}

#############################################
# Get the users information by the Member ID
# and assign the data to the cache
# Params hash
#		 member_id int MemberID
#		 field string Field Name
#		 force bool
# returns string
#############################################
sub __getMemberInformationByMemberID
{
	my $this = shift;
	my $args = shift;

	$args->{force} = 0 if !defined $args->{force};

	return if !defined $args->{member_id};



	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	if ($this->{member_info_by_id}->{$args->{member_id}} && !$args->{force})
	{
		return $this->{member_info_by_id}->{$args->{member_id}}->{$args->{field}};
	}



	# If the member cache does not exist, or the data needs to be refreshed
	if(!$this->__updateCache({member_id=>1, id=>$args->{member_id}}))
	{
		return $this->{member_info_by_id}->{$args->{member_id}}->{$args->{field}};
	} else {
		return;
	}


}



#############################################
# Get the users Skype name by their Member ID
# Params int $member_id MemberID
# returns string skype name
#############################################
sub getSkypeNameByMemberID
{
	my $this = shift;
	my $member_id = shift;

	return $this->__getMemberInformationByMemberID({member_id=>$member_id, field=>'other_contact_info', force=>0});

}







1;