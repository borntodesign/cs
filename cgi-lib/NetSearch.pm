package NetSearch;
######
###### Written by:	Karl Kohrt
######
###### Search a member's($id) downline network, then report back the data found based upon
###### $criteria_args, $filter_args, and $output_args that were passed into the script. 
######
###### last modified: 08/09/10 Bill MacArthur
######
###### $id = A numeric DHS ID; the top of the network tree to be searched.
###### $OUTPUT_ARGS - A reference to a list(array) of columns that we want output; must be from
######				 the members table or tables included in @TABLE_ARGS.
###### $FILTER_ARGS - A reference to a list(array) of hashes holding the values that we want to 
######				filter out and how we want them filtered.
###### 			Hashes are of the format:
######						{	column to filter on,
######						 	value to be filtered, 
######							exclude an individual 
######							or an entire downline group,
######							comparison operator }
######				Examples:
###### 					{	'column'	=> 'a_level',
###### 						'value'	=> 11,
###### 						'exclude'	=> 'group',
###### 						'operator'	=> '>='},
###### 					{	'column'	=> 'membertype',
###### 						'value'	=> 'ag',	
###### 						'exclude'	=> 'individual',
###### 						'operator'	=> 'eq'}
###### $TABLE_ARGS - A reference to a list(array) of tables to join to the members table.
######			To include a table in this list, a "join definition" must be defined below
######			in the %JOINS hash, and if the table to be added needs to be joined across
######			multiple tables to reach the members table, code may need to be added below
######			to avoid duplicate joins on intermediate tables.			

use strict;
use Carp qw/croak/;
#use DBI;

# All arguments except $db are optional so that we can customize the error messages below.
# It really requires the first 3 arguments to work.
sub Network_Search($;$$$$)
{	
	our ( $db, $id, $OUTPUT_ARGS, $FILTER_ARGS, $TABLE_ARGS ) = @_;

	###### if we receive a table which looks like a substitute for the members table
	###### we will use it instead of the default members table
	my ($Def_Member_Tbl) = grep /^members/, @{$TABLE_ARGS};
	$Def_Member_Tbl ||= 'members';

	# Define those fields that hard-coded into our SELECT query - Not necessarily output fields.
	our @DEFAULT_SELECT_FIELDS = (
		"$Def_Member_Tbl.id",
		"$Def_Member_Tbl.spid",
		"membertype");

	# The columns we need to coalesce and the coalesce values.
	our %COALESCE = (
		'emailaddress'	=> '',
		'emailaddress2'	=> '',
		'phone'		=> 'no phone',
		'a_level_stable.a_level'	=> 0,
		'refcomp.npp'	=> 0,
		'refcomp.ppp'	=> 0,
		'company_name'	=> '',
		'rp_summary_vw.balance' => 0,
		'cellphone'		=> '',
		'other_contact_info'	=> '',
		'partner_mgt_access.pool_mgt' => 'TRUE',
		'od.pool.id' => 0
	);
	my $REPORT = '';
	my %alias = ();
	my %depth = ();
	my $dat = '';
	my $item = '';
	my $depths= 0;
	my $depth = 1;
	my (%a, %b);
	my (@a, @b);
	
	# Initial error handling here.
	if ( $id =~ /\D/ )
	{
		croak("A numeric DHS ID must be passed as into Network_Search as the first argument. You passed '$id'.");
	}
	unless ( $OUTPUT_ARGS )
	{
		croak("At least one Output field must be passed into Network_Search using a list(array) as the second argument. id: $id");
	}

	# The potential table join definitions for the query.
	our %JOINS = (
		'a_level_stable'=> " LEFT JOIN a_level_stable ON $Def_Member_Tbl.id = a_level_stable.id ",
		'level_values'	=> " LEFT JOIN a_level_stable ON $Def_Member_Tbl.id = a_level.id 
					     LEFT JOIN level_values 
					     ON a_level_stable.a_level = level_values.lvalue ",
		'refcomp'	=> " LEFT JOIN refcomp ON $Def_Member_Tbl.id = refcomp.id ",
		'rp_summary_vw'	=> " LEFT JOIN rp_summary_vw ON $Def_Member_Tbl.id = rp_summary_vw.id",
		'od.pool_vw' => "LEFT JOIN od.pool_vw ON $Def_Member_Tbl.id = od.pool_vw.id",
		'od.pool' => "LEFT JOIN od.pool ON $Def_Member_Tbl.id = od.pool.id"
	);

	# We need to remove any table references that will cause duplicate joins.
	my $element = 0;
	foreach $item ( @$TABLE_ARGS )
	{
		my $found = 0;
		# If we have included this table to be joined...
		if ( $item eq 'a_level' )
		{
			foreach my $table ( @$TABLE_ARGS )
			{
				# ...we must make sure that this table is not also included.
				# If it is included, remove the above table from the list.
				if ( $table eq 'level_values' ) { $found = 1 }
			}
		}
		# If we found one of the above conditions, blank out the current table.
		if ( $found ) { @$TABLE_ARGS[$element] = '' }
		$element++;
	}

	# Produce the basic network query with the default select fields.
	my $QUERY = "SELECT ";
	foreach ( @DEFAULT_SELECT_FIELDS )
	{
		$QUERY .= "$_, ";
	}
	chop $QUERY;
	chop $QUERY;	# Remove the trailing space and comma.

	# Add SELECT fields required for later filter comparisons
	#  that are not already in our output args or default select fields lists.
	foreach $item ( @$FILTER_ARGS )
	{
		unless ( (grep { /$item->{'column'}/ } @$OUTPUT_ARGS)
			|| (grep { /$item->{'column'}/ } @DEFAULT_SELECT_FIELDS) )
		{
			my $comma = ",";	# We'll need a comma after a field name if not a coalesce.
			if ( defined $COALESCE{$item->{'column'}} )
			{
				$QUERY .= ", coalesce($item->{'column'}, '$COALESCE{$item->{'column'}}') AS ";
				$comma = "";	# No comma if the field name is part of a coalesce.
			}
			(my $tablename, my $columnname) = split (/\./, $item->{'column'});
			# If we found a column name, use it. Otherwise use the original column name.
			if ($columnname) { $QUERY .= "$comma \"$columnname\"" }
			else { $QUERY .= "$comma \"$item->{'column'}\"" }
		}
	}
	# Parse out the additional output fields, if any, that we're retrieving.
	foreach $item ( @$OUTPUT_ARGS )
	{
#print "output_arg= $item\n";
		unless ( 	$item eq 'id' || $item eq "$Def_Member_Tbl.id" || 
				$item eq 'spid' || $item eq "$Def_Member_Tbl.spid" )
		{
			if ( defined $COALESCE{$item} )
			{
				$QUERY .= ", coalesce($item, '$COALESCE{$item}') AS ";
				# If we found a column name, use it. Otherwise use the original column name.
			}
			else
			{
				$QUERY .= ", $item  AS ";
			}
			(my $tablename, my $columnname) = split (/\./, $item);
			if ($columnname) { $QUERY .= "\"$columnname\"" }
			else { $QUERY .= "\"$item\"" }
		}
	}
	$QUERY .= " FROM $Def_Member_Tbl ";

	# Parse out the additional tables to be joined, if any.
	foreach $item ( @$TABLE_ARGS )
	{
		# I don't know how we would end up with an empty table arg, but I'm not spending the time to look into it
		next unless $item;
		unless ($JOINS{$item}){
			next if $item =~ /^members/i;		# variations of members are handled automatically above
			print "missing \$JOINS{$item}\n";	# if we have forgotten to define a needed table, this will let us know
			next;
		}
		$QUERY .= " $JOINS{$item} ";
	}

	# Add on the query criteria.
	###### only these membertypes will be included or drilled out
	$QUERY .= "
		WHERE $Def_Member_Tbl.membertype IN ('m','ag','v','s','ma','tp')
		AND $Def_Member_Tbl.spid = ?";
#warn "qry: $QUERY";
	# Prepare the query once for the while loop below.
	my $sth = $db->prepare($QUERY);
#if ( $id == 12 || $id == 15 ) { print "$QUERY  *** spid = $id\n\n"; }

	# Initialize the SPID hash.
	$a{$id} = $id;

	# Keep only the column name for filtering below.
	foreach $item ( @$FILTER_ARGS )
	{
		(my $tablename, my $columnname) = split (/\./, $item->{'column'});
		# If we found a column name, use it. Otherwise use the original column name.
		if ($columnname) { $item->{'column'} = $columnname }
	}
	# Here's where we start growing our network tree.
	# While we have found members with the current spid(s).
	while (keys %a > 0)
	{
		foreach my $member (keys %a)
		{
warn "processing sponsor: $member";
			my $success = $sth->execute($member);
			unless ( $success )
			{
			#	$REPORT = "ERROR: sth->execute failed in NetSearch.pm.\n" . $db->errstr . $REPORT;
			#	goto END;
			# the above has proven to be unhelpful in determining the problem and a resolution
			# I think we will just continue with the next item on the list instead of bombing
				$REPORT = "ERROR: sth->execute failed in NetSearch.pm.\n" . $db->errstr . "\nmember: $member\nquery: $QUERY\n";
				next;
			}

			my ($result, %res) = ();
			# Process all members found based upon the query criteria.
			while ($dat = $sth->fetchrow_hashref)
			{
#warn "got data for $member";
				$result = %res = ();
				# Process all of the filter args for each member.
				foreach $item ( @$FILTER_ARGS )
				{
###### Change the id(s) to see only those of interest.
#if ($dat->{id} == 14041 || $dat->{id} == 18594 || $dat->{id} == 14254) {print "Filter_Arg= $item->{'column'}; Value= $item->{'value'}\n"};
					# If this is an alpha-numeric comparison, we single quote the args.
#warn "item name: $item->{'column'} op: $item->{'operator'} op_value: $item->{'value'} actual: $dat->{$item->{'column'}}";
#	if $item->{'column'} eq 'pool_mgt';
					if ( $item->{'operator'} =~ /\w/ )
					{
						$result = eval("'$dat->{$item->{'column'}}' $item->{'operator'} '$item->{'value'}'");
					}
					# If this is a numeric comparison, we don't use quotes.
					else
					{
						$result = eval("$dat->{$item->{'column'}} $item->{'operator'} $item->{'value'}");
					}
#warn "result: $result"
#	if $item->{'column'} eq 'pool_mgt';
#if ($dat->{id} == 14041 || $dat->{id} == 18594 || $dat->{id} == 14254) {print "Running eval on $dat->{$item->{'column'}} $item->{'operator'} $item->{'value'}\n"};
					if ( !defined $result || $result == 0 )
					{
						$result = -1;
					}

#if ($dat->{id} == 14041 || $dat->{id} == 18594 || $dat->{id} == 14254) {print "intermediate result= $result\n"};
					# our key looks like 'a_level individual' or 'a_level group'
					# If there is no value for this key or the current result is -1.											
					if ( (not defined $res{"$item->{'column'} $item->{'exclude'}"})	|| $result == -1 )
					{	
						$res{"$item->{'column'} $item->{'exclude'}"} = $result;
					}						
				}

				###### looking for cases that didn't match
				$result ='';
				foreach ( keys %res )
				{				
#if ($dat->{id} == 14041 || $dat->{id} == 18594 || $dat->{id} == 14254) {print "$_= $res{$_}\n";}
					if ( $res{$_} == -1 )
					{
						$result = $_;
						# If it's a group, we don't need to check further.
						last if $result =~ ' group$';
					}
				}
#if ($dat->{id} == 14041 || $dat->{id} == 18594 || $dat->{id} == 14254) {print "Final result - removed $result for $dat->{id}.\n\n"};
				if ( $result )
				{
#$REPORT .= "*** $item->{'column'}= $item->{'value'}\n";
					# If marked to exclude individual - this member only.
				#		if ( $item->{'exclude'} eq 'individual' )
					if ( $result =~ ' individual$' )
					{ 
						# Add this member to the drill list but not the report. 
						$b{$dat->{'id'}} = $dat->{'alias'};
						$depths = $depth unless ($depths gt $depth);
						$alias{$dat->{'id'}} = $dat->{'alias'};
						goto END_WHILE; 
					}
					# If marked to exclude group - member and their downline.
				#		elsif ( $item->{'exclude'} eq 'group' )
					elsif ( $result =~ ' group$' )
					{ 
						# Don't add this member to the drill list or report. 
						goto END_WHILE; 
					}								
				}								
				# Add this member to the drill list. 
				$b{$dat->{'id'}} = $dat->{'alias'};
				$depths = $depth unless ($depths gt $depth);
				$alias{$dat->{'id'}} = $dat->{'alias'};

				# Add this member to the report. 
				foreach $item ( @$OUTPUT_ARGS )
				{
					# Keep only the column name for output below.
					(my $tablename, my $columnname) = split (/\./, $item);
					
					# If we found a column name, use it. Otherwise use the original column name.
					if ($columnname)
					{
						$item = $columnname;
					}
#if ( $id == 12 || $id == 15 ) { print $item . "\n" }
					$dat->{$item} =~ s/,/ /g if $dat->{$item};
					$dat->{$item} = '' unless defined $dat->{$item};
					$REPORT .= "\"$dat->{$item}\",";
	 			}
				# get rid of trailing comma.
				chop $REPORT;
				$REPORT .= "\r\n";

				END_WHILE:
			} # End while

			$sth->finish;
			reset;
		}
		%a = %b;
		%b = ();
		$depth++;
	}
	END:
	return \$REPORT;
}

1;


###### 07/26/04 Modified the eval results (%res) tests to look through the hash until we either
######		find a group (then exit the loop) or until we have looked at all hash elements.
###### 10/15/04 Added a line to change the $result to -1 if it eval'd to 0 or was undefined.
######		Also added default assignment of the $result if it was -1, so as to override any
######		existing 1's, and therefore force an individual or group removal. 
###### 12/29/04 Added a_level.a_level, nbteam.level, and network_calcs.npp to the COALESCE hash.
######		Added network_calcs to the JOINS hash. Modified the $QUERY building section
######		 and the $REPORT output section to strip the tablename from output column names
######		 because the compound column names weren't being output.
######	03/04/05 Added the $comma variable to the FILTER_ARGS $QUERY building section. If the 
######		field was not a coalesce field, a comma was not being inserted before the field name.
# 07/05/06 added a coalesce on company_name
# 11/29/06 added join for rp_summary_vw
###### added cellphone & other_contact_info to %COALESCE
###### 10/23/09 added handling for the member_pools and member_pool_requests tables
# 12/28/09 added the ma membertype to the list of membertypes we would drill
###### 08/09/10 tweaked the error handling used while drilling
###### 03/05/12 ripped out a bunch of stuff pertaining to roles and the old network_calcs table
###### 04/16/14 added od.pool_vw to the table join definitions after receiving this email notice: ...missing FROM-clause entry for table "pool_vw"
