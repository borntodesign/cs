package EbizPrereq;

use strict;

sub Check_All($$$;$)
{
	###### looking for a team level and a member hashref against which
	###### we'll check to be sure they have met all the prerequisites
	###### if not we'll return a list of the ones they haven't

	###### at this time, I'm not going to check the validity of the arguments
	my ($db, $level, $mbr, $maint) = @_;
	my $result = 1;
	my @list = ();

	foreach my $tmp(Get_By_Level($db,$level,$maint))
	{
		my $ck = eval $tmp->{object};
		my $rv = &$ck($mbr);
		unless ($rv)
		{
			$result = 0;
			push (@list, $tmp->{label});
		}
	}
	return (wantarray) ? ($result, @list) : $result;
}

sub Get_By_Level($$;$)
{
	my $db = shift || die "DB handle cannot be undef";
	my $arg = shift || die "Argument cannot be undef";
	my $maint = shift;
	if ($arg =~ /\D/)
	{
		warn "Level argument must be numeric only";
		return;
	}
	my $qry ="
		SELECT * FROM nbteam_prerequisites
		WHERE 	active= TRUE
		AND 	level= ?
		AND ";

	$qry .= ($maint) ? "maint_req= TRUE" : "entry_prereq= TRUE";

	my $sth = $db->prepare($qry);
	$sth->execute($arg);
	my @list = ();
	while ($arg = $sth->fetchrow_hashref){ push (@list, $arg) }
	$sth->finish;
	return (wantarray) ? @list : \@list;
}

sub Get_Specific($$)
{
	my $db = shift || die "DB handle cannot be undef";
	my $arg = shift || die "Argument cannot be undef";
	if ($arg =~ /\D/)
	{
		warn "Argument must be numeric only";
		return;
	}
	my $sth = $db->prepare("
		SELECT * FROM nbteam_prerequisites
		WHERE 	active= TRUE
		AND 	pk= ?");
	$sth->execute($arg);
	$arg = $sth->fetchrow_hashref;
	$sth->finish;
	return $arg;
}

1;

