package G_S;
###### acronym for Global Stuff
###### last modified 02/19/16	Bill MacArthur
###### rev. 3.4

use strict;
use Carp;

use Encode;
use Digest::MD5 qw( md5_hex );
use Date::Calc;
use Apache::CS_Authen;
use Apache::CS_Merchant_Authen;
use DHSGlobals;
use base 'Exporter';
use Email::Valid;
#use CS_Session;
use csmLogin;
require '/home/httpd/cgi-lib/CC.list';	# used in cbo_Country()
	
our @EXPORT_OK = qw(
	%MEMBER_TYPES $EXEC_BASE $COACH_LEVEL $DATA_CONTACT $LOGIN_URL %URLS
	$ERROR_PAGE $SALT $BUILDER_LEVEL %GIVECREDIT @TEAM_LEADERS $ERR_MSG
 );

###### Internal Globals

# when this package is not instantiated, the following variable will end up containing an instance for use in subsequent function call
our ($Q) = ();

# we won't export this one, but it will be available anyway by calling @G_S::language_prefs
# it may be used externally, so don't remove it
our @language_prefs = ();	###### this will be an array of arrayrefs

###### EXPOSED GLOBAL VARIABLES

# this error message can be used when importing the set_message in CGI.pm to provide some
# further user instruction beyond the simple time and date to the webmaster thing
our $ERR_MSG = "For help, please send mail to the webmaster (<a href=\"mailto:webmaster\@dhs-club.com\">webmaster\@dhs-club.com</a>), giving the time and date of the error, the URL and a brief description of what you were trying to do when the error occurred. Your feedback is appreciated.";

our $BUILDER_LEVEL = 100;	# this is an artifact from years ago, but there are still scripts looking to import this so we cannot just make it go away.. 02/19/16
our $COACH_LEVEL = 4;	###### the level above which notifications will not be sent
our $DATA_CONTACT = 'data@dhs-club.com';
our $HTML_TMPL_DIR = '/home/httpd/cgi-templates/Html';
our $TEXT_TMPL_DIR = '/home/httpd/cgi-templates/Text';
our $EMAIL_TMPL_DIR = '/home/httpd/cgi-templates/Email';
our $EXEC_BASE = 11;	###### the lowest EXEC level
our $PRODUCTION_SERVER = DHSGlobals::PRODUCTION_SERVER;

#our $LOGIN_URL = $CS_Session::LOGIN_URL;	#rolled into %URLS below as well
our $LOGIN_URL = '/cs/login.shtml';

###### we may be able to use the Member_Types table in the future in case we have some additional variations
our %MEMBER_TYPES = (
	'm' => 'Affiliate',
	'r' => 'Removed',
	's' => 'Member',
	't' => 'Terminated',
	'v' => 'Partner',
	'c' => 'Cancelled',
	'ag'=> 'Affinity Group',
	'agu'=>'Affinity Group Unit',
	'staff'=>'Staff Member',
	'ma' => 'Merchant Affiliate',
	'x' => 'Unconfirmed',
	'amp' => 'Affiliate',
	'tp' => 'Trial Partner'
);

our %URLS = (
	'VLR'	=> '/cgi/vlinerpt.cgi',		# vertical line reports
	'PLR'	=> '/cgi-bin/ppreport.cgi',	# personal line reports
	'LOGIN'	=> $LOGIN_URL
);

our $ERROR_PAGE = qq|
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html><head>
	<title>Error</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head><body bgcolor="#FFFFFF">
	<h4>%%MESSAGE%%</h4>
	<tt>%%timestamp%%</tt> (PST)
	</body></html>|;

###### cgi_object should reflect the value here if its still active
###### changing as of 03/03/05 since it seems we may have memberapp links out there with the
###### old salt enabled
#our $SALT = 'onjAnk%3';
our $SALT = 'onjAnk%3(*)';

		
###### GLOBAL FUNCTIONS

=head2 new

	If you call G_S->new() you can pass in an active DB handle inside a hashref like this: G_S->new({'db'=>$dbh})
	Other arguments that can be passed inside that hashref are 'CGI' => a CGI object

=cut

sub new
{
	###### reinitialize these vars - especially important for mod_perl
	($Q, @language_prefs) = ();

	#
	my $class = shift;
	my $args = shift || {};
	croak "Arguments should be passed via a hashref" unless ref $args eq 'HASH';

	_initialize_language_prefs();

	$args->{'language_prefs'} = \@language_prefs;
	return bless ($args, $class);
}

sub Alias2ID($$$)
{
###### get a numeric ID from an alias
###### we are receiving a DB handle, a table to work from (convenient if we are using a test table) & an alias
	my ($self, $db, $tbl, $alias) = _self_or_default(@_);

	my $sth = $db->prepare("SELECT id FROM $tbl WHERE alias = ?");
	$sth->execute(uc $alias);
	my $id = $sth->fetchrow_array;
	$sth->finish;
	return $id;
}

sub authen_ses_key($;$)
{	###### in order to authenticate using different mechanisms on the other end, we need to tell
	###### it what we want, it defaults to 'Generic' on the other end at this time, but that may change
	my ( $self, $encrypted_session_key, $auth_name ) = _self_or_default(@_);
	$auth_name ||= 'Generic';
	my ($id, $hash) = ();
	
	if ($auth_name eq 'maf')
	{
		($id, $hash) = CS_Merchant_Authen->authen_ses_key(undef, $encrypted_session_key);
	}
	else
	{
		($id, $hash) = CS_Authen->authen_ses_key(undef, $encrypted_session_key);
	}
#	my ($id, $hash) = CS_Authen->authen_ses_key(undef, $encrypted_session_key);
#	my ($id, $hash) = CS_Session::authen_ses_key('','', $encrypted_session_key, 1, $auth_name);
	
	# we have run into issues with certain key values coming back with the utf8 flag on... this has proven to be a problem in at least one scenario
	# so at the risk of creating other problems, we are going to turn off the flag if it is on and let the caller deal with things as it sees fit
	foreach (keys %{$hash})
	{
		$hash->{$_} = Encode::encode_utf8($hash->{$_}) if Encode::is_utf8($hash->{$_});
	}
	return ($id, $hash);
}

sub cbo_Country($$;$$)
{
###### besides a cgi object, takes a control name, default value and optional class
###### and returns a select box with all the countries
	my ($self, $q, $param_name, $default, $class) = _self_or_default(@_);
	croak 'Missing $q' unless $q;
	croak 'Missing $param_name' unless $param_name;

	$default = '' unless $default;
	$class = 'in' unless $class;
	return $q->popup_menu(
		'-name'		=>$param_name,
		'-class'	=>$class,
		'-values'	=>\@CC::COUNTRY_CODES,
		'-default'	=>$default,
		'-labels'	=>\%CC::COUNTRIES
	);
}

=head2 cboCountry

	Create an HTML <select> element with a translated list of countries out of the DB
	This method requires that you call new() with a CGI object and a DB handle.
	The version of the similarly named method is a newer updated version.
	
=head3 Arguments

	An optional hashref of arguments to define aspects of the select control.
	The hashref key/values will be passed directly to CGI to create the control, so use the same syntax that you would use to define a popup_menu with CGI.
	This includes the use of -default for setting the selected option.
	
	An optional hashref of other arguments that are not passed to CGI. The following key/value pairs are recognized:
	{
		'language_pref' => a 2 character language code,	# If none is provided, this modules understanding of the first language pref will be the default.
		'no_first_elem_empty' => 1,			# setting this to a true value will disable the default behavior to create an option at the top of the list with an empty value
		'first_elem_label' => 'some value',	# this will change the default empty option at the top of the list and set it's label to this value
		'no_restricted_countries' => 1		# setting this to a true value will change the list to NOT include countries which are marked in the DB as restricted
	}
	
=head3 Returns

	An HTML <select> element
	
=cut

sub cboCountry($;$$)
{
	my ($self, $ctlargs, $args) = _self_or_default(@_);
	$ctlargs ||= {};
	$args ||= {};

	my @vals = ();
	my %labels = ();
	unless ($args->{'no_first_elem_empty'})
	{
		push @vals, '';
		$labels{''} = $args->{'first_elem_label'} || '';
	}
	
	my $tbl = $args->{'no_restricted_countries'} ? 'country_names_translated_no_restrictions' : 'country_names_translated';
	my $sth = $self->db->prepare("SELECT country_code, country_name FROM $tbl(?) ORDER BY country_name");
	$sth->execute($args->{'language_pref'} || $language_prefs[0]->[1]);
	
	while (my ($country_code, $country_name) = $sth->fetchrow_array)
	{
		push @vals, $country_code;
		$labels{$country_code} = $country_name;
	}
	return $self->CGI->popup_menu(
		%{$ctlargs},
		'-values' =>\@vals,
		'-labels' =>\%labels
	);
}

sub Check_Email($)
{
    # Initialize local email variable with input to subroutine.
    my ($self, $email) = _self_or_default(@_);
	return unless $email;

# let's just use a standard module
	return (Email::Valid->address($email) ? 1 : 0);
}

sub Check_in_downline($$$$)
{
###### takes an ID and checks to see if the other ID passed is in the downline of the first
###### this is to make sure someone doesn't cheat and try to look at someone not in their downline
	my ($self, $db, $tbl, $id, $did) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;

	my ($sth);
###### if both numbers are not passed, we have to quit
	return 0 unless ($id && $did);
	return 1 if ($id eq $did);

###### if aliases were passed instead of numeric IDs, we'll query for the numeric vals
	if ($id =~ /\D/){ $id = Alias2ID($db, $tbl, $id)}
	if ($did =~ /\D/){ $did = Alias2ID($db, $tbl, $did)}

###### if both numbers are not resolved after conversion from aliases, we have to quit
	return 0 unless ($id && $did);

###### we will keep drilling up from Downline ID until we reach the ID or 01
#	my $sth = $db->prepare("SELECT spid FROM $tbl WHERE id= $did"); $sth->execute;
#	$did = $sth->fetchrow_array; $sth->finish;
	while ($did != $id && $did != 1)
	{
		$sth = $db->prepare("SELECT spid FROM $tbl WHERE id= $did"); $sth->execute;
		$did = $sth->fetchrow_array; $sth->finish;

	###### since I have seen a few error messages about a query with an empty value, we'll quit if
	###### we don't get a spid for some reason
		return 0 unless $did;
	}
	return ($did == $id) ? (1) : (0);
}


=head2 CGI

	Intended as an internal method to return the CGI object that should have been set when new() was called.
	If it was not set or the "object" is undefined, this method will DIE.

=cut

sub CGI
{
	my ($self) = _self_or_default(@_);
	die 'Apparently CGI was not set when new() was called' unless $self->{'CGI'};
	return $self->{'CGI'};
}

=head2 CoerceLangPref

There are some cases where we want to set the display language manually instead of relying on the browser.
Although we could define the desired lang_pref in a Get_Object call, this is not that convenient if we end up having to modify lots of code to do so.
Being able to set this once and forget it, in some non-display code is preferable in many cases.
Plus, by doing it this way, we get the fallback benefit of the browser's language preferences if the one we have defined is unavailable for the specified resource.
English becomes the default if the lang_pref is passed into Get_Object

Anyway, calling this method with a simple 2 letter language code places that language at the top of the preferences stack

=cut

sub CoerceLangPref($)
{
	my ($self, $lp) = _self_or_default(@_);
	unshift @language_prefs, [$lp,$lp];
}

sub Conv2UTF8($)
{
	my ($self, $list) = _self_or_default(@_);
	_conv2('utf8', $list);
}

sub ConvHashref
{
	return;	# don't need this operation anymore
	# type = the type of conversion UTF8 or Asc
	# ref = the hashref we are working on
	# list = an arrayref to explicit list of elements to process from the hashref
	# (default is all elements)
	#
#	my ($self, $type, $ref, $list) = _self_or_default(@_);
#	my $conv = ();
#	if ($type =~ /^UTF8$/i){ $conv = \&Conv2UTF8 }
#	elsif ($type =~ /^Asc$/i){ $conv = \&Conv2Asc }
#	else { croak "Invalid conversion type: $type" }
#
#	croak "Variable is not a hashref" unless ref $ref eq 'HASH';
#	$list = [(keys %{$ref})] unless ($list && scalar @$list > 0);
#
#	foreach(@$list)
#	{
#		&$conv(\$ref->{$_});
#	}
#	return;
}

sub Country_Has_States($$)
{
	###### require a DB handle and a country to check on
	###### returns TRUE if there is a country in the state codes table indicating that we have state data for that country
	###### otherwise returns NULL
	my ($self, $db, $country) = _self_or_default(@_);
	croak 'DB handle is undefined in Country_Has_States.' unless $db;

	return unless $country;

	# this type of query is much cheaper than an aggregate					
	my $sth = $db->prepare("
		SELECT country
		FROM state_codes
		WHERE country= ?
		LIMIT 1");
	$sth->execute($country);
	my $rv = $sth->fetchrow_array;
	$sth->finish;
	return ($rv) ? 1 : undef;
}

=head2 Create_AuthCustom_Generic_ticket / Create_AuthCustom_Generic_cookie

Pass in a "member record" with the necessary fields, get a value back that can be used to create the actual cookie
We need {
		id => a numeric ID,
		spid => a numeric sponsor ID,
		username => typically a member "alias" but could be a merchant username if we are building a maf cookie
		firstname => self explanatory,
		lastname => "",
		emailaddress => "",
		membertype => "",
		status => the member status value
	}
	
the xxx_ticket simply returns the value of the cookie that would be set
the xxx_cookie returns a value that can be stuffed into a cookie array

=cut

sub Create_AuthCustom_Generic_cookie
{
	my ( $self, $hashref, $auth_name ) = _self_or_default(@_);
	my $rv = $self->Create_AuthCustom_Generic_ticket($hashref, $auth_name);
	return "AuthCustom_Generic=$rv; domain=.clubshop.com; path=/";
}

sub Create_AuthCustom_Generic_ticket
{
	my ( $self, $hashref, $auth_name ) = _self_or_default(@_);
#	return AuthCustom::CreateTicket(undef, undef, $hashref, ($auth_name || 'Generic'));
	return csmLogin::Create_CS_Authen_General_Ticket(undef, $hashref);
}

sub db
{
	my ($self, $db) = _self_or_default(@_);

	$self->{'db'} = $db if $db;
	
	# backwards compatibility where we were not initialized with a DB handle of any sort, we will look for one in main ;)
	eval(q|$self->{'db'} = $main::db|) unless defined $self->{'db'};

	return
		((ref $self->{'db'}) eq 'SCALAR' || (ref $self->{'db'}) eq 'REF') ? ${$self->{'db'}} :	# we could have been passed a scalar reference that will become a DB handle
		((ref $self->{'db'}) eq 'CODE') ? &{$self->{'db'}} :
			$self->{'db'};
}

sub Duplicate_Email_CK($$$$;$)
{
	###### takes the id and 1 or 2 email addresses and returns nulls for no match or a matching address/es
	###### if the id passed = 0, then we omit the id exclusion from the query
	###### this would be the case on free member signups
	###### whereas on member upgrades, we have to exclude the row with the ID
	my ( $self, $db, $id, $email, $email2, $mtype ) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;

	my $qry = "
		SELECT emailaddress,
				emailaddress2
		FROM 	members
		WHERE ( emailaddress = '$email'
		OR 	emailaddress2 = '$email'";
		
	if ( $email2 )
	{
		$qry .= " OR emailaddress = '$email2' OR emailaddress2 = '$email2'";
	}

	$qry .= ')';

	$qry .= " AND membertype IN ($mtype)\n" if $mtype;
	
	if ($id)
	{
		###### handle the case where we receive an alias instead of a numeric ID
		$qry .= ($id =~ /\D/) ? " AND alias != ${\$db->quote(uc $id)}" : " AND id != $id";
	}
		
	my $sth = $db->prepare( $qry );
	$sth->execute;
	my ($match, $match2) = $sth->fetchrow_array;
	$sth->finish;

	###### like to keep warnings quiet for -w
	$match ||= '';
	$match2 ||= '';

	return ($match, $match2);
}

sub Ebiz_Decode_Cookie($;$)
{
###### requires the value of the cookie to be passed
###### also takes an optional 'key' argument to indicate the specific return value
###### desired - the default is a hashref to all the values
	my ($self, $ck, $arg) = _self_or_default(@_);
	return unless $ck;

	my ($hash, %list) = ();

	# if we add values, we'll always leave the hash on the end
	(	$list{session_id}, 
		$list{'id'},
		$list{course_id},
		$hash	) = split(/\|/, $ck);

	###### if the cookie doesn't split out to the hash, then there is a problem
	return unless $hash;

	return unless $hash eq md5_hex( join ("|",
		$list{session_id}, 
		$list{'id'},
		$list{course_id},
		$SALT));

	if ($arg)
	{
		return (exists $list{$arg}) ? $list{$arg} : undef;
	}

	return \%list;
}

sub Get_Coach($$;$)
{
###### routine to pass back the info on our 'COACH'
###### we will actually return a hashref to the row of data on our COACH so that all available info will
###### be available to the caller
	my ($self, $db, $id, $coach_level) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;

	return  unless ( $id && $id > 1 );

###### in case the caller doesn't pass a level, we'll use the default
	$coach_level = $COACH_LEVEL unless ( $coach_level );

	my $sth = $db->prepare("
				SELECT	m.*,
					COALESCE(v.a_level, 0) AS a_level
				FROM	members m
				LEFT JOIN a_level v
				ON 	v.id=m.id
				WHERE 	m.id= ?");

	my $data = {spid => $id};

###### now we'll get our sponsor and then begin a loop to keep searching upline until we get to COACH material
###### if we don't have coach material, stop at 01
	do
	{
		$sth->execute($data->{spid});
		$data = $sth->fetchrow_hashref ||
	###### this occasion should not occur, but if it does, we want to know
			croak "Upline search failed in G_S::Get_Coach ID= $data->{spid}";

	} while ( $data->{'id'} > 1 && !(
			$data->{a_level} >= $coach_level &&
			$data->{membertype} eq 'v')
	);

	$sth->finish;

###### once we have found a coach,
###### we'll just pass back the hashref with all of their info
	return $data;
}

=head2 Get_LangPref

This method accepts no arguments.
Returns the highest valued language preference if the caller expects a scalar.
Otherwise, it returns the entire array of language preferences.

=cut

sub Get_LangPref(;$)
{
	my ($self) = _self_or_default(@_);
	_initialize_language_prefs() unless @language_prefs;
	return wantarray ? @language_prefs : $language_prefs[0]->[1];
}

=head2 Get_Object

=head3 Description

Retrieve an object from our cgi_objects collection

=head3 Arguments:

	An active DB handle
	The cgi_object ID
	An optional language code (the default is the language code of the browser)
	An optional flag, which if True, will cause the function to return an array instead of a scalar value.

=head3 Returns:

	The text of the document in the translation desired, if available, or English if not.
	We could return undef if the document was not found under the specified key or for other internal problems.
	If the caller is expecting an array, an array will be returned instead, with the second value being the actual 2 character code of the document
	
=cut

sub Get_Object($$;$)
{
	my ($self, $db, $obj_id, $lang_spec) = _self_or_default(@_);

	croak 'DB handle is undefined.' unless defined $db;
	croak 'Object identifier is undefined or empty.' unless $obj_id;
	croak 'Object ID must be numeric' if $obj_id =~ /\D/;

	my $actual_language = 'en';
	
	my ($obj, $obj_type, $tk) = $db->selectrow_array("
		SELECT obj_value, obj_type, translation_key
		FROM cgi_objects
		WHERE obj_id= ?
		AND active=TRUE	-- added this in on 11/20/09 as I thought it always behaved like this
					-- being able to disable templates and be alerted if they are still being used
					-- makes it possible to ultimately delete them
	", undef, $obj_id);	# you would think you could trust $obj_id, but some programmers have passed in possibly textual user provided data
				# and to make matters worse they somehow got by the 'croak' above.... ?

	# we've had a few cases where the DB handle was disconnected
	return unless defined $obj_type;

	###### if this is a translation key we'll pull the appropriate object from our
	###### translations
	my $result = ();
#	if ($obj_type == 10)
	if ($tk)
	{
		($result, $actual_language) = _get_translation($db, $obj_id, $lang_spec);
	}
	elsif ($obj_type == 11)
	{
		###### anticipating XML documents
	}
	
	if ($result)
	{
		return wantarray ? ($result, $actual_language) : $result;
	}
	return wantarray ? ($obj, $actual_language) : $obj;
}

=head2 GetCoach

	A rewrite of the old Get_Coach function. This one uses the internal DB handle and utilizes a DB function instead of code to drill out the result.
	
=head3 Arguments

	An integer ID for the starting point. The ID should be a Partner ID.
	
=head3 Returns

	A members record hashref.
	
=cut

sub GetCoach
{
	my ($self, $id) = _self_or_default(@_);
	return $self->db->selectrow_hashref('SELECT * FROM my_upline_coach(?)', undef, $id);
}

=head2 GetObject

	A wrapper around Get_Object that utilizes the DBH set in the call to new()
	
=cut

sub GetObject
{
	my ($self, $obj_id, $lang_spec) = _self_or_default(@_);
	my @a = $self->Get_Object($self->db, $obj_id, $lang_spec);
	return wantarray ? @a : $a[0];
}

=head2	GetPathParts

Accepts a path like /stuff/otherstuff
Returns an array ('stuff','otherstuff')

=cut

sub GetPathParts
{
	my ($self, $path) = _self_or_default(@_);
	my @a = split /\//, $path;
	# remove the 1st element as it is null... path = /stuff/otherstuff... split = (undef,'stuff','otherstuff')
	shift @a;
	return wantarray ? @a : $a[0];
}

sub Get_Sponsor($$)
{
###### $db is our passed DB handle
	my ($self, $db, $id) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;

	my $qry = "
		SELECT
			m.*,
			COALESCE(a.a_level, 0) AS a_level
		FROM
			members m,
			members
		LEFT JOIN a_level a
		ON 	a.id = members.spid
		WHERE
			members.spid = m.id
		AND 	members.id = $id";
		
	my $sth = $db->prepare($qry);
	$sth->execute;
	my $data = $sth->fetchrow_hashref;
	$sth->finish;
	return $data;
}

sub Get_VIP_Sponsor($$;$)
{
	my ($self, $db, $id) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;

	my ($sth, $dat) = ();

###### initialize our testing parameters
	$dat->{'id'} = 2;
	$dat->{spid} = $id;
	$dat->{membertype} = ' ';
#	$dat->{age} = 0;
#	$age = 0 unless $age;

	my $qry = "
		SELECT m.*,
			COALESCE(a.a_level, 0) AS a_level
	--		,DATE_PART('day', NOW() - COALESCE(vip_date, NOW())) AS age
		FROM members m
		LEFT JOIN a_level a
		ON a.id = m.id
		WHERE m.id= ?";

	$sth = $db->prepare($qry);

#	while ($dat->{membertype} !~ 'v' && $dat->{age} <= $age && $dat->{'id'} > 1){
	while ($dat->{membertype} !~ 'v' && $dat->{'id'} > 1)
	{
		$sth->execute($dat->{spid});
		$dat = $sth->fetchrow_hashref;

###### abort if we get nothing back
		unless ($dat->{'id'})
		{
			$sth->finish;
			return ;
		}
	}
	$sth->finish;
	return $dat;
}

=head2 GetPartnerSponsor

	A rewrite of the old Get_VIP_Sponsor function that uses our internal DB handle and a DB function instead of code to get the value.
	
=head3 Arguments

	An integer ID
	
=head3 Returns

	A member record hashref of the first Partner starting with the ID and upward.

=cut

sub GetPartnerSponsor
{
	my ($self, $id) = _self_or_default(@_);
	return $self->db->selectrow_hashref('SELECT * FROM my_upline_vip(?)', undef, $id);
}

=head2 GetTemplate

=head3 Description

A 'smart' method that will look for file based templates -or- cgi_object templates based upon the value passed.
Namely, if the template passed is a number, it will simply call Get_Object with that template ID,
otherwise, it will look for the template in the file system.

=head3 Arguments

A value that should identify a cgi_objects template ID -or- a file system path to a template.
If a path is passed that begins with a forward slash, then we will search from / otherwise we will search from the default base path of /home/clubshop.com/cgi-templates.

An optional language code.
For cgi_objects, Get_Object returns the desired language version or English as the default.
For file system versions, the logic goes like this in the presence of a language code:
1. Look for file_path.lang_spec
2. Look for file_path.en
3. Look for file_path

If no language code is provided, we do #3 only

=head3 Returns

The value of the template if found -or- empty if not and an error will be generated.

=cut

sub GetTemplate
{
	my ($self, $obj_id, $lang_spec) = _self_or_default(@_);
	
	# $lang_spec could be tainted, so we will impose a very simplistic evaluation... it must be two or three characters at most
	if ($lang_spec && $lang_spec !~ m/^[a-z]{2,3}$/)
	{
		warn "Illegal lang spec passed: $lang_spec";
		return undef;
	}
	
	return unless $obj_id;
	if ($obj_id =~ m/\D/)
	{
		my $rv = ();
		my $base_path = '';
		if ($obj_id =~ m#^/#)
		{
			# we should not be looking elsewhere in the file system
			if ($obj_id !~ m#^/home/(clubshop\.com|httpd)/(cgi\-templates|html)#)
			{
				die "invalid path attempted";
			}
		}
		else
		{
			$base_path = '/home/clubshop.com/cgi-templates/';
		}
		
		my $rsrc = $base_path . $obj_id;
		if ($lang_spec)
		{
			if (-e $rsrc . $lang_spec)
			{
				$rsrc .= $lang_spec
			}
			elsif (-e $rsrc . 'en')
			{
				$rsrc .= 'en';
			}
		}
		
		if (-e $rsrc)
		{
			if (open(FH, $rsrc))
			{
				local $/;
				$rv = <FH>;
				close FH;
			}
			else
			{
				warn "Failed to open existing file: $rsrc\nerror: $!";
			}
			return $rv;
		}
		
		warn "file not found: $obj_id";
		return undef;
	}
	else
	{
		return $self->Get_Object($self->db, $obj_id, $lang_spec)
	}
}

sub Hash2XML(\%;%)
{
	###### a quicky routine for converting a simple hashref to a block of XML
	###### by default we will turn off the utf8 flag in case perl turns it on
	###### but we will accept an argument to modify that behavior if desired
	my ($self, $hr, %args) = _self_or_default(@_);
	return unless $hr;
	my $root = $args{'RootName'} || 'opt';
	my $xml = "<$root>\n";
	foreach (keys %{$hr})
	{
		$xml .= "<$_>" . (defined $hr->{$_} ? $hr->{$_} : '') . "</$_>\n";
#		print "$_ flag: ", utf8::is_utf8($hr->{$_}), "\n";
	}
	$xml .= "</$root>\n";
#	if ($args{utf8})
#	{
#		Encode::_utf8_on($xml) unless Encode::is_utf8($xml);
#	}
#	else { Encode::_utf8_off($xml) }
#	else { decode_utf8($xml) }
#	return decode_utf8($xml);
	return $xml;
}

=head2 in_array

=head3 Description

Similar to PHP's function of the same name, look for a needle in the haystack

=head3 Arguments

A scalar value (the needle) should be the first argument
The second argument should be an array reference (the haystack)

=head3 Returns

True/False as 1/0

=cut

sub in_array($\@)
{
	my ($self, $needle, $haystack) = _self_or_default(@_);
	# if we have no needle, how can we find it... and if the haystack is empty, then we'll never match anything
	return 0 unless (defined $needle) && ref($haystack) eq 'ARRAY' && @{$haystack};
	return (grep $_ eq $needle, @{$haystack}) ? 1:0;
}

sub Load_Messages($$)
{
	###### we need a DB handle and a template ID to retrieve a template for parsing
	my ($self, $db, $tid) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;
	return '' unless $tid;

	$tid = $self->Get_Object($db, $tid);
	return '' unless $tid;

	require XML::Simple;
	my $xml = new XML::Simple;
	return $xml->XMLin($tid, NoAttr=>1);
}

sub Mail_Refused($$)
{
###### $db is our passed DB handle, $add is the email address we are going to check against the
###### domains_blocked table after we extract the domain (these domains refuse our mail as spam)
###### we'll return 1 for a match and null for no match
	my ($self, $db, $add) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;
	return '' unless $add;

	$add = lc $add;

	my $sth = $db->prepare("
		SELECT domain FROM domains_blocked
		WHERE ? LIKE '\%\@'||domain OR ? LIKE '\%.'||domain");
	$sth->execute($add, $add);
	my $res = $sth->fetchrow_array;
	$sth->finish;
	return ($res) ? (1) : ('');
}

=head2 Make_Connection_SSL

=head3 Description

If we have been called in an https session, return 1/true
Otherwise, using CGI->self_url, change our protocol to https and issue a redirect and return 0/false

=cut

sub Make_Connection_SSL
{
	my ($self) = _self_or_default(@_);
	return 1 if $self->CGI->https;
	(my $url = $self->CGI->self_url) =~ s/^http/https/;
	print $self->CGI->redirect($url);
	return;
}

=head2 MembershipDupeCK

=head3 Description

Determine if we have duplicate memberships based on emailaddress

=head3 Arguments

Accepts a scalar which should be a valid emailaddress. The default duplicate checking will be performed.
Alternatively, a hash reference can be passed in, minimally with an emailaddress key which should have the emailaddress.
Other keys include:
	ignore_mtypes : a CSV list of membertypes to ignore when checking for dupes
	match_mtypes : a CSV list of membertypes to match when checking for dupes
	tmp_tbl : a table to use instead of the default "members" table ... don't really recall why this was once important

=cut

sub MembershipDupeCK
{
	###### we will accept either an email value
	###### or a hashref which should minimally have an emailaddress key
	my ($self, $arg) = _self_or_default(@_);
	return unless $arg;
	my $ref = ref $arg;
	my (@data, @ret, $match, $ignore) = ();
#	my $ignore = "'d','x','s'";
#        my $ignore = "'d'";	# revised 10-17-07 per Dick (one membership per emailaddress)
# as 0f 04/10 we are really going to try this again... one active membership perl emailaddress
# however, the key is "active"... removals, cancels, terms do not count as dupes

	my $tbl = 'members';	# we will default to this one but will accept a temp table argument
	unless ($ref)
	{
		if ($arg !~ /@/)
		{
			warn 'Invalid email address';
			return;
		}
		###### since we need the value twice, we'll put it in twice
		@data = ($arg, $arg);
	}
	elsif ($ref ne 'HASH')
	{
		warn 'Argument must be a scalar or hashref';
		return;
	}
	else
	{
		return unless $arg->{'emailaddress'};
		@data = ($arg->{'emailaddress'}, $arg->{'emailaddress'});
		push (@data, $arg->{'id'}) if $arg->{'id'};

		###### In order to allow the caller control on how strict the dupe
		###### checking will be, we will look for a 'ignore_mtypes' key
		###### in the hash. It should contain a CSV list of membertypes like
		###### 's,x'
		if (exists $arg->{'ignore_mtypes'})
		{
			$ignore = '';	# whether our argument is empty or not we will start with a clean slate
			if ($arg->{'ignore_mtypes'})
			{
#				foreach(split ',', $arg->{'ignore_mtypes'}){ $ignore .= "'$_'," }
				$ignore = "'" . ( join("','", (split( /,/, $arg->{'ignore_mtypes'}))) ) . "'";
#				chop $ignore;
			}
		}
		if ($arg->{'match_mtypes'})
		{
#			foreach(split ',', $arg->{'match_mtypes'}){ $match .= "'$_'," }
			$match = "'" . ( join("','", (split( /,/, $arg->{'match_mtypes'}))) ) . "'";
#			chop $match;
		}
		$tbl = $arg->{'temp_tbl'} if $arg->{'temp_tbl'};
	}

	my $qry = qq#
		SELECT m.firstname ||' '|| m.lastname AS name,
			m.membertype, m.alias, m.id, m.spid, m.oid,
			mt.label AS membertype_label
		FROM $tbl m
		JOIN "Member_Types" mt ON mt."code" = m.membertype
		#;

	$qry .= "WHERE (m.emailaddress= ? OR m.emailaddress2= ?)\n";

	# by joining on only active membertypes, we automatically exclude all the others
	$qry .= "AND mt.active=TRUE\n" unless $ignore || $match;

	$qry .=	"AND m.membertype NOT IN ($ignore)\n" if $ignore;
	$qry .=	"AND m.membertype IN ($match)\n" if $match;

	###### if we have an id argument, we can exclude that one from the check
	$qry .= 'AND m.id != ?' if scalar @data > 2;

	###### we will assume 'main' has a global/shareable DB handle for use
#	my $sth = $main::db->prepare("$qry ORDER BY m.id");
	my $sth = $self->db->prepare("$qry ORDER BY m.id");
	$sth->execute(@data);
	while (my $rv = $sth->fetchrow_hashref)
	{
		push (@ret, $rv);
	}
	$sth->finish;
	return unless @ret;
	return (wantarray) ? @ret : 1;
}

=head2 Null_to_empty

Convert the keys of a hash, passed as a hashref, from undef to empty
-or-
If you do the same thing for a scalar or an array reference

=head3 Arguments

A scalar or a hashref or an arrayref

=head3 Returns

For references, 1 if successfull.
For a scalar, the new value.

=cut

sub Null_to_empty
{
	my ($self, $arg) = _self_or_default(@_);
	return unless $arg;
	
	if (! ref $arg)
	{
		return defined $arg ? $arg : '';
	}
	elsif (ref $arg eq 'HASH')
	{
		foreach(keys %{$arg})
		{
			if (ref $arg->{$_})		# deal with nested hashrefs
			{
				$self->Null_to_empty($arg->{$_});
			}
			else
			{
				$arg->{$_} = '' unless defined $arg->{$_};
			}
		}
	}
	elsif (ref $arg eq 'ARRAY')
	{
		for(my $x=0; $x < scalar @{$arg}; $x++)
		{
			if (ref $arg->[$x])		# deal with nested hashrefs
			{
				$self->Null_to_empty($arg->[$x]);
			}
			else
			{
				$arg->[$x] = '' unless defined $arg->[$x];
			}
		}
	}
	else
	{
		return;
	}
	return 1;
}

=head2 ParseString

Pass in a string to be parsed that has placeholders like this:
ParseString('test %%first%% and %%second%%','A','YES');

Get back: 'test A and YES'

Naturally, what is between the placeholders does not affect the parser.
They are just intuitive labels that should relate perhaps to the variable name being used at the time.

=cut

sub ParseString
{
	my ($self, $s, @args) = _self_or_default(@_);
	my $x = 0;
	$s =~ s/%%.+?%%/$args[$x++]/g;
	return $s;
}

=head2 Prepare_UTF8

=head3 Description

Pass in a scalar or a hash reference
Apply decode_utf8() to the scalar or the hash values -if- they are not already UTF-8 encoded.

=cut

sub Prepare_UTF8
{	###### take care of our utf8 problems
	my ($self, $arg, $flag) = _self_or_default(@_);
	return undef if ! defined $arg;

	$flag ||= 'decode';	# to make this method backwards compatible where it only performed decode_utf8()
	
	###### we can accept a simple value which we will convert and return
	###### or a hashref, in which case we'll convert every key in place
	my $hashref = ref $arg eq 'HASH' ? $arg : {'key'=>$arg};

	foreach (keys %{$hashref})
	{
		if (not defined $hashref->{$_})
		{
			next;
		}
		elsif (ref $hashref->{$_})	# this is to deal with multi-level hashrefs
		{
			$self->Prepare_UTF8($hashref->{$_}, $flag);
		}
		elsif (Encode::is_utf8($hashref->{$_}))	# the flag is on
		{
			next unless $flag eq 'encode';
			$hashref->{$_} = encode_utf8($hashref->{$_});
		}
		else
		{
			next unless $flag eq 'decode';
			$hashref->{$_} = decode_utf8($hashref->{$_});
		}
	}
	return (ref $arg eq 'HASH') ? undef : $hashref->{'key'};
}

=head2 PreProcess_Input

	Remove control characters, leading and trailing whitespace, as well as condense multiple whitespace characters down to one

=head3 Arguments

	A string

=head3 Returns

	A filtered string

=cut

sub PreProcess_Input($)
{
	my ($self, $p) = _self_or_default(@_);
	return unless defined $p;
	
###### remove all control characters (below ASCII 32)
###### and other characters that should never be needed in the data
###### like ? % ^ $ ~ | * ! =
#		$p =~ s/[\x00-\x1f]|\?|%|\^|\$|~|\||\*|!|=|\+|_//g;
# removing that underscore has been problemattical
		$p =~ s/[\x00-\x1f]|\?|%|\^|\$|~|\||\*|!|=|\+//g;


###### filter out leading & trailing whitespace
		$p =~ s/^\s*//;
		$p =~ s/\s*$//;

###### convert other duplicate whitespace into one space
		$p =~ s/\s{2,}/ /g;

	return $p;		
}

=head2 select_array_of_hashrefs

	DBI/DBD:Pg does not currently provide a function that will return an array of hashrefs, a useful struct for when you want a set of hashrefs ordered in a particular way.
	selectall_hashref suffers from not being able to order unless you specify a key field on which you can order by, which oftentimes is not convenient.

=head3 Arguments

	Similar to the DBI function calls, this function expects:
		either a prepared statement handle or a textual SQL query as the first argument
		an optional hashref set of attributes (which for all intents and purposes is undef... but we will retain the option just in case) as the second argument
		an optional set of bind values

=head3 Returns

	Internally this routine will create an array of hashrefs which themselves point to the rows returned from the query:
	(
		{key=>value,...},
		...
	)
	The return "value" depends on if the caller is expecting an array or a scalar.
	Using wantarray, we will return either a list or a reference to the list.

=head3 Notes

	We are assuming that G_S new has been called with a DB argument. We will not be looking for a DB handle in our arguments

=cut

sub select_array_of_hashrefs
{
	my ($self, $stmt, $attr, @bind) = _self_or_default(@_);
	croak "You must initialize G_S with an active DB handle" unless $self->db;
	my $sth = (ref $stmt) ? $stmt : $self->db->prepare($stmt, $attr);
	return () unless $sth;
	$sth->execute(@bind) || return ();
	my @rv = ();
	while (my $row = $sth->fetchrow_hashref)
	{
		push @rv, $row
	}
	return wantarray ? @rv : \@rv;
}

=head2 selectall_array

	DBI/DBD:Pg does not currently provide a function that will return an array of individual values, a useful struct for when you simply want a set of values.

=head3 Arguments

	Similar to the DBI function calls, this function expects:
		either a prepared statement handle or a textual SQL query as the first argument
		an optional hashref set of attributes (which for all intents and purposes is undef... but we will retain the option just in case) as the second argument
		an optional set of bind values

	Note: the SQL should only return 1 column as we are building an array of single values ;)
	
=head3 Returns

	An array of values, so you should be doing something like @var = selectall_array(...)

=head3 Notes

	We are assuming that G_S new has been called with a DB argument. We will not be looking for a DB handle in our arguments

=cut

sub selectall_array
{
	my ($self, $stmt, $attr, @bind) = _self_or_default(@_);
	croak "You must initialize G_S with an active DB handle" unless $self->db;
	my $sth = (ref $stmt) ? $stmt : $self->db->prepare($stmt, $attr);
	return () unless $sth;
	$sth->execute(@bind) || return ();
	my @rv = ();
	while (my ($row) = $sth->fetchrow_array)
	{
		push @rv, $row
	}
	return @rv;
}

sub Standard_State($$;$$)
{
###### expect database handle and a state (code or name) value could be empty
###### a state code could be empty
###### a country code could be empty
	my ($self, $db, $state, $state_code, $country) = _self_or_default(@_);
	return unless $db;

###### get some things in order
	$state = uc $state || '';
	$state_code = uc $state_code || '';
	$country = uc $country || '';	

###### initialize our return values				
	my $success = 0;
	my $country_match = 0;
	my @list = ();
	my %list_hash = ();

###### some interim values
	my $code_match = my $state_match = '';
	
	if ($country)
	{
		my $qry = "	SELECT code,
					name
				FROM 	state_codes WHERE country= ?
				ORDER BY name";

		my $sth = $db->prepare($qry);
		$sth->execute($country);
		my ($tmp, @rows) = ();
		while ($tmp = $sth->fetchrow_hashref){ push (@rows, $tmp) }
		$sth->finish;		
		if (@rows)
		{
			$country_match = 1;
			foreach (@rows)
			{
				push (@list, $_->{'code'});
				$list_hash{$_->{'code'}} = $_->{'name'};

				if ( $state_code eq uc $_->{'code'} )
				{
					$code_match = $state_code;
					$success = 1;					
				}
				elsif ( $state eq uc $_->{'code'} )
				{
					$state_match = $_->{'code'};
					$success = 1;
				}
				elsif ( $state eq uc $_->{'name'})
				{
					$state_match = $_->{'code'};
					$success = 1;
				}
			}
		###### now put a padding at the beginning
			unshift(@list, '');
			$list_hash{''} = 'Please Select From This List';
		}
		
	}

###### the code takes precedence since if they choose a different value from the dropdown we want that one
	if ($code_match)
	{
		$state = $code_match;
	}
	elsif ($state_match)
	{
		$state = $state_match;
		$state_code = $state_match;
	}

	return ($state, $state_code, $success, $country_match, \@list, \%list_hash);
}

sub Temp_Hash_ck($$@)
{
	###### we're looking for a DB handle, an IP address and a list of values
	###### we'll check to see if there is already a matching entry in the temp_hash table
	my ($self, $db, $ip, @vals) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;
	croak 'IP address is undefined.' unless $ip;

	my $list = '';
	foreach (@vals){ $list .= $_ }
	$list = md5_hex($list);

	###### let's see if we are already in
	my $sth = $db->prepare("SELECT hash FROM temp_hash WHERE hash= ? AND ip= ?");
	$sth->execute($list, $ip);
	my $rv = $sth->fetchrow_array;
	$sth->finish;
	return $rv;
}

sub Temp_Hash_insert($$@)
{
	###### we're looking for a DB handle, an IP address and a list of values
	###### we'll try inserting them into the temp_hash table
	###### if they've already been inserted we'll fail and can abort a duplicate submission of an application
	my ($self, $db, $ip, @vals) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;
	croak 'IP address is undefined.' unless $ip;

	my $list = '';
	foreach (@vals){ $list .= $_ }
	$list = md5_hex($list);

	my $sth = $db->prepare("INSERT INTO temp_hash (hash, ip) VALUES (?, ?)");
	my $rv = $sth->execute($list, $ip);
	$sth->finish;

	return $rv;	
}

sub Trade_Restriction_ck($$)
{
###### routine to test for trade restrictions on a particular country and return the global error object
###### for such a case	 
###### expecting a DB handle and a country code and an optional template ID to use instead of the default
	my ($self, $db, $cc, $tmpl) = _self_or_default(@_);
	$tmpl = 97 unless $tmpl;
	
	my $sth = $db->prepare("
		SELECT *
		FROM tbl_countrycodes
		WHERE restrictions= TRUE
		AND country_code= ?");
	$sth->execute($cc);
	my $rv = $sth->fetchrow_hashref;
	$sth->finish;
	
	if ($rv)
	{
###### if we have a result, then they are blocked and we'll pull up the global template for return	
		my $tmpl = Get_Object($self->db, $tmpl) || "FAILED to retrieve template $tmpl regarding trade restrictions.< br />\nG_S::Trade_Restrictions_ck";
		return $tmpl;
	}
	return;
}

###### ###### end of public routines ###### ######

sub _conv2
{
	###### we are handling refs to vars
	###### to avoid passing lots of data back and forth unnecessarily
	###### we'll do real vals if necessary
#	$U = Unicode::String->new unless $U;
	my ($type, $list) = @_;
	my @_list= ($list && ref $list eq 'ARRAY') ? @$list : $list;
	my @out = ();

	foreach(@_list)
	{
		###### handle our undefined 'vals'
		if ( (ref && ! defined $$_) || ! defined $_ ){}
		elsif ($type eq 'utf8')
		{
	#		$U->latin1((ref) ? $$_ : $_);
	#		(ref) ? $$_ : $_ = $U->utf8;
		}
		else
		{
	#		$U->utf8((ref) ? $$_ : $_);
	#		(ref) ? $$_ : $_ = $U->latin1;
		}

		push(@out, $_) unless (ref);
	}
	return wantarray ? @out : $out[0];
}

=head2
_get_translation

=head3
Description:

Intended as a private function. Pulls the "correct" translation of a particular cgi_object ID.
The idea is that this will only be called if the cgi_object itself is flagged as a translation key.

=cut

sub _get_translation($$;$)
{
	my ($db, $obj_id, $lang_spec) = @_;
	my ($pk) = ();

	###### if we haven't set this yet (like if a func was called without 'new')
	###### we will take care of it now
	@language_prefs = _initialize_language_prefs() unless @language_prefs;
	
	###### could just load all the translations and pick
	###### but that could potentially be a lot of data in memory for nothing
	###### so we'll select our translation ID and then just pull it
	my $rv = $db->selectall_hashref("
		SELECT language, pk FROM object_translations
		WHERE obj_key= $obj_id AND key_type= 1 AND active= TRUE
	", 'language');

	if ($lang_spec)
	{
		###### if we have received a language specification
		###### we will not rely on browser settings
		$pk = $rv->{$lang_spec}->{'pk'} || $rv->{'en'}->{'pk'};
	}
	else
	{

	###### first loop through the language prefs and see if we have a match
		foreach (@language_prefs)
		{
			$pk = $rv->{$_->[0]}->{'pk'};
			last if $pk;
		}

	###### failing the primary's let's look through the primary portions of the prefs
		unless ($pk)
		{
			foreach (@language_prefs)
			{
				$pk = $rv->{$_->[1]}->{'pk'};
				last if $pk;
			}
		}

	###### if we got to here without a $pk value,
	###### I guess we don't have foreign translations for this object
		$pk ||= $rv->{'en'}->{'pk'};
	}

	###### although there should always be an 'en' translation, that may not be the case
	return unless $pk;

	return $db->selectrow_array(qq|SELECT value, "language" FROM object_translations WHERE pk= $pk|);
}

sub _initialize_language_prefs
{
	###### handle our language prefs var
	my @list = ();
	foreach my $lang( split(/,/, ($ENV{'HTTP_ACCEPT_LANGUAGE'} || '')) )
	{
		###### chop off the "q= " part
		($lang) = split(/;/, $lang);

		###### get the primary part if this is one with subparts
		###### (a dialect) like 'us-EN'
		my ($p) = split(/-/, $lang);
		push (@language_prefs, [($lang, $p)]);
	}

	###### if we still have an empty language_prefs list, we'll set defaults
	@language_prefs = ['en','en'] unless @language_prefs;
}

sub _self_or_default
{
	return @_ if defined($_[0]) && (!ref($_[0])) && ($_[0] eq 'G_S');

	###### I clipped this from CGI.pm
	unless (defined($_[0]) && (ref($_[0]) eq 'G_S'))
	{
		$Q ||= new('G_S');
		unshift(@_, $Q);
	}
	return wantarray ? @_ : $Q;
}
		
sub getProductionServer()
{
    return DHSGlobals::PRODUCTION_SERVER;
}


=head2
buildCreditCardExpirationDate

=head3
Description:

    This subroutine creates an xml document of expiration dates for eight years into the future.
    
  Example:
    <cc_exp>
    <D122008>12/2008</D122008>
    <D012009>01/2009</D012009>
    <D022009>02/2009</D022009>
    </cc_exp>

=head3
Params:

    none

=head3
Returns:

    string

=cut

sub buildCreditCardExpirationDate
{
    my $xml = '';
    my $number_of_loops = 12 * 20 + 1;
    
    my ($year, $month, $day) = Date::Calc::Today();
    my $short_year = $year;
    
    $short_year =~ s/^\w\w//;
    $month = "0$month" if $month < 10;

#
#Here is some logic if we ever start using
#programmer friendly XML.
#
#my $card_expriation_dates = {
#date=>[
#"$month/$year",
#]
#};
#
#for (my $counter = 1; $counter < $number_of_loops; $counter++)
#{
#push @{$card_expriation_dates->{date}}, $month . '/' . $year;
#}
#
#$xml = $XML_Simple->XMLout($card_expriation_dates, RootName => "cc_exp");
#
    $short_year = $year;
    $short_year =~ s/^\w\w//;

    $xml = "<d$month$short_year>$month/$year</d$month$short_year>";
    
    for (my $counter = 1; $counter < $number_of_loops; $counter++)
    {
	($year,$month,$day) = Date::Calc::Add_Delta_YM( $year, $month, $day, 0, 1 );
	$short_year = $year;
	$short_year =~ s/^\w\w//;
	$month = "0$month" if $month < 10;
	$xml .= "<d$month$short_year>$month/$year</d$month$short_year>";
	
    }
    
    return "<cc_exp>$xml</cc_exp>"    
}



1;

__END__

 08/02/01 modified Upline_List to return an empty string in the event of errors
 also made it accept a 'coach level' different than the constant one if needed
 08/15/01 adjusted the auth_ses... routine to send back empty strings instead of NULLs if no key was
 sent to the routine
 09/10/01 added exclusion for whitespace and other extraneous characters in an email address
 11/06/01 added addtional testing for email addresses to allow .info domain
 11/07/01 changed the logic in the upline list generator to REALLY stop it at coach level
 02/11/02 added routine to get the upline coach
 02/25/02 added a routine to get the sponsor
 03/20/02 redefined the global template directory vars
 03/27/02 added a COALESCE to the a_level in the get sponsor routine so that we would return something
 rather than null
 04/02/02 adjusted the get coach routine so that it wouldn't get stuck in an endless loop if it
 got to the top without finding a 'coach'
 made further adjustment to the get coach routine so that it wouldn't skip an immediate 'coach'
 04/04/02 passed the alias var through execute for quoting, also got rid of a warnings error
 when using the session key checker on a null passed
 05/09/02 added a routine to check against the mail_refused_domains table
 05/20/02 fixed some bad return vals in Check_in_downline
 06/14/02 changed the labels of the free member & shopper membertypes
 06/27/02 added a routine to get template and other objects from the DB
 07/26/02 added a routine to get a 'VIP' sponsor (this party may not be the direct sponsor or even the 
 first VIP above)
 08/09/02 removed the exists test in Get_VIP_Sponsor in favor of a simple value test
 08/13/02 added an error handler to Check_in_downline
 09/27/02 added to the values in MEMBER_TYPES
 11/05/02 added some additional TLD's to the email check routine
 11/08/02 changed the Coach level var
 11/19/02 added EXEC_BASE var
 12/06/02 changed 'Affiliate' group to 'Affinity' group
 also added Exporter and exported the global vars
 12/10/02 added error handlers to various routines for NULL required parameters
 12/16/02 revised the Mail_Refused routine to pull explicitly off the blocked table rather than the view
 it was reading off, which included bogus domains - we'll handle those separately
 12/17/02 put the new authen_ses routine into place
 01/29/03 added %URLS
 03/25/03 added the Trade_Restriction_ck routine and reformatted some queries for easier readabililty
 04/10/03 revised the email checker to require [] when the 'domain' is all numeric
 04/24/03 added a generic 'error' page template variable for those cases when a DB connection is not established
 also added a common 'salt' for hashes and removed the reference to the Partner member type
 05/01/03 added the standardized state check
 05/29/03 added functionality in authen_ses_key for the MAF cookie
 06/05/03 added a routine to handle insertions into the temp_hash table
 also revamped Get VIP Sponsor to re-utilize the statement handle rather than recreate it and finish it
 on every recursion of the loop						
 06/25/03 added PreProcess_Input()
 08/08/03 changed the Alias2ID to upper case the passed ID
 08/22/03 added the Country_Has_States routine
 09/05/03 made Check_Email return false on an empty parameter
 also made dupe email check handle aliases
 09/11/03 changed the PreProcess_Input to return NULL
 if the argument passed was NULL instead of NULL or 'false' as in a value of zero
 09/29/03 made the age parameter in Get_VIP_Sponsor optional per the prototype
 10/27/03 updated the Coach level
 11/21/03 updated the Exec level
 12/03/03 made revisions in Get_Coach to look at the a_level view instead of vipcalcs
 also streamlined and optimized the program flow and DBI stuff
 12/31/03 revised the names of members & shoppers
 01/09/04 removed references to vipcalcs and also removed the old Upline_List() routine
 03/08/04 added optional handling for a SQL delimited list of membertypes
 in the dupe checker
 04/20/04 added the ebiz cookie decoder
 05/18/04 tweaked Mail_Refused to return empty if the 'address' argument was empty
 05/20/04 added the BUILDER_LEVEL var
 08/27/04 revised Get_VIP_Sponsor to no longer consider the 'age'
 added the 'new' construct to go to object oriented as well as the _self_or_default
 to add to the different routines
 revised the Get_Object routine to work with translations
 10/27/04 added the UTF8 <-> Latin1 conversion routines
 11/09/04 added a membertype criteria in Get_Coach
 11/11/04 fixed the membertype criteria in Get_Coach
 11/18/04 added Global var reinitialization in the new routine in an attempt to
 fix what appears to be value carry-over in mod_perl
 11/22/04 added a test for the 'active' flag in the get translation routine
 11/24/04 added the wantarray method to the _conv2 routine
 01/14/05 added the Load_Messages routine
 02/25/05 tweaked the SQL in Mail_Refused to match on a primary domain
 even if the host portion was different or not present
 this will deal effectively with derivatives like rr.com which has hosts like
 nc.rr.com, mail.rr.com, etc.
# 06/22/05 added the @TEAM_LEADERS and %GIVECREDIT vars
# 06/27/05 added the Prepare_UTF8 routine
 12/09/05 added Null_to_empty
# 01/25/06 disabled the utf8/asc conversions
# 01/31/06 made the null to empty routine return on an empty invalid arg instead of die
 also added the Hash2XML routine
# 02/07/06 disabled the encode_utf8 portion of Prepare_UTF8
 07/28/06 added the Get_LangPref routine to expose @language_prefs
# added the global $ERR_MSG
# 10-17-07 revised the membership exclusion list in the MembershipDupeCk to match on any membership besides dupes
# 2008-12-30 Added the "$PRODUCTION_SERVER" variable, and the "getProductionServer" subroutine.
# 03/12/09 added the GetPathPart routine
# 08/28/09 added the ParseString routine
# 11/20/09 added the test for "active" in the Get_Object method
 02/(08 & 09)/10 added Create_AuthCustom_Generic_cookie, Create_AuthCustom_Generic_ticket, CoerceLangPref
 04/20/10 commented out the Conv2Asc($) routine as well as removing the use of Unicode::String
# 09/07/10 removed the '_' from the auto-removal in PreProcess_Input()
# 08/23/11 added pod documentation for Get_Object()
 06/21/12 added db() and select_array_of_hashrefs()
 07/18/12 added GetObject()
 09/07/12 made db() default to $main::db if nothing else is available
 09/21/12 added the CGI method
 07/24/13 cosmetic changes
 03/20/14 added the in_array() method
 08/08/14 added the csmLogin and CS_Session modules to make this module portable between old and new servers
 08/21/14 added the ability for GetObject to also pass back the lang pref
 11/07/14 now that migration to the newer server is complete, removed CS_Session
04/02/15
	made Prepare_UTF8() return undef if the "value" argument is undef

02/17/16
	Added Make_Connection_SSL