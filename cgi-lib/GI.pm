package GI;
# deployed Oct. 2009	Bill MacArthur

=head1 GI

A collection of routines for use in utilizing the global Glocal Income look and feel (Glocalization)


=head3 Usage:

	use GI;
	my $GI = GI->new([cgi=>CGI object, db=>DB handle]);
	my $final_content = $GI->wrap({
		title=>'Our Title',
		style=>'div {color:red}' OR $GI->StyleLink('/css/stylish.css'),
		content=>\$actual_content
	});

=cut

sub new
{
	my ($class, $args) = @_;
	my $hr = {};
	foreach (qw/cgi db/){
		$hr->{$_} = $args->{$_} if $args->{$_};
	}
	return bless $hr;
}

=head3 StyleLink

	Pass in a URI to a stylesheet - get back a <link> tag

=cut

sub StyleLink
{
	my ($self, $href) = @_;
	return qq|<link rel="stylesheet" type="text/css" href="$href" />|;
}

=head3 wrap

	Wrap the real content in the global container (Glocalize it)
	We accept these key/value pairs in the hashref argument list
		title => Some textual value that will be placed in both the HTML title tag as well as in the page sub-header
		style => These should be well formed <link> tags pointing to extra stylesheet/s and or a <style></style> block
		script => These should be well formed <script> tags pointing to a source. Alternatively, a block of code could be inserted.
		content => A *reference* to the real content of the page which will be sandwiched in the wrapper
		timestamp => A true value will cause the timestamp to appear in the lower left corner of the content section
		final => The content of this argument will be inserted immediately prior to the closing </body> tag.

=cut
 
sub wrap
{
	# I realize that we will probably end up with a few different variations of glocalization template
	# once the limitations of the 975 px wide layout breaks
	# until then we'll embed the basic template here
	# afterwards, we can make this function backward compatible by accepting a template ID argument
	# and serving up this template as the default
	
	my ($self, $args) = @_;
	foreach (qw/title style script final timestamp/){
		# this keeps out undefined var errors
		$args->{$_} = '' unless defined $args->{$_};
	}

	die "Content is undefined" unless ${$args->{content}};

	return <<DOC;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>$args->{'title'}</title>
<link href="/css/cgi/main.css" rel="stylesheet" type="text/css" />

$args->{style}
$args->{script}
<style type="text/css">
body {
    margin-left: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
    background-color: #FFFFFF;
}
</style>
</head>
<body>
<!-- the following line must remain on -one- line or IE creates gaps between the images -->
<div style="width:975px; margin-right:auto; margin-left:auto; margin-bottom:1em;"><img src="/images/header/csr_interface_header1.png" width="975" height="188" alt=""/><div style="background-image: url(/images/headbar/csr_interface_plain.png); height:52px; text-align:center;"><span style="line-height:1.9; color:white; font-weight: bold; font-size: 17px; font-family: arial,helvetica,sans-serif;">$args->{'title'}</span></div><div style="background-image: url(/images/bg/csr_interface_04.png)">
<div style="width:890px; margin-right:auto; margin-left:auto;">
${$args->{'content'}}
</div>
${\($args->{'timestamp'} ?
   	'<div class="Text_Copy_Right_Green" style="margin: 1em 0pt 0pt 7em; font-size: 10px; font-family: sans-serif;">' .
	scalar localtime() . ' <i>PST</i></div>' : '')}
</div>
<img src="/images/csr_interface_08.png" width="975" height="60" alt="" />
</div>

<p align="center" class="Text_Copy_Right_Green">&copy; 2015 ClubShop Rewards</p>

$args->{'final'}
</body>
</html>
DOC
}

1;
