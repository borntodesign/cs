package MailTools::MassMailer;
use base qw( MailTools );
use MailingUtils;
use XML::Simple;
use XML::local_utils; # on mail2.clubshop.com this line should be "use local_utils;"
use Storable;
use Encode;

=head1 NAME:
MailTools::MassMailer

	This class extents the MailTools class, and is ment for sending many many emails.

=head1 VERSION:

	5

=head1 AUTHOR:

	Keith

=head1 SYNOPSIS:

=head2
HOW TO SET UP MASS MAILINGS

=head3
Process for Creating Content:

	Create an XML document with the standard content
	to be translated.

	Create two XSLT style sheets, one for HTML and
	the other for Text.

	Submit the XML, and XSLT files to the translation
	system.

	The Query that is crafted should always include the
	Members Email Address, Members OID as member_id,
	Members Language Code as lang_code, and the Members
	Member Type. The Members Email Address, and Members
	OID are required for the email footers removal links.
	The language code should use a case statement so if
	one is not defined 'en' is used.  If the "Member Type"
	is omitted the non VIP footer will be used on all
	emails (VIPs should not receive a removal link since
	they are a business partner, and the removal link will
	not work for them).

	When the Translated XML content is merged with the
	Query data the end result will look like this, with
	the children of the "member" node being the key=>value
	data returned from the query.

	<base>
	<member>
		<member_id>123456</member_id>
		<lang_code>Doe</lang_code>
		<emailaddress>someone@someplace.com</emailaddress>
		<membertype></membertype>
	</member>
	<lang_blocks>
	</lang_blocks>
	</base>

	Five Required Parameters
	* Database Connection
	* Query to extract the customized user information
	* XML containing the static content of the emails
	* XSLT for Text Emails
	* XSLT for HTML Emails

	Optional Parameters
	* Subject if one is not set the default
	  (ClubShop Mall :: "Where The World Shops!") will be used
	* From Address if one is not se the default
	  (announcements@clubshop.com) will be used
	* Default Language if one is not set 'en'
	  will be used
	* Log (the absolute path)
	* Default Image (the absolute path)
	* Footers (if these are not set the default footers will
	  be used for the appropriate member type)
		o Text Footer ID
		o HTML Footer ID
		o VIP Text Footer ID
		o VIP HTML Footer ID


=head3
Code Example:

	my %parmeters_to_pass = (
		db=>THE DATABASE OBJECT,
		query_extract_member_information=>THE QUERY,
		xml_configs=>{
			html_xsl_id=>THE OBJECT ID,
			txt_xsl_id=>THE OBJECT ID,
			content_xml_id=>THE OBJECT ID
			footer=>{ (Optional)
				text_id=> (Optional) THE OBJECT ID,
				html_id=> (Optional) THE OBJECT ID,
				vip_text_id=> (Optional) THE OBJECT ID,
				vip_html_id=> (Optional) THE OBJECT ID
			}
		},
		default_image=> (Optional) 'THE/ABSULUTE/PATH/TO/THE/IMAGE',
			     log=> (Optional) 'THE/ABSULUTE/PATH/TO/THE/LOG/FILE/TO/USE',
		from=> (Optional) 'THE-EMAIL-ADDRESS@DOMAIN.COM',
		subject=> (Optional) 'THE SUBJECT TO USE'
	);

	$MailTools_MassMailer->massMailer(\%parmeters_to_pass);

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

	Params: None

	Returns: obj $this


=head2
METHODS

=cut

###############################
#
# This sets the default Subject,
# From address, and instanciates
# the MIME::Lite class into the
# 'Mime_Lite' attribute.
#
###############################

sub __init{

	my $this = shift;

	$this->{mail_headers}->{Subject} = qq(ClubShop Mall :: "Where The World Shops!");
    $this->{mail_headers}->{From} = 'announcements@clubshop.com';
    $this->{version}	= '5';
    $this->{name}		= 'MailTools::MassMailer';

	eval{
		$this->{MIME_Types} = MIME::Types->new();
		$this->{xs} = XML::Simple->new(NoAttr=>1);
		$this->__factories({object=>'Mime_Lite', mail_headers=>$this->{mail_headers}});
		$this->{MailingUtils} = MailingUtils->new();
	};
	if($@)
	{
		
		print 'There was an error instanciating the MassMailer class. ' . $@;
		die $@;
		
	}
	
}

=head3
massMailer

=head4
Description:

	Take a query, compiles the results into an array of hashrefs.
	The translated content is extracted from the translation system.
	The array is then looped through turning the hashref (containing
	user specific data) into XML then combining the translated
	content with the user specific XML.  The XML, is then converted into
	plain text, and HTML, the footers are customized, then added. The
	email is then sent, and in the array of hashrefs an element is
	added stating the message was sucsesfully sent to the mail relay,
	or stating the error(why it wasnt).

	Every 100 loops the method writes the arrray of hashrefs to a log
	file using storable.  If there is some kind of system crash it
	shouldn't take to much scripting to reload the logfile, and
	send the messages that have not gone out yet.

=head4
Prameters:

	hashref
		db		obj		The database object.
		query_extract_member_information	string	The Query to retrieve member information.
		log		string	(optional) The full path, and name of the log to write.
		from		string	(optional) The from address to set in the mail headers.
					 	if one is not set the default will be used.

		subject		string	(optional) The subject to set in the mail headers.  If
						one is not set the "subject" node from the
						XML file will be used, and if one is not set
						in the XML file then the default will be used.
		subject_prefix	string	If this is set it will be placed at the beginning of the subject.
		default_image		string	(optional) The absoulte path of an image.
		default_language	string	(optional) The default language code to use, if it is not
							specified "en" is used.

		xml_configs		hashref
		{
			html_xsl_id	int	The object ID for the HTML Stylesheet,
			txt_xsl_id	int	The object ID for the Text Stylesheet,
			content_xml_id	int	The object ID for the Content,
			footer	hashref (optional) if no footers are set the default will be used.
			{
 				text_id int The object ID for the text footer,
				html_id int The object ID for the HTML footer,
				vip_text_id int (optional) The object ID for the text VIP footer,
				vip_html_id int (optional) The object ID for the HTML VIP footer,
				replace_ps	string	The text to replace the footers P.S. with (ex. P.S.S.),
 			}
		}

=head4
Errors:

	An exception is thrown if an error occurs.

=cut
sub massMailer
{

	my $this = shift;
	my $params = shift;

	die 'db is a required parameter (you must pass the database connection).' 						if(!exists $params->{db} || ! $params->{db});
    die 'query_extract_member_information is a required parameter (you must pass a query to use).' 	if(!exists $params->{query_extract_member_information} || ! $params->{query_extract_member_information});
    die 'xml_configs is a required parameter (you must pass a hashref to use).' 					if(!exists $params->{xml_configs} || ! $params->{xml_configs});

    die 'html_xsl_id is a required parameter (you must pass an integer to use).' 					if(!exists $params->{xml_configs}->{html_xsl_id} || ! $params->{xml_configs}->{html_xsl_id});
    die 'txt_xsl_id is a required parameter (you must pass an integer to use).' 					if(!exists $params->{xml_configs}->{txt_xsl_id} || ! $params->{xml_configs}->{txt_xsl_id});
    die 'content_xml_id is a required parameter (you must pass an integer to use).' 				if(!exists $params->{xml_configs}->{content_xml_id} || ! $params->{xml_configs}->{content_xml_id});

	die 'This is ment to be called as a method of a class not a Function in a Perl Module. You must instanciate this class before calling this method.' if (! exists $this->{MailingUtils} && ! $this->{MailingUtils});
	
	################################################################
	# Object Instanciations, Variables, Arrays, and Hash declaragions
	################################################################

	my %params_to_pass = (db=>$params->{db});
		
	my $DEFAULT_LANGUAGE = (exists $params->{default_language} && $params->{default_language}) ? lc($params->{default_language}) : 'en';
	my @member_info  = ();
	
	$params_to_pass{default_image} = $params->{default_image} if(exists $params->{default_image} && $params->{default_image});


	#############################################
	# If there are no footers to use add the
	# defaults.
	#############################################
	
    if(!exists $params->{xml_configs}->{footer} || ! $params->{xml_configs}->{footer})
    {
		$params->{xml_configs}->{footer} = {
												text_id => 99,
												html_id => 94,
												vip_text_id=>10615,
												vip_html_id=>10616
											}
    } else {

		$params->{xml_configs}->{footer}->{text_id} = 99 if(!exists $params->{xml_configs}->{footer}->{text_id} || ! $params->{xml_configs}->{footer}->{text_id});
		$params->{xml_configs}->{footer}->{html_id} = 94 if(!exists $params->{xml_configs}->{footer}->{html_id} || ! $params->{xml_configs}->{footer}->{html_id});
		$params->{xml_configs}->{footer}->{vip_text_id} = 10615 if(!exists $params->{xml_configs}->{footer}->{vip_text_id} || ! $params->{xml_configs}->{footer}->{vip_text_id});
		$params->{xml_configs}->{footer}->{vip_html_id} = 10616 if(!exists $params->{xml_configs}->{footer}->{vip_html_id} || ! $params->{xml_configs}->{footer}->{vip_html_id});
    }


	$params_to_pass{footer}{text_id} = $params->{xml_configs}->{footer}->{text_id} if(exists $params->{xml_configs}->{footer}->{text_id} && $params->{xml_configs}->{footer}->{text_id});
	$params_to_pass{footer}{html_id} = $params->{xml_configs}->{footer}->{html_id} if(exists $params->{xml_configs}->{footer}->{html_id} && $params->{xml_configs}->{footer}->{html_id});
	$params_to_pass{footer}{vip_text_id} = $params->{xml_configs}->{footer}->{vip_text_id} if(exists $params->{xml_configs}->{footer}->{vip_text_id} && $params->{xml_configs}->{footer}->{vip_text_id});
	$params_to_pass{footer}{vip_html_id} = $params->{xml_configs}->{footer}->{vip_html_id} if(exists $params->{xml_configs}->{footer}->{vip_html_id} && $params->{xml_configs}->{footer}->{vip_html_id});
	
	$params_to_pass{footer}{replace_ps} = $params->{xml_configs}->{footer}->{replace_ps} if(exists $params->{xml_configs}->{footer}->{replace_ps} && $params->{xml_configs}->{footer}->{replace_ps});
	
	$params_to_pass{subject_prefix} = $params->{subject_prefix} if(exists $params->{subject_prefix} && $params->{subject_prefix});
	
	$params_to_pass{subject} = $params->{subject} if(exists $params->{subject} && $params->{subject});
	
	$params_to_pass{from} = $params->{from} if(exists $params->{from} && $params->{from});
	
	###############################
	# Query for the members who
	# have activity in their downline.
	###############################
	my $sth = $params->{db}->prepare($params->{query_extract_member_information});
	$sth->execute();

	##########################################
	# Create an array of hashes that holds the
	# member information for mailing.
	# I put this in place so in the event there
	# is a failure, the array can be loaded up
	# and pick up from where it left off.
	##########################################
	while (my $row = $sth->fetchrow_hashref)
	{
		push @member_info, $row;
	}

	##########################################
	# Get the text email template
	# There is only one XSL file so it will
	# $text_xsl->{en}
	##########################################
	$params_to_pass{text_xsl} = $this->{MailingUtils}->Get_Object($params->{db}, $params->{xml_configs}->{txt_xsl_id}) ||
		die "Failed to load XML content: $params->{xml_configs}->{txt_xsl_id}\n";

	##########################################
	# Get the HTML email template
	##########################################
	$params_to_pass{html_xsl} = $this->{MailingUtils}->Get_Object($params->{db}, $params->{xml_configs}->{html_xsl_id}) ||
		die "Failed to load XML content: $params->{xml_configs}->{html_xsl_id}\n";

	################################################################
	# Here we create hashes of content that is reused (this way the
	# data is cached so we don't need to retrieve it every time).
	# $content_xml hash {
	#						'en' => '<xml>The Stuff</xml>',
	#						'es' => '<xml>El Stuff</xml>'
	#					}
	################################################################
	$params_to_pass{content_xml} = $this->{MailingUtils}->Get_Object($params->{db}, $params->{xml_configs}->{content_xml_id}, '') ||
	    die "Failed to load XML content: $params->{xml_configs}->{content_xml}\n";


	


	#print "Ready to send EMails.\n";

	eval{
		
		$this->loopSend(\%params_to_pass, \@member_info);
	};
	if($@)
	{
		die 'There was a fatal error sending the emails. Error: ' . $@;
	}

	


}

=head3
loopSend

=head4
Description:

	Put the Description Here.

=head4
Prameters:

	params	hashref	{
						db	obj	The database object,
						content_xml	hashref
						{
							en	string	The english content,
							es	string	The spanish content,
							etc, etc, etc
						},
						text_xsl	hashref
						{
							en	string	The XSL document for plain text emails
						},
						html_xsl	hashref (optional)
						{
							en	string	The XSL document for HTML emails
						},
						from	string or hashref	(optional) If this is a hashref it needs to be indexed by a language code,
						subject	string or hashref	(optional) If this is a hashref it needs to be indexed by a language code,	
						subject_prefix	string	(optional) If this is set it will set this at the beginning of the subject,
						footer	hashref	(optional)
						{
								text_id	int	(optional)
								html_id	int	(optional)
								vip_text_id	int	(optional)
								vip_html_id	int	(optional)
								replace_ps	string (optional)
						},
						default_image (optional)	string	The image for the email,
						log	string	(optional) The absolute path to the log to create.
		
	}
	
	member_info	arrayref of hashrefs	The information that is needed to send the emails.
	[
		{
			member.oid AS member_id	int	Needed for the default remove me link on the footer
			member.emailaddress	string	The email address to send to
			member.membertype	string	Needed to determin which footer to incluce
			member.alias	string	Needed for the default remove me link on the footer
			member.language_pref AS lang_code	string	Required to determin which language to use, the default will be english.
			member.cc	string	(Optional) The email address to CC
			member.bcc	string	(Optional) The email address to BCC
		}
	]

=head4
Errors:

	An exception is thrown if a fatal error occurs.

=cut
sub loopSend
{
	
	my $this = shift;
	my $params = shift;
	my $member_info = shift;
	

	
	die 'The params parameter must be defined, and it must be a hashref. ' if (! $params);
	die 'The content_xml parameter must be defined, and it must be a hashref. ' if (! exists $params->{content_xml} && ! $params->{content_xml});
	die 'The text_xsl->{en} parameter must be defined. ' if (! exists $params->{text_xsl}->{en} && ! $params->{text_xsl}->{en});
	#die 'The params parameter must be defined, and it must be a hashref. ' if (! exists $params->{html_xsl}->{en} && ! $params->{html_xsl}->{en});
	die 'The member_info parameter must be defined, and it must be an arrayref. ' if (! $member_info);

	my ($DAY, $MONTH, $YEAR) = (localtime)[3,4,5];
	$YEAR += 1900;
	
	my $DEFAULT_IMAGE_FOR_HTML_EMAIL = (exists $params->{default_image} && $params->{default_image}) ? $params->{default_image} : undef;
		
	###############################################
	# If no footer IDs are set, set the standard.	
	###############################################
	$params->{footer}->{text_id} = 99 if(!exists $params->{footer}->{text_id} || ! $params->{footer}->{text_id});
	$params->{footer}->{html_id} = 94 if(!exists $params->{footer}->{html_id} || ! $params->{footer}->{html_id});
	$params->{footer}->{vip_text_id} = 10615 if(!exists $params->{footer}->{vip_text_id} || ! $params->{footer}->{vip_text_id});
	$params->{footer}->{vip_html_id} = 10616 if(!exists $params->{footer}->{vip_html_id} || ! $params->{footer}->{vip_html_id});
	################################################################
	# Here we create hashes of content that is reused (this way the
	# data is cached so we don't need to retrieve it every time).
	#
	# $footer_text hash {
	#						'en' => '<xml>The Stuff</xml>',
	#						'es' => '<xml>El Stuff</xml>'
	#					}
	#
	# $footer_html hash {
	#						'en' => '<xml>The Stuff</xml>',
	#						'es' => '<xml>El Stuff</xml>'
	#					}
	################################################################
	my $footer_text = $this->{MailingUtils}->Get_Object($params->{db}, $params->{footer}->{text_id}, '') ||
	    die "Failed to load XML content: $params->{footer}->{text_id}\n";

	my $footer_html = $this->{MailingUtils}->Get_Object($params->{db}, $params->{footer}->{html_id}, '') ||
	    die "Failed to load XML content: $params->{footer}->{html_id}\n";

#	use Data::Dumper;

#	print Dumper($footer_html);
#	print "\n\n";
#	print Dumper($footer_text);
#	print "\n\n";


	my $footer_vip_text = $this->{MailingUtils}->Get_Object($params->{db}, $params->{footer}->{vip_text_id}, '') ||
	    die "Failed to load XML content: $params->{footer}->{text_id}\n";

	my $footer_vip_html = $this->{MailingUtils}->Get_Object($params->{db}, $params->{footer}->{vip_html_id}, '') ||
	    die "Failed to load XML content: $params->{footer}->{html_id}\n";
	    
	if (exists $params->{footer}->{replace_ps} && $params->{footer}->{replace_ps})
	{
		
		foreach my $language (keys %$footer_text)
		{
			$footer_text->{$language} =~ s/P\.S\.\s-/$params->{footer}->{replace_ps} -/;
		}
		
		foreach my $language (keys %$footer_html)
		{
			$footer_html->{$language} =~ s/P\.S\.\s-/$params->{footer}->{replace_ps} -/;
		}
		
		foreach my $language (keys %$footer_vip_text)
		{
			$footer_vip_text->{$language} =~ s/P\.S\.\s-/$params->{footer}->{replace_ps} -/;
		}
		
		foreach my $language (keys %$footer_vip_html)
		{
			$footer_vip_html->{$language} =~ s/P\.S\.\s-/$params->{footer}->{replace_ps} -/;
		}
		
				
	}
	
	my $send_mail_counter = 0;
	my $mail_pipped_to_the_mail_server = 0;
	
	
	#print 'Number of emails to send: ' . scalar(@$member_info) . "\n";
	
	################################################################
	# Loop through the "@member_info" array, set the template
	# for the members language preference (default to english if
	# the users prefered language can not be detected), send the email
	# and using the "Storable" library print to a file the
	# "@member_info" array.  If the script, or system fails
	# you should be able to recover the file, and with a little scripting
	# continue where the script left off.
	################################################################
	for(my $counter = 0; $counter < scalar(@$member_info); $counter++)
	{

		next if(! defined $member_info->[$counter] || ! exists $member_info->[$counter]->{emailaddress} || ! $member_info->[$counter]->{emailaddress});


		$member_info->[$counter]->{lang_code} = 'en' if (! exists $member_info->[$counter]->{lang_code} || ! $member_info->[$counter]->{lang_code});
		
		################################################################
		# In the event there is extra space in the language code
		# strip it out, so it doesn't cause problems.
		################################################################
		$member_info->[$counter]->{lang_code} =~ s/\s+//g;


		################################################################
		# If there is no content for a users prefered language default
		# to english.
		################################################################
		if(! exists $params->{content_xml}->{$member_info->[$counter]->{lang_code}})
		{
		    $params->{content_xml}->{$member_info->[$counter]->{lang_code}} = $params->{content_xml}->{en};
		}


	#    print "fetch the text footer xml file.\n";
		################################################################
		# If there is no footer for a users prefered language default
		# to english.
		################################################################
		if(! exists $footer_text->{$member_info->[$counter]->{lang_code}})
		{
		    $footer_text->{$member_info->[$counter]->{lang_code}} = $footer_text->{en};
		}

		################################################################
		# If there is no footer for a users prefered language default
		# to english.
		################################################################
		if(! exists $footer_vip_text->{$member_info->[$counter]->{lang_code}})
		{
		    $footer_vip_text->{$member_info->[$counter]->{lang_code}} = $footer_vip_text->{en};
		}

		################################################################
		# If there is no HTML footer for a users prefered language default
		# to english.
		################################################################
	#    print "fetch the HTML footer xml file\n";
		if(! exists $footer_html->{$member_info->[$counter]->{lang_code}})
		{
		    $footer_html->{$member_info->[$counter]->{lang_code}} = $footer_html->{en};
		}

		################################################################
		# If there is no HTML footer for a users prefered language default
		# to english.
		################################################################
	#    print "fetch the HTML footer xml file\n";
		if(! exists $footer_vip_html->{$member_info->[$counter]->{lang_code}})
		{
		    $footer_vip_html->{$member_info->[$counter]->{lang_code}} = $footer_vip_html->{en};
		}

	#	print Dumper($footer_html) . "\n";

		################################################################
		# Reset the footers for this user
		# The VIP members get a special footer, since they  are in
		# business with us they don't get a removal  link.  VIPs
		# need to login, and adjust their preferences.
		################################################################
		my $customized_text_footer = (exists $member_info->[$counter]->{membertype} && lc($member_info->[$counter]->{membertype}) eq 'v') ? $footer_vip_text->{$member_info->[$counter]->{lang_code}}:$footer_text->{$member_info->[$counter]->{lang_code}};
		my $customized_html_footer = (exists $member_info->[$counter]->{membertype} && lc($member_info->[$counter]->{membertype}) eq 'v') ? $footer_vip_html->{$member_info->[$counter]->{lang_code}}:$footer_html->{$member_info->[$counter]->{lang_code}};

		################################################################
		# Modify footer templates with customized user data..
		################################################################
		$customized_text_footer =~ s/%%oid%%/$member_info->[$counter]->{member_id}/;
		$customized_text_footer =~ s/%%emailaddress%%/$member_info->[$counter]->{emailaddress}/;

		$customized_html_footer =~ s/%%oid%%/$member_info->[$counter]->{member_id}/;
		$customized_html_footer =~ s/%%emailaddress%%/$member_info->[$counter]->{emailaddress}/;

	    #print "create the member_xml stuff.\n";
		################################################################
		# Create the XML nodes from the "Member Information" hash.
		################################################################
		
		foreach (keys %{$member_info->[$counter]})
		{
			$member_info->[$counter]->{$_} = '' if ! defined $member_info->[$counter]->{$_};
		}
		
		my $member_xml = $this->{xs}->XMLout($member_info->[$counter], RootName=>'member');
		
		################################################################
		# Create a hash from the "$content_xml" for setting the subject
		# in the email.
		################################################################
		my $simple_xml_data = $this->{xs}->XMLin($params->{content_xml}->{$member_info->[$counter]->{lang_code}}, ForceArray => 1);

		#XMLin seems to bugger up our UTF8 when it's converting an XML document into a hashref arrayref.
		foreach (keys %{$simple_xml_data})
		{
		    $simple_xml_data->{$_}->[0] = Encode::encode_utf8($simple_xml_data->{$_}->[0]) if Encode::is_utf8($simple_xml_data->{$_}->[0]);
		}

		###############################
		# Combine the applicable
		# pieces of the XML document.
		###############################
		my $xml = <<EOT;
			<base>
				$member_xml
				$params->{content_xml}->{$member_info->[$counter]->{lang_code}}
			</base>
EOT


		###############################
		# Create the text email
		###############################
		my $content_text = XML::local_utils::xslt($params->{text_xsl}->{'en'}, $xml);

		###############################
		# Create the HTML email
		###############################
		my $content_html = XML::local_utils::xslt($params->{html_xsl}->{'en'}, $xml) if (exists $params->{html_xsl}->{'en'});

		################################################################
		# Modify content to include the footer.
		################################################################
		$content_text =~ s/%%FOOTER%%/$customized_text_footer/;
		
		$content_html =~ s/%%FOOTER%%/$customized_html_footer/ if (defined $content_html && $content_html);

		################################################################
		# Set the default image for the header.
		################################################################
		my $image_to_email = {id=>'clubshop.gif', type=>'image/gif'};

		################################################################
		# Check to see if the user has a header image in their language,
		# and it is readable by this program.  If it doesn't exist,
		# check to see if the default header image exists, and is available.
		# If it isn't available undef is set.
		################################################################
		if($DEFAULT_IMAGE_FOR_HTML_EMAIL)
		{
			if (-f "$DEFAULT_IMAGE_FOR_HTML_EMAIL.$member_info->[$counter]->{lang_code}" && -R "$DEFAULT_IMAGE_FOR_HTML_EMAIL.$member_info->[$counter]->{lang_code}")
			{
				$image_to_email->{path} = "$DEFAULT_IMAGE_FOR_HTML_EMAIL.$member_info->[$counter]->{lang_code}";
			} elsif (-f "$DEFAULT_IMAGE_FOR_HTML_EMAIL.gif" && -R "$DEFAULT_IMAGE_FOR_HTML_EMAIL.gif")
			{
				$image_to_email->{path} = "$DEFAULT_IMAGE_FOR_HTML_EMAIL.gif";
			} else {
				$image_to_email = undef;
			}
		} else {
			$image_to_email = undef;
		}

		################################################################
		# Build the hash to sent to the "sendMultipPartEmail" method.
		#
		# No reason to check to see if the email address exists
		# that is done at the begining of the for loop.
		################################################################
		my $email_hash = {};
		
		if(defined $member_info->[$counter]->{firstname} && defined $member_info->[$counter]->{lastname} && $member_info->[$counter]->{firstname} && $member_info->[$counter]->{lastname})
		{
			$email_hash->{to} = $member_info->[$counter]->{firstname} . ' ' . $member_info->[$counter]->{lastname} . ' <' . $member_info->[$counter]->{emailaddress} . '>';
		} else {
			$email_hash->{to} = $member_info->[$counter]->{emailaddress};
		}
		
		#TODO: we may want to do some thing here for automatically formatting the CC address.
		$email_hash->{cc} = $member_info->[$counter]->{cc} if $member_info->[$counter]->{cc};

		#TODO: we may want to do some thing here for automatically formatting the BCC address.
		$email_hash->{bcc} = $member_info->[$counter]->{bcc} if $member_info->[$counter]->{bcc};
				
		#$member_info->[$counter]->{emailaddress}
		#'general@dhs-club.com'
		#$email_hash->{to} = $member_info->[$counter]->{firstname} . ' ' . $member_info->[$counter]->{lastname} . ' <general@dhs-club.com>';
		
		$email_hash->{text} = $content_text if ($content_text);
		
		if (defined $content_html && $content_html)
		{
			$email_hash->{html}->{data} = $content_html;
			
			$email_hash->{html}->{attatchments} = [$image_to_email] if ($image_to_email);
			
		}
		
		
		if(exists $params->{subject} && $params->{subject})
		{
			
			if(ref($params->{subject}) eq 'HASH')
			{
				$email_hash->{subject} = (exists $params->{subject}->{$member_info->[$counter]->{lang_code}} && $params->{subject}->{$member_info->[$counter]->{lang_code}} ) ? $params->{subject}->{$member_info->[$counter]->{lang_code}}: $params->{subject}->{en};	
			} else {
				$email_hash->{subject} = $params->{subject};				
			}
		}
		elsif(exists $simple_xml_data->{subject} && $simple_xml_data->{subject}->[0])
		{
			$email_hash->{subject} = $simple_xml_data->{subject}->[0];
		}

		$email_hash->{subject} = $params->{subject_prefix} . $email_hash->{subject} if(exists $params->{subject_prefix} && $params->{subject_prefix});
		
			
		#############################################################
		# Loop through the member information, and replace the template
		# tags with the user information.
		#############################################################
		foreach my $keys (keys %{$member_info->[$counter]})
		{
			last if !$email_hash->{subject};

			$upercase_key = uc($keys);	
			$email_hash->{subject} =~ s/%%$upercase_key%%/$member_info->[$counter]->{$keys}/;
			
			$email_hash->{html}->{data} =~ s/%%$upercase_key%%/$member_info->[$counter]->{$keys}/g if (exists $email_hash->{html}->{data} && $email_hash->{html}->{data});
			
			$email_hash->{text} =~ s/%%$upercase_key%%/$member_info->[$counter]->{$keys}/g if (exists $email_hash->{text} && $email_hash->{text});
			
		}

		if(exists $params->{from} && $params->{from})
		{
			if(ref($params->{from}) eq 'HASH')
			{
				$email_hash->{from} = (exists $params->{from}->{$member_info->[$counter]->{lang_code}} && $params->{from}->{$member_info->[$counter]->{lang_code}} ) ? $params->{from}->{$member_info->[$counter]->{lang_code}}: $params->{from}->{en};				
			} else {
				$email_hash->{from} = $params->{from};	
			}
		}

		eval{
			
			$this->sendMultiPartEmail($email_hash);
			###################################################
			# If the email is sent without triggering exceptions
			# modify the members member_info[members_index] hash
			# to mark the email as being sent.
			###################################################
			if( ! exists $member_info->[$counter]->{sent} || $member_info->[$counter]->{sent} !~ /^\d+$/)
			{
				$member_info->[$counter]->{sent} = 1;
			} else {
				$member_info->[$counter]->{sent}++;
			}
			
			
			$mail_pipped_to_the_mail_server++;
		};
		if($@)
		{
			$member_info->[$counter]->{send_error} = $@;
			# print $@ . "\n";
		}

		$send_mail_counter++;

		###################################################
		# Write the current state of the "@member_info"
		# array to a file using the Storable Module.
		# In the event the script dies halfway through
		# a run, or a system crash the process can begin
		# again after a little scripting.  This is for CMA.
		###################################################


		if($send_mail_counter >= 100)
		{
			#print "\nPausing for five seconds\n";

			if(exists $params->{log} && $params->{log})
			{
			    eval{
					store [@member_info], $params->{log} . '_' . $MONTH . '_' . $DAY . '_' . $YEAR . '.log' if (exists $params->{log} && $params->{log});
			    };
			    if($@)
			    {
					print "An error occured writing to the log. Error: " . $@;
			    }
			}

			sleep (5);
			$send_mail_counter = 1;
		}

	}

	if(exists $params->{log} && $params->{log})
	{
		eval{
		    store [@member_info], $params->{log} . '_' . $MONTH . '_' . $DAY . '_' . $YEAR . '.log' if (exists $params->{log} && $params->{log});
		};
		if($@)
		{
		    print "The script finished its run but, an error occured writing to the log. Error: " . $@;
		}
	}
	
	$params = undef;
	$member_info = undef;
	
	#print "Total number of emails sent: $mail_pipped_to_the_mail_server\n";
	
}

1;

__END__

=head1
SEE ALSO

L<MailTools>

=head1
CHANGE LOG

	10/15/2008	Keith	Change where the MailingUtils class 
						is instanciated, it is now instanciated 
						in the __init method, instead of the 
						massMailer method.

	10/15/2008	Keith	Created the loopSend method.
	
	10/15/2008	Keith	In the loopSend method I added a check 
						for the members language, and made the 
						default english if one is not specified.

	06/19/2009	Keith	Added the ability to CC, and BCC emails.

	12/10/2009	Keith	Added the ability to CC, and BCC emails.

=head1
TODO
	
	Add a private attribute for the database object, and add 
	a public getDB method that if the private database object 
	has not been instanciated, instanciate it, then pass 
	back the object.

=cut
