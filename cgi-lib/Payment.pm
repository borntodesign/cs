package Payment;

=head1 NAME:
ClassName

	Brief description of the class

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	Verbose Description of the class, including how to use it without code examples.

=head2
Code Example

	Code Example Goes Here

=head1
PREREQUISITS:

	Other classes, or packages that are used

=cut

use strict;

=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	$attribute_name	string	What this attributes purpose is.

=head4
Returns:

	obj $this

=cut

sub new
{
    my $class			= shift;
    my $this			= {};
    
    $this->{db} = shift;
    die 'The db Parameter is required to instanciate the Coop Class. ' if ! defined $this->{db};
    
    bless($this, $class);

    $this->__init();
    return $this;
}

=head2
METHODS

=head2
PRIVATE

=head3
__init

=head4
Description:

	Description of what is set in __init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub __init
{

	my $this = shift;

    $this->{_version}	= '0.1';
    $this->{_name}		= 'Payment';
	#return $something_if_there_is_something.
}


=head2
PUBLIC

=head3
determinPaymentType

=head4
Description:

	Determin the payment type for this member.  Pay Points, Credit Card, or none.
	First we check to see what the members Pay Point account balance will cover the charge,
	Then we check for a Credit Card on file,
	If either of those arent an option no payment type is available.
	

=head4
Params:

	member_id	int		The member id of the VIP who placed the order.

=head4
Returns:

	hash	{
				pay_type	string	The payment type to use "PP" (Pay Points) or "CC" (Credit Card),
				error		string	
									incomplete_credit_card_information: The members did not have enough pay point to cover the price and their credit card information is incomplete, 
									no_payment_information_is_available: The member did not have enough pay points to cover the price and no 'MOP' was found, 
									program_error: There was an error determining the payment method to use (most likely an SQL error).
									
			}

=cut

sub determinPaymentType
{
	my $this = shift;
	my $id = shift;
	my $price = shift;
	
	my %return = (pay_type =>'', error=>'');
	
	eval{
		
		if (($this->{db}->selectrow_array("SELECT account_balance FROM soa_account_balance_payable WHERE id= $id") || 0 ) >= $price)
		{
			$return{pay_type} = 'PP';
		} else {
			
			my @fieldnames = qw/	acct_holder
						address
						city
						state
						postal_code
						country
						cc_number
						card_code
						cc_exp/;
			
			my $fieldlist = join ',', @fieldnames;
			
			# Get the member's CC info.
			my $values_hash = $this->{db}->selectrow_hashref("
									SELECT 
										$fieldlist
									FROM
										mop
									WHERE
										id = $id
								");
			
			
			if ($values_hash && $values_hash->{payment_method} eq 'CC')
			{
				# Check required fields as necessary.
				foreach ( @fieldnames )
				{
					# All fields must have info.
					if ( ! defined $values_hash->{$_} || $values_hash->{$_} eq '' )
					{
						$return{pay_type} = '';
						$return{error} = 'incomplete_credit_card_information';
						last;
					}
					$return{pay_type} = $values_hash->{payment_method};
				}		
			} else {
				$return{error} = 'no_payment_information_is_available';
			}
		}
	};
	if($@)
	{
		$return{pay_type} = '';
		$return{error} = 'program_error';
		warn 'An error occured in Coop::determinPaymentType.  Error: ' . $@;
	}
	
	return %return;
}


=head3
retrievePaymentInformation

=head4
Description:

	Determin the payment type for this member.  Pay Points, Credit Card, or none.
	First we check to see what the members Pay Point account balance will cover the charge,
	Then we check for a Credit Card on file,
	If either of those arent an option no payment type is available.
	

=head4
Params:

	member_id	int		The member id of the VIP who placed the order.

=head4
Returns:

	hash	{
				payment_information	hashref	
											payment_type	The payment type to use "PP" (Pay Points) or "CC" (Credit Card),
											
				error		string	
									incomplete_credit_card_information: The members did not have enough pay point to cover the price and their credit card information is incomplete, 
									no_payment_information_is_available: The member did not have enough pay points to cover the price and no 'MOP' was found, 
									program_error: There was an error determining the payment method to use (most likely an SQL error).
									
			}

=cut

sub retrievePaymentInformation
{
	my $this = shift;
	my $id = shift;
	my $price = shift;
	
	my %return = (payment_information =>{pay_type=>''}, error=>'');
	
	eval{
		
		if (($this->{db}->selectrow_array("SELECT account_balance FROM soa_account_balance_payable WHERE id= $id") || 0 ) >= $price)
		{
			$return{payment_information}->{pay_type} = 'PP';
		} else {
			
			my @fieldnames = qw/	acct_holder
						address
						city
						state
						postal_code
						country
						cc_number
						card_code
						cc_exp/;
			
			my $fieldlist = join ',', @fieldnames;
			
			# Get the member's CC info.
			my $values_hash = $this->{db}->selectrow_hashref("
									SELECT 
										$fieldlist
									FROM
										mop
									WHERE
										id = $id
								");
			
			
			if ($values_hash && $values_hash->{payment_type} eq 'CC')
			{
				$return{payment_information}->{pay_type} = $values_hash->{payment_type};
				
				# Check required fields as necessary.
				foreach ( @fieldnames )
				{
					# All fields must have info.
					if ( ! defined $values_hash->{$_} || $values_hash->{$_} eq '' )
					{
						$return{payment_information}->{pay_type} = '';
						$return{error} = 'incomplete_credit_card_information';
						last;
					}
					
					$return{payment_information}->{$_} = $values_hash->{$_};
					
				}
				
				
				
				
				
						
			} else {
				$return{error} = 'no_payment_information_is_available';
			}
		}
	};
	if($@)
	{
		$return{payment_information}->{pay_type} = '';
		$return{error} = 'program_error';
		warn 'An error occured in Coop::determinPaymentType.  Error: ' . $@;
	}
	
	return %return;
}

=head3
getPaymentMethods

=head4
Description:

	This retrieves the Active Payment Methods we accept, as defined by the "payment_methods" table.
	

=head4
Params:

	None

=head4
Returns:

	hashref	
				{
					CC => 'Credit Card',
					CA => 'Club Account',
					etc.
				}

=cut

sub getPaymentMethods
{
	
	my $this = shift;
	
	
	return $this->{_payment_method} if (scalar (keys %{$this->{_payment_method}}));
	
	my $sth = $this->{db}->prepare('SELECT * FROM payment_methods WHERE active=TRUE');
	
	$sth->execute();
	
	
	while (my $rows = $sth->fetchrow_hashref())
	{
		$this->{_payment_method}->{$rows->{method}} = $rows->{description};
	}
	
	return $this->{_payment_method};
	
}

1;

__END__

=head1
SEE ALSO

L<LinkedPackage>, UnlinkedPackage

=head1
CHANGE LOG

	Date	What was changed

=cut

