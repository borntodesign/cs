package Locations;

=head1 NAME:
Locations

	General descripton of the Class.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	Verbose Description of the class, including how to use it without code examples.

=head2
Code Example

	Code Example Goes Here

=head1
PREREQUISITS:

	Other classes, or packages that are used

=cut

use strict;


=head1 DESCRIPTION

=head2 CONSTRUCTOR

=head3
new

=head4
Description:

	Describe the constructor.

=head4
Params:

	$attribute_name	string	What this attributes purpose is.

=head4
Returns:

	obj $self

=cut

sub new
{
    my $class			= shift;
    my $self = {};

    $self->{db} = shift;
    bless($self, $class);
    $self->_init();
    return $self;
}


=head2
METHODS

=head2
PRIVATE

=head3
_init

=head4
Description:

	Description of what is set in __init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $self = shift;

    $self->{version}	= '0.1';
    $self->{name}		= 'Locations';
    $self->{_provinces_by_country} => {};
    #$self->{_countries_by_iso_codes} => {}; No need to define this, it will be defined when it is needed.

}


=head3
_updateCache

=head4
Description:

	

=head4
Parameters:

	country_code	STRING	The country code, two letter ISO code.

=head4
Return:

	string	NULL	if all goes well, an exception will be thrown on error.

=cut

sub _updateCache
{
	my $self = shift;
	my $country_code = shift;
	
	if (! $country_code)
	{
		die "The 'country_code' is a required parameter, but it wasn't supplied.";
	}
	
	$country_code = uc($country_code);
	
	my $state_query ='

			SELECT 
				lower(code) AS lcode, 
				code as ucode, 
				name
			FROM 
				state_codes 
			WHERE 
				country = ?
			ORDER BY
				code

';

	
	eval{
		
		delete $self->{_provinces_by_country}->{$country_code} if defined $self->{_provinces_by_country}->{$country_code};
		
		my $sth = $self->{db}->prepare($state_query);
		$sth->execute($country_code);
		while(my $rows = $sth->fetchrow_hashref)
		{
			$self->{_provinces_by_country}->{$country_code}->{$rows->{ucode}} = 
			{
				name => $rows->{name},
				lcode => $rows->{lcode},
				ucode => $rows->{ucode}
			};
		}
	};
	if ($@)
	{
		die "The cache was not updated due to an error. Query: $state_query -- country_code: $country_code -- " .  $@;
	}

	return;
}


=head3
_updateCountryCache

=head4
Description:

	

=head4
Parameters:

	restrictions
	STRING
	UNDEF	All countries even ones with restrictions.
	NO_RESTRICTIONS All countries without restrictions
	RESTRICTIONS All countries with restrictions

=head4
Return:

	NULL	if all goes well, an exception will be thrown on error.

=cut

sub _updateCountryCache
{
	my $self = shift;
	my $where = '';
	
	my $country_query =<<EOT;

SELECT
	country_name,
	country_code,
	restrictions,
	has_postal_codes,
	gmt
FROM
	tbl_countrycodes
ORDER BY
	country_name
;

EOT


	
	eval{
		
		delete $self->{_countries_by_iso_codes} if defined $self->{_countries_by_iso_codes};
		
		my $sth = $self->{db}->prepare($country_query);
		$sth->execute();
		while(my $rows = $sth->fetchrow_hashref)
		{
			$self->{_countries_by_iso_codes}->{$rows->{country_code}} = 
			{
				name => $rows->{country_name},
				iso => $rows->{country_code},
				restrictions => $rows->{restrictions},
				has_postal_codes => $rows->{has_postal_codes},
				gmt => $rows->{gmt},
			};
		}
	};
	if ($@)
	{
		die "Locations::_updateCountryCache The cache was not updated due to an error. Query: $country_query -- " .  $@;
	}

	return;
}


=head3
_updateCache

=head4
Description:

	

=head4
Parameters:

	country_code	STRING	The country code, two letter ISO code.

=head4
Return:

	string	NULL	if all goes well, an exception will be thrown on error.

=cut

sub _formatXMLProvinceList
{
	my $self = shift;
	my $country_code = shift;
	my $node_name = shift;
	
	my $states_xml = '';
	
	return if ! $country_code || ! defined $self->{_provinces_by_country}->{$country_code};
	
	if ($self->{_provinces_by_country}->{$country_code})
	{
		my $state_options = '';
		
		foreach (keys %{$self->{_provinces_by_country}->{$country_code}})
		{
			$state_options .= qq(<$self->{_provinces_by_country}->{$country_code}{$_}->{lcode} val="$self->{_provinces_by_country}->{$country_code}{$_}->{ucode}">$self->{_provinces_by_country}->{$country_code}{$_}->{name}</$self->{_provinces_by_country}->{$country_code}{$_}->{lcode}>\n);
		}
		
		$states_xml .= $node_name ? "<$node_name> \n $state_options \n </$node_name> \n" : "<state> \n $state_options \n </state> \n ";
		
	}
	
	return $states_xml;
}


=head2
PUBLIC

=head3
getMemberInformationByMemberID

=head4
Description:

	Get the users information by the Member ID and assign
	the data to the cache

=head4
Params:

	hash
		country_code	string	Two character ISO Country Code
		force		bool	(Optional) Set to true if you want 
							the latest information from the database

=head4
Returns:

	string, hashref, or null	If the field parameter is specified a string 
						will be returned.  A hashref containing the 
						member record is returned if no field parameter 
						is set, if there is no data null is returned.

=cut

sub getProvincesByCountryCode
{
	my $self = shift;
	my $params = shift;

	$params->{force} = 0 if !defined $params->{force};

	return if (! exists $params->{country_code} || ! $params->{country_code});

	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	if ($self->{_provinces_by_country}->{$params->{country_code}} && !$params->{force})
	{
		return $self->{_provinces_by_country}->{$params->{country_code}};
	}

	# If the member cache does not exist, or the data needs to be refreshed
	if(! $self->_updateCache($params->{country_code}))
	{
		return $self->{_provinces_by_country}->{$params->{country_code}};
				
	}

	return;

}


=head3
getMemberInformationByMemberID

=head4
Description:

	Get the users information by the Member ID and assign
	the data to the cache

=head4
Params:

	hash
		country_code	string	Two character ISO Country Code
		force		bool	(Optional) Set to true if you want 
							the latest information from the database
		node_name		string	(Optional)

=head4
Returns:

	string, hashref, or null	If the field parameter is specified a string 
						will be returned.  A hashref containing the 
						member record is returned if no field parameter 
						is set, if there is no data null is returned.

=cut

sub getXMLProvincesByCountryCode
{
	my $self = shift;
	my $params = shift;

	$params->{force} = 0 if !defined $params->{force};

	return if (! exists $params->{country_code} || ! $params->{country_code});

	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	if ($self->{_provinces_by_country}->{$params->{country_code}} && !$params->{force})
	{
		return $self->_formatXMLProvinceList($params->{country_code}, $params->{node_name});
	}

	# If the member cache does not exist, or the data needs to be refreshed
	if(! $self->_updateCache($params->{country_code}))
	{
		return $self->_formatXMLProvinceList($params->{country_code}, $params->{node_name});
				
	}

	return;

}


sub getName
{
	my $self = shift;
	return $self->{name};
	
}

=head3
getCountries

=head4
Description:

	

=head4
Params:

	{
		force
		BOOL
		Set to recreate the cache, and return a fresh list.
	}


=head4
Returns:

	hashref

=head4
TODO

	Add a restrictions parameter, where you can choose all countries restricted countries, or untrestricted countries.

=cut

sub getCountries
{
	my $self = shift;
	my $params = shift;

	$params->{force} = 0 if ! defined $params->{force};

	# If the users cache already exists, and the
	# latest info does not need to be retrieved
	# return the cached data.
	if ($self->{_countries_by_iso_codes} && !$params->{force})
	{
		return $self->{_countries_by_iso_codes};
	}

	if(! $self->_updateCountryCache())
	{
		return $self->{_countries_by_iso_codes};
				
	}

	return;

}



1;

__END__

=head1
SEE ALSO

L<LinkedPackage>, UnlinkedPackage

=head1
CHANGE LOG

	Date	What was changed

=cut

