package MailingUtils;
###### created 08/26/04	Bill MacArthur
###### last modified: 06/03/13	Bill MacArthur

=head1 NAME

MailingUtils - some routines for use with background mailing scripts

=head1 SYNOPSIS
			
	use MailUtils;
	$mu = new MailUtils;

	# get all translations for a particular object as a hashref {language=>'value'}
	my $object = $mu->Get_Object($dbh, <object id>);

	# get a specific translation (will get english by default as well)
	my $object = $mu->Get_Object(
		$dbh,
		<object id>,
		<language pref as scalar or within a hash ref>
	);

	# calls to Get_Object will cause a database lookup

	# to get a specific translation from a previously retrieved set of translations
	# this routine will return the english version if the preferred one isn't available
	my $translation_specific_object = $mu->Select_Translation(
		$object,
		<language pref as scalar or within a hash ref>
	)

	# since any of these routines can return undef, a valid return should be checked
	# after a call to Get_Object at least

	The Mail routine handles sending of mailpieces that are ready for delivery.
	There is flexibility in the arguments. They should be passed as key/value pairs.
	The txt,html,composite values, if used, must themselves be scalar refs
	(why bother passing big chunks of data around?)
	A caveat of that is that the data in the source routine will be changed where necessary.
	txt & html mean the mail piece is already broken into it's components
	composite means that the text is inline with the html as a unit.
	The envelope headers may also be inline. (To: From: Subject; etc.)
	Envelope headers may also be inline for a text message.
	If envelope headers are inline, then the extract_headers value should be set to the corresponding key that holds the document to be parsed.
	ie. If the headers are inline in a composite document, then the extract_headers value should be 'composite'.
	The mail_type key/value will default to '2' (html mailing) unless for example the mail_option_lvl is passed in as a value.
	The headers key/value can be passed a hashref to already prepared headers.
	If this is done, then the headers should already be parsed and UTF-8 escaped.
	No further escaping will be performed on values passed in this manner.
	Separate envelope headers can be set using the appropriate key/value pairs.
	Envelope headers set in this way, as well as extracted headers, will be parsed and UTF-8 escaped.

	A debug flag can be passed which will cause the Mail routine to dump the value of the hash used to mail.
	'pre' will cause the dump to happen instead of mail delivery.
	'post' will cause the dump to happen after delivery is attempted and fails

	example:
	-simplest form-
	Mail(
		composite		=> \$mail_piece_with_embedded_headers,
		mail_type		=> $member->{mail_option_lvl},
		extract_headers	=> 'composite'
	)

	db is a simple DBI connection that takes care of authentications in a central place
	
=cut

use strict;
use Carp;
use DBI;
use Encode qw(encode);
use lib '/home/clubshop';
require MailingUtilsLocalCfg;

our ($Q, $error) = ();
our $boundary = 'B0UNDARY4654AA3D6893237A52FACAAF';

sub db
{
	my $db = DBI->connect("DBI:Pg:dbname=network;host=$MailingUtilsLocalCfg::CFG{'dbhost'}", $MailingUtilsLocalCfg::CFG{'dbusename'}, $MailingUtilsLocalCfg::CFG{'dbpasswd'}) ||
#       my $db = DBI->connect("DBI:Pg:dbname=network;host=localhost", 'bill') ||
		die "Couldn't connect to the database: " . $DBI::errstr . "\n";
	$db->{'RaiseError'} = 1;
	return $db;
}

sub new
{
    my $class = shift;
    bless({}, $class);
}

sub DrHeaders($)
{
	###### you got it...... we are going to doctor the headers to handle utf-8
	###### or upper ascii characters in the headers
	###### we are looking to receive a scalar or a hashref
	###### we'll return the revised scalar,
	###### we'll return nothing after revising the hashref values
	my ($self, $p) = _self_or_default(@_);
	return unless $p;
	unless (ref $p)
	{
		return _dr_header($p);
	}
	elsif (ref $p eq 'HASH')
	{
		foreach(keys %{$p})
		{
			$p->{$_} = _dr_header($p->{$_});
		}
		return;
	}
	else { croak "argument must be a scalar or hashref" }
}

sub Get_Object($$;$)
{
	###### in here we can do whatever other processing between the call and the
	###### actual object retrieval
	###### we are expecting a DB handle, an object ID and an optional language pref
	###### we will pass back a hashref like this: {'language'=>'value'}
	###### we'll always return english
	###### if the language pref is specified and available, we'll return it as well
	###### if langauge pref is not specified, we'll return all available
	# remember self with the object oriented style
	my ($self, @a) = _self_or_default(@_);
	return _get_raw_object(@a);
}

sub Mail
{
	require Mail::Sendmail;
	require ParseMe;
	my ($self, %args) = _self_or_default(@_);
	$args{'mail_type'} ||= 2;	###### default is html
	$args{'debug'} ||= '';		###### an empty default to test against
	my $ctype = "multipart/alternative;\n\tboundary=\"$boundary\"";
	my %Mail = ();
	$Mail{'Smtp'} = $args{'SMTP'} || $args{'smtp'} if $args{'SMTP'} || $args{'smtp'};

	if ($args{'headers'})
	{
		###### let's do all lower case for uniformity
		foreach (keys %{$args{'headers'}})
		{
			$Mail{lc $_} = $args{'headers'}->{$_};
		}
	}

	###### this is the potential list of headers we will pass to the actual mailer
	foreach my $v (qw/to from subject reply-to bcc cc/)
	{
		###### another little deal to lower case things
		my ($key) = grep /^$v$/i, (keys %args);
		next unless $key && $args{$key};
		$Mail{$v} ||= _dr_header($args{$key});
	}

	###### we can also look for headers in our document
	###### we will not overwrite any, but we will use 'em if they are present
	if ($args{'extract_headers'})
	{
		die "Argument to extract_headers must be one of txt,html,composite and the corresponding key value should be ref to the scalar\n"
		unless ref $args{$args{'extract_headers'}};
		my $hdr = ParseMe::Grab_Headers($args{$args{'extract_headers'}});

		foreach (keys %{$hdr})
		{
			$Mail{lc $_} ||= _dr_header($hdr->{$_});
		}
	}
	###### we should have our envelope headers by the time we reach here

	###### in case we have some empty or null key/value pairs, we'll delete 'em
	foreach (keys %args)
	{
		next if $_ eq 'debug';
		
		if (ref($args{$_}) eq 'SCALAR')				# 07/07/14 in reference to the comment below, that is apparently not the case as some processes bombed with a 'not a scalar reference'
		{											# so it was some kind of reference -but- not a scalar
			delete $args{$_} unless ${$args{$_}};	# as of 07/04/14 the only refs are txt and html, and if there is no content, then we can safely ignore them
		}
		else
		{
			delete $args{$_} unless $args{$_};
		}
	}

	###### now we'll work on setting up our message body and content-type
	if ($args{'composite'})
	{
		# if we had to output xml with our combined document
		# then we need to do this to get the doctype off the top of the doc
		${$args{'composite'}} =~ s/^(.*?>)\n*//;
		my $doctype = $1 || '';

		${$args{'composite'}} =~ /^(.*?)(<.*)/ms;
		if ($args{'mail_type'} > 1)
		{
			$Mail{'message'} = _create_html_mailpiece($1, $doctype . $2);
			$Mail{'content-type'} = $ctype;
		}
		else { $Mail{'message'} = $1 }
	}
	elsif ($args{'html'} && $args{'txt'})
	{
			$Mail{'message'} = _create_html_mailpiece(${$args{'txt'}}, ${$args{'html'}});
			$Mail{'content-type'} = $ctype;
	}
	elsif ($args{'html'})
	{
	###### I wouldn't recommend sending only HTML, but there is that option
		$Mail{'message'} = ${$args{'html'}};
		$Mail{'content-type'} ||= "text/html;\n\tcharset=\"UTF-8\"";
		$Mail{'Content-Transfer-Encoding'} ||=  '8bit';
	}
	else
	{
	###### our defaults
	###### the message key is for the existing scripts that used to use Mail::Sendmail directly
		$Mail{'message'} ||= ${$args{'txt'}} || ${$args{'message'}};
		$Mail{'content-type'} ||= "text/plain;\n\tcharset=\"UTF-8\"";
		$Mail{'Content-Transfer-Encoding'} ||=  '8bit';
	}

	$Mail{'List-Unsubscribe'} = '<mailto:unsubscribe@dhs-club.com>';
	
	if ($args{'debug'} eq 'pre')
	{
		_debug(%Mail);
		return;
	}
	my $rv = Mail::Sendmail::sendmail(%Mail);
	$error = $Mail::Sendmail::error;
	_debug(%Mail) if $rv != 1 && $args{'debug'} eq 'post';
	return $rv;
}

sub Select_Translation(\%;$)
{
	###### we will receive a hashref like Get_Object's output
	###### we may also receive a hashref like a member record with a language_pref key
	###### or we may receive a scalar value
	my ($self, $obj, $lang) = _self_or_default(@_);
	return unless $obj;
	$lang ||= 'en';

	if (ref $lang eq 'HASH')
	{
		$lang = $lang->{'language_pref'} || 'en';
	}
	elsif(ref $lang)
	{
		croak 'translation argument must be a scalar or a hashref';
	}
	return $obj->{$lang} || $obj->{'en'} || undef;
}

###### ###### Routines below are primarily for internal consumption ###### ######

sub _create_html_mailpiece($$)
{
		return qq|This is a multi-part message in MIME format.

--$boundary
Content-Type: text/plain;
	charset="UTF-8"
Content-Transfer-Encoding: 8bit

$_[0]
--$boundary
Content-Type: text/html;
	charset="UTF-8"
Content-Transfer-Encoding: 8bit

$_[1]
--$boundary--\n|;
}

sub _debug
{
	my %Mail = @_;
	foreach (keys %Mail){
		print "$_=$Mail{$_}\n";
	}
}

sub _dr_header($)
{
	my $p = shift || return;
	###### if these were generated from XML/XSL then the <> will be escaped
	$p =~ s/&gt;/>/;
	$p =~ s/&lt;/</;

	# put this line into place on 06/03/13 to deal with continued encoding issues
	$p = Encode::decode_utf8($p) unless Encode::is_utf8($p);

	return encode('MIME-Q', $p);
}

sub _get_raw_object($$;$)
{
	###### since we may want to do some preprocessing before passing back
	###### we'll place this here and use a higher level for the other stuff
	my ($self, $db, $obj_id, $language) = _self_or_default(@_);
	croak 'DB handle is undefined.' unless $db;
	croak 'Object identifier is undefined.' unless $obj_id;

	my $sth = $db->prepare("
		SELECT obj_type, obj_value, translation_key
		FROM cgi_objects
		WHERE obj_id= $obj_id
		AND active=TRUE");
	$sth->execute;
	my $rv = $sth->fetchrow_hashref;
	$sth->finish;
	return unless $rv->{'obj_type'};

	###### if we are dealing with a translation key, then lets pull the translations
	if ($rv->{'translation_key'})
	{
		$rv = "
			SELECT value, language
			FROM object_translations
			WHERE key_type= 1
			AND obj_key= $obj_id
			AND active=TRUE\n";

		$rv .= "AND (language= 'en' OR language= '$language')" if $language;
		$sth = $db->prepare($rv);
		$sth->execute;
		$rv = ();

		while (my ($val, $lang) = $sth->fetchrow_array)
		{
			$rv->{$lang} = $val;
		}
		$sth->finish;
	}
	else
	{
		###### turn our plain object into a standardized hashref assuming 'English'
		$rv = {'en'=>$rv->{'obj_value'}};
	}

	return $rv;
}

sub _self_or_default
{
	return @_ if defined($_[0]) && (!ref($_[0])) && ($_[0] eq 'MailingUtils');

	###### I clipped this from CGI.pm
	unless (defined($_[0]) && (ref($_[0]) eq 'MailingUtils'))
	{
		$Q ||= new('MailingUtils');
		unshift(@_, $Q);
	}
	return wantarray ? @_ : $Q;
}

1;	

###### 02/16/06 removed all the conversion stuff and added the Mail routine
###### and supporting internal routines, also added the DB connection component
###### 02/20/06 finetuned the grep matching for header matching
# 12/08/09 tweaked to accomodate the updated method of determining translation templates
# 08/31/10 tweaked the Get_Object stuff to ignore templates that are NOT marked active
###### 06/03/13 revised _dr_header to simply use Encode's MIME-Q encoding... how easy is that???