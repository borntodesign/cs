package Refcomp;
use strict;

my %Cache = ();

=head2 Refcomp

	A base class for things that have to do with refcomp data.
	This may end up being basically empty, but just in case ;-)
	
=head3 new

	A standard object instanciator.
	
=head4 Arguments

	None required.
	
	An optional hashref can be passed with the following key/value pair that will be used:
	{
		'db'=> an active DB handle
	}

=head4 Returns

	A new Refcomp object
	
=cut

sub new($;$)
{
	my ($class, $args) = @_;
	my %h = ();
	$h{'db'} = $args->{'db'} if $args->{'db'};
	return bless \%h, $class;
}

=head3 db

	Set/Reset/Obtain the available DB handle.
	
=head4 Arguments

	An optional DB handle.
	If one is provided, the existing one will be replaced with the new one.
	
=head4 Returns

	The available DB handle.
	
=cut

sub db
{
	my ($self, $db) = @_;
	$self->{'db'} = $db if $db;
	
	warn 'Cached DB handle is uninitialized' unless $self->{'db'};
	return $self->{'db'};
}

=head3 LoadRefcompRecord

	Load a record into the Cache for further use.

	Note, the Cache will be completely reinitialized prior to obtaining the new data.
	Also, you should have called new() with an active DB handle or set it with a call to DB().

=head4 Arguments

	A numeric ID
	An optional hashref. This key/value pair is currently recognized:
	{
		'tbl'=> 'the table name we should query'	# the default is 'refcomp'
	}
	
=head4 Returns

	A hashref of the record.
	
=cut

sub LoadRefcompRecord($$$)
{
	my ($self, $id, $args) = @_;
	die "ID argument must be passed and it must be numeric only" unless $id && $id !~ m/\D/;
	
	unless ( $Cache{'RefcompRecord'}->{$id} )
	{
		my $tbl = $args->{'tbl'} || 'refcomp';
	
		$Cache{'RefcompRecord'}->{$id} = $self->db->selectrow_hashref("SELECT * FROM $tbl WHERE id= $id");
	}
	
	return $Cache{'RefcompRecord'}->{$id};
}

1;