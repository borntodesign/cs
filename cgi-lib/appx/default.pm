package default;
###### a plugin for the member application
###### this one contains specific routines for the default track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: Nov. 2006	Bill MacArthur
###### last modified: 10/21/15 Bill MacArthur

use strict;
require wrap;

sub debug
{
	warn "In appx::debug on CS side";
}

=head3 DivertVipAPsToGI()

=head4 Description

	If a VIP sponsor has not specified the membertype, then we are assuming that the application is for an Affiliate Partner,
	formerly known as an associate member.
	Since the Clubshop.com default track of appx was always the funnel for externally hosted VIP apps, we need to redirect that sort of traffic to the app on GI
	This routine takes care of that using the global $sponsor which should be set by the time it is called.
	
	10/21/15 from what I can see, this function is no longer in use -and- indeed should not be since glocalincome is no more
=cut

#sub DivertVipAPsToGI
#{
#	# we aren't going to evaluate the value, we are only looking for a default of empty or something besides 'm'
#	return 1 unless ($main::params{'membertype'} || 'm') eq 'm';
#	
#	# if the sponsor is not VIP, we can skip redirection
#	return 1 unless $main::sponsor->{membertype} eq 'v';
#	
#	print $main::q->redirect("https://www.glocalincome.com/cgi/appx.cgi/$main::sponsor->{'id'}?" . $main::q->query_string);
#	return 0;
#}

# since we have VIPs funneling memberships in from frontend pages of their own creation
# and since we used to determine the sponsor from the sph param in step1
# and since the sph is subject to change
# and since this change breaks their 'apps'
# we will decide how we are going to arrive at a sponsor by using the sph if it's available
# otherwise we will simply go with the regular sponsorID and that built-in function
sub get_sponsor_from_params
{
	if ($main::q->param('sph')){
		main::ManageSPH('decrypt');
		return 0 unless main::SponsorCK();
	}
	else {
		main::DetermineSponsor();
		# SponsorCK() is invoked from within DetermineSponsor()
	}
	return 1;
}

1

# 07/22/09 added the SponsorCK when using the sph
# 08/03/10 added the debug() routine
# 07/12/11 added "require wrap"
