package ag;
###### a plugin for the member application
###### this one contains specific routines for the affinity group track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: Nov. 2006	Shane Partain	
###### last modified: 12/04/06 Shane Partain 

###### REMEMBER, the live app may run in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

sub new_alias
{
	my ($sequence) = $main::db->selectrow_array("SELECT nextval('ag_sequence')");
	return 'AG-' . $sequence;
}

sub ck_sponsor_vip
{
	unless ( ($main::sponsor->{membertype} eq 'v') || ($main::sponsor->{id}==1) ){
		my $uvip = $main::db->selectrow_array("SELECT upline 
						FROM my_upline_vip 
						WHERE id=$main::sponsor->{id}");
		$main::sponsor = $main::db->selectrow_hashref("$main::mbr_qry m.id=$uvip");
		$main::params{'sponsorID'} = $main::sponsor->{id};
	}else{ return 1; }
}

# since we have VIPs funneling memberships in from frontend pages of their own creation
# and since we used to determine the sponsor from the sph param in step1
# and since the sph is subject to change
# and since this change breaks their 'apps'
# we will decide how we are going to arrive at a sponsor by using the sph if it's available
# otherwise we will simply go with the regular sponsorID and that built-in function
sub get_sponsor_from_params
{
	if ($main::q->param('sph')){ main::ManageSPH('decrypt') }
	else { main::DetermineSponsor() }
	return 1;
}
1;
