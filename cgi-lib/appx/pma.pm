package pma;
use strict;

=head1 NAME:

	PMA - The Preregistration Merchant Application module for appx

=cut

=head2 submit

	This handles the insertion of the collected data from the
	Preregistration Merchant Application form.

=cut

sub submit
{
	my $args = shift || {};
	die "arguments should be passed in a hashref\n" unless ref $args eq 'HASH';
	# we will recognize the auto_activate key to automatically activate this prereg

	my $query = 'INSERT INTO potential_merchant_affiliates ';

	my $fields = $args->{auto_activate} ? 'accepted,' : '';
	my $values = $args->{auto_activate} ? 'NOW(),' : '';

# after reviewing the actual processing, there is no use for auto-activation since the member creation process is completely uncoupled to this application
# that part currently resides in the code base built for manual admin approval

	my @data = ();


#	if($main::params{'country'} !~ /^AN$|^AW$|^SR$/)
#	{
#		die 'This application is currently used for Aruba, Netherland Antillies, and Suriname.';
#	}

	foreach my $keys ($main::q->param)
	{
		if($main::params{$keys} && $keys ne 'Submit')
		{
			
			$fields .= $keys . ',';
			$values .= '?,';
			
			my $param_value = $main::q->param($keys);
			
			$param_value =~ s/\D//g if($keys =~ /^referral$/);
			
			push @data, $param_value;
			
		}
	}

	$fields =~ s/,$//;
	$values =~ s/,$//;


	$fields .= ', ipaddr';
	$values .= ',?';




	push @data, $main::q->remote_host();




	my @pk = ();

	eval{


		my @pk = $main::db->selectrow_array("SELECT NEXTVAL('potential_merchant_affiliates_pk_seq')");

		$fields .= ', pk';
		$values .= ',?';
		push @data, $pk[0];

		$query .= "($fields) VALUES ($values)";

		$main::db->do($query, undef, @data);

		$main::extras{pk} = $pk[0];
	};
	if($@)
	{
		die ' There was an issue saving the informationa. ' . $@;
	}




	return 1;
}

=head2 checkBusinessCategories

	Here we check to see if the user has 
	selected a business description from 
	the provided list.

=cut

sub checkBusinessCategories
{
	return 1 if $main::params{business_type} && $main::params{biz_description};
	push @main::errors, ['biz_description','type_of_business'];
	return 1;
}

=head2 convertAliasToID
	If they submitted an alphanumeric "ID" then convert it to a numeric ID
	This is necessary as the master XML configuration specifies that the id parameter is digits only

=cut

sub convertAliasToID
{
	return 1 unless $main::params{id} =~ m/\D/;
	($main::params{id}) = $main::db->selectrow_array('SELECT id FROM members WHERE alias=?',undef, uc($main::params{id})) || '';
	# if we end up empty, oh well, it was an invalid value anyway
	return 1;
}

1;
