package update;
use strict;
use lib ('/home/httpd/cgi-lib/appx');
use upg;

our ($id, $member) = ();

sub init
{
	$id = $main::mem_info->{'id'};
}

sub LoadMemberDetail
{
	upg::init();
	unless ( $id && $id !~ /\D/ )
	{
		push @main::errors, ['generic', qq|<a href="https://www.clubshop.com/cgi/LoginMembers.cgi?plainRedirect=1&amp;destination=$main::ME/update">Login Required</a>|];
	}
	else
	{
		###### we need more member info than is pulled from the main appx memberinfo query
		$member = $main::db->selectrow_hashref('SELECT * FROM members WHERE id= ?', undef, $id);
		die "Unable to retrieve a membership for $id\n" unless $member->{'id'};
		$main::member = $member;
	}
	return 1;	
}

sub LoginCk
{
	# rather than give ém a link to click with an empty interface like above, redirect them through the login process if they are not logged in
	unless ( $id && $id !~ /\D/ )
	{
		print CGI->redirect("https://www.clubshop.com/cgi/LoginMembers.cgi?plainRedirect=1&destination=$main::ME/$main::app_type");
	}
	return 1;
}

sub Update
{
	my $qry .= 'UPDATE members SET ';
	my @VALS = ();
	my $p = \%main::params;
	foreach (_update_cols())
	{
		$qry .= "$_ = ?,";
		###### don't put empty strings in, instead let DBI put NULLs
		push (@VALS, ((defined $p->{$_} && $p->{$_} gt '') ? $p->{$_} : undef));
	}
	
	foreach (qw/country/)	# treat these differently as, for example, we have disabled the ability to update one's country as of 09/08/15, but this may change
	{
		# if the parameter is present (defined), we will consider it for update, otherwise we will ignore it
		if (defined $p->{$_})
		{
			$qry .= "$_ = ?,";
			###### don't put empty strings in, instead let DBI put NULLs
			push (@VALS, ($p->{$_} gt '' ? $p->{$_} : undef));
		}
	}
#	chop $qry;
	$qry .= "memo='member update from: " . $main::q->remote_addr . "' WHERE id = $id";
	$main::db->do($qry, undef, @VALS);
	if ($DBI::err)
	{
		die "DBI problem: $DBI::errstr\nError updating your profile";
	}
	# insert a node for displaying a success message
	$main::extras{'messages'}->{'update_successful'} = ();
	return 1;
}

sub _update_cols
{
	return qw/
company_name emailaddress emailaddress2 address1 address2 city state postalcode
cellphone other_contact_info phone fax_num password tin
language_pref
	/;
}

1;
