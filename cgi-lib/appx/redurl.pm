package redurl;
###### a plugin for the member application
###### this one contains specific routines for the redurl track "redirect to a URL"
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name													
###### deployed: 09/14/05	Bill MacArthur
###### last modified: 05/07/10	Bill MacArthur

###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use Digest::MD5 qw( md5_hex );
use strict;
use CGI;

###### package Globals
sub Dr_Redurl
{
	_dr_redurl($main::member->{id});
}

sub Login
{
	###### since we want to do this before error handling, we could end up being called with an empty param
	###### we still need to return true, though, otherwise errors will not be shown
	return 1 unless $main::params{login_id};

	my $qry = $main::mbr_qry;
	###### get rid of the trailing 'WHERE'
	$qry =~ s/WHERE.*$//;

	###### if our ID is alphanumeric, then we'll assume an alias
	###### otherwise if its seven digits or less we'll assume an id
	###### else we'll try a clubbucks ID
	if ($main::params{login_id} =~ /\D/){ $qry .= 'WHERE m.alias= ?' }
# they don't want numeric logins
#	elsif (length $id <= 7){ $qry .= 'WHERE m.id= ?' }
	else
	{
		$qry .= ',clubbucks_master cb
			WHERE 	cb.current_member_id=m.id
			AND 	cb.id= ?::BIGINT';
	}
	$main::mem_info = $main::db->selectrow_hashref($qry, undef, uc $main::params{login_id});		
	unless ($main::mem_info->{id})
	{
		push @main::errors, ['mini_login_failed', 'login_id'];
	}
	push (@main::cookies, $main::q->cookie(
		-name=>'id',
		-domain=>'.clubshop.com',
		-expires=>'+1y',
		-value=>$main::mem_info->{id}));
	push (@main::cookies, $main::q->cookie(
		-name=>'pa',
		-domain=>'.clubshop.com',
		-expires=>'+1y',
		-value=>$main::mem_info->{alias}));
	return 1;
}

sub Redirect
{
	# since we are only redirecting back into our 'redirector'
	# we will let it take care of special URL cases where we don't want an ID on the end
	# we will simply ensure that the ID is placed for the 'cid' parameter
	_dr_redurl($main::mem_info->{id});

#	print $main::q->redirect(
#		-location=>$main::params{redurl} . $main::mem_info->{alias},
#		-cookies=>@main::cookies);
	print $main::q->redirect(
		-location=>$main::params{redurl},
		-cookies=>@main::cookies);

	###### return undef so that the script will stop executing
	return;
}

sub Save_URL
{	# Since a URL is awkward to pass around as a parameter,
	# we'll expect it to be part of our 'path'. This way we can take everything
	# after the app_type and save it until we need it and then stuff it in the XML.
	###### Hopefully we don't have to do something more awkward at a later date.
#	(my $url = $main::q->path_info) =~ s#/.*/.*/(.*)#$1#;
	(my $url = $main::q->path_info) =~ s#^/\w*/\w*/*##;
#	$url .= '?' . $main::q->query_string if $main::q->query_string;
	if (my $qs = $main::q->query_string){

	# change the modern semicolon delimiters to old style ampersands... some servers still choke on semicolons
		$qs =~ s/;/&/g;
		$url .= "?$qs";
	}

	# change the modern semicolon delimiters to old style ampersands... some servers still choke on semicolons
#	$url =~ s/;/&/g;	# we need to retain the semicolons in "our" redirector path, so we will only replace the semicolons in the vendor URL

	###### We could always stuff an error onto the stack, but since we will not
	###### be able to redirect them, we may as well stop here.
	die "Failed to extract the redirection URL\n" unless $url;

	###### create a redurl node by inserting a parameter
	$main::params{redurl} = $url;
	return 1;
}

sub _dr_redurl
{
#	$main::params{redurl} =~ s#(proceed=1;cid=)#$1$_[0]#;
# somewhere along the way, probably with the new malls, the URL was changed so it looks like ..proceed=1;mall_id=1;cid=
# breaking the pattern, but then why do we need anything beyond the cid= to match anyway?
	$main::params{redurl} =~ s#(;cid=)#$1$_[0]#;
	return 1;
}

1;

# 01/19/10 modified Save_URL to replace the semicolon delimiters with escaped ampersands,
# some web sites still choke on semicolon delimiters, but CGI.pm automatically performs an ampersand to semicolon conversion...
# we could initialize CGI.pm with the -oldstyle_urls pragma, but that may end up breaking other things
# 05/07/10 revised the conversion of semicolons to ampersands in Save_URL to not mess with the semicolons in our redirector path, it uses those specifically as the delimiter
