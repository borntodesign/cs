package ds1;
###### a plugin for the member application
###### this one contains specific routines for the ds1 track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: May 2010	Bill MacArthur
###### last modified: 

###### REMEMBER, the live app may run in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;
use appx::default_01;
use appx::gi01;
use wrap;

sub Check_for_existing_Unconfirmed
{
	my $args = shift;
	# we need to provide different feedback than the default in the event of a duplicate
	# if we have an empty return, then echo it
	return unless my $rv = main::Check_for_existing_Unconfirmed({no_display=>1});
	# if the return was normal echo that as well
	return 1 if $rv eq '1';
	main::Err_xml({list=>['temp_member_exists_gi_ap']});
	return;
}

sub Check_Membership_Dupe
{
	# the rules of the game are too detailed to use the simple appx version
	my @dupes = gi01::_Check_Membership_Dupe();
	return 1 unless @dupes;

	main::Err_xml({list=>['gi-dupe-alert']});
	return;
}

sub deflect_all_except_house
{
	return default_01::deflect_all_except_house(@_);
}

1;
