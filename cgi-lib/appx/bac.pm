package bac;
###### a plugin for the member application
# This one contains specific routines for bac track
# which is the confirmation track associated with the ba signup track.

# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
												
###### deployed: 10/10/05	Bill MacArthur
###### last modified:
		
###### REMEMBER, once the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again.
###### Changes will be available immediately in a regular cgi environment ie. cgi/

use strict;

sub log_activity
{
	######	We won't worry about this being transactional with the actual member
	###### creation statement.... at least, not for now.
	my $rv = $main::db->do("
		INSERT INTO activity_rpt_data (id,spid,activity_type)
		VALUES ($main::member->{id}, $main::member->{spid}, 31)");
	
	return 1 if $rv;

	###### we don't want to divulge our 'tracking'
	warn "SQL error in bac.pm";
	return 1;
}

1;


