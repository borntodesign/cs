package gi01;
###### a plugin for the member application
###### this one contains specific routines for the gi01 track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: May 2010	Bill MacArthur
###### last modified: 

###### REMEMBER, the live app may run in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;
use appx::default_01;
use wrap;

sub Check_for_existing_Unconfirmed
{
	my $args = shift;
	# we need to provide different feedback than the default in the event of a duplicate
	# if we have an empty return, then echo it

	return unless my $rv = main::Check_for_existing_Unconfirmed({'no_display'=>1});
	# if the return was normal echo that as well
	return 1 if $rv eq '1';

	WrapPrep() unless $args->{'nowrap'};

	main::Err_xml({'xml_err'=>1012, 'list'=>['temp_member_exists_gi_ap']});

	return;
}

sub Check_Membership_Dupe
{
	# the rules of the game are too detailed to use the simple appx version
	my @dupes = _Check_Membership_Dupe();
	return 1 unless @dupes;

	WrapPrep();
	main::Err_xml({xml_err=>1012, list=>['gi-dupe-alert']});
	return;
}

sub debug
{
	warn "in debug";
}

sub deflect_all_except_house
{
	return default_01::deflect_all_except_house(@_);
}

sub ProcessReferral
{
	my ($rv, $sp) = default_01::ProcessReferral();
	# just in case we have a submitted value that is not used
	# we'll pass it onto the next part of the process
	$main::params{memo} = $main::params{referral};

	# this is consistent with a normal return if no sponsor was found
	return $rv unless $rv && $sp;

	# now we deal with the case where a sponsor was found
	# theoretically, the only referrals on the GI site should be other GI members,
	# so we won't be referring offsite to CS for say AGs, MAs or what not
	# if the proposed sponsor can sponsor an associate, we'll send this one their way
	# otherwise we'll keep 'em for ourselves

	$rv = $main::db->selectrow_array("
		SELECT TRUE FROM membertype_sponsors WHERE membertype= ? AND can_sponsor= 'm'", undef, $sp->{membertype});
	return 1 unless $rv;
	print $main::q->redirect('https://www.glocalincome.com/cgi/appx.cgi/' . $sp->{id});
	$main::SkipDisplay = 1;
	return;
}

sub WrapPrep
{
	# since we need to wrap on regular errors and errors flagged within this module
	# we may as well consolidate the wrapping preparation in here so it can be called from in here or in a configuration
	my $lang_nodes = XML::local_utils::flatXHTMLtoHashref( $main::gs->Get_Object($main::db, 1009) );
	$main::Wrap = wrap->new({
		template=>1010,
		lang_nodes=>{header_text_larger=>$lang_nodes->{n0}},
		gs=>$main::gs,
		db=>$main::db});
	return 1;
}

sub _Check_Membership_Dupe
{
	my $sth = $main::db->prepare('
		SELECT m.id, m.spid, m.membertype, m.emailaddress, m.firstname, m.lastname
		FROM members m
		JOIN "Member_Types" mt
			ON m.membertype=mt."code"
		WHERE mt.gi_no_dupes=TRUE
		AND (
			m.emailaddress = ?
			OR
			m.emailaddress2 = ?
		)');
	$sth->execute($main::params{emailaddress}, $main::params{emailaddress});
	my @dupes = ();
	while (my $tmp = $sth->fetchrow_hashref){
		push @dupes, $tmp;
	}
	main::setNotification($_->{id}, 30) foreach @dupes;
	return @dupes;
}

1;
