package upg;
###### a plugin for the member application
###### this one contains specific routines for upg (upgrade) track applications
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name

###### deployed: 01/11/06	Bill MacArthur
###### last modified: 08/14/15	Bill MacArthur

use 5.010;
use lib ('/home/httpd/cgi-lib');
use strict;
use EventLogin;
require wrap;
require CC_Fraud_Ck;

###### set to -ONE- to not authorize for *testing* ONLY
our $SKIP_PAY_PROCESSING = 0;

###### for convenience, we'll make simple versions
###### however, if we want to change the main:: var in its entirely
###### we cannot subsequently do a $member = <new hashref>
###### instead we must still reference the real var $main::member = <new hashref>
###### the local version will reflect the change since it points to the main::
our ($db, $p, $cfg, $Event, $utype, %STYPES) = ();

sub init
{
	$db = $main::db;
	$p = \%main::params;
	$cfg = $main::cfg;
	$Event = EventLogin::ValidateTicket($main::q->cookie('EventLogin'));
						
	# load the event into the 'extras' hash so it makes it's way into the XML for use by templates
	$main::extras{'EventLogin'} = $Event if $Event;
	return 1;
}

sub Adjust_for_VIP_direct
{
	if ($p->{'flg'} eq 'dv')
	{
		###### if we received an alias parameter, then delete it,
		###### it shouldn't be there anyway
		$p->{'alias'} = '';

		$main::cfg->opt('alias',1);
	}
	return 1;
}

sub Adjust_for_tin
{
	###### the tin is required if they select 'US' as the country
	###### but only then, so we'll define it as optional in the config
	###### and make it required here if the country is 'US'
	if ($p->{'country'} && $p->{'country'} eq 'US')
	{
		$main::cfg->req('tin',1);
	}

	return 1;
}

sub Adjust_state_requirement
{
	###### if we have state/province data, then it is required
	###### otherwise we'll let them decide
	# Since we are not called until after the country has been evaluated and saved,
	# we should have a good country to work with.

	###### we'll call the main state loading function
	###### and then see if there are any states
	main::Load_States();
	$main::cfg->opt('state',1) unless keys %{$main::states};
	return 1;
}

sub Authorize
{
	require GI_ccauth;
	require ccauth_parse;
	my $args = shift || {};
	
	my (@ERR, $reason, $iaddr, $paddr, $autoship);
	my $date = scalar localtime;

	my $m = $main::member;
	my $price = $args->{'price'} || Get_Subscription_Price($m->{'country'});
	my %DATA = ();
###### get the next auto increment number
	$DATA{'invoice_number'} = $db->selectrow_array("SELECT nextval('inv_num_invoice_seq')") ||
		die "Failed to generate an invoice number\n";
	$DATA{'invoice_number'} = "UG-$DATA{invoice_number}";
	$DATA{'order_description'} = 'VIP Upgrade';
	$DATA{'id'} = $m->{'alias'};
	$DATA{'amount'} = $price;
	$DATA{'emailaddress'} = $m->{'emailaddress'};
	$DATA{'ship_address'} = $m->{'address1'};
	$DATA{'ship_city'} = $m->{'city'};
	$DATA{'ship_state'} = $m->{'state'};
	$DATA{'ship_zip'} = $m->{'postalcode'};
	$DATA{'ship_country'} = $m->{'country'};
	$DATA{'ship_firstname'} =$m->{'firstname'};
	$DATA{'ship_lastname'} = $m->{'lastname'};
	$DATA{'ship_company_name'} = $m->{'company_name'};
#	$DATA{phone} = "$m->{phone_ac} $m->{phone}";
	$DATA{'phone'} = $m->{'phone'} || $m->{'cellphone'};
	$DATA{'fax'} = $m->{'fax_num'};
	$DATA{'account_holder_firstname'} = '';
	$DATA{'account_holder_lastname'} = $p->{'acct_holder'};
	$DATA{'account_holder_address'} = $p->{'mop_address'};
	$DATA{'account_holder_city'} = $p->{'mop_city'};
	$DATA{'account_holder_state'} = $p->{'mop_state'};
	$DATA{'account_holder_zip'} = $p->{'mop_postal_code'};
	$DATA{'account_holder_country'} = $p->{'mop_country'};

	$DATA{'payment_method'} = 'CC';
	$DATA{'cc_number'} = $p->{'cc_number'};
	$DATA{'cc_expiration'} = $p->{'cc_exp'};
	$DATA{'card_code'} = $p->{'card_code'};
	$DATA{'ipaddr'} = $main::q->remote_addr;

###### following line to bypasses authorization when testing
###### ###### bypass actual authorization
	if ($SKIP_PAY_PROCESSING)
	{
		print "Content-type: text/html\n\n";
		print "SKIP_PAY_PROCESSING is ON in upg.pm<br />\n";
		print "SKIPPING ACTUAL AUTHORIZATION<br />\n";
		goto 'SKIP';
	}
#print "Content-type: text/plain\n\n";foreach(keys %DATA){print "$_=$DATA{$_}\n";}
#goto 'END';
	my ($auth) = CCAuth::Authorize(\%DATA, {'raw'=>1, 'member'=>$m});
#	my ($auth) = CCAuth::Authorize(\%DATA, {raw=>1});
#	my $auth= '3,,27,my avs mismatch,,Z,,extra stuff';

	unless ($auth)
	{
		push (@main::errors, ['generic_error', 'authentication_failure']);
	###### for now we'll just return the data entry screen with the error
		return 1;
	}

	my ($auth_errors, $rv) = ccauth_parse::Report($auth);
       unless ( $rv->{'resp_code'} == 1)
	{
###### we want to keep track of failed authorizations
###### we'll let failures do to 'errors' slide without recording it as 'bad'
		my $badmark = $rv->{'resp_code'} == 2 ? 1:0;
		my $qry = "UPDATE upgrade_tmp SET
				authorized= authorized - $badmark,
				last_modified= NOW(), 
				authz_results= ?
			WHERE id = $m->{'id'}";

		$db->do($qry, undef, "Transaction ID: $rv->{'transaction_id'}\n$rv->{'resp_reason_code'} : $rv->{'resp_reason_text'}");

	 	push (@main::errors, ['generic_error', $auth_errors]);
###### if they are at the max declines with this decline, we'll let 'em know not to try again
		if (abs ($m->{'authorized'} -1) >= $main::cfg->vars('max_cc_auth_attempts'))
		{
			main::Display_Page({'xsl'=>$main::cfg->xslt('fraud_alert')});
			$main::SkipDisplay = 1;
		}
		else
		{
		###### for now we'll just display the errors along with the form
			return 1;
		#	main::Display_Page({xsl=>$main::cfg->xslt(<FILL IN THE BLANK>)});
		#	$main::SkipDisplay = 1;
		}
		return;
       }
SKIP:

###### we will mark their tmp record authorized in case anything goes wrong beyond here
###### that way we can easily do the upgrade later
	$db->do("UPDATE upgrade_tmp SET
			authorized= 1,
			last_modified= NOW(),
			authz_results= ?,
			authorized_date= NOW()
		WHERE id = $m->{'id'}", undef,
		"Transaction ID: $rv->{'transaction_id'}\nApproval Code: $rv->{'approval_code'}");

	###### since they authorized, we'll create a Club Account credit reflecting the amount received
	$db->do("INSERT INTO soa (trans_type, id, amount, description, memo)
		VALUES (7000, ?, ?, ?, ?)", undef, 
		$p->{'id'}, $price,
		"Approval Code: $rv->{'approval_code'}", "Transaction ID: $rv->{'transaction_id'}");

	return 1;
}

sub Branch2PM
{
	###### switch to the app step corresponding to the pay method
	###### or if this is a VIP direct application redirect through there
	my $p = \%main::params;	###### I don't know why the global stopped working
	my $pm = $p->{'pm'};
#warn "pm: $pm";
	die "Pay method parameter unavailable\n" unless $pm;

	###### if we are handling a 'VIP direct' signup,
	###### then we have to first create an 'x' membership to work with for later on
	if ($p->{'sponsorID'})
	{
		# Create_Unconfirmed_Membership() does not do any kind of dupe checking
		# so we will do so here instead of blindly creating another 'x' membership if they somehow press reload
		$main::member ||= main::Get_Live_Record($p->{'emailaddress'},["m.membertype='x'"]);
		unless ($main::member->{'id'})
		{
			main::Create_Unconfirmed_Membership({'memo'=>'Partner direct application'});
		}
		###### we now have the new membership in the $main::member hashref
	}
	###### it is not loaded unless we have created a membership
	$main::member ||= main::Get_Live_Record($p->{'alias'}) || die "Failed to load main member hash";

	###### now let's create an upgrade tmp record in case payment is later or fails
#	tmp_record($main::member->{id});
	tmp_record();
#warn "back from tmp_record";
	###### figure out where we are going now :)
	$pm = $main::cfg->vars('pms')->{$p->{'pm'}};
	my $ns = ($p->{'flg'} eq 'dv' && $pm->{'dv'}->{'next_step'}) ? $pm->{'dv'}->{'next_step'} : $pm->{'next_step'};

	die "Configuration error: cannot determine next step\npm=$p->{'pm'}" unless $ns;

	main::DetermineAppStep( $ns );
	# reset our configuration for the duration
	$main::cfg = new _my_private_
	$main::RedoFlag = 1;
	return 1;
}

sub CC_Agreement_CK
{
	unless ( $p->{'agree'} && $p->{'agree'} eq '1' )
	{ push @main::errors, ['generic_error','agree'] }
	return 1;
}

=head3 CC_Fraud_Ck
	
	A private function for instantiating a CC_Fraud_Ck object for use by different functions in this class.

=cut

sub CC_Fraud_Ck
{
	state $rv;
	$rv = CC_Fraud_Ck->new({'db'=>$db}) if ! $rv;
	return $rv;
}

sub Check_Already_Authorized
{
	# we are checking to see if we have already paid for an upgrade by credit card
	# the temp record is loaded into the main::member record if we are along in the process,
	# so we can use that value if it exists
	unless (($main::member->{'authorized'} || '') gt '')
	{
		($main::member->{'authorized'}) = $db->selectrow_array("
			SELECT authorized FROM upgrade_tmp WHERE utype='v' AND authorized=1 AND id=?",
			undef, ($main::member->{'id'} || $p->{'id'})) || 0;
	}

	if ($main::member->{'authorized'} == 1)
	{
		main::DetermineAppStep( 'step12' );
		# reset our configuration for the duration
		$main::cfg = new _my_private_
		$main::RedoFlag = 1;
		$main::BreakStageProcessing = 1;
	}
	return 1;
}

sub CheckforDV
{
	###### since we've got existing members and possibly new 'x' members trying
	###### the direct-to-VIP route, we'll intercept them and send them the regular way
	my $id = ($p->{'alias'} || $main::q->cookie('id')) unless $main::ADMIN;
	if ($id)
	{
		# get the member record
		$main::member ||= main::Get_Live_Record($id);
		if ($main::member)
		{
			$p->{'alias'} = $main::member->{'alias'};
		###### if we successfully get an alias, we'll negate the direct-to-VIP parts
			$p->{'flg'} = '';
			$p->{'sponsorID'} = '';
			$p->{'id'} = $main::member->{'id'};	###### handy for a preliminary 'Fraud Check'
			return 1;
		}
	}
	###### by default, we will accept a sponsorID parameter
	###### in lieu of that, we'll look for a cookie if the flag is set
	my $sponsor = $p->{'sponsorID'} ||
		($p->{'flg'} eq 'dv' ? ($main::q->cookie('refspid') || 1) : undef);

	if ($sponsor)
	{
		###### if we received an alias parameter, then delete it,
		###### it shouldn't be there anyway
		$p->{'alias'} = '';

		# also set the 'spare' flag so we will know this is a direct-to-VIP application
		$p->{'flg'} = 'dv';

	###### since the alias parameter is required by default at the start of the app
	###### we have to un-require it
	###### if we are receiving a sponsorID param, this will be our flag for now
	#	$main::cfg->opt('alias',1);
		ValidateSponsorID($sponsor);
	}
	return 1;
}

=head2 Check_Authorization

=head3 Description:

	An appx style routine to serve as a wrapper around the real routines in CC_Fraud_Ck
	This routine requires no arguments.
	All the data it needs (ipaddr & ccnum) should be available in the main params hash or the CGI object.

=cut
	
sub Check_Authorization
{
	my $cc = shift;
	my $error = ();
	
	# according to Dick, 05/05/15, if they are from the US, then we will let 'em through, ostensibly because the card will be subject to real AVS
	# of course, they could have US as their home country and use an international card and still get by without AVS,
	# but it is a tangled web we weave when we don't want to use the mainstream tools provided by our card processor
	# theoretically we will be called once with the member's country of record and then again with the country of the card holder
	$cc ||= $main::member->{'country'} || '';
	return 1 if $cc eq 'US';
	
	# let's see if their ID is registered to give them a pass
	return 1 if CC_Fraud_Ck->Check_Member_Authorized({'id'=>$p->{'id'}});
	
	# maybe the card is already authorized
	return 1 if CC_Fraud_Ck->Check_CC_Number({'ccnum'=>$p->{'cc_number'}});
	
	if (CC_Fraud_Ck->Check_Country({'country_code'=>$cc}) || 
		CC_Fraud_Ck->Check_Taproot_Line({'id' => $p->{'id'}})
	)
	{
		$error = CC_Fraud_Ck->ErrorNotice();
	}

	return 1 unless $error;

	main::Err_xml({'list'=>[]}, "<div>$error</div>");
	return;
}

=head2 Check_Country_And_Ensure_Card_Authorized

=head3 Description:

	An appx style routine to serve as a wrapper around the real routine in CC_Fraud_Ck
	This routine requires no arguments.
	All the data it needs (ipaddr & ccnum) should be available in the main params hash or the CGI object.

=cut
	
sub Check_Country_And_Ensure_Card_Authorized
{
	my $cc = shift;
	
	my $rv = CC_Fraud_Ck->Check_Country_And_Ensure_Card_Authorized({
		'ipaddr'=>$main::q->remote_addr,
		'country_code'=>$cc || $main::member->{'country'} || '',
		'ccnum'=>$p->{'cc_number'}
	});
	return 1 unless $rv;

	# if we got to here they could be denied, so let's see if their ID is registered to give them a pass
	return 1 if CC_Fraud_Ck->Check_Member_Authorized({'id'=>$p->{'id'}});
	
	main::Err_xml({'list'=>[]}, "<div>$rv</div>");
	return;
}

sub Check_Membership_Dupe
{
	my %args = ('emailaddress' => $p->{'emailaddress'});
###### as of 01/10/06 the gs routine ignores d,s,x - that's fine for now
###### what do we care about dupes?
#	$args{ignore_mtypes} = 's,x' if ($arg eq 'strict' || $arg eq 'vip');

	###### for VIP upgrades we need to exclude our existing membership in the search
	$args{'id'} = $p->{'id'};

	my @list = $main::gs->MembershipDupeCK(\%args);
	return 1 unless @list;
	
	###### create a contact link
	my $html = qq|<div><a href="mailto:support1\@clubshop.com?subject=dupe membership: $p->{'alias'}&amp;body=VIP application email match: $p->{'emailaddress'}">support1\@clubshop.com</a></div>\n|;
	main::Err_xml({'list'=>['upg_dupe_error']}, $html);
	return;
}

sub Combine_cc_exp_fields
{
	return $p->{'cc_exp'} = $p->{'cc_exp_month'} . substr($p->{'cc_exp_year'}, 2);
}

sub CreateActivityEntry_app_started
{
	# direct to VIP apps will not have a member ID when this is called, so we have to make it conditional
	my $id = idFromPorMember();
	$db->do('INSERT INTO activity_tracking (id,"class") VALUES (?,35)', undef, $id) if $id;
	return 1;
}

sub CreateActivityEntry_app_submitted
{
	# direct to VIP apps will not have a member ID when this is called, so we have to make it conditional
	my $id = idFromPorMember();
	$db->do('INSERT INTO activity_tracking (id,"class") VALUES (?,69)', undef, $id) if $id;
	return 1;
}

sub CreateRankingEntry_app_started
{
	# direct to VIP apps will not have a member ID when this is called, so we have to make it conditional
	my $id = idFromPorMember();
	$db->do('INSERT INTO member_ranking_daily_trans (member_id, action) VALUES (?, 7)', undef, $id) if $id;
	return 1;
}

sub debug
{
	###### nothing really except a comvenient break-point
#	print "Content-type: text/html\n\n";
#	my $ref = $main::cfg->vars('pms');
#	foreach (keys %{$ref}){print "$_ = $ref->{$_}<br>\n"}
#	print "Content-type: text/html\n\npm=$p->{pm}<br>method=" .$main::cfg->vars('pms')->{$p->{pm}};
# Location of the sendmail program
    my $sendmail = '/usr/sbin/sendmail';

	if (open (MAIL, "| $sendmail -t")){
   print MAIL "To: webmaster\@dhs-club.com\n";
   print MAIL "From: appx\@www.clubshop.com\n";
   print MAIL "Subject: appx debug\n";
   print MAIL "\n";
   print MAIL "$_[0]\n";
   print MAIL "\n";
   close(MAIL);
	} else {
		warn "I can't open sendmail\n";
	}
}

# since this module is used across apps, we need to determine the app type from various criteria
# we will store it in the global $utype for reuse
sub DetermineAppType
{
	return $utype if $utype;
	$utype = 'amp' if $p->{'app_type'} eq 'amp';
	unless ($utype)
	{
		# we need the membertype to make a further determination
#warn "main::member: $main::member, id: $main::member->{'id'} membertype: $main::member->{'membertype'} utype: $utype";
		my $membertype = $main::member->{'membertype'} || main::Get_Live_Record($p->{'id'})->{'membertype'};
#warn "Get_Live_Record called utype: $utype";
		die "Failed to obtain member type record for: $p->{'id'}" unless $membertype;

		$utype = 'ampv' if $membertype eq 'amp';
#warn "member: $main::member, id: $main::member->{'id'} membertype: $membertype utype: $utype";

	}
	$utype ||= 'v';
	return $utype
}

sub Enforce_US_for_MO
{
	if ($p->{'pm'} eq 'mo' && $p->{'country'} ne 'US')
	{
		main::Err('The Money Order payment option is only available to US residents');
		return undef;
	}
	return 1;
}

sub Fraud_Check
{
	return 1 if $main::ADMINMODE;

###### determine if they have matched or exceeded the max number of authorization attempts
###### we'll handle 'error' reporting here since it will be a standard message
	my $ckid = $main::member->{'id'} || $p->{'id'};
	return 1 unless $ckid;

###### we should have a 'pm' here and we only want to do this for credit/debit cards
	return 1 unless $p->{'pm'} eq 'cc' || $p->{'pm'} eq 'dc';

	my $ck = $db->selectrow_array("
		SELECT authorized FROM upgrade_tmp
		WHERE stamp > NOW() - INTERVAL '1 day'
		AND id = ?
	", undef, $ckid) || 0;

	if ($ck <= -($main::cfg->vars('max_cc_auth_attempts')))
	{
		main::Display_Page({'xsl'=>$main::cfg->xslt('fraud_alert')});
	###### we only want this when we stop an actual authorization
	###### since Fraud_Check can be called early on in the process to stop 'em
	###### before they start, we are not interested in those attempts
	###### also, if we decide to no do Fraud_Notify we can simply omit the arg
		Fraud_Notify() if shift;
		
		return;
	}

	return 1;
}

sub Fraud_Notify
{
###### notify HQ about a 'fraud' attempt

	open (MAIL, "|/usr/sbin/sendmail -t") || return;
	print MAIL "From: appx.cgi\n";
	print MAIL "To: " . $main::cfg->vars('fraud_alert_contact') . "\n";
	print MAIL "Subject: Potential Fraud ALERT\n";
	print MAIL "The following party has tried to upgrade beyond the max decline count of ";
	print MAIL $main::cfg->vars('max_cc_auth_attempts') . "\n";
	print MAIL "ID: $main::member->{'id'}\n";
	print MAIL "More information is available in the temp upgrade record table and its archive\n";
	close MAIL;
}

=head2 Get_Subscription_Price

	Obtain the initial subscription price. This includes both the monthly component and the annual component.
	
=head3 Args

	Arg 1 - Optional country code
	Arg 2 - Optional a boolean
	
=head Returns

	A numeric value based upon the country and the subscription type.
	If the second argument is set, the price returned is the native currency amount.
	Otherwise, it is the USD value.

=cut

sub Get_Subscription_Price
{
	my $country = shift;
	my $col = shift || 'usd';				# there are cases where I want the USD value and others where I want the native currency
	$col = 'amount' unless $col eq 'usd';	# if anything came through on the second argument, like a value on the native_currency key recognized in LoadUpgradePricingIntoNonXmlExtras,
											# then we will go with the 'amount' column
	
	my $subtype = $p->{'subscription_type'} || $main::member->{'subscription_type'};	# for whatever reason, we lose the parameter when we finally go to authorize a card
#warn "Get_Subscription_Price id: $p->{'id'}, country: $country, pm: $p->{'payment_method'}, sub-type: $subtype";
	my %stypes = QuerySubscriptionPricing($country);
#use Data::Dumper;
#warn Dumper(\%stypes);
	# we have to employ certain logic as they must pay for 2 months ahead of time
	
	###### ###### this routine must be changed to look for the initial monthly + the renewal monthly + initial annual
#	my $multiplier = 2;#$p->{'payment_method'} ne 'CC' ? 2:1;
	
#warn "p->{'payment_method'}: $p->{'payment_method'} - multiplier: $multiplier";
	my $rv = $stypes{ $subtype }->{$col};
#warn "rv currently: $rv";
#	$rv *= $multiplier;
	# get the monthly renewal price... it is the subscription type that is in the related column
	foreach (keys %stypes)
	{
		if ($stypes{$_}->{'related'} == $subtype)
		{
			$rv += $stypes{ $_ }->{$col};
			last;
		}
	}
	$rv = $rv + $stypes{'2'}->{$col};
#warn "returning with price: $rv";
	return $rv;
}

sub idFromPorMember
{
	return $p->{'id'} || $main::member->{'id'};
}

sub LoadPayAgentIntoExtras
{
	my $pa = $db->selectrow_hashref("
		SELECT
			agent_id,
			agent_firstname,
			agent_lastname,
			agent_emailaddress,
			applicable_country,
			agent_firstname ||' '|| agent_lastname ||' - '|| agent_emailaddress AS myagent
		FROM get_upline_pay_agent_in_country(?)", undef, idFromPorMember());
	if ($pa)
	{
		$main::extras{'pay_agent'} = $pa;
	}
	return 1;
}

sub LoadSubscriptionPricing
{
	my $country = shift;
	my %rv = QuerySubscriptionPricing($country);

	foreach (keys %rv)
	{
		my %_rv = %{$rv{$_}};

		$main::extras{'subscription_types'}->{"t$_"} = [{$_ => \%_rv}];
	}

	return 1;
}

sub LoadUpgradePricingIntoNonXmlExtras
{
	my $country = shift || $p->{'country'} || $main::member->{'country'};	# we may want to coerce the country to get US/USD specific pricing
	my $args = shift || {};
	$country ||= 'US';
	
	my %stypes = QuerySubscriptionPricing($country);
	
	# for our formatting, we may be going for another country currency, but the USD value that we want formatted as USD, like for PayPal
	my $my_currency_code = $args->{'formatting_currency_code'} || $stypes{'2'}->{'currency_code'};
	my $my_country_code = $args->{'formatting_country_code'} || $country;
	
	my %pricing = (
		'pricing_initial' => Get_Subscription_Price($country, $args->{'native_currency'})
	);
	
	foreach my $st (keys %stypes)
	{
		$pricing{'pricing_monthly'} = $stypes{ $st }->{'usd'} if $stypes{ $st }->{'related'} == $p->{'subscription_type'};
		#$pricing{'pricing_monthly'} = $stypes{ $st }->{'amount'} if $stypes{ $st }->{'related'} == $p->{'subscription_type'};
	}

	$pricing{'pricing_monthly'} += ($args->{'pricing_monthly_premium'} || 0);
	$pricing{'pricing_initial'} += ($args->{'pricing_initial_premium'} || 0);
	
	# add our inbound PayPal fees
	if ($p->{'pm'} eq 'pp')
	{
		($pricing{'pricing_monthly'}) = $db->selectrow_array("SELECT calculate_paypal_funding_amount_total($pricing{'pricing_monthly'})");
		($pricing{'pricing_initial'}) = $db->selectrow_array("SELECT calculate_paypal_funding_amount_total($pricing{'pricing_initial'})");
	}
	
	$main::non_xml_extras{'pricing_currency_code'} = $stypes{'2'}->{'currency_code'};
	$main::non_xml_extras{'pricing_initial'} = $main::q->span({'-class'=>'transposeME'}, sprintf("%.2f", $pricing{'pricing_initial'}));
	$main::non_xml_extras{'pricing_monthly'} = $main::q->span({'-class'=>'transposeME'}, sprintf("%.2f", $pricing{'pricing_monthly'}));
	$main::non_xml_extras{'extra_javascripts'} = qq#
		<script type="text/javascript" src="//www.clubshop.com/js/transpose.js"></script>
		<script type="text/javascript" src="//www.clubshop.com/js/sprintf.js"></script>
		<script type="text/javascript" src="//www.clubshop.com/js/Currency.js"></script>
		
		<script type="text/javascript">
		    var my_language_code = "${\($main::gs->Get_LangPref || 'en')}";
		    var my_currency_code = '$my_currency_code';
		    var my_country_code = '$my_country_code';
		
		    \$(document).ready(function() {
		        Transpose.format_currency();
		    });
		</script>
	#;
	
	return 1;
}

sub Notification
{
	my $type = shift;
	my $qry = "
		INSERT INTO notifications (id, notification_type)
		VALUES (${\($p->{'id'} || $main::member->{'id'})}, $type)";
		
	if ($type == 164 && $main::extras{'pay_agent'} && $main::extras{'pay_agent'}->{'agent_id'})
	{
		#my $agent = $db->quote("$main::extras{'pay_agent'}->{'agent_firstname'} $main::extras{'pay_agent'}->{'agent_lastname'} - $main::extras{'pay_agent'}->{'agent_emailaddress'}");
		my $agent = $db->quote($main::extras{'pay_agent'}->{'myagent'});
		$qry = "
		INSERT INTO notifications (id, notification_type, memo)
		VALUES (${\($p->{'id'} || $main::member->{'id'})}, $type, $agent)"
	}
	return $db->do($qry);
}

sub PostAuthorization
{
	###### We may have an occasional case where we want to go directly to
	###### the completion steps (record updating and congratulations).
	###### This could occur if an authorization for funds was made
	###### but for some reason the upgrade process was interrupted
	###### like for a SQL error or other user error.
	###### Keeping the final procedures in a separate step
	###### allows us to go directly to it if a preauthorized condition is detected.
	
	###### for now we only need to change steps
	main::DetermineAppStep( $main::cfg->cfg('next_step') );
	# reset our configuration for the duration
	$main::cfg = new _my_private_
	$main::RedoFlag = 1;
	return 1;
}

sub PrepareAlertPay
{
	###### generate our form values for posting to Alert Pay
	
	# this is the fee we apply to Alert Pay funding
	($main::extras{'ap_additionalcharges'}) = $db->selectrow_array("SELECT calculate_fee_for_alertpay($p->{'amount'})");
	$main::extras{'ap_totalamount'} = $p->{'amount'} + $main::extras{'ap_additionalcharges'};
	$main::extras{'utype'} = DetermineAppType();
	
#	warn 'Leaving PrepareAlertPay()';

	return 1;
}

sub PrepareEcoCard
{
	require EcoCard;
	my $ec = new EcoCard($db);
#	my $m = $main::member;
	my $price = Get_Subscription_Price($p->{'country'});
	$main::extras{'funding_link'} = $ec->FundingRequest({
		'member_id' => $p->{'id'},
		'amount' => $price,
		'description' => 'GPS Subscription',
		'html_safe' => 0
	});
	push @main::cookies, $main::q->cookie(
		'-name'=>'ep_landing_url',
		'-domain'=>'.clubshop.com',
		'-expires'=>'+1y',
		'-value'=>"$main::ME/$p->{'id'}/landing/" . DetermineAppType());
	return 1;
}

sub PrevalidateCardnum
{
	require CreditCard;
	my $rv = CreditCard::validate( $p->{$_[0]} );
	return 1 if $rv;
	push (@main::errors, ['cc_number','cc_number_preval']);
	return 1;
}

sub QuerySubscriptionPricing
{
#	return %STYPES if %STYPES;
	my $country = shift || $main::member->{'country'};
	my $sth = $db->prepare("
		SELECT
			st.subscription_type,
			st.label,
			st.related,
			sp.amount,
			sp.usd,
			sp.currency_code,
			st.sub_class
		FROM subscription_types st
		JOIN subscription_pricing_by_country sp
			ON sp.subscription_type=st.subscription_type
		WHERE sp.country_code= ?
		AND st.active=TRUE
		AND st.sub_class IN ('VM','VA')
	");
	$sth->execute($country);
	while (my $tmp = $sth->fetchrow_hashref)
	{
		foreach (keys %{$tmp})
		{
			$STYPES{ $tmp->{'subscription_type'} }->{$_} = $tmp->{$_};
		}
	}

	return %STYPES;
}

sub Redirect
{
	my $dest = shift || '';

	print $main::q->redirect(
		'-location'=> 'https://www.clubshop.com' . $dest,
		'-cookies'=> @main::cookies
	);
	return;
}

# as of July, Dick wants to extend a special offer to former Partners where they can get back in for the price of this month's Basic subscription
# so we can refer to a table which contains these parties and simply redirect them if they are present in that table
sub Redirect_Priorvs_to_WelcomeBack
{
	# the idea is that the submitted alias will be checked and the numeric ID stored in the params hash, in a prior step like... ValidateAlias()
	if ($p->{'id'} || $p->{'alias'})
	{
		my $qry = '';
		my $arg = uc($p->{'alias'});
		if ($p->{'id'})
		{
			$qry = 'SELECT id FROM priorvs WHERE id=?';
			$arg = $p->{'id'};
		}
		elsif ($p->{'alias'} !~ m/\D/)	# ValidateAlias() requires an alphanumeric 'alias' but we don't care in here
		{
			$qry = 'SELECT id FROM priorvs WHERE id=?';
		}
		else
		{
			$qry = 'SELECT m.id FROM priorvs JOIN members m ON m.id=priorvs.id WHERE m.alias=?';
		}
		
		my ($id) = $db->selectrow_array($qry, undef, $arg);
		Redirect('/cgi/welcomeback.cgi?id=' . $id) if $id;
	}
	
	return 1;
}

sub Redirect_thru_login
{
	my $dest = shift || '';
	my $url = '/cgi/Login.cgi?username=' . ($p->{'alias'} || $main::member->{'alias'});
	$url .= '&password='. ($p->{'password'} || $main::member->{'password'});
	$url .= "&_action=login&destination=$dest";
	return Redirect($url);
}

sub retrieve_tmp_record
{
	###### we should have have an 'id'
	###### and a 'pk' parameter which is really the tmp_id column
	if ($main::member = $db->selectrow_hashref("
		SELECT * FROM upgrade_tmp WHERE tmp_id=? AND id=?", undef,
		($p->{'pk'}, $p->{'id'})))
	{
		return 1;
	}
	main::Err_xml({'list'=>['upgrade_tmp_missing']});
	return;
}

sub tmp_record
{
	my $dbState = $db->{'RaiseError'};
	$db->{'RaiseError'} = 0 if $dbState;
	
	my $id = $p->{'id'} || $main::member->{'id'};

	###### convert our simple code payment method to an actual parameter
	###### that will be used in our records
	$p->{'payment_method'} = $main::cfg->vars('pms')->{$p->{'pm'}}->{'payment_method'};

###### first we have to get any existing 'authorized' value to carry over into the new record
###### if the last stamp is over 24 hours old, we'll let 'em start fresh
	($p->{'authorized'}, $p->{'tmp_id'}) = $db->selectrow_array("
		SELECT
			CASE
				WHEN stamp < NOW() - INTERVAL '24 hours' THEN 0
				ELSE authorized
			END AS authorized,
			tmp_id
		FROM upgrade_tmp
		WHERE id= $id");
	die $DBI::errstr if $DBI::err;
	$p->{'authorized'} ||= 0;

###### although we don't need it at this exact moment,
###### we probably will later for some kind of subsequent access by upline?
###### 'tmp_id' is not very intuitive, but it is a holdover on a column name
	$p->{'tmp_id'} ||= substr(time, -5);

###### at this time we only have one upgrade amount (not anymore), so we'll just stuff it in
#	$p->{amount} = $main::cfg->vars('price');
	$p->{'amount'} = Get_Subscription_Price($p->{'country'} || $main::member->{'country'});

	$db->do("DELETE FROM upgrade_tmp WHERE id = ?", undef, $id);
	if ($DBI::err)
	{
		die "1 DBI problem: $DBI::errstr\nwith transaction in tmp_record()";
	}
	
	my $qry = 'INSERT INTO upgrade_tmp (ipaddr,utype,';
	my $qry_end = 'VALUES (?,?,';
	my @VALS = ($main::q->remote_addr, DetermineAppType());

	foreach (_tmp_cols())
	{
	# for some vals, why set the param hash when we have it in the member hash
		if ((defined $p->{$_}) || (defined $main::member->{$_}))
		{
			$qry .= "$_ ,";
			$qry_end .= '?,';
			push (@VALS, ( (defined $p->{$_} && $p->{$_} gt '') ? $p->{$_} : $main::member->{$_}) );
		}
	}
	chop $qry;
	chop $qry_end;
	$qry .= ") $qry_end )";
	$db->do($qry, undef, @VALS);
	if ($DBI::err)
	{
		die "2 DBI problem: $DBI::errstr\nwith transaction in tmp_record()";
	}
	
	$db->{'RaiseError'} = $dbState if $dbState;
	return 1;
}

sub Trade_Restriction_ck
{
	###### the G_S routine returns an HTML snip which is good enough for an error
	my $rv = G_S::Trade_Restriction_ck($db, $p->{'country'});
	
	###### we'll wrap an error in a 'node' so as to spit it out verbatim
	push (@main::errors, ['country', "<div class='trc'>$rv</div>"]) if $rv;
	return 1;
}

sub Upgrade
{
###### this routine handles the actual upgrade
	my $utype = shift || DetermineAppType();	# landing.pm will pass in an upgrade type
	die "utype: $utype not recognized" unless grep $utype eq $_, (qw/v/);
	
	my ($mtype, $qry_modifier, $mop_field, $paid_thru, $sth) = ();
	my $id = $p->{'id'} || $main::member->{'id'};
	my ($rv, $mopnotes) = ();
	
###### we'll capture the IP address for the records
	my $ipaddr = $main::q->remote_addr();

###### remember that if anything is called after this point
###### it is getting rolled into the whole enchilada
	$db->{'AutoCommit'} = 0;

###### don't forget to reset this before doing a 'return' from this routine

	($rv) = $db->selectrow_array("SELECT upgrade_from_tmp(?,'appx',?)", undef, $id, $mopnotes);
		
	if ($rv ne '1')
	{
		main::Err('There has been a problem while trying to complete your upgrade. A temporary record has been created.');
		$db->rollback;
		$db->{'AutoCommit'} = 1;
		return;
	}

	$db->commit;
	$db->{'AutoCommit'} = 1;

###### the following is for debugging query problems
	$main::SkipDisplay = 1;
	return 1;
}


sub ValidateAlias
{
	# the ID they submitted has to check against a few rules :)
	# it should have already been validated for the basics in the configuration
	return unless $p->{'alias'};

	my $mbr = $db->selectrow_hashref("$main::mbr_qry alias=?", undef, $p->{'alias'});
	unless ($mbr)
	{
		push @main::errors, ['alias', 'invalid_id_submitted'];
		return;
	}
#	if ($mbr->{membertype} !~ /m|s|r|x/)
	unless (grep $_ eq $mbr->{'membertype'}, ('m','s','r','x','tp') )
	{
		my $msg = "This membership doesn't qualify for upgrade: $main::params{'alias'}<br />\n";
		$msg .= "Membertype: (" . uc $mbr->{'membertype'} . ") $G_S::MEMBER_TYPES{$mbr->{'membertype'}}<br />\n";
		$msg .= 'Please contact us for assistance: <a href="mailto:support1@dhs-club.com">support1@dhs-club.com</a>';
		main::Err($msg);
		$main::SkipDisplay = 1;	###### we're done
		return;
	}
	$main::member = $mbr;
	###### put the id into the params hash so it will get carried along
	$p->{'id'} = $mbr->{'id'};

	return 1;
}

sub ValidateSponsorID
{
	###### first we'll upper case it
	my $sponsor = shift || $p->{'sponsorID'};

	###### the only valid sponsoring membertypes as of 12/16/05 are v,m,ag
	my $sponsor_test = sub {grep $_[0] eq $_, ('v','m','ag')};
	my $qry = "
		SELECT m.id, m.alias, m.spid, m.membertype,
			m.firstname, m.lastname,
			COALESCE(m.emailaddress, '') AS emailaddress,
			COALESCE(m.company_name, '') AS company_name
		FROM members m WHERE ";
	my $rv = $db->selectrow_hashref("
		$qry ${\($sponsor =~ /\D/ ? 'm.alias' : 'm.id')} =?
	", undef, $sponsor);

	unless ($rv)
	{
		###### invalid ID altogether - give credit to 01 below
	}
	elsif ( not &$sponsor_test($rv->{'membertype'}) )
	{
	#	###### this will cause the sponsor data, if any to appear in the XML
	#	###### under the data/extras/sponsor node
	#	$main::extras{sponsor} = $rv || {};

	###### let's look upline to find the next valid membership
		my $tmp = {'spid'=>$rv->{'spid'}};
		my $sth = $db->prepare("$qry id=?");
		while ($sth->execute($tmp->{'spid'}) eq '1')
		{
			$tmp = $sth->fetchrow_hashref;
		###### loop buster
			die "Loop detected at id: $rv->{'id'}" if $tmp->{'id'} == $rv->{'id'};
			
			last if &$sponsor_test($tmp->{'membertype'});
		}
		$sth->finish;
		$rv = $tmp;
	}
	###### We may want to provide a verification screen
	###### so they can at least see their sponsor's name
	###### to be sure they entered the right ID.
	###### But, for now, we'll just keep on trucking.
	$p->{'sponsorID'} = ($rv && &$sponsor_test($rv->{'membertype'})) ? $rv->{'id'} : 1;
	return 1;
}

sub _tmp_cols
{
	###### rather than query the DB each time for columns,
	###### we simply provide a list here
	return qw/
id tmp_id alias firstname mdname lastname secfirstname secmdname seclastname
company_name emailaddress emailaddress2 address1 address2 city state postalcode country
cellphone other_contact_info phone fax_num password tin
acct_holder cc_number cc_exp payment_method
mop_address mop_city mop_state mop_postal_code mop_country
amount authorized_date authorized authz_results last_modified card_code language_pref
subscription_type number_of_months
	/;
}

###### ###### only Admin Subs below this line
sub admin_CK
{
	require ADMIN_DB;
	###### do we have database access?
	my $operator = $main::q->cookie('operator');
	my $pwd = $main::q->cookie('pwd');
	###### this effectively changes our DB handle to one owned by the operator
	unless ($db = ADMIN_DB::DB_Connect( 'appx', $operator, $pwd ))
	{
		$main::SkipDisplay = 1;
		return;
	}
	$db->{'RaiseError'} = 1;

	###### do we have accounting access?
	unless (ADMIN_DB::CheckGroup($db, $operator, 'accounting'))
	{
		main::Err('You must have accounting group access to use this function');
		$main::SkipDisplay = 1;
		return;
	}
	return 1;
}

sub admin_retrieve_tmp_record
{
	my $sv = $p->{'id'} || $p->{'alias'};
	###### although main may have picked up a regular member record from an ID
	###### we have to manually search for the record in upgrade_tmp
	my $qry = 'SELECT * FROM upgrade_tmp WHERE ' .
		($sv =~ /\D/ ? 'alias=?' : 'id=?');

	my $member = $db->selectrow_hashref($qry, undef, $sv);
	unless ($member)
	{
		main::Err("No match for $sv<br />
			<a href='#' onclick='self.close()>Close Window</a>");
		$main::SkipDisplay = 1;
		return;
	}

	###### theoretically this cannot happen, but go figure
	###### make sure that the party in question is a potential upgradee
	$main::member = main::Get_Live_Record($member->{'id'}) unless $main::member;
	unless (grep $_ eq $main::member->{'membertype'}, ('s','m','r','x','tp'))
	{
		main::Err("$member->{'alias'} doesn't qualify for upgrade.
			Their membertype: $main::member->{'membertype_label'}");
		$main::SkipDisplay = 1;
		return;
	}

	###### since our template will populate from 'params' at this point in time
	###### we'll fold our data in after converting our NULLS
	G_S::Null_to_empty($member);
	%main::params = (%main::params, %{$member});
	# when the upgrade process is actually happening, the pm parameter is converted to a payment_method column
	# we need to remap that value to a 'parameter' for the sake of the Upgrade() routine
	# this is a brutal hack, but we basically want the CA debit to happen for any payment method
	# and since some various real param values map to the same DB values, we will simply coerce the value to one
	$main::params{'pm'} = lc($member->{'payment_method'});

	###### make this available as the member record too
	$main::member = $member;
	return 1;
}

sub admin_Upgrade
{
	###### we'll use the regular Upgrade routine so that everything happens uniformly
	if ( Upgrade() )
	{	###### Upgrade() flags SkipDisplay since a redirect usually follows
		undef $main::SkipDisplay;
		return 1;
	}
	return;
}

1;

###### 01/16/06 added CheckforDV and modified the Adjust_for_VIP_direct routine
###### also modified the ValidateSponsorID routine
###### also revised the Fraud_Check to ignore all archives and 'live' records older than 1 day
###### 01/18/06 tweaked to allow x members to upgrade and also override the direct-to-VIP
###### route for existing members
###### 02/07/06 added a search for the alias param in Redirect
# 02/09/06 added a criteria check of the 'void' column to the voucher check
# 02/16/06 added a notifications entry into the mix in upgrade()
# 02/22/06 added language_pref to the col lists
###### 02/24/06 added the Notification routine
###### 03/23/06 added admin components
# 08/21/07 added new functionalities for upgrading by PayPal and Moneybookers
# also added the init section
###### 08/28/07 added the inclusion of Club Account entries for CC methods as well
###### Dec. 2007 removed phone_ac and added cellphone & other_contact_info
# 01/02/08 made the 'phone' field submitted to authorize.net = phone OR cellphone
# 08/13/08 added remapping the upgrade_tmp payment_method to a pm 'parameter' in the admin module for retrieving the record
# added Check_Already_Authorized
###### 10/09/08 added passing of the member 'record' to the authorization module for member name/CC name comparison
# 03/17/09 added CreateActivityEntry_app_started()
# 07/14/11 added the capture of the user's IP address with the Authnet request
###### 08/23/11 added the Check_Country_And_Ensure_Card_Authorized method
###### 04/26/12 added the LoadUpgradePricingIntoNonXmlExtras routine
# 06/30/14 had to split up the SQL statements in tmp_record to keep the newer version of DBI happy
# also added saving the RaiseError value and setting it to -0- during processing so that we can trap errors
# 07/21/14 added an additional SQL conditional in QuerySubscriptionPricing() so as to only pull subscription types that are marked as ACTIVE
# 07/30/14 changed the domain in Redirect() from glocalincome to clubshop
# also introduced Redirect_Priorvs_to_WelcomeBack()
# 12/12/14 added CreateActivityEntry_app_submitted()
# 05/05/15 added Check_Authorization()
# 08/14/15 added LoadPayAgentIntoExtras()