package xc;
###### a plugin for the member application
###### this one contains specific routines for the Externally managed Coop app
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: Feb. 2012	Bill MacArthur
###### last modified: 

use strict;
use wrap;

=head1 NAME

	xc - External Coop
	Routines significant to offering interfaces specific to externally managed coops.

=cut

=head2 init

	appx module init() functions are called after loading the module for a given track
	using the package globals is easier than having to explicitly use the main::xxx versions

=cut 

our ($db, $p, $cfg) = ();
sub init {
	$db = $main::db;
	$p = \%main::params;
	$cfg = $main::cfg;
	return 1;
}

=head2 isAllowedDirectSignup

	We are going to check an incoming sponsor ID during step0 and make sure that it is non-house account coop that is allowed to use the direct signup app
	
=cut

sub isAllowedDirectSignup
{
#	die "sponsor=$main::sponsor\n";
	# if our sponsor is not set, then we are done
	unless ( $main::sponsor )
	{
		main::Err('Use of this application is reserved');
		return 0;
	}
	
	my ($rv) = $db->selectrow_array('SELECT direct_signup_allowed FROM coop.coops WHERE coop_id= ?', undef, $main::sponsor->{'id'});
	unless ($rv)
	{
		main::Err("Use of this application is reserved.<br />This ID is not allowed: $main::sponsor->{'id'}");
		return 0;
	}
	return 1;
}

1;
