package af;
###### a plugin for the member application
###### this one contains specific routines for the "af" track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: Aug. 2010	Bill MacArthur
###### last modified: 04/14/16	Bill MacArthur

use strict;
require appx::gi01;
require wrap;

sub debug
{
	warn "in debug";
}

=head3 deflect_house_to_xc

Determine if the sponsor of the applicant is a house account of the correct type
If so, redirect them to the direct signup version

=head4 Arguments

Accepts a hashref recognizing these keys:
	ids => a reference to an array of numeric IDs
	house_acct_type => one of 'CS' for Clubshop -or- 'GI' for Glocal Income (basically the types in the coop.coops table)

=head4 Returns

1 if the referring sponsor is not a house account
otherwise issues a redirect to the client

=cut

sub deflect_house_to_xc
{
	my $args = shift;
	my @ids = @{$args->{'ids'} || []};
	my $type = $args->{'house_acct_type'} || 'CS';	# our default
	unless (@ids)
	{
		my $sth = $main::db->prepare("SELECT coop_id FROM coop.coops WHERE house_acct=TRUE AND house_acct_type= ?");
		$sth->execute($type);
		while (my $tmp = $sth->fetchrow_array())
		{
			push @ids, $tmp;
		}
	}
	
	if (grep $_ == $main::params{'sponsorID'}, @ids)
	{
		print $main::q->redirect('https://www.clubshop.com/cgi/appx.cgi/' . $main::params{'sponsorID'} . '/xc');
		$main::SkipDisplay = 1;
		return;
	}

	return 1;
}

sub get_sponsor_from_params
{
	if ($main::q->param('sph')){
		main::ManageSPH('decrypt');
		return 0 unless main::SponsorCK();
	}
	else {
		main::DetermineSponsor();
		# SponsorCK() is invoked from within DetermineSponsor()
	}
	return 1;
}

1;

__END__

10/13/15	Added deflect_house_to_xc()
04/14/16	Added nothing after all... ended up putting deflect_house_to_xtp() directly into appx