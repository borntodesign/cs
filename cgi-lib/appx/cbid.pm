package cbid;
###### a plugin for the member application
###### this one contains ClubBucks activation specific routines
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name													
###### deployed: 09/05
###### last modified: 11/03/05	Bill MacArthur

###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use Digest::MD5 qw( md5_hex );
use strict;

###### package Globals
our ($cbid) = ();

sub Create_and_Activate
{	###### create a membership and link the ClubBucks card to it
	# setup a 'temporary' password
	main::Create_Password();

	my $memo = "Created in conjunction with a ClubBucks card activation.\n" .
		'IP: ' . $main::q->remote_addr . "\n";
	if ($main::VIP_login)
	{
	###### disallow activation of cards referred by other parties
	###### our cbid var should be set by now
		if ($cbid->{referral_id} && $cbid->{referral_id} != $main::mem_info->{id})
		{
			main::Err("Cards activated by VIPs must be under control of the VIP performing the activation.");
			return;
		}
			
		$memo .= "VIP action by: $main::mem_info->{alias}" ;
	}

	###### since we want this to be transactionally safe, we will modify the 
	###### default auto-commit behaviour until we are done
	my %specifics = (
		memo	=> $memo
	);
	
	$main::db->begin_work;
	my $rv = ($main::member = main::_create_membership(\%specifics));
	unless ($rv)
	{
		main::Err_xml(['member_creation_failure']);
		$main::db->rollback;
		$main::db->{AutoCommit} = 1;
		return;
	}

	###### now activate the card
	$rv = $main::db->do("UPDATE clubbucks_master SET
		class= 4,
		current_member_id= $main::member->{id},
		operator= 'member application',
		label= 'Initial',
		stamp= NOW(),	
		memo= 'activated in conjunction with a new member application'
		WHERE 	id= ?::bigint;\n", undef, $main::params{cbid});
	unless ($rv)
	{
		main::Err_xml(['card_activation_failure']);
		$main::db->rollback;
		$main::db->{AutoCommit} = 1;
		return;
	}

	$main::db->commit;	###### this automatically turns auto-commit back on
	return 1;
}

sub Check_Membership_Dupe
{
	###### For now we will simply *not* check for dupes if they are applying
	###### for a shopper membership.
	###### We could do more by actually calling the G_S routine ourselves and parsing
	###### the results or even doing our own query, but until it becomes an issue
	###### we'll go this route.
	return 1 if ($main::params{membertype} || '') eq 's';
	return main::Check_Membership_Dupe('cb');
}

sub ValidateCBID
{
###### we're actually validating that the values passed have not been munged
###### and that the CBID has not been activated by another invocation
	if ( $main::params{cbh} eq md5_hex($main::SALT . $main::params{cbid} . $main::sponsor->{id}) )
	{
		###### if we retrieve a record, we're all set, if not, then we can safely
		###### assume that it must have been activated since the hashing passed		
		unless (_get_cbid($main::params{cbid}))
		{
			main::Err_xml({list=>['non_existent_card']});
		}
		elsif (! grep $cbid->{class} == $_, (2,3,7))
		{
			main::Err_xml({list=>['card_already_activated']});
		}
		else
		{
			###### I guess we're all set.
			return 1;
		}
	}
	else { main::Err('cbid/cbh parameter mismatch') }
	return;
}

sub _get_cbid
{
	my $id = shift;
	return ( $cbid = $main::db->selectrow_hashref("
		SELECT *
		FROM 	clubbucks_master
		WHERE 	id= ?::bigint", undef, $id) );

}

1;

###### 11/03/05 added the dupe checker routine and fixed an = syntax err in the validator
