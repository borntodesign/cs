package resend;
###### a plugin for the member application
###### this one contains specific routines involved in
###### resending the 'confirm your membership' email
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
												
###### deployed: 09/15/05	Bill MacArthur
###### last modified: 02/24/11	Bill MacArthur
		
###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

sub Email
{
	###### because we cannot be sure of which track they came in on,
	###### we cannot simply send a generic email.
	###### we'll receive our default just like the main::Email routine would
	###### but we'll parse the memo field looking for a specific template

	my ($tmpl, $html) = main::Parse_Memo_for_Resend();
	my $rv = ();
	if ($html){
		$rv = main::EmailHtmlMailings($tmpl);
	}
	else {
		$rv = main::Email({xsl=>$tmpl  }, $main::members->{'language_pref'});
	}
	die "Failed to resend confirmation email\n" unless $rv;

	return 1;
}

sub ValidateID
{	# make sure the received ID which could be an alias or an ID if coming from admin
	# is valid and not already confirmed
	die "No ID or alias received\n" unless $main::params{'id'} || $main::params{'alias'};
	my @list = ();
	my $qryfrag = '';
	if ($main::params{'id'})
	{
		$qryfrag = 'id=?';
		@list = $main::params{'id'};
	}
	else
	{
		$qryfrag = 'alias=?';
		@list = uc $main::params{'alias'};
	}
	unless ( $main::member = $main::db->selectrow_hashref(
		"$main::mbr_qry $qryfrag", undef, @list ))
	{
		main::Err('No matching membership found');
		return;
	}
	elsif ($main::member->{membertype} ne 'x')
	{
		main::Err_xml({list=>['already_confirmed']});
		return;
	}
#	main::Prepare_UTF8($main::member);
	main::DetermineSponsor($main::member->{spid});
	return 1;
}

1;

###### 11/10/05 added the Email routine
# 02/08/06 dropped the Prepare_UTF8 call