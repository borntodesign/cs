package admin;
###### a plugin for the member application
###### this one contains specific routines for admin work
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
												
###### deployed: 09/15/05	Bill MacArthur
###### last modified: 11/27/06 Shane Partain
		
###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

sub confirmAG
{
	my $rv = $main::db->do("UPDATE members
				SET membertype='ag'
				WHERE id= $main::params{id}
				AND membertype='xag'");
	my $rv1 = '';
	if ($rv eq '1'){
		$rv1 = $main::db->do("INSERT INTO notifications (id, notification_type)
					 VALUES($main::params{id}, 80)");
	}
	if (($rv eq '1') && ($rv1 eq '1'))
        {
                print "Content-type: text/html\n\n",
                        qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">|,
                        qq|<html xmlns="http://www.w3.org/1999/xhtml"><head><title></title></head>|,
                        qq|<body onunload="opener.location.reload()">|,
                        "<h3>Membership confirmation successful</h3>\n",
                        qq|<p><a href="javascript::void(0)" onclick="self.close(); return false;">Close Window</a></p>\n|,
                        '</body></html>';
        }
        else { main::Err("Confirmation failed:<br />" . $DBI::errstr) }
        return;	
}

sub deleteID
{
	###### all the params should be in place so we won't check
	###### it's admin mode anyway ;-)
	my $rv = $main::db->do("
		DELETE FROM activity_rpt_data WHERE id= $main::params{id};
		DELETE FROM members WHERE id= $main::params{id}");

	if ($rv eq '1')
	{
		print "Content-type: text/html\n\n",
			qq|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">|,
			qq|<html xmlns="http://www.w3.org/1999/xhtml"><head><title></title></head>|,
			qq|<body onunload="opener.location.reload()">|,
			"<h3>Membership deletion successful</h3>\n",
			qq|<p><a href="javascript::void(0)" onclick="self.close(); return false;">Close Window</a></p>\n|,
			'</body></html>';
	}
	else { main::Err("Deletion failed:<br />" . $DBI::errstr) }
	return;
}

sub Extract_Resend_Track
{
	$main::member->{memo} =~ /resend:\d+:([a-z]+)/;
	$main::extras{confirm_track} = $1 || 'c';
}
1;

###### 11/10/05 added the track extraction sub
# 11/21/06 added a reload call for the opener of the deletion window
# 11/27/06 added function for confirming AGs
