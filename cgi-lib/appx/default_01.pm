package default_01;
###### a plugin for the member application
###### this one contains specific routines for the default_01 track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: May 2005	Bill MacArthur
###### last modified: 04/29/10	Bill MacArthur

###### REMEMBER, the live app may run in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

=head3 deflect_all_except_house

Determine if the sponsor of the applicant is a house account of the correct type
If not, send them on their way to the correct application

=head4 Arguments

Accepts a hashref recognizing these keys:
	ids => a reference to an array of numeric IDs
	house_acct_type => one of 'CS' for Clubshop -or- 'GI' for Glocal Income (basically the types in the coop.coops table)

=head4 Returns

1 if a match is found
otherwise issues a redirect to the client

=cut

sub deflect_all_except_house
{	# we only want 01 or house sponsorships coming through here
	my $args = shift;
	my @ids = @{$args->{ids} || []};
	my $type = $args->{house_acct_type} || 'CS';	# our default
	unless (@ids){
		my $sth = $main::db->prepare("SELECT coop_id FROM coop.coops WHERE house_acct=TRUE AND house_acct_type= ?");
		$sth->execute($type);
		while (my $tmp = $sth->fetchrow_array()){
			push @ids, $tmp;
		}
	}
	return 1 if grep $_ == $main::params{sponsorID}, @ids;

	print $main::q->redirect('https://www.clubshop.com/cgi/appx.cgi/' . $main::params{sponsorID});
	$main::SkipDisplay = 1;
	return;
}

sub Create_Membership
{	###### we just need to do some additional tweaking of the memo for this case
	###### if they did end up being assigned to the referral, we'll still know how
	###### they came in
	my $memo = "Signed up at 01\nAny referral information follows:\n";
	$memo .= $main::params{'referral'} || '';
	return main::Create_Unconfirmed_Membership({memo=>$memo});
}

=head3 ProcessReferral

Try to determine who may have referred the applicant based on a submitted piece of information.
This was originally written for strictly double optin applications, so we simply changed who the sponsor was and kept on trucking

=head4 Arguments

None, although this routine looks at main::params{'referral'} and main::params{'referral_txt'}

=head4 Returns

Returns 1 and if a sponsor is found and changed to, the sponsor hashref is returned as a second value

=cut

sub ProcessReferral
{
	my $referral = $main::params{'referral'};
	###### this is the simplest place to use the extraneous referral info
	$main::params{'referral'} .= "\n$main::params{'referral_txt'}" if $main::params{'referral_txt'};
	return 1 unless $referral;

	my $criteria = ();
	###### do we have a simple email address?
	if ($referral =~ /@/)
	{
		$criteria = 'emailaddress';
		$referral = lc $referral;
	}
	###### maybe we have an alias
	elsif ($referral =~ /^[a-z]+-*\d+$/i)
	{
		$criteria = 'alias';
		$referral = uc $referral;
	}
	###### maybe we have a numeric ID
	elsif ($referral !~ /\D/)
	{
		$criteria = 'id';
	}
	###### maybe we have something else
	###### we won't try to develop any further tests at this point
	else { return 1 }

	###### If we are searching by email address and there are multiple matches,
	###### we are, unfortunately, going to get the first one off the list.
	###### We could refine the query, but the whole concept of duplicate
	###### email addresses in the system ensures that we are going to have a problem
	###### if we try to get 'smart'. There is no smart way except to correspond
	###### with the new member and see who they really had in mind.
	# for now we'll just take the first match :)
	my $sp = $main::db->selectrow_hashref("$main::mbr_qry $criteria =?",
		undef, $referral);
	return 1 unless $sp;

	###### let's see if this membership is sponsor material
	###### we'll just use the main routine and let it change all the sponsor info
	###### if this party or someone upline is good
	main::DetermineSponsor($sp->{id});

	###### at this point we could advise them of the results and so on...
	###### but for now we'll just silently keep moving along
	return (1, $sp);
}

1;

###### 07/27/06 enhanced the ProcessReferral to recognize a numeric ID as well
###### as to prepend the extra referral_txt value onto the referral param for storage later on

