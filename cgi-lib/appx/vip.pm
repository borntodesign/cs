package vip;
###### a plugin for the member application
###### this one contains specific routines for the vip track
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
###### deployed: Sept. 2005 	Bill MacArthur
###### last modified: 09/12/07	Bill MacArthur

###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;
use EventLogin;
my $Event;
sub init { $Event = EventLogin::ValidateTicket($main::q->cookie('EventLogin')); }

sub coerce_sponsorship_to_self
{
	# this routine disallows 'direct' signups by a VIP under anybody but themselves
	if ($main::mem_info->{id} != $main::sponsor->{id} && ! $Event)
	{
		main::DetermineSponsor($main::mem_info->{id});
		push @main::errors, ['generic_error', 'direct_signup1'];
	}
	return 1;
}

sub Create_Membership
{
	# we will assign them a simple password, which they can change at a later date
	main::Create_Password();	###### this gets place in the main:params hash
	my $memo = '';
	$memo = "Event signup: $Event->{eventname}\n" if $Event;
	$memo .= "VIP direct signup by: $main::mem_info->{alias}\n" . $main::q->remote_addr;
	my $args = {
		memo			=> $memo,
		mail_option_lvl	=> 2
	};

	###### set our default if the membertype setter has been removed from the HTML
	$args->{membertype} = 'm' unless $main::params{membertype};

	return ($main::member = main::_create_membership($args)) ? 1 : undef;
}

sub Email
{
	###### the member welcome letter needs to go to the language spec chosen
	###### rather than the VIP user's browser selection which is the default
	###### we will simply call the main routine with the necessary lang_spec argument
	my $tmpl = shift;
	main::Email($tmpl, ($main::member->{language_pref} || 'en'));
}
1;

# 09/12/07 hooked up the concept of an Event Login