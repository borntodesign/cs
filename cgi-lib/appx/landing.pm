package landing;
# deployed 11/10	Bill MacArthur
# the track that uses this module also needs a piece from the upg module
require appx::upg;

my $utype = 'v';

my %utypes = (
	'v' => {'membertype'=>'v','congrats'=>'/congrats/congrats1.xml'}
);

sub init
{
	upg::init();
	return 1;
}

sub Landing
{
	my $db = $main::db;
	###### we'll hit this routine first after coming back from PayPal, MoneyBookers, etc.
	###### basically if we are VIP now, we're all set
	###### if not we'll do the regular funds checking/updating and if good, we're all set
	###### if that all doesn't work,
	###### we are going to return accordingly so they can be redirected to a static help page
	# since we are coming in without all of our data structures in place,
	# we'll have to rely on the path-embedded alias
	# we may as well set the global member var while we are at it
	die "No embedded ID received\n" unless $main::path_info[1];
	
	my $m = $main::member = main::Get_Live_Record($main::path_info[1]);
	die "Failed to lookup member record for: $main::path_info[1]\n" unless $m->{'id'};

	# if we are not doing a VIP upgrade, we should have the upgrade type available as an additional bit of the URL
	# appx.cgi/BM000012/landing for a VIP upgrade
	# appx.cgi/BM000012/landing/amp for an AMP upgrade, etc.
	$utype = $main::path_info[3] || 'v';

	push @main::cookies, $main::q->cookie('-name'=>'id', '-value'=>$m->{'id'});
	return 1 if $m->{'membertype'} eq $utypes{$utype}->{'membertype'};

	###### can lookup their tmp record by simply their ID
	###### we don't do it elsewhere as we are displaying information in the app
	###### that is not the case here, so there is no breach of privacy
	my $tmprec = $db->selectrow_hashref("SELECT * FROM upgrade_tmp WHERE utype='$utype' AND id = $m->{'id'}");
	die "Failed to locate a type $utype upgrade_tmp record for $main::path_info[1]\n" unless $tmprec->{'id'};
	# now we have the price of the upgrade :)

	###### rather than trying to deal with the applicant trying to refresh the page if payment hasn't posted
	###### we'll delay a bit an keep retrying ourselves
	my $rv = ();
	for (my $x = 0; $x < 4; $x++)
	{
		($rv) = $db->selectrow_array("
			SELECT SUM(amount) FROM soa WHERE void = FALSE AND id = $m->{'id'} HAVING SUM(amount) >= ?",
			undef, $tmprec->{'amount'});
		last if $rv;
		sleep 8;
	}
	
	unless ($rv)
	{
		# in the unlikely event that the funds arrived and they were upgrade by a backend while we were waiting
		return 1 if $db->selectrow_array("SELECT membertype FROM members WHERE id = $m->{'id'}") eq $utypes{$utype}->{'membertype'};

		###### if they don't have enough funds in their account, then we've got a problem ;)
		main::Err_xml({'list'=>['funding-prob']});
# 		main::Err('There is an issue with your Club Account balance.
# 			Please <a href="/contact.xml">contact us</a>.');
		return undef;
	}

	my $error = '';
	
#	if ($utype eq 'v')
#	{
#		upg::init();
#		$rv = upg::Upgrade();
#	}
#	elsif ($utype eq 'amp')
#	{
#		require appx::amp;
#		amp::init();
#		$rv = amp::Upgrade();
#	}
#	elsif ($utype eq 'ampv')
#	{
#		require appx::ampv;
#		ampv::init();
#		die "no ampv upgrade yet";
#		$rv = ampv::Upgrade();
#	}
	if (grep $utype eq $_, (qw/v/))
	{
		upg::Upgrade($utype);
	}
	else
	{
		die "Invalid utype: $utype in landing::Landing";
	}
	
	unless ($rv)
	{
		$error = $DBI::errstr ? $DBI::errstr : 'Upgrade from temp failed in upg::Landing';
	}

	if ($error)
	{
		main::Err($error);
		return undef;
	}
	return 1;
}

sub Redirect
{
	return upg::Redirect($utypes{$utype}->{'congrats'});
}

1;

