package debug;
###### a plugin for the member application
###### this one contains specific routines for debugging
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
# the technique for using this on the live app is to create an operation
# require 'appx/debug.pm'; debug::whatever_function_you_want_to_use
# if you call 'die' in your function, then processing will stop
# of course, if you return undef, the loop will probably goto the end anyway :)		
				
###### deployed: 05/05/05	Bill MacArthur
###### last modified: 05/12/05
		
###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

sub debug
{
	print "Content-type: text/plain\n\n";
	foreach (keys %main::params){print "$_:$main::params{$_}\n";}
	print "\nSponsor=\n";
	foreach (keys %{$main::sponsor}){print "$_:$main::sponsor->{$_}\n";}

	return;
}

1;


