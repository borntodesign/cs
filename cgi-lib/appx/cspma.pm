package cspma;
use strict;

=head1 NAME

	cspma - The clubshop.com version of the Potential Merchant Affiliate application
	This is a new concept as of late April '09, the idea is that URL will be hosted on clubshop.com
	and that the XSL template will be merchant friendly (absent VIP stuff)

=cut

=head2 init

	appx module init() functions are called after loading the module for a given track
	using the package globals is easier than having to explicitly use the main::xxx versions

=cut 

#our ($db, $p, $cfg, $sponsor) = ();
sub init {
#	$db = $main::db;
#	$p = \%main::params;
#	$cfg = $main::cfg;
#	$sponsor = $main::sponsor;	# sponsor is not initialized when init is called
	return 1;
}

=head2 isHouseAccount

	We are going to check an incoming sponsor ID during step0 and make sure that it is being used by a house account.
	No indication has been given to allowing it's general usage, so we will exclude it using this check
	
=cut

sub isHouseAccount
{
#	die "sponsor=$main::sponsor\n";
	# if our sponsor is not set or our spid is greater than 2, then we are not a house account
	unless ( $main::sponsor && ($main::sponsor->{spid} <= 2) && $main::sponsor->{membertype} eq 'v')
	{
		main::Err('Use of this application is reserved');
		return 0;
	}
	return 1;
}

1;