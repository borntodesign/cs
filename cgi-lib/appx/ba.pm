package ba;
###### a plugin for the member application
###### this one contains specific routines for ba track applications
# all the global vars available in the 'main' package are available as main::varname
# the same holds true for 'main' subroutines main::subroutine-name
												
###### deployed: 09/26/05	Bill MacArthur
###### last modified:
		
###### REMEMBER, the live app runs in mod_perl and any changes made here will not
###### be reflected/available until apache is stopped and started again
###### changes will be available immediately in a regular cgi environment ie. cgi/tmp

use strict;

sub ValidateSponsor
{
# there are no more roles, so just let 'em do whatever since the scenario has not been dealt with
	return 1;
	# ths sponsor that has been calculated should be at the T3 role or higher
	###### all the params should be in place so we won't check
	###### it's admin mode anyway ;-)
	return 1 if $main::db->selectrow_array("
		SELECT level FROM nbteam_vw WHERE level >= 3 AND id= $main::sponsor->{id}");
	
	print $main::q->redirect( $main::q->url . '/1/ba' );
	return;
}

1;

