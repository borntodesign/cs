package DoTT_Template;
use Template;

=head3 DoTT_Template

Initially being built as an accessory for occasional use by appx
Some documents that are heavily formatted do not do well as XML/XSLT
We will alleviate that problem by simply using an HTML template that can be directly translated.

=head4
We are simply going to export out main routine so that all the vars available will be available to Template
Since we will have to pull a template, main will have to be sure to have a $db initialized and have G_S loaded
=cut

=head4 Do()

We will minimally need:
	{
		db => <an active DB handle>,
		gs => <a G_S object>,
		cgi => <a cgi object>,
		tmpl => <a template ID>,
		data =>{label=> <some variable>}
	}
Optionally we can send in a lang_pref to coerce the template to that language
We can also send in a 'wrap' object on a "wrap" key and the content will be wrapped before printing

=cut

sub Do
{
	my ($rv) = ();
	my $args = shift;
	my $TT = Template->new();
	return undef unless my $tmpl = $args->{'gs'}->Get_Object($args->{'db'}, $args->{'tmpl'}, $args->{'lang_pref'});
	$TT->process(\$tmpl, $args->{'data'}, \$rv);

	$rv = $args->{'wrap'}->wrap({'content'=>$rv}) if $args->{'wrap'};
	print $args->{'cgi'}->header('-charset'=>'utf-8', '-cookies'=>\@main::cookies), $rv;
	return 0;
}

1;