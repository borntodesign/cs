package appx::APPX;
use strict;

###### ###### THIS IS THE CLUBSHOP VERSION ###### ######

our %APPXDEFS = (
	'ACCTTYPE' => 'CS',
	'DEF_HOUSE_APP_TRACK' => 'default_01',
	'APPXLIBS' => '/home/httpd/cgi-lib/appx'
);

use base 'Exporter';
our @EXPORT_OK = qw(%APPXDEFS);

1;