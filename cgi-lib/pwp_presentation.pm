package pwp_presentation;
# the page development and presentation module for the Personalized Web Pages
#
#
use strict;
use HTTP::Date;
use lib '/home/httpd/cgi-lib';
use XML::local_utils;
use G_S;

# the key is the profile version number
# we are developing this in anticipation of various changes after the initial 'beta' of 1
my %TMPL = (
	1 => {xml=>10583, xsl=>10584},
	2 => {xml=>10583, xsl=>10237}
);

sub Render($$$$;$){
	# $page is a hashref to the page record with a few extra fields generated in the SQL that pulls them
	# $member is a hashref of some of the member data matching the ID of the page record
	# $args is a hashref for some additional stuff like {xml=>1} for just spitting out XML
	my ($db, $q, $page, $member, $args) = @_;
	my $tools = new XML::local_utils;

	die "Invalid profile_version\n" unless $page->{profile_version} && $TMPL{$page->{profile_version}}{xsl};
	my ($xsl) = $db->selectrow_array('SELECT obj_value FROM cgi_objects WHERE obj_id= ?', undef, $TMPL{$page->{profile_version}}{xsl}) ||
		die "Failed to retrieve the XSL template\n";
	$q->server_name =~ /(\w+\.\w+)$/;
	my $server_name = $1;
	my $xml = "<root>\n";
	$xml .= G_S::Get_Object($db, $TMPL{$page->{profile_version}}{xml}, ($page->{language_code} || 'en')) ||
		die "Failed to retrive the xml base template\n";

	###### now convert newlines to HTML breaks
	$page->{profile_xml} =~ s#\n#<br />#g;

	$xml .= "$page->{profile_xml}\n<member>\n";
	$xml .= $tools->hashref2xml($member);
	$xml .= "</member><data><domain_name>$server_name</domain_name>\n";
	$xml .= "<page><pk>$page->{pk}</pk><language_code>$page->{language_code}</language_code>
		<page_name>$page->{page_name}</page_name></page>\n";
	$xml .= '</data></root>';
	$xml = $tools->xslt($xsl, $xml);
	my $length = length($xml);
	if ($args->{xml}){
		print $q->header('text/plain'), $xml;
		return;
	}
	print $q->header(
		-charset=>'utf-8',
		-'Content-Language'=> ($page->{language_code} || 'en'),
		-'Last-Modified'=> time2str($page->{perltime} || time),
		-'Content-Length'=>$length);
	print $xml;
}
