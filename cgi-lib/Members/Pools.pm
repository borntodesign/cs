package Members::Pools;
use Encode;
=head1 NAME:
Members::Pools

	General descripton of the Class.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
How To Use This Class:

	Verbose Description of the class, including how to use it without code examples.

=head2
Code Example

	Code Example Goes Here

=head1
PREREQUISITS:

	Other classes, or packages that are used

=cut

use base qw( Members );


=head2
METHODS

=head2
PRIVATE

=head3
privateMethodName

=head4
Description:

	Description of what is set in __init.

=head4
Parameters:

	hashref
		hash_index	string Description of what this holds

=head4
Return:

	string on error, or null if all goes well.
=cut

sub _init
{

	my $this = shift;

	$this->{'version'}						= '2';
	$this->{'name'}							= 'Members::Pools';
	$this->{'member_info_by_id'}			= {};
	$this->{'member_info_by_alias'}			= {}; # This is a hashref of references when using it's values use ${$this->{member_info_by_alias}}->{FIELD_NAME},
	$this->{'member_mop_info_by_id'}		= {};
	$this->{'member_pool_by_id'} 			= {};
	$this->{'member_pool_upgrades_by_id'}	= {};
    $this->{'member_info_by_spid'}			= {};
}


#=head3
#__updateCache
#
#=head4
#Description:
#
#	Get the users information by the "Member ID" or "Alias"
#	and assign the data to the "member_info_by_id" cache
#	and the "member_info_by_alias" cache.
#
#=head4
#Params:
#
#	hashref
#		member_id	bool	if true use the 'id' else use 'alias'
#		id		string	MemberID
#
#=head4
#Return:
#
#	string	on error
#=cut
#
#sub __updateCache
#{
#	my $this = shift;
#	my $args = shift;
#	my $field = $args->{member_id} ?'id':'alias';
#
#	my $query = "SELECT pm.* FROM pool_members AS pm WHERE pm.$field = ?";
#
#	my $return_message = '';
#	
#	if (!defined $args->{id})
#	{
#		return "The 'id' is a required parameter.";
#	}
#	
#	eval{
#
#		my $sth = $this->{db}->prepare($query);
#		$sth->execute($args->{id});
#		my $rows = $sth->fetchrow_hashref;
#
#		if($rows)
#		{
#			
#			$this->{member_info_by_id}->{$rows->{id}} = $rows;
#			
#			############################
#			# Here we use a reference
#			# so we don't turn into a
#			# memory hog.
#			############################
#			$this->{member_info_by_alias}->{$rows->{alias}} = \$this->{member_info_by_id}->{$rows->{id}};
#			
#		}
#		else
#		{
#			die "The member was not found. The Member ID, or the Member Alias may be incorrect. ";
#		}
#
#	};
#	if ($@)
#	{
#		return "The cache was not updated due to an error. Query: $query -- ID: $args->{id} -- " .  $@;
#	}
#
#	return $return_message;
#}



=head3
__updatePoolUpgradeCache

=head4
Description:

	Retrieve a pool, and store it in the cache, we also cache the members retrieved so if you need member information, retrieve it after you retrieve the pool so we only call the database once.

=head4
Params:

	id
	INT
	Pool ID

=head4
Return:

	string	on error
=cut

sub __updatePoolUpgradeCache
{
	my $this = shift;
	my $id = shift;
	
	return "Members::__updatePoolUpgradeCache -- 'id' is a required parameter." if (!defined $id || $id !~ /^\d+$/);
		
	my $query =<<EOT;

	SELECT
		pu.*
	FROM 
		pool_upgrades pu
	WHERE
		pu.pool_id = ?
	ORDER BY
		pu.vip_date

EOT

	
	my $return_message = '';
	

	
	eval{

		my $sth = $this->{'db'}->prepare($query);
		$sth->execute($id);
		
		
		
		
		while(my $rows = $sth->fetchrow_hashref)
		{
			
			
			$this->{'member_pool_upgrades_by_id'}->{$rows->{'pool_id'}} = [] if ! exists $this->{'member_pool_upgrades_by_id'}->{$rows->{'pool_id'}};

			push(@{$this->{'member_pool_upgrades_by_id'}->{$rows->{'pool_id'}}}, $rows);

			
			#Since we are here we might as well cache the member information, the only addition is the number_of_members_sponsoring column.
			$this->{'member_info_by_id'}->{$rows->{id}} = $rows;
			
			############################
			# Here we use a reference
			# so we don't turn into a
			# memory hog.
			############################
			$this->{'member_info_by_alias'}->{$rows->{'alias'}} = \$this->{'member_info_by_id'}->{$rows->{'id'}};
			
		}


	};
	if ($@)
	{
		return "Members::__updatePoolUpgradeCache -- The cache was not updated due to an error. Query: $query -- ID: $id -- " .  $@;
	}

	return $return_message;
}


=head3
__updatePoolCache

=head4
Description:

	Retrieve a pool, and store it in the cache, we also cache the members retrieved so if you need member information, retrieve it after you retrieve the pool so we only call the database once.

=head4
Params:

	id
	INT
	Member ID, or Pool ID
	
	by_pool_id
	BOOL
	If you are retrieving the pool information by the pool ID set this to true (1);

=head4
Return:

	string	on error
=cut

sub __updatePoolCache
{
	my $this = shift;
	my $id = shift;
	my $by_pool_id = shift;
	
	return "Members::__updatePoolCache -- 'id' is a required parameter." if (!defined $id || $id !~ /^\d+$/);
	
	my $query =<<EOT;

	SELECT DISTINCT
		--pm.*,
		pm.id, pm.spid, pm.alias, pm.firstname, pm.lastname,
		COALESCE(pm.country,'') AS country, COALESCE(pm.state, '') AS state, pm.pool_id,
		pm.display_code,
		pm.entry_date,
		pm.upgrade_deadline,
		ps.cnt,
		t.id AS transactions_id,
		pm.membertype
	FROM
		pool_members_for_pool_reports pm
	LEFT JOIN
		ps_cnt_for_pool_views ps
		ON 
			ps.id=pm.id
	LEFT JOIN
		transactions t
		ON
			t.id=pm.id
		AND
			t.trans_type IN (2,3,5,6)
		AND
			t.entered BETWEEN first_of_month() AND last_of_month()
	WHERE
		pm.pool_id = ?
	ORDER BY pm.id
EOT
	
#	$query = $by_pool_id ? " $query = ? " :" $query IN (SELECT pool_id FROM pool_members_for_pool_reports WHERE id = ?) ";
	# the original query was horribly inefficient when looking strictly by ID as it it using a SQL "IN"
	# so we will look up the pool ID and if we do not find one, then we are done
	unless ($by_pool_id)
	{
		my ($_id) = $this->{'db'}->selectrow_array("SELECT pool_id FROM member_pools WHERE member_id=?", undef, $id);
		
		return "Members::__updatePoolCache -- pool_id could not be found for $id" unless $_id;
		$id = $_id;
	}
	my $return_message = '';
	
#	eval{

		my $sth = $this->{'db'}->prepare($query);
		$sth->execute($id);
		
		
		while(my $rows = $sth->fetchrow_hashref)
		{
			foreach (qw/firstname lastname/)
			{
				$rows->{$_} = decode_utf8($rows->{$_}) unless Encode::is_utf8($rows->{$_});
			}
			$this->{'member_pool_by_id'}->{$rows->{'pool_id'}} = [] if(! exists $this->{'member_pool_by_id'}->{$rows->{'pool_id'}});


			push(@{$this->{'member_pool_by_id'}->{$rows->{'pool_id'}}}, $rows);

			
			#Since we are here we might as well cache the member information, the only addition is the number_of_members_sponsoring column.
			$this->{'member_info_by_id'}->{$rows->{'id'}} = $rows;
			
			############################
			# Here we use a reference
			# so we don't turn into a
			# memory hog.
			############################
			$this->{'member_info_by_alias'}->{$rows->{'alias'}} = \$this->{'member_info_by_id'}->{$rows->{'id'}};
			
		}


#	};
#	if ($@)
#	{
#		warn "Members::__updatePoolCache -- The cache was not updated due to an error. Query: $query -- ID: $id -- " .  $@;
#		return "Members::__updatePoolCache -- The cache was not updated due to an error. Query: $query -- ID: $id -- " .  $@;
#	}

	return $return_message;
}



=head2
PUBLIC


=head3
getMemberPoolByMemberID

=head4
Description:

	Retrieve the pool by a member who is in the pool.
	When we retrieve the pool we also cache member information,
	so if you need information for the member, or members in the pool
	use getMemberInformationByMemberAlias, and 
	getMemberInformationByMemberID after you call this
	method.

=head4
Params:

	member_id	int	MemberID
	force		bool	(Optional) Set to true if you want 
						the latest information from the database

=head4
Returns:

	hashref = 
	{
		pool_id INT The ID of the member sponsoring the pool,
		pool	arrayref	The members in the pool.
	}

=cut

sub getMemberPoolByMemberID
{

	my $this = shift;
	my $member_id = shift;
	my $force = shift;
	$force = 0 if !defined $force;
	
	$member_id =~ s/\s+//g;
	
	die 'Members::getMemberPoolByMemberID - The member_id parameter was not defined, or held the incorrect information only integers are allowed.' if (! defined $member_id || $member_id !~ /^\d+$/);
	
	my $pool_id = $this->{'member_info_by_id'}->{$member_id}->{'pool_id'} if (exists $this->{'member_info_by_id'}->{$member_id} && $this->{'member_info_by_id'}->{$member_id}->{'pool_id'});
	return {'pool_id'=>$pool_id, 'pool'=>$this->{'member_pool_by_id'}->{$pool_id}} if ($pool_id && ! $force && exists $this->{'member_pool_by_id'}->{$pool_id} );
	if (! $this->__updatePoolCache($member_id) && exists $this->{'member_info_by_id'}->{$member_id} && exists $this->{'member_info_by_id'}->{$member_id}->{'pool_id'})
	{
		return {'pool_id'=>$this->{'member_info_by_id'}->{$member_id}->{'pool_id'}, 'pool'=>$this->{'member_pool_by_id'}->{$this->{'member_info_by_id'}->{$member_id}->{'pool_id'}} };
	}
	
	return;

}


=head2
PUBLIC


=head3
getMemberPoolUpgradesByPoolId

=head4
Description:

	Retrieve the pool upgrades by the Pool ID.
	When we retrieve the pool upgrades we also cache member information,
	so if you need member information for an upgraded member
	use getMemberInformationByMemberAlias, and 
	getMemberInformationByMemberID after you call this
	method.

=head4
Params:

	pool_id
	int
	Pool ID
	
	entry_date
	date
	(Optional) If this is set we will return results from the date to present.
	
	force
	bool
	(Optional) Set to true if you want 
	the latest information from the database.

=head4
Returns:

	hashref = 
	{
		pool_id INT The ID of the member sponsoring the pool,
		pool_upgrades	arrayref	The members in the pool.
	}

=cut

sub getMemberPoolUpgradesByPoolId
{

	my $this = shift;
	my $pool_id = shift;
	my $date = shift;
	my $force = shift;
	$force = 0 if !defined $force;
	
	$pool_id =~ s/\s+//g;
	
	die 'Members::getMemberPoolUpgradesByPoolId - The member_id parameter was not defined, or held the incorrect information only integers are allowed.' if (! defined $pool_id || $pool_id !~ /^\d+$/);
	die 'Members::getMemberPoolUpgradesByPoolId - The date parameter holds the incorrect information only integers are allowed, and should be in the YYYYMMDD.' if (defined $date && $date !~ /^\d{8}$/);


	
	
		
	return {'pool_id'=>$pool_id, 'pool_upgrades'=>$this->{'member_pool_upgrades_by_id'}->{$pool_id}} if (! $force && ! $date && exists $this->{'member_pool_upgrades_by_id'}->{$pool_id} );

	
	

	if (! exists $this->{'member_pool_upgrades_by_id'}->{$pool_id} || $force)
	{
		return if $this->__updatePoolUpgradeCache($pool_id);
	}	


	if (exists $this->{'member_pool_upgrades_by_id'}->{$pool_id} )
	{

		return {'pool_id'=>$this->{'member_pool_upgrades_by_id'}->{$pool_id}, 'pool_upgrades'=>$this->{'member_pool_upgrades_by_id'}->{$pool_id} } if (! defined $date || ! $date);
		
		my $pool_upgrades = [];
		foreach (@{$this->{'member_pool_upgrades_by_id'}->{$pool_id}})
		{
			
			my $vip_upgarde_date = $_->{'vip_date'};
			$vip_upgarde_date =~ s/-//g;
			push @$pool_upgrades, $_ if $vip_upgarde_date >= $date;
			
			
			
		}

		
		return {'pool_id'=>$pool_id, 'pool_upgrades'=>$pool_upgrades};
	}

	
	
	
	
	return;

}


1;

__END__

=head1
SEE ALSO

L<Members>

=head1
CHANGE LOG

	Date	What was changed

=cut

