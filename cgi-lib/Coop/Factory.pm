package Coop::Factory;


=head1 NAME:
Coop::Factory

	This uses the Abstract Factory method to 
	instanciate the Coop Class using the interface
	of your choice.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Code Example

	my $class_name = 'MediaWiz' or 'Default' or '' (which is the same as Default)
	my $Coop = {};
	eval{
		$Coop = Coop::Factory->new($class_name, $db);
	};
	if ($@)
	{
		However you want to handle your fatal error.
	}

=cut

use strict;


=head1
DESCRIPTION

=head2
CONSTRUCTOR

=head3
new

=head4
Params:

	class	string	The name of the interface.
	db	obj	The database object (If you're going 
			to be importing new members it's best
			to use the db from Admin::DB so the
			oporators name is logged).

=head4
Returns:

	obj

=cut

sub new
{

    my $class          = shift;
    my $requested_type = shift;
    
    my $location       = "Coop/Default.pm";
    $class          = "Coop::Default";
    
    if ($requested_type)
    {
	    $location       = "Coop/$requested_type.pm";
    	$class          = "Coop::$requested_type";
    }
	eval{
		require $location;
	};
	if($@)
	{
		$location       = "Coop/Default.pm";
    	$class          = "Coop::Default";
    	require $location;
	}
    	return $class->new(@_);
	
}


1;

__END__

=head1
SEE ALSO

L<Coop>, L<Coop::Default>, L<Coop::MediaWiz>

=head1
CHANGE LOG

	Date	What was changed

=cut

