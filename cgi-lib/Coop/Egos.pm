package Coop::Egos;
use base qw( Coop );
use strict;

=head1 NAME:
Coop::MediaWiz

	This is the 'Egos' interface for the "Coop" abstraction class.
	When invoking this interface you should use "Coop::Factory->new('Egos', $db)".
	Using this interface should be used when you are parsing
	coop members from "Egos".  See the "_csv_mapping" attribute
	for the mapping.

=head1
VERSION:

	0.1

=head1
SYNOPSIS:

=head2
Code Example

	my $returned_arrayref_hashref = [];
	
	eval{
		my $Coop = Coop::Factory->new('Egos', $db);
		$returned_arrayref_hashref = $Coop->processList(CSV_STRING, 1);
	};
	if($@)
	{
		
		How ever you want to handle your fatal error.
		
	}

=head1
PREREQUISITS:

	Lingua::EN::NameCase	This is used to correct the case of names.
	Locale::Country	This is used to get the ISO codes for countries.
	Locale::SubCountry	This is used to get the ISO codes for state/provinces.

=cut

use Lingua::EN::NameCase 'NameCase';
use Locale::Country; 
use Locale::SubCountry;



=head1 DESCRIPTION

=cut

=head2 METHODS

=head3
PRIVATE

=cut

=head3
__init

=head4
Description:

	This sets the attributes specific for 
	this interface.  This should only be 
	called from the Constructor.

=head4
Params:

	none

=head4
Return:

	none

=cut

sub __init
{

	my $this = shift;

    $this->{_version}	= '0.1';
    $this->{_name}		= 'Coop::Egos';
    $this->{_members_age} = '16 hours';    
    #This should be in the same order of the recieving CSV, it 
    #is mainly for errors so the person importing can easily 
    #map the users that errored out to the spreadsheet.
    $this->{_csv_mapping}	= [
    							{memo=>'Id'},
    							{firstname=>'Firstname'},
    							{lastname=>'Lastname'},
    							{phone=>'Phone'},
    							{cellphone =>'EvePhone'},
    							{emailaddress=>'Email'},
    							{address1=>'Address'},
    							{city=>'City'},
    							{state=>'State'},
    							{postalcode=>'Zip'},
    							{country=>'Country'},
    							{ipaddress=>'IP'},
    							{stamp=>'DT'},
    							{source=>'LeadSource'} 
							];
							
}


=head3
PUBLIC

=head4
processList

=head4
Description:

	This prepares the CSV list to be processed.
	The names are put in the correct case, the
	country and province is turned into an ISO codes, 
	non-numberic characters are removed from the 
	telephone number, white space is removed 
	from the beginning, and end of strings, etc..  
	Then the information is put into an
	array of hashrefs and passed to the method 
	that creates the new members (this->createMemberships).
	

=head4
Parameters:

	list_to_parse	string	The CSV list of names to parse
	headers_available	bool	True if the first row of the 
								CSV string that is 
								passed into list_to_parse
								contains the standard
								header row.
	invalid_potential_members	arrayref	This is a reference to 
											an array from the calling 
											script. The members that 
											are rejected are placed 
											in this array.

=head4
Return:

	int	The Number of members created.
	or
	anonymous hashref on error.  Use ref() eq 'HASH' to determin if there was an error or not.

=cut

sub processList
{

	my $this = shift;
	my $list_to_parse = shift;
	my $headers_available = shift;
	my $invalid_potential_members = shift;	
	my @potential_members = ();
	my $original_text_header = '';
	#Get the header mapping the CSV comes in with.
	my $original_headers = $this->getOriginalHeaderMapping();
	#Make a CSV string of the headers the CSV file comes in with
	foreach my $value (@$original_headers)
	{
		$original_text_header .= "$value,";
	}
	
	$original_text_header =~ s/,$//;
	
	# Remove those pesky Microsith end of line characters.	
	$list_to_parse =~ s/\r\n/\n/g;
	
	# If the user specified "$list_to_parse" does not contain headers add the standard headers
	# for the list.
	$list_to_parse = $original_text_header . "\n" . $list_to_parse if (! $headers_available && $list_to_parse !~ /^$original_text_header/);
	
	
	# If the user did not specify that "$list_to_parse" does not contain headers and the headers do not match.
	# return an error.
	return {error=>'The CSV must contain headers, or you must unselect the "This List Contains Column Headings" checkbox when submitting this information'} if ($list_to_parse !~ /^$original_text_header/);
	
	my $potential_members_arrayref_hashref = [];
	
	$this->_setTextCSVSlurp() if ! exists $this->{Text_CSV_Slurp};
	
	#Convert the CSV into an arrayref of hashrefs
	$potential_members_arrayref_hashref = $this->{Text_CSV_Slurp}->load(string => $list_to_parse);
	
	foreach my $potential_member_hashref (@$potential_members_arrayref_hashref)
	{
		#Set the default SPID (Bucket)
		my $formatted_members = {spid=>3434841, membertype=>'m', mail_option_lvl=>2};
		#my $formatted_members = {spid=>12, membertype=>'m'};
		
		
		#Clean the data
		$potential_member_hashref->{Firstname} =~ s/^\s+|\s+$//g;
		$potential_member_hashref->{Lastname} =~ s/^\s+|\s+$//g;
		$potential_member_hashref->{Country} =~ s/^\s+|\s+$//g;
		$potential_member_hashref->{Email} =~ s/^\s+|\s+$//g;
		$potential_member_hashref->{Source} =~ s/^\s+|\s+$//g;
		$potential_member_hashref->{Phone} =~ s/\D//g;
		$potential_member_hashref->{EvePhone} =~ s/\D//g;
		
		#Data that is copied directly
		$formatted_members->{emailaddress} = $potential_member_hashref->{Email};
		$formatted_members->{phone} = $potential_member_hashref->{Phone};
		$formatted_members->{cellphone} = $potential_member_hashref->{EvePhone};
		$formatted_members->{address1} = $potential_member_hashref->{Address};
		$formatted_members->{city} = $potential_member_hashref->{City};
		$formatted_members->{postalcode} = $potential_member_hashref->{Zip};
		
		#Data that needs some processing before it can be used.
		$formatted_members->{firstname} = NameCase($potential_member_hashref->{Firstname});
		$formatted_members->{lastname} = NameCase($potential_member_hashref->{Lastname});
		$formatted_members->{country} = country2code($potential_member_hashref->{Country}) ? uc(country2code($potential_member_hashref->{Country})): uc($potential_member_hashref->{Country});

		$formatted_members->{country} =~ s/^UK$/GB/i;
		

		#Convert the Province/State into an ISO code if one is available.
		eval{
			
			my $Locale_SubCountry = Locale::SubCountry->new($formatted_members->{country});
			
			die "Invalid country code \n" if ( not $Locale_SubCountry ); 
			
			if (  $Locale_SubCountry->has_sub_countries )
			{
				my $province_code = $Locale_SubCountry->code($potential_member_hashref->{State});
				
				$formatted_members->{state} = ($province_code ne 'unknown') ? $province_code : $potential_member_hashref->{State};
			}
			
		};
		if($@)
		{
			# If an exception is thrown just use the text the user specified.
			$formatted_members->{state} = $potential_member_hashref->{State};			
		}

		# This can probably be modified so only a comment is put in 'memo' once 
		# the 'original_spid' table is handling the birth records.
		$formatted_members->{memo} = 'Source: ' . $potential_member_hashref->{LeadSource};
		$formatted_members->{memo} .= ' IP Address: ' . $potential_member_hashref->{IP};
		$formatted_members->{memo} .= ' When: ' . $potential_member_hashref->{DT};
		$formatted_members->{memo} .= ' Source ID: ' . $potential_member_hashref->{Id};
		
		#Data for the "original_spid" table.
		$formatted_members->{ipaddress} = $potential_member_hashref->{IP};
		$formatted_members->{signup_date} = $potential_member_hashref->{DT};
		$formatted_members->{coop_name} = 'Egos';
		$formatted_members->{landing_page} = 'Co-op Import';
		
#TODO: Add a check so it makes sure the email address is in the proper format.
		# If there is no email address the potential member is worthless to us so we add it to "invalid_potential_members" array.
		if ($formatted_members->{emailaddress} && $formatted_members->{country} =~ /^\w\w$/)
		{
			push @potential_members, $formatted_members;			
		} 
		elsif (! $formatted_members->{emailaddress}) 
		{
			$formatted_members->{'warn'} = 'No email address is available for this person. A member account could not be created.'; 
    		
    		$formatted_members->{source} = $potential_member_hashref->{Source};
    		$formatted_members->{stamp} = $potential_member_hashref->{DT};
    		
			push @$invalid_potential_members, $formatted_members;
		} else {
			$formatted_members->{'warn'} = 'The country was not valid, or is in the wrong format.'; 
    		
    		$formatted_members->{source} = $potential_member_hashref->{Source};
    		$formatted_members->{stamp} = $potential_member_hashref->{DT};
			
			push @$invalid_potential_members, $formatted_members;
		}
		
		undef $formatted_members;
	}
	
	# No reason to leave this initialized taking up space in memory.
	undef $potential_members_arrayref_hashref;
	
	my $number_of_members_created = 0;	
	
	$number_of_members_created = $this->createMemberships(\@potential_members, $invalid_potential_members) if(scalar @potential_members);	
	 
	
	return $number_of_members_created;
	
}

1;

__END__

=head1
SEE ALSO

L<Coop>, L<Coop::Factory>, L<Coop::MediaWiz>, L<Lingua::EN::NameCase>, L<Locale::Country>, L<Locale::SubCountry>

=head1
CHANGE LOG

	Date	What was changed

=cut

