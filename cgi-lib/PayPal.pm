package PayPal;

# created 08/07/07	Bill MacArthur
# last modified: 08/14/07	Bill MacArthur
###### this package is not related in any way to the original module which never made it into production
###### portions of this module::Encrypt taken from ewp.pl
###### by: # Copyright 2005 by Gray Watson
###### More details: http://256.com/gray/docs/paypal_encrypt/

# be sure to set the environment when using this module to prevent to avoid openssl errors in the log
# $ENV{HOME} = '/tmp';

use IPC::Open2;
use strict;
use base 'Exporter';

our @EXPORT_OK = qw($URL $BUSINESS);

our $URL = 'https://www.paypal.com/cgi-bin/webscr';
our $BUSINESS = 'supportpp@dhs-club.com';

###### see the Website Payments Standard Integration Guide
my %GlobalDefs = (
	business => $BUSINESS,
	cpp_header_image => 'https://www.clubshop.com/images/logos/dhs_club_logo.gif',	# 750x90 max
	display => 1,
	image_url => 'https://www.clubshop.com/images/logos/logo_150x50_dhs.png',	# 150x50
	no_note => 1,
	no_shipping => 1,
	cmd => '_xclick',
	cert_id => 'M78LC3F63L546'
);
my $OPENSSL = '/usr/bin/openssl';
my $certsdir = '/home/httpd/cgi-lib/certs';
my $MY_KEY_FILE = "$certsdir/our_pp_live_key.pem";
my $MY_CERT_FILE = "$certsdir/our_pp_live_cert.pem";
my $PAYPAL_CERT_FILE = "$certsdir/paypal_cert.pem";
my $error = ();

sub Encrypt($)	###### take a parameter of two and create an encrypted value to pass to PayPal
{			###### this routine will always set the global $error var and return undef on failure

	my $args = shift;	###### a hashref of values
				# amount, item_name, quantity, cancel_return, return, invoice, custom

	unless (-x $OPENSSL){
		$error = "Could not execute $OPENSSL: $!\n";
		return undef
	}
	unless ($args->{amount}){
		$error = 'An amount must be specified';
		return undef;
	}
	$args->{quantity} ||=1;
	
	my $txt = '';
	foreach (keys %GlobalDefs){ $txt .= "$_=$GlobalDefs{$_}\n" if $GlobalDefs{$_}; }
	foreach (keys %{$args}){ $txt .= "$_=$args->{$_}\n" if $args->{$_}; }

	unless ( my $pid = open2(*READER, *WRITER,
		"$OPENSSL smime -sign -signer $MY_CERT_FILE " .
		"-inkey $MY_KEY_FILE -outform der -nodetach -binary " .
		"| $OPENSSL smime -encrypt -des3 -binary -outform pem " .
		"$PAYPAL_CERT_FILE"))
	{
		$error = "Could not run open2 on $OPENSSL: $!\n";
		return undef;
	}
	print WRITER $txt;
	close WRITER;
	# read in the lines from openssl
	my @lines = <READER>;
	close READER;

	# combine them into one variable
	my $encrypted = join('', @lines);
	return $encrypted;
}

sub Create_SOA_Record($$)
{
	my $db = shift;
	my $r = shift;	###### should be a hashref to a key/value set matching an ipn transmission/transaction

	return undef unless $r->{id} ||= $r->{custom};

	return undef unless my ($trans_id) = $db->selectrow_array(q|SELECT nextval('"soa_trans_id_seq"')|);
	my $qry = "INSERT INTO soa (trans_id, trans_type, id, amount, description, memo)
		VALUES ($trans_id, 3000, ?, ?, ?, ?)";
	my @list = ($r->{id}, $r->{mc_gross}, "txn_id: $r->{txn_id}");
	###### now create an SOA memo from a few fields
	push (@list, "$r->{first_name} $r->{last_name}, $r->{payer_email}\n" .
		"PP memo: " . ($r->{memo} || '') .
		"PP payer_id: " . ($r->{payer_id} || ''));
		
	return $db->do($qry, undef, @list) ? $trans_id : undef;
}

1;

###### 08/14/07 added the Create_SOA_Record routine for CA record creation from ipn's

