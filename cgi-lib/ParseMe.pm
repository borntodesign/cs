package ParseMe;
###### last modified: 07/15/14	Bill MacArthur

use strict;
use Carp qw/croak/;

sub Grab_Headers($)
{
	my $_MSG = shift;

	###### we can accept either a ref to the real document or the document itself
	###### if we receive a ref, then the real document in the other routine
	###### will be parsed
	###### if we receive the document value, then the real document in the parent
	###### will remain unchanged
	my $MSG = (ref $_MSG) ? $_MSG : \$_MSG;

	my $rv = ();

	###### convert the \r\n combos to simple \n
	###### otherwise convert \r to \n
	$$MSG =~ s/(\r\n)|\r/\n/g;

	###### remove any comments
	###### as written, this precludes the use of a hash mark in a document
	###### if we received the actual document value we'll skip this part since
	###### the changes will be lost anyway
	$$MSG =~ s/#.*\n//g if ref $_MSG;

	###### if other headers are desired, then simply add them to the list
	###### as written, a key will be returned regardless if there is a real value
	###### we could if necessary, delete the key, but I think the current
	###### behaviour will be more convenient

	###### since some headers could have mutliple values
	###### like Cc: somebody@h.com, you@now.com
	###### and the Cc: tags may span lines, we'll handle those a bit differently
	foreach my $x (qw(TO FROM SUBJECT REPLY-TO))
	{
		$$MSG =~ s/(^|\n)($x:.*)/$1/i;
		($rv->{$x} = ($2 || '')) =~ s/^$x:\s*//i;
		chomp $rv->{$x};
		$rv->{$x} =~ s/\s+$//;
	}

	foreach my $x (qw(CC BCC))
	{
		$$MSG =~ s/(^|\n)($x:.*)/($1 . _combine_headers(\$rv, $x, $2))/ieg;
	}

	###### remove any leading blank lines
	$$MSG =~ s/^(\w*\n)*//g if ref $_MSG;

	# get rid of empty header keys
	foreach (keys %{$rv})
	{
		delete $rv->{$_} unless $rv->{$_};
	}
	
	return $rv;
}

sub ParseAll($$;$)
{
	my ($txt, $subs, $opt) = @_;

	###### we require the second argument to be a hashref whose keys
	###### point to hashrefs themselves
	unless (ref $subs eq 'HASH')
	{
		croak "The second argument must be a hashref.
			ref of arg= ${\ref $subs}\n";
	}
	
	foreach (keys %{$subs})
	{
		unless (ref $subs->{$_} eq 'HASH')
		{
			croak "The values in your hash must all be hashrefs.
				ref of $_= ${\ref $subs->{$_}}\n";
		}
	}

	###### we can accept either a ref in place of the real document
	###### or the real value
	###### the ref is more efficient as we will parse it 'in place'
	###### whereas if we receive the real we have to pass it back and forth
	if (ref $txt)
	{
		_parse($txt, $subs, $opt);
		return;
	}
	else
	{
		_parse(\$txt, $subs, $opt);
		return $txt;
	}
}

sub _combine_headers
{
	###### some headers may be multivalued and need to be concatenated
	###### like if they include 2 Cc: headers in a message
	###### we need to pull in all the values and combine them into one field for mailing
	my ($hdr, $col, $val) = @_;
	my $tmp = ();

	chomp $val;
	$val =~ s/\s+$//;

	###### initialize our key
	$$hdr->{$col} ||= '';

	($tmp = $val) =~ s/^$col:\s*//i;
	if ($tmp)
	{
		$$hdr->{$col} .= ',' if $$hdr->{$col};
		$$hdr->{$col} .= $tmp;
	}
	return '';
}

sub _parse($$;$)
{
	my ($test, $e, $opt) = @_;

	###### the rules are like this:
	# if a key doesn't exist, then the placeholder is left intact
	# IF the $opt flag is NOT set
	# (there may be cases where we want to always squash undef'd placeholders)
	# if a key exists but the value is undefined, then '' is substituted
	# otherwise the value is substituted
	$$test =~ s/%%(.+?):(.+?)%%/
		exists $e->{lc $1}->{lc $2} ?
		(defined $e->{lc $1}->{lc $2} ? $e->{lc $1}->{lc $2} : '') :
		($opt ? '' : $&)/xeg;
}
						
1;

###### 11/29/04 added chomping and whitespace elimination at the end of header lines
###### 05/16/05 added the REPLY-TO header into the mix
###### 07/15/14 implemented removal of empty header keys in Grab_Headers