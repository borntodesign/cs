package HtmlMailings;
use strict;
use Crypt::CBC;
use XML::LibXML::Simple qw(XMLin);
use Encode;
use Carp qw( croak );
use Template;
use HtmlMailingsConstants;
use XML::local_utils;
use Data::Dumper;
use MailingUtils;

####### this version has been tweaked and more or less works correctly on mail.dhs-club.com
####### HOWEVER, before using it on the web server, it should be tested as it seems that certain modules work differently on the different flavors of OS

=head2 HtmlMailings

=head3
Various methods for delivering and viewing componentized HTML mailings.
This same module can be used in a delivery script as well as in the browserview CGI version

This module requires:
Crypt::Blowfish
Crypt::CBC
Convert::Base32
XML::Simple
Carp

=head4
my $HM = HtmlMailings->new({key/value pairs});
All of the keys you pass in will be returned in the blessed reference, plus others which will be created
See init() for some it will use.

	Optional arguments:
		lang_pref: A 2 letter language code, if received, will be used for all content generated.
			Otherwise the language will have to be specified in various method calls that require a language
		BrowserView: since the browser view will ultimately be in an SSL session
			We will need to ensure that all images are pulled in SSL.
			Having SSL images in the email body is probably not a generally good idea.
			If we need to, we can convert other values as well besides img src attributes.

	Other keys created in the blessed reference
	config: a reference to a hash of hashes
		{en => {key/value pairs representing the XML configuration for that language}}

	You can pass NoTTprocess=>1 to new() or set the hash value subsequently to remove the final template processing of GenerateHtmlDocument
	This is convenient for seeing the tags sans interpolation

=cut

# Constants
use constant XML_DOC => 10741;

# Globals
my $TT = Template->new();
my $CIPHER = Crypt::CBC->new({'key'=>'5o9%l!+-', 'cipher'=> 'Blowfish', 'header'=>'none', 'iv'=>'1OZY76pu'});

=head4 addressTo({member=>$hashref})

Meant as an internal function
Returns the addressee in the format specified in the configuration.
If the format has not been specified, simply returns the email address.

=cut

sub addressTo
{
	my ($self, $data) = @_;
	return $data->{'member'}->{'emailaddress'} unless $self->config->{'address_format'};
	# we will not skip placeholder interpolation in this routine
	my $rv = '';
	$TT->process(\$self->config->{'address_format'}, $data, \$rv);
	return $rv;
}

=head4 config(lang_pref)

An internal function that will return the correct configuration for the specified language

=cut

sub config
{
	my ($self, $lang_pref) = @_;
	return $self->pickTranslation($self->{'config'}, $lang_pref);
}

sub _convertHttpToHttps
{
	my $r = shift;
	$r =~ s/^(.+?=")http:(.*)$/$1 . 'https:' . $2/e;
	return $r;
}

sub convertHttpToHttps
{
	my ($self, $ref) = @_;
	return unless defined $$ref;
	$$ref =~ s/(<img.+?>)/_convertHttpToHttps($1)/eg;
}

sub new
{
	my ($class, $args) = @_;
	die 'Arguments must be supplied as a hashref' if $args && ref $args ne 'HASH';
	foreach (qw//){
		croak "A $_ argument value is required" unless $args->{$_};
	}
	return bless $args;
}

sub db { return $_[0]->{'db'}; }

=head4 decodeBrowserViewIdentity( identity_token )

Receive a token
Receive back a docid, language pref, oid and alias values if the token could be unencrypted and split into its separate parts

=cut

sub decodeBrowserViewIdentity($$)
{
	my ($self, $token) = @_;
	($token = $CIPHER->decrypt_hex($token)) =~ m/^(\d+)([a-z]{2})(\d+)(\w+)$/;
	my ($docid, $lang_pref, $oid, $alias) = ($1,$2,$3,$4);
	# some rudimentary checks
	return undef unless $docid && $lang_pref && $oid && $alias;
	return ($docid, $lang_pref, $oid, $alias)
}

=head4 encodeBrowserViewIdentity( member_hashref )

Receive a member record hashref containing at least the alias and oid values
Return a token comprised of a Blowfish encrypted combination of the oid and alias concatenated

=cut

sub encodeBrowserViewIdentity($$)
{
	my ($self, $data) = @_;
	croak "Both an oid and an alias are required to create the identity token" unless $data->{'oid'} && $data->{'alias'};
	croak "The oid should be purely numeric and the alias should not be purely numeric"
		if $data->{'oid'} =~ /\D/ || $data->{'alias'} !~ /\D/;
	my $lp = $data->{'language_pref'} || 'en';
	return $CIPHER->encrypt_hex( $self->{'docid'} . $lp . $data->{'oid'} . $data->{'alias'} );
}

=head4 GenerateHtmlDocument({data hashref}, lang_pref)

A public method that returns a reference to the assembled and parsed HTML document, ready for mailing or delivery to the browser

=cut

sub GenerateHtmlDocument($$)
{
	my ($self, $data, $lang_pref) = @_;
	croak "In order to proceed, we minimally need a member key pointing to a hashref which contains oid and alias keys\n"
		unless $data->{'member'}->{'oid'} && $data->{'member'}->{'alias'};
		
	# we really need a language pref at this point to generate the correct content
	$lang_pref ||= $data->{'member'}->{'language_pref'} || $self->{'lang_pref'} || 'en';

	my $rv = '';
	# this effectively caches the assembled document under the appropriate key for reuse
	# this is important when this module is used for mass mailings to avoid having to assemble the document every time
	unless ($self->{'renderings'}->{'html'}->{$lang_pref})
	{
		# first we start with the top half of our document shell
		$self->{'renderings'}->{'html'}->{$lang_pref} = $self->pickTranslation($self->config($lang_pref)->{'doc_open'}, $lang_pref);

		# then we build it out
		my $var;
		foreach (@{$self->config($lang_pref)->{assembly}->{'section'}})
		{
			$var = $self->pickTranslation($_, $lang_pref);
			stripHTMLization(\$var);
			$self->{'renderings'}->{'html'}->{$lang_pref} .= $var;
		}
	}

# at this point the TT placeholders have not yet been interpolated in the shell

	# set a few other elements
	$data->{'subject'} = $self->Subject($data, $lang_pref);
	$data->{'docid'} = $self->{'docid'};
	$data->{'lang_pref'} = $lang_pref;
	$data->{'bv_domain'} = $self->config->{'bv_domain'};

	# we need to insert our browser view identity value in the data stream
	$data->{'BrowserViewIdentity'} = $self->encodeBrowserViewIdentity($data->{'member'});
	
	# make our XML available for insertion of simple text strings
	$data->{'xml'} = $self->pickTranslation($self->{'xml'}, $lang_pref);
	foreach (keys %{$data->{'xml'}})
	{
#		warn "key $_: flag: " . Encode::is_utf8($data->{'xml'}->{$_});
	}
	$data->{'xxml'} = $self->pickTranslation($self->{'xxml'}, $lang_pref);
	
	# some of our lang_nodes contain TT placeholders themselves
	# we could reparse the entire document after it is initially parsed,
	# but that is probably more overhead than simply parsing these nodes before insertion
	# $data->{lang_nodes} is a reference, so we will need to create a local copy to parse so as to protect the originals
	my %tmp = %{ $self->pickTranslation($self->{'lang_nodes'}, $lang_pref) || {} };
	foreach (keys %tmp)
	{
		my $t;
		$TT->process(\$tmp{$_}, $data, \$t);
		$tmp{$_} = $t;
	} 
	$data->{'lang_nodes'} = \%tmp;

	# and our "content" that will get substituted into a complete shell when that method is used
	$data->{'content'} = $self->pickTranslation($self->config($lang_pref)->{'content'}, $lang_pref);
	$self->convertHttpToHttps(\$data->{'content'}) if $self->{'BrowserView'};

	# now we need to do our TT work on the content since it won't be interpolated after it is inserted
	# and again we need to muck with the utf8 flag
	$data->{'member'}->{$_} = _decode_utf8_if_not_utf8($data->{'member'}->{$_}) foreach keys %{$data->{'member'}};
	$TT->process(\$data->{'content'}, $data, \$rv);
	
	# doing this here became necessary along with encode_utf8(the final output) in order to get the shell placeholders to render properly with accented characters
	$data->{'content'} = _decode_utf8_if_not_utf8($rv);
	
	$rv = '';

	# since the removal link is membertype specific first
	# we will tack that onto a cached document along with the document shell closing
	my $doc = $self->{'renderings'}->{'html'}->{$lang_pref};
	foreach( @{ $self->config->{'removal'}->{ $data->{'member'}->{membertype} }->{'section'} }, @{ $self->config->{'doc_close'}->{'section'} })
	{
		my $var = $self->pickTranslation($_, $lang_pref);
		stripHTMLization(\$var);
		$doc .= $var;
	}

# at this point, the TT placeholders in the shell remain uninterpolated

	$self->convertHttpToHttps(\$doc) if $self->{'BrowserView'};
	$data->{'BrowserViewFlag'} = $self->{'BrowserView'} || 0;

	# for HTML shells, we don't want to have to specify a subject for use as a browser view title,
	# but instead use the subject as it would be generated for email use
	$data->{'Html_Title'} = $self->Subject($data);

	# since we cannot seem to escape the need for subtle textual differences even in the shell
	# we will support the concept of additional nodes in the configuration that follow this concept
	# <shell_variable_1>vipnewsletter</shell_variable_1>
	# we will place the node name as a key on the $data root
	# with the value being the value of a node like named in xml or lang_nodes
	my $cfg = $self->config($data->{'member'}->{'language_pref'});
	foreach (keys %{$cfg})
	{
		next unless m/^shell_variable/;
		
		my ($s, $n) = split(/\./, $cfg->{$_});
		next unless $s && $n;
		
		my $nv = eval{$data->{$s}->{$n}};

		$nv = _decode_utf8_if_not_utf8($nv);	# needed to address encoding issues for these placeholders... ughhh
		
		my $tmp;
		$TT->process(\$nv, $data, \$tmp);
		$data->{$_} = $tmp;
	}

	# in order for admin to test and see most of the tags uninterpolated
	# we will not process if we have a flag
	unless ($self->{'NoTTprocess'})
	{
#warn "utf8 flag for xml.all_rights_reserved is:" . Encode::is_utf8($data->{'xml'}->{'all_rights_reserved'});
#warn "string value:$data->{'xml'}->{'all_rights_reserved'}";
#$data->{'xml'}->{'all_rights_reserved'} = decode_utf8($data->{'xml'}->{'all_rights_reserved'});
#warn "utf8 flag for xml.all_rights_reserved is:" . Encode::is_utf8($data->{'xml'}->{'all_rights_reserved'});
#warn "string value:$data->{'xml'}->{'all_rights_reserved'}";

	# I yi yi... we have to turn it off in initDataSet and then we have to turn it back on here....
		$data->{'xml'}->{$_} = _decode_utf8_if_not_utf8($data->{'xml'}->{$_}) foreach keys %{$data->{'xml'}};
		if ($data->{'xxml'})
		{
			$data->{'xxml'}->{$_} = _decode_utf8_if_not_utf8($data->{'xxml'}->{$_}) foreach keys %{$data->{'xxml'}};
		}
		
		$TT->process(\$doc, $data, \$rv);
		
		# this ended up being necessary to get accented characters in shell placeholders to finally render correctly
		$rv = encode_utf8($rv);
		return \$rv;
	}

	return \$doc;
}

=head4 GenerateTextDocument({data hashref}, lang_pref)

A public method that returns a reference to the assembled and parsed plain text document, ready for mailing

=cut

sub GenerateTextDocument($$)
{
	my ($self, $data, $lang_pref) = @_;
	croak "In order to proceed, we minimally need a member key pointing to a hashref which contains oid and alias keys\n"
		unless $data->{'member'}->{'oid'} && $data->{'member'}->{'alias'};

	# we really need a language pref at this point to generate the correct content
	$lang_pref ||= $data->{'member'}->{'language_pref'} || $self->{'lang_pref'} || 'en';

	# this effectively caches the assembled document under the appropriate key for reuse
	# this is important when this module is used for mass mailings to avoid having to assemble the document every time
	unless ($self->{'renderings'}->{'text'}->{$lang_pref})
	{
		# we build it out
		foreach (@{$self->config->{'text_build'}->{'assembly'}->{'section'}})
		{
			my $var = $self->pickTranslation($_, $lang_pref);
			$self->{'renderings'}->{'text'}->{$lang_pref} .= $var;
		}
	}

	# there may not be a textual version, so we need to make it empty to avoid error related to undef vars elsewhere
	my $doc = $self->{'renderings'}->{'text'}->{$lang_pref} || '';

	# now build out our removal verbiage
	foreach( @{ $self->config->{'text_build'}->{'removal'}->{ $data->{'member'}->{'membertype'} }->{'section'} })
	{
		my $var = $self->pickTranslation($_, $lang_pref);
		$doc .= $var;
	}
	
	$data->{'lang_pref'} = $lang_pref;
	$data->{'bv_domain'} = $self->config->{'bv_domain'};

	# make our XML available for insertion of simple text strings
	$data->{'xml'} = $self->pickTranslation($self->{'xml'}, $lang_pref);

	# do the same for our lang_nodes (which originate from XHTML lexicons)
	$data->{'lang_nodes'} = $self->pickTranslation($self->{'lang_nodes'}, $lang_pref);

	# we need to insert our browser view identity value in the data stream
	$data->{'BrowserViewIdentity'} = $self->encodeBrowserViewIdentity($data->{'member'});
	
	my $rv = '';
	unless ($self->{'NoTTprocess'})
	{
		$TT->process(\$doc, $data, \$rv);
		return \$rv;
	}
	return \$doc;
}

=head4 Get_Object(object ID)

This customized version of Get_Object for internal use will always return a reference to a hash of hashrefs like this:
{
	en => \%object_translation record,
	...
}

=cut

sub getObject($$)
{
	my ($self, $objid) = @_;

	# instead of using one of the existing Get_Object methods,
	# I am going to write a custom one in here as we really need to go both ways with this
	# we may be satisfied with a single "translation" or we may want them all
	
	# first get the cgi_object record to see if it is a translation_key or not
	my $obj = $self->db->selectrow_hashref('
		SELECT obj_id, obj_value, active, translation_key FROM cgi_objects WHERE obj_id=?', undef, $objid);
	croak "The template doesn't exist: $objid" unless $obj;
	croak "The template retrieved is inactive: $objid" unless $obj->{'active'};

	return { 'en' => _decode_utf8_if_not_utf8($obj->{'obj_value'})} unless $obj->{'translation_key'};

	my $qry = qq|
		SELECT value, "language"
		FROM object_translations WHERE active=TRUE AND obj_key= $obj->{obj_id} AND "language" ~ ?|;
	my $lp = $self->{'language_pref'} ? $self->{'language_pref'} : '.*';
	my $sth = $self->db->prepare($qry);
	$sth->execute($lp);
	my $trans = ();
	
	while (my ($val, $lang) = $sth->fetchrow_array)
	{
		$trans->{$lang} = _decode_utf8_if_not_utf8($val);
	}
	return $trans if $trans;
	
	# if we were looking for all translations and got none, then there aren't any to get
	croak "Document translations could not be found for: $obj->{'obj_id'}" if $lp eq '*';

	# if my $trans is undefined, then we may have tried to get a specific translation which is not available
	# so we'll try obtaining the english version
	$trans = $self->db->selectrow_hashref($qry, undef, 'en');
	croak "English document translation could not be found for: $obj->{'obj_id'}" unless $trans;
	return {$lp => _decode_utf8_if_not_utf8($trans->{'value'})};
}

=head4 init({{key/value pairs}})

This public function should be called to initialize the object subject to creation.
The reason for not doing these things is that certain methods available in this class are needed before the required arguments may be available.
Of particular note is the docid which will come encoded in the path of a CGI browser view.

Required arguments (unless they have been provided to new() ):
	docid: the template id for the document definition
	db: an active database handle

Optional arguments:
	lang_pref: A 2 letter language code, if received, will be used for all content generated.
		Otherwise the language will have to be specified in various method calls that require a language
	lang_nodes: a template ID of language nodes contained in an XHTML document
	content: a template ID of a a document that will be inserted into a complete shell.
		This methodology can be used when our "shell" is a complete document instead of one built in a serial fashion
=cut

sub init($)
{
	my ($self, $args) = @_;
	foreach (qw/db docid/)
	{
		croak "A $_ argument value is required" unless $args->{$_} || $self->{$_};
		$self->{$_} ||= $args->{$_};
	}
	
	foreach (qw/lang_pref lang_nodes content/)
	{
		$self->{$_} ||= $args->{$_};
	}
	
	initDataSet($self);
}

=head4 initDataSet( document ID ) an internal function that performs these steps

Retrieves the document definition in the document ID parameter (just a cgi_object)
Converts it to a hashref using XML::Simple's XMLin
	Although we will likely never need it, we are going to support the concept of language based configurations.
	This could prove helpful for when a mailpiece is significantly different based on the language.
Loads the XML_DOC template and creates a hashref on the $self->{'xml'} key of the nodes therein.
	ie. {'xml'}->{en)->{node1}, {'xml'}->{it)->{node1}, ...
Loads the "lang_nodes" template and creates a hashref on the $self->{lang_nodes} key of the nodes therein.
	ie. {lang_nodes}->{en)->{node1}, {lang_nodes}->{it)->{node1}, ...

=cut

sub initDataSet($)
{
	my ($self) = @_;
	my $c = getObject($self, $self->{'docid'});	# this will return a reference to a hash of hashes
	foreach (keys %{$c})
	{
		$self->{'config'}->{$_} = XMLin($c->{$_});
	}
	
	# now load all the templates defined in the configuration
	foreach my $lang (keys %{ $self->{'config'} })
	{
		# we always load English, but why load anything that we don't need if we have a language pref specified at start up
		next if $lang ne 'en' && $self->{'lang_pref'} && $self->{'lang_pref'} ne $lang;
		foreach my $nn (keys %{ $self->{'config'}->{$lang} })
		{
			$self->loadDocs(\$self->{'config'}->{$lang}->{$nn});
		}
	}

	$c = getObject($self, XML_DOC) || die "Failed to load the XML template\n";

	foreach my $k (keys %{$c})
	{
		$self->{'xml'}->{$k} = XMLin($c->{$k}, 'NoAttr' => 1);
		
		# our xml doc should be a single level layout... in other words, no nested nodes
		# otherwise, how will we reference the node in a TT template
		foreach my $node (keys %{$self->{'xml'}->{$k}})
		{
			# I thought we were all done with utf-8 problems, but for some reason,
			# we need to turn off the flag for this stuff to work properly (maybe a Template Toolkit thing?)
			# to make it even more fun, some servers software versions set the flag coming into here and some do not
			# we don't want to run encode on a value that doesn't have the flag turned on because that boogers up the data
			$self->{'xml'}->{$k}->{$node} = encode_utf8($self->{'xml'}->{$k}->{$node}) if Encode::is_utf8($self->{'xml'}->{$k}->{$node});
		}
	}

	my $lu = XML::local_utils->new();
	foreach my $k (keys %{ $self->{'config'} })
	{
		# if we have an 'xml' node defined in the root, lets merge it into the 'xml' node of the object
		foreach my $lang_pref (keys %{$self->{'config'}->{$k}->{'xxml'}})
		{
			$self->{'xxml'}->{$lang_pref} = XMLin($self->{'config'}->{$k}->{'xxml'}->{$lang_pref}, 'NoAttr'=>1);
			
			# we are taking the same concept as above, hoping that this is a single level document
			foreach my $node (keys %{$self->{'xxml'}->{$lang_pref}})
			{
				# I thought we were all done with utf-8 problems, but for some reason,
				# we need to turn off the flag for this stuff to work properly (maybe a Template Toolkit thing?)
				# to make it even more fun, some servers software versions set the flag coming into here and some do not
				# we don't want to run encode on a value that doesn't have the flag turned on because that boogers up the data
				$self->{'xxml'}->{$lang_pref}->{$node} = encode_utf8($self->{'xxml'}->{$lang_pref}->{$node}) if Encode::is_utf8($self->{'xxml'}->{$lang_pref}->{$node});
			}
		}

		# our "content" values may have been converted to complete HTML documents by tinyMCE
		# we need to strip that off
		stripHTMLization(\$self->{'config'}->{$k}->{'content'}->{$_}) foreach keys %{ $self->{'config'}->{$k}->{'content'} };
		
		# we should have either a lang_nodes configuration or a lang_nodes_set, but not both
		# if we do have both, then what we will load in the extra lang_nodes from the lang_nodes_set lower down
		foreach my $lang_pref (keys %{$self->{'config'}->{$k}->{'lang_nodes'}})
		{
			$self->{'lang_nodes'}->{$lang_pref} = $lu->flatXHTMLtoHashref( $self->config($k)->{'lang_nodes'}->{$lang_pref} );

#			foreach my $node (keys %{$self->{'lang_nodes'}->{$lang_pref}})
#			{
#				$self->{'lang_nodes'}->{$lang_pref}->{$node} = encode_utf8($self->{'lang_nodes'}->{$lang_pref}->{$node}) if Encode::is_utf8($self->{'lang_nodes'}->{$lang_pref}->{$node});
#			}
			
			# we don't need this tree any longer, so let's free up some memory
			delete $self->config($k)->{'lang_nodes'}->{$lang_pref};
		}
		
		foreach my $elem ( @{$self->{'config'}->{$k}->{'lang_nodes_set'}->{'lang_nodes'}} )
		{
			foreach my $lang_pref (keys %{$elem})
			{
				my $v = $lu->flatXHTMLtoHashref( $elem->{$lang_pref} );

#				foreach my $node (keys %{$v})
#				{
#					$v->{$node} = encode_utf8($v->{$node}) if Encode::is_utf8($v->{$node});
#				}
			
###### merge our hashrefs together
###### takes care of when the value is uninitialized...
###### perl doesn't like that for a hashref when performing this operation                  vvvvvv
				$self->{'lang_nodes'}->{$lang_pref} = {%{ $self->{'lang_nodes'}->{$lang_pref} || {} }, %{$v}};
			}
			
			# we don't need this tree any longer, so let's free up some memory
			delete $self->{'config'}->{$k}->{'lang_nodes_set'};
		}
	}
}

=head4 loadDocs( a value from the configuration hash )

An internal function that loads templates or local constants/functions for document fragments

=cut

sub loadDocs
{
	my ($self, $ref) = @_;
	# we are receiving a ref so that the caller doesn't have to be concerned about assigning a value to whatever it is sending
	croak "\$ref: $ref is not a reference\n" unless ref $ref;

	# we are looking for
	#	values that match only digits: templates
	#	value beginning with an underscore: constants or local subroutines
	#	arrayrefs or hashrefs: more nodes underneath
	# we will ignore all others as they are literal values
	if ($$ref !~ /\D/){
		$$ref = $self->getObject($$ref);
	}
	elsif (ref $$ref eq 'HASH'){
		foreach (keys %{$$ref}){ $self->loadDocs(\$$ref->{$_}); }
	}
	elsif (ref $$ref eq 'ARRAY'){
		foreach (my $x=0; $x < scalar@{$$ref}; $x++){ $self->loadDocs(\$$ref->[$x]); }
	}
	elsif ($$ref =~ /^_/){
		# we'd better have the constant/subroutine defined
		$$ref = {'en' => eval($$ref)};
	}
}

=head4 pickTranslation($self, a hashref of text values, an optional language preference)

An internal function that returns the correct language version of a given hashref object

=cut

sub pickTranslation($$;$)
{
	my ($self, $obj, $lang_pref) = @_;
	# we could croak here, but for now, let's just return an empty string if our object is undefined
	return '' unless $obj;

	$lang_pref ||= '';
	croak "object is undefined" unless $obj;
#	return $obj->{$lang_pref} || $obj->{$self->{'lang_pref'}} || $obj->{'en'};
	return $obj->{$lang_pref} || $obj->{'en'};
}

=head4 pickLangNodesNode($self, node name, optional lang_pref)

An internal function that obtains the desired node from the general lang_nodes document in the appropriate language

=cut

sub pickLangNodesNode
{
	my ($self, $node_name, $lang_pref) = @_;
	my $t = pickTranslation($self, $self->{'lang_nodes'}, $lang_pref);
	croak "pickTranslation returned no value" unless $t;
	# if the node has not been translated, we'll use the English version by default
	
	return $t->{$node_name} || $self->{'lang_nodes'}->{'en'}->{$node_name};
}

=head4 pickXmlNode($self, node name, optional lang_pref)

An internal function that obtains the desired node from the general XML document in the appropriate language

=cut

sub pickXmlNode
{
	my ($self, $node_name, $lang_pref) = @_;
	my $t = pickTranslation($self, $self->{'xml'}, $lang_pref);
	croak "pickTranslation returned no value" unless $t;
	# if the node has not been translated, we'll use the English version by default
	
	return $t->{$node_name} || $self->{'xml'}->{'en'}->{$node_name};
}

=head4 $HM->From()

Returns the designated FROM address for the mailing

=cut

sub From
{
	my $self = shift;
	return $self->pickTranslation($self->{'config'}, $self->{'lang_pref'})->{'from'};
}

=head4 SendMail({'member'=>data_record[,'key'=>data]})

	Other keys that can be passed in on the hashref are 'cc' which should be a text string to use for a CC list.
	
	Returns 1 on success and if not successful returns the error as a second return value

=cut

sub SendMail
{
	my ($self, $data) = @_;
	croak "data argument should minimally contain a 'member' key pointing to a hashref of member related data" unless $data->{'member'};
	
	foreach (keys %{$data->{'member'}})
	{
		$data->{'member'}->{$_} = _decode_utf8_if_not_utf8($data->{'member'}->{$_});
	}
	
	my %mail = ('subject'=>$self->Subject($data));
	
	# if we received an SMTP argument, set it... we may be rotating servers
	$mail{'smtp'} = $data->{'SMTP'} if $data->{'SMTP'};
	
	# we may receive a 'CC' list and/or a configuration setup to have one
	$mail{'cc'} = $data->{'cc'} if $data->{'cc'};
	if ($self->config->{'cc'})
	{
		if ($mail{'cc'})
		{
			$mail{'cc'} .= ',' . $self->config->{'cc'};
		}
		else
		{
			$mail{'cc'} = $self->config->{'cc'};
		}
	}

	my $textref = $self->GenerateTextDocument($data);
	my $htmlref = ();
	
	# we do not have a text version defined, they are getting HTML even if they have specified text
	if (($data->{'member'}->{'mail_option_lvl'} == 2) || ! ${$textref})
	{ 
		$htmlref = $self->GenerateHtmlDocument($data);
	}
	$mail{'txt'} = $textref;
	$mail{'html'} = $htmlref if $htmlref;
	$mail{'from'} = $self->From();
	$mail{'to'} = $self->addressTo($data);
	$mail{'bcc'} = $self->config->{'bcc'} if $self->config->{'bcc'};
	
	$mail{'reply-to'} = $self->config->{'reply-to'} if $self->config->{'reply-to'};
	
	my $rv = MailingUtils::Mail(%mail);
	return $rv, $MailingUtils::error;
}

=head4 stripHTMLization( text to be stripped)

	An internal function to deal with what TinyMCE does with HTML snips (it wraps them to make them a valid HTML document)
	We simply pick out the content and leave the wrapping on the floor

=cut

sub stripHTMLization
{
	my $ref = shift;
	return unless defined $$ref;
	$$ref =~ s#^.*<body.*?>(.+)</body.*$#$1#s;
}

=head4 Subject

	$HM->Subject( {data hashref}, lang_pref) returns the interpolated subject line in the appropriate language
	Using Template Toolkit style placeholders like: [%member.firstname%] [%member.lastname%],
	our data hashref would look like: {member=>$member_record_hashref}

=cut

sub Subject
{
	my ($self, $data, $lang_pref) = @_;
	$lang_pref ||= $data->{'member'}->{'language_pref'} || 'en';
	my $rv = '';
	croak 'Configuration is empty. Did you initialize the object by calling init() ?' unless $self->config($lang_pref);
	
	my $subj = $self->config->{'subject'};
	
	# if there is a document identifier as in xml.nodename or lang_nodes.nodename, then $a[0] will contain the identifier and $a[1] the nodename
	# otherwise the nodename will be in $a[0]
	my @a = ($subj =~ m/^(\w*\.*)(.*)/);
	
	# we were just placing the subject node in the master document for every new mailing... this became tedious
	# so let's create the option to place it in the actual language pack, then if we are still using the master XML doc we can omit any document designator like xml.subject and just use 'subject'
	# otherwise, we can specify lang_nodes.subject
	my $node = ($a[0] !~ m/\./ || $a[0] =~ m/^xml\./) ?
		pickXmlNode($self, ($a[1] || $a[0]), $lang_pref) :
		pickLangNodesNode($self, $a[1], $lang_pref);

	# perform interpolation as necessary
	unless ($self->{'NoTTprocess'})
	{
		my $rv = '';
		$node = _decode_utf8_if_not_utf8($node);
		$TT->process(\$node, $data, \$rv) ;
		return $rv;
	}
	return $node
}

sub _decode_utf8_if_not_utf8
{
	my $val = shift;
	return $val if Encode::is_utf8($val);
	return Encode::decode_utf8($val);
}

sub _encode_utf8_if_utf8
{
	my $val = shift;
	return $val unless Encode::is_utf8($val);
	return Encode::encode_utf8($val);
}

1;
