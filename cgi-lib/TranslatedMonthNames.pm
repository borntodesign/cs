package TranslatedMonthNames;
use strict;
require XML::LibXML::Simple;

# Deployed 07/19/12	Bill MacArthur

my $xs = XML::LibXML::Simple->new();
my $xml_template = 10572;	# where the list resides

=head1 TranslatedMonthNames

	A simple class to provide the translated names for months which we have available

=head2 new

	Create a new instance.

=head3 Arguments

	Pass arguments in as hash elements: new({'db'=>$db, 'G_S'=>$gs})
	A DB handle is required as is a G_S object.
	
=head3 Returns

	A bless TranslatedMonthNames object.
	
=cut

sub new($$)
{
	my $class = shift;
	my $args = shift;

	my $rv = {};
	
	foreach (qw/db G_S/)
	{
		$rv->{$_} = $args->{$_} if $args->{$_};
	};
	
	return bless $rv;
}

=head2 Hashref

	Intended primarily as an internal method to acquire the template and convert it to an initial hashref.
	
=head3 Arguments

	A two character language preference
	
=head3 Returns

	A hashref looking like this (French translation):
	{'months' => {'September' => {'month' => '8','content' => 'Septembre'},'May' => {'month' => '4','content' => 'Mai'},...}
	
	Note: the month numbers in the XML start at -0-.... why, I do not know

=cut
	 
sub Hashref($$)
{
	my ($self, $language_pref) = @_;
	my ($xml) = $self->{'G_S'}->Get_Object($self->{'db'}, $xml_template);
	return undef unless $xml;
	my $ref = $xs->XMLin($xml, 'GroupTags'=>{'months'=>'month'});

	return $ref;
}

=head2 HashrefByMonthNumber

	Obtain a hashref of translated months keyed by the number of the month.
	
=head3 Arguments

	A two character language preference
	
=head3 Returns

	A hashref looking like this (French translation):
	{'7' => 'Jullet','4' => 'Avril','10' => 'Octobre','3' => 'Mars','9' => 'Septembre',....}
	
	Note: We are changing the month numbers to match month numbers as would conventionally be found in a date like 2012-05-01 would be month 5/May.
		This is different than what is returned by Hashref().
	
=cut

sub HashrefByMonthNumber($$)
{
	my ($self, $language_pref) = @_;

	unless ($self->{'HashrefByMonthNumber'})
	{
		my $hr = $self->Hashref($language_pref);
		foreach (keys %{$hr->{'months'}})
		{
			$self->{'HashrefByMonthNumber'}->{ ($hr->{'months'}->{$_}->{'month'} +1) } = $hr->{'months'}->{$_}->{'content'};
		}
	}
	
	return $self->{'HashrefByMonthNumber'}
}

=head2 HashrefByMonthName

	Obtain a hashref of translated months keyed by the English name of the month.
	
=head3 Arguments

	A two character language preference
	
=head3 Returns

	A hashref looking like this (French translation):
	{'September' => 'Septembre','May' => 'Mai','October' => 'Octobre','November' => 'Novembre',...}
	
=cut

sub HashrefByMonthName($$)
{
	my ($self, $language_pref) = @_;
	
	unless ($self->{'HashrefByMonthName'})
	{
		my $hr = $self->Hashref($language_pref);
		foreach (keys %{$hr->{'months'}})
		{
			$self->{'HashrefByMonthName'}->{ $_ } = $hr->{'months'}->{$_}->{'content'};
		}
	}
	
	return $self->{'HashrefByMonthName'}
}

1;