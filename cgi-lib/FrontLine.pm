package FrontLine;

###### Display a pool's nspid frontline members that are in the viewers allowable organizational view
###### 
###### Admin access is of the form:
######		http://www.clubshop.com/cgi/nspid_frontline.cgi?admin=1&id=<member id>
######	
###### Created as a direct copy of vip/nspid_frontline.cgi on 03/18/14	Bill MacArthur
###### Last modified: 12/02/14	Bill MacArthur

use strict;
use Encode;
use XML::LibXML::Simple;
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util qw(looks_like_number);
use Template;
use lib ('/home/httpd/cgi-lib');
use G_S qw($LOGIN_URL);
use DB_Connect;
use ADMIN_DB;
use Data::Dumper;	# for debugging only

######
my $DEBUG = 0;	# set this to 0 for normal operation
my $TT = Template->new();
my %MTYPES = (			# Which members to include in the report. The key is the report_type paramater. The value is inserted directly into the SQL
	'm'		=> "'s'",	# actually, using abstract report_types was a bad idea as far as I (Bill) am concerned, so I will be remapping the abtracts to the real membertype when the params are loaded
	'a'		=> "'m'",
	'mr'	=> "'ma'",
	'ag'	=> "'ag'",
	'tp'	=> "'tp'",
	'*'		=> "'m'",
	'p'		=> "'v'",
	'ma'	=> "'ma'",
	's'		=> "'s'"
);

# some report types require different templates
# the keys are actual membertypes... with the * matching anything
my %TMPL = (
	'lang'          => 10918,
	'v' => {
		's' => {
			'report'	=> 11067,
			'member_row'	=> 11069,
			'tbl_header'	=> 11068,
			'csv_area'      => 10915
		},
		'm' => {
			'report'	=> 11067,
			'member_row'	=> 11069,
			'tbl_header'	=> 11068,
			'csv_area'      => 10915
		},
		'v' => {
			'report'	=> 11067,
			'member_row'	=> 11076,
			'tbl_header'	=> 11075,
			'csv_area'      => 10915
		},
		'ag' => {
			'report'	=> 11067,
			'member_row'	=> 11072,
			'tbl_header'	=> 11074,
			'csv_area'      => 10915
		},
		'ma' => {
			'report'	=> 11067,
			'member_row'	=> 11072,
			'tbl_header'	=> 11073,
			'csv_area'      => 10915
		},
		'tp' => {
			'report'	=> 11067,
			'member_row'	=> 11070,
			'tbl_header'	=> 11071,
			'csv_area'      => 10915
		}
	},
	'*' => {
		's' => {
			'report'	=> 11067,
			'member_row'	=> 10916,
			'tbl_header'	=> 11066,
			'csv_area'      => 10915
		},
		'm' => {
			'report'	=> 11067,
			'member_row'	=> 10916,
			'tbl_header'	=> 11066,
			'csv_area'      => 10915
		},
		'ag' => {
			'report'	=> 11067,
			'member_row'	=> 11072,
			'tbl_header'	=> 11074,
			'csv_area'      => 10915
		},
		'ma' => {
			'report'	=> 11067,
			'member_row'	=> 11072,
			'tbl_header'	=> 11073,
			'csv_area'      => 10915
		},
		'tp' => {
			'report'	=> 11067,
			'member_row'	=> 11064,
			'tbl_header'	=> 11065,
			'csv_area'      => 10915
		}
	}
);
my %ICONS = (
	'cb' => qq|<img src="/images/rd-grn-blt-10x10.gif" width="10" height="10" alt="active" />|
		);

###### GLOBALS
my $ME = ();
my (%param, $member_id, $login_party, $admin, $db, $gs, $url, $this) = ();
my $member_info = {'id'=>'', 'membertype'=>''};
my $count = 0;
my $class = 'g';					# Start with a gray background for our info row.
my $csv_url = ();
my $page_size = 500;	# if we ever allow this to be redefined by the browser, we must ensure it remains a numeric value to avoid SQL injection

my %TABLE_DEFS = (
	'm'		=> 'members m',
	'mt'	=> 'JOIN member_types mt ON mt.code=m.membertype',
	'mrs'	=> 'LEFT JOIN member_ranking_summary mrs ON mrs.member_id = m.id',
	'mpm'	=> 'LEFT JOIN members_prior_membertype mpm ON mpm.id=m.id',
	'mt2'	=> 'LEFT JOIN member_types mt2 ON mt2.code=mpm.membertype',
	'muv'	=> 'JOIN my_upline_vip muv ON muv.id=m.id	-- starting at this join we constrain the result set to memberships that the viewer is authorized to see',
	'tempmids'	=> 'JOIN tempmids ON tempmids.id=muv.upline	-- ending here',				
	'odp'	=> 'JOIN od.pool odp ON odp.id=m.id',
	'ma'	=> 'JOIN merchant_affiliates_master ma ON ma.member_id = m.id',
	'ns'	=> 'JOIN network.spids ns ON ns.id=m.id'
);

# when we build out the query, certain tables need to come before others for the joins to work properly
# be sure to add any tables in the TABLE_DEFS to this list
my @JOIN_ORDER = qw/m mt odp ma ns muv tempmids mrs mpm mt2/;

my %COL_DEFS = (
	'id' 	=> 'm.id',
	'alias' => 'm.alias',
	'firstname' => 'm.firstname',
	'lastname' 	=> 'm.lastname',
	'membertype'		 => 'm.membertype',
	'mail_option_lvl'	 => 'm.mail_option_lvl',
	'language_pref'		 => "COALESCE(m.language_pref,'') AS language_pref",
	'country'		=> "COALESCE(m.country, '') AS country",
	'emailaddress' 	=> "COALESCE(m.emailaddress, '') AS emailaddress",
	'emailaddress2' => "COALESCE(m.emailaddress2, '') AS emailaddress2",
	'phone'			=> "COALESCE(m.phone,'') AS phone",
	'cellphone'		=> "COALESCE(m.cellphone,'') AS cellphone",
	'company_name'	=> 'm.company_name',
	
	'display_code'		 => 'mt.display_code',
	
	# since we are doing DISTINCT when we form the query, and we need to do sorting on the raw data, we need to have the raw data cols available, otherwise the DB complains
	# so, we will pull in two "columns" when we specify these keys... the _version of the key will be what is used for the order by expression
	'last_action_time'	=> "mrs.last_action_time AS _last_action_time, COALESCE(mrs.last_action_time::DATE::TEXT,'') AS last_action_time",
	'recent_activity'	=> "mrs.current_flag AS _current_flag, CASE WHEN mrs.current_flag=1 THEN 'Y'::CHARACTER ELSE 'N' END AS recent_activity",
	'prospective_rank'	=> "mrs.ranking_summary AS _ranking_summary, COALESCE(mrs.ranking_summary::TEXT,'') AS prospective_rank",
	'upgrade_deadline'	=> "odp.deadline AS _deadline, COALESCE(odp.deadline::DATE::TEXT,'') AS upgrade_deadline",
	
	'prior_membertype'	=> "COALESCE(mt2.display_code,'n/a') AS prior_membertype"
);

my %TABLES = ('m'=>1, 'mt'=>1);	# we will always use this table

# we will include these columns always, even if we do not display some of the values
my %COLS = (
	'id' 	=> 1,
	'alias' => 1,
	'firstname' => 1,
	'lastname' 	=> 1,
	'membertype'		 => 1,
	'mail_option_lvl'	 => 1,
	'language_pref'		 => 1,
	'country'		=> 1,
	'emailaddress' 	=> 1,
	'emailaddress2' => 1,
	'phone'			=> 1,
	'cellphone'		=> 1,
	'display_code'	=> 1
);

my %ORDER_COLS = (
	'prospective_rank'	=> '_ranking_summary',
	'last_action_time'	=> '_last_action_time',
	'recent_activity'	=> '_current_flag',
	'id'				=> 'm.id',
	'upgrade_deadline'	=> '_deadline',
	'company_name'		=> 'm.company_name'
);

my %ORDER_COLS_SORT_DEFINITIONS = (
	'prospective_rank'	=> {'DESC'=>'DESC NULLS LAST', 'ASC'=>'ASC'},
	'last_action_time'	=> {'DESC'=>'DESC NULLS LAST', 'ASC'=>'ASC'},
	'recent_activity'	=> {'DESC'=>'DESC NULLS LAST', 'ASC'=>'ASC'},
	'id'				=> {'DESC'=>'DESC', 'ASC'=>'ASC'},
	'upgrade_deadline'	=> {'DESC'=>'DESC', 'ASC'=>'ASC'},
	'company_name'		=> {'DESC'=>'DESC NULLS LAST', 'ASC'=>'ASC'},
);

my %ORDER_COLS_SORT_DEFAULTS = (
	'prospective_rank'	=> 'DESC',
	'last_action_time'	=> 'DESC',
	'recent_activity'	=> 'DESC',
	'id'				=> 'DESC',
	'upgrade_deadline'	=> 'ASC',
	'company_name'		=> 'ASC'
);

my %SORT_ICONS = (
	'ASC' => '&#9652;',
	'DESC' => '&#9662;'
);

# the only valid columns for ordering, and the ordering they will receive if the corresponding key is set in %ORDER_BY_COLS
my @ORDER_BY_COLS = qw/prospective_rank last_action_time recent_activity upgrade_deadline id company_name/;
my %ORDER_BY_COLS = ('id'=>1);	# we will always use id
my @CRITERIA = ();

################################
###### Methods start here.
################################

=head2 new

Instantiate a new object of this class

=head3 Arguments

A hashref with these key value pairs
{
	'cgi' => a valid CGI object,
	'report_type' => a text string, currently one of these 'rspid' -or- 'nspid'
}

=head3 Returns

A new instance of this class

=cut

sub new
{
	my $class = shift;
	my $args = shift;
	die 'A key/value pair must be passed in on the hashref to new' unless $args->{'cgi'};
	
	my $self = bless $args, $class;
	$gs = G_S->new({'db'=>\$db, 'cgi'=>$args->{'cgi'}});
	$self->LoadParams();
	$ME = $self->cgi->self_url;
	$csv_url = $ME;
	
	$csv_url =~ s/csv_file=0/csv_file=1/;
	$csv_url .= ';csv_file=1' unless $csv_url =~ m/csv_file/;
	
	$url = $self->cgi->url();
	$this = $self;	# this class var is needed by HeaderCallback as it doesn't receive $self as the first arg in the callback
	return $self;
}

sub cgi
{
	return $_[0]->{'cgi'};
}

sub db
{
	return $db;
}

sub param
{
	my $self = shift;
	my $k = shift;
	return $param{$k};
}

sub AliasLink
{
	my $self = shift;
	return $self->cgi->a({
		'-href' => "http://www.clubshop.com/cgi/vip/member-followup.cgi?rid=$_[0]->{'id'}",
		'-onclick' => 'window.open(this.href);return false;'
	}, $_[0]->{'alias'})
}

sub BuildQuery
{
	my $self = shift;
	my $sql = 'SELECT DISTINCT';
	
	foreach (keys %COLS)
	{
		next unless $COLS{$_};	# if the key is not set, then even though it is present, we will not use it
		$sql .= "\n$COL_DEFS{$_},"
	}
	chop $sql;
	
	$sql .= "\nINTO TEMP TABLE flt\nFROM\n";
	
	foreach (@JOIN_ORDER)
	{
		next unless $TABLES{$_};	# if the key is not set, then even though it is present, we will not use it
		$sql .= "$TABLE_DEFS{$_}\n"
	}

	# apply the criteria for the query
	$sql .= 'WHERE m.membertype IN (' . ($MTYPES{$param{'report_type'}} || $MTYPES{'*'}) . ")\n";
	
	if ($self->{'report_type'} eq 'nspid')
	{
		if ($param{'report_type'} eq 'tp')
		{
			unshift @CRITERIA, 'odp.pnsid=' . $param{'refid'};
		}
		else
		{
			unshift @CRITERIA, 'ns.spid=' . $param{'refid'};
		}
	}
	else
	{
		unshift @CRITERIA, 'm.spid=' . $param{'refid'};
	}
	
	foreach (@CRITERIA)
	{
		$sql .= "AND $_\n"
	}
	
	# apply a sort order
	my $orderBy = "";
	if ($param{'ob'})	# if we received a parameter, it overrides any built in logic
	{
		$orderBy = " $ORDER_COLS{$param{'ob'}} " . ($ORDER_COLS_SORT_DEFINITIONS{ $param{'ob'} }->{ $param{'so'} } || $param{'so'});
	}
	else
	{
		foreach (@ORDER_BY_COLS)
		{
			next unless $COLS{$_};			# if the column key is not set, then even though it is present, we will not use it
			next unless $ORDER_BY_COLS{$_};	# if we have not registered the column, we will not use it
			warn "key= $_ - setting up order by col: $ORDER_COLS{$_} - param so: $param{'so'}" if $DEBUG;
			$orderBy .= " $ORDER_COLS{$_} " . ($param{'so'} || $ORDER_COLS_SORT_DEFINITIONS{$_}->{ $ORDER_COLS_SORT_DEFAULTS{$_} } ) . ',';
		}
		chop $orderBy;
	}
	$sql .= "ORDER BY $orderBy\n" if $orderBy;
	warn $sql if $DEBUG;
	return $sql;
}

sub CallbackConvertSpaceToBr
{
	my $tok = shift;
	$tok =~ s#\s#<br />#;
	return $tok;
}

##### Prints an error message to the browser.
sub Err
{
	my $self = shift;
	my $err = shift;
	my $type = shift || 'text/html';
	print $self->cgi->header('-expires'=>'now', '-type'=>$type);
	print $self->cgi->start_html('-bgcolor'=>'#ffffff', '-title'=>'Error') . $self->cgi->br if $type eq 'text/html';
	print $err;
	print $self->cgi->br . $self->cgi->end_html() if $type eq 'text/html';
	Exit();
}

sub Exit
{
	$db->disconnect if defined $db;
	exit;
}

# Fill out a member row.
sub Fill_Member_Row
{
	my $self = shift;
	my $one_row = shift;
	my $data = shift;

	# Put in a non-breaking space where we have no values.
	foreach (keys %{$data})
	{
		unless ( defined $data->{$_} )
		{
			$data->{$_} = '&nbsp;';
		}
	}

	# sometimes we do (data->somekey || '') because some of these keys may not exist at all for some report types
	# this keeps us from slews of unit'd var errors in the logs
	$one_row =~ s/%%PROSPECTIVE_RANK%%/($data->{'prospective_rank'} || '')/e;
        
    $one_row =~ s/%%ACTIVITY%%/($data->{'recent_activity'} || '')/e;
	
	$one_row =~ s/%%PRIOR_MEMBERTYPE%%/($data->{'prior_membertype'} || '')/e;
	$one_row =~ s/%%FIRSTNAME%%/$data->{'firstname'}/;
	$one_row =~ s/%%LASTNAME%%/$data->{'lastname'}/;
	$one_row =~ s/%%ALIAS%%/$self->AliasLink($data)/e;
	$one_row =~ s/%%EMAILSTATUS%%/$data->{'mail_option_lvl'}/;
	$one_row =~ s/%%COUNTRY%%/$data->{'country'}/;
	$one_row =~ s/%%PHONE%%/$data->{'phone'}/;
	$one_row =~ s/%%CELLPHONE%%/$data->{'cellphone'}/;
	$one_row =~ s/%%LANGUAGE%%/$data->{'language_pref'}/;
	$one_row =~ s/%%last_action_time%%/($data->{'last_action_time'} || '')/e;
	$one_row =~ s/%%upgrade_deadline%%/($data->{'upgrade_deadline'} || '')/e;
	$one_row =~ s/%%company_name%%/($data->{'company_name'} || '')/e;
	
	# If the email addresses are blank, then just put out a blank.
	unless ($data->{'emailaddress'} eq '&nbsp;')
	{
		$one_row =~ s/%%EMAILADDRESS%%/<a href="Mailto:$data->{'emailaddress'}">$data->{emailaddress}<\/a>/;
	}
	else
	{
		$one_row =~ s/%%EMAILADDRESS%%/&nbsp;/;
	}

	unless ($data->{'emailaddress2'} eq '&nbsp;')
	{
		$one_row =~ s/%%EMAILADDRESS2%%/<a href="Mailto:$data->{'emailaddress2'}">$data->{emailaddress2}<\/a>/;
	}
	else
	{
		$one_row =~ s/%%EMAILADDRESS2%%/&nbsp;/;
	}

	$one_row =~ s/%%CLASS%%/$class/;
	$class = ($class eq 'w') ? ('g') : ('w');		# Alternate the row's color.

	# Return the new row.
	return $one_row;
}

# Fill out a member CSV row.
# ideally we would use Template toolkit with these, but then we have to change the templates to TT placeholders... so it's a project for another day
sub Fill_Member_CSV_Row
{
	my $self = shift;
	my $one_row = shift;
	my $data = shift;

	foreach (keys %{$data})
	{
		unless ( defined $data->{$_} )
		{
			$data->{$_} = '';
		}
	}

	# sometimes we do (data->somekey || '') because some of these keys may not exist at all for some report types
	# this keeps us from slews of unit'd var errors in the logs
	$one_row =~ s/%%PROSPECTIVE_RANK%%/$data->{'prospective_rank'}/;
    $one_row =~ s/%%ACTIVITY%%/($data->{'recent_activity'} || '')/e;
	$one_row =~ s/%%PRIOR_MEMBERTYPE%%/($data->{'prior_membertype'} || '')/e;
	$one_row =~ s/%%FIRSTNAME%%/$data->{'firstname'}/;
	$one_row =~ s/%%LASTNAME%%/$data->{'lastname'}/;
	$one_row =~ s/%%ALIAS%%/$data->{'alias'}/;
	$one_row =~ s/%%EMAILSTATUS%%/$data->{'mail_option_lvl'}/;
	$one_row =~ s/%%COUNTRY%%/$data->{'country'}/;
	$one_row =~ s/%%PHONE%%/$data->{'phone'}/;
	$one_row =~ s/%%CELLPHONE%%/$data->{'cellphone'}/;
	$one_row =~ s/%%LANGUAGE%%/$data->{'language_pref'}/;
	$one_row =~ s/%%last_action_time%%/($data->{'last_action_time'} || '')/e;
	$one_row =~ s/%%upgrade_deadline%%/($data->{'upgrade_deadline'} || '')/e;
	$one_row =~ s/%%EMAILADDRESS%%/$data->{'emailaddress'}/;
	$one_row =~ s/%%EMAILADDRESS2%%/$data->{'emailaddress2'}/;

	return $one_row . "\n";
}

###### Get the info stored in the cookie.
sub Get_Cookie
{
	my $self = shift;
	my $rv = 0;
	
	# If this is an admin user.		 
	if ( $admin = $param{'admin'} )
	{
		# Get the admin user and try logging into the DB
		my $operator = $self->cgi->cookie('operator');
		my $pwd = $self->cgi->cookie('pwd');

		# Check for errors.
		unless ($db = ADMIN_DB::DB_Connect( 'member_frontline.cgi', $operator, $pwd ))
		{
			$self->Err('Admin DB login failed');
		}
		
		unless ( $member_id = $param{'id'})
		{
			$self->Err('Missing a required parameter: id');
		}
		else
		{
			$rv = 1;
		}
	}
	# If this is a member.		 
	else
	{
		exit unless $db = DB_Connect( 'member_frontline.cgi' );
		$db->{'RaiseError'} = 1;

		# Get the member Login ID and info hash.
		( $member_id, $member_info ) = $gs->authen_ses_key( $self->cgi->cookie('AuthCustom_Generic') );

		# If the Login ID doesn't exist or has any non-digits send them to login again.
		if (!$member_id || $member_id =~ /\D/)
		{
			$self->RedirectLogin();
			$self->Exit();
		}
		else
		{
			$rv = 1;
		}
	}

	###### get our reportee info if we don't already have it, like if this was an admin call with only an id parameter
	unless ($member_info->{'alias'})
	{
		my $sth = $db->prepare("
			SELECT id, spid, alias, membertype, firstname, lastname, emailaddress
			FROM members WHERE id= ?");
		$sth->execute($member_id);
		$member_info = $sth->fetchrow_hashref();
	}

	# we want the login party to remain unchanged in case of changes to member_info
	$login_party = \%{$member_info};
	
	return $rv;
}

sub GetMemberInfo
{
	my ($self, $member_id) = @_;
	my $sth = $db->prepare("
		SELECT id, alias, membertype, firstname, lastname, emailaddress
		FROM members WHERE id= ?");
	$sth->execute($member_id);
	$member_info = $sth->fetchrow_hashref;
}

sub HeaderCallback
{
	my ($val) = @_;
	
	# rewrite or create the ob param
	$this->{'cache_cburl'} ||= HeaderCallbackURL();
	warn "cache_cburl: $this->{'cache_cburl'}" if $DEBUG;
	my $rv = $this->{'cache_cburl'} . "ob=$val;";
	my $icon = ();
	
	# if the header we are dealing with is not the one we are actively sorting on, then we will set the icon to the default for that header
	if (($param{'ob'} || '') ne $val)
	{
		$rv .= "so=$ORDER_COLS_SORT_DEFAULTS{$val}";
		$icon = $SORT_ICONS{ $ORDER_COLS_SORT_DEFAULTS{$val} };
	}
	elsif ($param{'so'} eq '')
	{
		$rv .= "so=$ORDER_COLS_SORT_DEFAULTS{$val}";
		$icon = $SORT_ICONS{ $ORDER_COLS_SORT_DEFAULTS{$val} };
	}
	elsif ($param{'so'} eq 'ASC')
	{
		$rv .= "so=DESC";
		$icon = $SORT_ICONS{'DESC'};
	}
	else
	{
		$rv .= "so=ASC";
		$icon = $SORT_ICONS{'ASC'};
	}
	return $this->cgi->a({'href'=>$rv}, $icon);
}

sub HeaderCallbackURL
{
	my $rv = $url . '/' . $this->{'report_type'} .'?';
	foreach (keys %param)
	{
		next if $_ eq 'ob' || $_ eq 'so';
		$rv .= "$_=$param{$_};" if defined $param{$_};
	}
	return $rv;
}

sub LoadParams
{
	my ($self) = @_;
	foreach (qw/refid index csv_file report_type admin id debug ob so/)
	{
		next if $_ eq 'debug' && ! $DEBUG;
		$param{$_} = $self->cgi->param($_);
	}

	if ( $param{'refid'} && ($param{'refid'} =~ /\D/ || length $param{'refid'} > 8))	# we don't necessarily need a refid, we could simply use the ID of the user
	{
		$self->Err("Bad refid");
	}
	
	($param{'report_type_translated'} = $MTYPES{$param{'report_type'}}) =~ s/'//g;
	
	# initialize and then validate params
	$param{'index'} ||= 0;
	$param{'csv_file'} ||= 0;
	
	die "csv_file parameter looks invalid" unless looks_like_number($param{'csv_file'});
	die "index parameter looks invalid" unless looks_like_number($param{'index'});	# this value is used directly in SQL so we must ensure that we don't have any potential for SQL injection
	
	# ob is the ORDER BY parameter... since we will use the value directly in SQL, we will filter against known values and default to empty if there is a mismatch
	$param{'ob'} = '' unless $gs->in_array($param{'ob'}, \@ORDER_BY_COLS);
	$param{'so'} = '' unless $gs->in_array($param{'so'}, ['ASC','DESC']);
	warn "param{'so'} initialized to: $param{'so'}" if $DEBUG;
}

###### Display the member's frontline.
sub Report_Members
{
	my $self = shift;
	$param{'refid'} ||= shift;
	
	my $data = '';
	my $one_row = '';
	my $report_body = '';
	my $report_text = '';
	my $firstname = '';
	my $lastname = '';
	my $limits = ';';

	if ($self->{'report_type'} eq 'nspid')
	{
		# with the readvent of the Trial Partner program, along with an actual 'tp' membertype, and the 'pns' (Partner Network Separator), and the way those show in the Team Reports
		# we need to handle things specially if we receive a pns ID as the refid
		# so we will look up the membertype we are pulling the frontline of and make some decisions based upon the membertype
		my ($mt) = $db->selectrow_array("SELECT membertype FROM members WHERE id= ?", undef, $param{'refid'}) || die "Failed to identify a membership with that refid";
		if ($mt eq 'pns')
		{
			if ($param{'report_type'} ne 'tp')	# we just need to look up the actual taproot pool down under the 'pns' and change the refid to that and then continue processing normally
			{
				($param{'refid'}) = $db->selectrow_array("SELECT pid FROM network.partner_network_separators WHERE pnsid= $param{'refid'}");
			}
		}
	}
	
	$self->SetupTablesCols();
	my $qry = $self->BuildQuery();

	$self->Err($qry, 'text/plain') if $param{'debug'};

	$db->do($qry);	# this will setup a temp table -without- any limits... so they will have to be applied when we actually pull the data out
	my ($maxcnt) = $db->selectrow_array('SELECT COUNT(*) FROM flt');

	my $tid = $self->SelectTemplate('member_row');
	my $report_row = $gs->GetObject( $tid ) || $self->Err("Couldn't obtain template $tid");
	
	my $sql .= "SELECT * FROM flt";
	$sql .= " OFFSET $param{'index'} LIMIT $page_size" unless $param{'csv_file'};
	my $sth = $db->prepare($sql);

	# Get the frontline data.
	$sth->execute();
	
	# create text row version from the HTML version (we are assuming that each placeholder is on a separate line)
	my $txtfmt = $report_row;
	$txtfmt =~ s/<.*?>//g;
	$txtfmt =~ s/\n/,/g;
	$txtfmt =~ s/\r//g;
	$txtfmt =~ s/^,|,$//g;

	my $trans_xml = $gs->GetObject($TMPL{'lang'}) || $self->Err("Couldn't obtain template $TMPL{'lang'}");
	my $translation = XML::LibXML::Simple::XMLin($trans_xml, 'NoAttr'=>1);
	_utf8_off($translation);
	while ( $data = $sth->fetchrow_hashref )
	{
		$count++;

		# no need to do this unless we need it
		if ($param{'csv_file'})
		{
			$report_text .= $self->Fill_Member_CSV_Row($txtfmt, $data);
		}
		# Add the new row to the other HTML rows.
		else
		{
			$report_body .= $self->Fill_Member_Row($report_row, $data);
		}
	}		

	$sth->finish();

	$gs->Prepare_UTF8($member_info);
	
	my $tmpl = '';
	my $TBL_HEADER = '';
	if ($param{'csv_file'} == 0)
	{
		my $tid = $self->SelectTemplate('report');
	    $tmpl = $gs->GetObject( $tid ) || $self->Err("Couldn't obtain template $tid");
	    $tmpl =~ s/%%URL%%/$csv_url/;
	    
	    $tid = $self->SelectTemplate('tbl_header');
	    my $tmp = $gs->GetObject( $tid ) || $self->Err("Couldn't obtain template: $tid");
	    $TT->process(\$tmp, {'lb'=>$translation, 'hc'=>\&HeaderCallback, 'csbr'=>\&CallbackConvertSpaceToBr}, \$TBL_HEADER) ||
	    	warn "There was an error processing the header template: " . $TT->error;
	}
	else
	{
		my $tid = $self->SelectTemplate('csv_area');
	    $tmpl = $gs->GetObject( $tid ) || die "Couldn't obtain template: $tid";
	}

	$tmpl =~ s/%%FIRSTNAME%%/$member_info->{'firstname'}/;
	$tmpl =~ s/%%LASTNAME%%/$member_info->{'lastname'}/;
	$tmpl =~ s/%%ALIAS%%/$member_info->{'alias'}/;
	my $ptout = 0;
	
	if ($param{'index'} <= 0)
	{
            $tmpl =~ s/%%PREVIOUS%%//;
	}
	else
	{
            my $prev_link = "<a href=\"$ME\">Previous</a>";
            my $tmpindx = $param{'index'} - $page_size;
            my $chk = "index=$param{'index'}";
            $ptout = "index=$tmpindx";
            $prev_link =~ s/$chk/$ptout/;
            $tmpl =~ s/%%PREVIOUS%%/$prev_link/
	}
	
	if ($param{'index'} + $page_size >= $maxcnt)
	{
            $tmpl =~ s/%%NEXT%%//
	}
	else
	{
            my $next_link = "<a href=\"$ME\">Next</a>";
            my $tmpindx = $param{'index'} + $page_size;
            my $chk = "index=$param{'index'}";
            $ptout = "index=$tmpindx";
            $next_link =~ s/$chk/$ptout/;
            $tmpl =~ s/%%NEXT%%/$next_link/
	}
	
	my $tmpindx2 = $param{'index'} + $page_size;
	
	if ($page_size > $count)
	{
            $tmpindx2 = $param{'index'} + $count;
	}
	
	my $range = $gs->ParseString($translation->{'display'}, ($maxcnt > 0 ? ($param{'index'} || 1) : 0), $tmpindx2, $maxcnt); #"Displaying Records $param{'index'} to $tmpindx2";
	$tmpl =~ s/%%RANGE%%/$range/g;
	$report_text ||=  'Sorry - No members were found.';

	$tmpl =~ s/%%DISCLAIMER%%/$translation->{'disclaimer'}/g;

	$tmpl =~ s/%%REPORT_BODY%%/$report_body/;
	$tmpl =~ s/%%REPORT_TEXT%%/$report_text/g;
	$tmpl =~ s/%%COUNT%%/$count/g;
	$tmpl =~ s/%%TBL_HEADER%%/$TBL_HEADER/;

	print $self->cgi->header('-expires'=>'now', '-charset'=>'utf-8'), $tmpl;
}

sub member_info
{
	return $member_info;
}

###### Redirect the member to the login page. 
sub RedirectLogin
{
	my $self = shift;
	
	print $self->cgi->header('-expires'=>'now'),
		$self->cgi->start_html('-bgcolor'=>'#ffffff');
	print "Please login <a href=\"$LOGIN_URL?destination=$ME\">HERE</a>.<br /><br />";
	print scalar localtime(), "(PST)";
	print $self->cgi->end_html;
}

sub SelectTemplate
{
	my $self = shift;
	my $type = shift;
	my $mt = $login_party->{'membertype'} eq 'v' ? 'v' : '*';
	my $rv = $TMPL{$mt}->{ $param{'report_type_translated'} }->{$type} || $self->Err("Failed to identify a $type template for membertype: $mt - report_type: $param{'report_type_translated'}");
	return $rv;
}

sub SetupTablesCols	# based upon the user membertype and the reportee membertype, setup the columns that should be pulled
{
	my $self = shift;
#	warn "Login ID: $member_info->{'id'} - membertype: $member_info->{'membertype'}" if $DEBUG;
	# if %COLS is changed at the top to remove an item, we may need to add it in here for a specific report
	if ($login_party->{'membertype'} eq 'v' && $gs->in_array($param{'report_type_translated'}, ['m','s','tp','v']))
	{
		warn "Setting tables and cols for Partner report" if $DEBUG;
		$COLS{$_} = 1 foreach (qw/last_action_time recent_activity prospective_rank/);
		$TABLES{'mrs'} = 1;
		$ORDER_BY_COLS{'prospective_rank'} = 1;
		$ORDER_BY_COLS{'last_action_time'} = 1;
		$ORDER_BY_COLS{'recent_activity'} = 1;
	}
	
	# if we are a Partner and this is a network/team report
	if ($login_party->{'membertype'} eq 'v' && $self->{'report_type'} eq 'nspid')
	{
		# setup this temp table once and use it twice
		$db->do("SELECT grp_team_and_me AS id INTO TEMP TABLE tempmids FROM network.grp_team_and_me($member_info->{'id'})");
		$db->do('VACUUM ANALYZE tempmids');
		
		$TABLES{'tempmids'} = 1;
		$TABLES{'muv'} = 1;
		$TABLES{'ns'} = 1;
	}

	# there is no need for prior membertype unless we are providing a report to a Partner
	if ($login_party->{'membertype'} eq 'v' && $gs->in_array($param{'report_type_translated'}, [qw/m s/]))	# we don't need the prior membertype for Trial Partners or Partners or AGs of MAs, do we???
	{
		$COLS{'prior_membertype'} = 1;
		$TABLES{'mt2'} = 1;
		$TABLES{'mpm'} = 1;
	}
	
	if ($param{'report_type_translated'} eq 'ma')
	{
		$COLS{'company_name'} = 1;
		$TABLES{'ma'} = 1;
		$ORDER_BY_COLS{'company_name'} = 1;
	}
	
	if ($param{'report_type_translated'} eq 'ag')
	{
		$COLS{'company_name'} = 1;
		$ORDER_BY_COLS{'company_name'} = 1;
	}
	
	if ($param{'report_type'} eq 'tp')
	{
		$COLS{'upgrade_deadline'} = 1;
		$TABLES{'odp'} = 1;
		$TABLES{'ns'} = 1;
	}
}

sub DESTROY
{
	$db->disconnect() if $db;
}

sub _utf8_off
{
        my $hr = shift;
        foreach (keys %{$hr})
        {
                if (ref $hr->{$_} eq 'HASH')
                {
                	_utf8_off($hr->{$_});
                }
                else
                {
                	Encode::_utf8_off($hr->{$_});
                }
        }
}

1;

# 08/30/06 added AGs to the list of frontline members AND gave AGs access to the report as well
###### 06/11/09 added the new ma membertype to the viewable ones, also revised the way the list is used in the SQL
# removed the ma membertype as they end up in the mailing list
###### 04/21/11 added a link on the alias and chanaged a/c to skype name
###### 05/02/11 removed the spaces in the CSV list
# 04/02/12 made it available to the 'm' membertype
# 04/03/12 made the report types align with the current business logic... ie. changed the Member report to just s membertypes and the Affiliate report to m membertypes
###### 10/01/12 ripped out lots of the trial position stuff
###### 10/17/12 Made the default report pull membertype 'm' instead of AGs... geesh
###### Also ripped out a bunch of needless joins in the COUNT queries in Report_Members()
###### 04/09/13 made the access check use the network.access_chk1() routine instead of the G_S method
###### 11/02/13 added in the concept of the Trial Partner members\
###### 12/02/14 merely moved a few lines around in Report_Members() to avoid disconnecting the DB with an active statement handle when templates are not available for a particular membertype
