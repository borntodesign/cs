package Specials;

##############################################################
# This class handles information pertaining to "Special", or
# "Top Picks".
##############################################################
use strict;

###############################
# The constructor
#
# Parameters
# Param db obj The database object.
#
# Return $this
###############################
sub new
{
    my $class			= shift;
    my $this			= {};
    bless($this, $class);
    $this->{db}			= shift;

    if(! $this->{db})
    {
    	die 'The database object is a required parameter.';
    }

    $this->{version}	= '0.1';
    $this->{name}		= 'Specials Class';
    $this->{members}	= {};
	$this->{table_mapping} = {
							IT=>'_it',
							DZ=>'_dz',
							FR=>'_fr',
							TN=>'_tn',
							US=>'_us',
							NL=>'_nl',
							BE=>'_be',
							MA=>'_ma',
							AN=>'_an'
						};

    $this->{db}->{RaiseError} = 1;

    return $this;
}

###############################
# __getMemberShoppingPreferencesTable:
#
# Return
###############################
#sub __getMemberShoppingPreferencesTable
#{
#	my $this		= shift;
#	my $member_id	= shift;
#	my $member_shopping_preferences_table = 'member_shopping_preferences';
#
#	eval{
#			my $sth = $this->{db}->prepare('SELECT country from members WHERE id = ?');
#
#			$sth->execute($member_id);
#
#			my $row = $sth->fetchrow_hashref;
#
#			if(exists $this->{table_mapping}->{uc($row->{country})})
#			{
#				return $member_shopping_preferences_table . $this->{table_mapping}->{uc($row->{country})};
#			}
#	};
#	if($@){
#		return $member_shopping_preferences_table;
#	}
#
#	return $member_shopping_preferences_table;
#
#}


###############################
# processUserSpecialPreferences: Compile the users
# prefered mall categories, by comparing the users
# questionnaire answers to the associated mall categores,
# then insert the answers into a database table (member_shopping_preferences).
#
# Return null on sucess
###############################
sub processUserSpecialPreferences
{
	my $this = shift;
	my $member_id = shift;



	if(! defined $member_id || $member_id =~ /\D|^$/)
	{
		die 'The Member ID is not in the correct format, or empty.  I am expecting a number.';
	}


	eval{

			$this->{db}->do("
								DELETE
								FROM
									member_shopping_preferences
								WHERE
									member_id = ?
							", undef, ($member_id));

			$this->{db}->do("
							INSERT
							INTO
								member_shopping_preferences
								(member_id, incentive_mallcat_id, number_of_time_chosen, category, last_updated)
							SELECT
								qua.members_id,
								qal.incentive_mallcat_id,
								COUNT(qal.incentive_mallcat_id) AS number_of_time_chosen,
								qal.category,
								max(qus.updated)
							FROM
								questionnaires_user_answers AS qua
								JOIN
								questionnaires_user_status AS qus
								ON
								qus.userid = qua.members_id
								JOIN
								questionnaires_answers_links AS qal
								ON
								qal.questionnaires_answers_id = qua.questionnaires_answers_id
							WHERE
								qua.members_id = ?
								AND
								qus.completed IS NOT NULL
							GROUP BY
								qua.members_id,
								qal.incentive_mallcat_id,
								qal.category
							", undef, ($member_id));
	};

	if($@)
	{
		die $this->{db}->errstr;
	}

	return;
}


###############################
# __getMemberPreferences: Check the member
# cache for member information, if it does
# not exist, the database is queried, and the
# member cache is built.
#
# Parameters
# member_id int
# force		bool	Force a query, and refresh a users cache.
#
# Return hash
###############################
sub __getMemberPreferences
{
	my $this		= shift;
	my $member_id	= shift;
	my $force		= shift;
	my $sth 		= '';
#	$this->{members}

	# Check the validity of the Member ID, it should be numberic, but not zero
	if($member_id =~ /\D+|^$|^0$/)
	{
		return 'The Member ID must be specified';
	}

	if($force || ! exists $this->{members}->{$member_id})
	{
		eval{
			my $sth = $this->{db}->prepare('
									SELECT
										incentive_mallcat_id,
										number_of_time_chosen,
										category
									FROM
										member_shopping_preferences AS msp
									WHERE
										msp.member_id = ?
								');
		};

		if($@)
		{
			die $this->{db}->errstr;
		}


		eval{
			$sth->execute($member_id);

			while(my $row = $sth->fetch_hashref())
			{
				$this->{members}->{$member_id}->{$row->{category}}->{$row->{incentive_mallcat_id}}->{count} = $row->{number_of_time_chosen};
			}
		};

		if($@)
		{
			die $sth->errstr;
		}
	}

	return $this->{members}->{$member_id};
}

sub getMemberTopPicksXML
{

	use XML::Simple;
	use CGI;


	my $this			= shift;
	my $member_id		= shift;
	my $mall_id			= shift;
	my $sort_by_date	= shift;
	my $incentives 		= '';

	if(! defined $member_id || $member_id =~ /\D|^$/)
	{
		die 'The Member ID is not in the correct format, or empty.  I am expecting a number.';
	}

	if(! defined $mall_id || $mall_id =~ /\D|^$/)
	{
		die 'The Mall ID is not in the correct format, or empty.  I am expecting a number.';
	}

	my $return_xml = '';
	my $pre_formatted_hash = {};



	my $sth = '';

	# Check the validity of the Member ID, it should be numberic, but not zero
	if($member_id =~ /\D+|^$|^0$/)
	{
		die 'The Member ID must be specified';
	}

	# Check the validity of the Member ID, it should be numberic, but not zero
	if($mall_id =~ /\D+|^$|^0$/)
	{
		die 'The Mall ID must be specified';
	}

	#my $member_shopping_preferences_table = $this->__getMemberShoppingPreferencesTable($member_id);

	eval{

		$sth = $this->{db}->prepare("
		SELECT
			incentive_mallcat_id
		FROM
			member_shopping_preferences
		WHERE
			member_id = ?
			AND
			category = 'incentive'
		");

			$sth->execute($member_id);

			while(my $row = $sth->fetchrow_hashref())
			{
				$incentives .= "$row->{incentive_mallcat_id},";
			}

			$incentives =~ s/,$//;

			$incentives = 'NULL' if(!$incentives);

	};
	if($@)
	{
		die $this->{db}->errstr;
	}


	eval{



			my $sort_fields = $sort_by_date?'v_table.end_date, v_table.vendor_name':'v_table.vendor_name, v_table.end_date';

#			$sth = $this->{db}->prepare("
#											SELECT
#												DISTINCT(mve.pk),
#												v.vendor_id,
#												v.vendor_name,
#												v.url,
#												mve.ranking,
#												mve.val_text,
#												mve.end_date,
#											(
#												SELECT
#													i_mve.val_text
#												FROM
#													mall_vendor_extensions AS i_mve
#												WHERE
#													i_mve.exttype = 2
#													AND
#													i_mve.vendor_id = mve.vendor_id
#													AND
#													(
#														NOW() between i_mve.start_date and i_mve.end_date
#														OR
#														(
#															i_mve.start_date <= NOW()
#															AND
#															i_mve.end_date IS NULL
#														)
#													)
#												LIMIT 1
#											) as banner
#											FROM
#												member_shopping_preferences AS msp
#												JOIN
#												mall_vendor_extensions_links AS mvel
#												ON
#												mvel.incentive_mallcat_id = msp.incentive_mallcat_id
#												AND
#												mvel.category=msp.category
#												JOIN
#												mall_vendor_extensions_incentive_links AS mveil
#												ON
#												mveil.pk = mvel.pk
#												JOIN
#												mall_vendor_extensions AS mve
#												ON
#												mve.pk = mvel.pk
#												JOIN
#												mall_vendors AS mv
#												ON
#												mv.vendor_id = mve.vendor_id
#												JOIN vendors AS v
#												ON
#												v.vendor_id = mv.vendor_id
#											WHERE
#												mve.exttype IN (3,4,5)
#												AND
#												mveil.incentive_mallcat_id IN ($incentives)
#												AND
#												mve.ranking > 0
#												AND
#												NOW() between mve.start_date and mve.end_date
#												AND
#												mve.mall_id = ?
#												AND
#												msp.member_id = ?
#												AND
#												mv.active = TRUE
#												AND
#												mve.void = FALSE
#											ORDER BY
#												mve.ranking desc ,
#												$sort_fields
#											");

			$sth = $this->{db}->prepare("
									SELECT
									DISTINCT(v_table.pk) AS pk,
									v_table.vendor_id,
									v_table.vendor_name,
									v_table.url,
									(v_table.ranking + CAST(SUM(v_table.incentive_ranking) AS smallint)) AS ranking,
									v_table.val_text,
									v_table.end_date,
									i_mve.val_text AS banner
									FROM
									(
											SELECT
												DISTINCT(mve.pk) AS pk,
												mveil.incentive_mallcat_id,
												CASE WHEN mveil.incentive_mallcat_id IS NULL THEN 0 ELSE 1 END AS incentive_ranking,
												v.vendor_id,
												v.vendor_name,
												v.url,
												mve.ranking,
												mve.val_text,
												mve.end_date
											FROM
												member_shopping_preferences AS msp
												JOIN
												mall_vendor_extensions_links AS mvel
												ON
												mvel.incentive_mallcat_id = msp.incentive_mallcat_id
												AND
												mvel.category=msp.category
												LEFT JOIN
												mall_vendor_extensions_incentive_links AS mveil
												ON
												mveil.pk = mvel.pk
												AND
												mveil.incentive_mallcat_id IN ($incentives)
												JOIN
												mall_vendor_extensions AS mve
												ON
												mve.pk = mvel.pk
												JOIN
												mall_vendors AS mv
												ON
												mv.vendor_id = mve.vendor_id
												JOIN vendors AS v
												ON
												v.vendor_id = mv.vendor_id
											WHERE
												mve.exttype IN (3,4,5)
												AND
												mve.ranking > 0
												AND
												NOW() between mve.start_date and mve.end_date
												AND
												mve.mall_id = ?
												AND
												msp.member_id = ?
												AND
												mv.active = TRUE
												AND
												mve.void = FALSE
											GROUP BY
												mve.pk,
												mveil.incentive_mallcat_id,
												v.vendor_id,
												v.vendor_name,
												v.url,
												mve.ranking,
												mve.val_text,
												mve.end_date
										) AS v_table
										JOIN
											mall_vendor_extensions AS i_mve
											ON
												i_mve.vendor_id = v_table.vendor_id
											AND
												i_mve.mall_id = ?
											AND
												i_mve.exttype = 2
										GROUP BY
											v_table.pk,
											v_table.vendor_id,
											v_table.vendor_name,
											v_table.url,
											v_table.ranking,
											v_table.val_text,
											v_table.end_date,
											i_mve.val_text
										ORDER BY
											ranking desc,
											$sort_fields
			");
	};
	if($@)
	{
		die $this->{db}->errstr;
	}

	eval{


		$sth->execute($mall_id, $member_id, $mall_id);

		while(my $row = $sth->fetchrow_hashref())
		{
			if(exists $pre_formatted_hash->{vendor} && $pre_formatted_hash->{vendor}[$#{$pre_formatted_hash->{vendor}}]->{name}[0] eq $row->{vendor_name})
			{
				my $stuff = {
 								text=>[$row->{val_text}],
 								end_date=>[$row->{end_date}]
 							};
				

				push @{$pre_formatted_hash->{vendor}[$#{$pre_formatted_hash->{vendor}}]->{specials}[0]->{special}}, $stuff;
			}
			else
			{
							my $banner_location = $row->{banner};
							$banner_location =~ s/<\s*img\s+.*src\s*=\s*['|"]*(.+?)['|"]*\s+.*>/$1/;

							my $stuff = {
				 							name=>[$row->{vendor_name}],
								 			vendor_id=>[$row->{vendor_id}],
								 			banner=>[$banner_location],
											url=>[$row->{url}],
											specials=> [
															{
																special=> [
 																			{
																				text=>[$row->{val_text}],
																				end_date=>[$row->{end_date}]
																			},
			 															   ],
															}
														],
			 							};

				push @{$pre_formatted_hash->{vendor}}, $stuff;
			}
		}


		if(exists $pre_formatted_hash->{vendor} && scalar(@{$pre_formatted_hash->{vendor}}))
		{
			my $output_html = '';

			foreach my $vendors_hash (@{$pre_formatted_hash->{vendor}})
			{
				$vendors_hash->{name}[0] =~ s/&/&amp;/g;


				$output_html .=<<EOT;
				<tr>
					<td class="storeInfo">
					<a class="vendor" href="/cgi-bin/rd/14,$vendors_hash->{vendor_id}[0],mall_id=$mall_id">
						<img src="$vendors_hash->{banner}[0]" alt="$vendors_hash->{name}[0]" border="0" />
					</a>
					</td>
					<td style="width:10px" colspan="2"> </td>
					<td>
						<ul>
EOT


				foreach my $special_hash (@{$vendors_hash->{specials}[0]->{special}})
				{

					$output_html .=<<EOT;

							<li class="top-picks">
								$special_hash->{text}[0]
								<span style="padding-left:10px" class="rppp-col">
									($special_hash->{end_date}[0])
								</span>
							</li>
EOT
				}


				$output_html .=<<EOT;
						</ul>
					</td>
				</tr>
				<tr><td colspan="4" style="background-color: #C0C0C0; height:1px;"> </td></tr>
EOT


			}

			$return_xml = "<vendors>$output_html</vendors>";
			#XML::Simple::XMLout($pre_formatted_hash, RootName=>'vendors');

		}
		else
		{
			$return_xml ="<none>There are no results</none>";
		}
	};

	if($@)
	{
		if(defined $sth->errstr)
		{
			die $sth->errstr;
		}
		else
		{
			die $@;
		}
	}


	return $return_xml;

}


1;
